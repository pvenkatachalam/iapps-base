﻿--Patch script for 5.1.0726.01
GO
PRINT 'Updating the version of the products to 5.1.09XX.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.09XX.1'
GO
PRINT 'Altering procedure Taxonomy_delete'
GO
ALTER PROCEDURE [dbo].[Taxonomy_Delete] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id		uniqueidentifier,
	@ModifiedBy    	uniqueidentifier,
	@ApplicationId uniqueidentifier,
	@Status int output
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE	@Now datetime,
		@Stmt 	VARCHAR(36),
		@DeleteStatus int,
		@Nodes xml,
		@Lft bigint,
		@Rgt bigint

BEGIN
	-- if Id is specified, ensure exists 
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM   COTaxonomy WHERE  Id = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END
	
	SET @Now =getutcdate()
	
	--changes taxonomy status to deleted in COTaxonomy table
	--set @DeleteStatus =dbo.GetDeleteStatus()
	
	--select @Nodes = (select Id from GetTreeByHierarchy(@Id)  for xml auto)
	--update COTaxonomy set ModifiedDate = @Now, ModifiedBy =@ModifiedBy, Status = @DeleteStatus
	--		where Id in (SELECT T.c.value('@Id','uniqueidentifier') as id      
	--			FROM   @Nodes.nodes('/GetTreeByHierarchy') T(c))

	set @Status = dbo.GetDeleteStatus()
	
	SET @Lft = (Select LftValue from COTaxonomy Where Id=@Id And SiteId in (SELECT SiteId from dbo.GetVariantSites(@ApplicationId)))
	SET @Rgt = (Select RgtValue from COTaxonomy Where Id=@Id And SiteId in (SELECT SiteId from dbo.GetVariantSites(@ApplicationId)))

	Delete From COTaxonomy
	Where LftValue Between @Lft and @Rgt
	AND SiteId in (SELECT SiteId from dbo.GetVariantSites(@ApplicationId))
	
	-- Adjust lft and rgt
	Update COTaxonomy Set LftValue= case when LftValue >@Rgt then LftValue - (@Rgt -@Lft) -1  else LftValue end,
						  RgtValue= case when RgtValue >@Rgt then RgtValue - (@Rgt -@Lft) -1  else RgtValue end
				Where SiteId in (SELECT SiteId from dbo.GetVariantSites(@ApplicationId))
End
GO