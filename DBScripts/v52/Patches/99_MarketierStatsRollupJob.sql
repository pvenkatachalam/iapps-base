declare @dbname varchar(100)
set @dbname = db_name()

declare @loggedInUser varchar(100)
set @loggedInUser = SYSTEM_USER
declare @jobname varchar(100)
set @jobname = 'MarketierStatsRollup'
declare @step_id int
declare @freq_subday_interval int

set @step_id =1
set @freq_subday_interval =3

DECLARE @jobId binary(16) 
SELECT @jobId = job_id FROM msdb.dbo.sysjobs WHERE (name = @jobname)
 IF (@jobId IS  NULL) 
 begin
	 BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0
	/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 08/09/2013 14:52:17 ******/
	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	END

	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'MarketierStatsRollup', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'No description available.', 
			@category_name=N'[Uncategorized (Local)]', 
			@owner_login_name=@loggedInUser, @job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	
	/****** Object:  Step [Samueli]    Script Date: 08/09/2013 14:52:17 ******/
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@dbname, 
			@step_id=@step_id, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=N'exec [dbo].[EmailStatsRollup]', 
			@database_name=@dbname, 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Schedule1', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=8, 
			@freq_subday_interval=@freq_subday_interval, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20130809, 
			@active_end_date=99991231, 
			@active_start_time=0, 
			@active_end_time=235959, 
			@schedule_uid=N'331cf7ba-10ae-4b0a-8cf6-41531876d868'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:
 end
 else
 begin
	SELECT  @step_id=MAX(STEP.STEP_ID)+1 FROM Msdb.dbo.SysJobs JOB INNER JOIN Msdb.dbo.SysJobSteps STEP ON STEP.Job_Id = @jobId
    
    declare @previous_step_id int
    select @previous_step_id = step_id  from Msdb.dbo.SysJobSteps  where Job_Id = @jobid and on_success_action=1
    if(not exists(select 1  from Msdb.dbo.SysJobSteps  where Job_Id = @jobid and database_name=@dbname))
    begin
		EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@dbname, 
				@step_id=@step_id, 
				@cmdexec_success_code=0, 
				@on_success_action=1, 
				@on_success_step_id=0, 
				@on_fail_action=3, 
				@on_fail_step_id=0, 
				@retry_attempts=0, 
				@retry_interval=0, 
				@os_run_priority=0, @subsystem=N'TSQL', 
				@command=N'exec [dbo].[EmailStatsRollup]', 
				@database_name=@dbname, 
				@flags=0
				
	 
		exec msdb.dbo.sp_update_jobstep @job_id = @jobid,  @step_id = @previous_step_id, @on_success_action = 3 
	end
	else
	begin
		print 'This database step is already present in the MarketierEmailstatsrollup job.'
	end

 end
 
 

