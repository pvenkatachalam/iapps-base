INSERT INTO TADistributionListSite(Id,DistributionListId,SiteId)
SELECT NEWID(),* FROM 
(Select Id,ApplicationId FROM TADistributionLists
Except
Select DistributionListId,SiteId FROM TADistributionListSite
)T

-- mark all the emails which does not have @ as inactive
Update MKContact SET Status=3 Where Email not like '%@%'

GO
ALTER PROCEDURE [dbo].[Contact_ContactAttributeSearch]
    (
      @ContactAttributeSearchXml XML ,
      @ApplicationId UNIQUEIDENTIFIER
    )
AS 
    BEGIN

        DECLARE @ContactSearch TABLE
            (
              ConditionId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeValueID UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(1000) ,
              Value2 NVARCHAR(1000) ,
              Operator NVARCHAR(50) ,
              DataType NVARCHAR(50) ,
              ValueCount INT
            )
    
       
        CREATE TABLE #tbltest
            (
              ConditionId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(1000) ,
              Value2 NVARCHAR(1000) ,
              Operator NVARCHAR(50) ,
              DataType NVARCHAR(50) ,
              XmlValue XML
            )
        PRINT 'INSERT INTO #tblTest' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)
            
        INSERT  INTO #tbltest
                ( ConditionId ,
                  AttributeId ,
                  Value1 ,
                  Value2 ,
                  Operator ,
                  DataType ,
                  XmlValue
                    
                
                )
                SELECT  NEWID() AS ConditionId ,
                        col.value('@AttributeId', 'uniqueidentifier') AS AttributeId ,
                      --  sref.value('(text())[1]', 'uniqueidentifier') AS what ,
                        col.value('@Value1', 'nvarchar(max)') AS Value1 ,
                        col.value('@Value2', 'nvarchar(max)') AS Value2 ,
                        col.value('@Operator', 'nvarchar(50)') AS Operator ,
                        'System.String' AS DataType ,
                        col.query('AttributeValueId') AS XmlValue
                FROM    @ContactAttributeSearchXml.nodes('/ContactAttributeSearch/AttributeSearchItem') tab ( col )
                      --  CROSS APPLY col.nodes('AttributeValueId') AS AV ( sref )
          
          
          
          
        PRINT 'INSERT INTO @ContactSearch'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operator ,
                  DataType ,
                  ValueCount
                
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        pref.value('(text())[1]', 'uniqueidentifier') ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        COUNT(pref.value('(text())[1]', 'uniqueidentifier')) OVER ( PARTITION BY T.ConditionId,
                                                              T.AttributeId ) AS ValueCount
                FROM    #tbltest T
                        CROSS APPLY XmlValue.nodes('AttributeValueId') AS Value ( pref )
    
        PRINT 'INSERT INTO @ContactSearch 2'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operator ,
                  DataType ,
                  ValueCount
                
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        NULL ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        1
                FROM    #tblTest T
                        LEFT JOIN @ContactSearch S ON S.ConditionId = T.ConditionId
                WHERE   S.ConditionId IS NULL


    
        PRINT 'UPDATE @ContactSearch' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        UPDATE  CS
        SET     CS.DataType = ADT.TYPE
        FROM    @ContactSearch CS --INNER JOIN ( SELECT ConditionId ,
                --                    MAX(ValueCount) OVER ( PARTITION BY ConditionId ) AS MaxCount
                --             FROM   @ContactSearch
                --           ) CS2 ON CS2.ConditionId = CS.ConditionId
                INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
                INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 
              
            IF NOT EXISTS ( SELECT TOP 1
                                *
             FROM    [tblContactPropertiesAndAttributes] ) 
            BEGIN
                EXEC dbo.ContactDto_RebuildContactData
            END
              
        PRINT 'DECLARE @ModifiedDate DATETIME'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)
        DECLARE @ModifiedDate DATETIME
        SELECT TOP 1
                @ModifiedDate = ISNULL(CachedDate, '1-1-2000')
        FROM    [tblContactPropertiesAndAttributes]
        
        
        
        PRINT 'INSERT INTO NEWDATA' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)
        SELECT  *
        INTO    #NewData
        FROM    vwContactPropertiesAndAttributes CA
        WHERE   CA.ModifiedDate > @ModifiedDate
        OPTION  ( RECOMPILE ) ;
              
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #NewData ) 
            BEGIN
                    
                DELETE  FROM dbo.tblContactPropertiesAndAttributes
                WHERE   ContactId IN ( SELECT   ContactId
                                       FROM     #NewData )
                INSERT  INTO dbo.tblContactPropertiesAndAttributes
                        ( ContactId ,
                          AttributeId ,
                          AttributeEnumId ,
                          Value ,
                          Notes ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          DataType ,
                          CachedDate
                            
                        )
                        SELECT  ContactId ,
                                AttributeId ,
                                AttributeEnumId ,
                                Value ,
                                Notes ,
                                CreatedDate ,
                                CreatedBy ,
                                ModifiedDate ,
                                ModifiedBy ,
                                DataType ,
                                @ModifiedDate
                        FROM    #NewData
                    
            END
                
              
        CREATE TABLE #LocalTemp
            (
              Id UNIQUEIDENTIFIER ,
              AttributeValueId UNIQUEIDENTIFIER ,
              ConditionId UNIQUEIDENTIFIER
            )
     
----------------------------------------------------------------------------------------------------------------------------------		
        INSERT  INTO #LocalTemp
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   CPA.AttributeEnumId IS NOT NULL
                        AND ( ( CS.Operator = 'opOperatorEqual'
                                AND CPA.AttributeEnumId = CS.AttributeValueID
                              )
                              OR ( CS.Operator = 'opOperatorNotEqual'
                                   AND CPA.AttributeEnumId != CS.AttributeValueID
                                 )
                            )
                UNION ALL
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   ( CPA.DataType = 'System.String'
                          AND CS.AttributeValueID IS  NULL
                          AND ( CS.Operator = 'opOperatorContains'
                                AND CPA.VALUE LIKE '%' + CS.Value1 + '%'
                                OR CS.Operator = 'opOperatorEqual'
                                AND CPA.VALUE = CS.Value1
                                OR CS.Operator = 'opOperatorBetween'
                                AND CPA.VALUE BETWEEN CS.Value1 AND CS.Value2
                                OR CS.Operator = 'opOperatorLessThanEqual'
                                AND CPA.VALUE <= CS.Value1
                                OR CS.Operator = 'opOperatorLessThan'
                                AND CPA.VALUE <= CS.Value1
                                OR CS.Operator = 'opOperatorGreaterThanEqual'
                                AND CPA.VALUE >= CS.Value1
                                OR CS.Operator = 'opOperatorGreaterThan'
                                AND CPA.VALUE > CS.Value1
                                OR CS.Operator = 'opOperatorNotEqual'
                                AND CPA.VALUE != CS.Value1
								OR CS.Operator = 'opOperatorDoesNotContain'
								AND CPA.VALUE NOT LIKE '%' + CS.Value1 + '%' 
                              )
                        )
                UNION ALL
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   ( CPA.DataType = 'System.DateTime'
                          AND CS.DataType = 'System.DateTime'
                          AND CS.AttributeValueID IS  NULL
                          AND ( CS.Operator = 'opOperatorEqual'
                                AND CAST(CPA.VALUE AS DATETIME) = CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorBetween'
                                AND CAST(CPA.VALUE AS DATETIME) BETWEEN CAST(CS.Value1 AS DATETIME)
                                                              AND
                                                              CAST(CS.VALUE2 AS DATETIME)
                                OR CS.Operator = 'opOperatorLessThanEqual'
                                AND CAST(CPA.VALUE AS DATETIME) <= CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorLessThan'
                                AND CAST(CPA.VALUE AS DATETIME) <= CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorGreaterThanEqual'
                                AND CAST(CPA.VALUE AS DATETIME) >= CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorGreaterThan'
                                AND CAST(CPA.VALUE AS DATETIME) > CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorNotEqual'
                                AND CAST(CPA.VALUE AS DATETIME) != CAST(CS.Value1 AS DATETIME)
                              )
                        )
                UNION ALL
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   ( CS.DataType = 'System.Double'
                          OR CS.DataType = 'System.Int32'
                        )
                        AND CS.AttributeValueID IS  NULL
                        AND ( CS.Operator = 'opOperatorEqual'
                              AND CAST(CPA.Value AS MONEY) = CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorBetween'
                              AND CAST(CPA.Value AS MONEY) BETWEEN CAST(CS.Value1 AS MONEY)
                                                           AND
                                                              CAST(CS.VALUE2 AS MONEY)
                              OR CS.Operator = 'opOperatorLessThanEqual'
                              AND CAST(CPA.Value AS MONEY) <= CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorLessThan'
                              AND CAST(CPA.Value AS MONEY) <= CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorGreaterThanEqual'
                              AND CAST(CPA.Value AS MONEY) >= CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorGreaterThan'
                              AND CAST(CPA.Value AS MONEY) > CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorNotEqual'
                              AND CAST(CPA.Value AS MONEY) != CAST(CS.Value1 AS MONEY)
                            )

        PRINT 'CREATE TABLE #ContactIds' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        CREATE TABLE #ContactIds
            (
              ContactId UNIQUEIDENTIFIER
            )
        INSERT  INTO #ContactIds
                ( ContactId
                
                )
                --SELECT DISTINCT Id FROM #LocalTemp
                SELECT DISTINCT
                        TV2.Id
                FROM    ( SELECT    * ,
                                    COUNT(AttributeValueId) OVER ( PARTITION BY ConditionID,
                                                              Id ) AS ValueCount
                          FROM      ( SELECT  DISTINCT
                                                *
                                      FROM      #LocalTemp
                                    ) TV
                        ) TV2
                        INNER JOIN @ContactSearch CS ON ( CS.ConditionId = TV2.ConditionId
                                                          AND CS.ValueCount = TV2.ValueCount
                                                        )
                                                        OR TV2.AttributeValueID IS NULL 
                                                        
        PRINT 'ECLARE @currentRecords INT'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)                                  
       --   SELECT * FROM #ContactIds
        DECLARE @currentRecords INT
        SET @currentRecords = ( SELECT  COUNT(ID)
                                FROM    #tempContactSearchOutput
                              )

       
        IF @currentRecords > 0 
            BEGIN
                PRINT '@currentRecords > 0 '
                    + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)      
                DELETE  T
                FROM    #tempContactSearchOutput t
                        LEFT JOIN #ContactIds LT ON LT.ContactId = T.Id
                WHERE   LT.ContactId IS NULL
            END
        ELSE 
            BEGIN       
                PRINT ' INSERT  INTO #tempContactSearchOutput'
                    + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)                
                INSERT  INTO #tempContactSearchOutput
                        SELECT  ContactId
                        FROM    #ContactIds
            END
        SELECT  @@ROWCOUNT
    END


	GO
	
UPDATE  dbo.CTSearchQuery
SET     SearchXml = CAST(REPLACE(CAST(SearchXml AS NVARCHAR(MAX)),
                                 '922337203685477.5807', '9999999') AS XML)

GO
PRINT N'Adding WebApiAdministrator permission...';
DECLARE @MasterSiteId uniqueidentifier
DECLARE @WebApiAdminGroupId uniqueidentifier
DECLARE @WebApiAdminId uniqueidentifier
DECLARE @CMSProductId uniqueidentifier

SET @CMSProductId ='CBB702AC-C8F1-4C35-B2DC-839C26E39848'
SET @WebApiAdminId = (SELECT TOP 1 Id FROM USUser WHERE UserName = 'WebApiAdmin')
SET @WebApiAdminGroupId = (SELECT TOP 1 Id FROM USGroup WHERE Title = 'WebApi Administrator')

IF @WebApiAdminId IS NOT NULL AND @WebApiAdminGroupId IS NOT NULL
BEGIN
	DECLARE MASTER_SITES Cursor 
	FOR
	SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId

	Open MASTER_SITES 
	Fetch NEXT FROM MASTER_SITES INTO @MasterSiteId

	While (@@FETCH_STATUS <> -1)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM USMemberGroup WHERE MemberId = @WebApiAdminId AND GroupId = @WebApiAdminGroupId AND ApplicationId = @MasterSiteId)
			INSERT INTO USMemberGroup(MemberId, MemberType, GroupId, ApplicationId) 
			VALUES (@WebApiAdminId, 1, @WebApiAdminGroupId, @MasterSiteId) -- global group	

		IF NOT EXISTS (SELECT 1 FROM USSiteUser WHERE SiteId = @MasterSiteId AND UserId = @WebApiAdminId)
			INSERT INTO USSiteUser (SiteId, UserId, IsSystemUser, ProductId)
			VALUES (@MasterSiteId, @WebApiAdminId, 1, @CMSProductId ) --- inserting into site user to access the product
			
		Fetch NEXT FROM MASTER_SITES INTO @MasterSiteId 
	END
	CLOSE MASTER_SITES
	DEALLOCATE MASTER_SITES
END
GO
PRINT 'Altering procedure Task_History_Update'
GO
ALTER PROCEDURE [dbo].[Task_History_Update]
(
	@TaskHistoryId uniqueidentifier,
	@EndTime datetime,
	@Result int,
	@Exceptions varchar(max)
)
AS
BEGIN
	DECLARE @stmt	  varchar(256),
			@error	  int

	SET @stmt = 'Task History Update'
	
	UPDATE TATaskHistory SET
		EndTime = @EndTime,
		Result = @Result,
		Exceptions = @Exceptions
	WHERE
		Id = @TaskHistoryId

	SELECT @error = @@error

	IF @error <> 0
	BEGIN
		rollback
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	END
	
END
GO

GO
------------------------------------------------START Updating the pending scheduled campaigns to sent based onthe rundate --------------------------------

PRINT 'backing up mkcampaign table'

if(object_id('MKCamapign_V52_BKUP_11_21_2014') is null)
select * into MKCamapign_V52_BKUP_11_21_2014 from MKCampaign


print 'updating the status'

update MKCampaign set Status = 3 where Id in (
select Distinct C.Id
from MKCampaign C 
JOIN MKCampaignAdditionalInfo AI ON C.Id = AI.CampaignId
JOIN TASchedule S ON AI.ScheduleId = S.Id 
JOIN (Select CampaignId,max(RunDate)RunDate from MKCampaignRunHistory T Group By CampaignId ) H ON C.Id = H.CampaignId
where (C.Status=2 OR C.Status=5 OR C.Status=7) 
--and dateadd(millisecond, -datepart(millisecond, H.RunDate), H.RunDate)  >= dateadd(millisecond, -datepart(millisecond, S.LastRunTime), S.LastRunTime)
and (EndDate< GETUTCDATE() OR (MaxOccurrence!=0 AND  MaxOccurrence <= RunCount)
or (DateDiff(DAY,LastRunTime,GETUTCDATE())>365 AND DateDiff(DAY,RunDate,GETUTCDATE())>365))

)

------------------------------------------------ END Updating the pending scheduled campaigns to sent based onthe rundateselect * from MKCamapign_V52_BKUP_11_21_2014t --------------------------------
GO


