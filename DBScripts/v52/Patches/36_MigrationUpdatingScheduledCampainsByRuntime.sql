﻿
GO
------------------------------------------------START Updating the pending scheduled campaigns to sent based onthe rundate --------------------------------

PRINT 'backing up mkcampaign table'

if(object_id('MKCamapign_V52_BKUP_11_21_2014') is null)
select * into MKCamapign_V52_BKUP_11_21_2014 from MKCampaign


print 'updating the status'

update MKCampaign set Status = 3 where Id in (
select Distinct C.Id
from MKCampaign C 
JOIN MKCampaignAdditionalInfo AI ON C.Id = AI.CampaignId
JOIN TASchedule S ON AI.ScheduleId = S.Id 
JOIN (Select CampaignId,max(RunDate)RunDate from MKCampaignRunHistory T Group By CampaignId ) H ON C.Id = H.CampaignId
where (C.Status=2 OR C.Status=5 OR C.Status=7) 
--and dateadd(millisecond, -datepart(millisecond, H.RunDate), H.RunDate)  >= dateadd(millisecond, -datepart(millisecond, S.LastRunTime), S.LastRunTime)
and (EndDate< GETUTCDATE() OR (MaxOccurrence!=0 AND  MaxOccurrence <= RunCount)
or (DateDiff(DAY,LastRunTime,GETUTCDATE())>365 AND DateDiff(DAY,RunDate,GETUTCDATE())>365))

)

------------------------------------------------ END Updating the pending scheduled campaigns to sent based onthe rundateselect * from MKCamapign_V52_BKUP_11_21_2014t --------------------------------
GO

