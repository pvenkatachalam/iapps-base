﻿

GO
--Backup security level mapping table
IF OBJECT_ID(N'[dbo].[USSecurityLevelObject_BKUP_02_25_2014]', 'U') IS NULL
	SELECT * INTO USSecurityLevelObject_BKUP_02_25_2014 FROM USSecurityLevelObject
GO

--Deleting the security level mapping where there is no actual security level; 
DELETE FROM USSecurityLevelObject WHERE SecurityLevelId NOT IN (SELECT Id FROM USSecurityLevel)
GO
IF OBJECT_ID('Membership_GetSessionId','P') IS NOT NULL
	DROP PROCEDURE Membership_GetSessionId
GO
IF OBJECT_ID('Membership_UpdateSessionId','P') IS NOT NULL
	DROP PROCEDURE Membership_UpdateSessionId
GO
IF OBJECT_ID('Membership_GetLastPasswords','P') IS NOT NULL
	DROP PROCEDURE Membership_GetLastPasswords
GO
IF OBJECT_ID('Membership_LogPassword','P') IS NOT NULL
	DROP PROCEDURE Membership_LogPassword
GO
IF OBJECT_ID('Country_GetState','P') IS NOT NULL
	DROP PROCEDURE Country_GetState
GO
PRINT 'Creating current status=4 and order next status=3'
GO
--DATA Modifications
IF NOT EXISTS(SELECT 1 FROM OROrderNextStatus WHERE CurrentStatusId=4 AND NextStatusId=3)
BEGIN
	DECLARE @Id int
	SET @Id =(SELECT isnull(MAX(Id),0) FROM OROrderNextStatus) +1
	INSERT INTO OROrderNextStatus(Id,CurrentStatusId,NextStatusId)
	Values(@Id,4,3)
END	
GO
PRINT 'Updating INRestockingStatus '
GO
Update INRestockingStatus SET Name='Canceled',CustomerFriendlyName='Canceled' 
	Where Id=3
	GO
Update INRestockingStatus SET Name='In Progress',CustomerFriendlyName='In Progress'
	Where Id=4
	GO
	PRINT 'Updating site settings...'
	GO
	DECLARE @SiteId UNIQUEIDENTIFIER,@SettingTypeId INT,@Sequence INT

DECLARE SITE_CURSOR_500 Cursor 
FOR
SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId -- Only Master sites

Open SITE_CURSOR_500 
Fetch NEXT FROM SITE_CURSOR_500 INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'EnforcePCIRequirements')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('EnforcePCIRequirements', 1, 1, @Sequence)		
END

SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'EnforcePCIRequirements')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('EnforcePCIRequirements', 1, 1, @Sequence)		
END

SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'EnforcePCIRequirementsOnFrontEndUsers')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('EnforcePCIRequirementsOnFrontEndUsers', 1, 1, @Sequence)		
END

SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'PasswordExpirationInDays')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('PasswordExpirationInDays', 1, 1, @Sequence)		
END


SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'NumberOfRetainedPasswords')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('NumberOfRetainedPasswords', 1, 1, @Sequence)		
END


	Fetch NEXT FROM SITE_CURSOR_500 INTO @SiteId 		
END

CLOSE SITE_CURSOR_500
DEALLOCATE SITE_CURSOR_500

	GO

IF COL_LENGTH(N'[dbo].[USSiteUser]', N'SessionId') IS NULL
	ALTER TABLE [dbo].[USSiteUser] ALTER COLUMN [SessionId] VARCHAR (100) NULL;
GO
PRINT N'Creating [dbo].[USUserLastPassword]...';
GO
IF OBJECT_ID(N'[dbo].[USUserLastPassword]', 'U') IS NULL
CREATE TABLE [dbo].[USUserLastPassword] (
    [Id]             UNIQUEIDENTIFIER NOT NULL DEFAULT(newid()),
    [UserId]         UNIQUEIDENTIFIER NOT NULL,
    [Password]       NVARCHAR (128)   NOT NULL,
    [CreatedDate]    DATETIME         NOT NULL,
    [PasswordFormat] INT              NOT NULL,
    [PasswordSalt]   NVARCHAR (128)   NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY]
);
GO
PRINT N'Creating [dbo].[USUserLastPassword].[IX_USUserLastPassword_UserId]...';

GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_USUserLastPassword_UserId' AND object_id = OBJECT_ID(N'[dbo].[USUserLastPassword]'))
CREATE NONCLUSTERED INDEX [IX_USUserLastPassword_UserId] ON [dbo].[USUserLastPassword]([UserId] ASC) ON [PRIMARY];


GO
PRINT N'Creating Default Constraint on [dbo].[USUserLastPassword]....';


--GO
--ALTER TABLE [dbo].[USUserLastPassword] ADD DEFAULT (newid()) FOR [Id];


GO
PRINT N'Creating FK_USUserLastPassword_USUserId...';

GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USUserLastPassword_USUserId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[USUserLastPassword]', 'U'))
ALTER TABLE [dbo].[USUserLastPassword]  WITH NOCHECK ADD  CONSTRAINT [FK_USUserLastPassword_USUserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[USUser] ([Id]);
GO

GO


GO
PRINT N'Altering [dbo].[CheckMenuAccess]...';


GO
ALTER FUNCTION [dbo].[CheckMenuAccess](
	@UserRoles		xml, 
	@PageMapNodeId	uniqueidentifier
)
RETURNS bit
AS
BEGIN
	IF @UserRoles IS NULL
		RETURN 1
		
	DECLARE @MenuStatus int
	SET @MenuStatus = (SELECT TOP 1 MenuStatus FROM PageMapNode WHERE PageMapNodeId = @PageMapNodeId)
	
	IF @MenuStatus = 0
		RETURN 0
		
	IF NOT EXISTS (SELECT Top 1 SecurityLevelId FROM USSecurityLevelObject WHERE ObjectId = @PageMapNodeId AND ObjectTypeId = 2 AND SecurityLevelId <> 0 )
		RETURN 1
	
	DECLARE @tbSecLevel TABLE(Id int)
	IF @UserRoles IS NOT NULL
		INSERT INTO @tbSecLevel
			SELECT US.R.value('text()[1]','int') FROM @UserRoles.nodes('/ArrayOfAnyType/anyType') US(R)
	
	IF EXISTS(SELECT Top 1 S.SecurityLevelId FROM USSecurityLevelObject S
		JOIN @tbSecLevel T ON T.Id = S.SecurityLevelId
			AND S.ObjectId = @PageMapNodeId AND S.ObjectTypeId = 2)
			RETURN 1

	RETURN 0
END
GO
PRINT N'Altering [dbo].[CheckPageAccess]...';


GO
ALTER FUNCTION [dbo].[CheckPageAccess](
	@UserRoles			xml, 
	@PageDefinitionId	uniqueidentifier
)
RETURNS bit
AS
BEGIN
	DECLARE @PageStatus int
	SET @PageStatus = (SELECT TOP 1 PageStatus FROM PageDefinition WHERE PageDefinitionId = @PageDefinitionId)
	
	IF @PageStatus != 8
		RETURN 0
		
	DECLARE @ObjectId uniqueidentifier
	SET @ObjectId = (SELECT TOP 1 ObjectId FROM USSecurityLevelObject WHERE ObjectId = @PageDefinitionId AND ObjectTypeId = 8 AND SecurityLevelId <> 0)
	IF @ObjectId IS NULL 
		SET @ObjectId = (SELECT TOP 1 ObjectId FROM USSecurityLevelObject WHERE  SecurityLevelId <> 0 AND ObjectId IN 
		(SELECT PageMapNodeId FROM PageMapNodePageDef WHERE PageDefinitionId = @PageDefinitionId))
	
	IF @ObjectId IS NULL
		RETURN 1
		
	IF @UserRoles IS NULL
		RETURN 0
	
	DECLARE @tbSecLevel TABLE(Id int)
	IF @UserRoles IS NOT NULL
		INSERT INTO @tbSecLevel
			SELECT US.R.value('text()[1]','int') FROM @UserRoles.nodes('/ArrayOfAnyType/anyType') US(R)
	
	IF EXISTS(SELECT Top 1 S.SecurityLevelId FROM USSecurityLevelObject S
		JOIN @tbSecLevel T ON T.Id = S.SecurityLevelId
			AND S.ObjectId = @ObjectId)
			RETURN 1

	RETURN 0
END
GO
PRINT N'Refreshing [dbo].[User_GetUsersInRole]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[User_GetUsersInRole]';


GO
PRINT N'Altering [dbo].[vw_contacts]...';


GO

ALTER VIEW [dbo].[vw_contacts]
AS
SELECT     CP.[UserId], U.[FirstName], U.[MiddleName], U.[LastName], U.[CompanyName], U.[BirthDate], U.[Gender], CP.[AddressId], 
                      CASE WHEN U.[Status] = 1 THEN CP.Status ELSE 2 END [Status]/* if active return marketier status else return 2 inactive*/ , U.[HomePhone], U.[MobilePhone], 
                      U.[OtherPhone], U.[ImageId], CP.[Notes], M.Email, 0 ContactType, ContactSourceId
FROM         [dbo].[USMarketierUserProfile] CP INNER JOIN
                      USMembership M ON M.UserId = CP.UserId INNER JOIN
                      USUser U ON M.UserId = U.Id
WHERE     U.Status !=3  and CP.UserId NOT IN
                          (SELECT     CM.Id
                            FROM          USCommerceUserProfile CM)
UNION ALL
SELECT     CP.[Id] UserId, U.[FirstName], U.[MiddleName], U.[LastName], U.[CompanyName], U.[BirthDate], U.[Gender], UA.[AddressId], 
                      CASE WHEN CP.IsActive = 1 THEN U.[Status] ELSE 2 END/* if Not Active return inactive*/ , U.[HomePhone], U.[MobilePhone], U.[OtherPhone], U.[ImageId],  NULL Notes, M.Email, 
                      1 ContactType, 1 ContactSourceId
FROM         [USCommerceUserProfile] CP INNER JOIN
                      USUser U ON CP.Id = U.Id INNER JOIN
                      USMembership M ON M.UserId = U.Id LEFT OUTER JOIN 
                      (select top 1 * from USUserShippingAddress where UserId=Id )UA ON UA.UserId = U.Id AND UA.IsPrimary = 1
WHERE   U.Status !=3  and   U.Id NOT IN
                          (SELECT     UserId
                            FROM          USSiteUser
                            WHERE      IsSystemUser = 1)
UNION ALL
SELECT DISTINCT 
                      U.[Id] 'UserId', U.[FirstName], U.[MiddleName], U.[LastName], U.[CompanyName], U.BirthDate, U.[Gender], NULL AddressId, U.[Status], U.[HomePhone], 
                      U.[MobilePhone], U.[OtherPhone], U.[ImageId], NULL Notes, M.Email, (CASE WHEN Isnull(SU.IsSystemUser, 0) = 1 THEN 3 ELSE 2 END) ContactType, 
                      (CASE WHEN Isnull(SU.IsSystemUser, 0) = 1 THEN 7 ELSE 1 END) ContactSourceId
FROM         USUser U INNER JOIN
                      USMemberShip M ON M.UserId = U.Id LEFT JOIN
                      USSiteUser SU ON U.Id = SU.UserId
WHERE  U.Status !=3  and    U.Id NOT IN
                          (SELECT     UserId
                            FROM          [dbo].[USMarketierUserProfile]) AND U.Id NOT IN
                          (SELECT     CM.Id
                            FROM          USCommerceUserProfile CM)
GO
PRINT N'Refreshing [dbo].[vwAbandonedCheckout]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[vwAbandonedCheckout]';


GO
PRINT N'Refreshing [dbo].[vwSearchCustomer]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[vwSearchCustomer]';


GO
PRINT N'Altering [dbo].[AssetFile_GetAssetFiles]...';


GO
ALTER PROCEDURE [dbo].[AssetFile_GetAssetFiles] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id uniqueidentifier = null,
	@ApplicationId uniqueidentifier = null,
	@SourceContentId uniqueidentifier=NUll,
	@ObjectTypeId int =null,
	@Status int = null,
	@Extension nvarchar(10)= null,	
	@PageSize int= null,
	@PageIndex int = null,
	@MimeType nvarchar(256) = null,
	@FileName nvarchar(256) =null,
	@Url nvarchar(1200) =null
)
as
Begin
	If (@PageSize is null and @PageIndex is null) 
	Begin
	if @Status  IS NULL
		set @Status= dbo.GetActiveStatus()
		
		select  C.Id,
				C.ApplicationId,
				C.Title,
				C.Description, 
				C.Text, 
				C.URL,
				C.URLType, 
				C.ObjectTypeId, 
				C.CreatedDate,
				C.CreatedBy,
				C.ModifiedBy,
				C.ModifiedDate, 
				C.Status,
				C.RestoreDate, 
				C.BackupDate, 
				C.StatusChangedDate,
				C.PublishDate, 
				C.ArchiveDate, 
				C.XMLString,
				C.BinaryObject, 
				C.Keywords,
				C.SourceContentId,
				F.FileName, 
				F.FileSize, 
				F.Extension, 
				F.FolderName, 
				F.Attributes, 
				F.RelativePath, 
				F.PhysicalPath,
				F.MimeType,
				F.AltText,
				F.FileIcon, 
				F.DescriptiveMetadata, 
				F.ImageHeight, F.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				C.OrderNo,
				C.ParentId							
		from COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		INNER JOIN vwAssetFileUrls V ON V.Id = C.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		where (@Id IS NULL OR C.Id = @Id)
			  AND
			  (@ApplicationId IS NULL OR C.ApplicationId = @ApplicationId)
			  AND
			  (@Extension IS NULL OR @Extension = F.Extension)
			  AND
			  (@MimeType IS NULL OR F.MimeType = @MimeType)
			  AND
			  (@FileName IS NULL OR F.FileName =@FileName )
			  AND
			  (@ObjectTypeId IS NULL OR C.ObjectTypeId = @ObjectTypeId)
			  AND
			  (@Status IS NULL AND Status=dbo.GetActiveStatus() OR  C.Status = @Status)
			  AND 
			  (@SourceContentId IS NULL OR C.SourceContentId = @SourceContentId)
			  AND
			  (@Url IS NULL OR V.Url LIKE @Url)
	end
	else		
	Begin
		select  Id,
				ApplicationId,
				Title,
				Description, 
				Text, 
				URL,
				URLType, 
				ObjectTypeId, 
				CreatedDate,
				CreatedBy,
				ModifiedBy,
				ModifiedDate, 
				Status,
				RestoreDate, 
				BackupDate, 
				StatusChangedDate,
				SourceContentId,
				PublishDate, 
				ArchiveDate, 
				XMLString,
				BinaryObject, 
				Keywords,
				FileName, 
				FileSize, 
				Extension, 
				FolderName, 
				Attributes, 
				RelativePath, 
				PhysicalPath,
				MimeType,
				AltText,
				FileIcon, 
				DescriptiveMetadata, 
				ImageHeight,
				ImageWidth,
				CreatedByFullName,
				ModifiedByFullName,
				OrderNo,
				ParentId									
		From (select ROW_NUMBER() over (order by CreatedDate) as RowId,
					C.Id,
					C.ApplicationId,
					C.Title,
					C.Description, 
					C.Text, 
					C.URL,
					C.URLType, 
					C.ObjectTypeId, 
					C.CreatedDate,
					C.CreatedBy,
					C.ModifiedBy,
					C.ModifiedDate, 
					C.Status,
					C.RestoreDate, 
					C.BackupDate, 
					C.StatusChangedDate,
					C.SourceContentId,
					C.PublishDate, 
					C.ArchiveDate, 
					C.XMLString,
					C.BinaryObject, 
					C.Keywords,
					F.FileName, 
					F.FileSize, 
					F.Extension, 
					F.FolderName, 
					F.Attributes, 
					F.RelativePath, 
					F.PhysicalPath,
					F.MimeType,
					F.AltText,
					F.FileIcon, 
					F.DescriptiveMetadata, 
					F.ImageHeight,
					F.ImageWidth,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName,
					C.OrderNo,
				C.ParentId										
			from COContent C 
			INNER JOIN COFile F ON C.Id = F.ContentId
			INNER JOIN vwAssetFileUrls V ON V.Id = C.Id
			LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
			where (@Id IS NULL OR C.Id = @Id)
			  AND
			  (@ApplicationId IS NULL OR C.ApplicationId = @ApplicationId)
			  AND
			  (@Extension IS NULL OR @Extension = F.Extension)
			  AND
			  (@MimeType IS NULL OR F.MimeType = @MimeType)
			  AND
			  (@FileName IS NULL OR F.FileName =@FileName )
			  AND
			  (@ObjectTypeId IS NULL OR C.ObjectTypeId = @ObjectTypeId)
			  AND
			  (@Status IS NULL AND Status=dbo.GetActiveStatus() OR  C.Status = @Status)
			  AND 
			  (@SourceContentId IS NULL OR C.SourceContentId = @SourceContentId)
			  AND
			  (@Url IS NULL OR V.Url = @Url))
			as FileWithRowNumbers
		where RowId BETWEEN ((@PageIndex -1) * @PageSize +1) AND (@PageIndex * @PageSize)
		
	end
end
GO
PRINT N'Altering [dbo].[AssetFile_GetAssetFiles_Count]...';


GO
ALTER PROCEDURE [dbo].[AssetFile_GetAssetFiles_Count] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id uniqueidentifier =null,
	@ApplicationId uniqueidentifier = null,
	@ObjectTypeId int=null,
	@Status int=null,
	@Extension nvarchar(10)=null,
	@MimeType nvarchar(256) = null,
	@FileName nvarchar(256) =null	
)
as
Begin	
	select count(*)
	from COContent C
	INNER JOIN COFile F  ON C.Id = F.ContentId
	where (@Id IS NULL OR F.ContentId = @Id)
		  and
		  (@ApplicationId IS NULL OR C.ApplicationId  = @ApplicationId)
		  and
		  (@Extension IS NULL OR F.Extension= @Extension)
		  and
		  (@MimeType IS NULL OR F.MimeType = @MimeType)
		  and
		  (@FileName IS NULL OR F.FileName = @FileName)
		  and
		  (@ObjectTypeId IS NULL OR C.ObjectTypeId = @ObjectTypeId)
		  and
		  ((@Status IS NULL AND Status=dbo.GetActiveStatus()) OR C.Status = @Status
		  )
end
GO
PRINT N'Altering [dbo].[Blog_Save]...';


GO
ALTER PROCEDURE [dbo].[Blog_Save] 
(
	@Id						uniqueidentifier=null OUT ,
	@ApplicationId			uniqueidentifier,	
 	@Title					nvarchar(256)=null,
 	@Description			nvarchar(1024)=null,
 	@BlogNodeType			int=0,
 	@MenuId 				uniqueidentifier=null,
	@ContentDefinitionId	uniqueidentifier=null,
 	@ShowPostDate 			bit=true,
 	@DisplayAuthorName 		bit=false,
 	@EmailBlogOwner 		bit=true,
 	@ApproveComments 		bit=true,
 	@RequiresUserInfo 		bit=true,
 	@ModifiedBy				uniqueidentifier,
 	@ModifiedDate			datetime=null, 	
 	@Status					int=null,
	@BlogListPageId			uniqueidentifier=null,
	@BlogDetailPageId		uniqueidentifier=null
)
AS
BEGIN
	DECLARE
		@NewId		uniqueidentifier,
		@RowCount	INT,
		@Stmt 		VARCHAR(25),
		@Now 		datetime,
		@Error 		int	

	SET @Now = getutcdate()

	IF NOT EXISTS (SELECT * FROM BLBlog WHERE Id = @Id)
	BEGIN
		SET @Stmt = 'Create Blog'
		set @Status = dbo.GetStatus(8,'Create')
		INSERT INTO BLBlog  WITH (ROWLOCK)(
			 	 Id,
				 ApplicationId,	
 				 Title,
 				 Description,
 				 BlogNodeType,
 				 MenuId, 		
				 ContentDefinitionId,		 
 				 ShowPostDate,
 				 DisplayAuthorName,
 				 EmailBlogOwner,
 				 ApproveComments,
 				 RequiresUserInfo,
 				 CreatedBy,
 				 CreatedDate, 	
 				 Status
		)
		values
			(
				@Id,
				@ApplicationId,	
 				@Title,
 				@Description,
 				@BlogNodeType,
 				@MenuId, 			
				@ContentDefinitionId,	
				isnull(@ShowPostDate,1),
 				isnull(@DisplayAuthorName,0),
 				isnull(@EmailBlogOwner,1),
 				isnull(@ApproveComments,1),
				isnull(@RequiresUserInfo,1),
 				@ModifiedBy,
 				@Now, 	
 				@Status		
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END

		SELECT @Id 
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify Blog'
		
		UPDATE	BLBlog  WITH (rowlock)	SET 
			Title = ISNULL(@Title, Title),
			MenuId = ISNULL(@MenuId, MenuId),
 			ShowPostDate = ISNULL(@ShowPostDate, ShowPostDate),
 			DisplayAuthorName = ISNULL(@DisplayAuthorName, DisplayAuthorName),
 			EmailBlogOwner = ISNULL(@EmailBlogOwner, EmailBlogOwner),
 			ApproveComments = ISNULL(@ApproveComments, ApproveComments),
 			RequiresUserInfo = ISNULL(@RequiresUserInfo, RequiresUserInfo),
 			ModifiedBy = @ModifiedBy,
			ModifiedDate = @Now,
			BlogListPageId = ISNULL(@BlogListPageId, BlogListPageId),
			BlogDetailPageId = ISNULL(@BlogDetailPageId, BlogDetailPageId),
			ContentDefinitionId = ISNULL(@ContentDefinitionId, ContentDefinitionId),
			Status = ISNULL(@Status, Status)
        WHERE
			Id = @Id

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		

END
GO
PRINT N'Altering [dbo].[Content_GetContents_Count]...';


GO
ALTER PROCEDURE [dbo].[Content_GetContents_Count] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier=null,
	@ApplicationId uniqueidentifier = null,
	@ObjectTypeId int=null,
	@Status int=null,
	@CreatedBy uniqueidentifier=null
)
as
Begin
	
		select  count(*)
		from COContent 
		where (@Id IS NULL OR Id = @Id) 
			  AND
			  (@ApplicationId  IS NULL OR ApplicationId  = @ApplicationId ) 
			  AND
			  (@ObjectTypeId IS NULL OR ObjectTypeId = @ObjectTypeId) 
			  AND
			  (@Status IS NULL OR Status = dbo.GetActiveStatus()) 
			  AND
			  (@CreatedBy IS NULL OR CreatedBy= @CreatedBy) 
end
GO
PRINT N'Altering [dbo].[Country_Get]...';


GO
ALTER PROCEDURE [dbo].[Country_Get](@Id uniqueidentifier=null,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
	IF(@Id IS NULL)
		BEGIN
				SELECT 
					C.Id,
					CountryName Title,
					CountryCode,
					CreatedBy,
					CreatedDate,
					ModifiedDate,
					ModifiedBy,
					R.Name Region
					FROM GLCountry C
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					Order By C.CountryName

					SELECT 
					S.Id,
					S.State Title,
					S.StateCode,
					S.CountryId,
					S.CreatedBy,
					S.CreatedDate,
					S.ModifiedDate,
					S.ModifiedBy,
					R.Name Region 
					FROM GLState S
					INNER JOIN GLCountry C ON S.CountryId=C.Id
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					Where S.Status = 1	
		END
	ELSE
		BEGIN
					SELECT	C.Id,
					CountryName Title,
					CountryCode,
					CreatedBy,
					CreatedDate,
					ModifiedDate,
					ModifiedBy,
					R.Name Region
					FROM GLCountry C
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE C.Id = @Id

						SELECT 
					S.Id,
					S.State Title,
					S.StateCode,
					S.CountryId,
					S.CreatedBy,
					S.CreatedDate,
					S.ModifiedDate,
					S.ModifiedBy,
					R.Name Region 
					FROM GLState S
					INNER JOIN GLCountry C ON S.CountryId=C.Id
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE (@Id is null or C.Id = @Id) AND S.Status = 1	
		END
END
GO
PRINT N'Altering [dbo].[Coupon_ApplyToCart]...';


GO
-- Copyright ⌐ 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: To Apply Coupon to an Order for a Customer
-- Author:   
-- Created Date:07/14/2009  
-- Created By:Devin  
  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
  
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Coupon_ApplyToCart]
    (
      @CouponId UNIQUEIDENTIFIER = NULL ,
      @CouponCodeIds XML = NULL ,
      @CartId UNIQUEIDENTIFIER ,
      @CustomerId UNIQUEIDENTIFIER ,
      @ModifiedBy UNIQUEIDENTIFIER
    )
AS 
    BEGIN  
        DECLARE @CartCouponCount INT
        SET @CartCouponCount = 0
        IF @Cartid IS NOT NULL 
            BEGIN
                IF ( @CouponId IS NULL
                     AND @CouponCodeIds IS NULL
                   ) 
                    BEGIN
                        SELECT  @CartCouponCount = COUNT(1)
                        FROM    CSCartCoupon
                        WHERE   CartId = @CartId
                        IF ( @CartCouponCount > 0 ) 
                            BEGIN 
                                DELETE  FROM CSCartCoupon
                                WHERE   CartId = @CartId
                            END
                    END
                IF ( @CouponId IS NOT NULL ) 
                    BEGIN
                        SELECT  @CartCouponCount = COUNT(1)
                        FROM    CSCartCoupon
                        WHERE   CartId = @CartId
                        IF ( NOT EXISTS ( SELECT    *
                                          FROM      CSCartCoupon
                                          WHERE     CouponId = @CouponId
                                                    AND CartId = @CartId )
                             AND @CartCouponCount = 0
                           ) 
                            INSERT  INTO CSCartCoupon
                                    ( Id ,
                                      CartId ,
                                      CouponId ,
                                      CreatedDate ,
                                      CreatedBy
				            )
                                    SELECT  NEWID() ,
                                            @CartId ,
                                            @CouponId ,
                                            GETUTCDATE() ,
                                            @ModifiedBy
                        ELSE 
                            IF ( @CartCouponCount = 1 ) 
                                UPDATE  CSCartCoupon
                                SET     CouponId = @CouponId
                                WHERE   CartId = @CartId
                            ELSE -- When multiple Coupon for the same cart, but only one coupon is passed
                                BEGIN
                                    DELETE  FROM CSCartCoupon
                                    WHERE   CartId = @CartId

                                    INSERT  INTO CSCartCoupon
                                            ( Id ,
                                              CartId ,
                                              CouponId ,
                                              CreatedDate ,
                                              CreatedBy
				                    )
                                            SELECT  NEWID() ,
                                                    @CartId ,
                                                    @CouponId ,
                                                    GETUTCDATE() ,
                                                    @ModifiedBy
					
                                END
						 
                    END
                ELSE 
                    IF ( @CouponCodeIds IS NOT NULL ) 
                        BEGIN
                            DELETE  FROM CSCartCoupon
                            WHERE   CartId = @CartId
                            INSERT  INTO CSCartCoupon
                                    ( Id ,
                                      CartId ,
                                      CouponId ,
                                      CreatedDate ,
                                      CreatedBy
			                  )
                                    SELECT  NEWID() ,
                                            @CartId ,
                                            C.Id ,
                                            GETUTCDATE() ,
                                            @ModifiedBy
                                    FROM    ( SELECT DISTINCT
                                                        tab.col.value('text()[1]',
                                                              'uniqueidentifier') Id
                                              FROM      @CouponCodeIds.nodes('/GenericCollectionOfGuid/guid') tab ( col )
                                            ) C
                        END
            END  
    END
GO
PRINT N'Altering [dbo].[Facet_GetFacetsByNavigation]...';


GO
ALTER PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(Id)
        FROM    ATFacetRangeProduct_Cache

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN
                SELECT  C.FacetID ,
                        C.FacetValueID ,
                        MIN(DisplayText) DisplayText ,
                        COUNT(*) ProductCount ,
                        MIN(Sequence) FacetSequence ,
                        CASE MIN(SortOrder)
                          WHEN -1 THEN @totProd - COUNT(*)
                          ELSE MIN(SortOrder)
                        END AS SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

                    SELECT  C.FacetID ,
                            C.FacetValueID ,
                            MIN(DisplayText) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            CASE MIN(SortOrder)
                              WHEN -1 THEN @totProd - COUNT(*)
                              ELSE MIN(SortOrder)
                            END AS SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProduct_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
                            WHERE   F.AllowMultiple = 0
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END
GO
PRINT N'Altering [dbo].[Log_GetLogs]...';


GO
ALTER PROCEDURE [dbo].[Log_GetLogs]
(
	@ApplicationId	uniqueidentifier,  
	@EventId		int, 
	@CategoryIds	nvarchar(100) = NULL,
	@SiteId			uniqueidentifier = NULL,
	@Priority		int = NULL, 
	@Severity		nvarchar(32)= NULL, 
	@StartDate		datetime = NULL, 
	@EndDate		datetime = NULL,
	@MachineName	nvarchar(32) = NULL, 
	@Keyword		nvarchar(1500) = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL
)
AS 
BEGIN

	IF @Keyword IS NOT NULL
		SET @Keyword = '%' + @Keyword + '%'

	IF @StartDate IS NOT NULL
		SET @StartDate = dbo.ConvertTimeToUtc(@StartDate, @ApplicationId)
	IF @EndDate IS NOT NULL
		SET @EndDate = dbo.ConvertTimeToUtc(@EndDate, @ApplicationId)
		
	DECLARE @tblLogIds TABLE (LogId int)
	
	  SET @StartDate = dbo.ConvertTimeToUtc(@StartDate, @ApplicationId)
        SET @EndDate = dbo.ConvertTimeToUtc(@EndDate, @ApplicationId)
	
	INSERT INTO @tblLogIds
	SELECT DISTINCT LogId FROM LGCategoryLog	
		WHERE (@CategoryIds IS NULL OR
			CategoryId IN (SELECT Value FROM dbo.SplitComma(@CategoryIds, ',')))
	
	;With LogResults AS (
		SELECT ROW_NUMBER() OVER (ORDER BY L.CreatedDate DESC) AS RowNo,
			L.Id,
			L.Priority,
			L.Severity,
			dbo.ConvertTimeFromUtc(L.CreatedDate, L.SiteId) AS CreatedDate,
			L.MachineName,
			L.ActivityId,
			L.Message,
			E.Stacktrace,
			E.InnerException,
			L.AdditionalInfo
		FROM LGLog L
			INNER JOIN @tblLogIds C ON L.Id = C.LogId
			LEFT JOIN LGExceptionLog E ON L.Id = E.LogId
		WHERE
			L.EventId = @EventId AND
			(@SiteId IS NULL OR L.SiteId = @SiteId) AND
			(@Priority IS NULL OR L.Priority = @Priority) AND
			(@Severity IS NULL OR L.Severity like @Severity) AND
			(@StartDate IS NULL OR @EndDate IS NULL OR L.CreatedDate BETWEEN @StartDate AND @EndDate) AND
			(@MachineName IS NULL OR @MachineName like L.MachineName) AND
			(@Keyword IS NULL OR L.Message like @Keyword OR E.StackTrace like @Keyword)
	), TotalRecords AS (SELECT count(*) Cnt FROM LogResults)
	
	SELECT L.*, T.Cnt AS TotalRecords FROM LogResults L
		CROSS JOIN TotalRecords T
	WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR
		RowNo between (@PageNumber - 1) * @PageSize + 1 and @PageNumber*@PageSize)
	
END
GO
PRINT N'Altering [dbo].[Log_WriteLog]...';


GO
ALTER PROCEDURE [dbo].[Log_WriteLog]
(
	@EventID int, 
	@Priority int, 
	@Severity nvarchar(32), 
	@Title nvarchar(256), 
	@Timestamp datetime,
	@MachineName nvarchar(32), 
	@AppDomainName nvarchar(512),
	@ProcessID nvarchar(256),
	@ProcessName nvarchar(512),
	@ThreadName nvarchar(512),
	@Win32ThreadId nvarchar(128),
	@Message nvarchar(1500),
	@FormattedMessage xml,
	@LogId int OUTPUT
)
AS 
BEGIN
	DECLARE @ActivityId uniqueidentifier
	SET @ActivityId = @FormattedMessage.value('(//ActivityId)[1]','uniqueidentifier')
	IF @ActivityId IS NULL OR @ActivityId = dbo.GetEmptyGUID()
		SET @ActivityId = NEWID() 

	DECLARE @SiteId uniqueidentifier
	SET @SiteId = @FormattedMessage.value('(//SiteId)[1]','uniqueidentifier')
	IF @SiteId IS NULL
		SET @SiteId = dbo.GetEmptyGUID()

	INSERT INTO [LGLog] (
		SiteId,
		EventId,
		Priority,
		Severity,
		CreatedDate,
		MachineName,
		ProcessId,
		ActivityId,
		Message,
		AdditionalInfo
	)
	VALUES (
		@SiteId,
		@EventID,
		@Priority,
		@Severity, 
		@Timestamp,
		@MachineName, 
		@ProcessID,
		@ActivityId,
		@FormattedMessage.value('(//Message)[1]','nvarchar(max)'),
		@FormattedMessage.value('(//AdditionalInfo)[1]','nvarchar(max)'))

	SET @LogID = @@IDENTITY
	
	IF @EventID = 100
	BEGIN
		INSERT INTO LGExceptionLog (
			LogId,
			StackTrace,
			InnerException
		)
		VALUES (
			@LogId,
			@FormattedMessage.value('(//StackTrace)[1]','nvarchar(max)'),
			--@FormattedMessage.value('(//InnerException)[1]','nvarchar(max)')
			 @FormattedMessage.query('(//InnerException)[1]')  
		)
	END
	
	RETURN @LogID
END

GO
PRINT N'Altering [dbo].[Post_GetPost]...';


GO
ALTER PROCEDURE [dbo].[Post_GetPost]
(
	@ApplicationId		uniqueIdentifier,		
	@Id					uniqueIdentifier = null,		
	@PostMonth			varchar(20) = null,
	@PostYear			varchar(20) = null,
	@PageSize			int = null,
	@PageNumber			int = null,
	@FolderId			uniqueIdentifier = null,
	@BlogId				uniqueIdentifier = null,
	@Status				int = null,
	@CategoryId			uniqueIdentifier = null,
	@Label				nvarchar(1024) = null,
	@Author				nvarchar(1024) = null,
	@IncludeVersion		bit = null,
	@VersionId			uniqueidentifier=null,
	@IgnoreSite			bit = null
)
AS
BEGIN
	DECLARE @IsSticky bit
	IF @Status = 5
		SET @IsSticky = 1

	DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)
	;WITH PostIds AS(
		SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo, 
			COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId
		FROM BLPost 
		WHERE (@Id IS NULL OR Id = @Id) AND
			(Status != dbo.GetDeleteStatus()) AND
			(@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND
			(@Status IS NULL OR Status = @Status) AND
			(@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND 
			(@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND 
			(@FolderId IS NULL OR HSId = @FolderId) AND 
			(@BlogId IS NULL OR BlogId = @BlogId) AND
			(@IsSticky IS NULL OR IsSticky = @IsSticky) AND
			(@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND
			(@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND
			(@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))
	)
	
	INSERT INTO @tbPostIds
	SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds
	WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR 
		RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))
	
	SELECT A.Id,			
		A.ApplicationId,
		A.Title,
		A.Description,
		A.ShortDescription,
		A.AuthorName,
		A.AuthorEmail,
		A.Labels,
		A.AllowComments,
		A.PostDate,
		A.Location,
		A.Status,
		A.IsSticky,
		A.CreatedBy,
		A.CreatedDate,
		A.ModifiedBy,
		A.ModifiedDate,
		A.FriendlyName,
		A.PostContentId,
		A.ContentId,
		A.ImageUrl,
		A.TitleTag,
		A.H1Tag,
		A.KeywordMetaData,
		A.DescriptiveMetaData,
		A.OtherMetaData,
		B.BlogListPageId,
		B.Id AS BlogId,
		A.WorkflowState,
		A.PublishDate,
		A.PublishCount,
		A.INWFFriendlyName,
		A.HSId AS MonthId
	FROM BLPost A 
		JOIN @tbPostIds T ON A.Id = T.Id
		JOIN BLBlog B ON A.BlogId = B.Id
	ORDER BY T.RowNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.Text, 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		sDir.VirtualPath As FolderPath
	FROM COContent C
		INNER JOIN @tbPostIds T ON C.Id = T.PostContentId
		INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		
	SELECT Top 1 TotalRows FROM @tbPostIds

	SELECT A.Id,
		ApplicationId,
		Title,
		Description,
		BlogNodeType,
		MenuId,
		ContentDefinitionId,
		ShowPostDate,
		DisplayAuthorName,
		EmailBlogOwner,
		ApproveComments,
		RequiresUserInfo,
		Status,
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		ModifiedDate,
		BlogListPageId,
		BlogDetailPageId,
		ContentDefinitionId
	FROM BLBlog A 
		INNER JOIN @tbPostIds T ON T.BlogId = A.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
	
	SELECT T.Id,
		I.Title,
		C.TaxonomyId
	FROM COTaxonomyObject C
		JOIN @tbPostIds T ON T.Id = C.ObjectId
		JOIN COTaxonomy I ON I.Id = C.TaxonomyId

	SELECT Count(*) AS NoOfComments,
		T.Id
	FROM BLComments C
		JOIN @tbPostIds T ON C.ObjectId = T.Id
	GROUP BY T.Id

	IF @IncludeVersion = 1
	BEGIN
		DECLARE @tbVersionIds TABLE(Id uniqueidentifier)
		IF @VersionId IS NULL
		BEGIN
			;WITH CTEVersion AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,
					V.Id FROM VEVersion V
					JOIN BLPost T On T.Id = V.ObjectId
			)
			INSERT INTO @tbVersionIds
			SELECT Id FROM CTEVersion WHERE VerRank = 1
		END
		ELSE
		BEGIN
			INSERT INTO @tbVersionIds VALUES (@VersionId)
		END

		SELECT V.Id AS VersionId,
			V.ObjectId AS PostId,
			V.XMLString
		FROM VEVersion V
			JOIN @tbVersionIds T On T.Id = V.Id

		SELECT 
			V.PostVersionId AS VersionId,
			C.ContentId,
			C.XMLString
		FROM VEPostContentVersion V
			JOIN @tbVersionIds T On T.Id = V.PostVersionId
			JOIN VEContentVersion C ON C.Id = V.ContentVersionId
	END
END
GO
GO
PRINT N'Altering [dbo].[Post_Save]...';


GO
ALTER PROCEDURE [dbo].[Post_Save]   
(  
	@Id					uniqueidentifier=null OUT ,  
	@BlogId				uniqueidentifier,   
	@ApplicationId		uniqueidentifier,   
	@Title				nvarchar(256)=null,  
	@Description		ntext =null,  
	@ShortDescription	nvarchar(2000)=null,  
	@AuthorName			nvarchar(256)=null,  
	@AuthorEmail		nvarchar(256)=null,  
	@AllowComments		bit =true,  
	@PostDate			datetime=null,  
	@Location			nvarchar(256)=null,  
	@Labels				nvarchar(Max)=null,  
	@ModifiedBy			uniqueidentifier,  
	@IsSticky			bit=null, 
	@HSId				uniqueidentifier,  
	@FriendlyName		nvarchar(256)=null,
	@PostContentId		uniqueidentifier = null,
	@ContentId			uniqueidentifier = null,
	@Imageurl			nvarchar(500) = null,
	@TitleTag			nvarchar(2000)=null,  
	@H1Tag				nvarchar(2000)=null,
	@KeywordMetaData	nvarchar(2000)=null,
	@DescriptiveMetaData  nvarchar(2000)=null,
	@OtherMetaData		nvarchar(2000)=null,
	@PublishDate		datetime = null,
	@PublishCount		int = null,
	@WorkflowState		int = null,
	@Status				int=null
)  
AS  
BEGIN  
	DECLARE  
	 @NewId  uniqueidentifier,  
	 @RowCount  INT,  
	 @Stmt   VARCHAR(25),  
	 @Now    datetime,  
	 @Error   int ,  
	 @MonthId uniqueidentifier,  
	 @return_value int --,
	 --@Status int 
  
	 /* if ID specified, ensure exists */  
	 IF (@Id IS NOT NULL)  
	  IF (NOT EXISTS(SELECT 1 FROM   BLPost WHERE  Id = @Id AND Status <> dbo.GetDeleteStatus()))  
	  BEGIN  
	   set @Stmt = convert(varchar(36),@Id)  
	   RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
	   RETURN dbo.GetBusinessRuleErrorCode()  
	  END    

	  IF @Status = 5
	  BEGIN
		SET @IsSticky = 1
		SET @Status = null 
		/*
		Sankar: If the @status = sticky then 
		FOR INSERT : There is no provision now to make the post as sticky at the time of creation. so in the insert we are making it to default to 1(draft).Even if we send a @Status = 5 then it will create DRAFT STICKY POST.
		FOR update : if the status is sent as sticky, then we need to maintain the same status for the post by updating the sticky bit to 1. So here I am making the @status = null
		*/
	  END
	   
	 SET @Now = getutcdate()  
	  
	 IF (@Id IS NULL)   -- new insert  
	 BEGIN  
	  SET @Stmt = 'Create Post'  
	  SELECT @NewId =newid()  
	  
	  INSERT INTO BLPost  WITH (ROWLOCK)(  
		 Id,  
		ApplicationId,  
		Title,  
		BlogId,  
		Description,  
		ShortDescription,  
		AuthorName,  
		AuthorEmail,  
		Labels,  
		AllowComments,  
		PostDate,  
		Location,  
		Status,  
		IsSticky,
		CreatedBy,  
		CreatedDate,  
		HSId,  
		FriendlyName,
		PostContentId,
		ContentId,
		ImageUrl,
		TitleTag,
		H1Tag,
		KeywordMetaData,
		DescriptiveMetaData,
		OtherMetaData
	  )  
	  VALUES  
	   (  
			@NewId,  
			@ApplicationId,   
			@Title,  
			@BlogId,  
			@Description,  
			@ShortDescription,  
			@AuthorName,  
			@AuthorEmail,  
			@Labels,  
			@AllowComments,  
			@PostDate,  
			@Location,  
			ISNULL(@Status,1), 
			@IsSticky, 
			@ModifiedBy,  
			@Now,
			@HSId ,
			@FriendlyName,
			@PostContentId,
			@ContentId,
			@ImageUrl,
			@TitleTag,
			@H1Tag,
			@KeywordMetaData,
			@DescriptiveMetaData,
			@OtherMetaData
	   )  
	  SELECT @Error = @@ERROR  
	   IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()   
	  END  
	  
	  SELECT @Id = @NewId  
	 END  
	 ELSE   -- Update  
	 BEGIN  
	  SET @Stmt = 'Modify Post'  
	  UPDATE BLPost WITH (ROWLOCK)  
	   SET   
		 Id     = @Id,  
		ApplicationId  = @ApplicationId,  
		Title    = @Title,  
		BlogId    = @BlogId,  
		Description   = @Description,  
		ShortDescription = @ShortDescription,  
		AuthorName   = @AuthorName,  
		AuthorEmail   = @AuthorEmail,  
		Labels    = @Labels,  
		AllowComments  = @AllowComments,  
		PostDate   = @PostDate,  
		Location   = @Location ,  
		ModifiedBy   = @ModifiedBy,  
		ModifiedDate  = GETUTCDATE(),  
		HSId = @HSId,  
		FriendlyName=@FriendlyName,
		PostContentId = @PostContentId,
		ContentId = @ContentId,
		ImageUrl = @Imageurl,
		TitleTag = @TitleTag,
		H1Tag = @H1Tag,
		IsSticky = @IsSticky,
		KeywordMetaData = @KeywordMetaData,
		DescriptiveMetaData = @DescriptiveMetaData,
		OtherMetaData = @OtherMetaData,
		PublishDate = @PublishDate,
		PublishCount = @PublishCount,
		WorkflowState = @WorkflowState,
		Status = ISNULL(@Status,Status)
	  WHERE Id = @Id  
	    
	  SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT  
	  
	  IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()  
	  END  
	  
	  IF @RowCount = 0  
	  BEGIN  
	   --concurrency error  
	   RAISERROR('DBCONCURRENCY',16,1)  
	   RETURN dbo.GetDataConcurrencyErrorCode()  
	  END   
	 END    
   
	EXEC PageMapBase_UpdateLastModification @ApplicationId
END
GO
PRINT N'Altering [dbo].[ProductImage_GetImageForProduct]...';


GO
ALTER PROCEDURE [dbo].[ProductImage_GetImageForProduct]
    (
      @ProductId UNIQUEIDENTIFIER = NULL ,
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) WITH RECOMPILE
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************

        IF @ProductIds IS NULL 
            BEGIN
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI --INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or I.SiteId = @ApplicationId)AND
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
                ORDER BY OI.Sequence ,
                        C.Title
            END
        ELSE 
            BEGIN
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
	--INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
	--Inner join @ProductIds.nodes('/ObjectIdCollection/ObjectId')tab(col) ON  OI.ObjectId =tab.col.value('text()[1]','uniqueidentifier')
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or C.SiteId = @ApplicationId)
	--AND 
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                ORDER BY OI.Sequence ,
                        C.Title
            END
    END
GO
PRINT N'Altering [dbo].[Site_CreateCommerceDefaultData]...';


GO
ALTER Procedure [dbo].[Site_CreateCommerceDefaultData](@ApplicationId uniqueidentifier, @SiteTitle Varchar(1024), @CreatedById uniqueidentifier)
As
BEGIN

	DECLARE @CommerceProductId uniqueidentifier 
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'
	DECLARE @Now DateTime, @maxId int 
	DECLARE @maxSecurityId int
	
	SET @Now = GETUTCDATE()

	-- Product Category
	IF NOT EXISTS(SELECT * FROM PRCategory WHERE Title ='Unassigned')
	BEGIN
		INSERT INTO PRCategory (Id, Title, Description, Sequence, CreatedDate, Status)
		Values('6988B7A8-7C72-4B58-9B68-258B940E2363', 'Unassigned', 'Unassigned', 1, @Now, 1)
	END

	--Insert default value for order number
	IF NOT EXISTS(SELECT * FROM GLIdentity WHERE Name ='OrderNumber' )
	BEGIN
		INSERT INTO GLIdentity (Name,Value) Values ('OrderNumber', 0)
	END

	-- Product Import Config - Did not take it.

	-- Coupon Type
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('D1CA3949-EBFA-4DEC-B120-07DAA8961F81',	'% off Order','',		1,	1,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('2C24DC1C-268E-4ACB-933C-618DEF52E434',	'$ off Order',	'',	1,	2,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('C4FE5439-0B9F-4B41-9221-0302C527DEAE',	'$ off Shipping',	'',	1,	32,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('894D41B5-27D9-4C3B-885D-18FFC565DB8B',	'% off Shipping',	'',	1,	4,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='Flat rate shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('004202FA-B429-4AB7-A59F-ED37C74C0421',	'Flat rate shipping','',		1,	5,	1)
	END

	--Customer Group.sql
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Gold')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Gold',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Silver')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Silver',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Bronze')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Bronze',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Frequent Customer')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Frequent Customer',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='B2B')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'B2B',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Friends & Family')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Friends & Family',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Employees')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Employees',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	-- EveryOne Group - We are not using this group as of now
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Everyone')
	BEGIN
		INSERT INTO USGroup (ProductId, ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, 
		ModifiedBy, ModifiedDate, Status, ExpiryDate, GroupType)
		Values
		(@CommerceProductId,	@ApplicationId,	
		NEWID(),	'Everyone',	'everyone',	
		@CreatedById,	@Now,
		@CreatedById,	@Now,	1,	NULL,	2)
	END

	--- Security Level
	SELECT @maxSecurityId = Isnull(Max(Id),0) + 1 FROM USSecurityLevel

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Commerce Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId, 'Commerce Customer', 'Commerce Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Preferred Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+1, 'Preferred Customer', 'Preferred Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Employee')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+2, 'Employee', 'Employee', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Press')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+3, 'Press', 'Press', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	-- Feature Type
	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto Top Seller')
	BEGIN
		Insert into MRFeatureType Values(1,'Auto Top Seller')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto New Products')
	BEGIN
		Insert into MRFeatureType Values(2,'Auto New Products')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Manual')
	BEGIN
		Insert into MRFeatureType Values(3,'Manual')
	END

	-- Feature
	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Top Sellers' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Top Sellers' , 'Top Sellers Auto', 1, 1, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='New Products' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'New Products' , 'New Products Auto', 2,  2, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Moving Brand' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Moving Brand' , 'Moving Brand Manual', 3, 3, 1, newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Stock Clearance' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Stock Clearance' , 'Stock Clearance Manual', 3,  4, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Christmas Sales' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Christmas Sales' , 'Christmas Sales Manual', 3,  5, 2,newId(), GetUTCDate())

	-- AttributeDataType
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Image Library' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Image Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Date' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.DateTime','Date')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Decimal' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.Double','Decimal')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Asset Library File' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Asset Library File')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Page Library' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Page Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='HTML' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','HTML')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='File Upload' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','File Upload')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Integer' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.Int32','Integer')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='String' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','String')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Boolean')  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.Boolean','Boolean')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Content Library' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Content Library')  END

	-- CommerceStatus 
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Active' )  BEGIN   INSERT INTO CMCommerceStatus values(1,'Active','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Inactive' )  BEGIN   INSERT INTO CMCommerceStatus values(2,'Inactive','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Deleted' )  BEGIN   INSERT INTO CMCommerceStatus values(3,'Deleted','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Approved' )  BEGIN   INSERT INTO CMCommerceStatus values(4,'Approved','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Rejected' )  BEGIN   INSERT INTO CMCommerceStatus values(5,'Rejected','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='PendingForApproval' )  BEGIN   INSERT INTO CMCommerceStatus values(6,'PendingForApproval','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Included' )  BEGIN   INSERT INTO CMCommerceStatus values(7,'Included','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Excluded')  BEGIN   INSERT INTO CMCommerceStatus values(8,'Excluded','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Expired')  BEGIN   INSERT INTO CMCommerceStatus values(9,'Expired','')  END

	-- GLReason
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Damaged')  BEGIN   INSERT INTO GLReason values(NEWID(),'Damaged',210,4,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Restocking')  BEGIN   INSERT INTO GLReason values(NEWID(),'Restocking',210,1,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Bad Inventory')  BEGIN   INSERT INTO GLReason values(NEWID(),'Bad Inventory',210,2,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Returned Item')  BEGIN   INSERT INTO GLReason values(NEWID(),'Returned Item',210,8,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Lost')  BEGIN   INSERT INTO GLReason values(NEWID(),'Lost',210,3,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Physical Count')  BEGIN   INSERT INTO GLReason values(NEWID(),'Physical Count',210,6,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='New Shipment')  BEGIN   INSERT INTO GLReason values(NEWID(),'New Shipment',210,5,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Other')  BEGIN   INSERT INTO GLReason values(NEWID(),'Other',210,7,1)  END

	-- GLAddress
	IF NOT EXISTS (SELECT * FROM GLAddress Where Id = 'A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F')
	BEGIN
		INSERT INTO  GLAddress(
		Id, FirstName, LastName, AddressType, AddressLine1, AddressLine2, AddressLine3, City, StateId, Zip, 
		CountryId, Phone, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, StateName, County)
		Values
		('A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F',
		'Warehouse','Location',0,'Your warehouse address', 250,	NULL	,'City',
		'7E07B39B-BC66-4969-B16A-C62E41043E39',	'30092',	'AD72C013-B99D-4513-A666-A128343CCFF0',	'7819955555',
		'2011-08-01 11:18:32.880',	'E131010D-BE50-4526-B343-B0954E924080',	'2011-08-01 11:18:32.880',	
		'E131010D-BE50-4526-B343-B0954E924080',	1,	'MA','County')	
	END

	-- INWarehouse
	Declare @WarehouseId uniqueidentifier
	DECLARE @CopyWarehouseId uniqueidentifier
		
	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId Where Title = 'Default Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Default Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
			Values(@WarehouseId,	'Default warehouse',	
			'Default warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
			null,	1, 'Internal', 'InternalWarehouseProvider', 1,1)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
		from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
		Where T.IsDownloadableMedia =0 OR T.IsDownloadableMedia is null

	END

	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId  Where Title = 'Digital Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Digital Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
		Values(@WarehouseId,	'Digital Warehouse',	
		'Internal Digital Warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
		Null,	0, Null, 'DigitalWarehouseProvider', 2,0)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
			from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
			Where T.IsDownloadableMedia =1 
	END

	-- Product Type
	--IF NOT EXISTS (SELECT * FROM PRProductType Where SiteId = @ApplicationId)
	IF ((SELECT COUNT(*) FROM PRProductType)=0)
	BEGIN
		Insert into PRProductType(Id,ParentId,LftValue,RgtValue,Title, Description, SiteId, CreatedDate, CreatedBy, Status)
		Values(newId(),@ApplicationId,1,2,@SiteTitle, @SiteTitle ,@ApplicationId, @Now,@CreatedById ,1 )
	END

	-- ORShipper
	DECLARE @Shipper_USPS uniqueidentifier, @Shipper_UPS uniqueidentifier, @Shipper_Fedex uniqueidentifier

	IF EXISTS (SELECT * FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_USPS = (SELECT Id FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId)
	END	
	ELSE
	BEGIN
		SET @Shipper_USPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_USPS,'USPS','USPS','','iAppsUSPSProvider',1,3,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END	
	IF EXISTS (SELECT * FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_UPS = (SELECT Id FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_UPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_UPS,'UPS','UPS','http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum={0}&AgreeToTermsAndConditions=yes&ignore=&track.x=46&track.y=7',
		'iAppsUpsProvider',1,2,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END
	IF EXISTS (SELECT * FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_Fedex = (SELECT Id FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_Fedex = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_Fedex,'Fedex','Fedex','','provider',1,1,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END

	 
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpressSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpressSM',
		'Worldwide ExpressSM','',5,0,0,0,0,1,'07',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
		
	END	
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air Saver','Next Day Air Saver',
		'customid',1,5,0,0,1,0,'13',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='UPS Next Day Air Early A.M. SM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'UPS Next Day Air Early A.M. SM','UPS Next Day Air® Early A.M. SM',
		'',1,0,0,0,0,0,'14',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Next Flight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Next Flight',
		'','',5,0,0,0,0,1,'INTERNATIONAL_FIRST',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Standard','Standard','',8,0,0,0,0,1,'11',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air A.M' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air A.M','Second Day Air A.M','',2,0,0,0,0,0,'59',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First-Class Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'First-Class Mail','1st Class','customid',5,5,0,0,1,0,'0',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International  Economy' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International  Economy','','',7,0,0,0,0,1,'INTERNATIONAL_ECONOMY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Saver','Saver','',4,0,0,0,0,0,'65',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day A.M.' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day A.M.','','',0,0,0,0,0,0,'FEDEX_2_DAY_AM',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpeditedSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpeditedSM','Worldwide ExpeditedSM','',4,0,0,0,0,1,'08',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Three-Day Select' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Three-Day Select','UPS Three-Day Select','customid',3,5,0,0,1,0,'12',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air','UPS Second Day Air','customid',2,5,0,0,1,0,'02',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Priority Overnight','','',1,0,0,0,0,1,'PRIORITY_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Priority' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Priority','','',0,0,0,0,0,1,'INTERNATIONAL_PRIORITY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Priority Mail','Priority Mail','customid',2,5,0,0,1,0,'1',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Express Saver','','',3,0,0,0,0,0,'FEDEX_EXPRESS_SAVER',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'First Overnight','Collège dOccitanie','',4,0,0,0,0,1,'FIRST_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Home Delivery' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Home Delivery','','customid',2,5,0,0,1,0,'GROUND_HOME_DELIVERY',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air','UPS Next Day Air','customid',1,5,0,0,1,0,'01',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide Express PlusSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide Express PlusSM','Worldwide Express PlusSM','',4,0,0,0,0,1,'54',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day','','',5,0,0,0,0,0,'FEDEX_2_DAY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Standard Overnight','','',4,0,0,0,0,0,'STANDARD_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Express','Express','customid',1,5,0,0,1,0,'2',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Ground' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Ground','UPS Ground','customid',5,5,0,0,1,0,'03',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	
	
	
	-- ATAttributeGroup
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='01614e00-86f8-4448-9a48-7daa63171c38')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'01614e00-86f8-4448-9a48-7daa63171c38', N'Features', @ApplicationId, 1, @CreatedById, @Now, @CreatedById, @Now , 1)
	END	
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='b23ffc1b-ab9d-4985-beb4-a5f303b9ef41')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'SKULevel Attributes', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Advanced Details', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'AB730D27-5CCD-49D9-B515-168290438E0A', N'Product Properties', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='3C211CF2-A184-4375-A078-242F4387D1D2')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'3C211CF2-A184-4375-A078-242F4387D1D2', N'Customer Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='9DABE135-467C-459E-ABF2-5FBD64821989')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'9DABE135-467C-459E-ABF2-5FBD64821989', N'Order Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A', N'Advanced Details', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END

	-- ATAttribute
	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0611ed6a-d071-4dff-b6c1-b75a06a17ccf')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as Top Seller', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now,  1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0b94496d-9df4-4ac1-8c5c-48b42f99c4ef')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Discounts', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='2c4983dd-d7eb-40a2-9b05-bfd308e3da62')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Free Shipping', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='4ae05c5a-2375-49d8-8bfb-d131632c0e09')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as New Item', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='7b39ffe5-36b9-44ef-b7a5-08f3c18181aa')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Refundable', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c1a20d3c-985f-49be-9682-df09eaf82fc8')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Freight', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c57665fd-e681-4c13-82ce-d77e60bf9a72')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Tax Exempt', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f4963a96-57dc-4c67-830e-f7426539c8ce')
	--INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	--[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	--VALUES (N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Unlimited Quantity', N'', 1, 
	--NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f68ceb85-922b-4023-a6f9-20025f7fdc94')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'System.String', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Others', 
	NULL, 0, N'AT1', 1, 1, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f7102f66-4242-408e-a816-f8b3e600f4d0')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Shipping Promotions', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='39C2D877-525F-49A6-9E48-D3296989F83B')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('39C2D877-525F-49A6-9E48-D3296989F83B','System.String','AB730D27-5CCD-49D9-B515-168290438E0A','Product Name',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='ECB9E9D1-FC3F-43A5-B40F-3840212F693C')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('ECB9E9D1-FC3F-43A5-B40F-3840212F693C','System.Double','AB730D27-5CCD-49D9-B515-168290438E0A','ListPrice',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	-- ATAttributeEnum
	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='bbf74b61-05d8-49b4-8fee-72147d849814')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'bbf74b61-05d8-49b4-8fee-72147d849814', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='073c66b8-2fc2-4d82-aebe-3165f0138ad5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'073c66b8-2fc2-4d82-aebe-3165f0138ad5', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='51424fd1-0da2-4792-b692-7beef5679bf3')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'51424fd1-0da2-4792-b692-7beef5679bf3', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='f0974b83-4d83-4534-bf87-1f8a8b0237e8')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'f0974b83-4d83-4534-bf87-1f8a8b0237e8', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='0b99d94e-606a-43a7-b72c-d6512fd38bf2')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'0b99d94e-606a-43a7-b72c-d6512fd38bf2', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='235f7a94-52c7-42d8-b870-c0d53906b1e5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'235f7a94-52c7-42d8-b870-c0d53906b1e5', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='11bfcfbd-3ffa-4791-8b39-1f12b80d8524')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'11bfcfbd-3ffa-4791-8b39-1f12b80d8524', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='d8d961c0-5c4a-487b-9913-a920e73cda1a')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'd8d961c0-5c4a-487b-9913-a920e73cda1a', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='3a1e07c0-18d1-4c00-a73c-73a4953d5e41')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'3a1e07c0-18d1-4c00-a73c-73a4953d5e41', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='872eeb49-e8eb-4f73-837c-71ddb20629c4')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'872eeb49-e8eb-4f73-837c-71ddb20629c4', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='ee919400-c7de-4d9c-ae8f-fefd1c768e64')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'ee919400-c7de-4d9c-ae8f-fefd1c768e64', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='49a6b896-2f22-45d1-82fb-e47e8f2519c9')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'49a6b896-2f22-45d1-82fb-e47e8f2519c9', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='8bb63596-8fee-4005-9109-dda014ba147e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'8bb63596-8fee-4005-9109-dda014ba147e', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='70a27f03-06ca-4df2-99bb-5c23b840d465')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'70a27f03-06ca-4df2-99bb-5c23b840d465', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='e5f89737-8879-4e42-9aa1-bfffb05d808e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'e5f89737-8879-4e42-9aa1-bfffb05d808e', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='9b0a9c92-80c8-411f-9037-b0281c755201')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'9b0a9c92-80c8-411f-9037-b0281c755201', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='81a55708-cd4d-4fbd-b3c5-4e3292401511')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'81a55708-cd4d-4fbd-b3c5-4e3292401511', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='a0c6381b-2970-4d09-a582-334b93c51442')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'a0c6381b-2970-4d09-a582-334b93c51442', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'true', N'0', 0, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='691ee3cd-58ff-42d9-a8e8-385e85648cfc')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'691ee3cd-58ff-42d9-a8e8-385e85648cfc', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'false', N'1', 2, N'false', 0, 2, @CreatedById, @Now, @CreatedById, @Now, 1)


	--- CLImageType
	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='396b425c-e197-4f2d-a4a3-25448ca366f8')
	INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('396b425c-e197-4f2d-a4a3-25448ca366f8', N'main', 600, 800, null, N'alt_mainImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 1)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='63cf2418-684a-4d1f-88b2-34968c6f139f')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('63cf2418-684a-4d1f-88b2-34968c6f139f', N'system_thumb', 60, 60, N'systhumb_', null, 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 0)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='2770534e-4675-4764-a620-56776398e03e')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('2770534e-4675-4764-a620-56776398e03e', N'preview', 260, 372, N'prev_', N'alt_PrevImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 3)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='e02ed0f3-1271-4218-b81e-816e458434a1')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('e02ed0f3-1271-4218-b81e-816e458434a1', N'mini', 80, 80, N'mini_', N'alt_miniImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 4)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='ba9b3922-4f9f-4b21-a5cf-e03b26b588ad')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('ba9b3922-4f9f-4b21-a5cf-e03b26b588ad', N'thumbnail', 120, 120, N'thumb_', N'alt_thumbImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 2)

	--OROrderStatus
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='OK')  BEGIN   INSERT INTO OROrderStatus values(1,'OK','Submitted',1,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Shipped')  BEGIN   INSERT INTO OROrderStatus values(2,'Shipped','Shipped',2,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Canceled')  BEGIN   INSERT INTO OROrderStatus values(3,'Canceled','Canceled',3,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Back Ordered')  BEGIN   INSERT INTO OROrderStatus values(4,'Back Ordered','Back Ordered',4,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Approval')  BEGIN   INSERT INTO OROrderStatus values(5,'Pending Approval','On Hold',5,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Customer Registration Approval')  BEGIN   INSERT INTO OROrderStatus values(6,'Pending Customer Registration Approval','Pending Customer Registration Approval',6,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Bad Customer Order Approval')  BEGIN   INSERT INTO OROrderStatus values(7,'Pending Bad Customer Order Approval','Pending Bad Customer Order Approval',7,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Terms Limit')  BEGIN   INSERT INTO OROrderStatus values(8,'Exceeded Terms Limit','Exceeded Terms Limit',8,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Line of Credit')  BEGIN   INSERT INTO OROrderStatus values(9,'Exceeded Line of Credit','Exceeded Line of Credit',9,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Manual Hold')  BEGIN   INSERT INTO OROrderStatus values(10,'Manual Hold','Manual Hold',10,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Partially Shipped')  BEGIN   INSERT INTO OROrderStatus values(11,'Partially Shipped','Partially Shipped',11,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Created')  BEGIN   INSERT INTO OROrderStatus values(12,'Created','Created',12,0)  END

	-- OROrderNextStatus
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderNextStatus values(1,1,10)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderNextStatus values(2,1,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderNextStatus values(3,7,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderNextStatus values(4,7,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderNextStatus values(5,10,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderNextStatus values(6,10,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderNextStatus values(7,5,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =8)  BEGIN   INSERT INTO OROrderNextStatus values(8,5,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =9)  BEGIN   INSERT INTO OROrderNextStatus values(9,6,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =10)  BEGIN   INSERT INTO OROrderNextStatus values(10,6,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =11)  BEGIN   INSERT INTO OROrderNextStatus values(11,12,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =12)  BEGIN   INSERT INTO OROrderNextStatus values(12,11,2)  END

	-- OROrderType
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =1)  BEGIN   INSERT INTO OROrderType values(1,'Web','Web Order',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =2)  BEGIN   INSERT INTO OROrderType values(2,'CSR','Web Service Order',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =3)  BEGIN   INSERT INTO OROrderType values(3,'Brochure Request','Brochure Request',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =4)  BEGIN   INSERT INTO OROrderType values(4,'Recurring Order','Recurring ORder',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =5)  BEGIN   INSERT INTO OROrderType values(5,'Exchange Order','Exchange Order',5)  END

	-- OROrderItemStatus
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderItemStatus values(1,'Ok','Submitted',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderItemStatus values(2,'Shipped','Shipped',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderItemStatus values(3,'Cancelled','Cancelled',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderItemStatus values(4,'Backordered','Backordered',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderItemStatus values(5,'Pending','Pending',5)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderItemStatus values(6,'Released','Released',6)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderItemStatus values(7,'Partially Shipped','Partially Shipped',7)  END

	-- PTCreditCardType
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE Id ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487','American Express','American Express',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='28091FD7-00C2-4E71-A266-7FA718658A66')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('28091FD7-00C2-4E71-A266-7FA718658A66','MasterCard','MasterCard',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='A98F6177-90A5-420D-96B5-A234778F8D96')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('A98F6177-90A5-420D-96B5-A234778F8D96','Visa','Visa',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E','Discover','Discover',@CreatedById, @Now, @CreatedById, @Now)  
	END

	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE CreditCardTypeId ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487' And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='28091FD7-00C2-4E71-A266-7FA718658A66'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('28091FD7-00C2-4E71-A266-7FA718658A66',@ApplicationId,1) 
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='A98F6177-90A5-420D-96B5-A234778F8D96'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('A98F6177-90A5-420D-96B5-A234778F8D96',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E',@ApplicationId,1)  
	END

	-- PTGateway
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =1)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(1,'Website Payments Pro','Website Payments Pro',2,'WebsitePaymentsProGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =2)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(2,'None','None',1,'AutoGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =3)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(3,'Cybersource','Cybersource',3,'CybersourceGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =4)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description],  [Sequence], [DotNetProviderName]) 
		Values(4,'Payflow Pro','Payflow Pro',4,'PayflowProGatewayProvider') 
	END

	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =1)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(1,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =2)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(2,0,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =3)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(3,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =4)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(4,0,1,@ApplicationId) 
	END
	
	-- PTPaymentStatus
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =1)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(1,'Entered','Entered',1,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =2)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(2,'Auth','Authorized',2,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =3)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(3,'Captured','Captured',3,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =4)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(4,'Auth Failed','Authorization Failed',4,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =5)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(5,'Capture Failed','Capture Failed',5,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =6)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(6,'Clear','Clear',6,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =7)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(7,'Manual Cancel','Manual Cancel',7,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =8)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(8,'Partially Captured','Partially Captured',8,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =9)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(9,'Void','Void',9,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =10)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(10,'Refund Entered','Refund Entered',10,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =11)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(11,'Refund Waiting Approval','Refund Waiting Approval',11,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =12)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(12,'Refunded','Refunded',12,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =13)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(13,'Refund Failed','Refund Failed',13,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =14)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(14,'Refund Rejected','Refund Rejected',14,1)  END

	-- PTPaymentType
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =1)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(1,'CreditCard','Credit Card',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =2)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(2,'OnAccount','OnAccount',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =3)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(3,'Paypal','Paypal',5)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =4)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(4,'COD','Cash On Delivery',4)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =5)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(5,'GiftCard','Gift Card',2)  END

	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =1)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(1,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =2)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(2,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =3)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(3,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =4)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(4,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =5)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(5,@ApplicationId,1)  END

	IF (SELECT Count(*) FROM ORReturnReason)=0
	BEGIN

		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES( 
			'Defective')
			
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(  
			'Customer Ordered Wrong Product')
		    
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES('Refused Delivery')


		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Missing Merchandise')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Did not Like')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Changed Mind')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Billing Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Fraudulent Order')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Warehouse Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Returned by Shipper - No Response From Customer')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Other')

	END

	-- INWarehouseType
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =1)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(1,'Internal','iAPPS internal warehouse for Non-Digital Products')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =2)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(2,'Digital','iAPPS Digital warehouse')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =3)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(3,'External','Integrated with external system')  END

	-- INRestockingStatus
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Created'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (1,'Created','Created',1)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Queued'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (2,'Queued','Queued',2)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='In Progress'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (4,'In Progress','In Progress',4)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Canceled'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (3,'Canceled','Canceled',3)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Completed'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (5,'Completed','Completed',5)

	-- ORReturnResolutionType
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] ON
	Select @maxId =Isnull(MAX(Id),0) FROM ORReturnResolutionType
	SET @maxId = @maxId + 1

	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Refund'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Refund','Part or full paymnent is returned', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Replacement'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Replacement','Replacement', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Others'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Others','Others', @maxId)        
		SET @maxId = @maxId + 1
	END
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] OFF

	-- INInventory
	IF(NOT EXISTS(SELECT 1 FROM dbo.INInventory WHERE WarehouseId ='22FAC4BD-4954-415A-9014-3598DAEAB4D0'))  
	BEGIN   
		Declare @Id UniqueIdentifier
		Set @Id= '22FAC4BD-4954-415A-9014-3598DAEAB4D0'

		INSERT INTO [INInventory]
				   ([Id]
				   ,[WarehouseId]
				   ,[SKUId]
				   ,[Quantity]
				   ,[LowQuantityAlert]
				   ,[FirstTimeOrderMinimum]
				   ,[OrderMinimum]
				   ,[OrderIncrement]
				   ,[ReasonId]
				   ,[ReasonDetail]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
					SELECT newId()
					  ,@Id
					  ,Id
					  ,0
					  ,0
					  ,1
					  ,1
					  ,1
					  ,(SELECT Id FROM GLReason Where Title ='Other') As ReasonId
					  ,'Initial Data When SKU is created'
					  ,1
					  ,@CreatedById
					  ,@Now
					  ,@CreatedById
					  ,@Now
				   FROM PRProductSKU WHERE Id NOT IN 
					(SELECT SKUID FROM ININventory WHERE WarehouseId = @Id)
	END
	

	DECLARE @GroupObjectTypeId int
	DECLARE @MerchandisingRoleId int, @FulfillmentRoleId int, @MarketingRoleId int, @CSRRoleId int, 
		@EveryOneRoleId int, @CSRManagerRoleId int, @WHManagerRoleId int
	DECLARE @MerchandisingGroupId uniqueidentifier, @FulfillmentGroupId uniqueidentifier, 
		@MarketingGroupId uniqueidentifier, @CSRGroupId uniqueidentifier, @EveryOneGroupId uniqueidentifier, 
		@CSRManagerGroupId uniqueidentifier, @WHManagerGroupId uniqueidentifier

	SET @GroupObjectTypeId = 15 
	SET @MerchandisingRoleId = 19
	SET @FulfillmentRoleId = 20
	SET @MarketingRoleId = 21
	SET @CSRRoleId = 22
	SET @EveryOneRoleId = 29
	SET @CSRManagerRoleId = 32
	SET @WHManagerRoleId = 33	
	-- if the db has cms and default commerce data was missing 
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MerchandisingGroupId output,@Title = 'Merchandising',
			@Description = 'Merchandising',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
				
	END		
	ELSE
		SET @MerchandisingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')

	IF (@MerchandisingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MerchandisingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MerchandisingGroupId,2,@MerchandisingRoleId, @MerchandisingGroupId,1,1 )				
	END
	
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Fulfillment')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @FulfillmentGroupId output,@Title = 'Fulfillment',
			@Description = 'Fulfillment',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @FulfillmentGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Fulfillment')

	IF (@FulfillmentGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @FulfillmentGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@FulfillmentGroupId,2,@FulfillmentRoleId, @FulfillmentGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Marketing')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MarketingGroupId output,@Title = 'Marketing',
			@Description = 'Marketing',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @MarketingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Marketing')

	IF (@MarketingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MarketingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MarketingGroupId,2,@MarketingRoleId, @MarketingGroupId,1,1 )				
	END

	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSR')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRGroupId output,@Title = 'CSR',
			@Description = 'CSR',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSR')
	
	IF (@CSRGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRGroupId,2,@CSRRoleId, @CSRGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'EveryOne' And GroupType = 1)) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @EveryOneGroupId output,@Title = 'EveryOne',
			@Description = 'EveryOne',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @EveryOneGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'EveryOne')

	IF (@EveryOneGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @EveryOneGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@EveryOneGroupId,2,@EveryOneRoleId, @EveryOneGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSRManager')) 
	BEGIN	
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRManagerGroupId output,@Title = 'CSRManager',
			@Description = 'CSRManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSRManager')

	IF (@CSRManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRManagerGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRManagerGroupId,2,@CSRManagerRoleId, @CSRManagerGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'WHManager')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @WHManagerGroupId output,@Title = 'WHManager',
			@Description = 'WHManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @WHManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'WHManager')

	IF (@WHManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @WHManagerGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@WHManagerGroupId,2,@WHManagerRoleId, @WHManagerGroupId,1,1 )				
	END
			
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_Save]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_Save]
(
	@XmlString		xml,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@VirtualPath	nvarchar(max)  OUT,
	@SiteName		nvarchar(256) OUT
)
AS
BEGIN
	DECLARE @tbDirectory TYDirectory	
	INSERT INTO @tbDirectory(Id,
		Title,
		Description,
		Attributes,
		FolderIconPath,
		PhysicalPath,
		[Size],
		Status,
		ThumbnailImagePath,
		ObjectTypeId,
		ParentId,
		Lft,
		Rgt,
		IsSystem,
		IsMarketierDir,
		AllowAccessInChildrenSites,
		SiteId,
		MicrositeId)						
	SELECT  tab.col.value('@Id[1]','uniqueidentifier') Id,
		tab.col.value('@Title[1]','NVarchar(256)')Title,
		tab.col.value('@Description[1]','Nvarchar(1024)')Description,
		tab.col.value('@Attributes[1]','int')Attributes,
		tab.col.value('@FolderIconPath[1]','nvarchar(4000)')FolderIconPath,
		tab.col.value('@PhysicalPath[1]','nvarchar(4000)')PhysicalPath,
		tab.col.value('@Size[1]','bigint')[Size],
		tab.col.value('@Status[1]','int')Status,
		tab.col.value('@ThumbnailImagePath[1]','nvarchar(4000)')ThumbnailImagePath,
		tab.col.value('@ObjectTypeId[1]','int')ObjectTypeId,
		tab.col.value('@ParentId[1]','uniqueidentifier')ParentId,
		tab.col.value('@Lft[1]','bigint')Lft,
		tab.col.value('@Rgt[1]','bigint')Rgt,
		tab.col.value('@IsSystem[1]','bit')IsSystem,
		tab.col.value('@IsMarketierDir[1]','bit')IsMarketierDir,
		ISNULL(tab.col.value('@AllowAccessInChildrenSites[1]','bit'),0) AllowAccessInChildrenSites,
		@ApplicationId,
		@MicroSiteId
	FROM @XmlString.nodes('/SiteDirectoryCollection/SiteDirectory') tab(col)
	Order By Lft

	-- if one of the node is content then everything is content, no mix and match
	DECLARE @ObjectTypeId int
	SET @ObjectTypeId = (SELECT TOP 1 ObjectTypeId FROM @tbDirectory)
	
	IF (@ObjectTypeId = 7)
		EXEC [dbo].[SqlDirectoryProvider_SaveContentDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 9)
		EXEC [dbo].[SqlDirectoryProvider_SaveFileDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 33)
		EXEC [dbo].[SqlDirectoryProvider_SaveImageDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 38)
		EXEC [dbo].[SqlDirectoryProvider_SaveFormDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE
	BEGIN
		-- All other libraries are shared
		  DECLARE @MasterSiteId uniqueidentifier  
		  SET @MasterSiteId = (SELECT MasterSiteId FROM SISite WHERE Id = @ApplicationId)  
		  IF @MasterSiteId IS NOT NULL AND @MasterSiteId != dbo.GetEmptyGUID()  
		  BEGIN
		   SET @ApplicationId = @MasterSiteId  
		   UPDATE @tbDirectory Set SiteId = @ApplicationId
		  END
		EXEC [dbo].[SqlDirectoryProvider_SaveDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	END
	
	SET @VirtualPath = (SELECT Id, dbo.GetVirtualPathByObjectType(Id, @ObjectTypeId) VirtualPath, Attributes, SiteId 
							FROM @tbDirectory SISiteDirectory FOR XML Auto)
	
	SELECT @SiteName = Title FROM SISite WHERE Id = @ApplicationId
END
GO
PRINT N'Altering [dbo].[Taxonomy_GetTaxonomyByObject]...';


GO
-- Copyright ⌐ 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: 
-- Author: P. Jeevan
-- Created Date: 
-- Created By: 
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
-- exec  
--********************************************************************************
ALTER PROCEDURE [dbo].[Taxonomy_GetTaxonomyByObject](  
@ObjectId uniqueidentifier=null,  
@ApplicationId uniqueidentifier=null,
@ObjectTypeId int=null
 
)  
AS  
BEGIN  
  
  SELECT  TC.Id,  
  TC.ApplicationId,  
  TC.Title,  
  TC.Description,  
  TC.CreatedDate,  
  TC.CreatedBy,  
  TC.ModifiedBy,  
  TC.ModifiedDate,  
  TC.Status,  
  FN.UserFullName CreatedByFullName,
  MN.UserFullName ModifiedByFullName,
  TC.ParentId,
  OT.ObjectId,
  OT.ObjectTypeId     
  FROM COTaxonomy TC 
  INNER JOIN COTaxonomyObject OT ON TC.Id = OT.TaxonomyId
  LEFT JOIN VW_UserFullName FN ON FN.UserId = TC.CreatedBy
  LEFT JOIN VW_UserFullName MN ON MN.UserId = TC.ModifiedBy
  WHERE TC.Status =1 AND OT.Status=1 AND (@ObjectId IS NULL OR OT.ObjectId =@ObjectId)
  AND(@ObjectTypeId IS NULL OR OT.ObjectTypeId =@ObjectTypeId)
  AND (@ApplicationId IS NULL OR TC.SiteId=@ApplicationId)
End
GO
PRINT N'Altering [dbo].[UserManager_GetUserSegment]...';


GO
ALTER PROCEDURE [dbo].[UserManager_GetUserSegment]  
(  
	@VisitorType int ,  
	@Geo   nvarchar(50),  
	@Resolution  nvarchar(50),  
	@Os    nvarchar(50),  
	@Browser  nvarchar(50),  
	@JavaScriptSupport int,  
	@FlashSupport  int,  
	@SiteId  uniqueidentifier  ,
	@SinceLastVisitDays int = 0,
	@StateCode nvarchar(100) = '',
	@SearchKeyword nvarchar(100)=null,
	@DeviceType int =0,
	@ReferrerType int =0,
	@ReferrerUrl nvarchar(256),
	@EntryPage uniqueidentifier
)  
AS   
BEGIN   
	DECLARE	@ResolutionId		uniqueidentifier,  
			@OsId				uniqueidentifier,  
			@BrowserId			uniqueidentifier,  
			@GeoId				uniqueidentifier,  
			@GeoInt				bigint,  
			@CountryCode		nvarchar(50),  
			@ResolutionOtherId  uniqueidentifier,  
			@OsOtherId			uniqueidentifier,  
			@BrowserOtherId     uniqueidentifier,  
			@GeoOtherId			uniqueidentifier,  
			@Result				xml  
    
	DECLARE @intrinsicFrameworkAccount uniqueidentifier
	SET @intrinsicFrameworkAccount='6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
    
	if(select count(*) from dbo.ALAudienceSegmentCriteria where SegmentType = 0 and CriteriaName = @Browser) =0
	begin 
		insert into dbo.ALAudienceSegmentCriteria values (newid(),@Browser,@Browser,0,@intrinsicFrameworkAccount,getdate(),null,null)
	end
   
	if(select count(*) from dbo.ALAudienceSegmentCriteria where SegmentType =1 and CriteriaName=@Os)=0
	begin 
		insert into dbo.ALAudienceSegmentCriteria values (newid(),@Os,@Os,1,@intrinsicFrameworkAccount,getdate(),null,null)
	end 
    
    if(@ReferrerUrl is null)
		select @ReferrerUrl=''
    
	CREATE TABLE #UserSegment (  
			[SegmentId] [uniqueidentifier] NOT NULL,  
			[Browser] [nvarchar](max) NULL,  
			[OperatingSystems] [nvarchar](max)  NULL,  
			[Resolution] [nvarchar](max) NULL,  
			[Geography] [nvarchar](max) NULL,  
			[IsBrowserFiltered] int default 0 ,  
			[IsOperatingSystemsFiltered] int default 0,  
			[IsResolutionFiltered] int default 0,  
			[IsGeoFiltered] int default 0,
			[SegmentName] nvarchar(100),
			StatesInUs nvarchar(100),
			NoOfDaysSinceLastVisited int,[IsSearchKeywordFiltered] int default 0,
			SearchKeyword nvarchar(100) null,
			PagesVisitedPageGrpCondition int null,
			TotalNoOfPagesVisited int)

	INSERT INTO #UserSegment (SegmentId, Browser ,OperatingSystems,Resolution,Geography,SegmentName,
				StatesInUS,NoOfDaysSinceLastVisited,SearchKeyword,PagesVisitedPageGrpCondition,TotalNoOfPagesVisited)  
	SELECT	a.SegmentId, 
			a.Browser ,
			a.OperatingSystems,
			a.Resolution,
			a.Geography,
			b.SegmentName, 
			a.StatesInUS,
			a.NoOfDaysSinceLastVisited,
			a.SearchKeyword,
			a.TotalPagesVisitedOperator,
			a.TotalNoOfPagesVisited
	FROM dbo.ALAudienceSegmentDetails a, ALAudienceSegment b
	WHERE	(@DeviceType is Null Or a.DeviceType = 0 Or a.DeviceType=@DeviceType) AND a.SegmentId = b.Id and 
			(@ReferrerType is null Or a.ReferrerType=0 Or a.ReferrerType = @ReferrerType) 	
			AND a.VisitorType in (0,3,@VisitorType) -- Not Applicable , Both and identified visitor type  
			 and  a.FlashEnabled in (2,@FlashSupport)  -- Not Applicable and identified flash support  
			and a.JavascriptEnabled in (2,@JavaScriptSupport) -- Not Applicable and identified javascript support  
			and b.SiteId in (select SiteId from [dbo].[GetVariantSites](@siteId))  and b.Status = 1  
			and ( isnull(a.NoOfDaysSinceLastVisited,0) =0 or isnull(a.NoOfDaysSinceLastVisited,0)>=@SinceLastVisitDays) 
			and (a.SegmentId in (select SegmentId from ALAudienceSegmentDetails where (StatesInUs='' or StatesInUs is null or StatesInUs like '%'+ @StateCode +'%')))
			and a.SegmentId in (	
			select ad.SegmentId  from ALAudienceSegmentDetails ad Left join dbo.ALAudienceSegmentReferrers r on ad.SegmentId = r.SegmentId
				where (isnull(r.ReferrerUrl,@ReferrerUrl)='' or r.ReferrerUrl like '%'+ @ReferrerUrl + '%'))
			and a.SegmentId in (select ad.SegmentId  from ALAudienceSegmentDetails ad Left join dbo.ALAudienceSegmentPagesVisited pv on ad.SegmentId = pv.SegmentId
				where (@EntryPage IS NULL OR @EntryPage = dbo.GetEmptyGUID() Or ( pv.PageDefinitionId is null or @EntryPage = pv.PageDefinitionId and pv.IsEntry=1)))
			
			
			---ARUN CODE
						--and (a.SegmentId in (
			--	select SegmentId from ALAudienceSegmentDetails a cross apply dbo.SplitComma(StatesInUS,',') c 
			--	where C.Value in (Select value from dbo.SplitComma(@StateCode,','))) or isnull(StatesInUs,'')='')
			--and (a.SegmentId in (
			--	select ad.SegmentId from ALAudienceSegmentDetails ad ,dbo.ALAudienceSegmentReferrers r 
			--	where (r.ReferrerUrl = @ReferrerUrl )and ad.SegmentId = r.SegmentId ))
			--and (a.SegmentId in (
			--	select ad1.SegmentId from ALAudienceSegmentDetails ad1 cross apply dbo.ALAudienceSegmentPagesVisited pv 
			--	where @EntryPage IS NULL OR @EntryPage = dbo.GetEmptyGUID() Or ( @EntryPage = pv.PageDefinitionId and pv.IsEntry=1)))
			--ARUN CODE 
			
		
	exec GetIPNumber @Geo,@GeoInt OUT  
	select  top 1 @CountryCode = country from dbo.GeoIPCity where @GeoInt between StartIP and EndIP   
  

  
	--GUID of Operating system,Resolution and Browser from ALAudienceSegmentCriteria  
	SELECT @OsId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=1 and AliasCriteriaName=@Os  
	SELECT @ResolutionId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=2 and CriteriaName=@Resolution  
	SELECT @BrowserId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=0 and AliasCriteriaName=@Browser  
	SELECT @GeoId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=3 and AliasCriteriaName=@CountryCode  

	SELECT @ResolutionOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=2 and CriteriaName='Other'  
	SELECT @OsOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=1 and AliasCriteriaName='Other'
	SELECT @BrowserOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=0 and AliasCriteriaName='Other'    
	SELECT @GeoOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=3 and AliasCriteriaName='Other'    

	UPDATE #UserSegment Set IsGeoFiltered = 1  
		WHERE (Geography like '%'+ lower(cast(@GeoId as varchar(36))) +'%' or isnull(Geography,'')='')   

	UPDATE #UserSegment Set IsGeoFiltered=1  
		WHERE (Geography like '%'+ lower(cast(@GeoOtherId as varchar(36))) +'%' or isnull(Geography,'')='')   

	DELETE  from #UserSegment Where IsGeoFiltered=0  

	
	UPDATE #UserSegment Set IsBrowserFiltered=1  
		WHERE (Browser like '%'+ lower(cast(@BrowserId as varchar(36))) +'%' or isnull(Browser,'')='')   

	UPDATE #UserSegment Set IsBrowserFiltered=1  
		WHERE (Browser like '%'+ lower(cast(@BrowserOtherId as varchar(36))) +'%' or isnull(Browser,'')='')   

	DELETE  from #UserSegment Where IsBrowserFiltered=0  
	


	Update #UserSegment Set IsOperatingSystemsFiltered=1  
		WHERE (OperatingSystems like '%'+ lower(cast(@OsId as varchar(36))) +'%' or isnull(OperatingSystems,'')='')   

	Update #UserSegment Set IsOperatingSystemsFiltered=1  
		WHERE (OperatingSystems like '%'+ lower(cast(@OsOtherId as varchar(36))) +'%' or isnull(OperatingSystems,'')='')   

	DELETE  from #UserSegment Where IsOperatingSystemsFiltered=0  


	Update #UserSegment Set [IsResolutionFiltered]=1  
		WHERE (Resolution like '%'+ lower(cast(@ResolutionId as varchar(36))) +'%' or isnull(Resolution,'')='')  

	Update #UserSegment Set [IsResolutionFiltered]=1  
		WHERE (Resolution like '%'+ lower(cast(@ResolutionOtherId as varchar(36))) +'%' or isnull(Resolution,'')='')  
  
	DELETE  from #UserSegment Where [IsResolutionFiltered]=0  

	UPDATE #UserSegment SET [IsSearchKeywordFiltered]=0  

	select  + lower(b.value) as Searchvalue ,  a.SegmentId , 0 as filterValue  into #TmpSearch 
		from #UserSegment a cross apply dbo.SplitComma(replace(a.searchkeyword,char(13)+char(10),'|'),'|') b
		where b.value is not null and len(ltrim(rtrim(value)) )>0

		update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,''))=''
		update #UserSegment set [IsSearchKeywordFiltered]=1 WHERE lower(isnull(SearchKeyword,''))=''

	IF(@SearchKeyword <>'')
		BEgin
		update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,'')) like '%'+ lower(isnull(@SearchKeyword,'')) +'%' and  filterValue=0
		END

	--IF(@SearchKeyword <>'')
	--	update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,'')) like '%'+ lower(isnull(@SearchKeyword,'')) +'%'
	--ELSE 
	--begin
	--	update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,''))=''
	--	update #UserSegment set [IsSearchKeywordFiltered]=1 WHERE lower(isnull(SearchKeyword,''))=''
	--END 

	update p set [IsSearchKeywordFiltered]=1 
		FROM #TmpSearch t JOIN #UserSegment p on t.segmentid=p.segmentId and filterValue=1
   
	DELETE  from #UserSegment Where [IsSearchKeywordFiltered]=0  
--select * from #TmpSearch
 -- ;	select * from #UserSegment
	set @Result = 
	(SELECT UserSegment.SegmentId , UserSegment.SegmentName,UserSegment.PagesVisitedPageGrpCondition,UserSegment.TotalNoOfPagesVisited,
			cast((SELECT UserSegment.SegmentId , Indexterm.ObjectId as TermId from GetObjectByRelation('O-T',UserSegment.SegmentId)  Indexterm  for xml auto) as xml) ITData,  
			cast((SELECT UserSegment.SegmentId , Watch.Id as WatchId, Watch.Name as WatchName,  Isnull(SegmentWatch.SegmentWatchType,0) as SegmentWatchType, isnull(AssetWatch.ObjectId,'00000000-0000-0000-0000-000000000000') as ObjectId, 
			isnull(AssetWatch.ObjectURL,'') as  ObjectURL
		FROM dbo.ALAudienceSegmentWatch SegmentWatch, dbo.ALWatch Watch   LEFT OUTER JOIN dbo.ALAssetWatch AssetWatch ON AssetWatch.WatchId =Watch .Id    
		where Watch .Id = SegmentWatch.WatchId and UserSegment.SegmentId = SegmentWatch.SegmentId for xml auto) as xml)WData  
		FROM #UserSegment UserSegment  for xml auto)  
  
  select  @Result  
  
  select US.SegmentId, ALPV.PageDefinitionId
		from ALAudienceSegmentPagesVisited ALPV 
			inner join #UserSegment US on ALPV.SegmentId = US.SegmentId
		where ALPV.IsEntry=0
 END
GO
PRINT N'Creating [dbo].[Country_GetState]...';


GO
CREATE PROCEDURE [dbo].[Country_GetState]
(@Id uniqueidentifier=null
,@CountryId uniqueidentifier=null
,@ApplicationId uniqueidentifier=null
)
AS
BEGIN
			SELECT 
					S.Id,
					S.State Title,
					S.StateCode,
					S.CountryId,
					S.CreatedBy,
					S.CreatedDate,
					S.ModifiedDate,
					S.ModifiedBy,
					R.Name Region 
					FROM GLState S
					INNER JOIN GLCountry C ON S.CountryId=C.Id
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE (@Id is null or S.Id = @Id) 
					AND (@CountryId is null or C.Id = @CountryId) 
					AND S.Status = 1	
					Order by S.State
END
GO
PRINT N'Creating [dbo].[Membership_GetLastPasswords]...';


GO
CREATE PROCEDURE [dbo].[Membership_GetLastPasswords]
--********************************************************************************
-- PARAMETERS
    @ApplicationId UNIQUEIDENTIFIER ,
    @UserName NVARCHAR(256) ,
    @Count INT
   
--********************************************************************************
AS 
    BEGIN

    
        DECLARE @UserId UNIQUEIDENTIFIER
        SET @UserId = dbo.USGetUserIdForSite(@UserName, @ApplicationId)


		--Clean up old data
        DELETE  FROM UP
        FROM    ( SELECT    ROW_NUMBER() OVER ( PARTITION BY Userid ORDER BY CreatedDate DESC ) AS VersionNumber ,
                            UserId ,
                            CreatedDate ,
                            ID
                  FROM      USUserLastPassword
                ) TV
                INNER JOIN USUserLastPassword UP ON UP.Id = TV.Id
        WHERE   VersionNumber > @Count

	--User not found
        IF ( @UserId IS NULL ) 
            BEGIN
                RAISERROR ( 'NOTEXISTS||%s' , 16, 1, @UserName)
                RETURN dbo.GetBusinessRuleErrorCode()
            END
		
        SELECT TOP ( @Count )
                Password ,
                PasswordFormat ,
                PasswordSalt
        FROM    USUserLastPassword
        WHERE   UserId = @UserId
        ORDER BY CreatedDate DESC

        RETURN  @@ROWCOUNT
    END
GO
PRINT N'Creating [dbo].[Membership_GetSessionId]...';


GO
CREATE PROCEDURE [dbo].[Membership_GetSessionId]
--********************************************************************************
-- PARAMETERS
	@ApplicationId						uniqueidentifier,
    @UserId						uniqueidentifier
	AS
BEGIN
	Select SessionId
	FROM USSiteUser 
	Where UserId = @UserId AND @ApplicationId=@ApplicationId
	
END
GO
PRINT N'Creating [dbo].[Membership_LogPassword]...';


GO
CREATE PROCEDURE [dbo].[Membership_LogPassword]
--********************************************************************************
-- PARAMETERS

	@ApplicationId			  uniqueidentifier,
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @PasswordFormat   int = 0
--********************************************************************************

AS
BEGIN
	--Gets the Current DateTime
	DECLARE @CurrentTimeUtc DateTime
	SELECT	@CurrentTimeUtc =GetUTCDate()
    
	DECLARE @UserId uniqueidentifier
    SET @UserId = dbo.USGetUserIdForSite(@UserName , @ApplicationId)

	--User not found
    IF (@UserId IS NULL)
	BEGIN
       	RAISERROR ( 'NOTEXISTS||%s' , 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
		
INSERT INTO USUserLastPassword (UserId, Password, PasswordFormat, PasswordSalt, CreatedDate)
VALUES( @UserId, @NewPassword, @PasswordFormat, @PasswordSalt, @CurrentTimeUtc)


    RETURN  @@ROWCOUNT
END
GO
PRINT N'Creating [dbo].[Membership_UpdateSessionId]...';


GO
CREATE PROCEDURE [dbo].[Membership_UpdateSessionId]
--********************************************************************************
-- PARAMETERS
	@ApplicationId						uniqueidentifier,
    @UserId						uniqueidentifier,
	@SessionId					varchar(100)
	AS
BEGIN
	Update USSiteUser Set SessionId=@SessionId
	Where UserId = @UserId AND @ApplicationId=@ApplicationId
	
END
GO
PRINT N'Altering [dbo].[Cart_Save]...';


GO
ALTER PROCEDURE [dbo].[Cart_Save](
					@Id uniqueidentifier output,
					@Title nvarchar(50) = null,
					@Description nvarchar(256) = null,
					@CustomerId uniqueidentifier = null,
					@CouponId uniqueidentifier = null,
					@CouponIds xml = null,
					@ExpirationDate datetime,
					@CreatedBy uniqueidentifier,
					@ModifiedBy uniqueidentifier,
					@ApplicationId uniqueidentifier
						 )

AS 
BEGIN
  
DECLARE @CurrentDate datetime
  set @CurrentDate = getUTCDate()
DECLARE @CurrentCartId UNIQUEIDENTIFIER
SELECT @CurrentCartId = Id FROM dbo.CSCart WHERE Id = @Id
IF @Id = dbo.GetEmptyGUID()
BEGIN
	SET @Id = NEWID()	
END

 if(@CurrentCartId IS NULL)  
	 begin  
		 
			Insert into CSCart(
						Id,
						Title,
						Description,
						CustomerId,
						ExpirationDate,
						CreatedDate,
						CreatedBy,
						SiteId
							   )
			Values(
					@Id,
					@Title,
					@Description,
					@CustomerId,
					dbo.ConvertTimeToUtc(@ExpirationDate,@ApplicationId),
					@CurrentDate,
					@CreatedBy,
					@ApplicationId
				  )
	
		
					 	
	  end
else
	 begin  
			Update CSCart
				Set Title = @Title,
					CustomerId = @CustomerId,
					Description = @Description,
					ExpirationDate = dbo.ConvertTimeToUtc(@ExpirationDate,@ApplicationId),
					ModifiedDate = @CurrentDate,
					ModifiedBy = @ModifiedBy,
					SiteId = @ApplicationId
				Where Id = @Id
	  end

EXEC Coupon_ApplyToCart @CouponId,@CouponIds,@Id,@CustomerId,@ModifiedBy
	
	/*Delete from CSCartCoupon
		Where CartId = @Id
	Insert into CSCartCoupon 
					(Id
					 ,CartId
					 ,CouponId
					 ,CreatedBy
					 ,CreatedDate)
		Select newid()
			,@Id
				,tab.col.value('text()[1]','uniqueidentifier') CouponId
				,@CreatedBy CreatedBy
				,@CurrentDate CreatedDate
		FROM @CouponIds.nodes('/GenericCollectionOfGuid/guid')tab(col)
*/

Select @Id
END
GO
PRINT N'Altering [dbo].[Version_SaveBlogPost]...';
GO

ALTER PROCEDURE [dbo].[Version_SaveBlogPost](
	@VersionId	uniqueidentifier,
	@ContentId	uniqueidentifier,
	@FriendlyName	nvarchar(max),
	@ContentXml xml,
	@ModifiedBy uniqueidentifier,
	@UpdateWorkflowState bit
)
AS
BEGIN
	DECLARE @ContentVersionId uniqueidentifier
	SET @ContentVersionId = NEWID()

	INSERT INTO [dbo].[VEContentVersion]
		([Id],
		[ContentId],
		[ContentTypeId],
		[XMLString],
		[Status],
		[CreatedBy],
		[CreatedDate]
		)
	VALUES
		(@ContentVersionId,
		 @ContentId,
		 13,
		 @ContentXml,
		 1,
		 @ModifiedBy,
		 GETUTCDATE()
		)

	INSERT INTO VEPostContentVersion
		(Id,
		PostVersionId,
		ContentVersionId
		)
	VALUES
		(NEWID(),
		@VersionId,
		@ContentVersionId
		)
		
IF(@UpdateWorkflowState = 1)
	UPDATE P SET 
		P.INWFFriendlyName = @FriendlyName,
		P.StatusChangeDate = GETUTCDATE(),
		P.WorkflowState = 2 --InDraft
	FROM BLPost P JOIN VEVersion V ON P.Id = V.ObjectId 
	WHERE V.Id = @VersionId
ELSE
		UPDATE P SET 
		P.INWFFriendlyName = @FriendlyName
	FROM BLPost P JOIN VEVersion V ON P.Id = V.ObjectId 
	WHERE V.Id = @VersionId
	
END
GO
PRINT N'Altering [dbo].[CampaignRunHistory_GetByCampaign]...';
GO

ALTER PROCEDURE [dbo].[CampaignRunHistory_GetByCampaign]
(
	@CampaignId uniqueidentifier,
	@CampaignRunId uniqueidentifier,
    @PageNumber int =null,
    @PageSize int =null,
	@SortBy nvarchar(50) = 'RunDate desc',  
	@TotalRecords int  =null out,
	@ApplicationId uniqueidentifier 
)
AS
Begin
IF @PageNumber is null 
	BEGIN
	Select 
		Id
		,CampaignId
		,dbo.ConvertTimeFromUtc(RunDate,@ApplicationId)RunDate 
		,Sends
		,Delivered
		,Opens
		,Clicks
		,Unsubscribes
		,Bounces
		,TriggeredWatches
		,EmailSubject
		,SenderName
		,SenderEmail
		,CostPerEmail
		,ConfirmationEmail
		,EmailHtml
		,CMSPageId
		,EmailText
		,dbo.GetUniqueOpenInCampaignMail(Id) As UniqueOpen
	From MKCampaignRunHistory
	Where CampaignId = @CampaignId
	AND Id =isnull( @CampaignRunId,Id)  order by RunDate desc
	END
	ELSE
	BEGIN

 if @SortBy is null
			set @SortBy = 'RunDate desc'
			Declare @sql varchar(max)

			Set @sql='
			   With tmpContacts
			   AS (
					Select
					  Id
		,CampaignId
		,dbo.ConvertTimeFromUtc(RunDate,''' + cast(@ApplicationId as varchar(36)) + ''')RunDate 
		,Sends
		,Delivered
		,Opens
		,Clicks
		,Unsubscribes
		,Bounces
		,TriggeredWatches
		,EmailSubject
		,SenderName
		,SenderEmail
		,CostPerEmail
		,ConfirmationEmail
		,EmailHtml
		,CMSPageId
		,EmailText
		,dbo.GetUniqueOpenInCampaignMail(Id) As UniqueOpen
	From MKCampaignRunHistory
	Where CampaignId ='''+ cast( @CampaignId as varchar(36)) + ''')
		
				
				Select  A.*   
					FROM  
					 (  
					 Select *,  ROW_NUMBER() OVER(Order by ' + @SortBy + '
												) as RowNumber  
					  from tmpContacts  
					 ) A  
					where A.RowNumber >=' +  cast((@PageSize * @PageNumber) - (@PageSize -1 ) as varchar(10))  + 
						' AND A.RowNumber <=' + cast((@PageSize * @PageNumber) as varchar(10)) 
				 exec(@sql)

	Set @TotalRecords =(Select count(*)  From MKCampaignRunHistory
	Where CampaignId = @CampaignId)

	END
End
GO
PRINT N'Altering [dbo].[PageDefinition_Get]...';
GO
ALTER PROCEDURE [dbo].[PageDefinition_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageDefinitionIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageDefinitionIds
	SELECT PageDefinitionId FROM PageDefinition
		WHERE SiteId = @siteId AND PageStatus != 3 AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT M.PageDefinitionId, M.PageMapNodeId, SiteId, M.DisplayOrder, --PageDefinitionXml,
		TemplateId, Title, Description, PageStatus, WorkflowState, PublishCount, PublishDate, FriendlyName,
		WorkflowId, CreatedBy, dbo.ConvertTimeFromUtc(CreatedDate, @siteId) AS CreatedDate, ModifiedBy, 
		dbo.ConvertTimeFromUtc(ModifiedDate, @siteId) AS ModifiedDate, ArchivedDate, StatusChangedDate, 
		AuthorId, EnableOutputCache, OutputCacheProfileName, ExcludeFromSearch, IsDefault, IsTracked,
		HasForms, TextContentCounter, CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,
		ScheduledPublishDate, ScheduledArchiveDate, NextVersionToPublish
	FROM PageDefinition D 
		JOIN PageMapNodePageDef M ON D.PageDefinitionId = M.PageDefinitionId
		JOIN @PageDefinitionIds T ON D.PageDefinitionId = T.Id
	ORDER BY DisplayOrder ASC

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId
	FROM USSecurityLevelObject S
		JOIN @PageDefinitionIds T ON S.ObjectId = T.Id
	WHERE ObjectTypeId = 8
		
	SELECT PageDefinitionId, 
		StyleId, CustomAttributes 
	FROM SIPageStyle S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id
		
	SELECT PageDefinitionId, 
		ScriptId, CustomAttributes 
	FROM SIPageScript S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id

	SELECT P.PageDefinitionId, 
		P.PageMapNodeId, P.DisplayOrder 
	FROM PageMapNodePageDef P
		JOIN @PageDefinitionIds T ON P.PageDefinitionId = T.Id
END
GO
PRINT N'Altering [dbo].[PageMapNode_Get]...';
GO
ALTER PROCEDURE [dbo].[PageMapNode_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageMapNodeIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageMapNodeIds
	SELECT PageMapNodeId FROM PageMapNode
		WHERE SiteId = @siteId AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT PageMapNodeId,ParentId,LftValue,RgtValue, SiteId, ExcludeFromSiteMap, --PageMapNodeXml, 
		Description, DisplayTitle, FriendlyUrl, MenuStatus, TargetId, Target, TargetUrl, CreatedBy,
		dbo.ConvertTimeFromUtc(CreatedDate, @siteId) AS CreatedDate, ModifiedBy, 
		dbo.ConvertTimeFromUtc(ModifiedDate, @siteId) AS ModifiedDate, PropogateWorkflow, InheritWorkflow, PropogateSecurityLevels,
		InheritSecurityLevels, PropogateRoles, InheritRoles, Roles, LocationIdentifier, HasRolloverImages,
		RolloverOnImage, RolloverOffImage, IsCommerceNav, AssociatedQueryId, DefaultContentId, 
		AssociatedContentFolderId, CustomAttributes, HiddenFromNavigation, SourcePageMapNodeId
	FROM PageMapNode P 
		JOIN @PageMapNodeIds T ON P.PageMapNodeId = T.Id
	ORDER BY LftValue

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId 
	FROM USSecurityLevelObject S
		JOIN @PageMapNodeIds T ON S.ObjectId = T.Id
	WHERE S.ObjectTypeId = 2

	SELECT W.Id, W.PageMapNodeId, 
		W.WorkflowId, W.CustomAttributes 
	FROM PageMapNodeWorkflow W
		JOIN @PageMapNodeIds T ON W.PageMapNodeId = T.Id
	
	SELECT M.PageDefinitionId, 
		M.PageMapNodeId, M.DisplayOrder 
	FROM PageMapNodePageDef M
		JOIN @PageMapNodeIds T ON M.PageMapNodeId = T.Id

	SELECT P.PageMapNodeId, C.ContentId, RelativePath, FileName FROM COFile C
		JOIN PageMapNode P ON C.ContentId = P.TargetId
		JOIN @PageMapNodeIds T ON P.PageMapNodeId = T.Id
END
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[ProductImage_SimpleSearch]  
(
	@SearchTerm nvarchar(max),
	@IsBundle tinyint = null,
	@pageNumber		int = null,
	@pageSize		int = null,
	@SortColumn VARCHAR(25)=null,
	@ProductTypeId uniqueidentifier=null,
	@ApplicationId uniqueidentifier=null
 )  
AS  
BEGIN  
	DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 

	SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
	SET @PageUpperBound = @PageLowerBound - Isnull(@pageSize,0) + 1;

	DECLARE @searchText nvarchar(max)  
	SET @searchText = '%' + @SearchTerm + '%'
	IF @SortColumn IS NULL
		SET @SortColumn = 'productname asc'
	SET @SortColumn = LOWER(@SortColumn)

	DECLARE @tbProduct Table(ProductId uniqueidentifier, ProductName nvarchar(2000), Sku nvarchar(500), ProductTypeName nvarchar(500))
	
	INSERT INTO @tbProduct
	SELECT P.Id, P.Title, S.SKU, T.Title FROM PRProduct P 
			JOIN PRProductSku S ON S.ProductId = P.Id
			JOIN PRProductType T ON T.Id = P.ProductTypeId
	WHERE
		(@ApplicationId IS NULL OR P.SiteId = @ApplicationId) AND
		(@ProductTypeId IS NULL OR T.Id = @ProductTypeId) AND
		(@IsBundle IS NULL OR P.IsBundle = @IsBundle) AND
		(P.Title like @searchText 
				OR P.ShortDescription like @searchText 
				OR P.LongDescription like @searchText   
				OR P.Description like @searchText
				OR S.SKU like @searchText)
	
	DECLARE @tbProductImage Table(RowNumber int , ProductId uniqueidentifier, ProductName nvarchar(2000), Sku nvarchar(500), ProductTypeName nvarchar(500), ImageId uniqueidentifier)  

	--INSERT INTO @tbProductImage   
  ;WITH CTE_ProductImage( ProductId , ProductName , Sku , ProductTypeName , ImageId, FileName )
 AS
 (
 SELECT       
  T.ProductId,  
  T.ProductName,  
  T.Sku,  
  T.ProductTypeName,  
  C.[Id] ,
  C.FileName 
 FROM  
  CLObjectImage OI INNER JOIN CLImage I on I.Id = OI.ImageId  
  INNER JOIN CLImage C on (I.Id=C.ParentImage )  
  INNER JOIN CLImageType IT ON IT.Id = C.ImageType  
  INNER JOIN @tbProduct T on T.ProductId = OI.ObjectId  
 WHERE    
  (@ApplicationId is null or I.SiteId = @ApplicationId)  
  AND I.Status = dbo.GetActiveStatus() AND I.Id is not null   
  AND ObjectType = 205 AND IT.TypeNumber = 0  

  
  UNION 
  
  SELECT       
  T.ProductId,  
  T.ProductName,  
  T.Sku,  
  T.ProductTypeName,  
  C.[Id] ,
  C.FileName 
 FROM  
  CLObjectImage OI INNER JOIN CLImage I on I.Id = OI.ImageId  
  INNER JOIN CLImage C on (I.Id=C.Id)  
  INNER JOIN CLImageType IT ON IT.Id = C.ImageType  
  INNER JOIN @tbProduct T on T.ProductId = OI.ObjectId  
 WHERE    
  (@ApplicationId is null or I.SiteId = @ApplicationId)  
  AND I.Status = dbo.GetActiveStatus() AND I.Id is not null   
  AND ObjectType = 205 AND IT.TypeNumber = 0  
 
  ) 
  INSERT INTO @tbProductImage   
  SELECT      
  ROW_NUMBER() OVER(ORDER BY   CASE WHEN @SortColumn = 'productname desc' Then ProductName END DESC,      
  CASE WHEN @SortColumn = 'productname asc' Then ProductName END ASC,      
  CASE WHEN @SortColumn = 'producttypename desc' Then ProductTypeName END DESC,      
  CASE WHEN @SortColumn = 'producttypename asc' Then ProductTypeName END ASC,
  CASE WHEN @SortColumn = 'sku asc' Then Sku END ASC,
  CASE WHEN @SortColumn = 'sku desc' Then Sku END DESC,
  CASE WHEN @SortColumn = 'filename asc' Then FileName END ASC,
  CASE WHEN @SortColumn = 'filename desc' Then FileName END DESC
   
  ) as RowNumber,   
  ProductId,  
  ProductName,  
  Sku,  
  ProductTypeName,  
  ImageId  
 FROM  CTE_ProductImage
	
	
	SELECT 		
		C.[Id],
		C.[Title],
		C.[Description],
		C.[FileName],
		C.[Size],
		C.[Attributes],
		C.[RelativePath],
		C.[AltText],
		C.[Height],
		C.[Width],
		C.[Status],
		C.[SiteId],
		C.[ParentImage],
		C.ImageType,
		T.ProductName,
		T.ProductTypeName,
		T.Sku,
		T.Productid
	FROM
		CLImage C JOIN  @tbProductImage T ON T.ImageId = C.Id
	WHERE  
		@pageSize is null OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
 
	Select count(*) from @tbProductImage
END
GO