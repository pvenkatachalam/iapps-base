SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Copyright ⌐ 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[PaymentGiftCard_GetByPaymentId](@PaymentId uniqueidentifier)
AS
BEGIN


 SELECT 
  CO.Id,
  GiftCardId,
  GiftCardNumber,
  Last4Digits,
  BalanceAfterLastTransaction,
  PaymentId,
  BillingAddressId,
  CustomerId,
  Status,
  CreatedDate,
  CreatedBy,
  ModifiedBy,
  ModifiedDate
 FROM PTPaymentGiftCard CO
INNER JOIN PTGiftCardInfo CI On CO.GiftCardId = CI.Id
 WHERE PaymentId=@PaymentId

END

