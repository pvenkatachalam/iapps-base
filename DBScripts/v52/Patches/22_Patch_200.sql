﻿--This script is to update the patches for v5.1
/*
List of changed objects
1. 

*/
GO
IF OBJECT_ID('[dbo].[User_GetUsersInGroup]') IS NOT NULL DROP FUNCTION [dbo].[User_GetUsersInGroup]	
GO
IF OBJECT_ID('[dbo].[User_GetUsersInRole]') IS NOT NULL DROP FUNCTION [dbo].[User_GetUsersInRole]	
GO
IF OBJECT_ID('[dbo].[Membership_BuildUser]') IS NOT NULL DROP PROCEDURE [dbo].[Membership_BuildUser]	
GO
PRINT 'Altering view vwSearchOrder'
GO
ALTER VIEW [dbo].[vwSearchOrder]
AS
SELECT     O.Id, O.PurchaseOrderNumber, O.ExternalOrderNumber, O.OrderTypeId, O.OrderDate, US.FirstName, US.MiddleName, US.LastName, O.OrderStatusId, O.TotalTax, 
                      O.CODCharges, O.TotalShippingCharge AS ShippingTotal, O.TotalShippingCharge,O.PaymentRemaining, O.RefundTotal, O.SiteId, dbo.Order_GetQuantity(O.Id) AS Quantity, ISNULL
                          ((SELECT     SUM(ISNULL(dbo.OROrderItem.Quantity, 0)) AS Expr1
                              FROM         dbo.OROrderItem INNER JOIN
                                                    dbo.PRProductSKU AS PS ON dbo.OROrderItem.ProductSKUId = PS.Id INNER JOIN
                                                    dbo.PRProduct AS P ON PS.ProductId = P.Id
                              WHERE     (dbo.OROrderItem.OrderId = O.Id) AND (P.IsBundle = 0)), 0) AS NumSkus, O.OrderTotal, O.TotalDiscount, O.TaxableOrderTotal, O.GrandTotal, 
                      O.CustomerId, O.TotalShippingDiscount, O.CreatedBy, FN.UserFullName AS CreatedByFullName, CASE WHEN EXISTS
                          (SELECT     *
                            FROM          FFOrderShipment F INNER JOIN
                                                   dbo.OROrderShipping AS OS ON F.OrderShippingId = OS.Id
                            WHERE      OS.OrderId = O.Id AND (ShipmentStatus = 3 OR ShipmentStatus = 9)) THEN 3 
                            ELSE 0 END AS ExternalShippingStatus
FROM         dbo.OROrder AS O 
					INNER JOIN dbo.USCommerceUserProfile AS U ON O.CustomerId = U.Id 
					INNER JOIN dbo.USUser US on US.Id = U.Id
                    LEFT OUTER JOIN dbo.VW_CustomerFullName AS FN ON FN.UserId = O.CreatedBy
GO
--UDDT CHANGE
IF TYPE_ID(N'[dbo].[TYUser]') IS NOT NULL
DROP TYPE [TYUser]
GO
IF TYPE_ID(N'[dbo].[TYUser]') IS NULL
CREATE TYPE [dbo].[TYUser] AS TABLE
(
	Id						uniqueidentifier, 
	UserName				nvarchar(256),  
	Email					nvarchar(512),
	PasswordQuestion		nvarchar(512),  
	IsApproved				bit,
	CreatedDate				datetime,
	LastLoginDate			datetime,
	LastActivityDate		datetime,
	LastPasswordChangedDate	datetime,
	IsLockedOut				bit,
	LastLockoutDate			datetime,
	FirstName				nvarchar(256),
	LastName				nvarchar(256),
	MiddleName				nvarchar(256) ,
	ExpiryDate				datetime ,
	EmailNotification		bit ,
	TimeZone				nvarchar(100),
	ReportRangeSelection	varchar(50),
	ReportStartDate			datetime ,
	ReportEndDate			datetime, 
	BirthDate				datetime,
	CompanyName				nvarchar(1024),
	Gender					nvarchar(50),
	HomePhone				varchar(50),
	MobilePhone				varchar(50),
	OtherPhone				varchar(50),
	ImageId					uniqueidentifier,
	Status					int
)

--DATA CHANGE
GO
Update USGroup Set Title = 'Social Admin' where Title = 'Engagement Specialist'
Update USGroup Set Title = 'Social User' where Title = 'Blogger'

--SCHEMA CHANGE
GO
print 'Adding [OROrderItem].HandlingTax'
IF COL_LENGTH(N'[dbo].[OROrderItem]', N'HandlingTax') IS NULL
ALTER TABLE [dbo].[OROrderItem] ADD  [HandlingTax] MONEY NOT NULL DEFAULT ((0))

GO
print 'Adding [OROrderShipping].ShippingChargeTax'
IF COL_LENGTH(N'[dbo].[OROrderShipping]', N'ShippingChargeTax') IS NULL
ALTER TABLE [dbo].[OROrderShipping] ADD   [ShippingChargeTax] MONEY NOT NULL DEFAULT ((0))

GO
print 'Adding [ORRefundItem].Handling'
IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'Handling') IS  NULL
ALTER TABLE [dbo].[ORRefundItem] ADD [Handling] [money] NOT NULL  DEFAULT ((0))
GO
print 'Adding [ORRefundItem].HandlingTax'
IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'HandlingTax') IS  NULL 
ALTER TABLE [dbo].[ORRefundItem] ADD [HandlingTax] [money] NOT NULL  DEFAULT ((0))
GO
print 'Adding [ORRefundItem].ItemTax'
IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'ItemTax') IS  NULL
ALTER TABLE [dbo].[ORRefundItem] ADD [ItemTax] [money] NOT NULL  DEFAULT ((0))
GO
print 'Adding [ORRefundItem].ShippingTax'
IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'ShippingTax') IS  NULL
ALTER TABLE [dbo].[ORRefundItem] ADD [ShippingTax] [money] NOT NULL  DEFAULT ((0))
GO
print 'Dropping [ORRefundItem].Amount'
IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'Amount') IS NOT NULL
ALTER TABLE [dbo].[ORRefundItem]  drop column  [Amount]
GO
print 'Adding [ORRefundItem].Amount'
IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'Amount') IS NULL
ALTER TABLE [dbo].[ORRefundItem] ADD [Amount] AS (((((([ItemTotal]+[Shipping]) + Handling) +[AdditionalRefund])+[ItemTax])+[ShippingTax])+[HandlingTax])
GO


--This will take backup of orrefunitem , modifies the tax column as COMPUTED and then populate the itemtax with tax from backup.
IF NOT EXISTS ( SELECT  *
                FROM    sys.columns
                WHERE   Name = N'Tax'
                        AND Object_ID = OBJECT_ID(N'ORRefundItem') AND is_computed = 1) 
    BEGIN
    

		IF OBJECT_ID('tmpORRefundItem') IS  NULL
		SELECT  *
        INTO    tmpORRefundItem
        FROM    dbo.ORRefundItem

		
		declare @table_name nvarchar(256)
declare @col_name nvarchar(256)
declare @Command  nvarchar(1000)

set @table_name = N'ORRefundItem'
set @col_name = N'Tax'

select @Command = 'ALTER TABLE ' + @table_name + ' drop constraint ' + d.name
 from sys.tables t   
  join    sys.default_constraints d       
   on d.parent_object_id = t.object_id  
  join    sys.columns c      
   on c.object_id = t.object_id      
    and c.column_id = d.parent_column_id
 where t.name = @table_name
  and c.name = @col_name
  
 if(@Command is not null)
 execute(@Command)
 
		
		
        IF OBJECT_ID('DF_ORRefundItem_Tax') IS NOT NULL 
        	ALTER TABLE dbo.[ORRefundItem] DROP CONSTRAINT DF_ORRefundItem_Tax
        
        IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'Tax') IS NOT  NULL
          ALTER TABLE [dbo].[ORRefundItem]  drop column  [Tax]  
            
		IF COL_LENGTH(N'[dbo].[ORRefundItem]', N'Tax') IS  NULL
		ALTER TABLE [dbo].[ORRefundItem] ADD [Tax] AS  (([ItemTax]+[ShippingTax])+[HandlingTax])

            --Run script to alter ORRefundItemTable

        UPDATE  RI
        SET     RI.ItemTax = TRI.Tax
        FROM    dbo.ORRefundItem RI
                INNER JOIN tmpORRefundItem TRI ON TRI.Id = RI.Id

        DROP TABLE tmpORRefundItem


    END

GO
PRINT 'Creating procedure FormStructure_Delete'
GO
--DATABASE MODULE CHANGE
IF OBJECT_ID('[dbo].[FormStructure_Delete]') IS NOT NULL DROP PROCEDURE[dbo].[FormStructure_Delete]	
GO
CREATE PROCEDURE[dbo].[FormStructure_Delete] 
(
@Id uniqueidentifier
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @lft int,
	@rgt int,
	@error int,
	@Stmt	Varchar(100),
	@SiteId uniqueidentifier

--********************************************************************************
-- code
--********************************************************************************
BEGIN

	
	SELECT @lft=LftValue,@rgt=RgtValue,@SiteId=SiteId from COFormStructure  where Id=@Id 
		IF (@lft is not null AND @rgt is not null )
		BEGIN
		Set @Stmt ='Delete'
		DELETE  COFormStructure  where LftValue between @lft and @rgt  
		AND SiteId=@SiteId
		IF @@ERROR <> 0
		  BEGIN
			 RAISERROR('DBERROR||%s',16,1,@Stmt)
			 RETURN dbo.GetDataBaseErrorCode()
		  END

		Set @Stmt ='Adjust Tree'
		UPDATE COFormStructure  
			SET LftValue = CASE WHEN LftValue > @lft THEN LftValue - (@rgt - @lft + 1) ELSE LftValue END, 
			RgtValue = CASE WHEN RgtValue > @lft THEN RgtValue - (@rgt - @lft + 1) ELSE RgtValue END	
			Where SiteId=@SiteId
		IF @@ERROR <> 0
		  BEGIN
			  RAISERROR('DBERROR||%s',16,1,@Stmt)
			  RETURN dbo.GetDataBaseErrorCode()
		  END
      END     
END
GO
PRINT 'Altering procedure Membership_GetUserMembership'
GO
--exec Membership_GetUserMembership @ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@ApplicationId=NULL,@UserId='798613EE-7B85-4628-A6CC-E17EE80C09C5',@RoleId=NULL,@ObjectTypeId=NULL,@ParentSiteId=NULL
--exec Membership_GetUserMembership @ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@ApplicationId=NULL,@UserId='4FCBE87A-E452-4041-9523-9DEFA9E0422D',@RoleId=NULL,@ObjectTypeId=NULL,@ParentSiteId=NULL, @OnlyParentSite = 1



ALTER PROCEDURE [dbo].[Membership_GetUserMembership]
(
	@ProductId		uniqueidentifier = NULL,
	@ApplicationId	uniqueidentifier = NULL, --This is for getting applicationIds for the user and role.
	@UserId			uniqueidentifier,
	@RoleId			int = NULL,
	@ObjectTypeId	int,
	@ParentSiteId   uniqueidentifier = NULL,
	@OnlyParentSite bit=null,
	@OriginalMembership bit = null
	
)
AS
BEGIN

		Declare @Local_ProductId		uniqueidentifier,
		@Local_ApplicationId	uniqueidentifier, --This is for getting applicationIds for the user and role.
		@Local_UserId			uniqueidentifier,
		@Local_RoleId			int,
		@Local_ObjectTypeId	int,
		@Local_ParentSiteId   uniqueidentifier,
		@Local_OnlyParentSite bit
		Declare @siteTable as SiteTableType 
	
		IF(@OnlyParentSite IS NULL)
			SET @OnlyParentSite = 0
		
		Set @Local_ProductId = @ProductId 
		Set @Local_ApplicationId = @ApplicationId
		Set	@Local_UserId= @UserId
		Set	@Local_RoleId = @RoleId
		Set	@Local_ObjectTypeId	= @ObjectTypeId
		Set	@Local_ParentSiteId = @ParentSiteId
		Set @Local_OnlyParentSite = @OnlyParentSite

		IF(@OriginalMembership = 1)
		BEGIN
			SELECT DISTINCT SiteId AS ObjectId
			FROM	dbo.USSiteUser US
			WHERE ProductId = @ProductId
			AND UserId = @UserId
		END
		ELSE
		BEGIN

		--If Role Id is null then get all the sites in which the given user is having membership.
		IF (@Local_RoleId IS NULL)
		BEGIN
		Declare @EmptyGuid Uniqueidentifier
		Set @EmptyGuid = '00000000-0000-0000-0000-000000000000'
		--IF (@ParentSiteId IS NULL)
		--	SET @ParentSiteId = @EmptyGuid
	
			INSERT INTO @siteTable
			SELECT	DISTINCT SiteId AS ObjectId
			FROM	dbo.USSiteUser US
			INNER JOIN SISite S ON US.SiteId = S.Id
			WHERE	UserId = @Local_UserId AND  
					ProductId = ISNULL(@Local_ProductId, ProductId)
					AND ((@Local_OnlyParentSite = 0 AND S.ParentSiteId = ISNULL(@Local_ParentSiteId, S.ParentSiteId)) 
					OR (@Local_OnlyParentSite = 1 AND S.ParentSiteId = @EmptyGuid))
				
			
				IF(@Local_OnlyParentSite = 0)
					SELECT * from [dbo].[GetVariantSitesHavingPermission](@siteTable)
				ELSE	
					SELECT  * from @siteTable
		
		END
		ELSE IF (@Local_RoleId IS NOT NULL AND @Local_UserId IS NOT NULL) --Gets all the sites in which the user is having the given role
		BEGIN
	--		SELECT	DISTINCT ObjectId
	--		FROM	dbo.USMemberRoles MR
	--		WHERE	ApplicationId = ISNULL(@ApplicationId,ApplicationId)	AND
	--				RoleId = @RoleId	AND
	--				MemberId = @UserId
	--		UNION
	--
	--		SELECT DISTINCT	ObjectId
	--		FROM	dbo.USMemberGroup	MG, dbo.USMemberRoles MR
	--		WHERE	--MR.ApplicationId = ISNULL(@ApplicationId,MR.ApplicationId)	AND
	--				MR.RoleId = @RoleId	AND
	--				MR.MemberId	= MG.GroupId
			INSERT INTO @siteTable
			SELECT DISTINCT SU.SiteId  AS ObjectId
			FROM	USMemberRoles MR,
					USMemberGroup MG, 
					USSiteUser SU 
			WHERE	RoleId = @Local_RoleId AND 
					MR.MemberId = MG.GroupId AND 
					MG.MemberId = SU.UserId	AND
					SU.UserId = @Local_UserId	AND
					MR.ObjectTypeId = ISNULL(@Local_ObjectTypeId,MR.ObjectTypeId) AND
					MG.ApplicationId = SU.SiteId  AND  
					SU.ProductId = ISNULL(@Local_ProductId, SU.ProductId)
				
				SELECT * from [dbo].[GetVariantSitesHavingPermission](@siteTable)
		END
	END
END
GO
PRINT 'Altering procedure Workflow_GetNextActors'
GO

--exec Workflow_GetNextActors @ApplicationId='CBAAE16A-4188-4E77-9343-1A4130BA0B01',@ObjectId=default,@ObjectTypeId=8,@WorkflowId=default,@SequenceId=default,@ActorId=default,@ActorType=default,@PageIndex=default,@PageSize=default,@ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@GetSharedSites=1

-- Copyright © 2000-2006 Bridgeline Digital, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
   
-- Purpose: To get the details of workflow based on the parameters from the database.   
-- Author: Martin M V  
-- Created Date: 24-11-2006  
-- Created By: Martin M V  
--********************************************************************************  
-- List of all the changes with date and reason  
--********************************************************************************  
-- Modified Date:  
-- Modified By:  
-- Reason:  
-- Changes:  
  
--********************************************************************************  
-- Warm-up Script:  
--   exec [dbo].Workflow_GetNextActors   
--          @ApplicationId = 'uniqueidentifier'  null,  
--          @ObjectId = 'uniqueidentifier'  null,  
--          @ObjectTypeId = 'int'  null,  
--          @WorkflowId = 'uniqueidentifier'  null,  
--          @SequenceId = 'uniqueidentifier'  null,  
--          @ActorId = 'uniqueidentifier'  null,  
--          @ActorType = 'int'  null,  
--          @PageIndex = 'int' or 0,  
--          @PageSize = 'int' or 0,  
--********************************************************************************  
ALTER PROCEDURE [dbo].[Workflow_GetNextActors](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
            @ApplicationId  uniqueidentifier =  null,  
            @ObjectId  uniqueidentifier =  null,  
            @ObjectTypeId int = null,  
            @WorkflowId  uniqueidentifier = null,  
            @SequenceId  uniqueidentifier = null,  
            @ActorId  uniqueidentifier  =  null,  
            @ActorType  int =  null,  
   @PageIndex  int = null,  
   @PageSize  int = null,  
   @ProductId  uniqueidentifier = null,
   @GetSharedSites bit = null  
)  
AS  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE  
   @PageLowerBound   bigint,  
   @PageUpperBound   bigint --,  
   --@TotalRecords   bigint  
--********************************************************************************  
-- Procedure Body  
--********************************************************************************  
BEGIN  
  SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)  
  SET @PageUpperBound = @PageLowerBound - @PageSize + 1;  
  DECLARE @Sites Table ( SiteId uniqueidentifier)
  
   IF(@GetSharedSites = 1)
   BEGIN
	INSERT INTO @Sites  SELECT SiteId from GetAncestorSites(@ApplicationId)
   END
   ELSE
   BEGIN
		IF(@ApplicationId IS NOT NULL)
		BEGIN
			INSERT INTO @Sites SELECT @ApplicationId
		END	
   END
  
  
IF(@ApplicationId IS NULL)
BEGIN   
  ;WITH ResultEntries AS  
  (  
            SELECT   
     ROW_NUMBER() OVER (ORDER BY A.WorkflowId) AS RowNumber,  
     A.ObjectId,   
     A.ObjectTypeId,  
     A.WorkflowId,  
     A.SequenceId,  
     A.NextActorId,  
     A.NextActorType  
            FROM dbo.WFNextActor A,   
     dbo.WFWorkflow B,  
     dbo.WFWorkflowExecution C  
            WHERE A.ObjectId  = Isnull(@ObjectId, A.ObjectId)   AND   
     A.ObjectTypeId = Isnull(@ObjectTypeId, A.ObjectTypeId) AND  
     A.WorkflowId = Isnull(@WorkflowId, A.WorkflowId)  AND  
     A.SequenceId = Isnull(@SequenceId, A.SequenceId)  AND  
     A.NextActorId = Isnull(@ActorId, A.NextActorId)   AND  
     A.NextActorType = Isnull(@ActorType, A.NextActorType)  AND  
     B.ApplicationId = Isnull(@ApplicationId, B.ApplicationId) AND  
     --B.ApplicationId IN (SELECT SiteId from @Sites) AND
     B.ProductId  = Isnull(@ProductId, B.ProductId)   AND  
     B.Id   = A.WorkflowId       AND  
     C.WorkflowId = A.WorkflowId       AND   
     C.ObjectId  = A.ObjectId AND C.ObjectTypeId = A.ObjectTypeId AND Completed = 0  
  )  
  SELECT *   
  FROM ResultEntries   
  WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))  

END
ELSE
BEGIN
	  ;WITH ResultEntries AS  
  (  
            SELECT   
     ROW_NUMBER() OVER (ORDER BY A.WorkflowId) AS RowNumber,  
     A.ObjectId,   
     A.ObjectTypeId,  
     A.WorkflowId,  
     A.SequenceId,  
     A.NextActorId,  
     A.NextActorType  
            FROM dbo.WFNextActor A,   
     dbo.WFWorkflow B,  
     dbo.WFWorkflowExecution C  
            WHERE A.ObjectId  = Isnull(@ObjectId, A.ObjectId)   AND   
     A.ObjectTypeId = Isnull(@ObjectTypeId, A.ObjectTypeId) AND  
     A.WorkflowId = Isnull(@WorkflowId, A.WorkflowId)  AND  
     A.SequenceId = Isnull(@SequenceId, A.SequenceId)  AND  
     A.NextActorId = Isnull(@ActorId, A.NextActorId)   AND  
     A.NextActorType = Isnull(@ActorType, A.NextActorType)  AND  
     --B.ApplicationId = Isnull(@ApplicationId, B.ApplicationId) AND  
     B.ApplicationId IN (SELECT SiteId from @Sites) AND
     B.ProductId  = Isnull(@ProductId, B.ProductId)   AND  
     B.Id   = A.WorkflowId       AND  
     C.WorkflowId = A.WorkflowId       AND   
     C.ObjectId  = A.ObjectId AND C.ObjectTypeId = A.ObjectTypeId AND Completed = 0  
  )  
  

  SELECT *   
  FROM ResultEntries   
  WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))  

END
  
  
--  SELECT  @TotalRecords = COUNT(*)  
--  FROM    ResultEntries  
  
  --RETURN @TotalRecords  
END  
GO
PRINT 'Altering procedure Warehouse_GetInventoryByXmlGuids'

GO
ALTER PROCEDURE [dbo].[Warehouse_GetInventoryByXmlGuids]
    (
      @Ids XML ,
      @WarehouseTypeId INT ,
      @WarehouseId UNIQUEIDENTIFIER ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
	
    )
AS 
    BEGIN
        SELECT DISTINCT
                I.[Id] ,
                I.[WarehouseId] ,
                I.[SKUId] ,
                ISNULL(dbo.GetBundleQuantity(I.SKUId), I.[Quantity]) AS Quantity ,
                ISNULL(O.Quantity, 0) AS AllocatedQuantity ,
                ISNULL(BO.BackOrderQuantity, 0) AS BackOrderAllocatedQuantity ,
                I.[LowQuantityAlert] ,
                I.[FirstTimeOrderMinimum] ,
                I.[OrderMinimum] ,
                I.[OrderIncrement] ,
                I.[ReasonId] ,
                I.[ReasonDetail] ,
                I.[Status] ,
                I.[CreatedBy] ,
                I.CreatedDate AS CreatedDate ,
                I.[ModifiedBy] ,
                I.ModifiedDate AS ModifiedDate
        FROM    INInventory I
                INNER JOIN INWarehouse W ON I.WarehouseId = W.Id
                LEFT JOIN dbo.vwAllocatedQuantityperWarehouse O ON I.SKUId = O.ProductSKUId 
		--REMOVED
		--AND O.WarehouseId =W.Id
                LEFT JOIN dbo.vwBackOrderAllocatedQuantityPerWarehouse BO ON I.SKUId = BO.ProductSKUId 
		--REMOVED
		--AND BO.WarehouseId =W.Id
                INNER JOIN @Ids.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON I.SKUId = tab.col.value('text()[1]',
                                                              'uniqueidentifier')
        WHERE   I.Status = 1 
	  --ADDED
                AND ( O.ProductSKUId IS NULL
                      OR I.WarehouseId = O.WarehouseId
                    )
                AND ( BO.ProductSKUId IS NULL
                      OR W.Id = BO.WarehouseId
                    )
                AND ( @WarehouseId IS NULL
                      OR W.Id = @WarehouseId
                    )
                AND W.TypeId = @WarehouseTypeId
    END

	GO
	PRINT 'Altering procedure Membership_CreateDefaultMembership'
	GO
	ALTER PROCEDURE [dbo].[Membership_CreateDefaultMembership]
--********************************************************************************
-- PARAMETERS
	@ApplicationId				uniqueidentifier,
	@ProductId					uniqueidentifier 
--********************************************************************************

AS
BEGIN

	-- Get all the users in the admin role of analytics
	-- Add membership for the users in Analytics ( insert into ussiteuser table)
	-- Add all the users into admin group ( insert into usmembergroup table)
	-- 
	--This requires because the group id will change on each installation. Here the group name should not be change.
	DECLARE @Administrator uniqueidentifier
	DECLARE @Id int

	SELECT @Administrator = Id FROM USGroup WHERE Title = 'Administrator'
	SET @Id = 16 --Role id of the Analytics administrator
	
	IF (@ProductId = 'CAB9FF78-9440-46C3-A960-27F53D743D89') -- Analytics
	BEGIN
		SET @Id = 16 --Role id of the Analytics administrator
		SELECT @Administrator = Id FROM USGroup WHERE Title = 'Administrator'
	END
	IF (@ProductId = 'CCF96E1D-84DB-46E3-B79E-C7392061219B')
	BEGIN
		SET @Id = 5 --Role id of the Commerce Site administrator
		SELECT @Administrator = Id FROM USGroup WHERE Title = 'Site Administrator'
	END
	IF (@ProductId = 'CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E')
	BEGIN
		SET @Id = 5 --Role id of the Marketier Site administrator
		SELECT @Administrator = Id FROM USGroup WHERE Title = 'Site Administrator'
	END

	
	DECLARE  @MemberType int -- 1 for user
	SET @MemberType = 1

	DECLARE @IsSystemUser int
	SET @IsSystemUser = 1

	DECLARE @ObjectTypeId int --
	SET @ObjectTypeId = NULL

	DECLARE @WithProfile bit
	SET @WithProfile = 0

	DECLARE @PageIndex int
	SET @PageIndex = 0

	DECLARE @PageSize int
	SET @PageSize = 1


	--Gets all the users present in the Administrator role in Analytics
	CREATE TABLE #temp
	(
		AppId uniqueidentifier,
		Member uniqueidentifier,
		MemType smallint,
		GrpId	uniqueidentifier
	)	

	INSERT INTO #temp
	SELECT DISTINCT @ApplicationId,Id,1,@Administrator
	FROM [User_GetUsersInRole](@Id, @MemberType, NULL, @IsSystemUser, @ObjectTypeId, @ProductId)
	
	INSERT INTO USMemberGroup(ApplicationId,MemberId,MemberType,GroupId)
	SELECT AppId,Member,MemType,GrpId FROM #temp

	/*
		This is to make the memer
	*/
		
	 --This will delete the duplicate entries in the db.  
	   DELETE FROM dbo.USSiteUser  
	   FROM #temp T  
	   WHERE UserId = T.AppId AND   
			SiteId = @ApplicationId AND  
			ProductId = @ProductId 

	
	INSERT INTO USSiteUser (SiteId,UserId,IsSystemUser,ProductId)
	SELECT   AppId,Member,1,@ProductId FROM #temp
	
	DROP TABLE #temp

	IF (@@ERROR <> 0)
	BEGIN
		RAISERROR ('DBERROR||%s', 16, 1, 'Failed')
		RETURN dbo.GetDataBaseErrorCode()
	END
	
	RETURN @@ROWCOUNT
END
--********************************************************************************


GO
PRINT 'Altering procedure Site_Save'
GO
ALTER PROCEDURE [dbo].[Site_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@URL				xml,
	@VirtualPath		nvarchar(4000)=null,
	@ParentSiteId		uniqueidentifier=null,
	@SiteType 	        int=null,
	@Keywords	        nvarchar(4000)=null,
	@Status				int=null,
	@SendNotification	bit=null,
	@SubmitToTranslation bit=null,
	@AllowMasterWebSiteUser bit=null,
	@ImportAllAdminPermission bit=null,
	@Prefix			nvarchar(5) = null, 
	@UICulture			nvarchar(10) = null,
	@LoginUrl		nvarchar(max) =null,
	@DefaultURL		nvarchar(max) =null,
	@NotifyOnTranslationError bit = null,
	@TranslationEnabled bit = null,
	@TranslatePagePropertiesByDefault bit = null,
	@AutoImportTranslatedSubmissions bit = null,
	@DefaultTranslateToLanguage nvarchar(10) = null,
	@PageImportOptions int =null,
	@ImportContentStructure bit =null,
	@ImportFileStructure bit=null,
	@ImportImageStructure bit=null,
	@ImportFormsStructure bit=null,
	@SiteGroups nvarchar(4000) = null,
	@MasterSiteId		uniqueidentifier=null,
	@PrimarySiteUrl nvarchar(max) =null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
Declare @temp table(url1 nvarchar(max))
Declare @displayTitle varchar(512), @pageMapNodeDescription varchar(512), @pageMapNodeXml xml, @pageMapNodeId uniqueidentifier
Declare @LftValue int,@RgtValue int
Begin
--********************************************************************************
-- code
--********************************************************************************
	
	SET @displayTitle = @Title
	SET @pageMapNodeDescription = @Title

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  SISite where  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end

	set @Now = getdate()
	--set @Status= dbo.GetActiveStatus()



	if (@Id is null)			-- new insert
	begin
	
	IF (@ParentSiteId is not null AND @ParentSiteId <> '00000000-0000-0000-0000-000000000000' )  
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue
		from SISite
		Where Id=@ParentSiteId and MasterSiteId=@MasterSiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END

		Update SISite Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
								RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 


		set @stmt = 'Site Insert'
		set @Id = newid();
		insert into SISite  (
					   Id,	
					   Title,
                       Description,
                       CreatedBy,
                       CreatedDate,
					   Status,
                       URL,
                       VirtualPath,
					   ParentSiteId,
					   Type,
                       Keywords,
                       HostingPackageInstanceId,
                       SendNotification,
                       SubmitToTranslation,
                       AllowMasterWebSiteUser,
                       ImportAllAdminPermission,
                       Prefix, 
                       UICulture, 
                       LoginUrl, 
                       DefaultURL,
                       NotifyOnTranslationError,
                       TranslationEnabled,
                       TranslatePagePropertiesByDefault,
                       AutoImportTranslatedSubmissions,
                       DefaultTranslateToLanguage,
                       PageImportOptions,
                       ImportContentStructure,
                       ImportFileStructure,
                       ImportImageStructure,
                       ImportFormsStructure,
                       MasterSiteId,
                       PrimarySiteUrl,
                       LftValue,
                       RgtValue) 
					values (
						@Id,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@URL,
						@VirtualPath,
						@ParentSiteId,
						@SiteType,
						@Keywords,
						0,
						@SendNotification,
						@SubmitToTranslation,
						@AllowMasterWebSiteUser,
						@ImportAllAdminPermission, 
						@Prefix, 
						@UICulture, 
						@LoginUrl, 
						@DefaultURL,
						@NotifyOnTranslationError,
						@TranslationEnabled,
						@TranslatePagePropertiesByDefault,
						@AutoImportTranslatedSubmissions,
						@DefaultTranslateToLanguage,
						@PageImportOptions,
						@ImportContentStructure,
						@ImportFileStructure,
						@ImportImageStructure,
						@ImportFormsStructure,
						@MasterSiteId,
						@PrimarySiteUrl,
						@RgtValue,
						@RgtValue + 1)

		-- Inserting in SiteGroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')


			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			

			
			
         end
         else			-- update
         begin
			set @stmt = 'Site Update'
			
			Update HSStructure Set Title=@Title Where Id=@Id
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			--SELECT @pageMapNodeXml =  cast((PageMapNodexml + '</pageMapNode>') as xml), @pageMapNodeId = PageMapNodeId FROM PageMapNode Where SiteId = PageMapNodeId And SiteId = @Id								

			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@displayTitle)[1]
			--				  with
			--				(sql:variable("@displayTitle"))')
			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@description)[1]
			--				  with
			--				(sql:variable("@pageMapNodeDescription"))')			
			
			Update PageMapNode set displaytitle=@Title
			Where PageMapNodeId = @pageMapNodeId ANd SiteId = @Id
			-- Here we are not updating the friendly url
						
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			set @rowcount= 0
			
			select * from SISite where Id=@Id
			update	SISite  with (rowlock)	set 
            Title =@Title,
            Description=@Description,
            ModifiedBy=@ModifiedBy,
            ModifiedDate=@Now,
            Status=@Status,
            URL=@URL,
            VirtualPath=@VirtualPath,
            ParentSiteId=@ParentSiteId,
            Type=@SiteType,
            Keywords= @Keywords,
            SendNotification = @SendNotification,
            SubmitToTranslation = @SubmitToTranslation,
            AllowMasterWebSiteUser=@AllowMasterWebSiteUser,
			ImportAllAdminPermission=@ImportAllAdminPermission,
			Prefix = @Prefix, 
			UICulture = @UICulture,
			LoginUrl = @LoginUrl, 
			DefaultURL =@DefaultURL,
			NotifyOnTranslationError = @NotifyOnTranslationError,
			TranslationEnabled = @TranslationEnabled,
			TranslatePagePropertiesByDefault = @TranslatePagePropertiesByDefault,
			AutoImportTranslatedSubmissions = @AutoImportTranslatedSubmissions,
			DefaultTranslateToLanguage = @DefaultTranslateToLanguage,	
			PageImportOptions =	@PageImportOptions,
			ImportContentStructure=@ImportContentStructure,
			ImportFileStructure=@ImportFileStructure,
			ImportImageStructure=@ImportImageStructure,
			ImportFormsStructure=@ImportFormsStructure,
			MasterSiteId = @MasterSiteId,
			PrimarySiteUrl = @PrimarySiteUrl
 	where 	Id    = @Id --and
	--isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
	
	select @error = @@error, @rowcount = @@rowcount
	
			--Deleting from SiteGroups
			DELETE FROM SISiteGroup WHERE SiteId = @id
			-- Insert into sitegroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')
		
			
	

if @error <> 0
  begin
      raiserror('DBERROR||%s',16,1,@stmt)
      return dbo.GetDataBaseErrorCode()
   end
if @rowcount = 0
  begin
    raiserror('CONCURRENCYERROR',16,1)
     return dbo.GetDataConcurrencyErrorCode()			-- concurrency error
end	
end
End


GO
PRINT 'Altering procedure SqlDirectoryProvider_Save'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_Save]
(
	@XmlString		xml,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@VirtualPath	nvarchar(max)  OUT,
	@SiteName		nvarchar(256) OUT
)
AS
BEGIN
	DECLARE @tbDirectory TYDirectory	
	INSERT INTO @tbDirectory(Id,
		Title,
		Description,
		Attributes,
		FolderIconPath,
		PhysicalPath,
		[Size],
		Status,
		ThumbnailImagePath,
		ObjectTypeId,
		ParentId,
		Lft,
		Rgt,
		IsSystem,
		IsMarketierDir,
		AllowAccessInChildrenSites,
		SiteId,
		MicrositeId)						
	SELECT  tab.col.value('@Id[1]','uniqueidentifier') Id,
		tab.col.value('@Title[1]','NVarchar(256)')Title,
		tab.col.value('@Description[1]','Nvarchar(1024)')Description,
		tab.col.value('@Attributes[1]','int')Attributes,
		tab.col.value('@FolderIconPath[1]','nvarchar(4000)')FolderIconPath,
		tab.col.value('@PhysicalPath[1]','nvarchar(4000)')PhysicalPath,
		tab.col.value('@Size[1]','bigint')[Size],
		tab.col.value('@Status[1]','int')Status,
		tab.col.value('@ThumbnailImagePath[1]','nvarchar(4000)')ThumbnailImagePath,
		tab.col.value('@ObjectTypeId[1]','int')ObjectTypeId,
		tab.col.value('@ParentId[1]','uniqueidentifier')ParentId,
		tab.col.value('@Lft[1]','bigint')Lft,
		tab.col.value('@Rgt[1]','bigint')Rgt,
		tab.col.value('@IsSystem[1]','bit')IsSystem,
		tab.col.value('@IsMarketierDir[1]','bit')IsMarketierDir,
		ISNULL(tab.col.value('@AllowAccessInChildrenSites[1]','bit'),0) AllowAccessInChildrenSites,
		@ApplicationId,
		@MicroSiteId
	FROM @XmlString.nodes('/SiteDirectoryCollection/SiteDirectory') tab(col)
	Order By Lft

	-- if one of the node is content then everything is content, no mix and match
	DECLARE @ObjectTypeId int
	SET @ObjectTypeId = (SELECT TOP 1 ObjectTypeId FROM @tbDirectory)
	
	IF (@ObjectTypeId = 7)
		EXEC [dbo].[SqlDirectoryProvider_SaveContentDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 9)
		EXEC [dbo].[SqlDirectoryProvider_SaveFileDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 33)
		EXEC [dbo].[SqlDirectoryProvider_SaveImageDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 38)
		EXEC [dbo].[SqlDirectoryProvider_SaveFormDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE
	BEGIN
		-- All other libraries are shared
		DECLARE @MasterSiteId uniqueidentifier
		SET @MasterSiteId = (SELECT ParentSiteId FROM SISite WHERE Id = @ApplicationId)
		IF @MasterSiteId IS NOT NULL AND @MasterSiteId != dbo.GetEmptyGUID()
			SET @ApplicationId = @MasterSiteId
		EXEC [dbo].[SqlDirectoryProvider_SaveDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	END
	
	SET @VirtualPath = (SELECT Id, dbo.GetVirtualPathByObjectType(Id, @ObjectTypeId) VirtualPath, Attributes, SiteId 
							FROM @tbDirectory SISiteDirectory FOR XML Auto)
	
	SELECT @SiteName = Title FROM SISite WHERE Id = @ApplicationId
END

GO
PRINT 'Altering procedure [OrderItem_Save]'
GO
ALTER PROCEDURE [dbo].[OrderItem_Save]  
(   
 @Id uniqueidentifier output,  
 @OrderId uniqueidentifier,  
 @OrderShippingId uniqueidentifier,  
 @OrderItemStatusId int,  
 @ProductSKUId uniqueidentifier,  
 @Quantity decimal(18,2),  
 @Price money,  
 @UnitCost money,    
 @IsFreeShipping bit,  
 @Sequence int,  
 @Status int,  
 --@CreatedDate datetime,  
 @CreatedBy uniqueidentifier,  
 --@ModifiedDate datetime,  
 @ModifiedBy uniqueidentifier,
@HandlingCharge money,
@ParentOrderItemId uniqueidentifier,
@BundleItemId uniqueidentifier,
@CartItemId uniqueidentifier,
@Tax money,
@Discount money,
@HandlingTax Money
)  
  
AS  
BEGIN       

  DECLARE @CreatedDate datetime, @ModifiedDate DateTime
IF(@OrderShippingId is null OR @OrderShippingId = dbo.GetEmptyGUID())
BEGIN
	SET @OrderShippingId = null
END
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
  set @Id = newid()  
  set @CreatedDate = getUTCDate()
  INSERT INTO OROrderItem  
           ([Id]  
   ,[OrderId]  
   ,[OrderShippingId]  
   ,[OrderItemStatusId]  
   ,[ProductSKUId]  
   ,[Quantity]  
   ,[Price]  
   ,[UnitCost]  
   ,[IsFreeShipping]  
   ,[Sequence]  
   ,[Status]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[HandlingCharge]
   ,[ParentOrderItemId]
   ,[BundleItemId]
	,[CartItemId]
	,[Tax]
	,[Discount]
	,[HandlingTax])  
     VALUES  
           (@Id,   
   @OrderId ,  
   @OrderShippingId ,  
   @OrderItemStatusId ,  
   @ProductSKUId ,  
   @Quantity ,  
   @Price ,  
   @UnitCost ,  
   @IsFreeShipping ,  
   @Sequence ,  
   @Status ,  
   @CreatedDate ,  
   @CreatedBy ,  
   @CreatedDate ,  
   @ModifiedBy, 
   @HandlingCharge,
   @ParentOrderItemId,
   @BundleItemId,
	@CartItemId,
	@Tax,
	@Discount,
	@HandlingTax)  

 end  
 else  
 begin  
  --set @ModifiedDate= getUTCDate()
  update OROrderItem  
  set   
   [OrderId]=@OrderId ,  
   [OrderShippingId]=@OrderShippingId ,  
   [OrderItemStatusId]=@OrderItemStatusId ,  
   [ProductSKUId]=@ProductSKUId ,  
   [Quantity]=@Quantity ,  
   [Price]=@Price ,  
   [UnitCost]=@UnitCost ,  
   [IsFreeShipping]=@IsFreeShipping ,  
   [Sequence]=@Sequence ,  
   [Status]=@Status ,  
   --[CreatedDate]=@CreatedDate ,  
   [CreatedBy]=@CreatedBy ,  
   [ModifiedDate]=GetUTCDate() ,  
   [ModifiedBy]=@ModifiedBy ,
   [HandlingCharge] = @HandlingCharge,
   [ParentOrderItemId]=@ParentOrderItemId,
   [BundleItemId]=@BundleItemId,
   [CartItemId]=@CartItemId,
	[Tax] = @Tax,
	[Discount] = @Discount,
	[HandlingTax] = @HandlingTax
  where Id=@Id  
     


 end  


Update OROrderItemAttributeValue set OrderId=b.OrderId , OrderItemId=b.Id from OROrderItemAttributeValue a, 
	OROrderItem b where 
	a.CartItemId = b.CartItemId
	and a.OrderItemId is null and b.CartItemId=@CartItemId


	DECLARE @OrderItemQuantity money
	DECLARE @OrderShipmentItemQuantity money
	DECLARE @OrderStatusId int
	DECLARE @count1 int
	DECLARE @count2 int

	Select @OrderItemQuantity = Quantity
	FROM  OROrderItem  
	Where Id = @Id

	Select @OrderShipmentItemQuantity = sum(Quantity)
	FROM  FFOrderShipmentItem  SI
	INNER JOIN FFOrderShipmentPackage  S ON SI.OrderShipmentContainerId=S.Id
	Where OrderItemId =@Id AND ShipmentStatus=2
	Group By OrderItemId

	If(@OrderItemQuantity = @OrderShipmentItemQuantity OR @OrderShipmentItemQuantity>0.0)
	BEGIN
	
		Update OROrderItem 
		Set OrderItemStatusId=Case When @OrderItemQuantity = @OrderShipmentItemQuantity Then 2 Else 7 End
		Where  Id =@Id

		if @@error <> 0
		begin
			raiserror('DBERROR||%s',16,1,'Update Status Shipped OrderShipmentItem')
			return dbo.GetDataBaseErrorCode()	
		end
	END

--	SELECT TOP 1 @OrderId=OrderId 
--	FROM OROrderItem	
--	WHERE Id=@Id
--
--
--	Select @OrderStatusId =OrderStatusId 
--	from OROrder
--	Where Id=@OrderId
--
--	
--	IF @OrderStatusId=1 OR @OrderStatusId=11
--	BEGIN
--		Select @count1 = count(*) 
--		from OROrderItem
--		Where OrderId=@OrderId
--			
--		Select @count2=  count(*)
--		from OROrderItem
--		Where OrderId=@OrderId AND OrderItemStatusId=2
--		
--		
--	IF(@count1 = @count2)
--		BEGIN
--			Update OROrder Set OrderStatusId=2
--			Where Id=@OrderId	
--
--		END
--		ELSE IF(@count2>0 OR (Select count(*) 
--			from OROrderItem
--			Where OrderId=@OrderId AND OrderItemStatusId=7) >0)
--		BEGIN
--			Update OROrder Set OrderStatusId=11
--			Where Id=@OrderId
--		END
--	END



END  

GO
PRINT 'Altering procedure OrderShipping_Get'
  
--********************************************************************************
GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[OrderShipping_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
 SELECT [Id]
  ,[OrderId]
  ,[ShippingOptionId]
  ,[OrderShippingAddressId]
  ,dbo.ConvertTimeFromUtc(ShipOnDate,@ApplicationId)[ShipOnDate]
  ,dbo.ConvertTimeFromUtc(ShipDeliveryDate,@ApplicationId)[ShipDeliveryDate]
  ,[TaxPercentage]
  ,[ShippingTotal]
  ,[Sequence]
  ,[IsManualTotal]
  ,[Greeting]
  ,[Tax]
  ,[SubTotal]
  ,[TotalDiscount]
  ,[ShippingCharge]
  ,[Status]
  ,[CreatedBy]
  ,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate
  ,[ModifiedBy]
  ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
  ,[ShippingDiscount]
  ,ShipmentHash
  ,IsBackOrder
  ,IsHandlingIncluded
  ,ShippingChargeTax
 FROM OROrderShipping OS where [Id]=@Id

 SELECT O.* FROM ORShippingOption O
 INNER JOIN OROrderShipping OS ON OS.ShippingOptionId =O.Id
 where OS.[Id]=@Id

 SELECT S.* 
 FROM ORShippingOption O
 INNER Join ORShipper S on O.ShipperId = S.Id
 INNER JOIN OROrderShipping OS ON OS.ShippingOptionId =O.Id
 where OS.[Id]=@Id
 
 
SELECT [Id]
      ,[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(ShippedDate,@ApplicationId)ShippedDate
      ,[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(CreateDate,@ApplicationId)CreateDate
      ,[CreatedBy]
      ,[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
   ,[EstimatedShipmentTotal]
   ,ShipmentStatus
   ,WarehouseId
 ,ExternalReferenceNumber
FROM [dbo].[FFOrderShipment]
Where OrderShippingId =@Id
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
Where OrderShippingId =@Id


SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,SP.[ShippingContainerId]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
Where OrderShippingId =@Id

END


GO
PRINT 'Altering procedure OrderShipping_Save'
GO
ALTER PROCEDURE [dbo].[OrderShipping_Save]  
(   
 @Id uniqueidentifier output,  
 @OrderId uniqueidentifier,  
 @ShippingOptionId uniqueidentifier,  
 @OrderShippingAddressId uniqueidentifier,  
 @ShipOnDate datetime,  
 @ShipDeliveryDate datetime,  
 @TaxPercentage decimal(18,2),  
 @ShippingTotal money,
 @ShippingDiscount money,
 @Sequence int,  
 @IsManualTotal bit,  
 @Greeting nvarchar(max),
 @Tax money,
 @SubTotal money,
 @TotalDiscount money,
 @TaxableTotal money,
 @ShippingCharge money,  
 @Status int,  
 @CreatedBy uniqueidentifier,  
 --@CreatedDate datetime,  
 @ModifiedBy uniqueidentifier,  
 @ApplicationId uniqueidentifier,
 @ShipmentHash nvarchar(max),
 @IsBackOrder bit = null,
 @IsHandlingIncluded bit=null,
 @ShippingChargeTax money = null
)  
  
AS  
BEGIN       
  DECLARE @CreatedDate datetime, @ModifiedDate DateTime

If(@ShippingOptionId is null OR @ShippingOptionId = dbo.GetEmptyGUID())
BEGIN
	SET @ShippingOptionId = null
END

If(@OrderShippingAddressId is null OR @OrderShippingAddressId = dbo.GetEmptyGUID())
BEGIN
	SET @OrderShippingAddressId = null
END

If(@IsBackOrder is null)
BEGIN
	SET @IsBackOrder = 0
END

 if(@Id is null OR @Id = dbo.GetEmptyGUID() OR (Select count(*) FROM OROrderShipping Where Id=@Id) =0)  
 begin  
  set @Id = newid()  
  set @CreatedDate = getUTCDate()
  INSERT INTO OROrderShipping  
           ([Id]  
   ,[OrderId]  
   ,[ShippingOptionId]  
   ,[OrderShippingAddressId]  
   ,[ShipOnDate]  
   ,[ShipDeliveryDate]  
   ,[TaxPercentage]  
   ,[ShippingTotal] 
   ,[ShippingDiscount] 
   ,[Sequence]  
   ,[IsManualTotal]  
   ,[Greeting] 
   ,[Tax] 
   ,[TaxableTotal]
   ,[SubTotal] 
   ,[TotalDiscount] 
   ,[ShippingCharge]  
   ,[Status]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[ShipmentHash]
   ,[IsBackOrder]
   ,[IsHandlingIncluded]
   ,[ShippingChargeTax])  
     VALUES  
           (@Id,  
   @OrderId,  
   @ShippingOptionId,  
   @OrderShippingAddressId,  
   dbo.ConvertTimeToUtc(@ShipOnDate,@ApplicationId),  
   dbo.ConvertTimeToUtc(@ShipDeliveryDate,@ApplicationId),  
   @TaxPercentage,  
   @ShippingTotal, 
   @ShippingDiscount, 
   @Sequence,  
   @IsManualTotal,  
   @Greeting, 
   @Tax, 
   @TaxableTotal,
   @SubTotal, 
   @TotalDiscount, 
   @ShippingCharge,  
   @Status,  
   @CreatedDate,  
   @CreatedBy,  
   @CreatedDate,  
   @ModifiedBy,
   @ShipmentHash,
   @IsBackOrder,
   @IsHandlingIncluded,
   @ShippingChargeTax)  
 end  
 else  
 begin  
  update OROrderShipping  
  set   
   OrderId=@OrderId,  
   ShippingOptionId=@ShippingOptionId,  
   OrderShippingAddressId=@OrderShippingAddressId,  
   ShipOnDate=@ShipOnDate,  
   ShipDeliveryDate=@ShipDeliveryDate,  
   TaxPercentage=@TaxPercentage,  
   ShippingTotal=@ShippingTotal,  
   ShippingDiscount=@ShippingDiscount,
   Sequence=@Sequence,  
   IsManualTotal=@IsManualTotal,  
   Greeting=@Greeting,
   Tax=@Tax,
   TaxableTotal =@TaxableTotal,
   SubTotal=@SubTotal,
   TotalDiscount=@TotalDiscount,
   ShippingCharge=@ShippingCharge,
   Status=@Status,  
   --CreatedDate=@CreatedDate,  
   CreatedBy=@CreatedBy,  
   ModifiedDate=GetUTCDate(),  
    ModifiedBy=@ModifiedBy,
    ShipmentHash=@ShipmentHash,  
    IsBackOrder=@IsBackOrder,
    IsHandlingIncluded=@IsHandlingIncluded,
	ShippingChargeTax = @ShippingChargeTax
   where Id=@Id  
     
 end  

END  


GO
PRINT 'Altering procedure Order_Get'
GO
ALTER PROCEDURE [dbo].[Order_Get]
    (
      @Searchkeyword NVARCHAR(4000) = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @PageSize INT = NULL ,
      @PageIndex INT = 1 ,
      @CustomerId UNIQUEIDENTIFIER = NULL ,
      @OrderDate DATETIME = NULL ,
      @StartDate DATETIME = NULL ,
      @EndDate DATETIME = NULL ,
      @OrderStatus XML = NULL ,
      @ExternalShippingStatus INT = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL    
    )
AS 
    BEGIN    
        DECLARE @tblOrderStatus TABLE ( OrderStatus INT )    
        IF @OrderStatus IS NOT NULL 
            BEGIN
                INSERT  @tblOrderStatus
                        ( OrderStatus
                        )
                        SELECT  tab.col.value('text()[1]', 'int')
                        FROM    @OrderStatus.nodes('GenericCollectionOfInt32/int') tab ( col )
            END
    
        IF @SearchKeyword IS NULL 
            SET @SearchKeyword = '%'    
        ELSE 
            SET @SearchKeyword = '%' + ( @Searchkeyword ) + '%'    
    
    
        IF @PageSize IS NULL 
            SET @PageSize = 2147483647    
    
        IF @SortColumn IS NULL 
            SET @SortColumn = 'OrderDate Desc'    
    
        SET @SortColumn = LOWER(@SortColumn)    
    
    	 
        DECLARE @lOrderDate DATETIME
        IF @OrderDate IS NULL 
            BEGIN
                DECLARE @lStartDate DATETIME
                IF @StartDate IS NULL 
                    SET @lStartDate = '01-01-1970'
                ELSE 
                    SET @lStartDate = @StartDate
			
                DECLARE @lEndDate DATETIME
                IF @EndDate IS NULL 
                    SET @lEndDate = '01-01-3000'
                ELSE 
                    SET @lEndDate = @EndDate
            END
        ELSE 
            BEGIN
                SET @lStartDate = @OrderDate
                SET @lEndDate = DATEADD(d, 1, @OrderDate)
            END


        SET @lStartDate = dbo.ConvertTimeToUtc(@lStartDate, @ApplicationId)
        SET @lEndDate = dbo.ConvertTimeToUtc(@lEndDate, @ApplicationId)

		 
        DECLARE @tblOrders TABLE
            (
              Row_ID INT PRIMARY KEY ,
              Id UNIQUEIDENTIFIER ,
              PurchaseOrderNumber NVARCHAR(100) ,
              ExternalOrderNumber NVARCHAR(100) ,
              OrderTypeId INT ,
              OrderDate DATETIME ,
              FirstName NVARCHAR(255) ,
              MiddleName NVARCHAR(255) ,
              LastName NVARCHAR(255) ,
              OrderStatusId INT ,
              Quantity MONEY ,
              NumSkus DECIMAL(38, 2) ,
              OrderTotal MONEY ,
              CODCharges MONEY ,
              TotalDiscount MONEY ,
              RefundTotal MONEY ,
              TaxableOrderTotal MONEY ,
              GrandTotal MONEY ,
              TotalTax MONEY ,
              ShippingTotal MONEY ,
              TotalShippingCharge MONEY ,
              PaymentRemaining MONEY,
              SiteId UNIQUEIDENTIFIER ,
              CustomerId UNIQUEIDENTIFIER ,
              TotalShippingDiscount MONEY ,
              CreatedByFullName NVARCHAR(3074)
            ) ;
            WITH    PagingCTE ( Row_ID, Id, PurchaseOrderNumber, ExternalOrderNumber, OrderTypeId, OrderDate, FirstName, MiddleName, LastName, OrderStatusId, Quantity, NumSkus, OrderTotal, CODCharges, RefundTotal, TaxableOrderTotal, GrandTotal, TotalTax, ShippingTotal, TotalShippingCharge,PaymentRemaining, SiteId, CustomerId, TotalDiscount, TotalShippingDiscount, CreatedByFullName )
                      AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN @SortColumn = 'purchaseordernumber desc'
                                                              THEN PurchaseOrderNumber
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'purchaseordernumber asc'
                                                              OR @SortColumn = 'purchaseordernumber'
                                                              THEN PurchaseOrderNumber
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'ordertypeid desc'
                                                              THEN CAST(OrderTypeId AS VARCHAR(25))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'ordertypeid asc'
                                                              OR @SortColumn = 'ordertypeid'
                                                              THEN CAST(OrderTypeId AS VARCHAR(25))
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'orderdate desc'
                                                              THEN CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'orderdate asc'
                                                              OR @SortColumn = 'orderdate'
                                                              THEN CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'firstname desc'
                                                              OR @SortColumn = 'customer.firstname desc'
                                                              THEN FirstName
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'firstname asc'
                                                              OR @SortColumn = 'firstname'
                                                              OR @SortColumn = 'customer.firstname asc'
                                                              THEN FirstName
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'createdByfullname desc'
                                                              OR @SortColumn = 'customer.createdByfullname desc'
                                                              THEN FirstName
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'createdByfullname asc'
                                                              OR @SortColumn = 'createdByfullname'
                                                              OR @SortColumn = 'customer.createdByfullname asc'
                                                              THEN FirstName
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'orderstatusid desc'
                                                              OR @SortColumn = 'orderstatus desc'
                                                              THEN CAST(OrderStatusId AS INT)
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'orderstatusid asc'
                                                              OR @SortColumn = 'orderstatusid'
                                                              OR @SortColumn = 'orderstatus asc'
                                                              THEN CAST(OrderStatusId AS INT)
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'quantity desc'
                                                              THEN Quantity
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'quantity asc'
                                                              OR @SortColumn = 'quantity'
                                                              THEN Quantity
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'ordertotal desc'
                                                              THEN OrderTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'ordertotal asc'
                                                              OR @SortColumn = 'ordertotal'
                                                              THEN OrderTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totaldiscount desc'
                                                              THEN TotalDiscount
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totaldiscount asc'
                                                              OR @SortColumn = 'totaldiscount'
                                                              THEN TotalDiscount
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totaltax desc'
                                                              THEN TotalTax
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totaltax asc'
                                                              OR @SortColumn = 'totaltax'
                                                              THEN TotalTax
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'taxableordertotal desc'
                                                              THEN TaxableOrderTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'taxableordertotal asc'
                                                              OR @SortColumn = 'taxableordertotal'
                                                              THEN TaxableOrderTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'grandtotal desc'
                                                              THEN GrandTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'grandtotal asc'
                                                              OR @SortColumn = 'grandtotal'
                                                              THEN GrandTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'numskus desc'
                                                              THEN GrandTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'numskus asc'
                                                              OR @SortColumn = 'numskus'
                                                              THEN NumSkus
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totalshippingcharge desc'
                                                              THEN TotalShippingCharge
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totalshippingcharge asc'
                                                              OR @SortColumn = 'totalshippingcharge'
                                                              THEN TotalShippingCharge
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totalshippingdiscount desc'
                                                              THEN TotalShippingDiscount
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totalshippingdiscount asc'
                                                              OR @SortColumn = 'totalshippingdiscount'
                                                              THEN TotalShippingDiscount
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'customerid desc'
                                                              THEN CAST(CustomerId AS CHAR(36))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'customerid asc'
                                                              OR @SortColumn = 'customerid'
                                                              THEN CAST(CustomerId AS CHAR(36))
                                                              ELSE CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END ASC ) AS [Row_ID] ,
                                    Id ,
                                    PurchaseOrderNumber ,
                                    ExternalOrderNumber ,
                                    OrderTypeId ,
                                    OrderDate ,
                                    FirstName ,
                                    MiddleName ,
                                    LastName ,
                                    OrderStatusId ,
                                    Quantity ,
                                    NumSkus ,
                                    OrderTotal ,
                                    CODCharges ,
                                    RefundTotal ,
                                    TaxableOrderTotal ,
                                    GrandTotal ,
                                    TotalTax ,
                                    ShippingTotal ,
                                    TotalShippingCharge ,
                                    PaymentRemaining,
                                    SiteId ,
                                    CustomerId ,
                                    TotalDiscount ,
                                    TotalShippingDiscount ,
                                    CreatedByFullName
                           FROM     vwSearchOrder
                           WHERE    ( @CustomerId IS NULL
                                      OR CustomerId = @CustomerId
                                    )
                                    AND ( SiteId = @ApplicationId )
                                    AND OrderDate BETWEEN @lStartDate AND @lEndDate
                                    AND ( @OrderStatus IS NULL
                                          OR OrderStatusId IN ( SELECT
                                                              OrderStatus
                                                              FROM
                                                              @tblOrderStatus )
                                        )
                                    AND ( @Searchkeyword = '%'
                                          OR ( PurchaseOrderNumber ) LIKE @SearchKeyword
                                          OR ( ExternalOrderNumber ) LIKE @SearchKeyword
                                          OR ( FirstName ) LIKE @SearchKeyword
                                          OR ( LastName ) LIKE @SearchKeyword
                                          OR ( MiddleName ) LIKE @SearchKeyword
                                        )
                                    AND ( @ExternalShippingStatus IS NULL
                                          OR ExternalShippingStatus = @ExternalShippingStatus
                                        )
                         )
            INSERT  INTO @tblOrders
                    SELECT  Row_ID ,
                            Id ,
                            PurchaseOrderNumber ,
                            ExternalOrderNumber ,
                            OrderTypeId ,
                            dbo.ConvertTimeFromUtc(OrderDate, @ApplicationId) OrderDate ,
                            FirstName ,
                            MiddleName ,
                            LastName ,
                            OrderStatusId ,
                            Quantity ,
                            NumSkus ,
                            OrderTotal ,
                            CODCharges ,
                            TotalDiscount ,
                            RefundTotal ,                          
                            TaxableOrderTotal ,
                            GrandTotal ,
                            TotalTax ,
                            ShippingTotal ,
                            TotalShippingCharge ,
                            PaymentRemaining,
                            SiteId ,
                            CustomerId ,
                            TotalShippingDiscount ,
                            CreatedByFullName
                    FROM    PagingCTE pcte    



        SELECT  *
        FROM    @tblOrders
        WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                AND Row_ID <= @PageSize * @PageIndex    
    
        SELECT  COUNT(*) AS [Count] ,
                ISNULL(SUM(GrandTotal), 0) Total
        FROM    @tblOrders 

        SELECT  OS.[Id] ,
                OS.[OrderId] ,
                OS.[ShippingOptionId] ,
                OS.[OrderShippingAddressId] ,
                dbo.ConvertTimeFromUtc(OS.[ShipOnDate], @ApplicationId) ShipOnDate ,
                dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate], @ApplicationId) ShipDeliveryDate ,
                OS.[TaxPercentage] ,
                OS.[TaxPercentage] ,
                OS.[ShippingTotal] ,
                OS.[Sequence] ,
                OS.[IsManualTotal] ,
                OS.[Greeting] ,
                OS.[Tax] ,
                OS.[SubTotal] ,
                OS.[TotalDiscount] ,
                OS.[ShippingCharge] ,
                OS.[Status] ,
                OS.[CreatedBy] ,
                dbo.ConvertTimeFromUtc(OS.[CreatedDate], @ApplicationId) CreatedDate ,
                OS.[ModifiedBy] ,
                dbo.ConvertTimeFromUtc(OS.[ModifiedDate], @ApplicationId) ModifiedDate ,
                OS.[ShippingDiscount] ,
                OS.ShipmentHash ,
                OS.IsBackOrder ,
                OS.IsHandlingIncluded,
                OS.ShippingChargeTax
        FROM    OROrderShipping OS
                JOIN @tblOrders T ON T.Id = OS.OrderId
        WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                AND Row_ID <= @PageSize * @PageIndex ;
 
 
 
 
        WITH    cteShipmentItemQuantity
                  AS ( SELECT   SUM(SI.Quantity) Quantity ,
                                OI.Id
                       FROM     FFOrderShipmentItem SI
                                INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId = S.Id
                                INNER JOIN OROrderItem OI ON SI.OrderItemId = OI.Id
                                INNER JOIN @tblOrders T ON T.Id = OI.OrderId
                       WHERE    ShipmentStatus = 2
                       GROUP BY OI.Id
                     )
            SELECT  OI.[Id] ,
                    OI.[OrderId] ,
                    OI.[OrderShippingId] ,
                    OI.[OrderItemStatusId] ,
                    OI.[ProductSKUId] ,
                    OI.[Quantity] ,
                    ISNULL(SQ.Quantity, 0) AS ShippedQuantity ,
                    OI.[Price] ,
                    OI.[UnitCost] ,
                    OI.[IsFreeShipping] ,
                    OI.[Sequence] ,
                    dbo.ConvertTimeFromUtc(OI.CreatedDate, @ApplicationId) CreatedDate ,
                    OI.[CreatedBy] ,
                    FN.UserFullName CreatedByFullName ,
                    dbo.ConvertTimeFromUtc(OI.ModifiedDate, @ApplicationId) ModifiedDate ,
                    OI.[ModifiedBy] ,
                    OI.[ParentOrderItemId] ,
                    OI.[HandlingCharge] ,
                    OI.[BundleItemId] ,
                    OI.[CartItemId] ,
                    OI.[Tax] ,
                    OI.[Discount]
            FROM    OROrderItem OI
                    INNER JOIN @tblOrders T ON T.Id = OI.OrderId
                    LEFT JOIN cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
                    LEFT JOIN VW_UserFullName FN ON FN.UserId = OI.CreatedBy
            WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                    AND Row_ID <= @PageSize * @PageIndex    

 
    
    END
GO
PRINT 'Altering procedure Order_GetOrder'
GO
ALTER PROCEDURE [dbo].[Order_GetOrder](@Id uniqueidentifier = null, @CartId uniqueidentifier = null,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

IF @Id IS NULL AND @CartId IS NULL 
	begin
		raiserror('DBERROR||%s',16,1,'OrderId or CartId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],   
 o.[Id] OrderId, 
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.[PaymentRemaining],   
 O.RefundTotal,
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O  
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy  
WHERE
O.SiteId = @ApplicationId AND
(@Id is null or @Id = O.Id) AND
((@CartId is null or @CartId = CartId) OR (CartId IS NULL AND @CartId IS NULL))
--O.Id = ISnull(@Id,Id) AND (CartId = Isnull(@CartId,CartId) OR (CartId IS NULL AND @CartId IS NULL)) ;    


IF @Id IS NULL
	SELECT  @Id = Id FROM OROrder WHERE CartId = @CartId;

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId=@Id AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.[HandlingTax]
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy  
WHERE OI.OrderId = @Id;

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded,
OS.ShippingChargeTax
FROM OROrderShipping OS
WHERE OS.OrderId = @Id;

select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized
from PRProductAttribute PA  
Inner join 
	(Select ProductId FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId = @Id
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[AttributeGroupId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId =@Id

--- order shipments
SELECT F.[Id]
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId =@Id
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId =@Id



SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId=@Id

END
GO
PRINT 'Altering procedure Order_GetOrderItems'

GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order id
-- Author: 
-- Created Date:03/25/2009
-- Created By:Devin

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItems](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.Id=@Id  AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	OI.CreatedDate AS CreatedDate,
	OI.[CreatedBy],
	OI.ModifiedDate AS ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.HandlingTax
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
WHERE OI.OrderId = @Id;


END
GO
PRINT 'Altering procedure Order_GetOrderItemsByXmlGuids'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order item ids
-- Author: 
-- Created Date:06/24/2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItemsByXmlGuids](@Ids xml,@ApplicationId uniqueidentifier=null)
AS
BEGIN

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
WHERE SI.OrderItemId in(Select tab.col.value('text()[1]','uniqueidentifier') from  @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col))
AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.HandlingTax
FROM OROrderItem OI
INNER JOIN @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col) ON OI.Id = tab.col.value('text()[1]','uniqueidentifier')
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id


END
GO
PRINT 'Altering procedure Order_GetOrderItemsForShipping'
GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order id
-- Author: 
-- Created Date:03/25/2009
-- Created By:Devin

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItemsForShipping](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN

;with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderShippingId =@Id AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	Isnull(SI.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId),
	OI.[CreatedBy],
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId),
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.HandlingTax
FROM OROrderItem OI 
Left Join cteShipmentItemQuantity SI ON  OI.Id = SI.Id
WHERE OI.OrderShippingId = @Id
ORDER BY OI.[Sequence];


END
GO
PRINT 'Altering procedure Order_GetOrderItemsForShippment'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order id
-- Author: 
-- Created Date:06/24/2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItemsForShippment](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Where OI.OrderShippingId =@Id And ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	Isnull(SI.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount],
	OI.HandlingTax
FROM OROrderItem OI
INNER JOIN PRPRoductSKU PS ON PS.Id = OI.ProductSKUID
INNER JOIN PRProduct P ON P.Id = PS.ProductID
INNER JOIN PRProductType PT ON PT.Id = P.ProductTypeId
Left Join cteShipmentItemQuantity SI ON  OI.Id = SI.Id
WHERE OI.OrderShippingId = @Id
AND Isnull(SI.Quantity,0) < OI.Quantity
AND (NOT( ISNULL(PT.IsDownloadableMedia,0) = 1 
AND EXISTS (Select Id from PRProductMedia where SKUId = PS.Id)))

END
GO
PRINT 'Altering procedure Order_GetOrders'
GO
ALTER PROCEDURE [dbo].[Order_GetOrders](@OrderStatusId int,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

Declare @tblOrders table
(
Id    uniqueidentifier
,CartId uniqueidentifier
,OrderTypeId    INT
,OrderStatusId    int
,PurchaseOrderNumber nvarchar(100)
,ExternalOrderNumber    nvarchar(100)
,SiteId    uniqueidentifier
,CustomerId     uniqueidentifier
,TotalTax    money
,OrderTotal   money
,CODCharges money
,TaxableOrderTotal   money
,GrandTotal   money
,TotalShippingCharge money
,TotalDiscount    money
,PaymentTotal    money
,RefundTotal money
,PaymentRemaining    money
,OrderDate    dateTime
,CreatedDate  dateTime  
,CreatedBy    uniqueidentifier
,CreatedByFullName nvarchar(3074)
,ModifiedDate   dateTime 
,ModifiedBy uniqueidentifier
,TotalShippingDiscount  money
);

with cteOrders
AS
(
SELECT     
 O.[Id],    
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy
WHERE
O.SiteId = @ApplicationId AND
O.OrderStatusId = Isnull(@OrderStatusId, O.OrderStatusId)
)


Insert into @tblOrders
SELECT * FROM cteOrders 

SELECT * FROM @tblOrders
Order by OrderDate;

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId IN (SELECT Id FROM @tblOrders) AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.Tax,
	OI.Discount,
	OI.HandlingTax
FROM OROrderItem OI
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
WHERE OI.OrderId IN (SELECT Id FROM @tblOrders);

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded,
OS.ShippingChargeTax
FROM OROrderShipping OS
WHERE OS.OrderId IN (SELECT Id FROM @tblOrders);

select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  P.OrderId 
from PRProductAttribute PA  
Inner join 
	(Select ProductId, OI.OrderId  FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId IN (SELECT Id FROM @tblOrders)
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[AttributeGroupId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId IN (SELECT Id FROM @tblOrders)

--- order shipments
SELECT F.[Id]
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	  ,OS.OrderId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
      ,OS.OrderId
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)



SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
      ,OS.OrderId
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)

END

GO
PRINT 'Altering procedure Order_GetOrdersByXmlGuids'
GO
ALTER PROCEDURE [dbo].[Order_GetOrdersByXmlGuids](@Ids XML,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN
Declare @OrderIds table(Id uniqueidentifier)
INSERT INTO @OrderIds
Select tab.col.value('text()[1]','uniqueidentifier') 
from  @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col)

IF (select count(*) from @OrderIds) =0
	begin
		raiserror('DBERROR||%s',16,1,'OrderId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],  
 O.[Id]  OrderId,  
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy
WHERE
O.SiteId = @ApplicationId 
AND O.Id in (Select Id from @OrderIds)

;with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId in (Select Id from @OrderIds) AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.Tax,
	OI.Discount,
	OI.HandlingTax
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy
WHERE OI.OrderId in (Select Id from @OrderIds) ;

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.ShippingChargeTax
FROM OROrderShipping OS
WHERE OS.OrderId in (Select Id from @OrderIds) ;

select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  P.OrderId
from PRProductAttribute PA  
Inner join 
	(Select ProductId,OI.Orderid FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId in (Select Id from @OrderIds) 
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[AttributeGroupId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId in (Select Id from @OrderIds) 

--- order shipments
SELECT F.[Id]
	 ,OS.OrderId
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 
Order By CreateDate DESC

SELECT SI.[Id]
      ,OS.OrderId
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 



SELECT SP.[Id]
	  ,OS.OrderId
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 

END
GO
PRINT 'Altering procedure Order_GetOrderShippings'

GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order shippings with the given order id
-- Author: 
-- Created Date:03/25/2009
-- Created By:Devin

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderShippings](@Id uniqueidentifier,@ApplicationId uniqueidentifier =null)
AS
SELECT 
 OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId) ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId) ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
OS.[CreatedDate] AS  CreatedDate,
OS.[ModifiedBy],
OS.[ModifiedDate] AS   ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded,
OS.ShippingChargeTax
FROM OROrderShipping OS
WHERE OS.OrderId = @Id;

GO
PRINT 'Altering function GetAncestorSitesForSystemUser '
GO

-- =============================================
-- Author:		Vaibhav Sinha
-- Create date: 7/10/2013
-- Description:	Get all the ancestor site of a variant site
-- CHANGE LOG
---------------------------------------
-- Modified By: Sankar Raj
-- Date :  
-- Reason : To include the sibiling site access functionality 

-- =============================================
--select * from GetAncestorSites('CDD38906-6071-4DD8-9FB6-508C38848C61')

ALTER FUNCTION [dbo].[GetAncestorSitesForSystemUser](@ApplicationId uniqueidentifier, @IsSystemUser bit = null, @IsShared bit = null)

RETURNS 
@Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	IF(@IsSystemUser IS NULL)
		SET @IsSystemUser = 1

	IF(@IsShared IS NULL)
		SET @IsShared = 0
			
	IF @ApplicationId is NOT NULL
	BEGIN
	Declare @ImportAllAdminPermission bit
	SELECT @ImportAllAdminPermission = ImportAllAdminPermission FROM SISite where Id = @ApplicationId
	IF((@ImportAllAdminPermission = 1 AND @IsSystemUser = 1) OR @IsShared = 1)
	BEGIN
	
		DECLARE @AllowSiblingAccess BIT
		
		SELECT @AllowSiblingAccess = [VALUE] 
		FROM STSiteSetting SS JOIN STSettingType ST 
				ON ST.Id = SS.SettingTypeId 
					AND ST.Name='AllowSiblingSiteAccess'

		DECLARE @PARENTID UNIQUEIDENTIFIER;    
		SELECT @PARENTID = PARENTSITEID FROM SISITE WHERE ID = @ApplicationId


		;WITH siteAncestorHierarchy AS (
			SELECT [Id]
					,[ParentSiteId]
					  FROM SISite S
					  where Id = @ApplicationId
			UNION ALL 
			SELECT S1.[Id]
					  ,S1.[ParentSiteId]
					  FROM SISite S1 
					INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId 
					WHERE S1.Status != 3
			)

				INSERT INTO @Sites 
				SELECT [Id]
				FROM siteAncestorHierarchy S1
				--where Id != @ApplicationId
			
			IF (@AllowSiblingAccess = 1)
			BEGIN
				INSERT INTO @Sites
				SELECT [ID] SITEID FROM [GetSiblingSites](@ApplicationId)
				UNION
				SELECT Id FROM SISite WHERE ParentSiteId = @ApplicationId
			END
	END
	ELSE
		BEGIN
			INSERT INTO @Sites 
					SELECT @ApplicationId
		END	
	END
	
	RETURN 
END

GO
PRINT 'Altering procedure Log_WriteLog'
GO
ALTER PROCEDURE [dbo].[Log_WriteLog]
(
	@EventID int, 
	@Priority int, 
	@Severity nvarchar(32), 
	@Title nvarchar(256), 
	@Timestamp datetime,
	@MachineName nvarchar(32), 
	@AppDomainName nvarchar(512),
	@ProcessID nvarchar(256),
	@ProcessName nvarchar(512),
	@ThreadName nvarchar(512),
	@Win32ThreadId nvarchar(128),
	@Message nvarchar(1500),
	@FormattedMessage xml,
	@LogId int OUTPUT
)
AS 
BEGIN
	DECLARE @ActivityId uniqueidentifier
	SET @ActivityId = @FormattedMessage.value('(//ActivityId)[1]','uniqueidentifier')
	IF @ActivityId IS NULL OR @ActivityId = dbo.GetEmptyGUID()
		SET @ActivityId = NEWID() 

	DECLARE @SiteId uniqueidentifier
	SET @SiteId = @FormattedMessage.value('(//SiteId)[1]','uniqueidentifier')
	IF @SiteId IS NULL
		SET @SiteId = dbo.GetEmptyGUID()

	INSERT INTO [LGLog] (
		SiteId,
		EventId,
		Priority,
		Severity,
		CreatedDate,
		MachineName,
		ProcessId,
		ActivityId,
		Message,
		AdditionalInfo
	)
	VALUES (
		@SiteId,
		@EventID,
		@Priority,
		@Severity, 
		@Timestamp,
		@MachineName, 
		@ProcessID,
		@ActivityId,
		@FormattedMessage.value('(//Message)[1]','nvarchar(max)'),
		null)

	SET @LogID = @@IDENTITY
	
	IF @EventID = 100
	BEGIN
		INSERT INTO LGExceptionLog (
			LogId,
			StackTrace,
			InnerException
		)
		VALUES (
			@LogId,
			@FormattedMessage.value('(//StackTrace)[1]','nvarchar(max)'),
			@FormattedMessage.value('(//InnerException)[1]','nvarchar(max)')
		)
	END
	
	RETURN @LogID
END

GO
PRINT 'Altering procedure SqlDirectoryProvider_Delete'
GO
ALTER PROCEDURE  [dbo].[SqlDirectoryProvider_Delete] 
(
      @Id                     [uniqueidentifier],  
      @ObjectTypeId				int=null,
      @ModifiedBy             [uniqueidentifier],
      @RelativePath           [NVarchar](max) OUTPUT
      
)
as

--********************************************************************************
-- Variable declaration
Declare @ApplicationId uniqueidentifier
DECLARE @Now        datetime,
            @LocalStatus int,
            @Lft bigint,
            @Rgt bigint
--********************************************************************************
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	if @ObjectTypeId is null
		Set @ObjectTypeId = (Select ObjectTypeId from vwDirectoryIds where Id=@Id)
		
	IF @ObjectTypeId =7 
	BEGIN
	  Select @RelativePath =VirtualPath from COContentStructure Where Id=@Id
      EXEC ContentStructure_Delete @Id= @Id
	END
	ELSE IF @ObjectTypeId =9
	BEGIN
	  Select @RelativePath =VirtualPath from COFileStructure Where Id=@Id
      EXEC FileStructure_Delete @Id= @Id
	END
	ELSE IF @ObjectTypeId =33
	BEGIN
	  Select @RelativePath =VirtualPath from COImageStructure Where Id=@Id
      EXEC ImageStructure_Delete @Id= @Id
	END
	ELSE IF @ObjectTypeId =38
	BEGIN
	  Select @RelativePath =VirtualPath from COFormStructure Where Id = @Id
      EXEC FormStructure_Delete @Id= @Id
	END
	ELSE
	BEGIN
      Select @RelativePath =VirtualPath from SISiteDirectory Where Id=@Id
      SET @LocalStatus = dbo.GetDeleteStatus()
      Select @Lft=LftValue ,@Rgt=RgtValue,@ApplicationId=SiteId from HSStructure Where Id=@Id
      SET @Now = getdate()
        UPDATE    SISiteDirectory  WITH (rowlock)     SET 
                        ModifiedBy=@ModifiedBy,
                        ModifiedDate=@Now,
                        Status=@LocalStatus
       --   DELETE FROM SISiteDirectory
        WHERE Id IN (SELECT Id FROM HSStructure Where LftValue Between @Lft and @Rgt and SiteId=@ApplicationId)

       EXEC HierarchyNode_Delete @Id= @Id
       
      END
END
GO
PRINT 'Altering procedure Cart_Save'
GO
ALTER PROCEDURE [dbo].[Cart_Save](
					@Id uniqueidentifier output,
					@Title nvarchar(50) = null,
					@Description nvarchar(256) = null,
					@CustomerId uniqueidentifier = null,
					@CouponId uniqueidentifier = null,
					@CouponIds xml = null,
					@ExpirationDate datetime,
					@CreatedBy uniqueidentifier,
					@ModifiedBy uniqueidentifier,
					@ApplicationId uniqueidentifier
						 )

AS 
BEGIN
  
DECLARE @CurrentDate datetime
  set @CurrentDate = getUTCDate()

 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
	 begin  
		  set @Id = newid()  
			Insert into CSCart(
						Id,
						Title,
						Description,
						CustomerId,
						ExpirationDate,
						CreatedDate,
						CreatedBy,
						SiteId
							   )
			Values(
					@Id,
					@Title,
					@Description,
					@CustomerId,
					dbo.ConvertTimeToUtc(@ExpirationDate,@ApplicationId),
					@CurrentDate,
					@CreatedBy,
					@ApplicationId
				  )
	
		
					 	
	  end
else
	 begin  
			Update CSCart
				Set Title = @Title,
					CustomerId = @CustomerId,
					Description = @Description,
					ExpirationDate = dbo.ConvertTimeToUtc(@ExpirationDate,@ApplicationId),
					ModifiedDate = @CurrentDate,
					ModifiedBy = @ModifiedBy,
					SiteId = @ApplicationId
				Where Id = @Id
	  end

EXEC Coupon_ApplyToCart @CouponId,@CouponIds,@Id,@CustomerId,@ModifiedBy
	
	/*Delete from CSCartCoupon
		Where CartId = @Id
	Insert into CSCartCoupon 
					(Id
					 ,CartId
					 ,CouponId
					 ,CreatedBy
					 ,CreatedDate)
		Select newid()
			,@Id
				,tab.col.value('text()[1]','uniqueidentifier') CouponId
				,@CreatedBy CreatedBy
				,@CurrentDate CreatedDate
		FROM @CouponIds.nodes('/GenericCollectionOfGuid/guid')tab(col)
*/

Select @Id
END
GO
PRINT 'Altering procedure Coupon_ApplyToCart'
GO

-- Copyright ⌐ 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: To Apply Coupon to an Order for a Customer
-- Author:   
-- Created Date:07/14/2009  
-- Created By:Devin  
  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
  
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Coupon_ApplyToCart]  
(  
 @CouponId uniqueidentifier = null,
 @CouponCodeIds xml=null,
 @CartId uniqueidentifier,
 @CustomerId uniqueidentifier,
 @ModifiedBy uniqueidentifier
)  
as  
begin  
DECLARE @CartCouponCount int
SET @CartCouponCount = 0
 IF @Cartid is not null
	Begin
		IF(@CouponId IS NOT NULL)
		BEGIN
			SELECT @CartCouponCount = COUNT(1) FROM CSCartCoupon where CartId = @CartId
			IF (NOT EXISTS(SELECT * FROM CSCartCoupon WHERE CouponId = @CouponId AND CartId = @CartId) AND @CartCouponCount = 0)
				INSERT into  CSCartCoupon
				(
					Id,
					CartId,
					CouponId,
					CreatedDate,
					CreatedBy
				)
				Select
						newid() 
					  ,@CartId
					   ,@CouponId
						,GetUTCDATE(),
						@ModifiedBy
			ELSE IF(@CartCouponCount = 1)
				UPDATE CSCartCoupon Set CouponId = @CouponId WHERE CartId = @CartId
			ELSE -- When multiple Coupon for the same cart, but only one coupon is passed
				BEGIN
					Delete from CSCartCoupon
					Where CartId =@CartId

				INSERT into  CSCartCoupon
				(
					Id,
					CartId,
					CouponId,
					CreatedDate,
					CreatedBy
				)
				Select
						newid() 
					  ,@CartId
					   ,@CouponId
						,GetUTCDATE(),
						@ModifiedBy
					
				END
						 
		END
		ELSE IF(@CouponCodeIds IS NOT NULL)
		BEGIN
			Delete from CSCartCoupon
			Where CartId =@CartId
			Insert into  CSCartCoupon
			(
			Id,
			CartId,
			CouponId,
			CreatedDate,
			CreatedBy
			)
			Select
					newid() 
				  ,@CartId
				   ,C.Id
					,GetUTCDATE(),
					@ModifiedBy
			FROM
			(SELECT Distinct tab.col.value('text()[1]','uniqueidentifier') Id
			FROM @CouponCodeIds.nodes('/GenericCollectionOfGuid/guid')tab(col))C
		END
end  
END


GO

PRINT 'Altering procedure Coupon_ApplyToOrder'
GO
-- Copyright ⌐ 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: To Apply Coupon to an Order for a Customer
-- Author:   
-- Created Date:07/14/2009  
-- Created By:Devin  
  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
  
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Coupon_ApplyToOrder]    
(    
 @CouponCodeId uniqueidentifier=null,     
 @CouponCodeIds xml=null,  
 @OrderId uniqueidentifier,  
 @CustomerId uniqueidentifier,  
 @ModifiedBy uniqueidentifier  
)    
as    
begin    
   
 IF @CouponCodeId is not null  
 BEGIN  
 insert into CPOrderCouponCode  
 (  
  OrderId,  
  CouponCodeId,  
  CustomerId,  
  CreatedDate,  
  CreatedBy  
 )  
 values  
 (  
  @OrderId,  
  @CouponCodeId,  
  @CustomerId,  
  GetUTCDate(),  
  @ModifiedBy  
 )  
  
 DECLARE @CouponId uniqueidentifier  
 SELECT @CouponId = CouponId from CPCouponCode where Id = @CouponCodeId  
   
 DECLARE @CartId uniqueidentifier   
 SELECT @CartId = CartId from OROrder where Id = @OrderId  
 IF @CartId is not null  
 BEGIN  
 -- UPDATE CSCart SET CouponId = @CouponId  where Id = @CartId  
  IF Not Exists (Select * from CSCartCoupon Where CouponId=@CouponId And CartId=@CartId)  
  Insert CSCartCoupon(Id,CartId,CouponId,CreatedBy,CreatedDate)  
  Values(newid(),@CartId,@CouponId,@ModifiedBy,GetUTCDAte())  
 END  
   
 END  -- End of @CouponCodeId
 ELSE IF @CouponCodeIds is not null  
 BEGIN   
	 
	  Declare @CouponCode Table
	  (
		CouponCodeId uniqueidentifier
	  )
		
	  Declare @CouponCodeCount int
	  Declare @LocalCouponCodeId uniqueidentifier
	  Declare @OrderCouponCount int
	  
	  SELECT @OrderCouponCount = COUNT(1) FROM CPOrderCouponCode where OrderId = @OrderId		
	 
	 INSERT INTO @CouponCode  (CouponCodeId)
	 SELECT Distinct CC.Id  
	 FROM @CouponCodeIds.nodes('/GenericCollectionOfGuid/guid')tab(col)  
	 inner join CPCouponCode CC ON tab.col.value('text()[1]','uniqueidentifier')=CC.CouponId
 
	Select @CouponCodeCount = count(1) from @CouponCode 
 
	If(@CouponCodeCount = 1)
	BEGIN
		Select top 1 @LocalCouponCodeId  = CouponCodeId from @CouponCode
 
		IF (NOT EXISTS (Select * from CPOrderCouponCode Where CouponCodeId=@LocalCouponCodeId AND OrderId=@OrderId)  AND @OrderCouponCount = 0)
			Insert into  CPOrderCouponCode  
			  (  
			  OrderId,  
			  CouponCodeId,  
			  CustomerId,  
			  CreatedDate,  
			  CreatedBy  
			  )  
			  Select @OrderId  
				  ,@LocalCouponCodeId 
				,@CustomerId  
				,GetUTCDATE(),  
				@ModifiedBy 
		
	ELSE IF(@OrderCouponCount = 1)
		BEGIN
		--Update the CouponCode
		UPDATE CPOrderCouponCode Set CouponCodeId = @LocalCouponCodeId Where @OrderId = OrderId and CustomerId = @CustomerId
		
		END
	--END	
	ELSE --When multiple Coupon for the same order, but only one coupon is passed
	BEGIN
			Delete from CPOrderCouponCode  
			Where OrderId =@OrderId  
	
			Insert into  CPOrderCouponCode  
			  (  
			  OrderId,  
			  CouponCodeId,  
			  CustomerId,  
			  CreatedDate,  
			  CreatedBy  
			  )  
			  Select @OrderId  
				  ,@LocalCouponCodeId 
				,@CustomerId  
				,GetUTCDATE(),  
				@ModifiedBy 
	END				
	END
	
  ELSE 
	BEGIN
  
		  Delete from CPOrderCouponCode  
		  Where OrderId =@OrderId  
		  Insert into  CPOrderCouponCode  
		  (  
		  OrderId,  
		  CouponCodeId,  
		  CustomerId,  
		  CreatedDate,  
		  CreatedBy  
		  )  
		  Select @OrderId  
			  ,C.Id  
			,@CustomerId  
			,GetUTCDATE(),  
			@ModifiedBy  
		  FROM  
		  (Select Distinct CC.Id  
		  FROM @CouponCodeIds.nodes('/GenericCollectionOfGuid/guid')tab(col)  
		  inner join CPCouponCode CC ON tab.col.value('text()[1]','uniqueidentifier')=CC.CouponId) C  
    END
 END  -- end for else of @CouponCodeIds not null
  
end  -- sp end

GO
PRINT 'Altering procedure Coupon_GetUsesForCouponForUser'
GO
ALTER PROCEDURE [dbo].[Coupon_GetUsesForCouponForUser]  
(  
 @CouponId uniqueidentifier,  
 @CustomerId uniqueidentifier  
)  
as  
begin  
 SELECT count(OCC.OrderId) as Uses  
   FROM CPOrderCouponCode OCC WITH (NOLOCK)
   inner join CPCouponCode CC  ON OCC.CouponCodeId=CC.Id  
   WHERE CC.CouponId=@CouponId and OCC.CustomerId=@CustomerId  
end

GO
PRINT 'Creating procedure User_GetUsersInGroup'
GO

CREATE FUNCTION [dbo].[User_GetUsersInGroup]  
(  
 @ProductId    uniqueidentifier = NULL,   
 @ApplicationId   uniqueidentifier,  
 @Id            uniqueidentifier,   -- GroupId  
 @MemberType    smallint,  
 @MemberId    uniqueidentifier = NULL, -- Either userId or groupid.  
 @IsSystemUser   bit = 0  
)  
RETURNS @ResultTable TABLE       
(      
 Id      uniqueidentifier,      
 UserName    nvarchar(256),        
 Email     nvarchar(512),      
 PasswordQuestion  nvarchar(512),        
 IsApproved    bit,      
 CreatedDate    datetime,      
 LastLoginDate   datetime,      
 LastActivityDate  datetime,      
 LastPasswordChangedDate datetime,      
 IsLockedOut    bit,      
 LastLockoutDate   datetime,    
 FirstName    nvarchar(256),    
 LastName    nvarchar(256),    
 MiddleName    nvarchar(256) ,    
 ExpiryDate    datetime ,    
 EmailNotification  bit ,    
 TimeZone    nvarchar(100),    
 ReportRangeSelection varchar(50),    
 ReportStartDate   datetime ,    
 ReportEndDate   datetime,     
 BirthDate    datetime,    
 CompanyName    nvarchar(1024),    
 Gender     nvarchar(50),    
 HomePhone    varchar(50),    
 MobilePhone    varchar(50),    
 OtherPhone    varchar(50),    
 ImageId     uniqueidentifier,  
 Status     int
)   
AS  
BEGIN  
  
 DECLARE @tbUser TYUser  
 INSERT INTO @tbUser  
 SELECT DISTINCT   
  U.Id,  
  U.UserName,  
  M.Email,   
  M.PasswordQuestion,    
  M.IsApproved,  
  U.CreatedDate,  
  M.LastLoginDate,   
  U.LastActivityDate,  
  M.LastPasswordChangedDate,   
  M.IsLockedOut,  
  M.LastLockoutDate,  
  U.FirstName,  
  U.LastName,  
  U.MiddleName,  
  U.ExpiryDate,  
  U.EmailNotification,  
  U.TimeZone,  
  U.ReportRangeSelection,  
  U.ReportStartDate,  
  U.ReportEndDate,  
  U.BirthDate,  
  U.CompanyName,  
  U.Gender,  
  U.HomePhone,  
  U.MobilePhone,  
  U.OtherPhone,  
  U.ImageId,  
  U.Status
 FROM dbo.USUser U  
  INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
  INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
   (@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser)   
  INNER JOIN dbo.USMemberGroup G ON G.MemberId = U.Id AND  
   (@ApplicationId IS NULL OR  
    G.ApplicationId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0))   
   )   
 WHERE   
  G.GroupId = @Id AND  
  (U.ExpiryDate IS NULL OR U.ExpiryDate >= GETUTCDATE()) AND  
  M.IsApproved = 1 AND M.IsLockedOut = 0 AND U.Status != 3 AND  
  (@ProductId IS NULL OR S.ProductId = @ProductId) AND  
  (@MemberId IS NULL OR G.MemberId = @MemberId)  
   
 INSERT INTO @ResultTable  
 SELECT * FROM @tbUser  
  
 RETURN  
END
GO
PRINT 'Creating function User_GetUsersInRole'
GO
CREATE FUNCTION [dbo].[User_GetUsersInRole]      
(      
 @Id   int,      
 @MemberType smallint,      
 @ApplicationId uniqueidentifier = NULL,      
 @IsSystemUser  bit = 0,      
 @ObjectTypeId  int=null,      
 @ProductId  uniqueidentifier = NULL      
      
)      
RETURNS @ResultTable TABLE       
(      
 Id      uniqueidentifier,      
 UserName    nvarchar(256),        
 Email     nvarchar(512),      
 PasswordQuestion  nvarchar(512),        
 IsApproved    bit,      
 CreatedDate    datetime,      
 LastLoginDate   datetime,      
 LastActivityDate  datetime,      
 LastPasswordChangedDate datetime,      
 IsLockedOut    bit,      
 LastLockoutDate   datetime,    
 FirstName    nvarchar(256),    
 LastName    nvarchar(256),    
 MiddleName    nvarchar(256) ,    
 ExpiryDate    datetime ,    
 EmailNotification  bit ,    
 TimeZone    nvarchar(100),    
 ReportRangeSelection varchar(50),    
 ReportStartDate   datetime ,    
 ReportEndDate   datetime,     
 BirthDate    datetime,    
 CompanyName    nvarchar(1024),    
 Gender     nvarchar(50),    
 HomePhone    varchar(50),    
 MobilePhone    varchar(50),    
 OtherPhone    varchar(50),    
 ImageId     uniqueidentifier,  
 Status     int    
)      
AS      
BEGIN     
   
 DECLARE @tbUserIds Table (Id uniqueidentifier)  
 INSERT INTO @tbUserIds  
    SELECT DISTINCT G.MemberId   
 FROM USMemberGroup G  
  INNER JOIN USMemberRoles R ON G.GroupId = R.MemberId  
   AND R.RoleId = @Id AND R.MemberType = 2  
 WHERE  
  (@ObjectTypeId IS NULL OR R.ObjectTypeId = @ObjectTypeId) AND  
  (@ProductId IS NULL OR R.ProductId = @ProductId) AND  
  (@ApplicationId IS NULL OR G.ApplicationId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0)))   
  
 UNION  
  
 SELECT DISTINCT U.Id  
 FROM dbo.USUser U  
  INNER JOIN dbo.USMemberRoles R ON R.MemberId = U.Id   
   AND R.RoleId = @Id AND R.MemberType = 1  
 WHERE   
  (@ObjectTypeId IS NULL OR R.ObjectTypeId = @ObjectTypeId)   
  
 DECLARE @tbUser TYUser  
 INSERT INTO @tbUser  
 SELECT DISTINCT   
  U.Id,  
  U.UserName,  
  M.Email,   
  M.PasswordQuestion,    
  M.IsApproved,  
  U.CreatedDate,  
  M.LastLoginDate,   
  U.LastActivityDate,  
  M.LastPasswordChangedDate,   
  M.IsLockedOut,  
  M.LastLockoutDate,  
  U.FirstName,  
  U.LastName,  
  U.MiddleName,  
  U.ExpiryDate,  
  U.EmailNotification,  
  U.TimeZone,  
  U.ReportRangeSelection,  
  U.ReportStartDate,  
  U.ReportEndDate,  
  U.BirthDate,  
  U.CompanyName,  
  U.Gender,  
  U.HomePhone,  
  U.MobilePhone,  
  U.OtherPhone,  
  U.ImageId,  
  U.Status
 FROM dbo.USUser U  
  INNER JOIN @tbUserIds T ON U.Id = T.Id  
  INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
  INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
   (@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser) AND  
   (@ApplicationId IS NULL OR  
    S.SiteId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0))   
   )
 WHERE   
  U.ExpiryDate IS NULL OR U.ExpiryDate >= GETUTCDATE() AND  
  M.IsApproved = 1 AND M.IsLockedOut = 0 AND U.Status != 3 AND  
  (@ProductId IS NULL OR S.ProductId = @ProductId)  
  
 INSERT INTO @ResultTable  
 SELECT * FROM @tbUser  
  
 RETURN     
END
GO
PRINT 'Creating procedure Membership_BuildUser'
GO
CREATE PROCEDURE [dbo].[Membership_BuildUser]
(
	@tbUser			TYUser READONLY,
	@SortBy			nvarchar(100) = NULL,
	@SortOrder		nvarchar(10) = NULL,	
	@PageNumber		int = NULL,
	@PageSize		int = NULL
)
AS
BEGIN
	IF @SortBy IS NULL SET @SortBy = 'USERNAME'
	IF @SortOrder IS NULL SET @SortOrder = 'ASC'
	SET @SortBy = UPPER(@SortBy)
	SET @SortOrder = UPPER(@SortOrder)

	DECLARE @PageLowerBound int
	DECLARE @PageUpperBound int
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	ELSE
		SET @PageUpperBound = 0

	DECLARE @tbPagedUser TYUser
	;WITH CTEUsers AS(
		SELECT ROW_NUMBER() OVER (
			ORDER BY
				CASE WHEN (@SortBy = 'USERNAME' AND @SortOrder = 'ASC') THEN U.UserName END ASC,
				CASE WHEN (@SortBy = 'USERNAME' AND @SortOrder = 'DESC') THEN U.UserName END DESC,
				CASE WHEN (@SortBy = 'FIRSTNAME' AND @SortOrder = 'ASC') THEN U.FirstName END ASC,
				CASE WHEN (@SortBy = 'FIRSTNAME' AND @SortOrder = 'DESC') THEN U.FirstName END DESC,
				CASE WHEN (@SortBy = 'LASTNAME' AND @SortOrder = 'ASC') THEN U.LastName END ASC,
				CASE WHEN (@SortBy = 'LASTNAME' AND @SortOrder = 'DESC') THEN U.LastName END DESC,
				CASE WHEN (@SortBy = 'STATUS' AND @SortOrder = 'ASC') THEN U.Status END ASC,
				CASE WHEN (@SortBy = 'STATUS' AND @SortOrder = 'DESC') THEN U.Status END DESC	
			) AS RowNumber,
			U.*
		FROM @tbUser U
	)
		
	INSERT INTO @tbPagedUser
	SELECT
		Id,    
		UserName,      
		Email,    
		PasswordQuestion,      
		IsApproved,    
		CreatedDate,    
		LastLoginDate,    
		LastActivityDate,    
		LastPasswordChangedDate,    
		IsLockedOut,    
		LastLockoutDate,  
		FirstName,  
		LastName,  
		MiddleName,  
		ExpiryDate,  
		EmailNotification,  
		TimeZone,  
		ReportRangeSelection,  
		ReportStartDate,  
		ReportEndDate,   
		BirthDate,  
		CompanyName,  
		Gender,  
		HomePhone,  
		MobilePhone,  
		OtherPhone,  
		ImageId,
		Status					
	FROM CTEUsers
	WHERE (@PageNumber is null) OR (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound) 
		
	SELECT * FROM @tbPagedUser

	SELECT COUNT(*) FROM @tbUser
		
	SELECT UserId ,             
		PropertyName,
		PropertyValueString, 
		PropertyValuesBinary,
		LastUpdatedDate
	FROM dbo.USMemberProfile P, @tbPagedUser T
	WHERE P.UserId = T.Id 
				
	SELECT U.UserId, U.SecurityLevelId, S.Title  
	FROM USUserSecurityLevel U
		JOIN USSecurityLevel S ON U.SecurityLevelId = S.Id 
		JOIN @tbPagedUser T ON T.Id = U.UserId
END

GO
PRINT 'Altering procedure Membership_GetAllUsers'
GO
ALTER PROCEDURE [dbo].[Membership_GetAllUsers]  
(  
 @ProductId				uniqueidentifier = NULL,  
 @ProductIds			xml = NULL,  
 @ExcludedProductIds	xml = NULL,  
 @ApplicationId			uniqueidentifier = NULL,  
 @Status				int = NULL,  
 @IsSystemUser			bit = NULL,  
 @IsApproved			bit = NULL,  
 @IsLockedOut			bit = NULL,  
 @PageNumber			int = NULL,  
 @PageSize				int = NULL,  
 @IncludeExpiredUsers	bit = NULL,  
 @SortBy				nvarchar(100) = NULL,  
 @SortOrder				nvarchar(10) = NULL,  
 @FilterBy				nvarchar(500) = NULL,  
 @IncludeVariantUsers	bit = NULL,  
 @IgnoreStatus			bit = NULL,  
 @SinceExpired			datetime = NULL,  
 @UserIds				xml = NULL,  
 @UserId				uniqueidentifier = NULL,  
 @UserName				nvarchar(256) = NULL,
 @UserNameOnly			bit = NULL    
)  
AS  
BEGIN  
	DECLARE @tbVariantSiteIds TABLE (Id uniqueidentifier)  
	IF @ApplicationId IS NOT NULL  
		INSERT INTO @tbVariantSiteIds SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId)  
  
	DECLARE @tbProductIds TABLE (Id uniqueidentifier)  
	IF @ProductIds IS NOT NULL  
		INSERT INTO @tbProductIds   
		SELECT tab.col.value('@Id','uniqueidentifier') FROM @ProductIds.nodes('/Products/Product') tab(col)  
	ELSE IF @ProductId IS NOT NULL  
		INSERT INTO @tbProductIds VALUES (@ProductId)  
  
	DECLARE @tbUserIds TABLE (Id uniqueidentifier)  
	IF @UserIds IS NOT NULL  
		INSERT INTO @tbUserIds   
		SELECT tab.col.value('@Id','uniqueidentifier') FROM @UserIds.nodes('/Users/User') tab(col)  
  
	DECLARE @tbExcludedUserIds TABLE (Id uniqueidentifier)  
	IF @ExcludedProductIds IS NOT NULL  
		INSERT INTO @tbExcludedUserIds  
		SELECT UserId FROM @ExcludedProductIds.nodes('/Products/Product') tab1(col)  
			JOIN USSiteUser S ON tab1.col.value('@Id','uniqueidentifier') = S.ProductId  
  
	IF @IncludeVariantUsers IS NULL SET @IncludeVariantUsers = 0  
	IF @Status < 0 SET @Status = NULL  
   
	IF(@UserId IS NULL AND @UserName IS NOT NULL)    
		SELECT @UserId = Id FROM dbo.USUser U   
			INNER JOIN dbo.USSiteUser S ON U.Id = S.UserId   
		WHERE LOWER(u.UserName) = LOWER(@UserName) AND    
			(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))) AND  
			(@ProductId IS NULL OR @ProductId = S.ProductId)     
   
	DECLARE @tbUser TYUser  
	INSERT INTO @tbUser  
	SELECT DISTINCT   
		U.Id,   
		U.UserName,    
		M.Email,  
		M.PasswordQuestion,   
		M.IsApproved,  
		U.CreatedDate,  
		M.LastLoginDate,  
		U.LastActivityDate,  
		M.LastPasswordChangedDate,  
		M.IsLockedOut,  
		M.LastLockoutDate,  
		U.FirstName,  
		U.LastName,  
		U.MiddleName,  
		U.ExpiryDate,  
		U.EmailNotification,  
		U.TimeZone,  
		U.ReportRangeSelection,  
		U.ReportStartDate,  
		U.ReportEndDate,  
		U.BirthDate,  
		U.CompanyName,  
		U.Gender,  
		U.HomePhone,  
		U.MobilePhone,  
		U.OtherPhone,  
		U.ImageId,  
		U.Status
	FROM dbo.USUser U  
		INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
		INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
		(@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser) AND  
		(@ApplicationId IS NULL OR  
		(@IncludeVariantUsers = 0 AND S.SiteId = @ApplicationId) OR  
		(@IncludeVariantUsers = 1 AND S.SiteId IN (Select Id FROM @tbVariantSiteIds))   
		)   
		INNER JOIN @tbProductIds P ON P.Id = S.ProductId  
	WHERE (  
		(@Status IS NULL AND U.Status != 3) OR  
		(@Status IS NOT NULL AND @Status = U.Status )  
		) AND  
		(  
		@IgnoreStatus = 1 OR  
		(         
			(@IsApproved IS NULL OR M.IsApproved = @IsApproved) AND  --Checks whether the user is approved.  
			(@IsLockedOut IS NULL OR M.IsLockedOut = @IsLockedOut) AND -- Checks whether the user is locked out.  
			(@IncludeExpiredUsers = 1 OR U.ExpiryDate IS NULL OR U.ExpiryDate >= getutcdate()) AND  
			(@SinceExpired IS NULL OR U.ExpiryDate < @SinceExpired)  
		)  
		) AND  
		M.UserId NOT IN (SELECT Id FROM @tbExcludedUserIds) AND  
		(  
		@FilterBy IS NULL OR   
		U.UserName like @FilterBy OR U.FirstName like @FilterBy OR U.LastName like @FilterBy  
		) AND  
		(@UserIds IS NULL OR U.Id IN (SELECT Id FROM @tbUserIds)) AND  
		(@UserId IS NULL OR U.Id = @UserId)  
   
	IF @UserNameOnly = 1
		SELECT Id AS UserId, UserName, FirstName, LastName
			FROM @tbUser
	ELSE
		EXEC [dbo].[Membership_BuildUser]   
			@tbUser = @tbUser,   
			@SortBy = @SortBy,   
			@SortOrder = @SortOrder,  
			@PageNumber = @PageNumber,  
			@PageSize = @PageSize  
END
GO
PRINT 'Altering procedure iAppsForm_GetSummary'
GO
ALTER Procedure [dbo].[iAppsForm_GetSummary](
@ApplicationId uniqueidentifier,
@StartDate datetime,
@EndDate	datetime
)
AS
BEGIN
--********************************************************************************
-- code
--********************************************************************************
	set @StartDate = dbo.ConvertTimeToUtc(@StartDate,@ApplicationId)
	set @EndDate = dbo.ConvertTimeToUtc(@EndDate,@ApplicationId)
	
	SELECT top 4
				FormId			= f.Id,
				Title			= f.Title , 
				TotalResponses	= count(r.Id)
		FROM Forms f LEFT JOIN FormsResponse r ON r.FormsId = f.Id 
			AND r.ResponseDate between @StartDate and @EndDate
		WHERE
			f.Status <> dbo.GetDeleteStatus() 
			AND f.ApplicationId = @ApplicationId
		GROUP BY f.Id, f.Title, f.CreatedDate
		ORDER BY count(r.Id) DESC, f.CreatedDate DESC

	
END
GO
PRINT 'Altering procedure [Blog_Save]'
GO
ALTER PROCEDURE [dbo].[Blog_Save] 
(
	@Id						uniqueidentifier=null OUT ,
	@ApplicationId			uniqueidentifier,	
 	@Title					nvarchar(256)=null,
 	@Description			nvarchar(1024)=null,
 	@BlogNodeType			int=0,
 	@MenuId 				uniqueidentifier=null,
	@ContentDefinitionId	uniqueidentifier=null,
 	@ShowPostDate 			bit=true,
 	@DisplayAuthorName 		bit=false,
 	@EmailBlogOwner 		bit=true,
 	@ApproveComments 		bit=true,
 	@RequiresUserInfo 		bit=true,
 	@ModifiedBy				uniqueidentifier,
 	@ModifiedDate			datetime=null, 	
 	@Status					int=null,
	@BlogListPageId			uniqueidentifier=null,
	@BlogDetailPageId		uniqueidentifier=null
)
AS
BEGIN
	DECLARE
		@NewId		uniqueidentifier,
		@RowCount	INT,
		@Stmt 		VARCHAR(25),
		@Now 		datetime,
		@Error 		int	

	SET @Now = getutcdate()

	IF NOT EXISTS (SELECT * FROM BLBlog WHERE Id = @Id)
	BEGIN
		SET @Stmt = 'Create Blog'
		set @Status = dbo.GetStatus(8,'Create')
		INSERT INTO BLBlog  WITH (ROWLOCK)(
			 	 Id,
				 ApplicationId,	
 				 Title,
 				 Description,
 				 BlogNodeType,
 				 MenuId, 		
				 ContentDefinitionId,		 
 				 ShowPostDate,
 				 DisplayAuthorName,
 				 EmailBlogOwner,
 				 ApproveComments,
 				 RequiresUserInfo,
 				 CreatedBy,
 				 CreatedDate, 	
 				 Status
		)
		values
			(
				@Id,
				@ApplicationId,	
 				@Title,
 				@Description,
 				@BlogNodeType,
 				@MenuId, 			
				@ContentDefinitionId,	
				isnull(@ShowPostDate,1),
 				isnull(@DisplayAuthorName,0),
 				isnull(@EmailBlogOwner,1),
 				isnull(@ApproveComments,1),
				isnull(@RequiresUserInfo,1),
 				@ModifiedBy,
 				@Now, 	
 				@Status		
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END

		SELECT @Id 
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify Blog'
		
		UPDATE	BLBlog  WITH (rowlock)	SET 
			Title = @Title,
			MenuId = @MenuId,
 			ShowPostDate = @ShowPostDate,
 			DisplayAuthorName = @DisplayAuthorName,
 			EmailBlogOwner = @EmailBlogOwner,
 			ApproveComments = @ApproveComments,
 			RequiresUserInfo = @RequiresUserInfo,
 			ModifiedBy = @ModifiedBy,
			ModifiedDate = @Now,
			BlogListPageId = @BlogListPageId,
			BlogDetailPageId = @BlogDetailPageId,
			ContentDefinitionId = @ContentDefinitionId,
			Status = @Status
        WHERE
			Id = @Id

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		

END
GO
