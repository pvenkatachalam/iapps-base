﻿--Patch script for 5.1.0726.01
GO
PRINT 'Updating the version of the products to 5.1.0726.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.0726.1'
GO
PRINT 'Creating a sitesetting for Social.CentralizedSocialCredentials with value false'
GO

DECLARE @SiteId UNIQUEIDENTIFIER,@SettingTypeId INT,@Sequence INT, @RowCount INT
DECLARE @SiteIdTab AS TABLE(Id UNIQUEIDENTIFIER, RowNum INT)


INSERT INTO @SiteIdTab
SELECT Id , ROW_NUMBER() OVER (ORDER BY Id ) AS Row FROM SISite Where Status = 1 AND Id = MasterSiteId -- Only Master sites

SELECT @RowCount = ISNULL(MAX(RowNum),0) FROM @SiteIdTab


WHILE (@RowCount > 0)
BEGIN

	SELECT @SiteId = Id FROM @SiteIdTab WHERE [RowNum]=@RowCount

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.CentralizedSocialCredentials')
		IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
		BEGIN
			IF @SettingTypeId IS NULL
			BEGIN
					SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
					INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
					VALUES('Social.CentralizedSocialCredentials', 'Social.CentralizedSocialCredentials',1, 1, @Sequence)
			END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value )
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
						@SettingTypeId, -- SettingTypeId - int
						N'false'  -- Value - nvarchar(max)
						) 
		END
		--Decrese the row count
		SET @RowCount = @RowCount - 1
                   
END
GO


PRINT 'Altering procedure Membership_GetUserWithProfile'
GO
--exec Membership_GetUserWithProfile @ProductId='00000000-0000-0000-0000-000000000000',@ApplicationId='CDD38906-6071-4DD8-9FB6-508C38848C61',@UserId=NULL,@UserName=N'iappsuser'
--exec Membership_GetUserWithProfile @ProductId='00000000-0000-0000-0000-000000000000',@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@UserId=NULL,@UserName=N'iappsuser'

ALTER PROCEDURE [dbo].[Membership_GetUserWithProfile]  
--********************************************************************************  
-- PARAMETERS  
 @ProductId     uniqueidentifier = NULL,  
 @ApplicationId    uniqueidentifier,  
    @UserId      uniqueidentifier,  
 @UserName     nvarchar(256) = NULL  
--********************************************************************************  
AS   
BEGIN  
  Declare @EmptyGuid uniqueidentifier  
  Select @EmptyGuid = dbo.GetEmptyGUID()  
  
  IF(@UserId IS NULL)  
  BEGIN  
   SELECT @UserId = Id  
   FROM dbo.USUser u,dbo.USSiteUser s  
   WHERE LOWER(u.UserName) = LOWER(@UserName) AND  
     --s.SiteId = ISNULL(@ApplicationId,s.SiteId) AND  
     (@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
     AND u.Id = s.UserId AND  
     (@ProductId Is null or @ProductId = @EmptyGuid or @ProductId = s.ProductId)  
	 AND u.Status <> dbo.GetDeleteStatus()
  END    
  
  SELECT  u.UserName,  
    u.CreatedDate,  
    m.Email,   
    m.PasswordQuestion,    
    m.IsApproved,  
    m.LastLoginDate,   
    u.LastActivityDate,  
    m.LastPasswordChangedDate,   
    u.Id,   
    m.IsLockedOut,  
    m.LastLockoutDate,
    u.FirstName,
	u.LastName,
	u.MiddleName,
	u.ExpiryDate,
	u.EmailNotification,
	u.TimeZone,
	u.ReportRangeSelection,
	u.ReportStartDate,
	u.ReportEndDate,
	u.BirthDate,
	u.CompanyName,
	u.Gender,
	u.HomePhone,
	u.MobilePhone,
	u.OtherPhone,
	u.ImageId,
	u.DashboardData
  FROM    dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s--, dbo.USMemberProfile p  
  WHERE   u.Id = @UserId AND  
    u.Id = m.UserId     AND  
    --s.SiteId = ISNULL(@ApplicationId,s.SiteId)    AND  
     (@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
    AND m.UserId = s.UserId    AND  
    u.Status <> dbo.GetDeleteStatus() AND  
    --p.UserId = u.Id  AND  
    (@ProductId Is null or @ProductId = @EmptyGuid or @ProductId = s.ProductId)  
  
  --selects the user's profile from the UsMemberProfile table.  
  SELECT UserId,  
    PropertyName,  
    PropertyValueString,  
    PropertyValuesBinary,  
    LastUpdatedDate  
  FROM dbo.USMemberProfile  
  WHERE UserId = @UserId  
END
GO
PRINT 'Altering procedure Customer_GetCustomers'
GO
ALTER PROCEDURE [dbo].[Customer_GetCustomers]
    (
      @IsActive BIT = NULL ,
      @Searchkeyword NVARCHAR(4000) = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @PageSize INT = NULL ,
      @PageNumber INT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN  
  


  
        DECLARE @searchText NVARCHAR(MAX) ,
            @IsactiveCondition NVARCHAR(100)  
  
        IF @IsActive IS NULL 
            SET @IsactiveCondition = ''  
        ELSE 
            SET @IsactiveCondition = ' IsActive = '
                + CAST(@IsActive AS VARCHAR(1)) + ' AND '  
   
        IF @SearchKeyword IS NULL 
            SET @SearchKeyword = '%'  
        ELSE 
            SET @SearchKeyword = '%' + ( @Searchkeyword ) + '%'  
        SET @searchText = '''' + @Searchkeyword + ''''  
  
  
        IF @PageSize IS NULL 
            SET @PageSize = 2147483647  
  
        IF @SortColumn IS NULL 
            SET @SortColumn = 'LastName ASC'  
  
        DECLARE @SQL NVARCHAR(MAX) ,
            @params NVARCHAR(100)  
  
        SET @params = N'@SIZE INT, @nbr INT,  @Sort VARCHAR(25), @ApplicationId uniqueidentifier'  
  
        SET @SQL = N'  
  
;WITH PagingCTE (
	Pages
	,Row_ID  
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone
   ,ImageId  
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy
   ,County
   ,ExternalId
   ,ExternalProfileId
    )  
  
AS  
  
(  
  
SELECT   
Count(*) OVER()  as Pages,
     ROW_NUMBER()   
  
            OVER(ORDER BY ' + @SortColumn
            + '  
  
--             CASE WHEN @Sort=''LastName DESC'' THEN LastName END DESC,  
--  
--             CASE WHEN @Sort=''LastName ASC''  THEN LastName END ASC,  
--  
--       CASE WHEN @Sort=''FirstName DESC'' THEN FirstName END DESC,  
--  
--             CASE WHEN @Sort=''FirstName ASC''  THEN FirstName END ASC,  
--           
--    CASE WHEN @Sort=''Email DESC'' THEN Email END DESC,  
--  
--             CASE WHEN @Sort=''Email ASC''  THEN Email END ASC,  
--  
--    CASE WHEN @Sort=''AddressLine1 DESC'' THEN AddressLine1 END DESC,  
--  
--             CASE WHEN @Sort=''AddressLine1 ASC''  THEN AddressLine1 END ASC,  
--  
--       CASE WHEN @Sort=''AddressLine2 DESC'' THEN AddressLine2 END DESC,  
--  
--             CASE WHEN @Sort=''AddressLine2 ASC''  THEN AddressLine2 END ASC,  
--           
--    CASE WHEN @Sort=''City DESC'' THEN City END DESC,  
--  
--             CASE WHEN @Sort=''City ASC''  THEN City END ASC,  
--  
--    CASE WHEN @Sort=''Phone DESC'' THEN Phone END DESC,  
--  
--             CASE WHEN @Sort=''PHONE ASC''  THEN Phone END ASC,  
--  
--    CASE WHEN @Sort=''Status DESC'' THEN Status END DESC,  
--  
--             CASE WHEN @Sort=''Status ASC''  THEN Status END ASC  
           
           
            ) AS [Row_ID]  
  
     
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State 
	,StateCode 
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM vwSearchCustomer  
Where SiteId = @ApplicationId AND ' + @IsactiveCondition + '   
 (  	
  (LastName) Like ' + @searchText + '  
  OR (FirstName) Like ' + @searchText + '  
  OR (Email) Like ' + @searchText + '  
  OR (AddressLine1) Like ' + @searchText + '  
  OR (City) Like ' + @searchText + '  
  OR (State) Like ' + @searchText + '  
  OR (Zip) Like ' + @searchText + '  
  OR (HomePhone) Like ' + @searchText + '  
  OR (AccountNumber) Like ' + @searchText + ' 
  OR (CompanyName) Like ' + @searchText + ' 
  OR case when Status =1 then ''active'' else ''deactive'' end Like '
            + @searchText + '  
 )  
)   
  
SELECT   
Pages
 ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
   ,CreatedBy  
   ,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId) CreatedDate  
   ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId) ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM PagingCTE pcte  
WHERE Row_ID >= (@SIZE * @nbr) - (@SIZE -1) AND Row_ID <= @SIZE * @nbr'  
  
        PRINT @SQL  
        EXEC sp_executesql @SQL, @params, @SIZE = @PageSize,
            @nbr = @PageNumber, @Sort = @SortColumn,
            @ApplicationId = @ApplicationId 
        DECLARE @RowCount INT
        SELECT  @RowCount = @@ROwCOUNT
      

        SELECT  1 


    END
GO
PRINT 'Altering procedure Product_SimpleSearchProduct'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Product_SimpleSearchProduct]
    (
      @SearchTerm NVARCHAR(MAX) ,
      @IsBundle TINYINT = NULL ,
      @pageNumber INT = NULL ,
      @pageSize INT = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @ProductTypeId UNIQUEIDENTIFIER = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN  
        DECLARE @PageLowerBound BIGINT ,
            @PageUpperBound BIGINT 

        SET @PageLowerBound = ISNULL(@pageSize, 0) * ISNULL(@pageNumber, 0)
        SET @PageUpperBound = @PageLowerBound - ISNULL(@pageSize, 0) + 1 ;

        DECLARE @searchText NVARCHAR(MAX)  
        SET @searchText = '%' + @SearchTerm + '%'
        IF @SortColumn IS NULL 
            SET @SortColumn = 'productname asc'
        SET @SortColumn = LOWER(@SortColumn) ;
            WITH    PagingCTE ( Pages, RowNum, ProductId, AvailableToSell, Quantity, IsActive, IsUnlimitedQuantity, ProductName, ProductTypeName )
                      AS ( SELECT   COUNT(*) OVER ( ) AS Pages ,
                                    ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN @SortColumn = 'productname desc'
                                                              THEN MAX(P.Title)
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'productname asc'
                                                              THEN MAX(P.Title)
                                                              END ASC , CASE
                                                              WHEN @SortColumn = 'quantity desc'
                                                              THEN SUM(I.Quantity)
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'quantity asc'
                                                              THEN SUM(I.Quantity)
                                                              END ASC , CASE
                                                              WHEN @SortColumn = 'availabletosell desc'
                                                              THEN SUM(A.Quantity)
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'availabletosell asc'
                                                              THEN SUM(A.Quantity)
                                                              END ASC ) AS RowNum ,
                                    P.Id AS ProductId ,
                                    SUM(A.Quantity) AS AvailableToSell ,
                                    SUM(I.Quantity) AS Quantity ,
                                    P.IsActive ,
                                    MAX(CAST(S.IsUnlimitedQuantity AS INT)) ,
                                    P.Title AS ProductName ,
                                    T.Title AS ProductTypeName
                           FROM     PRProduct P
                                    INNER JOIN PRProductSku S ON S.ProductId = P.Id
                                    INNER JOIN PRProductType T ON T.Id = P.ProductTypeId
                                    LEFT JOIN vwInventoryPerSite I ON I.SkuId = S.Id
                                                              AND I.SiteId = @ApplicationId
                                    LEFT JOIN vwAvailableToSellPerWarehouse A ON A.SkuId = S.Id
                                                              AND A.SiteId = @ApplicationId
                           WHERE    --P.SiteId = @ApplicationId AND
                                    ( @ProductTypeId IS NULL
                                      OR T.Id = @ProductTypeId
                                    )
                                    AND ( @IsBundle IS NULL
                                          OR P.IsBundle = @IsBundle
                                        )
                                    AND ( P.Title LIKE @searchText
                                          OR P.ShortDescription LIKE @searchText
                                          OR P.LongDescription LIKE @searchText
                                          OR P.Description LIKE @searchText
                                          OR S.SKU LIKE @searchText
                                        )
                           GROUP BY P.Id ,
                                    P.IsActive ,
                                    P.Title ,
                                    T.Title
                         )
            SELECT  *
            INTO    #tmpProduct
            FROM    PagingCTE
            WHERE   ( RowNum BETWEEN @PageUpperBound
                             AND     @PageLowerBound ) 
            


        SELECT  ProductID AS Id ,
                ProductId ,
                ProductName ,
                Quantity ,
                AvailableToSell ,
                IsACtive ,
                IsUnlimitedQuantity ,
                ProductTypeName ,
                '' AS SKU
        FROM    #tmpProduct       

        
       declare @activestatus int = dbo.GetActiveStatus()

		SELECT	C.[Id],	C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
		FROM	#tmpProduct T
				INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
				INNER JOIN CLImage I on I.Id = OI.ImageId 
				INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.ParentImage
				INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType

		UNION ALL

		SELECT	C.[Id],	C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
		FROM	#tmpProduct T
				INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
				INNER JOIN CLImage I on I.Id = OI.ImageId 
				INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.Id
				INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType

		Order by Sequence--, Title

		 
        SELECT TOP 1
                Pages
        FROM    #tmpProduct
       
    END
GO
PRINT 'Altering procedure Product_SimpleSearchSKU'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  ALTER PROCEDURE [dbo].[Product_SimpleSearchSKU]
    (
      @SearchTerm NVARCHAR(MAX) ,
      @IsBundle TINYINT = NULL ,
      @pageNumber INT = NULL ,
      @pageSize INT = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @ProductTypeId UNIQUEIDENTIFIER = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN  
        DECLARE @PageLowerBound BIGINT ,
            @PageUpperBound BIGINT ,
            @SkuCount BIGINT

        SET @PageLowerBound = ISNULL(@pageSize, 0) * ISNULL(@pageNumber, 0)
        SET @PageUpperBound = @PageLowerBound - ISNULL(@pageSize, 0) + 1 ;

        DECLARE @searchText NVARCHAR(MAX)  
        SET @searchText = '%' + @SearchTerm + '%'
        IF @SortColumn IS NULL 
            SET @SortColumn = 'productname asc'
        SET @SortColumn = LOWER(@SortColumn) ;
        WITH    PagingCTE ( Pages, RowNum, SKUId, ProductId, ProductName, Qty, AllocatedQty, ProductTypeName, SKUTitle, SKU, IsUnlimitedQuantity, AvailableForBackOrder, MaximumDiscountPercentage, BackOrderLimit, ProductActive, SKUActive )
                  AS ( SELECT   COUNT(*) OVER ( ) AS Pages ,
                                ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN @SortColumn = 'productname desc'
                                                              THEN P.Title
                                                             END DESC , CASE
                                                              WHEN @SortColumn = 'productname asc'
                                                              THEN P.Title
                                                              END ASC , CASE
                                                              WHEN @SortColumn = 'sku desc'
                                                              THEN S.SKU
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'sku asc'
                                                              THEN S.SKU
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'quantity desc'
                                                              THEN I.Quantity
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'quantity asc'
                                                              THEN I.Quantity
                                                              END ASC , CASE
                                                              WHEN @SortColumn = 'availabletosell desc'
                                                              THEN A.Quantity
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'availabletosell asc'
                                                              THEN A.Quantity
                                                              END ASC ) AS RowNum ,
                                S.Id AS SKUId ,
                                P.Id AS ProductId ,
                                P.Title AS ProductName ,
                                I.Quantity ,
                                ISNULL(A.Quantity, 0) ,
                                T.Title AS ProductTypeName ,
                                S.Title AS SKUTitle ,
                                S.SKU ,
                                IsUnlimitedQuantity ,
                                AvailableForBackOrder ,
                                MaximumDiscountPercentage ,
                                S.BackOrderLimit ,
                                P.IsACtive AS ProductActive ,
                                S.IsActive AS SKUActive
                       FROM     PRProduct P
                                INNER JOIN PRProductSku S ON S.ProductId = P.Id
                                INNER JOIN PRProductType T ON T.Id = P.ProductTypeId
                                LEFT JOIN vwInventoryPerSite I ON I.SkuId = S.Id
                                                              AND I.SiteId = @ApplicationId
                                LEFT JOIN vwAvailableToSellPerWarehouse A ON A.SkuId = S.Id
                                                              AND A.SiteId = @ApplicationId
                       WHERE    --P.SiteId = @ApplicationId AND
                                ( @ProductTypeId IS NULL
                                  OR T.Id = @ProductTypeId
                                )
                                AND ( @IsBundle IS NULL
                                      OR P.IsBundle = @IsBundle
                                    )
                                AND ( P.Title LIKE @searchText
                                      OR P.ShortDescription LIKE @searchText
                                      OR P.LongDescription LIKE @searchText
                                      OR P.Description LIKE @searchText
                                      OR S.SKU LIKE @searchText
                                    )
                     )
            SELECT  Pages ,
                    RowNum ,
                    SKUId ,
                    ProductId ,
                    ProductName ,
                    ISNULL(dbo.GetBundleQuantityPerSite(SKUId, @ApplicationId),
                           Qty) AS Qty ,
                    AllocatedQty ,
                    ProductTypeName ,
                    SKUTitle ,
                    SKU ,
                    IsUnlimitedQuantity ,
                    AvailableForBackOrder ,
                    MaximumDiscountPercentage ,
                    BackOrderLimit ,
                    ProductActive ,
                    SKUActive
            INTO    #tbSku
            FROM    PagingCTE
            WHERE   ( RowNum BETWEEN @PageUpperBound
                             AND     @PageLowerBound ) 
		
--SELECT * FROM #tbSku
        SELECT  T.SKUId AS ID ,
                T.ProductId ,
                T.ProductName ,
                T.Qty AS Quantity ,
                T.Qty - T.AllocatedQty AS AvailableToSell ,
                T.SKUTitle ,
                T.SKU ,
                CASE WHEN T.Qty >= 999999 THEN 1
                     ELSE T.IsUnlimitedQuantity
                END IsUnlimitedQuantity ,
                T.AvailableForBackOrder ,
                T.MaximumDiscountPercentage ,
                ISNULL(T.BackOrderLimit, 0) AS BackOrderLimit ,
                T.AvailableForBackOrder ,
                CAST(CASE WHEN T.SKUActive = 1
                               AND T.ProductActive = 1 THEN 1
                          ELSE 0
                     END AS BIT) AS IsActive ,
                T.ProductTypeName
        FROM    #tbSku T
               
		
        DECLARE @activestatus INT = dbo.GetActiveStatus()

        SELECT  C.[Id] ,
                C.[Title] ,
                C.[Description] ,
                C.[FileName] ,
                C.[Size] ,
                C.[Attributes] ,
                C.[RelativePath] ,
                C.[AltText] ,
                C.[Height] ,
                C.[Width] ,
                C.[Status] ,
                C.[SiteId] ,
                OI.[ObjectId] AS ProductId ,
                OI.[ObjectType] ,
                OI.[IsPrimary] ,
                OI.[Sequence] ,
                C.[ParentImage] ,
                C.ImageType ,
                OI.ObjectId AS ProductId
        FROM    #tbSku T
                INNER JOIN CLObjectImage OI ON OI.ObjectType = 205
                                               AND T.ProductId = OI.ObjectId
                INNER JOIN CLImage I ON I.Id = OI.ImageId
                INNER JOIN CLImage C ON i.status = @activestatus
                                        AND i.id IS NOT NULL
                                        AND I.Id = C.ParentImage
                INNER JOIN CLImageType IT ON IT.TypeNumber = 0
                                             AND IT.Id = C.ImageType
        UNION ALL
        SELECT  C.[Id] ,
                C.[Title] ,
                C.[Description] ,
                C.[FileName] ,
                C.[Size] ,
                C.[Attributes] ,
                C.[RelativePath] ,
                C.[AltText] ,
                C.[Height] ,
                C.[Width] ,
                C.[Status] ,
                C.[SiteId] ,
                OI.[ObjectId] AS ProductId ,
                OI.[ObjectType] ,
                OI.[IsPrimary] ,
                OI.[Sequence] ,
                C.[ParentImage] ,
                C.ImageType ,
                OI.ObjectId AS ProductId
        FROM    #tbSku T
                INNER JOIN CLObjectImage OI ON OI.ObjectType = 205
                                               AND T.ProductId = OI.ObjectId
                INNER JOIN CLImage I ON I.Id = OI.ImageId
                INNER JOIN CLImage C ON i.status = @activestatus
                                        AND i.id IS NOT NULL
                                        AND I.Id = C.Id
                INNER JOIN CLImageType IT ON IT.TypeNumber = 0
                                             AND IT.Id = C.ImageType
        ORDER BY Sequence--, Title
 
	

        SELECT TOP 1
                Pages
        FROM    #tbSku
    END
GO

GO
PRINT 'Altering procedure ProductImage_GetImageForProduct'
GO
ALTER PROCEDURE [dbo].[ProductImage_GetImageForProduct]
    (
      @ProductId UNIQUEIDENTIFIER = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) 
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************

      
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        @ProductId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(@ProductId,
                                                              C.Id)
                FROM    CLObjectImage OI --INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
                WHERE   C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                        AND OI.ObjectId = @ProductId
                ORDER BY OI.Sequence ,
                        C.Title
       
    END
	GO
	PRINT 'Altering procedure ProductImage_GetImageForProduct'
GO
IF OBJECT_ID('ProductImage_GetImageForProducts') IS NOT NULL
	DROP PROCEDURE ProductImage_GetImageForProducts
GO
CREATE PROCEDURE [dbo].[ProductImage_GetImageForProducts]
    (
      @ProductIds XML = NULL ,
      @IncludeSKUs BIT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    ) 
AS --********************************************************************************
-- Variable declaration
--********************************************************************************
    BEGIN
--********************************************************************************
-- code
--********************************************************************************
                DECLARE @ProductIdTable TABLE ( Id UNIQUEIDENTIFIER )
                INSERT  INTO @ProductIdTable
                        ( Id
                        )
                        SELECT  tab.col.value('text()[1]', 'uniqueidentifier')
                        FROM    @ProductIds.nodes('/ObjectIdCollection/ObjectId') tab ( col )
                SELECT  C.[Id] ,
                        C.[Title] ,
                        C.[Description] ,
                        C.[FileName] ,
                        C.[Size] ,
                        C.[Attributes] ,
                        C.[RelativePath] ,
                        C.[AltText] ,
                        C.[Height] ,
                        C.[Width] ,
                        C.[Status] ,
                        dbo.ConvertTimeFromUtc(C.[CreatedDate], @ApplicationId) CreatedDate ,
                        C.[CreatedBy] ,
                        dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) ModifiedDate ,
                        C.[ModifiedBy] ,
                        C.[SiteId] ,
                        OI.[ObjectId] ,
                        OI.[ObjectType] ,
                        OI.[IsPrimary] ,
                        OI.[Sequence] ,
                        C.[ParentImage] ,
                        C.ImageType ,
                        OI.ObjectId AS ProductId ,
                        ProductSKUIds = dbo.GetRelatedSKUsToImageAsXml(OI.ObjectId,
                                                              C.Id)
                FROM    @ProductIdTable P
                        JOIN CLObjectImage OI ON P.Id = OI.ObjectId
	--INNER JOIN CLImage I on I.Id=OI.ImageId
                        INNER JOIN CLImage C ON OI.ImageId = ISNULL(C.ParentImage,
                                                              C.Id)
	--Inner join @ProductIds.nodes('/ObjectIdCollection/ObjectId')tab(col) ON  OI.ObjectId =tab.col.value('text()[1]','uniqueidentifier')
                WHERE   --I.SiteId=Isnull(@ApplicationId,I.SiteId) 
	--(@ApplicationId is null or C.SiteId = @ApplicationId)
	--AND 
                        C.Status = dbo.GetActiveStatus()
                        AND C.Id IS NOT NULL
                        AND ObjectType = 205
                ORDER BY OI.Sequence ,
                        C.Title
    END

	GO


PRINT 'Altering procedure CartItem_Delete'
GO
ALTER PROCEDURE [dbo].[CartItem_Delete]
		(
			@Id uniqueidentifier,
			@CartId uniqueidentifier
		)
AS
Begin

	DECLARE @CurrentDate datetime
	set @CurrentDate = getUTCDate()

		if( @Id is null OR  @Id = dbo.GetEmptyGUID())
			begin
				delete from CSCartItem where CartId = @CartId
			end
		else
			begin
				Declare @ThisItemSequence int
				Declare @MaxSequence int

				Select @ThisItemSequence = Sequence from CSCartItem where Id = @Id
				Select @MaxSequence = IsNull(Max(Sequence),1) from CSCartItem
									 Where CartId= @CartId
	
				DELETE FROM CSCartItem where ParentCartItemId = @Id  OR Id=@Id
						
				If @ThisItemSequence < 	@MaxSequence
					Begin	
						Update CSCartItem set Sequence = Sequence -1 where 
								Sequence > @ThisItemSequence and CartId = @CartId			
					End
			end
		Update CSCart Set ModifiedDate = @CurrentDate where Id= @CartId

	Delete from OROrderItemAttributeValue where 
		(@CartId is null or CartId = @CartId)
		--CartId = isnull(@CartId,CartId) 
		and CartItemId=@Id


End
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='CSCartItem_CartId' AND object_id = OBJECT_ID('CSCartItem'))
CREATE NONCLUSTERED INDEX [CSCartItem_CartId] ON dbo.CSCartItem([CartId]) 
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='CSCartItem_ParentCartItemId' AND object_id = OBJECT_ID('CSCartItem'))
CREATE NONCLUSTERED INDEX [CSCartItem_ParentCartItemId] ON dbo.CSCartItem([ParentCartItemId]) 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[UserManager_GetUserSegment]  
(  
	@VisitorType int ,  
	@Geo   nvarchar(50),  
	@Resolution  nvarchar(50),  
	@Os    nvarchar(50),  
	@Browser  nvarchar(500),  
	@JavaScriptSupport int,  
	@FlashSupport  int,  
	@SiteId  uniqueidentifier  ,
	@SinceLastVisitDays int = 0,
	@StateCode nvarchar(100) = '',
	@SearchKeyword nvarchar(max)=null,
	@DeviceType int =0,
	@ReferrerType int =0,
	@ReferrerUrl nvarchar(max),
	@EntryPage uniqueidentifier
)  
AS   
BEGIN   
	DECLARE	@ResolutionId		uniqueidentifier,  
			@OsId				uniqueidentifier,  
			@BrowserId			uniqueidentifier,  
			@GeoId				uniqueidentifier,  
			@GeoInt				bigint,  
			@CountryCode		nvarchar(50),  
			@ResolutionOtherId  uniqueidentifier,  
			@OsOtherId			uniqueidentifier,  
			@BrowserOtherId     uniqueidentifier,  
			@GeoOtherId			uniqueidentifier,  
			@Result				xml  
    
	DECLARE @intrinsicFrameworkAccount uniqueidentifier
	SET @intrinsicFrameworkAccount='6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
    
	if(select count(*) from dbo.ALAudienceSegmentCriteria where SegmentType = 0 and CriteriaName = @Browser) =0
	begin 
		insert into dbo.ALAudienceSegmentCriteria values (newid(),@Browser,@Browser,0,@intrinsicFrameworkAccount,getdate(),null,null)
	end
   
	if(select count(*) from dbo.ALAudienceSegmentCriteria where SegmentType =1 and CriteriaName=@Os)=0
	begin 
		insert into dbo.ALAudienceSegmentCriteria values (newid(),@Os,@Os,1,@intrinsicFrameworkAccount,getdate(),null,null)
	end 
	
	if @EntryPage = dbo.GetEmptyGUID()
		set @EntryPage = null
    
    if(@ReferrerUrl is null)
		select @ReferrerUrl=''
    
	CREATE TABLE #UserSegment (  
			[SegmentId] [uniqueidentifier] NOT NULL,  
			[Browser] [nvarchar](max) NULL,  
			[OperatingSystems] [nvarchar](max)  NULL,  
			[Resolution] [nvarchar](max) NULL,  
			[Geography] [nvarchar](max) NULL,  
			[IsBrowserFiltered] int default 0 ,  
			[IsOperatingSystemsFiltered] int default 0,  
			[IsResolutionFiltered] int default 0,  
			[IsGeoFiltered] int default 0,
			[SegmentName] nvarchar(max),
			StatesInUs nvarchar(max),
			NoOfDaysSinceLastVisited int,[IsSearchKeywordFiltered] int default 0,
			SearchKeyword nvarchar(max) null,
			PagesVisitedPageGrpCondition int null,
			TotalNoOfPagesVisited int)

	INSERT INTO #UserSegment (SegmentId, Browser ,OperatingSystems,Resolution,Geography,SegmentName,
				StatesInUS,NoOfDaysSinceLastVisited,SearchKeyword,PagesVisitedPageGrpCondition,TotalNoOfPagesVisited)  
	SELECT	a.SegmentId, 
			a.Browser ,
			a.OperatingSystems,
			a.Resolution,
			a.Geography,
			b.SegmentName, 
			a.StatesInUS,
			a.NoOfDaysSinceLastVisited,
			a.SearchKeyword,
			a.TotalPagesVisitedOperator,
			a.TotalNoOfPagesVisited
	FROM dbo.ALAudienceSegmentDetails a, ALAudienceSegment b
	WHERE	(@DeviceType is Null Or a.DeviceType = 0 Or a.DeviceType=@DeviceType) AND a.SegmentId = b.Id and 
			(@ReferrerType is null Or a.ReferrerType=0 Or a.ReferrerType = @ReferrerType) 	
			AND a.VisitorType in (0,3,@VisitorType) -- Not Applicable , Both and identified visitor type  
			 and  a.FlashEnabled in (2,@FlashSupport)  -- Not Applicable and identified flash support  
			and a.JavascriptEnabled in (2,@JavaScriptSupport) -- Not Applicable and identified javascript support  
			and b.SiteId in (select SiteId from [dbo].[GetVariantSites](@siteId))  and b.Status = 1  
			and ( isnull(a.NoOfDaysSinceLastVisited,0) =0 or isnull(a.NoOfDaysSinceLastVisited,0)>=@SinceLastVisitDays) 
			and (a.SegmentId in (select SegmentId from ALAudienceSegmentDetails where (StatesInUs='' or StatesInUs is null or StatesInUs like '%'+ @StateCode +'%')))
			and a.SegmentId in (	
			select ad.SegmentId  from ALAudienceSegmentDetails ad Left join dbo.ALAudienceSegmentReferrers r on ad.SegmentId = r.SegmentId
				where (isnull(r.ReferrerUrl,@ReferrerUrl)='' or r.ReferrerUrl like '%'+ @ReferrerUrl + '%'))
			and a.SegmentId in (select ad.SegmentId  from ALAudienceSegmentDetails ad Left join dbo.ALAudienceSegmentPagesVisited pv on ad.SegmentId = pv.SegmentId and IsEntry = 1
				where (@EntryPage IS NULL Or (pv.PageDefinitionId is null or @EntryPage = pv.PageDefinitionId))
				)
			
			
			---ARUN CODE
						--and (a.SegmentId in (
			--	select SegmentId from ALAudienceSegmentDetails a cross apply dbo.SplitComma(StatesInUS,',') c 
			--	where C.Value in (Select value from dbo.SplitComma(@StateCode,','))) or isnull(StatesInUs,'')='')
			--and (a.SegmentId in (
			--	select ad.SegmentId from ALAudienceSegmentDetails ad ,dbo.ALAudienceSegmentReferrers r 
			--	where (r.ReferrerUrl = @ReferrerUrl )and ad.SegmentId = r.SegmentId ))
			--and (a.SegmentId in (
			--	select ad1.SegmentId from ALAudienceSegmentDetails ad1 cross apply dbo.ALAudienceSegmentPagesVisited pv 
			--	where @EntryPage IS NULL OR @EntryPage = dbo.GetEmptyGUID() Or ( @EntryPage = pv.PageDefinitionId and pv.IsEntry=1)))
			--ARUN CODE 
			
		
	exec GetIPNumber @Geo,@GeoInt OUT  
	select  top 1 @CountryCode = country from dbo.GeoIPCity where @GeoInt between StartIP and EndIP   
  

  
	--GUID of Operating system,Resolution and Browser from ALAudienceSegmentCriteria  
	SELECT @OsId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=1 and AliasCriteriaName=@Os  
	SELECT @ResolutionId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=2 and CriteriaName=@Resolution  
	SELECT @BrowserId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=0 and AliasCriteriaName=@Browser  
	SELECT @GeoId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=3 and AliasCriteriaName=@CountryCode  

	SELECT @ResolutionOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=2 and CriteriaName='Other'  
	SELECT @OsOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=1 and AliasCriteriaName='Other'
	SELECT @BrowserOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=0 and AliasCriteriaName='Other'    
	SELECT @GeoOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=3 and AliasCriteriaName='Other'    

	UPDATE #UserSegment Set IsGeoFiltered = 1  
		WHERE (Geography like '%'+ lower(cast(@GeoId as varchar(36))) +'%' or isnull(Geography,'')='')   

	UPDATE #UserSegment Set IsGeoFiltered=1  
		WHERE (Geography like '%'+ lower(cast(@GeoOtherId as varchar(36))) +'%' or isnull(Geography,'')='')   

	DELETE  from #UserSegment Where IsGeoFiltered=0  

	
	UPDATE #UserSegment Set IsBrowserFiltered=1  
		WHERE (Browser like '%'+ lower(cast(@BrowserId as varchar(36))) +'%' or isnull(Browser,'')='')   

	UPDATE #UserSegment Set IsBrowserFiltered=1  
		WHERE (Browser like '%'+ lower(cast(@BrowserOtherId as varchar(36))) +'%' or isnull(Browser,'')='')   

	DELETE  from #UserSegment Where IsBrowserFiltered=0  
	


	Update #UserSegment Set IsOperatingSystemsFiltered=1  
		WHERE (OperatingSystems like '%'+ lower(cast(@OsId as varchar(36))) +'%' or isnull(OperatingSystems,'')='')   

	Update #UserSegment Set IsOperatingSystemsFiltered=1  
		WHERE (OperatingSystems like '%'+ lower(cast(@OsOtherId as varchar(36))) +'%' or isnull(OperatingSystems,'')='')   

	DELETE  from #UserSegment Where IsOperatingSystemsFiltered=0  


	Update #UserSegment Set [IsResolutionFiltered]=1  
		WHERE (Resolution like '%'+ lower(cast(@ResolutionId as varchar(36))) +'%' or isnull(Resolution,'')='')  

	Update #UserSegment Set [IsResolutionFiltered]=1  
		WHERE (Resolution like '%'+ lower(cast(@ResolutionOtherId as varchar(36))) +'%' or isnull(Resolution,'')='')  
  
	DELETE  from #UserSegment Where [IsResolutionFiltered]=0  

	UPDATE #UserSegment SET [IsSearchKeywordFiltered]=0  

	select  + lower(b.value) as Searchvalue ,  a.SegmentId , 0 as filterValue  into #TmpSearch 
		from #UserSegment a cross apply dbo.SplitComma(replace(a.searchkeyword,char(13)+char(10),'|'),'|') b
		where b.value is not null and len(ltrim(rtrim(value)) )>0

		update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,''))=''
		update #UserSegment set [IsSearchKeywordFiltered]=1 WHERE lower(isnull(SearchKeyword,''))=''

	IF(@SearchKeyword <>'')
		BEgin
		update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,'')) like '%'+ lower(isnull(@SearchKeyword,'')) +'%' and  filterValue=0
		END

	--IF(@SearchKeyword <>'')
	--	update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,'')) like '%'+ lower(isnull(@SearchKeyword,'')) +'%'
	--ELSE 
	--begin
	--	update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,''))=''
	--	update #UserSegment set [IsSearchKeywordFiltered]=1 WHERE lower(isnull(SearchKeyword,''))=''
	--END 

	update p set [IsSearchKeywordFiltered]=1 
		FROM #TmpSearch t JOIN #UserSegment p on t.segmentid=p.segmentId and filterValue=1
   
	DELETE  from #UserSegment Where [IsSearchKeywordFiltered]=0  
--select * from #TmpSearch
 -- ;	select * from #UserSegment
	set @Result = 
	(SELECT UserSegment.SegmentId , UserSegment.SegmentName,UserSegment.PagesVisitedPageGrpCondition,UserSegment.TotalNoOfPagesVisited,
			cast((SELECT UserSegment.SegmentId , Indexterm.ObjectId as TermId from GetObjectByRelation('O-T',UserSegment.SegmentId)  Indexterm  for xml auto) as xml) ITData,  
			cast((SELECT UserSegment.SegmentId , Watch.Id as WatchId, Watch.Name as WatchName,  Isnull(SegmentWatch.SegmentWatchType,0) as SegmentWatchType, isnull(AssetWatch.ObjectId,'00000000-0000-0000-0000-000000000000') as ObjectId, 
			isnull(AssetWatch.ObjectURL,'') as  ObjectURL
		FROM dbo.ALAudienceSegmentWatch SegmentWatch, dbo.ALWatch Watch   LEFT OUTER JOIN dbo.ALAssetWatch AssetWatch ON AssetWatch.WatchId =Watch .Id    
		where Watch .Id = SegmentWatch.WatchId and UserSegment.SegmentId = SegmentWatch.SegmentId for xml auto) as xml)WData  
		FROM #UserSegment UserSegment  for xml auto)  
  
  select  @Result  
  
  select US.SegmentId, ALPV.PageDefinitionId
		from ALAudienceSegmentPagesVisited ALPV 
			inner join #UserSegment US on ALPV.SegmentId = US.SegmentId
		where ALPV.IsEntry=0
 END
GO
PRINT 'Altering procedure Workflow_GetWorkflowsByActor'
GO
-- Copyright � 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 25-01-2007
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflowsByActor 
--			@ApplicationId	= uniqueidentifier null ,
--          @ObjectTypeId = 'int'  null,
--          @ActorId		uniqueidentifier ,
--          @ActorType		int,
--          @RoleId			int = null,
--          @OrderNo		int = null,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflowsByActor](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ActorId		uniqueidentifier ,
			@ActorType		int,
			@ApplicationId	uniqueidentifier = null,			
			@ProductId		uniqueidentifier = null,
			@ObjectTypeId	int = null,
			@RoleId			int = null,
			@OrderNo		int = null,
			@PageIndex		int = null,
			@PageSize		int = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint
			--@TotalRecords			bigint
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1;
	
		WITH ResultEntries AS
		(
            SELECT 
					ROW_NUMBER() OVER (ORDER BY A.Title) AS RowNumber,
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.IsGlobal,
					A.IsShared
            FROM	dbo.WFWorkflow A,
					dbo.WFSequence B
            WHERE	
					A.ObjectTypeId	=	Isnull(@ObjectTypeId,ObjectTypeId)		AND
					A.ApplicationId	=	Isnull(@ApplicationId, ApplicationId)	AND
					A.ProductId		=	Isnull(@ProductId, ProductId)			AND
					A.[Id]			=	B.WorkflowId							AND
					ActorId			=	@ActorId								AND 
					ActorType		=	@ActorType								AND
					RoleId			=	Isnull(@RoleId, RoleId)					AND
					[Order]			=	Isnull(@OrderNo, [Order])				AND
					A.Status = dbo.GetActiveStatus()
		)

		SELECT * ,FN.UserFullName CreatedByFullName,
MN.UserFullName ModifiedByFullName
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))

--		SELECT  @TotalRecords = COUNT(*)
--		FROM    ResultEntries

		--RETURN @TotalRecords
END
GO
PRINT 'Creating procedure Workflow_GetWorkflowsByActor'
GO
IF OBJECT_Id('SocialCredentials_Delete') IS NOT NULL
	DROP PROCEDURE SocialCredentials_Delete
GO
CREATE PROCEDURE [dbo].[SocialCredentials_Delete](
	@SocialMedia	nvarchar(1024) = NULL,
	@UserId			uniqueidentifier
)
AS
BEGIN
	DECLARE @SocialMediaId int
	SET @SocialMediaId = (SELECT Id FROM SMSocialMedia WHERE name like @SocialMedia AND IsActive = 1)

	DELETE FROM SMSocialCredentials 
	WHERE UserId = @UserId AND
	(@SocialMediaId IS NULL OR SocialMediaId = @SocialMediaId)
END
GO
PRINT 'Altering procedure SocialCredentials_Get'
GO
IF(OBJECT_ID('SocialCredentials_Get','P') IS NOT NULL)
	DROP PROCEDURE SocialCredentials_Get
GO
CREATE PROCEDURE [dbo].[SocialCredentials_Get](
	@SocialMedia	nvarchar(1024) = NULL,
	@UserId			uniqueidentifier = NULL,
	@UserIds		nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @SocialMediaId int
	SET @SocialMediaId = (SELECT Id FROM SMSocialMedia WHERE name like @SocialMedia AND IsActive = 1)

	DECLARE @tbIds TABLE(Id uniqueidentifier)
	IF @UserId IS NOT NULL
		INSERT INTO @tbIds SELECT @UserId
	
	IF @UserIds IS NOT NULL
		INSERT INTO @tbIds
		SELECT Items FROM dbo.SplitGUID(@UserIds, ',')
	
	SELECT  
		SocialMediaId,
		(SELECT name FROM SMSocialMedia WHERE Id = SocialMediaId) AS ChannelName,
		ExternalId,
		Token,
		Secret,
		UserId
	FROM SMSocialCredentials S
		JOIN @tbIds T ON T.Id = S.UserId
	WHERE (@SocialMediaId IS NULL OR SocialMediaId = @SocialMediaId)
END
GO
PRINT 'Updating location idenfifier in pagemap node'
GO
Update PageMapNode Set LocationIdentifier='Unassigned' where DisplayTitle = 'Unassigned'
and ParentId = SiteId
GO
PRINT 'Updating state abbrevations'
GO
UPDATE S Set S.StateCode = 'ID'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Idaho' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'ME'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Maine' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'DE'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Delaware' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NC'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='North Carolina' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'SD'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='South Dakota' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'PA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Pennsylvania' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'DC'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Washington DC' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Massachusetts' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'VA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Virginia' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'CO'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Colorado' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'CA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='California' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'HI'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Hawaii' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'KS'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Kansas' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'IL'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Illinois' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'KY'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Kentucky' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'WA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Washington' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'WV'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='West Virginia' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MI'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Michigan' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'OH'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Ohio' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'WI'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Wisconsin' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'FL'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Florida' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'AZ'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Arizona' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NJ'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='New Jersey' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'TN'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Tennessee' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'AK'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Alaska' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'IN'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Indiana' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'IA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Iowa' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NY'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='New York' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MN'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Minnesota' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'UT'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Utah' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'TX'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Texas' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'GA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Georgia' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'VT'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Vermont' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'WY'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Wyoming' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MO'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Missouri' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NH'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='New Hampshire' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NV'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Nevada' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'CT'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Connecticut' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'ND'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='North Dakota' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MD'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Maryland' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'AR'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Arkansas' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'OR'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Oregon' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MT'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Montana' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NM'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='New Mexico' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'MS'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Mississippi' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'LA'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Louisiana' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'RI'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Rhode Island' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'NE'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Nebraska' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'SC'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='South Carolina' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'OK'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Oklahoma' AND CountryId = C.Id and C.CountryCode ='US'
UPDATE S Set S.StateCode = 'AL'  FROM GLState S  INNER JOIN GLCountry C ON C.Id = S.CountryId  WHERE   State ='Alabama' AND CountryId = C.Id and C.CountryCode ='US'

GO
PRINT 'Altering procedureProduct_SearchReviews'
GO

ALTER PROCEDURE [dbo].[Product_SearchReviews](
									@ProductIdXml xml=null,
									@Status int=0,
									@PageNumber		int = 1,
									@PageSize		int = 10,
									@SortBy nvarchar(50) = null,	-- possible values : CreatedDate , email, ProductTitle, 
																	-- CommentTitle, ProductTypeName
									@StartDate datetime = null,
									@EndDate datetime = null,
									@Keyword nvarchar(255) = null,
									@ApplicationId uniqueidentifier,
									@VirtualCount	int output
									)
AS
	BEGIN
	
		if @SortBy is null
			set @SortBy = 'createddate desc'
		else
			set @SortBy = lower(@SortBy)	
			
		 IF @pageSize IS NULL  
				SET @PageSize = 2147483647  
				
		 IF @StartDate IS NULL
				SET @StartDate = cast('01-01-1900' as datetime)
		 IF @EndDate IS NULL  
				SET @EndDate = dbo.ConvertTimeFromUtc(GetUTCDate(),@ApplicationId)
		
		 
					
		Declare @nStartDate datetime 
		Declare @nEndDate datetime 
		set @nStartDate = cast(convert(varchar(12),@StartDate,101) as datetime) 
		set @nEndDate = cast(convert(varchar(12),@EndDate,101) as datetime) 
		set @nEndDate = DateAdd(d,1,@nEndDate)
		
		if @Keyword is not null
			set @Keyword = '%' + @Keyword + '%'
		
		Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)  
		    
		if(@ProductIdXml is not null)
			begin
				Insert Into @tempXml(ProductId)      
						Select tab.col.value('text()[1]','uniqueidentifier')      
						FROM @ProductIdXml.nodes('/ProductCollection/Product')tab(col)    
			end
      
		
		declare @tmpComments table(
							Id uniqueidentifier,
							ProductId uniqueidentifier,
							PostedBy nvarchar(1024),
							Email nvarchar(1024),
							CommentTitle nvarchar(255),
							Comments nvarchar(max),
							UserId uniqueidentifier,
							ParentId uniqueidentifier,
							Status int,
							CreatedDate datetime,
							ProductTitle nvarchar(255),
							ProductTypeId uniqueidentifier,
							ProductTypeName nvarchar(50),
							RatingValue int,
							RatingId uniqueidentifier
									  )
		
			if @Status=0
				begin
					INSERT INTO @tmpComments
						select  Com.Id,
								Com.ObjectId as ProductId,
								Com.PostedBy,
								Com.Email,
								Com.Title as CommentTitle,
								Com.Comments,
								Com.UserId,
								Com.ParentId,		
								Com.Status,
								dbo.ConvertTimeFromUtc(Com.CreatedDate,@ApplicationId),
								Pr.Title as ProductTitle,
								Pr.ProductTypeId,
								PrType.Title as ProductTypeName,
								R.RatingValue,
								R.Id
						from BLComments Com 
								inner join PRProduct Pr on Com.ObjectId = Pr.Id 
								inner join PRProductType PrType on Pr.ProductTypeId = PrType.Id
								left join BLCommentRating CR on Com.Id= CR.CommentId
								left join BLRatings R on R.Id = CR.RatingId
						where ( @Keyword is null or (Com.Comments like @Keyword or Com.Title like @Keyword or Com.PostedBy like @Keyword or Com.Email like @Keyword)) and
							  ( @ProductIdXml is null or Pr.Id in (Select ProductId from @tempXml)) and
								Com.Status  <> 3 and
								dbo.ConvertTimeFromUtc(Com.CreatedDate,@ApplicationId) between  @nStartDate and @nEndDate
				end
			else
				begin
					INSERT INTO @tmpComments
						select  Com.Id,
								Com.ObjectId as ProductId,
								Com.PostedBy,
								Com.Email,
								Com.Title as CommentTitle,
								Com.Comments,
								Com.UserId,
								Com.ParentId,		
								Com.Status,
								dbo.ConvertTimeFromUtc(Com.CreatedDate,@ApplicationId),
								Pr.Title as ProductTitle,
								Pr.ProductTypeId,
								PrType.Title as ProductTypeName,
								R.RatingValue,
								R.Id
						from BLComments Com 
								inner join PRProduct Pr on Com.ObjectId = Pr.Id 
								inner join PRProductType PrType on Pr.ProductTypeId = PrType.Id
								left join BLCommentRating CR on Com.Id= CR.CommentId
								left join BLRatings R on R.Id = CR.RatingId
						where ( @Keyword is null or (Com.Comments like @Keyword or Com.Title like @Keyword or Com.PostedBy like @Keyword or Com.Email like @Keyword)) and
							  ( @ProductIdXml is null or Pr.Id in (Select ProductId from @tempXml)) and
								Com.Status = @Status and
								dbo.ConvertTimeFromUtc(Com.CreatedDate,@ApplicationId) between  @nStartDate and @nEndDate
				
				end		
		


	Set @VirtualCount = @@ROWCOUNT

		IF @SortBy is null or (@SortBy)='createdDate desc'
			Begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CreatedDate desc) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CreatedDate DESC
			end
		else if (@sortBy) = 'createdDate asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CreatedDate ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CreatedDate ASC
			end
		else if (@sortBy) = 'Email asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by Email ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.Email ASC
			end
		else if (@sortBy) = 'Email desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by Email DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.Email DESC
			end
		else if (@sortBy) = 'PostedBy asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by PostedBy ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.Email ASC
			end
		else if (@sortBy) = 'PostedBy desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by PostedBy DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.PostedBy DESC
			end		
		else if (@sortBy) = 'ProductTitle asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTitle ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTitle ASC
			end
		else if (@sortBy) = 'ProductTitle desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTitle DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTitle DESC
			end		
		else if (@sortBy) = 'CommentTitle asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CommentTitle ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CommentTitle ASC
			end
		else if (@sortBy) = 'CommentTitle desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CommentTitle DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CommentTitle DESC
			end		
		else if (@sortBy) = 'ProductTypeName asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTypeName ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTypeName ASC
			end
		else if (@sortBy) = 'ProductTypeName desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTypeName DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTypeName DESC
			end		
	
END

GO
PRINT 'Altering procedure Ratings_Save'
GO
ALTER PROCEDURE [dbo].[Ratings_Save] 
(	
	@Id				uniqueidentifier = null OUT,
	@ObjectId		uniqueidentifier,
	@RatingValue	int,
	@IPAddress		varchar(100) = null,
	@Status			int,
	@CreatedBy		uniqueidentifier = null
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE         

          @Stmt     varchar(25),
          @Error    int,
		  @NewId	uniqueidentifier,
		  @Now 		datetime 	
		 

--********************************************************************************
-- code
--********************************************************************************
BEGIN
IF (@ObjectId IS NOT NULL)
BEGIN	
	SET @Now = getutcdate()
	Select @NewId =newid()	

	Insert into BLRatings (
		Id,
		ObjectId,
		RatingValue,
		IPAddress,
		Status,
		CreatedBy,
		CreatedDate 
	)
	Values
	(	
		@NewId,
		@ObjectId,
		@RatingValue,
		@IPAddress,
		@Status,
		@CreatedBy,
		@Now 
	)

	
	
	SELECT @Id = @NewId
END
ELSE IF (@Id IS NOT NULL)
BEGIN
UPDATE BLRatings Set RatingValue=@RatingValue Where Id=@Id
END	
SELECT @Error = @@ERROR
	IF @Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,@Stmt)
		RETURN dbo.GetDataBaseErrorCode()	
	END
END
GO
PRINT 'Altering procedure Product_GetReviews'
GO
ALTER PROCEDURE [dbo].[Product_GetReviews](
									@ProductTypeId uniqueidentifier = null,
									@Status int=0,
									@PageNumber		int = 1,
									@PageSize		int = 10,
									@SortBy nvarchar(50) = null,	-- possible values : CreatedDate , email, ProductTitle, 
																	-- CommentTitle, ProductTypeName
									@ApplicationId uniqueidentifier,
									@VirtualCount	int output
									)
AS
	BEGIN
	
			-- @Status = 3 -  deleted 
		if @SortBy is null
			set @SortBy = 'createddate desc'
		else
			set @SortBy = lower(@SortBy)	
			
		if @PageSize is null
			set @PageSize = 10
		
		declare @tmpComments table(
							Id uniqueidentifier,
							ProductId uniqueidentifier,
							PostedBy nvarchar(1024),
							Email nvarchar(1024),
							CommentTitle nvarchar(255),
							Comments nvarchar(max),
							UserId uniqueidentifier,
							ParentId uniqueidentifier,
							Status int,
							CreatedDate datetime,
							ProductTitle nvarchar(255),
							ProductTypeId uniqueidentifier,
							ProductTypeName nvarchar(50),
							RatingValue int,
                            RatingId uniqueidentifier

									  )
		
		if(@Status = 0)
			Begin
				INSERT INTO @tmpComments
					select  Com.Id,
						Com.ObjectId as ProductId,
						Com.PostedBy,
						Com.Email,
						Com.Title as CommentTitle,
						Com.Comments,
						Com.UserId,
						Com.ParentId,		
						Com.Status,
						dbo.ConvertTimeFromUtc(Com.CreatedDate,@ApplicationId),
						Pr.Title as ProductTitle,
						Pr.ProductTypeId,
						PrType.Title as ProductTypeName,
						R.RatingValue,
                        R.Id
				from BLComments Com 
						inner join PRProduct Pr on Com.ObjectId = Pr.Id 
						inner join PRProductType PrType on Pr.ProductTypeId = PrType.Id
						left join BLCommentRating CR on Com.Id= CR.CommentId
                        left join BLRatings R on R.Id = CR.RatingId
				where --Pr.ProductTypeId = isnull(@ProductTypeId, Pr.ProductTypeId) and 
				(@ProductTypeId is null or Pr.ProductTypeId = @ProductTypeId) and
				Com.Status <> 3
			end
		else
			Begin
				INSERT INTO @tmpComments
						select  Com.Id,
							Com.ObjectId as ProductId,
							Com.PostedBy,
							Com.Email,
							Com.Title as CommentTitle,
							Com.Comments,
							Com.UserId,
							Com.ParentId,		
							Com.Status,
							dbo.ConvertTimeFromUtc(Com.CreatedDate,@ApplicationId),
							Pr.Title as ProductTitle,
							Pr.ProductTypeId,
							PrType.Title as ProductTypeName,
							R.RatingValue,
							R.Id
					from BLComments Com 
							inner join PRProduct Pr on Com.ObjectId = Pr.Id 
							inner join PRProductType PrType on Pr.ProductTypeId = PrType.Id
							left join BLCommentRating CR on Com.Id= CR.CommentId
                        left join BLRatings R on R.Id = CR.RatingId
					where --Pr.ProductTypeId = isnull(@ProductTypeId, Pr.ProductTypeId) and 
						(@ProductTypeId is null or Pr.ProductTypeId = @ProductTypeId) and
					Com.Status = @Status
			End	
		

	Set @VirtualCount = @@ROWCOUNT

		IF @SortBy is null or (@SortBy)='createdDate desc'
			Begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CreatedDate desc) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CreatedDate DESC
			end
		else if (@sortBy) = 'createdDate asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CreatedDate ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CreatedDate ASC
			end
		else if (@sortBy) = 'Email asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by Email ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.Email ASC
			end
		else if (@sortBy) = 'Email desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by Email DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.Email DESC
			end
		else if (@sortBy) = 'PostedBy asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by PostedBy ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.Email ASC
			end
		else if (@sortBy) = 'PostedBy desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by PostedBy DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.PostedBy DESC
			end		
		else if (@sortBy) = 'ProductTitle asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTitle ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTitle ASC
			end
		else if (@sortBy) = 'ProductTitle desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTitle DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTitle DESC
			end		
		else if (@sortBy) = 'CommentTitle asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CommentTitle ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CommentTitle ASC
			end
		else if (@sortBy) = 'CommentTitle desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by CommentTitle DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.CommentTitle DESC
			end		
		else if (@sortBy) = 'ProductTypeName asc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTypeName ASC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTypeName ASC
			end
		else if (@sortBy) = 'ProductTypeName desc'
			begin
				Select A.* 
				FROM
					(
					Select *,  ROW_NUMBER() OVER(Order by ProductTypeName DESC) as RowNumber
					 from @tmpComments
					) A
				where A.RowNumber >= (@PageSize * @PageNumber) - 
									(@PageSize -1) AND A.RowNumber <= @PageSize * @PageNumber
				Order by A.ProductTypeName DESC
			end		
	
END
