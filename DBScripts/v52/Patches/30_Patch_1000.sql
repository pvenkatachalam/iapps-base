﻿--Patch script for 5.1.0726.01
GO
PRINT 'Updating the version of the products to 5.1.0823.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.0823.1'
GO
PRINT 'Creating a index for RPTCommerceReport_Category.(Id,NavNodeId)'
GO

IF (OBJECT_ID('COTaxonomy_bkup_08232014') IS NULL)
	SELECT * into COTaxonomy_bkup_08232014 from COTaxonomy

GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='IX_RPTCommerceReport_Category_ID_NavNodeId' AND object_id = OBJECT_ID('RPTCommerceReport_Category'))
	CREATE NONCLUSTERED INDEX [IX_RPTCommerceReport_Category_ID_NavNodeId] ON dbo.RPTCommerceReport_Category([Id] ASC,[NavNodeId] ASC) 
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunHistory]', N'UniqueOpens') IS NULL
	ALTER TABLE MKCampaignRunHistory ADD [UniqueOpens] INT  NOT NULL Default 0

GO
PRINT 'Altering procedure Product_SimpleSearchSKU'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  ALTER PROCEDURE [dbo].[Product_SimpleSearchSKU]
    (
      @SearchTerm NVARCHAR(MAX) ,
      @IsBundle TINYINT = NULL ,
      @pageNumber INT = NULL ,
      @pageSize INT = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @ProductTypeId UNIQUEIDENTIFIER = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN  
        DECLARE @PageLowerBound BIGINT ,
            @PageUpperBound BIGINT ,
            @SkuCount BIGINT

        SET @PageLowerBound = ISNULL(@pageSize, 0) * ISNULL(@pageNumber, 0)
        SET @PageUpperBound = @PageLowerBound - ISNULL(@pageSize, 0) + 1 ;

        DECLARE @searchText NVARCHAR(MAX)  
        SET @searchText = '%' + @SearchTerm + '%'
        IF @SortColumn IS NULL 
            SET @SortColumn = 'productname asc'
        SET @SortColumn = LOWER(@SortColumn) ;
        WITH    PagingCTE ( Pages, RowNum, SKUId, ProductId, ProductName, Qty, AllocatedQty, ProductTypeName, SKUTitle, SKU, IsUnlimitedQuantity, AvailableForBackOrder, MaximumDiscountPercentage, BackOrderLimit, ProductActive, SKUActive )
                  AS ( SELECT   COUNT(*) OVER ( ) AS Pages ,
                                ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN @SortColumn = 'productname desc'
                                                              THEN P.Title
                                                             END DESC , CASE
                                                              WHEN @SortColumn = 'productname asc'
                                                              THEN P.Title
                                                              END ASC , CASE
                                                              WHEN @SortColumn = 'sku desc'
                                                              THEN S.SKU
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'sku asc'
                                                              THEN S.SKU
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'quantity desc'
                                                              THEN I.Quantity
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'quantity asc'
                                                              THEN I.Quantity
                                                              END ASC , CASE
                                                              WHEN @SortColumn = 'availabletosell desc'
                                                              THEN A.Quantity
                                                              END DESC , CASE
                                                              WHEN @SortColumn = 'availabletosell asc'
                                                              THEN A.Quantity
                                                              END ASC ) AS RowNum ,
                                S.Id AS SKUId ,
                                P.Id AS ProductId ,
                                P.Title AS ProductName ,
                                I.Quantity ,
                                ISNULL(A.Quantity, 0) ,
                                T.Title AS ProductTypeName ,
                                S.Title AS SKUTitle ,
                                S.SKU ,
                                IsUnlimitedQuantity ,
                                AvailableForBackOrder ,
                                MaximumDiscountPercentage ,
                                S.BackOrderLimit ,
                                P.IsACtive AS ProductActive ,
                                S.IsActive AS SKUActive
                       FROM     PRProduct P
                                INNER JOIN PRProductSku S ON S.ProductId = P.Id
                                INNER JOIN PRProductType T ON T.Id = P.ProductTypeId
                                LEFT JOIN vwInventoryPerSite I ON I.SkuId = S.Id
                                                              AND I.SiteId = @ApplicationId
                                LEFT JOIN vwAvailableToSellPerWarehouse A ON A.SkuId = S.Id
                                                              AND A.SiteId = @ApplicationId
                       WHERE    --P.SiteId = @ApplicationId AND
                                ( @ProductTypeId IS NULL
                                  OR T.Id = @ProductTypeId
                                )
                                AND ( @IsBundle IS NULL
                                      OR P.IsBundle = @IsBundle
                                    )
                                AND ( P.Title LIKE @searchText
                                      OR P.ShortDescription LIKE @searchText
                                      OR P.LongDescription LIKE @searchText
                                      OR P.Description LIKE @searchText
                                      OR S.SKU LIKE @searchText
                                    )
                     )
            SELECT  Pages ,
                    RowNum ,
                    SKUId ,
                    ProductId ,
                    ProductName ,
                    ISNULL(dbo.GetBundleQuantityPerSite(SKUId, @ApplicationId),
                           Qty) AS Qty ,
                    AllocatedQty ,
                    ProductTypeName ,
                    SKUTitle ,
                    SKU ,
                    IsUnlimitedQuantity ,
                    AvailableForBackOrder ,
                    MaximumDiscountPercentage ,
                    BackOrderLimit ,
                    ProductActive ,
                    SKUActive
            INTO    #tbSku
            FROM    PagingCTE
            WHERE   ( RowNum BETWEEN @PageUpperBound
                             AND     @PageLowerBound ) 
		
--SELECT * FROM #tbSku
        SELECT  T.SKUId AS ID ,
                T.ProductId ,
                T.ProductName ,
                T.Qty AS Quantity ,
                T.AllocatedQty AS AvailableToSell ,
                T.SKUTitle ,
                T.SKU ,
                CASE WHEN T.Qty >= 999999 THEN 1
                     ELSE T.IsUnlimitedQuantity
                END IsUnlimitedQuantity ,
                T.AvailableForBackOrder ,
                T.MaximumDiscountPercentage ,
                ISNULL(T.BackOrderLimit, 0) AS BackOrderLimit ,
                T.AvailableForBackOrder ,
                CAST(CASE WHEN T.SKUActive = 1
                               AND T.ProductActive = 1 THEN 1
                          ELSE 0
                     END AS BIT) AS IsActive ,
                T.ProductTypeName
        FROM    #tbSku T
               
		
        DECLARE @activestatus INT = dbo.GetActiveStatus()

        SELECT  C.[Id] ,
                C.[Title] ,
                C.[Description] ,
                C.[FileName] ,
                C.[Size] ,
                C.[Attributes] ,
                C.[RelativePath] ,
                C.[AltText] ,
                C.[Height] ,
                C.[Width] ,
                C.[Status] ,
                C.[SiteId] ,
                OI.[ObjectId] AS ProductId ,
                OI.[ObjectType] ,
                OI.[IsPrimary] ,
                OI.[Sequence] ,
                C.[ParentImage] ,
                C.ImageType ,
                OI.ObjectId AS ProductId
        FROM    #tbSku T
                INNER JOIN CLObjectImage OI ON OI.ObjectType = 205
                                               AND T.ProductId = OI.ObjectId
                INNER JOIN CLImage I ON I.Id = OI.ImageId
                INNER JOIN CLImage C ON i.status = @activestatus
                                        AND i.id IS NOT NULL
                                        AND I.Id = C.ParentImage
                INNER JOIN CLImageType IT ON IT.TypeNumber = 0
                                             AND IT.Id = C.ImageType
        UNION ALL
        SELECT  C.[Id] ,
                C.[Title] ,
                C.[Description] ,
                C.[FileName] ,
                C.[Size] ,
                C.[Attributes] ,
                C.[RelativePath] ,
                C.[AltText] ,
                C.[Height] ,
                C.[Width] ,
                C.[Status] ,
                C.[SiteId] ,
                OI.[ObjectId] AS ProductId ,
                OI.[ObjectType] ,
                OI.[IsPrimary] ,
                OI.[Sequence] ,
                C.[ParentImage] ,
                C.ImageType ,
                OI.ObjectId AS ProductId
        FROM    #tbSku T
                INNER JOIN CLObjectImage OI ON OI.ObjectType = 205
                                               AND T.ProductId = OI.ObjectId
                INNER JOIN CLImage I ON I.Id = OI.ImageId
                INNER JOIN CLImage C ON i.status = @activestatus
                                        AND i.id IS NOT NULL
                                        AND I.Id = C.Id
                INNER JOIN CLImageType IT ON IT.TypeNumber = 0
                                             AND IT.Id = C.ImageType
        ORDER BY Sequence--, Title
 
	

        SELECT TOP 1
                Pages
        FROM    #tbSku
    END
GO
PRINT 'Altering procedure Coupon_GetCouponForOrderId'
GO
ALTER PROCEDURE [dbo].[Coupon_GetCouponForOrderId]      
(      
 @Id uniqueidentifier = null
 ,@ApplicationId uniqueidentifier=null
)      
as      
begin   
with CTEAppliedDiscount AS
(
select CC.CouponId,sum(OI.Discount) Discount
from OROrderItemCouponCode OIC
INNER JOIN OROrderItem OI on OIC.OrderItemId = OI.Id
INNER JOIN CPCouponCode CC on OIC.CouponCodeId =CC.Id
Where OI.OrderId=@Id AND OI.Status !=3
Group By CC.CouponId
)   
 SELECT C.[ID],      
 C.[Title],      
 C.[Description],      
 C.CreatedDate,  
 C.[CreatedBy],      
 C.ModifiedDate,  
 C.[ModifiedBy],      
 C.[Status],      
 C.[IsGenerated],      
 C.[AutoApply],
 C.[MaxUses],      
 C.[MaxUsesPerCustomer],      
 C.[CouponTypeId],      
 dbo.ConvertTimeFromUtc(C.[StartDate],@ApplicationId)StartDate,  
 dbo.ConvertTimeFromUtc(C.[EndDate],@ApplicationId)EndDate,  
 C.[CouponValue],      
 C.[MaxValue],      
 C.[MinPurchase],      
 C.[ShippingOptionId],    
 CT.Title As CouponType,    
    --OS.Name as ShippingOptionName,    
 dbo.GetUsedCouponCount(C.ID) As Used,    
 (SELECT Top 1 Code FROM CPCouponCode CC Where CC.CouponId = C.ID)  As Code,    
C.Stackable, 
		C.RestrictionTypeId , 
		dbo.GetCouponAttachedId(C.Id,C.RestrictionTypeId) As AttachedIds,
		AD.Discount AppliedDiscount
   FROM CPCoupon C  
	left join CPCouponCode CC on CC.CouponId=C.ID
	inner join CPOrderCouponCode OCC on OCC.CouponCodeId=CC.Id  
   Left Join CPCouponType CT ON CT.Id = C.CouponTypeId    
   Left Join CTEAppliedDiscount AD ON AD.CouponId = C.ID
   --Left Join ORShippingOption OS ON OS.Id = C.ShippingOptionId    
   Where OCC.OrderId=@Id
end
GO
PRINT 'Altering procedure CampaignRunHistory_GetAggregatesByCampaign'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetAggregatesByCampaign]
(
	@CampaignId uniqueidentifier
)
AS
Begin

	Select 
		isnull(sum(Sends),0) as TotalSends
		,isnull(sum(Delivered),0) as TotalDelivered
		,isnull(sum(Opens),0)	as TotalOpens
		,isnull(sum(Clicks),0) as TotalClicks
		,isnull(sum(Unsubscribes),0) as TotalUnsubscribes
		,isnull(sum(Bounces),0) as TotalBounces
		,isnull(sum(TriggeredWatches),0)	as TotalTriggeredWatches
		,isnull(sum(UniqueOpens),0)	as TotalUniqueOpen -- This will cause performance issue, we can keep this in a column in this table.

	From MKCampaignRunHistory
	Where CampaignId = @CampaignId

End
GO
PRINT 'Altering procedure CampaignRunHistory_GetByCampaign'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetByCampaign]
(
	@CampaignId uniqueidentifier,
	@CampaignRunId uniqueidentifier,
    @PageNumber int =null,
    @PageSize int =null,
	@SortBy nvarchar(50) = 'RunDate desc',  
	@TotalRecords int  =null out,
	@ApplicationId uniqueidentifier 
)
AS
Begin
IF @PageNumber is null 
	BEGIN
	Select 
		Id
		,CampaignId
		,dbo.ConvertTimeFromUtc(RunDate,@ApplicationId)RunDate 
		,Sends
		,Delivered
		,Opens
		,Clicks
		,Unsubscribes
		,Bounces
		,TriggeredWatches
		,EmailSubject
		,SenderName
		,SenderEmail
		,CostPerEmail
		,ConfirmationEmail
		,EmailHtml
		,CMSPageId
		,EmailText
		,UniqueOpens as UniqueOpen
	From MKCampaignRunHistory
	Where CampaignId = @CampaignId
	AND Id =isnull( @CampaignRunId,Id)
	END
	ELSE
	BEGIN

 if @SortBy is null
			set @SortBy = 'RunDate desc'
			Declare @sql varchar(max)

			Set @sql='
			   With tmpContacts
			   AS (
					Select
					  Id
		,CampaignId
		,dbo.ConvertTimeFromUtc(RunDate,''' + cast(@ApplicationId as varchar(36)) + ''')RunDate 
		,Sends
		,Delivered
		,Opens
		,Clicks
		,Unsubscribes
		,Bounces
		,TriggeredWatches
		,EmailSubject
		,SenderName
		,SenderEmail
		,CostPerEmail
		,ConfirmationEmail
		,EmailHtml
		,CMSPageId
		,EmailText
		,UniqueOpens As UniqueOpen
	From MKCampaignRunHistory
	Where CampaignId ='''+ cast( @CampaignId as varchar(36)) + ''')
		
				
				Select  A.*   
					FROM  
					 (  
					 Select *,  ROW_NUMBER() OVER(Order by ' + @SortBy + '
												) as RowNumber  
					  from tmpContacts  
					 ) A  
					where A.RowNumber >=' +  cast((@PageSize * @PageNumber) - (@PageSize -1 ) as varchar(10))  + 
						' AND A.RowNumber <=' + cast((@PageSize * @PageNumber) as varchar(10)) 
				 exec(@sql)

	Set @TotalRecords =(Select count(*)  From MKCampaignRunHistory
	Where CampaignId = @CampaignId)

	END

End
GO
PRINT 'Altering procedure EmailStatsRollup_RunHistory'
GO
ALTER PROCEDURE [dbo].[EmailStatsRollup_RunHistory]
As
Begin
	Declare @RunSummary Table
	(
		CampaignRunId uniqueidentifier,
		Sends INT,
		Bounces int,
		Opens int,
		Clicks int,
		TriggeredWatches int,
		Unsubscribes int,
		UniqueOpens int
	)

	Insert Into @RunSummary (CampaignRunId, Sends,Bounces, Opens, Clicks, TriggeredWatches, Unsubscribes, UniqueOpens)
	Select CampaignRunId, COUNT(*), Sum(Convert(int, Bounced)) 'Bounces', Sum(Opens) 'Opens', 
		Sum(Clicks) 'Clicks', Sum(TriggeredWatches) 'TriggeredWatches', 
		Sum(Convert(int, Unsubscribed)) 'Unsubscribes',
		[dbo].[GetUniqueOpenInCampaignMail](CampaignRunId)
	from MKEmailSendLog
	Group By CampaignRunId

	Update MKCampaignRunHistory SET
		Sends = r.Sends,
		Delivered = r.Sends - r.Bounces,
		Bounces = r.Bounces,
		Opens = r.Opens,
		Clicks = r.Clicks,
		TriggeredWatches = r.TriggeredWatches,
		Unsubscribes = r.Unsubscribes,
		UniqueOpens = r.UniqueOpens
	From MKCampaignRunHistory h
		Join @RunSummary r on r.CampaignRunId = h.Id

	Delete MKCampaignRunHistoryWatchStats

	Insert Into MKCampaignRunHistoryWatchStats (Id, CampaignRunId, WatchId, TriggerCount, TriggerPercent)
	Select NewId() 'Id', l.CampaignRunId, a.TargetId 'WatchId', Count(a.TargetId), 0
	From MKEmailSendActions a
		Join MKEmailSendLog l on a.SendId = l.Id
	Where a.ActionType = 5 AND a.TargetId in (Select Id from ALWatch)
	Group By l.CampaignRunId, a.TargetId

	Update MKCampaignRunHistoryWatchStats Set
		TriggerPercent = 
			Case
				When h.Delivered = 0 Then 0
			Else
				Convert(Decimal(8,3), Convert(decimal(16,3), s.TriggerCount) / Convert(decimal(16,3), h.Delivered) * 100)
			End
	From MKCampaignRunHistoryWatchStats s
		Join MKCampaignRunHistory h on h.Id = s.CampaignRunId
End

GO