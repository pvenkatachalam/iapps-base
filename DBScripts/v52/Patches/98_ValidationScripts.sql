-- Contact count
Declare @result varchar(500) 
SET nocount ON
select @result = 'Info:  Verification Test - Contact Count -' + cast(count(*) as varchar(10)) from vw_contacts
SELECT  @result Result
PRINT @result
GO
--Contact Properties Migration Check
Declare @result varchar(500) 
SET nocount ON
SELECT  @result = ( CASE WHEN COUNT(*) = 0
                         THEN 'Success: Verification Test - Contact Properties Migration Successful'
                         ELSE 'Fail: Verification Test - Contact Properties Migration Failed'
                    END )
FROM    dbo.USMemberProfile UP
        INNER JOIN dbo.MKContact C ON C.Id = UP.UserId
        LEFT JOIN ( SELECT  A.Title AS Title ,
                            A.Id AS AttributeId ,
                            CAV.Value AS VALUE ,
                            CAV.ContactId
                    FROM    dbo.ATAttribute A
                            INNER JOIN dbo.ATContactAttributeValue CAV ON CAV.AttributeId = A.Id
                  ) TV ON TV.ContactId = UP.UserId AND TV.ContactId = C.Id
                          AND TV.VALUE = UP.PropertyValueString
                          AND TV.Title = UP.PropertyName
WHERE   TV.AttributeId IS NULL
                        
                        
SELECT  @result Result
PRINT @result
GO
-- --Contact  Migration Check
Declare @count bigint
Declare @result varchar(500)
set nocount on
SET @count = (select count(*) from USUser Where Description='Migrated to MKContact Table')
SET @count = @Count - (select count(*) from MKContact Where CreatedDate ='2014-07-29 00:00:00.000'
)
select @result=( case when @count=0 then 'Success: Verification Test - Contact Migration successful' else 'Fail: Verification Test - Contact Migration Failed' end )
Select @result Result
print @result
GO
-- Campaign SiteId Check
Declare @result varchar(500)
set nocount on
select @result=(case when count(*) =0 then 'Success: Verification Test - Campaign SiteId Successful' else 'Fail: Verification Test - Campaign SiteId Failed' end )
from MKCampaign Where ApplicationId is null
Select @result Result
print @result
GO

-- Campaign Users to Disctribution list
set nocount on

if not exists(
select count(distinct UserId) from Migration52_bckup_MKCampaignuser
Group by CampaignId
except
Select  count(distinct UserId) from TADistributionListUser LU
INNER JOIN TADistributionLists L on LU.DistributionListId =L.Id
Where  L.Title like 'Migrated List For %'
Group By DistributionListId
)
BEGIN
select 'Success: Verification Test - Migration of Campaign users to Distributionlist successful' Result
print 'Success: Verification Test - Migration of Campaign users to Distributionlist successful'
END
else
BEGIN
select 'Fail: Verification Test - Migration of Campaign users to Distributionlist failed' Result
print 'Fail: Verification Test - Migration of Campaign users to Distributionlist failed'
END
GO


-- Validation for Taxonomy Migration 
IF EXISTS(Select Id FROM SISIte where ParentSIteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000') --if variant site exist
BEGIN
	IF EXISTS (Select 1 from COTaxonomy where ApplicationId <>SiteId)
	BEGIN
		Select 'Fail: Verification Test - Migration of Taxonomy from Variant Site to Master Site failed' AS Result
		Print 'Fail: Verification Test - Migration of Taxonomy from Variant Site to Master Site failed' 
	END	
	ELSE
	BEGIN
		Select 'Success: Verification Test - Migration of Taxonomy from Variant Site to Master Site successful' AS Result
		Print 'Success: Verification Test - Migration of Taxonomy from Variant Site to Master Site successful' 
	END
END
ELSE
BEGIN
	Select 'Success: Verification Test - No Varaint site available to migrate data for Taxonomy' AS Result
	Print 'Success: Verification Test - No Varaint site available to migrate data for Taxonomy' 
END
GO

-- Validation for Taxonomy Default data for variant site
IF EXISTS (Select Id FROM SISIte where ParentSIteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000') -- if variant site exists
BEGIN
	IF NOT EXISTS(SELECT 1 FROM COTaxonomy WHERE ApplicationId IN
	(Select Id FROM SISIte where ParentSIteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000') 
	and Title = 'Index Terms'
	)
	BEGIN
		Select 'Fail: Verification Test - Default Data for Taxonomy in Variant Site creation failed' AS Result
		Print 'Fail: Verification Test - Default Data for Taxonomy in Variant Site creation failed' 
	END	
	ELSE
	BEGIN
		Select 'Success: Verification Test - Default Data for Taxonomy in Variant Site creation successful' AS Result
		Print 'Success: Verification Test - Default Data for Taxonomy in Variant Site creation successful' 
	END
END
ELSE
BEGIN
		Select 'Success: Verification Test - No Varaint site available to create default Data for Taxonomy' AS Result
		Print 'Success: Verification Test - No Varaint site available to create default Data for Taxonomy' 
END

GO

--Getting information about the scheduled camapigns that past 
Declare @result varchar(500) ,@result1 varchar(500) 
SET nocount ON

select @result = 'Info:  Verification Test - Pending scheduled campaign Count BEFORE-' + cast(count(Distinct C.Id) as varchar(34)) from MKCamapign_V52_BKUP_11_21_2014 C JOIN MKCampaignAdditionalInfo AI ON C.Id = AI.CampaignId
JOIN TASchedule S ON AI.ScheduleId = S.Id JOIN MKCampaignRunHistory H ON C.Id = H.CampaignId
where C.Status=2 and H.RunDate >= S.LastRunTime

select @result1 = ' AFTER-' + cast(count(Distinct C.Id) as varchar(34)) from MKCampaign C JOIN MKCampaignAdditionalInfo AI ON C.Id = AI.CampaignId
JOIN TASchedule S ON AI.ScheduleId = S.Id JOIN MKCampaignRunHistory H ON C.Id = H.CampaignId
where C.Status=2 and H.RunDate >= S.LastRunTime

SELECT  @result+@result1 Result
PRINT @result+@result1
GO

Declare @result varchar(500)
Declare @sum1 int,@sum2 int
SET nocount ON
SET @sum1 = (Select Count(*)
FROM	MKCampaign AS C
				LEFT JOIN MKCampaignAdditionalInfo AS A ON C.Id = A.CampaignId
				LEFT JOIN TASchedule AS S ON S.Id = A.ScheduleId
Where 
(C.Status = 2 AND
					S.NextRunTime < GETUTCDATE() AND
					(ISNULL(S.MaxOccurrence, 0) <= 0 OR S.RunCount < S.MaxOccurrence)) OR
				 C.Status = 7 or C.Status=5)

SET @sum2 = (Select Count(*)
From MKCampaign C
		Join MKCampaignAdditionalInfo a on c.Id = a.CampaignId
		JOIN [dbo].[TASchedule] S ON S.Id = a.ScheduleId
  Where 
	((C.Status = 2 and NextRunTime <= GETUtcDate() 
	And (isnull(s.MaxOccurrence, 0) <= 0 or s.RunCount < s.MaxOccurrence)) or C.Status = 7)	)

IF @sum1 <> @sum2
	SET @result ='Fail: Campaign to run count does not match. This means old campaings emails go out.'
ELSE
	SET @result ='Success: Campaign to run count match'

Select @result
print @result
