﻿
GO
------------------------------------------------START Migrating contacts associated directly to campaign TO contact list --------------------------------



declare @tabCampaigns as table(CampaignId uniqueidentifier, ApplicationId uniqueidentifier,CampaignTitle nvarchar(max) ,RowNum INT identity(1,1))
declare @RowCount INT,@CampaignId uniqueidentifier,@AppId uniqueidentifier,@ListGroupId uniqueidentifier,@CampaignTitle nvarchar(max),@iAppsUserId uniqueidentifier

Print 'Select iappsuser for modified by'
select TOP 1 @iAppsUserId =  Id FROM USUser where UserName='iAppsUser' OR UserName='iAppsSystemUser'

Print 'Selecting the campaigns those have direct users'

--Take back up of MKCampaignUser
if(object_id('Migration52_bckup_MKCampaignuser') is null)
begin
	select * into Migration52_bckup_MKCampaignuser from MKCampaignuser
end

insert into @tabCampaigns
select Distinct	CampaignId,Applicationid,Title
from MKCampaignuser CU JOIN MKCampaign C ON CU.CampaignId = C.Id

--select * from @tabCampaigns


SELECT @RowCount = ISNULL(MAX(RowNum),0) FROM @tabCampaigns

PRINT @RowCount
--creates the contaact list group if there is something to be migrated

WHILE (@RowCount > 0)
		BEGIN
		PRINT 'Entering loop'

			SELECT @CampaignId = CampaignId,@AppId = ApplicationId,@CampaignTitle='Migrated List For '+ISNULL(CampaignTitle,'') FROM @tabCampaigns WHERE [RowNum]=@RowCount
			
			select @ListGroupId = Id from TADistributionGroup where Title='Migrated List Group' and ApplicationId=@AppId
			
			If (@ListGroupId is null)
			begin
				PRINT 'Creating contact list group'
				EXEC ContactListGroupDto_Save @Id=@ListGroupId out ,@Title='Migrated List Group',@Description='', @Status=1,@Applicationid=@AppId,@IsSystem=0 
			End
 
			--create a contact list
			PRINT 'Creating contact list'
			declare @contactListId uniqueidentifier
			EXEC ContactListDto_Save @Id=@contactListId out,@Title=@CampaignTitle,@SiteId=@AppId,@ContactListGroupId=@ListGroupId,@ListType=0,@TrackingId=NULL,@ModifiedBy=@iAppsUserId,@Status=1,@IsGlobal=0
			
			if(@contactListId is not null)
			begin
				PRINT 'Adding users to contact list'
				
				insert into TADistributionListUser (Id,
				DistributionListId,
				UserId,
				ContactListSubscriptionType)
				select NEWID(),* FROM (Select Distinct @ContactListId ContactListId,UserId,1 Type from MKCampaignUser where CampaignId=@CampaignId )T

				PRINT 'Deleting users from campaign user'
				--cleatrs campaignuser table
				delete from MKCampaignUser where CampaignId=@CampaignId
			end

			SET @contactListId = NULL
			
			SET @RowCount = @RowCount - 1
		END
------------------------------------------------ END Migrating contacts associated directly to campaign TO contact list --------------------------------
GO

