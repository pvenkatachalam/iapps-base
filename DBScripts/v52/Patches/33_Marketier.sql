﻿--Cleanup
--
GO
--
PRINT 'Updating the product version as 5.2.0.0'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.2.0.0'
GO

PRINT 'Creating template data for Lead Submission Notification'
GO
IF not exists (select 1 from TAEmailTemplate where Title='Lead Submission Notification')
begin
DECLARE 
	@SiteId UNIQUEIDENTIFIER,
	@TemplateId UNIQUEIDENTIFIER,
	@SettingTypeId INT,
	@Sequence INT

SET @SiteId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

INSERT INTO [TAEmailTemplate]
           ([Id]
           ,[Title]
           ,[Description]
           ,[Subject]
           ,[MimeType]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[Status]
           ,[ApplicationId]
           ,[SenderDefaultEmail]
           ,[MailMergeTags]
           ,[ProductId]
           ,[Body]
           ,[Key])
     VALUES
           (NEWID()
           ,'Lead Submission Notification'
           ,'Lead Submission Notification'
           ,'New Lead Submission'
           ,2
           ,(select Id from USUser where username like 'iappssystemuser')
           ,GETDATE()
           ,NULL
           ,NULL
           ,1
           ,@SiteId
           ,NULL
           ,NULL
           ,(select Id from iAppsProductSuite where Title='Marketier')
           ,'<table style="background-color: #F9F9F9; border: 1px solid #DDDDDD; width: 450px; max-width: 600px; color: #323232; font-family: Franklin Gothic medium, sans-serif; font-size: 1.1em; border-collapse: collapse;" cellpadding="15">      <tr style="padding: 3%; border-bottom: 1px solid #DDDDDD;">        <td>     <div>New submission on form:</div>     <div style="font-size: 1.2em;">[FORMTITLE]</div>    </td>   </tr>   [DYNAMICBODY]   <tr>    <td ></td>   </tr>   <tr>    <td style="font-size: 0.8em;">     <a href="[CONTACTURL]">Click to View Contact</a>    </td>      </tr>    </table>'
           ,'NewLeadSubmitted')

end
GO

PRINT 'Creating site settings.Name:OverrideiAPPSServiceCheck, Value: true '
GO

IF NOT EXISTS (SELECT * FROM STSettingType Where Name = 'OverrideiAPPSServiceCheck')
INSERT INTO dbo.STSettingType
        ( Name ,
          FriendlyName ,
          SettingGroupId ,
          IsSiteOnly ,
          Sequence
        )
        SELECT  N'OverrideiAPPSServiceCheck',
				N'Override iAPPS Service Check',
				1,
				1,
				MAX(Sequence)+ 1
				
        FROM    STSettingType  
GO


--SITE SETTINGS
DECLARE @SiteId UNIQUEIDENTIFIER,@SettingTypeId INT,@Sequence INT

DECLARE SITE_CURSOR_ Cursor 
FOR
SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId -- Only Master sites

Open SITE_CURSOR_ 
Fetch NEXT FROM SITE_CURSOR_ INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN

--Insert default value for OverrideiAPPSServiceCheck for every site. SettingType Created in the default data script.
    SET @SettingTypeId = ( SELECT TOP 1
                                    Id
                           FROM     STSettingType
                           WHERE    name = 'OverrideiAPPSServiceCheck'
                         )    	
    IF @SettingTypeId IS NOT NULL 
        BEGIN
            DECLARE @AlreadyExists BIT
            SET @AlreadyExists = ( SELECT TOP 1
                                            Id
                                   FROM     STSiteSetting
                                   WHERE    SiteId = @SiteId
                                            AND SettingTypeId = @SettingTypeId
                                 )

            IF @AlreadyExists IS NULL 
                BEGIN
                    INSERT  INTO STSiteSetting
                            ( SiteId, SettingTypeId, Value )
                    VALUES  ( @SiteId, @SettingTypeId, 'true' )      
                END	
        END
		
		PRINT 'Creating stite setting. Name:EmailTemplateForLeadFormSubmitted, Value:(template id of NewLeadSubmitted)'

	   SET @SettingTypeId =(SELECT TOP 1 Id FROM STSettingType WHERE name ='EmailTemplateForLeadFormSubmitted')
       IF NOT EXISTS(SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
       BEGIN
              IF @SettingTypeId IS NULL
              BEGIN
                     SELECT @Sequence =MAX(sequence)FROM dbo.STSettingType
                     INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
                     VALUES('EmailTemplateForLeadFormSubmitted','EmailTemplateForLeadFormSubmitted',1, 1, @Sequence)
              END
   
              SET @SettingTypeId =ISNULL(@SettingTypeId,@@Identity)
              INSERT INTO dbo.STSiteSetting( SiteId, SettingTypeId, Value )
              VALUES  ( @SiteId,-- SiteId - uniqueidentifier
                             @SettingTypeId,-- SettingTypeId - int
                             (SELECT TOP 1 Id FROM TAEmailTemplate WHERE [Key] = 'NewLeadSubmitted')  -- Value - nvarchar(max)
                             )   
             
       END

	   PRINT 'Creating stite setting. Name:AssetImage.OptimumImageSize, Value:100'
	   
	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'AssetImage.OptimumImageSize')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
			IF @SettingTypeId IS NULL
			BEGIN
					SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
					INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
					VALUES('AssetImage.OptimumImageSize', 'Size in KB of the image exceeding which alert icon will be displayed in image library',1, 1, @Sequence)
			END
	    
			SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
			INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
			VALUES  ( @SiteId, -- SiteId - uniqueidentifier
						@SettingTypeId, -- SettingTypeId - int
						N'100'  -- Value - nvarchar(max)
						)
	END

	Fetch NEXT FROM SITE_CURSOR_ INTO @SiteId 		
END

CLOSE SITE_CURSOR_
DEALLOCATE SITE_CURSOR_

GO


IF OBJECT_ID('vwAttributeValue') IS NOT NULL
	DROP VIEW vwAttributeValue
GO
PRINT 'Adding a column EditorOptionsStyleSheet'
GO
IF COL_LENGTH(N'[dbo].[SITemplate]', N'EditorOptionsStyleSheet') IS  NULL
 ALTER TABLE SITemplate Add [EditorOptionsStyleSheet] [nvarchar](256) NULL,
	[EditorContentStyleSheet] [nvarchar](256) NULL
GO
PRINT 'Altering  a column IsNotShared'
GO

IF OBJECT_ID('DF_SITemplate_IsNotShared') IS NULL 
ALTER TABLE SITemplate ADD CONSTRAINT DF_SITemplate_IsNotShared DEFAULT (0) FOR IsNotShared
 
GO
PRINT 'Altering a column TATask.Title'
GO
IF COL_LENGTH(N'[dbo].[TATask]', N'Title') IS NOT NULL
ALTER TABLE TATask ALTER COLUMN Title NVARCHAR (1024)   NOT NULL
GO
PRINT 'Adding a column MKCampaignAdditionalInfo.UseLocalTimeZoneToSend'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignAdditionalInfo]', N'UseLocalTimeZoneToSend') IS  NULL
 ALTER TABLE MKCampaignAdditionalInfo Add [UseLocalTimeZoneToSend] [bit] NOT NULL DEFAULT(0)
GO
PRINT 'dropping constraints for MKCampaignAdditionalInfo'
GO
declare @name varchar(100),@Command nvarchar(max)
select @name=name from sys.default_constraints where parent_object_id = object_id('MKCampaignAdditionalInfo')
        AND parent_column_id = columnproperty(object_id('MKCampaignAdditionalInfo'), 'AutoUpdateLists', 'ColumnId')
  if(@name is not null)
  begin
	 select @Command= N'ALTER TABLE MKCampaignAdditionalInfo DROP CONSTRAINT '+@name
	 EXEC sp_executesql @Command
  end
  
  SET @name = 'DF_MKCampaignAdditionalInfo_AutoUpdateLists'
 
 select @Command= N'ALTER TABLE MKCampaignAdditionalInfo ADD CONSTRAINT '+@name + ' DEFAULT (0) FOR AutoUpdateLists'
 IF COL_LENGTH(N'[dbo].[MKCampaignAdditionalInfo]', N'AutoUpdateLists') IS NOT NULL
 EXEC sp_executesql @Command
GO
PRINT 'Adding a constrainsts for SISite.TimeZone'
GO
IF COL_LENGTH(N'[dbo].[SISite]', N'TimeZone') IS  NULL
 ALTER TABLE SISite Add [TimeZone] NVARCHAR(255) NULL CONSTRAINT [DF_SISite_TimeZone]  DEFAULT (N'Eastern Standard Time') 
GO
PRINT 'Adding a column for USSiteUser.IsPrimarySite'
GO
IF COL_LENGTH(N'[dbo].[USSiteUser]', N'IsPrimarySite') IS  NULL
 ALTER TABLE USSiteUser Add IsPrimarySite bit default(1)
GO
PRINT 'Creating index USSiteUser.SiteId'
GO
if not exists(SELECT 1 FROM sys.indexes WHERE name = 'IX_SiteUser_SiteId' AND object_id = OBJECT_ID('USSiteUser'))
CREATE NONCLUSTERED INDEX [IX_SiteUser_SiteId] ON [dbo].[USSiteUser] ([SiteId])
GO
PRINT 'Creating column TATask.ScheduleId'
GO
IF COL_LENGTH(N'[dbo].[TATask]', N'ScheduleId') IS  NULL
ALTER TABLE TATask  ADD [ScheduleId]	UNIQUEIDENTIFIER NULL
GO
PRINT 'Creating column TATask.ObjectId'
GO
IF COL_LENGTH(N'[dbo].[TATask]', N'ObjectId') IS  NULL
ALTER TABLE TATask  ADD     [ObjectId]	UNIQUEIDENTIFIER NULL
GO
PRINT 'Creating column TATaskHistory.TaskTitle'
GO
IF COL_LENGTH(N'[dbo].[TATaskHistory]', N'TaskTitle') IS NOT NULL
ALTER TABLE TATaskHistory  ALTER COLUMN     [TaskTitle]       NVARCHAR (1024)   NULL

GO
PRINT 'Creating column TATaskHistory.TaskParameter'
GO
IF COL_LENGTH(N'[dbo].[TATaskHistory]', N'TaskParameter') IS NOT NULL
ALTER TABLE TATaskHistory  ALTER COLUMN     [TaskParameter]   VARCHAR (MAX)    NULL
GO
PRINT 'Creating column TADistributionGroup.IsSystem'
GO
IF COL_LENGTH(N'[dbo].[TADistributionGroup]', N'IsSystem') IS  NULL
ALTER TABLE TADistributionGroup  ADD     [IsSystem] BIT NULL

GO
PRINT 'Creating column TADistributionLists.IsSystem'
GO
IF COL_LENGTH(N'[dbo].[TADistributionLists]', N'IsSystem') IS  NULL
ALTER TABLE TADistributionLists  ADD    [IsSystem] BIT NULL

GO
PRINT 'Altering column TATaskHistory.Exceptions'
GO
IF COL_LENGTH(N'[dbo].[TATaskHistory]', N'Exceptions') IS NOT NULL
ALTER TABLE TATaskHistory  ALTER COLUMN     [Exceptions]      NVARCHAR (MAX)   NULL
GO
PRINT 'Creating table TADistributionListSite'
GO
IF OBJECT_ID('TADistributionListSite') IS  NULL
CREATE TABLE [dbo].[TADistributionListSite]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [DistributionListId] UNIQUEIDENTIFIER NOT NULL, 
    [SiteId] UNIQUEIDENTIFIER NOT NULL, 
    [Count] INT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_TADistributionListSite_TADistributionLists] FOREIGN KEY ([DistributionListId]) REFERENCES [TADistributionLists]([Id])
)
GO
PRINT 'altering table TADistributionListSite.LastSynced'
GO
IF COL_LENGTH(N'[dbo].[TADistributionListSite]', N'LastSynced') IS  NULL
ALTER TABLE TADistributionListSite  ADD    [LastSynced] DATETIME NULL
GO
PRINT 'creating index table TADistributionListSite.[DistributionListId]'
GO
if not exists(
           SELECT 1 
           FROM sys.indexes 
           WHERE name = 'IX_TADistributionListSite_DistributionListId' 
           AND object_id = OBJECT_ID('TADistributionListSite')
          )
CREATE INDEX [IX_TADistributionListSite_DistributionListId] ON [dbo].[TADistributionListSite] ([DistributionListId])

GO
PRINT 'creating table MKCampaignRunTimeZoneSites'
GO
IF OBJECT_ID('MKCampaignRunTimeZoneSites') IS  NULL
CREATE TABLE [dbo].[MKCampaignRunTimeZoneSites](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CampaignRunId] [uniqueidentifier] NULL,
	[CampaignId] [uniqueidentifier] NULL,
	[StartTime] [datetime] NULL,
	[SiteId] [uniqueidentifier] NULL,
	[IsProcessed] [bit] NULL,
	[ProcessedContactCount] [int] NULL,
 CONSTRAINT [PK_MKCampaign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
PRINT 'Altering table [MKCampaignRunTimeZoneSites]'
GO
IF OBJECT_ID('DF__MKCampaig__IsProcessed') IS  NULL 
ALTER TABLE [dbo].[MKCampaignRunTimeZoneSites] ADD  CONSTRAINT [DF__MKCampaig__IsProcessed]  DEFAULT ((0)) FOR [IsProcessed]
GO
PRINT 'Altering table [MKCampaignRunTimeZoneSites] for constraints'
GO
IF OBJECT_ID('FK_MKCampaignRunTimeZoneSites_MKCampaign') IS  NULL 
ALTER TABLE [dbo].[MKCampaignRunTimeZoneSites]  WITH CHECK ADD  CONSTRAINT [FK_MKCampaignRunTimeZoneSites_MKCampaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[MKCampaign] ([Id])
GO
PRINT 'Altering table [MKCampaignRunTimeZoneSites] for FK_MKCampaignRunTimeZoneSites_MKCampaignRunHistory'
GO
IF OBJECT_ID('FK_MKCampaignRunTimeZoneSites_MKCampaignRunHistory') IS  NULL 
ALTER TABLE [dbo].[MKCampaignRunTimeZoneSites]  WITH CHECK ADD  CONSTRAINT [FK_MKCampaignRunTimeZoneSites_MKCampaignRunHistory] FOREIGN KEY([CampaignRunId])
REFERENCES [dbo].[MKCampaignRunHistory] ([Id])
GO
PRINT 'creating table MKContactStats'
GO
IF OBJECT_ID('MKContactStats') IS NULL
CREATE TABLE [dbo].[MKContactStats](
	[ContactId] [uniqueidentifier] NOT NULL,
	[Opens] [int] NOT NULL,
	[Clicks] [int] NOT NULL,
	[TriggeredWatches] [int] NOT NULL,
	[Unsubscribes] [int] NOT NULL,
	[Bounces] [int] NOT NULL,
	[Opened]  AS (case when [Opens]<=(0) then (0) else (1) end) PERSISTED NOT NULL,
	[Clicked]  AS (case when [Clicks]<=(0) then (0) else (1) end) PERSISTED NOT NULL,
	[TriggeredWatch]  AS (case when [TriggeredWatches]<=(0) then (0) else (1) end) PERSISTED NOT NULL,
	[Unsubscribed]  AS (case when [Unsubscribes]<=(0) then (0) else (1) end),
	[Bounced]  AS (case when [Bounces]<=(0) then (0) else (1) end) PERSISTED NOT NULL,
 CONSTRAINT [PK_MKContactStats] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
PRINT 'altering table MKContactStats for DF_MKContactStats_Opens'
GO
IF OBJECT_ID('DF_MKContactStats_Opens') IS  NULL 
ALTER TABLE [dbo].[MKContactStats] ADD  CONSTRAINT [DF_MKContactStats_Opens]  DEFAULT ((0)) FOR [Opens]
GO
PRINT 'altering table MKContactStats for [DF_MKContactStats_Clicks'
GO
IF OBJECT_ID('DF_MKContactStats_Clicks') IS  NULL 
ALTER TABLE [dbo].[MKContactStats] ADD  CONSTRAINT [DF_MKContactStats_Clicks]  DEFAULT ((0)) FOR [Clicks]
GO
PRINT 'altering table MKContactStats for [DF_MKContactStats_TriggeredWatches'
GO
IF OBJECT_ID('DF_MKContactStats_TriggeredWatches') IS  NULL 
ALTER TABLE [dbo].[MKContactStats] ADD  CONSTRAINT [DF_MKContactStats_TriggeredWatches]  DEFAULT ((0)) FOR [TriggeredWatches]
GO
PRINT 'altering table MKContactStats for DF_MKContactStats_Unsubscribes'
GO
IF OBJECT_ID('DF_MKContactStats_Unsubscribes') IS  NULL 
ALTER TABLE [dbo].[MKContactStats] ADD  CONSTRAINT [DF_MKContactStats_Unsubscribes]  DEFAULT ((0)) FOR [Unsubscribes]
GO
PRINT 'altering table MKContactStats for [DF_MKContactStats_Bounces'
GO
IF OBJECT_ID('DF_MKContactStats_Bounces') IS  NULL 
ALTER TABLE [dbo].[MKContactStats] ADD  CONSTRAINT [DF_MKContactStats_Bounces]  DEFAULT ((0)) FOR [Bounces]
GO
PRINT 'create table MKCampaignRunErrorLog'
GO
IF OBJECT_ID('MKCampaignRunErrorLog') IS NULL
CREATE TABLE [dbo].[MKCampaignRunErrorLog](
	[Id] [uniqueidentifier] NOT NULL PRIMARY KEY CLUSTERED,
	[CampaignId] [uniqueidentifier] NOT NULL,
	[CampaignRunId] [uniqueidentifier] NOT NULL,
	[ContactId] [uniqueidentifier] NULL,
	[ErrorMessage] [nvarchar](max) NULL
)
GO
PRINT 'create table ATPageAttributeValue'
GO
IF OBJECT_ID('ATPageAttributeValue') IS NULL
CREATE TABLE [dbo].[ATPageAttributeValue](
	[Id] [uniqueidentifier] NOT NULL,
	[PageDefinitionId] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[AttributeEnumId] [uniqueidentifier] NULL,
	[Value] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	 
    CONSTRAINT [PK_ATPageAttributeValue] PRIMARY KEY ([Id])
)
GO
PRINT 'create table MKCampaignTestEmail'
GO
IF OBJECT_ID('MKCampaignTestEmail') IS NULL
Create TABLE [dbo].[MKCampaignTestEmail](
	[Id] [uniqueidentifier] NOT NULL,
	[CampaignId] [uniqueidentifier] NOT NULL,
	[CampaignEmailId] [uniqueidentifier] NOT NULL,
	[ContactId] [uniqueidentifier] NOT NULL,
	[EmailHtml] [nvarchar](max) NULL,
	[EmailText] [nvarchar](max) NULL,
	[EmailSubject] [nvarchar](1024) NULL,
	[ProcessedDateTime] [datetime] NULL,
	[SendToEmails] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_MKCampaignTestEmail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
PRINT 'create table MKContactNotes'
GO
IF OBJECT_ID('MKContactNotes') IS NULL
CREATE TABLE [dbo].[MKContactNotes]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	ContactId uniqueidentifier NOT NULL,
	ChangeLog xml NOT NULL,
	Version int NOT NULL,
	CreatedDate datetime NOT NULL,
	CreatedBy uniqueidentifier NOT NULL
)
GO
PRINT 'create table ATAttributeValidation'
GO
IF OBJECT_ID('ATAttributeValidation') IS NULL
CREATE TABLE [dbo].[ATAttributeValidation](
	[Id] [uniqueidentifier] NOT NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[ExpressionId] [uniqueidentifier] NULL,
	[MinValue] [nvarchar](2000) NULL,
	[MaxValue] [nvarchar](2000) NULL, 
    CONSTRAINT [PK_ATAttributeValidation] PRIMARY KEY ([Id])
	)
GO
PRINT 'create table ATAttributeExpression'
GO
IF OBJECT_ID('ATAttributeExpression') IS NULL
CREATE TABLE [dbo].[ATAttributeExpression](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](2000) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[RegularExpression] [nvarchar](2000) NULL,
	[Sequence] [int] NOT NULL, 
    CONSTRAINT [PK_ATAttributeExpression] PRIMARY KEY ([Id])
	 )

GO
PRINT 'create table GLSyncLog'
GO
IF OBJECT_ID('GLSyncLog') IS NULL
CREATE TABLE [dbo].[GLSyncLog](
	[Id] [uniqueidentifier] NOT NULL,
	[Type] [int] NOT NULL,
	[LastSynced] [datetime] NULL,
 CONSTRAINT [PK_GLSyncLog] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
PRINT 'creating index on  GLSyncLog.Id'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='IX_GLSyuncLog' AND object_id = OBJECT_ID('GLSyncLog'))
CREATE UNIQUE CLUSTERED INDEX [IX_GLSyuncLog] ON [dbo].[GLSyncLog]
(
	[Id] ASC,
	[LastSynced] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
PRINT 'creating table  ATAttributeCategory'
GO
IF OBJECT_ID('ATAttributeCategory') IS NULL
CREATE TABLE [dbo].[ATAttributeCategory]
(
	[Id] [int] NOT NULL ,
	[Name] [nvarchar](2000) NULL,
	[Description] [nvarchar](max) NULL,
	[ProductId] [uniqueidentifier] NULL, 
    CONSTRAINT [PK_ATAttributeCategory] PRIMARY KEY ([Id])
)
GO
PRINT 'creating table  ATAttributeCategoryItem'
GO
IF OBJECT_ID('ATAttributeCategoryItem') IS NULL
CREATE TABLE [dbo].[ATAttributeCategoryItem](
	[Id] [uniqueidentifier] NOT NULL ,
	[AttributeId] [uniqueidentifier] NULL,
	[CategoryId] [int] NULL,
	[IsRequired] [bit] NULL,
	[MinValue] [nvarchar](200) NULL,
	[MaxValue] [nvarchar](200) NULL,
	[ErrorMessage] [nvarchar](1000) NULL, 
    CONSTRAINT [PK_ATAttributeCategoryItem] PRIMARY KEY ([Id]),
 )
GO
PRINT 'creating table  tblContactPropertiesAndAttributes'
GO
IF OBJECT_ID('tblContactPropertiesAndAttributes') IS NULL
CREATE TABLE [dbo].[tblContactPropertiesAndAttributes](
	[ContactId] [uniqueidentifier] NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[AttributeEnumId] [uniqueidentifier] NULL,
	[Value] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [uniqueidentifier] NULL,
	[DataType] [nvarchar](50) NOT NULL,
	[CachedDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
PRINT 'creating index  tblContactPropertiesAndAttributes'
GO
if not exists(
           SELECT 1 
           FROM sys.indexes 
           WHERE name = 'IX_tblContactPropertiesAndAttributes_AttributeId' 
           AND object_id = OBJECT_ID('tblContactPropertiesAndAttributes')
          )
CREATE CLUSTERED INDEX [IX_tblContactPropertiesAndAttributes_AttributeId] ON [dbo].[tblContactPropertiesAndAttributes] 
        (
        [AttributeId] ASC
        )
GO
PRINT 'creating index  ATContactAttributeValue'
GO
IF OBJECT_ID('ATContactAttributeValue') IS NULL
CREATE TABLE [dbo].[ATContactAttributeValue](
	[Id] [uniqueidentifier] NOT NULL ,
	[ContactId] [uniqueidentifier] NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[AttributeEnumId] [uniqueidentifier] NULL,
	[Value] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
    CONSTRAINT [PK_ATContactAttributeValue] PRIMARY KEY ([Id])
)

GO
PRINT 'altering table  ATContactAttributeValue.ModifiedDate'
GO
IF COL_LENGTH(N'[dbo].[ATContactAttributeValue]', N'ModifiedDate') IS NOT NULL
ALTER TABLE ATContactAttributeValue DROP COLUMN ModifiedDate
GO
PRINT 'altering table  ATContactAttributeValue.ModifiedBy'
GO
IF COL_LENGTH(N'[dbo].[ATContactAttributeValue]', N'ModifiedBy') IS NOT NULL
ALTER TABLE ATContactAttributeValue DROP COLUMN ModifiedBy
GO
PRINT 'altering table  ATContactAttributeValue.ModifiedDate'
GO
IF COL_LENGTH(N'[dbo].[ATPageAttributeValue]', N'ModifiedDate') IS NOT NULL
ALTER TABLE ATPageAttributeValue DROP COLUMN ModifiedDate
GO
PRINT 'altering table  ATContactAttributeValue.ModifiedBy'
GO
IF COL_LENGTH(N'[dbo].[ATPageAttributeValue]', N'ModifiedBy') IS NOT NULL
ALTER TABLE ATPageAttributeValue DROP COLUMN ModifiedBy
GO
PRINT 'creating index ATContactAttributeValue.ModifiedBy'
GO
if not exists(
           SELECT 1 
           FROM sys.indexes 
           WHERE name = 'IX_ContactAttributeValue_ModifiedDate' 
           AND object_id = OBJECT_ID('ATContactAttributeValue')
          )
CREATE INDEX [IX_ContactAttributeValue_ModifiedDate] ON [dbo].[ATContactAttributeValue] ([CreatedDate] ASC)

GO
PRINT 'creating table ATSiteAttributeValue'
GO
IF OBJECT_ID('ATSiteAttributeValue') IS NULL
CREATE TABLE [dbo].[ATSiteAttributeValue](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NULL,
	[AttributeId] [uniqueidentifier] NULL,
	[AttributeEnumId] [uniqueidentifier] NULL,
	[Value] [nvarchar](max) NULL,
	[Notes] [nvarchar](max) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	
    CONSTRAINT [PK_ATSiteAttributeValue] PRIMARY KEY ([Id])
) 
GO

PRINT 'creating table STEmailProcessorSetting'
GO
IF OBJECT_ID('STEmailProcessorSetting') IS NULL
CREATE TABLE [dbo].[STEmailProcessorSetting](
	[SiteId] [uniqueidentifier] NOT NULL,
	[SenderEmailOverride] [nvarchar](255) NULL,
	[ReplyToEmail] [nvarchar](255) NULL,
	[ReturnPath] [nvarchar](255) NULL,
	[ReturnPathDomain] [nvarchar](255) NULL,
	[SmtpServer] [nvarchar](255)  NULL,
	[SmtpPort] [int]  NULL,
	[SmtpEnableSsl] [bit]  NULL,
	[SmtpAuthentication] [bit]  NULL,
	[SmtpUsername] [nvarchar](255) NULL,
	[SmtpPassword] [nvarchar](255) NULL,
	[BatchSize] [int]  NULL,
	[Enabled] [bit]  NULL,
 CONSTRAINT [PK_STEmailProcessorSetting_Id] PRIMARY KEY CLUSTERED 
(
	[SiteId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
PRINT 'creating table MKContact'
GO
IF (OBJECT_ID('MKContact') IS NULL)
CREATE TABLE [dbo].[MKContact](
	[Id] [uniqueidentifier] NOT NULL,
	[Email] [nvarchar](256) NULL,
	[FirstName] [nvarchar](256) NULL,
	[MiddleName] [nvarchar](256) NULL,
	[LastName] [nvarchar](256) NULL,
	[CompanyName] [nvarchar](1024) NULL,
	[Gender] [nvarchar](50) NULL,
	[BirthDate] [datetime] NULL,
	[HomePhone] [nvarchar](50) NULL,
	[MobilePhone] [nvarchar](50) NULL,
	[OtherPhone] [nvarchar](50) NULL,
	[ImageId] [uniqueidentifier] NULL,
	[AddressId] [uniqueidentifier] NULL,
	[Notes] [nvarchar](1000) NULL,
	[ContactSourceId] [int] NULL,
	[Status] [int] NULL,
	[CreatedBy]    UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]  DATETIME         NOT NULL,
    [ModifiedBy]   UNIQUEIDENTIFIER NULL,
    [ModifiedDate] DATETIME         NULL,
 CONSTRAINT [PK_MKLead] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

if not exists(
           SELECT 1 
           FROM sys.indexes 
           WHERE name = 'IX_MKContact_LastName_Status' 
           AND object_id = OBJECT_ID('MKContact')
          )
CREATE NONCLUSTERED INDEX [IX_MKContact_LastName_Status] ON [dbo].[MKContact] (LastName ,Status)
GO
PRINT 'creating table MKCampaignRunResponseWorktable'
GO
IF (OBJECT_ID('MKCampaignRunResponseWorktable') IS NULL)
CREATE TABLE [dbo].[MKCampaignRunResponseWorktable]
(
	[CampaignRunId] UNIQUEIDENTIFIER,
	ResponseStatus INT,
	ContactId UNIQUEIDENTIFIER,
	LogMessage NVARCHAR(MAX)
)
GO
PRINT 'creating index MKContact.[ModifiedDate]'
GO
if not exists(
           SELECT 1 
           FROM sys.indexes 
           WHERE name = 'IX_MKContact_ModifiedDate' 
           AND object_id = OBJECT_ID('MKContact')
          )
CREATE NONCLUSTERED INDEX [IX_MKContact_ModifiedDate] ON [dbo].[MKContact] 
(
	[ModifiedDate] ASC
)
GO
PRINT 'creating index MKContact.[[CreatedDate]]'
GO
if not exists(SELECT 1 FROM sys.indexes WHERE name = 'IX_MKContact_CreatedDate' AND object_id = OBJECT_ID('MKContact'))
CREATE NONCLUSTERED INDEX [IX_MKContact_CreatedDate] ON [dbo].[MKContact] 
(
	[CreatedDate] ASC
)
INCLUDE ( [Id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
PRINT 'creating index MKEmailSendLog.[UserId]'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='IDX_MKEmailSendLog_CampaignRunId_UserId' AND object_id = OBJECT_ID('MKEmailSendLog'))
CREATE NONCLUSTERED INDEX [IDX_MKEmailSendLog_CampaignRunId_UserId]     ON [dbo].[MKEmailSendLog]([CampaignRunId])   INCLUDE([UserId])
GO
PRINT 'dropping constraint FK_MKResponseFlowWorkTable_USUser'
GO
IF (OBJECT_ID('FK_MKResponseFlowWorkTable_USUser') IS NOT NULL)
	ALTER TABLE MKResponseFlowWorkTable DROP CONSTRAINT FK_MKResponseFlowWorkTable_USUser
GO
PRINT 'Adding a column TADistributionListUser.ContactListSubscriptionType'
GO
IF COL_LENGTH(N'[dbo].[TADistributionListUser]', N'ContactListSubscriptionType') IS NULL
ALTER TABLE TADistributionListUser ADD [ContactListSubscriptionType] INT NOT NULL DEFAULT (1)
GO
PRINT 'dropping constraint TADistributionListUser'
GO
IF (OBJECT_ID('FK_TADistributionListUser_USUser') IS NOT NULL)
	ALTER TABLE TADistributionListUser DROP CONSTRAINT FK_TADistributionListUser_USUser
GO
PRINT 'dropping constraint FK_MKEmailSendLog_USUser'
GO
IF (OBJECT_ID('FK_MKEmailSendLog_USUser') IS NOT NULL)
	ALTER TABLE MKEmailSendLog DROP CONSTRAINT FK_MKEmailSendLog_USUser
GO
PRINT 'dropping constraint FK_MKResponseFlow_USUser'
GO
IF (OBJECT_ID('FK_MKResponseFlow_USUser') IS NOT NULL)
	ALTER TABLE MKResponseFlow DROP CONSTRAINT FK_MKResponseFlow_USUser
GO
PRINT 'dropping constraint FK_USUnsubscribeHistory_USUser'
GO
IF (OBJECT_ID('FK_USUnsubscribeHistory_USUser') IS NOT NULL)
	ALTER TABLE USUnsubscribeHistory DROP CONSTRAINT FK_USUnsubscribeHistory_USUser
GO
PRINT 'dropping constraint FK_MKCampaignUser_USUser'
GO
IF (OBJECT_ID('FK_MKCampaignUser_USUser') IS NOT NULL)
	ALTER TABLE MKCampaignUser DROP CONSTRAINT FK_MKCampaignUser_USUser
GO
PRINT 'dropping constraint FK_MKCampaignUserDraft_USUser'
GO
IF (OBJECT_ID('FK_MKCampaignUserDraft_USUser') IS NOT NULL)
	ALTER TABLE MKCampaignUserDraft DROP CONSTRAINT FK_MKCampaignUserDraft_USUser
GO
PRINT 'dropping constraint FK_MKCampaignRunWorkTable_USUser'
GO
IF (OBJECT_ID('FK_MKCampaignRunWorkTable_USUser') IS NOT NULL)
	ALTER TABLE MKCampaignRunWorkTable DROP CONSTRAINT FK_MKCampaignRunWorkTable_USUser
GO
PRINT 'creating type NewPageTemplates'
GO
IF OBJECT_ID('Site_AddPageTemplates') IS NOT NULL
	DROP PROCEDURE Site_AddPageTemplates
GO
IF TYPE_ID(N'[dbo].[NewPageTemplates]') IS NOT NULL
	DROP Type NewPageTemplates
GO
IF TYPE_ID(N'[dbo].[NewPageTemplates]') IS NULL
CREATE TYPE	[dbo].[NewPageTemplates] AS TABLE (
	DirectoryPath			NVARCHAR(255),
	Title					NVARCHAR(255),
	Description				NVARCHAR(255),
	AscxFile				NVARCHAR(255),
	CsFile					NVARCHAR(255),
	ImageFile				NVARCHAR(255),
	Type					BIT NOT NULL,
	PagePart				BIT NOT NULL,
	IsThemed				BIT DEFAULT(0),
	ZonesXml				XML,
	EditorOptionsStyleSheet	NVARCHAR(256),
	EditorContentStyleSheet	NVARCHAR(256)
)
GO
PRINT 'creating table MKContactFilter'
GO
IF (OBJECT_ID('MKContactFilter') IS NULL)
CREATE TABLE [dbo].[MKContactFilter]
(
	[Id] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	ApplicationId uniqueidentifier not null,
	Title NVARCHAR(MAX) ,
	FilterType INT
)

GO
PRINT 'creating table MKContactAttributeValues'
GO
IF (OBJECT_ID('MKContactAttributeValues') IS NULL)
CREATE TABLE [dbo].[MKContactAttributeValues](
	[Id] [uniqueidentifier] NOT NULL,
	[ContactId] [uniqueidentifier] NOT NULL,
	[StoreNumber] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
 CONSTRAINT [PK_MKContactAttributeValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
PRINT 'creating constraints FK_MKContactAttributeValues_MKContact'
GO
IF OBJECT_ID('FK_MKContactAttributeValues_MKContact') IS  NULL 
ALTER TABLE [dbo].[MKContactAttributeValues]  WITH CHECK ADD  CONSTRAINT [FK_MKContactAttributeValues_MKContact] FOREIGN KEY([ContactId])
REFERENCES [dbo].[MKContact] ([Id])

GO
PRINT 'creating table MKContactSite'
GO
IF (OBJECT_ID('MKContactSite') IS NULL)
CREATE TABLE [dbo].[MKContactSite](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ContactId] [uniqueidentifier] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_MKContactSite] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
PRINT 'creating constraints FK_MKContactSite_MKContact'
GO
IF OBJECT_ID('FK_MKContactSite_MKContact') IS  NULL 
ALTER TABLE [dbo].[MKContactSite] WITH NOCHECK
    ADD  CONSTRAINT [FK_MKContactSite_MKContact] FOREIGN KEY([ContactId]) REFERENCES [dbo].[MKContact] ([Id])
GO
PRINT 'creating constraints IX_MKContactSite_ContactId'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name='IX_MKContactSite_ContactId' AND object_id = OBJECT_ID('MKContactSite'))
	CREATE NONCLUSTERED INDEX [IX_MKContactSite_ContactId] ON [dbo].[MKContactSite] ([ContactId])
GO
PRINT 'creating column MKContactSite.IsPrimarySite'
GO
IF COL_LENGTH(N'[dbo].[MKContactSite]', N'IsPrimarySite') IS  NULL
    ALTER TABLE MKContactSite Add IsPrimarySite bit default(1)
GO
PRINT 'Adding a new column ATAttribute.IsMultiValued'
GO
IF COL_LENGTH(N'[dbo].[ATAttribute]', N'IsMultiValued') IS NULL
	ALTER TABLE [dbo].[ATAttribute] ADD    [IsMultiValued] BIT NULL
GO    
PRINT 'Adding a new column ATAttributeDataType.Sequence'
GO
IF COL_LENGTH(N'[dbo].[ATAttributeDataType]', N'Sequence') IS NULL
	ALTER TABLE [dbo].[ATAttributeDataType] ADD    [Sequence] INT NULL
GO    

PRINT 'Adding a new column MKCampaignRunHistory.EmailText'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunHistory]', N'EmailText') IS NULL
	ALTER TABLE [dbo].[MKCampaignRunHistory] ADD   [EmailText]  TEXT  CONSTRAINT [DF_MKCampaignRunHistory_EmailText] DEFAULT ('') NULL
GO
PRINT 'Adding a new column MKCampaignRunHistory.EmailText'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunHistory]', N'EndDateTime') IS NULL
	ALTER TABLE [dbo].[MKCampaignRunHistory] ADD    [EndDateTime] DATETIME NULL

GO
PRINT 'Adding a new column MKCampaignRunHistory.UniqueOpens'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunHistory]', N'UniqueOpens') IS NULL
	ALTER TABLE [dbo].[MKCampaignRunHistory] ADD   [UniqueOpens] INT NULL

GO

PRINT 'Adding a new column MKContact.LastSynced'
GO
IF COL_LENGTH(N'[dbo].[MKContact]', N'LastSynced') IS NULL
	ALTER TABLE [dbo].[MKContact] ADD   [LastSynced] DATETIME NULL

GO
PRINT 'Adding a new column TADistributionLists.IsGlobal'
GO
IF COL_LENGTH(N'[dbo].[TADistributionLists]', N'IsGlobal') IS NULL
	ALTER TABLE [dbo].[TADistributionLists] ADD  [IsGlobal] BIT NOT NULL DEFAULT 0
GO
PRINT 'Adding a new column TADistributionLists.IsGlobal'
GO
IF COL_LENGTH(N'[dbo].[TADistributionLists]', N'ContactCount') IS NULL
	ALTER TABLE [dbo].[TADistributionLists] ADD [ContactCount] INT NOT NULL DEFAULT 0
 
GO
PRINT 'Adding a new column MKCampaignRunWorkTable.ProcessingCount'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunWorkTable]', N'ProcessingCount') IS NULL
	ALTER TABLE [dbo].[MKCampaignRunWorkTable] ADD ProcessingCount	INT NOT NULL DEFAULT(0)
GO

PRINT 'Adding a new column MKCampaignRunWorkTable.ProcessingStatus'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunWorkTable]', N'ProcessingStatus') IS NULL
	ALTER TABLE [dbo].[MKCampaignRunWorkTable] ADD ProcessingStatus INT NOT NULL DEFAULT(0)
GO
PRINT 'Adding a new column MKCampaignRunWorkTable.LogMessage'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignRunWorkTable]', N'LogMessage') IS NULL
	ALTER TABLE [dbo].[MKCampaignRunWorkTable] ADD LogMessage VARCHAR(MAX) NULL
GO
PRINT 'Adding a new column MKCampaignGroup.ApplicationId'
GO
IF COL_LENGTH(N'[dbo].[MKCampaignGroup]', N'ApplicationId') IS NULL
	ALTER TABLE [dbo].[MKCampaignGroup] ADD [ApplicationId] UNIQUEIDENTIFIER NULL
GO
PRINT 'Adding a new column ATAttributeGroup.ProductId'
GO
IF COL_LENGTH(N'[dbo].[ATAttributeGroup]', N'ProductId') IS NULL
	ALTER TABLE [dbo].[ATAttributeGroup] ADD [ProductId] UNIQUEIDENTIFIER NULL
GO
PRINT 'Adding a new column ATAttributeGroup.IsGlobal'
GO
IF COL_LENGTH(N'[dbo].[ATAttributeGroup]', N'IsGlobal') IS NULL
	ALTER TABLE [dbo].[ATAttributeGroup] ADD [IsGlobal] BIT NULL
GO
PRINT 'Creating view vw_Address' 
GO
PRINT 'DEFAULT DATA'
GO
--Template data
PRINT 'Creating attribute category Site Attributes'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeCategory Where Name = 'Site Attributes')
INSERT INTO [ATAttributeCategory]
           ([Id]
           ,[Name]
           ,[ProductId])
     VALUES
           (1
           ,'Site Attributes'
           ,'CBB702AC-C8F1-4C35-B2DC-839C26E39848')
GO
PRINT 'Creating attribute category Page Attributes'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeCategory Where Name = 'Page Attributes')
INSERT INTO [ATAttributeCategory]
           ([Id]
           ,[Name]
           ,[ProductId])
     VALUES
           (2
           ,'Page Attributes'
           ,'CBB702AC-C8F1-4C35-B2DC-839C26E39848')
GO
PRINT 'Creating attribute category Contact Attributes'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeCategory Where Name = 'Contact Attributes')
INSERT INTO [ATAttributeCategory]
           ([Id]
           ,[Name]
           ,[ProductId])
     VALUES
           (3
           ,'Contact Attributes'
           ,'CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E')

GO
PRINT 'Creating attribute expression Email'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeExpression Where Title = 'Email')
INSERT INTO ATAttributeExpression
(
	Id,
	Title,
	RegularExpression,
	Sequence
)
VALUES
(
	'c8779f66-090b-4150-ba3f-89ec240d5c3e',
	'Email',
	'^\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$',
	1
)
GO
PRINT 'Creating attribute expression US Phone Number'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeExpression Where Title = 'US Phone Number')
INSERT INTO ATAttributeExpression
(
	Id,
	Title,
	RegularExpression,
	Sequence
)
VALUES
(
	'60c165c6-bb51-4061-a390-59721a9632b4',
	'US Phone Number',
	'\(?\d{3}\)?-? *\d{3}-? *-?\d{4}',
	2
)
GO
PRINT 'Creating attribute expression URL'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeExpression Where Title = 'URL')
INSERT INTO ATAttributeExpression
(
	Id,
	Title,
	RegularExpression,
	Sequence
)
VALUES
(
	'e01e0eb9-674b-4fef-899b-8202c38e4e41',
	'URL',
	'^http(s)?://([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$',
	3
)
GO
PRINT 'Creating attribute expression US ZIP Code'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeExpression Where Title = 'US ZIP Code')
INSERT INTO ATAttributeExpression
(
	Id,
	Title,
	RegularExpression,
	Sequence
)
VALUES
(
	NEWID(),
	'US ZIP Code',
	'\d{5}(-\d{4})?',
	4
)
GO
PRINT 'Creating attribute expression US Social Security number'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeExpression Where Title = 'US Social Security number')
INSERT INTO ATAttributeExpression
(
	Id,
	Title,
	RegularExpression,
	Sequence
)
VALUES
(
	NEWID(),
	'US Social Security number',
	'\d{3}-\d{2}-\d{4}',
	5
)
GO
PRINT 'Creating attribute expression US Phone number'
GO
IF NOT EXISTS (SELECT * FROM ATAttributeExpression Where Title = 'US Phone number')
INSERT INTO ATAttributeExpression
(
	Id,
	Title,
	RegularExpression,
	Sequence
)
VALUES
(
	NEWID(),
	'US Phone number',
	'((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}',
	6
)
GO
PRINT 'Deleting attribute datatype Enumerated'
GO
DELETE FROM ATAttributeDataType WHERE Title like 'Enumerated'
GO
PRINT 'Updating attribute datatypes'
GO
IF EXISTS (SELECT * FROM ATAttributeDataType WHERE Sequence IS NULL)
BEGIN
	UPDATE ATAttributeDataType SET Sequence = 1 WHERE Title = 'String'
	UPDATE ATAttributeDataType SET Sequence = 2 WHERE Title = 'Integer'
	UPDATE ATAttributeDataType SET Sequence = 3 WHERE Title = 'Decimal'
	UPDATE ATAttributeDataType SET Sequence = 4 WHERE Title = 'Boolean'
	UPDATE ATAttributeDataType SET Sequence = 5 WHERE Title = 'Date'
	UPDATE ATAttributeDataType SET Sequence = 6 WHERE Title = 'HTML'
	UPDATE ATAttributeDataType SET Sequence = 7 WHERE Title = 'File Upload'
	UPDATE ATAttributeDataType SET Sequence = 8 WHERE Title = 'Page Library'
	UPDATE ATAttributeDataType SET Sequence = 9 WHERE Title = 'Content Library'
	UPDATE ATAttributeDataType SET Sequence = 10 WHERE Title = 'Asset Library File'
	UPDATE ATAttributeDataType SET Sequence = 11 WHERE Title = 'Image Library'
END
GO

PRINT 'Creating contact source for iAPPS Website Users'
GO
IF NOT EXISTS(Select 1 from CTContactSource Where Id=8)
INSERT INTO [dbo].[CTContactSource] Values(8,'iAPPS Website Users', 'Website users registered throught public site',2)
GO
PRINT 'Creating contact source for iAPPS E-Commerce Customer'
GO
IF NOT EXISTS(Select 1 from CTContactSource Where Id=9)
INSERT INTO [dbo].[CTContactSource] Values(9,'iAPPS E-Commerce Customer', 'E-Commerce customers registered through public E-Commerce site',2)

GO
PRINT 'Creating CTContactSearchConfig for DistributionList'
GO
IF NOT EXISTS (SELECT * FROM CTContactSearchConfig Where Name = 'DistributionList')
INSERT  INTO dbo.CTContactSearchConfig
        ( Id ,
          Name ,
          Sequence ,
          IsActive 
        )
        SELECT  ISNULL(MAX(ID), 0) + 1 ,
                'DistributionList' ,
                ISNULL(MAX(Sequence), 0) + 1 ,
                1
        FROM    dbo.CTContactSearchConfig
GO      
PRINT 'Creating CTContactSearchConfig for ProductTypesPurchased'
GO
IF NOT EXISTS (SELECT * FROM CTContactSearchConfig Where Name = 'ProductTypesPurchased')
INSERT  INTO dbo.CTContactSearchConfig
        ( Id ,
          Name ,
          Sequence ,
          IsActive 
        )
        SELECT  ISNULL(MAX(ID), 0) + 1 ,
                'ProductTypesPurchased' ,
                ISNULL(MAX(Sequence), 0) + 1 ,
                1
        FROM    dbo.CTContactSearchConfig
GO       
PRINT 'Creating CTContactSearchConfig for SiteGroups'
GO
IF NOT EXISTS (SELECT * FROM CTContactSearchConfig Where Name = 'SiteGroups')
INSERT  INTO dbo.CTContactSearchConfig
        ( Id ,
          Name ,
          Sequence ,
          IsActive 
        )
        SELECT  ISNULL(MAX(ID), 0) + 1 ,
                'SiteGroups' ,
                ISNULL(MAX(Sequence), 0) + 1 ,
                1
        FROM    dbo.CTContactSearchConfig
GO
PRINT 'Creating CTContactSearchConfig for SiteIds'
GO
IF NOT EXISTS (SELECT * FROM CTContactSearchConfig Where Name = 'SiteIds')
INSERT  INTO dbo.CTContactSearchConfig
        ( Id ,
          Name ,
          Sequence ,
          IsActive 
        )
        SELECT  ISNULL(MAX(ID), 0) + 1 ,
                'SiteIds' ,
                ISNULL(MAX(Sequence), 0) + 1 ,
                1
        FROM    dbo.CTContactSearchConfig
GO
PRINT 'Creating CTContactSearchConfig for ContactAttributeSearch'
GO
IF NOT EXISTS (SELECT * FROM CTContactSearchConfig Where Name = 'ContactAttributeSearch')
INSERT  INTO dbo.CTContactSearchConfig
        ( Id ,
          Name ,
          Sequence ,
          IsActive 
        )
        SELECT  ISNULL(MAX(ID), 0) + 1 ,
                'ContactAttributeSearch' ,
                ISNULL(MAX(Sequence), 0) + 1 ,
                1
        FROM    dbo.CTContactSearchConfig
GO
PRINT 'Creating CTContactSearchConfig for SiteAttributes'
GO
IF NOT EXISTS (SELECT * FROM CTContactSearchConfig Where Name = 'SiteAttributes')
INSERT  INTO dbo.CTContactSearchConfig
        ( Id ,
          Name ,
          Sequence ,
          IsActive 
        )
        SELECT  ISNULL(MAX(ID), 0) + 1 ,
                'SiteAttributes' ,
                ISNULL(MAX(Sequence), 0) + 1 ,
                1
        FROM    dbo.CTContactSearchConfig
GO
INSERT INTO TADistributionListSite(Id,DistributionListId,SiteId)
SELECT NEWID(),* FROM 
(Select Id,ApplicationId FROM TADistributionLists
Except
Select DistributionListId,SiteId FROM TADistributionListSite
)T
GO
PRINT 'Creating vw_ContactListCount'
GO
IF (OBJECT_Id('vw_ContactListCount') IS NOT NULL)
	DROP VIEW vw_ContactListCount
GO
IF (OBJECT_Id('vw_ContactListCount') IS NULL)
EXEC sp_executesql N'
CREATE VIEW vw_ContactListCount
AS
select D.Id DistributionListId, ISNULL(CASE WHEN D.ListType =1 THEN DLS.[Count] ELSE ML.ContactCount END,0) ContactCount,DLS.SiteId
FROM TADistributionLists D
	LEFT JOIN (Select DistributionListId, count(*) ContactCount FROM TADistributionListUser Group By DistributionListId) ML
			ON (ML.DistributionListId =D.Id AND D.ListType<>1)
	LEFT JOIN TADistributionListSite DLS ON (DLS.DistributionListId = D.ID AND D.ListType=1)
	'
GO
PRINT 'Creating vwContactPropertiesAndAttributes'
GO
IF (OBJECT_Id('vwContactPropertiesAndAttributes') IS NOT NULL)
	DROP VIEW vwContactPropertiesAndAttributes
GO
IF (OBJECT_Id('vwContactPropertiesAndAttributes') IS NULL)
EXEC sp_executesql N'
CREATE VIEW vwContactPropertiesAndAttributes
AS
SELECT ContactId, AttributeId, AttributeEnumId, Value, Notes, CAV.CreatedDate, CAV.CreatedBy, CAV.CreatedDate AS ModifiedDate, CAV.CreatedBy AS ModifiedBy, ADT.Type as DataType FROM ATContactAttributeValue CAV INNER JOIN ATAttribute A ON A.ID = CAV.AttributeId INNER JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId WHERE (Value IS NOT NULL OR AttributeEnumId IS NOT NULL) AND ContactId IS NOT NULL
UNION 
SELECT Id AS ContactId,''11111111-0000-0000-0000-000000000000'', NULL, FirstName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, ''System.String''  FROM dbo.MKContact WHERE FirstName IS NOT NULL
UNION
SELECT Id AS ContactId,''22222222-0000-0000-0000-000000000000'', NULL, MiddleName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, ''System.String''  FROM dbo.MKContact WHERE MiddleName IS NOT NULL
UNION
SELECT Id AS ContactId,''33333333-0000-0000-0000-000000000000'', NULL, LastName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, ''System.String''  FROM dbo.MKContact WHERE LastName IS NOT NULL
UNION
SELECT Id AS ContactId,''44444444-0000-0000-0000-000000000000'', NULL, Email AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, ''System.String''  FROM dbo.MKContact WHERE Email IS NOT NULL
UNION
SELECT Id AS ContactId,''55555555-0000-0000-0000-000000000000'', NULL, CompanyName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, ''System.String''  FROM dbo.MKContact WHERE CompanyName IS NOT NULL
--UNION
--SELECT Id AS ContactId,''20000000-0000-0000-0000-000000000000'', NULL, CAST(BirthDate AS nvarchar(500))AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE BirthDate IS NOT NULL
--UNION
--SELECT Id AS ContactId,''50000000-0000-0000-0000-000000000000'', NULL, HomePhone AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE HomePhone IS NOT NULL
--UNION
--SELECT Id AS ContactId,''70000000-0000-0000-0000-000000000000'', NULL, MobilePhone AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE MobilePhone IS NOT NULL
--UNION
--SELECT Id AS ContactId,''80000000-0000-0000-0000-000000000000'', NULL, OtherPhone AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE OtherPhone IS NOT NULL
'

GO
PRINT 'Creating vw_ContactSite'
GO
IF (OBJECT_Id('vw_ContactSite') IS NOT NULL)
DROP VIEW vw_ContactSite
GO
IF (OBJECT_Id('vw_ContactSite') IS  NULL)
EXEC sp_executesql N'
CREATE VIEW [dbo].[vw_contactSite]
	AS 

	SELECT Distinct UserId,SiteId,SU.IsPrimarySite as IsPrimary
	FROM [dbo].USSiteUser SU 
	INNER JOIN USUser U on U.Id =SU.UserId
	Where U.Status!=3
	
	UNION ALL
	
	SELECT ContactId AS UserId, SiteId,IsPrimarySite as IsPrimary
	FROM [dbo].MKContactSite CS
'
GO
PRINT 'Creating vw_Address'
GO
--IF OBJECT_ID('') IS NOT NULL
--	DROP VIEW vw_Address
IF (OBJECT_Id('vw_Address') IS NULL)
EXEC sp_executesql N'CREATE VIEW [dbo].[vw_Address]
	AS 
select A.Id,
A.FirstName,
A.LastName,
A.AddressType,
A.AddressLine1,
A.AddressLine2,
A.AddressLine3,
A.City,
A.StateId,
A.Zip,
A.CountryId,
A.Phone,
A.CreatedDate,
A.CreatedBy,
A.ModifiedDate,
A.ModifiedBy,
A.Status,
A.StateName,
A.County,
C.CountryName,
C.CountryCode, 
S.State,
S.StateCode
from GLAddress A LEFT JOIN GLCountry C ON A.CountryId = C.Id
LEFT JOIN GLState S ON A.StateId = S.Id  
	
'  
	
GO

PRINT 'Altering View vwContact'
GO



ALTER VIEW [dbo].[vw_contacts]
AS
SELECT     
	U.Id UserId, 
	U.[FirstName],
	U.[MiddleName], 
	U.[LastName],
	U.[CompanyName],
	U.[BirthDate],
	U.[Gender],
	U.[AddressId],
	[Status], U.[HomePhone],
	U.[MobilePhone], 
	U.[OtherPhone], 
	U.[ImageId], 
	U.[Notes], 
	U.Email,
	0 ContactType,
	ContactSourceId,
	CreatedBy,
	CreatedDate,
	ModifiedBy,
	ModifiedDate
FROM         
	MKContact U 
UNION ALL
SELECT     
	CP.[Id] UserId,
	U.[FirstName],
	U.[MiddleName],
	U.[LastName],
	U.[CompanyName],
	U.[BirthDate],
	U.[Gender],
	UA.[AddressId], 
	CASE WHEN CP.IsActive = 1 
	THEN U.[Status] 
	ELSE 2 END/* if Not Active return inactive*/,
	U.[HomePhone],
	U.[MobilePhone],
	U.[OtherPhone],
	U.[ImageId],
	NULL Notes, 
	M.Email, 
	1 ContactType,
	9 ContactSourceId,
	U.CreatedBy,
	U.CreatedDate,
	U.ModifiedBy,
	U.ModifiedDate
FROM        USUser U
	
	INNER JOIN  [USCommerceUserProfile] CP  ON CP.Id = U.Id 
	INNER JOIN USMembership M ON M.UserId = U.Id 
	LEFT JOIN (select top 1 * from USUserShippingAddress where UserId=Id )UA ON UA.UserId = U.Id AND UA.IsPrimary = 1
	LEFT JOIN dbo.USSiteUser SU ON SU.UserId = U.Id AND SU.IsSystemUser = 1
WHERE   
	U.Status !=3  and SU.UserId IS NULL
UNION ALL
SELECT 
DISTINCT 
	U.[Id] 'UserId',
	U.[FirstName], 
	U.[MiddleName], 
	U.[LastName],
	U.[CompanyName],
	U.BirthDate,
	U.[Gender],
	NULL AddressId,
	U.[Status],
	U.[HomePhone], 
	U.[MobilePhone],
	U.[OtherPhone], 
	U.[ImageId],
	NULL Notes, 
	M.Email, 
	(CASE WHEN Isnull(SU.IsSystemUser, 0) = 1 THEN 3 ELSE 2 END) ContactType, 
	(CASE WHEN Isnull(SU.IsSystemUser, 0) = 1 THEN 7 ELSE 8 END) ContactSourceId,
	U.CreatedBy,
	U.CreatedDate,
	U.ModifiedBy,
	U.ModifiedDate
FROM        
	USUser U 
	INNER JOIN USMemberShip M ON M.UserId = U.Id 
	LEFT JOIN USSiteUser SU ON U.Id = SU.UserId
	LEFT JOIN dbo.USCommerceUserProfile CUP ON CUP.Id = U.Id 
WHERE  U.Status !=3  and   CUP.Id IS NULL
GO




GO
PRINT 'Creating function GetContactListCount'
GO
IF (OBJECT_ID('GetContactListCount') IS NOT NULL)
	DROP FUNCTION GetContactListCount
GO
CREATE FUNCTION [dbo].[GetContactListCount]
(	
	@DistributionListId uniqueidentifier=null, 
	@SiteId uniqueidentifier=null
)
RETURNS TABLE 
AS
RETURN 
(
	Select DistributionListId,ContactCount,isnull(SiteId,@SiteId) SiteId
	from vw_ContactListCount
	Where (@DistributionListId IS NULL OR DistributionListId=@DistributionListId)
	AND (@SiteId is null OR SiteId is null OR SiteId=@SiteId)	
)

GO

PRINT 'Creating function Campaign_GetResponseSendDate'
GO
IF (OBJECT_ID('Campaign_GetResponseSendDate') IS NOT NULL)
	DROP FUNCTION Campaign_GetResponseSendDate
GO


CREATE FUNCTION [dbo].[Campaign_GetResponseSendDate]
(
	@StartedDate datetime,
	@EmailId uniqueidentifier --Campaignrunid is getting passed for responses
)
RETURNS DATETIME
AS
BEGIN
	--Get the campaignemail 
	declare @DatetimeToSend datetime
	declare @EmailTimeMeasurement varchar(100), @EmailTimeValue int

	SELECT @EmailTimeMeasurement=TimeMeasurement,@EmailTimeValue=TimeValue from MKcampaignEmail WHere Id=@EmailId

	if(@EmailTimeMeasurement = 'Mins')
		SET @DatetimeToSend = DATEADD(minute,@EmailTimeValue,@StartedDate)
	else if(@EmailTimeMeasurement = 'Hours')
		SET @DatetimeToSend = DATEADD(hour,@EmailTimeValue,@StartedDate)
	else if(@EmailTimeMeasurement = 'Days')
		SET @DatetimeToSend = DATEADD(day,@EmailTimeValue,@StartedDate)
	else if(@EmailTimeMeasurement = 'Month')
		SET @DatetimeToSend = DATEADD(month,@EmailTimeValue,@StartedDate)

	RETURN @DatetimeToSend
END
GO
PRINT 'Creating function Campaign_VerifyAutoReponsesCompleted'
GO
IF (OBJECT_ID('Campaign_VerifyAutoReponsesCompleted') IS NOT NULL)
	DROP FUNCTION Campaign_VerifyAutoReponsesCompleted
GO
CREATE FUNCTION [dbo].[Campaign_VerifyAutoReponsesCompleted]
(
	@ResponseId uniqueidentifier, --campaignid,
	@UserId uniqueidentifier,
	@ResponseLastSentDate datetime
)
RETURNS BIT
AS
BEGIN
	declare @ResponseStartDate datetime,@CampaignEmailId uniqueidentifier

	--select @ResponseLastSentDate=Min(LastSent) from MKResponseFlow where ResponseId=@ResponseId 
	
	select TOP 1 @ResponseStartDate=DateStarted from MKResponseFlow where ResponseId=@ResponseId and UserId=@UserId and Completed is null

	select TOP 1 @CampaignEmailId=Id from MKcampaignEmail where CampaignId=@ResponseId order by Sequence desc

	if(@ResponseLastSentDate >= dbo.Campaign_GetResponseSendDate(@ResponseStartDate,@CampaignEmailId) )
		RETURN 1;

	RETURN 0;
END

GO
PRINT 'Creating function iAppsForm_GetLeadCountByFormId'
GO

IF (OBJECT_ID('iAppsForm_GetLeadCountByFormId') IS NOT NULL)
	DROP FUNCTION iAppsForm_GetLeadCountByFormId
GO
CREATE FUNCTION [dbo].[iAppsForm_GetLeadCountByFormId] 
(
	@FormId	UNIQUEIDENTIFIER
)
RETURNS INT AS
BEGIN
	DECLARE	@LeadCount INT
	SET		@LeadCount = (	
		SELECT	COUNT(DISTINCT UserId)
		FROM	FormsResponse
		WHERE	FormsId = @FormId AND
				UserId IN (
					SELECT	Id
					FROM	MKContact
					WHERE	Status = 1
				)
	)
	
	RETURN	@LeadCount
END


GO
PRINT 'Creating function VerifyGlobalListExistense'
GO

IF (OBJECT_ID('VerifyGlobalListExistense') IS NOT NULL)
	DROP FUNCTION VerifyGlobalListExistense
GO
CREATE FUNCTION [dbo].[VerifyGlobalListExistense]
(
	@SiteId uniqueidentifier,
	@ListGroupId uniqueidentifier
)
RETURNS BIT
AS
BEGIN
	
	IF EXISTS(SELECT 1 FROM TADistributionListGroup G JOIN TADistributionLists L ON G.DistributionId = L.Id
	WHERE G.DistributionGroupId=@ListGroupId AND L.IsGlobal = 1)
		RETURN 1
	
	RETURN 0;
END

GO

PRINT 'Creating function GetTotalContactsCount'
GO

IF (OBJECT_ID('GetTotalContactsCount') IS NOT NULL)
	DROP FUNCTION GetTotalContactsCount
GO
CREATE FUNCTION [dbo].[GetTotalContactsCount](@ObjectTypeId INT , @Id uniqueidentifier, @SiteId uniqueidentifier) 

RETURNS INT
AS
BEGIN
	DECLARE @TotalContacts INT	
	DECLARE @ContactListType int	
	SET @TotalContacts = 0
	SELECT @ContactListType = ListType, @TotalContacts = ISNULL(DLS.[Count], 0) FROM TADistributionLists DL
	LEFT JOIN TADistributionListSite DLS ON DLS.DistributionListId = DL.ID AND DLS.SiteId = ISNULL(@SiteId, DL.ApplicationId)
	 where DL.Id = @Id	
	
	IF (@ObjectTypeId = 302 AND @ContactListType != 1)
		SET @TotalContacts = (select COUNT(1) from TADistributionListUser where DistributionListId=@Id and ContactListSubscriptionType != 3)
		
	RETURN @TotalContacts
	
END
GO

PRINT 'Creating function GetTotalCountByObjectType'
GO

IF (OBJECT_ID('GetTotalCountByObjectType') IS NOT NULL)
	DROP FUNCTION GetTotalCountByObjectType
GO
CREATE FUNCTION [dbo].[GetTotalCountByObjectType]
(
	@SiteId Uniqueidentifier = NULL,
	@ObjectId int,
	@CountForObjectTypeId int, --For this obejct type 
	@CountByObjectTypeId int -- For the @CountForObjectTypeId, get the count of objects of type @CountByObjectTypeId
)
RETURNS INT
AS
BEGIN
	RETURN 0;
	/*
	--For campaign object type, get the count of contact type objects
	IF(@CountForObjectTypeId = 301 AND @CountByObjectTypeId = 302)
	BEGIN
		
	END
	ELSE IF(@CountForObjectTypeId = 301 AND @CountByObjectTypeId = 302)
	BEGIN
		
	END
	*/
END

GO
PRINT 'Altering procedure GetTotalContactListsCount'
GO

IF (OBJECT_ID('GetTotalContactListsCount') IS NOT NULL)
	DROP FUNCTION GetTotalContactListsCount
GO
CREATE FUNCTION [dbo].[GetTotalContactListsCount]
(
	@DistributionGroup UNIQUEIDENTIFIER	
)
RETURNS INT
AS
BEGIN
	
	DECLARE @ContactListsRecordsCount INT
	
	SET @ContactListsRecordsCount = (SELECT COUNT(DistributionId) FROM TADistributionListGroup
                                     WHERE DistributionGroupId = @DistributionGroup)
	
	RETURN @ContactListsRecordsCount

END
GO
PRINT 'Altering procedure Users_ImportDropBatchTables'
GO
ALTER PROCEDURE [dbo].[Users_ImportDropBatchTables]
(
	@BatchUserTable varchar(250),
	@BatchMembershipTable varchar(250),
	@BatchProfileTable varchar(250),
	@BatchSiteUserTable varchar(250),
	@BatchUserDistributionListTable varchar(250),
	@BatchUSMarketierUserTable  varchar(250),
	@BatchIndexTermTable varchar(250),
	@BatchAddressTable varchar(250),
	@BatchImportIdMap varchar(250)
)
As
Begin


IF  EXISTS (SELECT * FROM sys.objects WHERE name= @BatchUserTable )
	Exec('Drop Table ' + @BatchUserTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name= @BatchMembershipTable)
	Exec('Drop Table ' + @BatchMembershipTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name=  @BatchProfileTable)
	Exec('Drop Table ' + @BatchProfileTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name= @BatchSiteUserTable )
	Exec('Drop Table ' + @BatchSiteUserTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name=  @BatchUserDistributionListTable)
	Exec('Drop Table ' + @BatchUserDistributionListTable)


IF  EXISTS (SELECT * FROM sys.objects WHERE name=  @BatchUserDistributionListTable)
	Exec('Drop Table ' + @BatchUserDistributionListTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name=  @BatchUSMarketierUserTable)
	Exec('Drop Table ' + @BatchUSMarketierUserTable)


IF  EXISTS (SELECT * FROM sys.objects WHERE name=  @BatchIndexTermTable)
	Exec('Drop Table ' + @BatchIndexTermTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name= @BatchAddressTable )
	Exec('Drop Table ' + @BatchAddressTable)

IF  EXISTS (SELECT * FROM sys.objects WHERE name= @BatchImportIdMap )
	Exec('Drop Table ' + @BatchImportIdMap)


End
GO
PRINT 'Altering procedure Membership_BuildUser'
GO
ALTER PROCEDURE [dbo].[Membership_BuildUser] 
(
	@tbUser			TYUser READONLY,
	@SortBy			nvarchar(100) = NULL,
	@SortOrder		nvarchar(10) = NULL,	
	@PageNumber		int = NULL,
	@PageSize		int = NULL
)
AS
BEGIN
	IF @SortBy IS NULL SET @SortBy = 'USERNAME'
	IF @SortOrder IS NULL SET @SortOrder = 'ASC'
	SET @SortBy = UPPER(@SortBy)
	SET @SortOrder = UPPER(@SortOrder)

	DECLARE @PageLowerBound int
	DECLARE @PageUpperBound int
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	ELSE
		SET @PageUpperBound = 0

	DECLARE @tbPagedUser TYUser
	;WITH CTEUsers AS(
		SELECT ROW_NUMBER() OVER (
			ORDER BY
				CASE WHEN (@SortBy = 'USERNAME' AND @SortOrder = 'ASC') THEN U.UserName END ASC,
				CASE WHEN (@SortBy = 'USERNAME' AND @SortOrder = 'DESC') THEN U.UserName END DESC,
				CASE WHEN (@SortBy = 'FIRSTNAME' AND @SortOrder = 'ASC') THEN U.FirstName END ASC,
				CASE WHEN (@SortBy = 'FIRSTNAME' AND @SortOrder = 'DESC') THEN U.FirstName END DESC,
				CASE WHEN (@SortBy = 'LASTNAME' AND @SortOrder = 'ASC') THEN U.LastName END ASC,
				CASE WHEN (@SortBy = 'LASTNAME' AND @SortOrder = 'DESC') THEN U.LastName END DESC,
				CASE WHEN (@SortBy = 'STATUS' AND @SortOrder = 'ASC') THEN U.Status END ASC,
				CASE WHEN (@SortBy = 'STATUS' AND @SortOrder = 'DESC') THEN U.Status END DESC,
				CASE WHEN (@SortBy = 'EMAIL' AND @SortOrder = 'ASC') THEN U.Email END ASC,
				CASE WHEN (@SortBy = 'EMAIL' AND @SortOrder = 'DESC') THEN U.Email END DESC
			) AS RowNumber,
			U.*
		FROM @tbUser U
	)
		
	INSERT INTO @tbPagedUser
	SELECT
		Id,    
		UserName,      
		Email,    
		PasswordQuestion,      
		IsApproved,    
		CreatedDate,    
		LastLoginDate,    
		LastActivityDate,    
		LastPasswordChangedDate,    
		IsLockedOut,    
		LastLockoutDate,  
		FirstName,  
		LastName,  
		MiddleName,  
		ExpiryDate,  
		EmailNotification,  
		TimeZone,  
		ReportRangeSelection,  
		ReportStartDate,  
		ReportEndDate,   
		BirthDate,  
		CompanyName,  
		Gender,  
		HomePhone,  
		MobilePhone,  
		OtherPhone,  
		ImageId,
		Status					
	FROM CTEUsers
	WHERE (@PageNumber is null) OR (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound) 
		
	SELECT * FROM @tbPagedUser

	SELECT COUNT(*) FROM @tbUser
		
	SELECT UserId ,             
		PropertyName,
		PropertyValueString, 
		PropertyValuesBinary,
		LastUpdatedDate
	FROM dbo.USMemberProfile P, @tbPagedUser T
	WHERE P.UserId = T.Id 
				
	SELECT U.UserId, U.SecurityLevelId, S.Title  
	FROM USUserSecurityLevel U
		JOIN USSecurityLevel S ON U.SecurityLevelId = S.Id 
		JOIN @tbPagedUser T ON T.Id = U.UserId
END
GO
PRINT 'Altering procedure CampaignResponseInfo_GetAllByDateRange'
GO
ALTER PROCEDURE [dbo].[CampaignResponseInfo_GetAllByDateRange]    
(    
 @ApplicationId uniqueidentifier = null,    
 @PageSize NVARCHAR(MAX)=null,       
 @PageNumber INT=null ,    
 @SortColumn nVARCHAR(25)=null,    
 @Type int =1,    
 @CreatedDate datetime = null,    
 @StatusIn nvarchar(50)  =null,    
 @TotalRecords int output    
)    
AS    
Begin    
    
 DECLARE @SQL nvarchar(max),@params nvarchar(100)      
    
 SET @params = N'@CreatedDate datetime, @TotalRecords int output'      
    
select @SortColumn = (case @SortColumn when '' then 'Title'  else @SortColumn  end)    
    
SET @SQL = N'     
 --stats    
 Select A.*,(case when A.Delivered >0 then convert(decimal(10,2),(convert(decimal(10,2), A.Opens) / 
			convert(decimal(10,2), A.Delivered)) * 100) else 0 end ) as DeliveredToOpens,
			(case when A.Delivered >0 then convert(decimal(10,2),(convert(decimal(10,2), A.Clicks) / 
			convert(decimal(10,2), A.Delivered)) * 100) else 0 end ) as DeliveredToClicks,    
			(case when A.Delivered >0 then convert(decimal(10,2),(convert(decimal(10,2), A.TriggeredWatches) / 
			convert(decimal(10,2), A.Delivered)) * 100) else 0 end ) as DeliveredToWatches,    
			(case when A.TriggeredWatches>0 and A.CostPerEmail >0  then  
				convert(decimal(10,2),(convert(decimal(10,2), A.Sends)* A.CostPerEmail)/
				convert(decimal(10,2), A.TriggeredWatches)) else 0 end) as CostPerTrigger
				
into #tmpRunHistory
From (
Select MK.CampaignId,sum(MK.Sends) as Sends,sum(MK.Delivered) as Delivered,sum(MK.Opens) as Opens,sum(MK.Clicks) as Clicks,    
sum(MK.TriggeredWatches) as TriggeredWatches,sum(MK.CostPerEmail) CostPerEmail, isnull(sum(SL.UniqueOpen),0) As UniqueOpen
From MKCampaignRunHistory AS MK     
 Left join 
	( SELECT  CampaignRunId ,Isnull(Count([Opens]),0) as UniqueOpen FROM MKEmailSendLog Where Opens > 0 Group by CampaignRunId) SL on MK.Id = SL.CampaignRunId 
	 group by MK.CampaignId
 ) A  
    
    
 SELECT C.ID,C.[Title]   ,C.[Description]   ,C.[CampaignGroupId]   ,C.[Status] ,C.[CreatedDate]  ,    
   R.CampaignId , isnull(R.Sends,0) as Sends , isnull(R.Delivered,0) Delivered, isnull(R.Opens,0) Opens,isnull(R.Clicks,0) Clicks,isnull(R.TriggeredWatches,0) TriggeredWatches,isnull(R.DeliveredToOpens,0) DeliveredToOpens, isnull(R.DeliveredToClicks,0) DeliveredToClicks,    
   isnull(R.DeliveredToWatches,0) DeliveredToWatches,isnull(R.CostPerTrigger,0) CostPerTrigger,(case status when 1 then ''Draft'' when 2 then ''Active'' when 3 then ''Completed'' when 4 then ''Archived'' else ''None'' end ) as StatusText,
   isnull(R.UniqueOpen,0) UniqueOpen
 into #tmpResponseRunHistory    
 FROM    
 #tmpRunHistory R right JOIN  MKCampaign C on R.CampaignId = C.Id     
 where ApplicationId =  Isnull('''+ cast(@ApplicationId as nvarchar(36))+ ''', ApplicationId) AND C.Type = isnull('    
  +  cast( @Type as varchar(2)) + ', C.Type) and C.[CreatedDate] >=isnull(@CreatedDate,C.[CreatedDate]) and C.[Status] in (' + isnull(@StatusIn,'1,2,3,4') + ')    
     
     
    
 --with campaign details    
 SELECT ROW_NUMBER() OVER(    
   ORDER BY  ' + @SortColumn + ' )    
   as Row_Id, *     
 into #tmpPaginatedRunHistory    
 FROM    
 #tmpResponseRunHistory
    
    
    
 --with & with no pagination    
 IF('+ cast(isnull(@PageSize,0) as nvarchar(5)) + '> 0 and ' +  cast(isnull(@PageNumber,0) as nvarchar(5)) + ' >0)    
 BEGIN    
      
  select @TotalRecords = isnull(max(Row_Id),0) from #tmpPaginatedRunHistory    
      
  select *    
  from #tmpPaginatedRunHistory where Row_ID >= ('+ cast(isnull(@PageSize,0) as nvarchar(5)) + ' * ' +      
  cast(isnull(@PageNumber,0) as nvarchar(5)) + ') - ('+ cast(isnull(@PageSize,0) as nvarchar(5)) + ' -1) AND     
  Row_ID <= '+ cast(isnull(@PageSize,0) as nvarchar(5)) + ' * ' +  cast(isnull(@PageNumber,0) as nvarchar(5)) + '     
      
    
 END    
 ELSE    
 BEGIN    
  select @TotalRecords = max(Row_Id) from #tmpPaginatedRunHistory    
    
  select * from #tmpPaginatedRunHistory order by 3    
 END'    
     
 --SET @SQL = @SQL +     
 --'     
     
 --SELECT C.EmailSubject, A.CampaignRunId, A.CampaignId, Count(A.Opens) As Delivered, Sum(A.Opens) As Opens,     
 --Sum(A.Clicks) As Clicks, Sum(A.TriggeredWatches) As TriggeredWatches    
 --FROM MKEmailSendLog A ,  #tmpPaginatedRunHistory B , MKCampaignEmail C    
 --WHERE A.CampaignId = B.Id  AND A.CampaignId = C.CampaignId And A.CampaignRunId = C.Id      
 --Group by A.CampaignId, A.CampaignRunId, C.EmailSubject'    
    
 SET @SQL = @SQL +    
 '    
     
     
  Select C.Id, C.EmailSubject, A.CampaignId, A.Sends , A.Delivered, A.Opens , A.Clicks ,    
  TriggeredWatches ,     
 (case when Delivered >0 then convert(decimal(10,2),(convert(decimal(10,2), Opens) / convert(decimal(10,2),  Delivered )) * 100) else 0 end ) as DeliveredToOpens,    
 (case when Delivered >0 then convert(decimal(10,2),(convert(decimal(10,2), Clicks) / convert(decimal(10,2),  Delivered )) * 100) else 0 end ) as DeliveredToClicks,    
 (case when Delivered >0 then convert(decimal(10,2),(convert(decimal(10,2), TriggeredWatches) / convert(decimal(10,2),  Delivered )) * 100) else 0 end ) as DeliveredToWatches,    
 (case when TriggeredWatches>0 and CostPerEmail >0  then  convert(decimal(10,2),(convert(decimal(10,2),  Sends )* CostPerEmail )/convert(decimal(10,2),  TriggeredWatches )) else 0 end) as CostPerTrigger,
 dbo.GetUniqueOpenInCampaignMail(A.Id) As UniqueOpen    
 From MKCampaignRunHistory A, MKCampaignEmail C     
 WHERE A.Id = C.Id  AND A.CampaignId IN (SELECT Id FROM #tmpPaginatedRunHistory 
 where Row_ID >= ('+ cast(isnull(@PageSize,0) as nvarchar(5)) + ' * ' +      
  cast(isnull(@PageNumber,0) as nvarchar(5)) + ') - ('+ cast(isnull(@PageSize,0) as nvarchar(5)) + ' -1) AND     
  Row_ID <= '+ cast(isnull(@PageSize,0) as nvarchar(5)) + ' * ' +  cast(isnull(@PageNumber,0) as nvarchar(5)) + '  ) '    
     
 
    
print @SQL    
    
EXEC sp_executesql       
      
      @SQL,      
      
      @params,      
 @CreatedDate=@CreatedDate,    
     @TotalRecords=@TotalRecords  output    
     
    
    
End

GO
PRINT 'Altering procedure Taxonomy_ReplaceRelationReference'
GO
ALTER PROCEDURE [dbo].[Taxonomy_ReplaceRelationReference] 
(
	@TaxonomyIds	xml,
	@ObjectId		uniqueidentifier,
	@ObjectTypeId	int,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier = null
)
AS
BEGIN
	DECLARE @UtcNow DateTime		
	SET @UtcNow = getutcdate()

	DECLARE @tbIds AS TABLE(Id uniqueidentifier)
	INSERT INTO @tbIds
	SELECT DISTINCT tab.col.value('text()[1]','uniqueidentifier')
		FROM  @TaxonomyIds.nodes('/GenericCollectionOfGuid/guid')tab(col)

	DELETE FROM COTaxonomyObject Where ObjectId = @ObjectId 
		AND TaxonomyId IN (SELECT Id FROM COTaxonomy WHERE ApplicationId = @ApplicationId)
		
	INSERT INTO COTaxonomyObject
	(
		Id,
		TaxonomyId,
		ObjectId,
		ObjectTypeId,
		SortOrder,
		Status,
		CreatedBy,
		CreatedDate,
		ModifiedBy,
		ModifiedDate
	)
	SELECT DISTINCT
		NEWID(),
		Id,
		@ObjectId, 
		@ObjectTypeId,
		1, 
		1,
		@ModifiedBy, 
		@UtcNow,
		@ModifiedBy, 
		@UtcNow
	FROM
		@tbIds

	IF @@Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,'')
		RETURN dbo.GetDataBaseErrorCode()
	END

END


GO
PRINT 'Altering procedure CampaignBase_Save'

GO
ALTER PROCEDURE [dbo].[CampaignBase_Save]
(
	@Id uniqueidentifier,
	@Title varchar(50),
	@Description varchar(250),
	@CampaignGroupId uniqueidentifier,
	@Type int=1,
	@Status int = null,
	@SenderName varchar(250) = '',
	@SenderEmail varchar(250) = '',
	@ConfirmationEmail varchar(250) = null,
	@ModifiedBy uniqueidentifier,
	@ApplicationId  uniqueidentifier = null,
	@SenderNameProperty nvarchar(250) = null,
	@SenderEmailProperty nvarchar(250) = null
)
AS
Begin

	If exists(select * from MKCampaign Where Id not in (@Id) and Title = @Title and Type=@Type)
	Begin 
		if(@Type=1)
		RAISERROR ( 'The campaign name already exists.  Please enter a unique name.' , 16, 1,'')
		if(@Type=2)
			RAISERROR ( 'The response name already exists.  Please enter a unique name.' , 16, 1,'')
	END
	If not exists(select * from MKCampaign Where Id = @Id)
	Begin
		DECLARE @CreatedDate datetime = GETUTCDATE()
			
		-- insert
		Insert Into MKCampaign(
			id, 
			Title, 
			Description, 
			CampaignGroupId,
			Type,
			Status, 
			SenderName, 
			SenderEmail,
			ConfirmationEmail,	
			CreatedBy, 
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			ApplicationId,
			SenderNameProperty,
			SenderEmailProperty
			)
		Values (
			@Id, 
			@Title, 
			@Description, 
			@CampaignGroupId, 
			@Type,
			@Status,
			@SenderName, 
			@SenderEmail,
			@ConfirmationEmail,
			@ModifiedBy, 
			@CreatedDate,
			@ModifiedBy,
			@CreatedDate,
			@ApplicationId,
			@SenderNameProperty,
			@SenderEmailProperty
			)
	End
	Else
	Begin
		-- update
		Update MKCampaign Set
			Title = @Title,
			Description = @Description,
			CampaignGroupId = @CampaignGroupId,
			Status = @Status,
			SenderName = @SenderName, 
			SenderEmail = @SenderEmail,
			ConfirmationEmail = @ConfirmationEmail,
			ModifiedBy = @ModifiedBy,
			ModifiedDate =  GetUTCDate(),
			SenderNameProperty = @SenderNameProperty,
			SenderEmailProperty = @SenderEmailProperty
			
		Where Id = @Id
	End
End

GO
PRINT 'Altering procedure iAppsForm_GetFormsByHierarchy'
GO
ALTER PROCEDURE [dbo].[iAppsForm_GetFormsByHierarchy]  
(  
 @Id     uniqueIdentifier=null ,  
 @ApplicationId  uniqueidentifier=null,  
 @Level    int=null,  
 @PageIndex   int=1,  
 @PageSize   int=null   
)  
AS  
BEGIN  
   
 SELECT    
  C.Id,      
  C.ApplicationId,  
  C.Title,  
  C.Description,    
  C.FormType,     
  H.ObjectTypeId,   
  C.CreatedDate,  
  C.CreatedBy,  
  C.ModifiedBy,  
  C.ModifiedDate,  
  C.XMLString,  
  C.XSLTString,  
  C.XSLTFileName,   
  C.Status,  
  C.SendEmailYesNo,  
  C.Email,  
  C.ThankYouURL,  
  C.SubmitButtonText,    
  C.PollResultType,   
  C.PollVotingType,  
  C.AddAsContact,  
  C.ContactEmailField,  
  C.WatchId,  
  C.EmailSubjectText,  
  C.EmailSubjectField,  
  C.SourceFormId,  
  C.ParentId,
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  dbo.iAppsForm_GetResponseCountByFormId(C.Id) ResponseCount,
  dbo.iAppsForm_GetLeadCountByFormId(C.Id) AS LeadCount,
  dbo.iAppsForm_GetResponseDateByFormId(C.Id) ResponseDate  
 FROM Forms C   
 INNER JOIN dbo.GetChildrenByHierarchyType(@Id,@Level,38) H ON    
  C.ParentId = H.Id       AND  
  C.Status != dbo.GetDeleteStatus() AND   
  H.ObjectTypeId IN (38)    AND      
  (@ApplicationId IS NULL OR C.ApplicationId = @ApplicationId)  
 LEFT OUTER JOIN FormsResponse F ON   
  FormsId = C.Id      AND  
  F.ResponseDate IS NOT NULL AND F.ResponseDate = NULL    
  LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy  
 ORDER By C.Title    
END

GO
PRINT 'Altering procedure iAppsForm_GetFormsResponse'
GO
ALTER PROCEDURE [dbo].[iAppsForm_GetFormsResponse]
(	
	@FormsId			Uniqueidentifier=null,
	@UserId				Uniqueidentifier = null,
	@StartDate			DateTime = null,
	@EndDate			DateTime = null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
BEGIN
--********************************************************************************
-- code
--********************************************************************************
	IF (@StartDate IS NOT NULL AND @EndDate IS NOT NULL)
	BEGIN

	SELECT
			R.Id,
			R.FormsId,
			R.FormsResponseXML,
			R.ResponseDate,
			R.UserId,
			FN.UserFullName CreatedByFullName,
			--M.LoweredEmail,
			R.IPAddress,
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') ElementId,
			R.FormsResponseXML.value('(/FormElements/FormElement/@value)[1]','varchar(256)') ElementValue
	FROM 
			FormsResponse R	LEFT JOIN Forms F ON R.FormsId = F.Id AND
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') = F.XMLString.value('(/FormElements/FormElement/@id)[1]','varchar(256)') AND F.Status	= 1	
			--JOIN USUser U ON U.Id = R.UserId AND U.Status	= 1 JOIN USMembership M ON U.Id = M.UserId
			LEFT JOIN VW_UserFullName FN on FN.UserId = R.UserId
	WHERE				
			R.ResponseDate > @StartDate AND R.ResponseDate < @EndDate 
			AND (@FormsId IS NULL OR R.FormsId = @FormsId)
			AND (@UserId IS NULL OR R.UserId =@UserId)

	ORDER BY  
			R.ResponseDate DESC	
	END
	ELSE
	BEGIN
	SELECT
			R.Id,
			R.FormsId,
			R.FormsResponseXML,
			R.ResponseDate,
			R.UserId,
			FN.UserFullName CreatedByFullName,
			--M.LoweredEmail,
			R.IPAddress,
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') ElementId,
			R.FormsResponseXML.value('(/FormElements/FormElement/@value)[1]','varchar(256)') ElementValue
	FROM 
			FormsResponse R	LEFT JOIN Forms F ON R.FormsId = F.Id AND
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') = F.XMLString.value('(/FormElements/FormElement/@id)[1]','varchar(256)') AND F.Status	= 1	
			--JOIN USUser U ON U.Id = R.UserId AND U.Status	= 1 JOIN USMembership M ON U.Id = M.UserId
			LEFT JOIN VW_UserFullName FN on FN.UserId = R.UserId
	WHERE				
			(@FormsId IS NULL OR R.FormsId = @FormsId)
			AND (@UserId IS NULL OR R.UserId =@UserId)
	ORDER BY  
			R.ResponseDate DESC	
	END
END

GO

PRINT 'Altering procedure iAppsForm_SaveFormsResponse'
GO
ALTER PROCEDURE [dbo].[iAppsForm_SaveFormsResponse]
(
	@Id							uniqueidentifier out,
	@FormsId	   				uniqueidentifier,
	@FormsResponseXML			xml = NULL,
	@IPAddress					nvarchar(100),
	@UserId						uniqueidentifier	
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE 
		@Now	  datetime,
		@stmt	  varchar(256)
BEGIN
--********************************************************************************
-- code
--********************************************************************************
	
	SET @Now = getUTCdate()
	
	IF (@Id is null)			-- New INSERT
	BEGIN
		SET @stmt = 'INSERT'
		SET @Id = newid();
			INSERT INTO FormsResponse(
									Id	                    ,
									FormsId   				,
									FormsResponseXML	    ,
									ResponseDate            ,
									UserId					,
									IPAddress				)
						values (
									@Id	                    ,
									@FormsId				,
									@FormsResponseXML   	,
									@Now					,
									@UserId					,
									@IPAddress				)		
    END
    ELSE			-- update
         BEGIN
			SET @stmt = 'UPDATE'
			UPDATE	FormsResponse	WITH (ROWLOCK)		SET 			
								FormsResponseXML	=	@FormsResponseXML,
								UserId				=   @UserId,
								ResponseDate		=   @Now,
								IPAddress			=	@IPAddress
 			WHERE 	Id    = @Id AND  FormsId = @FormsId
		END
END


GO



PRINT 'Altering procedure CampaignEmail_Get'
GO
ALTER PROCEDURE [dbo].[CampaignEmail_Get]  
(  
 @Id uniqueidentifier =null ,  
 @CampaignId uniqueidentifier=null  
   
)  
as  
begin  
 select Id,CampaignId,EmailSubject,EmailHtml,CMSPageId, EmailText, Sequence, TimeValue, TimeMeasurement, dbo.GetTemplateName(CMSPageId) As TemplateName, EmailHtmlPageVersion, EmailContentType, EmailContentTypePriority from MKCampaignEmail   
   where (@CampaignId IS NULL OR @CampaignId=CampaignId) and (@Id IS NULL OR @Id=Id) order by Sequence  
  
 select Id, CampaignEmailId, CampaignId, ContentId from MKCampaignFileAttachment  where (@CampaignId IS NULL OR @CampaignId=CampaignId) and (@Id IS NULL OR @Id=CampaignEmailId) 
end

GO
PRINT 'Altering procedure CampaignGroup_Get'
GO
ALTER PROCEDURE [dbo].[CampaignGroup_Get]
(
	@Id uniqueidentifier = null,
	@ApplicationId uniqueidentifier 
)
AS
Begin
	Select ApplicationId,
	Id
,Name
,CreatedBy
,CreatedDate AS CreatedDate
,ModifiedBy
,ModifiedDate AS ModifiedDate
	From MKCampaignGroup
	Where (@Id IS NULL OR @Id=Id)
	AND ApplicationId = @ApplicationId
order by name
End

GO
PRINT 'Altering procedure CampaignGroup_Save'
GO
ALTER PROCEDURE [dbo].[CampaignGroup_Save]
(
	@Id uniqueidentifier,
	@Name varchar(50),
	@CreatedBy uniqueidentifier,
	@CreatedDate datetime,
	@ModifiedBy uniqueidentifier,
	@ModifiedDate datetime, 
	@ApplicationId uniqueidentifier
)
AS
Begin
	If not exists(select * from MKCampaignGroup Where Id = @Id)
	Begin
		
		IF @CreatedDate is null
			SET @CreatedDate = GetUTCDate()
		ELSE
			Set @CreatedDate = dbo.ConvertTimeToUtc(@CreatedDate,@ApplicationId)

		Insert Into MKCampaignGroup (Id, Name, CreatedBy, CreatedDate, ApplicationId)
		Values (@Id, @Name, @CreatedBy, @CreatedDate,@ApplicationId)
	End
	Else
	Begin
		IF @ModifiedDate is null
			SET @ModifiedDate =GetUTCDate()
		ELSE
			Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

		Update MKCampaignGroup Set
			Name = @Name,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @ModifiedDate
		Where Id = @Id 
	End
End

GO
PRINT 'Altering procedure CampaignResponseInfo_Get'
GO
ALTER PROCEDURE [dbo].[CampaignResponseInfo_Get]  
(  
 @Id uniqueidentifier,
 @ApplicationId uniqueidentifier
)  
AS  
Begin  

SELECT 
	Id
	,Title
	,Description
	,Type
	,CampaignGroupId
	,Status
	,SenderName
	,SenderEmail
	,ConfirmationEmail
	,CreatedBy
	,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate
	,ModifiedBy
	,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
	,ApplicationId
	,SenderNameProperty
	,SenderEmailProperty
 FROM [dbo].[MKCampaign]  Where (@Id IS NULL OR @Id=dbo.GetEmptyGuid() OR @Id=Id)  And Type = 2 AND @ApplicationId=ApplicationId

SELECT 
	Id
	,CampaignId
	,EmailSubject
	,EmailHtml
	,CMSPageId
	,EmailText
	,Sequence
	,TimeValue
	,TimeMeasurement
	,EmailContentType
	,EmailContentTypePriority
FROM dbo.MKCampaignEmail E WHERE E.CampaignId = @Id

SELECT 
	Id
	,CampaignEmailId
	,CampaignId
	,ContentId
FROM MkCampaignFileAttachment WHERE CampaignId = @Id

SELECT 
	CampaignId
	,ResponseProviderName
	,ResponseProviderOptions
FROM dbo.MKCampaignResponseInfo WHERE  CampaignId = @Id

End

GO
PRINT 'Altering procedure CampaignResponseInfo_GetAll '
GO
ALTER PROCEDURE [dbo].[CampaignResponseInfo_GetAll]
(
@ApplicationId uniqueidentifier = NULL
)
AS
Begin
	Select 
		CampaignId,
		ResponseProviderName,
		ResponseProviderOptions
	From MKCampaignResponseInfo RI JOIN MKCampaign C ON RI.CampaignId=C.Id AND (@ApplicationId IS NULL OR @ApplicationId=ApplicationId)
End

GO
PRINT 'Altering procedure CampaignResponseInfo_GetUsageByDateRange'
GO
--exec CampaignResponseInfo_GetUsageByDateRange @StartDate='2010-01-01 20:27:01:000',@EndDate='2010-02-03 20:27:01:000',@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',
--@ReportType = 4

ALTER PROCEDURE [dbo].[CampaignResponseInfo_GetUsageByDateRange](
@ApplicationId uniqueidentifier = null,
@StartDate Datetime,
@EndDate Datetime,
@ReportType int
)
AS

--StartDate and EndDate parameter value is UTC


DECLARE @OrigStartDate datetime

DECLARE @TmpTable Table 
(
	Title nvarchar(100),
	RptYear int,
	RptWeekNo int,
	NoOfEmailSent bigint
)
Declare @TmpWeekly Table
(
	Title nvarchar(100),
	SendDate Datetime,
	NoOfEmailSent bigint
)


		if(@ReportType=1)
		BEGIN
			SELECT b.Title,convert(nvarchar(10),a.SendDate,101) as SendDate, isnull(SUM(a.NoOfEmailSent),0) as NoOfEmailSent 
			from MKEmailPerDayLog a, SISite b
			where a.SendDate between @StartDate and @EndDate and (@ApplicationId IS NULL OR @ApplicationId=a.ApplicationId) and a.ApplicationId=b.Id
			GROUP BY b.Title,a.SendDate order by a.SendDate
		END
		ELSE if(@ReportType=2)
		BEGIN


--			SET @OrigStartDate = @StartDate
--			SET @StartDate = DATEADD(wk, DATEDIFF(wk, 6, @StartDate) + (DATEPART(wk,@StartDate)-1), 6)

			insert into @TmpTable SELECT b.Title as Title,DATEPART(YEAR,SendDate) AS 'RptYear',DATEPART(wk,SendDate) AS 'RptWeekNo',      isnull(SUM(NoOfEmailSent),0) AS 'NoOfEmailSent'
			FROM MKEmailPerDayLog , SISite b where b.Id=ApplicationId and SendDate between @StartDate and @EndDate and (@ApplicationId IS NULL OR @ApplicationId=ApplicationId)
			GROUP BY b.Title,DATEPART(YEAR,SendDate), DATEPART(wk,SendDate) order by DATEPART(wk,SendDate) 

			insert into @TmpWeekly SELECT Title,DATEADD(wk, DATEDIFF(wk, 6, '1/1/' + cast(RptYear as varchar(10))) + (RptWeekNo-1), 6) AS SendDate,NoOfEmailSent
			FROM @TmpTable  order by Title,RptWeekNo,RptYear

			
		select Title,convert(nvarchar(10),SendDate,101) as Startweek,convert(nvarchar(10),SendDate + (7 - DATEPART(DW,  SendDate)),101) as WeekEndDate, NoOfEmailSent from 
		@TmpWeekly order by Title,SendDate

		END
		ELSE if(@ReportType=3)
		BEGIN

			insert into @TmpTable SELECT b.Title as Title,DATEPART(YEAR,SendDate) AS 'Year',DATEPART(Month,SendDate) AS 'Month',      isnull(SUM(NoOfEmailSent),0) AS 'NoOfMails'
			FROM MKEmailPerDayLog, SISite b where  b.Id=ApplicationId and SendDate between @StartDate and @EndDate and (@ApplicationId IS NULL OR @ApplicationId=ApplicationId)
			GROUP BY  b.Title,DATEPART(YEAR,SendDate), DATEPART(Month,SendDate) order by DATEPART(Month,SendDate) 
			
			Select Title,DateName( month , DateAdd( month , RptWeekNo , 0 ) - 1 ) +' '+ cast(RptYear as varchar(10)) as SendDate,NoOfEmailSent
			from @TmpTable
		END
		ELSE if(@ReportType=4)
		BEGIN
			SELECT  b.Title,DATEPART(YEAR,SendDate)  as SendDate,      isnull(SUM(NoOfEmailSent),0) AS 'NoOfMails'
			FROM MKEmailPerDayLog, SiSite b where b.Id=ApplicationId and SendDate between @StartDate and @EndDate and (@ApplicationId IS NULL OR @ApplicationId=ApplicationId)
			GROUP BY b.Title,DATEPART(YEAR,SendDate) order by DATEPART(YEAR,SendDate)

		END

GO
PRINT 'Altering procedure CampaignRunHistoryWatchStats_GetByCampaign '
GO
ALTER PROCEDURE [dbo].[CampaignRunHistoryWatchStats_GetByCampaign]
(
	@CampaignId uniqueidentifier = null
)
AS
Begin
	SELECT 
		s.Id
		,s.CampaignRunId
		,s.WatchId
		,s.TriggerCount
		,TriggerPercent
		, w.Name 'WatchName'
	from MKCampaignRunHistoryWatchStats s
		join MKCampaignRunHistory h on h.Id = s.CampaignRunId
		join ALWatch w on w.Id = s.WatchId
	Where (@CampaignId IS NULL OR @CampaignId=h.CampaignId)
End

GO
PRINT 'Altering procedure CampaignRunHistory_GetByDate'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetByDate]
(
	@StartDate datetime = null,
	@EndDate datetime = null,
	@ApplicationId uniqueidentifier
)
AS
Begin

	IF @StartDate is null
		SET @StartDate =GetUTCDate()
	ELSE
		Set @StartDate = dbo.ConvertTimeToUtc(@StartDate,@ApplicationId)

	IF @EndDate is null
		SET @EndDate = GetUTCDate()
	ELSE
		Set @EndDate = dbo.ConvertTimeToUtc(@EndDate,@ApplicationId)

	Select CampaignId,sum(Sends) as Sends,sum(Delivered) as Delivered,sum(Opens) as Opens,sum(Clicks) as Clicks,
	sum(TriggeredWatches) as TriggeredWatches, 
	(case when sum(Delivered) >0 then sum(Opens) /sum(Delivered) else 0 end ) as DeliveredToOpens,
	(case when sum(Delivered) >0 then sum(Clicks) /sum(Delivered) else 0 end ) as DeliveredToClicks,
	(case when sum(Delivered) >0 then sum(TriggeredWatches) /sum(Delivered) else 0 end ) as DeliveredToWatches,
	(case when sum(TriggeredWatches)>0 and sum(CostPerEmail) >0  then  (sum(Sends)*sum(CostPerEmail))/sum(TriggeredWatches) else 0 end) as CostPerTrigger
	From MKCampaignRunHistory CH JOIN MKCampaign C ON C.Id = CH.CampaignId AND ApplicationId=@ApplicationId
	Where RunDate >= IsNull(@StartDate, RunDate)
		And RunDate < DateAdd(dd, 1, IsNull(@StartDate, RunDate))
	group by CampaignId
End

GO
PRINT 'Altering procedure CampaignRunHistory_GetDistinctCampaignRunDates'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetDistinctCampaignRunDates]
(
	@CampaignId uniqueidentifier,
	@CampaignRunId uniqueidentifier = null,
	@ApplicationId uniqueidentifier 
)
AS
Begin
	Select 
		Distinct dbo.ConvertTimeFromUtc(RunDate,@ApplicationId) Date 
	From MKCampaignRunHistory
	Where CampaignId = @CampaignId
		and (@CampaignRunId IS NULL OR @CampaignRunId=Id)
End

GO
PRINT 'Altering procedure Campaign_Get'
GO
ALTER PROCEDURE [dbo].[Campaign_Get]  
(  
	@Id uniqueidentifier = null,  
	@ApplicationId uniqueidentifier = null, 
	@Type int = null  ,
	@PageSize INT=null,   
	@PageNumber INT=null ,
	@SortColumn nVARCHAR(25)=null
)  
AS  
Begin  


if(@Id is null)
	BEGIN 

   SELECT ROW_NUMBER() OVER(ORDER BY a.CampaignId) as Row_Id, C.[Id]  ,C.[Title]   ,C.[Description]   ,C.[CampaignGroupId]   ,C.[Status]     ,C.[SenderName]  
		  ,C.[SenderEmail]       ,A.[CostPerEmail]     ,C.[ConfirmationEmail]    ,C.[CreatedBy]  ,dbo.ConvertTimeFromUtc(C.[CreatedDate] ,@ApplicationId)   CreatedDate 
		  ,C.[ModifiedBy]       ,  dbo.ConvertTimeFromUtc(C.[ModifiedDate] ,@ApplicationId) ModifiedDate        ,C.[ApplicationId]        ,A.[ScheduleId]        ,A.[WorkflowStatus]  
		  ,A.[UniqueRecipientsOnly]        ,A.[AutoUpdateLists]        , dbo.ConvertTimeFromUtc(A.[LastPublishDate] ,@ApplicationId) LastPublishDate        ,A.[WorkflowId]        ,A.[AuthorId]
		  ,A.[SendToNotTriggered], C.[SenderNameProperty], C.[SenderEmailProperty], A.UseLocalTimeZoneToSend
	  FROM [dbo].[MKCampaign] C  
	 Left JOIN MKCampaignAdditionalInfo A on C.Id = A.CampaignId  
	 Where (@Id IS NULL OR C.Id=@Id) And (@ApplicationId is null OR ApplicationId=@ApplicationId)  AND ( @Type  IS NULL OR C.Type = @Type)
	order by c.Title
	  
	END 
ELSE
	BEGIN

	 SELECT C.[Id]  ,C.[Title]   ,C.[Description]   ,C.[CampaignGroupId]   ,C.[Status]     ,C.[SenderName]  
		  ,C.[SenderEmail]       ,A.[CostPerEmail]     ,C.[ConfirmationEmail]    ,C.[CreatedBy]  , dbo.ConvertTimeFromUtc(C.[CreatedDate] ,@ApplicationId)   CreatedDate
		  ,C.[ModifiedBy]       ,dbo.ConvertTimeFromUtc(C.[ModifiedDate] ,@ApplicationId) ModifiedDate        ,C.[ApplicationId]        ,A.[ScheduleId]        ,A.[WorkflowStatus]  
		  ,A.[UniqueRecipientsOnly]        ,A.[AutoUpdateLists]        , dbo.ConvertTimeFromUtc(A.[LastPublishDate] ,@ApplicationId) LastPublishDate        ,A.[WorkflowId]        ,A.[AuthorId]  
		  ,A.[SendToNotTriggered], C.[SenderNameProperty], C.[SenderEmailProperty], A.UseLocalTimeZoneToSend
	  FROM [dbo].[MKCampaign] C  
	 Left JOIN MKCampaignAdditionalInfo A on C.Id = A.CampaignId  
	 Where (@Id IS NULL OR C.Id = @Id) And (@ApplicationId IS NULL OR @ApplicationId = ApplicationId)  AND (@Type IS NULL OR @Type = C.Type )
	  


		 SELECT E.Id, E.CampaignId ,E.[EmailSubject] ,E.[EmailHtml]  
			  ,E.[CMSPageId]  
			  ,E.[EmailText]  
		   ,E.TimeValue  
		   ,E.TimeMeasurement  
		   ,E.Sequence
		   ,E.EmailContentType
		   ,E.EmailContentTypePriority
		 FROM dbo.MKCampaignEmail E  
		 WHERE E.CampaignId = @Id  


		  
		 SELECT FA.Id, FA.CampaignEmailId, FA.CampaignId, FA.ContentId  
		 FROM dbo.MKCampaignFileAttachment FA  
		 WHERE FA.CampaignId = @Id  

		 exec [Campaign_GetRelevantWatch] @Id,@ApplicationId
	END  
  
End


GO
PRINT 'Altering procedure Campaign_GetCampaignHistoryByUser'
GO
ALTER PROCEDURE [dbo].[Campaign_GetCampaignHistoryByUser]
(
	@UserId uniqueidentifier , 
	@LastNoOfDays Int, 
	@PageSize INT=null,   
	@PageNumber INT=null ,
	@SortColumn nVARCHAR(25)=null,
	@ApplicationId uniqueidentifier
)
AS

	IF(isnull(@PageSize,0)= 0 and isnull(@PageNumber,0) =0)
	BEGIN 
    		SELECT Row_Id,newid() Id,a.CampaignId,b.Title, a.Sends, a.Opens,a.Clicks from (
			SELECT ROW_NUMBER() OVER(ORDER BY a.CampaignId) as Row_Id ,a.CampaignId, Count(*) 'Sends', Sum(a.Opens) 'Opens', Sum(a.Clicks) 'Clicks'
			from MkEmailSendLog a  where   a.UserId=@UserId AND (@ApplicationId IS NULL OR a.ApplicationId=@ApplicationId)
			Group By  a.CampaignId) a ,MKCampaign b where a.CampaignId = b.Id  AND (@ApplicationId IS NULL OR b.ApplicationId=@ApplicationId) ORDER BY CASE isnull(@SortColumn ,'')
		    when 'Name' then 3  when 'Sends' then Sends when 'Opens' then Opens when 'Clicks' then Clicks else 3 end

	END
	ELSE 
	BEGIN


			declare @RecordCount int
			Declare @HistroyTable Table (
								Row_id int, Id uniqueidentifier,CampaignId uniqueidentifier, Name nvarchar(1024),Sends int ,Opens int, Clicks int)


			insert into @HistroyTable 
			SELECT Row_Id,newid() Id,a.CampaignId,b.Title, a.Sends, a.Opens,a.Clicks  from (

					SELECT ROW_NUMBER() OVER(ORDER BY a.CampaignId) as Row_Id ,a.CampaignId, Count(*) 'Sends', Sum(a.Opens) 'Opens', Sum(a.Clicks) 'Clicks'
					from MkEmailSendLog a where a.SendDate >= DATEADD(day, -(@LastNoOfDays) , getUTCdate()) and   a.UserId=@UserId  AND (@ApplicationId IS NULL OR a.ApplicationId=@ApplicationId)
				Group By   a.CampaignId
			) a  , MKCampaign b where a.CampaignId = b.Id AND (@ApplicationId IS NULL OR b.ApplicationId=@ApplicationId)

			
			select @RecordCount=count(*) from @HistroyTable

			select *,@RecordCount as RecordCount from @HistroyTable where 
			Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
					ORDER BY CASE isnull(@SortColumn ,'')
				when 'Name' then 3  when 'Sends' then Sends when 'Opens' then Opens when 'Clicks' then Clicks else 3 end

	END

GO
PRINT 'Altering procedure Campaign_GetCampaignsBasedonNextActor'
GO
ALTER PROCEDURE [dbo].[Campaign_GetCampaignsBasedonNextActor]    
(    
 @Id uniqueidentifier = null,    
 @ApplicationId uniqueidentifier = null,    
 @ProductId uniqueidentifier = null,   
 @ObjectTypeId int = null,  
 @UserId uniqueidentifier = null ,
 @Type int = 1 
)    
AS    
Begin    
	Set @Type = isnull(@Type,1)
--  Declare @Id uniqueidentifier   
--  Declare @ApplicationId uniqueidentifier   
--  Declare @ProductId uniqueidentifier   
--  Declare @ObjectTypeId int  
--  Declare @UserId uniqueidentifier  
  
  Declare @lastLoginDate DateTime, @lastActionedDate DateTime  
  SELECT Top 1 @lastLoginDate = LastLoginDate FROM USMembership Order by LastLoginDate desc  
  set @lastLoginDate = isnull(@lastLoginDate, getUTCDate())  
  
  SELECT Top 1 @lastActionedDate = ActionedDate FROM  WFWorkflowExecution Order by ActionedDate desc  
  set @lastActionedDate = isnull(@lastActionedDate, getUTCDate())  
  
  IF ( (datePart(day,@lastActionedDate) <> datePart(day,@lastLoginDate)) OR   
  (datePart(month,@lastActionedDate) <> datePart(month,@lastLoginDate)) OR   
  (datePart(year,@lastActionedDate) <> datePart(year,@lastLoginDate) ))     
  BEGIN  
  exec [dbo].Workflow_GetWorkflowExecutionToSkipScheduler  
  END  
  
  Select  cm.[Id]  
      ,cm.[Title]  
      ,cm.[Description]  
      ,cm.[CampaignGroupId]  
      ,cm.[Status]  
      ,cm.[SenderName]  
      ,cm.[SenderEmail]  
      ,A.[CostPerEmail]  
      ,cm.[ConfirmationEmail]  
      ,cm.[CreatedBy]  
      ,cm.[CreatedDate]  
      ,cm.[ModifiedBy]  
      ,cm.[ModifiedDate]  
      ,cm.[ApplicationId]  
      ,A.[ScheduleId]  
      ,A.[WorkflowStatus]  
      ,A.[UniqueRecipientsOnly]  
      ,A.[AutoUpdateLists]  
      ,A.[LastPublishDate]  
      ,A.[WorkflowId]  
      ,A.[AuthorId]   
     ,na.ObjectId     
     ,na.ObjectTypeId    
     ,na.SequenceId    
     ,na.NextActorId    
     ,na.NextActorType   
	 ,A.[SendToNotTriggered]
	 ,cm.[SenderNameProperty]
	 ,cm.[SenderEmailProperty]
	 ,A.UseLocalTimeZoneToSend
  From MKCampaign  cm   
 INNER JOIN MKCampaignAdditionalInfo A on cm.Id = A.CampaignId,  
  WFNextActor na, WFWorkflowExecution ex, WFWorkflow wf  
  Where cm.Type = @Type AND (@Id IS NULL OR @Id=cm.Id) AND (@ApplicationId IS NULL OR cm.ApplicationId = @ApplicationId ) AND A.WorkflowId  = na.WorkflowId AND  
  na.ObjectId = cm.Id AND (@ObjectTypeId IS NULL OR @ObjectTypeId=na.ObjectTypeId) AND  
  ex.WorkflowId = na.WorkflowId  AND ex.ObjectId  = na.ObjectId AND ex.ObjectTypeId = na.ObjectTypeId AND ex.Completed = 0 AND  
     (@ApplicationId IS NULL OR @ApplicationId=wf.ApplicationId) AND    
     (@ProductId IS NULL OR @ProductId=wf.ProductId) AND    
     wf.Id   = A.WorkflowId           
  AND  
  (  
  (@UserId Is null ) --- In case of admin  
  OR   
  (  
   (@UserId Is not null ) AND   
   (  
    (na.NextActorType = 1 AND (@UserId IS NULL OR @UserId=na.NextActorId) )   
     OR  
    (na.NextActorType = 2 AND @UserId IN (  
      SELECT Id FROM [dbo].[User_GetUsersInGroup]   
      (@ProductId,@ApplicationId,na.NextActorId,1,@UserId,1)   
     )       
    )  
   )  
  )  
  )  
End




GO
PRINT 'Altering procedure Campaign_GetCampaignsForTemplate'
GO
ALTER PROCEDURE [dbo].[Campaign_GetCampaignsForTemplate]
(
	@TemplateId uniqueidentifier,
	@ApplicationId uniqueidentifier
)
AS
BEGIN
	SELECT		pd.PageDefinitionId, mke.CampaignId, mkg.Name AS [Group], mkc.Title AS Campaign
	FROM		PageDefinition pd
				INNER JOIN MKCampaignEmail mke	ON mke.CMSPageId = pd.PageDefinitionId
				INNER JOIN MKCampaign mkc		ON mkc.Id = mke.CampaignId
				INNER JOIN MKCampaignGroup mkg	ON mkg.Id = mkc.CampaignGroupId
	WHERE		pd.TemplateId = @TemplateId AND (@ApplicationId IS NULL OR @ApplicationId=mkc.ApplicationId)
	ORDER BY	[Group] ASC, Campaign ASC
END

GO
PRINT 'Altering procedure Campaign_GetCampaignsWithNextActor'
GO
ALTER PROCEDURE [dbo].[Campaign_GetCampaignsWithNextActor]    
(    
 @Id uniqueidentifier = null,    
 @ApplicationId uniqueidentifier = null,    
 @ProductId uniqueidentifier = null,   
 @ObjectTypeId int = null ,
 @Type int = 1 
)    
AS    
Begin    
	Set @Type = isnull(@Type,1)
--  Declare @Id uniqueidentifier   
--  Declare @ApplicationId uniqueidentifier   
--  Declare @ProductId uniqueidentifier   
--  Declare @ObjectTypeId int  
  
  Declare @lastLoginDate DateTime, @lastActionedDate DateTime  
  SELECT Top 1 @lastLoginDate = LastLoginDate FROM USMembership Order by LastLoginDate desc  
  set @lastLoginDate = isnull(@lastLoginDate, getUTCDate())  
  
  SELECT Top 1 @lastActionedDate = ActionedDate FROM  WFWorkflowExecution Order by ActionedDate desc  
  set @lastActionedDate = isnull(@lastActionedDate, getUTCDate())  
  
  IF ( (datePart(day,@lastActionedDate) <> datePart(day,@lastLoginDate)) OR   
  (datePart(month,@lastActionedDate) <> datePart(month,@lastLoginDate)) OR   
  (datePart(year,@lastActionedDate) <> datePart(year,@lastLoginDate) ))     
  BEGIN  
  exec [dbo].Workflow_GetWorkflowExecutionToSkipScheduler  
  END  
  
  Select cm.[Id]  
      ,cm.[Title]  
      ,cm.[Description]  
      ,cm.[CampaignGroupId]  
      ,cm.[Status]  
      ,cm.[SenderName]  
      ,cm.[SenderEmail]  
      ,A.[CostPerEmail]  
      ,cm.[ConfirmationEmail]  
      ,cm.[CreatedBy]  
      ,cm.[CreatedDate]  
      ,cm.[ModifiedBy]  
      ,cm.[ModifiedDate]  
      ,cm.[ApplicationId]  
      ,A.[ScheduleId]  
      ,A.[WorkflowStatus]  
      ,A.[UniqueRecipientsOnly]  
      ,A.[AutoUpdateLists]  
      ,A.[LastPublishDate]  
      ,A.[WorkflowId]  
      ,A.[AuthorId]   
     ,na.ObjectId     
     ,na.ObjectTypeId    
     ,na.SequenceId    
     ,na.NextActorId    
     ,na.NextActorType    
	 ,A.[SendToNotTriggered]
	 ,cm.[SenderNameProperty]
	 ,cm.[SenderEmailProperty]
	 ,A.UseLocalTimeZoneToSend
  From MKCampaign  cm  
 INNER JOIN MKCampaignAdditionalInfo A on cm.Id = A.CampaignId  
  Left Join WFNextActor na on  A.WorkflowId  = na.WorkflowId AND na.ObjectId = cm.Id AND (@ObjectTypeId IS NULL OR @ObjectTypeId=na.ObjectTypeId)   
  Left Join WFWorkflowExecution ex on A.WorkflowId  = ex.WorkflowId AND ex.WorkflowId = na.WorkflowId  AND ex.ObjectId  = na.ObjectId AND ex.ObjectTypeId = na.ObjectTypeId AND ex.Completed = 0   
  Left Join WFWorkflow wf on A.WorkflowId  = wf.Id AND wf.Id   = A.WorkflowId  And (@ApplicationId IS NULL OR @ApplicationId=wf.ApplicationId) AND  (@ProductId IS NULL OR @ProductId=wf.ProductId)   
  Where cm.Type = @Type AND (@Id IS NULL OR @Id=cm.Id) AND (@ApplicationId IS NULL OR @ApplicationId=cm.ApplicationId)   
            
End
GO

PRINT 'Altering procedure Campaign_Save'
GO
ALTER PROCEDURE [dbo].[Campaign_Save]
(
	@CampaignId uniqueidentifier,
	@CostPerEmail decimal(10, 2) = null,
	@ScheduleId	uniqueidentifier = null,
	@WorkFlowStatus int = 0,
	@UniqueRecipientsOnly bit=null,
	@AutoUpdateLists bit=null,
	@LastPublishDate DateTime=null,
	@WorkFlowId uniqueidentifier = null,
	@AuthorId uniqueidentifier =null,
	@ApplicationId uniqueidentifier,
	@SendToNotTriggered bit = null,
	@UseLocalTimeZoneToSend bit=0
)
AS
Begin

if(@WorkFlowId ='00000000-0000-0000-0000-000000000000')
set @WorkFlowId = null

if(@ScheduleId ='00000000-0000-0000-0000-000000000000')
set @ScheduleId = null

if(@AuthorId ='00000000-0000-0000-0000-000000000000')
set @AuthorId = null

	If not exists(select * from dbo.MKCampaignAdditionalInfo Where CampaignId = @CampaignId)
	Begin
	
	INSERT INTO [dbo].[MKCampaignAdditionalInfo]
           ([Id]
           ,[CampaignId]
           ,[CostPerEmail]
           ,[ScheduleId]
           ,[WorkflowStatus]
           ,[UniqueRecipientsOnly]
           ,[AutoUpdateLists]
           ,[LastPublishDate]
           ,[WorkflowId]
           ,[AuthorId]
			,[SendToNotTriggered]
			,[UseLocalTimeZoneToSend])
     VALUES
          ( newid()
           ,@CampaignId
           ,@CostPerEmail
           ,@ScheduleId
           ,@WorkflowStatus
           ,@UniqueRecipientsOnly
           ,@AutoUpdateLists
           ,@LastPublishDate
           ,@WorkflowId
           ,@AuthorId
			,@SendToNotTriggered
			,@UseLocalTimeZoneToSend)
	End
	Else
	Begin
	

		UPDATE [dbo].[MKCampaignAdditionalInfo]
		   SET [CostPerEmail] = @CostPerEmail
			  ,[ScheduleId] = @ScheduleId
			  ,[WorkflowStatus] = @WorkflowStatus
			  ,[UniqueRecipientsOnly] = @UniqueRecipientsOnly
			  ,[AutoUpdateLists] = @AutoUpdateLists
			  ,[LastPublishDate] = @LastPublishDate
			  ,[WorkflowId] = @WorkflowId
			  ,[AuthorId] = @AuthorId
				,[SendToNotTriggered] = @SendToNotTriggered
				,[UseLocalTimeZoneToSend]=@UseLocalTimeZoneToSend
		 WHERE [CampaignId] = @CampaignId
	End
End

GO
PRINT 'Altering procedure Campaign_GetContacts'
GO
ALTER PROCEDURE [dbo].[Campaign_GetContacts](
	@CampaignId uniqueidentifier, @ApplicationId uniqueidentifier
	)
As
Begin
		declare @contactIds varchar(max),@RowCount INT,@ContactId uniqueidentifier

		DECLARE @ContactTab AS TABLE(Id UNIQUEIDENTIFIER, RowNum INT)

		INSERT INTO @ContactTab
		Select UserId , ROW_NUMBER() OVER (ORDER BY Id ) AS Row from MKCampaignUser where CampaignId = @CampaignId 

		SELECT @RowCount = ISNULL(MAX(RowNum),0) FROM @ContactTab

		set @contactIds = '<GenericCollectionOfGuids>'
		WHILE (@RowCount > 0)
		BEGIN
			SELECT @ContactId = Id FROM @ContactTab WHERE [RowNum]=@RowCount

			SET @contactIds = @contactIds + '<guid>' + cast(@ContactId as varchar(36)) + '</guid>'

			SET @RowCount = @RowCount - 1
		END
			
		set @contactIds = @contactIds + '</GenericCollectionOfGuids>'


		Declare @xmlContactIds xml
		set @xmlContactIds = cast(@contactIds as xml)
			Exec dbo.Contact_GetContactByIds @ContactIds =@xmlContactIds, @ApplicationId = @ApplicationId, @Status=0, @SortOrder='',@MaxRecords =0
		
End

GO
PRINT 'Altering procedure Campaign_GetContactsDraft'
GO
ALTER PROCEDURE [dbo].[Campaign_GetContactsDraft](
	@CampaignId uniqueidentifier, @ApplicationId uniqueidentifier)
As
Begin
		declare @contactIds varchar(max),@RowCount INT,@ContactId uniqueidentifier

		DECLARE @ContactTab AS TABLE(Id UNIQUEIDENTIFIER, RowNum INT)

		INSERT INTO @ContactTab
		Select UserId , ROW_NUMBER() OVER (ORDER BY Id ) AS Row from MKCampaignUserDraft where CampaignId = @CampaignId 

		SELECT @RowCount = ISNULL(MAX(RowNum),0) FROM @ContactTab

		set @contactIds = '<GenericCollectionOfGuids>'
		WHILE (@RowCount > 0)
		BEGIN
			SELECT @ContactId = Id FROM @ContactTab WHERE [RowNum]=@RowCount

			SET @contactIds = @contactIds + '<guid>' + cast(@ContactId as varchar(36)) + '</guid>'

			SET @RowCount = @RowCount - 1
		END
			
		set @contactIds = @contactIds + '</GenericCollectionOfGuids>'


		Declare @xmlContactIds xml
		set @xmlContactIds = cast(@contactIds as xml)
			Exec dbo.Contact_GetContactByIds @ContactIds =@xmlContactIds, @ApplicationId = @ApplicationId, @Status=0, @SortOrder='',@MaxRecords =0
		
End

GO
PRINT 'Altering procedure Campaign_GetSubscribedCampaignsByUserId'
GO
ALTER PROCEDURE [dbo].[Campaign_GetSubscribedCampaignsByUserId]
(
	@UserId			UNIQUEIDENTIFIER,
	@ApplicationId	UNIQUEIDENTIFIER,
	@UnsubscribedFromFutureCampaigns	BIT OUTPUT
)
AS
BEGIN
	DECLARE @SubscribedCampaigns TABLE (
		CampaignId	UNIQUEIDENTIFIER
	)
	
	-- Get all campaigns that the specified user has received emails from
	INSERT INTO	@SubscribedCampaigns (CampaignId)
		SELECT	DISTINCT CampaignId
		FROM	MKEmailSendLog
		WHERE	UserId = @UserId
		
	-- Remove any campaigns that the user is unsubscribed from
	DELETE
	FROM	@SubscribedCampaigns
	WHERE	CampaignId IN (
				SELECT	CampaignId
				FROM	USUserUnsubscribe AS U
				WHERE	U.UserId = @UserId AND
						U.CampaignId IS NOT NULL
			) OR (
				EXISTS (
					SELECT	*
					FROM	USUserUnsubscribe AS U
					WHERE	U.UserId = @UserId AND
							U.CampaignId IS NULL
				) AND
				CampaignId NOT IN (
					SELECT	CampaignId
					FROM	USResubscribe AS R
					WHERE	R.UserId = @UserId AND
							R.CampaignId IS NOT NULL
				)
			)
	
	-- Set output parameter
	IF EXISTS (
		SELECT	*
		FROM	USUserUnsubscribe
		WHERE	UserId = @UserId AND
				CampaignId IS NULL
	)
		BEGIN
			SET	@UnsubscribedFromFutureCampaigns = 1
		END
	ELSE
		BEGIN
			SET	@UnsubscribedFromFutureCampaigns = 0
		END		
	
	-- Return campaign information
	SELECT	C.Id ,C.Title, C.[Description], C.CampaignGroupId, C.[Status], C.SenderName, C.SenderEmail, A.CostPerEmail, C.ConfirmationEmail,
			C.CreatedBy, dbo.ConvertTimeFromUtc(C.CreatedDate, @ApplicationId) AS CreatedDate, C.ModifiedBy,
			dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) AS ModifiedDate, C.ApplicationId, A.ScheduleId,
			A.WorkflowStatus, A.UniqueRecipientsOnly, A.AutoUpdateLists, dbo.ConvertTimeFromUtc(A.LastPublishDate, @ApplicationId) AS LastPublishDate,
			A.WorkflowId, A.AuthorId, A.SendToNotTriggered
	FROM	MKCampaign AS C
			LEFT JOIN MKCampaignAdditionalInfo AS A ON C.Id = A.CampaignId
	WHERE	C.Id IN (
				SELECT	DISTINCT CampaignId
				FROM	@SubscribedCampaigns
			) AND
			C.[Type] = 1 AND
			C.[Status] != 4 AND C.ApplicationId = @ApplicationId
	ORDER BY	C.Title ASC
END

GO
PRINT 'Altering procedure Campaign_GetToRun'
GO
ALTER PROCEDURE [dbo].[Campaign_GetToRun]
(
@ApplicationId	uniqueIdentifier=null,
@CurrentDate DateTime=null
)
AS
Begin

	IF (@CurrentDate is null)
		SET @CurrentDate = GetUTCDATE()
	ELSE
		SET @CurrentDate = dbo.ConvertTimeToUtc(@CurrentDate,@ApplicationId)

	Select 
		C.Id
		,C.Title
		,C.Description
		,C.Type
		,C.CampaignGroupId
		,C.Status
		,C.SenderName
		,C.SenderEmail
		,C.ConfirmationEmail
		,C.CreatedBy
		,dbo.ConvertTimeFromUtc(C.[CreatedDate] ,@ApplicationId) CreatedDate
		,C.ModifiedBy
		,dbo.ConvertTimeFromUtc(C.[ModifiedDate] ,@ApplicationId) ModifiedDate
		,C.ApplicationId
	From MKCampaign C
		Join MKCampaignAdditionalInfo a on c.Id = a.CampaignId
		JOIN [dbo].[TASchedule] S ON S.Id = a.ScheduleId
  Where 
	((C.Status = 2 and NextRunTime <= @CurrentDate And (isnull(s.MaxOccurrence, 0) <= 0 or s.RunCount < s.MaxOccurrence)) or C.Status = 7)	
	AND S.ApplicationId =isnull(@ApplicationId,S.ApplicationId)	
	
End


GO
PRINT 'Altering procedure Campaign_GetUnsubscribedCampaignsByUserId'
GO
ALTER PROCEDURE [dbo].[Campaign_GetUnsubscribedCampaignsByUserId]
(
	@UserId			UNIQUEIDENTIFIER,
	@ApplicationId	UNIQUEIDENTIFIER,
	@UnsubscribedFromFutureCampaigns	BIT OUTPUT
)
AS
BEGIN
	DECLARE @UnsubscribedCampaigns TABLE (
		CampaignId	UNIQUEIDENTIFIER
	)
	
	-- Get all individual campaigns that the specified user has unsubscribed
	INSERT INTO	@UnsubscribedCampaigns (CampaignId)
		SELECT	DISTINCT CampaignId
		FROM	USUserUnsubscribe
		WHERE	UserId = @UserId AND
				CampaignId IS NOT NULL
	
	-- Set output parameter
	IF EXISTS (
		SELECT	*
		FROM	USUserUnsubscribe
		WHERE	UserId = @UserId AND
				CampaignId IS NULL
	)
		BEGIN
			SET	@UnsubscribedFromFutureCampaigns = 1
			
			-- Get all campaigns in the system that the user is not resubscribed to
			INSERT INTO	@UnsubscribedCampaigns (CampaignId)
				SELECT	Id
				FROM	MKCampaign AS C
				WHERE	ApplicationId = @ApplicationId AND
						Id NOT IN (SELECT CampaignId FROM USResubscribe WHERE UserId = @UserId) AND
						Id NOT IN (SELECT CampaignId FROM @UnsubscribedCampaigns)
		END
	ELSE
		BEGIN
			SET	@UnsubscribedFromFutureCampaigns = 0
		END		
	
	-- Return campaign information
	SELECT	C.Id ,C.Title, C.[Description], C.CampaignGroupId, C.[Status], C.SenderName, C.SenderEmail, A.CostPerEmail, C.ConfirmationEmail,
			C.CreatedBy, dbo.ConvertTimeFromUtc(C.CreatedDate, @ApplicationId) AS CreatedDate, C.ModifiedBy,
			dbo.ConvertTimeFromUtc(C.ModifiedDate, @ApplicationId) AS ModifiedDate, C.ApplicationId, A.ScheduleId,
			A.WorkflowStatus, A.UniqueRecipientsOnly, A.AutoUpdateLists, dbo.ConvertTimeFromUtc(A.LastPublishDate, @ApplicationId) AS LastPublishDate,
			A.WorkflowId, A.AuthorId, A.SendToNotTriggered
	FROM	MKCampaign AS C
			LEFT JOIN MKCampaignAdditionalInfo AS A ON C.Id = A.CampaignId
	WHERE	C.Id IN (
				SELECT	DISTINCT CampaignId
				FROM	@UnsubscribedCampaigns
			) AND
			C.[Type] = 1 AND
			C.[Status] != 4 AND
			C.ApplicationId = @ApplicationId

	ORDER BY	C.Title ASC
END

GO
PRINT 'Altering procedure Campaign_GetWatchGoals'
GO

ALTER Procedure [dbo].[Campaign_GetWatchGoals]
(
	@CampaignId UniqueIdentifier=null,
	@ApplicationId Uniqueidentifier,
	@Status int=null,
	@OnlyActiveCampaigns bit=0
)
AS
Begin

		Select 	distinct 
           S.[CampaignWatchId] as Id
           ,S.[WatchId]
           ,S.[CampaignId]
           ,S.[TargetQuantity]
           ,dbo.ConvertTimeFromUtc(S.[TargetCompletion],ApplicationId) TargetCompletion
           ,S.[CampaignName]
           ,S.[WatchName] Name
           ,S.[CurrentActualQuantity]
           ,dbo.ConvertTimeFromUtc([FirstRunDate],ApplicationId) FirstRunDate
           ,S.[EstimatedCompletionQuantity]
           ,S.[Status]
     FROM vwCampaignWatchStats S
     Where (@CampaignId IS NULL OR @CampaignId=CampaignId)
     AND (@Status is null OR (S.Status & @Status)in (1,2))
    AND (@OnlyActiveCampaigns=0 OR CampaignStatus=2)
		
	
End


GO
PRINT 'Altering procedure Campaign_IsNameAvailable'
GO

ALTER PROCEDURE [dbo].[Campaign_IsNameAvailable]
(
--********************************************************************************
@CampaignName nvarchar(256),
@ApplicationId uniqueidentifier,
@Id uniqueidentifier
--********************************************************************************
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

--********************************************************************************
begin
	select count(*) from dbo.MKCampaign where lower(LTRIM(RTRIM(Title))) = lower(LTRIM(RTRIM(@CampaignName)))
	and ApplicationId =@ApplicationId and Id not in (@Id) and Type = 1 and Status != 8
end


GO
PRINT 'Altering procedure Contact_Get'
GO
ALTER PROCEDURE [dbo].[Contact_Get]
(
	@Id uniqueidentifier=null,
	@Email	nvarchar(256)=null,
	@ApplicationId uniqueidentifier
)
as
begin


SELECT C.Id [UserId]
      ,[FirstName]
	  ,[MiddleName]
      ,[LastName]
      ,[CompanyName]
      ,dbo.ConvertTimeFromUtc(BirthDate,@ApplicationId)BirthDate  
      ,[Gender]
      ,[AddressId]
	  ,C.[Status]
      ,[HomePhone]
      ,[MobilePhone]
      ,[OtherPhone]
      ,ImageId
	  ,[Notes]
	  ,Email
	  ,0 ContactType
	  ,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate 
	  ,CreatedBy
	  ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId) ModifiedDate
	  ,ModifiedBy
	  ,[ContactSourceId]
  FROM MKContact C
  INNER JOIN MKContactSite CS ON C.Id = CS.ContactId
  WHERE (@Id is null OR C.Id = @Id )
		AND (@Email is null OR Email = @Email)
		AND CS.SiteID = @ApplicationId

UNION ALL
	SELECT CP.[Id] UserId
		  ,U.[FirstName]
		  ,U.[MiddleName]
		  ,U.[LastName]
		  ,U.[CompanyName]
		  ,dbo.ConvertTimeFromUtc(U.BirthDate,@ApplicationId)BirthDate
		  ,U.[Gender]
		  ,(Select top 1 AddressId from dbo.USUserShippingAddress where UserId=Isnull(@Id,CP.Id) and IsPrimary=1) as AddressId
		  ,CP.[Status]
		  ,U.[HomePhone]
		  ,U.[MobilePhone]
		  ,U.[OtherPhone]
		  ,U.ImageId
		  ,NULL Notes
		  ,M.Email
		  ,1 ContactType
		  ,dbo.ConvertTimeFromUtc(U.CreatedDate,@ApplicationId)CreatedDate 
		  ,U.CreatedBy
		  ,dbo.ConvertTimeFromUtc(U.ModifiedDate,@ApplicationId) ModifiedDate
		  ,U.ModifiedBy
		  ,1 ContactSourceId
	 FROM [USCommerceUserProfile] CP
	 INNER JOIN USUser U on CP.Id=U.Id
	 INNER JOIN USMembership M on M.UserId =U.Id
	 WHERE CP.Id = Isnull(@Id,CP.Id)
			AND (M.Email) = (isnull(@Email,M.Email))
UNION ALL

		Select U.Id [UserId]
			 ,U.[FirstName]
			,U.[MiddleName] 
			,U.[LastName]
			,U.[CompanyName]
			,U.BirthDate
			,U.[Gender]
			,NULL [AddressId]
			,U.[Status]
			,U.[HomePhone]
		    ,U.[MobilePhone]
		    ,U.[OtherPhone]
		    ,U.ImageId
			 ,NULL Notes
			,M.Email
		    ,2 ContactType
			,dbo.ConvertTimeFromUtc(U.CreatedDate,@ApplicationId)CreatedDate
			,U.CreatedBy
			,dbo.ConvertTimeFromUtc(U.ModifiedDate,@ApplicationId) ModifiedDate
			,U.ModifiedBy
			,1 ContactSourceId
		from USMemberShip M 
	 INNER JOIN USUser U on M.UserId=U.Id
	 Where M.UserId=@Id
end

GO
PRINT 'Altering procedure Contact_GetContactByIds'
GO
ALTER PROCEDURE [dbo].[Contact_GetContactByIds](
	@ContactIds xml = null
	,@ApplicationId uniqueidentifier
	,@Status int
	,@SortOrder varchar(25)=null,
	@MaxRecords int
)
AS
BEGIN 
 -- Code goes here
--Select @ContactIds

DECLARE @ContactsTab AS TABLE(ContactId uniqueidentifier)

IF (@ContactIds IS NOT NULL)
BEGIN
	insert into @ContactsTab
	SELECT tab.col.value('text()[1]','uniqueidentifier') 
	FROM @ContactIds.nodes('/GenericCollectionOfGuids/guid') tab(col)
END

if(@MaxRecords =0)
begin
	
		if(@ContactIds is not null)
		BEGIN

			Select [UserId] Id
				  ,[UserId]
				,isnull([FirstName],'') FirstName
				  ,isnull([MiddleName],'') MiddleName
				  ,isnull([LastName],'') LastName
				  ,[CompanyName]
				  ,dbo.ConvertTimeFromUtc(BirthDate,@ApplicationId)BirthDate 
				  ,[Gender]
				  ,[AddressId]
				  ,[Status]
				  ,[HomePhone]
				  ,[MobilePhone]
				  ,[OtherPhone]
				  ,ImageId
				  ,[Notes]
				  ,[Email]
				  ,[ContactType]
				  ,[ContactSourceId]
			from dbo.vw_contacts C
			Inner Join @ContactsTab T on C.UserId=T.ContactId
			Where (@Status=0 OR C.Status = @Status OR (@Status = 99 AND (C.Status = 1 OR C.Status=2)))
			order by LastName asc
		END
		else
		Select 
				[UserId] Id
			  ,[UserId]
			,isnull([FirstName],'') FirstName
			  ,isnull([MiddleName],'') MiddleName
			  ,isnull([LastName],'') LastName
			  ,[CompanyName]
			  ,dbo.ConvertTimeFromUtc(BirthDate,@ApplicationId)BirthDate 
			  ,[Gender]
			  ,[AddressId]
			  ,[Status]
			  ,[HomePhone]
			  ,[MobilePhone]
			  ,[OtherPhone]
			  ,ImageId
			  ,[Notes]
			  ,[Email]
			  ,[ContactType]
			  ,[ContactSourceId]
		from dbo.vw_contacts C
		Where (@Status=0 OR C.Status = @Status OR (@Status = 99 AND (C.Status = 1 OR C.Status=2)))
		order by LastName asc
	end
else 
begin

		if(@ContactIds is not null)
			Select top (@MaxRecords) 	[UserId] Id
				  ,[UserId]
				,isnull([FirstName],'') FirstName
				  ,isnull([MiddleName],'') MiddleName
				  ,isnull([LastName],'') LastName
				  ,[CompanyName]
				  ,dbo.ConvertTimeFromUtc(BirthDate,@ApplicationId)BirthDate 
				  ,[Gender]
				  ,[AddressId]
				  ,[Status]
				  ,[HomePhone]
				  ,[MobilePhone]
				  ,[OtherPhone]
				  ,ImageId
				  ,[Notes]
				  ,[Email]
				  ,[ContactType]
				  ,[ContactSourceId]
			from dbo.vw_contacts C
			Inner Join @ContactsTab T ON  C.UserId=T.ContactId
			Where (@Status=0 OR C.Status = @Status OR (@Status = 99 AND (C.Status = 1 OR C.Status=2)))
			order by LastName asc

		else
		Select  top (@MaxRecords) 
				[UserId] Id
			  ,[UserId]
			,isnull([FirstName],'') FirstName
			  ,isnull([MiddleName],'') MiddleName
			  ,isnull([LastName],'') LastName
			  ,[CompanyName]
			  ,dbo.ConvertTimeFromUtc(BirthDate,@ApplicationId)BirthDate 
			  ,[Gender]
			  ,[AddressId]
			  ,[Status]
			  ,[HomePhone]
			  ,[MobilePhone]
			  ,[OtherPhone]
			  ,ImageId
			  ,[Notes]
			  ,[Email]
			  ,[ContactType]
			  ,[ContactSourceId]
		from dbo.vw_contacts C
		Where (@Status=0 OR C.Status = @Status OR (@Status = 99 AND (C.Status = 1 OR C.Status=2)))
		order by LastName asc
end

END

GO
PRINT 'Altering procedure Contact_GetContactsByIdsForSearch'
GO
ALTER PROCEDURE [dbo].[Contact_GetContactsByIdsForSearch] (
	@ContactIds xml,
	@SiteId		uniqueidentifier
)
AS
BEGIN 

	DECLARE @ContactsTab AS TABLE(ContactId uniqueidentifier)

	IF (@ContactIds IS NOT NULL)
	BEGIN
		insert into @ContactsTab
		SELECT tab.col.value('text()[1]','uniqueidentifier') 
		FROM @ContactIds.nodes('/GenericCollectionOfGuids/guid') tab(col)
	END

	SELECT		UserId AS Id,
				UserId,
				ISNULL(FirstName, '') AS FirstName,
				ISNULL(MiddleName, '') AS MiddleName,
				ISNULL(LastName, '') AS LastName,
				CompanyName,
				dbo.ConvertTimeFromUtc(BirthDate, @SiteId) AS BirthDate,
				Gender,
				AddressId,
				[Status],
				HomePhone,
				MobilePhone,
				OtherPhone,
				ImageId,
				Notes,
				Email,
				ContactType,
				ContactSourceId
	FROM		vw_contacts AS C
				INNER JOIN @ContactsTab T ON C.UserId = T.ContactId
	WHERE		C.[Status] != 3
	ORDER BY	LastName ASC, FirstName ASC, Email ASC
END

GO
PRINT 'Altering procedure DistributionGroup_GetAllActive'
GO
ALTER PROCEDURE [dbo].[DistributionGroup_GetAllActive]
(
--********************************************************************************
@ApplicationId uniqueidentifier
--********************************************************************************
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

--********************************************************************************
begin


select Id,Title,Description,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate
,CreatedBy,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
,ModifiedBy,(case status when 1 then 'Active' else 'Inactive' end  ) as Status,ApplicationId
					
					 from dbo.TADistributionGroup where  Status=dbo.GetActiveStatus() AND ApplicationId=@ApplicationId
order by Title

end

GO
PRINT 'Altering procedure DistributionGroup_GetAllGroup'
GO
ALTER PROCEDURE [dbo].[DistributionGroup_GetAllGroup]
(
--********************************************************************************
@ApplicationId uniqueidentifier
--********************************************************************************
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

--********************************************************************************
begin


select Id,Title,Description,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,CreatedBy,
dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId) ModifiedDate ,ModifiedBy,
(case status when 1 then 'Active' else 'Inactive' end  ) as Status,ApplicationId
	 from dbo.TADistributionGroup WHERE ApplicationId=@ApplicationId
order by Title

end

GO
PRINT 'Altering procedure Contact_Save'
GO
ALTER PROCEDURE [dbo].[Contact_Save](
@UserId uniqueidentifier,
@Email nvarchar(256) =null,
@FirstName nvarchar(50),
@MiddleName nvarchar(50) = null,
@LastName nvarchar(50) = null,
@CompanyName nvarchar(50) = null,
@BirthDate datetime = null,
@Gender nvarchar(50) = null,
@AddressId uniqueidentifier = null,
@status int = 1,
@HomePhone nvarchar(50) = null,
@Mobilephone nvarchar(50) = null,
@OtherPhone nvarchar(50) = null,
@ImageId uniqueidentifier = null,
@Notes varchar(max) =null,
@ContactSourceId int=null,
@ModifiedBy uniqueidentifier=null,
@ApplicationId uniqueidentifier)
as
Begin
	IF @BirthDate is not null
		Set @BirthDate = dbo.ConvertTimeToUtc(@BirthDate,@ApplicationId)
	IF @ContactSourceId = 0 
		Set @ContactSourceId =1
	IF @UserId = dbo.GetEmptyGuid()
		SET @UserId = newid()

	IF not exists(select * from MKContact where Id= @UserId)
	BEGIN
		INSERT INTO [dbo].[MKContact]
           ([Id]
           ,[Email]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[CompanyName]
           ,[Gender]
           ,[BirthDate]
           ,[HomePhone]
           ,[MobilePhone]
           ,[OtherPhone]
           ,[ImageId]
           ,[AddressId]
           ,[Notes]
           ,[ContactSourceId]
           ,[Status]
		   ,CreatedBy
		   ,CreatedDate)
		   Values
		   (
		   @UserId,
		   @Email,
			@FirstName,
			@MiddleName ,
			@LastName,
			@CompanyName,
			@Gender,
			@BirthDate,
			@HomePhone,
			@Mobilephone,
			@OtherPhone,
			@ImageId,
			@AddressId,
			@Notes,
			@ContactSourceId,
			@status,
			@ModifiedBy,
			getutcdate()
		   )
		   INSERT INTO MKContactSite(ContactId,SiteId)
		   Values(@UserId,@ApplicationId)
	END
	ELSE
	BEGIN
		Update MKContact SET 
           [Email]=@Email
           ,[FirstName]=@FirstName
           ,[MiddleName]=@MiddleName
           ,[LastName]=@LastName
           ,[CompanyName]=@CompanyName
           ,[Gender]=@Gender
           ,[BirthDate]=@BirthDate
           ,[HomePhone]=@HomePhone
           ,[MobilePhone]=@Mobilephone
           ,[OtherPhone]=@OtherPhone
           ,[ImageId]=@ImageId
           ,[AddressId]=@AddressId
           ,[Notes]=@Notes
           ,ContactSourceId=isnull(@ContactSourceId,ContactSourceId)
           ,[Status]=@status
		   ,ModifiedBy =@ModifiedBy
		   ,ModifiedDate =GETUTCDATE()
		WHERE Id=@UserId

		IF not exists(select * from MKContactSite where ContactId= @UserId AND SiteId=@ApplicationId)
		   INSERT INTO MKContactSite(ContactId,SiteId)
		   Values(@UserId,@ApplicationId)
	END
	
	update USUser set FirstName=@FirstName,MiddleName=@MiddleName,LastName=@LastName,CompanyName=@CompanyName
			,BirthDate=@BirthDate,Gender=@Gender, HomePhone=@HomePhone,MobilePhone=@MobilePhone,
			OtherPhone=@OtherPhone, ImageId = @ImageId where Id=@UserId

END
GO
PRINT 'Altering procedure Contact_UpdateStatus'
GO
ALTER PROCEDURE [dbo].[Contact_UpdateStatus]
(
	@ContactIdsXml xml,
	@Status int
)
as
begin
	update USUser set Status=@Status where Id in(select tab.col.value('text()[1]','uniqueidentifier') from @ContactIdsXml.nodes('/GenericCollectionOfGuid/guid')tab(col))
	Update M Set M.Status=@Status from MKContact  M 
		Inner Join @ContactIdsXml.nodes('/GenericCollectionOfGuid/guid')tab(col)
			ON tab.col.value('text()[1]','uniqueidentifier') = M.Id
end
GO
PRINT 'Altering procedure Customer_Save'
GO
ALTER PROCEDURE [dbo].[Customer_Save]
(	
	@ApplicationId							uniqueidentifier,
	@Id                                 uniqueidentifier,
	@FirstName								nvarchar(255),
	@LastName								nvarchar(255),
	@MiddleName								nvarchar(255) =null,
	@CompanyName							nvarchar(255),
	@BirthDate								DateTime =null,
	@Gender									nvarchar(50)=null,
	@IsBadCustomer							bit=null,
	@IsActive								bit=null,
	@IsOnMailingList						bit=null,
	@Status									int,
	@CSRSecurityQuestion					nvarchar(max)=null,
	@CSRSecurityPassword					varbinary(max) =null,
	@HomePhone								nvarchar(50)=null,
	@MobilePhone							nvarchar(50)=null,
	@OtherPhone								nvarchar(50)=null,
	@ImageId								uniqueidentifier=null,
	@AccountNumber							nvarchar(255)=null,
	@IsExpressCustomer						bit=null,
	@AddedAttributes						xml=null,
	@ExternalProfileId						nvarchar(255)=null,
	@ExternalId								nvarchar(255)
)

AS
BEGIN     
	
	Declare @createdBy uniqueidentifier
	
	IF (NOT EXISTS(SELECT 1 FROM  USCommerceUserProfile WHERE  Id = @Id))
	begin
		INSERT INTO USCommerceUserProfile
           ([Id]
           ,[IsBadCustomer]
           ,[IsActive]
           ,[IsOnMailingList]
		   ,[IsExpressCustomer]
           ,[Status]           
           ,[CSRSecurityQuestion]
           ,[CSRSecurityPassword]
		   ,[AccountNumber]
		   ,[ExternalProfileId] 
		   ,[ExternalId] 
		   )
     VALUES
           (@Id
           ,@IsBadCustomer
           ,@IsActive
           ,@IsOnMailingList
			,@IsExpressCustomer
           ,dbo.GetActiveStatus()          
           ,@CSRSecurityQuestion
           ,@CSRSecurityPassword
		   ,@AccountNumber
		   ,@ExternalProfileId 
		   ,@ExternalId
		  )

		--Assign customer to everyone group
		declare @EveryOneGroupId uniqueidentifier, @CustomerSecurityLevelId int
	    Set @EveryOneGroupId= ( select top 1 Value from STSiteSetting SV, STSettingType ST where 
			SV.SettingTypeId=ST.Id and ST.Name='CommerceEveryOneGroupId' and SV.SiteId = @ApplicationId)
		Exec Group_AddUser @ApplicationId,@Id,'1', @EveryOneGroupId, Null
		
		SET @CustomerSecurityLevelId = (select  top 1 Value from STSiteSetting SV, STSettingType ST where 
			SV.SettingTypeId=ST.Id and ST.Name='CustomerSecurityLevelId' and SV.SiteId = @ApplicationId)
		INSERT INTO USUserSecurityLevel (UserId, SecurityLevelId, MemberType) Values(@Id,@CustomerSecurityLevelId,1)
		
		
		if @AddedAttributes is not null
			begin
				select @createdBy = CreatedBy from USUser where Id=@Id								 
				Insert into CSCustomerAttributeValue(Id, CustomerId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
				Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), getUTCDate(),@createdBy
				From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple') tab(col)

			end
	end
	else
	begin
		update USCommerceUserProfile
		set 
            IsBadCustomer = @IsBadCustomer,
            IsActive=@IsActive,
            IsOnMailingList=@IsOnMailingList,
            Status =@Status,           
            CSRSecurityQuestion = @CSRSecurityQuestion,
			CSRSecurityPassword=@CSRSecurityPassword,
			AccountNumber = @AccountNumber,
			IsExpressCustomer=@IsExpressCustomer,
			ExternalProfileId =@ExternalProfileId,
			ExternalId =@ExternalId,
			ModifiedDate = getUTCDate()
			where Id=@Id
			
			if @AddedAttributes is not null
				begin
					select @createdBy = IsNull(ModifiedBy, CreatedBy) from USUser where Id=@Id								 
	
					Delete From dbo.CSCustomerAttributeValue where CustomerId= @Id 
					
					Insert into CSCustomerAttributeValue(Id, CustomerId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
					Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), getUTCDate(),@createdBy
					From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple')
					 tab(col)
				end
	end
	
	-- delete user entry from marketier if exists
	if(OBJECT_ID ('MKContact','U') is not null)
	begin	
		delete from MKContact
		where Id = @Id
	end
	

END
GO
PRINT 'Altering procedure Membership_CheckUser'
GO
-- select * from usmembership where email = 'pkalani@blinedigital.com'
-- [Membership_CheckUser] 'pkalani11@bridgelinesw.com', 'pkalani11@bridgelinesw.com', 1
ALTER PROCEDURE [dbo].[Membership_CheckUser]
(
	@UserName nvarchar(256)
	, @Email nvarchar(256)
	, @MatchedOnEmail int output
	, @ActualUsername nvarchar(256) output
	, @ApplicationId uniqueidentifier
)
as
begin

set @MatchedOnEmail = 0
set @ActualUsername = ''

declare @UserType int
set @UserType = 0

declare @UserId uniqueidentifier

select @UserId = U.Id from USUser U JOIN USSiteUser S ON U.Id = S.UserId 
where U.Status = 1 and U.UserName = @UserName and S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))

if(@UserId is null)
begin
	set @MatchedOnEmail = 1
	select @UserId = U.Id from USUser U JOIN USSiteUser S ON U.Id = S.UserId 
	where U.Status = 1 and U.UserName = @Email and S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
end

if(@UserId is null)
begin
	set @MatchedOnEmail = 2
	select @UserId = U.Id, @ActualUsername = U.UserName from USUser U inner join
	usmembership um on um.UserId = U.Id
	JOIN USSiteUser S ON U.Id = S.UserId  
	where U.Status = 1 and um.email = @Email and S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
end

if(@UserId is not null)
begin
	set @UserType = 2
	
	if(OBJECT_ID ('MKContact','U') is not null)
	begin	
		select @UserType = 1 from MKContact
		where Id = @UserId
	end
	
	if(OBJECT_ID ('USCommerceUserProfile','U') is not null)
	begin
		select @UserType = 3 from USCommerceUserProfile
		where Id = @UserId and IsExpressCustomer = 1
		
		select @UserType = 4 from USCommerceUserProfile
		where Id = @UserId and (IsExpressCustomer is null or IsExpressCustomer = 0)
	END
end
--select @userid
--select @UserType
return @UserType

end
GO
PRINT 'Altering procedure Users_ImportIndexTerms'
GO
ALTER PROCEDURE [dbo].[Users_ImportIndexTerms]
    (
      @BatchIndexTermTableName VARCHAR(250) ,
      @BatchSiteUserTable VARCHAR(250)
    )
AS 
    BEGIN

        DECLARE @ObjectRelationParentId UNIQUEIDENTIFIER
        DECLARE @RelLft BIGINT ,
            @RelRgt BIGINT
        DECLARE @RelationCode VARCHAR(10)

        SELECT  @ObjectRelationParentId = Nodeid ,
                @RelLft = Lft ,
                @RelRgt = Rgt
        FROM    RMRelations
        WHERE   NodeType = 'O-T'


-- Index term data
        CREATE TABLE #IndexTermTbl
            (
              ObjectId UNIQUEIDENTIFIER
            )
        DECLARE @sqlstatement NVARCHAR(4000) 
        SET @sqlstatement = 'Insert into #IndexTermTbl (ObjectId) SELECT ObjectId FROM '
            + @BatchIndexTermTableName 
        EXEC(@sqlstatement)
	
	---print @sqlstatement

	--User Data
        CREATE TABLE #UserTbl
            (
              userID UNIQUEIDENTIFIER
            )
	--Original
        SET @sqlstatement = 'Insert into #UserTbl (userID) SELECT ContactId FROM   '
            + @BatchSiteUserTable
            + ' B Inner Join RMRelations R on B.ContactId=R.ObjectId Where R.Lft >'
            + CAST(@RelLft AS VARCHAR(10)) + ' And R.Rgt <'
            + CAST(@RelRgt AS VARCHAR(10))
        EXEC(@sqlstatement)
	
        CREATE TABLE #NewUserTbl
            (
              UserID UNIQUEIDENTIFIER
            )
        SET @sqlstatement = 'Insert into #NewUserTbl (userID) SELECT ContactId FROM   '
            + @BatchSiteUserTable
						--+'except UserId from #UserTbl'
        EXEC(@sqlstatement)

		

        SET @RelationCode = 'O-T'
		
		-- Section - 007 Going to delete all the existing relations
		
        SELECT  NodeId ,
                Lft ,
                Rgt
        FROM    #UserTbl T
                INNER JOIN RMRelations R ON T.UserId = R.ObjectId
        WHERE   R.Lft BETWEEN @RelLft AND @RelRgt
				
        DECLARE @NodeId UNIQUEIDENTIFIER ,
            @DelLft BIGINT ,
            @DelRgt BIGINT
        DECLARE cursor_Relation CURSOR FOR 
        SELECT NodeId,Lft,Rgt FROM #UserTbl T 
        INNER JOIN RMRelations R ON T.UserId = R.ObjectId
        WHERE R.Lft BETWEEN @RelLft AND @RelRgt
        OPEN cursor_Relation

        FETCH NEXT FROM cursor_Relation INTO @NodeId,@DelLft,@DelRgt
		
        WHILE @@FETCH_STATUS = 0 
            BEGIN
		
		-- Delete the acutal records
                PRINT 'Deleting'
                PRINT @NodeId
		
                DELETE  FROM RMRelations
                WHERE   Lft BETWEEN @DelLft AND @DelRgt
				
		-- Adjust the gap
		
                UPDATE  RMRelations
                SET     Lft = CASE WHEN Lft > @DelRgt
                                   THEN Lft - ( @DelRgt - @DelLft + 1 )
                                   ELSE Lft
                              END ,
                        Rgt = CASE WHEN Rgt > @DelRgt
                                   THEN Rgt - ( @DelRgt - @DelLft + 1 )
                                   ELSE Rgt
                              END
		
                FETCH NEXT FROM cursor_Relation INTO @NodeId,@DelLft,@DelRgt
            END
        CLOSE cursor_Relation
        DEALLOCATE cursor_Relation
		-- Section -007 End
		
        SELECT  @RelLft = Lft ,
                @RelRgt = Rgt
        FROM    RMRelations
        WHERE   NodeType = 'O-T'
		
		 --Section -1: This section is only for new users or users who doesnt have any index terms attached before
		
        DECLARE @tempLocalRelation TABLE
            (
              [NodeId] [uniqueidentifier] NOT NULL ,
              [ObjectId] [uniqueidentifier] NULL ,
              [NodeType] [varchar](36) NULL ,
              [Lft] [bigint] NOT NULL ,
              [Rgt] [bigint] NOT NULL ,
              [ParentId] [uniqueidentifier] NULL
            )
        DECLARE @IndexTermsCount INT
        DECLARE @UserCount INT
        SELECT  @IndexTermsCount = COUNT(*)
        FROM    #IndexTermTbl
        SELECT  @UserCount = COUNT(*)
        FROM    #NewUserTbl 
		
		-- Create a complete table for the relation and then update the Relation table with the data
        INSERT  INTO @tempLocalRelation
                SELECT  NEWID() ,
                        UserID ,
                        14 ,
                        row_number() OVER ( ORDER BY UserID ) ,
                        0 ,
                        @ObjectRelationParentId
                FROM    #NewUserTbl
		

		
        UPDATE  @tempLocalRelation
        SET     Lft = ( 2 * Lft * @IndexTermsCount ) + ( 2 * Lft ) - ( 2
                                                              * @IndexTermsCount )
                - 1 ,
                Rgt = ( 2 * Lft * @IndexTermsCount ) + ( 2 * Lft )
		
				
        INSERT  INTO @tempLocalRelation
                SELECT  NEWID() ,
                        U.ObjectId ,
                        12 ,
                        R.Lft + ( 2
                                  * row_number() OVER ( PARTITION BY R.Lft ORDER BY U.ObjectId ) )
                        - 1 ,
                        R.Lft + ( 2
                                  * row_number() OVER ( PARTITION BY R.Lft ORDER BY U.ObjectId ) ) ,
                        R.NodeId
                FROM    @tempLocalRelation R
                        CROSS JOIN #IndexTermTbl U
		
		-- Create Space in the Relation Tree
		
        SELECT  *
        FROM    @tempLocalRelation R 
        SELECT  ( 2 * @UserCount * ( @IndexTermsCount + 1 ) )
        UPDATE  RMRelations
        SET     Lft = CASE WHEN Lft > @RelRgt
                           THEN Lft + ( 2 * @UserCount * ( @IndexTermsCount
                                                           + 1 ) )
                           ELSE Lft
                      END ,
                Rgt = CASE WHEN Rgt >= @RelRgt
                           THEN Rgt + ( 2 * @UserCount * ( @IndexTermsCount
                                                           + 1 ) )
                           ELSE Rgt
                      END
				
		-- Insert the temporary relation into the actual relation table


        INSERT  INTO RMRelations
                ( [NodeId] ,
                  [ObjectId] ,
                  [NodeType] ,
                  [Lft] ,
                  [Rgt] ,
                  [ParentId]
                )
                SELECT  [NodeId] ,
                        [ObjectId] ,
                        [NodeType] ,
                        [Lft] + @RelRgt - 1 ,
                        [Rgt] + @RelRgt - 1 ,
                        [ParentId]
                FROM    @tempLocalRelation 
		
		--- End Section -1:


    END


GO
PRINT 'Altering procedure Users_ImportBatch'
GO
ALTER PROCEDURE [dbo].[Users_ImportBatch]
    (
      @OverwriteExistingContacts BIT ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @BatchId UNIQUEIDENTIFIER ,
      @BatchUserTable VARCHAR(250) ,
      @BatchMembershipTable VARCHAR(250) ,
      @BatchProfileTable VARCHAR(250) ,
      @BatchSiteUserTable VARCHAR(250) ,
      @BatchUserDistributionListTable VARCHAR(250) ,
      @BatchUSMarketierUserTable VARCHAR(250) ,
      @BatchIndexTermTable VARCHAR(250) ,
      @BatchAddressTable VARCHAR(250) ,
      @OverrideWSUSersAndCustomers BIT = 0
    )
AS 
    BEGIN
        DECLARE @BatchTablePrefix VARCHAR(250) ,
            @BatchIdMapTableName VARCHAR(250) ,
            @StrModifiedBy VARCHAR(36) ,
            @ImportedContacts INT ,
            @AddedToDistributionList INT ,
            @ExistingContacts INT ,
            @UpdatedContacts INT
		
        SET @AddedToDistributionList = 0
        SET @ImportedContacts = 0
        SET @UpdatedContacts = 0

	-- set up temp table name for batch import user id mapping
        SET @BatchTablePrefix = 'ZTMP' + REPLACE(CONVERT(VARCHAR(36), @BatchId),
                                                 '-', '')
        SET @BatchIdMapTableName = @BatchTablePrefix + '_BatchImportIdMap'
        SET @StrModifiedBy = CONVERT(VARCHAR(36), @ModifiedBy)
	
	--start delete duplicate records within the temporary tables if any
        EXEC('delete from ' + @BatchUserTable + '  where ID in (select ID from
        (Select Id, row_number()over(partition by Email order by CreatedDate) as rnum--,Email,FirstName,LastActivityDate
        from ' + @BatchUserTable + '
        Where Email in (
        select Email
        from ' + @BatchUserTable + ' U
        Group by Email
        Having count(*)>1
        )) Duplicates where rnum >1)')
	
        EXEC ('delete from ' + @BatchMembershipTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchProfileTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchSiteUserTable + ' where ContactId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUserDistributionListTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUSMarketierUserTable + ' where UserId not in (select Id from '+@BatchUserTable+')')	
        EXEC ('delete from ' + @BatchAddressTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	--end delete duplicate records within the temporary tables if any


        IF EXISTS ( SELECT  *
                    FROM    sysobjects
                    WHERE   name = @BatchIdMapTableName ) 
            EXEC('Drop Table ' + @BatchIdMapTableName)

	-- Step 1 locate existing records and create mapping
        EXEC(
        'Create Table ' + @BatchIdMapTableName + '(' +
        'importId UniqueIdentifier, ' +
        'actualId UniqueIdentifier, ' +
        'importAddressId UniqueIdentifier, ' +
        'actualAddressId uniqueidentifier ' +
        ')'
        )

        PRINT @BatchIdMapTableName

        EXEC(
        'Create Index ' + @BatchTablePrefix + '_MappingImportId On ' + @BatchIdMapTableName + '(importId)'
        )

        PRINT @BatchTablePrefix 

        PRINT '1a: locate existing users and populate id mapping'
            + @BatchUSMarketierUserTable
	-- 1a: locate existing users and populate id mapping
        EXEC(
        'Insert Into ' + @BatchIdMapTableName + ' (importId, actualId, importAddressId, actualAddressId) ' +
        'Select z.Id, m.UserId, z.AddressId, m.AddressId ' +
        'From ' + @BatchUserTable + ' z ' +			
        'Join vw_contacts m on m.Email = z.Email ' 
			
        )

        PRINT @BatchMembershipTable 
        PRINT '1b: update batch table user id fields to the existing user''s id'

	-- 1b: update batch table user id fields to the existing user's id
        EXEC(
        'Update ' + @BatchUserTable + ' Set ' +
        'ID = m.actualId ' +
        'From ' + @BatchUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.Id'
        )
		
        PRINT @BatchAddressTable
        EXEC(
        'Update ' + @BatchAddressTable + ' Set ' +
        'ID = isnull(m.actualAddressId, m.importAddressId), UserId = m.actualId ' +
        'From ' + @BatchAddressTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 

        EXEC(
        'Update ' + @BatchMembershipTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchMembershipTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 


        EXEC(
        'Update ' + @BatchProfileTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchProfileTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchProfileTable

        EXEC(
        'Update ' + @BatchSiteUserTable + ' Set ' +
        'ContactId = m.actualId ' +
        'From ' + @BatchSiteUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.ContactId'
        )

        PRINT @BatchSiteUserTable

        EXEC(
        'Update ' + @BatchUSMarketierUserTable + ' Set ' +
        'UserId = m.actualId, ' +
        'AddressId = m.actualAddressId ' +
        'From ' + @BatchUSMarketierUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUSMarketierUserTable


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchUserDistributionListTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUserDistributionListTable

	-- Insert Index term
        DECLARE @idxTerms INT
        CREATE TABLE #tmpIdxTerms ( recCount INT )

        EXEC('insert into #tmpIdxTerms (recCount) select count(*) from ' + @BatchIndexTermTable)

        SELECT  @idxTerms = recCount
        FROM    #tmpIdxTerms
        IF @idxTerms > 0 
            BEGIN
                EXEC Users_ImportIndexTerms @BatchIndexTermTable,
                    @BatchSiteUserTable
            END
        DROP TABLE #tmpIdxTerms

	-- To get existing records count
        DECLARE @ParmDefinition NVARCHAR(100) ;
        DECLARE @ExistingContactsStr NVARCHAR(30)
        DECLARE @query NVARCHAR(500)
        SET @query = 'select @result =count(U.Id) from MKContact U inner join '
            + @BatchUserTable + ' z on z.id = u.id'
        SET @ParmDefinition = N'@result varchar(30) OUTPUT' ;	 
        EXEC sp_executesql @query, @ParmDefinition,
            @result = @ExistingContactsStr OUTPUT
        SET @ExistingContacts = CAST(@ExistingContactsStr AS INT)
        PRINT 'remove system users'
-- remove system users
        PRINT 'delete batchusertable system users'
        EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchMembershipTable system users'
        EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchProfileTable system users'
        EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchSiteUserTable system users'
        EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchUserDistributionListTable system users'
        EXEC ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchUSMarketierUserTable system users'
        EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchAddressTable system users'
        EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')

-- remove commerce users
        PRINT 'remove commerce users'
        SELECT  *
        FROM    uscommerceuserprofile
        IF ( @OverrideWSUSersAndCustomers <> 1 ) 
            BEGIN
				PRINT 'DELETE @BatchUserTable'
                EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchMembershipTable'
                EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchProfileTable'
                EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                    PRINT 'DELETE @BatchSiteUserTable'
                EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from vw_contacts where contactType in(1,2))')
--exec ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                  PRINT 'DELETE @BatchUSMarketierUserTable'
                EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                    PRINT 'DELETE @BatchAddressTable'
                EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
            END
	
        PRINT 'Step 2: Update records for existing users'
	-- Step 2: Update records for existing users
        IF ( @OverwriteExistingContacts = 1
             OR @OverrideWSUSersAndCustomers = 1
           ) 
            BEGIN
		-- Step 2a: User Record
                EXEC(
                'Update MKContact Set ' +
                'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
                'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
                'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone,AddressId=z.AddressId, '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From MKContact u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id'
                )
			
                SET @UpdatedContacts = ( SELECT @@ROWCOUNT
                                       )
                SET @ImportedContacts = ( @ImportedContacts + @UpdatedContacts )
		PRINT 'Step 2a Must update User records outside of MKContact'
		--Step 2a Must update User records outside of MKContact
                EXEC(
                'Update USUser Set ' +
                'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
                'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
                'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone, '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From USUser u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id'
                )
			
                SET @UpdatedContacts = ( SELECT @@ROWCOUNT
                                       )
                SET @ImportedContacts = ( @ImportedContacts + @UpdatedContacts )
		
		
		
		--SET @ImportedContacts = (SELECT @@ROWCOUNT)
		-- Step 2b: Profile Record
		-- update profile properties that exist
		
		-- THIS NEEDS TO BE MOVED TO ATTRIBUTE MODEL LOGIC
		
		--Exec(
		--	'Update USMemberProfile Set ' +
		--		'PropertyValueString = z.PropertyValueString, ' +
		--		'PropertyValuesBinary = z.PropertyValuesBinary, ' +
		--		'LastUpdatedDate = z.LastUpdatedDate ' +
		--	'From USMemberProfile u ' +
		--		'Join ' + @BatchProfileTable + ' z on z.userid = u.userid and z.PropertyName = u.PropertyName'
		--	)
		
	
		
		
		
		
PRINT 'UPDATE GLADDRESS'
                EXEC(
                'Update GLAddress Set ' +
                'AddressType = z.AddressType, ' + 
                'AddressLine1 = z.AddressLine1, ' +
                'AddressLine2 = z.AddressLine2, ' +
                'City = z.City, ' +
                'StateId = s.Id, ' +
                'Zip = z.Zip, ' +
                'CountryId = c.Id, ' +
                'ModifiedDate = z.ModifiedDate, ' +
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'Status = z.Status ' +
                'From GLAddress u ' +
                'join ' + @BatchAddressTable + ' z on z.Id = u.Id ' +
                'join GLState s on s.StateCode = z.State ' +
                'join GLCountry c on c.CountryName = z.Country '
				
                )
		-- Removed by NBOWMAN - new contact batch table should have address ID in it
		--Exec('Update MKContact Set ' +
		--	'AddressId=z.AddressId '+			
		--	'From MKContact u ' +
		--		'join ' + @BatchUSMarketierUserTable + ' z on z.Userid = u.Id')
				
            END
PRINT 'Insert Records for users that do not exist'
	-- Step 3: Insert Records for users that do not exist
	-- Step 3a: User record
        EXEC(
        'INSERT INTO dbo.MKContact
        ( Id ,
        Email ,
        FirstName ,
        MiddleName ,
        LastName ,
        CompanyName ,
        Gender ,
        BirthDate ,
        HomePhone ,
        MobilePhone ,
        OtherPhone ,
        ImageId ,
        AddressId ,
        Notes ,
        ContactSourceId ,
        Status ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate ,
        LastSynced
        ) ' +
        'select 
        z.Id ,
        z.Email ,
        z.FirstName ,
        z.MiddleName ,
        z.LastName ,
        z.CompanyName ,
        z.Gender ,
        z.BirthDate ,
        z.HomePhone ,
        z.MobilePhone ,
        z.OtherPhone ,
        z.ImageId ,
        z.AddressId ,
        z.Notes ,
        z.ContactSourceId ,
        z.Status ,
        z.CreatedBy ,
        z.CreatedDate ,
        z.ModifiedBy ,
        ISNULL(z.ModifiedDate, Z.CreatedDate) ,
        z.LastSynced ' +
        'from ' + @BatchUserTable + ' z ' +
        'Left Outer Join vw_contacts u on u.UserId = z.Id ' +
        'Where u.UserId is null'
        )
        SET @ImportedContacts = ( @ImportedContacts + ( SELECT
                                                              @@ROWCOUNT
                                                      ) )
	PRINT 'Insert into GLAddress'
        EXEC('insert into GLAddress(Id, AddressType, AddressLine1, AddressLine2, City, StateId, Zip, CountryId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status) ' +
        'select ISNULL(z.Id, NEWID()), z.AddressType, z.AddressLine1, z.AddressLine2, z.City, s.Id, z.Zip, c.Id, z.CreatedDate, z.CreatedBy, null, null, z.Status ' +
        'from ' + @BatchAddressTable + ' z ' +
        'left outer join GLCountry c on c.CountryName = z.Country ' +
        'left outer join GLState s on s.StateCode = z.State and s.CountryId = c.Id ' +
        'left outer join GLAddress a on a.Id = z.Id ' +
        'Where a.Id is null '
        )

        EXEC ('UPDATE MKContact SET AddressId = ISNULL(z.Id, NEWID()) ' + 
        'from  ' + @BatchAddressTable + '  z  '+
        'INNER Join MKContact u on u.Id = z.userId ')

	-- Step 3b: Membership Record
	--Exec(
	--	'insert into USMembership (UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, ' +
	--			'Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved,  ' +
	--			'IsLockedOut, LastLoginDate, LastPasswordChangedDate, LastLockoutDate,  ' +
	--			'FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart,  ' +
	--			'FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart) ' +
	--	'select z.UserId, z.Password, z.PasswordFormat, z.PasswordSalt, z.MobilePIN, ' +
	--			'z.Email, z.LoweredEmail, z.PasswordQuestion, z.PasswordAnswer, z.IsApproved,  ' +
	--			'z.IsLockedOut, z.LastLoginDate, z.LastPasswordChangedDate, z.LastLockoutDate,  ' +
	--			'z.FailedPasswordAttemptCount, z.FailedPasswordAttemptWindowStart,  ' +
	--			'z.FailedPasswordAnswerAttemptCount, z.FailedPasswordAnswerAttemptWindowStart ' +
	--	'from ' + @BatchMembershipTable + ' z ' +
	--		'left outer join USMembership u on u.UserId = z.UserId ' +
	--	'where u.UserId is null '
	--	)

	-- Step 3c: Profile Record
	
	--THIS NEEDS TO BE UPDATED TO ATTRIBUTE MODEL
	
	--Exec(
	--	'insert into USMemberProfile (UserId, PropertyName, PropertyValueString, ' +
	--		'PropertyValuesBinary, LastUpdatedDate) ' +
	--	'select z.UserId, z.PropertyName, z.PropertyValueString, z.PropertyValuesBinary, ' +
	--		'z.LastUpdatedDate ' +
	--	'from ' + @BatchProfileTable + ' z ' +
	--		'left outer join USMemberProfile u on u.UserId = z.UserId and u.PropertyName = z.PropertyName ' +
	--	'where u.PropertyName is null '
	--	)


	---------------------ATTRIBUTE INSERT-----------------------
        EXEC(
        'SELECT * FROM '+@BatchProfileTable+'')
		
             
                    
        CREATE TABLE #tmpContactAttributes
            (
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              VALUE NVARCHAR(4000) ,
              UserId UNIQUEIDENTIFIER
            )
                    
                    
        PRINT 'insert into #tmpContactAttributes'
        INSERT  INTO #tmpContactAttributes
                EXEC
                    ( '	 SELECT  
                A.Id AS AttributeId ,
                AE.Id AS AttributeEnumId ,
                P.PropertyValueString AS Value ,
                P.UserId                 
                FROM   ' + @BatchProfileTable
                      + ' P
                INNER JOIN dbo.ATAttribute A ON A.Title = P.PropertyName
                INNER JOIN dbo.ATAttributeCategoryItem ACI ON ACI.AttributeId = A.Id
                INNER JOIN dbo.ATAttributeCategory AC ON AC.ID = ACI.CategoryId
                AND AC.Name = ''Contact Attributes''
                LEFT JOIN dbo.ATAttributeEnum AE ON AE.AttributeID = A.Id
                AND AE.Title = P.PropertyValueString
                WHERE  ( A.IsEnum = 1
                AND AE.ID IS NOT NULL
                )
                OR A.IsEnum = 0'
                    )
 
 
        DELETE  CAV
        FROM    dbo.ATContactAttributeValue CAV
                INNER JOIN #tmpContactAttributes TCA ON TCA.AttributeId = CAV.AttributeId
                                                        AND TCA.UserId = CAV.ContactId
 
        INSERT  INTO dbo.ATContactAttributeValue
                ( Id ,
                  ContactId ,
                  AttributeId ,
                  AttributeEnumId ,
                  Value ,
                  Notes ,
                  CreatedDate ,
                  CreatedBy
         
                        
                )
                SELECT  NEWID() ,
                        UserId ,
                        AttributeId ,
                        AttributeEnumId ,
                        Value ,
                        '' ,
                        GETUTCDATE() ,
                        '6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
                FROM    #tmpContactAttributes


		
		
		-------------------------------------------------------------




PRINT ' Step 3d: Site User Record'
	-- Step 3d: Site User Record
        EXEC(
        'insert into MKContactSite (SiteId, ContactId,  Status) ' +
        'select z.SiteId, z.ContactId, 1' +
        'from ' + @BatchSiteUserTable + ' z ' +
        ' inner join MKContact MC ON MC.Id = z.ContactId'+
        ' left outer join MKContactSite u on u.ContactId = z.ContactId ' +
        'where u.ContactId is null '
        )

	-- Step 3e: Distribution List Record

        PRINT 'Update ' + @BatchUserDistributionListTable + ' Set '
            + '  ListId = null ' + 'From ' + @BatchUserDistributionListTable
            + ' z '
            + 'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        ' ListId = null ' +
        ' from  ' + @BatchUserDistributionListTable + ' z ' +
        'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'
        )
        SET @AddedToDistributionList = ( SELECT @@ROWCOUNT
                                       )

        PRINT 'insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '
            + ' select newid(),z.ListId,z.UserId ' + 'from '
            + @BatchUserDistributionListTable + ' z '
            + 'Left Outer Join TADistributionListUser u on u.UserId = z.UserId '
            + 'Where u.UserId is null and z.ListId is not null '


        EXEC ('insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '+
        ' select newid(),z.ListId,z.UserId ' + 
        'from ' + @BatchUserDistributionListTable + ' z ' +
        'Join vw_contacts u on u.userid = z.UserId ' +
        'Where z.ListId is not null ')
        SET @AddedToDistributionList = ( @AddedToDistributionList + ( SELECT
                                                              @@ROWCOUNT
                                                              ) )
	

	-- peform cleanup
	--Exec('Drop Table ' + @BatchIdMapTableName)
--
--	-- empty batch tables

	--Exec('truncate table ' + @BatchUserTable)
	--Exec('truncate table ' + @BatchMembershipTable)
	--Exec('truncate table ' + @BatchProfileTable)
	--Exec('truncate table ' + @BatchSiteUserTable)
	--exec('truncate table ' + @BatchAddressTable)
	--exec('truncate table ' + @BatchUserDistributionListTable)
	--exec('truncate table ' + @BatchUSMarketierUserTable)
	--exec('truncate table ' + @BatchIndexTermTable)
	--exec('truncate table ' + @BatchAddressTable)

        UPDATE  UploadContactData
        SET     ImportedContacts = @ImportedContacts ,
                AddedToDistributionList = @AddedToDistributionList ,
                ExistingRecords = @ExistingContacts ,
                UpdatedContacts = @UpdatedContacts
        WHERE   UploadHistoryId = @BatchId
    END
GO
PRINT 'Altering procedure Campaign_GetWatchGoals'
GO

ALTER Procedure [dbo].[Campaign_GetWatchGoals]
(
	@CampaignId UniqueIdentifier=null,
	@ApplicationId Uniqueidentifier,
	@Status int=null,
	@OnlyActiveCampaigns bit=0
)
AS
Begin

		Select 	distinct 
           S.[CampaignWatchId] as Id
           ,S.[WatchId]
           ,S.[CampaignId]
           ,S.[TargetQuantity]
           ,dbo.ConvertTimeFromUtc(S.[TargetCompletion],ApplicationId) TargetCompletion
           ,S.[CampaignName]
           ,S.[WatchName] Name
           ,S.[CurrentActualQuantity]
           ,dbo.ConvertTimeFromUtc([FirstRunDate],ApplicationId) FirstRunDate
           ,S.[EstimatedCompletionQuantity]
           ,S.[Status]
     FROM vwCampaignWatchStats S
     Where (@CampaignId IS NULL OR @CampaignId=CampaignId)
     AND (@Status is null OR (S.Status & @Status)in (1,2))
    AND (@OnlyActiveCampaigns=0 OR CampaignStatus=2)
	AND @ApplicationId = S.ApplicationId
		
	
End
GO
PRINT 'Altering procedure DistributionList_GetContactsWithPaging'
GO
ALTER PROCEDURE [dbo].[DistributionList_GetContactsWithPaging](
	@ListId uniqueidentifier, 
	@ApplicationId uniqueidentifier,
	@PageSize int = 10,
	@PageNumber int,   
	@SortBy nvarchar(50) = 'Email asc',  
	@TotalRecords int out)
As
Begin
	Declare @listType bit
	Select @listType = isnull(ListType,0) from TADistributionLists
	Where Id= @ListId

	if @listType = 0  --- manual
		begin
		
		  if @SortBy is null
			set @SortBy = 'Email asc'
			Declare @sql varchar(max)

			Set @sql='
			   With tmpContacts
			   AS (
					Select
					   C.[UserId]
					  ,C.[FirstName]
					  ,C.[MiddleName]
					  ,C.[LastName]
					  ,C.[CompanyName]
					  ,dbo.ConvertTimeFromUtc(BirthDate,''' + cast(@ApplicationId as char(36))+ ''')BirthDate 
					  ,C.[Gender]
					  ,C.[AddressId]
					  ,C.[Status]
					  ,C.[HomePhone]
					  ,C.[MobilePhone]
					  ,C.[OtherPhone]
					  ,C.[Notes]
					  ,C.[Email]
					  ,C.[ContactType]
					  ,C.[ContactSourceId]
				from dbo.vw_contacts C
				inner join 	TADistributionListUser TAL ON
						C.UserId = TAL.UserID And  TAL.DistributionListId =''' +  cast(@ListId as char(36))
				 + ''' AND ContactListSubscriptionType != 3)
				
				Select A.UserId as Id, A.*   
					FROM  
					 (  
					 Select *,  ROW_NUMBER() OVER(Order by ' + @SortBy + '
												) as RowNumber  
					  from tmpContacts  
					 ) A  
					where A.RowNumber >=' +  cast((@PageSize * @PageNumber) - (@PageSize -1 ) as varchar(10))  + 
						' AND A.RowNumber <=' + cast((@PageSize * @PageNumber) as varchar(10))
				
				 exec(@sql)
						
				 Select @TotalRecords = count(*) from dbo.TADistributionListUser 
						 where DistributionListId= @ListId AND ContactListSubscriptionType != 3
		
		end
	else
		begin
			Declare @queryXml xml
			SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
					b.Id = a.SearchQueryId and a.DistributionListId=@ListId 
			exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@ApplicationId, @MaxRecords=0,  @ContactListId = @ListId, @TotalRecords=@TotalRecords output

		end
End


GO
PRINT 'Altering procedure [DistributionList_GetContactsNew]'
GO
ALTER PROCEDURE [dbo].[DistributionList_GetContactsNew](
	@ListId uniqueidentifier, 
	@ApplicationId uniqueidentifier,
	@PageSize int = 10,@PageNumber int,   
	@SortBy nvarchar(50) = 'Email asc',  
	@TotalRecords int out)
As
Begin
	Declare @listType bit
	Select @listType = isnull(ListType,0) from TADistributionLists
	Where Id= @ListId

	if @listType = 0  --- manual
		begin
		
		  if @SortBy is null
			set @SortBy = 'Email asc'
			Declare @sql varchar(max)

			Set @sql='
			   With tmpContacts
			   AS (
					Select
					   C.[UserId]
					  ,C.[FirstName]
					  ,C.[MiddleName]
					  ,C.[LastName]
					  ,C.[CompanyName]
					  ,dbo.ConvertTimeFromUtc(BirthDate,''' + cast(@ApplicationId as char(36))+ ''')BirthDate 
					  ,C.[Gender]
					  ,C.[AddressId]
					  ,C.[Status]
					  ,C.[HomePhone]
					  ,C.[MobilePhone]
					  ,C.[OtherPhone]
					  ,C.[Notes]
					  ,C.[Email]
					  ,C.[ContactType]
					  ,C.[ContactSourceId]
				from dbo.vw_contacts C
				inner join 	TADistributionListUser TAL ON
						C.UserId = TAL.UserID And  TAL.DistributionListId =''' +  cast(@ListId as char(36))
				 + ''' AND ContactListSubscriptionType != 3)
				
				Select A.UserId as Id, A.*   
					FROM  
					 (  
					 Select *,  ROW_NUMBER() OVER(Order by ' + @SortBy + '
												) as RowNumber  
					  from tmpContacts  
					 ) A  
					where A.RowNumber >=' +  cast((@PageSize * @PageNumber) - (@PageSize -1 ) as varchar(10))  + 
						' AND A.RowNumber <=' + cast((@PageSize * @PageNumber) as varchar(10))
				
				 exec(@sql)
						
				 Select @TotalRecords = count(*) from dbo.TADistributionListUser 
						 where DistributionListId= @ListId AND ContactListSubscriptionType != 3
		
		end
	else
		begin
			Declare @queryXml xml
			SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
					b.Id = a.SearchQueryId and a.DistributionListId=@ListId 
			exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@ApplicationId, @MaxRecords=0,  @ContactListId = @ListId, @TotalRecords=@TotalRecords output

		end
End


GO
PRINT 'Altering procedure [DistributionList_GetContacts]'
GO
ALTER PROCEDURE [dbo].[DistributionList_GetContacts](
	@ListId uniqueidentifier, 
	@ApplicationId uniqueidentifier,
	@TotalRecords int out)
As
Begin

	Declare @listType bit
	Select @listType = isnull(ListType,0) from TADistributionLists
	Where Id= @ListId
	
	declare @contactIds varchar(max)

	if @listType = 0  --- manual
		begin
			Select              
								C.[UserId] Id
								,C.[UserId]
                                ,C.[FirstName]
                                ,C.[MiddleName]
                                ,C.[LastName]
                                ,C.[CompanyName]
                                ,dbo.ConvertTimeFromUtc(BirthDate, @ApplicationId)BirthDate 
                                ,C.[Gender]
                                ,C.[AddressId]
                                ,C.[Status]
                                ,C.[HomePhone]
                                ,C.[MobilePhone]
                                ,C.[OtherPhone]
                                ,C.ImageId 
                                ,C.[Notes]
                                ,C.[Email]
                                ,C.[ContactType]
                                ,C.[ContactSourceId]
                        from dbo.vw_contacts C
                        inner join TADistributionListUser TAL ON
                                    C.UserId = TAL.UserID And  TAL.DistributionListId=@ListId AND ContactListSubscriptionType != 3

		Select @TotalRecords = count(*) from dbo.TADistributionListUser where DistributionListId= @ListId AND ContactListSubscriptionType != 3

		end
	else
		begin
			Declare @queryXml xml
			SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
					b.Id = a.SearchQueryId and a.DistributionListId=@ListId 
			exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@ApplicationId, @MaxRecords=0, @ContactListId = @ListId, @TotalRecords=@TotalRecords output

		end
End
GO
PRINT 'Altering Procedure CampaignMailing_PopulateRunWorkData'
GO
ALTER PROCEDURE [dbo].[CampaignMailing_PopulateRunWorkData]
	@CampaignId uniqueIdentifier,
	@CampaignRunId UNIQUEIDENTIFIER,
	@EmailPerCampaignLimit INT
AS
Begin
	Declare
		@UniqueRecipientsOnly bit,
		@AutoUpdateLists bit,
		@LastPublishDate datetime,
		@listRecs int,
		@totalRecs int,
		@listId uniqueidentifier,
		@GetCurrentRecipients bit,
		@SendToNotTriggered bit,
		@ApplicationId uniqueidentifier

	-- clean up run work table
	if exists(select top 1 * from MKCampaignRunWorkTable where CampaignRunId = @CampaignRunId)
		Delete MKCampaignRunWorkTable Where CampaignRunId = @CampaignRunId

	Declare @tblContacts Table
	(
		Id uniqueidentifier,
		UserId uniqueidentifier,
		FirstName varchar(1024) null,
		MiddleName varchar(1024) null,
		LastName varchar(1024) null,
		CompanyName varchar(1024) null,
		BirthDate datetime null,
		Gender varchar(1024) null,
		AddressId uniqueidentifier null,
		Status int null,
		HomePhone varchar(1024) null,
		MobilePhone varchar(1024) null,
		OtherPhone varchar(1024) null,
		Notes varchar(max) null,
		Email varchar(256),
		ContactType int,
		ContactSourceId int
	)

	Create Table #tblContacts
	(
		Id uniqueidentifier,
		UserId uniqueidentifier,
		FirstName varchar(1024) null,
		MiddleName varchar(1024) null,
		LastName varchar(1024) null,
		CompanyName varchar(1024) null,
		BirthDate datetime null,
		Gender varchar(1024) null,
		AddressId uniqueidentifier null,
		Status int null,
		HomePhone varchar(1024) null,
		MobilePhone varchar(1024) null,
		OtherPhone varchar(1024) null,
		ImageId uniqueidentifier null,
		Notes varchar(max) null,
		Email varchar(256),
		ContactType int,
		ContactSourceId int
	)

	Set @GetCurrentRecipients = 1

	select @ApplicationId = ApplicationId from mkcampaign Where Id = @CampaignId

	-- get the options for list selection
	select top 1 @UniqueRecipientsOnly = UniqueRecipientsOnly,
		@AutoUpdateLists = AutoUpdateLists, @LastPublishDate = IsNull(LastPublishDate, '2009-01-01'),
		@SendToNotTriggered = SendToNotTriggered
	from MKCampaignAdditionalInfo
	Where CampaignId = @CampaignId

	if @AutoUpdateLists = 1
	Begin
		if exists (select top 1 * From MKEmailSendLog Where CampaignId = @CampaignId And SendDate > @LastPublishDate)
			set @GetCurrentRecipients = 0
	End

	if @GetCurrentRecipients = 1
	Begin
	
		-- either the campaign always updates or this is the first time
		declare curLists cursor fast_forward
		for
		select DistributionListId
		From MKCampaignDistributionList
		Where CampaignId = @CampaignId

		open curLists
		Fetch Next From curLists 
		Into @listId

		-- insert contacts from distribution list
		While @@Fetch_Status = 0
		Begin
		
			insert #tblContacts 
			(
			 Id
			,UserId
			,FirstName
			,MiddleName
			,LastName
			,CompanyName
			,BirthDate
			,Gender
			,AddressId
			,Status
			,HomePhone
			,MobilePhone
			,OtherPhone
			,ImageId
			,Notes
			,Email
			,ContactType
			,ContactSourceId)
			
			exec DistributionList_GetContacts @listId, @ApplicationId, @totalRecs

			Fetch Next From curLists 
			Into @listId
		End

		Close curLists
		deallocate curLists

		-- insert the manually added contacts
		insert #tblContacts
		(Id
			,UserId
			,FirstName
			,MiddleName
			,LastName
			,CompanyName
			,BirthDate
			,Gender
			,AddressId
			,Status
			,HomePhone
			,MobilePhone
			,OtherPhone
			,ImageId
			,Notes
			,Email
			,ContactType
			,ContactSourceId)
		select NewId(), c.UserId, c.FirstName, c.MiddleName, c.LastName, c.CompanyName, c.BirthDate,
			c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone, c.OtherPhone, c.ImageId, c.Notes, 
			c.Email, c.ContactType,c.ContactSourceId
		from vw_contacts c
			join MKCampaignUser cu on cu.UserId = c.UserId and cu.CampaignId = @CampaignId
			
			
	End
	Else
	Begin
		insert #tblContacts
		(Id
			,UserId
			,FirstName
			,MiddleName
			,LastName
			,CompanyName
			,BirthDate
			,Gender
			,AddressId
			,Status
			,HomePhone
			,MobilePhone
			,OtherPhone
			,ImageId
			,Notes
			,Email
			,ContactType
			,ContactSourceId)
		select NewId(), c.UserId, c.FirstName, c.MiddleName, c.LastName, c.CompanyName, c.BirthDate,
			c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone, c.OtherPhone,C.ImageId, c.Notes, 
			c.Email, c.ContactType,C.ContactSourceId
		from vw_contacts c
			join mkEmailSendLog sl on sl.UserId = c.UserId 
				and sl.CampaignId = @CampaignId
				and sl.SendDate > @LastPublishDate
	End

	--select count(*) from @tblContacts

	CREATE INDEX IDX_Tmp_Contacts
	on #tblContacts (UserId)

	-- remove users that have triggered watches
	if IsNull(@SendToNotTriggered, 0) = 1
	Begin
		Delete #tblContacts where UserId in(
			select distinct l.UserId
			from mkemailsendactions a
				join mkemailsendlog l on a.SendId = l.Id
				join mkcampaignrelevantwatches w on w.CampaignId = l.CampaignId and w.WatchId = a.TargetId
			where a.actiontype = 5
				and l.CampaignId = @CampaignId)
	End

	insert into MKCampaignRunWorkTable (CampaignRunId, UserId, FirstName, MiddleName, LastName,
		CompanyName, BirthDate, Gender, AddressId, Status, HomePhone, MobilePhone, OtherPhone,
		Notes, Email, ContactType)
	SELECT distinct TOP(@EmailPerCampaignLimit) @CampaignRunId, c.UserId, isnull(c.FirstName, ''), isnull(c.MiddleName, ''), isnull(c.LastName, ''),
		isnull(c.CompanyName, ''), isnull(c.BirthDate, ''), isnull(c.Gender, ''), c.AddressId, c.Status, isnull(c.HomePhone, ''), 
		isnull(c.MobilePhone, ''), 
		isnull(c.OtherPhone, ''), isnull(c.Notes, ''), isnull(c.Email, ''), c.ContactType
	from #tblContacts c
		-- get previous send record for the campaign
		left outer join MKEmailSendLog l on l.UserId = c.UserId and l.CampaignId = @CampaignId
		-- is user unsubscribed
		left outer join USUserUnsubscribe u on u.userid = c.userid and isnull(u.CampaignId, @CampaignId) = @CampaignId
		left outer join USResubscribe AS R ON R.UserID = c.UserId and r.CampaignId = @CampaignId
	where (u.Id is null or R.Id IS NOT NULL)  -- removes users that are either unsubscribed from this campaign or all campaigns
		and c.Status = 1
		and 
		(
			(@UniqueRecipientsOnly = 0)
			Or
			(@UniqueRecipientsOnly = 1 and l.Id is null) -- removes users that have received this campaign previously
		)

		--Remove contacts that are already sent for the campaignRunId in case of partiallysent campaigns
		Delete from MKCampaignRunWorkTable where UserId in(select UserId from MKEmailSendLog  where CampaignRunId=@CampaignRunId)

	select count(*) 'TotalContacts' from MKCampaignRunWorkTable where CampaignRunId = @CampaignRunId
End
GO
PRINT 'Altering procedure CampaignRunHistory_GetByCampaign'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetByCampaign]
(
	@CampaignId uniqueidentifier,
	@CampaignRunId uniqueidentifier=null,
    @PageNumber int =null,
    @PageSize int =null,
	@SortBy nvarchar(50) = 'RunDate desc',  
	@TotalRecords int  =null out,
	@ApplicationId uniqueidentifier 
)
AS
Begin
IF @PageNumber is null 
	BEGIN
	Select 
		Id
		,CampaignId
		,dbo.ConvertTimeFromUtc(RunDate,@ApplicationId)RunDate 
		,Sends
		,Delivered
		,Opens
		,Clicks
		,Unsubscribes
		,Bounces
		,TriggeredWatches
		,EmailSubject
		,SenderName
		,SenderEmail
		,CostPerEmail
		,ConfirmationEmail
		,EmailHtml
		,CMSPageId
		,EmailText
		,dbo.GetUniqueOpenInCampaignMail(Id) As UniqueOpen
	From MKCampaignRunHistory
	Where CampaignId = @CampaignId
	AND Id =isnull( @CampaignRunId,Id)  order by RunDate desc
	END
	ELSE
	BEGIN

 if @SortBy is null
			set @SortBy = 'RunDate desc'
			Declare @sql varchar(max)

			Set @sql='
			   With tmpContacts
			   AS (
					Select
					  Id
		,CampaignId
		,dbo.ConvertTimeFromUtc(RunDate,''' + cast(@ApplicationId as varchar(36)) + ''')RunDate 
		,Sends
		,Delivered
		,Opens
		,Clicks
		,Unsubscribes
		,Bounces
		,TriggeredWatches
		,EmailSubject
		,SenderName
		,SenderEmail
		,CostPerEmail
		,ConfirmationEmail
		,EmailHtml
		,CMSPageId
		,EmailText
		,dbo.GetUniqueOpenInCampaignMail(Id) As UniqueOpen
	From MKCampaignRunHistory
	Where CampaignId ='''+ cast( @CampaignId as varchar(36)) + ''')
		
				
				Select  A.*   
					FROM  
					 (  
					 Select *,  ROW_NUMBER() OVER(Order by ' + @SortBy + '
												) as RowNumber  
					  from tmpContacts  
					 ) A  
					where A.RowNumber >=' +  cast((@PageSize * @PageNumber) - (@PageSize -1 ) as varchar(10))  + 
						' AND A.RowNumber <=' + cast((@PageSize * @PageNumber) as varchar(10)) 
				 exec(@sql)

	Set @TotalRecords =(Select count(*)  From MKCampaignRunHistory
	Where CampaignId = @CampaignId)

	END
End
GO
PRINT 'Alter procedure LinkGroup_Exists'
GO
ALTER PROCEDURE [dbo].[LinkGroup_Exists]
(
	@Id uniqueidentifier,
	@Description varchar(50),
	@ApplicationId UniqueIdentifier
)
AS
Begin
	Declare
		@Exists bit,
		@Count int

	set @Exists = 0
	
	select @Count = count(*)
	from MKLinkGroup
	where lower(Description) = lower(@Description)
		and Id <> @Id and Applicationid =@ApplicationId
		
	if @Count > 0
		Set @Exists = 1
		
	Select @Exists 'Exists'
End
GO
PRINT 'Altering procedure LinkGroup_Get'
GO
ALTER PROCEDURE [dbo].[LinkGroup_Get]
	@Id UniqueIdentifier,
	@ApplicationId UniqueIdentifier
As
Begin
	Select 
		Id
		,Description
		,Applicationid
		,CreatedBy
		,CreatedDate AS  CreatedDate
		,ModifiedBy
		,ModifiedDate AS  ModifiedDate
	From MKLinkGroup
	Where Id = @Id and ApplicationId=@ApplicationId
End
GO
PRINT 'Altering procedure ManagedLink_Exists'
GO
ALTER PROCEDURE [dbo].[ManagedLink_Exists]
(
	@Id uniqueidentifier,
	@Title nvarchar(1000),
	@ApplicationId UniqueIdentifier
)
AS
Begin
	Declare
		@Exists bit,
		@Count int

	set @Exists = 0
	
	select @Count = count(*)
	from MKManagedLink
	where lower(Title) = lower(@Title)
		and Id <> @Id and ApplicationId=@ApplicationId
		
	if @Count > 0
		Set @Exists = 1
		
	Select @Exists 'Exists'
End
GO
PRINT 'Altering procedure ManagedLink_Get'
GO
ALTER PROCEDURE [dbo].[ManagedLink_Get]
	@Id UniqueIdentifier,
	@ApplicationId UniqueIdentifier
As
Begin
	Select 
	Id
	,Title
	,Url
	,ApplicationId
	,CreatedBy
	,CreatedDate AS CreatedDate
	,ModifiedBy
	,ModifiedDate as ModifiedDate
	,LinkGroupId
	From MKManagedLink
	Where Id = @Id and ApplicationId=@ApplicationId
End
GO
PRINT 'Creating procedure ContactDto_GetTimeZoneSites'
GO
IF (OBJECT_ID('ContactDto_GetTimeZoneSites') IS NOT NULL)
	DROP PROCEDURE ContactDto_GetTimeZoneSites

GO
CREATE PROCEDURE ContactDto_GetTimeZoneSites(@CampaignRunId uniqueidentifier)
AS
BEGIN
	SELECT Distinct SiteId Id FROM MKCampaignRunTimeZoneSites WHERE CampaignRunId = @CampaignRunId AND StartTime >= GETUTCDATE()
END
GO
PRINT 'Creating procedure Campaign_GetLandingPageClicks'
GO
IF (OBJECT_ID('Campaign_GetLandingPageClicks') IS NOT NULL)
	DROP PROCEDURE Campaign_GetLandingPageClicks
GO
Create procedure [dbo].[Campaign_GetLandingPageClicks](@CampaignId uniqueidentifier)  
as  
select CampaignId,PageId,sum(LinkClicks) as LinkClicks , sum(isnull(bounce,0)) as Bounce from FactCampaignActivity  
where CampaignId=@CampaignId group by CampaignId,PageId  

GO
PRINT 'Creating procedure Campaign_GetLandingPageVisits'
GO
IF (OBJECT_ID('Campaign_GetLandingPageVisits') IS NOT NULL)
	DROP PROCEDURE Campaign_GetLandingPageVisits
GO
CREATE PROCEDURE [dbo].[Campaign_GetLandingPageVisits](@CampaignId uniqueIdentifier)  
as   
SELECT PageId,sum(Clicks) as Clicks ,sum(Bounce) as Bounce FROM dbo.MkCampaignActivity  
where CampaignId=@CampaignId  
group by PageId
GO
PRINT 'Creating procedure ContactListDto_ExecuteFilter'
GO
IF (OBJECT_ID('ContactListDto_ExecuteFilter') IS NOT NULL)
	DROP PROCEDURE ContactListDto_ExecuteFilter
GO
CREATE PROCEDURE [dbo].[ContactListDto_ExecuteFilter]
    (
      @Id UNIQUEIDENTIFIER
	)
	  AS
	  BEGIN
	  DECLARE @SearchXml XML = NULL
	  DECLARE @SiteId UNIQUEIDENTIFIER 
	  DECLARE @TotalRecords INT

	  SET @SearchXml =( Select Q.SearchXml FROM TADistributionLists L
	  INNER JOIN TADistributionListSearch SQ ON L.Id =SQ.DistributionListId
	  INNER JOIN CTSearchQuery Q on SQ.SearchQueryId =Q.Id
	  Where L.Id =@Id)

	  IF @SearchXml IS NOT NULL
	  BEGIN

	     DELETE  FROM TADistributionListUser
                        WHERE   DistributionListId = @Id AND ContactListSubscriptionType = 1 

		DECLARE cursor_list CURSOR FOR  
		SELECT Distinct SiteId FROM TADistributionListSite Where DistributionListId=@Id

		OPEN cursor_list   
		FETCH NEXT FROM cursor_list INTO @SiteId   

		WHILE @@FETCH_STATUS = 0   
		BEGIN   
			   
			   

                        DECLARE @tblContacts TABLE
                            (
                              Id UNIQUEIDENTIFIER ,
                              UserId UNIQUEIDENTIFIER ,
                              FirstName VARCHAR(1024) NULL ,
                              MiddleName VARCHAR(1024) NULL ,
                              LastName VARCHAR(1024) NULL ,
                              CompanyName VARCHAR(1024) NULL ,
                              BirthDate DATETIME NULL ,
                              Gender VARCHAR(1024) NULL ,
                              AddressId UNIQUEIDENTIFIER NULL ,
                              Status INT NULL ,
                              HomePhone VARCHAR(1024) NULL ,
                              MobilePhone VARCHAR(1024) NULL ,
                              OtherPhone VARCHAR(1024) NULL ,
                              ImageId UNIQUEIDENTIFIER NULL ,
                              Notes VARCHAR(MAX) NULL ,
                              Email VARCHAR(256) ,
                              ContactType INT ,
                              ContactSourceId INT ,
                              TotalRecords INT
                            )

                        BEGIN TRY
                        INSERT  INTO @tblContacts
                                EXEC Contact_SearchContact @Xml = @SearchXml,
                                    @ApplicationId = @SiteId, @MaxRecords = 0,
									@ContactListId = @Id,
                                    @TotalRecords = @TotalRecords OUTPUT,
										@IncludeAddress =0
						END TRY
						BEGIN CATCH
						print 'Error Running List-' + cast(@Id as char(36)) + ' Error-' + ERROR_MESSAGE()
						END CATCH

                        INSERT  INTO TADistributionListUser
                                ( Id ,
                                  DistributionListId ,
                                  UserId
                                )
                                SELECT  C.Id ,
                                        @Id DistributionListId ,
                                        C.UserId
                                FROM    @tblContacts C
                                LEFT JOIN dbo.TADistributionListUser TA ON TA.UserId = C.UserId AND TA.DistributionListId = @Id
                                WHERE TA.Id IS NULL

					Update TADistributionListSite SET [Count]=@TotalRecords,LastSynced=GETUTCDATE() Where DistributionListId=@Id AND SiteId=@SiteId

		FETCH NEXT FROM cursor_list INTO @SiteId   
		END   

		CLOSE cursor_list   
		DEALLOCATE cursor_list
END
END
GO
PRINT 'Altering procedure CMSSite_CreateDefaultData'
GO
ALTER PROCEDURE [dbo].[CMSSite_CreateDefaultData] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@SiteId uniqueidentifier,
	@CreatedBy uniqueidentifier,
	@CopyFromSiteId uniqueidentifier = null
)
as	
BEGIN
	Declare @Now datetime 
	SET @Now =getutcdate()
	Declare @SiteTitle varchar(512)
	--Get the Site Information
	Select @SiteTitle = Title from SISite Where Id = @SiteId

	Declare @p1 uniqueidentifier
	EXEC Taxonomy_Save 
		@Id=@p1 output,
		@ApplicationId=@SiteId,
		@Title=N'Index Terms', 
		@CreatedBy=@CreatedBy,
		@Status=1

	Insert into PageMapBase (SiteId, ModifiedDate)
		Values(@SiteId, @Now)
	
	DECLARE @pageMapNodeXml NVARCHAR(max)
	DECLARE @pageMapNodeId uniqueidentifier
	SET @pageMapNodeId = @SiteId
	
	--SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
	--					 displayTitle="CommerceNav" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="1" 
	--					 targetId="00000000-0000-0000-0000-000000000000" 
	--					 propogateWorkFlow="True" inheritWorkFlow="False" 
	--					 propogateSecurityLevels="True" inheritSecurityLevels="False" 
	--					 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'
						 
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" description="' + @SiteTitle + '"  
		   displayTitle="' + @SiteTitle + '" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="1"   
		   targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
		   createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
		   propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="True"   
		   inheritRoles="False" roles="" securityLevels="" locationIdentifier="" />'  						 

	EXEC PageMapNode_Save @Id =@pageMapNodeId,
			@SiteId =@SiteId,
			@ParentNodeId = NULL,
			@PageMapNode= @pageMapNodeXml,
			@CreatedBy = @CreatedBy
			
	SET @pageMapNodeId = NEWID()
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
						 displayTitle="Unassigned" friendlyUrl="Unassigned" description="Unassigned" menuStatus="1" 
						 parentId="' + CAST(@SiteId AS CHAR(36)) + '" 
					     targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
						 createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
						 propogateSecurityLevels="True" inheritSecurityLevels="False" 
						 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'

	EXEC PageMapNode_Save @Id = @pageMapNodeId,
			@SiteId = @SiteId,
			@ParentNodeId = @SiteId,
			@PageMapNode = @pageMapNodeXml,
			@CreatedBy = @CreatedBy

	Declare @ProductId uniqueidentifier
    Select @ProductId = Id from iAppsProductSuite where Title like 'Analyzer'
    if(@ProductId is not null)
      exec Membership_CreateDefaultMembership @SiteId,@ProductId
	Select @ProductId = Id from iAppsProductSuite where Title like 'Commerce'
	if(@ProductId is not null)
		exec Membership_CreateDefaultMembership @SiteId,@ProductId
	Select @ProductId = Id from iAppsProductSuite where Title like 'Marketier'
	if(@ProductId is not null)
		exec Membership_CreateDefaultMembership @SiteId,@ProductId

	insert into STSiteSetting
	select 
	@SiteId,
	SettingTypeId,
	Value
	from STSiteSetting where 
	SiteId IN (
		select top 1 S.Id from SISite S Where Status = 1 and Id in 
		(Select distinct SiteId from STSiteSetting)
	)


	-- Otherwise it won't display in workflow author/approver/publisher tab. below it is adding for the site 

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId , Id, 2, 1, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Global Author')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 2, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Global Approver')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 3, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Global Publisher')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 4, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Content Administrator')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 5, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Site Administrator')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 6, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Installation Administrator')


	-- GLObjectType
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Site')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(1,'Site')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='MenuItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(2,'MenuItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Template')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(3,'Template')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Style')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(4,'Style')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Container')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(5,'Container')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='SiteDirectory')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(6,'SiteDirectory')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Content')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(7,'Content')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Page')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(8,'Page')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='AssetFile')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(9,'AssetFile')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Folder')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(10,'Folder')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='List')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(11,'List')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Taxonomy')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(12,'Taxonomy')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='XMLForm')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(13,'XMLForm')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='User')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(14,'User')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Group')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(15,'Group')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Role')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(16,'Role')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Permission')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(17,'Permission')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Workflow')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(18,'Workflow')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Job')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(19,'Job')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Task')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(20,'Task')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Emails')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(21,'Emails')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='DistributionList')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(22,'DistributionList')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='EmailTemplate')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(23,'EmailTemplate')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='EventType')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(24,'EventType')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='FWFeed')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(25,'FWFeed')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssChannel')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(26,'RssChannel')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssCategory')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(27,'RssCategory')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssCloud')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(28,'RssCloud')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssImage')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(29,'RssImage')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RegisteredRssItemHandler')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(30,'RegisteredRssItemHandler')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Links')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(31,'Links')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Version')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(32,'Version')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='AssetImage')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(33,'AssetImage')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Calendar')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(34,'Calendar')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Script')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(35,'Script')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Product')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(36,'Product')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='AudienceSegment')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(37,'AudienceSegment')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Form')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(38,'Form')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Blog')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(39,'Blog')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Post')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(40,'Post')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='FormResponse')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(47,'FormResponse')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Video')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(48,'Video')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Store')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(201,'Store')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Catalog')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(202,'Catalog')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Category')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(203,'Category')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ProductType')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(204,'ProductType')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='CommerceProduct')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(205,'CommerceProduct')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='SKU')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(206,'SKU')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Filter')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(207,'Filter')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ProductImage')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(208,'ProductImage')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='NavItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(209,'NavItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Inventory')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(210,'Inventory')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Customer')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(211,'Customer')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Order')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(212,'Order')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='OrderShipping')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(213,'OrderShipping')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='OrderItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(214,'OrderItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingWishList')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(215,'ShoppingWishList')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingWishListItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(216,'ShoppingWishListItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingCart')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(217,'ShoppingCart')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingCartItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(218,'ShoppingCartItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='FeaturedItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(219,'FeaturedItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='CrossSellUpSellItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(220,'CrossSellUpSellItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Bundle')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(221,'Bundle')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Campaign')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(301,'Campaign')  END

	-- Roles
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =1)  BEGIN  INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal) Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',1,'GlobalAuthor','Author for all ObjectTypes',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =2)  BEGIN  INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal) 	Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',2,'GlobalApprover','Approver for all ObjectTypes',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =3)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',3,'GlobalPublisher','Publisher for all ObjectTypes',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =4)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',4,'ContentAdministrator','Content Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =5)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',5,'SiteAdministrator','Site Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =6)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',6,'InstallationAdministrator','Installation Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =7)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',7,'Author','Represents Author role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =8)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',8,'Approver','Represents Approver role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =9)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',9,'Publisher','Represents Publisher role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =10)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',10,'Archiver','Represents Archiver role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =11)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',11,'NavEditor','Represents Navigational editor role in the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =12)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',12,'Manager','Represents Manage role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =13)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',13,'Viewer','Represents View role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =14)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',14,'CustomPermission','Represents CustomPermission role in the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =15)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',15,'EveryOne','Represents EveryOne role in the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =16)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CAB9FF78-9440-46C3-A960-27F53D743D89',16,'Administrator','Represents Administrator role in the Analytics product',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =17)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CAB9FF78-9440-46C3-A960-27F53D743D89',17,'Analyst','Represents Analyst role in the Analytics product',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =18)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CAB9FF78-9440-46C3-A960-27F53D743D89',18,'AnalystCustomPermission','Represents AnalystCustompermission role in the Analytics product',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =19)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',19,'Merchandising','Merchandising for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =20)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',20,'Fulfillment','Fulfillment for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =21)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',21,'Marketing','Marketing for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =22)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',22,'CSR','CSR for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =23)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',23,'BlogAdministrator','Blog Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =24)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',24,'MarketierAdmin','Marketier Admin for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =25)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',25,'Marketer','Marketer for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =26)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',26,'LibraryAdmin','Library Admin for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =27)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',27,'GlobalCampaignApprover','Global Campaign Approver',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =28)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',28,'GlobalCampaignPublisher','Global Campaign Publisher',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =29)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',29,'EveryOne','Commerce Everyone for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =30)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',30,'CustomNavEditor','Represents custom Navigational editor role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =31)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',31,'GlobalCampaignAuthor','Global Campaign Author',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =32)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',32,'CSRManager','CSR Manger for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =33)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',33,'WHManager','Warehouse Manger for the system',1)  END

	EXEC [CMSSite_InsertDefaultPermission] @SiteId
	-- copy all existing main admin users to new site
	--IF NOT EXISTS (SELECT * FROM USMemberGroup WHERE ApplicationId=@SiteId)   
	--BEGIN   
		INSERT INTO USMemberGroup (ApplicationId, MemberId, MemberType, GroupId)
		SELECT distinct @SiteId, MemberId, MemberType, GroupId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
		) AND ApplicationId = @CopyFromSiteId
		
		Except
		
		SELECT distinct @SiteId, MemberId, MemberType, GroupId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
		) AND ApplicationId = @SiteId
		
	--END

	--IF NOT EXISTS (SELECT * FROM USSiteUser WHERE SiteId=@SiteId)   
	--BEGIN
		INSERT INTO USSiteUser (SiteId, UserId, IsSystemUser, ProductId)    
		
		SELECT @SiteId, UserId, IsSystemUser, ProductId FROM USSiteUser WHERE UserId IN (
			SELECT distinct MemberId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
			) AND ApplicationId = @CopyFromSiteId ) 
		AND SiteId = @CopyFromSiteId
		
		Except
		
		SELECT @SiteId, UserId, IsSystemUser, ProductId FROM USSiteUser WHERE UserId IN (
			SELECT distinct MemberId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
			) AND ApplicationId = @CopyFromSiteId ) 
		AND SiteId = @SiteId
		
	--END

	--- Inserting default SiteSetting Data
	EXEC CMSSite_CreateDefaultSiteSetting @SiteId  --- This will call CodeLibrary_CreateDefaultSiteSetting SP

	EXEC Site_CreateCommerceDefaultMenuAndPages  @SiteId, @SiteTitle, @CreatedBy

	EXEC Site_CopyCommerceDefaultData @CopyFromSiteId, @SiteId ,@SiteTitle, @CreatedBy

	EXEC Site_CreateCommerceDefaultData @SiteId,@SiteTitle, @CreatedBy

	EXEC [Site_CreateTemplateBuilderData] @SiteId


	 INSERT INTO [dbo].[TAEmailTemplate]  
		  ([Id]  
		  ,[Key]  
		  ,[Title]  
		  ,[Description]  
		  ,[Subject]  
		  ,[MimeType]  
		  ,[CreatedBy]  
		  ,[CreatedDate]  
		  ,[ModifiedBy]  
		  ,[ModifiedDate]  
		  ,[Status]  
		  ,[ApplicationId]  
		  ,[SenderDefaultEmail]  
		  ,[MailMergeTags]  
		  ,[ProductId]  
		  ,[Body])  
	 SELECT NEWID()  
		,[Key]  
		,[Title]  
		,[Description]  
		,[Subject]  
		,[MimeType]  
		,[CreatedBy]  
		,GETUTCDATE()  
		,[ModifiedBy]  
		,GETUTCDATE()  
		,[Status]  
		,@SiteId  
		,[SenderDefaultEmail]  
		,[MailMergeTags]  
		,[ProductId]  
		,[Body]  
	   FROM [dbo].[TAEmailTemplate] 
	WHERE Title IN (   
	SELECT  
		[Title]  
	   FROM [dbo].[TAEmailTemplate]  
	  Where ApplicationId = @copyFromSiteId  
	Except
	SELECT  
		[Title]  
	   FROM [dbo].[TAEmailTemplate]  
	  Where ApplicationId = @SiteId  
	)  AND ApplicationId = @copyFromSiteId  
	Order by Title 

	--CREATE DEFAULT MARKETIER TEMPLATES BEGIN
	--DECLARE	@Templates AS NewPageTemplates

	--INSERT INTO @Templates (DirectoryPath, Title, AscxFile, CsFile, ImageFile, Type, PagePart)
	--	VALUES	('~/Page Template Library/Email Templates/Generic', 'One Column', 'One-Column.ascx', 'One-Column.ascx.cs', 'One-Column.gif', 1, 0),
	--			('~/Page Template Library/Email Templates/Generic', 'Two Columns', 'Two-Columns.ascx', 'Two-Columns.ascx.cs', 'Two-Columns.gif', 1, 0),
	--			('~/Page Template Library/Email Templates/Generic', 'Three Columns', 'Three-Columns.ascx', 'Three-Columns.ascx.cs', 'Three-Columns.gif', 1, 0),
	--			('~/Page Template Library/Email Templates/Generic', 'Sidebar Left', 'Sidebar-Left.ascx', 'Sidebar-Left.ascx.cs', 'Sidebar-Left.gif', 1, 0),
	--			('~/Page Template Library/Email Templates/Generic', 'Sidebar Right', 'Sidebar-Right.ascx', 'Sidebar-Right.ascx.cs', 'Sidebar-Right.gif', 1, 0)

	--EXEC Site_AddPageTemplates @SiteId, null, @Templates

	
	--CREATE DEFAULT MARKETIER TEMPLATES END



	--Create form structure 
	--Form library node
	/*
if not exists (select 1 from COFormStructure Where SiteId=@SiteId)
Begin
	
	declare @NewFormStructureId uniqueidentifier
	SET @NewFormStructureId = NEWID()
	--Form library node
	insert into COFormStructure 
	(
	Id,
	ParentId,
	LftValue,
	RgtValue,
	Title,
	Description,
	SiteId,
	[Exists],
	Attributes,
	Size,
	Status,
	CreatedBy,
	CreatedDate,
	ModifiedBy,
	ModifiedDate,
	PhysicalPath,
	VirtualPath,
	ThumbnailImagePath,
	FolderIconPath,
	Keywords,
	IsSystem,
	IsMarketierDir,
	SourceDirId,
	AllowAccessInChildrenSites
	)
	SELECT @NewFormStructureId,
	@SiteId,
	1,
	4,
	Title,
	Description,
	@SiteId,
	[Exists],
	Attributes,
	Size,
	Status,
	CreatedBy,
	@Now,
	ModifiedBy,
	ModifiedDate,
	PhysicalPath,
	VirtualPath,
	ThumbnailImagePath,
	FolderIconPath,
	Keywords,
	IsSystem,
	IsMarketierDir,
	SourceDirId,
	AllowAccessInChildrenSites
	FROM COFormStructure
	WHERE ParentId=@copyFromSiteId


	if not exists (select 1 from HSDefaultParent where SiteId=@SiteId And ObjectTypeId=38)
	begin

		----insert into default parent
		INSERT INTO HSDefaultParent
		(
		SiteId,
		ObjectTypeId,
		Title,
		ParentId
		)
		VALUES
		(
		@SiteId,
		38,
		NULL, --form
		@NewFormStructureId
		)

	end

	--For Unassigned node
	insert into COFormStructure 
	(
	Id,
	ParentId,
	LftValue,
	RgtValue,
	Title,
	Description,
	SiteId,
	[Exists],
	Attributes,
	Size,
	Status,
	CreatedBy,
	CreatedDate,
	ModifiedBy,
	ModifiedDate,
	PhysicalPath,
	VirtualPath,
	ThumbnailImagePath,
	FolderIconPath,
	Keywords,
	IsSystem,
	IsMarketierDir,
	SourceDirId,
	AllowAccessInChildrenSites
	)
	SELECT 
	TOP 1
	newid(),
	@NewFormStructureId,--THis needs to be hard coded
	2,
	3,
	Title,
	Description,
	@SiteId,
	[Exists],
	Attributes,
	Size,
	Status,
	CreatedBy,
	@Now,
	ModifiedBy,
	ModifiedDate,
	PhysicalPath,
	VirtualPath,
	ThumbnailImagePath,
	FolderIconPath,
	Keywords,
	IsSystem,
	IsMarketierDir,
	SourceDirId,
	AllowAccessInChildrenSites
	FROM COFormStructure
	WHERE SiteId=@copyFromSiteId and Title='Unassigned' AND LftValue=2 and RgtValue=3 

End
*/

END



GO
PRINT 'Creating procedure Contact_AddManualContacts'
GO
IF (OBJECT_ID('Contact_AddManualContacts') IS NOT NULL)
	DROP PROCEDURE Contact_AddManualContacts

GO
CREATE PROCEDURE [dbo].[Contact_AddManualContacts]
    (
      @ContactListId UNIQUEIDENTIFIER
    )
AS 
    BEGIN
        IF ( @ContactListId IS NOT NULL
             AND @ContactListId != dbo.GetEmptyGuid()
           ) 
            BEGIN

                INSERT  INTO #tempContactSearchOutput
                        SELECT  UserId
                        FROM    TADistributionListUser DU
                                LEFT JOIN #tempContactSearchOutput SO ON SO.Id = DU.UserId
                        WHERE   DistributionListId = @ContactListId
                                AND ContactListSubscriptionType = 2
                                AND SO.Id IS NULL

            END
    END
    SELECT @@ROWCOUNT
GO
PRINT 'Creating procedure ContactListUserDto_Save'
GO
IF (OBJECT_ID('ContactListUserDto_Save') IS NOT NULL)
	DROP PROCEDURE ContactListUserDto_Save

GO

CREATE PROCEDURE [dbo].[ContactListUserDto_Save]
    (
      @UserIds XML ,
      @DistributionListId UNIQUEIDENTIFIER ,
      @ContactListSubscriptionType INT
	
    )
AS 
    DELETE  FROM TADistributionListUser
    WHERE   UserId IN ( SELECT  A.n.value('@Id', 'uniqueidentifier')
                        FROM    @UserIds.nodes('/UserIds/users') AS A ( n ) )
            AND DistributionListId = @DistributionListId

    INSERT  INTO dbo.TADistributionListUser
            ( Id ,
              DistributionListId ,
              UserId ,
              ContactListSubscriptionType
            )
            SELECT  NEWID() ,
                    @DistributionListId ,
                    A.n.value('@Id', 'uniqueidentifier') AS UserId ,
                    @ContactListSubscriptionType
            FROM    @UserIds.nodes('/UserIds/users') AS A ( n )
            
GO
PRINT 'Creating procedure ContactListUserDto_Delete'
GO
IF (OBJECT_ID('ContactListUserDto_Delete') IS NOT NULL)
	DROP PROCEDURE ContactListUserDto_Delete

GO
CREATE PROCEDURE [dbo].[ContactListUserDto_Delete]
    (
      @Id UNIQUEIDENTIFIER,
      @ModifiedBy  UNIQUEIDENTIFIER
    )
AS 
    DELETE  FROM TADistributionListUser
    WHERE    DistributionListId = @Id
GO

PRINT 'Creating procedure Site_CreateMicrositeDataForMarketier'
GO
IF (OBJECT_ID('Site_CreateMicrositeDataForMarketier') IS NOT NULL)
	DROP PROCEDURE Site_CreateMicrositeDataForMarketier
GO
CREATE PROCEDURE [dbo].[Site_CreateMicrositeDataForMarketier]
(
	@MicroSiteId uniqueidentifier
	--@PageImportOptions
)
AS
BEGIN

--Pointer used for text / image updates. This might not be needed, but is declared here just in case
	DECLARE @pv binary(16)
	DECLARE @ApplicationId uniqueidentifier
	SET @ApplicationId = @MicroSiteId

	DECLARE @CommerceProductId uniqueidentifier
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'

	DECLARE @MarketierProductId uniqueidentifier
	SET @MarketierProductId='CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'

	--DECLARE @UnsubscribePageId  uniqueidentifier
	--SET @UnsubscribePageId='18F27A17-03B2-4482-B375-3C48577B5702'

	--if not exists (select 1 from COContentStructure where VirtualPath='~/Content Library/MarketierLibrary' and SiteId=@MicroSiteId)
	--begin
	BEGIN TRANSACTION

	--========== Creating ManageContentLibrary folder for marketier
		DECLARE @ContentLibraryId uniqueidentifier, @MarketierContentLibrary uniqueidentifier, @unassignedFoldeId uniqueidentifier
		SELECT @ContentLibraryId = Id from COContentStructure where Title = 'Content Library' AND SiteId=@MicroSiteId
		SET @ContentLibraryId = Isnull(@ContentLibraryId, @ApplicationId)
		SET @MarketierContentLibrary = newid()--'E196A2C5-C070-4904-B93D-DFECED4ADABF'
		SET @unassignedFoldeId = newid() --'B843F49B-1427-48D9-A439-CBD265B36077'


		
			declare @p1 xml
			set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="' + cast(@MarketierContentLibrary as char(36)) + '" Title="MarketierLibrary" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@ContentLibraryId as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
			declare @p5 nvarchar(max)
			set @p5=N'<SISiteDirectory Id="'+ cast(@MarketierContentLibrary as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary" Attributes="0"/>'
			declare @p6 nvarchar(256)
			set @p6=N'ACME'
			IF NOT EXISTS (Select 1 from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)
			exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@ContentLibraryId,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
			ELSE
				SET @MarketierContentLibrary = (Select top 1 Id  from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)

		  --UPDATING SITE SETTINGS FOR MARKETIER CONTENT LIBRARY
		  declare @MarketierContentLibSettingTypeId int 
		  SELECT @MarketierContentLibSettingTypeId = Id  FROM STSettingType Where Name = 'MarketierContentLibraryId'

		  IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		  BEGIN
			IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierContentLibSettingTypeId) )
			BEGIN
				INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
				VALUES (@MicroSiteId,@MarketierContentLibSettingTypeId,@MarketierContentLibrary)
			END 
			ELSE
			BEGIN
				update [dbo].[STSiteSetting] set Value = @MarketierContentLibrary where SettingTypeId=@MarketierContentLibSettingTypeId AND SiteId=@MicroSiteId
			END
		 END
		
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
		--declare @p1 xml
		set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="'+ cast(@unassignedFoldeId as char(36)) + '" Title="Unassigned" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@MarketierContentLibrary as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
		--declare @p5 nvarchar(max)
		set @p5=N'<SISiteDirectory Id="'+cast(@unassignedFoldeId as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary/Unassigned" Attributes="0"/>'
		--declare @p6 nvarchar(256)
		set @p6=N'ACME'
		IF NOT EXISTS (Select 1 from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@MarketierContentLibrary,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
		ELSE 
			SET @unassignedFoldeId =( Select top 1 Id   from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	--============= Creating Maketier PageMapNodes 
		DECLARE @MarkierPageMapNodeRootId uniqueidentifier, @MarkierPageMapNodeId uniqueidentifier
		DECLARE @pmnXml xml
		DECLARE	@return_value int

		set @MarkierPageMapNodeRootId = newid() 
		print @MarkierPageMapNodeRootId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" description="MARKETIERPAGES" displayTitle="MARKETIERPAGES" friendlyUrl="MARKETIERPAGES" menuStatus="1" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@ApplicationId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  1:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels="" locationIdentifier="MARKETIERPAGES"/>'

		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeRootId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @ApplicationId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		
		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
 
		set @MarkierPageMapNodeId = newid() 
		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeId) +'" description="MARKETIERCAMPAIGN" displayTitle="MARKETIERCAMPAIGN" friendlyUrl="MARKETIERCAMPAIGN" menuStatus="0" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  6:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels=""/>'

		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @MarkierPageMapNodeRootId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		--UPDATING SITE SETTINGS FOR MARKETIER Campaign Page map node id
		declare @MarketierPageMapNodeSettingTypeId int 
		SELECT @MarketierPageMapNodeSettingTypeId = Id  FROM STSettingType Where Name = 'CampaignPageMapNodeId'

		IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		BEGIN
		IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierPageMapNodeSettingTypeId) )
		BEGIN
			INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
			VALUES (@MicroSiteId,@MarketierPageMapNodeSettingTypeId,@MarkierPageMapNodeId)
		END 
		ELSE
		BEGIN
			update [dbo].[STSiteSetting] set Value = @MarkierPageMapNodeId where SettingTypeId=@MarketierPageMapNodeSettingTypeId AND SiteId=@MicroSiteId
		END
		END
	--end
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END

	--===== Updaging existing commerce group 
		-- Before Commerce custom settings group was 2(i.e Custom) and now it changed to 4 (i.e CommerceCustomSettings)
		--update STSettingType set SettingGroupId=(select Id from STSettingGroup where Name='CommerceCustomSettings') where SettingGroupId=2

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	
IF @@ERROR<>0 
Begin
	ROLLBACK TRANSACTION
	GOTO QuitWithErrors
END
ELSE
BEGIN
	COMMIT TRANSACTION
END

QuitWithErrors:

END
GO
PRINT 'Altering procedure Contact_GetGroupAndDistributionListByUser'
GO
ALTER PROCEDURE [dbo].[Contact_GetGroupAndDistributionListByUser](
	@UserId UniqueIdentifier,@ApplicationId UniqueIdentifier,@FilterType int = 0 
	)
As
Begin

Declare @NotDistributionList table(Id uniqueidentifier)
Declare @TempDistributionListIds Table( DistributionListId uniqueidentifier)
--
--Declare @FilterType int
--Set @FilterType =-1
--Declare @UserId uniqueidentifier
--Set @UserId='54D0CC3E-FA3E-4E87-935F-614BBFEE60CF'
--Declare @ApplicationId uniqueidentifier
--Set @ApplicationId ='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

Declare @userIdCollection nvarchar(200)
Set @userIdCollection ='<GenericCollectionOfGuids><guid>' + cast(@UserId as varchar(36)) + '</guid></GenericCollectionOfGuids>'

insert into @TempDistributionListIds Select DistributionListId from TADistributionListUser where UserId = @UserId AND ContactListSubscriptionType != 3
if(@UserId <>'00000000-0000-0000-0000-000000000000' and @FilterType =-1)
BEGIN

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/IndexTerms/guid/text())[1]','uniqueidentifier') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
Except 
Select S.DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join vw_UserTaxonomy P ON P.TaxonomyId= Q.SearchXml.value('(ContactSearch/IndexTerms/guid/text())[1]','uniqueidentifier')  
Where P.UserId=@UserId

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/SecurityLevels/int/text())[1]','int') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join USUserSecurityLevel P ON P.SecurityLevelId= Q.SearchXml.value('(ContactSearch/SecurityLevels/int/text())[1]','int')  
Where P.UserId=@UserId
AND S.DistributionListId not in (Select Id From @NotDistributionList)

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/CustomerGroups/guid/text())[1]','uniqueidentifier') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join USMemberGroup P ON P.GroupId= Q.SearchXml.value('(ContactSearch/CustomerGroups/guid/text())[1]','uniqueidentifier')
Where P.MemberId=@UserId
AND S.DistributionListId not in (Select Id From @NotDistributionList)

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/Watches/guid/text())[1]','uniqueidentifier') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join TRUserWatch P ON P.WatchId= Q.SearchXml.value('(ContactSearch/Watches/guid/text())[1]','uniqueidentifier')
Where P.UserId=@UserId

-- Website User



-- Product Purchased
Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/ProductsPurchased/KeyValuePair/@Key)[1]','uniqueidentifier') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join  (SELECT Distinct	 Pr.Id as ProductId,  
			 O.CustomerId
			FROM   
			 OROrder O     
			 INNER JOIN OROrderItem OI ON OI.OrderId = O.Id  
			 INNER JOIN PRProductSKU PSKU ON OI.ProductSKUId = PSKU.Id  
			 INNER JOIN PRProduct Pr ON Pr.Id = PSKU.ProductId
					) A 
	ON A.ProductId =  Q.SearchXml.value('(ContactSearch/ProductsPurchased/KeyValuePair/@Key)[1]','uniqueidentifier')
Where A.CustomerId=@UserId

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/FormSubmitted/guid/text())[1]','uniqueidentifier') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join FormsResponse P ON P.FormsId =Q.SearchXml.value('(ContactSearch/FormSubmitted/guid/text())[1]','uniqueidentifier')
Where P.UserId=@UserId

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND len(Q.SearchXml.value('(ContactSearch/LastLogin/@Value1)[1]','nvarchar(20)'))>0 AND  len(Q.SearchXml.value('(ContactSearch/LastLogin/@Value2)[1]','nvarchar(20)')) >0
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join USMembership M ON 
							(cast(convert(varchar(12),dbo.ConvertTimeFromUtc(M.LastLoginDate,@ApplicationId),101) as datetime)
					between
						cast(convert(varchar(12),Q.SearchXml.value('(ContactSearch/LastLogin/@Value1)[1]','DateTime'),101) as datetime)
							AND 
						cast(convert(varchar(12),Q.SearchXml.value('(ContactSearch/LastLogin/@Value2)[1]','DateTime'),101) as datetime)
							)
Where M.UserId=@UserId

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND len(Q.SearchXml.value('(ContactSearch/Purchase/@Value1)[1]','nvarchar(20)'))>0 AND  len(Q.SearchXml.value('(ContactSearch/Purchase/@Value2)[1]','nvarchar(20)')) >0
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner join OROrder O ON 
							(cast(convert(varchar(12),dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId),101) as datetime)
					between
						cast(convert(varchar(12),Q.SearchXml.value('(ContactSearch/Purchase/@Value1)[1]','DateTime'),101) as datetime)
							AND 
						cast(convert(varchar(12),Q.SearchXml.value('(ContactSearch/Purchase/@Value2)[1]','DateTime'),101) as datetime)
							)
-- checking status

Insert into @NotDistributionList 
Select DistributionListId
from dbo.TADistributionListSearch S,CTSearchQuery Q 
where Q.Id = S.SearchQueryId 
AND Q.SearchXml.value('(ContactSearch/Status/text())[1]','int') is not null
AND S.DistributionListId not in (Select Id From @NotDistributionList)
except 
select  DistributionListId
from dbo.TADistributionListSearch S
Inner join CTSearchQuery Q ON Q.Id = S.SearchQueryId 
Inner Join USUser U ON U.Status =Q.SearchXml.value('(ContactSearch/Status/text())[1]','int')
Declare @TempTable table(Id uniqueidentifier)
Declare @ListId uniqueidentifier
Declare @SearchQuery xml
Declare @JunkTable table(cnt int)
declare  curIds cursor for 
			Select S.Id,SearchXml.query('ContactSearch/WebsiteUserSearch') 
			from TADistributionLists S 
			Inner Join dbo.TADistributionListSearch SL ON S.Id =SL.DistributionListId
			Inner join CTSearchQuery Q ON Q.Id = SL.SearchQueryId
			Where S.Id not in (Select Id from @NotDistributionList) And S.ListType=1 and S.Status = dbo.GetActiveStatus()
			open curIds
			Fetch next from curIds into @ListId,@SearchQuery
			WHILE @@FETCH_STATUS = 0
				BEGIN
					IF EXISTS(Select * from tempdb.dbo.sysobjects o where o.xtype in ('U')	and	o.id = object_id( N'tempdb..#tempContactSearchOutput'))
                        Truncate table #tempContactSearchOutput
                     ELSE
                        create table #tempContactSearchOutput(Id uniqueidentifier)
                 
                 Insert into #tempContactSearchOutput
                 Values(@UserId)
				Insert into @JunkTable
				Exec [dbo].[Contact_SearchWebsiteUser] @SearchQuery,@ApplicationId
				
				Insert into @TempTable
				Select Id from #tempContactSearchOutput
				
				if not exists (Select 1 from @TempTable Where Id=@UserId)			
				begin
						Insert into @NotDistributionList(Id)
						Values(@ListId)
					end
				delete from @TempTable
				Fetch next from curIds into @ListId,@SearchQuery
				END
			CLOSE curIds
			Deallocate curIds
Insert into @TempDistributionListIds
Select S.Id
			from TADistributionLists S 			
			Where S.Id not in (Select Id from @NotDistributionList) And S.ListType=1 and S.Status = dbo.GetActiveStatus()
		

END 

Select Distinct G.Id,G.Title,G.Description 
From TAdistributionGroup G
	 Inner Join TADistributionListGroup LG  ON G.Id= LG.DistributionGroupId
	 Inner Join @TempDistributionListIds T ON LG.DistributionId =T.DistributionLIstId
select a.Id,a.Title,a.Description,DistributionGroupId,a.ListType
	from @TempDistributionListIds b 
		 Inner Join TADistributionLists a ON a.Id=b.DistributionListId
		 Inner Join TADistributionListGroup c ON c.DistributionId=b.DistributionListId
	
END





GO
PRINT 'Altering procedure Site_CreateMicroSiteData'
GO
ALTER procedure [dbo].[Site_CreateMicroSiteData]
(
	@ProductId uniqueidentifier,
	@MasterSiteId uniqueidentifier,
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser bit,
	@PageImportOptions int =2,
	@ImportContentStructure bit =0,
	@ImportFileStructure bit =0,
	@ImportImageStructure bit =0,
	@ImportFormsStructure bit =0
)
AS
BEGIN
	
	DECLARE @now datetime
	SET @now = GETUTCDATE()
	
	--CMSFrontUSer
	IF(@ImportAllWebSiteUser = 1)
	BEGIN			
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId,
			SU.UserId,
			SU.ISSystemUser,
			SU.ProductId
		FROM USUser U 
			INNER JOIN USSiteUser SU on U.Id = SU.UserId  
			INNER JOIN USMembership M on SU.UserId = M.UserId  
		WHERE SU.SiteId = @MasterSiteId AND SU.ProductId = @ProductId  
			AND SU.IsSystemUser = 0 AND  U.Status = 1 
			AND M.IsApproved = 1 AND M.IsLockedOut= 0 AND u.ExpiryDate >= @now
	END
	
-- This will not insert any record as we have not inserted any record in USSiteuser table
	INSERT INTO USMemberGroup(MemberId, MemberType, GroupId, ApplicationId) 
	SELECT SU.UserId, 1, MG.GroupId, @MicroSiteId  
		FROM USSiteUser SU 
			INNER JOIN USMemberGroup MG on SU.UserId = MG.MemberId 
	WHERE SU.SiteId = @MicroSiteId 
		AND MG.ApplicationId = @MasterSiteId --and MG.MemberType=1
	
	-- This is not required now, as it will inherit permision from parent site
	--****Admin USer****
	IF (@ImportAllAdminUserAndPermission=0)
	BEGIN
		-- Giving permission to Default users. Allow only CMS , Analyzer and Social product default users
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId] 
		FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MasterSiteId AND ProductId in ('CBB702AC-C8F1-4C35-B2DC-839C26E39848','CAB9FF78-9440-46C3-A960-27F53D743D89','CED1F2B6-A3FF-4B82-A856-1583FA811906')
		Except
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId]  FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MicroSiteId 	
			
		INSERT INTO [dbo].[USMemberGroup]
		   ([ApplicationId]
		   ,[MemberId]
		   ,[MemberType]
		   ,[GroupId])   
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MasterSiteId
				AND GroupId IN (SELECT Id FROM USGroup WHERE ApplicationId = ProductId)
		Except
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MicroSiteId				
	END

	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,@PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,7,@ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,9,@ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,33,@ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,38,@ImportFormsStructure
	
	
	--HSStructure data for Blog
	Declare @hsParentId uniqueidentifier,@SiteDirectoryObjectTypeId int
	Declare @RgtValue bigint , @SiteTitle nvarchar(1000)

	set @hsParentId  = dbo.GetNetEditorId();
	SELECT @RgtValue = RgtValue FROM HSStructure WHERE Id=@hsParentId
	SET @SiteDirectoryObjectTypeId= dbo.GetSiteDirectoryObjectType()

	IF NOT EXISTS(SELECT 1 FROM COTaxonomy WHERE ApplicationId=@MicroSiteId)
	BEGIN
		--Create the default node for the INDEX TERMS
		INSERT INTO COTaxonomy
		(
			Id,
			ApplicationId,
			Title,
			Description,
			CreatedDate,
			CreatedBy,
			ModifiedBy,
			ModifiedDate,
			Status,
			LftValue,
			RgtValue,
			ParentId
		)
		VALUES
		(
			NEWID(),
			@MicroSiteId,
			'Index Terms',
			'Index Terms',
			@now,
			(SELECT TOP 1 Id FROM USUser WHERE UserName='iappssystemuser'),
			NULL,
			NULL,
			1,
			1,
			2,
			NULL
		)
	END

	IF ((@RgtValue IS NOT NULL)
		AND (NOT EXISTS(SELECT 1 FROM HSStructure WHERE Title='Blog' And SiteId = @MicroSiteId )))
	BEGIN
		Set @RgtValue = 1
		
		Select @SiteTitle=Title from sisite where Id=@MicroSiteId

		if not exists(select * from Hsstructure where id=@MicroSiteId)
		begin
			INSERT INTO HSStructure(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@MicroSiteId,@SiteTitle,dbo.GetSiteObjectType(),null,@hsParentId,@RgtValue,@RgtValue+37,@MicroSiteId)
		end
		Declare @L1Id uniqueidentifier
		SELECT @L1Id =newid()
		INSERT INTO HSStructure
		(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L1Id,'Blog',@SiteDirectoryObjectTypeId,null,@MicroSiteId,@RgtValue+35,@RgtValue+36,@MicroSiteId)

		EXEC	[dbo].[SiteDirectory_Save]
		@Id = @L1Id OUTPUT,
		@ApplicationId = @MicroSiteId,
		@Title = N'Blog',
		@Description = NULL,
		@ModifiedBy = @ModifiedBy,
		@ModifiedDate = NULL,
		@ObjectTypeId = 39,
		@PhysicalPath = NULL,
		@VirtualPath = N'~/Blog'	
		-- Inserting to default Parent table
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@MicroSiteId,39,@L1Id)	
	End
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		
	
	EXEC [Site_CreateTemplateBuilderData] @MicroSiteId

	-- This might be not required now
	--EXEC [CMSSite_InsertDefaultPermission] @MicroSiteId	

	--CREATE MARKETIER RELATED DEFAULT DATA FOR VARIANT SITES
	EXEC Site_CreateMicrositeDataForMarketier @MicroSiteId
		

END
GO
--Attribute related procedures


PRINT 'Creating procedure Contact_ContactAttributeSearch'
GO
IF (OBJECT_ID('Contact_ContactAttributeSearch') IS NOT NULL)
	DROP PROCEDURE Contact_ContactAttributeSearch
GO

CREATE PROCEDURE [dbo].[Contact_ContactAttributeSearch]
    (
      @ContactAttributeSearchXml XML ,
      @ApplicationId UNIQUEIDENTIFIER
    )
AS 
    BEGIN

        DECLARE @ContactSearch TABLE
            (
              ConditionId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeValueID UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(1000) ,
              Value2 NVARCHAR(1000) ,
              Operator NVARCHAR(50) ,
              DataType NVARCHAR(50) ,
              ValueCount INT
            )
    
       
        CREATE TABLE #tbltest
            (
              ConditionId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(1000) ,
              Value2 NVARCHAR(1000) ,
              Operator NVARCHAR(50) ,
              DataType NVARCHAR(50) ,
              XmlValue XML
            )
        PRINT 'INSERT INTO #tblTest' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)
            
        INSERT  INTO #tbltest
                ( ConditionId ,
                  AttributeId ,
                  Value1 ,
                  Value2 ,
                  Operator ,
                  DataType ,
                  XmlValue
                    
                
                )
                SELECT  NEWID() AS ConditionId ,
                        col.value('@AttributeId', 'uniqueidentifier') AS AttributeId ,
                      --  sref.value('(text())[1]', 'uniqueidentifier') AS what ,
                        col.value('@Value1', 'nvarchar(max)') AS Value1 ,
                        col.value('@Value2', 'nvarchar(max)') AS Value2 ,
                        col.value('@Operator', 'nvarchar(50)') AS Operator ,
                        'System.String' AS DataType ,
                        col.query('AttributeValueId') AS XmlValue
                FROM    @ContactAttributeSearchXml.nodes('/ContactAttributeSearch/AttributeSearchItem') tab ( col )
                      --  CROSS APPLY col.nodes('AttributeValueId') AS AV ( sref )
          
          
          
          
        PRINT 'INSERT INTO @ContactSearch'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operator ,
                  DataType ,
                  ValueCount
                
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        pref.value('(text())[1]', 'uniqueidentifier') ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        COUNT(pref.value('(text())[1]', 'uniqueidentifier')) OVER ( PARTITION BY T.ConditionId,
                                                              T.AttributeId ) AS ValueCount
                FROM    #tbltest T
                        CROSS APPLY XmlValue.nodes('AttributeValueId') AS Value ( pref )
    
        PRINT 'INSERT INTO @ContactSearch 2'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operator ,
                  DataType ,
                  ValueCount
                
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        NULL ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        1
                FROM    #tblTest T
                        LEFT JOIN @ContactSearch S ON S.ConditionId = T.ConditionId
                WHERE   S.ConditionId IS NULL


    
        PRINT 'UPDATE @ContactSearch' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        UPDATE  CS
        SET     CS.DataType = ADT.TYPE
        FROM    @ContactSearch CS --INNER JOIN ( SELECT ConditionId ,
                --                    MAX(ValueCount) OVER ( PARTITION BY ConditionId ) AS MaxCount
                --             FROM   @ContactSearch
                --           ) CS2 ON CS2.ConditionId = CS.ConditionId
                INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
                INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 
              
              
              
        PRINT 'DECLARE @ModifiedDate DATETIME'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)
        DECLARE @ModifiedDate DATETIME
        SELECT TOP 1
                @ModifiedDate = ISNULL(CachedDate, '1-1-2000')
        FROM    [tblContactPropertiesAndAttributes]
        
        
        
        PRINT 'INSERT INTO NEWDATA' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)
        SELECT  *
        INTO    #NewData
        FROM    vwContactPropertiesAndAttributes CA
        WHERE   CA.ModifiedDate > @ModifiedDate
        OPTION  ( RECOMPILE ) ;
              
        IF EXISTS ( SELECT TOP 1
                            *
                    FROM    #NewData ) 
            BEGIN
                    
                DELETE  FROM dbo.tblContactPropertiesAndAttributes
                WHERE   ContactId IN ( SELECT   ContactId
                                       FROM     #NewData )
                INSERT  INTO dbo.tblContactPropertiesAndAttributes
                        ( ContactId ,
                          AttributeId ,
                          AttributeEnumId ,
                          Value ,
                          Notes ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          DataType ,
                          CachedDate
                            
                        )
                        SELECT  ContactId ,
                                AttributeId ,
                                AttributeEnumId ,
                                Value ,
                                Notes ,
                                CreatedDate ,
                                CreatedBy ,
                                ModifiedDate ,
                                ModifiedBy ,
                                DataType ,
                                @ModifiedDate
                        FROM    #NewData
                    
            END
                
              
        CREATE TABLE #LocalTemp
            (
              Id UNIQUEIDENTIFIER ,
              AttributeValueId UNIQUEIDENTIFIER ,
              ConditionId UNIQUEIDENTIFIER
            )
     
----------------------------------------------------------------------------------------------------------------------------------		
        INSERT  INTO #LocalTemp
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   CPA.AttributeEnumId IS NOT NULL
                        AND ( ( CS.Operator = 'opOperatorEqual'
                                AND CPA.AttributeEnumId = CS.AttributeValueID
                              )
                              OR ( CS.Operator = 'opOperatorNotEqual'
                                   AND CPA.AttributeEnumId != CS.AttributeValueID
                                 )
                            )
                UNION ALL
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   ( CPA.DataType = 'System.String'
                          AND CS.AttributeValueID IS  NULL
                          AND ( CS.Operator = 'opOperatorContains'
                                AND CPA.VALUE LIKE '%' + CS.Value1 + '%'
                                OR CS.Operator = 'opOperatorEqual'
                                AND CPA.VALUE = CS.Value1
                                OR CS.Operator = 'opOperatorBetween'
                                AND CPA.VALUE BETWEEN CS.Value1 AND CS.Value2
                                OR CS.Operator = 'opOperatorLessThanEqual'
                                AND CPA.VALUE <= CS.Value1
                                OR CS.Operator = 'opOperatorLessThan'
                                AND CPA.VALUE <= CS.Value1
                                OR CS.Operator = 'opOperatorGreaterThanEqual'
                                AND CPA.VALUE >= CS.Value1
                                OR CS.Operator = 'opOperatorGreaterThan'
                                AND CPA.VALUE > CS.Value1
                                OR CS.Operator = 'opOperatorNotEqual'
                                AND CPA.VALUE != CS.Value1
								OR CS.Operator = 'opOperatorDoesNotContain'
								AND CPA.VALUE NOT LIKE '%' + CS.Value1 + '%' 
                              )
                        )
                UNION ALL
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   ( CPA.DataType = 'System.DateTime'
                          AND CS.DataType = 'System.DateTime'
                          AND CS.AttributeValueID IS  NULL
                          AND ( CS.Operator = 'opOperatorEqual'
                                AND CAST(CPA.VALUE AS DATETIME) = CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorBetween'
                                AND CAST(CPA.VALUE AS DATETIME) BETWEEN CAST(CS.Value1 AS DATETIME)
                                                              AND
                                                              CAST(CS.VALUE2 AS DATETIME)
                                OR CS.Operator = 'opOperatorLessThanEqual'
                                AND CAST(CPA.VALUE AS DATETIME) <= CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorLessThan'
                                AND CAST(CPA.VALUE AS DATETIME) <= CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorGreaterThanEqual'
                                AND CAST(CPA.VALUE AS DATETIME) >= CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorGreaterThan'
                                AND CAST(CPA.VALUE AS DATETIME) > CAST(CS.Value1 AS DATETIME)
                                OR CS.Operator = 'opOperatorNotEqual'
                                AND CAST(CPA.VALUE AS DATETIME) != CAST(CS.Value1 AS DATETIME)
                              )
                        )
                UNION ALL
                SELECT  CPA.ContactId ,
                        CPA.AttributeEnumId ,
                        CS.ConditionId
                FROM    dbo.tblContactPropertiesAndAttributes CPA
                        INNER JOIN @ContactSearch CS ON CS.AttributeId = CPA.AttributeId
                WHERE   ( CS.DataType = 'System.Double'
                          OR CS.DataType = 'System.Int32'
                        )
                        AND CS.AttributeValueID IS  NULL
                        AND ( CS.Operator = 'opOperatorEqual'
                              AND CAST(CPA.Value AS MONEY) = CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorBetween'
                              AND CAST(CPA.Value AS MONEY) BETWEEN CAST(CS.Value1 AS MONEY)
                                                           AND
                                                              CAST(CS.VALUE2 AS MONEY)
                              OR CS.Operator = 'opOperatorLessThanEqual'
                              AND CAST(CPA.Value AS MONEY) <= CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorLessThan'
                              AND CAST(CPA.Value AS MONEY) <= CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorGreaterThanEqual'
                              AND CAST(CPA.Value AS MONEY) >= CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorGreaterThan'
                              AND CAST(CPA.Value AS MONEY) > CAST(CS.Value1 AS MONEY)
                              OR CS.Operator = 'opOperatorNotEqual'
                              AND CAST(CPA.Value AS MONEY) != CAST(CS.Value1 AS MONEY)
                            )

        PRINT 'CREATE TABLE #ContactIds' + CONVERT(NVARCHAR(23), GETUTCDATE(), 121) 
        CREATE TABLE #ContactIds
            (
              ContactId UNIQUEIDENTIFIER
            )
        INSERT  INTO #ContactIds
                ( ContactId
                
                )
                --SELECT DISTINCT Id FROM #LocalTemp
                SELECT DISTINCT
                        TV2.Id
                FROM    ( SELECT    * ,
                                    COUNT(AttributeValueId) OVER ( PARTITION BY ConditionID,
                                                              Id ) AS ValueCount
                          FROM      ( SELECT  DISTINCT
                                                *
                                      FROM      #LocalTemp
                                    ) TV
                        ) TV2
                        INNER JOIN @ContactSearch CS ON ( CS.ConditionId = TV2.ConditionId
                                                          AND CS.ValueCount = TV2.ValueCount
                                                        )
                                                        OR TV2.AttributeValueID IS NULL 
                                                        
        PRINT 'ECLARE @currentRecords INT'
            + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)                                  
       --   SELECT * FROM #ContactIds
        DECLARE @currentRecords INT
        SET @currentRecords = ( SELECT  COUNT(ID)
                                FROM    #tempContactSearchOutput
                              )

       
        IF @currentRecords > 0 
            BEGIN
                PRINT '@currentRecords > 0 '
                    + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)      
                DELETE  T
                FROM    #tempContactSearchOutput t
                        LEFT JOIN #ContactIds LT ON LT.ContactId = T.Id
                WHERE   LT.ContactId IS NULL
            END
        ELSE 
            BEGIN       
                PRINT ' INSERT  INTO #tempContactSearchOutput'
                    + CONVERT(NVARCHAR(23), GETUTCDATE(), 121)                
                INSERT  INTO #tempContactSearchOutput
                        SELECT  ContactId
                        FROM    #ContactIds
            END
        SELECT  @@ROWCOUNT
    END



GO
PRINT 'Creating function ContactReportDto_Get'
GO
IF (OBJECT_ID('ContactCountDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactCountDto_Get
GO

IF (OBJECT_ID('ContactReportDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactReportDto_Get
GO
CREATE PROCEDURE [dbo].[ContactReportDto_Get]
    (
      @SiteId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN

        DECLARE @tblTemp TABLE
            (
              Year INT ,
              Month INT ,
              NumberOfContacts INT
            )


        DECLARE @Monthy INT ,
            @Weekly INT ,
            @Daily INT
            
             
        SELECT  COUNT(C.UserId) AS ContactId ,
                DATEADD(dd, 0, DATEDIFF(dd, 0, C.CreatedDate)) AS CreatedDate
        INTO    #ContactCount
        FROM    dbo.vw_contacts C
                INNER JOIN dbo.vw_ContactSite CS ON CS.UserId = C.UserId
        WHERE   SiteId = @SiteId
        GROUP BY DATEADD(dd, 0, DATEDIFF(dd, 0, C.CreatedDate))
    
          
              

        INSERT  INTO @tblTemp
                SELECT  DATEPART(YEAR, CreatedDate) YEAR ,
                        DATEPART(MONTH, CreatedDate) MONTH ,
                        SUM(C.ContactId) AS TotalContacts
                FROM    #ContactCount C
                WHERE   CreatedDate > DATEADD(month, -7, GETUTCDATE())
                GROUP BY DATEPART(year, CreatedDate) ,
                        DATEPART(MONTH, CreatedDate)




       

        DECLARE @StartDate DATETIME
        DECLARE @EndDate DATETIME
            
        SET @StartDate = DATEADD(DAY, 1 - DATEPART(WEEKDAY, GETUTCDATE()),
                                 CAST(GETUTCDATE() AS DATE))
        SET @EndDate = DATEADD(DAY, 7 - DATEPART(WEEKDAY, GETUTCDATE()),
                               CAST(GETUTCDATE() AS DATE))

        SELECT  @Weekly = SUM(C.ContactId)
        FROM    #ContactCount C
        WHERE   C.CreatedDate BETWEEN @StartDate AND @EndDate


        SET @StartDate = DATEADD(mm, DATEDIFF(mm, 0, GETUTCDATE()), 0) 

        SET @EndDate = DATEADD(dd, -1,
                               DATEADD(mm, DATEDIFF(mm, 0, GETUTCDATE()) + 1,
                                       0))



        SELECT  @Monthy = SUM(C.ContactId)
        FROM    #ContactCount C
        WHERE   C.CreatedDate BETWEEN @StartDate AND @EndDate


        SET @StartDate = DATEADD(dd, 0, DATEDIFF(dd, 0, GETUTCDATE()))

        SELECT  @Daily = SUM(C.ContactId)
        FROM    #ContactCount C
        WHERE   C.CreatedDate = @StartDate


        SELECT  SUM(C.ContactId) TotalContacts ,
                ISNULL(@Monthy, 0) CurrentMonth ,
                ISNULL(@Weekly,0) CurrentWeek ,
                ISNULL(@Daily, 0) CurrentDay
        FROM    #ContactCount C

        SELECT  *
        FROM    @tblTemp

    END
    
GO


PRINT 'Creating function Contact_GetContactByIdsFromTemp'
GO
IF (OBJECT_ID('Contact_GetContactByIdsFromTemp') IS NOT NULL)
	DROP PROCEDURE Contact_GetContactByIdsFromTemp
GO
CREATE PROCEDURE [dbo].[Contact_GetContactByIdsFromTemp]
    (
      @ApplicationId UNIQUEIDENTIFIER ,
      @Status INT ,
      @SortOrder VARCHAR(25) = NULL ,
      @MaxRecords INT,
	  @IncludeAddress bit = 1
    )
AS 
    BEGIN 
 -- Code goes here
--Select @ContactIds


        IF ( @MaxRecords = 0 ) 
            BEGIN
	
                SELECT  [UserId] Id ,
                        [UserId] ,
                        ISNULL([FirstName], '') FirstName ,
                        ISNULL([MiddleName], '') MiddleName ,
                        ISNULL([LastName], '') LastName ,
                        [CompanyName] ,
                        dbo.ConvertTimeFromUtc(BirthDate, @ApplicationId) BirthDate ,
                        [Gender] ,
                        [AddressId] ,
                        [Status] ,
                        [HomePhone] ,
                        [MobilePhone] ,
                        [OtherPhone] ,
                        ImageId ,
                        [Notes] ,
                        [Email] ,
                        [ContactType] ,
                        [ContactSourceId] ,
                        COUNT(tmp.Id) OVER ( ) AS TotalRecords
                FROM                         dbo.vw_contacts C
                                             INNER JOIN #tempContactSearchOutput tmp ON tmp.Id = C.UserId
                WHERE                        ( @Status = 0
                                               OR C.Status = @Status
                                               OR ( @Status = 99
                                                    AND ( C.Status = 1
                                                          OR C.Status = 2
                                                        )
                                                  )
                                             )
                ORDER BY                     LastName ASC
				IF @IncludeAddress = 1
				BEGIN
              	  SELECT  A.*
                FROM    vw_Address A
                        JOIN MKContact C ON C.AddressId = A.Id
                        JOIN   #tempContactSearchOutput T ON T.Id = C.Id
				END
		
            END
        ELSE 
            BEGIN

                SELECT TOP ( @MaxRecords )
                        [UserId] Id ,
                        [UserId] ,
                        ISNULL([FirstName], '') FirstName ,
                        ISNULL([MiddleName], '') MiddleName ,
                        ISNULL([LastName], '') LastName ,
                        [CompanyName] ,
                        dbo.ConvertTimeFromUtc(BirthDate, @ApplicationId) BirthDate ,
                        [Gender] ,
                        [AddressId] ,
                        [Status] ,
                        [HomePhone] ,
                        [MobilePhone] ,
                        [OtherPhone] ,
                        ImageId ,
                        [Notes] ,
                        [Email] ,
                        [ContactType] ,
                        [ContactSourceId] ,
                        COUNT(tmp.Id) OVER ( ) AS TotalRecords
                FROM                         dbo.vw_contacts C
                                             INNER JOIN #tempContactSearchOutput tmp ON tmp.Id = C.UserId
                WHERE                        ( @Status = 0
                                               OR C.Status = @Status
                                               OR ( @Status = 99
                                                    AND ( C.Status = 1
                                                          OR C.Status = 2
                                                        )
                                                  )
                                             )
                ORDER BY                     LastName ASC
				IF @IncludeAddress = 1
				BEGIN
				  SELECT  A.*
                FROM    vw_Address A
                        JOIN MKContact C ON C.AddressId = A.Id
                        JOIN (SELECT TOP ( @MaxRecords ) ID FROM #tempContactSearchOutput) T ON T.Id = C.Id
				END
               
            END

			

    END
GO
PRINT 'Dropping column TATask.TaskStatus'
GO

IF COL_LENGTH(N'[dbo].[TATask]', N'TaskStatus') IS  NOT NULL
ALTER TABLE TATask  DROP COLUMN    [TaskStatus]	

GO
PRINT 'Creating function Task_GetTaskStatus'
GO
IF (OBJECT_ID('Task_GetTaskStatus') IS NOT NULL)
	DROP function Task_GetTaskStatus
GO
CREATE FUNCTION [dbo].[Task_GetTaskStatus]
(
	@TaskId		uniqueIdentifier
)
RETURNS INT
AS
BEGIN
	DECLARE @TaskStatus		int,
			@TaskResult		int,
			@RunCount		int,
			@MaxOccurence	int,
			@NextRunTime	datetime,
			@EndTime		datetime

	SET @TaskResult = (SELECT TOP 1 Result FROM TATaskHistory Where TaskId = @TaskId ORDER BY StartTime DESC)
	SET @TaskStatus = 1 --Active
	IF @TaskResult IS NOT NULL 
	BEGIN
		IF @TaskResult = 3 --Running
			SET @TaskStatus = 3 --Running
		ELSE
		BEGIN
			SELECT
				@RunCount = RunCount,
				@MaxOccurence = MaxOccurrence,
				@NextRunTime = NextRunTime,
				@EndTime = EndTime
			FROM TATask T JOIN TASchedule S ON T.ScheduleId = S.Id WHERE T.Id = @TaskId

			IF @RunCount >= @MaxOccurence OR @NextRunTime > @EndTime
				SET @TaskStatus = 2 --Completed
		END
	END

	RETURN @TaskStatus
END
GO
PRINT 'Creating column TATask.TaskStatus'
GO
IF COL_LENGTH(N'[dbo].[TATask]', N'TaskStatus') IS  NULL
ALTER TABLE TATask  ADD    [TaskStatus]	AS ([dbo].[Task_GetTaskStatus]([Id])) 

GO
PRINT 'Creating column Task_GetTask'
GO
ALTER PROCEDURE [dbo].[Task_GetTask]
(
	@Id uniqueidentifier
)
AS
BEGIN
	Select 
		Id, Title, Description, Path, Parameter, TaskType,
		CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
		Status, ScheduleId, ObjectId, TaskStatus,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName
	from 
		TATask  A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
	where 
		Id = @Id 
		and Status=dbo.GetActiveStatus()

END

GO

PRINT 'Creating function AttributeValue_GetByObjectId'
GO
IF (OBJECT_ID('AttributeValue_GetByObjectId') IS NOT NULL)
	DROP function AttributeValue_GetByObjectId
GO
CREATE Function [dbo].[AttributeValue_GetByObjectId]
(	
	@ObjectId	uniqueidentifier,
	@CategoryId	int
)
RETURNS @Results TABLE 
(
	Id				uniqueidentifier,
	ObjectId		uniqueidentifier,
	CategoryId		int,
	AttributeId		uniqueidentifier,
	AttributeEnumId uniqueidentifier,
	Value			nvarchar(max),
	Notes			nvarchar(max),
	CreatedDate		datetime,
	CreatedBy		uniqueidentifier
)
AS
BEGIN
	IF @CategoryId = 1
	BEGIN
		INSERT INTO @Results 
		SELECT
			Id,
			SiteId, 
			1 AS CategoryId,
			AttributeId, 
			AttributeEnumId, 
			Value, 
			Notes,
			CreatedDate, 
			CreatedBy
		FROM  dbo.ATSiteAttributeValue
		WHERE SiteId = @ObjectId
	END
	ELSE IF @CategoryId = 2
	BEGIN
		INSERT INTO @Results 
		SELECT
			Id,
			PageDefinitionId, 
			2 AS CategoryId,
			AttributeId, 
			AttributeEnumId, 
			Value, 
			Notes,
			CreatedDate, 
			CreatedBy
		FROM  dbo.ATPageAttributeValue
		WHERE PageDefinitionId = @ObjectId
	END
	ELSE IF @CategoryId = 3
	BEGIN
		INSERT INTO @Results 
		SELECT
			Id,
			ContactId, 
			3 AS CategoryId,
			AttributeId, 
			AttributeEnumId, 
			Value, 
			Notes,
			CreatedDate, 
			CreatedBy
		FROM  dbo.ATContactAttributeValue
		WHERE ContactId = @ObjectId
	END
	RETURN
END
GO
PRINT 'Altering procedure Task_GetManager'
GO
ALTER PROCEDURE [dbo].[Task_GetManager]
(
	@id uniqueidentifier=null,
	@PageIndex int, 
	@PageSize int,
	@TotalRecords int
)
AS
begin
	if @id is null
    begin
		IF @PageSize is Null
			BEGIN
				select 
					Id,Title,Description,Path,Parameter,TaskType,
					CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
					Status,ScheduleId,ObjectId,TaskStatus,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName
				 from TATask A
				 LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
				 LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
				 where Status=dbo.GetActiveStatus()
			END
		ELSE
			BEGIN
				WITH ResultEntries AS ( 
					SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
						Id,Title,Description,Path,Parameter,TaskType,
						CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
						Status,ScheduleId,ObjectId,TaskStatus
					from TATask where Status=dbo.GetActiveStatus()
				)
				SELECT 
					Id,Title,Description,Path,Parameter,TaskType,
					CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status, ScheduleId,ObjectId,TaskStatus,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName
				FROM ResultEntries A
				LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
				WHERE Row between 
					(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize
		END
    end
	else
    begin
		select 
			Id,Title,Description,Path,Parameter,TaskType,
			CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status, ScheduleId,ObjectId,TaskStatus,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName	
		from 
			TATask A
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		where 
			Id=@id 
			and Status=dbo.GetActiveStatus()
    end
end

GO
PRINT 'Altering procedure Schedule_UpdateLastRun'
GO
ALTER PROCEDURE [dbo].[Schedule_UpdateLastRun] 
(
			@Id  uniqueidentifier
		   ,@LastRunTime DateTime=NULL
		   ,@ModifiedBy uniqueidentifier
		   ,@ModifiedDate datetime
		   ,@ApplicationId uniqueidentifier
		   ,@IncrementRunCount bit = NULL
)
AS
BEGIN
	
	SET @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId) 
	Declare @Type int,
			@NextRunTime dateTime,
			@OldLastRunTime dateTime,
			@StartDate Datetime,
			@StartTime DateTime,
			@EndDate DateTime,
			@EndTime DateTime,
			@MaxOccurence int,
			@Runcount int,	
			@ScheduleTypeId uniqueidentifier,
			@OldNextRunTime dateTime

	SET @NextRunTime =NULL

	SELECT @StartDate =[StartDate]
		  ,@EndDate=[EndDate]
		  ,@StartTime =[StartTime]
		  ,@EndTime =[EndTime]
		  ,@MaxOccurence=[MaxOccurrence]
		  ,@Type=[Type]
		  ,@ScheduleTypeId=[ScheduleTypeId]
		  ,@NextRunTime=[NextRunTime]
		  ,@OldLastRunTime=[LastRunTime]
		  ,@Runcount=[RunCount]
	  FROM [dbo].[TASchedule]
	  Where Id=@Id AND Status=dbo.GetActiveStatus()
			--AND ApplicationId=@ApplicationId
	
	--if(@LastRunTime is null and @NextRunTime is not null) set @LastRunTime=GETUTCDATE()

	IF @LastRunTime is not null
	Set @OldLastRunTime =@LastRunTime
	--If @LastRunTime is null
	--	Select @LastRunTime = LastRunTime From TASchedule Where Id = @Id


	
	IF @Type=1
	BEGIN
	-- Do Daily calculations
	
	
	--	Select @LastRunTime = LastRunTime From TASchedule Where Id = @Id
		Declare @OccursEvery int, @WeekDaysOnly bit
		
		Select @OccursEvery=OccursEvery,@WeekDaysOnly=isnull(WeekDaysOnly,0) From TAScheduleDaily
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()

		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() 
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate 
			SET @OccursEvery =0
		END	
			IF @WeekDaysOnly=1
			BEGIN 
				IF datepart(weekday,@LastRunTime + @OccursEvery) =7
				BEGIN
					SET @NextRunTime = @LastRunTime + @OccursEvery + 2
				END
				ELSE IF datepart(weekday,@LastRunTime + @OccursEvery) =1
				BEGIN
					SET @NextRunTime = @LastRunTime + @OccursEvery + 1
				END
				ELSE
				BEGIN
					SET @NextRunTime = @LastRunTime + @OccursEvery
				END
			END
			ELSE
			BEGIN
				set @NextRunTime = @LastRunTime + @OccursEvery
			END

	END
	ELSE IF @Type=2
	BEGIN
	-- Do weekly Calculations
		
		Declare @Sunday bit,@Monday bit,@Tuesday bit,@Wednesday  bit,@Thursday  bit,@Friday bit,@Saturday bit
		Declare @Weekday int,@CurrentWeekday int,@Increment int,@TempInc int
		
		Select @OccursEvery=isnull(@OccursEvery,1),@Sunday=Sunday,@Monday=Monday,@Tuesday=Tuesday,@Wednesday=Wednesday,@Thursday=Thursday,@Friday=Friday,@Saturday=Saturday 
		From TAScheduleWeekly
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()
		
		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() -1
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate -1
			SET @OccursEvery =1
		END	
		
		SET @CurrentWeekday = datepart(weekday,@LastRunTime)
		SET @Weekday = datepart(weekday,@LastRunTime + 1)
		SET @Increment= (7 * @OccursEvery) 
		IF(@Sunday=1)
		BEGIN
			SET @Increment = (7 * @OccursEvery) + 1 - @CurrentWeekday
		END
		IF(@Monday=1)
		BEGIN
			IF @CurrentWeekday <2
				SET @TempInc =2 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery)+ 2 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Tuesday=1)
		BEGIN
			IF @CurrentWeekday <3
				SET @TempInc =3 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 3 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Wednesday=1)
		BEGIN
			IF @CurrentWeekday <4
				SET @TempInc =4 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 4 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc

		END
		IF(@Thursday=1)
		BEGIN
			IF @CurrentWeekday <5
				SET @TempInc =5 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 5 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Friday=1)
		BEGIN
			IF @CurrentWeekday <6
				SET @TempInc =6 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery)+ 6 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Saturday=1)
		BEGIN
			IF @CurrentWeekday <7
				SET @TempInc =7 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 7 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		--IF @Increment < (7 * @OccursEvery) + 1
			SET @NextRunTime = @LastRunTime + @Increment

	END
	ELSE IF @Type=3
	BEGIN
	-- Do Monthly Calculations
		
		Declare @Day int,@NthWeek int,@DayofWeek int,@MonthType int
		Select @Day=Day,@OccursEvery =isnull(OccursEvery,1),@NthWeek=NthWeek,@DayofWeek=DayofWeek,@MonthType=Type 
		From TAScheduleMonthly
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()
		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() -1
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate -1
			SET @OccursEvery =1
		END	
		
		IF @MonthType=1
		BEGIN
			IF datepart(day,@LastRunTime)< @Day
				SET @NextRunTime = @LastRunTime + @Day - datepart(day,@LastRunTime)
			ELSE
			BEGIN		
				IF datepart(month,@LastRunTime) + @OccursEvery <=12
					SET @NextRunTime = cast(datepart(month,@LastRunTime) + @OccursEvery as varchar(2)) + '/' + cast(@Day as varchar(2)) + '/' +  cast(datepart(year,@LastRunTime) as varchar(4))
				ELSE
				BEGIN
					Declare @YearPart int,@MonthPart int
					SET @MonthPart = (datepart(month,@LastRunTime) + @OccursEvery) % 12
					SET @Yearpart =  datepart(year,@LastRunTime)  +(datepart(month,@LastRunTime)+ @OccursEvery) / 12
					SET @NextRunTime = cast(@MonthPart as varchar(2)) + '/' + @Day + '/' + @YearPart
				END
			END
		END
		ELSE
		BEGIN
			Declare @LastDayOfMonth DateTime, @NumberOfWeek int
			Declare @weekFirstDay int
			Declare @interMediateDate datetime
			Declare @FirstWeekDay DateTime
			Declare @LastWeekDay DateTime
			Declare @weekdayoffset int
			
			--@NthWeek is the firt dropdown in second row. -- first-1, Second-2, Third-3,Fourth-4, Last-5
			--@DayOfWeek second dropdown, Sunday-1 Saturday -7 Day-8, Weekday -9 weekends-10
			SET @LastDayOfMonth =DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@LastRunTime)+@OccursEvery + 1,0))
			--Set @FirstWeekDay = cast(cast(datepart(month,@LastRunTime) + @OccursEvery + 1 as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
			Set @FirstWeekDay =cast(cast( DATEPART(MONTH,@LASTDayOfMonth) as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
			Set @WeekFirstDay =datepart(weekday,@FirstWeekDay)
			if(@WeekFirstDay<= @DayofWeek)
				SET @weekdayoffset = @DayofWeek - @WeekFirstDay
			ELSE
				SET @weekdayoffset = @DayofWeek + 7 - @WeekFirstDay

			if(@DayOfWeek between 1 and 7)
			BEGIN
				Declare @currentWeek int
				Set @currentWeek = datepart(week,@LastRunTime)
				IF @NthWeek =5	
				BEGIN
					SET @NthWeek = datepart(week,@LastDayOfMonth) - datePart(week,@FirstWeekDay)
				END
				
				IF(datepart(month,@LastRunTime) = datepart(month,@FirstWeekDay))
				IF((datepart(week,@LastRunTime) - datepart(week,@FirstWeekDay)) < @NthWeek)
					Set @OccursEvery =0
				ELSE IF((datepart(week,@LastRunTime) - datepart(week,@FirstWeekDay)) = @NthWeek AND datepart(weekday,@LastRunTime)< @weekdayoffset)	
					Set @OccursEvery =0
					
					Set @NextRunTime = cast(cast(datepart(month,@LastRunTime) + @OccursEvery as varchar(2)) + '/' + cast((@NthWeek * 7) -6 + @weekdayoffset as varchar(2)) +'/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
					
			END
			ELSE IF @DayOfWeek = 8 -- Nth Day
			BEGIN 
				Set @NextRunTime = cast(cast(datepart(month,@LastRunTime) + @OccursEvery + 1 as varchar(2)) + '/' + cast(@NthWeek as varchar(2)) +'/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
			END
			ELSE IF @DayOfWeek = 9 -- Nth Week day
			BEGIN 
				IF (@NthWeek!=5)
				BEGIN
					
					
					Set @weekFirstDay = datepart(week,@FirstWeekDay)
					if(@weekFirstDay =1)
						SET @FirstWeekDay = @FirstWeekDay + 1
					if(@weekFirstDay =7)
						SET @FirstWeekDay = @FirstWeekDay + 2
					SET @NextRunTime = @FirstWeekDay + @NthWeek
					Set @weekFirstDay = datepart(week,@NextRunTime)
					if(@weekFirstDay =1)
						SET @NextRunTime = @NextRunTime + 1
					if(@weekFirstDay =7)
						SET @NextRunTime = @NextRunTime + 2
				END
				ELSE
				BEGIN
					SET @LastWeekDay = datepart(week,@LastDayOfMonth)
					IF(@LastWeekDay =1) 
						SET @NextRunTime = @LastDayOfMonth -2
					ELSE IF (@LastWeekDay=7)
						SET @NextRunTime = @LastDayOfMonth -1
					ELSE
						SET @NextRunTime = @LastDayOfMonth
				END
			END
			ELSE IF @DayOfWeek = 10 -- Nth week end day
			BEGIN 
				IF (@NthWeek!=5)
				BEGIN
				Set @FirstWeekDay = cast(cast(datepart(month,@LastRunTime) + @OccursEvery  as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
				print @FirstWeekDay 
					Set @weekFirstDay = datepart(weekday,@FirstWeekDay)
					print @weekFirstDay
					IF(@weekFirstDay !=1)
						Set @FirstWeekDay = @FirstWeekDay + (7 - @WeekFirstDay) -- going to next sarturday
					IF(@NthWeek=1)
						SET @NextRunTime = @FirstWeekDay
					IF(@NthWeek=2)
						IF @WeekFirstDay =1
							SET @NextRunTime = @FirstWeekDay + 6
						ELSE 
							SET @NextRunTime = @FirstWeekDay + 1
					IF(@NthWeek=3)
							SET @NextRunTime = @FirstWeekDay + 7
					IF(@NthWeek=4)
						IF @WeekFirstDay =1
							SET @NextRunTime = @FirstWeekDay + 13
						ELSE 
							SET @NextRunTime = @FirstWeekDay + 8
						
				END
				ELSE
				BEGIN
					SET @LastWeekDay = datepart(week,@LastDayOfMonth)
					IF(@LastWeekDay =1 OR @LastWeekDay=6)
						SET @NextRunTime = @LastDayOfMonth
					ELSE
						SET @NextRunTime = @LastDayOfMonth - (@LastWeekDay - 1)
				END
			END
		END

	END
	ELSE IF @Type=4
	BEGIN
	-- Do Yearly Calculations
	Declare @Month int,@NthMonth int,@YearType int
		Select @Month=Month,@Day=Day,@NthWeek=NthWeek,@DayofWeek =DayofWeek, @NthMonth=NthMonth,@YearType=Type 
		From TAScheduleYearly
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()
		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() -1
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate -1
			SET @OccursEvery =1
		END	
		IF @YearType =1 
		BEGIN
			IF DatePart(Month,@LastRunTime)<@Month OR (DatePart(Month,@LastRunTime)=@Month AND
				DatePart(Day,@LastRunTime)<@Day)
				  SET @NextRunTime = cast(@Month as varchar(2)) + '/' + cast(@Day as varchar(2)) + '/' +  CAST(DatePart(Year,@LastRunTime) as varchar(4))
			ELSE
			BEGIN
				  SET @NextRunTime = cast(@Month as varchar(2)) + '/' + cast(@Day as varchar(2)) + '/' +  CAST(DatePart(Year,@LastRunTime) +1 as varchar(4))
			END
			
		END
		ELSE
		BEGIN
			Declare @FirstDayOfMonth DateTime
			
			If(DatePart(month,@LastRunTime)< @NthMonth)
				SET @FirstDayOfMonth =Cast(@NthMonth as varchar(2)) +  '/1/'  +cast( datepart(year,@LastRunTime) as varchar(4))
			else
				SET @FirstDayOfMonth =Cast(@NthMonth as varchar(2)) +  '/1/'  +cast( datepart(year,@LastRunTime) + 1 as varchar(4))
			SET @LastDayOfMonth =DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FirstDayOfMonth) + 1,0))
			Set @WeekFirstDay =datepart(weekday,@FirstDayOfMonth) -- first week day ie sunday=1 ,, sat=7 for first day of month
			if(@FirstWeekDay <= @DayofWeek)
				SET @weekdayoffset = @DayofWeek - @WeekFirstDay
			ELSE
				SET @weekdayoffset = @DayofWeek + 7 - @WeekFirstDay

			if(@DayOfWeek between 1 and 7)
				BEGIN
					
					IF @NthWeek =5	
					BEGIN
						SET @NthWeek = datepart(weekday,@LastDayOfMonth)
					END
						Set @NextRunTime = cast(cast(datepart(month,@FirstDayOfMonth) as varchar(2)) + '/' + cast((@NthWeek * 7) -6 + @weekdayoffset as varchar(2)) +'/' + cast(datepart(year,@FirstDayOfMonth) as varchar(4)) as datetime)
				END
				ELSE IF @DayOfWeek = 8 -- Nth Day
				BEGIN 
					Set @NextRunTime = cast(cast(datepart(month,@FirstDayOfMonth)  as varchar(2)) + '/' + cast(@NthWeek as varchar(2)) +'/' + cast(datepart(year,@FirstDayOfMonth) as varchar(4)) as datetime)
				END
				ELSE IF @DayOfWeek = 9 -- Nth Week day
				BEGIN 
					IF (@NthWeek!=5)
					BEGIN
						
						Set @FirstWeekDay = cast(cast(datepart(month,@FirstDayOfMonth) as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
						Set @weekFirstDay = datepart(week,@FirstWeekDay)
						if(@weekFirstDay =1)
							SET @FirstWeekDay = @FirstWeekDay + 1
						if(@weekFirstDay =7)
							SET @FirstWeekDay = @FirstWeekDay + 2
						SET @NextRunTime = @FirstWeekDay + @NthWeek
						Set @weekFirstDay = datepart(week,@NextRunTime)
						if(@weekFirstDay =1)
							SET @NextRunTime = @NextRunTime + 1
						if(@weekFirstDay =7)
							SET @NextRunTime = @NextRunTime + 2
					END
					ELSE
					BEGIN
						SET @LastWeekDay = datepart(week,@LastDayOfMonth)
						IF(@LastWeekDay =1) 
							SET @NextRunTime = @LastDayOfMonth -2
						ELSE IF (@LastWeekDay=7)
							SET @NextRunTime = @LastDayOfMonth -1
						ELSE
							SET @NextRunTime = @LastDayOfMonth
					END
				END
				ELSE IF @DayOfWeek = 10 -- Nth week end day
				BEGIN 
					IF (@NthWeek!=5)
					BEGIN
						Set @FirstWeekDay = cast(cast(datepart(month,@FirstDayOfMonth) as varchar(2)) + '/1/' + cast(datepart(year,@FirstDayOfMonth) as varchar(4)) as datetime)
						Set @weekFirstDay = datepart(week,@FirstWeekDay)
						IF(@weekFirstDay !=1)
							Set @FirstWeekDay = @FirstWeekDay + (7 - @WeekFirstDay) -- going to next sarturday
						IF(@NthWeek=1)
							SET @NextRunTime = @FirstWeekDay
						IF(@NthWeek=2)
							IF @WeekFirstDay =1
								SET @NextRunTime = @FirstWeekDay + 6
							ELSE 
								SET @NextRunTime = @FirstWeekDay + 1
						IF(@NthWeek=3)
								SET @NextRunTime = @FirstWeekDay + 7
						IF(@NthWeek=4)
							IF @WeekFirstDay =1
								SET @NextRunTime = @FirstWeekDay + 13
							ELSE 
								SET @NextRunTime = @FirstWeekDay + 8
							
					END
					ELSE
					BEGIN
						SET @LastWeekDay = datepart(week,@LastDayOfMonth)
						IF(@LastWeekDay =1 OR @LastWeekDay=6)
							SET @NextRunTime = @LastDayOfMonth
						ELSE
							SET @NextRunTime = @LastDayOfMonth - (@LastWeekDay - 1)
					END
				END

		END

	END

	Set @NextRunTime = CAST(FLOOR(CAST(@NextRunTime AS FLOAT ))AS DateTime)
	IF @NextRunTime > @EndDate AND @EndDate is not null
		SET @NextRunTime =NULL	
	ELSE
		Set @NextRunTime = @NextRunTime  + CONVERT(VARCHAR(8),@StartTime,108)
	
	IF @IncrementRunCount = 1
		SET @Runcount = @Runcount + 1

	Update [dbo].[TASchedule]
		SET LastRunTime=@OldLastRunTime,
			NextRunTime=@NextRunTime,
			ModifiedBy =@ModifiedBy,
			ModifiedDate =@ModifiedDate,
			RunCount = @Runcount
			Where Id=@Id


END

GO

PRINT 'Alternig procedure DistributionList_AttachContacts'

GO
ALTER PROCEDURE [dbo].[DistributionList_AttachContacts](
	@xmlSourceDistributionIds text,@DestDistributionListId UniqueIdentifier,@ApplicationId UniqueIdentifier
	)
As
Begin

Declare	@hDoc int
Declare @listType bit,
	    @ListId  uniqueidentifier,
       @DynamicSQL  AS NVARCHAR(MAX) ,
		@TotalRecords int

Declare @TempMTable Table( UserId uniqueidentifier)

declare @TempATable Table (
		Id uniqueidentifier, 
		UserId uniqueidentifier,         
		FirstName nvarchar(50) null,       
		MiddleName nvarchar(50) null ,    
		 LastName   nvarchar(50) null ,     
		CompanyName  nvarchar(50) null  ,  
		BirthDate   datetime null , 
		Gender  nvarchar(50) null, 
		AddressId uniqueidentifier null,     
		Status    int null,  
		HomePhone  nvarchar(50) null ,     
		MobilePhone   nvarchar(50) null,   
		OtherPhone  nvarchar(50) null, 
		ImageId     uniqueidentifier null,
		Notes  nvarchar(50) null,  
		Email  nvarchar(50) null,  
		ContactType int null,
		ContactSourceId int null
)

Exec sp_xml_preparedocument @hDoc output, @xmlSourceDistributionIds



-- Delete contacts which are removed from contacts selected list
DELETE FROM TADistributionListUser WHERE DistributionListId=@DestDistributionListId and UserId in (Select distinct UserId from
		openxml (@hDoc, '/DistributionList/DeletedContactsId/guid',2)      
	with 
		(
			DistributionListId uniqueidentifier '../../../guid',
			UserId varchar(255) '.'
		) 
)

insert into @TempMTable Select distinct UserId from
		openxml 
		(
			@hDoc, '/DistributionList/ContactId/guid',2
		)      
	with 
		(
			DistributionListId uniqueidentifier '../../../guid',
			UserId varchar(255) '.'
		) 


			declare  curIds cursor for select	distributionId from openxml (@hDoc, '/DistributionList/DistributionId/guid',2	)      
				with 
				(
					distributionId varchar(255) '.'
				) where distributionId not in (@DestDistributionListId)

			open curIds
			Fetch next from curIds into @ListId
			WHILE @@FETCH_STATUS = 0
				BEGIN
				

					Select @listType = isnull(ListType,0) from TADistributionLists
					Where Id= @ListId



					if @listType = 0  --- manual
					begin
						insert into @TempMTable Select UserId from TADistributionListUser where DistributionListId = @ListId
					end
					else
					begin
				    
						Declare @queryXml xml
						
						SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@ListId 

						--print cast(@ApplicationId as nvarchar(max))
						insert into @TempATable Exec Contact_SearchCLR @Xml=@queryXml,@ApplicationId =@ApplicationId,@MaxRecords=0,@TotalRecords =@TotalRecords out


					end

				Fetch next from curIds into @ListId
				END
			CLOSE curIds
			Deallocate curIds



Select Userid into #OriginalList FROM TADistributionListUser WHERE DistributionListId=@DestDistributionListId

DELETE FROM TADistributionListUser WHERE DistributionListId=@DestDistributionListId

insert into TADistributionListUser select  newid(),@DestDistributionListId,UserId,1 from (
 select Distinct userid from (
	select UserId  from @TempMTable
	union all
    select UserId  from @TempATable
	union all 
	select UserId from #OriginalList 	
) A
) x

End
GO
PRINT 'Altering procedure Job_Save'
GO


ALTER PROCEDURE [dbo].[Job_Save]
(
	@xmlData xml, 
	@ApplicationId uniqueidentifier,
	@ModifiedBy		uniqueidentifier,
	@Id uniqueidentifier OUTPUT
)
AS
BEGIN
	DECLARE @JobId	uniqueidentifier,
			@error	int,
			@Now	datetime,
			@stmt	varchar(256),
			@Status	int

	SET @Status= dbo.GetActiveStatus()
	
	SET @Now = getdate()
	
	SELECT @JobId= T.c.value('Id[text()][1]','uniqueidentifier')
		FROM   @xmlData.nodes('/Job') T(c)

	IF NOT EXISTS(SELECT * FROM TAJob WHERE Id= @JobId)
	BEGIN
		SELECT @JobId = newid()
		SET @stmt = 'Job INSERT'
		INSERT INTO TAJob
		(
			Id,
			Title,
			Description,
			CreatedDate,
			CreatedBy,
			Status,
			ApplicationId
		)
		SELECT	
			@JobId,
			T.c.value('Title[text()][1]','nvarchar(256)'),
			T.c.value('Description[text()][1]','nvarchar(1024)'),
			@Now,
			@ModifiedBy,
			@Status,
			@ApplicationId
		FROM   @xmlData.nodes('/Job') T(c)
	
		SELECT 	@error = @@error
		IF @error <> 0
		BEGIN
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		END
	END
	ELSE 
	BEGIN
		SET @stmt = 'Job UPDATE'
		UPDATE TAJob SET 
			Title=T.c.value('Title[text()][1]','nvarchar(256)'),
			Description=T.c.value('Description[text()][1]','nvarchar(1024)'),
			ModifiedDate=@Now,
			ModifiedBy=T.c.value('ModifiedBy[text()][1]','uniqueidentifier'),
			ApplicationId=@ApplicationId
		FROM   @xmlData.nodes('/Job') T(c)
		WHERE Id = @JobId

		SELECT @error = @@error
		IF @error <> 0
		BEGIN
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		END
			
		SET @stmt = 'JobTask DELETE'
				
		DELETE FROM TAJobTask WHERE JobId=@JobId
				
		SELECT @error = @@error
		IF @error <> 0
		BEGIN
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		END
	END

	SET @stmt = 'Task UPDATE'
	UPDATE TATask SET
		Id = T.c.value('Id[text()][1]','uniqueidentifier'),
		Title = T.c.value('Title[text()][1]','nvarchar(256)'),
		Description = T.c.value('Description[text()][1]','nvarchar(1024)'),
		Path = T.c.value('Path[text()][1]','nvarchar(4000)'),
		Parameter = T.c.value('Parameter[text()][1]','nvarchar(max)'),
		TaskType = T.c.value('TaskType[text()][1]','int'),
		ScheduleId = T.c.value('ScheduleId[text()][1]','uniqueidentifier'),
		ObjectId = T.c.value('ObjectId[text()][1]','uniqueidentifier'),
		ModifiedDate = @Now,
		ModifiedBy = @ModifiedBy,
		Status = @Status
	FROM @xmlData.nodes('/Job/Tasks/Task') T(c)
	WHERE T.c.value('Id[text()][1]','uniqueidentifier')=TATask.Id

	SELECT @error = @@error
	IF @error <> 0
	BEGIN
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	END

	SET @stmt = 'Task INSERT'
	INSERT INTO TATask
	(
		Id,
		Title,
		Description,
		Path,
		Parameter,
		TaskType,
		CreatedDate,
		CreatedBy,
		ScheduleId,
		ObjectId,
		Status
	)
	SELECT 
		T.c.value('Id[text()][1]','uniqueidentifier'),
		T.c.value('Title[text()][1]','nvarchar(256)'),
		T.c.value('Description[text()][1]','nvarchar(1024)'),
		T.c.value('Path[text()][1]','nvarchar(4000)'),
		T.c.value('Parameter[text()][1]','nvarchar(max)'),
		T.c.value('TaskType[text()][1]','int'),
		@Now,
		@ModifiedBy,
		T.c.value('ScheduleId[text()][1]','uniqueidentifier'),
		T.c.value('ObjectId[text()][1]','uniqueidentifier'),
		@Status
	FROM   @xmlData.nodes('/Job/Tasks/Task') T(c)
	WHERE T.c.value('Id[text()][1]','uniqueidentifier') NOT IN (SELECT Id FROM TATask)
		
	SELECT @error = @@error
	IF @error <> 0
	BEGIN
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	END

	SET @stmt = 'JobTask INSERT'
	INSERT INTO TAJobTask
	(
		JobId,
		TaskId,
		Sequence
	)
	SELECT 
		@JobId,
		T.c.value('Id[text()][1]','uniqueidentifier'),
		T.c.value('Sequence[text()][1]','int')
	FROM   @xmlData.nodes('/Job/Tasks/Task') T(c)
	
	SELECT 	@error = @@error
	IF @error <> 0
	BEGIN
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	END

	SET @Id = @JobId
END

GO
PRINT 'Altering procedure Task_History_Save'
GO
ALTER PROCEDURE [dbo].[Task_History_Save]
(
	@JobId uniqueidentifier,
	@TaskId uniqueidentifier,
	@TaskTitle varchar(255),
	@TaskDescription varchar(255)=null,
	@TaskType int,
	@TaskParameter varchar(255)=null,
	@Sequence int,
	@StartTime datetime,
	@EndTime datetime,
	@Result int,
	@Exceptions varchar(max)=null,
	@Id uniqueidentifier OUTPUT
)
AS
BEGIN
	declare @stmt	  varchar(256),
			@error	  int,
			@TaskHistoryId uniqueidentifier

	IF @JobId = dbo.GetEmptyGUID()
		SET @JobId = (SELECT TOP 1 JobId FROM TAJobTask WHERE TaskId = @TaskId)

	set @stmt = 'Task History Insert'
	set @TaskHistoryId=newid()
	insert into TATaskHistory
	(	
		Id,
		JobId,
		TaskId,
		TaskTitle,
		TaskDescription,
		TaskType,
		TaskParameter,
		Sequence,
		StartTime,
		EndTime,
		Result,
		Exceptions
	)
	values
	(
		@TaskHistoryId,
		@JobId,
		@TaskId,
		@TaskTitle,
		@TaskDescription,
		@TaskType,
		@TaskParameter,
		@Sequence,
		@StartTime,
		@EndTime,
		@Result,
		@Exceptions
	)
	
	set @Id=@TaskHistoryId
	select 
		@error = @@error
	if @error <> 0
	begin
		rollback
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end
	else
	begin
		return 0	
	end
END
GO
PRINT 'Creating procedure Task_GetTaskToRun'
GO
IF (OBJECT_ID('Task_GetTaskToRun') IS NOT NULL)
	DROP PROCEDURE Task_GetTaskToRun
GO
CREATE PROCEDURE [dbo].[Task_GetTaskToRun]
(
	@SiteId	uniqueIdentifier = null,
	@CurrentDate	dateTime = null
)
AS
BEGIN

	IF (@CurrentDate is null)
		SET @CurrentDate = GetUTCDATE()
	ELSE
		SET @CurrentDate = dbo.ConvertTimeToUtc(@CurrentDate,@SiteId)

	DECLARE @tbTasks TABLE(Id uniqueidentifier, SiteId uniqueidentifier)

	INSERT INTO @tbTasks
	SELECT T.Id, J.ApplicationId FROM TATask T
		JOIN TAJobTask JT ON JT.TaskId = T.Id
		JOIN TAJob J ON JT.JobId = J.Id
		JOIN [dbo].[TASchedule] S ON S.Id = T.ScheduleId
	WHERE NextRunTime <= @CurrentDate
		AND T.Status = 1 AND T.TaskStatus = 1 -- ACTIVE & Not Running
		AND (@SiteId IS NULL OR S.ApplicationId = @SiteId)
		AND (S.MaxOccurrence IS NULL OR S.MaxOccurrence <= 0 OR S.RunCount < S.MaxOccurrence)

	SELECT 
		T.Id,
		T.Title,
		T.Description,
		T.Path,
		T.Parameter,
		T.TaskType,
		T.CreatedBy,
		T.CreatedDate,
		T.ModifiedBy,
		T.ModifiedDate,
		T.Status,
		T.ScheduleId,
		FN.UserFullName AS CreatedByFullName,
		MN.UserFullName AS ModifiedByFullName,
		I.SiteId,
		T.TaskStatus
	FROM TATask T
		JOIN @tbTasks I ON I.Id = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy

	SELECT Id, Title, PrimarySiteUrl FROM SISite WHERE Id IN (SELECT SiteId FROM @tbTasks)
END
GO
PRINT 'Creating procedure TaskDto_Get'
GO
IF (OBJECT_ID('TaskDto_Get') IS NOT NULL)
	DROP PROCEDURE TaskDto_Get
GO
CREATE PROCEDURE [dbo].[TaskDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@ParentId		uniqueidentifier = NULL,
	@SiteId			uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@Status			int = NULL,
	@MaxRecords		int = NULL,
	@IsScheduled	bit = NULL,
	@Query			nvarchar(max) = NULL
)
AS
BEGIN
	SET @IsScheduled = 1

	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()

	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT Id, 0 FROM TATask ORDER BY CreatedDate DESC
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
			
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY Temp.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			T.Id AS Id			
		FROM TATask T
			JOIN TAJobTask JT ON JT.TaskId = T.Id
			JOIN TAJob J ON JT.JobId = J.Id
			JOIN @tbIds Temp ON T.Id = Temp.Id
		WHERE (@Id IS NULL OR T.Id = @Id)
			AND (@ParentId IS NULL OR J.Id = @ParentId)
			AND (@SiteId IS NULL OR J.ApplicationId = @SiteId)
			AND (@Status IS NULL OR T.Status = @Status)
			AND (@IsScheduled IS NULL OR (T.ScheduleId IS NOT NULL AND T.ScheduleId != @EmptyGuid))
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT TA.*,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM TATask TA
		JOIN @tbPagedResults T ON TA.Id = T.Id
		LEFT JOIN VW_UserFullName CU ON CU.UserId = TA.CreatedBy
		LEFT JOIN VW_UserFullName MU ON MU.UserId = TA.ModifiedBy
	ORDER BY RowNumber
	
	SELECT TA.Id AS TaskId, S.* FROM TASchedule S
		JOIN TATask TA ON S.Id = TA.ScheduleId
		JOIN @tbPagedResults T ON TA.Id = T.Id	
		
	SELECT * FROM TATaskHistory H
		JOIN @tbPagedResults T ON H.TaskId = T.Id		
END
GO
PRINT 'Creating procedure AttributeCategoryDto_Get'
GO
IF (OBJECT_ID('AttributeCategoryDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeCategoryDto_Get
GO
CREATE PROCEDURE [dbo].[AttributeCategoryDto_Get]
(
	@ProductId uniqueidentifier
)
AS
BEGIN
	SELECT * FROM ATAttributeCategory
		WHERE ProductId IS NULL OR ProductId = @ProductId
END

GO
PRINT 'Creating procedure AttributeCategoryItemDto_Delete'
GO
IF (OBJECT_ID('AttributeCategoryItemDto_Delete') IS NOT NULL)
	DROP PROCEDURE AttributeCategoryItemDto_Delete
GO
CREATE PROCEDURE [dbo].[AttributeCategoryItemDto_Delete]
(
	@Id				uniqueidentifier
)
AS
BEGIN
	DELETE FROM ATAttributeCategoryItem WHERE Id = @Id
END
GO
PRINT 'Creating procedure AttributeCategoryItemDto_BulkUpdate'
GO
IF (OBJECT_ID('AttributeCategoryItemDto_BulkUpdate') IS NOT NULL)
	DROP PROCEDURE AttributeCategoryItemDto_BulkUpdate
GO
IF (OBJECT_ID('AttributeCategoryItemDto_Add') IS NOT NULL)
	DROP PROCEDURE AttributeCategoryItemDto_Add
GO
CREATE PROCEDURE [dbo].[AttributeCategoryItemDto_Add]
(
	@Items			xml,
	@RemoveExisting	bit = NULL,
	@ModifiedBy		uniqueidentifier	
)
AS
BEGIN
	IF @RemoveExisting = 1
	BEGIN
		DELETE FROM ATAttributeCategoryItem
		WHERE CategoryId IN
			(SELECT D.n.value('(CategoryId)[1]', 'int') FROM @Items.nodes('/Items/Item') AS D(n))
	END
	ELSE
	BEGIN
		UPDATE ATAttributeCategoryItem SET 
			AttributeId = D.n.value('(AttributeId)[1]','uniqueidentifier'),
			CategoryId = D.n.value('(CategoryId)[1]','int'),
			IsRequired = D.n.value('(IsRequired)[1]','bit')
		FROM @Items.nodes('/Items/Item') AS D(n)
		WHERE D.n.value('(AttributeId)[1]', 'uniqueidentifier') = AttributeId
			AND D.n.value('(CategoryId)[1]', 'int') = CategoryId
	END
	
	INSERT INTO ATAttributeCategoryItem
	(
		Id,
		AttributeId,
		CategoryId,
		IsRequired
	)
	SELECT NEWID(),
		D.n.value('(AttributeId)[1]','uniqueidentifier'),
		D.n.value('(CategoryId)[1]','int'),
		D.n.value('(IsRequired)[1]','bit')
	FROM @Items.nodes('/Items/Item') AS D(n)
	WHERE NOT EXISTS
	(
		SELECT * FROM ATAttributeCategoryItem 
			WHERE D.n.value('(AttributeId)[1]', 'uniqueidentifier') = AttributeId
				AND D.n.value('(CategoryId)[1]', 'int') = CategoryId
	)
END
GO
PRINT 'Creating procedure AttributeCategoryItemDto_Save'
GO
IF (OBJECT_ID('AttributeCategoryItemDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeCategoryItemDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeCategoryItemDto_Save]
(
	@Id				uniqueidentifier = NULL OUTPUT,
	@AttributeId	uniqueidentifier = NULL,
	@CategoryId		int = NULL,
	@IsRequired		bit = NULL,
	@MinValue		nvarchar(200) = NULL,
	@MaxValue		nvarchar(200) = NULL,
	@ErrorMessage	nvarchar(max) = NULL	
)
AS
BEGIN

	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO ATAttributeCategoryItem
		(
			Id,
			AttributeId,
			CategoryId,
			IsRequired,
			MinValue,
			MaxValue,
			ErrorMessage	
		)
		VALUES
		(
			@Id,
			@AttributeId,
			@CategoryId,
			@IsRequired,
			@MinValue,
			@MaxValue,
			@ErrorMessage
		)

	END
	ELSE
	BEGIN
		UPDATE ATAttributeCategoryItem
		SET AttributeId = ISNULL(@AttributeId, AttributeId),
			CategoryId = ISNULL(@CategoryId, CategoryId),
			IsRequired = ISNULL(@IsRequired, IsRequired),
			MinValue = ISNULL(@MinValue, MinValue),
			MaxValue = ISNULL(@MaxValue, MaxValue),
			ErrorMessage = ISNULL(@ErrorMessage, ErrorMessage)
		WHERE Id = @Id 
	END
END
GO

PRINT 'Creating procedure AttributeDataTypeDto_Get'
GO
IF (OBJECT_ID('AttributeDataTypeDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeDataTypeDto_Get
GO
CREATE PROCEDURE [dbo].[AttributeDataTypeDto_Get]
AS
BEGIN
	SELECT * FROM ATAttributeDataType ORDER BY Sequence
END
GO
PRINT 'Creating procedure CampaignTestEmailDto_Save'
GO
IF (OBJECT_ID('CampaignTestEmailDto_Save') IS NOT NULL)
	DROP PROCEDURE CampaignTestEmailDto_Save
GO
Create procedure CampaignTestEmailDto_Save
(
	@Id uniqueidentifier output,
	@CampaignId uniqueidentifier,
	@CampaignEmailId uniqueidentifier,
	@ContactId uniqueidentifier,
	@EmailHtml nvarchar(max),
	@EmailText nvarchar(max),
	@EmailSubject nvarchar(1024),
	@SendToEmails nvarchar(max)
)
as
begin
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS(SELECT * FROM MKCampaignTestEmail WHERE Id = @Id))
	BEGIN
		IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
			SET @Id = NEWID()
		
		INSERT INTO [dbo].[MKCampaignTestEmail]
           ([Id]
           ,[CampaignId]
           ,[CampaignEmailId]
           ,[ContactId]
           ,[EmailHtml]
           ,[EmailText]
           ,[EmailSubject]
           ,[ProcessedDateTime]
           ,[SendToEmails])
		VALUES
           (@Id
           ,@CampaignId 
           ,@CampaignEmailId
           ,@ContactId
           ,@EmailHtml
           ,@EmailText
           ,@EmailSubject 
           ,null
           ,@SendToEmails)

    end
    else
    begin
		UPDATE [dbo].[MKCampaignTestEmail]
		   SET [CampaignId] = @CampaignId 
			  ,[CampaignEmailId] =@CampaignEmailId 
			  ,[ContactId] = @ContactId
			  ,[EmailHtml] = @EmailHtml
			  ,[EmailText] = @EmailText
			  ,[EmailSubject] =@EmailSubject 			  
			  ,[SendToEmails] = @SendToEmails
		 WHERE [Id] = @Id
    end
end
GO
PRINT 'Creating procedure CampaignTestEmailDto_Get'
GO
IF (OBJECT_ID('CampaignTestEmailDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignTestEmailDto_Get
GO
create procedure CampaignTestEmailDto_Get
as
begin
	delete from MKCampaignTestEmail where ProcessedDateTime is not null
	update MKCampaignTestEmail set ProcessedDateTIme= GETUTCDATE() where ProcessedDateTime is null
	select * from MKCampaignTestEmail
	
end


GO
PRINT 'Creating procedure Site_AddPageTemplates'
GO
IF (OBJECT_ID('Site_AddPageTemplates') IS NOT NULL)
	DROP PROCEDURE Site_AddPageTemplates
GO
CREATE PROCEDURE Site_AddPageTemplates
	@SiteId		UNIQUEIDENTIFIER,
	@UserId		UNIQUEIDENTIFIER = NULL,
	@Templates	dbo.NewPageTemplates READONLY
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@NewTemplates TABLE (
		Id						UNIQUEIDENTIFIER DEFAULT(NEWID()),
		DirectoryPath			NVARCHAR(255),
		Title					NVARCHAR(255),
		Description				NVARCHAR(255),
		AscxFile				NVARCHAR(255),
		CsFile					NVARCHAR(255),
		ImageFile				NVARCHAR(255),
		Type					BIT NOT NULL,
		PagePart				BIT NOT NULL,
		IsThemed				BIT DEFAULT(0),
		ZonesXml				XML,
		EditorOptionsStyleSheet NVARCHAR(255),
		EditorContentStyleSheet NVARCHAR(255),
		Processed				BIT	DEFAULT(0)
	)
	
	INSERT INTO @NewTemplates
		SELECT	NEWID(), DirectoryPath, Title, Description, AscxFile, CsFile, ImageFile, Type, PagePart, IsThemed, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet, 0
		FROM	@Templates

	BEGIN TRY
		BEGIN TRANSACTION

		IF @UserId IS NULL
			SET		@UserId = (SELECT Id FROM USUser WHERE UserName = 'iappssystemuser')

		DECLARE	@ParentId	UNIQUEIDENTIFIER

		-- Clean up directory paths
		UPDATE	@NewTemplates
		SET		DirectoryPath = dbo.Regex_ReplaceMatches(DirectoryPath, '(^/+|/+$)', 0, '')

		UPDATE	@NewTemplates
		SET		DirectoryPath = '~/Page Template Library/' + DirectoryPath
		WHERE	DirectoryPath NOT LIKE '~/Page Template Library/%'

		-- Ensure all directories exist
		DECLARE	@Directories	TABLE (
			Id				UNIQUEIDENTIFIER,
			DirectoryPath	NVARCHAR(255),
			Processed		BIT DEFAULT(0)
		)

		INSERT INTO	@Directories (DirectoryPath)
			SELECT	DISTINCT DirectoryPath
			FROM	@NewTemplates

		WHILE	(SELECT COUNT(*) FROM @Directories WHERE Processed = 0) > 0
		BEGIN
			DECLARE	@DirectoryPath	NVARCHAR(255)
			SET		@DirectoryPath = (SELECT TOP 1 DirectoryPath FROM @Directories WHERE Processed = 0)
			
			DECLARE	@Folders	TABLE (
				Position	INT IDENTITY(1, 1),
				Id			UNIQUEIDENTIFIER,
				FolderName	NVARCHAR(255),
				Processed	BIT DEFAULT(0)
			)
			
			DELETE FROM @Folders
			
			INSERT INTO @Folders (FolderName)
				SELECT * FROM dbo.Regex_GetMatches(@DirectoryPath, '(?:~/)?[^/$]+', 0)
			
			DECLARE	@ProcessedFolderPath NVARCHAR(255)
			SET		@ProcessedFolderPath = NULL
			
			WHILE	(SELECT COUNT(*) FROM @Folders WHERE Processed = 0) > 0
			BEGIN
				DECLARE	@Position	INT,
						@FolderId	UNIQUEIDENTIFIER,
						@FolderName NVARCHAR(255)
						
				SELECT		@Position = Position, @FolderId = Id, @FolderName = FolderName
				FROM		@Folders
				WHERE		Processed = 0
				ORDER BY	Position DESC
				
				SET		@ProcessedFolderPath = CASE WHEN @ProcessedFolderPath IS NULL THEN @FolderName ELSE @ProcessedFolderPath + '/' + @FolderName END
				
				SELECT	@FolderId = Id
				FROM	SISiteDirectory
				WHERE	VirtualPath = @ProcessedFolderPath AND
						ApplicationId = @SiteId
				
				IF @FolderId IS NULL
				BEGIN
					SET		@ParentId =	(SELECT TOP 1 Id FROM @Folders WHERE Processed = 1 ORDER BY Position DESC)
					
					IF @ParentId IS NOT NULL
					BEGIN
						EXEC SiteDirectory_Save @FolderId OUTPUT, @SiteId, @FolderName, NULL, @UserId, NULL, 3, NULL, @ProcessedFolderPath
						
						EXEC HierarchyNode_Save @FolderId OUTPUT, @FolderName, @ParentId, NULL, 6, @SiteId
					END
				END
				
				UPDATE	@Folders
				SET		Id = @FolderId, Processed = 1
				WHERE	Position = @Position
				
				UPDATE	@Directories
				SET		Id = @FolderId
				WHERE	DirectoryPath = @ProcessedFolderPath
			END
						
			UPDATE	@Directories
			SET		Processed = 1
			WHERE	DirectoryPath = @DirectoryPath
		END

		-- Add templates
		WHILE	(SELECT COUNT(*) FROM @NewTemplates WHERE Processed = 0) > 0
		BEGIN
			DECLARE	@TemplateId		UNIQUEIDENTIFIER,
					@TemplatePath	NVARCHAR(255),
					@TemplateTitle	NVARCHAR(255),
					@TemplateFile	NVARCHAR(255)
			
			SELECT	TOP 1 @TemplateId = Id, @TemplatePath = DirectoryPath, @TemplateTitle = Title, @TemplateFile = AscxFile
			FROM	@NewTemplates
			WHERE	Processed = 0
			
			SET	@ParentId = (SELECT Id FROM @Directories WHERE DirectoryPath = @TemplatePath)
			
			IF NOT EXISTS(
				SELECT	*
				FROM	SITemplate AS T
						INNER JOIN HSStructure AS S ON S.Id = T.Id
				WHERE	S.SiteId = @SiteId AND
						S.ParentId = @ParentId AND
						T.FileName = @TemplateFile
			)
			BEGIN		
				INSERT INTO	SITemplate (Id, ApplicationId, Title, Description, FileName, CodeFile, ImageURL, Type, PagePart, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet, Status, CreatedBy, CreatedDate)
					SELECT	@TemplateId, @SiteId, @TemplateTitle, Description, AscxFile, CsFile, ImageFile, Type, PagePart, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet, 1, @UserId, GETUTCDATE()
					FROM	@NewTemplates
					WHERE	Id = @TemplateId
					
				EXEC HierarchyNode_Save @TemplateId OUTPUT, @TemplateTitle, @ParentId, NULL, 3, @SiteId
			END
			
			UPDATE	@NewTemplates
			SET		Processed = 1
			WHERE	Id = @TemplateId
		END	
		
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK
	END CATCH
END
GO
PRINT 'Creating Procedure Contact_SearchSiteIds'
GO
IF (OBJECT_ID ('Contact_SearchSiteIds') IS NOT NULL)
	DROP PROCEDURE Contact_SearchSiteIds
GO
CREATE PROCEDURE [dbo].[Contact_SearchSiteIds]
	@SiteIds XML ,
    @ApplicationId UNIQUEIDENTIFIER
AS
	DECLARE @tblSiteIds TABLE ( Id UNIQUEIDENTIFIER )

	INSERT  INTO @tblSiteIds
                SELECT  tab.col.value('(text())[1]', 'uniqueidentifier')
                FROM    @SiteIds.nodes('GenericCollectionOfGuids/guid') tab ( col )  
				        
	DECLARE @LocalTemp TABLE ( Id UNIQUEIDENTIFIER )
	DECLARE @currentRecords INT
	
	SET @currentRecords = (SELECT Count(Id) FROM #tempContactSearchOutput )

	IF @currentRecords > 0
		BEGIN
			INSERT  INTO @LocalTemp
					SELECT  DISTINCT 
							VC.UserId
					FROM    dbo.vw_contacts VC
					INNER JOIN dbo.vw_ContactSite CS ON	VC.UserId = CS.UserId
					INNER JOIN @tblSiteIds SIds ON	SIds.Id = CS.SiteId
					INNER JOIN #tempContactSearchOutput CSO ON CSO.id = VC.UserId 
		
			TRUNCATE TABLE #tempContactSearchOutput
		
		END
	ELSE	
		BEGIN
			INSERT  INTO @LocalTemp
						SELECT  DISTINCT 
								VC.UserId
						FROM    dbo.vw_contacts VC
						INNER JOIN dbo.vw_ContactSite CS ON	VC.UserId = CS.UserId
						INNER JOIN @tblSiteIds SIds ON	SIds.Id = CS.SiteId
		END

	
	INSERT  INTO #tempContactSearchOutput
		SELECT  DISTINCT
                LT.Id
        FROM    @LocalTemp LT
        --LEFT JOIN #tempContactSearchOutput CSO ON CSO.Id = LT.Id
        --WHERE   CSO.Id IS NOT NULL

	SELECT  @@ROWCOUNT

GO
PRINT 'Altering Procedure [Template_Save]'
GO
ALTER PROCEDURE [dbo].[Template_Save]
(
	@Id					uniqueidentifier =null OUT,
	@ApplicationId		uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	@Description        nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier=null,
	@ModifiedDate    	datetime=null,
	@FileName            nvarchar(4000)=null,
	@ImageURL	         nvarchar(4000)=null,
	@CodeFile			 nvarchar(4000)=null,
	@TemplateType        int=null,
	@Status				 int=null,
	@PagePart			 bit=0,
	@LayoutTemplateId		 uniqueidentifier=null,
	@ZonesXml			xml = null,
	@ThemeId			UNIQUEIDENTIFIER = null,
	@EditorOptionsStyleSheet nvarchar(256) = null,
	@EditorContentStyleSheet nvarchar(256) = null,
	@IsNotShared bit = null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
BEGIN
--********************************************************************************
-- code
--********************************************************************************

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	/* IF @Id specified, ensure exists */
   IF (@Id is not null)
       IF((SELECT count(*) FROM  SITemplate WHERE Id = @Id AND Status != dbo.GetDeleteStatus() 
			AND ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) = 0)
       BEGIN
			RAISERROR('NOTEXISTS||Id', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
       END

	SET @Now = getdate()
	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	/*
	Sankar(05/01/2014) : 
	1. Upload a template with DEFAULT Option => Insert a row in the sitemplate with IsThemed = 0
	2. Upload a template with SELECTED Theme => Insert a row in the sitemplate with IsThemed = 1
	
	*/


	--For insert, no need to worry about the theme as we always upload the default template 1st. There can not be a Theme without a default template.
	IF (@Id is null)			-- New INSERT
	BEGIN
		SET @stmt = 'TEMPLATE INSERT'
		SET @Id = newid();
		INSERT INTO SITemplate(
					   Id,	
					   ApplicationId,
					   Title,
                       Description,
					   CreatedBy,
                       CreatedDate,
					   Status,
                       FileName,
						ImageURL,
						CodeFile,	
                       Type,
						PagePart,
						LayoutTemplateId,
						ZonesXml,
						EditorOptionsStyleSheet,
						EditorContentStyleSheet,
						IsNotShared) 
					values (
						@Id,
						@ApplicationId,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@FileName,
						@ImageURL,
						@CodeFile,
						@TemplateType,
						@PagePart,
						@LayoutTemplateId,
						@ZonesXml,
						@EditorOptionsStyleSheet,
						@EditorContentStyleSheet,
						@IsNotShared)

		SELECT @error = @@error
		IF @error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END
    END
    ELSE			-- update
         BEGIN
			
			--We need to update the SIObjectTheme table if we have a theme id greater than 0; Themeid 0 is for default template and it should go to SITemplate table
			SET @stmt = 'TEMPLATE UPDATE'
			IF ( @ThemeId = dbo.GetEmptyGUID())
			BEGIN
				UPDATE	SITemplate  WITH (rowlock)	SET 
					ApplicationId=@ApplicationId,
					Title =@Title,
					Description=@Description,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=@Now,
					ImageURL=@ImageURL,
					CodeFile=@CodeFile,
					FileName=@FileName,
					--Type=@TemplateTyp ,
					Status =@Status,
					PagePart =@PagePart,
					LayoutTemplateId = ISNULL(@LayoutTemplateId, LayoutTemplateId),
					ZonesXml = ISNULL(@ZonesXml, ZonesXml),
					EditorOptionsStyleSheet = ISNULL(@EditorOptionsStyleSheet, EditorOptionsStyleSheet),
					EditorContentStyleSheet = ISNULL(@EditorContentStyleSheet, EditorContentStyleSheet),
					IsNotShared = ISNULL(@IsNotShared,IsNotShared)
 				WHERE 	Id    = @Id 
				--AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

			END
			ELSE
			BEGIN
				
				--Update the SIObjectTheme table with the properties
				DELETE FROM SIObjectTheme WHERE ObjectId=@Id AND ObjectTypeId=3 AND ThemeId=@ThemeId
				
				INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
				VALUES (NEWID(),@Id,@ThemeId,@FileName,@CodeFile,3,@ModifiedBy,@Now,NULL,NULL,1,@Title) -- 3 for template

				UPDATE SITemplate SET IsThemed=1 WHERE Id=@Id
			END
			SELECT @error = @@error, @rowcount = @@rowcount

			IF @error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END
END
GO
PRINT 'Altering Procedure [Template_GetTemplatesByLayoutTemplate]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplatesByLayoutTemplate]
(
	@LayoutTemplateId uniqueidentifier=null,
	@ApplicationId	uniqueidentifier=null
)

AS
BEGIN
SELECT [Id]
      ,[Title]
      ,[Description]
      ,[FileName]
      ,[ImageURL]
      ,[Type]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[Status]
      ,[CodeFile]
      ,[PagePart]
      ,[LayoutTemplateId]
      ,[ZonesXml]
	  ,[EditorOptionsStyleSheet]
	  ,[EditorContentStyleSheet]
	  ,IsNotShared
      ,[ApplicationId]
      ,(SELECT StyleId FROM SITemplateStyle CSS WHERE S.Id = CSS.TemplateId FOR XML AUTO ) StyleId
	  ,(SELECT ScriptId FROM SITemplateScript SC WHERE S.Id = SC.TemplateId FOR XML AUTO ) ScriptId
      ,FN.UserFullName CreatedByFullName
	  ,MN.UserFullName ModifiedByFullName
	  ,[dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath
  FROM [SITemplate] S
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
  WHERE Status != dbo.GetDeleteStatus()
		AND (@LayoutTemplateId IS NULL OR LayoutTemplateId = @LayoutTemplateId)
  		AND (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId)))
 
END


GO
PRINT 'Altering Procedure [Template_GetTemplateByStyle]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplateByStyle]
(
	@StyleId		uniqueIdentifier ,
	@ApplicationId	uniqueidentifier=null,
	@Status			int=null,
	@Type			int=null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,CodeFile,Type,ApplicationId,
				(SELECT TS.StyleId FROM SITemplateStyle S WHERE S.StyleId =TS.StyleId FOR XML AUTO ) StyleId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile,
				PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
		FROM SITemplate T INNER JOIN SITemplateStyle TS ON TS.StyleId = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE	TS.StyleId = @StyleId and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
END


GO
PRINT 'Altering Procedure [Template_GetTemplateByScript]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplateByScript]
(
	@ScriptId		uniqueIdentifier ,
	@ApplicationId	uniqueidentifier=null,
	@Status			int=null,
	@Type			int=null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type,ApplicationId,
				(SELECT TS.ScriptId FROM SITemplateScript S WHERE S.ScriptId =TS.ScriptId FOR XML AUTO ) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile,
				PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
		FROM SITemplate T 
		INNER JOIN SITemplateScript TS ON TS.ScriptId = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE	TS.ScriptId = @ScriptId and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
END


GO
PRINT 'Altering Procedure [Template_GetTemplateByRelation]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplateByRelation]
(
	@RelationName		varchar(256)=null,
	@ObjectId			varchar(8000)=null,
	@ReturnObjectType	int ,
	@Id					uniqueIdentifier=null ,
	@ApplicationId		uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	@FileName			nvarchar(4000)=null,
	@Status				int=null,
	@PageIndex			int=1,
	@PageSize			int=null,
	@Type				int=null 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier
BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN

		SELECT T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, T.ModifiedDate,
				T.FileName,T.ImageURL,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile,PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
		FROM SITemplate T
		INNER JOIN dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R
		ON T.Id = R.ObjectId
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE  
			T.Id = isnull(@Id, T.Id) and
			isnull(upper(T.Title),'') = isnull(upper(@Title), isnull(upper(T.Title),'')) and
			isnull(upper(T. FileName),'') = isnull(upper(@FileName), isnull(upper(T.FileName),'')) and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
			(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, 
				T.ModifiedDate, T.FileName,T.ImageURL,T.CodeFile,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				PagePart
			FROM SITemplate T, dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R
			WHERE T.Id = R.ObjectId and
				 T.Id = isnull(@Id, T.Id) and
				isnull(upper(T.Title),'') = isnull(upper(@Title), isnull(upper(T.Title),'')) and
				isnull(upper(T. FileName),'') = isnull(upper(@FileName), isnull(upper(T.FileName),'')) and
				isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
				T.Type=isnull(@Type,T.Type) and
				T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,CodeFile,Type,ApplicationId,StyleId,ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				PagePart
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END


GO
PRINT 'Altering Procedure [Template_GetTemplateByHierarchy]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplateByHierarchy]
(
	
	@Id					uniqueIdentifier=null ,
	@ApplicationId		uniqueidentifier=null,
	@FileName			nvarchar(4000)=null,
	@Level				int=null,
	@PageIndex			int=1,
	@PageSize			int=null,
	@Type				int=null 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier
BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN

		SELECT T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, T.ModifiedDate,
				T.FileName,T.ImageURL,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				 CodeFile,PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
		FROM SITemplate T 
		INNER JOIN dbo.GetChildrenByHierarchy(@Id,@Level) H ON T.ID = H.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE 
			T.Type=isnull(@Type,T.Type) and
			T.Status != dbo.GetDeleteStatus() and
			isnull(T.FileName,'') = isnull(@FileName,isnull(T.FileName,'')) and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) 
			
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, T.Id, T.Title, T.Description, T.Status, T.CreatedBy, T.CreatedDate, T.ModifiedBy, 
				T.ModifiedDate, T.FileName,T.ImageURL,T.CodeFile,T.Type,T.ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE T.Id = CSS.TemplateId FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE T.Id = SC.TemplateId FOR XML AUTO ) ScriptId,
				PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
			FROM SITemplate T, dbo.GetChildrenByHierarchy(@Id,@Level) H
			WHERE T.Id = H.Id and
				T.Type=isnull(@Type,T.Type) and
				 T.Status != dbo.GetDeleteStatus() and
				 isnull(T.FileName,'') = isnull(@FileName,isnull(T.FileName,'')) and
				isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) )

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,CodeFile,Type,ApplicationId,StyleId,ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,PagePart, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END


GO
PRINT 'Altering Procedure [Template_GetTemplateByContainer]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplateByContainer]
(
	@ContainerId		uniqueIdentifier ,
	@ApplicationId	uniqueidentifier=null,
	@Status			int=null,
	@Type			int=null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type,ApplicationId,
				(SELECT TS.ContainerId FROM SITemplateContainer S WHERE S.ContainerId =TS.ContainerId FOR XML AUTO ) ContainerId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CodeFile, LayoutTemplateId, ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
		FROM SITemplate T INNER JOIN SITemplateContainer TS ON TS.ContainerId = T.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = T.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = T.ModifiedBy
		WHERE	TS.ContainerId = @ContainerId and
			isnull(T.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(T.ApplicationId,@EmptyGUID)) and
			T.Type=isnull(@Type,T.Type) and
			T.Status = isnull(@Status, T.Status)and 
				(T.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
END

GO
PRINT 'Altering Procedure [Template_GetTemplate]'
GO
ALTER PROCEDURE [dbo].[Template_GetTemplate]    
(      
 @Id    uniqueIdentifier=null ,      
 @ApplicationId uniqueidentifier=null,      
 @Title   nvarchar(256)=null,      
 @FileName  nvarchar(4000)=null,      
 @Status   int=null,      
 @PageIndex  int=1,      
 @PageSize  int=null,      
 @Type   int=null,      
 @ThemeId  UNIQUEIDENTIFIER=null,      
 @ResolveThemeId bit = null       
 )      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
DECLARE @EmptyGUID uniqueidentifier      
      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
IF(@ResolveThemeId IS NULL)      
 SET @ResolveThemeId = 0      

    IF EXISTS( Select 1 from SISIte where Id = @ApplicationId and ThemeId IS NULL ) -- This is changed as Template was not pulled in case of multisite, if template was created in a different site and used in another site
	SET	@ApplicationId = NULL

SET @EmptyGUID = dbo.GetEmptyGUID()      
 IF @PageSize is Null      
 BEGIN      
   IF(@ResolveThemeId = 1)      
    SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 3)      
          
  IF(@ThemeId = dbo.GetEmptyGUID() OR @ThemeId IS NULL)      
  BEGIN      
  --Print 'In ThemeId is null'
	IF(@Id IS NOT NULL)
	BEGIN
		--PRINT 'In ID is not null'
		   SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
			FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared,
		  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
		  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
			FN.UserFullName CreatedByFullName,      
			MN.UserFullName ModifiedByFullName,      
		  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId      
		  FROM SITemplate S       
		  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
		  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
		  INNER JOIN HSStructure H on S.Id = H.Id      
		  WHERE S.Id = isnull(@Id, S.Id) and      
		   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
		   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
		   (@ApplicationId IS NULL OR S.ApplicationId=@ApplicationId OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
		   S.Type=isnull(@Type,S.Type) and      
		   S.Status = isnull(@Status, S.Status)and       
			(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())      
		END	
		ELSE
		BEGIN	
			--PRINT 'In ID is null'
			 SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
			FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared,
		  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
		  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
			FN.UserFullName CreatedByFullName,      
			MN.UserFullName ModifiedByFullName,      
		  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId      
		  FROM SITemplate S       
		  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
		  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
		  INNER JOIN HSStructure H on S.Id = H.Id      
		  WHERE S.Id = isnull(@Id, S.Id) and      
		   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
		   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
		   (@ApplicationId IS NULL OR  S.ApplicationId=@ApplicationId OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId)) ) and      
		   S.Type=isnull(@Type,S.Type) and      
		   S.Status = isnull(@Status, S.Status)and       
			(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())      
			AND (S.IsNotShared IS NULL OR S.IsNotShared = 0 OR ApplicationId=@ApplicationId)
		END	
  END      
  ELSE      
  BEGIN  
     --Print 'In ThemeId is not null'
    SELECT S.Id,   
    CASE WHEN OT.Title IS NULL THEN S.Title ELSE OT.Title END AS Title,    
    S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate,      
    CASE WHEN OT.FileName IS NULL THEN S.FileName ELSE OT.FileName END AS FileName,  
    ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared,
  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
    FN.UserFullName CreatedByFullName,      
    MN.UserFullName ModifiedByFullName,      
  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, OT.CodeFile, H.ParentId      
  FROM SITemplate S       
  LEFT JOIN SIObjectTheme OT ON OT.ObjectId = S.Id AND OT.ObjectTypeId = 3  AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId=@ThemeId)  
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
  INNER JOIN HSStructure H on S.Id = H.Id      
  WHERE   
  S.Id = isnull(@Id, S.Id) and      
   isnull(upper(OT.Title),'') = isnull(upper(@Title), isnull(upper(OT.Title),'')) and      
   isnull(upper(OT. FileName),'') = isnull(upper(@FileName), isnull(upper(OT.FileName),'')) and      
   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
   S.Type=isnull(@Type,S.Type) and      
   S.Status = isnull(@Status, S.Status)and       
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())   
  END      
        
 END      
       
 ELSE      
 BEGIN      
  WITH ResultEntries AS (       
   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)      
    AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,       
    ModifiedDate, FileName,ImageURL,Type,ApplicationId,      
 (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO ) StyleId,      
    (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO ) ScriptId,      
    [dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared
   FROM SITemplate S      
   WHERE S.Id = isnull(@Id, S.Id) and      
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
    isnull(S.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(S.ApplicationId,@EmptyGUID)) and      
    S.Type=isnull(@Type,S.Type) and      
    S.Status = isnull(@Status, S.Status)and       
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))      
      
  SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
    FileName,ImageURL,Type,ApplicationId, StyleId,ScriptId,      
    FN.UserFullName CreatedByFullName,      
    MN.UserFullName ModifiedByFullName,      
    ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, H.ParentId, EditorOptionsStyleSheet, EditorContentStyleSheet,IsNotShared 
  FROM ResultEntries S      
  INNER JOIN HSStructure H on S.Id = H.Id      
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
   LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
  WHERE Row between       
   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize      
      
 END      
      
END
GO
PRINT 'Altering procedure Contact_SearchDistributionList'
GO
ALTER PROCEDURE [dbo].[DistributionList_GetContactCount]
--********************************************************************************
-- PARAMETERS
(
		@Id			uniqueidentifier, 
	    @TotalContact		int  output
)
--********************************************************************************
AS
BEGIN
--********************************************************************************
Declare @ListType int,
@ApplicationId	uniqueidentifier


SELECT @ListType=ListType,@ApplicationId = ApplicationId from dbo.TADistributionLists where Id = @Id

		if(@ListType=0)
				BEGIN
				SELECT @TotalContact=COUNT(*) from dbo.TADistributionListUser where DistributionListId = @Id AND ContactListSubscriptionType != 3
				END
		else 
				BEGIN
						Declare @queryXml xml
						SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@Id 
						Exec Contact_GetAutoDistributionListContactCount @Xml=@queryXml,@ApplicationId =@ApplicationId,@MaxRecords=0,@TotalRecords =@TotalContact out
				END
set @TotalContact = isnull(@TotalContact,0)
END

GO
PRINT 'Altering procedure Contact_SearchDistributionList'
GO
ALTER PROCEDURE [dbo].[Contact_SearchDistributionList](
	@DistributionListIds xml,
	@ApplicationId uniqueidentifier)
AS

 -- Code goes here

	DECLARE @tblDistributionListIds TABLE ( Id UNIQUEIDENTIFIER )

	INSERT  INTO @tblDistributionListIds
                SELECT  tab.col.value('(text())[1]', 'uniqueidentifier')
                FROM    @DistributionListIds.nodes('GenericCollectionOfGuids/guid') tab ( col )  
				        
	DECLARE @LocalTemp TABLE ( Id UNIQUEIDENTIFIER )
	DECLARE @currentRecords INT
	
	SET @currentRecords = (SELECT Count(Id) FROM #tempContactSearchOutput )

	IF @currentRecords > 0
		BEGIN
			INSERT  INTO @LocalTemp
					SELECT  DISTINCT 
							VC.UserId
					FROM    dbo.vw_contacts VC
					INNER JOIN dbo.TADistributionListUser DLU ON VC.UserId = DLU.UserId AND ContactListSubscriptionType != 3 
					INNER JOIN dbo.TADistributionLists DL ON DLU.DistributionListId = DL.Id
					INNER JOIN @tblDistributionListIds Temp ON Temp.Id = dl.Id
					INNER JOIN #tempContactSearchOutput CSO ON CSO.id = DLU.UserId
		
			TRUNCATE TABLE #tempContactSearchOutput
		
		END
	ELSE	
		BEGIN
			INSERT  INTO @LocalTemp
					SELECT  DISTINCT 
							VC.UserId
					FROM    dbo.vw_contacts VC
					INNER JOIN dbo.TADistributionListUser DLU ON VC.UserId = DLU.UserId AND ContactListSubscriptionType != 3 
					INNER JOIN dbo.TADistributionLists DL ON DLU.DistributionListId = DL.Id
					INNER JOIN @tblDistributionListIds Temp ON Temp.Id = dl.Id
		END

	
	INSERT  INTO #tempContactSearchOutput
		SELECT  DISTINCT
                LT.Id
        FROM    @LocalTemp LT

	SELECT  @@ROWCOUNT


GO
PRINT 'Altering Procedure [Contact_SearchContact]'
GO
ALTER PROCEDURE [dbo].[Contact_SearchContact]
    (
      @Xml XML ,
      @ApplicationId UNIQUEIDENTIFIER ,
      @MaxRecords INT = NULL ,
      @ContactListId UNIQUEIDENTIFIER = NULL ,
      @TotalRecords INT OUT,
	  @IncludeAddress bit = 1

    )
AS 
    BEGIN
        SET @MaxRecords = ISNULL(@MaxRecords, 500)

        EXEC Contact_SearchCLR @Xml, @ApplicationId = @ApplicationId,
            @MaxRecords = @MaxRecords, @TotalRecords = @TotalRecords OUT, @IncludeAddress = @IncludeAddress, @ContactListId = @ContactListId
--EXEC Contact_Search @Xml,@ApplicationId=@ApplicationId,@MaxRecords=@MaxRecords,@TotalRecords =@TotalRecords out

        IF ( @ContactListId IS NOT NULL
             AND @ContactListId != dbo.GetEmptyGuid()
           ) 
            BEGIN
                IF EXISTS ( SELECT  ID
                            FROM    TADistributionListSite
                            WHERE   DistributionListId = @ContactListId
                                    AND SiteId = @ApplicationId ) 
                    BEGIN 
                        UPDATE  TADistributionListSite
                        SET     Count = @TotalRecords
                        WHERE   DistributionListId = @ContactListId
                                AND SiteId = @ApplicationId
                    END
                ELSE 
                    BEGIN 
                        INSERT  INTO TADistributionListSite
                                ( Id ,
                                  DistributionListId ,
                                  SiteId ,
                                  Count
                                )
                                SELECT  NEWID() ,
                                        @ContactListId ,
                                        @ApplicationId ,
                                        @TotalRecords
                    END
            END

--UPDATE dbo.TADistributionLists SET ContactCount = @TotalRecords WHERE ID = @ContactListId

    END



GO
PRINT 'Creating Procedure ContactListDto_GetSites'
GO
IF (OBJECT_ID ('ContactListDto_GetSites') IS NOT NULL)
	DROP PROCEDURE ContactListDto_GetSites
GO
CREATE PROCEDURE [dbo].[ContactListDto_GetSites]
    (
      @Id UNIQUEIDENTIFIER = NULL ,
      @SiteId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN
	
			--Get all the contacts for the given contact list
        DECLARE @ListType INT
        SELECT  @ListType = ListType
        FROM    TADistributionLists
        WHERE   Id = @Id

        IF ( @ListType = 0 ) --For manual
            BEGIN

                SELECT DISTINCT
                        SiteId
                FROM    TADistributionListUser DLU
                        INNER JOIN vw_contactSite cs ON cs.UserId = DLU.UserId
                WHERE   DLU.DistributionListId = @Id
                        AND cs.IsPrimary = 1
				
            END
        ELSE 
            IF ( @ListType = 1 ) --For auto list
                BEGIN
				
                    SELECT DISTINCT
                            SiteId
                    FROM    TADistributionListSite
                    WHERE   DistributionListId = @Id and Count > 0
				

                END
    END
GO

PRINT 'Altering procedure EmailStatsRollup'
GO
ALTER PROCEDURE [dbo].[EmailStatsRollup]
AS
Begin
	Declare @Id uniqueidentifier 
	Set @Id =newid()
	Insert into MKEmailStatsRollupHistory(Id,StartTime,Status)
	Values(@Id,getUTCdate(),0)
	BEGIN TRY
		Exec EmailStatsRollup_SendLog
		Exec EmailStatsRollup_Bounces
		Exec EmailStatsRollup_RunHistory
		exec dbo.EmailStatsRollup_WatchGoals
		exec [dbo].[EmailStatsRollup_ContactStats]

		Update MKEmailStatsRollupHistory
		SET EndTime=getUTCdate()
			,Message= 'Success'
			,Status=1
	Where Id=@Id
	END TRY
	BEGIN CATCH
	Update MKEmailStatsRollupHistory
		SET EndTime=getUTCdate()
			,Message= ERROR_MESSAGE()
			,Status=3
	Where Id=@Id
	
	END CATCH
	
End

GO
PRINT 'Altering procedure EmailStatsRollup_SendLog'
GO
ALTER PROCEDURE [dbo].[EmailStatsRollup_SendLog]
As
Begin
	Create Table #SendLogRollup
	(
		SendId uniqueidentifier not null,
		Opens integer default 0,
		Clicks integer default 0,
		TriggeredWatches integer default 0
	)

	ALTER TABLE #SendLogRollup ADD PRIMARY KEY (SendId)

	insert into #SendLogRollup (SendId, Opens, Clicks, TriggeredWatches)
	Select SendId,
		ISNULL((Select count(*) From MKEmailSendActions a1 Where a1.ActionType = 1 and a1.SendId = a.SendId),0) 'Opens',
		(Select count(*) From MKEmailSendActions a1 Where a1.ActionType = 2 and a1.SendId = a.SendId) 'Clicks',
		(Select count(*) From MKEmailSendActions a1 Where a1.ActionType = 5 and a1.SendId = a.SendId) 'TriggeredWatches'
	From MKEmailSendActions a 
	Group By SendId
	
	--SELECT * FROM #SendLogRollup

		delete from MKEmailPerDayLog 
		Insert into MKEmailPerDayLog(SendDate,NoOfEmailSent,ApplicationId) 
		SELECT Convert(nvarchar(10),SendDate,101), count(*) as NoOfLog,ApplicationId from MKEmailSendLog 
		group by ApplicationId,Convert(nvarchar(10),SendDate,101)

	--select * from @SendLogRollup

	Update MKEmailSendLog Set
		Opened = 
			Case 
				When r.Opens > 0 Then 1
				Else 0
			End,
		Opens = ISNULL(r.Opens,0),
		Clicked =
			Case 
				When r.Clicks > 0 Then 1
				Else 0
			End,
		Clicks = ISNULL(r.Clicks,0),
		TriggeredWatch =
			Case 
				When r.TriggeredWatches > 0 Then 1
				Else 0
			End,
		TriggeredWatches = ISNULL(r.TriggeredWatches,0)
	From MKEmailSendLog a
		left outer join #SendLogRollup r on a.Id = r.SendId

		drop table #SendLogRollup
End
GO
PRINT 'Altering procedure Campaign_GetUserActionResultsForCampaign'
GO

ALTER PROCEDURE [dbo].[Campaign_GetUserActionResultsForCampaign] (
	@CampaignId uniqueidentifier,
	@IncludeAll bit
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @tblAddress TABLE(Id uniqueidentifier primary key, AddressLine1 nvarchar(100), AddressLine2 nvarchar(100), AddressLine3 nvarchar(100), City nvarchar(100), 
				State nvarchar(100), StateCode nvarchar(100), CountryName nvarchar(100), CountryCode nvarchar(100), Zip nvarchar(100), County nvarchar(255))
	DECLARE @tblSendLog TABLE (UserId uniqueidentifier primary key, LoweredEmail nvarchar(100), Opens int, Clicks int, Unsubscribes int, Bounces int, TriggeredWatches int)	
			
	INSERT INTO @tblAddress
	SELECT a.Id,
		a.[AddressLine1],
		a.[AddressLine2],
		a.[AddressLine3],
		a.[City],
		s.[State],
		s.[StateCode],
		c.[CountryName],
		c.[CountryCode],
		a.[Zip],
		a.County
	FROM GLAddress AS a
	INNER JOIN GLState AS s ON a.StateId = s.Id
	INNER JOIN GLCountry AS c ON a.CountryId = c.Id
	
	--If IncludeAll is true, perform a right join on MKEmailSendActions to include users who did not perform any action
	IF @IncludeAll = 0
		BEGIN
			INSERT INTO @tblSendLog
			SELECT		
				sl.UserId AS Id,
				C.Email  AS Email,
				SUM(CASE WHEN sa.ActionType = 1 THEN 1 ELSE 0 END) AS Opens,
				SUM(CASE WHEN sa.ActionType = 2 THEN 1 ELSE 0 END) AS Clicks,
				SUM(CASE WHEN sa.ActionType = 3 THEN 1 ELSE 0 END) AS Unsubscribes,
				(SELECT COUNT(Id) AS Bounces
									FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND Bounced = 1 AND UserId = sl.UserId) AS Bounces,
				SUM(CASE WHEN sa.ActionType = 5 THEN 1 ELSE 0 END) AS TriggeredWatches						
			 FROM		MKEmailSendLog AS sl
						INNER JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
						INNER JOIN	vw_contacts  AS C ON C.UserId = sl.UserId
			 WHERE		sl.CampaignId = @CampaignId
			 GROUP BY	C.Email, sl.UserId
						 
			SELECT DISTINCT(c.UserId), 
				m.[Opens],
				m.[Clicks],
				m.[Unsubscribes],
				m.[Bounces],
				m.[TriggeredWatches],
				c.[FirstName], 
				c.[MiddleName],
				c.[LastName],
				c.[CompanyName],
				c.[BirthDate],
				c.[Gender],
				c.[Status],
				c.[HomePhone],
				c.[MobilePhone],
				c.[OtherPhone],
				c.[Notes],
				c.[Email],
				a.[AddressLine1],
				a.[AddressLine2],
				a.[AddressLine3],
				a.[City],
				a.[State],
				a.[StateCode],
				a.[CountryName],
				a.[CountryCode],
				a.[Zip],
				a.County
			FROM vw_contacts AS c
			INNER JOIN @tblSendLog m ON m.UserId = c.UserId
			LEFT JOIN @tblAddress a ON a.Id = c.AddressId
			
			
			
			--Second table of Profile values to prevent recurring database
			SELECT distinct(sl.UserId),AV.ContactId as UserId,
			AT.Title as PropertyName, AV.Value as PropertyValueString  from ATAttribute AT inner join ATContactAttributeValue AV on AT.Id=AV.AttributeId  
			INNER JOIN MKEmailSendLog AS sl ON AV.ContactId = sl.UserId
			INNER JOIN MKEmailSendActions AS sa ON sa.SendId = sl.Id
			WHERE sl.CampaignId = @CampaignId
		END
	ELSE
		BEGIN	
			INSERT INTO @tblSendLog
			SELECT		
				sl.UserId AS Id,
				C.Email AS Email,
				SUM(CASE WHEN sa.ActionType = 1 THEN 1 ELSE 0 END) AS Opens,
				SUM(CASE WHEN sa.ActionType = 2 THEN 1 ELSE 0 END) AS Clicks,
				SUM(CASE WHEN sa.ActionType = 3 THEN 1 ELSE 0 END) AS Unsubscribes,
				(SELECT COUNT(Id) AS Bounces
									FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND Bounced = 1 AND UserId = sl.UserId) AS Bounces,
				SUM(CASE WHEN sa.ActionType = 5 THEN 1 ELSE 0 END) AS TriggeredWatches						
			 FROM		MKEmailSendLog AS sl
						LEFT JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
						INNER JOIN	vw_contacts  AS C ON C.UserId  = sl.UserId
			 WHERE		sl.CampaignId = @CampaignId
			 GROUP BY	C.Email, sl.UserId
	 				
			SELECT DISTINCT(c.UserId), 
				m.[Opens],
				m.[Clicks],
				m.[Unsubscribes],
				m.[Bounces],
				m.[TriggeredWatches],
				c.[FirstName], 
				c.[MiddleName],
				c.[LastName],
				c.[CompanyName],
				c.[BirthDate],
				c.[Gender],
				c.[Status],
				c.[HomePhone],
				c.[MobilePhone],
				c.[OtherPhone],
				c.[Notes],
				c.[Email],
				a.[AddressLine1],
				a.[AddressLine2],
				a.[AddressLine3],
				a.[City],
				a.[State],
				a.[StateCode],
				a.[CountryName],
				a.[CountryCode],
				a.[Zip],
				a.County
			FROM vw_contacts AS c
				INNER JOIN	@tblSendLog m ON m.UserId = c.UserId
				LEFT JOIN @tblAddress a ON a.Id = c.AddressId
				
			
			
			--Second table of Profile values to prevent recurring database
			SELECT distinct(sl.UserId),AV.ContactId as UserId,
			AT.Title as PropertyName, AV.Value as PropertyValueString  from ATAttribute AT inner join ATContactAttributeValue AV on AT.Id=AV.AttributeId  
			INNER JOIN MKEmailSendLog AS sl ON AV.ContactId = sl.UserId
			INNER JOIN MKEmailSendActions AS sa ON sa.SendId = sl.Id
			WHERE sl.CampaignId = @CampaignId
		END
END

GO
PRINT 'Creating Procedure EmailStatsRollup_ContactStats'
GO
IF (OBJECT_ID ('EmailStatsRollup_ContactStats') IS NOT NULL)
	DROP PROCEDURE EmailStatsRollup_ContactStats
GO
CREATE PROCEDURE [dbo].[EmailStatsRollup_ContactStats]
As
Begin

Declare @DistributionListId uniqueidentifier
Declare @SiteId uniqueidentifier
Declare @SearchXml xml
Declare @TotalRecords INT
Declare @ListType int
	DECLARE db_cursor CURSOR FOR  
		Select L.DistributionListId,SiteId,SearchXml,ListType
		from TADistributionListSite L
		INNER JOIN dbo.TADistributionLists DL On DL.Id =L.DistributionListId
		INNER JOIN dbo.TADistributionListSearch LS on LS.DistributionListId = L.DistributionListId
		INNER JOIN CTSearchQuery Q ON Q.Id = LS.SearchQueryId
		Where DL.Status=1 


	OPEN db_cursor   
	FETCH NEXT FROM db_cursor INTO @DistributionListId,@SiteId,@SearchXml,@ListType

	WHILE @@FETCH_STATUS = 0   
	BEGIN   
				BEGIN TRY
				IF @ListType=0
				BEGIN
					Select 1;
					-- static list are not shared across multiple sites
					--IF EXISTS (Select 1 from TADistributionListSite LS INNER JOIN TADistributionLists L On L.Id =LS.DistributionListId Where LS.LastSynced IS NULL OR LS.LastSynced < isnull(L.ModifiedDate,L.CreatedDate))
					--EXEC [dbo].[ContactListDto_ExecuteFilter] @Id=@DistributionListId
				END
				ELSE
				BEGIN
				exec Contact_SearchContact @Xml=@SearchXml,@ApplicationId=@SiteId, @MaxRecords=1,@TotalRecords=@TotalRecords output,@IncludeAddress =0
				Update TADistributionListSite  SET [Count] =isnull(@TotalRecords,0),LastSynced=GETUTCDATE()
				Where DistributionListId=@DistributionListId AND SiteId=@SiteId
				END
				END TRY
				BEGIN CATCH
				print 'Error Running List-' + cast(@DistributionListId as char(36)) + ' Error-' + ERROR_MESSAGE()
				END CATCH
			
			

		FETCH NEXT FROM db_cursor INTO @DistributionListId,@SiteId,@SearchXml,@ListType 
	END   

	CLOSE db_cursor   
	DEALLOCATE db_cursor
					

		Truncate table MKContactStats
		INSERT INTO MKContactStats(ContactId,Opens,Clicks,TriggeredWatches,Unsubscribes,Bounces)
		Select UserId ContactId,sum(Opens)Opens,sum(Clicks) Clicks, sum(TriggeredWatches) TriggeredWatches,sum(case when Unsubscribed=1 then 1 else 0 end)Unsubscribes,sum(case when Bounced=1 then 1 else 0 end) Bounces 
		from MKEmailSendLog
		Group By UserId

End

GO
PRINT 'Creating Procedure ContactListDto_GetStats'
GO
IF (OBJECT_ID ('ContactListDto_GetStats') IS NOT NULL)
	DROP PROCEDURE ContactListDto_GetStats
GO
PRINT 'Creating Procedure ContactListStatisticsDto_Get'
GO
IF (OBJECT_ID ('ContactListStatisticsDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactListStatisticsDto_Get
GO
CREATE PROCEDURE [dbo].[ContactListStatisticsDto_Get]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@SiteId UNIQUEIDENTIFIER = NULL,
	@TotalRecords INT OUT
)
AS
BEGIN
	
  Declare @ListType int
  --Declare @TotalRecords int 
			SELECT @ListType = ListType from TADistributionLists where Id=@Id

			IF (@ListType = 0) --For manual
			BEGIN
				SET @TotalRecords = (Select count(1) from TADistributionListUser Where DistributionListId =@Id)
				SELECT
					DistributionListId Id	
					  ,case when count(1) =0 then 0 else  sum(case when [Opened]=1 then 1 else 0 end)/count(1) end OpenPercent
					  ,case when count(1) =0 then 0 else  sum(case when [Clicked]=1 then 1 else 0 end)/count(1) end ClickPercent
					  ,case when count(1) =0 then 0 else  sum(case when [TriggeredWatch]=1 then 1 else 0 end)/count(1) end WatchPercent
					  ,case when count(1) =0 then 0 else  sum(case when [Unsubscribed]=1 then 1 else 0 end)/count(1) end UnsubscribePercent
					  ,case when count(1) =0 then 0 else  sum(case when [Bounced]=1 then 1 else 0 end)/count(1) end BouncePercent
				from TADistributionListUser DLU 
				LEFT JOIN [MKContactStats] cs on cs.ContactId=DLU.UserId
				WHERE DLU.DistributionListId = @Id
				Group By DLU.DistributionListId
				
			END
			ELSE IF (@ListType = 1) --For auto list
			BEGIN
				Declare @queryXml xml
				SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@Id 
					
					Declare @tblContacts Table
					(
						Id uniqueidentifier,
						UserId uniqueidentifier,
						FirstName varchar(1024) null,
						MiddleName varchar(1024) null,
						LastName varchar(1024) null,
						CompanyName varchar(1024) null,
						BirthDate datetime null,
						Gender varchar(1024) null,
						AddressId uniqueidentifier null,
						Status int null,
						HomePhone varchar(1024) null,
						MobilePhone varchar(1024) null,
						OtherPhone varchar(1024) null,
						ImageId uniqueidentifier null,
						Notes varchar(max) null,
						Email varchar(256),
						ContactType int,
						ContactSourceId int,
						TotalRecords int null,
						DistributionListId uniqueidentifier Default('00000000-0000-0000-0000-000000000000')
					)
				
				insert into @tblContacts(Id,UserId,FirstName,MiddleName,LastName,CompanyName,BirthDate,Gender,AddressId,Status,HomePhone,MobilePhone,OtherPhone,ImageId,Notes,
				Email,ContactType,ContactSourceId,TotalRecords)
				exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@SiteId, @MaxRecords=0,@TotalRecords=@TotalRecords output, @IncludeAddress = 0
				

				 SELECT @Id Id,*
				 FROM 
				 (Select  
				 case when count(1) =0 then 0 else  sum(case when [Opened]=1 then 1 else 0 end)/count(1) end OpenPercent
					  ,case when count(1) =0 then 0 else  sum(case when [Clicked]=1 then 1 else 0 end)/count(1) end ClickPercent
					  ,case when count(1) =0 then 0 else  sum(case when [TriggeredWatch]=1 then 1 else 0 end)/count(1) end WatchPercent
					  ,case when count(1) =0 then 0 else  sum(case when [Unsubscribed]=1 then 1 else 0 end)/count(1) end UnsubscribePercent
					  ,case when count(1) =0 then 0 else  sum(case when [Bounced]=1 then 1 else 0 end)/count(1) end BouncePercent
				FROM @tblContacts C
				LEFT JOIN [MKContactStats] cs on cs.ContactId=C.UserId
				Group by C.DistributionListId)T
			END

END

GO
PRINT 'Creating Procedure ContactListDto_Get'
GO
IF (OBJECT_ID ('ContactListDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactListDto_Get
GO
CREATE PROCEDURE [dbo].[ContactListDto_Get]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@Ids			nvarchar(max) = NULL,
	@ContactListGroupId UNIQUEIDENTIFIER = NULL,
	@CampaignId			UNIQUEIDENTIFIER =NULL,
	@Status			int = NULL ,
	@SiteId			uniqueidentifier = NULL,
	@TrackingId  UNIQUEIDENTIFIER = NULL,
	@ListType		int = NULL ,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@MaxRecords		int = NULL,
	@Query	nvarchar(max) = NULL,
	@IsGlobal BIT = NULL
)
AS
BEGIN

	IF @ContactListGroupId = dbo.GetEmptyGuid()
		SET @ContactListGroupId = NULL
	
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL AND ( @CampaignId is NULL OR @CampaignId = dbo.GetEmptyGuid())
	BEGIN
		INSERT INTO @tbIds
		SELECT Id, 0 FROM TADistributionLists
	END
	ELSE IF @Query IS NOT NULL
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
	ELSE IF @CampaignId IS NOT NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT DistributionListId,0 FROM MKCampaignDistributionList
		Where CampaignId =@CampaignId
	END

	IF @Ids IS NOT NULL
	BEGIN
		DELETE FROM @tbIds 
		WHERE Id NOT IN (SELECT Items FROM dbo.SplitGUID(@Ids, ','))
	END

	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()
			
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) As RowNumber,
			COUNT(DL.Id) OVER () AS TotalRecords,
			DL.Id AS Id			
		FROM TADistributionLists DL
			LEFT JOIN TADistributionListGroup DLG ON DL.Id = DLG.DistributionId
			JOIN @tbIds T ON T.Id = DL.Id 
		WHERE (@Id IS NULL OR DL.Id = @Id)
			AND (@SiteId IS NULL OR 
					( 
						(DL.IsGlobal = 0 AND DL.ApplicationId = @SiteId) 
							OR (DL.IsGlobal = 1 AND DL.ApplicationId IN (SELECT A.SiteId  FROM dbo.GetAncestorSites(@SiteId) A))
					)
				)
				 
			AND (@Status IS NULL OR DL.Status = @Status)
			AND (@ListType IS NULL OR DL.ListType = @ListType)
			AND (@TrackingId IS NULL OR DL.TrackingId = @TrackingId)
			 AND (@ContactListGroupId IS NULL OR DLG.DistributionGroupId = @ContactListGroupId)
			 --AND (@IsGlobal IS NULL OR DL.IsGlobal = @IsGlobal )
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT DL.*,SQ.*,DLG.DistributionGroupId AS ContactListGroupId,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords,
		DL.ApplicationId as SiteId,
	(SELECT dbo.GetTotalContactsCount( 302 ,DL.Id, @SiteId))AS TotalContacts
	FROM TADistributionLists DL 
	LEFT JOIN TADistributionListSearch DLS ON DLS.DistributionListId = DL.Id
		    LEFT JOIN CTSearchQuery SQ ON SQ.Id = DLS.SearchQueryId
		JOIN @tbPagedResults T ON T.Id = DL.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = DL.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = DL.ModifiedBy
		LEFT JOIN TADistributionListGroup DLG ON DLG.DistributionId = T.Id
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	ORDER BY T.RowNumber

END

GO
PRINT 'Creating Procedure ContactListDto_Save'
GO
IF (OBJECT_ID ('ContactListDto_Save') IS NOT NULL)
	DROP PROCEDURE ContactListDto_Save
GO
CREATE PROCEDURE [dbo].[ContactListDto_Save]
    (
      @Id UNIQUEIDENTIFIER = NULL OUTPUT ,
      @Title NVARCHAR(MAX) = NULL ,
      @Description NVARCHAR(MAX) = NULL ,
      @Status INT = NULL ,
      @SiteId UNIQUEIDENTIFIER ,
      @TrackingId NVARCHAR(MAX) ,
      @ListType INT = NULL ,
      @ModifiedBy UNIQUEIDENTIFIER = NULL ,
      @SearchXml XML = NULL ,
      @ContactListGroupId UNIQUEIDENTIFIER = NULL ,
      @IsGlobal BIT = NULL--,
	--@IsVariantAccessible bit = null,
    )
AS 
    BEGIN
        DECLARE @UtcNow DATETIME
        SET @UtcNow = GETUTCDATE()
	
        IF ( @ContactListGroupId = dbo.GetEmptyGUID() ) 
            SET @ContactListGroupId = NULL
	
        IF ( @Id IS NULL
             OR @Id = dbo.GetEmptyGUID()
           ) 
            BEGIN
                SET @Id = NEWID()
		
                INSERT  INTO TADistributionLists
                        ( Id ,
                          Title ,
                          Description ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status ,
                          ApplicationId ,
                          TrackingId ,
                          ListType ,
                          IsGlobal--,
			--IsVariantAccessible			
		            )
                VALUES  ( @Id ,
                          @Title ,
                          @Description ,
                          @UtcNow ,
                          @ModifiedBy ,
                          NULL ,
                          NULL ,
                          @Status ,
                          @SiteId ,
                          @TrackingId ,
                          @ListType ,
                          @IsGlobal--,
			--@IsVariantAccessible
			
		            )

                IF ( @ContactListGroupId IS NOT NULL ) 
                    BEGIN
                        INSERT  INTO TADistributionListGroup
                                ( DistributionGroupId ,
                                  DistributionId
                                )
                        VALUES  ( @ContactListGroupId ,
                                  @Id
                                )
                    END

					INSERT INTO TADistributionListSite(Id,DistributionListId,SiteId,Count)
					Values(NEWID(),@Id,@SiteId,0)
		
            END
        ELSE 
            BEGIN
		
		
                UPDATE  TADistributionLists
                SET     Title = ISNULL(@Title, Title) ,
                        Description = ISNULL(@Description, Description) ,
                        Status = ISNULL(@Status, Status) ,
                        TrackingId = ISNULL(@TrackingId, TrackingId) ,
                        ModifiedBy = @ModifiedBy ,
                        ListType = @ListType ,
                        ModifiedDate = @UtcNow ,
                        IsGlobal = ISNULL(@IsGlobal, IsGlobal)
                WHERE   Id = @Id
                        AND ApplicationId = @SiteId
		
		--select * from TADistributionListSearch

                IF ( @ContactListGroupId IS NOT NULL ) 
                    BEGIN
                        DELETE  FROM TADistributionListGroup
                        WHERE   DistributionId = @Id

                        INSERT  INTO TADistributionListGroup
                                ( DistributionGroupId ,
                                  DistributionId
                                )
                        VALUES  ( @ContactListGroupId ,
                                  @Id
                                )
                    END
				IF NOT EXISTS(Select 1 FROM TADistributionListSite Where DistributionListId=@Id AND SiteId=@SiteId)
				INSERT INTO TADistributionListSite(Id,DistributionListId,SiteId,Count)
					Values(NEWID(),@Id,@SiteId,0)
            END
	
	
        IF ( @SearchXml IS NOT NULL ) 
            BEGIN
		
                IF EXISTS ( SELECT  1
                            FROM    TADistributionListSearch
                            WHERE   DistributionListId = @Id ) 
                    BEGIN
			----Updating the search query table
                        UPDATE  C
                        SET     SearchXml = ISNULL(@SearchXml, SearchXml) ,
                                Status = ISNULL(@Status, Status) ,
                                ModifiedBy = @ModifiedBy ,
                                ModifiedDate = @UtcNow
                        FROM    CTSearchQuery C
                                JOIN TADistributionListSearch DLS ON DLS.SearchQueryId = C.Id
                        WHERE   DLS.DistributionListId = @Id
                    END
                ELSE 
                    BEGIN
		
                        DECLARE @SearchQueryId UNIQUEIDENTIFIER
                        SET @SearchQueryId = NEWID()
		
		
                        INSERT  INTO CTSearchQuery
                                ( Id ,
                                  SearchXml ,
                                  Status ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  ModifiedBy ,
                                  ModifiedDate
			              )
                        VALUES  ( @SearchQueryId ,
                                  @SearchXml ,
                                  ISNULL(@Status, 1) ,
                                  ISNULL(@ModifiedBy, dbo.GetEmptyGuid()) ,
                                  @UtcNow ,
                                  NULL ,
                                  NULL
			              )
			
                        INSERT  INTO TADistributionListSearch
                                ( Id ,
                                  DistributionListId ,
                                  SearchQueryId
			              )
                        VALUES  ( NEWID() ,
                                  @Id ,
                                  @SearchQueryId
			              )
			
			--INSERT INTO MKContactFilter
			--(
			--Id,
			--ApplicationId,
			--Title,
			--FilterType
			--)
			--VALUES
			--(
			--@SearchQueryId,
			--@SiteId,
			--@Title,
			--0 --For Template
			--)
                    END

            END
    END



GO
PRINT 'Creating Procedure Contact_SearchSiteGroup'
GO
IF (OBJECT_ID ('Contact_SearchSiteGroup') IS NOT NULL)
	DROP PROCEDURE Contact_SearchSiteGroup
GO
CREATE PROCEDURE Contact_SearchSiteGroup
    (
      @SiteGroups XML ,
      @ApplicationId UNIQUEIDENTIFIER
    )
AS 
    PRINT CAST(@SiteGroups AS NVARCHAR(MAX))

    DECLARE @LocalTemp TABLE ( Id UNIQUEIDENTIFIER )
    INSERT  INTO @LocalTemp
            SELECT  DISTINCT
                    TV.UserId
            FROM    dbo.SISiteGroup SG
                    INNER JOIN vw_ContactSite TV ON SG.SiteId = TV.SiteId
                    INNER JOIN @SiteGroups.nodes('GenericCollectionOfGuids/guid') tab ( col ) ON tab.col.value('(text())[1]',
                                                              'uniqueidentifier') = SG.GroupId
        
    DECLARE @currentRecords INT
    SET @currentRecords = ( SELECT  COUNT(Id)
                            FROM    #tempContactSearchOutput
                          )
       
    IF @currentRecords > 0 
        BEGIN
            DELETE  T
            FROM    #tempContactSearchOutput t
                    LEFT JOIN @LocalTemp LT ON LT.Id = T.Id
            WHERE   LT.Id IS NULL
        END
    ELSE 
        BEGIN
            INSERT  INTO #tempContactSearchOutput
                    SELECT  DISTINCT
                            Id
                    FROM    @LocalTemp
        END


   
    SELECT  @@ROWCOUNT

GO
PRINT 'Creating Procedure EmailRecordDto_Save'
GO
IF (OBJECT_ID ('EmailRecordDto_Save') IS NOT NULL)
	DROP PROCEDURE EmailRecordDto_Save
GO
CREATE PROCEDURE [dbo].[EmailRecordDto_Save]
(
	@Id					UNIQUEIDENTIFIER = NULL OUTPUT,
	@SiteId				UNIQUEIDENTIFIER,
	@CampaignId			UNIQUEIDENTIFIER,
	@CampaignRunId		UNIQUEIDENTIFIER,
	@UserId				UNIQUEIDENTIFIER,
	@SendDate			DATETIME = NULL,
	@Bounced			BIT = NULL,
	@Opened				BIT = NULL,
	@Opens				INT = NULL,
	@Clicked			BIT = NULL,
	@Clicks				INT = NULL,
	@TriggeredWatch		BIT = NULL,
	@TriggeredWatches	INT = NULL,
	@Unsubscribed		BIT = NULL,
	@ProductId			UNIQUEIDENTIFIER,
	@ModifiedBy			UNIQUEIDENTIFIER
)
AS
BEGIN	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS(SELECT * FROM MKEmailSendLog WHERE Id = @Id))
	BEGIN
		SET @Id = NEWID()
		
		IF @SendDate IS NULL
			SET @SendDate = GETUTCDATE()
		
		INSERT INTO MKEmailSendLog
		(
			Id,
			ApplicationId,
			CampaignId,
			CampaignRunId,
			UserId,
			SendDate,
			Bounced,
			Opened,
			Opens,
			Clicked,
			Clicks,
			TriggeredWatch,
			TriggeredWatches,
			Unsubscribed			
		)
		VALUES
		(
			@Id,
			@SiteId,
			@CampaignId,
			@CampaignRunId,
			@UserId,
			@SendDate,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		)
	END
	ELSE
	BEGIN
		UPDATE	MKEmailSendLog
		SET		ApplicationId = @SiteId,
				CampaignId = @CampaignId,
				CampaignRunId = @CampaignRunId,
				UserId = @UserId,
				SendDate = ISNULL(@SendDate, SendDate),
				Bounced = ISNULL(@Bounced, Bounced),
				Opened = ISNULL(@Opened, Opened),
				Opens = ISNULL(@Opens, Opens),
				Clicked = ISNULL(@Clicked, Clicked),
				Clicks = ISNULL(@Clicks, Clicks),
				TriggeredWatch = ISNULL(@TriggeredWatch, TriggeredWatch),
				TriggeredWatches = ISNULL(@TriggeredWatches, TriggeredWatches),
				Unsubscribed = ISNULL(@Unsubscribed, Unsubscribed)
		WHERE	Id = @Id 
	END
END
GO
PRINT 'Creating Procedure EmailRecordDto_Get'
GO
IF (OBJECT_ID ('EmailRecordDto_Get') IS NOT NULL)
	DROP PROCEDURE EmailRecordDto_Get
GO
CREATE PROCEDURE [dbo].[EmailRecordDto_Get]
(
	@Id				UNIQUEIDENTIFIER = NULL,
	@CampaignId		UNIQUEIDENTIFIER = NULL,
	@CampaignRunId	UNIQUEIDENTIFIER = NULL,
	@SiteId			UNIQUEIDENTIFIER = NULL,
	@PageNumber		INT = NULL,
	@PageSize		INT = NULL,
	@MaxRecords		INT = NULL,
	@SortBy			NVARCHAR(100) = NULL,  
	@SortOrder		NVARCHAR(10) = NULL,
	@Query			NVARCHAR(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound INT, @PageUpperBound INT, @SortClause NVARCHAR(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id UNIQUEIDENTIFIER, RowNumber INT)
	IF @Query IS NULL
	BEGIN
		IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
		IF (@SortBy IS NOT NULL)
			SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	
		INSERT	INTO @tbIds
		SELECT	Id,  ROW_NUMBER() OVER (ORDER BY SendDate DESC) AS RowNumber
		FROM	MKEmailSendLog
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
			
	DECLARE @tbPagedResults TABLE(Id UNIQUEIDENTIFIER, RowNumber INT, TotalRecords INT)
	
	;WITH CTE AS(
		SELECT	ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				L.Id AS Id
		FROM	MKEmailSendLog AS L
				INNER JOIN @tbIds T ON L.Id = T.Id
		WHERE	(@Id IS NULL OR L.Id = @Id) AND
				(@CampaignId IS NULL OR L.CampaignId = @CampaignId) AND
				(@CampaignRunId IS NULL OR L.CampaignRunId = @CampaignRunId) AND
				(@SiteId IS NULL OR L.ApplicationId = @SiteId)
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		L.*,
				T.RowNumber,
				T.TotalRecords
	FROM		MKEmailSendLog AS L
				INNER JOIN	@tbPagedResults T ON T.Id = L.Id
	ORDER BY RowNumber
END

GO
PRINT 'Creating Procedure ContactListGroupDto_Get'
GO
IF (OBJECT_ID ('ContactListGroupDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactListGroupDto_Get
GO
CREATE PROCEDURE [dbo].[ContactListGroupDto_Get]
    (
      @Id UNIQUEIDENTIFIER = NULL ,
      @Status INT = NULL ,
      @SiteId UNIQUEIDENTIFIER = NULL ,
      @PageNumber INT = NULL ,
      @PageSize INT = NULL ,
      @MaxRecords INT = NULL ,
      @Query NVARCHAR(MAX) = NULL

    )
AS 
    BEGIN
        DECLARE @PageLowerBound INT ,
            @PageUpperBound INT ,
            @SortClause NVARCHAR(50)
        SET @PageLowerBound = @PageSize * @PageNumber
        IF ( @PageLowerBound IS NOT NULL
             AND @PageNumber > 0
           ) 
            SET @PageUpperBound = @PageLowerBound - @PageSize + 1

        DECLARE @tbCount TABLE
            (
              Id UNIQUEIDENTIFIER ,
              [Count] INT
            )
        INSERT  INTO @tbCount
                SELECT  DistributionGroupId ,
                        COUNT(DISTINCT DLG.DistributionId)
                FROM    TADistributionListGroup DLG
                        INNER JOIN dbo.TADistributionLists DL ON DL.Id = DLG.DistributionId
                                                              AND ( @Status IS NULL
                                                              OR DL.Status = @Status
                                                              )
                                                              AND ( ( DL.IsGlobal = 0
                                                              AND DL.ApplicationId = @SiteId
                                                              )
                                                              OR ( DL.IsGlobal = 1
                                                              AND DL.ApplicationId IN (
                                                              SELECT
                                                              A.SiteId
                                                              FROM
                                                              dbo.GetAncestorSites(@SiteId) A )
                                                              )
                                                              )
                GROUP BY DistributionGroupId      

	
        DECLARE @EmptyGuid UNIQUEIDENTIFIER
        SET @EmptyGuid = dbo.GetEmptyGUID()
			
        DECLARE @tbIds TABLE
            (
              Id UNIQUEIDENTIFIER ,
              RowNumber INT
            )
        IF @Query IS NULL 
            BEGIN
                INSERT  INTO @tbIds
                        SELECT  Id ,
                                0
                        FROM    TADistributionGroup
            END
        ELSE 
            BEGIN
                INSERT  INTO @tbIds
                        EXEC sp_executesql @Query
            END

        DECLARE @tbPagedResults TABLE
            (
              Id UNIQUEIDENTIFIER ,
              RowNumber INT ,
              TotalRecords INT
            ) ;
        WITH    CTE
                  AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY T.RowNumber ) AS RowNumber ,
                                COUNT(DLG.Id) OVER ( ) AS TotalRecords ,
                                DLG.Id AS Id
                       FROM     TADistributionGroup DLG
                                JOIN @tbIds T ON T.Id = DLG.Id
                                LEFT JOIN @tbCount T2 ON T2.Id = T.Id
                       WHERE    ( @Id IS NULL
                                  OR DLG.Id = @Id
                                )
                                AND ( ( @SiteId IS NULL
                                        OR DLG.ApplicationId = @SiteId
                                      )
                                      OR ( DLG.ApplicationId IN (
                                           SELECT   A.SiteId
                                           FROM     dbo.GetAncestorSites(@SiteId) A ) )
                                      AND T2.Count > 0
                                    )
                                AND ( @Status IS NULL
                                      OR DLG.Status = @Status
                                    )
                     )
            INSERT  INTO @tbPagedResults
                    SELECT  Id ,
                            RowNumber ,
                            TotalRecords
                    FROM    CTE
                    WHERE   ( @PageLowerBound IS NULL
                              OR @PageUpperBound IS NULL
                              OR ( RowNumber BETWEEN @PageUpperBound
                                             AND     @PageLowerBound )
                            )
                            AND ( @MaxRecords IS NULL
                                  OR @MaxRecords = 0
                                  OR RowNumber <= @MaxRecords
                                )
	
        SELECT  DLG.* ,
                CU.UserFullName AS CreatedByFullName ,
                MU.UserFullName AS ModifiedByFullName ,
                T.RowNumber ,
                T.TotalRecords ,
                C.[Count]
        FROM    TADistributionGroup DLG
                JOIN @tbPagedResults T ON T.Id = DLG.Id
                LEFT JOIN VW_UserFullName CU ON CU.UserId = DLG.CreatedBy
                LEFT JOIN VW_UserFullName MU ON MU.UserId = DLG.ModifiedBy
                LEFT JOIN @tbCount C ON T.Id = C.Id
        WHERE   ( @PageLowerBound IS NULL
                  OR @PageUpperBound IS NULL
                  OR ( RowNumber BETWEEN @PageUpperBound
                                 AND     @PageLowerBound )
                )
                AND ( @MaxRecords IS NULL
                      OR @MaxRecords = 0
                      OR RowNumber <= @MaxRecords
                    )
        ORDER BY DLG.IsSystem DESC, RowNumber
		
    END

GO


PRINT 'Creating Procedure AddressDto_Save'
GO
IF (OBJECT_ID ('AddressDto_Save') IS NOT NULL)
	DROP PROCEDURE AddressDto_Save

GO
CREATE PROCEDURE [dbo].[AddressDto_Save]
(
	@Id				uniqueidentifier = null OUTPUT,
	@Address		xml = null,
	@ModifiedBy		uniqueidentifier = null
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF (@Address IS NOT NULL)
	BEGIN
		DECLARE @StateId uniqueidentifier, @CountryId uniqueidentifier
		SET @Id = (SELECT TOP 1 A.n.value('@Id', 'uniqueidentifier') FROM 
			@Address.nodes('/Address') AS A(n))

		SELECT TOP 1 @StateId = A.n.value('@StateId', 'uniqueidentifier'),
			@CountryId = A.n.value('@CountryId', 'uniqueidentifier')
		FROM @Address.nodes('/Address') AS A(n)

		IF @StateId = dbo.GetEmptyGUID()
			SET @StateId = NULL
		IF @CountryId = dbo.GetEmptyGUID()
			SET @CountryId = NULL

		IF @Id IS NOT NULL AND @Id != dbo.GetEmptyGUID()
		BEGIN
			UPDATE GLAddress
			SET
				FirstName = A.n.value('@FirstName', 'nvarchar(max)'),
				LastName = A.n.value('@LastName', 'nvarchar(max)'),
				AddressLine1 = A.n.value('@AddressLine1', 'nvarchar(max)'),
				AddressLine2 = A.n.value('@AddressLine2', 'nvarchar(max)'),
				AddressLine3 = A.n.value('@AddressLine3', 'nvarchar(max)'),
				City = A.n.value('@City', 'nvarchar(max)'),
				Zip = A.n.value('@Zip', 'nvarchar(max)'),
				StateId = @StateId,
				CountryId = @CountryId,
				Phone = A.n.value('@Phone', 'nvarchar(max)'),
				ModifiedBy = @ModifiedBy,
				ModifiedDate = @UtcNow
			FROM @Address.nodes('/Address') AS A(n)
			WHERE Id = @Id
		END
		ELSE
		BEGIN
			SET @Id = NEWID()
			INSERT INTO GLAddress
			(
				Id,
				FirstName,
				LastName,
				AddressType,
				AddressLine1,
				AddressLine2,
				AddressLine3,
				City,
				StateId,
				Zip,
				CountryId,
				Phone,
				CreatedDate,
				CreatedBy,
				Status
			)
			SELECT
				@Id,
				A.n.value('@FirstName', 'nvarchar(max)'),
				A.n.value('@LastName', 'nvarchar(max)'),
				1,
				A.n.value('@AddressLine1', 'nvarchar(max)'),
				A.n.value('@AddressLine2', 'nvarchar(max)'),
				A.n.value('@AddressLine3', 'nvarchar(max)'),
				A.n.value('@City', 'nvarchar(max)'),
				@StateId,
				A.n.value('@Zip', 'nvarchar(max)'),
				@CountryId,
				A.n.value('@Phone', 'nvarchar(max)'),
				@UtcNow,
				@ModifiedBy,
				1
			FROM 
				@Address.nodes('/Address') AS A(n)
		END
	END
END

GO

PRINT 'Creating Procedure ContactListGroupDto_Save'
GO
IF (OBJECT_ID ('ContactListGroupDto_Save') IS NOT NULL)
	DROP PROCEDURE ContactListGroupDto_Save
GO
CREATE PROCEDURE [dbo].[ContactListGroupDto_Save]
(
	@Id UNIQUEIDENTIFIER = NULL OUTPUT,
	@Title NVARCHAR(MAX) = NULL,
	@Description  NVARCHAR(MAX) = NULL,
	@Status INT = NULL,
	@ApplicationId UNIQUEIDENTIFIER ,
	@IsSystem	bit = null
	--@ModifiedBy					uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime, @ModifiedBy uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	
	set @ModifiedBy = dbo.GetEmptyGuid()
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO TADistributionGroup
		(
			Id,
			Title,
			Description,
			CreatedDate,
			CreatedBy,
			ModifiedDate,
			ModifiedBy,
			Status,
			IsSystem,
			ApplicationId			
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			@UtcNow,
			@ModifiedBy,
			NULL,
			NULL,
			@Status,
			@IsSystem,
			@ApplicationId
		)
	END
	ELSE
	BEGIN
		UPDATE TADistributionGroup
		SET Title = ISNULL(@Title,Title),
			Description = ISNULL(@Description,Description),
			Status = ISNULL(@Status,Status),
			IsSystem = ISNULL(@IsSystem,IsSystem),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow
		WHERE Id = @Id AND ApplicationId = @ApplicationId
	END
END



GO

PRINT 'Creating Procedure ContactListDto_Delete'
GO
IF (OBJECT_ID ('ContactListDto_Delete') IS NOT NULL)
	DROP PROCEDURE ContactListDto_Delete
GO
CREATE PROCEDURE [dbo].[ContactListDto_Delete]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@ModifiedBy					uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	
	UPDATE TADistributionLists 
	SET 
	Status = dbo.GetDeleteStatus() ,
	ModifiedBy = @ModifiedBy,
	ModifiedDate = @UtcNow
	WHERE Id=@Id
	
END
GO
PRINT 'Creating Procedure ContactListGroupDto_Delete'
GO
IF (OBJECT_ID ('ContactListGroupDto_Delete') IS NOT NULL)
	DROP PROCEDURE ContactListGroupDto_Delete
GO
CREATE PROCEDURE [dbo].[ContactListGroupDto_Delete]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@ModifiedBy	uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	
	UPDATE TADistributionGroup 
	SET 
	Status = dbo.GetDeleteStatus() ,
	ModifiedBy = @ModifiedBy,
	ModifiedDate = @UtcNow
	WHERE Id=@Id
	
	--Removes the ditribution lists from the group but the actual distribution lists still exist
	DELETE FROM TADistributionListGroup
	WHERE DistributionGroupId=@Id
END
GO
PRINT 'Altering procedure PageDefinition_Get '
GO
ALTER PROCEDURE [dbo].[PageDefinition_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageDefinitionIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageDefinitionIds
	SELECT PageDefinitionId FROM PageDefinition
		WHERE SiteId = @siteId AND PageStatus != 3 AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT M.PageDefinitionId, M.PageMapNodeId, SiteId, M.DisplayOrder, --PageDefinitionXml,
		TemplateId, Title, Description, PageStatus, WorkflowState, PublishCount, PublishDate, FriendlyName,
		WorkflowId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ArchivedDate, StatusChangedDate, 
		AuthorId, EnableOutputCache, OutputCacheProfileName, ExcludeFromSearch, IsDefault, IsTracked,
		HasForms, TextContentCounter, CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,
		ScheduledPublishDate, ScheduledArchiveDate, NextVersionToPublish, E.CampaignId
	FROM PageDefinition D 
		JOIN PageMapNodePageDef M ON D.PageDefinitionId = M.PageDefinitionId
		JOIN @PageDefinitionIds T ON D.PageDefinitionId = T.Id
		Left JOIN MKCampaignEmail E ON D.PageDefinitionId = E.CMSPageId
	ORDER BY DisplayOrder ASC

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId
	FROM USSecurityLevelObject S
		JOIN @PageDefinitionIds T ON S.ObjectId = T.Id
	WHERE ObjectTypeId = 8
		
	SELECT PageDefinitionId, 
		StyleId, CustomAttributes 
	FROM SIPageStyle S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id
		
	SELECT PageDefinitionId, 
		ScriptId, CustomAttributes 
	FROM SIPageScript S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id

	SELECT P.PageDefinitionId, 
		P.PageMapNodeId, P.DisplayOrder 
	FROM PageMapNodePageDef P
		JOIN @PageDefinitionIds T ON P.PageDefinitionId = T.Id
END
GO
PRINT 'Creating Procedure Contact_FilterBySiteContext'
GO
IF (OBJECT_ID ('Contact_FilterBySiteContext') IS NOT NULL)
	DROP PROCEDURE Contact_FilterBySiteContext
GO
CREATE PROCEDURE [dbo].[Contact_FilterBySiteContext]
    (
      @ApplicationId UNIQUEIDENTIFIER
    )
AS 
    IF EXISTS ( SELECT  Id
                FROM    #tempContactSearchOutput ) 
        BEGIN
        
          
   
 
 
            DECLARE @Lft BIGINT ,
                @Rgt BIGINT
	
            SELECT  @Lft = LftValue ,
                    @Rgt = RgtValue
            FROM    SISite
            WHERE   Id = @ApplicationId
 


            DECLARE @SiteIds TABLE
                (
                  SiteId UNIQUEIDENTIFIER
                )
                
                
            INSERT  INTO @SiteIds
                    SELECT DISTINCT
                            ID
                    FROM    dbo.SISite
                    WHERE   LftValue >= @Lft
                            AND RgtValue <= @Rgt
                        
                       
        
            DELETE  t
            FROM    #tempContactSearchOutput t
                    LEFT JOIN ( SELECT  CS.ContactId AS UserID
                                FROM    dbo.MKContactSite CS
                                        INNER JOIN @SiteIds S ON CS.SiteId = S.SiteId
                                UNION
                                SELECT  UserID
                                FROM    dbo.USSiteUser CS
                                        INNER JOIN @SiteIds S ON CS.SiteId = S.SiteId
                              ) TV ON TV.UserId = t.Id
            WHERE   TV.UserId IS NULL
            
            SELECT  COUNT(Id)
            FROM    #tempContactSearchOutput
        END
    SELECT  0
GO
PRINT 'Creating Procedure AttributeDto_Get'
GO
IF (OBJECT_ID ('AttributeDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Get
GO

CREATE PROCEDURE [dbo].[AttributeDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@Ids			nvarchar(max) = NULL,
	@GroupId		uniqueidentifier = NULL,
	@Category		int = NULL,
	@SiteId			uniqueidentifier = NULL,
	@ObjectId		uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@Status			int = NULL,
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL,
	@Query			nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
		IF (@SortBy IS NOT NULL)
			SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	
		INSERT INTO @tbIds
		SELECT Id,  ROW_NUMBER() OVER (ORDER BY
			CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
			CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
			CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
		) AS RowNumber
		FROM ATAttribute
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
	
	IF @Category IS NOT NULL OR @ObjectId IS NOT NULL
	BEGIN
		;WITH CTE AS(
			SELECT AttributeId FROM ATAttributeCategoryItem WHERE CategoryId = @Category
			UNION
			SELECT AttributeId FROM AttributeValue_GetByObjectId(@ObjectId, @Category)
		)
		DELETE T 
		FROM @tbIds T
			LEFT JOIN CTE C ON T.Id = C.AttributeId 
		WHERE C.AttributeId IS NULL
	END
	IF @Ids IS NOT NULL
	BEGIN
		DELETE FROM @tbIds 
		WHERE Id NOT IN (SELECT Items FROM dbo.SplitGUID(@Ids, ','))
	END
		
	DECLARE @tbPagedResults TABLE(Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			A.Id AS Id
		FROM ATAttribute A
			JOIN ATAttributeGroup G ON A.AttributeGroupId = G.Id
			JOIN @tbIds T ON A.Id = T.Id
		WHERE (@Id IS NULL OR A.Id = @Id)
			AND ((@Status IS NULL AND A.Status != 3) OR A.Status = @Status)
			AND (@GroupId IS NULL OR G.Id = @GroupId)
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT A.*,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM ATAttribute A
		JOIN @tbPagedResults T ON T.Id = A.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = A.ModifiedBy
	ORDER BY IsSystem DESC, RowNumber ASC
	
	SELECT G.*, A.Id AS AttributeId
	FROM ATAttribute A
		JOIN ATAttributeGroup G ON A.AttributeGroupId = G.Id
		JOIN @tbPagedResults T ON T.Id = A.Id
	
	SELECT D.*, A.Id AS AttributeId
	FROM ATAttribute A
		JOIN ATAttributeDataType D ON A.AttributeDataTypeId = D.Id
		JOIN @tbPagedResults T ON T.Id = A.Id
		
	SELECT E.*, A.Id AS AttributeId
	FROM ATAttribute A
		JOIN ATAttributeEnum E ON A.Id = E.AttributeId
		JOIN @tbPagedResults T ON T.Id = A.Id
	
	SELECT V.*, E.Id AS ExpressionId, E.RegularExpression, E.Title AS ExpressionName, A.Id AS AttributeId
	FROM ATAttribute A
		JOIN ATAttributeValidation V ON A.Id = V.AttributeId
		JOIN @tbPagedResults T ON T.Id = A.Id
		LEFT JOIN ATAttributeExpression E ON E.Id = V.ExpressionId
		
	SELECT D.*, A.Id AS AttributeId
	FROM ATAttribute A
		JOIN ATAttributeCategoryItem D ON A.Id = D.AttributeId
		JOIN @tbPagedResults T ON T.Id = A.Id
	
	IF @ObjectId IS NOT NULL
	BEGIN
		SELECT V.*, A.Id AS AttributeId
		FROM ATAttribute A
			JOIN AttributeValue_GetByObjectId(@ObjectId, @Category) V ON A.Id = V.AttributeId
			JOIN @tbPagedResults T ON T.Id = A.Id
		WHERE V.ObjectId = @ObjectId
	END
END
GO
PRINT 'Creating Procedure AttributeGroupDto_Delete'
GO
IF (OBJECT_ID ('AttributeGroupDto_Delete') IS NOT NULL)
	DROP PROCEDURE AttributeGroupDto_Delete
GO
CREATE PROCEDURE [dbo].[AttributeGroupDto_Delete]
(
	@Id				uniqueidentifier = NULL,
	@ModifiedBy		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()

	IF EXISTS(SELECT * FROM ATAttribute WHERE AttributeGroupId = @Id AND Status != 3)
    BEGIN
        DECLARE @ErrMsg nvarchar(256)
		SET	@ErrMsg = 'Attribute'
		RAISERROR ('EXISTS||%s', 16, 1, @ErrMsg)
		RETURN
    END
	
	UPDATE ATAttributeGroup 
	SET 
		Status = dbo.GetDeleteStatus(),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @UtcNow
	WHERE Id = @Id
	
END

GO
PRINT 'Creating Procedure SiteDto_SearchSiteAttributes'
GO
IF (OBJECT_ID ('SiteDto_SearchSiteAttributes') IS NOT NULL)
	DROP PROCEDURE SiteDto_SearchSiteAttributes
GO
CREATE PROCEDURE [dbo].[SiteDto_SearchSiteAttributes]
    @SiteAttributes XML ,
    @ApplicationId UNIQUEIDENTIFIER
AS 
    BEGIN
        DECLARE @ContactSearch TABLE
            (ConditionId UNIQUEIDENTIFIER,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeValueID UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(MAX) ,
              Value2 NVARCHAR(MAX) ,
              Operartor NVARCHAR(50) ,
              DataType NVARCHAR(50),
              ValueCount INT
            )

      
                SELECT  NEWID() as ConditionId,
						tab.col.value('@AttributeId', 'uniqueidentifier') as AttributeId ,                        
                        tab.col.value('@Value1', 'nvarchar(max)') as Value1,
                        tab.col.value('@Value2', 'nvarchar(max)') as Value2,
                        tab.col.value('@Operator', 'nvarchar(50)') as Operator,
                        'System.String' as DataType,
						col.query('AttributeValueId') AS XmlValue
				INTO    #tbltest
                FROM    @SiteAttributes.nodes('/ArrayOfAttributeSearchItem/AttributeSearchItem') tab ( col )


				      INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operartor ,
                  DataType ,
                  ValueCount
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        pref.value('(text())[1]', 'uniqueidentifier') ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        COUNT(pref.value('(text())[1]', 'uniqueidentifier')) OVER ( PARTITION BY T.ConditionId,
                                                              T.AttributeId  ) AS ValueCount
                FROM    #tbltest T
                        OUTER APPLY XmlValue.nodes('AttributeValueId') AS Value ( pref )


        UPDATE  CS
        SET     CS.DataType = ADT.Type
        FROM    @ContactSearch CS
                INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
                INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 


        DECLARE @CacheTable TABLE
            (
              SiteId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              DataType NVARCHAR(50) ,
              VALUE NVARCHAR(4000)
            )
     
        INSERT  INTO @CacheTable
                ( SiteId ,
                  AttributeId ,
                  AttributeEnumId ,
                  DataType ,
                  VALUE
               
                )
                SELECT  CA.SiteId ,
                        CS.AttributeId ,
                        CA.AttributeEnumId ,
                        ADT.Type AS DataType ,
                        CA.Value
                FROM    @ContactSearch CS
                        INNER JOIN dbo.ATSiteAttributeValue CA ON CA.AttributeId = CS.AttributeId
                        INNER JOIN ATAttribute A ON A.ID = CA.AttributeId
                        INNER JOIN ATAttributeDataType ADT ON ADT.ID = A.AttributeDataTypeId



        DECLARE @LocalSiteId TABLE ( Id UNIQUEIDENTIFIER, AttributeValueId UNIQUEIDENTIFIER, ConditionId UNIQUEIDENTIFIER )
       
	    INSERT  INTO @LocalSiteId
                SELECT  
                        CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = '='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = '>'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = '>='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = '<'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = '<='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = '!='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = 'BETWEEN'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'CONTAINS'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'Does Not Contain'
 -------------------------------------------------------------------------------------------------------------------------------------
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = '='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = '>'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = '>='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = '<'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = '<='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = '!='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = 'BETWEEN'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'CONTAINS'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'Does Not Contain'
 
 ---------------------------------------------------------------------------------------------------------------------------------------       


 -------------------------------------------------------------------------------------------------------------------------------------
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = '='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = '>'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = '>='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = '<'
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = '<='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = '!='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = 'BETWEEN'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'CONTAINS'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'Does Not Contain'
 
 --------------------------------------------------------------------------------------------------------------------------------------- 

        DECLARE @LocalTemp TABLE ( Id UNIQUEIDENTIFIER )
      --  INSERT  INTO @LocalTemp
                --SELECT DISTINCT
                --        CS.UserId
                --FROM    vw_ContactSite CS
                --        INNER JOIN @LocalSiteId LS ON LS.Id = CS.SiteId




						 
						 SELECT DISTINCT
                        TV2.Id as SiteId
                FROM    ( SELECT    * ,
                                    COUNT(AttributeValueId) OVER ( PARTITION BY ConditionID,
                                                              Id ) AS ValueCount
                          FROM      ( SELECT  DISTINCT
                                                *
                                      FROM      @LocalSiteId
                                    ) TV
                        ) TV2
                        INNER JOIN @ContactSearch CS ON CS.ConditionId = TV2.ConditionId
                                                        AND CS.ValueCount = TV2.ValueCount
						

END

GO
PRINT 'Creating Procedure ContactDto_GetUnSynced'
GO
IF (OBJECT_ID ('ContactDto_GetUnSynced') IS NOT NULL)
	DROP PROCEDURE ContactDto_GetUnSynced
GO

CREATE  PROCEDURE [dbo].[ContactDto_GetUnSynced]
(
	@SiteId			uniqueidentifier = NULL,
	@PageNumber		int,
	@PageSize		int
)
AS
BEGIN
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY Email) As RowNumber,
			COUNT(Id) OVER () AS TotalRecords,
			Id		
		FROM(	
				SELECT C.Email,Id 
				FROM MKContact C 
				--INNER JOIN MKContactSite SC ON SC.ContactId = C.Id
				WHERE  --(@SiteId IS NULL OR SC.SiteId = @SiteId)
					C.LastSynced IS NULL OR C.LastSynced < C.ModifiedDate
				UNION ALL
				SELECT c.Email,C.UserId Id 
				FROM vw_contacts c
				LEFT JOIN GLSyncLog s on c.UserId = s.Id
				WHERE ContactType!=0
					AND (s.LastSynced IS NULL OR S.LastSynced < isnull(c.ModifiedDate,c.CreatedDate))
			) T
	)
		SELECT C.UserId Id, C.*,
		T.RowNumber,
		T.TotalRecords,
		TS.SiteIds
	FROM 
		vw_contacts C JOIN CTE T ON T.Id = C.UserId
		CROSS APPLY (select STUFF(
                   (Select SiteIds
				   FROM
				   (
				   SELECT
                        ', ' + cast(t2.SiteId as char(36)) SiteIds
                        FROM MKContactSite t2
                        WHERE C.UserId=t2.ContactId
						UNION  
						Select ', ' + cast(t3.SiteId as char(36)) SiteIds
                        FROM USSiteUser t3
                        WHERE C.UserId=t3.UserId
						) Temp
                        FOR XML PATH(''), TYPE
                   ).value('.','varchar(max)')
                   ,1,2, ''
              ) AS SiteIds) TS 
	WHERE T.RowNumber BETWEEN ((@PageNumber-1) * @PageSize)+1 AND ((@PageNumber) * @PageSize)
	ORDER BY RowNumber

END

GO
PRINT 'Creating Procedure ContactDto_Get'
GO
IF (OBJECT_ID ('ContactDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactDto_Get
GO
/*
* This procedure is used for the following purposes
* 1. Gettting contacts (directly from MKContact)
* 2. Getting Contacts for the CampaignRun (email processor core logic to get contacts to send emails)
* 3. Getting Contacts for the given ContactList (distribution list); for Manual as well as dynamic lists
* 4. Verifying logic for CampaignRun for email processor;in this case only CampaignRun Id is required.
*/
CREATE PROCEDURE [dbo].[ContactDto_Get]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@Ids varchar(max)=NULL,
	@Status			int = NULL ,
	@SiteId			uniqueidentifier = NULL,
	@CampaignId		uniqueidentifier = null, --Pass only campaignRun id to verify whether all the batches are complete for email processor.
	@CampaignRunId uniqueidentifier = null, -- Pass campaign id along with run id to get the next batches of contacts.
	@EmailPerCampaignLimit INT = NULL, -- Need to be passed;this is obsolete. 
	@ContactListId uniqueidentifier = null, --Will get the contacts for the given list(DL)
	@FormId			uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL,
	@Query	NVARCHAR(MAX) = NULL,
	@BatchSize INT = NULL,
	@Email nvarchar(max) = null
)
AS
BEGIN

DECLARE @LoweredEmail nvarchar(max)

IF (@Email IS NOT NULL AND LTRIM(RTRIM(@Email)) <> '')
	SET @LoweredEmail = LOWER(LTRIM(RTRIM(@Email)))
IF (@Ids is NOT NULL)
BEGIN
	IF (@PageNumber IS NOT NULL AND @PageSize IS NOT NULL)
	BEGIN
	WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY Email) As RowNumber,
			COUNT(Id) OVER () AS TotalRecords,
			T.*
		FROM(	
		SELECT C.UserId Id, C.*
		FROM 
		vw_contacts C JOIN dbo.SplitGUID(@Ids,',') T ON T.Items = C.UserId
		)T
		)
	Select * 
	FROM CTE
	WHERE RowNumber BETWEEN ((@PageNumber-1) * @PageSize)+1 AND ((@PageNumber) * @PageSize)
	ORDER BY RowNumber
	END
	ELSE
		SELECT C.UserId Id, C.*
		FROM 
		vw_contacts C JOIN dbo.SplitGUID(@Ids,',') T ON T.Items = C.UserId

END
ELSE IF (@CampaignRunId IS NULL OR @CampaignRunId = dbo.GetEmptyGuid())
	BEGIN
		DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	
		DECLARE @EmptyGuid uniqueidentifier
		SET @EmptyGuid = dbo.GetEmptyGUID()
	
		DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
		IF @SortBy IS NULL AND @Query IS NOT NULL
		BEGIN
			IF (@ContactListId IS NULL OR @ContactListId =  dbo.GetEmptyGuid())
			INSERT INTO @tbIds
			EXEC sp_executesql @Query
		END
		ELSE IF @Ids is NOT NULL
		BEGIN 
			INSERT INTO @tbIds
			Select Items,0 from dbo.SplitGUID(@Ids,',')
		END
		ELSE
		BEGIN
			-- if the request is to get the contacts for the given list then this temp table will be populated below
			IF (@ContactListId IS NULL OR @ContactListId =  dbo.GetEmptyGuid())
			BEGIN
				IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
				IF (@SortBy IS NOT NULL)
					SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)
			
				INSERT INTO @tbIds
				SELECT	Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'FirstName ASC' THEN FirstName END ASC,
							CASE WHEN @SortClause = 'FirstName DESC' THEN FirstName END DESC,
							CASE WHEN @SortClause = 'LastName ASC' THEN LastName END ASC,
							CASE WHEN @SortClause = 'LastName DESC' THEN LastName END DESC,
							CASE WHEN @SortClause = 'Email ASC' THEN Email END ASC,
							CASE WHEN @SortClause = 'Email DESC' THEN Email END DESC,
							CASE WHEN @SortClause = 'CreatedDate ASC' THEN CreatedDate END ASC,
							CASE WHEN @SortClause = 'CreatedDate DESC' THEN CreatedDate END DESC
						) AS RowNumber
				FROM	MKContact WHERE @Status is null or status = @Status
			END
		END
			
		DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
		--If the contact list id is passed then get the contacts(including the customers,etc) for the list
		IF (@ContactListId IS NOT NULL AND @ContactListId <>  dbo.GetEmptyGuid())
		BEGIN
			--Get all the contacts for the given contact list
			Declare @ListType int
			SELECT @ListType = ListType from TADistributionLists where Id=@ContactListId

			IF (@ListType = 0) --For manual
			BEGIN

			IF @Query IS NOT NULL
			BEGIN
			-- IMP NOTE: this will not work for getting contacts as it should be joined with vwcontacts rather than just MKContact. This has to be fixed at the framework level
				INSERT INTO @tbIds
				EXEC sp_executesql @Query
			END
			--ELSE
			--BEGIN
			--	-- if the request is to get the contacts for the given list then this temp table will be populated below
			--	--INSERT INTO @tbIds
			--	--SELECT UserId Id, 0 FROM vw_contacts
			
			--END

			;WITH CTE AS(
				SELECT ROW_NUMBER() OVER (ORDER BY DL.Id) As RowNumber,
					COUNT(C.UserId) OVER () AS TotalRecords,
					C.UserId AS Id			
				from TADistributionListUser DLU JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
				--JOIN @tbIds T ON T.Id = DLU.UserId 
				JOIN TADistributionLists DL ON DLU.DistributionListId = DL.Id
				WHERE DL.Id = @ContactListId AND DL.ListType = @ListType AND ContactListSubscriptionType != 3
			)
			INSERT INTO @tbPagedResults
			SELECT Id, RowNumber, TotalRecords FROM CTE
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
				
			END
			ELSE IF (@ListType = 1) --For auto list
			BEGIN
				Declare @queryXml xml,@TotalRecords int 
				SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@ContactListId 
					
					Declare @tblContacts Table
					(
						Id uniqueidentifier,
						UserId uniqueidentifier,
						FirstName varchar(1024) null,
						MiddleName varchar(1024) null,
						LastName varchar(1024) null,
						CompanyName varchar(1024) null,
						BirthDate datetime null,
						Gender varchar(1024) null,
						AddressId uniqueidentifier null,
						Status int null,
						HomePhone varchar(1024) null,
						MobilePhone varchar(1024) null,
						OtherPhone varchar(1024) null,
						ImageId uniqueidentifier null,
						Notes varchar(max) null,
						Email varchar(256),
						ContactType int,
						ContactSourceId int,
						TotalRecords int null
					)
				
				insert into @tblContacts(Id,UserId,FirstName,MiddleName,LastName,CompanyName,BirthDate,Gender,AddressId,Status,HomePhone,MobilePhone,OtherPhone,ImageId,Notes,
				Email,ContactType,ContactSourceId,TotalRecords)
				exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@SiteId, @MaxRecords=0, @ContactListId = @ContactListId,@TotalRecords=@TotalRecords output, @IncludeAddress = 0
				
				INSERT INTO @tbIds
				SELECT UserId Id, 0 FROM @tblContacts
				
				;WITH CTE AS(
				SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) As RowNumber,
					COUNT(T.Id) OVER () AS TotalRecords,T.Id AS Id			
				from @tbIds T
					)
					INSERT INTO @tbPagedResults
					SELECT Id, RowNumber, TotalRecords FROM CTE
					WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
						OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
						AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)

			END

			SELECT U.UserId Id, U.[FirstName], U.[MiddleName], U.[LastName], U.[CompanyName], U.[BirthDate], U.[Gender], U.[AddressId],   
                      [Status]/* if active return marketier status else return 2 inactive*/ , U.[HomePhone], U.[MobilePhone],   
                      U.[OtherPhone], U.[ImageId], U.[Notes], U.Email, 0 ContactType, ContactSourceId  ,
				CU.UserFullName AS CreatedByFullName,
				MU.UserFullName AS ModifiedByFullName,
				T.RowNumber,
				T.TotalRecords,
				CS.SiteId 
			from @tbPagedResults T JOIN vw_Contacts U ON U.UserId = T.Id
				JOIN vw_ContactSite CS on CS.UserId = U.UserId and CS.IsPrimary = 1
				LEFT JOIN VW_UserFullName CU on CU.UserId = U.CreatedBy
				LEFT JOIN VW_UserFullName MU on MU.UserId = U.ModifiedBy
			ORDER BY RowNumber
		END
		ELSE
		BEGIN --FOR GETTING CONTACTS
			;WITH CTE AS(
				SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) As RowNumber,
					COUNT(C.Id) OVER () AS TotalRecords,
					C.Id AS Id			
				FROM MKContact C JOIN MKContactSite SC ON SC.ContactId = C.Id
					 INNER JOIN @tbIds T ON T.Id = C.Id
				WHERE (@Id IS NULL OR C.Id = @Id)
					AND (@SiteId IS NULL OR SC.SiteId = @SiteId)
					AND (@Status IS NULL OR C.Status = @Status)
					AND (@LoweredEmail IS NULL OR LOWER(C.Email) = @LoweredEmail)
					AND (@FormId IS NULL OR @FormId = dbo.GetEmptyGuid() OR C.Id IN (
							SELECT	DISTINCT UserId
							FROM	FormsResponse
							WHERE	FormsId = @FormId
						))
			)
			INSERT INTO @tbPagedResults
			SELECT Id, RowNumber, TotalRecords FROM CTE
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
		

			SELECT C.Id,
				C.Email,
				C.FirstName,
				C.MiddleName,
				C.LastName,
				C.CompanyName,
				C.Gender,
				C.BirthDate,
				C.HomePhone,
				C.MobilePhone,
				C.OtherPhone,
				C.ImageId,
				C.AddressId,
				C.Notes,
				C.ContactSourceId,
				C.Status,
				C.CreatedBy,
				dbo.ConvertTimeFromUtc(C.CreatedDate, @SiteId) CreatedDate,
				C.ModifiedBy,
				dbo.ConvertTimeFromUtc(C.ModifiedDate, @SiteId) ModifiedDate,
				C.LastSynced,
				CU.UserFullName AS CreatedByFullName,
				MU.UserFullName AS ModifiedByFullName,
				T.RowNumber,
				T.TotalRecords,
				CS.SiteId
			FROM 
				MKContact C JOIN @tbPagedResults T ON T.Id = C.Id
				JOIN vw_ContactSite CS on CS.UserId = C.Id and CS.IsPrimary = 1
				LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
				LEFT JOIN VW_UserFullName MU on MU.UserId = C.ModifiedBy
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
			ORDER BY RowNumber

			SELECT A.*
			from vw_Address A
				JOIN MKContact C ON C.AddressId = A.Id
				Join @tbPagedResults T on T.Id = C.Id
		END

	END
	ELSE IF (@CampaignRunId IS NOT NULL AND @CampaignRunId <> dbo.GetEmptyGuid())
	BEGIN

		DECLARE @VerifyBatchCompletion BIT
		IF (@CampaignId IS NULL OR @CampaignId = dbo.GetEmptyGuid())
			SET @VerifyBatchCompletion = 1

		EXEC CampaignContact_Get @SiteId,@CampaignId,@CampaignRunId,@EmailPerCampaignLimit,@BatchSize,@VerifyBatchCompletion
	END
	

END



GO
PRINT 'Creating Procedure ContactDto_Save'
GO
IF (OBJECT_ID ('ContactDto_Save') IS NOT NULL)
	DROP PROCEDURE ContactDto_Save
GO
CREATE PROCEDURE [dbo].[ContactDto_Save]
(
	
	@Id				uniqueidentifier = null OUTPUT,
	@SiteId			uniqueidentifier ,
	@Email			nvarchar(max) = null,
	@FirstName		nvarchar(max) = null,
	@MiddleName		nvarchar(max) = null,
	@LastName		nvarchar(max) = null,
	@CompanyName	nvarchar(max) = null,
	@Gender			nvarchar(max) = null,
	@BirthDate		datetime = null,
	@HomePhone		nvarchar(max) = null,
	@MobilePhone	nvarchar(max) = null,
	@OtherPhone		nvarchar(max) = null,
	@ImageId		uniqueidentifier,
	@Notes			nvarchar(max) = null,
	@ContactSourceId int = null,
	@Status			int = NULL,
	@LastSynced		datetime =null,
	@ModifiedBy		uniqueidentifier = null,
	@ChangedLogXml	xml = null,
	@Address		xml = null
)
AS
BEGIN
	DECLARE @UtcNow datetime, @IsNewUser bit
	SET @UtcNow = GETUTCDATE()
	
	IF (@ChangedLogXml IS NULL)
		SET @IsNewUser = 1
	ELSE
		SET @IsNewUser = 0

	IF ((@Id IS NULL OR @Id = dbo.GetEmptyGUID() ) AND  @IsNewUser = 1)
	BEGIN
		SET @Id = NEWID()
		
		IF EXISTS (SELECT 1 FROM MKContact Where Email = @Email )
		BEGIN
			 RAISERROR('EXISTS||%s', 16, 1, 'Email')
             RETURN dbo.GetBusinessRuleErrorCode()
		END

		INSERT INTO MKContact
		(
			Id,
			Email,
			FirstName,
			MiddleName,
			LastName,
			CompanyName,
			Gender,
			BirthDate,
			HomePhone,
			MobilePhone,
			OtherPhone,
			ImageId,
			Notes,
			ContactSourceId,
			Status,
			LastSynced,
			CreatedBy,
			CreatedDate,
			ModifiedDate
		)
		VALUES
		(
			@Id,
			@Email,
			@FirstName,
			@MiddleName,
			@LastName,
			@CompanyName,
			@Gender,
			@BirthDate,
			@HomePhone,
			@MobilePhone,
			@OtherPhone,
			@ImageId,
			@Notes,
			@ContactSourceId,
			@Status,
			@LastSynced,
			@ModifiedBy,
			@UtcNow,
			@UtcNow
		)

		--Update the MKContactSite
		INSERT INTO MKContactSite
		(
			ContactId,
			SiteId,
			Status
		)
		VALUES
		(
			@Id,
			@SiteId,
			@Status
		)

	END
	ELSE
	BEGIN

		IF(@IsNewUser = 0 AND @Id IS NULL )
			SELECT TOP 1 @Id =  Id FROM MKContact WHERE LOWER(LTRIM(RTRIM(Email))) = LOWER(LTRIM(RTRIM(@Email)))

		UPDATE MKContact
		SET 
			Email = ISNULL(@Email,Email),
			FirstName = ISNULL(@FirstName,FirstName),
			MiddleName = ISNULL(@MiddleName,MiddleName),
			LastName = ISNULL(@LastName,LastName),
			CompanyName = ISNULL(@CompanyName,CompanyName),
			Gender = ISNULL(@Gender,Gender),
			BirthDate = ISNULL(@BirthDate,BirthDate),
			HomePhone = ISNULL(@HomePhone,HomePhone),
			MobilePhone = ISNULL(@MobilePhone,MobilePhone),
			OtherPhone = ISNULL(@OtherPhone,OtherPhone),
			ImageId = ISNULL(@ImageId,ImageId),
			Notes = ISNULL(@Notes,Notes),
			ContactSourceId = ISNULL(@ContactSourceId,ContactSourceId),
			Status = ISNULL(@Status,Status),
			LastSynced = @LastSynced,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = case when @LastSynced>LastSynced then isnull(ModifiedDate,CreatedDate) else @UtcNow end 
		WHERE Id = @Id 

		--Updates the MKContactNotes table with the changed log
		INSERT INTO MKContactNotes 
		(
			Id,
			ContactId,
			changelog,
			[Version],
			CreatedDate, 
			CreatedBy
		)
		VALUES
		( 
			NEWID(), 
			@Id, 
			ISNULL(@ChangedLogXml,''),
			(SELECT ISNULL(MAX([Version]), 0) + 1 FROM MKContactNotes WHERE ContactId=@Id),
			@UtcNow,
			@ModifiedBy
		)

		IF (@Address IS NOT NULL)
		BEGIN
			DECLARE @AddressId uniqueidentifier
			EXEC [dbo].[AddressDto_Save] 
				@Id = @AddressId OUTPUT,
				@Address = @Address,
				@ModifiedBy = @ModifiedBy

			UPDATE MKContact SET AddressId = @AddressId WHERE Id = @Id
		END
	END
END
GO
PRINT 'Creating Procedure ContactFilterDto_Get'
GO
IF (OBJECT_ID ('ContactFilterDto_Get') IS NOT NULL)
	DROP PROCEDURE ContactFilterDto_Get
GO

CREATE PROCEDURE [dbo].[ContactFilterDto_Get]
(
	@Id uniqueidentifier = NULL,
	@SiteId uniqueidentifier ,
	@PageNumber  int = null,
	@PageSize int = null,
	@Status int = null,
	@MaxRecords		int = NULL,
	@Query			nvarchar(max) = NULL 

)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT Id, 0 FROM MKContactFilter 
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
			
	DECLARE @tbPagedResults TABLE(Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
			COUNT(CF.Id) OVER () AS TotalRecords,
			CF.Id AS Id
		FROM MKContactFilter CF
			JOIN @tbIds T ON CF.Id = T.Id
		WHERE (@Id IS NULL OR CF.Id = @Id)
			AND (@SiteId IS NULL OR CF.ApplicationId = @SiteId)
			
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT CF.Id,
		CF.FilterType, 
		CF.ApplicationId,
		CF.Title,
		SQ.Status,
		SQ.CreatedDate,
		SQ.ModifiedDate, 
		SQ.SearchXml,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		T.RowNumber,
		T.TotalRecords
	FROM MKContactFilter CF JOIN CTSearchQuery SQ ON SQ.Id = CF.Id
		JOIN @tbPagedResults T ON T.Id = CF.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = SQ.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = SQ.ModifiedBy
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	ORDER BY RowNumber
END


GO
PRINT 'Creating Procedure ContactFilterDto_Save'
GO
IF (OBJECT_ID ('ContactFilterDto_Save') IS NOT NULL)
	DROP PROCEDURE ContactFilterDto_Save
GO

CREATE PROCEDURE [dbo].[ContactFilterDto_Save]
(
	@Id UNIQUEIDENTIFIER = NULL OUTPUT,
	@SiteId uniqueidentifier,
	@Title NVARCHAR(MAX) = NULL,
	@FilterType INT = NULL,
	@SearchXml xml = NULL,
	@Status INT  = null,
	@ModifiedBy uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO MKContactFilter
		(
			Id,
			ApplicationId,
			Title,
			FilterType			
		)
		VALUES
		(
			@Id,
			@SiteId,
			@Title,
			@FilterType
		)

		IF (@SearchXml IS NOT NULL)
		INSERT INTO CTSearchQuery
		(
			Id,
			SearchXml,
			Status,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate
		)
		VALUES
		(
			@Id,
			@SearchXml,
			ISNULL(@Status,1),
			@ModifiedBy,
			@UtcNow,
			NULL,
			NULL
		)

	END
	ELSE
	BEGIN
		UPDATE MKContactFilter 
		SET Title = ISNULL(@Title,Title),
		FilterType=ISNULL(@FilterType,FilterType)
		WHERE Id = @Id

		UPDATE CTSearchQuery
		SET SearchXml = ISNULL(@SearchXml,SearchXml),
		Status = ISNULL(@Status, Status),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @UtcNow
		WHERE Id = @Id 
	END
END

GO
PRINT 'Creating procedure ContactFilterDto_Delete'
GO
IF (OBJECT_ID('ContactFilterDto_Delete') IS NOT NULL)
	DROP PROCEDURE ContactFilterDto_Delete
GO
CREATE PROCEDURE [dbo].[ContactFilterDto_Delete]
(
	@Id uniqueidentifier
)
AS
BEGIN
	DELETE FROM MKContactFilter where Id=@Id 
END	

GO
PRINT 'Creating procedure CampaignListDto_Get'
GO
IF (OBJECT_ID('CampaignListDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignListDto_Get
GO

IF (OBJECT_ID('CampaignDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignDto_Get]  
(  
	@Id UNIQUEIDENTIFIER = NULL,  
	@SiteId UNIQUEIDENTIFIER = NULL,
	@IncludeVariantSites BIT = 0,
	@Status	INT = NULL,
	@Type INT = NULL,
	@CampaignGroupId UNIQUEIDENTIFIER = NULL,
	@AwaitingSendOnly BIT = 0,
	@PageSize INT = NULL,   
	@PageNumber INT = NULL ,
	@MaxRecords	INT = NULL,
	@Query NVARCHAR(MAX) = NULL,
	@IsRun BIT = 0,
	@IgnoreDetails BIT = NULL
)  
AS  
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	
	IF @Query IS NULL
	BEGIN
		IF(@IsRun = 0)
			INSERT INTO @tbIds
			SELECT Id, 0 FROM MKCampaign
		ELSE
			INSERT INTO @tbIds
			SELECT Distinct M.Id, 0 FROM MKCampaign M
			INNER JOIN MKCampaignRunHistory MR ON M.Id = MR.CampaignId
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
	
	
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()
			
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT	ROW_NUMBER() OVER (	
					ORDER BY T.RowNumber	
				) AS RowNumber, COUNT(C.Id) OVER () AS TotalRecords,
				C.Id AS Id			
		FROM	MKCampaign AS C
				LEFT JOIN MKCampaignAdditionalInfo AS A ON C.Id = A.CampaignId
				LEFT JOIN TASchedule AS S ON S.Id = A.ScheduleId
				JOIN @tbIds AS T ON T.Id = C.Id
		WHERE	(@Id IS NULL OR C.Id = @Id) AND
				(@Status IS NULL OR C.Status = @Status) AND
				(@CampaignGroupId IS NULL OR @CampaignGroupId ='00000000-0000-0000-0000-000000000000' OR C.CampaignGroupId = @CampaignGroupId) AND
				(@SiteId IS NULL OR 
					((@IncludeVariantSites = 0 AND C.ApplicationId = @SiteId) OR 
					(@IncludeVariantSites = 1 AND C.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId))))) AND
				(@Type IS NULL OR C.Type = @Type) AND 
				(@AwaitingSendOnly = 0 OR (
					C.Status = 2 AND
					S.NextRunTime < GETUTCDATE() AND
					(ISNULL(S.MaxOccurrence, 0) <= 0 OR S.RunCount < S.MaxOccurrence)) OR
				 C.Status = 7 or C.Status=5)
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT C.ApplicationId, 
	       C.CampaignGroupId,
	       C.ConfirmationEmail,
	       C.CreatedBy,
	       dbo.ConvertTimeFromUtc(C.CreatedDate, @SiteId)CreatedDate,	      
	       C.Description,
		   C.Id,	       
	       C.ModifiedBy,
	       dbo.ConvertTimeFromUtc(C.ModifiedDate, @SiteId)ModifiedDate,
	       C.SenderEmail,
	       C.SenderEmailProperty,
	       C.SenderName,
	       C.SenderNameProperty,
	       C.Status,
	       C.Title,
	       C.Type,	 
		   A.UseLocalTimeZoneToSend,       
	       A.AuthorId,
	       A.AutoUpdateLists,
	       A.CampaignId,
	       A.CostPerEmail,	      
	       dbo.ConvertTimeFromUtc(A.LastPublishDate, @SiteId)LastPublishDate,
	       A.LastPublishDate,
	       A.ScheduleId,
	       A.SendToNotTriggered,
	       A.UniqueRecipientsOnly,
	       A.WorkflowId,
	       A.WorkflowStatus,
		   T.TotalRecords
	FROM MKCampaign C
		JOIN @tbPagedResults T ON T.Id = C.Id
		LEFT JOIN MKCampaignAdditionalInfo A on A.CampaignId = C.Id
	ORDER BY T.RowNumber
	
	SELECT E.[Id]
      ,[CampaignId]
      ,[EmailSubject]
      ,CASE WHEN @IgnoreDetails = 1 THEN '' ELSE [EmailHtml] END AS EmailHtml
      ,[CMSPageId]
      ,[EmailText]
      ,[Sequence]
      ,[TimeValue]
      ,[TimeMeasurement]
      ,[EmailHtmlPageVersion]
      ,[PageMapNodeId]
      ,[EmailContentType]
      ,[EmailContentTypePriority]
  FROM [MKCampaignEmail] E
	JOIN @tbPagedResults T ON T.Id = E.CampaignId
		
--	--Getting the contact count for each campaign
	Declare @tabCampaignListCount as table (CampaignId uniqueidentifier,ContactListId uniqueidentifier,ContactListTitle nvarchar(max),ContactCount int,RowNum int)

	INSERT INTO @tabCampaignListCount(CampaignId,ContactListId,ContactListTitle,ContactCount,RowNum)
	Select R.Id,CL.DistributionLIstId,D.Title,ISNULL(DC.Count,0),ROW_NUMBER() over(PARTITION BY R.Id,CL.DistributionListId ORDER BY CL.DistributionListId ASC)
	FROM @tbPagedResults R
	INNER JOIN (Select AI.CampaignId,CDL.DistributionListId FROM
				MKCampaignAdditionalInfo AI 
				INNER JOIN MKCampaignDistributionListDraft CDL ON (CDL.CampaignId = AI.CampaignId AND AI.WorkflowStatus =1)
				UNION ALL
				Select AJ.CampaignId,CPL.DistributionListId
				FROM MKCampaignAdditionalInfo AJ 
				LEFT JOIN MKCampaignDistributionListDraft CPL ON (CPL.CampaignId = AJ.CampaignId AND AJ.WorkflowStatus <>1)) CL
		ON CL.CampaignId = R.Id
		INNER JOIN TADistributionLists D on D.Id = CL.DistributionListId
		LEFT JOIN TADistributionListSite DC ON DC.DistributionListid =CL.DistributionListId AND (@SiteId IS NULL OR 
					((@IncludeVariantSites = 0 AND DC.SiteId = @SiteId) OR 
					(@IncludeVariantSites = 1 AND DC.SiteId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId)))))


	SELECT CampaignId,sum(ContactCount) ContactCount FROM @tabCampaignListCount
	Group By CampaignId
	
	SELECT * FROM @tabCampaignListCount

END
GO

PRINT 'Creating procedure ContactDto_RebuildContactData'
GO
IF (OBJECT_ID('ContactDto_RebuildContactData') IS NOT NULL)
	DROP PROCEDURE ContactDto_RebuildContactData
GO

CREATE PROCEDURE [dbo].[ContactDto_RebuildContactData]
AS 
    BEGIN
        BEGIN TRAN T1 ; 
        DECLARE @NewTable NVARCHAR(600)
        SET @NewTable = 'tblContactPropertiesAndAttributes'
            + REPLACE(CAST(NEWID() AS NVARCHAR(40)), '-', '')
        DECLARE @Sql NVARCHAR(MAX)

        SET @Sql = 'SELECT *, GetUtcDate() as CachedDate INTO ' + @NewTable
            + ' FROM dbo.vwContactPropertiesAndAttributes'
        PRINT @Sql
        EXEC (  @Sql)
      
        SET @Sql = 'CREATE CLUSTERED INDEX [IX_tblContactPropertiesAndAttributes_AttributeId] ON '
            + @NewTable
            + '
        (
        [AttributeId] ASC
        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]'
        PRINT @Sql
        EXEC (@Sql)
		
        DECLARE @BackupTable NVARCHAR(500)
        SET @BackupTable = @NewTable + '_BK'
        EXEC sp_rename 'tblContactPropertiesAndAttributes', @BackupTable
        EXEC sp_rename @NewTable, 'tblContactPropertiesAndAttributes' ;
        COMMIT TRAN T1 ;
        EXEC ('DROP TABLE '+@BackupTable)



    END
GO
PRINT 'Creating procedure CampaignListGroupDto_Get'
GO
IF (OBJECT_ID('CampaignListGroupDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignListGroupDto_Get
GO



IF (OBJECT_ID('CampaignGroupDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignGroupDto_Get
GO
CREATE PROCEDURE [dbo].[CampaignGroupDto_Get]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@Status			int = NULL ,
	@SiteId			uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,	
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL,
	@Query	NVARCHAR(MAX) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	DECLARE @tbCount TABLE(Id uniqueidentifier, [Count] int)
    INSERT INTO @tbCount
    SELECT CampaignGroupId, COUNT(*) FROM MKCampaign  
    WHERE ApplicationId = @SiteId
    GROUP BY CampaignGroupId 
    
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()
			
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
		IF (@SortBy IS NOT NULL)
			SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	

		INSERT INTO @tbIds
		SELECT Id,  ROW_NUMBER() OVER (ORDER BY
			CASE WHEN @SortClause = 'Name ASC' THEN Name END ASC,
			CASE WHEN @SortClause = 'Name DESC' THEN Name END DESC,
			CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
		) AS RowNumber
		FROM MKCampaignGroup
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
			
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
			COUNT(MKG.Id) OVER () AS TotalRecords,
			MKG.Id AS Id			
		FROM MKCampaignGroup MKG
			JOIN @tbIds T ON MKG.Id = T.Id
		WHERE (@Id IS NULL OR MKG.Id = @Id)
			AND (@SiteId IS NULL OR MKG.ApplicationId = @SiteId)			
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT MKG.ApplicationId,
	       MKG.Id,
	       MKG.Name,	      
	       MKG.CreatedBy,
	       dbo.ConvertTimeFromUtc(MKG.CreatedDate, @SiteId)CreatedDate,
	       MKG.ModifiedBy,
	       dbo.ConvertTimeFromUtc(MKG.ModifiedDate, @SiteId)ModifiedDate,
	       C.[Count]	       		
	FROM MKCampaignGroup MKG
		JOIN @tbPagedResults T ON T.Id = MKG.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = MKG.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = MKG.ModifiedBy
		LEFT JOIN @tbCount C ON T.Id = C.Id
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
		AND MKG.ApplicationId = @SiteId
	ORDER BY RowNumber,Name
		
END

GO
PRINT 'Creating procedure CampaignContact_FillResponse'
GO
IF (OBJECT_ID('CampaignContact_FillResponse') IS NOT NULL)
	DROP PROCEDURE CampaignContact_FillResponse
GO
CREATE PROCEDURE [dbo].[CampaignContact_FillResponse]
	(
	@SiteId Uniqueidentifier,
	@CampaignId Uniqueidentifier,
	@CampaignRunId Uniqueidentifier, --response id
	@CampaignEmailId Uniqueidentifier
	)
AS
begin
	declare @ResponseProviderName varchar(250) 
	declare @ResponseOptions xml 
	
	DECLARE @CanPopulateWorkTable INT
		EXEC CampaignContact_VerifyFillable @CampaignId,@CampaignRunId, @CanPopulateWorkTable OUTPUT

	IF (@CanPopulateWorkTable = 0)
		RETURN;

	select @ResponseProviderName = ResponseProviderName, @ResponseOptions= ResponseProviderOptions  from MKCampaignResponseInfo where CampaignId=@CampaignId
	--declare @ResponseSPName varchar(250)
	--if(@ResponseProviderName ='LoginResponseProvider')
	--	set @ResponseSPName='Response_PopulateLoggedIn'
	--else if(@ResponseProviderName ='TriggeredWatchResponseProvider')
	--	set @ResponseSPName='Response_PopulateTriggeredWatches'
	--else if(@ResponseProviderName ='ProductPurchaseResponseProvider')
	--	set @ResponseSPName='Response_PopulatePurchasedProducts'
	--else if(@ResponseProviderName ='SubmittedFormResponseProvider')
	--	set @ResponseSPName='Response_PopulateFormsSubmitted'

	--Populate the response worktable from which the email processor worktable will be populated.
	if(@ResponseProviderName ='LoginResponseProvider')
		exec Response_PopulateLoggedIn @CampaignId,@ResponseOptions
	else if(@ResponseProviderName ='TriggeredWatchResponseProvider')
		exec Response_PopulateTriggeredWatches @CampaignId,@ResponseOptions
	else if(@ResponseProviderName ='ProductPurchaseResponseProvider')
		exec Response_PopulatePurchasedProducts @CampaignId,@ResponseOptions
	else if(@ResponseProviderName ='SubmittedFormResponseProvider')
		exec Response_PopulateFormsSubmitted @CampaignId,@ResponseOptions

    
	--Populate the email processor work table for sending emails
	insert into MKCampaignRunWorkTable (CampaignRunId,UserId,FirstName,MiddleName,LastName,CompanyName,BirthDate,Gender,AddressId,Status,HomePhone,
			MobilePhone,OtherPhone,Notes,Email,ContactType,ProcessingCount,ProcessingStatus,LogMessage)
	select  @CampaignRunId, C.UserId, C.FirstName,C.MiddleName,C.LastName,C.CompanyName , C.BirthDate, C.Gender, C.AddressId , C.Status , C.HomePhone, C.MobilePhone, C.OtherPhone,C.Notes, C.Email,C.ContactType ,0,0,''   
	from MKResponseFlow rf join vw_contacts c on c.UserId = rf.UserId
	Where rf.ResponseId=@CampaignId and rf.Completed is null and ([dbo].[Campaign_GetResponseSendDate](rf.DateStarted, @CampaignRunId) <= GETUTCDATE() and (rf.LastSent is null or [dbo].[Campaign_GetResponseSendDate](rf.DateStarted, @CampaignRunId) > rf.LastSent))

	EXCEPT

	select  @CampaignRunId, UserId, FirstName,MiddleName,LastName,CompanyName , BirthDate, Gender, AddressId , Status , HomePhone, MobilePhone, OtherPhone,Notes, Email,ContactType ,0,0,''   
	FROM MKCampaignRunWorkTable
	WHERE CampaignRunId = @CampaignRunId 


end


GO
PRINT 'Creating procedure CampaignContact_Fill'
GO
IF (OBJECT_ID('CampaignContact_Fill') IS NOT NULL)
	DROP PROCEDURE CampaignContact_Fill
GO
CREATE PROCEDURE [dbo].[CampaignContact_Fill]
(
	@SiteId uniqueidentifier,
	@CampaignId uniqueidentifier,
	@CampaignRunId uniqueidentifier,
	@EmailPerCampaignLimit INT
)
AS
BEGIN
	Declare
		@UniqueRecipientsOnly bit,
		@AutoUpdateLists bit,
		@LastPublishDate datetime,
		@listRecs int,
		@totalRecs int,
		@listId uniqueidentifier,
		@GetCurrentRecipients bit,
		@SendToNotTriggered bit--,
		--@ApplicationId uniqueidentifier

		DECLARE @CanPopulateWorkTable INT
		EXEC CampaignContact_VerifyFillable @CampaignId,@CampaignRunId, @CanPopulateWorkTable OUTPUT

		IF (@CanPopulateWorkTable = 0)
		BEGIN
			PRINT 'CampaignContact_Fill No valid campaign or response to fill contacts'
			RETURN;
		END

		Declare @TabProcessableSites table(SiteId uniqueidentifier, Id int identity(1,1))
		Declare @CurrentUtcDateTime datetime, @UseLocalTimeZoneProcessing BIT
		SET @CurrentUtcDateTime = GetUtcDate()
		SET @UseLocalTimeZoneProcessing = 0

		IF EXISTS (SELECT 1 FROM MKCampaignAdditionalInfo Where UseLocalTimeZoneToSend = 1 AND CampaignId=@CampaignId)
		BEGIN
			PRINT 'Timezone is set to 1 for campaign '
			SET @UseLocalTimeZoneProcessing = 1
		END

		IF (@UseLocalTimeZoneProcessing = 1)
		BEGIN
			PRINT 'Gettings sites from timezone table'
		--Get the list of processable sites so that only contact belonging to those sites will be retrived
		insert into @TabProcessableSites (SiteId)
		Select SiteId 
		From MKCampaignRunTimeZoneSites
			Where CampaignRunId = @CampaignRunId AND StartTime <= @CurrentUtcDateTime AND IsProcessed = 0

			--Updating the status to IsProcessed for those sites in the timezone table
			Update MKCampaignRunTimeZoneSites SET IsProcessed = 1 WHERE SiteId in (SELECT SiteId from @TabProcessableSites) AND CampaignRunId=@CampaignRunId
		 END

	print 'getting contacts for sites'
	Declare @tblContacts Table
	(
		Id uniqueidentifier,
		UserId uniqueidentifier,
		FirstName varchar(1024) null,
		MiddleName varchar(1024) null,
		LastName varchar(1024) null,
		CompanyName varchar(1024) null,
		BirthDate datetime null,
		Gender varchar(1024) null,
		AddressId uniqueidentifier null,
		Status int null,
		HomePhone varchar(1024) null,
		MobilePhone varchar(1024) null,
		OtherPhone varchar(1024) null,
		ImageId uniqueidentifier null,
		Notes varchar(max) null,
		Email varchar(256),
		ContactType int,
		ContactSourceId int,
		TotalRecords INT null
	)


	--Populate the worktable
	Set @GetCurrentRecipients = 1

		--If the campaign is shared and to be sent to contacts of a variant site, then this should be changed to resolve the applications
		--select @ApplicationId = ApplicationId from mkcampaign Where Id = @CampaignId

		-- get the options for list selection
		select top 1 @UniqueRecipientsOnly = UniqueRecipientsOnly,
			@AutoUpdateLists = AutoUpdateLists, @LastPublishDate = IsNull(LastPublishDate, '2009-01-01'),
			@SendToNotTriggered = SendToNotTriggered
		from MKCampaignAdditionalInfo
		Where CampaignId = @CampaignId

		if @AutoUpdateLists = 1
		Begin
			--TODO: For the timezone specific site we need to verify whether any sent data is available.
			if exists (select top 1 * From MKEmailSendLog Where CampaignId = @CampaignId And SendDate > @LastPublishDate)
				set @GetCurrentRecipients = 0
		End


		if @GetCurrentRecipients = 1
		Begin
			--Get the distribution lists
			-- either the campaign always updates or this is the first time

			--Get the contacts for Manual Lists
			INSERT INTO  @tblContacts 
			select C.[UserId] Id
				,C.[UserId]
				,C.[FirstName]
				,C.[MiddleName]
				,C.[LastName]
				,C.[CompanyName]
				,dbo.ConvertTimeFromUtc(BirthDate, @SiteId)BirthDate 
				,C.[Gender]
				,C.[AddressId]
				,C.[Status]
				,C.[HomePhone]
				,C.[MobilePhone]
				,C.[OtherPhone]
				,C.ImageId 
				,C.[Notes]
				,C.[Email]
				,C.[ContactType]
				,C.[ContactSourceId]
				,0  
			from MKCampaignDistributionList CDL JOIN TADistributionListUser DLU ON CDL.DistributionListId=DLU.DistributionListId and ContactListSubscriptionType != 3
			JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
			JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id
			JOIN vw_contactsite CS ON CS.UserId = C.UserId
			WHERE CDL.CampaignId = @CampaignId AND DL.ListType = 0
			AND (@UseLocalTimeZoneProcessing = 0 OR CS.SiteId in (select SiteId from  @TabProcessableSites))
			--TODO: Check for primary site??

			--Get all the contacts for AUTO LIST
			declare @autoListIds table (Id uniqueidentifier, RowNum int)

			insert into @autoListIds
			select DistributionListId,
			ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
			From MKCampaignDistributionList CDL JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			Where CampaignId = @CampaignId  AND DL.ListType = 1 --needs to be verified.
		
			declare @cnt int,  @TempId uniqueidentifier
			select @cnt = MAX(RowNum) from @autoListIds
		 
			declare @ProcessableSiteCount int, @LocalSiteCounter int,@LocalSiteId uniqueidentifier
			select @ProcessableSiteCount=Count(1) from @TabProcessableSites


			while (@cnt >0)
			begin
					
				SELECT @TempId = Id FROM @autoListIds WHERE RowNum = @cnt

				Declare @queryXml xml,@TotalRecords int 
				SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@TempId 
				
				IF (@UseLocalTimeZoneProcessing = 1 )
				BEGIN
				set @LocalSiteCounter = @ProcessableSiteCount
				while (@LocalSiteCounter > 0)
				begin
					print 'getting contacts for dynamic list with timezone'
					--Get the site id to process for dynamic list
					select @LocalSiteId = SiteId from @TabProcessableSites where @LocalSiteCounter = Id

					insert into @tblContacts
					exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@LocalSiteId, @MaxRecords=0, @ContactListId = @TempId, @TotalRecords=@TotalRecords output,  @IncludeAddress = 0

					Set @LocalSiteCounter = @LocalSiteCounter - 1
				end
				END
				ELSE
				BEGIN
						print 'getting contacts for dynamic list without timezone'

						insert into @tblContacts
						exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@SiteId, @MaxRecords=0, @ContactListId = @TempId, @TotalRecords=@TotalRecords output,  @IncludeAddress = 0
				END

				SET @cnt = @cnt - 1
			end
		
			print 'getting contacts that are manually added to campaign'
			-- Get all the contacts that are manually added to the campaign
			insert @tblContacts
			(Id
				,UserId
				,FirstName
				,MiddleName
				,LastName
				,CompanyName
				,BirthDate
				,Gender
				,AddressId
				,Status
				,HomePhone
				,MobilePhone
				,OtherPhone
				,ImageId
				,Notes
				,Email
				,ContactType
				,ContactSourceId)
			select NewId(), c.UserId, c.FirstName, c.MiddleName, c.LastName, c.CompanyName, c.BirthDate,
				c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone, c.OtherPhone, c.ImageId, c.Notes, 
				c.Email, c.ContactType,c.ContactSourceId
			from vw_contacts c
				join MKCampaignUser cu on cu.UserId = c.UserId and cu.CampaignId = @CampaignId
				join vw_contactsite CS ON CS.UserId = C.UserId
				WHERE (@UseLocalTimeZoneProcessing = 0 OR CS.SiteId in (select SiteId from @TabProcessableSites))


		end
		else
		begin
			insert @tblContacts
			(Id
				,UserId
				,FirstName
				,MiddleName
				,LastName
				,CompanyName
				,BirthDate
				,Gender
				,AddressId
				,Status
				,HomePhone
				,MobilePhone
				,OtherPhone
				,ImageId
				,Notes
				,Email
				,ContactType
				,ContactSourceId)
			select NewId(), c.UserId, c.FirstName, c.MiddleName, c.LastName, c.CompanyName, c.BirthDate,
				c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone, c.OtherPhone,C.ImageId, c.Notes, 
				c.Email, c.ContactType,C.ContactSourceId
			from vw_contacts c
				join vw_contactsite CS ON CS.UserId = C.UserId
				join mkEmailSendLog sl on sl.UserId = c.UserId 
					and sl.CampaignId = @CampaignId
					and sl.SendDate > @LastPublishDate
				where (@UseLocalTimeZoneProcessing = 0 OR CS.SiteId in (select SiteId from @TabProcessableSites))
		end


		-- remove users that have triggered watches
		if IsNull(@SendToNotTriggered, 0) = 1
		Begin
			Delete @tblContacts where UserId in(
				select distinct l.UserId
				from mkemailsendactions a
					join mkemailsendlog l on a.SendId = l.Id
					join mkcampaignrelevantwatches w on w.CampaignId = l.CampaignId and w.WatchId = a.TargetId
				where a.actiontype = 5
					and l.CampaignId = @CampaignId)
		End

		print 'populating the worktable'

		--populates the worktable
		insert into MKCampaignRunWorkTable (CampaignRunId, UserId, FirstName, MiddleName, LastName,
			CompanyName, BirthDate, Gender, AddressId, Status, HomePhone, MobilePhone, OtherPhone,
			Notes, Email, ContactType)
		SELECT distinct TOP(@EmailPerCampaignLimit) @CampaignRunId, c.UserId, isnull(c.FirstName, ''), isnull(c.MiddleName, ''), isnull(c.LastName, ''),
			isnull(c.CompanyName, ''), isnull(c.BirthDate, ''), isnull(c.Gender, ''), c.AddressId, c.Status, isnull(c.HomePhone, ''), 
			isnull(c.MobilePhone, ''), 
			isnull(c.OtherPhone, ''), isnull(c.Notes, ''), isnull(c.Email, ''), c.ContactType
		from @tblContacts c
			-- get previous send record for the campaign
			left outer join MKEmailSendLog l on l.UserId = c.UserId and l.CampaignId = @CampaignId
			-- is user unsubscribed
			left outer join USUserUnsubscribe u on u.userid = c.userid and isnull(u.CampaignId, @CampaignId) = @CampaignId
			left outer join USResubscribe AS R ON R.UserID = c.UserId and r.CampaignId = @CampaignId
		where (u.Id is null or R.Id IS NOT NULL)  -- removes users that are either unsubscribed from this campaign or all campaigns
			and c.Status = 1
			and 
			(
				(@UniqueRecipientsOnly = 0)
				Or
				(@UniqueRecipientsOnly = 1 and l.Id is null) -- removes users that have received this campaign previously
			)
			and C.UserId not in (
			select  UserId
			from MKCampaignRunWorkTable Where CampaignRunId=@CampaignRunId)
			--Remove contacts that are already sent for the campaignRunId in case of partiallysent campaigns
			Delete from MKCampaignRunWorkTable where UserId in(select UserId from MKEmailSendLog  where CampaignRunId=@CampaignRunId)
			
		--select count(*) 'TotalContacts' from MKCampaignRunWorkTable where CampaignRunId = @CampaignRunId
END


GO
PRINT 'Creating procedure CampaignRunHistoryDto_UpdateResponse'
GO
IF (OBJECT_ID('CampaignRunHistoryDto_UpdateResponse') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_UpdateResponse
GO
CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_UpdateResponse]
	(
		@Id uniqueidentifier --CampaignRunId
		--This is dependent on MKCampaignRunResponseWorktable. So the response status should be updated on this worktable
	)
AS
BEGIN
	
	Declare @CurrentTimeUtc datetime
	set @CurrentTimeUtc = GetUtcDate()

	;With ResponseCTE (StatusToUpdate,ProcessingCountToUpdate,ContactId,LogMessage)
	As
	(
		SELECT CASE WHEN ResponseStatus = 3 AND C.ProcessingCount<3   THEN 0
		 WHEN ResponseStatus = 2 OR ResponseStatus = 4 THEN ResponseStatus
		 WHEN  C.ProcessingCount>=3  THEN ResponseStatus END StatusToUpdate,
		0,
		 -- CASE WHEN ResponseStatus = 3 AND C.ProcessingCount<3   THEN C.ProcessingCount+1
		 --WHEN ResponseStatus = 2  THEN C.ProcessingCount END  ProcessingCountToUpdate,
		WT.ContactId,
		WT.LogMessage 
		FROM MKCampaignRunResponseWorktable WT JOIN MKCampaignRunWorkTable C ON C.CampaignRunId=WT.CampaignRunId
		WHERE WT.CampaignRunId = @Id

	)
	UPDATE C 
	SET ProcessingStatus = StatusToUpdate,
	C.LogMessage = R.LogMessage
	--ProcessingCount = ProcessingCountToUpdate
	FROM MKCampaignRunWorkTable C JOIN ResponseCTE R ON R.ContactId = C.UserId

	--Updates the run history with sends count
	;With RunHistory_Cte(Sends, CampaignRunId)
	AS
	(
		--Update the campaign run history send count
		SELECT COUNT(ContactId),CampaignRunId FROM MKCampaignRunResponseWorktable 
		Where ResponseStatus = 2 --success response
		Group By CampaignRunId	
	)
	Update H SET H.Sends= H.Sends+C.Sends 
	FROM MKCampaignRunHistory H JOIN RunHistory_Cte C ON H.Id = C.CampaignRunId

	
	--Get the campaign id for the run id
	declare @CampaignId uniqueidentifier ,@type int

	--Get the campaignid for the campaign run id if the type if reponse.
	select top 1 @type = C.Type,@CampaignId = C.Id from MKCampaign C Join MKCampaignRunHistory  CH On C.Id = CH.CampaignId Where CH.Id = @Id

	if(@type = 2) --response
	begin
		--for successful sends and hard error update the last sent date on MKResponseFlow table
		update MKResponseFlow set LastSent = @CurrentTimeUtc,
		Completed = case when dbo.Campaign_VerifyAutoReponsesCompleted(ResponseId,UserId,@CurrentTimeUtc) = 1 then @CurrentTimeUtc  else Completed end
		where UserId in (select UserId from MKCampaignRunWorkTable where CampaignRunId = @Id and (ProcessingStatus = 2 or ProcessingStatus = 4 OR ProcessingCount = 3)) and ResponseId = @CampaignId and Completed is null

	end

	--Update the errored records from the MKCampaignRunWorkTable to MKCampaignRunErrorLog table; success emails are already logged by different process
	INSERT INTO MKCampaignRunErrorLog (Id,
	CampaignId,
	CampaignRunId,
	ContactId,
	ErrorMessage)
	SELECT NEWID(),@CampaignId,@Id,UserId,LogMessage FROM MKCampaignRunWorkTable WHERE CampaignRunId=@Id AND (ProcessingStatus = 4 OR ProcessingCount = 3)

	--in case of AUTO Responders and campaigns , REMOVE all the processed contacts; 
	--this deletes all the contacts those are errored out OR those are processed with error after 3 reties OR successfullt sent
	-- Successfully sent statuses are getting updated to MKEmailSendLog table using different procedure. 
	DELETE FROM  MKCampaignRunWorkTable WHERE CampaignRunId=@Id AND (ProcessingStatus = 2 OR ProcessingStatus = 4 OR ProcessingCount = 3)

	--Cleanup the ResponseWorktable
	DELETE FROM MKCampaignRunResponseWorktable WHERE CampaignRunId=@Id

	SELECT @@ROWCOUNT
END
GO
PRINT 'Creating procedure Watch_GetWatchByUser'
GO
IF (OBJECT_ID('Watch_GetWatchByUser') IS NOT NULL)
	DROP PROCEDURE Watch_GetWatchByUser
GO

CREATE PROCEDURE [dbo].[Watch_GetWatchByUser]
--********************************************************************************

@UserId uniqueIdentifier 
--********************************************************************************
AS
BEGIN
--********************************************************************************
SET NOCOUNT ON
SELECT  W.Id,
  		W.SiteId,
  		W.[Name],
  		W.Description,
  		W.IncludesinOverlay,
  		W.Status,
  		W.CreatedBy,
  		W.CreatedDate,
  		W.ModifiedBy,
  		W.ModifiedDate
      
FROM ALWatch W
INNER JOIN TRUserWatch UW on W.Id = UW.WatchId
where  UW.UserId=@UserId
END
GO
PRINT 'Creating procedure CampaignContact_VerifyFillable'
GO
IF (OBJECT_ID('CampaignContact_VerifyFillable') IS NOT NULL)
	DROP PROCEDURE CampaignContact_VerifyFillable
GO
CREATE PROCEDURE [dbo].[CampaignContact_VerifyFillable]
(
	@CampaignId uniqueidentifier,
	@CampaignRunId uniqueidentifier,
	@CanPopulateWorkTable INT OUTPUT
)
AS
BEGIN

	--PART 3: THIS IS USED BY CAMPAIGN PROCESSOR to fill the contacts and select contacts by batch; both for campaign and auto responders
	--DECLARE @CanPopulateWorkTable int --Decides whether to populate the worktable or not
	DECLARE @IsRunnableCampaign bit -- Decides whether the given campaign is in running state still.
	declare @IsAutoResponder bit , @CurrentUtcDateTime datetime

	set @CanPopulateWorkTable = 0

	set @CurrentUtcDateTime = GetUtcDate()


	--If the campaign is started processing and the worktable does not have any records for the runid
	IF  EXISTS (SELECT 1 FROM MKCampaign Where Status = 5 and Id=@CampaignId)
	BEGIN
		 SET @IsRunnableCampaign = 1  
		  PRINT 'Runnable Campaign'  
		  IF EXISTS (SELECT 1 FROM MKCampaignAdditionalInfo where CampaignId=@CampaignId AND UseLocalTimeZoneToSend=1) 
		  AND EXISTS (SELECT 1 FROM MKCampaignRunTimeZoneSites WHERE CampaignRunId = @CampaignRunId AND StartTime >= @CurrentUtcDateTime)  
		  BEGIN  
			   SET @CanPopulateWorkTable = 1  
			   PRINT 'Populates the worktable for timezone'  
		  END 
		  ELSE IF NOT EXISTS(SELECT 1 FROM MKCampaignRunWorkTable WHERE CampaignRunId=@CampaignRunId) and Not Exists(select 1 from MKEmailSendLog where CampaignRunId=@CampaignRunId) and Not Exists(select 1 from MKCampaignRunErrorLog where CampaignRunId=@CampaignRunId)
		  BEGIN
			SET @CanPopulateWorkTable = 1  
			   PRINT 'Populates the worktable' 
		  END
		  ELSE
		  BEGIN
			PRINT 'No need to populate the worktable.process the existing contacts in the worktable'
		  END

	END
	--For auto responders just check whether any records are there in the worktable based on the below assumptions
	--1. Everytime, each response processing is going to be a separate run
	--2. If there is no records inthe worktable, it means that we need to verify the response conditions to fill the worktable
	--3. Each responseId(CampaignRunId) is generated in such a way that for each configured schduelded runs for response, there could be duplicates emails
	-- for example, if everyday logged users to be send with some coupon, Everyday ONLY ONE time we are going get contacts for the logged in response instead of pinging every time.
	ELSE IF EXISTS (SELECT 1 FROM MKCampaign  WHERE Type=2 and Id=@CampaignId) 
	BEGIN
		SET @IsAutoResponder = 1
		IF NOT EXISTS (SELECT 1 FROM MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId)
		BEGIN
			PRINT 'Runnable auto responder'
			SET @CanPopulateWorkTable = 2
		END
	END
	ELSE 
	BEGIN
		PRINT 'No runnable campaign or no auto responders to process'
	END



	/*
	--Populates the worktable 
	IF @CanPopulateWorkTable = 1 
	BEGIN
		PRINT 'Populating the worktable'

		--Call this for all the sites; there is a restriction to put this logic on Fill procedure itself;
		-- The place where we fill

		EXEC CampaignContact_Fill @SiteId,@CampaignId,@CampaignRunId, @EmailPerCampaignLimit
	END 
	ELSE IF (@CanPopulateWorkTable = 2)
	BEGIN
		PRINT 'Fill the worktable with auto responders contacts'
		
		EXEC CampaignContact_FillResponse @SiteId,@CampaignId,@CampaignRunId,NULL --@CampaignEmailId
	END
	*/

	RETURN @CanPopulateWorkTable
END

GO
PRINT 'Creating procedure Framework_GetSystemUserId'
GO
IF (OBJECT_ID('Framework_GetSystemUserId') IS NOT NULL)
	DROP PROCEDURE Framework_GetSystemUserId
GO
CREATE PROCEDURE [dbo].[Framework_GetSystemUserId]
as
begin
	declare @SystemUserId uniqueidentifier
	
	set @SystemUserId = dbo.GetEmptyGUID()
	select @SystemUserId = Id from USUser where lower(UserName)='iappssystemuser' and Status=1
	select @SystemUserId
end
GO
PRINT 'Creating procedure CampaignContact_Get'
GO
IF (OBJECT_ID('CampaignContact_Get') IS NOT NULL)
	DROP PROCEDURE CampaignContact_Get
GO
CREATE PROCEDURE [dbo].[CampaignContact_Get]
(
	@SiteId uniqueidentifier = null,
	@CampaignId uniqueidentifier = null,
	@CampaignRunId UNIQUEIDENTIFIER = null,
	@EmailPerCampaignLimit INT = NULL,
	@BatchSize INT,
	@VerifyBatchCompletion BIT = NULL,
	@OnlyCount BIT = NULL --This is used along with the campaignid to know the count

)
AS
BEGIN

/*
PART 1: Getting contacts for the campaign
PART 2: Verifying whether a email campaign is finished processing by the email processor
PART 3: Getting contacts for the Email processor to send emails using the batch logic; this also includes the AUTO RESPONDER contact processing
CampaignRunHistoryDto_UpdateResponse is used to update the response back to the email processor worktable
*/

	/*
	Create columns for ProcessingCount , Message, Status
• For the given Campaign Run Id, Batch size do the following
◦Verify whether the work table has data for this campaign and the campaign is still in processing status
◦If no contacts found , get the contacts for the campaign and insert into worktable
◦Get the contacts for the NEXT batch and return it.
◾The contacts should not have Processing count > 0
◦Mark the batch as processing by increasing the ProcessingCount by 1
◦If there is no contacts with ProcessingCount 0, then USE retry by sending Processing count starting from 1 to 3
◦If no contacts found EVEN after retry with ProcessingCount < 3 send empty result set.

	*/
	/*
	CAMPAIGN STATUS
	---------------------
		Draft = 1,
        Active = 2,
        Completed = 3,
        Archived = 4,
        Running = 5,
        Cancelled = 6,
        PartiallySent = 7,
	
	*/
	--PART 1: This is to get the contacts for the given campaign;THIS IS NOT USED BY EMAIL PROCESSOR
	IF ( (@CampaignRunId IS NULL OR @CampaignRunId = dbo.GetEmptyGuid()) AND @CampaignId IS NOT NULL AND @CampaignId <> dbo.GetEmptyGuid() )
	BEGIN
		
		Declare @tblContacts as Table
		(
			Id uniqueidentifier,
			UserId uniqueidentifier,
			FirstName varchar(1024) null,
			MiddleName varchar(1024) null,
			LastName varchar(1024) null,
			CompanyName varchar(1024) null,
			BirthDate datetime null,
			Gender varchar(1024) null,
			AddressId uniqueidentifier null,
			Status int null,
			HomePhone varchar(1024) null,
			MobilePhone varchar(1024) null,
			OtherPhone varchar(1024) null,
			ImageId uniqueidentifier null,
			Notes varchar(max) null,
			Email varchar(256),
			ContactType int,
			ContactSourceId int,
			TotalRecords int null
		)

		--Get the direct list attached to the campaign
		insert into @tblContacts
		select C.[UserId] Id
				,C.[UserId]
				,C.[FirstName]
				,C.[MiddleName]
				,C.[LastName]
				,C.[CompanyName]
				,dbo.ConvertTimeFromUtc(BirthDate, @SiteId)BirthDate 
				,C.[Gender]
				,C.[AddressId]
				,C.[Status]
				,C.[HomePhone]
				,C.[MobilePhone]
				,C.[OtherPhone]
				,C.ImageId 
				,C.[Notes]
				,C.[Email]
				,C.[ContactType]
				,C.[ContactSourceId] ,0
			from MKCampaignDistributionList CDL JOIN TADistributionListUser DLU ON CDL.DistributionListId=DLU.DistributionListId AND ContactListSubscriptionType != 3
			JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
			JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id
			WHERE CDL.CampaignId = @CampaignId AND DL.ListType = 0

		--Get all the contacts for AUTO LIST
			declare @autoListIds table (Id uniqueidentifier, RowNum int)

			insert into @autoListIds
			select DistributionListId,
			ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
			From MKCampaignDistributionList CDL JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			Where CampaignId = @CampaignId  AND DL.ListType = 1 --needs to be verified.
		
			declare @cnt int,  @TempId uniqueidentifier
			select @cnt = MAX(RowNum) from @autoListIds
		 
			while (@cnt >0)
			begin
					
				SELECT @TempId = Id FROM @autoListIds WHERE RowNum = @cnt

				Declare @queryXml xml,@TotalRecords int 
				SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@TempId 
				
				IF (@queryXml IS NOT NULL AND @SiteId IS NOT NULL)
				BEGIN
					--It is a must to have a valid site id 
					insert into @tblContacts
					exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@SiteId, @MaxRecords=0,@ContactListId = @TempId, @TotalRecords=@TotalRecords output,  @IncludeAddress = 0
				END

				SET @cnt = @cnt - 1
			end

			--Get the contacts attached directly to the campaign
			insert @tblContacts
			(	Id
				,UserId
				,FirstName
				,MiddleName
				,LastName
				,CompanyName
				,BirthDate
				,Gender
				,AddressId
				,Status
				,HomePhone
				,MobilePhone
				,OtherPhone
				,ImageId
				,Notes
				,Email
				,ContactType
				,ContactSourceId)
			select c.UserId, c.UserId, c.FirstName, c.MiddleName, c.LastName, c.CompanyName, c.BirthDate,
				c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone, c.OtherPhone, c.ImageId, c.Notes, 
				c.Email, c.ContactType,c.ContactSourceId
			from vw_contacts c
				join MKCampaignUser cu on cu.UserId = c.UserId and cu.CampaignId = @CampaignId

		IF (@OnlyCount = 1)
			SELECT COUNT(DISTINCT(UserId)),@CampaignId CampaignId FROM @tblContacts 
		ELSE
			SELECT C.*,CS.SiteId FROM @tblContacts C JOIN vw_ContactSite CS on Cs.UserId = C.Id and CS.IsPrimary = 1

		RETURN;
	END 

	DECLARE @CurrentUtcDateTime datetime

	SET @CurrentUtcDateTime = GetUtcDate()

	--PART 2:THIS IS USED BY EMAIL PROCESSOR to verify whether a campaign is completed fully
	IF (@VerifyBatchCompletion = 1)
	BEGIN
		DECLARE @RVal BIT
		SET @RVal = 0

		IF (
		NOT EXISTS(SELECT 1 FROM MKCampaignRunTimeZoneSites WHERE CampaignRunId = @CampaignRunId AND IsProcessed=0 AND StartTime >= @CurrentUtcDateTime )  
		AND NOT EXISTS(SELECT 1 FROM MKCampaignRunWorkTable WHERE (ProcessingStatus = 0 OR ProcessingStatus = 1) AND CampaignRunId = @CampaignRunId)
		
		 )
		BEGIN
			SET  @RVal = 1
			--Update the completion date only if there are some contacts.
			--IF (EXISTS (SELECT 1 FROM MKCampaignRunWorkTable WHERE CampaignRunId=@CampaignRunId))
			--BEGIN
				UPDATE MKCampaignRunHistory SET EndDateTime=@CurrentUtcDateTime WHERE Id= @CampaignRunId
			--END
			--cleanup the worktable; this is moved to update batch status process; this is to ensure that Verify should not remove contacts JUST after the status updates.
			--DELETE FROM MKCampaignRunWorkTable WHERE CampaignRunId=@CampaignRunId
		END

		SELECT @RVal

		RETURN;
	END --Ends the process.


	--PART 3: THIS IS USED BY CAMPAIGN PROCESSOR to fill the contacts and select contacts by batch; both for campaign and auto responders
	DECLARE @CanPopulateWorkTable int --Decides whether to populate the worktable or not
	DECLARE @IsRunnableCampaign bit -- Decides whether the given campaign is in running state still.
	declare @IsAutoResponder bit 
	Declare @TabContactIds as table (ContactId uniqueidentifier,AddressId uniqueidentifier)


	SET @CanPopulateWorkTable = 0 --by default return the contacts from worktable
	SET @IsRunnableCampaign = 0
	SET @IsAutoResponder = 0

	
	Declare @IsRetry bit
	SET @IsRetry = 0

	--If the campaign is in processing state then proceed;otherwise DO NOT Proceed
	IF ((SELECT COUNT(1) FROM MKCampaignRunWorkTable WHERE @CampaignRunId=CampaignRunId) > 0)
	BEGIN
		PRINT 'Getting contacts from worktable'
		
		;with ContactId_Cte(ContactId,AddressId)
		AS
		(
			SELECT UserId,AddressId--,ROW_NUMBER() OVER (ORDER BY CampaignRunId) RowNum 
			FROM MKCampaignRunWorkTable
			WHERE @CampaignRunId = CampaignRunId AND ProcessingCount = 0 AND ProcessingStatus = 0 
		)
		insert into @TabContactIds (ContactId,AddressId)
		Select TOP (@BatchSize) ContactId,AddressId from ContactId_Cte

		--Updates the selected contacts as PROCESSING
		UPDATE MKCampaignRunWorkTable 
		SET ProcessingCount = 1 , ProcessingStatus = 1 
		WHERE UserId in (Select ContactId from @TabContactIds)

		--For retry
		IF ((SELECT COUNT(1) FROM @TabContactIds) = 0)
		BEGIN
			PRINT 'Processing RETRY'

			;with ContactId_Cte(ContactId,AddressId)
			AS
			(
				SELECT UserId,AddressId--,ROW_NUMBER() OVER (ORDER BY CampaignRunId) RowNum 
				FROM MKCampaignRunWorkTable
				WHERE @CampaignRunId = CampaignRunId AND ProcessingCount > 0 AND ProcessingCount <= 3 AND ProcessingStatus = 0 
			)
			insert into @TabContactIds (ContactId,AddressId)
			Select TOP (@BatchSize) ContactId,AddressId from ContactId_Cte

			--Updates the selected contacts as PROCESSING
			UPDATE MKCampaignRunWorkTable 
			SET ProcessingCount = ProcessingCount + 1 , ProcessingStatus = 1 
			WHERE UserId in (Select ContactId from @TabContactIds)

		END
	END

	-- Contacts
	Select 
		--Row,
		CampaignRunId,
		WT.UserId Id,
		FirstName ,
		MiddleName ,
		LastName,
		CompanyName ,
		BirthDate ,
		Gender ,
		WT.AddressId ,
		Status ,
		HomePhone ,
		MobilePhone ,
		OtherPhone ,
		Email ,
		Notes ,
		ContactType ,
		CS.SiteId
	From @TabContactIds T JOIN MKCampaignRunWorkTable WT ON T.ContactId = WT.UserId AND WT.CampaignRunId = @CampaignRunId
	JOIN vw_ContactSite CS on CS.UserId = T.ContactId and CS.IsPrimary = 1


	-- Address
	select	a.Id
			,a.FirstName
			,a.LastName
			,a.AddressType
			,a.AddressLine1
			,a.AddressLine2
			,a.AddressLine3
			,a.City
			,a.StateId
			,a.Zip			
			,a.CountryId
			,a.Phone
			,a.CreatedDate
			,a.CreatedBy
			,a.ModifiedDate
			,a.ModifiedBy
			,a.Status
			,a.County
			,a.CountryName
			,a.CountryCode
			,a.StateCode
			,a.StateName
	from vw_Address a
		Join @TabContactIds c on c.AddressId = a.Id

	--	-- Profile
	--select * 
	--from UsMemberProfile p
	--	join @tblContacts c on c.UserId = p.UserId

		/*
		Status
		0 - Processing NOT YET started;contacts available for 1st time processing
		1 - Processing Started;contacts sent for email processor
		2 - Processing Complete; contacts successfully processed by the email processor
		3 - Processing Complete with Error or Number of retries exceeded the limit (3)
		*/

	
END	
				
GO
PRINT 'Creating procedure CampaignRunHistoryDto_Save'
GO
IF (OBJECT_ID('CampaignRunHistoryDto_Save') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_Save
GO

CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_Save]
(	
	@Id UNIQUEIDENTIFIER = NULL OUTPUT,
    @CampaignId UNIQUEIDENTIFIER = NULL,
    @RunDate DATETIME = NULL,
    @Sends INT = 0,
    @Delivered INT = 0,
    @Opens INT = 0,
    @Clicks INT = 0,
    @Unsubscribes INT = 0,
    @Bounces INT = 0,
    @TriggeredWatches INT = 0,
    @EmailSubject VARCHAR(250),
    @SenderName VARCHAR(250),
    @SenderEmail VARCHAR(250),
    @CostPerEmail DECIMAL(5, 2) = 0,
    @ConfirmationEmail VARCHAR(250),
    @EmailHtml TEXT = NULL,
    @CMSPageId UNIQUEIDENTIFIER,
    @EmailText TEXT = NULL,
	@IsNew bit=0 OUTPUT
)
AS
BEGIN	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID() OR NOT EXISTS(SELECT * FROM MKCampaignRunHistory WHERE Id = @Id))
	BEGIN
		IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
			SET @Id = NEWID()
		
		IF @RunDate IS NULL
			SET @RunDate = GETUTCDATE()
		
		INSERT
		INTO	MKCampaignRunHistory (
			Id,
			CampaignId,
			RunDate,
			Sends,
			Delivered,
			Opens,
			Clicks,
			Unsubscribes,
			Bounces,
			TriggeredWatches,
			EmailSubject,
			SenderName,
			SenderEmail,
			CostPerEmail,
			ConfirmationEmail,
			EmailHtml,
			CMSPageId,
			EmailText
		)
		VALUES	(
			@Id,
			@CampaignId,
			@RunDate,
			@Sends,
			@Delivered,
			@Opens,
			@Clicks,
			@Unsubscribes,
			@Bounces,
			@TriggeredWatches,
			@EmailSubject,
			@SenderName,
			@SenderEmail,
			@CostPerEmail,
			@ConfirmationEmail,
			@EmailHtml,
			@CMSPageId,
			@EmailText
		)
		SET @IsNew =1
		UPDATE	TASchedule
		SET		RunCount = RunCount + 1
		FROM	TASchedule AS S
				INNER JOIN MKCampaignAdditionalInfo AS A ON A.ScheduleId = S.Id
		WHERE	A.CampaignId = @CampaignId
	END
	ELSE
	BEGIN
		SET @IsNew =0
		UPDATE	MKCampaignRunHistory
		SET		CampaignId = @CampaignId,
				
				Sends = @Sends,
				Delivered = @Delivered,
				Opens = @Opens,
				Clicks = @Clicks,
				Unsubscribes = @Unsubscribes,
				Bounces = @Bounces,
				TriggeredWatches = @TriggeredWatches,
				EmailSubject = @EmailSubject,
				SenderName = @SenderName,
				SenderEmail = @SenderEmail,
				CostPerEmail = @CostPerEmail,
				ConfirmationEmail = @ConfirmationEmail,
				EmailHtml = @EmailHtml,
				CMSPageId = @CMSPageId,
				EmailText = @EmailText
		WHERE	Id = @Id 
	END
END



GO
PRINT 'Creating procedure CampaignRunHistoryDto_Get'
GO
IF (OBJECT_ID('CampaignRunHistoryDto_Get') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_Get
GO

CREATE PROCEDURE [dbo].[CampaignRunHistoryDto_Get]  
(  
	@Id				UNIQUEIDENTIFIER = NULL,
	@CampaignId		UNIQUEIDENTIFIER = NULL,
	@Status			INT = NULL ,
	@SiteId			UNIQUEIDENTIFIER = NULL,
	@PageNumber		INT = NULL,
	@PageSize		INT = NULL,	
	@MaxRecords		INT = NULL,
	@Query			NVARCHAR(MAX) = NULL
)  
AS 
BEGIN
	DECLARE @PageLowerBound INT, @PageUpperBound INT, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id UNIQUEIDENTIFIER, RowNumber INT)
	
	IF @Query IS NULL
	BEGIN
		INSERT INTO @tbIds
		SELECT Id, 0 FROM MKCampaignRunHistory
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
	DECLARE @EmptyGuid UNIQUEIDENTIFIER
	SET @EmptyGuid = dbo.GetEmptyGUID()
			
	DECLARE @tbPagedResults TABLE (Id UNIQUEIDENTIFIER, RowNumber INT, TotalRecords INT)
	
	;WITH CTE AS(
		SELECT	ROW_NUMBER() OVER (	
					         ORDER BY T.RowNumber	
				                   ) AS RowNumber, COUNT(MKRH.Id) OVER () AS TotalRecords,
				             MKRH.Id AS Id			
		FROM  MKCampaignRunHistory MKRH 
		 			
		INNER JOIN MKCampaign MKC ON MKRH.CampaignId = MKC.Id		
		JOIN @tbIds AS T ON T.Id = MKRH.Id		
		WHERE 
			    (@Id IS NULL OR MKRH.Id = @Id) AND
			    (@CampaignId IS NULL OR MKRH.CampaignId = @CampaignId) AND
				(@SiteId IS NULL OR MKC.ApplicationId = @SiteId) AND			
				(@Status IS NULL OR MKC.Status = @Status)
	)			        	
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT MKRH.Bounces,
	       MKRH.CMSPageId,
	       MKRH.CampaignId,
	       MKRH.Clicks,
	       MKRH.ConfirmationEmail,
	       MKRH.CostPerEmail,
	       MKRH.Delivered,
	       MKRH.EmailHtml,
	       MKRH.EmailSubject,
	       MKRH.EmailText,
	       dbo.ConvertTimeFromUtc(MKRH.EndDateTime, @SiteId)EndDateTime,	      
	       MKRH.Id,
	       MKRH.Opens,
		   MKRH.UniqueOpens,
	       dbo.ConvertTimeFromUtc(MKRH.RunDate, @SiteId)RunDate,	     
	       MKRH.SenderEmail,
	       MKRH.SenderName,
	       MKRH.Sends,
	       MKRH.TriggeredWatches,
	       MKRH.Unsubscribes,   
	       MKC.ApplicationId,
		   T.TotalRecords	                     
	FROM MKCampaignRunHistory MKRH
		JOIN @tbPagedResults T ON T.Id = MKRH.Id
		INNER JOIN MKCampaign MKC ON MKC.Id = MKRH.CampaignId		
	ORDER BY RowNumber
		
END

GO

PRINT 'Creating procedure ContactDto_Delete'
GO
IF (OBJECT_ID('ContactDto_Delete') IS NOT NULL)
	DROP PROCEDURE ContactDto_Delete
GO
CREATE PROCEDURE [dbo].[ContactDto_Delete]
(
	@Id uniqueidentifier
)
AS
BEGIN
	UPDATE MKContact SET Status=3,ModifiedDate=GetUtcDate() WHERE Id=@Id

	--Delete it from TADistribution list
	DELETE FROM TADistributionListUser WHERE UserId=@Id

	--Delete it from MKCampaignUser
	DELETE FROM MKCampaignUser WHERE UserId=@Id

	DELETE FROM MKCampaignUserDraft WHERE UserId=@Id
END


GO
PRINT 'Creating procedure EmailProcessorSettingsDto_Get'
GO
IF (OBJECT_ID('EmailProcessorSettingsDto_Get') IS NOT NULL)
	DROP PROCEDURE EmailProcessorSettingsDto_Get
GO
CREATE PROCEDURE [dbo].[EmailProcessorSettingsDto_Get]  
(  
	@SiteId UNIQUEIDENTIFIER = NULL,
	@IncludeVariantSites BIT = 0
)  
AS  
BEGIN
	SELECT	S.Id AS SiteId, 
			ISNULL(V.SenderEmailOverRide, M.SenderEmailOverRide) AS SenderEmailOverRide,
			ISNULL(V.ReplyToEmail, M.ReplyToEmail) AS ReplyToEmail,
			ISNULL(V.ReturnPath, M.ReturnPath) AS ReturnPath,
			ISNULL(V.ReturnPathDomain, M.ReturnPathDomain) AS ReturnPathDomain,
			ISNULL(V.SmtpServer, M.SmtpServer) AS SmtpServer,
			ISNULL(V.SmtpPort, M.SmtpPort) AS SmtpPort,
			ISNULL(V.SmtpEnableSsl, M.SmtpEnableSsl) AS SmtpEnableSsl,
			ISNULL(V.SmtpAuthentication, M.SmtpAuthentication) AS SmtpAuthentication,
			ISNULL(V.SmtpUsername, M.SmtpUsername) AS SmtpUsername,
			ISNULL(V.SmtpPassword, M.SmtpPassword) AS SmtpPassword,
			ISNULL(V.BatchSize, M.BatchSize) AS BatchSize,
			ISNULL(V.Enabled, M.Enabled) AS Enabled
	FROM	SISite AS S
			LEFT JOIN STEmailProcessorSetting AS M ON M.SiteId = S.MasterSiteId
			LEFT JOIN STEmailProcessorSetting AS V ON V.SiteId = S.Id AND (S.MasterSiteId <> S.Id)
	WHERE	@SiteId IS NULL OR (
				S.Id = @SiteId OR (
					@IncludeVariantSites = 1 AND 
					S.Id IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId))
				)
			) AND
			S.Status = 1
END

GO

GO

PRINT 'Creating procedure EmailProcessorSettingsDto_Save'
GO
IF (OBJECT_ID('EmailProcessorSettingsDto_Save') IS NOT NULL)
	DROP PROCEDURE EmailProcessorSettingsDto_Save

GO
CREATE PROCEDURE [dbo].[EmailProcessorSettingsDto_Save]
(
	@SiteId	UNIQUEIDENTIFIER,
	@SenderEmailOverride NVARCHAR(255) = NULL,
	@ReplyToEmail NVARCHAR(255) = NULL,
	@ReturnPath NVARCHAR(255) = NULL,
	@ReturnPathDomain NVARCHAR(255) = NULL,
	@SmtpServer NVARCHAR(255) = NULL,
	@SmtpPort INT = NULL,
	@SmtpEnableSsl BIT = NULL,
	@SmtpAuthentication BIT = NULL,
	@SmtpUsername NVARCHAR(255) = NULL,
	@SmtpPassword NVARCHAR(255) = NULL,
	@BatchSize INT = NULL,
	@Enabled BIT = NULL
)
AS
BEGIN	
	BEGIN TRY
		BEGIN TRANSACTION

		DELETE
		FROM	STEmailProcessorSetting
		WHERE	SiteId = @SiteId

		INSERT INTO STEmailProcessorSetting
		(
			SiteId,	
			SenderEmailOverride,
			ReplyToEmail,
			ReturnPath,
			ReturnPathDomain,
			SmtpServer,
			SmtpPort,
			SmtpEnableSsl,
			SmtpAuthentication,
			SmtpUsername,
			SmtpPassword,
			BatchSize,
			Enabled
		)
			SELECT	@SiteId,
					CASE WHEN S.Id <> S.MasterSiteId AND @SenderEmailOverride = M.SenderEmailOverride THEN NULL ELSE @SenderEmailOverride END AS SenderEmailOverride,
					CASE WHEN S.Id <> S.MasterSiteId AND @ReplyToEmail = M.ReplyToEmail THEN NULL ELSE @ReplyToEmail END AS ReplyToEmail,
					CASE WHEN S.Id <> S.MasterSiteId AND @ReturnPath = M.ReturnPath THEN NULL ELSE @ReturnPath END AS ReturnPath,
					CASE WHEN S.Id <> S.MasterSiteId AND @ReturnPathDomain = M.ReturnPathDomain THEN NULL ELSE @ReturnPathDomain END AS ReturnPathDomain,
					CASE WHEN S.Id <> S.MasterSiteId AND @SmtpServer = M.SmtpServer THEN NULL ELSE @SmtpServer END AS SmtpServer,
					CASE WHEN S.Id <> S.MasterSiteId AND @SmtpPort = M.SmtpPort THEN NULL ELSE @SmtpPort END AS SmtpPort,
					CASE WHEN S.Id <> S.MasterSiteId AND @SmtpEnableSsl = M.SmtpEnableSsl THEN NULL ELSE @SmtpEnableSsl END AS SmtpEnableSsl,
					CASE WHEN S.Id <> S.MasterSiteId AND @SmtpAuthentication = M.SmtpAuthentication THEN NULL ELSE @SmtpAuthentication END AS SmtpAuthentication,
					CASE WHEN S.Id <> S.MasterSiteId AND @SmtpUsername = M.SmtpUsername THEN NULL ELSE @SmtpUsername END AS SmtpUsername,
					CASE WHEN S.Id <> S.MasterSiteId AND @SmtpPassword = M.SmtpPassword THEN NULL ELSE @SmtpPassword END AS SmtpPassword,
					CASE WHEN S.Id <> S.MasterSiteId AND @BatchSize = M.BatchSize THEN NULL ELSE @BatchSize END AS BatchSize,
					CASE WHEN S.Id <> S.MasterSiteId AND @Enabled = M.Enabled THEN NULL ELSE @Enabled END AS Enabled					
			FROM	SISite AS S
					LEFT JOIN STEmailProcessorSetting AS M ON M.SiteId = S.MasterSiteId
			WHERE	S.Id = @SiteId
		
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK
	END CATCH
END
GO
PRINT 'Creating procedure AttributeEnumDto_Save'
GO
IF (OBJECT_ID('AttributeEnumDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeEnumDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeEnumDto_Save]
(
	@Id				uniqueidentifier = null OUTPUT,  
	@AttributeId	uniqueidentifier,   
	@Title			nvarchar(500),  
	@Code			nvarchar(200),  
	@NumericValue	decimal(9),  
	@Value			nvarchar(max),  
	@IsDefault		bit,  
	@Sequence		int,  
	@ModifiedBy		uniqueidentifier,  
	@Status			int,
	@ApplicationId	uniqueidentifier  
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		DECLARE @CurrentSequence int  
		SELECT @CurrentSequence = ISNULL(MAX(Sequence),0) 
			FROM ATAttributeEnum WHERE AttributeId = @AttributeId  

		IF @Status <= 0
			SET @Status = 1
		
		INSERT INTO ATAttributeEnum
		(  
			Id,   
			AttributeId,   
			Title,  
			Code,  
			NumericValue,  
			[Value],  
			IsDefault,  
			Sequence,  
			CreatedDate,  
			CreatedBy,  
			[Status]
		)  
		VALUES
		(  
			@Id,  
			@AttributeId,   
			@Title,  
			@Code,  
			@NumericValue,  
			@Value,  
			@IsDefault,  
			@CurrentSequence + 1,  
			@UtcNow,  
			@ModifiedBy,  
			@Status
		)  
  
		UPDATE ATAttribute WITH (ROWLOCK) SET IsEnum = 1 WHERE Id = @AttributeID  

	END
	ELSE
	BEGIN
		IF @Status <= 0
			SET @Status = NULL
			
		UPDATE ATAttributeEnum  WITH (ROWLOCK)  
		SET Id = @Id,  
			AttributeId = @AttributeId,   
			Title = @Title,  
			Code = @Code,  
			NumericValue = @NumericValue,  
			[Value] = @Value,  
			IsDefault = @IsDefault,  
			Sequence = @Sequence,  
			ModifiedDate = @UtcNow,  
			ModifiedBy = @ModifiedBy,  
			[Status] = @Status  
		 WHERE [Id] = @Id   
	END
END

GO
PRINT 'Creating procedure AttributeEnumDto_Delete'
GO
IF (OBJECT_ID('AttributeEnumDto_Delete') IS NOT NULL)
	DROP PROCEDURE AttributeEnumDto_Delete
GO
CREATE PROCEDURE [dbo].[AttributeEnumDto_Delete]
(
	@Id				uniqueidentifier = NULL,
	@ModifiedBy		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	
	DELETE FROM ATAttributeEnum WHERE Id = @Id
	
END
GO

PRINT 'Creating procedure SiteAttributeDto_Add'
GO
IF (OBJECT_ID('SiteAttributeDto_Save') IS NOT NULL)
	DROP PROCEDURE SiteAttributeDto_Save
GO
GO
IF (OBJECT_ID('SiteAttributeDto_Add') IS NOT NULL)
	DROP PROCEDURE SiteAttributeDto_Add
GO
CREATE PROCEDURE [dbo].[SiteAttributeDto_Add]
(
	@Items			xml,
	@RemoveExisting	bit = NULL,
	@ModifiedBy		uniqueidentifier	
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	DELETE V 
	FROM ATSiteAttributeValue V
		JOIN @Items.nodes('/Items/Item') AS D(n) ON V.SiteId = D.n.value('(ObjectId)[1]', 'uniqueidentifier')
			AND (@RemoveExisting = 1 OR V.AttributeId = D.n.value('(AttributeId)[1]', 'uniqueidentifier'))
	
	INSERT INTO ATSiteAttributeValue
	(
		Id,   
		SiteId,   
		AttributeId,  
		AttributeEnumId,  
		[Value],
		Notes,  
		CreatedDate,  
		CreatedBy
	)
	SELECT NEWID(),
		D.n.value('(ObjectId)[1]','uniqueidentifier'),
		D.n.value('(AttributeId)[1]','uniqueidentifier'),
		D.n.value('(AttributeEnumId)[1]','uniqueidentifier'),
		D.n.value('(Value)[1]','nvarchar(max)'),
		D.n.value('(Notes)[1]','nvarchar(max)'),
		@UtcNow,  
		@ModifiedBy  
	FROM @Items.nodes('/Items/Item') AS D(n)
	WHERE D.n.value('(Value)[1]','nvarchar(max)') != ''

END

GO
PRINT 'Creating procedure ContactAttributeDto_Save'
GO
IF (OBJECT_ID('ContactAttributeDto_Save') IS NOT NULL)
	DROP PROCEDURE ContactAttributeDto_Save
GO

IF (OBJECT_ID('ContactAttributeDto_Add') IS NOT NULL)
	DROP PROCEDURE ContactAttributeDto_Add
GO
CREATE PROCEDURE [dbo].[ContactAttributeDto_Add]
(
	@Items			xml,
	@RemoveExisting	bit = NULL,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	DELETE V 
	FROM ATContactAttributeValue V
		JOIN @Items.nodes('/Items/Item') AS D(n) ON V.ContactId = D.n.value('(ObjectId)[1]', 'uniqueidentifier')
			AND (@RemoveExisting = 1 OR V.AttributeId = D.n.value('(AttributeId)[1]', 'uniqueidentifier'))
	
	INSERT INTO ATContactAttributeValue
	(
		Id,   
		ContactId,   
		AttributeId,  
		AttributeEnumId,  
		[Value],
		Notes,  
		CreatedDate,  
		CreatedBy
	)
	SELECT NEWID(),
		D.n.value('(ObjectId)[1]','uniqueidentifier'),
		D.n.value('(AttributeId)[1]','uniqueidentifier'),
		D.n.value('(AttributeEnumId)[1]','uniqueidentifier'),
		D.n.value('(Value)[1]','nvarchar(max)'),
		D.n.value('(Notes)[1]','nvarchar(max)'),
		@UtcNow,  
		@ModifiedBy  
	FROM @Items.nodes('/Items/Item') AS D(n)
	WHERE D.n.value('(Value)[1]','nvarchar(max)') != ''
	
END
GO

PRINT 'Creating procedure AttributeValueDto_Save'
GO
IF (OBJECT_ID('AttributeValueDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeValueDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeValueDto_Save]
(
	@Id				uniqueidentifier = null OUTPUT,  
	@AttributeId	uniqueidentifier,   
	@ObjectId		uniqueidentifier,   
	@CategoryId		int,  
	@AttributeEnumId uniqueidentifier,  
	@Value			nvarchar(max),  
	@Notes			nvarchar(max),  
	@ModifiedBy		uniqueidentifier,  
	@ApplicationId	uniqueidentifier  
)
AS
BEGIN
	IF @CategoryId = 1
	BEGIN
		EXEC [dbo].[SiteAttributeDto_Save]
			@Id = @Id OUTPUT,
			@AttributeId = @AttributeId,
			@SiteId = @ObjectId,
			@AttributeEnumId = @AttributeEnumId,
			@Value = @Value,
			@Notes = @Notes,
			@ModifiedBy = @ModifiedBy
	END
	ELSE IF @CategoryId = 3
	BEGIN
		EXEC [dbo].[ContactAttributeDto_Save]
			@Id = @Id OUTPUT,
			@AttributeId = @AttributeId,
			@ContactId = @ObjectId,
			@AttributeEnumId = @AttributeEnumId,
			@Value = @Value,
			@Notes = @Notes,
			@ModifiedBy = @ModifiedBy
	END
END


GO
PRINT 'Creating procedure AttributeExpressionDto_Get'
GO
IF (OBJECT_ID('AttributeExpressionDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeExpressionDto_Get
GO
CREATE PROCEDURE [dbo].[AttributeExpressionDto_Get]
AS
BEGIN
	SELECT * FROM ATAttributeExpression ORDER BY Sequence
END
PRINT 'Creating procedure AttributeDto_Save'
GO
IF (OBJECT_ID('AttributeDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeDto_Save]
(
	@Id						uniqueidentifier = NULL OUTPUT,
	@SiteId					uniqueidentifier,
	@AttributeDataTypeId	uniqueidentifier = NULL,
	@AttributeGroupId		uniqueidentifier = NULL,
	@Title					nvarchar(max) = NULL,
	@Description			nvarchar(max) = NULL,
	@Code					nvarchar(max) = NULL,
	@Sequence				int = NULL,
	@IsFaceted				bit = NULL,
	@IsSearched				bit = NULL,
	@IsSystem				bit = NULL,
	@IsDisplayed			bit = NULL,
	@IsEnum					bit = NULL,
	@IsPersonalized			bit = NULL,
	@IsMultiValued			bit = NULL,
	@Status					int = NULL,
	@Group					xml = NULL,
	@Validation				xml = NULL,
	@ModifiedBy				uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	IF (@AttributeGroupId IS NULL AND @Group IS NOT NULL)
	BEGIN
		SET @AttributeGroupId = (SELECT TOP 1 A.n.value('@Id', 'uniqueidentifier') FROM 
			@Group.nodes('/Group') AS A(n))
	END
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO ATAttribute
		(
			Id,
			AttributeDataTypeId,
			AttributeGroupId,
			Title,
			Description,
			Sequence,
			Code,
			IsFaceted,
			IsSearched,
			IsSystem,
			IsDisplayed,
			IsEnum,
			CreatedBy,
			CreatedDate,
			Status,
			IsPersonalized,
			IsMultiValued	
		)
		VALUES
		(
			@Id,
			@AttributeDataTypeId,
			@AttributeGroupId,
			@Title,
			@Description,
			@Sequence,
			@Code,
			ISNULL(@IsFaceted, 0),
			ISNULL(@IsSearched, 1),
			ISNULL(@IsSystem, 0),
			ISNULL(@IsDisplayed, 1),
			ISNULL(@IsEnum, 0),
			@ModifiedBy,
			@UtcNow,
			1,
			ISNULL(@IsPersonalized, 0),
			ISNULL(@IsMultiValued, 0)	
		)

	END
	ELSE
	BEGIN
		IF @Status <= 0
			SET @Status = NULL
		
		UPDATE ATAttribute
		SET 
			AttributeDataTypeId = ISNULL(@AttributeDataTypeId, AttributeDataTypeId),
			AttributeGroupId = ISNULL(@AttributeGroupId, AttributeGroupId),
			Title = ISNULL(@Title, Title),
			Description = ISNULL(@Description, Description),
			Sequence = ISNULL(@Sequence, Sequence),
			Code = ISNULL(@Code, Code),
			IsFaceted = ISNULL(@IsFaceted, IsFaceted),
			IsSearched = ISNULL(@IsSearched, IsSearched),
			IsSystem = ISNULL(@IsSystem, IsSystem),
			IsDisplayed = ISNULL(@IsDisplayed, IsDisplayed),
			IsEnum = ISNULL(@IsEnum, IsEnum),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow,
			Status = ISNULL(@Status, Status),
			IsPersonalized = ISNULL(@IsPersonalized, IsPersonalized),	
			IsMultiValued = ISNULL(@IsMultiValued, IsMultiValued)	
		WHERE Id = @Id 
	END	
	
	IF @Validation IS NOT NULL
	BEGIN
		DELETE FROM ATAttributeValidation WHERE AttributeId = @Id
		
		INSERT INTO ATAttributeValidation
		(
			Id,
			AttributeId,
			ExpressionId,
			MinValue,
			MaxValue
		)
		SELECT
			NEWID(),
			@Id,
			A.n.value('@ExpressionId', 'uniqueidentifier'),
			A.n.value('@MinValue', 'nvarchar(max)'),
			A.n.value('@MaxValue', 'nvarchar(max)')
		FROM 
			@Validation.nodes('/Validation') AS A(n)
				
	END	
END
GO
PRINT 'Creating procedure AttributeDto_Delete'
GO
IF (OBJECT_ID('AttributeDto_Delete') IS NOT NULL)
	DROP PROCEDURE AttributeDto_Delete
GO
CREATE PROCEDURE [dbo].[AttributeDto_Delete]
(
	@Id				uniqueidentifier = NULL,
	@ModifiedBy		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	UPDATE ATAttribute
	SET 
		Status = dbo.GetDeleteStatus(),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @UtcNow
	WHERE Id = @Id
	
END
GO
PRINT 'Creating procedure AttributeGroupDto_Get'
GO
IF (OBJECT_ID('AttributeGroupDto_Get') IS NOT NULL)
	DROP PROCEDURE AttributeGroupDto_Get
GO
CREATE PROCEDURE [dbo].[AttributeGroupDto_Get]
(
	@Id				uniqueidentifier = NULL,
	@SiteId			uniqueidentifier = NULL,
	@ProductId		uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@Status			int = NULL ,
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL,
	@Query			nvarchar(max) = NULL
)
AS
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(500)
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	DECLARE @tbCount TABLE(Id uniqueidentifier, [Count] int)
	INSERT INTO @tbCount
	SELECT AttributeGroupId, COUNT(*) FROM ATAttribute
		WHERE Status != 3
	GROUP BY AttributeGroupId
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	IF @Query IS NULL
	BEGIN
		IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
		IF (@SortBy IS NOT NULL)
			SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	
		INSERT INTO @tbIds
		SELECT Id,  ROW_NUMBER() OVER (ORDER BY
			CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
			CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
			CASE WHEN @SortClause IS NULL THEN CreatedDate END ASC	
		) AS RowNumber
		FROM ATAttributeGroup
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
			
	DECLARE @tbPagedResults TABLE(Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY IsSystem DESC, T.RowNumber) AS RowNumber,
			COUNT(T.Id) OVER () AS TotalRecords,
			A.Id AS Id
		FROM ATAttributeGroup A
			JOIN @tbIds T ON A.Id = T.Id
		WHERE (@Id IS NULL OR A.Id = @Id)
			AND ((@Status IS NULL AND Status != 3) OR Status = @Status)
			AND (@ProductId IS NULL OR A.IsGlobal = 1 OR A.ProductId = @ProductId)
	)

	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT A.*,
		CU.UserFullName AS CreatedByFullName,
		MU.UserFullName AS ModifiedByFullName,
		C.[Count],
		T.RowNumber,
		T.TotalRecords
	FROM ATAttributeGroup A
		JOIN @tbPagedResults T ON T.Id = A.Id
		LEFT JOIN @tbCount C ON T.Id = C.Id
		LEFT JOIN VW_UserFullName CU on CU.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MU on MU.UserId = A.ModifiedBy
	ORDER BY RowNumber
	
END
GO
PRINT 'Creating procedure AttributeGroupDto_Save'
GO
IF (OBJECT_ID('AttributeGroupDto_Save') IS NOT NULL)
	DROP PROCEDURE AttributeGroupDto_Save
GO
CREATE PROCEDURE [dbo].[AttributeGroupDto_Save]
(
	@Id			uniqueidentifier = NULL OUTPUT,
	@SiteId		uniqueidentifier,
	@ProductId	uniqueidentifier,
	@Title		nvarchar(max) = NULL,
	@IsSystem	bit = NULL,
	@IsGlobal	bit = NULL,
	@Status		int = NULL,
	@ModifiedBy uniqueidentifier
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO ATAttributeGroup
		(
			Id,
			Title,
			SiteId,
			ProductId,
			IsSystem,
			IsGlobal,
			CreatedBy,
			CreatedDate,
			Status			
		)
		VALUES
		(
			@Id,
			@Title,
			@SiteId,
			@ProductId,
			@IsSystem,
			@IsGlobal,
			@ModifiedBy,
			@UtcNow,
			1
		)

	END
	ELSE
	BEGIN
		IF @Status <= 0
			SET @Status = NULL
			
		UPDATE ATAttributeGroup
		SET Title = ISNULL(@Title, Title),
			Status = ISNULL(@Status, Status),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @UtcNow
		WHERE Id = @Id 
	END
END
GO
PRINT 'Creating procedure PageAttributeDto_Add'
GO
IF (OBJECT_ID('PageAttributeDto_Add') IS NOT NULL)
	DROP PROCEDURE PageAttributeDto_Add
GO

CREATE PROCEDURE [dbo].[PageAttributeDto_Add]
(
	@Items			xml,
	@RemoveExisting	bit = NULL,
	@ModifiedBy		uniqueidentifier	
)
AS
BEGIN
	DECLARE @UtcNow datetime
	SET @UtcNow = GETUTCDATE()
	
	DELETE V 
	FROM ATPageAttributeValue V
		JOIN @Items.nodes('/Items/Item') AS D(n) ON V.PageDefinitionId = D.n.value('(ObjectId)[1]', 'uniqueidentifier')
			AND (@RemoveExisting = 1 OR V.AttributeId = D.n.value('(AttributeId)[1]', 'uniqueidentifier'))
	
	INSERT INTO ATPageAttributeValue
	(
		Id,   
		PageDefinitionId,   
		AttributeId,  
		AttributeEnumId,  
		[Value],
		Notes,  
		CreatedDate,  
		CreatedBy
	)
	SELECT NEWID(),
		D.n.value('(ObjectId)[1]','uniqueidentifier'),
		D.n.value('(AttributeId)[1]','uniqueidentifier'),
		D.n.value('(AttributeEnumId)[1]','uniqueidentifier'),
		D.n.value('(Value)[1]','nvarchar(max)'),
		D.n.value('(Notes)[1]','nvarchar(max)'),
		@UtcNow,  
		@ModifiedBy  
	FROM @Items.nodes('/Items/Item') AS D(n)
	WHERE D.n.value('(Value)[1]','nvarchar(max)') != ''
	
END
GO

IF (OBJECT_ID('AttributeValueDto_Add') IS NOT NULL)
	DROP PROCEDURE AttributeValueDto_Add
GO

CREATE PROCEDURE [dbo].[AttributeValueDto_Add]
(
	@Items			xml,
	@RemoveExisting	bit = NULL,
	@ModifiedBy		uniqueidentifier	
)
AS
BEGIN
	DECLARE @CategoryId int
	SET @CategoryId = (SELECT TOP 1 D.n.value('(CategoryId)[1]', 'int') 
		FROM @Items.nodes('/Items/Item') AS D(n))

	IF @CategoryId = 1
	BEGIN
		EXEC [dbo].[SiteAttributeDto_Add]
			@Items = @Items,
			@RemoveExisting = @RemoveExisting,
			@ModifiedBy = @ModifiedBy
	END
	ELSE IF @CategoryId = 2
	BEGIN
		EXEC [dbo].[PageAttributeDto_Add]
			@Items = @Items,
			@RemoveExisting = @RemoveExisting,
			@ModifiedBy = @ModifiedBy
	END
	ELSE IF @CategoryId = 3
	BEGIN
		EXEC [dbo].[ContactAttributeDto_Add]
			@Items = @Items,
			@RemoveExisting = @RemoveExisting,
			@ModifiedBy = @ModifiedBy
	END
END
GO
PRINT 'Altering procedure GetAutoBlogList'
GO
ALTER PROCEDURE [dbo].[GetAutoBlogList]
(
@SiteId uniqueidentifier,
@ListId uniqueidentifier,
@BlogId uniqueidentifier,
@PublishDateType int,
@FromDate datetime,
@ToDate datetime,
@DaysOlder int,
@OrderBy varchar(20),
@OrderType int

)
As
begin
declare 
@str nvarchar(max),
@i_Ordertype varchar(15),
@iCount int,
@i int,
@Taxonomyxml xml,
@Lft bigint,
@Rgt bigint

if (@OrderBy is null or @OrderBy = '')
	set @OrderBy = 'P.Title'
if (lower(@OrderBy) = 'createddate')
	set @OrderBy = 'PublishDate'

if (@OrderType=2)
      set @i_Ordertype = 'desc'
else
	set @i_Ordertype = 'asc'

set @str = 'declare @Pagedef varchar(max),
@Taxonomyxml xml,
@Lft bigint,
@Rgt bigint,
@PageStatus int,
@WokkFlowState int
declare
@Taxonomy_table table(
TaxonomyId uniqueidentifier,
ObjectId uniqueidentifier
)

set @Taxonomyxml  = (select ObjectId from GetObjectByRelation(''L-T'',''' + convert(varchar(36),@ListId) + ''') for XMl Auto, root)
--Select @Taxonomyxml
set @PageStatus = dbo.GetStatus(8,''Publish'')
set @WokkFlowState = dbo.GetWorkFlowStatus(''None'')
set @Pagedef=''''
	if(convert(nvarchar(max),@Taxonomyxml) != '''')
		Begin
		 insert into @Taxonomy_table Select  tab.col.value(''@ObjectId'',''uniqueidentifier'') TaxonomyId,T.ObjectId  
         from @Taxonomyxml.nodes(''/root/GetObjectByRelation'') tab(col)   
                inner join COTaxonomy H on tab.col.value(''@ObjectId'',''uniqueidentifier'')  = H.Id  
                INNER JOIN COTaxonomyObject T on H.Id = T.TaxonomyId
                Where T.ObjectTypeId=40
                
		 --select * from @Taxonomy_table
		 '
if(@PublishDateType=1)
Begin
set @str = @str + '
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId =B.Id inner join  @Taxonomy_table T on P.Id = T.ObjectId where B.Id = ''' + convert(nvarchar(36),@BlogId) + ''' and P.PublishCount >= 1 and P.Status = 8 and  convert(varchar(10),PublishDate,21) between '''+ convert(varchar(10),@FromDate,21) + ''' and ''' + convert(varchar(10),@ToDate,21) + '''  order by ' + @OrderBy + ' ' + @i_Ordertype + ' End
				Else
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id where P.PublishCount >= 1 and P.Status = 8 and  B.Id = ''' + convert(nvarchar(36),@BlogId) + ''' and convert(varchar(10),PublishDate,21) between '''+ convert(varchar(10),@FromDate,21) + ''' and ''' + convert(varchar(10),@ToDate,21) + '''  order by ' + @OrderBy + ' ' + @i_Ordertype + '
					'

End

Else if(@PublishDateType=2)
Begin
declare 
@today datetime
set @today = GETUTCDATE()
print @today

set @str =  @str +  '
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id  inner join  @Taxonomy_table T on P.Id = T.ObjectId where B.Id = ''' + convert(nvarchar(36),@BlogId) + ''' and P.PublishCount >= 1 and P.Status = 8 and  convert(varchar(10),PublishDate,21) between '''+ convert(varchar(10),(@today - @DaysOlder), 21) + ''' and ''' + convert(varchar(10),@today,21) + '''  order by ' + @OrderBy + ' ' + @i_Ordertype + ' End
				Else
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id where P.PublishCount >= 1 and P.Status = 8 and B.Id = ''' + convert(nvarchar(36),@BlogId) + ''' and convert(varchar(10),PublishDate,21) between '''+ convert(varchar(10),(@today - @DaysOlder), 21) + ''' and ''' + convert(varchar(10),@today,21) + '''  order by ' + @OrderBy + ' ' + @i_Ordertype + '			
				'
End

Else
Begin
set @str  = @str +  '
			select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id inner join  @Taxonomy_table T on P.Id = T.ObjectId where B.Id = ''' + convert(nvarchar(36),@BlogId) + ''' and P.PublishCount >= 1  and P.Status = 8 order by ' + @OrderBy + ' ' + @i_Ordertype + ' End
		Else
			select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id where P.PublishCount >= 1 and B.Id = '''+ convert(nvarchar(36),@BlogId) + ''' and P.Status = 8   order by ' + @OrderBy + ' ' + @i_Ordertype + '  

			'
End
PRINT @str

EXECUTE sp_executesql @str 
return end
GO
PRINT 'Creating procedure Contact_SearchSiteAttributes'
GO
IF (OBJECT_ID('Contact_SearchSiteAttributes') IS NOT NULL)
	DROP PROCEDURE Contact_SearchSiteAttributes
GO
CREATE PROCEDURE [dbo].[Contact_SearchSiteAttributes]
    @SiteAttributes XML ,
    @ApplicationId UNIQUEIDENTIFIER
AS 
    BEGIN
        DECLARE @ContactSearch TABLE
            (ConditionId UNIQUEIDENTIFIER,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeValueID UNIQUEIDENTIFIER ,
              Value1 NVARCHAR(MAX) ,
              Value2 NVARCHAR(MAX) ,
              Operartor NVARCHAR(50) ,
              DataType NVARCHAR(50),
              ValueCount INT
            )

      
                SELECT  NEWID() as ConditionId,
						tab.col.value('@AttributeId', 'uniqueidentifier') as AttributeId ,                        
                        tab.col.value('@Value1', 'nvarchar(max)') as Value1,
                        tab.col.value('@Value2', 'nvarchar(max)') as Value2,
                        tab.col.value('@Operator', 'nvarchar(50)') as Operator,
                        'System.String' as DataType,
						col.query('AttributeValueId') AS XmlValue
				INTO    #tbltest
                FROM    @SiteAttributes.nodes('/SiteAttributes/AttributeSearchItem') tab ( col )


				      INSERT  INTO @ContactSearch
                ( ConditionId ,
                  AttributeId ,
                  AttributeValueID ,
                  Value1 ,
                  Value2 ,
                  Operartor ,
                  DataType ,
                  ValueCount
                )
                SELECT  T.ConditionId ,
                        T.AttributeId ,
                        pref.value('(text())[1]', 'uniqueidentifier') ,
                        T.Value1 ,
                        T.Value2 ,
                        T.Operator ,
                        T.DataType ,
                        COUNT(pref.value('(text())[1]', 'uniqueidentifier')) OVER ( PARTITION BY T.ConditionId,
                                                              T.AttributeId  ) AS ValueCount
                FROM    #tbltest T
                        OUTER APPLY XmlValue.nodes('AttributeValueId') AS Value ( pref )


        UPDATE  CS
        SET     CS.DataType = ADT.Type
        FROM    @ContactSearch CS
                INNER JOIN dbo.ATAttribute A ON A.Id = CS.AttributeId
                INNER JOIN dbo.ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId 


        DECLARE @CacheTable TABLE
            (
              SiteId UNIQUEIDENTIFIER ,
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              DataType NVARCHAR(50) ,
              VALUE NVARCHAR(4000)
            )
     
        INSERT  INTO @CacheTable
                ( SiteId ,
                  AttributeId ,
                  AttributeEnumId ,
                  DataType ,
                  VALUE
               
                )
                SELECT  CA.SiteId ,
                        CS.AttributeId ,
                        CA.AttributeEnumId ,
                        ADT.Type AS DataType ,
                        CA.Value
                FROM    @ContactSearch CS
                        INNER JOIN dbo.ATSiteAttributeValue CA ON CA.AttributeId = CS.AttributeId
                        INNER JOIN ATAttribute A ON A.ID = CA.AttributeId
                        INNER JOIN ATAttributeDataType ADT ON ADT.ID = A.AttributeDataTypeId



        DECLARE @LocalSiteId TABLE ( Id UNIQUEIDENTIFIER, AttributeValueId UNIQUEIDENTIFIER, ConditionId UNIQUEIDENTIFIER )
       
	    INSERT  INTO @LocalSiteId
                SELECT  
                        CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = '='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = '>'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = '>='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = '<'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = '<='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = '!='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = 'BETWEEN'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'CONTAINS'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.String'
                                                      AND CS.DataType = 'System.String'
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'Does Not Contain'
 -------------------------------------------------------------------------------------------------------------------------------------
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = '='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = '>'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = '>='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = '<'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = '<='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = '!='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = 'BETWEEN'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'CONTAINS'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND CAV.DataType = 'System.DateTime'
                                                      AND CS.DataType = 'System.DateTime'
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'Does Not Contain'
 
 ---------------------------------------------------------------------------------------------------------------------------------------       


 -------------------------------------------------------------------------------------------------------------------------------------
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND ( ( CAV.AttributeEnumId = CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE = CS.Value1
                                                          )
                WHERE   CS.Operartor = '='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE > CS.Value1
                WHERE   CS.Operartor = '>'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE >= CS.Value1
                WHERE   CS.Operartor = '>='
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE < CS.Value1
                WHERE   CS.Operartor = '<'
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE <= CS.Value1
                WHERE   CS.Operartor = '<='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND ( ( CAV.AttributeEnumId != CS.AttributeValueID
                                                              AND CS.AttributeValueID != dbo.GetEmptyGuid()
                                                            )
                                                            OR CAV.VALUE != CS.Value1
                                                          )
                WHERE   CS.Operartor = '!='
                UNION
                SELECT  CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE BETWEEN CS.Value1 AND CS.Value2
                WHERE   CS.Operartor = 'BETWEEN'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'CONTAINS'
                UNION
                SELECT   CAV.SiteId,
						CAV.AttributeEnumId,
						CS.ConditionId
                FROM    @ContactSearch CS
                        INNER JOIN @CacheTable CAV ON CAV.AttributeId = CS.AttributeId
                                                      AND ( ( CAV.DataType = 'System.Double'
                                                              OR CAV.DataType = 'System.Int32'
                                                            )
                                                            AND ( CS.DataType = 'System.Double'
                                                              OR CS.DataType = 'System.Int32'
                                                              )
                                                          )
                                                      AND CAV.VALUE NOT LIKE '%'
                                                      + CS.Value1 + '%'
                WHERE   CS.Operartor = 'Does Not Contain'
 
 --------------------------------------------------------------------------------------------------------------------------------------- 

        DECLARE @LocalTemp TABLE ( Id UNIQUEIDENTIFIER )
      --  INSERT  INTO @LocalTemp
                --SELECT DISTINCT
                --        CS.UserId
                --FROM    vw_ContactSite CS
                --        INNER JOIN @LocalSiteId LS ON LS.Id = CS.SiteId




						  INSERT  INTO @LocalTemp
                SELECT DISTINCT
                        CS.UserId
                FROM    vw_ContactSite CS
                        INNER JOIN (
						 SELECT DISTINCT
                        TV2.Id as SiteId
                FROM    ( SELECT    * ,
                                    COUNT(AttributeValueId) OVER ( PARTITION BY ConditionID,
                                                              Id ) AS ValueCount
                          FROM      ( SELECT  DISTINCT
                                                *
                                      FROM      @LocalSiteId
                                    ) TV
                        ) TV2
                        INNER JOIN @ContactSearch CS ON CS.ConditionId = TV2.ConditionId
                                                        AND CS.ValueCount = TV2.ValueCount
						)TV on TV.SiteId = CS.SiteId



        DECLARE @currentRecords INT
        SET @currentRecords = ( SELECT  COUNT(Id)
                                FROM    #tempContactSearchOutput
                              )
       
        IF @currentRecords > 0 
            BEGIN
                DELETE  T
                FROM    #tempContactSearchOutput t
                        LEFT JOIN @LocalTemp LT ON LT.Id = T.Id
                WHERE   LT.Id IS NULL
            END
        ELSE 
            BEGIN 
                INSERT  INTO #tempContactSearchOutput
                        SELECT  Id
                        FROM    @LocalTemp
            END
        SELECT  @@rowcount
     
    END

GO
IF (OBJECT_ID('Content_GetContentForPageList') IS NOT NULL)
	DROP PROCEDURE Content_GetContentForPageList
GO
CREATE PROCEDURE [dbo].[Content_GetContentForPageList]
(
	@PageMapNodeId			uniqueidentifier = null,
	@XmlFormId				uniqueidentifier = null,
	@IncludeChild			bit = 0,
	@PageNumber				int = 1,
	@PageSize				int = null,
	@MaxRecords				int = null,
	@TaxonomyIds			nvarchar(max) = null,
	@IncludeChildTaxonomy	bit = null,
	@MatchAllTaxonomyIds	bit = null,
	@SortClause				nvarchar(max) = null,
	@FilterClause			nvarchar(max) = null,
	@ContainerId			uniqueidentifier = null,
	@UserRoles				xml = null,
	@IsSystemUser			bit = 0,
	@PageMapNodeIds			xml = null,
	@PageDefinitionIds		xml = null,
	@DeviceId				uniqueidentifier = null,
	@AudienceIds			nvarchar(max) = null,
	@SiteId					uniqueidentifier = null
)
AS
BEGIN
	DECLARE @ContentTypeId int
	IF @XmlFormId IS NULL
		SET @ContentTypeId = 7
	ELSE
		SET @ContentTypeId = 13
		
	IF @IsSystemUser IS NULL
		SET @IsSystemUser = 0
	
	DECLARE @tbPage TABLE(Id uniqueidentifier)
	IF @PageDefinitionIds IS NOT NULL
		INSERT INTO @tbPage
			SELECT PD.P.value('text()[1]','uniqueidentifier') FROM @PageDefinitionIds.nodes('/GenericCollectionOfGuid/guid') PD(P)
	ELSE IF @PageMapNodeIds IS NOT NULL
		INSERT INTO @tbPage
			SELECT P.PageDefinitionId FROM PageDefinition P 
			JOIN PageMapNodePageDef PMD ON PMD.PageDefinitionId = P.PageDefinitionId
			JOIN @PageMapNodeIds.nodes('/GenericCollectionOfGuid/guid') PM(N)
					ON PMD.PageMapNodeId = PM.N.value('text()[1]','uniqueidentifier')
	ELSE IF @PageMapNodeId IS NOT NULL
	BEGIN
		IF @IncludeChild = 1
		BEGIN						
			DECLARE @LftValue int, @RgtValue int, @PSiteId uniqueidentifier
			SELECT @LftValue = LftValue, @RgtValue = RgtValue, @PSiteId = SiteId from PageMapNode WHERE PageMapNodeId = @PageMapNodeId
			INSERT INTO @tbPage
				SELECT DISTINCT P.PageDefinitionId FROM PageDefinition P 
					JOIN PageMapNodePageDef PMD ON PMD.PageDefinitionId = P.PageDefinitionId
					JOIN PageMapNode PM ON PMD.PageMapNodeId  = PM.PageMapNodeId AND
						(@IsSystemUser = 1 OR PM.MenuStatus = 0)
					WHERE  LftValue >= @LftValue AND RgtValue <= @RgtValue AND PM.SiteId = @PSiteId
		END
		ELSE
		BEGIN
			INSERT INTO @tbPage
				SELECT DISTINCT P.PageDefinitionId FROM PageDefinition P
					JOIN PageMapNodePageDef PMD ON PMD.PageDefinitionId = P.PageDefinitionId 
					JOIN PageMapNode PM ON PMD.PageMapNodeId = PM.PageMapNodeId AND
						(@IsSystemUser = 1 OR PM.MenuStatus = 0)
				WHERE PM.PageMapNodeId = @PageMapNodeId
		END
	END
	ELSE IF @XmlFormId IS NOT NULL
		INSERT INTO @tbPage SELECT PageDefinitionId FROM PageDefinition
	ELSE
		RETURN
		
	DECLARE @tbFilteredPage TABLE(Id uniqueidentifier)
	IF @TaxonomyIds IS NULL
		INSERT INTO @tbFilteredPage SELECT Id FROM @tbPage
	ELSE
	BEGIN
		DECLARE @tbTaxonomy TABLE(Id uniqueidentifier, LftValue bigint, RgtValue bigint)		
		IF @IncludeChildTaxonomy IS NOT NULL AND @IncludeChildTaxonomy = 1
		BEGIN
			INSERT INTO @tbTaxonomy 
			SELECT Id, LftValue, RgtValue FROM COTaxonomy C Inner JOIN dbo.SplitGuid (@TaxonomyIds, ',') TI ON C.Id = TI.Items
			INSERT INTO @tbTaxonomy 
				SELECT C.Id, C.LftValue, C.RgtValue FROM @tbTaxonomy P INNER JOIN COTaxonomy C ON C.LftValue Between P.LftValue AND P.RgtValue 
		END
		ELSE
			INSERT INTO @tbTaxonomy SELECT TI.Items,0,0 FROM dbo.SplitGuid (@TaxonomyIds, ',') TI
		
		IF @MatchAllTaxonomyIds = 1
			INSERT INTO @tbFilteredPage
			SELECT T.Id FROM @tbPage T
				JOIN COTaxonomyObject TAO ON TAO.ObjectId = T.Id AND TAO.Status = 1 AND TAO.ObjectTypeId = 8
				JOIN @tbTaxonomy Tx ON Tx.Id =  TAO.TaxonomyId
			GROUP BY T.Id 
			HAVING COUNT(*) >= (SELECT COUNT(*) FROM @tbTaxonomy)
		ELSE
			INSERT INTO @tbFilteredPage
			SELECT T.Id FROM @tbPage T
				JOIN COTaxonomyObject TAO ON TAO.ObjectId = T.Id AND TAO.Status = 1 AND TAO.ObjectTypeId = 8
				JOIN @tbTaxonomy Tx ON Tx.Id =  TAO.TaxonomyId
	END
	
	IF @IsSystemUser != 1 AND @UserRoles IS NOT NULL
	BEGIN
		DELETE FROM @tbFilteredPage 
		WHERE Id NOT IN
		(
			SELECT S.ObjectId
			FROM @tbFilteredPage T 
				LEFT JOIN USSecurityLevelObject S ON S.ObjectId = T.Id 
			WHERE S.SecurityLevelId IS NULL
				OR S.SecurityLevelId IN (SELECT U.R.value('text()[1]','int') FROM @UserRoles.nodes('/ArrayOfAnyType/anyType') U(R))
		)
	END
		
	IF @DeviceId IS NULL
		SET @DeviceId = [dbo].[GetDesktopDeviceId](@SiteId)
	IF @AudienceIds IS NULL
		SET @AudienceIds = [dbo].[GetDesktopAudienceSegmentId](@SiteId)
			
	CREATE TABLE #PageContent (PageId uniqueidentifier, ContentId uniqueidentifier, inWFContentId uniqueidentifier)
	
	IF EXISTS (SELECT P.PageDefinitionId from PageDefinitionSegment P
	INNER JOIN @tbPage T ON T.Id = P.PageDefinitionId
	Group By P.PageDefinitionId
	Having Count(P.PageDefinitionId) > 1)

		INSERT INTO #PageContent
			SELECT DISTINCT P.PageDefinitionId, V.ContentId, V.inWFContentId FROM PageDefinition P 
				JOIN @tbFilteredPage T ON P.PageDefinitionId = T.Id 
				JOIN PageDefinitionContainer V ON P.PageDefinitionId = V.PageDefinitionId AND
					V.ContentTypeId = @ContentTypeId AND
					(@ContainerId IS NULL OR V.ContainerId = @ContainerId) AND
					(V.PageDefinitionSegmentId = [dbo].[GetPageDefinitionSegmentId](P.PageDefinitionId, @DeviceId, @AudienceIds))
				JOIN COContent C ON C.Id = V.ContentId AND
					(@XmlFormId IS NULL OR C.TemplateId = @XmlFormId)
			WHERE (@SiteId IS NULL OR P.SiteId = @SiteId)
	
	ELSE -- No need to have a condition to check for pagedefinitionsegmentid, there would be only record in PageDefinitionContainer table
	
		INSERT INTO #PageContent
			SELECT DISTINCT P.PageDefinitionId, V.ContentId, V.inWFContentId FROM PageDefinition P 
				JOIN @tbFilteredPage T ON P.PageDefinitionId = T.Id 
				JOIN PageDefinitionContainer V ON P.PageDefinitionId = V.PageDefinitionId AND
					V.ContentTypeId = @ContentTypeId AND
					(@ContainerId IS NULL OR V.ContainerId = @ContainerId) 
					--(V.PageDefinitionSegmentId = [dbo].[GetPageDefinitionSegmentId](P.PageDefinitionId, @DeviceId, @AudienceIds)) AND
				JOIN COContent C ON C.Id = V.ContentId AND
					(@XmlFormId IS NULL OR C.TemplateId = @XmlFormId)
			WHERE (@SiteId IS NULL OR P.SiteId = @SiteId)
	
	
	IF @@ROWCOUNT > 0 
	BEGIN
		IF @PageNumber <= 0
			SET @PageNumber = 1

		IF @FilterClause IS NULL
			SET @FilterClause = ''
		IF @SortClause IS NULL
			SET @SortClause = ''
			
		IF @IsSystemUser = 1
			Update #PageContent SET ContentId = inWFContentId
		
		DECLARE @SQLStmt nvarchar(max)
		SET @SQLStmt = N'
			;WITH ResultEntries AS ( 
				SELECT ROW_NUMBER() OVER (ORDER BY ' + @SortClause + ' PM.DisplayOrder ASC) AS Row,
					P.PageDefinitionId as PageId,
					C.Id AS ContentId,
					C.Title AS ContentTitle,
					C.Description AS ContentDescription, 
					C.Text, 
					C.XMLString,
					P.Title as PageTitle,
					PM.DisplayOrder,
					P.PublishDate
				FROM #PageContent T JOIN COContent C ON C.Id = T.ContentId  
						JOIN PageDefinition P ON P.PageDefinitionId = T.PageId
						JOIN PageMapNodePageDef PM on PM.PageDefinitionId = P.PageDefinitionId
				WHERE C.Status = 1 
						' + @FilterClause + '
			), TotalEntries AS (SELECT count(*) Cnt FROM ResultEntries)	
			
			SELECT R.*,
					CASE When T.Cnt > @MaxRecords THEN @MaxRecords
					ELSE T.Cnt
					END AS TotalRecords
			FROM ResultEntries R CROSS JOIN TotalEntries T
			WHERE (@MaxRecords IS NULL OR Row <= @MaxRecords) AND (@PageSize IS NULL OR Row between 
					(@PageNumber - 1) * @PageSize + 1 and @PageNumber*@PageSize)
		'

		EXEC sp_executesql @SQLStmt,
				N'@PageSize int,
					@PageNumber int,
					@MaxRecords int',
				@PageSize,
				@PageNumber,
				@MaxRecords
					
		--PRINT @SQLStmt
	END
	
	DROP TABLE #PageContent
END

GO
PRINT 'Creating procedure ScheduleDto_Get'
GO
IF (OBJECT_ID('ScheduleDto_Get') IS NOT NULL)
	DROP PROCEDURE ScheduleDto_Get
GO
CREATE PROCEDURE [dbo].[ScheduleDto_Get]
(
	@Id uniqueidentifier =NULL,
	@SiteId uniqueidentifier =NULL
)
AS
BEGIN

	 SELECT 
	   A.[Id]  
	  ,A.Title  
	  ,A.Description  
      ,cast(convert(varchar(12), dbo.ConvertTimeFromUtc(StartTime,@SiteId),101)  as dateTime)  StartDate  
      ,cast(convert(varchar(12), dbo.ConvertTimeFromUtc(EndDate,@SiteId),101)  as dateTime)  EndDate     
      ,dbo.ConvertTimeFromUtc(StartTime,@SiteId) StartTime    
      ,dbo.ConvertTimeFromUtc(EndTime,@SiteId) EndTime
      ,[MaxOccurrence]  
      ,[Type]  
   ,[NextRunTime]  
   ,[LastRunTime]   
   ,[ScheduleTypeId]  
   ,A.[Status]  
   ,A.[CreatedBy]  
	,dbo.ConvertTimeFromUtc(A.CreatedDate,@SiteId) CreatedDate   
    ,A.[ModifiedBy]  
    ,dbo.ConvertTimeFromUtc(A.ModifiedDate,@SiteId) ModifiedDate 
   ,FN.UserFullName CreatedByFullName
   ,MN.UserFullName ModifiedByFullName
  FROM [dbo].[TASchedule]  A
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
  Where  (@Id IS NULL OR A.Id = @Id)
  --AND S.Status =isnull(@Status,Status)  
  AND ApplicationId =isnull(@SiteId,ApplicationId)  

END
GO
PRINT 'Creating procedure CampaignRunHistoryDto_SaveTimeZone'
GO
IF (OBJECT_ID('CampaignRunHistoryDto_SaveTimeZone') IS NOT NULL)
	DROP PROCEDURE CampaignRunHistoryDto_SaveTimeZone
GO
CREATE  PROCEDURE [dbo].[CampaignRunHistoryDto_SaveTimeZone]
(	
	@CampaignRunId UNIQUEIDENTIFIER = NULL OUTPUT,
    @CampaignId UNIQUEIDENTIFIER = NULL,
    @StartTime DATETIME = NULL,
    @SiteId UNIQUEIDENTIFIER
)
AS
BEGIN	

INSERT INTO [dbo].[MKCampaignRunTimeZoneSites]
           ([CampaignRunId]
           ,[CampaignId]
           ,[StartTime]
           ,[SiteId]
           ,[IsProcessed]
           ,[ProcessedContactCount])
     VALUES
           (@CampaignRunId
           ,@CampaignId
           ,@StartTime
           ,@SiteId
           ,0
           ,0)
END



GO
PRINT 'Creating procedure CampaignDto_Save'
GO
IF (OBJECT_ID('CampaignDto_Save') IS NOT NULL)
	DROP PROCEDURE CampaignDto_Save
GO
CREATE PROCEDURE [dbo].[CampaignDto_Save]
(
	
	@Id UNIQUEIDENTIFIER = NULL OUT,
	@Title NVARCHAR(MAX) = NULL,
	@Description NVARCHAR(MAX) = NULL,
	@Type INT ,
	@CampaignGroupId UNIQUEIDENTIFIER = NULL,
	@Status INT = NULL,
	@SenderName NVARCHAR(MAX) = NULL,
	@SenderEmail NVARCHAR(MAX) = NULL,
	@ConfirmationEmail NVARCHAR(MAX) = NULL,
	@CreatedDate DATETIME,
	@CreatedBy NVARCHAR(MAX) = NULL,
	@CampaignId UNIQUEIDENTIFIER = NULL,
	@SiteId UNIQUEIDENTIFIER ,
	@EmailId UNIQUEIDENTIFIER = NULL,
	@Emails XML = NULL,
	@ScheduleId UNIQUEIDENTIFIER = NULL,
	@WorkflowId UNIQUEIDENTIFIER = NULL,
	@WorkflowStatus INT = NULL,
	@UniqueRecipientsOnly BIT = NULL,
	@AutoUpdateLists BIT = NULL,
	@SendToNotTriggered BIT = NULL,	
	@UseLocalTimeZoneToSend BIT =0,
	@SenderNameProperty nvarchar(250) =null,
	@SenderEmailProperty nvarchar(250) =null
)
AS
BEGIN
	DECLARE @UtcNow datetime,@ModifiedBy uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	SET @ModifiedBy = NULL
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO MKCampaign
		(
			Id,
			Title,
			Description,
			Type,
			CampaignGroupId,
			Status,
			SenderName,
			SenderEmail,
			ConfirmationEmail,
			CreatedDate,
			CreatedBy,
			ModifiedDate,
			ModifiedBy,			
			ApplicationId,
			SenderNameProperty,
			SenderEmailProperty
				
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			@Type,
			@CampaignGroupId,
			@Status,
			@SenderName,
			@SenderEmail,
			@ConfirmationEmail,
			@CreatedDate,
			@CreatedBy,
			@UtcNow,
			@ModifiedBy,
			@SiteId,
			@SenderNameProperty,
			@SenderEmailProperty
		)
		
		INSERT INTO MKCampaignAdditionalInfo
		(
			Id,
			CampaignId,
			CostPerEmail,
			ScheduleId,
			WorkflowStatus,
			UniqueRecipientsOnly,
			AutoUpdateLists,
			LastPublishDate,
			WorkflowId,
			AuthorId,
			SendToNotTriggered,
			UseLocalTimeZoneToSend
		)
		VALUES
		(
			NEWID(),
			@Id,
			NULL,
			@ScheduleId,
			@WorkflowStatus,
			@UniqueRecipientsOnly,
			@AutoUpdateLists,
			NULL,
			@WorkflowId,
			NULL,
			@SendToNotTriggered,
			@UseLocalTimeZoneToSend
		)		

		INSERT INTO MKCampaignEmail
		(
			Id,
			CampaignId,
			EmailSubject,
			EmailHtml,
			CMSPageId,
			EmailText,
			Sequence,
			TimeValue,
			TimeMeasurement,
			EmailHtmlPageVersion,
			PageMapNodeId,
			EmailContentType,
			EmailContentTypePriority
		)
		SELECT
			NEWID(),
			@Id,
			Emails.email.value('@EmailSubject', 'nvarchar(max)'),
			Emails.email.value('@EmailHtml', 'nvarchar(max)'),
			Emails.email.value('@CMSPageId', 'uniqueidentifier'),
			Emails.email.value('@EmailText', 'nvarchar(max)'),
			ISNULL(Emails.email.value('@Sequence', 'int'), 0),
			Emails.email.value('@TimeValue', 'int'),
			Emails.email.value('@TimeMeasurement', 'nvarchar(max)'),
			Emails.email.value('@EmailHtmlPageVersion', 'nvarchar(max)'),
			Emails.email.value('@PageMapNodeId', 'uniqueidentifier'),
			Emails.email.value('@EmailContentType', 'int'),
			Emails.email.value('@EmailContentTypePriority', 'int')
		FROM @Emails.nodes('Emails/Email') AS Emails(email)
	END
	
	ELSE
	BEGIN
		UPDATE MKCampaign
	    SET [Title] = @Title
			,[Description] = @Description
			,[Type] = @Type
			,[CampaignGroupId] = @CampaignGroupId
			,[Status] = @Status
			,[SenderName] = @SenderName
			,[SenderEmail] = @SenderEmail
		    ,[ConfirmationEmail] = @ConfirmationEmail
		    ,[CreatedBy] =@CreatedBy
		    ,[CreatedDate] = @CreatedDate
		    ,[ModifiedBy] = @ModifiedBy
		    ,[ModifiedDate] = @UtcNow
		    ,[ApplicationId] = @SiteId
		    ,[SenderNameProperty] = @SenderNameProperty
			,[SenderEmailProperty] = @SenderEmailProperty
			
		WHERE Id = @Id	
	  
		UPDATE MKCampaignAdditionalInfo
		SET CostPerEmail = NULL,
			ScheduleId = @ScheduleId,
			WorkflowStatus = @WorkflowStatus,
			UniqueRecipientsOnly = @UniqueRecipientsOnly,
			AutoUpdateLists = @AutoUpdateLists,
			LastPublishDate = NULL,
			WorkflowId = @WorkflowId,
			AuthorId = NULL,
			SendToNotTriggered = @SendToNotTriggered
			,[UseLocalTimeZoneToSend] = @UseLocalTimeZoneToSend
		WHERE CampaignId = @Id
		
		;WITH EmailXML AS
		(
			SELECT 
				Emails.email.value('@EmailSubject', 'nvarchar(max)') AS EmailSubject,
				Emails.email.value('@EmailHtml', 'nvarchar(max)') AS EmailHtml,
				Emails.email.value('@CMSPageId', 'uniqueidentifier') AS CMSPageId,
				Emails.email.value('@EmailText', 'nvarchar(max)') AS EmailText,
				Emails.email.value('@Sequence', 'int') AS Sequence,
				Emails.email.value('@TimeValue', 'int') AS TimeValue,
				Emails.email.value('@TimeMeasurement', 'nvarchar(max)') AS TimeMeasurement,
				Emails.email.value('@EmailHtmlPageVersion', 'nvarchar(max)') as EmailHtmlPageVersion,
				Emails.email.value('@PageMapNodeId', 'uniqueidentifier') as PageMapNodeId,
				Emails.email.value('@EmailContentType', 'int') AS EmailContentType,
				Emails.email.value('@EmailContentTypePriority', 'int') AS EmailContentTypePriority
			FROM @Emails.nodes('Emails/Email') AS Emails(email)
		)
		UPDATE e
		SET EmailSubject = ISNULL(x.EmailSubject, e.EmailSubject),
			EmailHtml = ISNULL(x.EmailHtml, e.EmailHtml),
			CMSPageId = ISNULL(x.CMSPageId, e.CMSPageId),
			EmailText = ISNULL(x.EmailText, e.EmailText),
			Sequence = ISNULL(x.Sequence, e.Sequence),
			TimeValue = ISNULL(x.TimeValue, e.TimeValue),
			TimeMeasurement = ISNULL(x.TimeMeasurement, e.TimeMeasurement),
			EmailHtmlPageVersion = ISNULL(x.EmailHtmlPageVersion, e.EmailHtmlPageVersion),
			PageMapNodeId = ISNULL(x.PageMapNodeId, e.PageMapNodeId),
			EmailContentType = ISNULL(x.EmailContentType, e.EmailContentType),
			EmailContentTypePriority = ISNULL(x.EmailContentTypePriority, e.EmailContentTypePriority)
		FROM MKCampaignEmail e, EmailXML x
	    WHERE CampaignId = @Id
	END
END

Go


PRINT 'Creating procedure SiteDto_Get'
GO
IF (OBJECT_ID('SiteDto_Get') IS NOT NULL)
	DROP PROCEDURE SiteDto_Get
GO
CREATE PROCEDURE [dbo].[SiteDto_Get]
(
	@Id						UNIQUEIDENTIFIER = NULL,
	@Ids					VARCHAR(MAX)=NULL,
	@Status					INT = NULL,
	@MasterSiteId			UNIQUEIDENTIFIER = NULL,
	@IncludeVariantSites	BIT = NULL,
	@PageNumber				INT = NULL,
	@PageSize				INT = NULL,
	@MaxRecords				INT = NULL,
	@SortBy					NVARCHAR(100) = NULL,  
	@SortOrder				NVARCHAR(10) = NULL,
	@Query					NVARCHAR(MAX) = NULL
)
AS
BEGIN
	IF @IncludeVariantSites IS NULL
		SET @IncludeVariantSites = 1

	IF @Status IS NULL
		SET @Status = 1
	
	DECLARE	@PageLowerBound	INT,
			@PageUpperBound	INT,
			@SortClause		NVARCHAR(500)
	
	SET @PageLowerBound = @PageSize * @PageNumber

	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (
		Id			UNIQUEIDENTIFIER,
		RowNumber	INT
	)

	DECLARE @filterIds TABLE(Id UNIQUEIDENTIFIER)
	IF @Ids IS NOT NULL
		INSERT INTO @filterIds
		Select Items FROM dbo.SplitGUID(@Ids,',') 


	IF @Query IS NULL
	BEGIN
		IF (@SortOrder IS NULL)
			SET @SortOrder = 'ASC'

		IF (@SortBy IS NOT NULL)
			SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)	
	
		INSERT	INTO @tbIds
			SELECT	Id,  ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'Title ASC' THEN Title END ASC,
							CASE WHEN @SortClause = 'Title DESC' THEN Title END DESC,
							CASE WHEN @SortClause IS NULL THEN Title END ASC	
						) AS RowNumber
			FROM	SISite
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
			EXEC sp_executesql @Query
	END
			
	DECLARE @tbPagedResults TABLE(
		Id				UNIQUEIDENTIFIER,
		RowNumber		INT,
		TotalRecords	INT
	)
	
	;WITH CTE AS(
		SELECT	ROW_NUMBER() OVER (ORDER BY T.RowNumber) AS RowNumber,
				COUNT(T.Id) OVER () AS TotalRecords,
				S.Id AS Id
		FROM	SISite AS S
				INNER JOIN @tbIds AS T ON T.Id = S.Id
		WHERE	(@Id IS NULL OR S.Id = @Id) AND
				(@Ids is NULL OR EXISTS (Select 1 FROM @filterIds F Where F.Id =S.Id) ) AND
				(@Status IS NULL OR S.Status = @Status) AND
				(@MasterSiteId IS NULL OR S.Id = @MasterSiteId OR S.MasterSiteId = @MasterSiteId) AND
				(@IncludeVariantSites = 1 OR S.MasterSiteId = S.Id OR S.MasterSiteId IS NULL)
	)

	INSERT INTO @tbPagedResults
	SELECT	Id, RowNumber, TotalRecords
	FROM	CTE
	WHERE	(@PageLowerBound IS NULL OR @PageUpperBound IS NULL OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)) AND
			(@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT		S.*,
				T.RowNumber,
				T.TotalRecords
	FROM		SISite AS S
				INNER JOIN	@tbPagedResults AS T ON T.Id = S.Id
	ORDER BY RowNumber
END
GO

PRINT 'Creating procedure ContactDto_UpdateSync'
GO
IF (OBJECT_ID('ContactDto_UpdateSync') IS NOT NULL)
	DROP PROCEDURE ContactDto_UpdateSync
GO
CREATE PROCEDURE [dbo].[ContactDto_UpdateSync]
(
	@ModifiedBy	 uniqueidentifier = NULL ,
	@SyncXml xml
	
)
AS
BEGIN
	CREATE Table #tmpContacts(ContactId uniqueidentifier,LastSynced DateTime)
	Declare @updated table(Id uniqueidentifier)
	INSERT INTO #tmpContacts(ContactId,LastSynced)
	SELECT DISTINCT T.c.value('(Key/text())[1]','uniqueidentifier'), T.c.value('(Value/text())[1]','DateTime')
		FROM @SyncXml.nodes('/ArrayOfKV/KV') T(c)

		UPDATE M SET M.LastSynced = T.LastSynced 
		OUTPUT inserted.Id INTO @updated
		FROM MKContact M 
		INNER JOIN #tmpContacts T on M.Id =T.ContactId

		UPDATE S SET S.LastSynced = T.LastSynced
		OUTPUT inserted.Id INTO @updated
		FROM GLSyncLog S
		INNER JOIN #tmpContacts T on S.Id =T.ContactId

		DELETE T 
		FROM #tmpContacts T
		INNER JOIN @updated U ON T.ContactId = U.Id

		INSERT INTO GLSyncLog(Id,Type,LastSynced)
		Select ContactId,211, LastSynced from #tmpContacts

END
GO
PRINT 'Creating procedure ContactDto_GetCount'
GO
IF (OBJECT_ID('ContactDto_GetCount') IS NOT NULL)
	DROP PROCEDURE ContactDto_GetCount
GO
CREATE  PROCEDURE [dbo].[ContactDto_GetCount]
(

	@Status			int = NULL ,
	@SiteId			uniqueidentifier = NULL,
	@Synced		int=0
)AS
BEGIN
		Select COUNT_BIG(*) from MKContact C
		INNER JOIN MKContactSite CS on C.Id =CS.ContactId
		Where (@Status is null OR cs.Status=@Status) AND (@SiteId is null OR @SiteId =dbo.GetEmptyGUID() OR cs.SiteId =@SiteId)
		AND (
			@Synced=0 
			OR (@Synced=1 AND C.LastSynced is not null AND  isnull(C.ModifiedDate,c.CreatedDate) < C.LastSynced)
			OR (@Synced=2 AND (C.LastSynced IS NULL OR C.ModifiedDate>C.LastSynced ))
			)
END

GO
PRINT 'Altering procedure DistributionList_GetContactsWithProfile'
GO
ALTER PROCEDURE [dbo].[DistributionList_GetContactsWithProfile](
	@ListId uniqueidentifier, 
	@ApplicationId uniqueidentifier,
	@TotalRecords int out)
As
Begin

Declare @tblContacts Table
	(
		Id uniqueidentifier,
		UserId uniqueidentifier,
		FirstName varchar(50),
		MiddleName varchar(50),
		LastName varchar(50),
		CompanyName varchar(50),
		BirthDate datetime,
		Gender varchar(50),
		AddressId uniqueidentifier,
		Status int,
		HomePhone varchar(50),
		MobilePhone varchar(50),
		OtherPhone varchar(50),	
		ImageId     uniqueidentifier null,  	
		Notes varchar(max),
		Email varchar(256),
		ContactType int,
		ContactSourceId int,
		TotalRecords int  null
	)


	Declare @listType bit
	Select @listType = isnull(ListType,0) from TADistributionLists
	Where Id= @ListId
	
	declare @contactIds varchar(max)

	if @listType = 0  --- manual
		begin
			Select                     C.[UserId]
                                ,C.[FirstName]
                                ,C.[MiddleName]
                                ,C.[LastName]
                                ,C.[CompanyName]
                                ,BirthDate AS BirthDate 
                                ,C.[Gender]
                                ,C.[AddressId]
                                ,C.[Status]
                                ,C.[HomePhone]
                                ,C.[MobilePhone]
                                ,C.[OtherPhone]
                                ,C.ImageId   
                                ,C.[Notes]
                                ,C.[Email]
                                ,C.[ContactType]
                                ,C.[ContactSourceId]
                        from dbo.vw_contacts C
                        inner join TADistributionListUser TAL ON
                                    C.UserId = TAL.UserID And  TAL.DistributionListId=@ListId AND ContactListSubscriptionType != 3

		Select @TotalRecords = count(*) from dbo.TADistributionListUser where DistributionListId= @ListId AND ContactListSubscriptionType != 3


		select p.* from UsMemberProfile p join TADistributionListUser c on c.UserId = p.UserId AND ContactListSubscriptionType != 3
		join TADistributionLists u on c.DistributionListId = @ListId

		end
	else
		begin
			Declare @queryXml xml
			SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
					b.Id = a.SearchQueryId and a.DistributionListId=@ListId 
			insert into @tblContacts exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@ApplicationId, @MaxRecords=0, @ContactListId = @ListId, @TotalRecords=@TotalRecords output

			select * from @tblContacts
			select p.* from UsMemberProfile p join @tblContacts c on c.UserId = p.UserId

		end
End

GO
PRINT 'Altering procedure Site_Save'
GO
ALTER PROCEDURE [dbo].[Site_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@URL				xml,
	@VirtualPath		nvarchar(4000)=null,
	@ParentSiteId		uniqueidentifier=null,
	@SiteType 	        int=null,
	@Keywords	        nvarchar(4000)=null,
	@Status				int=null,
	@SendNotification	bit=null,
	@SubmitToTranslation bit=null,
	@AllowMasterWebSiteUser bit=null,
	@ImportAllAdminPermission bit=null,
	@Prefix			nvarchar(5) = null, 
	@UICulture			nvarchar(10) = null,
	@LoginUrl		nvarchar(max) =null,
	@DefaultURL		nvarchar(max) =null,
	@NotifyOnTranslationError bit = null,
	@TranslationEnabled bit = null,
	@TranslatePagePropertiesByDefault bit = null,
	@AutoImportTranslatedSubmissions bit = null,
	@DefaultTranslateToLanguage nvarchar(10) = null,
	@PageImportOptions int =null,
	@ImportContentStructure bit =null,
	@ImportFileStructure bit=null,
	@ImportImageStructure bit=null,
	@ImportFormsStructure bit=null,
	@SiteGroups nvarchar(4000) = null,
	@MasterSiteId		uniqueidentifier=null,
	@PrimarySiteUrl nvarchar(max) =null,
	@CustomAttribute xml = null,
	@ThemeId UNIQUEIDENTIFIER = NULL,
	@TimeZone nvarchar(255)=NULL
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
Declare @temp table(url1 nvarchar(max))
Declare @displayTitle varchar(512), @pageMapNodeDescription varchar(512), @pageMapNodeXml xml, @pageMapNodeId uniqueidentifier
Declare @LftValue int,@RgtValue int
Begin
--********************************************************************************
-- code
--********************************************************************************
	
	SET @displayTitle = @Title
	SET @pageMapNodeDescription = @Title

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	IF(@ThemeId IS NOT NULL AND NOT EXISTS (Select 1 FROM SITheme where Id = @ThemeId))
		SET @ThemeId = NULL


/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  SISite where  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end

	set @Now = getdate()
	--set @Status= dbo.GetActiveStatus()



	if (@Id is null)			-- new insert
	begin
	
	IF (@ParentSiteId is not null AND @ParentSiteId <> '00000000-0000-0000-0000-000000000000' )  
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue
		from SISite
		Where Id=@ParentSiteId and MasterSiteId=@MasterSiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END

		Update SISite Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
								RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 


		set @stmt = 'Site Insert'
		set @Id = newid();
		IF @MasterSiteId IS NULL OR @MasterSiteId = dbo.GetEmptyGUID()
			SET @MasterSiteId = @Id

		insert into SISite  (
					   Id,	
					   Title,
                       Description,
                       CreatedBy,
                       CreatedDate,
					   Status,
                       URL,
                       VirtualPath,
					   ParentSiteId,
					   Type,
                       Keywords,
                       HostingPackageInstanceId,
                       SendNotification,
                       SubmitToTranslation,
                       AllowMasterWebSiteUser,
                       ImportAllAdminPermission,
                       Prefix, 
                       UICulture, 
                       LoginUrl, 
                       DefaultURL,
                       NotifyOnTranslationError,
                       TranslationEnabled,
                       TranslatePagePropertiesByDefault,
                       AutoImportTranslatedSubmissions,
                       DefaultTranslateToLanguage,
                       PageImportOptions,
                       ImportContentStructure,
                       ImportFileStructure,
                       ImportImageStructure,
                       ImportFormsStructure,
                       MasterSiteId,
                       PrimarySiteUrl,
                       LftValue,
                       RgtValue,
                       CustomAttribute,
					   ThemeId,
					   TimeZone) 
					values (
						@Id,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@URL,
						@VirtualPath,
						@ParentSiteId,
						@SiteType,
						@Keywords,
						0,
						@SendNotification,
						@SubmitToTranslation,
						@AllowMasterWebSiteUser,
						@ImportAllAdminPermission, 
						@Prefix, 
						@UICulture, 
						@LoginUrl, 
						@DefaultURL,
						@NotifyOnTranslationError,
						@TranslationEnabled,
						@TranslatePagePropertiesByDefault,
						@AutoImportTranslatedSubmissions,
						@DefaultTranslateToLanguage,
						@PageImportOptions,
						@ImportContentStructure,
						@ImportFileStructure,
						@ImportImageStructure,
						@ImportFormsStructure,
						@MasterSiteId,
						@PrimarySiteUrl,
						@RgtValue,
						@RgtValue + 1,
						@CustomAttribute,
						@ThemeId,
						@TimeZone)

		-- Inserting in SiteGroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')


			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
         end
         else			-- update
         begin
			set @stmt = 'Site Update'
			
			Update HSStructure Set Title=@Title Where Id=@Id
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			--SELECT @pageMapNodeXml =  cast((PageMapNodexml + '</pageMapNode>') as xml), @pageMapNodeId = PageMapNodeId FROM PageMapNode Where SiteId = PageMapNodeId And SiteId = @Id								

			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@displayTitle)[1]
			--				  with
			--				(sql:variable("@displayTitle"))')
			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@description)[1]
			--				  with
			--				(sql:variable("@pageMapNodeDescription"))')			
			
			Update PageMapNode set displaytitle=@Title
			Where PageMapNodeId = @pageMapNodeId ANd SiteId = @Id
			-- Here we are not updating the friendly url
						
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			set @rowcount= 0
			
			select * from SISite where Id=@Id
			update	SISite  with (rowlock)	set 
            Title =@Title,
            Description=@Description,
            ModifiedBy=@ModifiedBy,
            ModifiedDate=@Now,
            Status=@Status,
            URL=@URL,
            VirtualPath=@VirtualPath,
            ParentSiteId=@ParentSiteId,
            Type=@SiteType,
            Keywords= @Keywords,
            SendNotification = @SendNotification,
            SubmitToTranslation = @SubmitToTranslation,
            AllowMasterWebSiteUser=@AllowMasterWebSiteUser,
			ImportAllAdminPermission=@ImportAllAdminPermission,
			Prefix = @Prefix, 
			UICulture = @UICulture,
			LoginUrl = @LoginUrl, 
			DefaultURL =@DefaultURL,
			NotifyOnTranslationError = @NotifyOnTranslationError,
			TranslationEnabled = @TranslationEnabled,
			TranslatePagePropertiesByDefault = @TranslatePagePropertiesByDefault,
			AutoImportTranslatedSubmissions = @AutoImportTranslatedSubmissions,
			DefaultTranslateToLanguage = @DefaultTranslateToLanguage,	
			PageImportOptions =	@PageImportOptions,
			ImportContentStructure=@ImportContentStructure,
			ImportFileStructure=@ImportFileStructure,
			ImportImageStructure=@ImportImageStructure,
			ImportFormsStructure=@ImportFormsStructure,
			MasterSiteId = @MasterSiteId,
			PrimarySiteUrl = @PrimarySiteUrl,
			CustomAttribute = @CustomAttribute,
			ThemeId = @ThemeId,
			TimeZone=@TimeZone
 	where 	Id    = @Id --and
	--isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
	
	select @error = @@error, @rowcount = @@rowcount
	
			--Deleting from SiteGroups
			DELETE FROM SISiteGroup WHERE SiteId = @id
			-- Insert into sitegroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')
		
			
	

if @error <> 0
  begin
      raiserror('DBERROR||%s',16,1,@stmt)
      return dbo.GetDataBaseErrorCode()
   end
if @rowcount = 0
  begin
    raiserror('CONCURRENCYERROR',16,1)
     return dbo.GetDataConcurrencyErrorCode()			-- concurrency error
end	
end
End
GO
ALTER PROCEDURE [dbo].[Site_GetSiteByXmlGuids]
(
	@Id Xml
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

BEGIN
--********************************************************************************
-- code
--********************************************************************************
		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, 
				URL,Type,ParentSiteId,Keywords,VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName ,Prefix, UICulture, LoginURL, DefaultURL
				,NotifyOnTranslationError, TranslationEnabled, TranslatePagePropertiesByDefault, AutoImportTranslatedSubmissions, DefaultTranslateToLanguage,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteUrl,
				MasterSiteId,
				TimeZone
		FROM @Id.nodes('/guidCollection/guid')tab(col)
		INNER JOIN SISite S on S.Id=tab.col.value('text()[1]','uniqueidentifier')
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE S.Status != dbo.GetDeleteStatus()
END
GO
PRINT 'Altering procedure SqlDirectoryProvider_SaveContentDirectory'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_SaveContentDirectory]
(
	@tbDirectory	TYDirectory READONLY,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE	@Lft bigint,
		@Rgt bigint,
		@Count bigint,
		@NodeCount bigint,
		@PLft bigint,
		@PRgt bigint,
		@IsParentSet bit,
		@Id uniqueidentifier,
		@Inc int,
		@ModifiedDate DateTime
		
	SET @ModifiedDate =GetUtcDate()
	
	IF @ApplicationId is null AND @ParentId is not null
		SELECT @ApplicationId = SiteId FROM COContentStructure WHERE Id = @ParentId 

	-- checking for duplicates
	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
	OR EXISTS (Select 1 From COContentStructure P, @tbDirectory C  Where  P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId )
	BEGIN
		RAISERROR('ISDUPLICATE||Title', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END   

	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From COContentStructure P, @tbDirectory C Where P.Status !=dbo.GetDeleteStatus()and P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId)
	BEGIN
		RAISERROR('ISDUPLICATE||Title', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE COContentStructure  Set Title =T.Title,
		Description =T.Description,
		Attributes =T.Attributes,
		[Exists] =T.[Exists],
		FolderIconPath  =T.FolderIconPath,
		PhysicalPath =T.PhysicalPath,
		[Size] =T.[Size],
		Status =T.Status,
		ThumbnailImagePath =T.ThumbnailImagePath,
		ModifiedBy =@ModifiedBy, 
		ModifiedDate =@ModifiedDate,
		IsMarketierDir = T.IsMarketierDir,
		IsSystem = T.IsSystem,
		AllowAccessInChildrenSites =  T.AllowAccessInChildrenSites
	FROM @tbDirectory T 
	WHERE T.Id = COContentStructure.Id AND COContentStructure.SiteId=@ApplicationId

	IF(@ParentId is null)
	BEGIN
		SELECT TOP 1 @ParentId = ParentId FROM @tbDirectory Order By Lft
		IF @ParentId is null
		BEGIN
			SELECT TOP 1 @ParentId = ParentId FROM COContentStructure 
				WHERE SiteId=@ApplicationId Order by LftValue
		END
	END

	Select @PLft = LftValue , @PRgt = RgtValue From COContentStructure Where Id=@ParentId

	If @PLft is null
	BEGIN
		RAISERROR('INVALID||ParentId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE H SET H.Title = T.Title
	FROM @tbDirectory T INNER JOIN COContentStructure H
		ON T.Id = H.Id AND H.SiteId = @ApplicationId

	SELECT @NodeCount = count(*) from @tbDirectory
	SET @Lft=0
	SET @Count=0	
	SET @IsParentSet =1
		
	WHILE (@Count<@NodeCount)
		BEGIN
			--Selecting Record one by one
			Select Top 1 @Id =Id,
				@Lft =Lft,
				@Rgt =Rgt, 
				@ParentId =ParentId
			FROM @tbDirectory WHERE Lft>@Lft AND SiteId =@ApplicationId
			Order By Lft
			
			IF @IsParentSet =0 -- Select the parent Details of the current record
			BEGIN
				SELECT  @ParentId=Id	,
						@PLft = LftValue,
						@PRgt = RgtValue
						FROM COContentStructure 
						WHERE Id = @ParentId AND SiteId=@ApplicationId
			END

			--Check if that node exists or not
			IF Exists (Select 1 FROM COContentStructure 
						WHERE Id=@Id AND SiteId = @ApplicationId)	
			BEGIN
				Print  'Node Already Exists'
				-- Node Already Exists so Just Skip
				-- Check if it has Childs if so set the Parent properties
				IF @Rgt -@Lft >1
				BEGIN
					SELECT @ParentId=Id	,
						   @PLft = LftValue,
						   @PRgt = RgtValue
					FROM COContentStructure 
					WHERE LftValue Between @PLft and @PRgt And Id=@Id AND SiteId=@ApplicationId
				END
				SET @IsParentSet = 1
			END
			ELSE
			BEGIN
				--Not Exists Insert the Node and all its child the Relation Table 
				--Create Space to Insert
				SET @Inc = (@Rgt - @Lft) + 1 -- Value to be added to create space
				UPDATE COContentStructure 
				SET LftValue=CASE WHEN LftValue>=@PRgt THEN LftValue + @Inc ELSE LftValue END,
					RgtValue=CASE WHEN RgtValue>=@PRgt THEN RgtValue + @Inc ELSE RgtValue END
				Where (LftValue >=@PRgt OR RgtValue >=@PRgt)			
				AND SiteId=@ApplicationId 
				
				INSERT INTO COContentStructure
				(Id
				,Title
				,LftValue
				,RgtValue
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,Status
				,ThumbnailImagePath
				,VirtualPath
				,CreatedBy
				,CreatedDate
				,IsMarketierDir
				,IsSystem
				,AllowAccessInChildrenSites )
				SELECT Id
				,Title
				,(Lft-@Lft)+ @PRgt 
				,(Rgt-@Lft) + @PRgt
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,dbo.GetActiveStatus()
				,ThumbnailImagePath
				,dbo.GetVirtualPathByObjectType(ParentId,7)+ '/' + Title
				,@ModifiedBy
				,@ModifiedDate 
				,IsMarketierDir
				,IsSystem
				,AllowAccessInChildrenSites
				FROM @tbDirectory 
				WHERE Lft Between @Lft and @Rgt
				
				-- Increment the Count to skip the no of time loop is executed
				SET @Count =@Count + ((@Rgt - @Lft) - 1)/2
				-- Increment the Lft value of the loop skips there records

				SET @Lft = @Rgt

				--- Propagating Pemission from the Parent
				If(@ParentId is not null)
				Begin
					print 'copy permission to its  child'
					INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, Propagate, ObjectId, ObjectTypeId ) 
					(
					SELECT ProductId, ApplicationId, MemberId, MemberType, RoleId,  Propagate, @Id, ObjectTypeId  
					FROM USMemberRoles Where ObjectId = @ParentId AND ApplicationId=@ApplicationId
					)
				END
			END
		
			SET @Count= @Count + 1
		END	

	END
	

GO
PRINT 'Altering procedure Site_GetSiteByRelation'
GO
ALTER PROCEDURE [dbo].[Site_GetSiteByRelation]
(
	@RelationName		varchar(256)=null,
	@ObjectId			varchar(8000)=null,
	@ReturnObjectType	int ,
	@Id					uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	--@URL				nvarchar(4000),
	@ParentSiteId		uniqueidentifier=null,
	@Status			    int,
	@PageIndex			int = 1,
	@PageSize			int 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueIdentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN
		
		SELECT S.Id, S.Title, S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate, 
				S.URL,S.Type,S.ParentSiteId,S.Keywords,S.VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName ,Prefix, UICulture, LoginURL, DefaultURL
				,NotifyOnTranslationError, TranslationEnabled, TranslatePagePropertiesByDefault, AutoImportTranslatedSubmissions, DefaultTranslateToLanguage,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				TimeZone
		FROM SISite S  
		INNER JOIN dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R ON S.Id = R.ObjectId 
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE 	S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				--isnull(upper(S. URL),'') = isnull(upper(@URL), isnull(upper(S.URL),'')) and
				isnull(S.ParentSiteId,@EmptyGUID) = isnull(@ParentSiteId, isnull(S.ParentSiteId,@EmptyGUID)) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, S.Id, S.Title, S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate, 
				S.URL,S.Type,S.ParentSiteId,S.Keywords,S.VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation ,Prefix, UICulture, LoginURL, DefaultURL,NotifyOnTranslationError, TranslationEnabled, TranslatePagePropertiesByDefault, AutoImportTranslatedSubmissions, DefaultTranslateToLanguage,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				TimeZone
		FROM SISite S  
		INNER JOIN dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R ON S.Id = R.ObjectId 
		WHERE 
				S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				--isnull(upper(S. URL),'') = isnull(upper(@URL), isnull(upper(S.URL),'')) and
				isnull(S.ParentSiteId,@EmptyGUID) = isnull(@ParentSiteId, isnull(S.ParentSiteId,@EmptyGUID)) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, 
				URL,Type,ParentSiteId,Keywords,VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName ,Prefix, UICulture, LoginURL, DefaultURL
				,TimeZone
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END
GO
PRINT 'Altering procedure Site_GetSite'
GO
ALTER PROCEDURE [dbo].[Site_GetSite]  
(  
	@Id				uniqueIdentifier = null ,  
	@Title			nvarchar(256) = null,  
	@ParentSiteId	uniqueIdentifier = null,  
	@Status			int = null,  
	@PageIndex		int,  
	@PageSize		int,
	@ForceAllSites	bit = null,
	@MasterSiteId   uniqueIdentifier = null,
	@SiteGroupId	uniqueIdentifier = null
 )  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	IF @PageSize is Null  
	BEGIN  
		SELECT S.Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId, 
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName, 
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			NotifyOnTranslationError, 
			TranslationEnabled, 
			TranslatePagePropertiesByDefault, 
			AutoImportTranslatedSubmissions, 
			DefaultTranslateToLanguage,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
			MasterSiteId,
			CustomAttribute,
			ThemeId,
			TimeZone
		FROM SISite S  
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id
		WHERE 
			(@Id IS NULL OR S.Id = @Id) AND
			(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
			(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
			(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
			(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus)
			
	END  
	ELSE  
	BEGIN  
		WITH ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
				S.Id, 
				Title, 
				Description, 
				Status, 
				CreatedBy, 
				CreatedDate, 
				ModifiedBy,   
				ModifiedDate, 
				URL,
				Type,
				ParentSiteId,
				Keywords,
				VirtualPath,
				HostingPackageInstanceId, 
				LicenseLevel, 
				SendNotification, 
				SubmitToTranslation,
				ImportAllAdminPermission,
				AllowMasterWebSiteUser,
				Prefix, 
				UICulture, 
				LoginURL, 
				DefaultURL,
				NotifyOnTranslationError, 
				TranslationEnabled, 
				TranslatePagePropertiesByDefault, 
				AutoImportTranslatedSubmissions, 
				DefaultTranslateToLanguage,
				PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteURL,
				dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
				MasterSiteId,
				CustomAttribute,
				ThemeId,
				TimeZone
			FROM SISite S
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id 
			WHERE 
				(@Id IS NULL OR S.Id = @Id) AND
				(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
				(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
				(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
				(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
				(@Status IS NULL OR S.Status = @Status) AND
				(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus) 
		)
		
		SELECT 
			Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId,  
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			SitePath,
			MasterSiteId,
			CustomAttribute,
			ThemeId,
			TimeZone
		FROM ResultEntries  A  
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize  
	END  
END
GO
PRINT 'Altering procedure Site_CreateCommerceDefaultData'
GO
ALTER Procedure [dbo].[Site_CreateCommerceDefaultData](@ApplicationId uniqueidentifier, @SiteTitle Varchar(1024), @CreatedById uniqueidentifier)
As
BEGIN

	DECLARE @CommerceProductId uniqueidentifier 
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'
	DECLARE @Now DateTime, @maxId int 
	DECLARE @maxSecurityId int
	
	SET @Now = GETUTCDATE()

	-- Product Category
	IF NOT EXISTS(SELECT * FROM PRCategory WHERE Title ='Unassigned')
	BEGIN
		INSERT INTO PRCategory (Id, Title, Description, Sequence, CreatedDate, Status)
		Values('6988B7A8-7C72-4B58-9B68-258B940E2363', 'Unassigned', 'Unassigned', 1, @Now, 1)
	END

	--Insert default value for order number
	IF NOT EXISTS(SELECT * FROM GLIdentity WHERE Name ='OrderNumber' )
	BEGIN
		INSERT INTO GLIdentity (Name,Value) Values ('OrderNumber', 0)
	END

	-- Product Import Config - Did not take it.

	-- Coupon Type
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('D1CA3949-EBFA-4DEC-B120-07DAA8961F81',	'% off Order','',		1,	1,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('2C24DC1C-268E-4ACB-933C-618DEF52E434',	'$ off Order',	'',	1,	2,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('C4FE5439-0B9F-4B41-9221-0302C527DEAE',	'$ off Shipping',	'',	1,	32,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('894D41B5-27D9-4C3B-885D-18FFC565DB8B',	'% off Shipping',	'',	1,	4,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='Flat rate shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('004202FA-B429-4AB7-A59F-ED37C74C0421',	'Flat rate shipping','',		1,	5,	1)
	END

	--Customer Group.sql
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Gold')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Gold',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Silver')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Silver',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Bronze')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Bronze',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Frequent Customer')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Frequent Customer',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='B2B')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'B2B',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Friends & Family')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Friends & Family',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Employees')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Employees',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	-- EveryOne Group - We are not using this group as of now
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Everyone')
	BEGIN
		INSERT INTO USGroup (ProductId, ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, 
		ModifiedBy, ModifiedDate, Status, ExpiryDate, GroupType)
		Values
		(@CommerceProductId,	@ApplicationId,	
		NEWID(),	'Everyone',	'everyone',	
		@CreatedById,	@Now,
		@CreatedById,	@Now,	1,	NULL,	2)
	END

	--- Security Level
	SELECT @maxSecurityId = Isnull(Max(Id),0) + 1 FROM USSecurityLevel

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Commerce Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId, 'Commerce Customer', 'Commerce Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Preferred Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+1, 'Preferred Customer', 'Preferred Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Employee')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+2, 'Employee', 'Employee', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Press')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+3, 'Press', 'Press', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	-- Feature Type
	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto Top Seller')
	BEGIN
		Insert into MRFeatureType Values(1,'Auto Top Seller')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto New Products')
	BEGIN
		Insert into MRFeatureType Values(2,'Auto New Products')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Manual')
	BEGIN
		Insert into MRFeatureType Values(3,'Manual')
	END

	-- Feature
	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Top Sellers' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Top Sellers' , 'Top Sellers Auto', 1, 1, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='New Products' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'New Products' , 'New Products Auto', 2,  2, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Moving Brand' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Moving Brand' , 'Moving Brand Manual', 3, 3, 1, newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Stock Clearance' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Stock Clearance' , 'Stock Clearance Manual', 3,  4, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Christmas Sales' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Christmas Sales' , 'Christmas Sales Manual', 3,  5, 2,newId(), GetUTCDate())

	-- AttributeDataType
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Image Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Image Library', 11)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Date' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.DateTime','Date', 5)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Decimal' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Double','Decimal', 3)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Asset Library File' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Asset Library File', 10)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Page Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Page Library', 8)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='HTML' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','HTML', 6)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='File Upload' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','File Upload', 7)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Integer' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Int32','Integer', 2)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='String' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','String', 1)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Boolean')  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Boolean','Boolean', 4)  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Content Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Content Library', 9)  END

	-- CommerceStatus 
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Active' )  BEGIN   INSERT INTO CMCommerceStatus values(1,'Active','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Inactive' )  BEGIN   INSERT INTO CMCommerceStatus values(2,'Inactive','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Deleted' )  BEGIN   INSERT INTO CMCommerceStatus values(3,'Deleted','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Approved' )  BEGIN   INSERT INTO CMCommerceStatus values(4,'Approved','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Rejected' )  BEGIN   INSERT INTO CMCommerceStatus values(5,'Rejected','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='PendingForApproval' )  BEGIN   INSERT INTO CMCommerceStatus values(6,'PendingForApproval','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Included' )  BEGIN   INSERT INTO CMCommerceStatus values(7,'Included','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Excluded')  BEGIN   INSERT INTO CMCommerceStatus values(8,'Excluded','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Expired')  BEGIN   INSERT INTO CMCommerceStatus values(9,'Expired','')  END

	-- GLReason
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Damaged')  BEGIN   INSERT INTO GLReason values(NEWID(),'Damaged',210,4,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Restocking')  BEGIN   INSERT INTO GLReason values(NEWID(),'Restocking',210,1,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Bad Inventory')  BEGIN   INSERT INTO GLReason values(NEWID(),'Bad Inventory',210,2,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Returned Item')  BEGIN   INSERT INTO GLReason values(NEWID(),'Returned Item',210,8,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Lost')  BEGIN   INSERT INTO GLReason values(NEWID(),'Lost',210,3,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Physical Count')  BEGIN   INSERT INTO GLReason values(NEWID(),'Physical Count',210,6,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='New Shipment')  BEGIN   INSERT INTO GLReason values(NEWID(),'New Shipment',210,5,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Other')  BEGIN   INSERT INTO GLReason values(NEWID(),'Other',210,7,1)  END

	-- GLAddress
	IF NOT EXISTS (SELECT * FROM GLAddress Where Id = 'A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F')
	BEGIN
		INSERT INTO  GLAddress(
		Id, FirstName, LastName, AddressType, AddressLine1, AddressLine2, AddressLine3, City, StateId, Zip, 
		CountryId, Phone, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, StateName, County)
		Values
		('A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F',
		'Warehouse','Location',0,'Your warehouse address', 250,	NULL	,'City',
		'7E07B39B-BC66-4969-B16A-C62E41043E39',	'30092',	'AD72C013-B99D-4513-A666-A128343CCFF0',	'7819955555',
		'2011-08-01 11:18:32.880',	'E131010D-BE50-4526-B343-B0954E924080',	'2011-08-01 11:18:32.880',	
		'E131010D-BE50-4526-B343-B0954E924080',	1,	'MA','County')	
	END

	-- INWarehouse
	Declare @WarehouseId uniqueidentifier
	DECLARE @CopyWarehouseId uniqueidentifier
		
	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId Where Title = 'Default Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Default Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
			Values(@WarehouseId,	'Default warehouse',	
			'Default warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
			null,	1, 'Internal', 'InternalWarehouseProvider', 1,1)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
		from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
		Where T.IsDownloadableMedia =0 OR T.IsDownloadableMedia is null

	END

	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId  Where Title = 'Digital Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Digital Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
		Values(@WarehouseId,	'Digital Warehouse',	
		'Internal Digital Warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
		Null,	0, Null, 'DigitalWarehouseProvider', 2,0)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
			from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
			Where T.IsDownloadableMedia =1 
	END

	-- Product Type
	--IF NOT EXISTS (SELECT * FROM PRProductType Where SiteId = @ApplicationId)
	IF ((SELECT COUNT(*) FROM PRProductType)=0)
	BEGIN
		Insert into PRProductType(Id,ParentId,LftValue,RgtValue,Title, Description, SiteId, CreatedDate, CreatedBy, Status)
		Values(newId(),@ApplicationId,1,2,@SiteTitle, @SiteTitle ,@ApplicationId, @Now,@CreatedById ,1 )
	END

	-- ORShipper
	DECLARE @Shipper_USPS uniqueidentifier, @Shipper_UPS uniqueidentifier, @Shipper_Fedex uniqueidentifier

	IF EXISTS (SELECT * FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_USPS = (SELECT Id FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId)
	END	
	ELSE
	BEGIN
		SET @Shipper_USPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_USPS,'USPS','USPS','','iAppsUSPSProvider',1,3,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END	
	IF EXISTS (SELECT * FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_UPS = (SELECT Id FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_UPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_UPS,'UPS','UPS','http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum={0}&AgreeToTermsAndConditions=yes&ignore=&track.x=46&track.y=7',
		'iAppsUpsProvider',1,2,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END
	IF EXISTS (SELECT * FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_Fedex = (SELECT Id FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_Fedex = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_Fedex,'Fedex','Fedex','','provider',1,1,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END

	 
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpressSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpressSM',
		'Worldwide ExpressSM','',5,0,0,0,0,1,'07',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
		
	END	
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air Saver','Next Day Air Saver',
		'customid',1,5,0,0,1,0,'13',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='UPS Next Day Air Early A.M. SM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'UPS Next Day Air Early A.M. SM','UPS Next Day Air® Early A.M. SM',
		'',1,0,0,0,0,0,'14',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Next Flight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Next Flight',
		'','',5,0,0,0,0,1,'INTERNATIONAL_FIRST',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Standard','Standard','',8,0,0,0,0,1,'11',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air A.M' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air A.M','Second Day Air A.M','',2,0,0,0,0,0,'59',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First-Class Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'First-Class Mail','1st Class','customid',5,5,0,0,1,0,'0',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International  Economy' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International  Economy','','',7,0,0,0,0,1,'INTERNATIONAL_ECONOMY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Saver','Saver','',4,0,0,0,0,0,'65',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day A.M.' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day A.M.','','',0,0,0,0,0,0,'FEDEX_2_DAY_AM',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpeditedSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpeditedSM','Worldwide ExpeditedSM','',4,0,0,0,0,1,'08',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Three-Day Select' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Three-Day Select','UPS Three-Day Select','customid',3,5,0,0,1,0,'12',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air','UPS Second Day Air','customid',2,5,0,0,1,0,'02',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Priority Overnight','','',1,0,0,0,0,1,'PRIORITY_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Priority' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Priority','','',0,0,0,0,0,1,'INTERNATIONAL_PRIORITY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Priority Mail','Priority Mail','customid',2,5,0,0,1,0,'1',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Express Saver','','',3,0,0,0,0,0,'FEDEX_EXPRESS_SAVER',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'First Overnight','Collège dOccitanie','',4,0,0,0,0,1,'FIRST_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Home Delivery' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Home Delivery','','customid',2,5,0,0,1,0,'GROUND_HOME_DELIVERY',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air','UPS Next Day Air','customid',1,5,0,0,1,0,'01',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide Express PlusSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide Express PlusSM','Worldwide Express PlusSM','',4,0,0,0,0,1,'54',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day','','',5,0,0,0,0,0,'FEDEX_2_DAY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Standard Overnight','','',4,0,0,0,0,0,'STANDARD_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Express','Express','customid',1,5,0,0,1,0,'2',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Ground' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Ground','UPS Ground','customid',5,5,0,0,1,0,'03',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	
	
	
	-- ATAttributeGroup
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='01614e00-86f8-4448-9a48-7daa63171c38')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'01614e00-86f8-4448-9a48-7daa63171c38', N'Features', @ApplicationId, 1, @CreatedById, @Now, @CreatedById, @Now , 1)
	END	
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='b23ffc1b-ab9d-4985-beb4-a5f303b9ef41')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'SKULevel Attributes', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Advanced Details', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'AB730D27-5CCD-49D9-B515-168290438E0A', N'Product Properties', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='3C211CF2-A184-4375-A078-242F4387D1D2')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'3C211CF2-A184-4375-A078-242F4387D1D2', N'Customer Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='9DABE135-467C-459E-ABF2-5FBD64821989')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'9DABE135-467C-459E-ABF2-5FBD64821989', N'Order Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A', N'Advanced Details', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END

	-- ATAttribute
	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0611ed6a-d071-4dff-b6c1-b75a06a17ccf')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as Top Seller', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now,  1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0b94496d-9df4-4ac1-8c5c-48b42f99c4ef')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Discounts', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='2c4983dd-d7eb-40a2-9b05-bfd308e3da62')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Free Shipping', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='4ae05c5a-2375-49d8-8bfb-d131632c0e09')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as New Item', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='7b39ffe5-36b9-44ef-b7a5-08f3c18181aa')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Refundable', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c1a20d3c-985f-49be-9682-df09eaf82fc8')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Freight', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c57665fd-e681-4c13-82ce-d77e60bf9a72')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Tax Exempt', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f4963a96-57dc-4c67-830e-f7426539c8ce')
	--INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	--[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	--VALUES (N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Unlimited Quantity', N'', 1, 
	--NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f68ceb85-922b-4023-a6f9-20025f7fdc94')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'System.String', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Others', 
	NULL, 0, N'AT1', 1, 1, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f7102f66-4242-408e-a816-f8b3e600f4d0')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Shipping Promotions', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='39C2D877-525F-49A6-9E48-D3296989F83B')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('39C2D877-525F-49A6-9E48-D3296989F83B','System.String','AB730D27-5CCD-49D9-B515-168290438E0A','Product Name',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='ECB9E9D1-FC3F-43A5-B40F-3840212F693C')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('ECB9E9D1-FC3F-43A5-B40F-3840212F693C','System.Double','AB730D27-5CCD-49D9-B515-168290438E0A','ListPrice',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	-- ATAttributeEnum
	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='bbf74b61-05d8-49b4-8fee-72147d849814')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'bbf74b61-05d8-49b4-8fee-72147d849814', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='073c66b8-2fc2-4d82-aebe-3165f0138ad5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'073c66b8-2fc2-4d82-aebe-3165f0138ad5', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='51424fd1-0da2-4792-b692-7beef5679bf3')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'51424fd1-0da2-4792-b692-7beef5679bf3', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='f0974b83-4d83-4534-bf87-1f8a8b0237e8')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'f0974b83-4d83-4534-bf87-1f8a8b0237e8', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='0b99d94e-606a-43a7-b72c-d6512fd38bf2')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'0b99d94e-606a-43a7-b72c-d6512fd38bf2', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='235f7a94-52c7-42d8-b870-c0d53906b1e5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'235f7a94-52c7-42d8-b870-c0d53906b1e5', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='11bfcfbd-3ffa-4791-8b39-1f12b80d8524')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'11bfcfbd-3ffa-4791-8b39-1f12b80d8524', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='d8d961c0-5c4a-487b-9913-a920e73cda1a')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'd8d961c0-5c4a-487b-9913-a920e73cda1a', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='3a1e07c0-18d1-4c00-a73c-73a4953d5e41')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'3a1e07c0-18d1-4c00-a73c-73a4953d5e41', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='872eeb49-e8eb-4f73-837c-71ddb20629c4')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'872eeb49-e8eb-4f73-837c-71ddb20629c4', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='ee919400-c7de-4d9c-ae8f-fefd1c768e64')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'ee919400-c7de-4d9c-ae8f-fefd1c768e64', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='49a6b896-2f22-45d1-82fb-e47e8f2519c9')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'49a6b896-2f22-45d1-82fb-e47e8f2519c9', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='8bb63596-8fee-4005-9109-dda014ba147e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'8bb63596-8fee-4005-9109-dda014ba147e', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='70a27f03-06ca-4df2-99bb-5c23b840d465')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'70a27f03-06ca-4df2-99bb-5c23b840d465', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='e5f89737-8879-4e42-9aa1-bfffb05d808e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'e5f89737-8879-4e42-9aa1-bfffb05d808e', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='9b0a9c92-80c8-411f-9037-b0281c755201')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'9b0a9c92-80c8-411f-9037-b0281c755201', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='81a55708-cd4d-4fbd-b3c5-4e3292401511')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'81a55708-cd4d-4fbd-b3c5-4e3292401511', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='a0c6381b-2970-4d09-a582-334b93c51442')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'a0c6381b-2970-4d09-a582-334b93c51442', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'true', N'0', 0, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='691ee3cd-58ff-42d9-a8e8-385e85648cfc')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'691ee3cd-58ff-42d9-a8e8-385e85648cfc', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'false', N'1', 2, N'false', 0, 2, @CreatedById, @Now, @CreatedById, @Now, 1)


	--- CLImageType
	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='396b425c-e197-4f2d-a4a3-25448ca366f8')
	INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('396b425c-e197-4f2d-a4a3-25448ca366f8', N'main', 600, 800, null, N'alt_mainImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 1)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='63cf2418-684a-4d1f-88b2-34968c6f139f')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('63cf2418-684a-4d1f-88b2-34968c6f139f', N'system_thumb', 60, 60, N'systhumb_', null, 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 0)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='2770534e-4675-4764-a620-56776398e03e')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('2770534e-4675-4764-a620-56776398e03e', N'preview', 260, 372, N'prev_', N'alt_PrevImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 3)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='e02ed0f3-1271-4218-b81e-816e458434a1')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('e02ed0f3-1271-4218-b81e-816e458434a1', N'mini', 80, 80, N'mini_', N'alt_miniImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 4)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='ba9b3922-4f9f-4b21-a5cf-e03b26b588ad')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('ba9b3922-4f9f-4b21-a5cf-e03b26b588ad', N'thumbnail', 120, 120, N'thumb_', N'alt_thumbImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 2)

	--OROrderStatus
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='OK')  BEGIN   INSERT INTO OROrderStatus values(1,'OK','Submitted',1,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Shipped')  BEGIN   INSERT INTO OROrderStatus values(2,'Shipped','Shipped',2,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Canceled')  BEGIN   INSERT INTO OROrderStatus values(3,'Canceled','Canceled',3,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Back Ordered')  BEGIN   INSERT INTO OROrderStatus values(4,'Back Ordered','Back Ordered',4,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Approval')  BEGIN   INSERT INTO OROrderStatus values(5,'Pending Approval','On Hold',5,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Customer Registration Approval')  BEGIN   INSERT INTO OROrderStatus values(6,'Pending Customer Registration Approval','Pending Customer Registration Approval',6,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Bad Customer Order Approval')  BEGIN   INSERT INTO OROrderStatus values(7,'Pending Bad Customer Order Approval','Pending Bad Customer Order Approval',7,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Terms Limit')  BEGIN   INSERT INTO OROrderStatus values(8,'Exceeded Terms Limit','Exceeded Terms Limit',8,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Line of Credit')  BEGIN   INSERT INTO OROrderStatus values(9,'Exceeded Line of Credit','Exceeded Line of Credit',9,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Manual Hold')  BEGIN   INSERT INTO OROrderStatus values(10,'Manual Hold','Manual Hold',10,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Partially Shipped')  BEGIN   INSERT INTO OROrderStatus values(11,'Partially Shipped','Partially Shipped',11,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Created')  BEGIN   INSERT INTO OROrderStatus values(12,'Created','Created',12,0)  END

	-- OROrderNextStatus
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderNextStatus values(1,1,10)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderNextStatus values(2,1,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderNextStatus values(3,7,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderNextStatus values(4,7,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderNextStatus values(5,10,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderNextStatus values(6,10,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderNextStatus values(7,5,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =8)  BEGIN   INSERT INTO OROrderNextStatus values(8,5,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =9)  BEGIN   INSERT INTO OROrderNextStatus values(9,6,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =10)  BEGIN   INSERT INTO OROrderNextStatus values(10,6,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =11)  BEGIN   INSERT INTO OROrderNextStatus values(11,12,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =12)  BEGIN   INSERT INTO OROrderNextStatus values(12,11,2)  END

	-- OROrderType
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =1)  BEGIN   INSERT INTO OROrderType values(1,'Web','Web Order',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =2)  BEGIN   INSERT INTO OROrderType values(2,'CSR','Web Service Order',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =3)  BEGIN   INSERT INTO OROrderType values(3,'Brochure Request','Brochure Request',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =4)  BEGIN   INSERT INTO OROrderType values(4,'Recurring Order','Recurring ORder',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =5)  BEGIN   INSERT INTO OROrderType values(5,'Exchange Order','Exchange Order',5)  END

	-- OROrderItemStatus
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderItemStatus values(1,'Ok','Submitted',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderItemStatus values(2,'Shipped','Shipped',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderItemStatus values(3,'Cancelled','Cancelled',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderItemStatus values(4,'Backordered','Backordered',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderItemStatus values(5,'Pending','Pending',5)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderItemStatus values(6,'Released','Released',6)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderItemStatus values(7,'Partially Shipped','Partially Shipped',7)  END

	-- PTCreditCardType
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE Id ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487','American Express','American Express',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='28091FD7-00C2-4E71-A266-7FA718658A66')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('28091FD7-00C2-4E71-A266-7FA718658A66','MasterCard','MasterCard',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='A98F6177-90A5-420D-96B5-A234778F8D96')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('A98F6177-90A5-420D-96B5-A234778F8D96','Visa','Visa',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E','Discover','Discover',@CreatedById, @Now, @CreatedById, @Now)  
	END

	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE CreditCardTypeId ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487' And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='28091FD7-00C2-4E71-A266-7FA718658A66'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('28091FD7-00C2-4E71-A266-7FA718658A66',@ApplicationId,1) 
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='A98F6177-90A5-420D-96B5-A234778F8D96'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('A98F6177-90A5-420D-96B5-A234778F8D96',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E',@ApplicationId,1)  
	END

	-- PTGateway
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =1)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(1,'Website Payments Pro','Website Payments Pro',2,'WebsitePaymentsProGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =2)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(2,'None','None',1,'AutoGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =3)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(3,'Cybersource','Cybersource',3,'CybersourceGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =4)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description],  [Sequence], [DotNetProviderName]) 
		Values(4,'Payflow Pro','Payflow Pro',4,'PayflowProGatewayProvider') 
	END

	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =1)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(1,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =2)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(2,0,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =3)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(3,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =4)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(4,0,1,@ApplicationId) 
	END
	
	-- PTPaymentStatus
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =1)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(1,'Entered','Entered',1,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =2)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(2,'Auth','Authorized',2,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =3)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(3,'Captured','Captured',3,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =4)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(4,'Auth Failed','Authorization Failed',4,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =5)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(5,'Capture Failed','Capture Failed',5,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =6)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(6,'Clear','Clear',6,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =7)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(7,'Manual Cancel','Manual Cancel',7,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =8)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(8,'Partially Captured','Partially Captured',8,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =9)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(9,'Void','Void',9,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =10)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(10,'Refund Entered','Refund Entered',10,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =11)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(11,'Refund Waiting Approval','Refund Waiting Approval',11,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =12)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(12,'Refunded','Refunded',12,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =13)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(13,'Refund Failed','Refund Failed',13,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =14)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(14,'Refund Rejected','Refund Rejected',14,1)  END

	-- PTPaymentType
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =1)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(1,'CreditCard','Credit Card',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =2)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(2,'OnAccount','OnAccount',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =3)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(3,'Paypal','Paypal',5)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =4)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(4,'COD','Cash On Delivery',4)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =5)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(5,'GiftCard','Gift Card',2)  END

	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =1)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(1,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =2)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(2,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =3)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(3,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =4)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(4,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =5)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(5,@ApplicationId,1)  END

	IF (SELECT Count(*) FROM ORReturnReason)=0
	BEGIN

		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES( 
			'Defective')
			
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(  
			'Customer Ordered Wrong Product')
		    
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES('Refused Delivery')


		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Missing Merchandise')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Did not Like')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Changed Mind')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Billing Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Fraudulent Order')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Warehouse Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Returned by Shipper - No Response From Customer')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Other')

	END

	-- INWarehouseType
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =1)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(1,'Internal','iAPPS internal warehouse for Non-Digital Products')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =2)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(2,'Digital','iAPPS Digital warehouse')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =3)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(3,'External','Integrated with external system')  END

	-- INRestockingStatus
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Created'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (1,'Created','Created',1)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Queued'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (2,'Queued','Queued',2)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='In Progress'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (4,'In Progress','In Progress',4)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Canceled'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (3,'Canceled','Canceled',3)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Completed'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (5,'Completed','Completed',5)

	-- ORReturnResolutionType
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] ON
	Select @maxId =Isnull(MAX(Id),0) FROM ORReturnResolutionType
	SET @maxId = @maxId + 1

	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Refund'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Refund','Part or full paymnent is returned', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Replacement'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Replacement','Replacement', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Others'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Others','Others', @maxId)        
		SET @maxId = @maxId + 1
	END
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] OFF

	-- INInventory
	IF(NOT EXISTS(SELECT 1 FROM dbo.INInventory WHERE WarehouseId ='22FAC4BD-4954-415A-9014-3598DAEAB4D0'))  
	BEGIN   
		Declare @Id UniqueIdentifier
		Set @Id= '22FAC4BD-4954-415A-9014-3598DAEAB4D0'

		INSERT INTO [INInventory]
				   ([Id]
				   ,[WarehouseId]
				   ,[SKUId]
				   ,[Quantity]
				   ,[LowQuantityAlert]
				   ,[FirstTimeOrderMinimum]
				   ,[OrderMinimum]
				   ,[OrderIncrement]
				   ,[ReasonId]
				   ,[ReasonDetail]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
					SELECT newId()
					  ,@Id
					  ,Id
					  ,0
					  ,0
					  ,1
					  ,1
					  ,1
					  ,(SELECT Id FROM GLReason Where Title ='Other') As ReasonId
					  ,'Initial Data When SKU is created'
					  ,1
					  ,@CreatedById
					  ,@Now
					  ,@CreatedById
					  ,@Now
				   FROM PRProductSKU WHERE Id NOT IN 
					(SELECT SKUID FROM ININventory WHERE WarehouseId = @Id)
	END
	

	DECLARE @GroupObjectTypeId int
	DECLARE @MerchandisingRoleId int, @FulfillmentRoleId int, @MarketingRoleId int, @CSRRoleId int, 
		@EveryOneRoleId int, @CSRManagerRoleId int, @WHManagerRoleId int
	DECLARE @MerchandisingGroupId uniqueidentifier, @FulfillmentGroupId uniqueidentifier, 
		@MarketingGroupId uniqueidentifier, @CSRGroupId uniqueidentifier, @EveryOneGroupId uniqueidentifier, 
		@CSRManagerGroupId uniqueidentifier, @WHManagerGroupId uniqueidentifier

	SET @GroupObjectTypeId = 15 
	SET @MerchandisingRoleId = 19
	SET @FulfillmentRoleId = 20
	SET @MarketingRoleId = 21
	SET @CSRRoleId = 22
	SET @EveryOneRoleId = 29
	SET @CSRManagerRoleId = 32
	SET @WHManagerRoleId = 33	
	-- if the db has cms and default commerce data was missing 
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MerchandisingGroupId output,@Title = 'Merchandising',
			@Description = 'Merchandising',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
				
	END		
	ELSE
		SET @MerchandisingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')

	IF (@MerchandisingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MerchandisingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MerchandisingGroupId,2,@MerchandisingRoleId, @MerchandisingGroupId,1,1 )				
	END
	
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Fulfillment')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @FulfillmentGroupId output,@Title = 'Fulfillment',
			@Description = 'Fulfillment',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @FulfillmentGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Fulfillment')

	IF (@FulfillmentGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @FulfillmentGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@FulfillmentGroupId,2,@FulfillmentRoleId, @FulfillmentGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Marketing')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MarketingGroupId output,@Title = 'Marketing',
			@Description = 'Marketing',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @MarketingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Marketing')

	IF (@MarketingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MarketingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MarketingGroupId,2,@MarketingRoleId, @MarketingGroupId,1,1 )				
	END

	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSR')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRGroupId output,@Title = 'CSR',
			@Description = 'CSR',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSR')
	
	IF (@CSRGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRGroupId,2,@CSRRoleId, @CSRGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'EveryOne' And GroupType = 1)) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @EveryOneGroupId output,@Title = 'EveryOne',
			@Description = 'EveryOne',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @EveryOneGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'EveryOne')

	IF (@EveryOneGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @EveryOneGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@EveryOneGroupId,2,@EveryOneRoleId, @EveryOneGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSRManager')) 
	BEGIN	
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRManagerGroupId output,@Title = 'CSRManager',
			@Description = 'CSRManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSRManager')

	IF (@CSRManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRManagerGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRManagerGroupId,2,@CSRManagerRoleId, @CSRManagerGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'WHManager')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @WHManagerGroupId output,@Title = 'WHManager',
			@Description = 'WHManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @WHManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'WHManager')

	IF (@WHManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @WHManagerGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@WHManagerGroupId,2,@WHManagerRoleId, @WHManagerGroupId,1,1 )				
	END
			
END

GO

PRINT 'DistributionList_GetTreeManualDistributionListWithGroup'
GO

IF (OBJECT_ID('DistributionList_GetTreeManualDistributionListWithGroup') IS NOT NULL)
	DROP PROCEDURE DistributionList_GetTreeManualDistributionListWithGroup

GO

CREATE PROCEDURE [dbo].[DistributionList_GetTreeManualDistributionListWithGroup]
(@ApplicationId uniqueidentifier)
as

Declare @xmlResult xml

set @xmlResult =  (
select GroupAlias.Id,GroupAlias.Title as [Text],DistributionAlias.Id,DistributionAlias.Title as [Text],'true' as ShowCheckBox from  dbo.TADistributionGroup GroupAlias ,TADistributionLists DistributionAlias ,
dbo.TADistributionListGroup c
where c.DistributionId = DistributionAlias.Id and c.DistributionGroupId = GroupAlias.Id and DistributionAlias.ApplicationId=@ApplicationId  AND GroupAlias.ApplicationId =@ApplicationId
and GroupAlias.Status=dbo.GetActiveStatus() and DistributionAlias.Status=dbo.GetActiveStatus() and DistributionAlias.ListType=0 order by  GroupAlias.Title
  for xml auto) 

select @xmlResult
GO

PRINT 'Altering Procedure CampaignGroup_SaveAnalyzerCampaignGroup'
GO
ALTER PROCEDURE [dbo].[CampaignGroup_SaveAnalyzerCampaignGroup](@GroupId uniqueidentifier,@ApplicationId uniqueidentifier)
AS
BEGIN
	If not exists(select * from dbo.MKCampaignGroup Where Id = @GroupId)
	BEGIN
		INSERT INTO MKCampaignGroup (Id, Name, CreatedBy, CreatedDate,ApplicationId)
			SELECT Id, Name, CreatedBy, CreatedDate,@ApplicationId FROM ALCampaign WHERE Id = @GroupId
	END	
	ELSE
	BEGIN
		UPDATE MKCampaignGroup SET Name = ALCampaign.Name
		FROM ALCampaign  
		WHERE ALCampaign.Id = MKCampaignGroup.Id AND ALCampaign.Id = @GroupId
	END
END
--DATA MIGRATION
GO
PRINT 'Creating object type for Contact'
GO
IF NOT EXISTS (SELECT 1 FROM GLObjectType WHERE Name='Contact' AND Id = 302)
BEGIN
	INSERT INTO GLObjectType VALUES(302,'Contact','Contact for marketier')
END
GO
PRINT 'ADDING MARKETIER TEMPLATES'
GO
-- Add default templates
DECLARE	@Templates AS NewPageTemplates

INSERT INTO @Templates (DirectoryPath, Title, AscxFile, CsFile, ImageFile, Type, PagePart)
	VALUES	('~/Page Template Library/Email Templates/Generic', 'One Column', 'One-Column.ascx', 'One-Column.ascx.cs', 'One-Column.gif', 1, 0),
			('~/Page Template Library/Email Templates/Generic', 'Two Columns', 'Two-Columns.ascx', 'Two-Columns.ascx.cs', 'Two-Columns.gif', 1, 0),
			('~/Page Template Library/Email Templates/Generic', 'Three Columns', 'Three-Columns.ascx', 'Three-Columns.ascx.cs', 'Three-Columns.gif', 1, 0),
			('~/Page Template Library/Email Templates/Generic', 'Sidebar Left', 'Sidebar-Left.ascx', 'Sidebar-Left.ascx.cs', 'Sidebar-Left.gif', 1, 0),
			('~/Page Template Library/Email Templates/Generic', 'Sidebar Right', 'Sidebar-Right.ascx', 'Sidebar-Right.ascx.cs', 'Sidebar-Right.gif', 1, 0)

DECLARE	@Sites TABLE (
	Id			UNIQUEIDENTIFIER,
	Processed	BIT DEFAULT(0)
)

DECLARE	@SiteId	UNIQUEIDENTIFIER

INSERT INTO @Sites (Id)
	SELECT	Id
	FROM	SISite
	WHERE	ParentSiteId = dbo.GetEmptyGUID()
	
WHILE	(SELECT COUNT(*) FROM @Sites WHERE Processed = 0) > 0
BEGIN
	SET		@SiteId = (SELECT TOP 1 Id FROM @Sites WHERE Processed = 0)

	EXEC	Site_AddPageTemplates @SiteId, null, @Templates

	UPDATE	@Sites
	SET		Processed = 1
	WHERE	Id = @SiteId
END
GO
PRINT 'Updating product id for attribute groups'
GO
Update ATAttributeGroup SET ProductId = 'CCF96E1D-84DB-46E3-B79E-C7392061219B' 
WHERE ProductId IS NULL
GO
PRINT 'Migrating users to contact'
GO
INSERT INTO [dbo].[MKContact]
           ([Id]
           ,[Email]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[CompanyName]
           ,[Gender]
           ,[BirthDate]
           ,[HomePhone]
           ,[MobilePhone]
           ,[OtherPhone]
           ,[ImageId]
           ,[AddressId]
           ,[Notes]
           ,[ContactSourceId]
           ,[Status]
		   ,CreatedBy
		   ,CreatedDate)

SELECT U.Id
	  ,[Email]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[CompanyName]
	  ,[Gender]
      ,[BirthDate]
      ,[HomePhone]
      ,[MobilePhone]
      ,[OtherPhone]
      ,[ImageId]
	  ,[AddressId]
      ,[Notes]
      ,[ContactSourceId]
	  ,CP.Status
	  ,'798613EE-7B85-4628-A6CC-E17EE80C09C5'
	  ,'2014-07-29 00:00:00.000'
  FROM         [dbo].[USMarketierUserProfile] CP INNER JOIN
                      USMembership M ON M.UserId = CP.UserId INNER JOIN
                      USUser U ON M.UserId = U.Id
					 
WHERE     U.Status !=3  and CP.UserId NOT IN
                          (SELECT     CM.Id
                            FROM          USCommerceUserProfile CM)							
							and CP.UserId NOT IN(
							SELECT ID FROM MKContact)
 EXCEPT
 SELECT Id
	  ,[Email]
      ,[FirstName]
      ,[MiddleName]
      ,[LastName]
      ,[CompanyName]
	  ,[Gender]
      ,[BirthDate]
      ,[HomePhone]
      ,[MobilePhone]
      ,[OtherPhone]
      ,[ImageId]
	  ,[AddressId]
      ,[Notes]
      ,[ContactSourceId]
	  ,Status
	  ,'798613EE-7B85-4628-A6CC-E17EE80C09C5'
	  ,'2014-07-29 00:00:00.000'
FROM
MKContact

GO

PRINT 'Updating MKCaontctSite for all the contacts' 
GO
 IF ((SELECT COUNT(1) FROM MKContactSite ) = 0)
  BEGIN
  
	  Declare @tmp_Contacts table(UserId uniqueidentifier)

	  INSERT INTO @tmp_Contacts
	  select userId from [dbo].[vw_contacts] 
	  Where ContactType=0 --and UserId in (select UserId from USMemberProfile )
	  UNION 
	  Select Id FROM [dbo].MKContact
  
	  INSERT INTO MKContactSite
	  (
		ContactId,
		SiteId,
		Status
	  )
	  SELECT UserId, '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', 1
	  FROM @tmp_Contacts TC
	  	--WHERE TC.UserId NOT IN(
				--			SELECT ID FROM MKContact)
	  
	  
		  
		 --  DELETE CP FROM    [dbo].[USMarketierUserProfile] CP 
	  --INNER JOIN @tmp_Contacts C on CP.UserId =C.UserId

	  --DELETE M FROM USMemberProfile M 
	  --INNER JOIN @tmp_Contacts C on M.UserId =C.UserId
	  

	  --DELETE M FROM USMembership M 
	  --INNER JOIN @tmp_Contacts C on M.UserId =C.UserId
	  
	  -- DELETE U FROM USSiteUser U
	  --INNER JOIN @tmp_Contacts C on U.UserId =C.UserId
	  
	  --DELETE U FROM USUser U 
	  --INNER JOIN @tmp_Contacts C on U.Id =C.UserId
	  END
 
GO
PRINT 'Making users as deleted which are all moved to MKContact' 
GO
	UPDATE USUser SET Status =3, ModifiedDate='2014-07-29 00:00:00.000',Description='Migrated to MKContact Table' Where Id in (Select Id from MKContact)
  GO
  PRINT 'Updating the contact count for each DL'
  GO
Update LS SET LS.[Count] = C.Cnt
FROM TADistributionListSite LS
INNER JOIN (
Select DistributionListId,count(UserId) Cnt
from TADistributionListUser
Group By DistributionListId)C ON C.DistributionListId = LS.DistributionListId
GO

 --------------------BEGIN MIGRATE USER PROPERTIES TO ATTRIBUTES---------------------------
  GO

PRINT 'BEGIN MIGRATE USER PROPERTIES TO ATTRIBUTES'

DECLARE @Properties TABLE
    (
      PropertyName NVARCHAR(500)
    )

INSERT  INTO @Properties
        SELECT DISTINCT
                PropertyName
        FROM    dbo.USMemberProfile MP
                


DECLARE @SiteID UNIQUEIDENTIFIER
SET @SiteID = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

DECLARE @AttributeGroupName NVARCHAR(50)
SET @AttributeGroupName = 'Profile Import'

DECLARE @SystemUser UNIQUEIDENTIFIER
SELECT  @SystemUser = ID
FROM    dbo.USUser
WHERE   UserName = 'iappssystemuser'

DECLARE @iAPPSProductID UNIQUEIDENTIFIER
SELECT  @iAPPSProductID = ID
FROM    dbo.iAppsProductSuite

PRINT 'Starting insert of new attribute group'
INSERT  INTO dbo.ATAttributeGroup
        ( Id ,
          Title ,
          SiteId ,
          IsSystem ,
          CreatedDate ,
          CreatedBy ,
          ModifiedDate ,
          ModifiedBy ,
          Status ,
          ProductId ,
          IsGlobal
        )
        SELECT  TV.*
        FROM    ( SELECT    NEWID() AS ID ,						-- Id - uniqueidentifier
                            @AttributeGroupName AS AttributeGroupName ,	-- Title - nvarchar(255)
                            @SiteID AS SiteID ,					-- SiteId - uniqueidentifier
                            0 AS IsSystem ,						-- IsSystem - bit
                            GETUTCDATE() AS CreatedDate ,		-- CreatedDate - datetime
                            @SystemUser AS CreatedBy ,			-- CreatedBy - uniqueidentifier
                            GETUTCDATE() AS ModifiedDate ,		-- ModifiedDate - datetime
                            @SystemUser AS ModifiedBy ,			-- ModifiedBy - uniqueidentifier
                            0 AS [Status] ,						-- Status - int
                            @iAPPSProductID AS ProductId ,		-- ProductId - uniqueidentifier
                            1 AS IsGlobal						-- IsGlobal - bit
                  
                ) TV
                LEFT JOIN dbo.ATAttributeGroup AG ON AG.Title = TV.AttributeGroupName
        WHERE   AG.ID IS NULL
      
DECLARE @StringAttributeDataType UNIQUEIDENTIFIER
SELECT  @StringAttributeDataType = Id
FROM    dbo.ATAttributeDataType
WHERE   Title = 'String'
      
PRINT 'Starting insert of new attributes'

INSERT  INTO dbo.ATAttribute
        ( Id ,
          AttributeDataType ,
          AttributeDataTypeId ,
          AttributeGroupId ,
          Title ,
          Description ,
          Sequence ,
          Code ,
          IsFaceted ,
          IsSearched ,
          IsSystem ,
          IsDisplayed ,
          IsEnum ,
          CreatedDate ,
          CreatedBy ,
          ModifiedDate ,
          ModifiedBy ,
          Status ,
          IsPersonalized ,
          IsMultiValued
              
        )
        SELECT  NEWID() ,					-- Id - uniqueidentifier
                'System.String' ,			-- AttributeDataType - nvarchar(50)
                @StringAttributeDataType ,	-- AttributeDataTypeId - uniqueidentifier
                AG.ID ,						-- AttributeGroupId - uniqueidentifier
                P.PropertyName ,			-- Title - nvarchar(50)
                P.PropertyName ,			-- Description - nvarchar(200)
                ( SELECT    MAX(Sequence)
                  FROM      dbo.ATAttribute
                ) ,							-- Sequence - int
                P.PropertyName ,			-- Code - nvarchar(64)
                0 ,							-- IsFaceted - bit
                0 ,							-- IsSearched - bit
                0 ,							-- IsSystem - bit
                0 ,							-- IsDisplayed - bit
                0 ,							-- IsEnum - bit
                GETUTCDATE() ,				-- CreatedDate - datetime
                @SystemUser ,				-- CreatedBy - uniqueidentifier
                GETUTCDATE() ,				-- ModifiedDate - datetime
                @SystemUser ,				-- ModifiedBy - uniqueidentifier
                1 ,							-- Status - int
                0 ,							-- IsPersonalized - bit
                0							-- IsMultiValued - bit
        FROM    @Properties P
                INNER JOIN dbo.ATAttributeGroup AG ON AG.Title = @AttributeGroupName
                LEFT JOIN dbo.ATAttribute A ON A.AttributeGroupId = AG.Id
                                               AND A.Title = P.PropertyName
        WHERE   A.ID IS NULL
    
PRINT 'Starting insert of new contact attributes values'   
INSERT  INTO dbo.ATContactAttributeValue
        ( Id ,
          ContactId ,
          AttributeId ,
          AttributeEnumId ,
          Value ,
          Notes ,
          CreatedDate ,
          CreatedBy 
          
            
        )
        SELECT  NEWID() ,				-- Id - uniqueidentifier
                C.Id ,					-- ContactId - uniqueidentifier
                A.Id ,					-- AttributeId - uniqueidentifier
                NULL ,					-- AttributeEnumId - uniqueidentifier
                MP.PropertyValueString ,-- Value - nvarchar(max)
                NULL ,					-- Notes - nvarchar(max)
                GETUTCDATE() ,			-- CreatedDate - datetime
                @SystemUser 			-- CreatedBy - uniqueidentifier
              
        FROM    dbo.USMemberProfile MP
                INNER JOIN dbo.MKContact C ON C.Id = MP.UserId
                INNER JOIN dbo.ATAttributeGroup AG ON AG.Title = @AttributeGroupName
                INNER JOIN dbo.ATAttribute A ON A.AttributeGroupId = AG.Id
                                                AND A.Title = MP.PropertyName
                LEFT JOIN dbo.ATContactAttributeValue CAV ON CAV.AttributeId = A.Id
                                                             AND CAV.ContactId = C.Id
        WHERE   CAV.ID IS NULL
    
        
DECLARE @ContactCategoryID INT
SELECT  @ContactCategoryID = ID
FROM    dbo.ATAttributeCategory
WHERE   Name = 'Contact Attributes'
        
PRINT 'Starting insert of new category items'        
INSERT  INTO dbo.ATAttributeCategoryItem
        ( Id ,
          AttributeId ,
          CategoryId ,
          IsRequired ,
          MinValue ,
          MaxValue ,
          ErrorMessage
                
        )
        SELECT  NEWID() ,			-- Id - uniqueidentifier
                TV.AttributeId ,	-- AttributeId - uniqueidentifier
                @ContactCategoryID ,-- CategoryId - int
                0 ,					-- IsRequired - bit
                NULL ,				-- MinValue - nvarchar(200)
                NULL ,				-- MaxValue - nvarchar(200)
                NULL				-- ErrorMessage - nvarchar(1000)
        FROM    ( SELECT DISTINCT
                            A.Id AS AttributeId
                  FROM      dbo.ATAttribute A
                            INNER JOIN dbo.ATAttributeGroup AG ON AG.Id = A.AttributeGroupId
                                                              AND AG.Title = @AttributeGroupName
                ) TV
                LEFT JOIN ATAttributeCategoryItem ACI ON ACI.AttributeId = TV.AttributeId
                                                         AND ACI.CategoryId = @ContactCategoryID
        WHERE   ACI.Id IS NULL


  GO
  --------------------END MIGRATE USER PROPERTIES TO ATTRIBUTES-----------------------------

  ----------------------------BEGIN MIGRATE PRImARY SITE USER -------------------------------


  
GO
	if not exists(select 1 from USSiteUser where IsPrimarySite is not null)
	UPDATE  SU SET SU.IsPrimarySite = case when T.Num =1 then 1 else 0 end
	FROM USSiteUser SU
	INNER JOIN (
	select UserId,ROW_NUMBER() over (partition by UserId order by UniqueNo) Num 
	from USSiteUser) T on SU.UserId=T.UserId

	GO
	
	if not exists(select 1 from MKContactSite where IsPrimarySite is not null)
	UPDATE  SU SET SU.IsPrimarySite = case when T.Num =1 then 1 else 0 end
	FROM MKContactSite SU
	INNER JOIN (
	select ContactId,ROW_NUMBER() over (partition by ContactId order by SiteId) Num 
	from MKContactSite) T on SU.ContactId=T.ContactId
GO

  --END MIGRATE PRIMARY SITE USER


--DEFAULT MARKITIER EMAIL TEMPLATES

-- Add default templates


GO

PRINT 'Updating assembly Bridgeline.CLRFunctions'


GO
PRINT N'Dropping [dbo].[GenerateXml]...';


GO
IF OBJECT_ID(N'[dbo].[GenerateXml]', 'AF') IS NOT NULL
DROP AGGREGATE [dbo].[GenerateXml];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedImageProperty]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedImageProperty]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedFileProperty]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedFileProperty]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetContentLocation]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetContentLocation]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetContentLocation];


GO
PRINT N'Dropping [dbo].[RegexSelectAll]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegexSelectAll]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[RegexSelectAll];


GO
PRINT N'Dropping [dbo].[Regex_ReplaceMatches]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_ReplaceMatches]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_ReplaceMatches];


GO
PRINT N'Dropping [dbo].[Regex_IsMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_IsMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_IsMatch];


GO
PRINT N'Dropping [dbo].[Regex_GetMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_GetMatch];


GO
PRINT N'Dropping [dbo].[NavFilter_GetQuery]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NavFilter_GetQuery]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[NavFilter_GetQuery];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsTextContentMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsTextContentMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsTextContentMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsPagePropertiesMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsPageNameMatch]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPageNameMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsPageNameMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsLinkInContentDefinition]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsImagePropertiesMatch]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsImagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsFilePropertiesMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsFilePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsContentDefinitionMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsContentDefinitionMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedTextContentText]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedTextContentText]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedPageProperties]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageProperties]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedPageName]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageName]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition];


GO
PRINT N'Dropping [dbo].[Regex_GetMatches]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatches]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_GetMatches];


GO
PRINT N'Dropping [dbo].[GetRelationTable]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRelationTable]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[GetRelationTable];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_Save]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Save]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_Save];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_RemoveContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_RemoveContainer]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_AddContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_AddContainer]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_AddContainer];


GO
PRINT N'Dropping [dbo].[CLRPageMap_SavePageDefinition]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMap_SavePageDefinition]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMap_SavePageDefinition];


GO
PRINT N'Dropping [dbo].[FindAndReplace_UpdateLinks]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_UpdateLinks]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[FindAndReplace_UpdateLinks];


GO
PRINT N'Dropping [dbo].[FindAndReplace_Replace]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Replace]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[FindAndReplace_Replace];


GO
PRINT N'Dropping [dbo].[FindAndReplace_Find]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Find]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[FindAndReplace_Find];


GO
PRINT N'Dropping [dbo].[Contact_SearchCLR]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_SearchCLR]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[Contact_SearchCLR];


GO
PRINT N'Dropping [dbo].[Contact_GetAutoDistributionListContactCount]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_GetAutoDistributionListContactCount]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount];


GO
PRINT N'Dropping [dbo].[CLSPageMapNode_AttachWorkflow]...';


GO
IF OBJECT_ID(N'[dbo].[CLSPageMapNode_AttachWorkflow]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_Update]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Update]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_Update];


GO
PRINT N'Dropping [Bridgeline.CLRFunctions]...';


GO
IF EXISTS (SELECT 1 FROM sys.assemblies WHERE name = N'Bridgeline.CLRFunctions')
DROP ASSEMBLY [Bridgeline.CLRFunctions];


GO
PRINT N'Creating [Bridgeline.CLRFunctions]...';
GO

PRINT N'Adding WebApiAdministrator...';
IF NOT EXISTS (SELECT * FROM USRoles WHERE Name ='WebApiAdministrator')
BEGIN
	DECLARE @CreatedById	uniqueidentifier
	SET @CreatedById = 'B10CB05D-7000-49AD-B115-872C19F5700B' --This has to be modified to InstallAdmin

	DECLARE @CMSProductId uniqueidentifier
	SET @CMSProductId ='CBB702AC-C8F1-4C35-B2DC-839C26E39848'

	DECLARE @SiteId uniqueidentifier
	SET @SiteId = '8039ce09-e7da-47e1-bcec-df96b5e411f4'

	DECLARE @GroupObjectTypeId int
	SET @GroupObjectTypeId = 15 

	DECLARE @WebApiAdminRoleId int
	SELECT @WebApiAdminRoleId = MAX(Id) + 1 FROM USRoles
	
	DECLARE @WebApiAdminGroupId uniqueidentifier
	DECLARE @WebApiAdminId uniqueidentifier

	INSERT INTO USRoles (ApplicationId,Id,[Name],Description,IsGlobal) 
	VALUES (@CMSProductId, @WebApiAdminRoleId, 'WebApiAdministrator','WebApi Admin for the system', 1)
	
	EXEC Group_Save  
		@CMSProductId,
		@ApplicationId=@CMSProductId, 
		@Id = @WebApiAdminGroupId output,
		@Title = 'WebApi Administrator',
		@Description = 'WebApi Admin Group',
		@CreatedBy=@CreatedById,
		@ModifiedBy=NULL,
		@ExpiryDate=NULL,
		@Status=NULL,
		@GroupType=1
	
	EXEC HierarchyNode_Save 
		@Id=@WebApiAdminGroupId output, 
		@Title=N'WebApi Administrator',
		@ParentId=@SiteId,
		@Keywords=NULL,
		@ObjectTypeId=@GroupObjectTypeId, 
		@ApplicationId=@SiteId
	
	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
	VALUES(@CMSProductId, @SiteId,@WebApiAdminGroupId,2,@WebApiAdminRoleId, @CMSProductId,1,1 )

	EXEC Membership_CreateUser 
		@CMSProductId,
		@ApplicationId = @SiteId,
		@UserName = 'WebApiAdmin',
		@Password='webapiadmin!@#',
		@PasswordFormat=0,
		@PasswordSalt=NULL,
		@Email='webapiadmin@iapps.com',
		@PasswordQuestion=NULL,
		@PasswordAnswer=NULL,
		@IsApproved=1,
		@UniqueEmail=1,
		@CreatedBy=@CreatedById,
		@IsSystemUser=1,
		@UserProfile=NULL,
		@UserId=@WebApiAdminId output,
		@CreatedDate=NULL,
		@ExpiryDate='9999-12-31 23:59:59.997', 
		@FirstName='WebApi', 
		@LastName='Administrator'
END
GO
PRINT N'default data to insert tasks for contact sync to isys';

IF NOT EXISTS (select 1 from TAJob Where Title='Sync Contact for ISYS')
BEGIN
	declare @scheduleId uniqueidentifier
	Declare @ScheduleTypeId uniqueidentifier
	declare @p1 uniqueidentifier
	declare @p2 xml
	declare @p4 uniqueidentifier
	exec Schedule_Save @Id=@scheduleId output,@Title=NULL,@Description=NULL,@StartDate='2014-09-19 00:00:00',@EndDate=NULL,@StartTime='2014-09-19 00:00:00',@EndTime=NULL,@MaxOccurrence=1,@Type=0,@LastRunTime=NULL,@NextRunTime='2014-09-19 04:00:00',@ScheduleTypeId=NULL,@ModifiedBy='DB5AC9DC-F537-496A-BE2A-42A50E5E829D',@ModifiedDate=NULL,@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
	exec ScheduleDaily_Save @Id=@ScheduleTypeId output,@ScheduleId=@scheduleId,@OccursEvery=1,@WeekDaysOnly=0,@Status=1,@ModifiedBy='DB5AC9DC-F537-496A-BE2A-42A50E5E829D',@ModifiedDate=NULL,@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
	exec Schedule_Save @Id=@p1 output,@Title=NULL,@Description=NULL,@StartDate='2014-09-19 00:00:00',@EndDate=NULL,@StartTime='2014-09-19 00:00:00',@EndTime=NULL,@MaxOccurrence=1,@Type=1,@LastRunTime=NULL,@NextRunTime='2014-09-19 04:00:00',@ScheduleTypeId=@ScheduleTypeId,@ModifiedBy='DB5AC9DC-F537-496A-BE2A-42A50E5E829D',@ModifiedDate=NULL,@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
	set @p2=convert(xml,N'<Job><Id>00000000-0000-0000-0000-000000000000</Id><Title>Sync Contact for ISYS</Title><Description/><CreatedBy>00000000-0000-0000-0000-000000000000</CreatedBy><CreatedDate>1/1/0001</CreatedDate><ModifiedBy>00000000-0000-0000-0000-000000000000</ModifiedBy><ModifiedDate>1/1/0001</ModifiedDate><Status>1</Status><Tasks><Task><Id>89f3eec8-42f2-4e14-80d5-b6847244c6b4</Id><Title>Sync Contact for ISYs</Title><Description/><JobId>00000000-0000-0000-0000-000000000000</JobId><Sequence>0</Sequence><Path>Bridgeline.FW.TaskBlock.TaskProxy</Path><Parameter>&lt;LibraryTask xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"&gt;&lt;AssemblyName&gt;Bridgeline.FW.TaskBlock&lt;/AssemblyName&gt;&lt;TypeName&gt;Bridgeline.FW.TaskBlock.TaskProxy&lt;/TypeName&gt;&lt;MethodName&gt;SyncContacts&lt;/MethodName&gt;&lt;/LibraryTask&gt;</Parameter><ScheduleId>' + cast(@p1 as varchar(36)) + '</ScheduleId><ObjectId>00000000-0000-0000-0000-000000000000</ObjectId><TaskType>3</TaskType><CreatedBy>00000000-0000-0000-0000-000000000000</CreatedBy><CreatedDate>1/1/0001</CreatedDate><ModifiedBy>00000000-0000-0000-0000-000000000000</ModifiedBy><ModifiedDate>1/1/0001</ModifiedDate><Status>1</Status></Task></Tasks></Job>')
	--set @p4='1CB5152C-E5A5-4385-8D86-AA2299AF7393'
	exec Job_Save @xmlData=@p2,@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@ModifiedBy='DB5AC9DC-F537-496A-BE2A-42A50E5E829D',@Id=@p4 output
END

GO
---------START---------- DEFAULT DATA FOR ALL CONTACTS Contact List----------START------------------



DECLARE @DistributionListTitle NVARCHAR(100)
DECLARE @DistributionListDescription NVARCHAR(100)
SET @DistributionListTitle = 'All Contacts'
SET @DistributionListDescription = 'All contacts in iAPPS'

PRINT 'Creating Contact list groups'

INSERT  INTO dbo.TADistributionGroup
        ( Id ,
          Title ,
          Description ,
          CreatedDate ,
          CreatedBy ,
          ModifiedDate ,
          ModifiedBy ,
          Status ,
          ApplicationId,
		  IsSystem
        )
        SELECT  NEWID() ,
                @DistributionListTitle ,
                @DistributionListDescription ,
                GETUTCDATE() ,
                '00000000-0000-0000-0000-000000000000' ,
                GETUTCDATE() ,
                '00000000-0000-0000-0000-000000000000' ,
                1 ,
                S.ID,
				1
        FROM    dbo.SISite S
                LEFT JOIN dbo.TADistributionGroup TG ON TG.ApplicationId = S.Id
                                                        AND TG.Title = @DistributionListTitle
                                                        AND TG.Status = 1
        WHERE   MasterSiteId = S.ID
                AND TG.Id IS NULL
                
     PRINT 'Creating Contact lists'
          

INSERT  INTO dbo.TADistributionLists
        ( Id ,
          Title ,
          Description ,
          CreatedDate ,
          CreatedBy ,
          ModifiedDate ,
          ModifiedBy ,
          Status ,
          ApplicationId ,
          TrackingId ,
          ListType ,
          IsGlobal,
		  IsSystem
        )
        SELECT  NEWID() ,
                @DistributionListTitle ,
                @DistributionListDescription ,
                GETUTCDATE() ,
                '00000000-0000-0000-0000-000000000000' ,
                GETUTCDATE() ,
                '00000000-0000-0000-0000-000000000000' ,
                1 ,
                S.ID ,
                NULL ,
                1 ,
                1,
				1
        FROM    dbo.SISite S
                LEFT JOIN TADistributionLists DL ON DL.ApplicationId = S.Id
                                                    AND DL.Title = @DistributionListTitle
                                                    AND DL.Status = 1
        WHERE   MasterSiteId = S.ID
                AND DL.ID IS NULL
        
        
		PRINT 'Associating contact lists to contact list group'


INSERT  INTO dbo.TADistributionListGroup
        ( DistributionGroupId ,
          DistributionId
                
        )
        SELECT  TG.Id ,
                TL.Id
        FROM    dbo.TADistributionLists TL
                CROSS JOIN dbo.TADistributionGroup TG
                INNER JOIN dbo.SISite S ON S.Id = TG.ApplicationId
                                           AND S.Id = TL.ApplicationId
                LEFT JOIN dbo.TADistributionListGroup TLG ON TLG.DistributionGroupId = TG.Id
                                                             AND TLG.DistributionId = TL.ID
        WHERE   TL.Title = @DistributionListTitle
                AND TG.Status = 1
                AND TL.Status = 1
                AND S.Id = S.MasterSiteId
                AND TG.Title = @DistributionListTitle
                AND TLG.DistributionGroupId IS NULL
                
            
            
                
                

				PRINT 'Creating search query for existing dynamic lists'

                
                
INSERT  INTO dbo.CTSearchQuery
        ( Id ,
          SearchXml ,
          Status ,
          CreatedBy ,
          CreatedDate ,
          ModifiedBy ,
          ModifiedDate
                        
        )
        SELECT  NEWID() ,
                CAST(N'<ContactSearch xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <SiteId>' + CAST(S.Id AS NVARCHAR(50)) + '</SiteId>
  <CreatedDate>0001-01-01T00:00:00</CreatedDate>
  <ModifiedDate>0001-01-01T00:00:00</ModifiedDate>
  <Status>1</Status>
  <ContactSources>
    <int>9</int>
    <int>3</int>
    <int>2</int>
    <int>4</int>
    <int>6</int>
    <int>5</int>
    <int>7</int>
    <int>8</int>
    <int>1</int>
  </ContactSources>
</ContactSearch>' AS XML) ,
                1 ,
                S.Id ,
                GETUTCDATE() ,
                S.Id ,
                GETUTCDATE()
        FROM    dbo.SISite S
                LEFT JOIN CTSearchQuery SQ ON SQ.CreatedBy = S.Id
        WHERE   MasterSiteId = S.ID
                AND SQ.Id IS NULL
        
        
INSERT  INTO dbo.TADistributionListSearch
        ( Id ,
          DistributionListId ,
          SearchQueryId
                
        )
        SELECT  NEWID() ,
                TL.Id ,
                SQ.ID
        FROM    dbo.SISite S
                INNER JOIN dbo.TADistributionLists TL ON TL.ApplicationId = S.Id
                                                         AND TL.Title = @DistributionListTitle
                                                         AND TL.Status = 1
                INNER JOIN dbo.CTSearchQuery SQ ON SQ.CreatedBy = S.Id
                LEFT JOIN dbo.TADistributionListSearch DLS ON DLS.DistributionListId = TL.Id
        WHERE   S.Id = S.MasterSiteId
                AND DLS.ID IS NULL
                
                



----------END---------- DEFAULT DATA FOR ALL CONTACTS Contact List--------------END------------------

GO
-------------------------------------------------Start Migrate CTSearch Operators-------------------------------------------------
PRINT 'Migrating CTSearch Operators'

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorEqual"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') = '='

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorNotEqual"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') = '!='

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorLessThan"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') = '<'

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorLessThanEqual"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') = '<='

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorGreaterThan"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') = '>'

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorGreaterThanEqual"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') = '>='

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorBetween"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') LIKE 'between'

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorContains"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') LIKE 'contains'

UPDATE  dbo.CTSearchQuery
SET     SearchXml.modify('replace value of (/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1] with "opOperatorDoesNotContain"')
WHERE   SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') LIKE 'not contains' OR SearchXml.value('(/ContactSearch/ContactAttributeSearch/AttributeSearchItem/@Operator)[1]',
                        'varchar(max)') LIKE 'does not contain'

-------------------------------------------------End Migrate CTSearch Operators---------------------------------------------------
GO
PRINT 'Adding Column PageMapNode.CssClass'
GO

-- Modification to PageMapNode Css Class START ---
IF COL_LENGTH(N'[dbo].[PageMapNode]', N'CssClass') IS  NULL
 ALTER TABLE PageMapNode Add [CssClass] [nvarchar](125) NULL
GO

PRINT 'Altering procedure PageMapNode_Get'
GO

ALTER PROCEDURE [dbo].[PageMapNode_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageMapNodeIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageMapNodeIds
	SELECT PageMapNodeId FROM PageMapNode
		WHERE SiteId = @siteId AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT PageMapNodeId,ParentId,LftValue,RgtValue, SiteId, ExcludeFromSiteMap, --PageMapNodeXml, 
		Description, DisplayTitle, FriendlyUrl, MenuStatus, TargetId, Target, TargetUrl, CreatedBy,
		dbo.ConvertTimeFromUtc(CreatedDate, @siteId) AS CreatedDate, ModifiedBy, 
		dbo.ConvertTimeFromUtc(ModifiedDate, @siteId) AS ModifiedDate, PropogateWorkflow, InheritWorkflow, PropogateSecurityLevels,
		InheritSecurityLevels, PropogateRoles, InheritRoles, Roles, LocationIdentifier, HasRolloverImages,
		RolloverOnImage, RolloverOffImage, IsCommerceNav, AssociatedQueryId, DefaultContentId, 
		AssociatedContentFolderId, CustomAttributes, HiddenFromNavigation, SourcePageMapNodeId, CssClass
	FROM PageMapNode P 
		JOIN @PageMapNodeIds T ON P.PageMapNodeId = T.Id
	ORDER BY LftValue

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId 
	FROM USSecurityLevelObject S
		JOIN @PageMapNodeIds T ON S.ObjectId = T.Id
	WHERE S.ObjectTypeId = 2

	SELECT W.Id, W.PageMapNodeId, 
		W.WorkflowId, W.CustomAttributes 
	FROM PageMapNodeWorkflow W
		JOIN @PageMapNodeIds T ON W.PageMapNodeId = T.Id
	
	SELECT M.PageDefinitionId, 
		M.PageMapNodeId, M.DisplayOrder 
	FROM PageMapNodePageDef M
		JOIN @PageMapNodeIds T ON M.PageMapNodeId = T.Id

	SELECT P.PageMapNodeId, C.ContentId, RelativePath, FileName FROM COFile C
		JOIN PageMapNode P ON C.ContentId = P.TargetId
		JOIN @PageMapNodeIds T ON P.PageMapNodeId = T.Id
END
GO

PRINT 'Altering procedure PageMapNode_Save'
GO

ALTER PROCEDURE [dbo].[PageMapNode_Save] 
(
--********************************************************************************
-- Parameters
--
--********************************************************************************
		@Id uniqueidentifier output,
		@SiteId uniqueidentifier,
		@ParentNodeId uniqueidentifier,
		@PageMapNode xml,
		@CreatedBy uniqueidentifier,
		@ModifiedBy uniqueidentifier='00000000-0000-0000-0000-000000000000',
		@ModifiedDate datetime=null
)	
AS
BEGIN

Declare
	@PageMapNodeId [uniqueidentifier],
	@ExcludeFromSiteMap bit,
	@Description [nvarchar](500),
	@DisplayTitle [nvarchar](256),
	@FriendlyUrl [nvarchar](500),
	@MenuStatus [int],
	@TargetId [uniqueidentifier],
	@Target [int],
	@TargetUrl [nvarchar](500),
	@PropogateWorkflow [bit],
	@InheritWorkflow [bit],
	@PropogateSecurityLevels [bit],
	@InheritSecurityLevels [bit],
	@PropogateRoles [bit],
	@InheritRoles [bit],
	@Roles [nvarchar](max),
	@LocationIdentifier [nvarchar](50),
	@HasRolloverImages [bit],
	@RolloverOnImage [nvarchar](256),
	@RolloverOffImage [nvarchar](256),
	@IsCommerceNav [bit],
	@AssociatedQueryId [uniqueidentifier],
	@DefaultContentId [uniqueidentifier],
	@AssociatedContentFolderId [uniqueidentifier],
	@SecurityLevels	[varchar](max),
	@PageMapNodeWorkFlow [varchar](max),
	@CustomAttributes [xml],
	@HiddenFromNavigation [bit],
	@CssClass [nvarchar](125)

Set @ExcludeFromSiteMap = 0
Set @PropogateWorkflow = 0
Set @InheritWorkflow = 0
Set @InheritSecurityLevels = 0
Set @InheritRoles = 0
Set @HasRolloverImages = 0
Set @IsCommerceNav = 0
Set @HiddenFromNavigation = 0


Set @PageMapNodeId = @PageMapNode.value('(pageMapNode/@id)[1]','uniqueidentifier')
Set @ExcludeFromSiteMap = CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@excludeFromSiteMap)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@Description =@PageMapNode.value('(pageMapNode/@description)[1]','nvarchar(500)')
set	@DisplayTitle  =@PageMapNode.value('(pageMapNode/@displayTitle)[1]','nvarchar(256)')
Set	@FriendlyUrl  =@PageMapNode.value('(pageMapNode/@friendlyUrl)[1]','nvarchar(500)')
Set	@MenuStatus =@PageMapNode.value('(pageMapNode/@menuStatus)[1]','int')
Set	@TargetId  =@PageMapNode.value('(pageMapNode/@targetId)[1]','uniqueidentifier')
Set	@Target =@PageMapNode.value('(pageMapNode/@target)[1]','int')
Set	@TargetUrl  =@PageMapNode.value('(pageMapNode/@targetUrl)[1]','nvarchar(500)')
set	@CssClass  =@PageMapNode.value('(pageMapNode/@cssClass)[1]','nvarchar(125)')

Set	@ModifiedBy = @ModifiedBy
Set	@ModifiedDate = GETUTCDATE()
Set	@PropogateWorkflow =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@propogateWorkflow)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@InheritWorkflow =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@inheritWorkflow)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@PropogateSecurityLevels =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@propogateSecurityLevels)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@InheritSecurityLevels =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@inheritSecurityLevels)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@PropogateRoles  =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@propogateRoles)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@InheritRoles  =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@inheritRoles)[1]','char(5)'))='false'
							THEN 0
							ELSE 1
							END 
Set	@Roles  =@PageMapNode.value('(pageMapNode/@roles)[1]','nvarchar(max)')

Set	@LocationIdentifier=@PageMapNode.value('(pageMapNode/@locationIdentifier)[1]','nvarchar(500)')

Set	@HasRolloverImages =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@hasRolloverImages)[1]','char(5)'))='true'
							THEN 1
							ELSE 0
							END 
Set	@RolloverOnImage =@PageMapNode.value('(pageMapNode/@rolloverOnImage)[1]','nvarchar(256)')
Set	@RolloverOffImage =@PageMapNode.value('(pageMapNode/@rolloverOffImage)[1]','nvarchar(256)')
Set	@IsCommerceNav =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@IsCommerceNav)[1]','char(1)'))='0'
							THEN 0
							ELSE 1
							END 
Set	@AssociatedQueryId =@PageMapNode.value('(pageMapNode/@AssociatedQueryId)[1]','uniqueidentifier')
Set	@DefaultContentId =@PageMapNode.value('(pageMapNode/@DefaultContentId)[1]','uniqueidentifier')
Set	@AssociatedContentFolderId =@PageMapNode.value('(pageMapNode/@AssociatedContentFolderId)[1]','uniqueidentifier')
Set	@SecurityLevels	 =@PageMapNode.value('(pageMapNode/@securityLevels)[1]','nvarchar(max)')

DECLARE @WFCount INT ,@CurrentWFnode INT
DECLARE @workFlowId UNIQUEIDENTIFIER
set @WFCount=@PageMapNode.value('count(/pageMapNode/pageMapNodeWorkFlow)','int')	
					set @CurrentWFnode=1
					while(@CurrentWFNode <= @WFCount)
					Begin
						set @workFlowId=@PageMapNode.value('(/pageMapNode/pageMapNodeWorkFlow[sql:variable("@CurrentWFNode")]/@id)[1]','uniqueidentifier')
						SET @PageMapNodeWorkFlow = @PageMapNodeWorkFlow + ',' + CAST(@workFlowId AS CHAR(36))
						set @CurrentWFnode =@CurrentWFnode +1
					End 

Set	@HiddenFromNavigation =CASE WHEN
							 lower(@PageMapNode.value('(pageMapNode/@HiddenFromNavigation)[1]','char(5)'))='true'
							THEN 1
							ELSE 0
							END 



EXEC PageMapNode_SaveWithParams @PageMapNodeId,@ParentNodeId,@SiteId ,@ExcludeFromSiteMap,@Description ,@DisplayTitle,
	@FriendlyUrl,@MenuStatus,@TargetId,	@Target,@TargetUrl,@ModifiedBy,@ModifiedDate,
	@PropogateWorkflow,	@InheritWorkflow ,@PropogateSecurityLevels ,
	@InheritSecurityLevels,
	@PropogateRoles ,
	@InheritRoles ,
	@Roles,
	@LocationIdentifier,
	@HasRolloverImages,
	@RolloverOnImage,
	@RolloverOffImage,
	@IsCommerceNav ,
	@AssociatedQueryId ,
	@DefaultContentId ,
	@AssociatedContentFolderId ,
	@SecurityLevels	,
	@PageMapNodeWorkFlow ,
	@CustomAttributes,
	@HiddenFromNavigation,
	@CssClass 

EXEC [PageDefinition_AdjustDisplayOrder] @PageMapNodeId

END
GO
PRINT 'Altering procedure PageMapNode_SaveWithParams'
GO

ALTER PROCEDURE [dbo].[PageMapNode_SaveWithParams] 
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0,
	@CssClass [nvarchar](125) = NULL
)	
AS
BEGIN
	Declare @LftValue int,@RgtValue int
	Declare @P_PropogateWorkflow bit,
			@P_PropogateSecurityLevels bit,
			@P_PropogateRoles bit

	Set @ModifiedDate = cast(isnull(@ModifiedDate,getdate()) as varchar(50))
	Set @LftValue=0
	Set @RgtValue=0
	
	IF (@ParentId is not null) 
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue, 
			@P_PropogateWorkflow =PropogateWorkflow,
			@P_PropogateSecurityLevels =PropogateSecurityLevels,
			@P_PropogateRoles=PropogateRoles
		from PageMapNode 
		Where PageMapNodeId=@ParentId and SiteId=@SiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END
	
	IF @LftValue <=0
	begin
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	end

	if((@ParentId is not null) AND @P_PropogateSecurityLevels=1 OR @InheritSecurityLevels=1)
	BEGIN
		INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
		Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
		Where ObjectId=@ParentId
	END
			
	INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
	Select @PageMapNodeId,2,Value from SplitComma(@SecurityLevels,',')
	Where len(Value)>0
	except 
	Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
	Where ObjectId=@PageMapNodeId

	Update PageMapNode Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
							RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 
	Where SiteId=@SiteId

	INSERT INTO [dbo].[PageMapNode]
           ([PageMapNodeId]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[ExcludeFromSiteMap]
           ,[Description]
           ,[DisplayTitle]
           ,[FriendlyUrl]
           ,[MenuStatus]
           ,[TargetId]
           ,[Target]
           ,[TargetUrl]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[PropogateWorkflow]
           ,[InheritWorkflow]
           ,[PropogateSecurityLevels]
           ,[InheritSecurityLevels]
           ,[PropogateRoles]
           ,[InheritRoles]
           ,[Roles]
           ,[LocationIdentifier]
           ,[HasRolloverImages]
           ,[RolloverOnImage]
           ,[RolloverOffImage]
           ,[IsCommerceNav]
           ,[AssociatedQueryId]
           ,[DefaultContentId]
           ,[AssociatedContentFolderId]
           ,[CustomAttributes]
           ,[HiddenFromNavigation]
		   ,[CssClass])
     VALUES
           (@PageMapNodeId
           ,@ParentId
           ,@RgtValue
           ,@RgtValue+1
           ,@SiteId
           ,@ExcludeFromSiteMap
           ,@Description
           ,@DisplayTitle
           ,@FriendlyUrl
           ,@MenuStatus
           ,@TargetId
           ,@Target
           ,@TargetUrl
           ,@ModifiedBy
           ,@ModifiedDate
           ,@ModifiedBy
           ,@ModifiedDate
           ,@PropogateWorkflow
           ,@InheritWorkflow
           ,@PropogateSecurityLevels
           ,@InheritSecurityLevels
           ,@PropogateRoles
           ,@InheritRoles
           ,@Roles
           ,@LocationIdentifier
           ,@HasRolloverImages
           ,@RolloverOnImage
           ,@RolloverOffImage
           ,@IsCommerceNav
           ,@AssociatedQueryId
           ,@DefaultContentId
           ,@AssociatedContentFolderId
           ,@CustomAttributes
           ,@HiddenFromNavigation
		   ,@CssClass)

	-- Moved down after the Insert page map node as there is a relationship with workflowId
	if((@ParentId is not null) AND @P_PropogateWorkflow=1 OR @InheritWorkflow=1)
	BEGIN
		--PRINT 'In propogate workflow'
		Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
		Select NEWID(),@PageMapNodeId,WorkflowId,CustomAttributes from PageMapNodeWorkflow
		Where PageMapNodeId=@ParentId
	END
	
	Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
	Select NEWID(),@PageMapNodeId,WorkflowId,NULL
	from (
	select Value WorkflowId
	from SplitComma(@PageMapNodeWorkFlow,',')
	Where LEN(Value)>0
	except
	Select WorkflowId 
	from PageMapNodeWorkflow
	Where PageMapNodeId=@PageMapNodeId
	)F

	EXEC  [dbo].[PageMapNode_UpdateMemberRoles] 
		@SiteId = @SiteId,
		@PageMapNodeId = @PageMapNodeId,
		@Propogate = @PropogateRoles,
		@Inherit = @InheritRoles
				
	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 1

END

GO
PRINT 'Altering procedure PageMapNode_Update'
GO
ALTER PROCEDURE [dbo].[PageMapNode_Update]
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0,
	@CssClass [nvarchar](125) = NULL
	)
AS
BEGIN
	Declare @Old_PropogateWorkflow bit,
		@Old_InheritWorkflow bit,
		@Old_PropogateSecurityLevels bit,
		@Old_InheritSecurityLevels bit,
		@Old_PropogateRoles bit,
		@Old_InheritRoles bit,
		@LftValue bigint,
		@RgtValue bigint

	if(@PageMapNodeId='00000000-0000-0000-0000-000000000000')
	BEGIN
		RAISERROR('INVALID||PageMapNodeId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	if not exists(Select 1 from PageMapNode where PageMapNodeId =@pageMapNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOEXISTS||PageMapNodeId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
		
	Select @Old_PropogateWorkflow =PropogateWorkflow,
			@Old_InheritWorkflow =InheritWorkflow,
			@Old_PropogateSecurityLevels =PropogateSecurityLevels,
			@Old_InheritSecurityLevels =InheritSecurityLevels,
			@Old_PropogateRoles=PropogateRoles,
			@Old_InheritRoles =InheritRoles,
			@LftValue =LftValue,
			@RgtValue=RgtValue
	FROM PageMapNode Where PageMapNodeId=@PageMapNodeId
			
	UPDATE [dbo].[PageMapNode]
	SET [SiteId] = @SiteId
      ,[ExcludeFromSiteMap] = @ExcludeFromSiteMap
      ,[Description] = @Description
      ,[DisplayTitle] = @DisplayTitle
      ,[FriendlyUrl] = @FriendlyUrl
      ,[MenuStatus] = @MenuStatus
      ,[TargetId] = @TargetId
      ,[Target] = @Target
      ,[TargetUrl] = @TargetUrl
      ,[ModifiedBy] = @ModifiedBy
      ,[ModifiedDate] = @ModifiedDate
      ,[PropogateWorkflow] = @PropogateWorkflow
      ,[InheritWorkflow] = @InheritWorkflow
      ,[PropogateSecurityLevels] = @PropogateSecurityLevels
      ,[InheritSecurityLevels] = @InheritSecurityLevels
      ,[PropogateRoles] = @PropogateRoles
      ,[InheritRoles] = @InheritRoles
      ,[Roles] = @Roles
      ,[LocationIdentifier] =@LocationIdentifier
      ,[HasRolloverImages] = @HasRolloverImages
      ,[RolloverOnImage] = @RolloverOnImage
      ,[RolloverOffImage] = @RolloverOffImage
      ,[IsCommerceNav] = @IsCommerceNav
      ,[AssociatedQueryId] = @AssociatedQueryId
      ,[DefaultContentId] = @DefaultContentId
      ,[AssociatedContentFolderId] = @AssociatedContentFolderId
      ,[CustomAttributes] = @CustomAttributes
      ,[HiddenFromNavigation] = @HiddenFromNavigation
	  ,[CssClass] = @CssClass
	WHERE  [PageMapNodeId] = @PageMapNodeId

	if (@ExcludeFromSiteMap=1)
	BEGIN
		Update C set C.ExcludeFromSiteMap = @ExcludeFromSiteMap
		FROM PageMapNode C 
		Inner Join PageMapNode P on C.LftValue>P.LftValue AND C.RgtValue < P.RgtValue and P.SiteId=C.SiteId
		Where  P.SiteId = @SiteId AND P.PageMapNodeId=@PageMapNodeId
	END			
	
	IF (@Old_PropogateSecurityLevels != @PropogateSecurityLevels 
		OR @Old_InheritSecurityLevels!=@InheritSecurityLevels
		OR @SecurityLevels!=''	)
	BEGIN
		DELETE FROM USSecurityLevelObject Where ObjectId=@PageMapNodeId
		EXEC dbo.PageMapNode_AttachSecurityLevels @SiteId=@SiteId
			,@PageMapNodeId=@PageMapNodeId
			,@SecurityLevels=@SecurityLevels
			,@Propogate= @PropogateSecurityLevels
			,@Inherit= @InheritSecurityLevels
			,@ModifiedBy=@ModifiedBy
			,@ModifiedDate=@ModifiedDate
	END
	ELSE IF (@SecurityLevels = '')
		DELETE FROM USSecurityLevelObject Where ObjectId=@PageMapNodeId

		
	IF (@Old_PropogateWorkflow != @PropogateWorkflow 
	OR @Old_InheritWorkflow!=@InheritWorkflow
	OR @PageMapNodeWorkFlow!='')
	BEGIN
		EXECUTE [dbo].[PageMapNode_AttachWorkFlow] 
		   @SiteId =@SiteId
		  ,@PageMapNodeId=@PageMapNodeId
		  ,@PageMapNodeWorkFlow=@PageMapNodeWorkFlow
		  ,@Propogate=@PropogateWorkflow
		  ,@Inherit=@InheritWorkflow
		  ,@AppendToExistingWorkflows =0
		  ,@ModifiedBy=@ModifiedBy
		  ,@ModifiedDate=@ModifiedDate
			  
	END

	EXEC  [dbo].[PageMapNode_UpdateMemberRoles] 
		@SiteId = @SiteId,
		@PageMapNodeId = @PageMapNodeId,
		@Propogate = @PropogateRoles,
		@Inherit = @InheritRoles
	
	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 2
END

GO



IF OBJECT_ID('Campaign_GetTreeSubscribedCampaignsWithGroup') IS NOT NULL
	DROP PROCEDURE Campaign_GetTreeSubscribedCampaignsWithGroup
GO

CREATE PROCEDURE [dbo].[Campaign_GetTreeSubscribedCampaignsWithGroup]
(
	@UserId			UNIQUEIDENTIFIER,
	@ApplicationId	UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @xmlResult XML
	DECLARE @SubscribedCampaigns TABLE (
				CampaignId	UNIQUEIDENTIFIER,
				CampaignGroupId UNIQUEIDENTIFIER
			)
	DECLARE @CampaignGroups TABLE (
				GroupId UNIQUEIDENTIFIER
			)
	
	-- Get all campaigns that the specified user has received emails from
	INSERT INTO	@SubscribedCampaigns (CampaignId, CampaignGroupId)
	SELECT DISTINCT esl.CampaignId, c.CampaignGroupId
	FROM MKEmailSendLog esl
		LEFT JOIN MKCampaign c on esl.CampaignId = c.Id
	WHERE UserId = @UserId

	-- Remove any campaigns that the user is unsubscribed from
	DELETE
	FROM	@SubscribedCampaigns
	WHERE	CampaignId IN (
				SELECT	CampaignId
				FROM	USUserUnsubscribe AS U
				WHERE	U.UserId = @UserId AND
						U.CampaignId IS NOT NULL
			) OR (
				EXISTS (
					SELECT	*
					FROM	USUserUnsubscribe AS U
					WHERE	U.UserId = @UserId AND
							U.CampaignId IS NULL
				) AND
				CampaignId NOT IN (
					SELECT	CampaignId
					FROM	USResubscribe AS R
					WHERE	R.UserId = @UserId AND
							R.CampaignId IS NOT NULL
				)
			)
	
	-- Get the groups which subscribed campaigns are in 
	INSERT INTO @CampaignGroups (GroupId)
	SELECT DISTINCT CampaignGroupId
	FROM @SubscribedCampaigns
	
	-- Return information as XML for treeview 
	SELECT CAST(
		(SELECT
			 NULL AS 'Id'
			,g.Name AS 'Text'
			,'true' AS 'ShowCheckBox'
			,(	SELECT c.Id, c.Title AS 'Text', 'true' AS 'ShowCheckBox' 
				FROM MKCampaign c 
				WHERE c.Id IN (SELECT CampaignId FROM @SubscribedCampaigns) AND c.CampaignGroupId = g.Id
				ORDER BY c.Title
				FOR XML RAW('TreeViewNode'), TYPE)
		FROM MKCampaignGroup g
		WHERE g.Id IN (SELECT GroupId FROM @CampaignGroups)
		ORDER BY g.Name
		FOR XML RAW('TreeViewNode'), ROOT('Nodes')
		) AS VARCHAR(MAX)
	) AS XmlData
END
GO

Update SITemplate Set ISNotShared = 0 where IsNotShared IS NULL
GO


ALTER PROCEDURE [dbo].[XMLForm_GetXMLForm] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(  
 @Id uniqueidentifier =null,  
 @ApplicationId uniqueidentifier = null,  
 @PageSize int=null,  
 @PageIndex int=null,  
 @ThemeId uniqueidentifier,  
 @ResolveThemeId bit = null  
)  
as  
Begin  
  
DECLARE @EmptyGUID uniqueidentifier  
SET @EmptyGUID = dbo.GetEmptyGUID()  
  
IF (@ResolveThemeId IS NULL)  
 Set  @ResolveThemeId  = 0    
  
 IF(@Id IS NOT NULL)  --In some case, from code library API all content definition is pulled from a site, we dont have to make applicationid null in that case, its casuing issue if there are multisite and both site is having the same content definition name
  BEGIN  
	  -- This condition is added as XMLForm was not pulled in case of multisite. If contentdefinitoin was created in a different site and used in another site, if we want to use Themeing AND want to use content defintion from a different site then we may have to visit this again.
	  IF NOT EXISTS( Select 1 from SISIte S 
					INNER JOIN SITheme ST ON S.ThemeId = ST.Id
					where S.Id = @ApplicationId) 
		SET	@ApplicationId = NULL
  END
	

 If (@PageSize is null and @PageIndex is null)   
 Begin  
  
  IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)  
   SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 13)   
  If(@Id is NOT NULL)
	BEGIN
		IF NOT EXISTS(Select 1 FROM SIObjectTheme where ObjectId = @Id and ThemeId = @ThemeId)
		BEGIN
			SET @ThemeId = NULL
		END
	  select A.Id,  
		ApplicationId,  
		Name,  
		XMLString,  
		CASE WHEN @ThemeId IS NULL THEN A.XsltString ELSE OT.CodeFile END AS XsltString,  
		A.Status,  
		Description,  
		A.XMLFileName,  
		CASE WHEN @ThemeId IS NULL THEN A.XSLTFileName ELSE OT.FileName END AS XSLTFileName,   
		A.CreatedDate,   
		A.CreatedBy,   
		A.ModifiedDate, A.ModifiedBy,   
		FN.UserFullName CreatedByFullName,  
		MN.UserFullName ModifiedByFullName,  
		@ThemeId  
		FROM COXMLForm A  
		LEFT JOIN SIObjectTheme OT ON OT.ObjectId = A.Id  AND OT.ObjectTypeId = 13  
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
	  where A.Id = Isnull(@Id, A.Id)  
		 and  
		 (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
		 and  
		 A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()  
		 AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)  
	END		 
	ELSE
		BEGIN
			with CTEXmlFormId(Id)
			AS(
			Select Id FROM COXMLForm A where 
			(@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) )  
			
			select  A.Id,  
			ApplicationId,  
			Name,  
			XMLString,  
			A.XsltString AS XsltString,  
			A.Status,  
			Description,  
			A.XMLFileName,  
			A.XSLTFileName AS XSLTFileName,   
			A.CreatedDate,   
			A.CreatedBy,   
			A.ModifiedDate, A.ModifiedBy,   
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,  
			@ThemeId  
			FROM COXMLForm A  
			INNER JOIN CTEXmlFormId X ON A.Id = X.Id
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		  where   
			 (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
			 and  
			 A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()  
		 
		END	 
 end  
 else    
 Begin  
  select  Id,  
    ApplicationId,  
    Name,  
    XMLString,  
    XsltString,  
    Status,  
    Description,  
    XMLFileName,  
    XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
    CreatedByFullName, ModifiedByFullName   
  from (select ROW_NUMBER() over (order by Id) as RowId,  
      Id,  
      ApplicationId,  
      Name,  
      XMLString,  
      XsltString,  
      Status,  
      Description,  
      XMLFileName,  
      XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
      FN.UserFullName CreatedByFullName,  
      MN.UserFullName ModifiedByFullName  
    from COXMLForm A  
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
    LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
    where Id = Isnull(@Id, Id)  
       and  
       (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
       and  
       Status != dbo.GetDeleteStatus()) --= dbo.GetActiveStatus()) -- it will change after creating updateStatus() mehod  
   as XMLFormWithRowNumbers  
  where RowId between ((@PageIndex -1) * @PageSize +1) and (@PageIndex * @PageSize)  
    
 end  
end 

GO

ALTER FUNCTION [dbo].[GetChildSites](@SiteId uniqueidentifier)  
  
RETURNS   
@SITES TABLE   
(  
 [Id]   uniqueidentifier         
 ,[Title]  nvarchar(600)      
 ,[Description] nvarchar(600)       
 ,[URL]  xml      
 ,[VirtualPath]  nvarchar(4000)      
 ,[ParentSiteId] uniqueidentifier       
 ,[Type]      int  
 ,[Keywords]  nvarchar(4000)     
 ,[CreatedBy] uniqueidentifier       
 ,[CreatedDate] datetime       
 ,[ModifiedBy]  uniqueidentifier      
 ,[ModifiedDate] datetime       
 ,[Status]  int      
 ,[LftValue]   int     
 ,[RgtValue]  int      
 ,MasterSiteId uniqueidentifier  
 ,CreatedByFullName nvarchar(4000)  
 ,ModifiedByFullName nvarchar(4000)  
)  
AS  
BEGIN  
  
DECLARE @DeleteStatus int        
SET @DeleteStatus = dbo.GetDeleteStatus()   
  
DECLARE @DraftStatus int  
SET @DraftStatus = dbo.GetSiteDraftStatus()   
    
 IF EXISTS(Select 1 FROM SISite where Id = @siteId and (ParentSiteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000' ))
	 INSERT INTO @sites   
	 SELECT  [Id]        
	 ,[Title]        
	 ,[Description]        
	 ,[URL]        
	 ,[VirtualPath]        
	 ,[ParentSiteId]        
	 ,[Type]        
	 ,[Keywords]        
	 ,[CreatedBy]        
	 ,[CreatedDate]        
	 ,[ModifiedBy]        
	 ,[ModifiedDate]        
	 ,[Status]        
	 ,[LftValue]        
	 ,[RgtValue]        
	 ,MasterSiteId  
	 ,CU.UserFullName CreatedByFullName        
	 ,MU.UserFullName ModifiedByFullName             
	 FROM SISITE S    
	 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy          
	 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy  
	 WHERE ParentSiteId = @SiteId  
	 AND STATUS NOT IN(@DeleteStatus,@DraftStatus)   
  
  
RETURN  
END  
GO
IF NOT EXISTS (SELECT 1 FROM sys.assemblies WHERE name = N'Bridgeline.CLRFunctions')
CREATE ASSEMBLY [Bridgeline.CLRFunctions]
    AUTHORIZATION [dbo]
    FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C0103007CC232540000000000000000E00002210B010B000064010000060000000000008E8201000020000000A001000000001000200000000200000400000000000000040000000000000000E0010000020000000000000300408500001000001000000000100000100000000000001000000000000000000000003C8201004F00000000A00100E80300000000000000000000000000000000000000C001000C000000048101001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E7465787400000094620100002000000064010000020000000000000000000000000000200000602E72737263000000E803000000A001000004000000660100000000000000000000000000400000402E72656C6F6300000C00000000C0010000020000006A010000000000000000000000000040000042000000000000000000000000000000007082010000000000480000000200050048BD0000BCC300000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000360002731500000A7D010000042A0000133003002500000001000011000F01281600000A0A062D18027B0100000472010000700F01281700000A6F1800000A262A5600027B010000040F017B010000046F1900000A262A0013300300390000000200001100027B010000041672230000706F1A00000A26027B0100000472570000706F1B00000A26027B010000046F1C00000A731D00000A0A2B00062A4E0002036F1E00000A731F00000A7D010000042A520003027B010000046F1C00000A6F2000000A002A00001B300700C609000003000011007E2200000A0B0F02FE16050000016F1C00000A282300000A131811182D7E000F02FE16050000016F1C00000A728D000070282400000A16FE01131811182D0A0072990000700B002B520F02FE16050000016F1C00000A722C010070282400000A16FE01131811182D0A0072400100700B002B280F02FE16050000016F1C00000A72EB010070282400000A16FE01131811182D080072030200700B0000721D020070028C09000001070E048C09000001282500000A0C72A6020070028C09000001070E048C09000001282500000A0D728B0300700A72BB030070130406732600000A130500732700000A1306732700000A130772F10300700F00282800000A13191219FE16110000016F1C00000A7220060070282900000A130811081105732A00000A130911056F2B00000A00110911086F2C00000A001109732D00000A130A110A11066F2E00000A26724406007072030900707217090070282F00000A1308110911086F2C00000A00110A11076F2E00000A2611066F3000000A6F3100000A11076F3000000A6F3100000A5817588D36000001130B110B167E2200000AA27E2200000A130E16130F722F0900700F01281700000A722F090070282900000A130E0F01281700000A6F3200000A13102B6800110E722F0900701210283300000A283400000A72330900701210283300000A7239090070282900000A6F3500000A130E110E723F0900701210283300000A283400000A72430900701210283300000A7239090070282900000A6F3500000A130E00111017591310111017FE0416FE01131811182D8A7249090070110E283400000A130E1713110011066F3000000A6F3600000A131A381F060000111A6F3700000A7440000001131200111272510900706F3800000AA53E000001130C1112726D0900706F3800000AA53E000001130D000011076F3000000A6F3600000A131B38A3050000111B6F3700000A7440000001131500111272890900706F3800000A6F1C00000A6F3900000A111572A10900706F3800000A6F1C00000A6F3900000A282400000A2C2C111272AB0900706F3800000A6F1C00000A111572BB0900706F3800000A6F1C00000A282400000A16FE012B011700131811183A2905000000111572C10900706F3800000A6F1C00000A72E509007072F50900706F3500000A6F3A00000A1314111272F70900706F3800000A6F1C00000A72090A0070720D0A00706F3500000A1313111272130A00706F3800000A6F1C00000A6F3A00000A6F3900000A721B0A0070282400000A16FE01131811182D1372250A0070111372250A0070282900000A1313111572290A00706F3800000A6F1C00000A723F0A0070282400000A16FE01131811183AB201000000111472430A0070282400000A2D11111472510A0070282400000A16FE012B011600131811182D7E00110B11111D8D36000001131C111C1672590A0070A2111C17111572A10900706F3800000A6F1C00000A72090A0070720D0A00706F3500000AA2111C1872650A0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A722F090070A2111C1B1113A2111C1C722F090070A2111C283B00000AA20038000100001114727D0A0070283C00000A16FE01131811182D7200110B11111C8D36000001131C111C16111572A10900706F3800000A6F1C00000A72090A0070720D0A00706F3500000AA2111C17722F090070A2111C18111272130A00706F3800000A6F1C00000AA2111C19728F0A0070A2111C1A1113A2111C1B72090A0070A2111C283B00000AA2002B7900110B11111D8D36000001131C111C1672950A0070A2111C17111572A10900706F3800000A6F1C00000A72090A0070720D0A00706F3500000AA2111C1872B90A0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A72C90A0070A2111C1B1113A2111C1C72110B0070A2111C283B00000AA2001111175813110038C4020000111572290A00706F3800000A6F1C00000A72230B0070282400000A16FE01131811183A9D0200000017130F111472510A0070282400000A2D11111472430A0070282400000A16FE012B011600131811183AC700000000110E72270B00706F3D00000A16FE01131811182D3C00110B1111722F0B0070111272AB0900706F3800000A6F1C00000A111272130A00706F3800000A6F1C00000A11136F3A00000A282500000AA2002B6F00110B11111D8D36000001131C111C16721E0C0070A2111C17111272AB0900706F3800000A6F1C00000AA2111C18723C0C0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A722F090070A2111C1B11136F3A00000AA2111C1C727A0C0070A2111C283B00000AA2000038A10100001114727D0A0070283C00000A16FE01131811183AC700000000110E72270B00706F3D00000A16FE01131811182D3C00110B1111727E0C0070111272AB0900706F3800000A6F1C00000A111272130A00706F3800000A6F1C00000A11136F3A00000A282500000AA2002B6F00110B11111D8D36000001131C111C16721E0C0070A2111C17111272AB0900706F3800000A6F1C00000AA2111C1872530D0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A728F0A0070A2111C1B11136F3A00000AA2111C1C72710D0070A2111C283B00000AA2000038C200000000110E72270B00706F3D00000A16FE01131811182D3C00110B111172770D0070111272AB0900706F3800000A6F1C00000A111272130A00706F3800000A6F1C00000A11136F1C00000A282500000AA2002B6F00110B11111D8D36000001131C111C16721E0C0070A2111C17111272AB0900706F3800000A6F1C00000AA2111C1872EC0E0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A72C90A0070A2111C1B11136F3A00000AA2111C1C72580F0070A2111C283B00000AA20000111117581311000000111B6F3E00000A131811183A4DFAFFFFDE1D111B7541000001131D111D14FE01131811182D08111D6F3F00000A00DC000000111A6F3E00000A131811183AD1F9FFFFDE1D111A7541000001131D111D14FE01131811182D08111D6F3F00000A00DC007E2200000A13160F02FE16050000016F1C00000A282300000A131811182D2D00726C0F00700F02FE16050000016F1C00000A722F0900700F03FE16050000016F1C00000A284000000A131600110F2C11110E72270B00706F3D00000A16FE012B011600131811182D3F001B8D36000001131C111C1672800F0070A2111C1708A2111C18110E110B284100000AA2111C1972B40F0070A2111C1A1116A2111C283B00000A1304002B3D001B8D36000001131C111C1672800F0070A2111C1709A2111C18110E110B284100000AA2111C1972B40F0070A2111C1A1116A2111C283B00000A13040000DE14110514FE01131811182D0811056F3F00000A00DC001104284200000A13172B0011172A0000414C000002000000B1020000BA0500006B0800001D000000000000000200000067020000360600009D0800001D0000000000000002000000E4000000BF080000A30900001400000000000000133002003700000005000011000F00281600000A0C082D25000F01281700000A0F02284300000A734400000A0A060F00281700000A6F4500000A0B2B04160B2B00072A00133002005100000006000011000F00281600000A0C082D3F000F01281700000A0F02284300000A734400000A0A060F00281700000A6F4500000A16FE010C082D15060F00281700000A6F4600000A6F4700000A0B2B0500140B2B00072A000000133002004C00000007000011000F00281600000A0C082D3A000F01281700000A0F02284300000A734400000A0A060F00281700000A6F4500000A16FE010C082D10060F00281700000A6F4800000A0B2B0500140B2B00072A62000302740E0000016F4700000A284200000A81050000012A000000133003003E00000006000011000F00281600000A0C082D2C000F01281700000A0F02284300000A734400000A0A060F00281700000A0F03281700000A6F4900000A0B2B04140B2B00072A000013300400FD0000000800001100731500000A0A72C00F0070732600000A0B076F4A00000A0C0872F00F00706F2C00000A00086F4B00000A72571600701F0E6F4C00000A0F00282800000A8C110000016F4D00000A00086F4B00000A725F1600701F0E6F4C00000A0F01282800000A8C110000016F4D00000A00076F2B00000A00086F4E00000A0D096F4F00000A16FE01130511052D2E002B1E0006726F16007009727D1600706F5000000A74360000016F1800000A2600096F5100000A130511052DD600096F5200000A00076F5300000A00066F5400000A19FE0216FE01130511052D1900066F1C00000A16066F5400000A19596F5500000A13042B097E2200000A13042B0011042A0000001B3004006601000009000011001A0F01281700000A0F02285600000A0F03285600000A28790000060A170F01281700000A0F02285600000A0F03285600000A28780000060B0006026F5700000A6F4800000A6F5800000A130838DB00000011086F3700000A740E0000010C00086F4700000A287E0000060D07096F4500000A16FE01130911092D08171307DDDE0000000F04285600000A16FE01130911093A95000000001714161628790000061304180F01281700000A0F02285600000A0F03285600000A28790000061305001104096F4800000A6F5800000A130A2B2C110A6F3700000A740E000001130600110511066F4700000A6F4500000A16FE01130911092D05171307DE6500110A6F3E00000A130911092DC7DE1D110A7541000001130B110B14FE01130911092D08110B6F3F00000A00DC00000011086F3E00000A130911093A15FFFFFFDE1D11087541000001130B110B14FE01130911092D08110B6F3F00000A00DC001613072B000011072A0000011C00000200D0003D0D011D0000000002004D00F23F011D000000001E02285900000A2A1E02285900000A2A1E02285900000A2A13300500330000000A0000110003027B200000047B1F000004027B210000047C1D000004281700000A027B22000004027B2300000428110000060A2B00062A0013300300220000000A0000110003027B1F000004027B1E0000047C1D000004281700000A28100000060A2B00062A000013300500070100000B000011738300000613051105047D1D00000400026F5A00000A130711073AE0000000140D73840000061304110411057D1E000004001A0F01281700000A0F03285600000A0F04285600000A28790000060A1104170F01281700000A0F03285600000A0F04285600000A28780000067D1F0000040F05285600000A16FE01130711072D5773860000060C0811047D200000040811057D2100000400081714161628790000067D2200000408180F01281700000A0F03285600000A0F04285600000A28790000067D2300000408FE0687000006735B00000A0B002B1700092D101104FE0685000006735B00000A0D2B00090B0006026F5700000A076F5C00000A13062B051413062B0011062A0013300300230000000C00001100026F4700000A287E0000060A0306046F4900000A0A06287F0000060A060B2B00072A1E02285900000A2A0013300300180000000A0000110003027B25000004027B24000004281C0000060A2B00062A13300300530000000D00001173880000060C08047D24000004080E047D2500000400026F4700000A287E0000060A0306087B240000046F4900000A0A08FE0689000006735B00000A0B0506076F5C00000A0A06287F0000060A060D2B00092A00133004009300000005000011001F200F03281700000A0F04285600000A0F05285600000A28780000060A0F00281600000A2D12060F00281700000A6F4500000A16FE012B0117000C082D04170B2B4E0F01281600000A2D12060F01281700000A6F4500000A16FE012B0117000C082D04170B2B290F02281600000A2D12060F02281700000A6F4500000A16FE012B0117000C082D04170B2B04160B2B00072A00133004004700000006000011000F00281600000A0C082D35001F200F01281700000A0F03285600000A0F04285600000A28780000060A060F00281700000A0F02281700000A6F4900000A0B2B04140B2B00072A00133004009300000005000011001F100F03281700000A0F04285600000A0F05285600000A28780000060A0F00281600000A2D12060F00281700000A6F4500000A16FE012B0117000C082D04170B2B4E0F01281600000A2D12060F01281700000A6F4500000A16FE012B0117000C082D04170B2B290F02281600000A2D12060F02281700000A6F4500000A16FE012B0117000C082D04170B2B04160B2B00072A00133004004700000006000011000F00281600000A0C082D35001F100F01281700000A0F03285600000A0F04285600000A28780000060A060F00281700000A0F02281700000A6F4900000A0B2B04140B2B00072A00133004002F0000000E000011001A0F01281700000A0F02285600000A0F03285600000A28780000060A060F00281700000A6F4500000A0B2B00072A00133004004600000006000011000F00281600000A0C082D34001A0F01281700000A0F03285600000A0F04285600000A28780000060A060F00281700000A0F02281700000A6F4900000A0B2B04140B2B00072A0000133004002E0000000E000011001E0F01281700000A0F02285600000A0F03285600000A28780000060A06026F5700000A6F4500000A0B2B00072A000013300400440000000600001100026F5A00000A0C082D33001E0F01281700000A0F03285600000A0F04285600000A28780000060A06026F5700000A0F02281700000A6F4900000A0B2B04140B2B00072A1B300400FE0000000F000011000F00281600000A130511053AE400000000170F01281700000A0F02285600000A0F03285600000A28780000060A060F00281700000A6F4500000A16FE01130511052D0817130438AE0000000F04285600000A16FE01130511053A95000000001714161628790000060B180F01281700000A0F02285600000A0F03285600000A28790000060C00070F00281700000A6F4800000A6F5800000A13062B2911066F3700000A740E0000010D0008096F4700000A6F4500000A16FE01130511052D05171304DE350011066F3E00000A130511052DCADE1D110675410000011307110714FE01130511052D0811076F3F00000A00DC0000001613042B000011042A00000110000002009B003AD5001D000000001E02285900000A2A1E02285900000A2A13300300220000000A0000110003027B28000004027B270000047C26000004281700000A281C0000060A2B00062A000013300500CB00000010000011738A00000613051105047D26000004000F00281600000A130711073AA3000000000F00281700000A0A170F01281700000A0F03285600000A0F04285600000A28780000060B070611057C26000004281700000A6F4900000A0A0F05285600000A16FE01130711072D55738B0000061304110411057D27000004001714161628790000060C1104180F01281700000A0F03285600000A0F04285600000A28790000067D280000041104FE068C000006735B00000A0D0806096F5C00000A0A000613062B051413062B0011062A0013300300130000000A0000110003026F4700000A046F4900000A0A2B00062A001B3004009B00000011000011001A14161628790000060A0F01281700000A0F02285600000A287B0000060B0006026F5700000A6F4800000A6F5800000A13052B3011056F3700000A740E0000010C00086F4700000A287E0000060D07096F4500000A16FE01130611062D05171304DE330011056F3E00000A130611062DC3DE1D110575410000011307110714FE01130611062D0811076F3F00000A00DC001613042B000011042A0001100000020033004174001D000000001B300500130100001200001100026F5A00000A130811083AFB000000001A14161628790000060A0F01281700000A0F03285600000A287B0000060B731500000A0C160D0006026F5700000A6F4800000A6F5800000A13092B6711096F3700000A740E00000113040011046F4700000A287E00000613050711050F02281700000A6F4900000A287F000006130608026F5700000A0911046F5D00000A09596F5500000A1106283400000A6F1B00000A2611046F5D00000A11046F5E00000A580D0011096F3E00000A130811082D8CDE1D11097541000001130A110A14FE01130811082D08110A6F3F00000A00DC0008026F5700000A09026F5700000A6F3200000A09596F5500000A6F1B00000A26086F1C00000A13072B051413072B0011072A000110000002004B0078C3001D000000001B300500720400001300001100728B0300700A06732600000A0B732700000A0C732700000A0D7289160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A007293160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A00729D160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072A7160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072B1160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072BB160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072C5160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072CF160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072D9160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072E3160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072EF16007002286300000A1306110607732A00000A1307076F2B00000A0011076F6400000A6F1C00000A13051105178D4E00000113101110161F2D9D11106F6500000A130811088E6918FE0116FE01131111113A8B020000000F01286600000A131111112D560072BE1700701108169A1108179A0F01FE16090000016F1C00000A282500000A1306110711066F2C00000A0011076F6400000A6F1C00000A13051105178D4E00000113101110161F2D9D11106F6500000A1308002B17001108161108169A286700000A176A58286800000AA20011088E6918FE0116FE01131111113AFC0100000072C51800701108169A1108179A282F00000A1306110711066F2C00000A001107732D00000A13091109086F2E00000A26076F5300000A00086F3000000A6F3100000A736900000A130A082C13086F3000000A6F3100000A16FE0216FE012B011700131111113A9001000000086F3000000A6F3600000A1312384D01000011126F3700000A7440000001130C00110C72501900706F3800000AA551000001110C72581900706F3800000AA55100000159176AFE0116FE01131111112D7B00110A110C6F6A00000A00110A6F6B00000A130D096F6C00000A130B17130E2B2D00110B110E1759110D110D8E69110E599A744000000172601900706F3800000A6F6D00000A0000110E1758130E110E110D8E69FE0216FE01131111112DC2096F3000000A110B6F6E00000A00110A6F6F00000A26003892000000002B7100110A6F7000000A7440000001130B110B72581900706F3800000AA551000001110C72581900706F3800000AA5510000012F29110B72501900706F3800000AA551000001110C72581900706F3800000AA551000001FE0216FE012B011700131111112D022B1E00110A6F6F00000A260000110A6F7100000A16FE02131111113A7CFFFFFF110A110C6F6A00000A00000011126F3E00000A131111113AA3FEFFFFDE1D111275410000011313111314FE01131111112D0811136F3F00000A00DC000000096F3000000A130F2B00110F2A0000411C000002000000E102000064010000450400001D00000000000000133003008E03000014000011000214FE010B073A0A030000000274400000010A06166F7200000A7E7300000A2E1306166F7200000A6F1C00000A282300000A2B0117000B072D190306166F7200000AA511000001287400000A81090000012B0B037E7500000A810900000106176F7200000A7E7300000A2E1306176F7200000A6F1C00000A282300000A2B0117000B072D190406176F7200000AA511000001287400000A81090000012B0B047E7500000A810900000106186F7200000A7E7300000A2E1306186F7200000A6F1C00000A282300000A2B0117000B072D190506186F7200000AA511000001287400000A81090000012B0B057E7500000A810900000106196F7200000A7E7300000A2E1306196F7200000A6F1C00000A282300000A2B0117000B072D1A0E0406196F7200000AA511000001287400000A81090000012B0C0E047E7500000A8109000001061A6F7200000A7E7300000A2E13061A6F7200000A6F1C00000A282300000A2B0117000B072D1A0E05061A6F7200000AA511000001287400000A81090000012B0C0E057E7500000A8109000001061B6F7200000A7E7300000A2E13061B6F7200000A6F1C00000A282300000A2B0117000B072D1A0E06061B6F7200000AA511000001287400000A81090000012B0C0E067E7500000A8109000001061C6F7200000A7E7300000A2E13061C6F7200000A6F1C00000A282300000A2B0117000B072D1A0E07061C6F7200000AA511000001287400000A81090000012B0C0E077E7500000A8109000001061D6F7200000A7E7300000A2E13061D6F7200000A6F1C00000A282300000A2B0117000B072D1A0E08061D6F7200000AA511000001287400000A81090000012B0C0E087E7500000A8109000001061E6F7200000A7E7300000A2E13061E6F7200000A6F1C00000A282300000A2B0117000B072D1A0E09061E6F7200000AA511000001287400000A81090000012B0C0E097E7500000A8109000001061F096F7200000A7E7300000A2E14061F096F7200000A6F1C00000A282300000A2B0117000B072D1B0E0A061F096F7200000AA511000001287400000A81090000012B0C0E0A7E7500000A8109000001002B7700037E7500000A8109000001047E7500000A8109000001057E7500000A81090000010E047E7500000A81090000010E057E7500000A81090000010E067E7500000A81090000010E077E7500000A81090000010E087E7500000A81090000010E097E7500000A81090000010E0A7E7500000A8109000001002A1E02285900000A2A00001B300800D50000001500001100170A7E7800000A0B0F04287900000A130611062D0A000F04285600000A0A000F05286600000A130611062D0A000F05282800000A0B000516737A00000A810A000001026F5A00000A130611063A8200000000160C7E2200000A0D151304728B030070732600000A13050011056F2B00000A001105283000000600020304120412021203110507282900000600051104737A00000A810A0000010F01282800000A08090F02284300000A110506283200000600110528310000060000DE14110514FE01130611062D0811056F3F00000A00DC00002A0000000110000002006A0054BE0014000000001B3008008600000016000011000516737A00000A810A000001026F5A00000A130411042D6C00160A7E2200000A0B150C728B030070732600000A0D00096F2B00000A0009283000000600020304120212001201097E7800000A282900000600060928240000060C0928310000060000DE120914FE01130411042D07096F3F00000A00DC000508737A00000A810A000001002A00000110000002002F00366500120000000013300300440000001700001100178D130000010A737B00000A0B0772721900706F7C00000A00071E6F7D00000A0007028C3E0000016F4D00000A00061607A27280190070060328420000060C2B00082A133003006C0000001800001100188D130000010A737B00000A0B0772B21900706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272E4190070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772141A00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272241A0070060428420000060D2B00092A13300300450000001700001100178D130000010A737B00000A0B0772C81900706F7C00000A00071F0E6F7D00000A0007028C110000016F4D00000A00061607A272501A0070060328420000060C2B00082A000000133003006C0000001800001100188D130000010A737B00000A0B0772881A00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272A61A0070060428420000060D2B00092A1B300400790700001900001100737E00000A0A06026F5700000A6F7F00000A000E0416540E0572F509007051051554000E0628440000066F8000000A130F381F070000120F288100000A0C00054A2D1D087272190070283C00000A2C100872E01A0070283C00000A16FE012B011700131011102D0538E8060000060828450000060D092C0E096F8200000A16FE0116FE012B011600131011102D0538C20600000813111111399C060000FE137E290000043A160100001F15738300000A2572881A007016288400000A2572F41A007017288400000A2572B219007018288400000A2572141A007019288400000A2572121B00701A288400000A2572341B00701B288400000A25724A1B00701C288400000A2572681B00701D288400000A2572861B00701E288400000A2572961B00701F09288400000A2572BA1B00701F0A288400000A2572E81B00701F0B288400000A25720C1C00701F0C288400000A2572381C00701F0D288400000A2572541C00701F0E288400000A2572681C00701F0F288400000A25727A1C00701F10288400000A25728E1C00701F11288400000A2572A41C00701F12288400000A2572721900701F13288400000A2572E01A00701F14288400000AFE138029000004FE137E2900000411111212288500000A3965050000111245150000000500000027000000490000006600000083000000A0000000BD000000DF000000FC0000001B0100003D0100005F01000081010000A3010000C20100002B02000094020000960300007D040000E3040000F904000038050500000972881A007028530000060B05070F01282800000A0E0628280000065438E30400000972C01C007028530000060B05070F01282800000A0E06282F0000065438C10400000928520000060B05070F01282800000A0E0628250000065438A40400000928520000060B05070F01282800000A0E0628260000065438870400000928520000060B05070F01282800000A0E06284100000654386A0400000928520000060B05070F01282800000A0E06284000000654384D0400000972C01C007028530000060B05070F01282800000A0E06283F00000654382B0400000928520000060B05070F01282800000A0E06283E00000654380E04000009284800000613040511040F01282800000A0E0628380000065438EF0300000972961B007028530000060B05070F01282800000A0E06283C0000065438CD0300000972BA1B007028530000060B05070F01282800000A0E06282B0000065438AB0300000972E81B007028530000060B05070F01282800000A0E06283A00000654388903000009720C1C007028530000060B05070F01282800000A0E06283B00000654386703000009284900000613050511050F01282800000A0E06283900000654384803000009284A000006130609166F8600000A6F8700000A72F01C00706F8800000A2C2A09166F8600000A6F8700000A72F01C00706F8800000A6F8900000A7E2200000A283C00000A16FE012B011700131011102D120511060F01282800000A0E0628370000065438DF02000009284B000006130709166F8600000A6F8700000A72F01C00706F8800000A2C2A09166F8600000A6F8700000A72F01C00706F8800000A6F8900000A7E2200000A283C00000A16FE012B011700131011102D120511070F01282800000A0E06283300000654387602000009284C000006130816738A00000A130909166F8600000A6F8700000A72FE1C00706F8800000A2C2409166F8600000A6F8700000A72FE1C00706F8800000A6F8900000A1209288B00000A2B011700131011102D0A0016738A00000A1309007E8C00000A288D00000A130A09166F8600000A6F8700000A72181D00706F8800000A2C2409166F8600000A6F8700000A72181D00706F8800000A6F8900000A120A288B00000A2B011700131011102D0E007E8C00000A288D00000A130A00110916738A00000A288E00000A2C16110A7E8C00000A288D00000A288E00000A16FE012B011700131011102D14000511080F01282800000A0E0628340000065400387401000009284D000006130B16130C09166F8600000A6F8700000A72321D00706F8800000A2C2409166F8600000A6F8700000A72321D00706F8800000A6F8900000A120C288F00000A2B011700131011102D050016130C007E9000000A289100000A130D09166F8600000A6F8700000A724C1D00706F8800000A2C2409166F8600000A6F8700000A724C1D00706F8800000A6F8900000A120D288F00000A2B011700131011102D0E007E9000000A289100000A130D00110C2C10110D7E9000000A289100000AFE012B011700131011102D140005110B0F01282800000A0E0628350000065400388D00000009284E000006130E09166F8600000A6F8700000A72F01C00706F8800000A2C2A09166F8600000A6F8700000A72F01C00706F8800000A6F8900000A7E2200000A283C00000A16FE012B011700131011102D1205110E0F01282800000A0E062836000006542B2709166F8600000A6F9200000A0E04288F00000A262B110E0509166F8600000A6F9200000A512B00050E070E06282A00000654050F01282800000A0E0628270000065400120F289300000A131011103AD1F8FFFFDE0F120FFE160200001B6F3F00000A00DC002A000000411C0000020000003200000036070000680700000F0000000000000013300300450000001700001100178D130000010A737B00000A0B0772661D00706F7C00000A00071F0E6F7D00000A0007028C110000016F4D00000A00061607A272821D0070060328420000060C2B00082A000000133003006C0000001800001100188D130000010A737B00000A0B0772B61D00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272EA1D0070060428420000060D2B00092A13300300720000001800001100198D130000010A737B00000A0B0772B21900706F7C00000A00071F196F7D00000A00070F007B030000046F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272E4190070060428420000060D2B00092A00001B300400880100001A000011001200FE150500000200026F9400000A1306383901000011066F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613071207289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D02000004739600000A0C076F8700000A72481E00706F8800000A2C1C076F8700000A72481E00706F8800000A6F8900000A282300000A2B011700130811083A860000000000076F8700000A72481E00706F8800000A6F8900000A178D4E00000113091109161F209D11096F6500000A130A16130B2B35110A110B9A0D00096F3200000A1F24FE0116FE01130811082D1400120409289700000A000811046F9800000A000000110B1758130B110B110A8E69FE04130811082DBD12000828510000067D03000004002B0E0012007E2200000A7D03000004000011066F3E00000A130811083AB7FEFFFFDE1D11067541000001130C110C14FE01130811082D08110C6F3F00000A00DC000613052B0011052A411C0000020000001200000050010000620100001D00000000000000133003005B0000001B000011001B8D360000010C081672601E0070A2081702A20818729E1E0070A208190F01283300000AA2081A727A0C0070A208283B00000A0A739900000A0B07046F9A00000A0007066F2C00000A0007176F9B00000A00076F9C00000A262A00133003006C0000001800001100188D130000010A737B00000A0B0772F41A00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272A41E0070060428420000060D2B00092A133002002D0000001C0000110072DC1E00700A739900000A0B07026F9A00000A0007066F2C00000A0007176F9B00000A00076F9C00000A262A000000133002002D0000001C0000110072B32000700A739900000A0B07026F9A00000A0007066F2C00000A0007176F9B00000A00076F9C00000A262A000000133002004B0100001D00001100737B00000A0A0672C81900706F7C00000A00061F0E6F7D00000A0006028C110000016F4D00000A00737B00000A0B0772721900706F7C00000A00071E6F7D00000A0007038C3E0000016F4D00000A00737B00000A0C0872E01A00706F7C00000A00081F166F7D00000A00081F196F9D00000A0008046F4D00000A00737B00000A0D0972FB2000706F7C00000A00091E6F7D00000A0009058C3E0000016F4D00000A00737B00000A1304110472112100706F7C00000A001104186F7D00000A0011040E058C5A0000016F4D00000A00739900000A130511050E046F9A00000A001105722F2100706F2C00000A0011051A6F9B00000A0011056F4B00000A066F9E00000A2611056F4B00000A076F9E00000A2611056F4B00000A086F9E00000A2611056F4B00000A096F9E00000A2611056F4B00000A11046F9E00000A26289F00000A11056FA000000A002A0013300300DA0000001E000011001A8D130000010A737B00000A0B0772281E00706F7C00000A0007186F7D00000A00070F007B0400000428A100000A8C5A0000016F4D00000A00061607A2737B00000A0C08726F2100706F7C00000A00081A6F7D00000A00080F007B070000046F4D00000A00061708A2737B00000A0D0972832100706F7C00000A00091A6F7D00000A00090F007B080000046F4D00000A00061809A2737B00000A1304110472C81900706F7C00000A0011041F0E6F7D00000A001104038C110000016F4D00000A0006191104A272932100700604284200000613052B0011052A000013300300170100001F000011001B8D130000010A737B00000A0B0772FE1C00706F7C00000A00071F096F7D00000A00070F007B090000048C1B0000016F4D00000A00061607A2737B00000A0C0872181D00706F7C00000A00081F096F7D00000A00080F007B0A0000048C1B0000016F4D00000A00061708A2737B00000A0D09726F2100706F7C00000A00091A6F7D00000A00090F007B0B0000048C170000016F4D00000A00061809A2737B00000A1304110472832100706F7C00000A0011041A6F7D00000A0011040F007B0C0000048C170000016F4D00000A0006191104A2737B00000A1305110572C81900706F7C00000A0011051F0E6F7D00000A001105038C110000016F4D00000A00061A1105A272C92100700604284200000613062B0011062A0013300300150100001F000011001B8D130000010A737B00000A0B0772321D00706F7C00000A00071E6F7D00000A00070F007B0D0000048C0A0000016F4D00000A00061607A2737B00000A0C08724C1D00706F7C00000A00081E6F7D00000A00080F007B0E0000048C0A0000016F4D00000A00061708A2737B00000A0D09726F2100706F7C00000A00091A6F7D00000A00090F007B0F0000048C170000016F4D00000A00061809A2737B00000A1304110472832100706F7C00000A0011041A6F7D00000A0011040F007B100000048C170000016F4D00000A0006191104A2737B00000A1305110572C81900706F7C00000A0011051F0E6F7D00000A001105038C110000016F4D00000A00061A1105A272F92100700604284200000613062B0011062A000000133003009F0000002000001100198D130000010A737B00000A0B07726F2100706F7C00000A00071A6F7D00000A00070F007B070000046F4D00000A00061607A2737B00000A0C0872832100706F7C00000A00081A6F7D00000A00080F007B080000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A2722B2200700604284200000613042B0011042A00133003009F0000002000001100198D130000010A737B00000A0B07726F2100706F7C00000A00071A6F7D00000A00070F007B070000046F4D00000A00061607A2737B00000A0C0872832100706F7C00000A00081A6F7D00000A00080F007B080000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A272632200700604284200000613042B0011042A0013300300AA0000002000001100198D130000010A737B00000A0B0772281E00706F7C00000A0007186F7D00000A00070F007B0200000428A100000A8C5A0000016F4D00000A00061607A2737B00000A0C0872932200706F7C00000A00081F196F7D00000A00080F007B030000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A272A92200700604284200000613042B0011042A000013300300AA0000002000001100198D130000010A737B00000A0B0772281E00706F7C00000A0007186F7D00000A00070F007B0200000428A100000A8C5A0000016F4D00000A00061607A2737B00000A0C0872D52200706F7C00000A00081F196F7D00000A00080F007B030000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A272E52200700604284200000613042B0011042A0000133003006C0000001800001100188D130000010A737B00000A0B07721F2300706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27235230070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772752300706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27293230070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772DB2300706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27205240070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772932200706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272A9220070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772392400706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A2725D240070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772972400706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272B9240070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772F12400706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A2720D250070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B07723F2500706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27267250070060428420000060D2B00092A13300200790000002100001100732700000A0A739900000A0B07046F9A00000A0007026F2C00000A00071A6F9B00000A0007166FA200000A00000313041613052B25110411059A0C000814FE01130611062D0D076F4B00000A086F9E00000A2600110517581305110511048E69FE04130611062DCD076F6400000AA53E0000010D2B00092A00000013300200920000002200001100732700000A0A728B0300700B07732600000A0C739900000A0D09086F9A00000A0009026F2C00000A00091A6F9B00000A0009166FA200000A00000313071613082B28110711089A130400110414FE01130911092D0E096F4B00000A11046F9E00000A2600110817581308110811078E69FE04130911092DCA09732D00000A13051105066F2E00000A260613062B0011062A00001B300300B1000000230000110073A300000A0A732700000A0B72A52500700C0802732A00000A0D09732D00000A13041104076F2E00000A2600076F3000000A6F3600000A13072B2811076F3700000A7440000001130500061105723A2600706F3800000A6F1C00000A6FA400000A000011076F3E00000A130811082DCBDE1D110775410000011309110914FE01130811082D0811096F3F00000A00DC000672721900706FA400000A000672E01A00706FA400000A000613062B0011062A0000000110000002003A003973001D00000000133004006803000024000011000313161116394B030000FE137E2A0000043A160100001F15738300000A2572881A007016288400000A2572F41A007017288400000A2572B219007018288400000A2572141A007019288400000A2572121B00701A288400000A2572341B00701B288400000A25724A1B00701C288400000A2572681B00701D288400000A2572861B00701E288400000A2572961B00701F09288400000A2572BA1B00701F0A288400000A2572E81B00701F0B288400000A25720C1C00701F0C288400000A2572381C00701F0D288400000A2572541C00701F0E288400000A2572681C00701F0F288400000A25727A1C00701F10288400000A25728E1C00701F11288400000A2572A41C00701F12288400000A2572721900701F13288400000A2572E01A00701F14288400000AFE13802A000004FE137E2A00000411161217288500000A39140200001117451500000005000000190000002D00000041000000550000006B0000008100000097000000AD000000C3000000D9000000EF000000050100001B01000031010000470100005A0100006D0100008001000093010000A601000038B40100000272442600706FA500000A0A06131538AF0100000272A62600706FA500000A0B071315389B0100000272E82600706FA500000A0C08131538870100000272242700706FA500000A0D091315387301000002725A2700706FA500000A130411041315385D0100000272A22700706FA500000A13051105131538470100000272DE2700706FA500000A13061106131538310100000272202800706FA500000A130711071315381B0100000272642800706FA500000A13081108131538050100000272B42800706FA500000A13091109131538EF00000002721A2900706FA500000A130A110A131538D900000002728C2900706FA500000A130B110B131538C30000000272E62900706FA500000A130C110C131538AD0000000272482A00706FA500000A130D110D131538970000000272A42A00706FA500000A130E110E131538810000000272D42A00706FA500000A130F110F13152B6E0272022B00706FA500000A1310111013152B5B0272322B00706FA500000A1311111113152B480272642B00706FA500000A1312111213152B3502729C2B00706FA500000A1313111313152B220272C62B00706FA500000A1314111413152B0F0272F62B00706FA500000A13152B0011152A1B300200620000002500001100739600000A0A00026F9400000A0D2B20096F3700000A74180000010B0006076F9200000A739700000A6F9800000A0000096F3E00000A130411042DD4DE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A00000110000002000F00303F001C000000001B30020062000000260000110073A600000A0A00026F9400000A0D2B20096F3700000A74180000010B0006076F9200000A28A700000A6FA800000A0000096F3E00000A130411042DD4DE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A00000110000002000F00303F001C000000001B300400880100001A000011001200FE150500000200026F9400000A1306383901000011066F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613071207289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D02000004739600000A0C076F8700000A72481E00706F8800000A2C1C076F8700000A72481E00706F8800000A6F8900000A282300000A2B011700130811083A860000000000076F8700000A72481E00706F8800000A6F8900000A178D4E00000113091109161F209D11096F6500000A130A16130B2B35110A110B9A0D00096F3200000A1F24FE0116FE01130811082D1400120409289700000A000811046F9800000A000000110B1758130B110B110A8E69FE04130811082DBD12000828510000067D03000004002B0E0012007E2200000A7D03000004000011066F3E00000A130811083AB7FEFFFFDE1D11067541000001130C110C14FE01130811082D08110C6F3F00000A00DC000613052B0011052A411C0000020000001200000050010000620100001D000000000000001B300400880100001A000011001200FE150500000200026F9400000A1306383901000011066F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613071207289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D02000004739600000A0C076F8700000A72481E00706F8800000A2C1C076F8700000A72481E00706F8800000A6F8900000A282300000A2B011700130811083A860000000000076F8700000A72481E00706F8800000A6F8900000A178D4E00000113091109161F209D11096F6500000A130A16130B2B35110A110B9A0D00096F3200000A1F24FE0116FE01130811082D1400120409289700000A000811046F9800000A000000110B1758130B110B110A8E69FE04130811082DBD12000828510000067D03000004002B0E0012007E2200000A7D03000004000011066F3E00000A130811083AB7FEFFFFDE1D11067541000001130C110C14FE01130811082D08110C6F3F00000A00DC000613052B0011052A411C0000020000001200000050010000620100001D000000000000001B3003005C01000027000011001200FE150600000200026F9400000A0D3812010000096F3700000A74180000010B00120072541C00707D050000041200076F8700000A72122C00706F8800000A2C17076F8700000A72122C00706F8800000A6F8900000A2B0572242C0070007D060000041200076F8700000A72F01C00706F8800000A2C33076F8700000A72F01C00706F8800000A6F8900000A282300000A2D17076F8700000A72F01C00706F8800000A6F8900000A2B147EA900000A13041204FE16170000016F1C00000A007D070000041200076F8700000A72342C00706F8800000A2C33076F8700000A72342C00706F8800000A6F8900000A282300000A2D17076F8700000A72342C00706F8800000A6F8900000A2B147EAA00000A13041204FE16170000016F1C00000A007D0800000400096F3E00000A130511053ADFFEFFFFDE1C0975410000011306110614FE01130511052D0811066F3F00000A00DC00060C2B00082A411C0000020000001100000028010000390100001C000000000000001B300300B301000028000011001200FE150600000200026F9400000A0D3869010000096F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613041204289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D04000004120072681C00707D050000041200076F8700000A72122C00706F8800000A2C17076F8700000A72122C00706F8800000A6F8900000A2B0572242C0070007D060000041200076F8700000A72F01C00706F8800000A2C33076F8700000A72F01C00706F8800000A6F8900000A282300000A2D17076F8700000A72F01C00706F8800000A6F8900000A2B147EA900000A13051205FE16170000016F1C00000A007D070000041200076F8700000A72342C00706F8800000A2C33076F8700000A72342C00706F8800000A6F8900000A282300000A2D17076F8700000A72342C00706F8800000A6F8900000A2B147EAA00000A13051205FE16170000016F1C00000A007D0800000400096F3E00000A130611063A88FEFFFFDE1C0975410000011307110714FE01130611062D0811076F3F00000A00DC00060C2B00082A00411C000002000000110000007F010000900100001C000000000000001B300300F101000029000011001200FE150700000200026F9400000A0D38A7010000096F3700000A74180000010B00076F8700000A72FE1C00706F8800000A2C1C076F8700000A72FE1C00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72FE1C00706F8800000A6F8900000A28AB00000A7D090000042B11120072422C007028AB00000A7D09000004076F8700000A72181D00706F8800000A2C1C076F8700000A72181D00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72181D00706F8800000A6F8900000A28AB00000A7D0A0000042B0C12007E8C00000A7D0A000004076F8700000A726F2100706F8800000A2C1C076F8700000A726F2100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A726F2100706F8800000A6F8900000A28AC00000A7D0B0000042B0C12007EA900000A7D0B000004076F8700000A72832100706F8800000A2C1C076F8700000A72832100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72832100706F8800000A6F8900000A28AC00000A7D0C0000042B0C12007EAA00000A7D0C00000400096F3E00000A130411043A4AFEFFFFDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A000000411C00000200000011000000BD010000CE0100001C000000000000001B300300ED0100002A000011001200FE150800000200026F9400000A0D38A3010000096F3700000A74180000010B00076F8700000A72321D00706F8800000A2C1C076F8700000A72321D00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72321D00706F8800000A6F8900000A28AD00000A7D0D0000042B0D12001628AE00000A7D0D000004076F8700000A724C1D00706F8800000A2C1C076F8700000A724C1D00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A724C1D00706F8800000A6F8900000A28AD00000A7D0E0000042B0C12007E9000000A7D0E000004076F8700000A726F2100706F8800000A2C1C076F8700000A726F2100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A726F2100706F8800000A6F8900000A28AC00000A7D0F0000042B0C12007EA900000A7D0F000004076F8700000A72832100706F8800000A2C1C076F8700000A72832100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72832100706F8800000A6F8900000A28AC00000A7D100000042B0C12007EAA00000A7D1000000400096F3E00000A130411043A4EFEFFFFDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A000000411C00000200000011000000B9010000CA0100001C000000000000001B3003005C01000027000011001200FE150600000200026F9400000A0D3812010000096F3700000A74180000010B00120072A41C00707D050000041200076F8700000A72122C00706F8800000A2C17076F8700000A72122C00706F8800000A6F8900000A2B0572242C0070007D060000041200076F8700000A72F01C00706F8800000A2C33076F8700000A72F01C00706F8800000A6F8900000A282300000A2D17076F8700000A72F01C00706F8800000A6F8900000A2B147EA900000A13041204FE16170000016F1C00000A007D070000041200076F8700000A72342C00706F8800000A2C33076F8700000A72342C00706F8800000A6F8900000A282300000A2D17076F8700000A72342C00706F8800000A6F8900000A2B147EAA00000A13041204FE16170000016F1C00000A007D0800000400096F3E00000A130511053ADFFEFFFFDE1C0975410000011306110614FE01130511052D0811066F3F00000A00DC00060C2B00082A411C0000020000001100000028010000390100001C000000000000001B300300C70000002B0000110073AF00000A0A00026F9400000A13042B7F11046F3700000A74180000010B001202FE1506000002120272541C00707D050000041202076F8700000A72122C00706F8800000A6F8900000A7D060000041202076F8700000A72F01C00706F8800000A6F8900000A7D070000041202076F8700000A72342C00706F8800000A6F8900000A7D0800000406086FB000000A000011046F3E00000A130511053A71FFFFFFDE1D110475410000011306110614FE01130511052D0811066F3F00000A00DC00060D2B00092A00011000000200100093A3001D000000001B3004008A0000002C00001100731500000A0A06724C2C00706F1B00000A2600026F3000000A6F3600000A0D2B26096F3700000A74400000010B0006720100007007166F7200000A6F1C00000A6F1800000A2600096F3E00000A130411042DCEDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC000672822C00706F1B00000A26066F1C00000A0C2B00082A000001100000020020003656001C000000001B3003007D0000002D00001100731500000A0A06724C2C00706F1B00000A260214FE010D092D4B00026FB100000A13042B23120428B200000A0B000672010000701201FE16110000016F1C00000A6F1800000A2600120428B300000A0D092DD2DE0F1204FE160700001B6F3F00000A00DC000672822C00706F1B00000A26066F1C00000A0C2B00082A00000001100000020024003256000F0000000013300200110000000A000011000272BA2C007028530000060A2B00062A0000001B3003007C0000002E00001100731500000A0A0672EC2C0070036F1800000A2600026F9400000A0D2B1B096F3700000A74180000010B0006076FB400000A6F1B00000A2600096F3E00000A130411042DD9DE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC000672F82C0070036F1800000A26066F1C00000A0C2B00082A0110000002001C002B47001C000000001B3003006D0000002F00001100739600000A0A00026F3000000A6F3600000A0D2B26096F3700000A74400000010B000607166F7200000A6F1C00000A739700000A6F9800000A0000096F3E00000A130411042DCEDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A0000000110000002001400364A001C000000001B300200690000003000001100737E00000A0A0672062D00706F7F00000A0000026F9400000A0D2B16096F3700000A74180000010B0006076FB500000A2600096F3E00000A130411042DDEDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00066FB400000A0C2B00082A0000000110000002001B002641001C00000000133005003D010000310000110072462D00701F11734400000A0A140B060F01281700000A6F4500000A16FE010D092D13060F01281700000A6F4600000A6F4700000A0B731500000A0C0872542D00701813041204283300000A7E7800000A8C110000010F00282800000A13051205FE16110000016F1C00000A6FB600000A260872612E0070168D030000016FB700000A260872812E00700F00282800000A13051205FE16110000016F1C00000A6F1800000A2607282300000A0D092D0D08720A2F0070076F1800000A260872702F00701A8D0300000113061106160F01281700000AA21106170F02285600000A28B800000A8C3E000001A21106180F03285600000A28B800000A8C3E000001A21106190F04285600000A28B800000A8C3E000001A211066FB700000A2608721D300070168D030000016FB700000A26086F1C00000A2875000006002A00000013300500A20000003200001100731500000A0A067243300070168D030000016FB700000A260672673000701B8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2071A0F06285600000A28B800000A8C3E000001A2076FB700000A2606724A310070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500CB0000003300001100731500000A0A0672B33300701F200B1201283300000A7E7800000A8C110000010F00282800000A0C1202FE16110000016F1C00000A6FB600000A260672E4340070168D030000016FB700000A260672643500701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A260672D1360070168D030000016FB700000A26066F1C00000A2875000006002A00133005001A0100003200001100731500000A0A067243300070168D030000016FB700000A260672FD3600701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A2606726E380070168D030000016FB700000A26066F1C00000A036F5700000A287600000600731500000A0A0672CF3A0070168D030000016FB700000A260672ED3A00701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A260672A23B0070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500CB0000003300001100731500000A0A0672B33300701F100B1201283300000A7E7800000A8C110000010F00282800000A0C1202FE16110000016F1C00000A6FB600000A260672E4340070168D030000016FB700000A260672273E00701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A260672D1360070168D030000016FB700000A26066F1C00000A2875000006002A00133005001A0100003200001100731500000A0A067243300070168D030000016FB700000A260672983F00701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A2606720D410070168D030000016FB700000A26066F1C00000A036F5700000A287600000600731500000A0A0672CF3A0070168D030000016FB700000A260672704300701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A26067227440070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500C00000003300001100731500000A0A0672AE4600701A0B1201283300000A0F00282800000A0C1202FE16110000016F1C00000A6FB900000A26067285470070168D030000016FB700000A260672B04800701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A26067269490070168D030000016FB700000A26066F1C00000A2875000006002A133005008E0000003200001100731500000A0A067295490070168D030000016FB700000A260672C34900701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A260672684A0070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500D20000003300001100731500000A0A06721D4D00701E0B1201283300000A0F00282800000A0C1202FE16110000016F1C00000A6FB900000A260672F64D0070168D030000016FB700000A260672B54E0070168D030000016FB700000A260672B44F00701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A26067293500070168D030000016FB700000A26066F1C00000A2875000006002A0000133005008E0000003200001100731500000A0A0672C1500070168D030000016FB700000A260672E95000701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A260672BA510070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A0000133005003D010000310000110072375400701F11734400000A0A140B060F01281700000A6F4500000A16FE010D092D13060F01281700000A6F4600000A6F4700000A0B731500000A0C0872455400701713041204283300000A7E7800000A8C110000010F00282800000A13051205FE16110000016F1C00000A6FB600000A260872612E0070168D030000016FB700000A260872525500700F00282800000A13051205FE16110000016F1C00000A6F1800000A2607282300000A0D092D0D0872D9550070076F1800000A2608720B5600701A8D0300000113061106160F01281700000AA21106170F02285600000A28B800000A8C3E000001A21106180F03285600000A28B800000A8C3E000001A21106190F04285600000A28B800000A8C3E000001A211066FB700000A2608721D300070168D030000016FB700000A26086F1C00000A2875000006002A00000013300500A20000003200001100731500000A0A067243300070168D030000016FB700000A260672A65600701B8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2071A0F06285600000A28B800000A8C3E000001A2076FB700000A26067267570070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500C500000034000011000F01284300000A0A28740000060B289F00000A076FBA00000A0006175F17FE0116FE010C082D0E02050E040E050E0628600000060006185F18FE0116FE010C082D0E02050E040E050E06285600000600061A5F1AFE0116FE010C082D0C02040E040E05285C00000600061E5F1EFE0116FE010C082D0C02040E040E05285E00000600061F105F1F10FE0116FE010C082D0C02040E040E05285A00000600061F205F1F20FE0116FE010C082D0C02040E040E05285800000600289F00000A6FBB00000A002A00000013300700ED0000003500001100737E00000A0A06036F5700000A6F7F00000A00140B0672C25900706FBC00000A0B0714FE010C082D110203050E050E060E070E082861000006000672FA5900706FBC00000A0B0714FE010C082D110203050E050E060E070E082857000006000672405A00706FBC00000A0B0714FE010C082D0F0203040E040E060E07285D000006000672745A00706FBC00000A0B0714FE010C082D0F0203040E040E060E07285F000006000672B25A00706FBC00000A0B0714FE010C082D0F0203040E040E060E07285B000006000672F25A00706FBC00000A0B0714FE010C082D0F0203040E040E060E072859000006002A000000133005003701000036000011000F01281700000A0F03285600000A287B0000060A731500000A0B077243300070168D030000016FB700000A260772305B0070066F1C00000A066FBD00000A8C3E0000010F02281700000A6FB600000A260772B55B00700F00282800000A0C1202FE16110000016F1C00000A6F1800000A260772445C0070066F1C00000A066FBD00000A8C3E0000016FB900000A26076F1C00000A287700000600731500000A0B077243300070168D030000016FB700000A260772985C00700F01281700000A0F02281700000A0F03285600000A28B800000A8C3E0000016FB600000A260772615D00700F00282800000A0C1202FE16110000016F1C00000A0F01281700000A6FB900000A260772565E00700F01281700000A0F03285600000A28B800000A8C3E0000016FB900000A26076F1C00000A2877000006002A00133007001E00000037000011007E7800000A0A027E7800000A037E7800000A040512002869000006002A4A00020304050E040E050E062869000006002A0000001B300500DD0400003800001100026F3200000A16310E036F3200000A16FE0216FE012B011700131B111B3AB90400000072C00F0070732600000A0A737E00000A0B737E00000A0C000372090A00706FBE00000A2C100372090A00706FBF00000A16FE012B011700131B111B2D18070317036F3200000A18596F5500000A6F7F00000A002B0807036F7F00000A000272090A00706FBE00000A2C100272090A00706FBF00000A16FE012B011700131B111B2D18080217026F3200000A18596F5500000A6F7F00000A002B0808026F7F00000A00086FC000000A72E75E00706FA500000A0D096F8200000A73C100000A130400096F9400000A131C2B29111C6F3700000A7418000001130500110472235F007011056F9200000A6FC200000A6F1800000A2600111C6F3E00000A131B111B2DCADE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0011046F1C00000A130611066F3200000A17FE0216FE01131B111B2D1311061611066F3200000A17596F5500000A1306732700000A130772315F00701106286300000A1308110806732A00000A13091109166FA200000A00066F2B00000A001109732D00000A130A110A11076F2E00000A26066F5300000A007E2200000A130B14130C7E2200000A130D7E2200000A130E7E2200000A130F0011076F3000000A6F3600000A131C38A9010000111C6F3700000A74400000011310007E2200000A130F111072006000706F3800000A6F1C00000A130D111072246000706F3800000A6F1C00000A130E737E00000A130C110C111072006000706F3800000A6F1C00000A6F7F00000A00076FC000000A72E75E00706FA500000A1311110C6FC000000A723E6000706FBC00000A131211126F8700000A72666000706F8800000A0F02FE16110000016F1C00000A6FC300000A0011126F8700000A727C6000706F8800000A28C400000A131E121E729660007028C500000A178D4E000001131F111F161F5A9D111F6FC600000A6FC300000A000011116F9400000A13202B6111206F3700000A741800000113130011136F9200000A6FC200000A1314110E11146F3A00000A6F3D00000A2D10110E11146FC200000A6F3D00000A2B011700131B111B2D1B00729A6000701114286300000A1315110F1115283400000A130F000011206F3E00000A131B111B2D92DE1D11207541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0011107200600070110C6FB400000A6FC700000A0011107224600070110E110F283400000A6FC700000A0000111C6F3E00000A131B111B3A47FEFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC00066F2B00000A00724562007013167E2200000A13171413180011076F3000000A6F3600000A131C2B7B111C6F3700000A74400000011319001116111972006000706F3800000A6F1C00000A72090A0070720D0A00706F3500000A111972246000706F3800000A6F1C00000A111972106300706F3800000A6F1C00000A282500000A1317111706732A00000A13181118166FA200000A00289F00000A11186FA000000A0000111C6F3E00000A131B111B3A75FFFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC00066F5300000A0000DE10131A007232630070111A73C800000A7A00DE2000062C0B066FC900000A16FE012B011700131B111B2D07066F5300000A0000DC00002A0000004194000002000000ED0000003A000000270100001D0000000000000002000000DE02000072000000500300001D0000000000000002000000EB010000C0010000AB0300001D0000000000000002000000F00300008F0000007F0400001D00000000000000000000003B0000006C040000A7040000100000005F000001020000003B0000007F040000BA04000020000000000000001B300500D80500003900001100026F3200000A16310E036F3200000A16FE0216FE012B011700131C111C3AB40500000072C00F0070732600000A0A737E00000A0B737E00000A0C000372090A00706FBE00000A2C100372090A00706FBF00000A16FE012B011700131C111C2D18070317036F3200000A18596F5500000A6F7F00000A002B0807036F7F00000A000272090A00706FBE00000A2C100272090A00706FBF00000A16FE012B011700131C111C2D18080217026F3200000A18596F5500000A6F7F00000A002B0808026F7F00000A00086FC000000A72E75E00706FA500000A0D096F8200000A73C100000A130400096F9400000A131D2B29111D6F3700000A7418000001130500110472235F007011056F9200000A6FC200000A6F1800000A2600111D6F3E00000A131C111C2DCADE1D111D7541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC0011046F1C00000A130611066F3200000A17FE0216FE01131C111C2D1311061611066F3200000A17596F5500000A1306732700000A130772586300701106286300000A1308110806732A00000A13091109166FA200000A00066F2B00000A001109732D00000A130A110A11076F2E00000A26066F5300000A007E2200000A130B14130C14130D0011076F3000000A6F3600000A131D38B6020000111D6F3700000A7440000001130E00737E00000A130C110C110E72006000706F3800000A6F1C00000A6F7F00000A00737E00000A130D110D7223640070110E72246000706F3800000A6F1C00000A723D640070282900000A6F7F00000A00076FC000000A72E75E00706FA500000A130F110C6FC000000A723E6000706FBC00000A131011106F8700000A72666000706F8800000A0F02FE16110000016F1C00000A6FC300000A0011106F8700000A727C6000706F8800000A28C400000A131F121F729660007028C500000A178D4E00000113201120161F5A9D11206FC600000A6FC300000A0000110F6F9400000A1321383B01000011216F3700000A741800000113110011116F9200000A6FC200000A1312110D6FC000000A72596400701112286300000A6FA500000A1313110D6FC000000A725964007011126F3A00000A286300000A6FA500000A131411136F8200000A2D0F11146F8200000A16FE0216FE012B011700131C111C2D041114131311136F8200000A16310F11146F8200000A16FE0116FE012B011700131C111C2D0411131314161315388200000000111517FE0116FE01131C111C2D04111413130011136F9400000A13222B2F11226F3700000A741800000113160011166FCA00000A14FE01131C111C2D0F11166FCA00000A11166FCB00000A260011226F3E00000A131C111C2DC4DE1D11227541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC0000111517581315111517FE0216FE01131C111C3A6DFFFFFF0011216F3E00000A131C111C3AB5FEFFFFDE1D11217541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC00110E7200600070110C6FB400000A6FC700000A00110E7224600070110D6FCC00000A72236400707E2200000A6F3500000A723D6400707E2200000A6F3500000A72876400707E2200000A6F3500000A6FC700000A0000111D6F3E00000A131C111C3A3AFDFFFFDE1D111D7541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC00066F2B00000A0072A364007013177E2200000A13181413190011076F3000000A6F3600000A131D2B7B111D6F3700000A7440000001131A001117111A72006000706F3800000A6F1C00000A72090A0070720D0A00706F3500000A111A72246000706F3800000A6F1C00000A111A72106300706F3800000A6F1C00000A282500000A1318111806732A00000A13191119166FA200000A00289F00000A11196FA000000A0000111D6F3E00000A131C111C3A75FFFFFFDE1D111D7541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC00066F5300000A0000DE10131B007232630070111B73C800000A7A00DE2000062C0B066FC900000A16FE012B011700131C111C2D07066F5300000A0000DC00002A41AC000002000000ED0000003A000000270100001D00000000000000020000009703000040000000D70300001D0000000000000002000000CE02000052010000200400001D0000000000000002000000D9010000CD020000A60400001D0000000000000002000000EB0400008F0000007A0500001D00000000000000000000003B00000067050000A2050000100000005F000001020000003B0000007A050000B505000020000000000000001B300700B30300003A0000110072C00F0070732600000A0A140B0E067E7800000A8111000001737E00000A0C737E00000A0D000008046F5700000A6F7F00000A00726E6500700F00FE16110000016F1C00000A286300000A1304110406732A00000A13051105166FA200000A00066F2B00000A0011056F6400000A6F1C00000A0B066F5300000A0009076F7F00000A007E2200000A1306141307086FC000000A6F8700000A72D26500706F8800000A13087E7800000A1309110814FE01130D110D2D21000011086F8900000A739700000A130900DE0C26007E7800000A130900DE0000000F017E7800000A28CD00000A16FE01130D110D3AEB0000000012097E7800000A28CD00000A16FE01130D110D2D0C0072D865007073CE00000A7A72406600701209FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A1307110714FE0116FE01130D110D2D2F0072406600701209FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130700110714FE01130D110D2D4E000E040E05086FC000000A11070973CF00000A17286E00000600086FC000000A6FD000000A16FE01130D110D2D1600086FC000000A11070E040E050914286C00000600000E06110981110000010000380C0100000012097E7800000A28CD00000A16FE01130D110D3AF20000000072406600700F01FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A130A110A14FE0116FE01130D110D2D2F0072406600700F01FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130A00110A14FE0116FE01130D110D2D0C00727266007073CE00000A7A72B6660070050E05086FC000000A0917286D0000061307110A11076FB500000A26086FC000000A6FD000000A16FE01130D110D2D1500086FC000000A1107050E050914286C00000600000E0611076F8700000A72D26500706F8800000A6F8900000A739700000A81110000010000110714FE01130D110D2D6700091107286B0000060072CE660070096FB400000A72090A0070720D0A00706F3500000A0F00FE16110000016F1C00000A282F00000A1304110406732A00000A13051105166FA200000A00066F2B00000A00289F00000A11056FA000000A00066F5300000A000000DE10130B007232630070110B73C800000A7A0000DE10130C007232630070110C73C800000A7A00DE2E00062C0B066FC900000A16FE012B011700130D110D2D07066F5300000A00086FD100000A00096FD100000A0000DC002A004164000000000000B800000012000000CA0000000C000000030000010000000027000000350300005C030000100000005F00000100000000260000004A03000070030000100000005F00000102000000260000005D030000830300002E000000000000001B3005006D0600003B00001100036F3200000A16310E046F3200000A16FE0216FE012B011700131D111D3A490600000072C00F0070732600000A0A737E00000A0B737E00000A0C000472090A00706FBE00000A2C100472090A00706FBF00000A16FE012B011700131D111D2D18070417046F3200000A18596F5500000A6F7F00000A002B0807046F7F00000A000372090A00706FBE00000A2C100372090A00706FBF00000A16FE012B011700131D111D2D18080317036F3200000A18596F5500000A6F7F00000A002B0808036F7F00000A00086FC000000A72E75E00706FA500000A0D096F8200000A73C100000A130400096F9400000A131E2B29111E6F3700000A7418000001130500110472235F007011056F9200000A6FC200000A6F1800000A2600111E6F3E00000A131D111D2DCADE1D111E7541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC0011046F1C00000A130611066F3200000A17FE0216FE01131D111D2D1311061611066F3200000A17596F5500000A1306732700000A130772406700700F00FE16110000016F1C00000A1106282F00000A1308110806732A00000A13091109166FA200000A00066F2B00000A001109732D00000A130A110A11076F2E00000A26066F5300000A007E2200000A130B14130C0011076F3000000A6F3600000A131E3855030000111E6F3700000A7440000001130D00737E00000A130C110C110D72006000706F3800000A6F1C00000A6F7F00000A00076FC000000A72E75E00706FA500000A130E110C6FC000000A723E6000706FBC00000A130F110F6F8700000A72666000706F8800000A0F03FE16110000016F1C00000A6FC300000A00110F6F8700000A727C6000706F8800000A28C400000A13201220729660007028C500000A178D4E00000113211121161F5A9D11216FC600000A6FC300000A0000110E6F9400000A1322384A02000011226F3700000A741800000113100011106F9200000A6FC200000A13110E041728D200000A28D300000A28D400000A16FE01131D111D3A4201000000110C177213680070146FD500000A1312110C72D26500706FD600000A1313111311116FC300000A0011126F8700000A11136FD700000A26110C72276800706FD600000A131311137E7800000A13231223FE16110000016F1C00000A6FC200000A6FC300000A0011126F8700000A11136FD700000A26110C723B6800706FD600000A131311137E7800000A13231223FE16110000016F1C00000A6FC200000A6FC300000A0011126F8700000A11136FD700000A26110C72576800706FD600000A1313111372230B00706FC300000A0011126F8700000A11136FD700000A26110C72736800706FD600000A1313111372896800706FC300000A0011126F8700000A11136FD700000A26110C72956800706FD600000A1313111372896800706FC300000A0011126F8700000A11136FD700000A26110F11126FB500000A260038CC00000000110F72596400701111286300000A6FA500000A1314110F725964007011116F3A00000A286300000A6FA500000A1315161316388200000000111617FE0116FE01131D111D2D04111513140011146F9400000A13242B2F11246F3700000A741800000113170011176FCA00000A14FE01131D111D2D0F11176FCA00000A11176FCB00000A260011246F3E00000A131D111D2DC4DE1D11247541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC0000111617581316111617FE0216FE01131D111D3A6DFFFFFF000011226F3E00000A131D111D3AA6FDFFFFDE1D11227541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC00110D7200600070110C6FB400000A6FC700000A0000111E6F3E00000A131D111D3A9BFCFFFFDE1D111E7541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC00066F2B00000A0072A968007013187E2200000A131914131A0011076F3000000A6F3600000A131E2B6A111E6F3700000A7440000001131B001118111B72006000706F3800000A6F1C00000A72090A0070720D0A00706F3500000A111B72106300706F3800000A6F1C00000A282F00000A1319111906732A00000A131A111A166FA200000A00289F00000A111A6FA000000A0000111E6F3E00000A131D111D2D89DE1D111E7541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC00066F5300000A0000DE10131C007232630070111C73C800000A7A00DE2000062C0B066FC900000A16FE012B011700131D111D2D07066F5300000A0000DC00002A00000041AC000002000000ED0000003A000000270100001D00000000000000020000008004000040000000C00400001D0000000000000002000000A9020000610200000A0500001D0000000000000002000000E30100006C0300004F0500001D0000000000000002000000940500007B0000000F0600001D00000000000000000000003B000000FC05000037060000100000005F000001020000003B0000000F0600004A060000200000000000000013300400CF0600003C00001100036F8700000A724A6900706F8800000A0A036F8700000A727A6900706F8800000A0B036F8700000A72A66900706F8800000A0C036F8700000A72C46900706F8800000A0D036F8700000A72E26900706F8800000A1304036F8700000A72FC6900706F8800000A1305062C0D066F8900000A28A100000A2B0116001306072C0D076F8900000A28A100000A2B0116001307082C08086F8900000A2B057E2200000A001308092C0D096F8900000A28A100000A2B011600130911042C0E11046F8900000A28A100000A2B011600130A11052C0911056F8900000A2B057E2200000A00130B036F8700000A72D26500706F8800000A6F8900000A130C72086A0070110C286300000A130D026FC000000A110D6FBC00000A130E110E14FE01132C112C3AA905000000723A6A0070130D110E110D6FA500000A130F16131038DE02000000110F11106F8600000A1311110616FE01132C112C3A6F0100000011116F8700000A72A66900706F8800000A1312111214FE01132C112C3A24010000001108282300000A132C112C3A100100000011126F8900000A282300000A16FE01132C112C2D1100111211086FC300000A000038E8000000001108178D4E000001132D112D161F2C9D112D6F6500000A131311126F8900000A178D4E000001132D112D161F2C9D112D6F6500000A131473CF00000A1315001114132E16132F2B19112E112F9A131600111511166FD800000A2600112F1758132F112F112E8E69FE04132C112C2DD9001113132E16132F2B37112E112F9A131700111511176FD900000A2D091117282300000A2B011700132C112C2D0C00111511176FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB1112726A6A00701115D036000001285F00000A6FDA00000A740800001B28DB00000A6FC300000A000000002B28000272A66900706FD600000A1312111211086FC300000A0011116F8700000A11126FD700000A260000110916FE01132C112C3A400100000011116F8700000A72FC6900706F8800000A1318111814FE01132C112C3AF500000000110B282300000A132C112C3AE10000000011186F8900000A282300000A16FE01132C112C2D11001118110B6FC300000A000038B900000000110B178D4E000001132D112D161F2C9D112D6F6500000A131911186F8900000A178D4E000001132D112D161F2C9D112D6F6500000A131A111A28DC00000A131B001119132E16132F2B37112E112F9A131C00111B111C6FD900000A2D09111C282300000A2B011700132C112C2D0C00111B111C6FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB1118726A6A0070111BD036000001285F00000A6FDA00000A740800001B28DB00000A6FC300000A000000002B28000272FC6900706FD600000A13181118110B6FC300000A0011116F8700000A11186FD700000A260000001110175813101110110F6F8200000AFE04132C112C3A0EFDFFFF036FCA00000A131D111D6F8700000A72A66900706F8800000A131E111D6F8700000A72FC6900706F8800000A131F111E2C09111E6F8900000A2B057E2200000A001320111F2C09111F6F8900000A2B057E2200000A001321110716FE01132C112C3A13010000001120282300000A132C112C3A01010000001108178D4E000001132D112D161F2C9D112D6F6500000A13221120178D4E000001132D112D161F2C9D112D6F6500000A1323112228DC00000A1324001123132E16132F2B37112E112F9A132500112411256FD900000A2D091125282300000A2B011700132C112C2D0C00112411256FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB0814FE01132C112C2D3000036F8700000A72A66900706F8800000A726A6A007011246FDD00000A740800001B28DB00000A6FC300000A00002B3B000272A66900706FD600000A13261126726A6A007011246FDD00000A740800001B28DB00000A6FC300000A00036F8700000A11266FD700000A26000000110A16FE01132C112C3A14010000001121282300000A132C112C3A0201000000110B178D4E000001132D112D161F2C9D112D6F6500000A13271121178D4E000001132D112D161F2C9D112D6F6500000A1328112728DC00000A1329001128132E16132F2B37112E112F9A132A001129112A6FD900000A2D09112A282300000A2B011700132C112C2D0C001129112A6FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB110514FE01132C112C2D3000036F8700000A72FC6900706F8800000A726A6A007011296FDD00000A740800001B28DB00000A6FC300000A00002B3B000272A66900706FD600000A132B112B726A6A007011296FDD00000A740800001B28DB00000A6FC300000A00036F8700000A112B6FD700000A26000000002A001B300700ED0100003D0000110000026FDE00000A6F9400000A130738A801000011076F3700000A74180000010A00066FDF00000A6F3A00000A0B066F8700000A72D26500706F8800000A6F8900000A6FC200000A0C7E2200000A0D1413041413051613060713081108391F0100001108726E6A0070282400000A2D27110872866A0070282400000A3A88000000110872A46A0070282400000A3AB300000038EA00000072CC6A007008286300000A0D03096FBC00000A1304110414FE01130911092D390004050611040E0473CF00000A17286E0000060011041005066FD000000A16FE01130911092D0F06110404050E041104286C00000600002B170017130672B66600700405060E0417286D0000061305002B7872086B007008286300000A0D03096FBC00000A1304110414FE0116FE01130911092D1700171306724A6B00700405060E0417286D0000061305002B3C72686B007008286300000A0D03096FBC00000A1304110414FE0116FE01130911092D170017130672B46B00700405060E0416286D0000061305002B0011062C07110514FE012B011700130911092D27000E0514FE01130911092D0F000311050E056FE000000A26002B0B000311056FB500000A2600000011076F3E00000A130911093A48FEFFFFDE1D11077541000001130A110A14FE01130911092D08110A6F3F00000A00DC002A000000411C0000020000000F000000BF010000CE0100001D000000000000001B300500EE0100003E000011000E04026FE100000A0A00056F8700000A6FE200000A13072B3711076F3700000A74580000010B000E04076FDF00000A6FD600000A0C08076F8900000A6FC300000A00066F8700000A086FD700000A260011076F3E00000A130811082DBCDE1D110775410000011309110914FE01130811082D0811096F3F00000A00DC000E0516FE01130811083A5A0100000028E300000A0D066F8700000A72D26500706F8800000A1203FE16110000016F1C00000A6FC200000A6FC300000A00066F8700000A72DC6B00706F8800000A14FE01130811082D2C00066F8700000A72DC6B00706F8800000A0F01FE16110000016F1C00000A6FC200000A6FC300000A00002B38000E0472DC6B00706FD600000A130411040F01FE16110000016F1C00000A6FC200000A6FC300000A00066F8700000A11046FD700000A2600066F8700000A72F06B00706F8800000A14FE01130811082D4400066F8700000A72F06B00706F8800000A0F0228E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00002B50000E0472F06B00706FD600000A130511050F0228E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00066F8700000A11056FD700000A2600000613062B0011062A000001100000020018004860001D000000001B300500830200003F0000110000046F8700000A6FE200000A1307380401000011076F3700000A74580000010A000E05066FDF00000A6FD900000A130811083ADF00000000066F8900000A0B7E7800000A0C0007739700000A0C00DE0B26007E7800000A0C00DE0000056F8700000A066FDF00000A6F8800000A0D0914FE01130811082D4000087E7800000A28E500000A16FE01130811082D1B091202FE16110000016F1C00000A6FC200000A6FC300000A002B0D09066F8900000A6FC300000A00002B5D000E04066FDF00000A6FD600000A1304087E7800000A28E500000A16FE01130811082D1C11041202FE16110000016F1C00000A6FC200000A6FC300000A002B0E1104066F8900000A6FC300000A00056F8700000A11046FD700000A2600000011076F3E00000A130811083AECFEFFFFDE1D110775410000011309110914FE01130811082D0811096F3F00000A00DC000E0616FE01130811083A2C01000000056F8700000A72666000706F8800000A14FE01130811082D2C00056F8700000A72666000706F8800000A0F00FE16110000016F1C00000A6FC200000A6FC300000A00002B38000E0472666000706FD600000A130511050F00FE16110000016F1C00000A6FC200000A6FC300000A00056F8700000A11056FD700000A2600056F8700000A727C6000706F8800000A14FE01130811082D4400056F8700000A727C6000706F8800000A0F0128E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00002B50000E04727C6000706FD600000A130611060F0128E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00056F8700000A11066FD700000A2600002A004134000000000000460000000B000000510000000B00000065000001020000000F0000001B0100002A0100001D000000000000001B3007008A040000400000110072086C0070168D03000001284100000A0A03066FA500000A0B722A6C0070168D03000001284100000A0C03086FA500000A0D72446C0070168D03000001284100000A13040311046FA500000A130500076F9400000A131738B500000011176F3700000A741800000113060011066F8700000A72D26500706F8800000A6F8900000A6F1C00000A130772606C007011076FC200000A286300000A13080211086FBC00000A1309110914FE0116FE01131811182D1F0072606C007011076F3A00000A286300000A13080211086FBC00000A130900110914FE01131811182D28007E7800000A7EA900000A110911060473CF00000A16286E000006000211096FCB00000A26002B0B000311066FCB00000A26000011176F3E00000A131811183A3BFFFFFFDE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC0002066FA500000A130A00110A6F9400000A13172B3311176F3700000A7418000001130B0072136800707E7800000A7EA900000A110B0416286D000006130C03110C6FB500000A260011176F3E00000A131811182DC0DE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC0000096F9400000A131738B500000011176F3700000A7418000001130D00110D6F8700000A72D26500706F8800000A6F8900000A6F1C00000A130772986C007011076FC200000A286300000A13080211086FBC00000A130E110E14FE0116FE01131811182D1F0072986C007011076F3A00000A286300000A13080211086FBC00000A130E00110E14FE01131811182D28007E7800000A7EA900000A110E110D0473CF00000A16286E0000060002110E6FCB00000A26002B0B0003110D6FCB00000A26000011176F3E00000A131811183A3BFFFFFFDE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC0002086FA500000A130F00110F6F9400000A13172B3311176F3700000A741800000113100072C86C00707E7800000A7EA900000A11100416286D00000613110311116FB500000A260011176F3E00000A131811182DC0DE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC000011056F9400000A131738B500000011176F3700000A741800000113120011126F8700000A72D26500706F8800000A6F8900000A6F1C00000A130772D46C007011076FC200000A286300000A13080211086FBC00000A1313111314FE0116FE01131811182D1F0072D46C007011076F3A00000A286300000A13080211086FBC00000A131300111314FE01131811182D28007E7800000A7EA900000A111311120473CF00000A16286E000006000211136FCB00000A26002B0B000311126FCB00000A26000011176F3E00000A131811183A3BFFFFFFDE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC000211046FA500000A13140011146F9400000A13172B3311176F3700000A741800000113150072066D00707E7800000A7EA900000A11150416286D00000613160311166FB500000A260011176F3E00000A131811182DC0DE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC002A0000014C000002005800CC24011D00000000020055014499011D000000000200C001CC8C021D000000000200BD024401031D0000000002002903CCF5031D0000000002002704446B041D0000000013300500CE0100004100001100026F8700000A72146D00706F8800000A14FE01130511052D3F00026F8700000A72146D00706F8800000A6F8900000A28E600000A0A0617580B026F8700000A72146D00706F8800000A0728E700000A6FC300000A00002B27000372146D00706FD600000A0C0872230B00706FC300000A00026F8700000A086FD700000A2600026F8700000A722E6D00706F8800000A14FE01130511052D4200026F8700000A722E6D00706F8800000A28C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00002B4A0003722E6D00706FD600000A0D0928C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00026F8700000A096FD700000A2600026F8700000A72466D00706F8800000A14FE01130511052D4200026F8700000A722E6D00706F8800000A28C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00002B4D000372466D00706FD600000A1304110428C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00026F8700000A11046FD700000A26002A00001B300700D403000042000011000E057E7800000A811100000172C00F0070732600000A0A140B00737E00000A0C737E00000A0D726E6500700F00FE16110000016F1C00000A286300000A1304110406732A00000A13051105166FA200000A00066F2B00000A0011056F6400000A6F1C00000A0B066F5300000A0009076F7F00000A0072406600700F01FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A1307110714FE0116FE01131111112D2F0072406600700F01FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130700110714FE0116FE01131111112D0C00726A6D007073CE00000A7A00080E046F5700000A6F7F00000A0000DE042600FE1A007E7800000A1308161309086FC000000A6F8700000A72D26500706F8800000A130A110A14FE01131111112D5A00110A6F8900000A282300000A16FE01131111112D0700171309002B3C0000110A6F8900000A739700000A130800DE0D260072BC6D007073CE00000A7A0012087E7800000A28CD00000A16FE01131111112D05001713090000001109131111113A490100000072F06D00701208FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FA500000A130B110B2C0F110B6F8200000A16FE0116FE012B011600131111112D2F0072F06D00701208FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FA500000A130B00110B2C0F110B6F8200000A16FE0216FE012B011700131111113AA40000000016130C388600000000110B110C6F8600000A130D73CF00000A130E110E72146D00706FD800000A26110E722E6D00706FD800000A26110E72466D00706FD800000A260528E800000A28E900000A086FC000000A110D09110E17286E000006000E0628D400000A16FE01131111112D09110D09287000000600086FC000000A110D09286F0000060000110C1758130C110C110B6F8200000AFE04131111113A66FFFFFF000E051108811100000100388200000000724A6B00700428E800000A28E900000A086FC000000A0917286D000006130F110F14FE01131111112D56001107110F6FB500000A260E0628D400000A16FE01131111112D09110F09287000000600086FC000000A110F09286F000006000E05110F6F8700000A72D26500706F8800000A6F8900000A739700000A8111000001000072CE660070096FB400000A72090A0070720D0A00706F3500000A0F00FE16110000016F1C00000A282F00000A1304110406732A00000A13051105166FA200000A00066F2B00000A00289F00000A11056FA000000A00066F5300000A0000DE0613100011107A002A414C000000000000FA000000120000000C0100000400000003000001000000005B010000120000006D0100000D00000003000001000000001A000000B2030000CC030000060000005F0000011B300600D2050000430000110072C00F0070732600000A0A140B00737E00000A0C737E00000A0D726E6500700F00FE16110000016F1C00000A286300000A1304110406732A00000A13051105166FA200000A00066F2B00000A0011056F6400000A6F1C00000A0B066F5300000A0009076F7F00000A0072406600700F01FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A1307110714FE0116FE01131B111B2D2F0072406600700F01FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130700110714FE0116FE01131B111B2D0C0072286E007073CE00000A7A725E6E00701306110711066FA500000A13080E0528EA00000A28D400000A16FE01131B111B2D55000011086F9400000A131C2B1A111C6F3700000A7418000001130900110711096FCB00000A2600111C6F3E00000A131B111B2DD9DE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC000073EB00000A130A110711066FA500000A13080011086F9400000A131C2B30111C6F3700000A7418000001130B00110A110B6F8700000A72D26500706F8800000A6F8900000A110B6FEC00000A0000111C6F3E00000A131B111B2DC3DE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0016130C0E0428D400000A16FE01131B111B3AF60000000011076FCA00000A11066FA500000A130D00110D6F9400000A131C38A5000000111C6F3700000A7418000001130E00110E6F8700000A72D26500706F8800000A6F8900000A130F110A110F6FC200000A6FED00000A130C110C131B111B2D1200110A110F6F3A00000A6FED00000A130C00110C131B111B2D4B0072B46B00707E7800000A7EA900000A110E0916286D0000061310110711106FB500000A26110A11106F8700000A72D26500706F8800000A6F8900000A6FC200000A11106FEC00000A000000111C6F3E00000A131B111B3A4BFFFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0000000872946E0070046F5700000A72AC6E0070282900000A6F7F00000A0000DE0613110011117A0000086FC000000A6FDE00000A6F9400000A131C38A5000000111C6F3700000A741800000113120011126F8700000A72D26500706F8800000A6F8900000A1313110A11136FC200000A6FED00000A130C110C131B111B2D1200110A11136F3A00000A6FED00000A130C00110C131B111B2D4B0072B46B00707E7800000A7EA900000A11120916286D0000061314110711146FB500000A26110A11146F8700000A72D26500706F8800000A6F8900000A6FC200000A11146FEC00000A000000111C6F3E00000A131B111B3A4BFFFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC000528D400000A16FE01131B111B3A5101000000723A6A00701306110711066FA500000A13150011156F9400000A131C38FE000000111C6F3700000A741800000113160000110A6FEE00000A6FEF00000A131E38AA000000111E6F3700000A743600000113170072686B007011176F1C00000A6FC200000A286300000A1306111611066FBC00000A1318111814FE0116FE01131B111B2D250072686B007011176F1C00000A6F3A00000A286300000A1306111611066FBC00000A131800111814FE0116FE01131B111B2D3600110A11176FF000000A7418000001131972B46B00707E7800000A7EA900000A11190916286D000006131A1116111A6FB500000A260000111E6F3E00000A131B111B3A46FFFFFFDE1D111E7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0000111C6F3E00000A131B111B3AF2FEFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC000014130A72CE660070096FB400000A72090A0070720D0A00706F3500000A0F00FE16110000016F1C00000A282F00000A1304110406732A00000A13051105166FA200000A00066F2B00000A00289F00000A11056FA000000A00066F5300000A0000DE0613110011117A002A000041C4000002000000200100002B0000004B0100001D00000000000000020000008601000041000000C70100001D000000000000000200000016020000BC000000D20200001D0000000000000000000000F10200002000000011030000060000005F000001020000002B030000BC000000E70300001D000000000000000200000057040000C1000000180500001D00000000000000020000003404000015010000490500001D00000000000000000000000E000000BC050000CA050000060000005F0000011E02285900000A2A133005006300000044000011001B8D680000010C081672BB0900701F0E73F100000AA20817727D1600701F0C156A73F200000AA2081872C66E00701E73F100000AA2081972D86E00701F0E73F100000AA2081A72EA6E00701F0C156A73F200000AA20873F300000A0A060B2B00072A0013300400EC000000450000110072FC6E0070732600000A0A066F4A00000A0B07026F2C00000A00066F2B00000A00076F4E00000A0C28740000060D086F4F00000A16FE01130411043A9C0000000038860000000009160872BB0900706F5000000AA5110000016FF400000A00091708727D1600706F5000000A74360000016FF500000A0009180872C66E00706F5000000AA53E0000016FF600000A0009190872D86E00706F5000000AA5110000016FF400000A00091A0872EA6E00706F5000000A74360000016FF500000A00289F00000A096FF700000A0000086F5100000A130411043A6BFFFFFF00086F5200000A00066F5300000A002A133003004A000000460000110072FC6E0070732600000A0A066F4A00000A0B07026F2C00000A00076F4B00000A72306F00701F196F4C00000A036F4D00000A00066F2B00000A00076F9C00000A26066F5300000A002A00001330020031000000460000110072FC6E0070732600000A0A066F4A00000A0B07026F2C00000A00066F2B00000A00076F9C00000A26066F5300000A002A000000133003009B00000047000011000314FE0116FE010C082D077E2200000A10010328F800000A10010516FE010C082D1272466F00700372466F0070282900000A1001140A020D091A300A09172E16091A2E202B3D091E2E1E091F102E2C091F202E2B2B2D724C6F007003283400000A0A2B2A030A2B2672866F00700372CE6F0070282900000A0A2B13030A2B0F030A2B0B721270007073CE00000A7A0604287A0000060B2B00072A00133003007E00000048000011000314FE0116FE010C082D077E2200000A10010328F800000A10010516FE010C082D1272466F00700372466F0070282900000A1001140A020D0917594504000000020000000A000000160000000E0000002B1472347000700A2B17030A2B13727E7000700A2B0B721571007073CE00000A7A0604287A0000060B2B00072A0000133002001E0000000E00001100030B072D0B021F11734400000A0A2B0B021F10734400000A0A2B00062A0000133002003000000049000011000316FE010C082D0E723971007002286300000A0A2B0C72B571007002286300000A0A061F11734400000A0B2B00072A133003001A0000000C00001100020A0672090A0070720D0A00706F3500000A0A060B2B00072A0000133003001A0000000C00001100020A06723572007072397200706F3500000A0A060B2B00072A000013300300B10000004A00001100020A06282300000A0C083A9B0000000006723972007072357200706F3500000A0A06724572007072397200706F3500000A0A06725572007072637200706F3500000A0A067263720070726D7200706F3500000A0A067271720070727B7200706F3500000A0A06727F72007072997200706F3500000A0A06729972007072A77200706F3500000A0A0672AB72007072090A00706F3500000A0A0672B972007072C97200706F3500000A0A00060B2B00072A000000133003008C0000004A00001100020A06282300000A0C082D79000672C972007072B97200706F3500000A0A0672090A007072AB7200706F3500000A0A0672A7720070727F7200706F3500000A0A06726D72007072557200706F3500000A0A06723972007072457200706F3500000A0A06723572007072397200706F3500000A0A06727B72007072717200706F3500000A0A00060B2B00072A13300200680000004B00001100037E1C000004734400000A0A06026F4600000A0B731500000A0C2B2F0008076F4700000A6F1B00000A26076FF900000A0B076FFA00000A16FE01130411042D0A0008046F1B00000A260000076FFA00000A130411042DC5086F1C00000A731D00000A0D2B00092A221F23801C0000042A1E02285900000A2A00000042534A4201000100000000000C00000076322E302E35303732370000000005006C000000301F0000237E00009C1F00007C1E000023537472696E677300000000183E0000D072000023555300E8B00000100000002347554944000000F8B00000C412000023426C6F620000000000000002000001571F02080902000000FA2533001600000100000069000000140000002A0000008C000000A901000001000000FA00000009000000390000004B0000000800000001000000040000000C00000000000A0001000000000006000D0106010A003E01230106004F0106010600560106010A00750160010600A6019A010600C501BB010600D701BB010A00EA0160010A000502600106003E022B020A00AB0260010A00B20260010E00380319030E003E0319030A00B805A2050600D70506011200910686060A003508A2050A0050081701060086086B0812009E0886060A009F0A60011200490B86061200870B860606009F0B2B020A00620C60010A00440D23010E00E70D19030A00550E23010600B20E930E06001C130A13060033130A13060050130A1306006F130A1306008813930E06009C130A130600C2130A130600DB130A130600F8130A13060011140A1306002C140A13060045140A13060073146014B300871400000600B61496140600D614961406000C1506010A00221523010A004315230106004A15930E06006015930E0A00AF1523010600C41506010A00F215A2050A001016FD150A002216FD150A003C16A2050A004B16FD150A005E1617010A00791617010600A91606010600B7162B020A00DD16170106001B1706010E00431719030E004B1719030A007117A2050A00971717010A00A517FD150A00BB17A2050A00D717FD150E00CD1819030600841906010600891906010A00AD1917010A00B81917010600D91906010600E41906010600F4192B020600FA1906010600261A06010600321A96140A004D1A230157008B1A00000600DB1A6B081200131B86061200391B86060600461B060106007A1B06010A00911B17010A00B61B23010A00C11B23010600B11C06010600CA1C06010A00D41C17011200411D86060600681D06010600731D2B021200B31D86060600CB1D06010600F41D2B0206000A1E2B020A001F1E23010E00110E19030000000001000000000001000100092110002600000005000100010001001000320000000D000200070001001000470000000D00020022000A011000580000000500020074000A011000730000000500040074000A011000860000000500090074000A0110009E00000005000D00740081011000B800C4000D001100740002010000D600000011001100740002010000DF00000011001800740081011000E900C4000D001C00740001001000F10000000D001C00800003011000001800000D001D00830003011000131800000D001E00840003011000701800000D002000860003011000DC1800000D002400880003011000151900000D0026008A0003011000281900000D0027008B0000000000961A00000D0029008D000100B4011F000600230C79030600330C79030600230C790306003F0C790306004B0C79030600540C790306005B0C790306006B0C7C030600780C7C030600850C800306008F0C80030600970C84030600A40C84030600850C800306008F0C80030606B10C88035680B90C8B035680C50C8B035680D70C8B035680E00C8B035680EF0C8B035680FF0C8B030606B10C880356800E0DAD0356801B0DAD0356802D0DAD033600F40DE1030600130F2E080600261832080600250F360806008318400806002618320806002F0F360806003E0F36080600130F790306003E0F36080600130F2E0806003B19910806003E0F36081300E81A520B1300FA1B520B50200000000086005B010A00010060200000000086007F010E00010091200000000086008A0114000200A82000000000860090011A000300ED2000000000E601D20123000300012100000000E601E401290004001821000000009600F2012F000500382B0000000096000E023E000A007C2B0000000096001C0248000D00DC2B0000000096004A0252001000342C0000000096005B025D001300502C0000000096007402650015009C2C000000009600890271001A00A82D000000009600BD0279001C00C02F000000009600E50287002100D43000000000910044039700270030310000000091004403A0002A0090310000000096006403AD002F0030320000000096008903BD0035008432000000009600AF03AD003A002433000000009600D503BD0040007833000000009600FC03CB004500B4330000000096001B04BD00490008340000000096003D04D7004E0044340000000096006204E300520094340000000096008A04F1005700F035000000009600AC04FF005C00C836000000009100D50497006200E836000000009600EB040F016500A037000000009600140519016800D038000000009600450525016C006C3D00000000910056052D016E0006410000000086185E050A00790010410000000096006405500179000442000000009600760561017F00A842000000009100C6056E018300F842000000009100DC05750185007043000000009100ED0575018800E843000000009100FC057E018B003C44000000009100180675018D00B4440000000091002E0686019000584C00000000910044067E019800AC4C000000009100560675019A00244D00000000910074069A019D00A44D0000000091009D06A401A000544F000000009100AB06AB01A100BC4F000000009100B7067501A4003450000000009100CC06B301A7007050000000009100E706B301A800AC500000000091000007B901A90004520000000091001307C501AF00EC520000000091002307CF01B20010540000000091003407D901B50034550000000091004607C501B800E0550000000091005B07C501BB008C560000000091006C079A01BE0044570000000091007B079A01C100FC5700000000910090077501C4007458000000009100A9077501C700EC58000000009100C6077501CA0064590000000091006C077501CD00DC59000000009100DF077501D000545A000000009100F5077501D300CC5A0000000091000B087501D600445B0000000091001D087501D900BC5B0000000091004208E301DC00445C0000000091005A08ED01DF00E45C0000000091008D08F601E100B45D000000009100AA080002E2002861000000009100B3080802E400A861000000009100BC081302E5002862000000009100C408A401E600D863000000009100CF08A401E7008865000000009100E0081D02E8000C67000000009100ED081D02E900E868000000009100F9082402EA00046B00000000910006092B02EB001C6D00000000910014091D02EC00A06E00000000910025093202ED00846F0000000091003B093D02EE002C700000000091003B094302EF00C8700000000091004A094D02F000E8700000000091004A095302F1008071000000009100B3085A02F3000C7200000000910059094D02F400947200000000960075096502F500E0730000000096008B097302FA009074000000009600A409850201016875000000009600B609910205019076000000009600CB0985020B016877000000009600DE0991020F019078000000009600F409850215015C79000000009600010A91021901F879000000009600110A85021F01D87A000000009600230A91022301747B000000009600380A65022901C07C000000009600470A73022E01707D000000009600590AA1023501447E0000000096006D0AB3023C01407F000000009600840AC90245018480000000009600AB0AD5024901AE80000000009600C10AE1024D01C480000000009600D50AF40254014486000000009600F10AF4025701D48C000000009100100BE1025A01F8900000000091002A0BFC0261012098000000009100510B08036601FC9E000000009100770B1003680114A1000000009100920B20036E0120A3000000009100A90B2F037401E4A5000000009100B70B40037B01C8AA000000009100D30B4A037E01A4AC000000009600E70B52038001D0B0000000009600050C6503870174B70000000086185E050A008F017CB7000000009600520DB1038F01ECB7000000009600630DB6038F01E4B8000000009600740DBB0390013CB9000000009600830DB60392017CB9000000009600930DC103930124BA000000009600930DCB039701B0BA000000009100930DD5039B01DCBA0000000096009C0DD5039D0118BB000000009600A90DDC039F0140BB000000009600BA0DDC03A00168BB000000009600C50DDC03A10128BC000000009600D60DDC03A201C0BC000000009600FC0DE503A3013DBD0000000086185E050A00A60134BD000000009118721E1A12A601382F0000000086185E050A00A601402F0000000086185E050A00A601902F00000000860036183A08A601482F0000000086185E050A00A701502F00000000860093183A08A70103310000000086185E050A00A8010C31000000008600EF183A08A801B0350000000086185E050A00A901B8350000000086185E050A00A901C0350000000086004B193A08A901000001000B0E00000100110E00000100170E00000100190E000001001B0E00000200230E000003002D0E000004003D0E00000500470E000001004E0E00000200670E00000300740E000001004E0E00000200670E00000300740E000001004E0E00000200670E00000300740E00000100810E020002008D0E000000000000000001004E0E00000200670E00000300740E00000400BF0E00000100CB0E00000200D50E000001004E0E00000200DC0E00000300E70E00000400F10E00000500FC0E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E00000600FC0E000001001F0F00000200250F00000300130F000001001F0F00000200250F00000300130F000004002F0F000005003E0F00000100520F00000200580F00000300640F00000400DC0E00000500E70E00000600F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E00000100520F00000200580F00000300640F00000400DC0E00000500E70E00000600F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E000001004E0E00000200DC0E00000300E70E00000400F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E000001004E0E00000200DC0E00000300E70E00000400F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E000001004E0E00000200DC0E00000300E70E00000400F10E00000500FC0E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E00000600FC0E000001008D0E000002003E0F00000300130F000001004E0E000002006C0F00000300730F000001004E0E000002006C0F00000300820F00000400730F00000100890F00000200960F000001009F0F02000200A30F02000300A80F02000400AD0F02000500B20F02000600B70F02000700BC0F02000800C10F02000900C60F02000A00CB0F02000B00D00F00000100D60F00000200DA0F00000300E80F02000400F30F000005000010000006000F1000000100D60F00000200DA0F00000300E80F02000400F30F000001001D10000002002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001003C10000002002410000001002F10000002003C1000000300241000000100D60F00000200DA0F00000300E80F02000400F30F020005001D10020006004A10000007002410000008005410000001005410000002002410000001002F10000002003C10000003002410000001006210000002003C10000003002410000001007010000001007910000002008310000003002410000001002F10000002003C10000003002410000001002410000001002410000001003C10000002001D10000003004A1000000400E80F000005002410000006000010000001009010000002003C10000003002410000001009C10000002003C10000003002410000001009C10000002003C10000003002410000001009010000002003C10000003002410000001009010000002003C1000000300241000000100A910000002003C1000000300241000000100A910000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C1000000300241000000100B410000002003C1000000300241000000100C510000002003C10000003002410000001002F10000002003C1000000300241000000100D31000000200DF1000000300EA1000000100D31000000200DF1000000100EA1000000100EF1000000200F810000001007010000001007010000001007010000001007010000001007010000001007010000001000111000001000111000001007010000001007010000001000711000001000F1100000100011100000100011100000200181100000100071100000100211100000100D50E00000200DC0E00000300E70E00000400F10E00000500FC0E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000700FC0E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000500FC0E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000700FC0E00000100D50E00000200301100000300DC0E00000400391100000500E70E00000600F10E00000700FC0E00000100D50E00000200261100000300DC0E00000400391100000500130F000006004F1100000700E70E00000800F10E00000900FC0E00000100D50E000002006C0F00000300820F00000400730F00000100D50E000002006611000003007211000004007D1100000100D50E000002008A11000003006611000004009711000005007211000006007D1102000700A11100000100A41100000200AF1100000300721100000100A41100000200AF1100000300721100000100D50E000002008A11000003006611000004009711000005007211000006007D1102000700A11100000100D50E00000200BC1100000300AF1100000400721100000500C81100000100D91100000200DF1100000100E61100000200DF1100000300ED11000004007D1100000500D91100000600FA1100000100F81000000200ED11000003007D1100000400E61100000500D91100000600081200000100ED11000002007D1100000300E61100000400DF1100000500D91100000600151200000700081200000100241200000200311200000300D911000001003F1200000200D91100000100D50E000002008A11000003004A12000004005412000005005F1202000600711200000700741200000100D50E000002008012000003008E1200000400A21200000500AC1200000600B412000007007211000008007D1100000100CE1200000100CE1200000200261100000100CE1200000100DD1200000200E21200000300E70E00000400F10E00000100DD1200000200E21200000300E70E00000400F10E00000100670E00000200E70E00000100E91200000200730F00000100ED1200000100ED1200000100ED1200000100ED1200000100ED1200000200F31200000300FB12000001008D0E000001008D0E000001008D0E000001008D0E02000900F1005E050A00F9005E050A0001015E05000409015E05000411015E05000419015E05000421015E05050429015E05000431015E05000439015E05000441015E05000449015E05000451015E05000459015E05000461015E050A0471015E05110479015E050A0081015E050A0089015E05160499015E05A40431005E050A0029006B15AB0429007615AF0431008015B30431008D15BE0431009415C40431008D15CB0419009B15AF0429005E0500043900A415AF0431005E0500044100E4010004A9015E050A00B101CB157903B101D115FE05B101DF150306B1014315090681005E050004A1005E050A00490076151106B101EB151606B9015E051D06C1011D160A00C9012C160004D1015E052406D90159162B06B10143153106A10070163806E90194163E06B1019E163E06F1019B15AF04B101EB154206B101AF164806E901C3164E06F901D11654060102E5165806B101EE16AF04B101F316AF04B101EB155D06B101FB160306B10109176306F9011217AB04090227170A00B101EB156806B1014315700629002F177706510076153E0679005E05B30679003B17630679003803C10611027615AF0479005B1726077900AF16480681006317DE07B9018817E4072102A117EA073102B117F307B901C917F8074102E417AB044102E51658064102D201AB044102F0170A00C101F0170A0031009E163E06B101F617FE0769007615AB0461007615AF041902C3164E0619005E050A0061006B15AB0449025E0548087900AF164E0811027A193E0611029E163E0651029B196B0A61025E05740AA100CD197C0A6902A117820AB1014315890AC90142085406B101DE198F0A49006B15AB047902EC19960A79029B159B0A81025E0511048102001AF3078102051AA00AA1000D1AA50A0102141AAB0AE101A117B10A81021D1A54068102211A5406810294163E060102E516E60A91020B0EEB0A49002F17F00A49002D1AF70A99025E050A00A1025E050A008900CB15020B69006B15AB0451005E05110499005E050A003102631A00049900751A1B0BB1005E050A00B100831A00040C00C3163C0B1400D1164D0B910094163E061C005E0511041C00A117630B1C00FC1A6B0B9100081B740BC1002A1B7A0BB902081B800BC1007615AF04C9025E051104C9024E1B870BD900571B7C03D900601B900BC902FB16980BF1014E1BA20B5100571B84035100601BA90BC1006C1BAF0414001217AB049100C3164E06D1029B15AF0424005E050A0089005E0500042400A117DE0BB9015E050A00B901821B030CC9019D1B090CC901830D3E063102AD1B11042102A117200CE102C91B270CE902D21B2406D102E11BFE05C901E71B11040C005E050A000C00A117DE0BC1000E1CB10C2C005E050A00F101E11B060D2C00A117DE0BB9001A1C8003B900571B8003D900E11B460DB900E11B4C0D5100E11B620D51002F17680D34005E050A003400A117DE0B2400C3163C0B3C00D1164D0B3C001217AB04C100231CAF04C100301CEF0D31008015050E310080150E0E79023C1C160E310080153A0EE902441C420EE902551C0A00C100641C500E7900751C5E0EB101811C6306B1018C1C6306B100951C710E31005E051104B101A91CAF04C100B1170004F102BA1C760EF1029B157C0EB101C21C810E0102141A870EF9025E058D0EC101E41C950EC100EE1CDF0EC100FD1CEF0DC100091DAF048900161D300FF9025E050004D1005E050A00C1001D1DAB04C1002F1D0A0069002F17550F6900DF155B0F6900391D640FB1004D1D6A0FB100581D800BB9028D15740FD100A117CE0FD1000917D30FD100051AD80FB1016E1DE40FD100791DEB0FD100051AA00AC100811D5010C100901DAF04C100991D5510B100A51D73102103C3164E068900C31D7910B90076157E108900FB16A51079023C1C060D79029B150111F102DB1D760EB9002F171A116900E61D461131035E050A003103A1174D113103FE1DD30F3103161E53115900C3164E063103E516591141035E059D1141035E05A511E1005E05AE11E1002B1EC111E100331EC811E1003D1ECE11E902461E420E7900551EDC0371005C1E09124903661EAB04080048008F0308004C009403080050009903080054009E0308005800A30308005C00A803080064008F0308006800940308006C0099032E00830099122E008B00A2122E007B0090122E002B0034122E00730034122E003B0034122E0043003A122E006B0085122E0023001E122E0053003A122E00630063122E001B001E122E005B00471243009B001D04E0000B01D60400010B018F0320010B018F0340010B01CE0644010B00ED0380010B013507A0010B014D07C0010B018F03C301B3038F03E0010B018F03E301B3038F030302B3038F032302B3038F0340020B018F034302B3038F0360020B018F036302B3038F0380020B018F038302B3038F03A0020B018F03A4020B00ED03C0020B018F03C4020B00ED03E0020B018F0300030B018F0320030B018F0340030B018F0360030B018F03A0030B018F03C0030B018F03E0030B01CE084004BB038F036004BB038F03400CBB038F03600CBB038F03800CBB038F03A00CBB038F03C00CBB038F03E00CBB038F03000DBB038F03200EBB038F03400EBB038F0300100B018F03BA04D1047D067903BA06C7062D07040813084408560869086E0878087E089508A608B808B80AFB0A060B120B220B2B0BAF0BE40B100C190C2D0C3D0C4C0C5D0C6A0C7A0C910CB70CE80C0B0D210D330D520D6E0D850D9F0DB70DC70DD60DF60D1B0E290E300E480E560E630E6C0E9B0EE40E360F7D0FF30F5E108410AD10CE10061122115E11B611D411E211EA11F311FC1103120E12360B460B5B0BD70B000D7E0DAF0DE10F048000000500020000000000000000000000F41400000200000000000000000000000100FD0000000000020000000000000000000000010017010000000002000000000000000000000001000601000000000200000000000000000000000100860600000000050004000600040007000400080004000A0009000B0009000E0003000F000300100003001100030012000300130003000000003C4D6F64756C653E004272696467656C696E652E434C5246756E6374696F6E732E646C6C0047656E6572617465586D6C0055736572446566696E656446756E6374696F6E730053746F72656450726F63656475726573004E6567617461626C65436F6C6C656374696F6E4F6647756964730050726F70657274795365617263684974656D0044617465416E644D6F6E657952616E67655365617263680044617465416E64496E746567657252616E676553656172636800456E756D657261746F72730046696E64416E645265706C616365434C52004974656D5479706500526567657854797065005574696C697479005265676578536561726368006D73636F726C69620053797374656D0056616C7565547970650053797374656D2E44617461004D6963726F736F66742E53716C5365727665722E536572766572004942696E61727953657269616C697A65004F626A65637400456E756D00496E69740053797374656D2E446174612E53716C54797065730053716C537472696E6700416363756D756C617465004D65726765005465726D696E6174650053797374656D2E5465787400537472696E674275696C64657200737472586D6C0053797374656D2E494F0042696E61727952656164657200526561640042696E6172795772697465720057726974650053716C47756964004E617646696C7465725F47657451756572790053716C496E7433320052656765785F49734D617463680052656765785F4765744D617463680053797374656D2E436F6C6C656374696F6E730049456E756D657261626C650052656765785F4765744D6174636865730052656765785F4765744D6174636865735F46696C6C526F770052656765785F5265706C6163654D6174636865730046696E64416E645265706C6163655F476574436F6E74656E744C6F636174696F6E0053716C586D6C0053716C426F6F6C65616E0046696E64416E645265706C6163655F4973436F6E74656E74446566696E6974696F6E4D617463680046696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E670053797374656D2E546578742E526567756C617245787072657373696F6E73004D6174636800526567657800436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F720046696E64416E645265706C6163655F497346696C6550726F706572746965734D617463680046696E64416E645265706C6163655F4765745570646174656446696C6550726F70657274790046696E64416E645265706C6163655F4973496D61676550726F706572746965734D617463680046696E64416E645265706C6163655F47657455706461746564496D61676550726F70657274790046696E64416E645265706C6163655F4973506167654E616D654D617463680046696E64416E645265706C6163655F47657455706461746564506167654E616D650046696E64416E645265706C6163655F49735061676550726F706572746965734D617463680046696E64416E645265706C6163655F476574557064617465645061676550726F706572746965730046696E64416E645265706C6163655F497354657874436F6E74656E744D617463680046696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E7454657874005461674174747269627574654576616C7561746F720046696E64416E645265706C6163655F49734C696E6B496E436F6E74656E74446566696E6974696F6E0046696E64416E645265706C6163655F476574557064617465644C696E6B496E436F6E74656E74446566696E6974696F6E0047657452656C6174696F6E5461626C650046696C6C526F77002E63746F7200436F6E746163745F536561726368434C5200436F6E746163745F4765744175746F446973747269627574696F6E4C697374436F6E74616374436F756E740053797374656D2E446174612E53716C436C69656E740053716C436F6E6E656374696F6E00476574436F756E744279537461747573004775696400457865637574655369746547726F75700045786563757465536974654964730045786563757465427943757272656E7453697465436F6E746578740045786563757465536974654174747269627574657300526574726965766556616C75657346726F6D584D4C004765744D616E75616C436F6E74616374730045786563757465417474726962757465436F6E746163745365617263680045786563756974655369746547726F75700053797374656D2E586D6C00586D6C4E6F64654C697374004765745369746547726F7570730057726974654F75747075740045786563757465436F6E74616374536F757263650043726561746554656D706F726172794F75747075745461626C650044726F7054656D706F726172794F75747075745461626C650045786563757465476574436F6E74616374730045786563757465507572636861736500457865637574654F7264657253697A6500457865637574654F72646572436F756E7400457865637574654162616E646F6E65644361727400457865637574654C6173744C6F67696E0045786563757465576174636865730045786563757465466F726D5375626D6974746564004578656375746550726F6475637473507572636861736564004578656375746550726F647563745479706573507572636861736564004578656375746557656273697465557365725365617263680045786563757465437573746F6D657247726F757073004578656375746553656375697274794C6576656C730045786563757465496E6465785465726D730045786563757465446973747269627574696F6E4C6973740053716C506172616D6574657200457865637574655363616C617200446174615461626C650045786563757465446174615461626C650053797374656D2E436F6C6C656374696F6E732E47656E65726963004C697374603100476574436F6E66696775726174696F6E00586D6C446F63756D656E74006765744E6F64657300476574477569647300476574496E7473004765745761746368657300476574466F726D5375626D6974746564004765744C6173744C6F67696E004765745075726368617365004765744F7264657253697A65004765744F72646572436F756E74004765744162616E646F6E6564436172740047657450726F70657274795365617263684974656D00476574586D6C466F72477569647300476574586D6C466F724E6F64657300476574586D6C466F7250726F70657274795365617263684974656D00476574436F6E74656E74446566696E6974696F6E7300557064617465436F6E74656E74446566696E6974696F6E730047657446696C6550726F706572746965730055706461746546696C6550726F7065727469657300476574496D61676550726F7065727469657300557064617465496D61676550726F7065727469657300476574506167654E616D657300557064617465506167654E616D6573004765745061676550726F70657274696573005570646174655061676550726F706572746965730047657454657874436F6E74656E740055706461746554657874436F6E74656E740046696E64416E645265706C6163655F46696E640046696E64416E645265706C6163655F5265706C6163650046696E64416E645265706C6163655F5570646174654C696E6B730053716C4461746554696D6500434C52506167654D61704E6F64655F55706461746500434C52506167654D61704E6F64655F5361766500434C52506167654D61704E6F64655F416464436F6E7461696E657200434C52506167654D61704E6F64655F52656D6F7665436F6E7461696E657200434C52506167654D61704E6F64655F5361766555706461746500434C52506167654D61704E6F64655F557064617465436F6E7461696E657200586D6C4E6F646500536574757050726F7061676174696F6E416E64496E6865726974616E63654174747269627300497465726174654368696C6472656E00586D6C456C656D656E740053657475704E65774E6F64650041727261794C697374005365744174747269627574657300536574757050616765446566696E6974696F6E4368696C6472656E0053657475705075626C69736844657461696C7300434C52506167654D61705F5361766550616765446566696E6974696F6E00434C53506167654D61704E6F64655F417474616368576F726B666C6F77004E656761746553656C656374696F6E004C6973744F6647756964730050726F706572794E616D65004F70657261746F720056616C7565310056616C7565320053716C4D6F6E6579004D696E696D756D56616C7565004D6178696D756D56616C75650053746172744461746500456E6444617465004D696E696D756D436F756E74004D6178696D756D436F756E740076616C75655F5F0054657874436F6E74656E7400436F6E74656E74446566696E6974696F6E00506167654E616D65005061676550726F7065727469657300496D61676550726F706572746965730046696C6550726F70657274696573005461674174747269627574650054616741747472696275746556616C756500436F6E74656E74446566696E6974696F6E4669656C640053716C446174615265636F72640047657453716C446174615265636F726400476574536561726368526573756C747300506572666F726D5265706C61636500457865637574654E6F6E5175657279004765745265676578004765744C696E6B526567657800466F726D61745365617263685465726D0048746D6C456E636F646500436F6E76657274586D6C546F48746D6C00436F6E7665727448746D6C546F586D6C0052656765784F7074696F6E73004F7074696F6E7300526567657853656C656374416C6C0056616C75650047726F757000720077007175657279496400636F6E646974696F6E004F72646572427950726F706572747900536F72744F726465720053697465496400736F757263650053716C46616365744174747269627574650072656765785061747465726E0072656765784F7074696F6E73006D617463684F626A656374006D617463680053797374656D2E52756E74696D652E496E7465726F705365727669636573004F7574417474726962757465007265706C6163656D656E7400636F6E74656E74496400736974654964007365617263685465726D006D61746368436173650077686F6C65576F72647300696E636C75646541747472696275746556616C756573007265706C6163655465726D006669656C640068746D6C52656765780061747472696275746552656765780061747472696275746556616C75655265676578007469746C65006465736372697074696F6E00616C7454657874006F6C6455726C006D6174636857686F6C654C696E6B006E657755726C0072656C6174696F6E54797065006F626A6563744964006F626A00636F6C3100636F6C3200636F6C3300636F6C3400636F6C3500636F6C3600636F6C3700636F6C3800636F6C3900636F6C313000786D6C004170706C69636174696F6E4964004D61785265636F72647300546F74616C5265636F72647300696E636C7564654164647265737300436F6E746163744C69737449640073746174757300636F6E6E656374696F6E006F7065726174696F6E586D6C006170706C69636174696F6E496400736F72744F7264657200636F6E746163744C6973744964007369746547726F75704974656D006E6F64654C697374004F7065726174696F6E00746F74616C5265636F72647300705365617263684974656D0073656172636856616C756573007365617263684974656D0073656375726974794C6576656C586D6C00696E6465785465726D73586D6C00636F6D6D616E644E616D6500706172616D657465727300636F6E6E00646F63756D656E74006E6F64654E616D65006E6F646573006474477569647300636F6C477569647300726F6F744E6F6465006C697374006F626A65637449647300736561726368496E0068746D6C456E636F6465645365617263685465726D0068746D6C456E636F6465645265706C6163655465726D00706167654D61704E6F6465006D6F6469666965644279006D6F6469666965644461746500706172656E744E6F64654964006372656174656442790069640074656D706C617465496400636F6E7461696E65724964730074656D706C61746549647300696E7365727444656C657465466C6167006462446F630064624E6F646500696E4E6F6465006D6F64696669656442794964006C617374466F756E644E6F6465007365744578747261496E666F006578636C7564654174747269627300696E70757450616765446566006F6E655061676544624E6F6465006462506167654E6F646500437265617465644279004D6F64696669656442790070616765446566696E6974696F6E586D6C004964007075626C6973685061676500706167654D61704E6F6465496400706167654D61704E6F6465576F726B466C6F770070726F706F6761746500696E686572697400617070656E64546F4578697374696E67576F726B666C6F77730073716C436F6D6D616E64546578740074797065007365617263680075726C00696E707574007061747465726E006D6174636844656C696D697465720053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C7943756C7475726541747472696275746500436F6D56697369626C6541747472696275746500417373656D626C79496E666F726D6174696F6E616C56657273696F6E41747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C7954726164656D61726B4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C697479417474726962757465004272696467656C696E652E434C5246756E6374696F6E730053657269616C697A61626C654174747269627574650053716C55736572446566696E656441676772656761746541747472696275746500466F726D6174005374727563744C61796F7574417474726962757465004C61796F75744B696E64006765745F49734E756C6C006765745F56616C756500417070656E64466F726D617400417070656E6400496E7365727400546F537472696E670052656164537472696E670053716C46756E6374696F6E41747472696275746500537472696E6700456D7074790049734E756C6C4F72456D707479006F705F457175616C69747900436F6E6361740053716C436F6D6D616E640053797374656D2E446174612E436F6D6D6F6E004462436F6E6E656374696F6E004F70656E004462436F6D6D616E64007365745F436F6D6D616E64546578740053716C446174614164617074657200446244617461416461707465720046696C6C0044617461526F77436F6C6C656374696F6E006765745F526F777300496E7465726E616C44617461436F6C6C656374696F6E42617365006765745F436F756E74006765745F4C656E67746800496E743332005265706C6163650049456E756D657261746F7200476574456E756D657261746F72006765745F43757272656E740044617461526F77006765745F4974656D005472696D00546F4C6F776572006F705F496E657175616C69747900436F6E7461696E73004D6F76654E6578740049446973706F7361626C6500446973706F7365006F705F496D706C696369740049734D617463680043617074757265004D61746368436F6C6C656374696F6E004D61746368657300437265617465436F6D6D616E640053716C506172616D65746572436F6C6C656374696F6E006765745F506172616D65746572730053716C44625479706500416464004462506172616D65746572007365745F56616C75650053716C44617461526561646572004578656375746552656164657200446244617461526561646572006765745F486173526F777300436C6F736500537562737472696E67003C3E635F5F446973706C6179436C61737332003C3E635F5F446973706C6179436C61737335004353243C3E385F5F6C6F63616C7333003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F31003C3E635F5F446973706C6179436C61737337004353243C3E385F5F6C6F63616C7336003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F30004D617463684576616C7561746F72003C3E635F5F446973706C6179436C61737361003C436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F723E625F5F39003C3E635F5F446973706C6179436C61737364003C3E635F5F446973706C6179436C61737366004353243C3E385F5F6C6F63616C7365003C46696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E74546578743E625F5F63006765745F496E64657800547970650052756E74696D655479706548616E646C65004765745479706546726F6D48616E646C650044617461436F6C756D6E0044617461436F6C756D6E436F6C6C656374696F6E006765745F436F6C756D6E7300436861720053706C697400436F6E7665727400546F496E74363400537461636B00496E743634005075736800546F4172726179004E6577526F77007365745F4974656D00506F70005065656B0044424E756C6C004E756C6C00436F6D70696C657247656E6572617465644174747269627574650053716C50726F636564757265417474726962757465007365745F506172616D657465724E616D65007365745F53716C446254797065004C6F6164586D6C00456E756D657261746F72003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B46454336423245332D423630302D343544342D393532432D4137423342383134393734437D0044696374696F6E61727960320024246D6574686F643078363030303032392D310054727947657456616C7565006765745F4974656D4F6600586D6C417474726962757465436F6C6C656374696F6E006765745F4174747269627574657300586D6C41747472696275746500446563696D616C005472795061727365004D617856616C7565006F705F4578706C69636974006765745F496E6E65725465787400426F6F6C65616E007365745F436F6E6E656374696F6E00436F6D6D616E6454797065007365745F436F6D6D616E6454797065007365745F53697A650053716C436F6E746578740053716C50697065006765745F506970650045786563757465416E6453656E64005061727365007365745F436F6D6D616E6454696D656F75740024246D6574686F643078363030303034352D310053656C6563744E6F646573004D696E56616C7565006765745F4F75746572586D6C00417070656E644368696C6400546F496E7433320053656E64526573756C747353746172740053656E64526573756C7473456E640053656C65637453696E676C654E6F6465006765745F4F7074696F6E73005374617274735769746800456E647357697468006765745F446F63756D656E74456C656D656E7400546F5570706572004461746554696D65006765745F4E6F77005472696D456E6400457863657074696F6E00436F6E6E656374696F6E5374617465006765745F5374617465006765745F506172656E744E6F64650052656D6F76654368696C64006765745F496E6E6572586D6C00457175616C73006765745F4861734368696C644E6F6465730052656D6F7665416C6C006F705F5472756500586D6C4E6F646554797065004372656174654E6F646500437265617465417474726962757465004172726179004A6F696E00494C6973740041646170746572006765745F4368696C644E6F646573006765745F4E616D6500496E73657274416674657200437265617465456C656D656E7400586D6C4E616D65644E6F64654D6170004E65774775696400466F726D6174457863657074696F6E006765745F5574634E6F77006F705F4C6F676963616C4E6F7400486173687461626C6500436F6E7461696E734B65790049436F6C6C656374696F6E006765745F4B6579730053716C4D65746144617461005365744775696400536574537472696E6700536574496E7433320053656E64526573756C7473526F7700457363617065004E6578744D61746368006765745F53756363657373002E6363746F720000000000213C0067007500690064003E007B0030007D003C002F0067007500690064003E0000333C00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064003E0000353C002F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064003E00000B500072006900630065000080912C002800730065006C0065006300740020006D0069006E0028004C006900730074005000720069006300650029002000660072006F006D00200050005200500072006F00640075006300740053004B0055002000770068006500720065002000500072006F006400750063007400490064003D0050002E00490064002900200061007300200050007200690063006500001354006F007000530065006C006C00650072000080A92C002800730065006C006500630074002000730075006D002800500072006500760069006F007500730053006F006C00640043006F0075006E00740029002000660072006F006D00200050005200500072006F00640075006300740053004B0055002000770068006500720065002000500072006F006400750063007400490064003D0050002E00490064002900200061007300200054006F007000530065006C006C006500720000174300720065006100740065006400440061007400650000192C0043007200650061007400650064004400610074006500008087530065006C006500630074002000440069007300740069006E0063007400200027007B0030007D0027002000610073002000460069006C007400650072004900640020007B0031007D002C00200050002E00490064002000660072006F006D00200050005200500072006F0064007500630074002000500020005700680065007200650020000180E3530065006C006500630074002000440069007300740069006E0063007400200027007B0030007D00270020002000610073002000460069006C007400650072004900640020007B0031007D002C00200050002E00490064002000660072006F006D00200050005200500072006F00640075006300740020005000200049006E006E006500720020004A006F0069006E00200056005700500072006F0064007500630074004100740074007200690062007500740065002000560020006F006E00200050002E00490064003D0056002E00490064002000570068006500720065002000012F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D0074007200750065000035730065006C00650063007400200074006F00700031002000660072006F006D00200050005200500072006F00640075006300740000822D530065006C00650063007400200053006E006F002C004C00660074004F0070006E0064002C005200670074004F0070006E0064002C004C00660074004F0070006E0064004E0061006D0065002C005200670074004F0070006E0064004E0061006D0065002C004F00700072002C004C00660074004F0062006A0065006300740054007900700065002C005200670074004F0062006A0065006300740054007900700065002C0056002E00560061006C00750065002000520067007400560061006C007500650020002000660072006F006D0020004E005600460069006C007400650072004900740065006D00200046004900200049006E006E006500720020004A006F0069006E0020004E005600460069006C007400650072004900740065006D005100750065007200790020004900510020006F006E002000460049002E004900640020003D00490051002E004900740065006D0049006400200049006E006E006500720020004A006F0069006E0020004E005600460069006C00740065007200510075006500720079002000510020006F006E002000490051002E0051007500650072007900490064003D0051002E0049006400200049006E006E006500720020004A006F0069006E0020004E005600460069006C00740065007200560061006C00750065002000560020006F006E002000460049002E005200670074004F0070006E0064003D0056002E0049006400200057006800650072006500200051002E00490064003D0027000123270020004F0072006400650072002000420079002000460049002E0053006E006F000182BD530065006C006500630074002000640069007300740069006E006300740020002700300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D0030003000300030003000300030003000300030003000300027002000490064002C0043002E006E0061006D0065002C0054002E006E0061006D0065002000410074007400720069006200750074006500440061007400610054007900700065002C00310020004900730050006800790073006900630061006C0020002000660072006F006D0020007300790073002E0063006F006C0075006D006E00730020004300200049006E006E006500720020006A006F0069006E0020007300790073002E006F0062006A00650063007400730020004F0020006F006E0020004F002E006F0062006A006500630074005F00690064003D0043002E006F0062006A006500630074005F0069006400200069006E006E006500720020006A006F0069006E00200020007300790073002E00740079007000650073002000540020006F006E00200043002E00730079007300740065006D005F0074007900700065005F00690064003D0054002E00730079007300740065006D005F0074007900700065005F0069006400200020005700680065007200650020004F002E006E0061006D00650020003D0027007B0030007D002700200041006E00640020004F002E0074007900700065003D00270055002700200061006E006400200054002E006E0061006D006500200021003D0027007300790073006E0061006D0065002700200055006E0069006F006E00200041006C006C002000530065006C006500630074002000490064002C005400690074006C0065002C00410074007400720069006200750074006500440061007400610054007900700065002C00300020002000660072006F006D0020007B0031007D00011350005200500072006F00640075006300740000174100540041007400740072006900620075007400650000032000000520007B0000057D00200000032800000528007B0000077B0030007D00001B4C00660074004F0062006A006500630074005400790070006500001B5200670074004F0062006A00650063007400540079007000650000174C00660074004F0070006E0064004E0061006D00650000096E0061006D006500000F4C00660074004F0070006E006400000549006400002341007400740072006900620075007400650044006100740061005400790070006500000F530079007300740065006D002E0000010011520067007400560061006C00750065000003270001052700270001074F007000720000096C0069006B0065000003250000154900730050006800790073006900630061006C0000033100000D64006F00750062006C006500000769006E007400000B63006100730074002800001720006100730020006D006F006E00650079002900200000116400610074006500740069006D0065000005200027000123200063006F006E007600650072007400280076006100720063006800610072002C00000F2C002000310030003100290020000047200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027000111270029002C002000310030003100290001033000000741004E0044000080ED280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200056005700500072006F00640075006300740041007400740072006900620075007400650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E0064002000430041005300540028005B00560061006C00750065005D0020006100730020004D004F004E0045005900290020007B0031007D0020007B0032007D0029002900011D2800410074007400720069006200750074006500490064003D002700013D2700200061006E0064002000430041005300540028005B00560061006C00750065005D0020006100730020004D004F004E0045005900290020002000010329000080D3280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200056005700500072006F00640075006300740041007400740072006900620075007400650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E00640020005B00560061006C00750065005D0020007B0031007D00200027007B0032007D00270029002900011D2700200061006E00640020005B00560061006C00750065005D002000010527002900018173280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200056005700500072006F00640075006300740041007400740072006900620075007400650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E006400200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C005B00560061006C00750065005D0029002C003100300031002900200020007B0031007D00200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027007B0032007D00270029002C002000310030003100290029002900016B2700200061006E006400200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C005B00560061006C00750065005D0029002C00310030003100290020000113270029002C0020003100300031002900290001136F00720064006500720020006200790020000033530065006C006500630074002000460069006C00740065007200490064002C00490064002000660072006F006D0020002800000B2900200046004F002000002F43006F006E007400650078007400200043006F006E006E0065006300740069006F006E003D00740072007500650000866557004900540048000900520065006C0061007400650064004E006F0064006500730020002800490064002C00200050006100720065006E007400490064002C0020005400690074006C0065002C0020005B004C006500760065006C005D0029002000410053000D000A000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200028000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900490064002C00200050006100720065006E007400490064002C0020005400690074006C0065002C00200030000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900480053005300740072007500630074007500720065000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005700480045005200450009004900640020003D0020004000490064000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000090009000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200055004E0049004F004E00200041004C004C000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000090009000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400090070006100720065006E0074002E00490064002C00200070006100720065006E0074002E0050006100720065006E007400490064002C00200070006100720065006E0074002E005400690074006C0065002C0020005B004C006500760065006C005D0020002B00200031000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900520065006C0061007400650064004E006F0064006500730020004100530020006300680069006C0064002C00200048005300530074007200750063007400750072006500200041005300200070006100720065006E0074000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500090070006100720065006E0074002E004900640020003D0020006300680069006C0064002E0050006100720065006E00740049006400200041004E0044000D000A000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200070006100720065006E0074002E004900640020003C003E002000400053006900740065004900640020000D000A000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000D000A000D000A0009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400090009005400690074006C0065000D000A0009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0009000900520065006C0061007400650064004E006F006400650073000D000A00090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F00520044004500520020004200590009005B004C006500760065006C005D00200044004500530043000007400049006400000F4000530069007400650049006400000D7B0030007D0020003E002000000B5400690074006C006500000963006F006C003100000963006F006C003200000963006F006C003300000963006F006C003400000963006F006C003500000963006F006C003600000963006F006C003700000963006F006C003800000963006F006C003900000B63006F006C00310030000080CD530065006C006500630074002000630061007300740028004C0066007400200061007300200076006100720063006800610072002800310030002900290020002B00200027002D00270020002B0020006300610073007400280052006700740020006100730020007600610072006300680061007200280031003000290029002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004E006F006400650054007900700065003D0027007B0030007D002700018105530065006C006500630074002000630061007300740028004C0066007400200061007300200076006100720063006800610072002800310030002900290020002B00200027002D00270020002B0020006300610073007400280052006700740020006100730020007600610072006300680061007200280031003000290029002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004C006600740020003E0020007B0030007D00200061006E006400200052006700740020003C0020007B0031007D00200061006E00640020004F0062006A00650063007400490064003D0027007B0032007D002700018089530065006C0065006300740020002A002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004C006600740020004200650074007700650065006E0020007B0030007D00200061006E00640020007B0031007D0020004F00720064006500720020006200790020004C0066007400000752006700740000074C006600740000114F0062006A0065006300740049006400000D530074006100740075007300003143006F006E0074006100630074005F0047006500740043006F0075006E0074004200790053007400610074007500730000155300690074006500470072006F00750070007300001B4100700070006C00690063006100740069006F006E0049006400002F43006F006E0074006100630074005F005300650061007200630068005300690074006500470072006F0075007000000F5300690074006500490064007300002B43006F006E0074006100630074005F005300650061007200630068005300690074006500490064007300003743006F006E0074006100630074005F00460069006C0074006500720042007900530069007400650043006F006E007400650078007400001D53006900740065004100740074007200690062007500740065007300003943006F006E0074006100630074005F0053006500610072006300680053006900740065004100740074007200690062007500740065007300001353006F00720074004F007200640065007200001D43006F006E00740061006300740053006F0075007200630065007300002144006900730074007200690062007500740069006F006E004C00690073007400001549006E006400650078005400650072006D007300001D530065006300750072006900740079004C006500760065006C007300001D43007500730074006F006D0065007200470072006F00750070007300000F5700610074006300680065007300002357006500620073006900740065005500730065007200530065006100720063006800002D43006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068000023500072006F0064007500630074007300500075007200630068006100730065006400002B500072006F00640075006300740054007900700065007300500075007200630068006100730065006400001B46006F0072006D005300750062006D006900740074006500640000134C006100730074004C006F00670069006E0000115000750072006300680061007300650000134F007200640065007200530069007A00650000154F00720064006500720043006F0075006E007400001B4100620061006E0064006F006E00650064004300610072007400002F470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660049006E0074007300000D560061006C0075006500310000194D0069006E0069006D0075006D00560061006C007500650000194D006100780069006D0075006D00560061006C007500650000194D0069006E0069006D0075006D0043006F0075006E00740000194D006100780069006D0075006D0043006F0075006E007400001B43006F006E0074006100630074004C0069007300740049006400003343006F006E0074006100630074005F004100640064004D0061006E00750061006C0043006F006E0074006100630074007300003343006F006E00740061006300740041007400740072006900620075007400650053006500610072006300680058006D006C00003D43006F006E0074006100630074005F0043006F006E007400610063007400410074007400720069006200750074006500530065006100720063006800001F4E0065006700610074006500530065006C0065006300740069006F006E0000174C006900730074004F00660047007500690064007300003D49006E007300650072007400200069006E0074006F002000740065007300740043006F0075006E0074002000760061006C0075006500730028002700010527002C00013743006F006E0074006100630074005F0053006500610072006300680043006F006E00740061006300740053006F0075007200630065000081D54900460020004500580049005300540053002800530065006C0065006300740020002A002000660072006F006D002000740065006D007000640062002E00640062006F002E007300790073006F0062006A00650063007400730020006F0020007700680065007200650020006F002E0078007400790070006500200069006E00200028002700550027002900090061006E00640009006F002E006900640020003D0020006F0062006A006500630074005F0069006400280020004E002700740065006D007000640062002E002E002300740065006D00700043006F006E0074006100630074005300650061007200630068004F007500740070007500740027002900290020005400720075006E00630061007400650020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400200045004C0053004500200063007200650061007400650020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400280049006400200075006E0069007100750065006900640065006E0074006900660069006500720029000147440072006F00700020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F007500740070007500740000154D00610078005200650063006F00720064007300001D49006E0063006C007500640065004100640064007200650073007300003F43006F006E0074006100630074005F0047006500740043006F006E00740061006300740042007900490064007300460072006F006D00540065006D0070000013530074006100720074004400610074006500000F45006E0064004400610074006500003543006F006E0074006100630074005F005300650061007200630068004C0061007300740050007500720063006800610073006500002F43006F006E0074006100630074005F005300650061007200630068004F007200640065007200530069007A006500003143006F006E0074006100630074005F005300650061007200630068004F00720064006500720043006F0075006E007400003743006F006E0074006100630074005F005300650061007200630068004100620061006E0064006F006E00650064004300610072007400002F43006F006E0074006100630074005F005300650061007200630068004C006100730074004C006F00670069006E0000155700610074006300680065007300490064007300002B43006F006E0074006100630074005F005300650061007200630068005700610074006300680065007300000F46006F0072006D00490064007300003943006F006E0074006100630074005F0053006500610072006300680046006F0072006D0073005300750062006D00690074007400650064000015500072006F006400750063007400490064007300003F43006F006E0074006100630074005F00530065006100720063006800500072006F0064007500630074007300500075007200630068006100730065006400001D500072006F0064007500630074005400790070006500490064007300004743006F006E0074006100630074005F00530065006100720063006800500072006F0064007500630074005400790070006500730050007500720063006800610073006500640000295700650062007300690074006500550073006500720053006500610072006300680058006D006C00003343006F006E0074006100630074005F0053006500610072006300680057006500620073006900740065005500730065007200002343007500730074006F006D0065007200470072006F00750070007300490064007300003943006F006E0074006100630074005F0053006500610072006300680043007500730074006F006D0065007200470072006F007500700073000021530065006300750072006900740079004C006500760065006C00490064007300003743006F006E0074006100630074005F00530065006100720063006800530065006300750072006900740079004C006500760065006C00001B49006E006400650078005400650072006D007300490064007300003143006F006E0074006100630074005F0053006500610072006300680049006E006400650078005400650072006D007300002744006900730074007200690062007500740069006F006E004C00690073007400490064007300003D43006F006E0074006100630074005F0053006500610072006300680044006900730074007200690062007500740069006F006E004C00690073007400008093530065006C0065006300740020004E0061006D0065002000660072006F006D0020004300540043006F006E00740061006300740053006500610072006300680043006F006E006600690067002000570068006500720065002000490073004100630074006900760065003D00310020004F0072006400650072002000420079002000530065007100750065006E006300650000094E0061006D006500006143006F006E0074006100630074005300650061007200630068002F00530069007400650041007400740072006900620075007400650073002F004100740074007200690062007500740065005300650061007200630068004900740065006D00004143006F006E0074006100630074005300650061007200630068002F0043006F006E00740061006300740053006F00750072006300650073002F0069006E007400003B43006F006E0074006100630074005300650061007200630068002F005300690074006500470072006F007500700073002F006700750069006400003543006F006E0074006100630074005300650061007200630068002F0053006900740065004900640073002F006700750069006400004743006F006E0074006100630074005300650061007200630068002F0044006900730074007200690062007500740069006F006E004C006900730074002F006700750069006400003B43006F006E0074006100630074005300650061007200630068002F0049006E006400650078005400650072006D0073002F006700750069006400004143006F006E0074006100630074005300650061007200630068002F00530065006300750072006900740079004C006500760065006C0073002F0069006E007400004343006F006E0074006100630074005300650061007200630068002F0043007500730074006F006D0065007200470072006F007500700073002F006700750069006400004F43006F006E0074006100630074005300650061007200630068002F0057006100740063006800650073005B0040004C006900730074004F0066004700750069006400730021003D00220022005D00006543006F006E0074006100630074005300650061007200630068002F00570065006200730069007400650055007300650072005300650061007200630068002F00500072006F00700065007200740079005300650061007200630068004900740065006D00007143006F006E0074006100630074005300650061007200630068002F0043006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068002F004100740074007200690062007500740065005300650061007200630068004900740065006D00005943006F006E0074006100630074005300650061007200630068002F00500072006F00640075006300740073005000750072006300680061007300650064002F004B0065007900560061006C00750065005000610069007200006143006F006E0074006100630074005300650061007200630068002F00500072006F006400750063007400540079007000650073005000750072006300680061007300650064002F004B0065007900560061006C00750065005000610069007200005B43006F006E0074006100630074005300650061007200630068002F0046006F0072006D005300750062006D00690074007400650064005B0040004C006900730074004F0066004700750069006400730021003D00220022005D00002F43006F006E0074006100630074005300650061007200630068002F004C006100730074004C006F00670069006E00002D43006F006E0074006100630074005300650061007200630068002F0050007500720063006800610073006500002F43006F006E0074006100630074005300650061007200630068002F004F007200640065007200530069007A006500003143006F006E0074006100630074005300650061007200630068002F004F00720064006500720043006F0075006E007400003743006F006E0074006100630074005300650061007200630068002F004100620061006E0064006F006E00650064004300610072007400002943006F006E0074006100630074005300650061007200630068002F00530074006100740075007300002F43006F006E0074006100630074005300650061007200630068002F0053006F00720074004F007200640065007200001B43006F006E00740061006300740053006500610072006300680000114F00700065007200610074006F007200000F4200650074007700650065006E00000D560061006C00750065003200000930002E003000300000353C00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F006600470075006900640073003E0000373C002F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F006600470075006900640073003E000031470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064007300000B3C007B0030007D003E00000D3C002F007B0030007D003E00003F3C0043006F006E0074006100630074005300650061007200630068003E003C002F0043006F006E0074006100630074005300650061007200630068003E00000D5C0077007B0032002C007D0000810B530045004C004500430054002000490064002C0020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280050006100720065006E007400490064002C00200027007B0031007D0027002900200041005300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A00011F460052004F004D00200043004F0043006F006E00740065006E0074000A000080875700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D00200031003300200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D0027002000016541004E00440020004300410053005400280058006D006C0053007400720069006E00670020004100530020006E00760061007200630068006100720028004D00410058002900290020004C0049004B0045002000270025007B0030007D002500270020000180AB41004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730043006F006E00740065006E00740044006500660069006E006900740069006F006E004D006100740063006800280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C0020007B0031007D002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A0001254F00520044004500520020004200590020005400690074006C00650020004100530043000023550050004400410054004500200043004F0043006F006E00740065006E0074000A000080E1530045005400200058006D006C0053007400720069006E00670020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640043006F006E00740065006E00740044006500660069006E006900740069006F006E0058006D006C0053007400720069006E006700280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D002C0020007B0034007D0029000A0001826757004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0043006F006E00740065006E00740044006500660069006E006900740069006F006E0073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001812F530045004C00450043005400200063006F002E00490064002C00200063006F002E005400690074006C00650020004100730020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280063006F002E0050006100720065006E007400490064002C00200027007B0031007D0027002900200061007300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A00017F460052004F004D00200043004F0043006F006E00740065006E007400200063006F00200049004E004E004500520020004A004F0049004E00200043004F00460069006C00650020006600690020004F004E002000660069002E0043006F006E00740065006E0074004900640020003D00200063006F002E00490064000A0000816B57004800450052004500200063006F002E005B005300740061007400750073005D0020003D0020003100200041004E004400200063006F002E004F0062006A0065006300740054007900700065004900640020003D0020003900200041004E004400200063006F002E004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0049007300460069006C006500500072006F0070006500720074006900650073004D006100740063006800280063006F002E005400690074006C0065002C00200063006F002E005B004400650073006300720069007000740069006F006E005D002C002000660069002E0041006C00740054006500780074002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012B4F005200440045005200200042005900200063006F002E005400690074006C006500200041005300430000816F53004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F007000650072007400790028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029002C0020005B004400650073006300720069007000740069006F006E005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F007000650072007400790028005B004400650073006300720069007000740069006F006E005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001825F57004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900011D550050004400410054004500200043004F00460069006C0065000A000080B3530045005400200041006C007400540065007800740020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F0070006500720074007900280041006C00740054006500780074002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001828357004800450052004500090043006F006E00740065006E00740049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200066002E0043006F006E00740065006E007400490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F00460069006C006500200041005300200066000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200066002E0043006F006E00740065006E007400490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001816F57004800450052004500200063006F002E005B005300740061007400750073005D0020003D0020003100200041004E004400200063006F002E004F0062006A0065006300740054007900700065004900640020003D00200033003300200041004E004400200063006F002E004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730049006D00610067006500500072006F0070006500720074006900650073004D006100740063006800280063006F002E005400690074006C0065002C00200063006F002E005B004400650073006300720069007000740069006F006E005D002C002000660069002E0041006C00740054006500780074002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A0001817353004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F007000650072007400790028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029002C0020005B004400650073006300720069007000740069006F006E005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F007000650072007400790028005B004400650073006300720069007000740069006F006E005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001826157004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180B5530045005400200041006C007400540065007800740020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F0070006500720074007900280041006C00740054006500780074002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001828557004800450052004500090043006F006E00740065006E00740049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200066002E0043006F006E00740065006E007400490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F00460069006C006500200041005300200066000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200066002E0043006F006E00740065006E007400490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180D5530045004C004500430054002000500064002E00500061006700650044006500660069006E006900740069006F006E00490064002000410053002000490064002C002000500064002E005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C00200050006E002E0050006100670065004D00610070004E006F006400650049006400200061007300200046006F006C00640065007200490064002C0020002700270020004100530020004C006F0063006100740069006F006E000A00018129460052004F004D002000500061006700650044006500660069006E006900740069006F006E002000500064002000430052004F005300530020004100500050004C00590020002800530045004C00450043005400200054004F00500020003100200050006E0064002E0050006100670065004D00610070004E006F0064006500490064002000660072006F006D00200050006100670065004D00610070004E006F00640065005000610067006500440065006600200050006E006400200077006800650072006500200050006E0064002E00500061006700650044006500660069006E006900740069006F006E004900640020003D002000500064002E00500061006700650044006500660069006E006900740069006F006E00490064002900200050006E000A000080B757004800450052004500200053006900740065004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730050006100670065004E0061006D0065004D0061007400630068002800500064002E005400690074006C0065002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012B4F0052004400450052002000420059002000500064002E005400690074006C0065002000410053004300002D5500500044004100540045002000500061006700650044006500660069006E006900740069006F006E000A000080A353004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640050006100670065004E0061006D00650028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A000182B3570048004500520045000900500061006700650044006500660069006E006900740069006F006E0049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200070002E00500061006700650044006500660069006E006900740069006F006E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900500061006700650044006500660069006E006900740069006F006E00200041005300200070000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0050006100670065004E0061006D00650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200070002E00500061006700650044006500660069006E006900740069006F006E00490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180D7530045004C0045004300540020006400650074002E005000610067006500490064002000410053002000490064002C0020006400650066002E005400690074006C00650020004100530020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C00200050006E002E0050006100670065004D00610070004E006F006400650049006400200041007300200046006F006C00640065007200490064002C0020002700270020004100530020004C006F0063006100740069006F006E000A000180BD460052004F004D0020005000610067006500440065007400610069006C0073002000410053002000640065007400200049004E004E004500520020004A004F0049004E002000500061006700650044006500660069006E006900740069006F006E00200041005300200064006500660020004F004E0020006400650066002E00500061006700650044006500660069006E006900740069006F006E004900640020003D0020006400650074002E005000610067006500490064000A000080FD430052004F005300530020004100500050004C00590020002800530045004C00450043005400200054004F00500020003100200050006E0064002E0050006100670065004D00610070004E006F0064006500490064002000660072006F006D00200050006100670065004D00610070004E006F00640065005000610067006500440065006600200050006E006400200077006800650072006500200050006E0064002E00500061006700650044006500660069006E006900740069006F006E004900640020003D0020006400650066002E00500061006700650044006500660069006E006900740069006F006E00490064002900200050006E000A000080DD5700480045005200450020006400650066002E0053006900740065004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00490073005000610067006500500072006F0070006500720074006900650073004D00610074006300680028006400650074002E005000610067006500440065007400610069006C0058004D004C002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012D4F00520044004500520020004200590020006400650066002E005400690074006C0065002000410053004300002755005000440041005400450020005000610067006500440065007400610069006C0073000A000080CF53004500540020005000610067006500440065007400610069006C0058006D006C0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740055007000640061007400650064005000610067006500500072006F00700065007200740069006500730028005000610067006500440065007400610069006C0058006D006C002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001827B570048004500520045000900500061006700650049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200070002E005000610067006500490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0009005000610067006500440065007400610069006C007300200041005300200070000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F005000610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200070002E005000610067006500490064000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900010D5C0077007B0033002C007D0000810B530045004C004500430054002000490064002C0020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280050006100720065006E007400490064002C00200027007B0031007D0027002900200061007300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A000180855700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D0020003700200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D0027002000013141004E00440020005B0054006500780074005D0020004C0049004B0045002000270025007B0030007D0025002700200001809941004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0049007300540065007800740043006F006E00740065006E0074004D00610074006300680028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A000180BF53004500540020005B0054006500780074005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400540065007800740043006F006E00740065006E007400540065007800740028005B0054006500780074005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D002C0020007B0034007D0029000A0001825957004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00540065007800740043006F006E00740065006E0074002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001372F00460069006E00640041006E0064005200650070006C006100630065002F00540065007800740043006F006E00740065006E00740000452F00460069006E00640041006E0064005200650070006C006100630065002F0043006F006E00740065006E00740044006500660069006E006900740069006F006E00730000332F00460069006E00640041006E0064005200650070006C006100630065002F0050006100670065004E0061006D0065007300003D2F00460069006E00640041006E0064005200650070006C006100630065002F005000610067006500500072006F007000650072007400690065007300003F2F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F007000650072007400690065007300003D2F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F00700065007200740069006500730000808353004500540020005B0054006500780074005D0020003D002000640062006F002E00520065006700650078005F005200650070006C006100630065004D0061007400630068006500730028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D002C00200027007B0032007D00270029000A0001808D5700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D0020003700200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E00440020000153640062006F002E00520065006700650078005F00490073004D00610074006300680028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D00290020003D00200031000180C7530045005400200058006D006C0053007400720069006E00670020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740055007000640061007400650064004C0069006E006B0049006E0043006F006E00740065006E00740044006500660069006E006900740069006F006E00280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D0029000A000180F35700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D00200031003300200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E00440020004300410053005400280058006D006C0053007400720069006E00670020004100530020006E00760061007200630068006100720028004D00410058002900290020004C0049004B0045002000270025007B0031007D0025002700200041004E004400200001808F640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00490073004C0069006E006B0049006E0043006F006E00740065006E00740044006500660069006E006900740069006F006E00280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C0020007B0031007D00290020003D0020003100013B2F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064002F006700750069006400000D27007B0030007D0027002C000180CD530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002C0043006F006E007400610069006E006500720058006D006C0020002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E0020005700680065007200650020002000540065006D0070006C0061007400650049006400200069006E00200028007B0030007D0029000023500061006700650044006500660069006E006900740069006F006E0058006D006C00001943006F006E007400610069006E006500720058006D006C0000272F002F00700061006700650044006500660069006E006900740069006F006E005B0031005D0000156D006F006400690066006900650064004200790000196D006F006400690066006900650064004400610074006500000375000081A93C0063006F006E007400610069006E00650072002000690064003D0022007B0030007D002200200063006F006E00740065006E007400490064003D002200300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D003000300030003000300030003000300030003000300030002200200069006E005700460043006F006E00740065006E007400490064003D002200300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D003000300030003000300030003000300030003000300030002200200063006F006E00740065006E0074005400790070006500490064003D002200300022002000690073004D006F006400690066006900650064003D002200460061006C0073006500220020006900730054007200610063006B00650064003D002200460061006C007300650022002000760069007300690062006C0065003D00220054007200750065002200200069006E0057004600560069007300690062006C0065003D002200540072007500650022002F003E000180C95500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002C0043006F006E007400610069006E006500720058006D006C0020003D004E0027007B0031007D0027002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0032007D0027000121500061006700650044006500660069006E006900740069006F006E0049006400002550006100670065004D00610070004E006F00640065005F005500700064006100740065000080C9530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002C0043006F006E007400610069006E006500720058006D006C002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E002000570068006500720065002000540065006D0070006C0061007400650049006400200069006E00200028007B0030007D00290000193C0043006F006E007400610069006E006500720073003E00001B3C002F0043006F006E007400610069006E006500720073003E00002D2F002F0063006F006E007400610069006E00650072005B004000690064003D0022007B0030007D0022005D00001B3C0043006F006E007400610069006E006500720073002F003E000080C95500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002C0043006F006E007400610069006E006500720058006D006C003D004E0027007B0031007D00270020002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0032007D0027000163530065006C00650063007400200070006100670065004D006100700058006D006C002000460052004F004D00200050006100670065004D006100700020005700680065007200650020007300690074006500490064003D0027007B0030007D002700010569006400006750006100670065004D00610070004E006F0064006500200064006F006500730020006E006F00740020006500780069007300740020006F007200200069006E00760061006C0069006400200050006100670065004D00610070004E006F00640065004900640000312F002F0070006100670065004D00610070004E006F00640065005B004000690064003D0022007B0030007D0022005D00004350006100720065006E007400200050006100670065004D00610070004E006F0064006500200064006F006500730020006E006F007400200065007800690073007400001770006100670065004D00610070004E006F00640065000071550070006400610074006500200050006100670065004D00610070002000530065007400200070006100670065004D006100700058006D006C0020003D00200027007B0030007D00270020005700680065007200650020007300690074006500490064003D0027007B0031007D0027000180D1530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E0020005700680065007200650020005300690074006500490064003D0027007B0030007D002700200061006E0064002000540065006D0070006C0061007400650049006400200069006E00200028007B0031007D002900011363006F006E007400610069006E0065007200001363006F006E00740065006E00740049006400001B69006E005700460043006F006E00740065006E00740049006400001B63006F006E00740065006E0074005400790070006500490064000015690073004D006F00640069006600690065006400000B460061006C007300650000136900730054007200610063006B006500640000809F5500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0031007D002700012F700072006F0070006F006700610074006500530065006300750072006900740079004C006500760065006C007300002B69006E0068006500720069007400530065006300750072006900740079004C006500760065006C007300001D730065006300750072006900740079004C006500760065006C007300001D700072006F0070006F00670061007400650052006F006C0065007300001969006E006800650072006900740052006F006C0065007300000B72006F006C006500730000312F002F0070006100670065004D00610070004E006F00640065005B004000690064003D0027007B0030007D0027005D00012F640065007300630065006E00640061006E0074003A003A0070006100670065004D00610070004E006F006400650000032C00001770006100670065006D00610070006E006F0064006500001D700061006700650064006500660069006E006900740069006F006E00002770006100670065006D00610070006E006F006400650077006F0072006B0066006C006F007700003B6300680069006C0064003A003A0070006100670065004D00610070004E006F00640065005B004000690064003D0022007B0030007D0022005D0000416300680069006C0064003A003A00700061006700650044006500660069006E006900740069006F006E005B004000690064003D0022007B0030007D0022005D00001D700061006700650044006500660069006E006900740069006F006E00004B6300680069006C0064003A003A0070006100670065004D00610070004E006F006400650057006F0072006B0046006C006F0077005B004000690064003D0022007B0030007D0022005D00002770006100670065004D00610070004E006F006400650057006F0072006B0046006C006F007700001363007200650061007400650064004200790000176300720065006100740065006400440061007400650000216300680069006C0064003A003A0063006F006E007400610069006E006500720000196300680069006C0064003A003A007300740079006C006500001B6300680069006C0064003A003A0073006300720069007000740000376300680069006C0064003A003A0063006F006E007400610069006E00650072005B004000690064003D0022007B0030007D0022005D00002F6300680069006C0064003A003A007300740079006C0065005B004000690064003D0022007B0030007D0022005D00000B7300740079006C00650000316300680069006C0064003A003A007300630072006900700074005B004000690064003D0022007B0030007D0022005D00000D73006300720069007000740000197000750062006C0069007300680043006F0075006E00740000177000750062006C00690073006800440061007400650000237300740061007400750073004300680061006E006700650064004400610074006500005150006100720065006E00740020004E006F006400650020002D00200050006100670065004D00610070004E006F0064006500200064006F006500730020006E006F007400200065007800690073007400013349006E00760061006C00690064002000500061006700650044006500660069006E006900740069006F006E0020004900440000372F002F00700061006700650044006500660069006E006900740069006F006E005B004000690064003D0022007B0030007D0022005D00003550006100670065004D00610070004E006F0064006500200064006F006500730020006E006F00740020006500780069007300740000356300680069006C0064003A003A0070006100670065004D00610070004E006F006400650057006F0072006B0046006C006F00770000173C0077006F0072006B0066006C006F00770073003E0000193C002F0077006F0072006B0066006C006F00770073003E0000114900740065006D005400790070006500001146006F006C006400650072004900640000114C006F0063006100740069006F006E00003343006F006E007400650078007400200043006F006E006E0065006300740069006F006E0020003D0020007400720075006500001540004F0062006A0065006300740049006400730000055C006200003928003F003A0028003F003C0021005B003C005D002E002A003F0029007C0028003F003C003D003E005B005E003C005D002A003F0029002900004728003F003C003D003C007000610067006500440065007400610069006C0073003E002E002A003F0028003F003A003C0028005C0077002B0029003E0029002E002A003F002900004328003F003D0028003F003A002E002A003F003C002F005C0031003E0029002E002A003F003C002F007000610067006500440065007400610069006C0073003E002900002149006E00760061006C006900640020004900740065006D005400790070006500004928003F003C003D003C005C0077002B002E002A003F005C0062005C0077002B003D0028005B00220027005D002900290028003F003A0028003F0021005C00310029002E0029002A0001809528003F003C003D003C0063006F006E00740065006E00740044006500660069006E006900740069006F006E005C0073002B00540065006D0070006C00610074006500490064003D0022005B005E0022005D007B00330036007D0022002E002A005C00620028003F003C00500072006F00700065007200740079003E005C0077002B0029003D00220029005B005E0022005D002A00002349006E00760061006C00690064002000520065006700650078005400790070006500007B28003F003C003D003C0061005B005C0073005D002B005B005E003E005D002A0028003F003A0068007200650066007C00720065007400750072006E00760061006C007500650029003D00280022007C0027002700290029007B0030007D0028003F003D0028003F003A005C0031007C005B002C005D0029002900017F28003F003C003D003C0061005B005C0073005D002B005B005E003E005D002A0028003F003A0068007200650066007C00720065007400750072006E00760061006C007500650029003D00280022007C0027002700290029007B0030007D0028003F003D0028003F003A0028003F0021005C00310029002E0029002A00290001032600000B260061006D0070003B00000F26007300700061006D0070003B00000D2600730070006C0074003B00000926006C0074003B0000033C0000092600670074003B0000033E000019260064006F00750062006C006500710075006F0074003B00000D2600710075006F0074003B0000032200000D2600610070006F0073003B00000F260073006C006100730068003B0000035C0000000000E3B2C6FE00B6D445952CA7B3B814974C0008B77A5C561934E0890320000105200101111505200101110804200011150306121905200101121D0520010112210E0005111511251115111511151125090003021115111511290900030E1115111511290A0003122D111511151129070002011C1011150B00040E11151115112911150700020E112511250D000502123111151135113511350F00060E1231111511151135113511350800030E1239123D0E0C00050E1239123D0E123D123D0F0006021115111511151115113511350D00050E111511151115113511350B00040211151115113511350B00040212311115113511350D00050E123111151115113511350D000502111511151135113511350F00060E111511151115113511351135090003021231111511350B00040E1231111511151135070002122D0E112522000B011C10112510112510112510112510112510112510112510112510112510112510000601123111251129101129113511250C00040112311125112910112906000208081241080003080E1145124107000208114512411300080112311125112910081008100E124111450900030811141145124106000111141249070003010E0812410500010112410B0006011145080E081241020900030811181145124109000308111C1145124109000308112011451241090003080E1D124D124108000212510E1D124D090001151255010E1241070002124912590E0A000115125501114512490900011512550108124906000111181249060001111C1249060001112012490A000115125501111812490500010E12510900010E1512550111450500010E12490600020E12490E0A000115125501114512510D000501112511151135113511351100070111251231111511151135113511350B00040111251115113511350F000601112512311115111511351135110007011125112911151115113511351135150009011125123111151115111511151135113511350B00040111251115111511350B000401114512311145115D1200070111451145123111451145115D101145070003010E0E11450B00050111450E0E1145113507000201125912610F000601126112611145115D125912610E000612650E1145115D1261125902100007011145115D12611261125912690209000301126512611259070002011261125912000701114511451145114512311011451135130008011145114512311135113511351145115D02060E0306116D0306115D03061129020608030611280401000000040200000004040000000408000000041000000004200000000306112C0400001271040001010E050002010E0E090004123D11280E0202090004123D112C0E0202060002123D0E020400010E0E0306117507000311150E0E0E12010001005408074D617853697A65FFFFFFFF042001010E0420010102062001011180B50420010108062001011180C9808501000200000006005402174973496E76617269616E74546F4475706C696361746573005402124973496E76617269616E74546F4E756C6C73015402124973496E76617269616E74546F4F726465720054020D49734E756C6C4966456D7074790154080B4D61784279746553697A65401F0000540E044E616D650B47656E6572617465586D6C062001011180D1032000020320000E06200212190E1C0307010205200112191C0620021219080E05200112190E04070111158126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730100000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D4461746141636365737301000000040001020E050002020E0E0700040E0E1C1C1C04200011450600030E0E0E0E062002010E1241062001011280DD0520010812510600030E0E1C1C0520001280F1032000080500020E0E0E0520020E0E0E0520001280FD0320001C0420011C0E0500010E1D0E042001020E0700040E0E0E0E0E0600020E0E1D1C05000111150E35071E0E0E0E0E0E1241125112510E1280DD1280E91D0E08080E0208081281010E0E1281010E11150211451280FD1280FD1D0E128105062002010E1175060703123D020205200112390E060703123D0E025701000200540E1146696C6C526F774D6574686F644E616D651852656765785F4765744D6174636865735F46696C6C526F77540E0F5461626C65446566696E6974696F6E134D61746368206E76617263686172284D41582906200112810D0E070703123D122D02170100010054020F497344657465726D696E697374696301808F010001005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A44617461416363657373010000000520001280DD052000128111082002124D0E118115042001011C05200012811D0520020E08080E0706121912411280DD12811D0E021A070C123D123D12390E123D123D1239021280FD021280FD12810503061115030612380306123D0520010E12390306123C0307010E052002011C180720020E0E128125120708123D1281251240128125123C12380E020407020E0E0907040E12812512440E050702123D02120708123D123D123D123902021280FD128105030612481007080E123D123D128125124C12480E02110708123D123D12390E021280FD0212810515070B123D123D12190812390E0E0E021280FD128105819B010003005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A4461746141636365737301000000540E1146696C6C526F774D6574686F644E616D650746696C6C526F77540E0F5461626C65446566696E6974696F6E80DC636F6C3120756E697175656964656E7469666965722C636F6C3220756E697175656964656E7469666965722C636F6C3320756E697175656964656E7469666965722C636F6C3420756E697175656964656E7469666965722C636F6C3520756E697175656964656E7469666965722C636F6C3620756E697175656964656E7469666965722C636F6C3720756E697175656964656E7469666965722C636F6C3820756E697175656964656E7469666965722C636F6C3920756E697175656964656E7469666965722C636F6C313020756E697175656964656E74696669657208000112812911812D072002010E128129052000128135062001011281310500020E0E1C0620011D0E1D030400010A0E0400010E0A0420001D1C05200012810105200201081C062001011281012D07140E1241125112511281310E0E1280DD1D0E1280E91281411281011281011D1C08122D1D03021280FD1281050420011C080406128149060001112511450306112506070212810102030611450B0707021145080E08124102080705080E08124102062001011181150807031D124D124D080A07041D124D124D124D0805151255010E092000151181550113000615118155010E0420001300080615128159020E080715128159020E08072002011300130108200202130010130105200112610805200012815D0620011281610E080002020E10118165070001118165116D09000202118165118165060002020E100805000108112927071312590E0E12491114111411181118111C11816511816511200808111815118155010E020E08061512550111450520010113001E070D111412611512550111450E114511141280FD02021D031D0E081281050520010112410620010111816D0807030E1280DD1D0E0607020E1280DD062001124D124D0500001281750F0706124D124D124D124D124D1280DD0E07061D124D124D124D124D124D081007071D124D124D124D124D124D124D080C07051D124D124D124D124D080F070712511280DD124D081D124D080216070A12510E12411280DD124D1280E912511D124D08021F070A151255010E12510E1280DD1280E9128101151255010E1280FD0212810505200112490E30071812491249124912491249124912491249124912491249124912491249124912491249124912491249124912490E0817070615125501114512611512550111451280FD02128105051512550108040001080E1507061512550108126115125501081280FD021281051107071118126111181280FD115D021281051207081118126111181280FD02115D02128105050001116D0E050001115D0E0F0706111C1261111C1280FD0212810505000111290E0500011129080F07061120126111201280FD0212810506151255011118190707151255011118126111181512550111181280FD021281050F070612191281010E1280FD0212810507151181550111450F0705121911450E02151181550111450E0706121912610E1280FD021281051807061512550111451281011512550111451280FD02128105062001126112610E0706125912610E1280FD0212810508200412190E1C1C1C07200212190E1D1C04000108020D0707123D0E1219020811451D1C06070212191D1C09070412190811451D1C07200312190E1C1C052001011271070703112812710205200112610E07070312591261020420001175080703123D12191145040701114504200012650500001181790420010E0E0520010E1D03052002010E1C072002010E12817D0520001181814307211241125912591249121912610E12510E1280DD1280E90E12590E0E0E1281011249126112610E0E0E0E1280DD12810112817D021280FD1281051181791D031280FD04200012614B07231241125912591249121912610E12510E1280DD1280E90E125912591281011249126112610E124912490812610E0E1280DD12810112817D021280FD1281051181791D031280FD1280FD0520010211451E070E12410E125912590E1280DD0E12611281611145126112817D12817D0205000111350208000211351135113505000102113509200312611181850E0E0820011281611281615007251241125912591249121912610E12510E1280DD1280E90E12591281011249126112610E1261128161124912490812610E0E1280DD12810112817D021280FD1281051181791D031280FD11451280FD042001081C042001021C082001128189128129021D0E0600020E0E1D0E070001126912818D5C073012816112816112816112816112816112816102020E02020E0E0E126112490812611281611D0E1D0E12690E0E1281611D0E1D0E12690E12611281611281610E0E1D0E1D0E12690E1281611D0E1D0E12690E128161021D031D0E08042000124908200212611261126114070B12610E0E0E12611265021280FD0E0212810505200112650E040000114505200011817920070C1265128161128161114512816112816112651280FD021281051181791D03070002021145114520070C1281610E11451281611281611281611281611280FD021281051181791D0332071A0E12490E12490E124912610E0E126112491261126512611261124912611265126112611249126112651280FD021281050400010E081307080808128161128161128161021181791D03070001115D11817923071212410E125912590E1280DD0E126111450212816112490812611269126512817D0206000111351135052002011C1C05200012819D0420011C1C3E071F12410E125912590E1280DD0E126112491261128199126102124912610E126512817D12610E1265124912610E126112611265021280FD1281051280FD072002010E118115082003010E1181150A072001011D1281A10A0703127112711D1281A10620020108114505200201080E0520020108080D070512411280DD12811D12710207070212411280DD0807040E123D0211280807040E123D02112C0607030E123D020507030E0E0204200012390B0705123D1239121911150203000001150100104657434C5353746F72656450726F637300000501000000000C010007352E322E302E3000001B0100164272696467656C696E65204469676974616C20496E6300002101001C436F7079726967687420C2A9204272696467656C696E65203230313300000A010005694150505300000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F777301000000000000007CC2325400000000020000001C0100002081010020630100525344535B358977FF1C634F902A3BCC3FF11A470B000000643A5C69415050535C446576656C6F706D656E745C6465762D5635312D54656C65766F785C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C6F626A5C44656275675C4272696467656C696E652E434C5246756E6374696F6E732E706462000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000006482010000000000000000007E82010000200000000000000000000000000000000000000000000070820100000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF25002000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058A00100900300000000000000000000900334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100020005000000000002000500000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004F0020000010053007400720069006E006700460069006C00650049006E0066006F000000CC02000001003000300030003000300034006200300000003C001100010043006F006D006D0065006E007400730000004600570043004C005300530074006F00720065006400500072006F00630073000000000050001700010043006F006D00700061006E0079004E0061006D006500000000004200720069006400670065006C0069006E00650020004400690067006900740061006C00200049006E006300000000004C0011000100460069006C0065004400650073006300720069007000740069006F006E00000000004600570043004C005300530074006F00720065006400500072006F006300730000000000300008000100460069006C006500560065007200730069006F006E000000000035002E0032002E0030002E003000000058001C00010049006E007400650072006E0061006C004E0061006D00650000004200720069006400670065006C0069006E0065002E0043004C005200460075006E006300740069006F006E0073002E0064006C006C0000005C001C0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004200720069006400670065006C0069006E00650020003200300031003300000060001C0001004F0072006900670069006E0061006C00460069006C0065006E0061006D00650000004200720069006400670065006C0069006E0065002E0043004C005200460075006E006300740069006F006E0073002E0064006C006C0000002C0006000100500072006F0064007500630074004E0061006D00650000000000690041005000500053000000340008000100500072006F006400750063007400560065007200730069006F006E00000035002E0032002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000035002E0032002E0030002E0030000000000000000000000000000000000000000000000000000000008001000C000000903200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    WITH PERMISSION_SET = UNSAFE;


GO

PRINT N'Creating [dbo].[GenerateXml]...';


GO
IF OBJECT_ID(N'[dbo].[GenerateXml]', 'AF') IS NULL
CREATE AGGREGATE [dbo].[GenerateXml](@Value NVARCHAR (4000))
    RETURNS NVARCHAR (4000)
    EXTERNAL NAME [Bridgeline.CLRFunctions].[GenerateXml];


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedImageProperty]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedImageProperty]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedImageProperty]'


GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedFileProperty]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedFileProperty]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedFileProperty]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
(@source XML, @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_GetContentLocation]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetContentLocation]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetContentLocation]
(@contentId UNIQUEIDENTIFIER, @siteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetContentLocation]
'

GO
PRINT N'Creating [dbo].[RegexSelectAll]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegexSelectAll]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[RegexSelectAll]
(@input NVARCHAR (4000), @pattern NVARCHAR (4000), @matchDelimiter NVARCHAR (4000))
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[RegexSearch].[RegexSelectAll]
'

GO
PRINT N'Creating [dbo].[Regex_ReplaceMatches]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_ReplaceMatches]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[Regex_ReplaceMatches]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (4000), @regexOptions INT, @replacement NVARCHAR (4000))
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_ReplaceMatches]
'

GO
PRINT N'Creating [dbo].[Regex_IsMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_IsMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[Regex_IsMatch]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (4000), @regexOptions INT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_IsMatch]
'

GO
PRINT N'Creating [dbo].[Regex_GetMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[Regex_GetMatch]
(@source NVARCHAR (4000), @regexPattern NVARCHAR (4000), @regexOptions INT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatch]
'

GO
PRINT N'Creating [dbo].[NavFilter_GetQuery]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NavFilter_GetQuery]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[NavFilter_GetQuery]
(@queryId UNIQUEIDENTIFIER, @condition NVARCHAR (4000), @OrderByProperty NVARCHAR (4000), @SortOrder NVARCHAR (4000), @SiteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[NavFilter_GetQuery]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsTextContentMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsTextContentMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsTextContentMatch]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsTextContentMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsPagePropertiesMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch]
(@source XML, @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPagePropertiesMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsPageNameMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPageNameMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsPageNameMatch]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPageNameMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsLinkInContentDefinition]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (4000), @matchWholeLink BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsLinkInContentDefinition]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsImagePropertiesMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsImagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch]
(@title NVARCHAR (4000), @description NVARCHAR (4000), @altText NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsImagePropertiesMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsFilePropertiesMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsFilePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch]
(@title NVARCHAR (4000), @description NVARCHAR (4000), @altText NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsFilePropertiesMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsContentDefinitionMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsContentDefinitionMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch]
(@source XML, @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsContentDefinitionMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedTextContentText]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedTextContentText]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedTextContentText]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageProperties]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageProperties]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties]
(@source XML, @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageProperties]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageName]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageName]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageName]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (4000), @newUrl NVARCHAR (4000), @matchWholeLink BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedLinkInContentDefinition]
'

GO
PRINT N'Creating [dbo].[Regex_GetMatches]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatches]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[Regex_GetMatches]
(@source NVARCHAR (4000), @regexPattern NVARCHAR (4000), @regexOptions INT)
RETURNS 
     TABLE (
        [Match] NVARCHAR (MAX) NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatches]
'

GO
PRINT N'Creating [dbo].[GetRelationTable]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRelationTable]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[GetRelationTable]
(@relationType NVARCHAR (4000), @objectId UNIQUEIDENTIFIER)
RETURNS 
     TABLE (
        [col1]  UNIQUEIDENTIFIER NULL,
        [col2]  UNIQUEIDENTIFIER NULL,
        [col3]  UNIQUEIDENTIFIER NULL,
        [col4]  UNIQUEIDENTIFIER NULL,
        [col5]  UNIQUEIDENTIFIER NULL,
        [col6]  UNIQUEIDENTIFIER NULL,
        [col7]  UNIQUEIDENTIFIER NULL,
        [col8]  UNIQUEIDENTIFIER NULL,
        [col9]  UNIQUEIDENTIFIER NULL,
        [col10] UNIQUEIDENTIFIER NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[GetRelationTable]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_Save]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Save]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[CLRPageMapNode_Save]
@siteId UNIQUEIDENTIFIER, @parentNodeId UNIQUEIDENTIFIER, @pageMapNode XML, @createdBy UNIQUEIDENTIFIER, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME, @id UNIQUEIDENTIFIER OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Save]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_RemoveContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_RemoveContainer]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer]
@templateId NVARCHAR (4000), @containerIds NVARCHAR (4000), @modifiedBy UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_RemoveContainer]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_AddContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_AddContainer]', 'PC') IS  NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[CLRPageMapNode_AddContainer]
@templateId NVARCHAR (4000), @containerIds NVARCHAR (4000), @modifiedBy UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_AddContainer]
'

GO
PRINT N'Creating [dbo].[CLRPageMap_SavePageDefinition]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMap_SavePageDefinition]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[CLRPageMap_SavePageDefinition]
@siteId UNIQUEIDENTIFIER, @parentNodeId UNIQUEIDENTIFIER, @CreatedBy UNIQUEIDENTIFIER, @ModifiedBy UNIQUEIDENTIFIER, @pageDefinitionXml XML, @Id UNIQUEIDENTIFIER OUTPUT, @publishPage BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMap_SavePageDefinition]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_UpdateLinks]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_UpdateLinks]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[FindAndReplace_UpdateLinks]
@siteId UNIQUEIDENTIFIER, @oldUrl NVARCHAR (4000), @newUrl NVARCHAR (4000), @matchWholeLink BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_UpdateLinks]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_Replace]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Replace]', 'PC') IS  NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[FindAndReplace_Replace]
@siteId UNIQUEIDENTIFIER, @objectIds XML, @searchTerm NVARCHAR (4000), @htmlEncodedSearchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @htmlEncodedReplaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Replace]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_Find]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Find]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[FindAndReplace_Find]
@siteId UNIQUEIDENTIFIER, @searchIn INT, @searchTerm NVARCHAR (4000), @htmlEncodedSearchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Find]
'

GO
PRINT N'Creating [dbo].[Contact_SearchCLR]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_SearchCLR]', 'PC') IS  NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[Contact_SearchCLR]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @MaxRecords INT, @TotalRecords INT OUTPUT, @includeAddress BIT = 1, @ContactListId Uniqueidentifier = NULL
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_SearchCLR]
'

GO
PRINT N'Creating [dbo].[Contact_GetAutoDistributionListContactCount]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_GetAutoDistributionListContactCount]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @MaxRecords INT, @TotalRecords INT OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_GetAutoDistributionListContactCount]
'

GO
PRINT N'Creating [dbo].[CLSPageMapNode_AttachWorkflow]...';


GO
IF OBJECT_ID(N'[dbo].[CLSPageMapNode_AttachWorkflow]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow]
@siteId UNIQUEIDENTIFIER, @pageMapNodeId UNIQUEIDENTIFIER, @pageMapNodeWorkFlow XML, @propogate BIT, @inherit BIT, @appendToExistingWorkflows BIT, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLSPageMapNode_AttachWorkflow]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_Update]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Update]', 'PC') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[CLRPageMapNode_Update]
@siteId UNIQUEIDENTIFIER, @pageMapNode XML, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Update]
'


PRINT N'Assembly update for Bridgeline.CLRFunctions is complete';


GO
PRINT 'Creating procedure Campaign_AttachDistributionListAndContacts'
GO

if object_id('dbo.Campaign_AttachDistributionListAndContacts', 'p') is null
    exec ('create procedure Campaign_AttachDistributionListAndContacts as select 1 a')
go

ALTER PROCEDURE [dbo].[Campaign_AttachDistributionListAndContacts](  
 @xmlSourceIds xml,@CampaignId UniqueIdentifier,@ApplicationId UniqueIdentifier  
 )  
As  
Begin  
  
  --Distribution List  
  DELETE FROM MKCampaignDistributionList WHERE CampaignId=@CampaignId  
  insert into MKCampaignDistributionList(Id,CampaignId,DistributionListId) select newid(),@CampaignId, T.col.value('text()[1]','uniqueidentifier') from @xmlSourceIds.nodes('/IdentifyRecipient/SelectedDistributionListIds/guid') T(col)   
    
   -- Contacts  
  --DELETE FROM MKCampaignUser WHERE CampaignId=@CampaignId  
  
  insert into MKCampaignUser(Id,CampaignId,UserId) select newid(),CampaignId,ContactId
  FROM 
  (select @CampaignId CampaignId, T.col.value('text()[1]','uniqueidentifier') ContactId from @xmlSourceIds.nodes('/IdentifyRecipient/SelectedContactIds/guid') T(col) 
  except 
  select CampaignId,UserId from MKCampaignUser
  ) T
    
    
End
GO
PRINT 'Updating Application id for Campaign group'
GO

UPDATE [dbo].[MKCampaignGroup]
   SET 
      ApplicationId = '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
 WHERE ApplicationId IS NULL
GO
-- Creating default email group
DECLARE @SiteId UNIQUEIDENTIFIER
Declare @CreatedBy UNIQUEIDENTIFIER
SET @CreatedBy = (select top 1 Id from USUser Where UserName like 'IAppsSystemUser')
DECLARE SITE_CURSOR__ Cursor 
FOR
SELECT Id FROM SISite Where Status = 1

Open SITE_CURSOR__ 
Fetch NEXT FROM SITE_CURSOR__ INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN
IF NOT EXISTS(Select 1 from MKCampaignGroup Where Name ='My first email group' and ApplicationId=@SiteId)
INsert into MKCampaignGroup(Id,Name,CreatedBy,CreatedDate,ApplicationId)
Select newid(),'My first email group',@createdBy,getdate(),@SiteId


Fetch NEXT FROM SITE_CURSOR__ INTO @SiteId 		
END

CLOSE SITE_CURSOR__
DEALLOCATE SITE_CURSOR__
GO
PRINT 'Altering column USSiteUser.SessionId'
GO
ALTER TABLE USSiteUser Alter Column SessionId varchar(max)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
PRINT 'Altering procedure Membership_UpdateSessionId'
GO
ALTER PROCEDURE [dbo].[Membership_UpdateSessionId]
--********************************************************************************
-- PARAMETERS
	@ApplicationId						uniqueidentifier,
    @UserId						uniqueidentifier,
	@SessionId					varchar(max)
	AS
BEGIN
	Update USSiteUser Set SessionId=@SessionId
	Where UserId = @UserId AND @ApplicationId=@ApplicationId
	
END
GO