﻿/*
Televox script.....temp....

delete from SIObjectTheme where ObjectId <> 'CD4CA724-B6ED-4DB6-8C39-E99A7464E7FE'

select * from SITheme
select * from SIObjectTheme
select * from SIThemeGroup



select * from SISite where Id='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
update SISite set ThemeId=2 where  Id='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'

*/

GO
IF (OBJECT_ID('Theme_GetThemes') ) IS NOT NULL
	DROP PROCEDURE Theme_GetThemes
GO



IF (OBJECT_ID ('SITheme','U') IS NULL)
CREATE TABLE SITheme
(
	Id UNIQUEIDENTIFIER PRIMARY KEY,
	Title nvarchar(max),
	Description nvarchar(max),
	CreatedBy Uniqueidentifier,
	CreatedDate datetime,
	ModifiedBy uniqueidentifier,
	ModifiedDate datetime,
	Status INT
)

GO
IF (OBJECT_ID ('SIObjectTheme','U') IS NULL)
CREATE TABLE SIObjectTheme
(
	--ApplicationId UNIQUEIDENTIFIER,
	Id UNIQUEIDENTIFIER PRIMARY KEY,
	Title nvarchar(max),
	ObjectId UNIQUEIDENTIFIER NOT NULL,
	ThemeId UNIQUEIDENTIFIER NOT NULL,
	FileName nvarchar(max) NULL,
	CodeFile nvarchar(max) NULL,
	ObjectTypeId INT NOT NULL,
	CreatedBy UNIQUEIDENTIFIER,
	CreatedDate datetime,
	ModifiedBy UNIQUEIDENTIFIER,
	ModifiedDate datetime,
	Status INT
	
)
GO

IF NOT EXISTS ( SELECT  name
                FROM    sys.foreign_keys
                WHERE   name = 'FK_SIObjectTheme_ThemeId_SITheme_Id' ) 
    ALTER TABLE SIObjectTheme ADD CONSTRAINT FK_SIObjectTheme_ThemeId_SITheme_Id FOREIGN KEY (ThemeId) 
                           REFERENCES SITheme(Id)

GO
--drop table SITheme
--drop table SIThemeGroup
--drop table SIObjectTheme

GO
IF (OBJECT_ID ('SIThemeGroup','U') IS NULL)
CREATE TABLE SIThemeGroup
(
	--ApplicationId UNIQUEIDENTIFIER,
	ThemeId UNIQUEIDENTIFIER,
	SourceThemeId UNIQUEIDENTIFIER,
	ObjectTypeId INT
)
GO
IF NOT EXISTS ( SELECT  name
                FROM    sys.foreign_keys
                WHERE   name = 'FK_SIThemeGroup_ThemeId_SITheme_Id' ) 
    ALTER TABLE SIObjectTheme ADD CONSTRAINT FK_SIThemeGroup_ThemeId_SITheme_Id FOREIGN KEY (ThemeId) 
                           REFERENCES SITheme(Id)

GO

IF COL_LENGTH(N'[dbo].[SITemplate]', N'IsThemed') IS NULL
	ALTER TABLE [dbo].[SITemplate] ADD [IsThemed] BIT
GO
IF COL_LENGTH(N'[dbo].[COXMLForm]', N'IsThemed') IS NULL
	ALTER TABLE [dbo].[COXMLForm] ADD [IsThemed] BIT
GO
IF COL_LENGTH(N'[dbo].[COXMLFormXslt]', N'IsThemed') IS NULL
	ALTER TABLE [dbo].[COXMLFormXslt] ADD [IsThemed] BIT
GO
IF COL_LENGTH(N'[dbo].[SIStyle]', N'IsThemed') IS NULL
	ALTER TABLE [dbo].[SIStyle] ADD [IsThemed] BIT
GO
IF COL_LENGTH(N'[dbo].[SIScript]', N'IsThemed') IS NULL
	ALTER TABLE [dbo].[SIScript] ADD [IsThemed] BIT
GO
IF COL_LENGTH(N'[dbo].[SISite]', N'ThemeId') IS NULL
	ALTER TABLE [dbo].[SISite] ADD [ThemeId] UNIQUEIDENTIFIER NULL
GO
IF (COL_LENGTH(N'[dbo].[SISite]', N'DefaultToMasterSiteTags') IS NULL)
BEGIN
	ALTER TABLE SISite Add [DefaultToMasterSiteTags]	bit CONSTRAINT [DF__SISite__DefaultToMasterSiteTags] DEFAULT ((0)) NOT NULL

	--Adding this update here ensures that it runs only once at the time of adding the new column.

END
GO



/* Default Theme of Televox was inserted, deleting the same. */
IF NOT EXISTS(Select 1 from SIObjectTheme)
	DELETE FROM SITheme 
GO

/*
EXEC Theme_GetThemes 2,NULl,'9FE7C1D1-57CC-4269-A866-0904359BC22B'
EXEC Theme_GetThemes 2
*/
CREATE PROCEDURE Theme_GetThemes
(
@ThemeId UNIQUEIDENTIFIER = NULL,
@TemplateId UNIQUEIDENTIFIER = NULL
)
AS
BEGIN
 DECLARE @DeleteStatus int  
 SET @DeleteStatus = dbo.GetDeleteStatus()  
 
IF(@ThemeId IS NULL)
	SELECT Id,
	Title,
	Description,
	CreatedBy,
	CreatedDate,
	ModifiedBy,
	ModifiedDate, 
	Status 
	FROM SITheme	
	WHERE Status <> @DeleteStatus 
	ORDER BY Title
ELSE
	--Resolve Extended Theme
	--IF ((SELECT COUNT(1) FROM SIThemeGroup WHERE ThemeId=@ThemeId ) > 0)
	--BEGIN
	
	--resolving indirect theme mapping
			SELECT DISTINCT
			OT.ObjectId, 
			Ot.ObjectTypeId, 
			OTG.ThemeId,
			OT.FileName, 
			OT.CodeFile 
		FROM SIObjectTheme OT JOIN SIThemeGroup OTG ON OTG.ThemeId=@ThemeId AND OT.ThemeId=OTG.SourceThemeId AND OTG.ObjectTypeId=OT.ObjectTypeId
		--WHERE Id IN (SELECT Id FROM SIThemeGroup OTG WHERE OTG.ThemeId=@ThemeId) AND (ObjectId = ISNULL(@TemplateId,ObjectId) OR ObjectTypeId <> 3) 
		
		UNION
		--Resolving direct theme mapping
		SELECT DISTINCT
			ObjectId, 
			ObjectTypeId, 
			ThemeId,
			FileName, 
			CodeFile 
		FROM SIObjectTheme
		WHERE ThemeId=@ThemeId --AND (ObjectId = ISNULL(@TemplateId,ObjectId) OR ObjectTypeId <> 3)
	--END
	--ELSE
	--BEGIN
	--	SELECT 
	--		ObjectId, 
	--		ObjectTypeId, 
	--		ThemeId,
	--		FileName, 
	--		CodeFile 
	--	FROM SIObjectTheme
	--	WHERE ThemeId=@ThemeId
	--END
	
END
GO
ALTER PROCEDURE [dbo].[Script_GetScript]
(
	@Id				uniqueIdentifier=null ,
	@ApplicationId	uniqueidentifier=null,
	@Title			nvarchar(256)=null,
	@FileName		nvarchar(4000)=null,
	@Status			int=null,
	@TemplateId		uniqueidentifier=null,
	@PageIndex		int=1,
	@PageSize		int=null ,
	 @ThemeId uniqueidentifier= null,
	@ResolveThemeId bit = null     
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier
BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
 IF (@ResolveThemeId IS NULL)
	Set  @ResolveThemeId  = 0  
	If @TemplateId IS NULL
	BEGIN
		IF @PageSize is Null
		BEGIN
		
		 IF (@Id IS NULL)  
			BEGIN 
				IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)
					SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 35)   
					
			IF NOT EXISTS(Select 1 FROM SIObjectTheme where ObjectId = @Id and ThemeId = @ThemeId)
			BEGIN
				SET @ThemeId = NULL
			END

			SELECT DISTINCT S.Id, 
				CASE WHEN @ThemeId IS NULL THEN S.Title ELSE OT.Title END AS Title,
				S.Description,--CASE WHEN @ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGuid() THEN S.Description ELSE (SELECT TOP 1 OT.Description FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Description,
				S.Status,  
				S.CreatedBy, 
				S.CreatedDate, 
				S.ModifiedBy, 
				S.ModifiedDate,
				CASE WHEN @ThemeId IS NULL THEN S.FileName ELSE OT.FileName END AS FileName, 
				S.ApplicationId,  
				S.GlobalProperty,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				S.OrderNo,
				[dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath,
				H.ParentId,
				@ThemeId
			FROM SIScript S
			LEFT JOIN SIObjectTheme OT ON OT.ObjectId = S.Id AND OT.ObjectTypeId = 35
				INNER JOIN HSStructure H ON S.Id = H.Id
				LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			WHERE	S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
				(@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetAncestorSites(@ApplicationId))) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
				AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)
			Order by OrderNo
		END	
		ELSE
		BEGIN	
			SELECT DISTINCT S.Id, 
				CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGuid() THEN S.Title ELSE (SELECT TOP 1 OT.Title FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Title,
				S.Description,--CASE WHEN @ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGuid() THEN S.Description ELSE (SELECT TOP 1 OT.Description FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Description,
				CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGuid() THEN S.Status ELSE (SELECT TOP 1 OT.Status FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Status,  
				S.CreatedBy, 
				S.CreatedDate, 
				S.ModifiedBy, 
				S.ModifiedDate,
				CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGuid() THEN S.FileName ELSE (SELECT TOP 1 OT.FileName FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS FileName,  
				S.ApplicationId,  
				S.GlobalProperty,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				S.OrderNo,
				[dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath,
				H.ParentId,
				(SELECT TOP 1 ThemeId FROM SIObjectTheme WHERE ObjectId=@Id) AS ThemeId
			FROM SIScript S
				INNER JOIN HSStructure H ON S.Id = H.Id
				LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			WHERE	S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
				(@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
			Order by OrderNo
		END
		END
		ELSE
		BEGIN
			WITH ResultEntries AS ( 
				SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
					Id, 
					Title, 
					Description, 
					Status, 
					CreatedBy, 
					CreatedDate, 
					ModifiedBy, 
					ModifiedDate, 
					FileName, 
					ApplicationId, 
					GlobalProperty,
					S.OrderNo,
					[dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath
				FROM SIScript S
				WHERE S.Id = isnull(@Id, S.Id) and
					isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
					isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
					(@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and
					S.Status = isnull(@Status, S.Status)and 
					(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

				SELECT A.Id, 
					A.Title, 
					A.Description, 
					A.Status, 
					A.CreatedBy, 
					A.CreatedDate, 
					A.ModifiedBy, 
					A.ModifiedDate,
					A.FileName,
					A.ApplicationId,  
					A.GlobalProperty,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName,
					A.OrderNo,
					A.ParentVirtualPath,
					H.ParentId,
					NULL AS ThemeId --This has to be added with specific themeid if we need information;passing null to improve performance when we get all the scripts
				FROM ResultEntries A
					INNER JOIN HSStructure H ON H.Id = A.Id
					LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
				WHERE Row between 
					(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize
				Order by OrderNo

		END
	END
ELSE
BEGIN
		IF @PageSize is Null
		BEGIN
			SELECT S.Id, 
				S.Title, 
				S.Description, 
				S.Status, 
				S.CreatedBy, 
				S.CreatedDate, 
				S.ModifiedBy, 
				S.ModifiedDate,
				S.FileName,
				S.ApplicationId,  
				S.GlobalProperty,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				S.OrderNo,
				[dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath,
				H.ParentId
			FROM SIScript S 
				INNER JOIN HSStructure H ON H.Id = S.Id
				INNER JOIN SITemplateScript T ON (T.ScriptId = S.Id AND T.TemplateId = @TemplateId)
				LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			WHERE	S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
				(@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
			Order by S.OrderNo
		END
		ELSE
		BEGIN
			WITH ResultEntries AS ( 
				SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row,
					Id, 
					Title, 
					Description, 
					Status, 
					CreatedBy, 
					CreatedDate, 
					ModifiedBy, 
					ModifiedDate, 
					FileName, 
					ApplicationId, 
					GlobalProperty,
					S.OrderNo,
					[dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath
				FROM SIScript S 
					INNER JOIN SITemplateScript T ON (T.ScriptId = S.Id AND T.TemplateId = @TemplateId)
				WHERE S.Id = isnull(@Id, S.Id) and
					isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
					isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
					(@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and
					S.Status = isnull(@Status, S.Status)and 
					(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

			SELECT A.Id, 
				A.Title, 
				A.Description, 
				A.Status, 
				A.CreatedBy, 
				A.CreatedDate, 
				A.ModifiedBy, 
				A.ModifiedDate,
				A.FileName,
				A.ApplicationId,  
				A.GlobalProperty,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				A.ParentVirtualPath,
				H.ParentId
			FROM ResultEntries A
				INNER JOIN HSStructure H ON A.Id = H.Id
				LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
			WHERE Row between 
				(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize
			Order by OrderNo

		END
	END
END
GO
ALTER PROCEDURE [dbo].[Script_Save]
(
 @Id    uniqueidentifier =null OUT,
 @ApplicationId uniqueidentifier=null,
 @Title   nvarchar(256)=null,
 @Description   nvarchar(1024)=null,
 @ModifiedBy    uniqueidentifier,
 @ModifiedDate  datetime,
 @FileName  nvarchar(4000)=null,
 @Type   int,
 @GlobalProperty int,
 @Status   int =null,
 @OrderNo  int = null,
 @ParentId  uniqueidentifier = null ,
 @ThemeId UNIQUEIDENTIFIER
)
AS
DECLARE @reccount int, 
  @error   int,
  @Now   datetime,
  @stmt   varchar(256),
  @rowcount int
BEGIN
 IF (@Status=dbo.GetDeleteStatus())
 BEGIN
  RAISERROR('INVALID||Status', 16, 1)
  RETURN dbo.GetBusinessRuleErrorCode()
 END

 IF(@Id IS NOT NULL AND (SELECT count(*) FROM  SIScript WHERE Id = @Id AND Status != dbo.GetDeleteStatus() AND ApplicationId in (SELECT * FROM dbo.GetVariantSites(@ApplicationId))) = 0)
 BEGIN
  RAISERROR('NOTEXISTS||Id', 16, 1)
  RETURN dbo.GetBusinessRuleErrorCode()
 END

 SET @Now = getdate()
 IF (@Status is null OR @Status =0)
 BEGIN
  SET @Status = dbo.GetActiveStatus()
 END

 IF (@Id is null)   -- new INSERT
 BEGIN
  SET @OrderNo = (SELECT isnull(MAX(OrderNo)+1,1) FROM SIScript Where 
   ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId)))
   
  SET @stmt = 'SCRIPT INSERT'
  SET @Id = newid();
  INSERT INTO SIScript  (
   Id, 
   ApplicationId,
   Title,
   Description,
   CreatedBy,
   CreatedDate,
   Status,
   FileName,
   Type,
   GlobalProperty,
   OrderNo,
   IsThemed) 
  VALUES (
   @Id,
   @ApplicationId,
   @Title,
   @Description,
   @ModifiedBy,
   @Now,
   @Status,
   @FileName,
   @Type,
   @GlobalProperty,
   @OrderNo,
   CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END 
	 )

	 ----If the script is themed, then add an entry into SIObjectTheme
  -- IF(@ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGUID())
  -- BEGIN
		--INSERT INTO SIObjectTheme (ObjectId,ThemeId,ObjectTypeId,FileName,CodeFile)
		--VALUES (@Id, @ThemeId, 9, @FileName, NULL)
  -- END

  SELECT @error = @@error
  IF @error <> 0
  BEGIN
   RAISERROR('DBERROR||%s',16,1,@stmt)
   RETURN dbo.GetDataBaseErrorCode() 
  END
    END
    ELSE   -- update
 BEGIN
  SET @stmt = 'SCRIPT UPDATE'
  SET @stmt = 'TEMPLATE UPDATE'
IF ( @ThemeId = dbo.GetEmptyGUID())
BEGIN
	  UPDATE SIScript  WITH (rowlock) SET 
	   ApplicationId=@ApplicationId,
	   Title =@Title,
	   Description=@Description,
	   ModifiedBy=@ModifiedBy,
	   ModifiedDate=@Now,
	   FileName= @FileName,
	   Type = @Type,
	   GlobalProperty = @GlobalProperty  ,
	   Status =@Status,
	   OrderNo = @OrderNo,
	   IsThemed = CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END
	  WHERE  Id    = @Id 
END
ELSE
BEGIN
	IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
	BEGIN
		UPDATE SIObjectTheme 
		SET ModifiedBy=@ModifiedBy,
	   ModifiedDate=@Now,
	   FileName= @FileName,
	   Status =@Status
	   WHERE ObjectId=@Id AND ThemeId=@ThemeId
	END
	ELSE
	BEGIN
	
		IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
		BEGIN
			UPDATE SIObjectTheme SET FileName=@FileName,Title=@Title,ModifiedBy=@ModifiedBy,ModifiedDate=@Now WHERE ObjectId=@Id AND ThemeId=@ThemeId
		END
		ELSE
		BEGIN
			INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
				VALUES (NEWID(),@Id,@ThemeId,@FileName,NULL,35,@ModifiedBy,@Now,NULL,NULL,1,@Title) -- 3 for template
		END		
		UPDATE SIScript SET IsThemed=1 WHERE Id=@Id
	END	
END

  SELECT @error = @@error, @rowcount = @@rowcount

  IF @error <> 0
  BEGIN
   RAISERROR('DBERROR||%s',16,1,@stmt)
   RETURN dbo.GetDataBaseErrorCode()
  END

  IF @rowcount = 0
  BEGIN
   RAISERROR('CONCURRENCYERROR',16,1)
   RETURN dbo.GetDataConcurrencyErrorCode()   -- concurrency error
  END 
 END
END

GO
ALTER PROCEDURE [dbo].[Site_GetSite]  
(  
	@Id				uniqueIdentifier = null ,  
	@Title			nvarchar(256) = null,  
	@ParentSiteId	uniqueIdentifier = null,  
	@Status			int = null,  
	@PageIndex		int,  
	@PageSize		int,
	@ForceAllSites	bit = null,
	@MasterSiteId   uniqueIdentifier = null,
	@SiteGroupId	uniqueIdentifier = null
 )  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	IF @PageSize is Null  
	BEGIN  
		SELECT S.Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId, 
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName, 
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			NotifyOnTranslationError, 
			TranslationEnabled, 
			TranslatePagePropertiesByDefault, 
			AutoImportTranslatedSubmissions, 
			DefaultTranslateToLanguage,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
			MasterSiteId,
			CustomAttribute,
			ThemeId
		FROM SISite S  
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id
		WHERE 
			(@Id IS NULL OR S.Id = @Id) AND
			(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
			(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
			(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
			(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus)
			
	END  
	ELSE  
	BEGIN  
		WITH ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
				S.Id, 
				Title, 
				Description, 
				Status, 
				CreatedBy, 
				CreatedDate, 
				ModifiedBy,   
				ModifiedDate, 
				URL,
				Type,
				ParentSiteId,
				Keywords,
				VirtualPath,
				HostingPackageInstanceId, 
				LicenseLevel, 
				SendNotification, 
				SubmitToTranslation,
				ImportAllAdminPermission,
				AllowMasterWebSiteUser,
				Prefix, 
				UICulture, 
				LoginURL, 
				DefaultURL,
				NotifyOnTranslationError, 
				TranslationEnabled, 
				TranslatePagePropertiesByDefault, 
				AutoImportTranslatedSubmissions, 
				DefaultTranslateToLanguage,
				PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteURL,
				dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
				MasterSiteId,
				CustomAttribute,
				ThemeId
			FROM SISite S
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id 
			WHERE 
				(@Id IS NULL OR S.Id = @Id) AND
				(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
				(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
				(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
				(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
				(@Status IS NULL OR S.Status = @Status) AND
				(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus) 
		)
		
		SELECT 
			Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId,  
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			SitePath,
			MasterSiteId,
			CustomAttribute,
			ThemeId
		FROM ResultEntries  A  
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize  
	END  
END
GO
ALTER PROCEDURE [dbo].[Site_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@URL				xml,
	@VirtualPath		nvarchar(4000)=null,
	@ParentSiteId		uniqueidentifier=null,
	@SiteType 	        int=null,
	@Keywords	        nvarchar(4000)=null,
	@Status				int=null,
	@SendNotification	bit=null,
	@SubmitToTranslation bit=null,
	@AllowMasterWebSiteUser bit=null,
	@ImportAllAdminPermission bit=null,
	@Prefix			nvarchar(5) = null, 
	@UICulture			nvarchar(10) = null,
	@LoginUrl		nvarchar(max) =null,
	@DefaultURL		nvarchar(max) =null,
	@NotifyOnTranslationError bit = null,
	@TranslationEnabled bit = null,
	@TranslatePagePropertiesByDefault bit = null,
	@AutoImportTranslatedSubmissions bit = null,
	@DefaultTranslateToLanguage nvarchar(10) = null,
	@PageImportOptions int =null,
	@ImportContentStructure bit =null,
	@ImportFileStructure bit=null,
	@ImportImageStructure bit=null,
	@ImportFormsStructure bit=null,
	@SiteGroups nvarchar(4000) = null,
	@MasterSiteId		uniqueidentifier=null,
	@PrimarySiteUrl nvarchar(max) =null,
	@CustomAttribute xml = null,
	@ThemeId UNIQUEIDENTIFIER = NULL
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
Declare @temp table(url1 nvarchar(max))
Declare @displayTitle varchar(512), @pageMapNodeDescription varchar(512), @pageMapNodeXml xml, @pageMapNodeId uniqueidentifier
Declare @LftValue int,@RgtValue int
Begin
--********************************************************************************
-- code
--********************************************************************************
	
	SET @displayTitle = @Title
	SET @pageMapNodeDescription = @Title

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  SISite where  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end

	set @Now = getdate()
	--set @Status= dbo.GetActiveStatus()



	if (@Id is null)			-- new insert
	begin
	
	IF (@ParentSiteId is not null AND @ParentSiteId <> '00000000-0000-0000-0000-000000000000' )  
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue
		from SISite
		Where Id=@ParentSiteId and MasterSiteId=@MasterSiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END

		Update SISite Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
								RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 


		set @stmt = 'Site Insert'
		set @Id = newid();
		IF @MasterSiteId IS NULL OR @MasterSiteId = dbo.GetEmptyGUID()
			SET @MasterSiteId = @Id

		insert into SISite  (
					   Id,	
					   Title,
                       Description,
                       CreatedBy,
                       CreatedDate,
					   Status,
                       URL,
                       VirtualPath,
					   ParentSiteId,
					   Type,
                       Keywords,
                       HostingPackageInstanceId,
                       SendNotification,
                       SubmitToTranslation,
                       AllowMasterWebSiteUser,
                       ImportAllAdminPermission,
                       Prefix, 
                       UICulture, 
                       LoginUrl, 
                       DefaultURL,
                       NotifyOnTranslationError,
                       TranslationEnabled,
                       TranslatePagePropertiesByDefault,
                       AutoImportTranslatedSubmissions,
                       DefaultTranslateToLanguage,
                       PageImportOptions,
                       ImportContentStructure,
                       ImportFileStructure,
                       ImportImageStructure,
                       ImportFormsStructure,
                       MasterSiteId,
                       PrimarySiteUrl,
                       LftValue,
                       RgtValue,
                       CustomAttribute,
					   ThemeId) 
					values (
						@Id,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@URL,
						@VirtualPath,
						@ParentSiteId,
						@SiteType,
						@Keywords,
						0,
						@SendNotification,
						@SubmitToTranslation,
						@AllowMasterWebSiteUser,
						@ImportAllAdminPermission, 
						@Prefix, 
						@UICulture, 
						@LoginUrl, 
						@DefaultURL,
						@NotifyOnTranslationError,
						@TranslationEnabled,
						@TranslatePagePropertiesByDefault,
						@AutoImportTranslatedSubmissions,
						@DefaultTranslateToLanguage,
						@PageImportOptions,
						@ImportContentStructure,
						@ImportFileStructure,
						@ImportImageStructure,
						@ImportFormsStructure,
						@MasterSiteId,
						@PrimarySiteUrl,
						@RgtValue,
						@RgtValue + 1,
						@CustomAttribute,
						@ThemeId)

		-- Inserting in SiteGroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')


			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
         end
         else			-- update
         begin
			set @stmt = 'Site Update'
			
			Update HSStructure Set Title=@Title Where Id=@Id
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			--SELECT @pageMapNodeXml =  cast((PageMapNodexml + '</pageMapNode>') as xml), @pageMapNodeId = PageMapNodeId FROM PageMapNode Where SiteId = PageMapNodeId And SiteId = @Id								

			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@displayTitle)[1]
			--				  with
			--				(sql:variable("@displayTitle"))')
			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@description)[1]
			--				  with
			--				(sql:variable("@pageMapNodeDescription"))')			
			
			Update PageMapNode set displaytitle=@Title
			Where PageMapNodeId = @pageMapNodeId ANd SiteId = @Id
			-- Here we are not updating the friendly url
						
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			set @rowcount= 0
			
			select * from SISite where Id=@Id
			update	SISite  with (rowlock)	set 
            Title =@Title,
            Description=@Description,
            ModifiedBy=@ModifiedBy,
            ModifiedDate=@Now,
            Status=@Status,
            URL=@URL,
            VirtualPath=@VirtualPath,
            ParentSiteId=@ParentSiteId,
            Type=@SiteType,
            Keywords= @Keywords,
            SendNotification = @SendNotification,
            SubmitToTranslation = @SubmitToTranslation,
            AllowMasterWebSiteUser=@AllowMasterWebSiteUser,
			ImportAllAdminPermission=@ImportAllAdminPermission,
			Prefix = @Prefix, 
			UICulture = @UICulture,
			LoginUrl = @LoginUrl, 
			DefaultURL =@DefaultURL,
			NotifyOnTranslationError = @NotifyOnTranslationError,
			TranslationEnabled = @TranslationEnabled,
			TranslatePagePropertiesByDefault = @TranslatePagePropertiesByDefault,
			AutoImportTranslatedSubmissions = @AutoImportTranslatedSubmissions,
			DefaultTranslateToLanguage = @DefaultTranslateToLanguage,	
			PageImportOptions =	@PageImportOptions,
			ImportContentStructure=@ImportContentStructure,
			ImportFileStructure=@ImportFileStructure,
			ImportImageStructure=@ImportImageStructure,
			ImportFormsStructure=@ImportFormsStructure,
			MasterSiteId = @MasterSiteId,
			PrimarySiteUrl = @PrimarySiteUrl,
			CustomAttribute = @CustomAttribute,
			ThemeId = @ThemeId
 	where 	Id    = @Id --and
	--isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
	
	select @error = @@error, @rowcount = @@rowcount
	
			--Deleting from SiteGroups
			DELETE FROM SISiteGroup WHERE SiteId = @id
			-- Insert into sitegroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')
		
			
	

if @error <> 0
  begin
      raiserror('DBERROR||%s',16,1,@stmt)
      return dbo.GetDataBaseErrorCode()
   end
if @rowcount = 0
  begin
    raiserror('CONCURRENCYERROR',16,1)
     return dbo.GetDataConcurrencyErrorCode()			-- concurrency error
end	
end
End
GO
ALTER PROCEDURE [dbo].[Style_GetStyle]    
(    
 @Id    uniqueIdentifier=null ,    
 @ApplicationId uniqueidentifier=null,    
 @Title   nvarchar(256)=null,    
 @FileName  nvarchar(4000)=null,    
 @Status   int=null,    
 @TemplateId  uniqueidentifier=null,    
 @PageIndex  int=1,    
 @PageSize  int=null,  
 @ThemeId uniqueidentifier= null,
 @ResolveThemeId bit = null     
 )    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
DECLARE @EmptyGUID uniqueidentifier    
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
 SET @EmptyGUID = dbo.GetEmptyGUID()    
 IF (@ResolveThemeId IS NULL)
	Set  @ResolveThemeId  = 0  
 If @TemplateId IS NULL    
 BEGIN    
  IF @PageSize is Null    
  BEGIN      
    
   IF (@Id IS NULL)    
   BEGIN 
	IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)
		SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 4)    

		IF NOT EXISTS(Select 1 FROM SIObjectTheme where ObjectId = @Id and ThemeId = @ThemeId)
		BEGIN
			SET @ThemeId = NULL
		END

    --Print @ThemeId 
   SELECT S.Id,   
   CASE WHEN @ThemeId IS NULL THEN S.Title ELSE OT.Title END AS Title,
    S.Description,
    S.Status,
    S.CreatedBy,   
    S.CreatedDate,   
    S.ModifiedBy,   
    S.ModifiedDate,    
    CASE WHEN @ThemeId IS NULL THEN S.FileName ELSE OT.FileName END AS FileName, 
    S.ApplicationId,   
    S.Media,   
    S.GlobalProperty,    
    FN.UserFullName CreatedByFullName,  
    MN.UserFullName ModifiedByFullName,  
    S.ConditionalCss,    
    [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath,   
    S.OrderNo,  
    H.ParentId,
    @ThemeId
   FROM SIStyle S    
   LEFT JOIN SIObjectTheme OT ON OT.ObjectId = S.Id AND OT.ObjectTypeId = 4
    INNER JOIN HSStructure H ON S.Id = H.Id  
    LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
    LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy  
   WHERE S.Id = isnull(@Id, S.Id) and    
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
    (@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetAncestorSites(@ApplicationId))) and  
    S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  
    AND (@ThemeId IS NULL OR OT.ThemeId = @ThemeId)
   ORDER BY OrderNo ASC  
  END
  ELSE
  BEGIN
		  SELECT S.Id,   
   CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGuid() THEN S.Title ELSE (SELECT TOP 1 OT.Title FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Title,  
    S.Description,--CASE WHEN @ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGuid() THEN S.Description ELSE (SELECT TOP 1 OT.Description FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Description,  
    CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGuid() THEN S.Status ELSE (SELECT TOP 1 OT.Status FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS Status,   
    S.CreatedBy,   
    S.CreatedDate,   
    S.ModifiedBy,   
    S.ModifiedDate,    
    CASE WHEN @ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGuid() THEN S.FileName ELSE (SELECT TOP 1 OT.FileName FROM SIObjectTheme OT WHERE ObjectId=@Id AND ThemeId=@ThemeId) END AS FileName,  
    S.ApplicationId,   
    S.Media,   
    S.GlobalProperty,    
    FN.UserFullName CreatedByFullName,  
    MN.UserFullName ModifiedByFullName,  
    S.ConditionalCss,    
    [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath,   
    S.OrderNo,  
    H.ParentId,  
    (SELECT TOP 1 ThemeId FROM SIObjectTheme WHERE ObjectId=@Id) AS ThemeId  
   FROM SIStyle S    
    INNER JOIN HSStructure H ON S.Id = H.Id  
    LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
    LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy  
   WHERE S.Id = isnull(@Id, S.Id) and    
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
    (@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and  
    S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  
   ORDER BY OrderNo ASC  
  END
  
   
  END    
  ELSE    
  BEGIN    
   WITH ResultEntries AS (     
    SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row,   
    Id,   
    Title,   
    Description,   
    Status,   
    CreatedBy,   
    CreatedDate,   
    ModifiedBy,     
    ModifiedDate,   
    FileName,   
    ApplicationId,   
    Media,   
    GlobalProperty,    
    ConditionalCss,    
    [dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath,   
    S.OrderNo   
   FROM SIStyle S    
   WHERE S.Id = isnull(@Id, S.Id) and    
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
    (@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and  
    S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))    
     
   SELECT A.Id,   
    A.Title,   
    A.Description,   
    A.Status,   
    A.CreatedBy,   
    A.CreatedDate,   
    A.ModifiedBy,   
    A.ModifiedDate,    
    A.FileName,  
    A.ApplicationId,   
    A.Media,   
    A.GlobalProperty,    
    FN.UserFullName CreatedByFullName,  
    MN.UserFullName ModifiedByFullName,  
    A.ConditionalCss,  
    A.ParentVirtualPath,   
    A.OrderNo,  
    H.ParentId,  
    NULL AS ThemeId --This has to be added with specific themeid if we need information  
   FROM ResultEntries A  
    INNER JOIN HSStructure H ON A.Id = H.Id  
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
    LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy    
   WHERE Row between     
    (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize    
     
  END    
 END    
 ELSE    
 BEGIN    
 IF @PageSize is Null    
 BEGIN    
  SELECT S.Id,   
   S.Title,   
   S.Description,   
   S.Status,   
   S.CreatedBy,   
   S.CreatedDate,   
   S.ModifiedBy,   
   S.ModifiedDate,    
   S.FileName,  
   S.ApplicationId,   
   S.Media,   
   S.GlobalProperty,    
   FN.UserFullName CreatedByFullName,  
   MN.UserFullName ModifiedByFullName,  
   T.TemplateId,  
   S.ConditionalCss,    
   [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath,   
   S.OrderNo,  
   H.ParentId  
  FROM SIStyle S   
   INNER JOIN SITemplateStyle T ON (T.StyleId = S.Id AND T.TemplateId = @TemplateId)    
   INNER JOIN HSStructure H ON S.Id = H.Id  
   LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
   LEFT JOIN VW_UserFullName MN on MN.UserId = s.ModifiedBy  
  WHERE S.Id = isnull(@Id, S.Id) and    
     isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
     isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
     (@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and  
     S.Status = isnull(@Status, S.Status)and     
     (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())    
 END    
 ELSE    
 BEGIN    
  WITH ResultEntries AS (     
   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row,   
    Id,   
    Title,   
    Description,   
    Status,   
    CreatedBy,   
    CreatedDate,   
    ModifiedBy,     
    ModifiedDate,   
    FileName,   
    ApplicationId,   
    Media,   
    GlobalProperty,    
    ConditionalCss,    
    [dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath  ,   
    S.OrderNo   
   FROM SIStyle S   
    INNER JOIN SITemplateStyle T ON (T.StyleId = S.Id AND T.TemplateId = @TemplateId)    
   WHERE S.Id = isnull(@Id, S.Id) and    
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
    (@ApplicationId is null or S.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and  
    S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  
  )    
    
  SELECT A.Id,   
   A.Title,   
   A.Description,   
   A.Status,   
   A.CreatedBy,   
   A.CreatedDate,   
   A.ModifiedBy,   
   A.ModifiedDate,    
   A.FileName,  
   A.ApplicationId,   
   A.Media,   
   A.GlobalProperty,    
   FN.UserFullName CreatedByFullName,  
   MN.UserFullName ModifiedByFullName,  
   A.ConditionalCss,  
   A.ParentVirtualPath,   
   OrderNo,  
   H.ParentId  
  FROM ResultEntries  A  
   INNER JOIN HSStructure H ON A.Id = H.Id  
   LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
   LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy   
  WHERE Row between     
   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize    
    
  END    
 END    
END  
GO
ALTER PROCEDURE [dbo].[Style_Save]  
(  
 @Id    uniqueidentifier =null OUT,  
 @ApplicationId uniqueidentifier=null,  
 @Title   nvarchar(256)=null,  
 @Description   nvarchar(1024)=null,  
 @ModifiedBy    uniqueidentifier,  
 @ModifiedDate  datetime,  
 @FileName  nvarchar(4000)=null,  
 @Media   int,  
 @GlobalProperty int,  
 @Status   int =null,  
 @ConditionalCss nvarchar(100) = null,   
 @OrderNo  int = null,  
 @ParentId  uniqueidentifier = null   ,
 @ThemeId UNIQUEIDENTIFIER
)  
AS  
DECLARE @reccount int,   
 @error   int,  
 @Now   datetime,  
 @stmt   varchar(256),  
 @rowcount int  
BEGIN  
 IF (@Status=dbo.GetDeleteStatus())  
 BEGIN  
  RAISERROR('INVALID||Status', 16, 1)  
  RETURN dbo.GetBusinessRuleErrorCode()  
 END  
   
 IF(@Id is not null AND (SELECT count(*) FROM  SIStyle WHERE Id = @Id AND Status != dbo.GetDeleteStatus() AND 
  ApplicationId in (SELECT * FROM dbo.GetVariantSites(@ApplicationId))) = 0)  
 BEGIN  
  RAISERROR('NOTEXISTS||Id', 16, 1)  
  RETURN dbo.GetBusinessRuleErrorCode()  
 END  

 SET @Now = getdate()  
 IF (@Status is null OR @Status =0)  
 BEGIN  
 SET @Status = dbo.GetActiveStatus()  
 END  
   
 IF (@Id is null)   -- new INSERT  
 BEGIN  
  SET @OrderNo = (SELECT isnull(MAX(OrderNo)+1,1) FROM SIStyle Where 
   ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId)))
  
  SET @stmt = 'STYLE INSERT'  
  SET @Id = newid() 
  INSERT INTO SIStyle  (  
   Id,   
   ApplicationId,  
   Title,  
   Description,  
   CreatedBy,  
   CreatedDate,  
   Status,  
   FileName,  
   Media,  
   GlobalProperty,
   ConditionalCss,
   OrderNo,
   IsThemed)   
   VALUES (  
   @Id,  
   @ApplicationId,  
   @Title,  
   @Description,  
   @ModifiedBy,  
   @Now,  
   @Status,  
   @FileName,  
   @Media,  
   @GlobalProperty,
   @ConditionalCss,
   @OrderNo,
   CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END 
   
   )  


  -- --If the style is themed, then add an entry into SIObjectTheme
  -- IF(@ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGUID())
  -- BEGIN
		--INSERT INTO SIObjectTheme (ObjectId,ThemeId,ObjectTypeId,FileName,CodeFile)
		--VALUES (@Id, @ThemeId, 4, @FileName, NULL)
  -- END
   
  SELECT @error = @@error  
  IF @error <> 0  
  BEGIN  
   RAISERROR('DBERROR||%s',16,1,@stmt)  
   RETURN dbo.GetDataBaseErrorCode()   
  END  
 END  
 ELSE   -- update  
 BEGIN  
  SET @stmt = 'STYLE UPDATE'  
  IF(@ThemeId = dbo.GetEmptyGUID())
  BEGIN
  UPDATE SIStyle  WITH (rowlock) SET   
   ApplicationId = @ApplicationId,  
   Title = @Title,  
   Description = @Description,  
   ModifiedBy = @ModifiedBy,  
   ModifiedDate = @Now,  
   FileName = @FileName,  
   Media = @Media,  
   GlobalProperty = @GlobalProperty,
   ConditionalCss = @ConditionalCss,  
   Status = @Status,            
   OrderNo = @OrderNo,
   IsThemed = CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END
  WHERE  Id = @Id   
   END
   ELSE
   BEGIN
  IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
	BEGIN
		UPDATE SIObjectTheme 
		SET ModifiedBy=@ModifiedBy,
	   ModifiedDate=@Now,
	   FileName= @FileName,
	   Status =@Status
	   WHERE ObjectId=@Id AND ThemeId=@ThemeId
	END
	ELSE
	BEGIN
	
		IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
		BEGIN
			UPDATE SIObjectTheme SET FileName=@FileName,Title=@Title,ModifiedBy=@ModifiedBy,ModifiedDate=@Now WHERE ObjectId=@Id AND ThemeId=@ThemeId
		END
		ELSE
		BEGIN
			INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
				VALUES (NEWID(),@Id,@ThemeId,@FileName,NULL,4,@ModifiedBy,@Now,NULL,NULL,1,@Title) -- 3 for template
		END		
		
		UPDATE SIStyle SET IsThemed=1 WHERE Id=@Id
	END	

END
  SELECT @error = @@error, @rowcount = @@rowcount  
   
  IF @error <> 0  
  BEGIN  
   RAISERROR('DBERROR||%s',16,1,@stmt)  
   RETURN dbo.GetDataBaseErrorCode()  
  END  
   
  IF @rowcount = 0  
  BEGIN  
   RAISERROR('CONCURRENCYERROR',16,1)  
   RETURN dbo.GetDataConcurrencyErrorCode()   -- concurrency error  
  END   
 END  
END
GO
ALTER PROCEDURE [dbo].[Template_GetTemplate]    
(    
 @Id    uniqueIdentifier=null ,    
 @ApplicationId uniqueidentifier=null,    
 @Title   nvarchar(256)=null,    
 @FileName  nvarchar(4000)=null,    
 @Status   int=null,    
 @PageIndex  int=1,    
 @PageSize  int=null,    
 @Type   int=null,    
 @ThemeId  UNIQUEIDENTIFIER=null,    
 @ResolveThemeId bit = null     
 )    
AS    
    
--********************************************************************************    
-- Variable declaration    
--********************************************************************************    
DECLARE @EmptyGUID uniqueidentifier    
    
BEGIN    
--********************************************************************************    
-- code    
--********************************************************************************    
IF(@ResolveThemeId IS NULL)    
 SET @ResolveThemeId = 0    
SET @EmptyGUID = dbo.GetEmptyGUID()    
 IF @PageSize is Null    
 BEGIN    
   IF(@ResolveThemeId = 1)    
    SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 3)    
        
  IF(@ThemeId = dbo.GetEmptyGUID() OR @ThemeId IS NULL)    
  BEGIN    
   SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,    
    FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml,    
  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,    
  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,    
    FN.UserFullName CreatedByFullName,    
    MN.UserFullName ModifiedByFullName,    
  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId    
  FROM SITemplate S     
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy    
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy    
  INNER JOIN HSStructure H on S.Id = H.Id    
  WHERE S.Id = isnull(@Id, S.Id) and    
   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and    
   S.Type=isnull(@Type,S.Type) and    
   S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())    
  END    
  ELSE    
  BEGIN
  	
  		
    SELECT S.Id, 
    CASE WHEN OT.Title IS NULL THEN S.Title ELSE OT.Title END AS Title,  
    S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate,    
    CASE WHEN OT.FileName IS NULL THEN S.FileName ELSE OT.FileName END AS FileName,
    ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml,    
  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,    
  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,    
    FN.UserFullName CreatedByFullName,    
    MN.UserFullName ModifiedByFullName,    
  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, OT.CodeFile, H.ParentId    
  FROM SITemplate S     
  LEFT JOIN SIObjectTheme OT ON OT.ObjectId = S.Id AND OT.ObjectTypeId = 3  AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId=@ThemeId)
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy    
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy    
  INNER JOIN HSStructure H on S.Id = H.Id    
  WHERE 
  S.Id = isnull(@Id, S.Id) and    
   isnull(upper(OT.Title),'') = isnull(upper(@Title), isnull(upper(OT.Title),'')) and    
   isnull(upper(OT. FileName),'') = isnull(upper(@FileName), isnull(upper(OT.FileName),'')) and    
   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and    
   S.Type=isnull(@Type,S.Type) and    
   S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()) 
  END    
      
 END    
     
 ELSE    
 BEGIN    
  WITH ResultEntries AS (     
   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)    
    AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,     
    ModifiedDate, FileName,ImageURL,Type,ApplicationId,    
 (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO ) StyleId,    
    (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO ) ScriptId,    
    [dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml    
   FROM SITemplate S    
   WHERE S.Id = isnull(@Id, S.Id) and    
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and    
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and    
    isnull(S.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(S.ApplicationId,@EmptyGUID)) and    
    S.Type=isnull(@Type,S.Type) and    
    S.Status = isnull(@Status, S.Status)and     
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))    
    
  SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,    
    FileName,ImageURL,Type,ApplicationId, StyleId,ScriptId,    
    FN.UserFullName CreatedByFullName,    
    MN.UserFullName ModifiedByFullName,    
    ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, H.ParentId    
  FROM ResultEntries S    
  INNER JOIN HSStructure H on S.Id = H.Id    
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy    
   LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy    
  WHERE Row between     
   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize    
    
 END    
    
END   
  

GO
ALTER PROCEDURE [dbo].[Template_Save]
(
	@Id					uniqueidentifier =null OUT,
	@ApplicationId		uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	@Description        nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier=null,
	@ModifiedDate    	datetime=null,
	@FileName            nvarchar(4000)=null,
	@ImageURL	         nvarchar(4000)=null,
	@CodeFile			 nvarchar(4000)=null,
	@TemplateType        int=null,
	@Status				 int=null,
	@PagePart			 bit=0,
	@LayoutTemplateId		 uniqueidentifier=null,
	@ZonesXml			xml = null,
	@ThemeId			UNIQUEIDENTIFIER = null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
BEGIN
--********************************************************************************
-- code
--********************************************************************************

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	/* IF @Id specified, ensure exists */
   IF (@Id is not null)
       IF((SELECT count(*) FROM  SITemplate WHERE Id = @Id AND Status != dbo.GetDeleteStatus() 
			AND ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) = 0)
       BEGIN
			RAISERROR('NOTEXISTS||Id', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
       END

	SET @Now = getdate()
	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	/*
	Sankar(05/01/2014) : 
	1. Upload a template with DEFAULT Option => Insert a row in the sitemplate with IsThemed = 0
	2. Upload a template with SELECTED Theme => Insert a row in the sitemplate with IsThemed = 1
	
	*/


	--For insert, no need to worry about the theme as we always upload the default template 1st. There can not be a Theme without a default template.
	IF (@Id is null)			-- New INSERT
	BEGIN
		SET @stmt = 'TEMPLATE INSERT'
		SET @Id = newid();
		INSERT INTO SITemplate(
					   Id,	
					   ApplicationId,
					   Title,
                       Description,
					   CreatedBy,
                       CreatedDate,
					   Status,
                       FileName,
						ImageURL,
						CodeFile,	
                       Type,
						PagePart,
						LayoutTemplateId,
						ZonesXml) 
					values (
						@Id,
						@ApplicationId,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@FileName,
						@ImageURL,
						@CodeFile,
						@TemplateType,
						@PagePart,
						@LayoutTemplateId,
						@ZonesXml)

		SELECT @error = @@error
		IF @error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END
    END
    ELSE			-- update
         BEGIN
			
			--We need to update the SIObjectTheme table if we have a theme id greater than 0; Themeid 0 is for default template and it should go to SITemplate table
			SET @stmt = 'TEMPLATE UPDATE'
			IF ( @ThemeId = dbo.GetEmptyGUID())
			BEGIN
				UPDATE	SITemplate  WITH (rowlock)	SET 
					ApplicationId=@ApplicationId,
					Title =@Title,
					Description=@Description,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=@Now,
					ImageURL=@ImageURL,
					CodeFile=@CodeFile,
					FileName=@FileName,
					--Type=@TemplateType ,
					Status =@Status  ,
					PagePart =@PagePart,
					LayoutTemplateId = ISNULL(@LayoutTemplateId, LayoutTemplateId),
					ZonesXml = ISNULL(@ZonesXml, ZonesXml)         
 				WHERE 	Id    = @Id 
				--AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

			END
			ELSE
			BEGIN
				
				--Update the SIObjectTheme table with the properties
				DELETE FROM SIObjectTheme WHERE ObjectId=@Id AND ObjectTypeId=3 AND ThemeId=@ThemeId
				
				INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
				VALUES (NEWID(),@Id,@ThemeId,@FileName,@CodeFile,3,@ModifiedBy,@Now,NULL,NULL,1,@Title) -- 3 for template

				UPDATE SITemplate SET IsThemed=1 WHERE Id=@Id
			END
			SELECT @error = @@error, @rowcount = @@rowcount

			IF @error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END
END
GO

IF (OBJECT_ID('GetThemeObjects','TF') IS NOT NULL)
	DROP FUNCTION dbo.GetThemeObjects
GO
CREATE FUNCTION dbo.GetThemeObjects(@ApplicationId uniqueidentifier,@ObjectTypeId int) 
RETURNS @Results TABLE 
(
	ObjectId uniqueidentifier,  
	ObjectTypeId int,  
	ThemeId uniqueidentifier,
	FileName nvarchar(max),
	CodeFile nvarchar(max)
)  
AS
BEGIN
 
 /*
 1. Get the site theme from sisite table
 2. 
 */
 DECLARE @SiteThemeId UNIQUEIDENTIFIER
 SELECT @SiteThemeId = ThemeId FROM SISite WHERE Id=@ApplicationId AND Status <> dbo.GetDeleteStatus()
 
 IF NOT EXISTS(SELECT 1 FROM SIThemeGroup WHERE ThemeId=@SiteThemeId)
 BEGIN
	 INSERT INTO @Results
	 SELECT 
	  ObjectId,
	  ObjectTypeId,
	  ThemeId,
	  FileName,
	  CodeFile
	 FROM SIObjectTheme 
	 WHERE ObjectTypeId=@ObjectTypeId AND ThemeId=@SiteThemeId
 END
 ELSE
 BEGIN
	INSERT INTO @Results
	 SELECT 
	  ObjectId,
	  @ObjectTypeId ,
	  @SiteThemeId,
	  FileName,
	  CodeFile
	 FROM SIObjectTheme 
	 WHERE ThemeId IN (SELECT SourceThemeId FROM SIThemeGroup WHERE ThemeId=@SiteThemeId AND ObjectTypeId=@ObjectTypeId)
 END

RETURN 
END
GO
IF (OBJECT_ID('GetThemeId','FN') IS NOT NULL)
	DROP FUNCTION dbo.GetThemeId
GO
CREATE FUNCTION [dbo].[GetThemeId](@SiteId uniqueidentifier, @ObjectTypeId int)  
RETURNS uniqueidentifier  
AS  
BEGIN  
 DECLARE @ThemeId uniqueidentifier  
 DECLARE @SiteTheme uniqueidentifier  
   
 SELECT @SiteTheme = ThemeId from SISite where Id = @SiteId  
   
 IF EXISTS(Select 1 FROM SIThemeGroup where ThemeId = @SiteTheme AND ObjectTypeId = @ObjectTypeId)  
  SELECT @ThemeId = SourceThemeId From SIThemeGroup where ThemeId = @SiteTheme AND ObjectTypeId = @ObjectTypeId  
 ELSE  
  SET @ThemeId = @SiteTheme   
    
 Return @ThemeId   
END  
GO
PRINT 'Altering Procedure Taxonomy_ReplaceRelationReference'
GO
ALTER PROCEDURE [dbo].[Taxonomy_ReplaceRelationReference] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@TaxonomyIds  xml,
	@ObjectId uniqueidentifier,
	@ObjectTypeId int,
	@ApplicationId uniqueidentifier,
	@ModifiedBy  uniqueidentifier = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Now DateTime
--********************************************************************************
-- code
--********************************************************************************
BEGIN
		
		declare @tags as table(Id uniqueidentifier)
		declare @appId uniqueidentifier 
		
		
		insert into @tags
		select distinct tab.col.value('text()[1]','uniqueidentifier')
		from  @TaxonomyIds.nodes('/GenericCollectionOfGuid/guid')tab(col)
		
		-- this is to find out to which the application the tags are belonging to
		SELECT @appId =  ApplicationId FROM  COTaxonomy WHERE Id in (SELECT TOP 1 Id FROM @tags)
		
		
		SET @Now = getutcdate()
		Delete FROM COTaxonomyObject Where ObjectId =@ObjectId AND TaxonomyId IN (SELECT Id FROM COTaxonomy WHERE ApplicationId=@appId  )
		
		INSERT INTO COTaxonomyObject(Id,TaxonomyId,ObjectId,ObjectTypeId,SortOrder,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		Select NEWID(),Id,
			@ObjectId AS ObjectId, @ObjectTypeId AS ObjectTypeId,
			1 As SortOrder, 1 AS Status,
			@ModifiedBy AS CreatedBy, @Now AS CreatedDate,
			@ModifiedBy AS ModifiedBy,@Now AS ModifiedDate 
		FROM
			@tags 
			

	 	IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,'')
			RETURN dbo.GetDataBaseErrorCode()
		END

	
END

GO
PRINT 'Altering Procedure Taxonomy_Save'
GO
ALTER PROCEDURE [dbo].[Taxonomy_Save] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id  uniqueidentifier output,
	@ApplicationId uniqueidentifier,
	@Title  nvarchar(256),
	@Description  nvarchar(256) = null,
	@ParentId uniqueidentifier=null,
	@CreatedBy  uniqueidentifier,
	@ModifiedBy  uniqueidentifier = null,
	@Status  int
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
	@NewId		uniqueidentifier,
	@RowCount		INT,
	@Stmt 		VARCHAR(36),
	@Now 			datetime,
	@Error 		int,
	@Lft bigint,
	@Rgt bigint

--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM   COTaxonomy WHERE  Id = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END		
	IF(@ParentId is null)
	SET @ParentId = (Select top 1 Id from COTaxonomy Where ApplicationId=@ApplicationId and ParentId is null)
	
	
	SET @Now = getutcdate()

	IF (@Id IS NULL)			-- new insert
	BEGIN
		SET @Stmt = 'Create Taxonomy item'
		SET @Rgt =(Select top 1 RgtValue from COTaxonomy Where ApplicationId=@ApplicationId and Id =@ParentId)
		
		-- Create space for the new node
		Update COTaxonomy SET LftValue = case when LftValue > @Rgt Then LftValue + 2 Else LftValue End,
								RgtValue = case when RgtValue >= @Rgt Then RgtValue + 2 Else RgtValue End
		Where ApplicationId= @ApplicationId
		
		IF @Rgt IS NULL
			SET @Rgt = 1

		SET @Lft = @Rgt
		SET @Rgt = @Lft + 1
		Select @NewId =newid()
		SET @Status = dbo.GetActiveStatus()
		INSERT INTO COTaxonomy  WITH (ROWLOCK)(
			Id,
			ApplicationId,
			Title,
			Description,			
			CreatedDate,					
			CreatedBy,			
			Status,
			LftValue,
			RgtValue,
			SiteId,
			ParentId
		)
		values
			(
				@NewId,
				@ApplicationId,
				@Title,
				@Description,				
				@Now,
				@CreatedBy,				
				@Status,
				@Lft,
				@Rgt,
				@ApplicationId,
				@ParentId
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		SELECT @Id = @NewId
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify Taxonomy item'
		Update COTaxonomy WITH (ROWLOCK)
		set 
			ApplicationId = @ApplicationId,
			Title = @Title,
			Description = @Description,				
			ModifiedDate = @Now,
			ModifiedBy = @ModifiedBy,
			Status = @Status
		where Id = @Id

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		
END

GO
PRINT 'Altering Procedure Taxonomy_GetTaxonomyHierarchy'
GO

-- Copyright ⌐ 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: 
-- Author: P. Jeevan
-- Created Date: 
-- Created By: 
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
-- exec  
--********************************************************************************
ALTER PROCEDURE [dbo].[Taxonomy_GetTaxonomyHierarchy](
@SiteId uniqueidentifier,
@TaxonomyId uniqueidentifier = null,
@ParentLevel int,
@ChildrenLevel int 
)
as
Begin
Declare
@Lft bigint,
@Rgt bigint,
@Id uniqueidentifier


if(@TaxonomyId is null)
	begin
		select @Id = Id from COTaxonomy s where s.ParentId is null And ApplicationId =@SiteId and status<> dbo.GetDeleteStatus()
		SELECT @Lft=LftValue,@Rgt=RgtValue FROM COTaxonomy WHERE Id=@Id And ApplicationId=@SiteId
		 
	End
else
	begin
		SELECT @Lft=LftValue,@Rgt=RgtValue FROM COTaxonomy WHERE Id=@TaxonomyId AND ApplicationId=@SiteId
	End

IF @ParentLevel <> -1
		Select @ParentLevel = count(*)+1 - @ParentLevel from COTaxonomy S
		Where S.LftValue < @Lft and S.RgtValue > @Rgt AND ApplicationId=@SiteId
	ELSE
		Select @ParentLevel = 1

If (@ChildrenLevel is null)
	set @ChildrenLevel =0

	IF @ChildrenLevel = 1 
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
							    FN.UserFullName CreatedByFullName,
								MN.UserFullName ModifiedByFullName,
								xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM  COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId  AND T.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId  AND T.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
					WHERE T.ParentId = @TaxonomyId or T.Id = @TaxonomyId AND T.ApplicationId=@SiteId
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
					WHERE T.ParentId = @TaxonomyId or T.Id = @TaxonomyId  AND T.ApplicationId=@SiteId)as xmlTable
					LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
					 ORDER BY xmlTable.LftValue
	END
	ELSE IF ( @ChildrenLevel = 0 and @TaxonomyId is not null)
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
						   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
						   FN.UserFullName CreatedByFullName,
						   MN.UserFullName ModifiedByFullName,
						   xmlTable.Status
		FROM
			(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue )
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
							   T.ModifiedBy,T.ModifiedDate,
							   T.Status
				FROM  COTaxonomy T
				WHERE T.Id = @TaxonomyId AND T.ApplicationId=@SiteId
				UNION ALL
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
				FROM COTaxonomy T 
				WHERE T.Id = @TaxonomyId AND T.ApplicationId=@SiteId)as xmlTable 
				LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
				ORDER BY xmlTable.LftValue
	END
	ELSE IF @ChildrenLevel = -1 
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
							   FN.UserFullName CreatedByFullName,
						       MN.UserFullName ModifiedByFullName,
							   xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM  COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId) as xmlTable 
					LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
					ORDER BY xmlTable.LftValue
	END
	ELSE IF (@ChildrenLevel = 0 and @TaxonomyId is null) 
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
							   FN.UserFullName CreatedByFullName,
						       MN.UserFullName ModifiedByFullName,
							   xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND ApplicationId=@SiteId AND P.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM  COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId) as xmlTable 
					LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
					LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
					ORDER BY xmlTable.LftValue
	End
	Else
	BEGIN
		select xmlTable.[Sign],xmlTable.Id,xmlTable.Title,xmlTable.Description,xmlTable.CreatedBy,
							   xmlTable.CreatedDate,xmlTable.ModifiedBy,xmlTable.ModifiedDate,
						       FN.UserFullName CreatedByFullName,
						       MN.UserFullName ModifiedByFullName,
						       xmlTable.Status
			FROM
				(
			-- Selecting Lft for Parent
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
		
			-- Selecting Rgt for Parent
			SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM COTaxonomy T
			WHERE T.LftValue < @Lft and T.RgtValue > @Rgt AND T.ApplicationId=@SiteId
			And @ParentLevel <= (Select count(*)+1 from COTaxonomy P Where T.LftValue > P.LftValue and T.RgtValue < P.RgtValue  AND P.ApplicationId=@SiteId)
			UNION ALL
			
			-- children
			SELECT  LftValue, 0 [Sign],T.Id,T.Title,T.Description,T.CreatedBy,T.CreatedDate,
								   T.ModifiedBy,T.ModifiedDate,
								   T.Status
					FROM COTaxonomy T
					WHERE T.LftValue >= @Lft and T.RgtValue <= @Rgt AND T.ApplicationId=@SiteId
						  AND @ChildrenLevel>=(Select count(*) + 1 
												From COTaxonomy M 
												WHERE M.LftValue Between @Lft and @Rgt
												And T.LftValue Between M.LftValue and M.RgtValue
												And M.LftValue Not in (T.LftValue ,@Lft) AND M.ApplicationId=@SiteId)
					UNION ALL
				SELECT  RgtValue, 1 [Sign],null,null,null,null,null,null,null,null
					FROM  COTaxonomy T 
					WHERE LftValue >= @Lft and RgtValue <= @Rgt AND T.ApplicationId=@SiteId
							AND @ChildrenLevel>=(Select count(*) + 1 
												From COTaxonomy M 
												WHERE M.LftValue Between @Lft and @Rgt
												And T.LftValue Between M.LftValue and M.RgtValue
												And M.LftValue Not in (T.LftValue ,@Lft) AND M.ApplicationId=@SiteId)) as xmlTable 
												LEFT JOIN VW_UserFullName FN on FN.UserId = xmlTable.CreatedBy
												LEFT JOIN VW_UserFullName MN on MN.UserId = xmlTable.ModifiedBy
												ORDER BY xmlTable.LftValue

	END
END

GO
PRINT 'Altering procedure Taxonomy_GetTaxonomyByObject'
GO
	-- Copyright ⌐ 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: 
-- Author: P. Jeevan
-- Created Date: 
-- Created By: 
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
-- exec  
--********************************************************************************
ALTER PROCEDURE [dbo].[Taxonomy_GetTaxonomyByObject](  
@ObjectId uniqueidentifier=null,  
@ApplicationId uniqueidentifier=null,
@ObjectTypeId int=null
 
)  
AS  
BEGIN  
  
  SELECT  TC.Id,  
  TC.ApplicationId,  
  TC.Title,  
  TC.Description,  
  TC.CreatedDate,  
  TC.CreatedBy,  
  TC.ModifiedBy,  
  TC.ModifiedDate,  
  TC.Status,  
  FN.UserFullName CreatedByFullName,
  MN.UserFullName ModifiedByFullName,
  TC.ParentId,
  OT.ObjectId,
  OT.ObjectTypeId     
  FROM COTaxonomy TC 
  INNER JOIN COTaxonomyObject OT ON TC.Id = OT.TaxonomyId
  LEFT JOIN VW_UserFullName FN ON FN.UserId = TC.CreatedBy
  LEFT JOIN VW_UserFullName MN ON MN.UserId = TC.ModifiedBy
  WHERE TC.Status =1 AND OT.Status=1 AND (@ObjectId IS NULL OR OT.ObjectId =@ObjectId)
  AND(@ObjectTypeId IS NULL OR OT.ObjectTypeId =@ObjectTypeId)
  AND (@ApplicationId IS NULL OR TC.ApplicationId=@ApplicationId)
End


GO
PRINT 'Altering Procedure Taxonomy_Delete '
GO
ALTER PROCEDURE [dbo].[Taxonomy_Delete] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id		uniqueidentifier,
	@ModifiedBy    	uniqueidentifier,
	@ApplicationId uniqueidentifier,
	@Status int output
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE	@Now datetime,
		@Stmt 	VARCHAR(36),
		@DeleteStatus int,
		@Nodes xml,
		@Lft bigint,
		@Rgt bigint

BEGIN
	-- if Id is specified, ensure exists 
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM   COTaxonomy WHERE  Id = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END
	
	SET @Now =getutcdate()
	
	--changes taxonomy status to deleted in COTaxonomy table
	--set @DeleteStatus =dbo.GetDeleteStatus()
	
	--select @Nodes = (select Id from GetTreeByHierarchy(@Id)  for xml auto)
	--update COTaxonomy set ModifiedDate = @Now, ModifiedBy =@ModifiedBy, Status = @DeleteStatus
	--		where Id in (SELECT T.c.value('@Id','uniqueidentifier') as id      
	--			FROM   @Nodes.nodes('/GetTreeByHierarchy') T(c))

	set @Status = dbo.GetDeleteStatus()
	
	Select @Lft = LftValue,@Rgt=RgtValue from COTaxonomy Where Id=@Id
	
	Delete From COTaxonomy
	Where LftValue Between @Lft and @Rgt AND ApplicationId=@ApplicationId
	
	
	-- Adjust lft and rgt
	Update COTaxonomy Set LftValue= case when LftValue >@Rgt then LftValue - (@Rgt -@Lft) -1  else LftValue end,
						  RgtValue= case when RgtValue >@Rgt then RgtValue - (@Rgt -@Lft) -1  else RgtValue end 
						  WHERE ApplicationId=@ApplicationId
End

GO
PRINT 'Altering procedure RegenerateLftRgt'
GO
ALTER PROCEDURE [dbo].[RegenerateLftRgt](@SiteId uniqueidentifier=null,@key int=2,@backup bit =1)
as
BEGIN
-- 1 PageMapNode
-- 2 COContentStructure
-- 3 COFileStructure
-- 4 COImageStructure
-- 9 COFormStructure
IF @SiteId is NULL
	Set @SiteId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
Declare @sqlStmt varchar(1000)
Declare @tblName varchar(100)

IF @Key =1
	SET @tblName = 'PageMapNode'
ELSE IF @Key=2
	SET @tblName='COContentStructure'
ELSE IF @Key =3
	SET @tblName='COFileStructure'
ELSE IF @Key =4
	SET @tblName='COImageStructure'
ELSE IF @Key =5
	SET @tblName='HSStructure'
ELSE IF @key =6
	SET @tblName='PRProductType'
ELSE IF @Key =7
	SET @tblName='COTaxonomy'	
ELSE IF @Key =8
	SET @tblName='SISite'		
ELSE IF @Key =9
	SET @tblName='COFormStructure'		
	
if(@backup =1)
begin
if  exists (Select * from sys.tables Where NAME='Regen_backup_'+  replace(replace(replace(cast(getdate() as datetime),'-','_'),' ','_'),':','_')) 
BEGIN
	SET @sqlStmt = 'Drop table Regen_backup_' +  replace(replace(replace(cast(getdate() as datetime),'-','_'),' ','_'),':','_')
	Exec (@sqlStmt)
END
SET @sqlStmt='Select  * into Regen_backup_' +  replace(replace(replace(cast(getdate() as datetime),'-','_'),' ','_'),':','_') +' from '+ @tblName 
EXEC   (@sqlStmt)
end

if  exists (Select * from sys.tables Where NAME='Regen_Work')
	Drop table Regen_Work

	CREATE TABLE Regen_Work
( Id Uniqueidentifier NOT NULL,
 LftValue Bigint NOT NULL,
 RgtValue bigint NOT NULL,
 ParentId uniqueidentifier);

 IF @tblName = 'PageMapNode'
 INSERT INTO Regen_Work
	Select  PageMapNodeId,LftValue,RgtValue,ParentId from PageMapNode  Where SiteId=@SiteId
 ELSE IF @tblName = 'COContentStructure'
	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COContentStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'COFileStructure'
 	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId from COFileStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'COImageStructure'
 	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COImageStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'HSStructure'
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from HSStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'PRProductType'
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from PRProductType
ELSE IF @tblName = 'COTaxonomy'
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COTaxonomy  Where ApplicationId=@SiteId	
ELSE IF @tblName = 'SISite'
BEGIN
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,case when ParentSiteId IS NULL then '00000000-0000-0000-0000-000000000000' else ParentSiteId END ParentId  
	from SISite Where MasterSiteId=@SiteId
	Order By Title
END	
ELSE IF @tblName = 'COFormStructure'
BEGIN
	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COFormStructure  Where SiteId=@SiteId
END	


if exists (Select * from sys.tables Where NAME='Regen_Temp')
DROP TABLE Regen_Temp;

CREATE TABLE Regen_Temp
(stack_top bigint NOT NULL,
 Id Uniqueidentifier NOT NULL,
 lft Bigint NOT NULL,
 rgt bigint);


BEGIN

DECLARE @lft_rgt bigint, @stack_pointer bigint, @max_lft_rgt bigint;

SET @max_lft_rgt = 2 * (SELECT COUNT(*) FROM Regen_Work);

INSERT INTO Regen_Temp
SELECT top 1 1, Id, 1, @max_lft_rgt 
  FROM Regen_Work
 Order By LftValue,ParentId;

SET @lft_rgt = 2;
SET @Stack_pointer = 1;

DELETE FROM Regen_Work 
 WHERE Id in (select Id from Regen_Temp)

-- The Regen_Temp is now loaded and ready to use

WHILE (@lft_rgt < @max_lft_rgt)
BEGIN 
 IF EXISTS (SELECT *
              FROM Regen_Temp AS S1, Regen_Work AS T1
             WHERE S1.Id = T1.ParentId
               AND S1.stack_top = @stack_pointer)
    BEGIN -- push when stack_top has subordinates and set lft value
      INSERT INTO Regen_Temp
      SELECT top 1 (@stack_pointer + 1), T1.Id, @lft_rgt, NULL
        FROM Regen_Temp AS S1, Regen_Work AS T1
       WHERE S1.Id = T1.ParentId
         AND S1.stack_top = @stack_pointer
		
		Order By T1.LftValue;

      -- remove this row from Regen_Work 
      DELETE FROM Regen_Work
       WHERE Id IN (SELECT Id
                        FROM Regen_Temp
WHERE stack_top = @stack_pointer + 1 );
      SET @stack_pointer = @stack_pointer + 1;
    END -- push
    ELSE
    BEGIN  -- pop the Regen_Temp and set rgt value
      UPDATE Regen_Temp
         SET rgt = @lft_rgt,
             stack_top = -stack_top
       WHERE stack_top = @stack_pointer 
		--AND SiteId =@SiteId
      SET @stack_pointer = @stack_pointer - 1;
    END; -- pop
  SET @lft_rgt = @lft_rgt + 1;
  END; -- if
END; -- while
 IF @tblName = 'PageMapNode'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from PageMapNode Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM PageMapNode M
		INNER JOIN Regen_Temp R ON M.PageMapNodeId = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating PageMapNode in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'COContentStructure'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COContentStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COContentStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COContentStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'COFileStructure'
 	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COFileStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COFileStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating  COFileStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'COImageStructure'
 	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COImageStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COImageStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COImageStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'HSStructure'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from HSStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM HSStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating HSStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'PRProductType'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from PRProductType))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM PRProductType M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
	END
	ELSE
	BEGIN
		print 'Error Updating PRProductType in Site ' + cast(@SiteId as char(36))
	END	
	ELSE IF @tblName = 'COTaxonomy'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COTaxonomy Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COTaxonomy M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COTaxonomy in Site ' + cast(@SiteId as char(36))
	END
	ELSE IF @tblName = 'SISite'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from SISite Where MasterSiteId=@SiteId ))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM SISite M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.MasterSiteId=@SiteId
	END
	ELSE
	BEGIN
	select * from Regen_Temp
		print 'Error Updating SISite in Site ' 
	END
	ELSE IF @tblName = 'COFormStructure'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COFormStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COFormStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COFormStructure in Site ' + cast(@SiteId as char(36))
	END
Drop Table Regen_Temp
Drop table Regen_Work;

END
GO



GO
/*
SiteQueue Tables and stored Procedures

Tables
[dbo].[SQSiteDesign]
[dbo].[SQSiteDesignTheme]
[dbo].[SQSiteQueue]
[dbo].[SQSiteQueueRunHistory]
[dbo].[SQSiteQueueData]
[dbo].[SQSiteQueueUser]
[dbo].[SQSiteQueueUserData]

SPS
[dbo].[SiteDesignTheme_Get]
[dbo].[SiteDesign_Save]
[dbo].[SiteDesign_Get]
[dbo].[SiteQueueRunHistory_Save]
[dbo].[SiteQueue_Save]
[dbo].[SiteQueue_Get]
[dbo].[SiteQueue_Delete]
[dbo].[SiteQueueUser_get]

*/

/****** Object:  Table [dbo].[SQSiteDesign]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteDesign]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteDesign](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[ContentRootFolderId] [uniqueidentifier] NULL,
	[AssetFileRootFolderid] [uniqueidentifier] NULL,
	[AssetImageRootFolderId] [uniqueidentifier] NULL,
	[PageMapNodeRootFolderId] [uniqueidentifier] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_SCSiteDesign] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SQSiteDesignTheme]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteDesignTheme]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteDesignTheme](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteDesignId] [uniqueidentifier] NOT NULL,
	[SiteThemeId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_SCSiteDesignTheme] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[SiteDesignTheme_Get]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteDesignTheme_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE Procedure [dbo].[SiteDesignTheme_Get]
(
	@Id uniqueidentifier =null,
	@SiteDesignId uniqueidentifier =null,
	@SiteThemeId uniqueidentifier =null
)
as
begin
	select * from SQSiteDesignTheme where (@Id is null or Id=@Id) and (@SiteDesignId is null or SiteDesignId =@SiteDesignId) and (@SiteThemeId is null or SiteThemeId =@SiteThemeId)
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[SiteDesign_Save]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteDesign_Save]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[SiteDesign_Save]
(
	@Id uniqueidentifier output,
	@Title nvarchar(255),
	@SiteId uniqueidentifier,
	@ContentRootFolderId uniqueidentifier=null,
	@AssetFileRootFolderId uniqueidentifier=null,
	@AssetImageRootFolderId uniqueidentifier=null,
	@PageMapNodeRootFolderId uniqueidentifier=null,
	@Status int,
	@ThemeIds xml =null
)
as
begin

	if(@Id is null)
	begin
		set @Id=NEWID()
		INSERT INTO [dbo].[SQSiteDesign]
           ([Id]
           ,[Title]
           ,[SiteId]
           ,[ContentRootFolderId]
           ,[AssetFileRootFolderid]
           ,[AssetImageRootFolderId]
           ,[PageMapNodeRootFolderId]
           ,[CreatedDate]
           ,Status
           )
     VALUES
           (@Id
           ,@Title
           ,@SiteId 
           ,@ContentRootFolderId 
           ,@AssetFileRootFolderId
           ,@AssetImageRootFolderId 
           ,@PageMapNodeRootFolderId 
           ,GETUTCDATE(), @Status)
	end
	else
	begin
		UPDATE [dbo].[SQSiteDesign]
		   SET [Title] = @Title 
			  ,[SiteId] =@SiteId 
			  ,[ContentRootFolderId] = @ContentRootFolderId 
			  ,[AssetFileRootFolderid] = @AssetFileRootFolderId 
			  ,[AssetImageRootFolderId] = @AssetImageRootFolderId 
			  ,[PageMapNodeRootFolderId] = @PageMapNodeRootFolderId       
			  ,[ModifiedDate] = getutcdate()
			  ,Status=@Status
		 WHERE Id = @Id
	end
	
	if(@ThemeIds is not null)
	begin
		INSERT INTO [dbo].SQSiteDesignTheme
           (Id
            ,[SiteDesignId]
           ,[SiteThemeId])            
		select newid(), @Id, t.X.value(''(@value)[1]'',''uniqueidentifier'')  
			From @ThemeIds.nodes(''/list/item'') t(X) where  t.X.value(''(@value)[1]'',''uniqueidentifier'') not in (select SiteThemeId from SQSiteDesignTheme where SiteDesignId=@Id)
	end
	
	
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[SiteDesign_Get]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteDesign_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[SiteDesign_Get]
(
	@Id uniqueidentifier =null
)
as
begin
	select *,(select SiteThemeId as ''@value''  from SQSiteDesignTheme where SiteDesignId=Id FOR XML Path(''item''), Root(''list'')) as ThemeIds  from SQSiteDesign where @Id is null or Id=@Id
end' 
END
GO
/****** Object:  Table [dbo].[SQSiteQueue]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteQueue]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteQueue](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](255) NOT NULL,
	[PrimarySiteUrl] [nvarchar](255) NULL,
	[CreatedSiteId] [uniqueidentifier] NULL,
	[QueuedDateTime] [datetime] NULL,
	[ProcessedDateTime] [datetime] NULL,
	[Status] [int] NOT NULL,
	[SiteDesignThemeId] [uniqueidentifier] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_SCSiteCreation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SQSiteQueueUser]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteQueueUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteQueueUser](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteQueueId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](255) NOT NULL,
	[Password] [nvarchar](255) NOT NULL,
	[Email] [nvarchar](255) NULL,
 CONSTRAINT [PK_SQSiteQueueUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SQSiteQueueRunHistory]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteQueueRunHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteQueueRunHistory](
	[Id] [uniqueidentifier] NOT NULL,
	[SiteQueueId] [uniqueidentifier] NOT NULL,
	[LogData] [nvarchar](max) NULL,
	[RunDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SQSiteQueueData]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteQueueData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteQueueData](
	[SiteQueueId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Value] [nvarchar](4000) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SQSiteQueueUserData]    Script Date: 05/22/2014 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SQSiteQueueUserData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SQSiteQueueUserData](
	[SQUserId] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Value] [nvarchar](4000) NULL
) ON [PRIMARY]
END
GO
/****** Object:  StoredProcedure [dbo].[SiteQueue_Get]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteQueue_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[SiteQueue_Get]
(
	@Id uniqueidentifier=null,
	@Status int =null
)
as
begin
	--select Name as ''@key'', Value as ''@value'' from SQSiteQueueData where SiteQueueId=''5A828554-7005-45AE-873D-57EAE2821570'' FOR XML Path(''keyvalue''), Root(''dictionary'')
	select *, (select Name as ''@key'', Value as ''@value'' from SQSiteQueueData where SiteQueueId=Id FOR XML Path(''keyvalue''), Root(''dictionary'')) as  CustomAttributes 

	from SQSiteQueue S where (@Id is null or Id=@Id) and (@Status is null or Status=@Status)
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[SiteQueue_Delete]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteQueue_Delete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[SiteQueue_Delete]
(
	@Id uniqueidentifier
)
as
begin
	delete from SQSiteQueueRunHistory where SiteQueueId=@Id
	delete from SQSiteQueueData where SiteQueueId=@Id
	delete from SQSiteQueue where Id=@Id
end
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SiteQueueRunHistory_Save]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteQueueRunHistory_Save]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[SiteQueueRunHistory_Save]
(
	@Id uniqueidentifier output,
	@SiteQueueId uniqueidentifier,
	@LogData nvarchar(max),
	@Status int
)
as
begin
	if(@Id is null)
	begin
		set @Id=NEWID()
		
		INSERT INTO [dbo].[SQSiteQueueRunHistory]
			   ([Id]
			   ,[SiteQueueId]
			   ,[LogData]
			   ,[RunDate]
			   ,[Status])
		 VALUES
			   (@Id
			   ,@SiteQueueId 
			   ,@LogData
			   ,GETUTCDATE()
			   ,@Status)
 	end 
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[SiteQueueUser_get]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteQueueUser_get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[SiteQueueUser_get]
(
	@Id uniqueidentifier=null,
	@SiteQueueId uniqueidentifier
)
as
begin
	select *, (select Name as ''@key'', Value as ''@value'' from SQSiteQueueUserData where SiteQueueId=Id FOR XML Path(''keyvalue''), Root(''dictionary'')) as  CustomAttributes	
	from SQSiteQueueUser  S where (@Id is null or Id=@Id) and (@SiteQueueId is null or SiteQueueId=@SiteQueueId)
end' 
END
GO
/****** Object:  StoredProcedure [dbo].[SiteQueue_Save]    Script Date: 05/22/2014 16:32:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteQueue_Save]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE procedure [dbo].[SiteQueue_Save]
(
	@Id uniqueidentifier output,
	@Title nvarchar(255),
	@PrimarySiteUrl nvarchar(255),
	@CreatedSiteId uniqueidentifier=null,
	@QueuedDateTime Datetime = null,
	@ProcessedDateTime DateTime=null,
	@Status int,
	@SiteDesignThemeId uniqueidentifier, 
	@CustomAttributes xml =null,
	@SiteUsersXml Xml =null
)
as
begin
	if (@QueuedDateTime  is null) set @QueuedDateTime= getutcdate()
	IF (@Id IS NULL)			-- new insert
	BEGIN
	set @Id  = NEWID()		
		INSERT INTO [dbo].[SQSiteQueue]
			   ([Id]
			   ,[Title]
			   ,[PrimarySiteUrl]
			   ,[CreatedSiteId]
			   ,[QueuedDateTime]
			   ,[ProcessedDateTime]
			   ,[Status]
			   ,[SiteDesignThemeId]
			   ,[CreatedDate])
		 VALUES
			   (@Id,
				@Title,
				@PrimarySiteUrl,
				@CreatedSiteId,
				@QueuedDateTime,
				@ProcessedDateTime,
				isnull(@Status,dbo.GetActiveStatus()),
				@SiteDesignThemeId ,
				GETUTCDATE()
			    )     
	end
	else
	begin
		UPDATE [dbo].[SQSiteQueue]
			SET [Title] = @Title 
			,[PrimarySiteUrl] = @PrimarySiteUrl 
			,[CreatedSiteId] =@CreatedSiteId 
			,[QueuedDateTime] = @QueuedDateTime 
			,[ProcessedDateTime] = @ProcessedDateTime 
			,[Status] = @Status 
			,[SiteDesignThemeId] = @SiteDesignThemeId 	
			,ModifiedDate=GETUTCDATE()		
		WHERE Id=@Id
	end
	if (@CustomAttributes is not null)
	begin
		delete from SQSiteQueueData where SiteQueueId=@Id and Name in (select t.X.value(''(@key)[1]'',''nvarchar(255)'') From @customAttributes.nodes(''/dictionary/keyValue'') t(X))
		INSERT INTO [dbo].[SQSiteQueueData]
           ([SiteQueueId]
           ,[Name]
           ,[Value])    
		select @Id, t.X.value(''(@key)[1]'',''nvarchar(255)''),t.X.value(''(@value)[1]'',''nvarchar(255)'')  
			From @customAttributes.nodes(''/dictionary/keyValue'') t(X)
	end
	
	delete from SQSiteQueueUserData where SQUserId in (select Id from SQSiteQueueUser where SiteQueueId=@Id)
	delete from SQSiteQueueUser where SiteQueueId=@Id
	
	if(@SiteUsersXml is not null)
	begin	
		
		
		INSERT INTO [dbo].[SQSiteQueueUser]
           ([Id]
           ,[SiteQueueId]
           ,[UserName]
           ,[Password]
           ,[Email])
		 select NEWID(),@Id
			   ,t.X.value(''(UserName)[1]'',''nvarchar(255)'')
			   ,t.X.value(''(Password)[1]'',''nvarchar(255)'')
			   ,t.X.value(''(Email)[1]'',''nvarchar(255)'')
			   From @SiteUsersXml.nodes(''/GenericCollectionOfSiteQueueUser/SiteQueueUser'') t(X)	
	
	
		INSERT INTO [dbo].[SQSiteQueueUserData]
           ([SQUserId]
           ,[Name]
           ,[Value])    
		select u.id, t.X.value(''(@key)[1]'',''nvarchar(255)''),t.X.value(''(@value)[1]'',''nvarchar(255)'')  
		FROM [dbo].[SQSiteQueueUser] u    		
			cross apply  @SiteUsersXml.nodes(''/GenericCollectionOfSiteQueueUser/SiteQueueUser/CustomAttributes/dictionary/keyValue[../../../UserName=sql:column("u.UserName")]'') t(X)
		where u.SiteQueueId=@Id  
		
	end


end' 
END
GO
/****** Object:  ForeignKey [FK_SQSiteDesign_SISite]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteDesign_SISite]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteDesign]'))
ALTER TABLE [dbo].[SQSiteDesign]  WITH CHECK ADD  CONSTRAINT [FK_SQSiteDesign_SISite] FOREIGN KEY([SiteId])
REFERENCES [dbo].[SISite] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteDesign_SISite]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteDesign]'))
ALTER TABLE [dbo].[SQSiteDesign] CHECK CONSTRAINT [FK_SQSiteDesign_SISite]
GO
/****** Object:  ForeignKey [FK_SCSiteDesignTheme_SCSiteDesign]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SCSiteDesignTheme_SCSiteDesign]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteDesignTheme]'))
ALTER TABLE [dbo].[SQSiteDesignTheme]  WITH CHECK ADD  CONSTRAINT [FK_SCSiteDesignTheme_SCSiteDesign] FOREIGN KEY([SiteDesignId])
REFERENCES [dbo].[SQSiteDesign] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SCSiteDesignTheme_SCSiteDesign]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteDesignTheme]'))
ALTER TABLE [dbo].[SQSiteDesignTheme] CHECK CONSTRAINT [FK_SCSiteDesignTheme_SCSiteDesign]
GO
/****** Object:  ForeignKey [FK_SCSiteCreation_SCSiteDesignTheme]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SCSiteCreation_SCSiteDesignTheme]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueue]'))
ALTER TABLE [dbo].[SQSiteQueue]  WITH CHECK ADD  CONSTRAINT [FK_SCSiteCreation_SCSiteDesignTheme] FOREIGN KEY([SiteDesignThemeId])
REFERENCES [dbo].[SQSiteDesignTheme] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SCSiteCreation_SCSiteDesignTheme]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueue]'))
ALTER TABLE [dbo].[SQSiteQueue] CHECK CONSTRAINT [FK_SCSiteCreation_SCSiteDesignTheme]
GO
/****** Object:  ForeignKey [FK_SQSiteQueueData_SQSiteQueue]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueData_SQSiteQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueData]'))
ALTER TABLE [dbo].[SQSiteQueueData]  WITH CHECK ADD  CONSTRAINT [FK_SQSiteQueueData_SQSiteQueue] FOREIGN KEY([SiteQueueId])
REFERENCES [dbo].[SQSiteQueue] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueData_SQSiteQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueData]'))
ALTER TABLE [dbo].[SQSiteQueueData] CHECK CONSTRAINT [FK_SQSiteQueueData_SQSiteQueue]
GO
/****** Object:  ForeignKey [FK_SQSiteQueueRunHistory_SQSiteQueue]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueRunHistory_SQSiteQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueRunHistory]'))
ALTER TABLE [dbo].[SQSiteQueueRunHistory]  WITH CHECK ADD  CONSTRAINT [FK_SQSiteQueueRunHistory_SQSiteQueue] FOREIGN KEY([SiteQueueId])
REFERENCES [dbo].[SQSiteQueue] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueRunHistory_SQSiteQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueRunHistory]'))
ALTER TABLE [dbo].[SQSiteQueueRunHistory] CHECK CONSTRAINT [FK_SQSiteQueueRunHistory_SQSiteQueue]
GO
/****** Object:  ForeignKey [FK_SQSiteQueueUser_SQSiteQueue]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueUser_SQSiteQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueUser]'))
ALTER TABLE [dbo].[SQSiteQueueUser]  WITH CHECK ADD  CONSTRAINT [FK_SQSiteQueueUser_SQSiteQueue] FOREIGN KEY([SiteQueueId])
REFERENCES [dbo].[SQSiteQueue] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueUser_SQSiteQueue]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueUser]'))
ALTER TABLE [dbo].[SQSiteQueueUser] CHECK CONSTRAINT [FK_SQSiteQueueUser_SQSiteQueue]
GO
/****** Object:  ForeignKey [FK_SQSiteQueueUserData_SQSiteQueueUser]    Script Date: 05/22/2014 16:32:21 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueUserData_SQSiteQueueUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueUserData]'))
ALTER TABLE [dbo].[SQSiteQueueUserData]  WITH CHECK ADD  CONSTRAINT [FK_SQSiteQueueUserData_SQSiteQueueUser] FOREIGN KEY([SQUserId])
REFERENCES [dbo].[SQSiteQueueUser] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SQSiteQueueUserData_SQSiteQueueUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[SQSiteQueueUserData]'))
ALTER TABLE [dbo].[SQSiteQueueUserData] CHECK CONSTRAINT [FK_SQSiteQueueUserData_SQSiteQueueUser]
GO


--Site settings
DECLARE @SiteId UNIQUEIDENTIFIER,@SettingTypeId INT,@Sequence INT

DECLARE SITE_CURSOR___ Cursor 
FOR
SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId -- Only Master sites

Open SITE_CURSOR___ 
Fetch NEXT FROM SITE_CURSOR___ INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'IncludeParentSite')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
	BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('IncludeParentSite', 1, 1, @Sequence)
	END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'0')		
	END

	Fetch NEXT FROM SITE_CURSOR___ INTO @SiteId 		
END

CLOSE SITE_CURSOR___
DEALLOCATE SITE_CURSOR___

GO

ALTER PROCEDURE [dbo].[Site_GetAncestorSites]            
(            
@SiteId uniqueIdentifier          
)            
AS            
BEGIN            
    
    
 DECLARE @DeleteStatus int          
 SET @DeleteStatus = dbo.GetDeleteStatus()        
    
 DECLARE @AllowSiblingAccess BIT      
    
 SELECT @AllowSiblingAccess = [VALUE] FROM STSiteSetting SS JOIN STSettingType ST ON ST.Id = SS.SettingTypeId AND ST.Name='AllowSiblingSiteAccess'     
     
    
IF (@AllowSiblingAccess = 1)    
    
BEGIN    
    
 ;WITH siteAncestorHierarchy AS (          
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]          
 ,MasterSiteId          
 FROM SISite S          
 where Id = @SiteId          
     
 UNION ALL           
     
 SELECT S1.[Id]          
 ,S1.[Title]          
 ,S1.[Description]          
 ,S1.[URL]          
 ,S1.[VirtualPath]          
 ,S1.[ParentSiteId]          
 ,S1.[Type]          
 ,S1.[Keywords]          
 ,S1.[CreatedBy]          
 ,S1.[CreatedDate]          
 ,S1.[ModifiedBy]          
 ,S1.[ModifiedDate]          
 ,S1.[Status]          
 ,S1.[LftValue]          
 ,S1.[RgtValue]          
 ,S1.MasterSiteId          
 FROM SISite S1           
 INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId           
 WHERE S1.Status != @DeleteStatus          
 )          
    
    
    
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]          
 ,MasterSiteId      
 ,CU.UserFullName CreatedByFullName          
 ,MU.UserFullName ModifiedByFullName          
          
 FROM siteAncestorHierarchy S1          
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S1.CreatedBy            
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S1.ModifiedBy            
 WHERE Id != @SiteId       
     
 UNION ALL    
     
 SELECT  [Id]          
 ,[TITLE]          
 ,[DESCRIPTION]          
 ,[URL]          
 ,[VIRTUALPATH]          
 ,[PARENTSITEID]          
 ,[TYPE]          
 ,[KEYWORDS]          
 ,[CREATEDBY]          
 ,[CREATEDDATE]          
 ,[MODIFIEDBY]          
 ,[MODIFIEDDATE]          
 ,[STATUS]          
 ,[LFTVALUE]          
 ,[RGTVALUE]    
 ,MASTERSITEID        
 ,CreatedByFullName          
 ,ModifiedByFullName        
 FROM [GetSiblingSites](@SITEID)    
  
 UNION ALL  
   
 SELECT  [Id]          
 ,[TITLE]          
 ,[DESCRIPTION]          
 ,[URL]          
 ,[VIRTUALPATH]          
 ,[PARENTSITEID]          
 ,[TYPE]          
 ,[KEYWORDS]          
 ,[CREATEDBY]          
 ,[CREATEDDATE]          
 ,[MODIFIEDBY]          
 ,[MODIFIEDDATE]          
 ,[STATUS]          
 ,[LFTVALUE]          
 ,[RGTVALUE]    
 ,MASTERSITEID        
 ,CreatedByFullName          
 ,ModifiedByFullName        
 FROM [GetChildSites](@SITEID)              
     
END    
     
ELSE    
     
BEGIN    
  
      
 ;WITH siteAncestorHierarchy AS (          
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]          
 ,MasterSiteId          
 FROM SISite S          
 where Id = @SiteId          
     
 UNION ALL           
     
 SELECT S1.[Id]          
 ,S1.[Title]    
 ,S1.[Description]          
 ,S1.[URL]          
 ,S1.[VirtualPath]          
 ,S1.[ParentSiteId]          
 ,S1.[Type]          
 ,S1.[Keywords]          
 ,S1.[CreatedBy]          
 ,S1.[CreatedDate]          
 ,S1.[ModifiedBy]          
 ,S1.[ModifiedDate]          
 ,S1.[Status]          
 ,S1.[LftValue]          
 ,S1.[RgtValue]          
 ,S1.MasterSiteId          
 FROM SISite S1           
 INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId           
 WHERE S1.Status != @DeleteStatus          
 )          
    
    
    
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]      
 ,MasterSiteId      
 ,CU.UserFullName CreatedByFullName          
 ,MU.UserFullName ModifiedByFullName          
 FROM siteAncestorHierarchy S1          
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S1.CreatedBy            
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S1.ModifiedBy            
 where Id != @SiteId     
   
 UNION ALL  
   
 SELECT  [Id]          
 ,[TITLE]          
 ,[DESCRIPTION]          
 ,[URL]          
 ,[VIRTUALPATH]          
 ,[PARENTSITEID]          
 ,[TYPE]          
 ,[KEYWORDS]          
 ,[CREATEDBY]          
 ,[CREATEDDATE]          
 ,[MODIFIEDBY]          
 ,[MODIFIEDDATE]          
 ,[STATUS]          
 ,[LFTVALUE]          
 ,[RGTVALUE]    
 ,MASTERSITEID
 ,CreatedByFullName          
 ,ModifiedByFullName        
 FROM [GetChildSites](@SITEID)     
     
END    
    
END      
GO

ALTER FUNCTION [dbo].[GetVariantSitesForUser](@UserId uniqueidentifier)
RETURNS 
@SitesTable TABLE 
(
      SiteId uniqueidentifier
)
AS
BEGIN
      --DECLARE @siteTable as SiteTableType 
      
      --INSERT INTO @siteTable
      --SELECT    DISTINCT SiteId AS ObjectId
      --FROM      dbo.USSiteUser US
      --INNER JOIN SISite S ON US.SiteId = S.Id
      --WHERE     UserId = @UserId
      
      --INSERT INTO @SitesTable           
      --SELECT Distinct ObjectId from [dbo].[GetVariantSitesHavingPermission](@siteTable)   
      
      DECLARE @tbSites SiteTableType
      
      -- Master Sites
      DECLARE @tbMasterSites SiteTableType
      INSERT INTO @tbMasterSites
      SELECT SiteId FROM USSiteUser U
            JOIN SISite S ON U.SiteId = S.Id
      WHERE S.Id = S.MasterSiteId AND U.UserId = @UserId
      
      INSERT INTO @tbSites
      SELECT DISTINCT Id FROM SISite S
            JOIN @tbMasterSites T ON S.MasterSiteId = T.ObjectId
      
      -- Variant Sites
      DECLARE @tbVariantSites SiteTableType
      INSERT INTO @tbVariantSites
      SELECT SiteId FROM USSiteUser U
            JOIN SISite S ON U.SiteId = S.Id
      WHERE S.Id != S.MasterSiteId AND U.UserId = @UserId
            AND S.MasterSiteId NOT IN (SELECT ObjectId FROM @tbMasterSites)
      
      --DECLARE @VariantSiteId uniqueidentifier
      --SET @VariantSiteId = (SELECT TOP 1 ObjectId FROM @tbVariantSites)
      --INSERT INTO @tbSites
      --SELECT SiteId FROM dbo.GetAncestorSites(@VariantSiteId) A
      --    EXCEPT (SELECT Id FROM SISite WHERE Id = MasterSiteId)
      INSERT INTO @tbSites
      SELECT A.SiteId FROM @tbVariantSites T
            CROSS APPLY dbo.GetAncestorSites(T.ObjectId) A
      EXCEPT (SELECT Id FROM SISite WHERE Id = MasterSiteId)
      
      
       -- To remove the parent site to be shown in the drop down
        DECLARE @IncludeParentSite BIT  
        SET @IncludeParentSite = 0
        
        SELECT @IncludeParentSite = [VALUE]   
		FROM STSiteSetting SS JOIN STSettingType ST   
		ON ST.Id = SS.SettingTypeId   
		 AND ST.Name='IncludeParentSite'  
     
     
     IF(@IncludeParentSite = 0)
      DELETE from @tbSites where ObjectId IN (
      SELECT ParentSiteId from @tbSites TS
      INNER JOIN SISite  S ON S.Id = TS.ObjectId)
      
      
      -- Direct Sites
      INSERT INTO @tbSites
      SELECT SiteId FROM USSiteUser U
            JOIN SISite S ON U.SiteId = S.Id
      WHERE UserId = @UserId
      
      INSERT INTO @SitesTable
      SELECT DISTINCT ObjectId FROM @tbSites
      
      RETURN 
END
GO
ALTER procedure [dbo].[Site_CreateMicroSiteData]
(
	@ProductId uniqueidentifier,
	@MasterSiteId uniqueidentifier,
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser bit,
	@PageImportOptions int =2,
	@ImportContentStructure bit =0,
	@ImportFileStructure bit =0,
	@ImportImageStructure bit =0,
	@ImportFormsStructure bit =0
)
AS
BEGIN
	
	DECLARE @now datetime
	SET @now = GETUTCDATE()
	
	--CMSFrontUSer
	IF(@ImportAllWebSiteUser = 1)
	BEGIN			
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId,
			SU.UserId,
			SU.ISSystemUser,
			SU.ProductId
		FROM USUser U 
			INNER JOIN USSiteUser SU on U.Id = SU.UserId  
			INNER JOIN USMembership M on SU.UserId = M.UserId  
		WHERE SU.SiteId = @MasterSiteId AND SU.ProductId = @ProductId  
			AND SU.IsSystemUser = 0 AND  U.Status = 1 
			AND M.IsApproved = 1 AND M.IsLockedOut= 0 AND u.ExpiryDate >= @now
	END
	
-- This will not insert any record as we have not inserted any record in USSiteuser table
	INSERT INTO USMemberGroup(MemberId, MemberType, GroupId, ApplicationId) 
	SELECT SU.UserId, 1, MG.GroupId, @MicroSiteId  
		FROM USSiteUser SU 
			INNER JOIN USMemberGroup MG on SU.UserId = MG.MemberId 
	WHERE SU.SiteId = @MicroSiteId 
		AND MG.ApplicationId = @MasterSiteId --and MG.MemberType=1
	
	-- This is not required now, as it will inherit permision from parent site
	--****Admin USer****
	IF (@ImportAllAdminUserAndPermission=0)
	BEGIN
		-- Giving permission to Default users. Allow only CMS , Analyzer and Social product default users
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId] 
		FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MasterSiteId AND ProductId in ('CBB702AC-C8F1-4C35-B2DC-839C26E39848','CAB9FF78-9440-46C3-A960-27F53D743D89','CED1F2B6-A3FF-4B82-A856-1583FA811906')
		Except
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId]  FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MicroSiteId 	
			
		INSERT INTO [dbo].[USMemberGroup]
		   ([ApplicationId]
		   ,[MemberId]
		   ,[MemberType]
		   ,[GroupId])   
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MasterSiteId
				AND GroupId IN (SELECT Id FROM USGroup WHERE ApplicationId = ProductId)
		Except
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MicroSiteId				
	END

	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,@PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,7,@ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,9,@ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,33,@ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,38,@ImportFormsStructure
	
	
	--HSStructure data for Blog
	Declare @hsParentId uniqueidentifier,@SiteDirectoryObjectTypeId int
	Declare @RgtValue bigint , @SiteTitle nvarchar(1000)

	set @hsParentId  = dbo.GetNetEditorId();
	SELECT @RgtValue = RgtValue FROM HSStructure WHERE Id=@hsParentId
	SET @SiteDirectoryObjectTypeId= dbo.GetSiteDirectoryObjectType()

	IF NOT EXISTS(SELECT 1 FROM COTaxonomy WHERE ApplicationId=@MicroSiteId)
	BEGIN
		--Create the default node for the INDEX TERMS
		INSERT INTO COTaxonomy
		(
			Id,
			ApplicationId,
			Title,
			Description,
			CreatedDate,
			CreatedBy,
			ModifiedBy,
			ModifiedDate,
			Status,
			LftValue,
			RgtValue,
			ParentId
		)
		VALUES
		(
			NEWID(),
			@MicroSiteId,
			'Index Terms',
			'Index Terms',
			@now,
			(SELECT TOP 1 Id FROM USUser WHERE UserName='iappssystemuser'),
			NULL,
			NULL,
			1,
			1,
			2,
			NULL
		)
	END

	IF ((@RgtValue IS NOT NULL)
		AND (NOT EXISTS(SELECT 1 FROM HSStructure WHERE Title='Blog' And SiteId = @MicroSiteId )))
	BEGIN
		Set @RgtValue = 1
		
		Select @SiteTitle=Title from sisite where Id=@MicroSiteId

		if not exists(select * from Hsstructure where id=@MicroSiteId)
		begin
			INSERT INTO HSStructure(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@MicroSiteId,@SiteTitle,dbo.GetSiteObjectType(),null,@hsParentId,@RgtValue,@RgtValue+37,@MicroSiteId)
		end
		Declare @L1Id uniqueidentifier
		SELECT @L1Id =newid()
		INSERT INTO HSStructure
		(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L1Id,'Blog',@SiteDirectoryObjectTypeId,null,@MicroSiteId,@RgtValue+35,@RgtValue+36,@MicroSiteId)

		EXEC	[dbo].[SiteDirectory_Save]
		@Id = @L1Id OUTPUT,
		@ApplicationId = @MicroSiteId,
		@Title = N'Blog',
		@Description = NULL,
		@ModifiedBy = @ModifiedBy,
		@ModifiedDate = NULL,
		@ObjectTypeId = 39,
		@PhysicalPath = NULL,
		@VirtualPath = N'~/Blog'	
		-- Inserting to default Parent table
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@MicroSiteId,39,@L1Id)	
	End
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		
	
	EXEC [Site_CreateTemplateBuilderData] @MicroSiteId

	-- This might be not required now
	--EXEC [CMSSite_InsertDefaultPermission] @MicroSiteId	
		

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_Save]
(
	@XmlString		xml,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@VirtualPath	nvarchar(max)  OUT,
	@SiteName		nvarchar(256) OUT
)
AS
BEGIN
	DECLARE @tbDirectory TYDirectory	
	INSERT INTO @tbDirectory(Id,
		Title,
		Description,
		Attributes,
		FolderIconPath,
		PhysicalPath,
		[Size],
		Status,
		ThumbnailImagePath,
		ObjectTypeId,
		ParentId,
		Lft,
		Rgt,
		IsSystem,
		IsMarketierDir,
		AllowAccessInChildrenSites,
		SiteId,
		MicrositeId)						
	SELECT  tab.col.value('@Id[1]','uniqueidentifier') Id,
		tab.col.value('@Title[1]','NVarchar(256)')Title,
		tab.col.value('@Description[1]','Nvarchar(1024)')Description,
		tab.col.value('@Attributes[1]','int')Attributes,
		tab.col.value('@FolderIconPath[1]','nvarchar(4000)')FolderIconPath,
		tab.col.value('@PhysicalPath[1]','nvarchar(4000)')PhysicalPath,
		tab.col.value('@Size[1]','bigint')[Size],
		tab.col.value('@Status[1]','int')Status,
		tab.col.value('@ThumbnailImagePath[1]','nvarchar(4000)')ThumbnailImagePath,
		tab.col.value('@ObjectTypeId[1]','int')ObjectTypeId,
		tab.col.value('@ParentId[1]','uniqueidentifier')ParentId,
		tab.col.value('@Lft[1]','bigint')Lft,
		tab.col.value('@Rgt[1]','bigint')Rgt,
		tab.col.value('@IsSystem[1]','bit')IsSystem,
		tab.col.value('@IsMarketierDir[1]','bit')IsMarketierDir,
		ISNULL(tab.col.value('@AllowAccessInChildrenSites[1]','bit'),0) AllowAccessInChildrenSites,
		@ApplicationId,
		@MicroSiteId
	FROM @XmlString.nodes('/SiteDirectoryCollection/SiteDirectory') tab(col)
	Order By Lft

	-- if one of the node is content then everything is content, no mix and match
	DECLARE @ObjectTypeId int
	SET @ObjectTypeId = (SELECT TOP 1 ObjectTypeId FROM @tbDirectory)
	
	IF (@ObjectTypeId = 7)
		EXEC [dbo].[SqlDirectoryProvider_SaveContentDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 9)
		EXEC [dbo].[SqlDirectoryProvider_SaveFileDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 33)
		EXEC [dbo].[SqlDirectoryProvider_SaveImageDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 38)
		EXEC [dbo].[SqlDirectoryProvider_SaveFormDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE
	BEGIN
		-- All other libraries are shared
		  DECLARE @MasterSiteId uniqueidentifier  
		  SET @MasterSiteId = (SELECT MasterSiteId FROM SISite WHERE Id = @ApplicationId)  
		  IF (@MasterSiteId IS NOT NULL AND @MasterSiteId != dbo.GetEmptyGUID()  AND @ObjectTypeId <> 39)
		  BEGIN
		   SET @ApplicationId = @MasterSiteId  
		   UPDATE @tbDirectory Set SiteId = @ApplicationId
		  END
		EXEC [dbo].[SqlDirectoryProvider_SaveDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	END
	
	SET @VirtualPath = (SELECT Id, dbo.GetVirtualPathByObjectType(Id, @ObjectTypeId) VirtualPath, Attributes, SiteId 
							FROM @tbDirectory SISiteDirectory FOR XML Auto)
	
	SELECT @SiteName = Title FROM SISite WHERE Id = @ApplicationId
END
GO

--DATA MIGRATION
--This is to change all the variant sites' Taxonomy trees to be site specific instead of shared

--Changes for Content definition theming
ALTER PROCEDURE [dbo].[Content_GetContentsByDictionary] 
--********************************************************************************
--Parameters
--********************************************************************************
(
@xmlGuids xml,
@ThemeId uniqueidentifier = null,
@ApplicationId uniqueidentifier = null,
@ResolveThemeId bit = 0
)
AS
BEGIN

	IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)
			SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 13) 

	DECLARE @tbContentIds TABLE (Id uniqueidentifier, ContentTypeId int)
	INSERT INTO @tbContentIds
	SELECT DISTINCT T.c.value('@Id','uniqueidentifier'), T.c.value('@ContentTypeID','int')
		FROM @xmlGuids.nodes('/Contents/Content') T(c)
		
	DECLARE @tbParentFolder TABLE(ContentId uniqueidentifier primary key, ParentId uniqueidentifier, FolderPath nvarchar(1000), ContentTypeID int)
	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COContentStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3

	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COImageStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3


	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COFileStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3
		
	DECLARE @tbContentDefinitionIds TABLE(TemplateId uniqueidentifier, SiteId uniqueidentifier)
	INSERT INTO @tbContentDefinitionIds
		SELECT DISTINCT XMLString.value('(//contentDefinition/@TemplateId)[1]','uniqueidentifier'), C.ApplicationId  
			FROM COContent C INNER JOIN @tbParentFolder t on C.Id = t.ContentId 
		WHERE ObjectTypeId = 13
			
	
	SELECT Id,
		ApplicationId,
		Title,
		Description, 
		[Text], 
		URL,
		URLType, 
		ObjectTypeId, 
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		ModifiedDate, 
		Status,
		RestoreDate, 
		BackupDate, 
		StatusChangedDate,
		PublishDate, 
		ArchiveDate, 
		XMLString,
		BinaryObject, 
		Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		OrderNo, SourceContentId,
		PA.ParentId ,
		PA.FolderPath  
	FROM COContent CO
		INNER JOIN @tbParentFolder PA ON CO.Id = PA.ContentId AND 
			(pa.ContentTypeID = 7 or pa.ContentTypeID = 13 ) and Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = CO.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = CO.ModifiedBy
	Order by OrderNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata,             
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COFileStructure PA ON C.ParentId = PA.Id	
				AND C.ObjectTypeId = 9 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN ON FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN ON MN.UserId = C.ModifiedBy
	Order by C.OrderNo
		
	IF(@ThemeId IS NOT NULL AND @ThemeId <> dbo.GetEmptyGUID())
	BEGIN
	SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			CASE WHEN @ThemeId IS NULL THEN xF.XsltString ELSE ISNULL(OT.CodeFile,xF.XsltString) END AS XsltString, 
			--ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			CASE WHEN @ThemeId IS NULL THEN xF.XSLTFileName ELSE ISNULL(OT.FileName,xF.XSLTFileName) END AS XSLTFileName, 
			CASE WHEN @ThemeId IS NULL THEN xF.CreatedDate ELSE ISNULL(OT.CreatedDate,xF.CreatedDate) END AS CreatedDate, 
			CASE WHEN @ThemeId IS NULL THEN xF.CreatedBy ELSE ISNULL(OT.CreatedBy,xF.CreatedBy) END AS CreatedBy, 
			CASE WHEN @ThemeId IS NULL THEN xF.ModifiedDate ELSE ISNULL(OT.ModifiedDate,xF.ModifiedDate) END AS ModifiedDate, 
			 CASE WHEN @ThemeId IS NULL THEN xF.ModifiedBy ELSE ISNULL(OT.ModifiedBy,xF.ModifiedBy) END AS ModifiedBy,  
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			OT.ThemeId
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId
		LEFT JOIN SIObjectTheme OT ON OT.ObjectId = xF.Id AND OT.ObjectTypeId = 13
		AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)
		
	END
	ELSE
	BEGIN
		SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			xF.XSLTFileName, xF.CreatedDate, xF.CreatedBy, 
			xF.ModifiedDate, xF.ModifiedBy, 
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId AND VX.Status != 3
	END
	
	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo,	C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COImageStructure PA ON C.ParentId = PA.Id
				AND C.ObjectTypeId = 33 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
		
	SELECT C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		dbo.GetEmptyGUID() ParentId ,
		'' FolderPath,
		V.BackgroundColor,
		V.DynamicStreaming,
		V.Height,
		V.IsUI,
		V.IsVideo,
		V.PlayerId,
		V.PlayerKey,
		V.VideoId,
		V.PlaylistId,
		V.VideoType,
		V.Width  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN COVideo V on V.ContentId=C.Id
		INNER JOIN @tbContentIds T ON T.Id = C.Id
				AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
END

GO
ALTER PROCEDURE [dbo].[XMLFormXslt_GetXslt] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier =null,
	@SiteId uniqueidentifier =null,
	@XmlFormId uniqueidentifier =null,
	@ThemeId uniqueidentifier,
	@ResolveThemeId bit = null
)
as
Begin

DECLARE @EmptyGUID uniqueidentifier
SET @EmptyGUID = dbo.GetEmptyGUID()

IF (@ResolveThemeId IS NULL)
	Set  @ResolveThemeId  = 0  

	IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)
			SELECT @ThemeId = dbo.GetThemeId(@SiteId, 13) 

	select  A.Id,
			SiteId,
			XmlFormId,
			CASE WHEN @ThemeId IS NULL THEN A.XsltString ELSE OT.CodeFile END AS XsltString,
			A.Status,
			CASE WHEN @ThemeId IS NULL THEN A.XSLTFileName ELSE OT.FileName END AS XSLTFileName, 
			CASE WHEN @ThemeId IS NULL THEN A.CreatedDate ELSE OT.CreatedDate END AS CreatedDate, 
			CASE WHEN @ThemeId IS NULL THEN A.CreatedBy ELSE OT.CreatedBy END AS CreatedBy, 
			CASE WHEN @ThemeId IS NULL THEN A.ModifiedDate ELSE OT.ModifiedDate END AS ModifiedDate, 
			 CASE WHEN @ThemeId IS NULL THEN A.ModifiedBy ELSE OT.ModifiedBy END AS ModifiedBy,   
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			IsDefault	
			FROM COXMLFormXslt A 
			LEFT JOIN SIObjectTheme OT ON OT.ObjectId = A.Id AND OT.ObjectTypeId = 13
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
	where A.Id = Isnull(@Id, A.Id)
		  and
		  SiteId = Isnull(@SiteId, SiteId)
		  and
		  XmlFormId = Isnull(@XmlFormId, XmlFormId)
		  and
		  A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()
end
GO
ALTER PROCEDURE [dbo].[XMLFormXslt_Save]  
(  
 @Id uniqueidentifier  =null output,  
 @SiteId uniqueidentifier,  
 @XmlFormId uniqueidentifier,  
 @XsltFileName nvarchar(max),  
 @XsltString ntext=null,  
 @Status int,  
 @IsDefault bit,  
 @CreatedBy uniqueidentifier,  
 @ModifiedBy uniqueidentifier = null,  
 @ModifiedDate datetime ,
 @ThemeId uniqueidentifier 
 )  
As  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE  
 @NewId  uniqueidentifier,  
 @RowCount  INT,  
 @Stmt   VARCHAR(36),   
 @Error   int,  
 @Now datetime  
   
--********************************************************************************  
-- code  
--********************************************************************************  
   
BEGIN  
 /* if ID specified, ensure exists */  
 IF (@Id IS NOT NULL)  
  IF (NOT EXISTS(SELECT 1 FROM COXMLFormXslt WHERE  Id = @Id))  
  BEGIN  
   set @Stmt = convert(varchar(36),@Id)  
   RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
   RETURN dbo.GetBusinessRuleErrorCode()  
  END    
  
 SET @Now = getdate()  
   
 IF (@Id IS NULL AND @ThemeId = dbo.GetEmptyGUID())   -- new insert  
 BEGIN  
  SET @Stmt = 'Create XMLFormXslt'  
  Select @NewId =newid()  
  set @Status = dbo.GetActiveStatus()  
  INSERT INTO COXMLFormXslt  WITH (ROWLOCK)(  
   Id,  
   SiteId,
   XmlFormId,  
   XsltString,  
   Status,  
   XSLTFileName,  
   IsDefault,  
   CreatedBy,  
   CreatedDate,
   IsThemed     
  )  
  values  
   (  
    @NewId,  
    @SiteId,
    @XmlFormId,  
    @XsltString,  
    @Status,  
    @XSLTFileName,  
    @IsDefault,  
    @CreatedBy,  
    @Now ,
	CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END  
   )  
  SELECT @Error = @@ERROR  
   IF @Error <> 0  
  BEGIN  
   RAISERROR('DBERROR||%s',16,1,@Stmt)  
   RETURN dbo.GetDataBaseErrorCode()  
  END  
  
  SELECT @Id = @NewId  
    
  IF (@IsDefault = 1) --OR (SELECT Count(*) FROM COXMLFormXslt Where XMLFormId = @XmlFormId) = 1)  
  BEGIN  
   --UPDATE COXMLForm SET XsltFileName = @XsltFileName, XsltString = @XsltString  
   --WHERE   
   -- COXMLForm.Id =  @XmlFormId   
      
   UPDATE  COXMLFormXslt SET IsDefault = 1 WHERE XMLFormId = @XmlFormId AND Id = @Id AND SiteId = @SiteId  
   --- Update Other Records   
   UPDATE  COXMLFormXslt SET IsDefault = 0 WHERE XMLFormId = @XmlFormId AND Id <> @Id AND SiteId = @SiteId
  END  
 END  
 ELSE   -- Update  
 BEGIN  
  SET @Stmt = 'Modify XMLFormXslt'  
  IF ( @ThemeId = dbo.GetEmptyGUID())
BEGIN

  Update COXMLFormXslt WITH (ROWLOCK)  
  set    
  SiteId = @SiteId,
   XmlFormId = @XmlFormId,  
   XsltString = @XsltString,  
   Status = @Status,  
   XSLTFileName = @XSLTFileName,  
   IsDefault = @IsDefault,  
   ModifiedBy = @ModifiedBy,  
   ModifiedDate = @Now   
   where Id = @Id --AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)  
  END 
  ELSE
  BEGIN
			----Get the xslt object id if the theme id set
			--declare @xsltObjectId uniqueidentifier
			--select @xsltObjectId = Id FROM COXMLFormXslt where XmlFormId=@XmlFormId
			
			
			IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@XmlFormId AND ThemeId=@ThemeId)
			BEGIN
				UPDATE SIObjectTheme 
				SET ModifiedBy=@ModifiedBy,
			   ModifiedDate=@Now,
			   FileName= @XSLTFileName,
			   CodeFile = @XsltString,
			   Status =@Status
			   WHERE ObjectId=@XmlFormId AND ThemeId=@ThemeId
			END
			ELSE
			BEGIN
	
				IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@XmlFormId AND ThemeId=@ThemeId)
				BEGIN
					UPDATE SIObjectTheme SET FileName=@XSLTFileName,
					--Title=@Name, 
					CodeFile = @XsltString,ModifiedBy=@ModifiedBy,ModifiedDate=@Now WHERE ObjectId=@XmlFormId AND ThemeId=@ThemeId
				END
				ELSE
				BEGIN
					INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
						VALUES (NEWID(),@XmlFormId,@ThemeId,@XSLTFileName,@XsltString,13,@ModifiedBy,@Now,NULL,NULL,1,NULL) -- 3 for template
				END		
				UPDATE COXMLFormXslt SET IsThemed=1 WHERE Id=@Id
			END	
		END
  SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT  
  
  IF @Error <> 0  
  BEGIN  
   RAISERROR('DBERROR||%s',16,1,@Stmt)  
   RETURN dbo.GetDataBaseErrorCode()  
  END  
  
  --IF @RowCount = 0  
  --BEGIN  
  -- --concurrency error  
  -- RAISERROR('DBCONCURRENCY',16,1)  
  -- RETURN dbo.GetDataConcurrencyErrorCode()  
  --END   
 END    
END  
--********************************************************************************  
GO
ALTER PROCEDURE [dbo].[XMLForm_GetXMLForm]   
--********************************************************************************  
-- Input Parameters  
--********************************************************************************  
(  
 @Id uniqueidentifier =null,  
 @ApplicationId uniqueidentifier = null,  
 @PageSize int=null,  
 @PageIndex int=null,  
 @ThemeId uniqueidentifier,  
 @ResolveThemeId bit = null  
)  
as  
Begin  
  
DECLARE @EmptyGUID uniqueidentifier  
SET @EmptyGUID = dbo.GetEmptyGUID()  
  
IF (@ResolveThemeId IS NULL)  
 Set  @ResolveThemeId  = 0    
  
 If (@PageSize is null and @PageIndex is null)   
 Begin  
  
  IF(@ThemeId IS NOT NULL AND @ResolveThemeId = 1)  
   SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 13)   
  If(@Id is NOT NULL)
	BEGIN
		IF NOT EXISTS(Select 1 FROM SIObjectTheme where ObjectId = @Id and ThemeId = @ThemeId)
		BEGIN
			SET @ThemeId = NULL
		END
	  select A.Id,  
		ApplicationId,  
		Name,  
		XMLString,  
		CASE WHEN @ThemeId IS NULL THEN A.XsltString ELSE OT.CodeFile END AS XsltString,  
		A.Status,  
		Description,  
		A.XMLFileName,  
		CASE WHEN @ThemeId IS NULL THEN A.XSLTFileName ELSE OT.FileName END AS XSLTFileName,   
		A.CreatedDate,   
		A.CreatedBy,   
		A.ModifiedDate, A.ModifiedBy,   
		FN.UserFullName CreatedByFullName,  
		MN.UserFullName ModifiedByFullName,  
		@ThemeId  
		FROM COXMLForm A  
		LEFT JOIN SIObjectTheme OT ON OT.ObjectId = A.Id  AND OT.ObjectTypeId = 13  
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
	  where A.Id = Isnull(@Id, A.Id)  
		 and  
		 (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
		 and  
		 A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()  
		 AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId = @ThemeId)  
	END		 
	ELSE
		BEGIN
			with CTEXmlFormId(Id)
			AS(
			Select Id FROM COXMLForm A where 
			(@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) )  
			
			select  A.Id,  
			ApplicationId,  
			Name,  
			XMLString,  
			A.XsltString AS XsltString,  
			A.Status,  
			Description,  
			A.XMLFileName,  
			A.XSLTFileName AS XSLTFileName,   
			A.CreatedDate,   
			A.CreatedBy,   
			A.ModifiedDate, A.ModifiedBy,   
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,  
			@ThemeId  
			FROM COXMLForm A  
			INNER JOIN CTEXmlFormId X ON A.Id = X.Id
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		  where   
			 (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
			 and  
			 A.Status != dbo.GetDeleteStatus() --= dbo.GetActiveStatus()  
		 
		END	 
 end  
 else    
 Begin  
  select  Id,  
    ApplicationId,  
    Name,  
    XMLString,  
    XsltString,  
    Status,  
    Description,  
    XMLFileName,  
    XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
    CreatedByFullName, ModifiedByFullName   
  from (select ROW_NUMBER() over (order by Id) as RowId,  
      Id,  
      ApplicationId,  
      Name,  
      XMLString,  
      XsltString,  
      Status,  
      Description,  
      XMLFileName,  
      XSLTFileName, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
      FN.UserFullName CreatedByFullName,  
      MN.UserFullName ModifiedByFullName  
    from COXMLForm A  
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
    LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
    where Id = Isnull(@Id, Id)  
       and  
       (@ApplicationId is null or A.ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)))   
       and  
       Status != dbo.GetDeleteStatus()) --= dbo.GetActiveStatus()) -- it will change after creating updateStatus() mehod  
   as XMLFormWithRowNumbers  
  where RowId between ((@PageIndex -1) * @PageSize +1) and (@PageIndex * @PageSize)  
    
 end  
end  
GO
ALTER PROCEDURE [dbo].[XMLForm_Save] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier =null output,
	@ApplicationId uniqueidentifier,
	@Name nvarchar(256),
	@Description nvarchar(1024)=null,
	@XMLString xml =null,
	@XsltString ntext=null,
	@Status int,
	@XMLFileName nvarchar(1024)=null,
	@XSLTFileName nvarchar(1024)=null,
	@CreatedBy uniqueidentifier,
    @ModifiedBy uniqueidentifier = null,
    @ModifiedDate datetime,
	@ThemeId uniqueidentifier = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
	@NewId		uniqueidentifier,
	@RowCount		INT,
	@Stmt 		VARCHAR(36),	
	@Error 		int,
	@Now datetime

--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM   COXMLForm WHERE  Id = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END		
		

	SET @Now = getdate()
	
	IF (@Id IS NULL AND @ThemeId = dbo.GetEmptyGUID())			-- new insert
	BEGIN
		SET @Stmt = 'Create XMLForm'
		Select @NewId =newid()
		set @Status = dbo.GetActiveStatus()
		INSERT INTO COXMLForm  WITH (ROWLOCK)(
			Id,
			ApplicationId,
			Name,
			XMLString,
			XsltString,
			Status,
			Description,
			XMLFileName,
			XSLTFileName,
			CreatedBy,
			CreatedDate,
			IsThemed			
		)
		values
			(
				@NewId,
				@ApplicationId,
				@Name,
				@XMLString,
				@XsltString,
				@Status,
				@Description,
				@XMLFileName,
				@XSLTFileName,
				@CreatedBy,
				@Now,
				CASE WHEN @ThemeId IS  NULL OR @ThemeId = dbo.GetEmptyGUID() THEN 0 ELSE 1 END 
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		SELECT @Id = @NewId
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify XMLForm'
		IF ( @ThemeId = dbo.GetEmptyGUID())
		BEGIN

		Update COXMLForm WITH (ROWLOCK)
		Set 	
			--ApplicationId = @ApplicationId,
			Name = @Name,
			Status = @Status,
			Description = @Description,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @Now, 
			XMLString = @XMLString,
			XMLFileName = @XMLFileName,				
			
			XsltString = 
				Case 
				WHEN ApplicationId = @ApplicationId
				THEN @XsltString
				ELSE XsltString
				END,			
			XSLTFileName = 
				Case 
				WHEN ApplicationId = @ApplicationId
				Then @XSLTFileName
				Else XSLTFileName
				End
		Where Id = @Id --AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

		END
		ELSE
			BEGIN
				IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
				BEGIN
					UPDATE SIObjectTheme 
					SET ModifiedBy=@ModifiedBy,
				   ModifiedDate=@Now,
				   FileName= @XSLTFileName,
				   CodeFile = @XsltString,
				   Status =@Status
				   WHERE ObjectId=@Id AND ThemeId=@ThemeId
				END
				ELSE
				BEGIN
		
					IF EXISTS(SELECT 1 FROM SIObjectTheme WHERE ObjectId=@Id AND ThemeId=@ThemeId)
					BEGIN
						UPDATE SIObjectTheme SET FileName=@XSLTFileName,Title=@Name, CodeFile = @XsltString,ModifiedBy=@ModifiedBy,ModifiedDate=@Now WHERE ObjectId=@Id AND ThemeId=@ThemeId
					END
					ELSE
					BEGIN
						INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
							VALUES (NEWID(),@Id,@ThemeId,@XSLTFileName,@XsltString,13,@ModifiedBy,@Now,NULL,NULL,1,@Name) -- 3 for template
					END		
					UPDATE COXMLForm SET IsThemed=1 WHERE Id=@Id
				END	
			END

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		
END
--********************************************************************************


GO

/****** Object:  StoredProcedure [dbo].[SiteQueueRunHistory_Get]    Script Date: 06/11/2014 13:33:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SiteQueueRunHistory_Get]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'create procedure [dbo].[SiteQueueRunHistory_Get]
(
	@SiteQueueId uniqueidentifier =null
)
as
begin
	select * from SQSiteQueueRunHistory where @SiteQueueId is null or  SiteQueueId =@SiteQueueId order by RunDate desc
end' 
END
GO


    
ALTER PROCEDURE [dbo].[SiteSettings_GetSettings](@siteId uniqueidentifier)    
AS    
BEGIN  
SELECT     
 SG.[Name] as [GroupName],    
 SG.[FriendlyName] as [GroupFriendlyName],    
 SG.[Sequence] as [GroupSequence],    
 ST.[Name] as [TypeName],    
 ST.[FriendlyName] as [TypeFriendlyName],    
 ST.[Sequence] as [TypeSequence],    
 SS.[Value],    
 SS.SettingTypeId,    
 SS.SiteId    
FROM STSiteSetting SS     
 inner join STSettingType ST on SS.SettingTypeId = ST.Id     
 inner join STSettingGroup SG on ST.SettingGroupId=SG.Id    
 inner join SISite SI on SS.SiteId = SI.Id    
--WHERE SS.SiteId=@siteId or SI.ParentSiteId = @siteId    
ORDER BY SS.SiteId, SG.Sequence, ST.[Name]--, ST.Sequence    
  
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[SiteSettings_Save]    
(     
 @siteId uniqueidentifier,  
 @settingsXml xml,  
 @customSettingsXml xml=null,  
 @CustomSettingsGroupId int=null,
 @RemoveMissingSettings bit = 1
)    
    
AS    
BEGIN   
 declare @maxSequence int  
 DECLARE @tempSettings TABLE   
 (   
  settingType varchar(256),   
  settingValue varchar(max)  
 )  
  
 if (@CustomSettingsGroupId is  null) set @CustomSettingsGroupId = 2 -- if @CustomSettingsGroupId id null then it goes common custom settings group  
  
 DECLARE @tempInsertSettings TABLE   
 (   
  settingType varchar(256),   
  settingValue varchar(max),  
  sequence int identity(1,1)  
 )  
  
 Insert Into @tempSettings(settingType,settingValue)  
 Select tab.col.value('(key/string/text())[1]','varchar(256)'),tab.col.value('(value/string/text())[1]','varchar(max)')  
 FROM @settingsXml.nodes('dictionary/SerializableDictionary/item') tab(col)   
  
 --update existing settings  
 update S   
 set S.Value = isnull(TS.settingValue,'')  
 from STSiteSetting S   
 inner join STSettingType ST on S.SettingTypeId=ST.Id  
 inner join @tempSettings TS  
 on (ST.Name)=(TS.settingType)  
 where S.SiteId=@siteId  
  
--Inserting records which are not there in STSiteSettings but Type is already created for that
 DECLARE @tempInsertSiteSettings TABLE   
 (   
  siteId uniqueidentifier,   
  settingTypeId int,  
  value   varchar(max) 
 ) 
 
insert into @tempInsertSiteSettings  select @siteId, ty.Id, tmp.settingValue from @tempSettings tmp, STSettingType ty
where tmp.SettingType  = ty.Name and ty.Id not in (select settingTypeId from STSiteSetting Where SiteId = @siteId )  

INSERT INTO STSiteSetting (SiteId, SettingTypeId, Value) SELECT SiteId, SettingTypeId, Value FROM @tempInsertSiteSettings where Value is not null  
  
 --Custom settings  
 delete from @tempSettings  
  IF @customSettingsXml is not null
  BEGIN
	  
	 Insert Into @tempSettings(settingType,settingValue)  
	 Select tab.col.value('(key/string/text())[1]','varchar(256)'),tab.col.value('(value/string/text())[1]','varchar(max)')  
	 FROM @customSettingsXml.nodes('dictionary/SerializableDictionary/item') tab(col)   
	   
	 
	 --update existing custom settings  
	 update S   
	 set S.Value = isnull(TS.settingValue,'')  
	 from STSiteSetting S   
	 inner join STSettingType ST on S.SettingTypeId=ST.Id  
	 inner join @tempSettings TS  
	 on (ST.Name)=(TS.settingType)  
	 where S.SiteId=@siteId  

	 --insert new records from input xml  
	 select @maxSequence= max(Sequence) from STSettingType  
	 insert into @tempInsertSettings  
	 select settingType,settingValue from @tempSettings 
		where (settingType) in(select (settingType) from @tempSettings   except select (Name) from STSettingType)  
	 
	 insert into STSettingType (Name, FriendlyName, SettingGroupId,IsSiteOnly,Sequence) 
		select settingType,settingType,@CustomSettingsGroupId,1,@maxSequence +Sequence from @tempInsertSettings  
		
	--In case the setting type already existed because it was in there for a siteid, we will not be able to insert
	-- the value in for another siteid. So we need to pull all these setting types into the @tempInsertSettings table for the following join to 
	-- work
 
		delete from @tempInsertSettings
		insert into @tempInsertSettings  select settingType,settingValue from @tempSettings 
			where (settingType) in(select (settingType) from @tempSettings)

		insert into STSiteSetting 
		select @SiteId,ST.Id,isnull(TIS.settingValue,'') 
			from STSettingType ST inner join @tempInsertSettings TIS on ST.Name = TIS.settingType
			Where ST.Id not in (select SettingTypeId from STSiteSetting Where SiteId=@siteId)
  
	 --delete the records those are not in input xml  
	 IF	@RemoveMissingSettings = 1
		BEGIN
			 delete from STSiteSetting where SettingTypeId in (select Id from STSettingType where (Name) in (select (ST.Name) from  STSettingType ST except select (settingType) from @tempSettings)  and SettingGroupId=@CustomSettingsGroupId) And SiteId=@siteId   
			 delete from STSettingType where (Name) in (select (ST.Name) from  STSettingType ST except select (settingType) from @tempSettings)  and SettingGroupId=@CustomSettingsGroupId  And Id Not IN (SELECT SettingTypeId From STSiteSetting)
		END 
  END    

  select dbo.ClearCachedTimeZone(@siteId)  -- Clear CLR Time Zone 
END
    
--********************************************************************************

GO
IF COL_LENGTH(N'[dbo].[SITemplate]', N'IsNotShared') IS NULL
ALTER TABLE SITemplate ADD IsNotShared bit NULL
GO

Update SITemplate Set ISNotShared = 1 where Id IN ('A71679C2-1FEA-4D46-A760-0EC65469FB2A','79eb2711-bdb1-46b5-87c0-d9b567e05560','e2a93fce-6a36-4726-a239-a4d5d5b240f7','3f44b1c3-08c9-425b-8d53-9daa37a60969','c98250a8-ac70-43af-aa7a-7764d1c3d259')
GO

ALTER PROCEDURE [dbo].[Template_GetTemplate]      
(      
 @Id    uniqueIdentifier=null ,      
 @ApplicationId uniqueidentifier=null,      
 @Title   nvarchar(256)=null,      
 @FileName  nvarchar(4000)=null,      
 @Status   int=null,      
 @PageIndex  int=1,      
 @PageSize  int=null,      
 @Type   int=null,      
 @ThemeId  UNIQUEIDENTIFIER=null,      
 @ResolveThemeId bit = null       
 )      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
DECLARE @EmptyGUID uniqueidentifier      
      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
IF(@ResolveThemeId IS NULL)      
 SET @ResolveThemeId = 0      
SET @EmptyGUID = dbo.GetEmptyGUID()      
 IF @PageSize is Null      
 BEGIN      
   IF(@ResolveThemeId = 1)      
    SELECT @ThemeId = dbo.GetThemeId(@ApplicationId, 3)      
          
  IF(@ThemeId = dbo.GetEmptyGUID() OR @ThemeId IS NULL)      
  BEGIN      
  --Print 'In ThemeId is null'
	IF(@Id IS NOT NULL)
	BEGIN
		--PRINT 'In ID is not null'
		   SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
			FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml,      
		  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
		  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
			FN.UserFullName CreatedByFullName,      
			MN.UserFullName ModifiedByFullName,      
		  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId      
		  FROM SITemplate S       
		  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
		  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
		  INNER JOIN HSStructure H on S.Id = H.Id      
		  WHERE S.Id = isnull(@Id, S.Id) and      
		   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
		   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
		   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
		   S.Type=isnull(@Type,S.Type) and      
		   S.Status = isnull(@Status, S.Status)and       
			(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())      
		END	
		ELSE
		BEGIN	
			--PRINT 'In ID is null'
			 SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
			FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml,      
		  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
		  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
			FN.UserFullName CreatedByFullName,      
			MN.UserFullName ModifiedByFullName,      
		  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId      
		  FROM SITemplate S       
		  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
		  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
		  INNER JOIN HSStructure H on S.Id = H.Id      
		  WHERE S.Id = isnull(@Id, S.Id) and      
		   isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
		   isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
		   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
		   S.Type=isnull(@Type,S.Type) and      
		   S.Status = isnull(@Status, S.Status)and       
			(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())      
			AND (S.IsNotShared IS NULL OR S.IsNotShared = 0)
		END	
  END      
  ELSE      
  BEGIN  
     --Print 'In ThemeId is not null'
    SELECT S.Id,   
    CASE WHEN OT.Title IS NULL THEN S.Title ELSE OT.Title END AS Title,    
    S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate,      
    CASE WHEN OT.FileName IS NULL THEN S.FileName ELSE OT.FileName END AS FileName,  
    ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml,      
  (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,      
  (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,      
    FN.UserFullName CreatedByFullName,      
    MN.UserFullName ModifiedByFullName,      
  [dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, OT.CodeFile, H.ParentId      
  FROM SITemplate S       
  LEFT JOIN SIObjectTheme OT ON OT.ObjectId = S.Id AND OT.ObjectTypeId = 3  AND (@ThemeId IS NULL OR @ThemeId = dbo.GetEmptyGUID() OR OT.ThemeId=@ThemeId)  
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
  LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
  INNER JOIN HSStructure H on S.Id = H.Id      
  WHERE   
  S.Id = isnull(@Id, S.Id) and      
   isnull(upper(OT.Title),'') = isnull(upper(@Title), isnull(upper(OT.Title),'')) and      
   isnull(upper(OT. FileName),'') = isnull(upper(@FileName), isnull(upper(OT.FileName),'')) and      
   (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
   S.Type=isnull(@Type,S.Type) and      
   S.Status = isnull(@Status, S.Status)and       
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())   
  END      
        
 END      
       
 ELSE      
 BEGIN      
  WITH ResultEntries AS (       
   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)      
    AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,       
    ModifiedDate, FileName,ImageURL,Type,ApplicationId,      
 (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO ) StyleId,      
    (SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO ) ScriptId,      
    [dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml      
   FROM SITemplate S      
   WHERE S.Id = isnull(@Id, S.Id) and      
    isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and      
    isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and      
    isnull(S.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(S.ApplicationId,@EmptyGUID)) and      
    S.Type=isnull(@Type,S.Type) and      
    S.Status = isnull(@Status, S.Status)and       
    (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))      
      
  SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,      
    FileName,ImageURL,Type,ApplicationId, StyleId,ScriptId,      
    FN.UserFullName CreatedByFullName,      
    MN.UserFullName ModifiedByFullName,      
    ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, H.ParentId      
  FROM ResultEntries S      
  INNER JOIN HSStructure H on S.Id = H.Id      
  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy      
   LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy      
  WHERE Row between       
   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize      
      
 END      
      
END     
    
 GO 

 /****** Object:  StoredProcedure [dbo].[Site_CopyMenusAndPages]    Script Date: 8/13/2014 3:39:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Site_CopyMenusAndPages]
(@SiteId uniqueidentifier,
@MicroSiteId uniqueidentifier,
@ModifiedBy  uniqueidentifier,
@CopiedAllUsersFromMaster bit,
@PageImportOptions int =2
)
AS
BEGIN

if(@PageImportOptions = 0) -- Create default nodes root node and unassigned when selected dont import menus and pages
begin
	Insert into PageMapBase (SiteId, ModifiedDate)
		Values(@MicroSiteId, GETUTCDATE())
	
	DECLARE @pageMapNodeXml NVARCHAR(max)
	DECLARE @pageMapNodeId uniqueidentifier
	SET @pageMapNodeId = @MicroSiteId
	declare @SiteTitle nvarchar(512)
	set @SiteTitle = (select DisplayTitle  from PageMapNode where PageMapNodeId =@SiteId)
							 
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" description="' + @SiteTitle + '"  
		   displayTitle="' + @SiteTitle + '" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="0"   
		   targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
		   createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
		   propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="True"   
		   inheritRoles="False" roles="" securityLevels="" locationIdentifier="" />'  						 

	EXEC PageMapNode_Save @Id =@pageMapNodeId,
			@SiteId =@MicroSiteId,
			@ParentNodeId = NULL,
			@PageMapNode= @pageMapNodeXml,
			@CreatedBy = @ModifiedBy
			
	SET @pageMapNodeId = NEWID()
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
						 displayTitle="Unassigned" friendlyUrl="Unassigned" description="Unassigned" menuStatus="0" 
						 parentId="' + CAST(@MicroSiteId AS CHAR(36)) + '" 
					     targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
						 createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
						 propogateSecurityLevels="True" inheritSecurityLevels="False" 
						 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'

	EXEC PageMapNode_Save @Id = @pageMapNodeId,
			@SiteId = @MicroSiteId,
			@ParentNodeId = @MicroSiteId,
			@PageMapNode = @pageMapNodeXml,
			@CreatedBy = @ModifiedBy


end
else
begin


	declare @ImportMenusOnly bit 
	Set @ImportMenusOnly =0
	declare @MakePageStatusAsDraft bit 
	Set @MakePageStatusAsDraft = 1

	if(@PageImportOptions = 1) set @ImportMenusOnly = 1  -- Import only menus
	if(@PageImportOptions = 3) set @MakePageStatusAsDraft = 0 -- @PageImportOptions =3 Import menus and pages, page status same as master site page status, @PageImportOptions =2 import all pages as created page status and workflow state as draft

	IF (Select count(*)  FROM [dbo].[PageMapNode]   Where SiteId=@MicroSiteId ) =0
	BEGIN

		INSERT INTO [dbo].[PageMapNode]
			   ([PageMapNodeId]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
	           
			   ,[SiteId]
			   ,[ExcludeFromSiteMap]
			   ,[Description]
			   ,[DisplayTitle]
			   ,[FriendlyUrl]
			   ,[MenuStatus]
			   ,[TargetId]
			   ,[Target]
			   ,[TargetUrl]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PropogateWorkflow]
			   ,[InheritWorkflow]
			   ,[PropogateSecurityLevels]
			   ,[InheritSecurityLevels]
			   ,[PropogateRoles]
			   ,[InheritRoles]
			   ,[Roles]
			   ,[LocationIdentifier]
			   ,[HasRolloverImages]
			   ,[RolloverOnImage]
			   ,[RolloverOffImage]
			   ,[IsCommerceNav]
			   ,[AssociatedQueryId]
			   ,[DefaultContentId]
			   ,[AssociatedContentFolderId]
			   ,[CustomAttributes]
			   ,[HiddenFromNavigation]
			   ,SourcePageMapNodeId)
			   SELECT  case when parentId is null then @MicroSiteId else NEWID()end
		  ,[ParentId]
		  ,[LftValue]
		  ,[RgtValue]
	      
		  ,@MicroSiteId
		  ,[ExcludeFromSiteMap]
		  ,[Description]
		  ,[DisplayTitle]
		  ,[FriendlyUrl]
		  ,[MenuStatus]
		  ,[TargetId]
		  ,[Target]
		  ,[TargetUrl]
		  ,@ModifiedBy
		  ,GETUTCDATE()
		  ,@ModifiedBy
		  ,GETUTCDATE()
		  ,[PropogateWorkflow]
		  ,[InheritWorkflow]
		  ,[PropogateSecurityLevels]
		  ,[InheritSecurityLevels]
		  ,[PropogateRoles]
		  ,[InheritRoles]
		  ,[Roles]
		  ,[LocationIdentifier]
		  ,[HasRolloverImages]
		  ,[RolloverOnImage]
		  ,[RolloverOffImage]
		  ,[IsCommerceNav]
		  ,[AssociatedQueryId]
		  ,[DefaultContentId]
		  ,[AssociatedContentFolderId]
		  ,[CustomAttributes]
		  ,[HiddenFromNavigation]
		  ,PageMapNodeId
	  FROM [dbo].[PageMapNode]
	   Where SiteId=@SiteId
	   AND PageMapNodeId not in    
	   (select distinct P.PageMapNodeId 
	 FROM [dbo].[PageMapNode] P
	 cross  join PageMapNode DP 
	   Where P.SiteId=@SiteId and P.LftValue between DP.LftValue and DP.RgtValue
	   AND DP.PageMapNodeId   in 
	   (
		   select top 4 Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		Where Name in ('CommerceNavRootNodeId', 'ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId
		And Value is not null
		AND rtrim(ltrim(Value)) !=''
		union 
		select PageMapNodeId from  PageMapNode where lower(DisplayTitle) = 'storefront' and SiteId=@SiteId 
	   )   
	   )
	  
	  
	    
	--remove commerceproduct type and marketierpages menu group   
	delete from PageMapNode where SiteId = @MicroSiteId  and  SourcePageMapNodeId in 
	(select ParentId from PageMapNode where PageMapNodeId in 
		(select Top 2 Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		Where Name in ('ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId
		And Value is not null
		AND rtrim(ltrim(Value)) !=''))
		
		--remove commerceproduct type  and marketierpages menu group   subnodes if any exist(generally it should not)
	delete from PageMapNode where SiteId = @MicroSiteId  and  ParentId in 
	(select ParentId from PageMapNode where PageMapNodeId in 
		(select Top 2 Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		Where Name in ('ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId
		And Value is not null
		AND rtrim(ltrim(Value)) !=''))

		
	    
	   Update  M Set M.ParentId = P.PageMapNodeId
	   FROM PageMapNode M 
	   INNER JOIN PageMapNode P ON P.SourcePageMapNodeId = M.ParentId
	   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
	   
	   UPdate PN SET PN.TargetId=T.PageMapNodeId
		FROM PageMapNode PN
		INNER JOIN PageMapNode T ON PN.TargetId = T.SourcePageMapNodeId
		Where PN.Target='2' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

	 --  if(@CopiedAllUsersFromMaster = 1)
	 --  begin
		--   INSERT INTO [dbo].[PageMapNodeWorkflow]
		--		   ([Id]
		--		   ,[PageMapNodeId]
		--		   ,[WorkflowId]
		--		   ,[CustomAttributes])
		--SELECT NEWID()
		--	  ,P.[PageMapNodeId]
		--	  ,[WorkflowId]
		--	  ,W.[CustomAttributes]
		--  FROM [dbo].[PageMapNodeWorkflow] W
		--  INNER JOIN PageMapNode P ON W.PageMapNodeId = P.SourcePageMapNodeId
		--  Where P.SiteId = @MicroSiteId
	 -- end
	  
	  INSERT INTO [dbo].[USSecurityLevelObject]
			   ([ObjectId]
			   ,[ObjectTypeId]
			   ,[SecurityLevelId])
	  SELECT P.PageMapNodeId
			   ,ObjectTypeId
			   ,SecurityLevelId
	FROM [USSecurityLevelObject] SO
	INNER JOIN PageMapNode P ON SO.ObjectId = P.SourcePageMapNodeId
	 Where P.SiteId = @MicroSiteId

	 --import shared workflow mapping of pagemapnode and workflow
	INSERT INTO [dbo].[PageMapNodeWorkflow]
           ([Id]
           ,[PageMapNodeId]
           ,[WorkflowId]
           ,[CustomAttributes])
	select NEWID(),
		P.PageMapNodeId,
		PW.WorkflowId,
		null
	FROM [dbo].[PageMapNodeWorkflow] PW
		INNER JOIN PageMapNode P ON PW.PageMapNodeId = P.SourcePageMapNodeId
		inner join WFWorkflow W on PW.WorkflowId = W.Id 
		 Where P.SiteId = @MicroSiteId  and W.IsShared=1 



	if(@CopiedAllUsersFromMaster = 1)
	   begin
	   -- This is updating the custome permission page map node id
		  INSERT INTO [dbo].[USMemberRoles]
				   ([ProductId]
				   ,[ApplicationId]
				   ,[MemberId]
				   ,[MemberType]
				   ,[RoleId]
				   ,[ObjectId]
				   ,[ObjectTypeId]
				   ,[Propagate]
				   )
		SELECT Distinct [ProductId]
			  ,@MicroSiteId
			  ,[MemberId]
			  ,[MemberType]
			  ,[RoleId]
			  ,P.PageMapNodeId
			  ,[ObjectTypeId]
			  ,[Propagate]
		  FROM [dbo].[USMemberRoles]M
		INNER JOIN PageMapNode P ON M.ObjectId = P.SourcePageMapNodeId
		 Where P.SiteId = @MicroSiteId 
		 and MemberId not in(select Id from USGroup where GroupType=2) -- Excluding custom groups, custom groups are not shared with variant sites
	 end 
	 
	 
	 if exists(select 1 from  [dbo].PageMapBase B INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId Where P.SiteId = @MicroSiteId)
	begin
	 INSERT INTO [dbo].[PageMapBase]
					   ([SiteId]
					   ,[HomePageDefinitionId]
					   ,[ModifiedDate]
					   ,[ModifiedBy]
					   ,[LastPageMapModificationDate])
			SELECT top 1 @MicroSiteId
				  ,P.PageMapNodeId
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,[LastPageMapModificationDate]
			FROM [dbo].PageMapBase B
			INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId
			 Where P.SiteId = @MicroSiteId
			 end
		else	
			Insert into PageMapBase (SiteId, ModifiedDate,ModifiedBy)	Values(@MicroSiteId, GETUTCDATE(),@ModifiedBy) 
	 
	 if(@ImportMenusOnly = 0)
	 begin
	 
			INSERT INTO [dbo].[PageDefinition]
					   ([PageDefinitionId]
					   ,[TemplateId]
					   ,[SiteId]
					   ,[Title]
					   ,[Description]
					   ,[PageStatus]
					   ,[WorkflowState]
					   ,[PublishCount]
					   ,[PublishDate]
					   --,[DisplayOrder]
					   ,[FriendlyName]
					   ,[WorkflowId]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[ArchivedDate]
					   ,[StatusChangedDate]
					   ,[AuthorId]
					   ,[EnableOutputCache]
					   ,[OutputCacheProfileName]
					   ,[ExcludeFromSearch]
					   ,[IsDefault]
					   ,[IsTracked]
					   ,[HasForms]
					   ,[TextContentCounter]
					   ,[SourcePageDefinitionId]
					   ,[CustomAttributes]
					   )
			SELECT NEWID()
				  ,[TemplateId]
				  ,@MicroSiteId
				  ,[Title]
				  ,D.[Description]
				  ,case when @MakePageStatusAsDraft=1 AND PageStatus <> 9 then dbo.GetActiveStatus()  else [PageStatus]  end
				  ,case when @MakePageStatusAsDraft=1 then  2  else case when WorkflowState =1 then WorkflowState else 2 end end
				  ,case when @MakePageStatusAsDraft=1 then 0 else 1 end
				  ,[PublishDate]
				  --,[DisplayOrder]
				  ,[FriendlyName]
				  ,dbo.GetEmptyGUID()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[ArchivedDate]
				  ,[StatusChangedDate]
				  ,@ModifiedBy
				  ,[EnableOutputCache]
				  ,[OutputCacheProfileName]
				  ,[ExcludeFromSearch]
				  ,[IsDefault]
				  ,[IsTracked]
				  ,[HasForms]
				  ,[TextContentCounter]
				  ,PageDefinitionId
				  ,D.[CustomAttributes]
			      
			  FROM [dbo].[PageDefinition] D
			Where D.SiteId =@SiteId and PageDefinitionId in (select PageDefinitionId  from PageMapNodePageDef PMD inner join PageMapNode PM on PMD.PageMapNodeId = PM.SourcePageMapNodeId where PM.SiteId =@MicroSiteId)


			insert into PageMapNodePageDef
			select distinct  c.PageMapNodeId ,a.PageDefinitionId, b.DisplayOrder from [PageDefinition] a , PageMapNodePageDef b  , PageMapNode c
			where a.SiteId  = @MicroSiteId and a.SourcePageDefinitionId = b.PageDefinitionId 
			and c.SourcePageMapNodeId = b.PageMapNodeId and 
			c.SiteId = @MicroSiteId



			UPdate PN SET PN.TargetId=T.PageDefinitionId
			FROM PageMapNode PN
			INNER JOIN PageDefinition T ON PN.TargetId = T.SourcePageDefinitionId
			Where PN.Target='1' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

			create table  #tempIds (OldId uniqueidentifier, NewId uniqueidentifier)
			insert into #tempIds SELECT PS.Id, NEWID() FROM  [dbo].[PageDefinitionSegment] PS INNER JOIN PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 	  
			 
			 INSERT INTO [dbo].[PageDefinitionSegment]
						   ([Id]
						   ,[PageDefinitionId]
						   ,[DeviceId]
						   ,[AudienceSegmentId]
						   ,[UnmanagedXml]
						   ,[ZonesXml])
						   SELECT T.NewId 
							  ,P.PageDefinitionId 
							  ,[DeviceId]
							  ,[AudienceSegmentId]
							  ,[UnmanagedXml]
							  ,[ZonesXml]
						  FROM [dbo].[PageDefinitionSegment] PS 
						  inner join #tempIds T on PS.Id= T.OldId 
						  inner join PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
						  Where P.SiteId = @MicroSiteId
						  
						  INSERT INTO [PageDefinitionContainer]
							   (
								[Id]
							   ,PageDefinitionSegmentId 
							   ,[PageDefinitionId]
							   ,[ContainerId]
							   ,[ContentId]
							   ,[InWFContentId]
							   ,[ContentTypeId]
							   ,[IsModified]
							   ,[IsTracked]
							   ,[Visible]
							   ,[InWFVisible]
							   ,DisplayOrder 
							   ,InWFDisplayOrder 
							   ,[CustomAttributes]
							   ,[ContainerName])
							SELECT
								 NewId() 
								 ,T.NewId 
								,P.PageDefinitionId
								,ContainerId
								,ContentId
								,InWFContentId
								,ContentTypeId
								,IsModified
								,PC.IsTracked
								,Visible
								,InWFVisible
								,PC.DisplayOrder 
							   ,PC.InWFDisplayOrder 
								,PC.CustomAttributes
								,ContainerName
							FROM PageDefinitionContainer PC
							inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId
							inner join PageDefinition P ON PC.PageDefinitionId=P.SourcePageDefinitionId 
							Where P.SiteId = @MicroSiteId
						  
			truncate table #tempIds

				

			INSERT INTO [dbo].[PageDetails]
					   ([PageId]
					   ,[PageDetailXML])
			  SELECT 
				 P.PageDefinitionId
					   ,[PageDetailXML]
			  FROM [dbo].[PageDetails] PD
			INNER JOIN PageDefinition P ON P.SourcePageDefinitionId = PD.PageId
			 Where P.SiteId = @MicroSiteId

			  INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageDefinitionId
					   ,ObjectTypeId
					   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN PageDefinition P ON SO.ObjectId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageScript]
					   ([PageDefinitionId]
					   ,[ScriptId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[ScriptId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageScript] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageStyle]
					   ([PageDefinitionId]
					   ,[StyleId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[StyleId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageStyle] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[COTaxonomyObject]
					   ([Id]
					   ,[TaxonomyId]
					   ,[ObjectId]
					   ,[ObjectTypeId]
					   ,[SortOrder]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
			 SELECT NEWID()
				  ,[TaxonomyId]
				  ,P.PageDefinitionId
				  ,[ObjectTypeId]
				  ,[SortOrder]
				  ,[Status]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[COTaxonomyObject] OT
			INNER JOIN PageDefinition P ON OT.ObjectId= P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 
			 
			 
			 --create microsite pages version from master pages latest version
			insert into #tempIds select Id, NEWID() from 
			(Select distinct Id,ObjectId
			from (
			Select ROW_NUMBER () Over (Partition By ObjectId order by CreatedDate desc) RowNum,ObjectId,Id,XMLString, ObjectStatus, VersionStatus,Comments, TriggeredObjectId,TriggeredObjectTypeId 
			FROM VEVersion 
			Where ObjectTypeId =8
			)V
			Where V.RowNum =1) LV inner  join PageDefinition pd on LV.ObjectId =pd.SourcePageDefinitionId where pd.SiteId=@MicroSiteId




			INSERT INTO [dbo].[VEVersion]
					   ([Id]
					   ,[ApplicationId]
					   ,[ObjectTypeId]
					   ,[ObjectId]
					   ,[CreatedDate]
					   ,[CreatedBy]
					   ,[VersionNumber]
					   ,[RevisionNumber]
					   ,[Status]
					   ,[ObjectStatus]
					   ,[VersionStatus]
					   ,[XMLString]
					   ,[Comments]
					   ,[TriggeredObjectId]
					   ,[TriggeredObjectTypeId])          
					   select T.NewId,
					   @MicroSiteId ,
					   8, 
					   pd.PageDefinitionId, 
					   GETUTCDATE(),
					   @ModifiedBy ,
					   1 ,
					   0,
					   'false' ,
					   LV.ObjectStatus,
					   LV.VersionStatus, 
					   case when @MakePageStatusAsDraft=1 then dbo.UpdatePageXml(LV.XMLString, pd.PageDefinitionId,pd.WorkflowId,2,dbo.GetActiveStatus(),0, pd.CreatedBy,pd.CreatedDate, pd.AuthorId,pd.SiteId,pd.SourcePageDefinitionId)
					   else dbo.UpdatePageXml(LV.XMLString, pd.PageDefinitionId,pd.WorkflowId, case when pd.WorkflowState =1 then pd.WorkflowState else 2 end,pd.PageStatus,1 ,pd.CreatedBy,pd.CreatedDate, pd.AuthorId,pd.SiteId,pd.SourcePageDefinitionId) end, 
					   LV.Comments,
					   LV.TriggeredObjectId,
					   LV.TriggeredObjectTypeId       
			  from VEVersion LV
			 inner join #tempIds T on LV.Id= T.OldId 
			 inner  join PageDefinition pd on LV.ObjectId =pd.SourcePageDefinitionId 
			 
			  where pd.SiteId=@MicroSiteId
			 
			 create table #VPSIds (OldId uniqueidentifier, NewId uniqueidentifier)
			 
			 insert into #VPSIds select Id, NEWID() from [dbo].[VEPageDefSegmentVersion] VPS inner join #tempIds T on VPS.VersionId= T.OldId
			  

			    
			 
			 INSERT INTO [dbo].[VEPageDefSegmentVersion]
					   ([Id]
					   ,[VersionId]
					   ,[PageDefinitionId]
					   ,[MinorVersionNumber]
					   ,[DeviceId]
					   ,[AudienceSegmentId]   
					   ,[ContainerContentXML]        
					   ,[UnmanagedXml]
					   ,[ZonesXml]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   )
				SELECT TPS.NewId 
				  ,V.Id
				  ,V.ObjectId 
				  ,[MinorVersionNumber]
				  ,[DeviceId]
				  ,[AudienceSegmentId]
				  ,[ContainerContentXML]  
				  ,[UnmanagedXml]
				  ,[ZonesXml]
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[VEPageDefSegmentVersion] VPS
			  inner join #VPSIds TPS on VPS.Id= TPS.OldId 
			  inner join #tempIds T on VPS.VersionId = T.OldId 
			  inner join VEVersion V on T.NewId = V.Id  
			  

			 truncate table #tempIds 

			 insert into #tempIds select VC.Id, newId() from dbo.VEContentVersion VC inner join [dbo].[VEPageContentVersion] VPC on VC.Id= VPC.ContentVersionId  inner join #VPSIds VPS on VPC.PageDefSegmentVersionId = VPS.OldId



			 INSERT INTO [dbo].[VEContentVersion]
					   ([Id]
					   ,[ContentId]
					   ,[XMLString]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
				 SELECT T.NewId 
				  ,[ContentId]
				  ,[XMLString]
				  ,[Status]
				  ,@ModifiedBy 
				  ,GETUTCDATE()
				  ,@ModifiedBy 
				  ,GETUTCDATE()
			  FROM [dbo].[VEContentVersion] VC
			  inner join #tempIds T on VC.Id= T.OldId 


			INSERT INTO [dbo].[VEPageContentVersion]
					   ([Id]
					   ,[ContentVersionId]
					   ,[IsChanged]
					   ,[PageDefSegmentVersionId])
			   
			SELECT NewId()
				  ,T.NewId
				  ,[IsChanged]
				  ,VPS.NewId
			  FROM [dbo].[VEPageContentVersion] VPC
			inner join #VPSIds VPS on VPC.PageDefSegmentVersionId = VPS.OldId 
			inner join #tempIds T on VPC.ContentVersionId = T.OldId

			drop table #VPSIds 
			drop table #tempIds
		End

	 END
	
 end

END
GO