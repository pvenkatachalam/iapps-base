﻿--Patch script for 5.1.1025.01
GO
PRINT 'Updating the version of the products to 5.1.1025.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.1025.1'
GO
PRINT 'Creating index IX_USUserShippingAddress_UserId'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_USUserShippingAddress_UserId' AND object_id = OBJECT_ID(N'[dbo].[USUserShippingAddress]'))
CREATE NONCLUSTERED INDEX [IX_USUserShippingAddress_UserId] ON [dbo].[USUserShippingAddress] ([UserId], [Status])
GO
PRINT 'Altering procedure Campaign_GetUserActionResultsForCampaign'
GO
ALTER PROCEDURE [dbo].[Campaign_GetUserActionResultsForCampaign] (
	@CampaignId uniqueidentifier,
	@IncludeAll bit,
	@UserProperties xml=null
)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @tblAddress TABLE(Id uniqueidentifier primary key, AddressLine1 nvarchar(100), AddressLine2 nvarchar(100), AddressLine3 nvarchar(100), City nvarchar(100), 
				State nvarchar(100), StateCode nvarchar(100), CountryName nvarchar(100), CountryCode nvarchar(100), Zip nvarchar(100), County nvarchar(255))
	DECLARE @tblSendLog TABLE (UserId uniqueidentifier primary key, LoweredEmail nvarchar(100), Opens int, Clicks int, Unsubscribes int, Bounces int, TriggeredWatches int)	
			
	INSERT INTO @tblAddress
	SELECT a.Id,
		a.[AddressLine1],
		a.[AddressLine2],
		a.[AddressLine3],
		a.[City],
		s.[State],
		s.[StateCode],
		c.[CountryName],
		c.[CountryCode],
		a.[Zip],
		a.County
	FROM GLAddress AS a
	INNER JOIN GLState AS s ON a.StateId = s.Id
	INNER JOIN GLCountry AS c ON a.CountryId = c.Id
	
	--If IncludeAll is true, perform a right join on MKEmailSendActions to include users who did not perform any action
	IF @IncludeAll = 0
		BEGIN
			INSERT INTO @tblSendLog
			SELECT		
				sl.UserId AS Id,
				us.LoweredEmail AS Email,
				SUM(CASE WHEN sa.ActionType = 1 THEN 1 ELSE 0 END) AS Opens,
				SUM(CASE WHEN sa.ActionType = 2 THEN 1 ELSE 0 END) AS Clicks,
				SUM(CASE WHEN sa.ActionType = 3 THEN 1 ELSE 0 END) AS Unsubscribes,
				(SELECT COUNT(Id) AS Bounces
									FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND Bounced = 1 AND UserId = sl.UserId) AS Bounces,
				SUM(CASE WHEN sa.ActionType = 5 THEN 1 ELSE 0 END) AS TriggeredWatches
			 FROM		MKEmailSendLog AS sl
						INNER JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
						INNER JOIN	USMembership AS us ON us.UserId = sl.UserId
			 WHERE		sl.CampaignId = @CampaignId
			 GROUP BY	us.LoweredEmail, sl.UserId
						 
			
		END
	ELSE
		BEGIN	
			INSERT INTO @tblSendLog
			SELECT		
				sl.UserId AS Id,
				us.LoweredEmail AS Email,
				SUM(CASE WHEN sa.ActionType = 1 THEN 1 ELSE 0 END) AS Opens,
				SUM(CASE WHEN sa.ActionType = 2 THEN 1 ELSE 0 END) AS Clicks,
				SUM(CASE WHEN sa.ActionType = 3 THEN 1 ELSE 0 END) AS Unsubscribes,
				(SELECT COUNT(Id) AS Bounces
									FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND Bounced = 1 AND UserId = sl.UserId) AS Bounces,
				SUM(CASE WHEN sa.ActionType = 5 THEN 1 ELSE 0 END) AS TriggeredWatches
			 FROM		MKEmailSendLog AS sl
						LEFT JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
						INNER JOIN	USMembership AS us ON us.UserId = sl.UserId
			 WHERE		sl.CampaignId = @CampaignId
			 GROUP BY	us.LoweredEmail, sl.UserId
	 				
			
		END
		
		
		SELECT DISTINCT(c.UserId), 
				m.[Opens],
				m.[Clicks],
				m.[Unsubscribes],
				m.[Bounces],
				m.[TriggeredWatches],
				c.[FirstName], 
				c.[MiddleName],
				c.[LastName],
				c.[CompanyName],
				c.[BirthDate],
				c.[Gender],
				c.[Status],
				c.[HomePhone],
				c.[MobilePhone],
				c.[OtherPhone],
				c.[Notes],
				c.[Email],
				a.[AddressLine1],
				a.[AddressLine2],
				a.[AddressLine3],
				a.[City],
				a.[State],
				a.[StateCode],
				a.[CountryName],
				a.[CountryCode],
				a.[Zip],
				a.County
			FROM vw_contacts AS c
			INNER JOIN @tblSendLog m ON m.UserId = c.UserId
			LEFT JOIN @tblAddress a ON a.Id = c.AddressId
			
			
			
			--Second table of Profile values to prevent recurring database
			SELECT distinct(sl.UserId),um.*
			FROM  @tblSendLog AS sl
			INNER JOIN USMemberProfile As um ON sl.UserId=um.UserId
			where(um.PropertyName is null or  um.PropertyName in (select t.X.value('(@value)[1]','nvarchar(255)') from @UserProperties.nodes('/list/listItem') t(X)))
END
GO
PRINT 'Altering procedure CommerceReport_SalesByNavCategory'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesByNavCategory]
    (
      @startDate DATETIME ,
      @endDate DATETIME ,
      @maxReturnCount INT ,
      @pageSize INT = 10 ,
      @pageNumber INT ,
      @sortBy NVARCHAR(50) = NULL ,
      @virtualCount INT OUTPUT ,
      @ApplicationId UNIQUEIDENTIFIER = NULL  
    )
  AS 
    BEGIN  

        IF @maxReturnCount = 0 
            SET @maxReturnCount = NULL  
 
  
  
        IF @pageSize IS NULL 
            SET @PageSize = 2147483647  
        IF @endDate IS NULL 
            SET @endDate = GETDATE()  
        DECLARE @TotalSales DECIMAL(18, 2)
        DECLARE @TotalQuantity DECIMAL(18, 2)
        DECLARE @LoadedOrder DATETIME
           
        SELECT  @LoadedOrder = MAX(OrderDate)
        FROM    RPTCommerceReport_Category

        INSERT  INTO dbo.RPTCommerceReport_Category
                ( Id ,
                  OrderTotal ,
                  Quantity ,
                  NavNodeId ,
                  OrderDate
                                
                )
                SELECT  TV.*
                FROM    RPTCommerceReport_Category LR
                        RIGHT JOIN ( SELECT O.Id ,
                                            SUM(Price * Quantity) - SUM(OI.Discount) AS OrderTotal ,
                                            SUM(Quantity) AS Quantity ,
                                            NV.NavNodeId ,
                                            O.OrderDate
                                     FROM   OROrder O
                                            INNER JOIN dbo.OROrderItem OI ON OI.OrderId = O.Id
                                            INNER JOIN dbo.PRProductSKU PS ON PS.Id = OI.ProductSKUId
                                            INNER JOIN NVFilterOutput NVO ON NVO.ObjectId = PS.ProductId
                                            INNER JOIN NVFilterQuery FQ ON NVO.QueryId = FQ.Id
                                                              AND FQ.SiteId = @ApplicationId
                                            INNER JOIN NVNavNodeNavFilterMap NV ON NVO.QueryId = NV.QueryId
                                            LEFT JOIN NVFilterExclude NVEx ON NVEx.QueryId = FQ.Id
                                                              AND NVO.ObjectId = NVEx.ObjectId
                                     WHERE  NVEx.ObjectId IS NULL
                                            AND OI.OrderItemStatusId <> 3
                                            AND O.OrderStatusId IN ( 1, 2, 11,
                                                              4 )
                                            AND ( @LoadedOrder IS NULL
                                                  OR O.OrderDate > @LoadedOrder
                                                )
                                     GROUP BY O.OrderDate ,
                                            NV.NavNodeId ,
                                            O.Id
                                   ) TV ON TV.Id = LR.Id
                                           AND TV.NavNodeId = LR.NavNodeId
                WHERE   LR.Id IS NULL
                        
                       
                        
                     
                        
                       
                                      
                        
                        
                        
        SELECT  @TotalSales = SUM(OrderTotal) ,
                @TotalQuantity = SUM(Quantity)
        FROM    RPTCommerceReport_Category
        WHERE   OrderDate >= dbo.ConvertTimeToUtc(@startDate, @ApplicationId)
                AND OrderDate < dbo.ConvertTimeToUtc(@endDate, @ApplicationId)
    
     
        SELECT  @virtualCount = COUNT(DISTINCT NavNodeId)
        FROM    RPTCommerceReport_Category
        WHERE   OrderDate >= dbo.ConvertTimeToUtc(@startDate, @ApplicationId)
                AND OrderDate < dbo.ConvertTimeToUtc(@endDate, @ApplicationId)
     
                                                                                            
      
    
           -- INSERT  INTO #tmpCategorySale
        SELECT  *
        FROM    ( SELECT    C.NavNodeId AS NavId ,
                            NavNodeUrl as NavigationCategory ,
                            SUM(Quantity) AS Quantity ,
                            SUM(OrderTotal) AS SaleTotal ,
                            COUNT(Id) AS NumberOfOrders ,
                            ROW_NUMBER() OVER ( ORDER BY CASE WHEN @sortBy = 'navigationcategory asc'
                                                              THEN NavNodeUrl
                                                         END ASC, CASE
                                                              WHEN @sortBy = 'navigationcategory desc'
                                                              THEN NavNodeUrl
                                                              END DESC, CASE
                                                              WHEN @sortBy = 'quantity asc'
                                                              THEN SUM(Quantity)
                                                              END ASC, CASE
                                                              WHEN @sortBy = 'quantity desc'
                                                              THEN SUM(Quantity)
                                                              END DESC, CASE
                                                              WHEN @sortBy = 'saletotal asc'
                                                              THEN SUM(OrderTotal)
                                                              END ASC, CASE
                                                              WHEN @sortBy = 'saletotal desc'
                                                              THEN SUM(OrderTotal)
                                                              END DESC ) AS RowNumber ,
                            ( SUM(Quantity) / @TotalQuantity ) * 100 AS PercentOfQuantity ,
                            ( SUM(OrderTotal) / @TotalSales ) * 100 AS PercentOfSales
                  FROM      RPTCommerceReport_Category C
                            INNER JOIN NVNavNodeNavFilterMap M ON M.NavNodeId = C.NavNodeId
                  WHERE     OrderDate >= dbo.ConvertTimeToUtc(@startDate,
                                                              @ApplicationId)
                            AND OrderDate < dbo.ConvertTimeToUtc(@endDate,
                                                              @ApplicationId)
                  GROUP BY  C.NavNodeId ,
                            NavNodeUrl
                ) TV
        WHERE   RowNumber <= ISNULL(@maxReturnCount, @virtualCount)
                AND RowNumber >= ( @pageSize * @pageNumber ) - ( @pageSize - 1 )
                AND RowNumber <= @pageSize * @pageNumber
        ORDER BY RowNumber
        
     
         
       
     

 
    END
GO
PRINT 'Altering procedure CommerceReport_SalesByProduct'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesByProduct]  
(   
 @startDate datetime,@endDate datetime, @maxReturnCount int,  
 @pageSize int = 10,@pageNumber int,   
 @sortBy nvarchar(50) = null,  
 @navNodeId uniqueidentifier = null,  
 @virtualCount int output,  
 @ApplicationId uniqueidentifier=null  
)   
AS  

  
--DECLARE @startDate datetime,@endDate datetime, 
-- @maxReturnCount int,  --what's the point of this one?
-- @pageSize int,
-- @pageNumber int,   
-- @sortBy nvarchar(50) ,  
-- @navNodeId uniqueidentifier ,  
-- @virtualCount int ,  
-- @ApplicationId uniqueidentifier
  
--	SET @startDate='2010-09-01 00:00:00:000'
--	SET @endDate='2011-01-13 00:00:00:000'
--	SET @maxReturnCount=0
--	SET @pageSize=NULL
--	SET @pageNumber=1
--	SET @sortBy=N'SaleTotal desc'
--	SET @navNodeId=NULL
--	SET @ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'  
	
BEGIN  
  
if @maxReturnCount = 0  
set @maxReturnCount = null  

IF @pageSize IS NULL  
	SET @PageSize = 2147483647  
IF @endDate IS NULL  
	SET @endDate = GetDate()  

declare @tmpProductSale table(  
	ProductId uniqueidentifier,  
	ProductSKUName nvarchar(1000),  
	Quantity decimal(18,2),  
	ItemTotal money,  
	DiscountTotal MONEY,
	NetTotal MONEY,
	NumberOfOrders int,
	BundleQuantity decimal(18,2)    
)  

IF  @navNodeId is null Or @navNodeId = dbo.GetEmptyGUID()  
BEGIN  
--  Print '@navNodeId :'   
  INSERT INTO @tmpProductSale  
	SELECT  
	 Pr.Id as ProductId,  
	 Pr.Title AS ProductSKUName,     
	 case when MIN(Pr.IsBundle) =0 then SUM( OI.Quantity ) else 0 end AS Quantity,  
	 SUM( OI.Price * OI.Quantity) AS ItemTotal,  
	 SUM(OI.Discount) as DiscountTotal,
	  SUM( OI.Price * OI.Quantity)  - SUM(OI.Discount) as NetTotal,
	 COUNT(O.Id ), -- no need for distinct
	 case when MIN(Pr.IsBundle) !=0 then SUM( OI.Quantity ) else 0 end AS BundleQuantity  
	FROM   
	 OROrder O     
	 INNER JOIN OROrderItem OI ON OI.OrderId = O.Id AND OI.OrderItemStatusId <> 3
	 INNER JOIN PRProductSKU PSKU ON OI.ProductSKUId = PSKU.Id  
	 INNER JOIN PRProduct Pr ON Pr.Id = PSKU.ProductId  
	WHERE   
	 OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)  
	 AND OrderDate <= dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	 AND O.OrderStatusId in (1,2,11,4)  
	 AND O.SiteId = @ApplicationId
	GROUP BY  
	 Pr.Id, Pr.Title  
 END  
ELSE  
 BEGIN  
   Declare @filterId uniqueidentifier  
   Select @filterId = QueryId from NVNavNodeNavFilterMap where NavNodeId = @navNodeId  
  
   INSERT INTO @tmpProductSale  
	SELECT  
	 Pr.Id as ProductId,  
	 Pr.Title AS ProductSKUName,     	 
	 case when MIN(Pr.IsBundle) =0 then SUM( OI.Quantity ) else 0 end AS Quantity,  
	 SUM( OI.Price * OI.Quantity) AS ItemTotal,  
	 SUM(OI.Discount) as DiscountTotal,
	 SUM( OI.Price * OI.Quantity)  - SUM(OI.Discount) as NetTotal,
	 COUNT( O.Id ), -- no need for distinct
	 case when MIN(Pr.IsBundle) !=0 then SUM( OI.Quantity ) else 0 end AS BundleQuantity  
	FROM   
	 OROrder O     
	 INNER JOIN OROrderItem OI ON OI.OrderId = O.Id AND OI.OrderItemStatusId <> 3
	 INNER JOIN PRProductSKU PSKU ON OI.ProductSKUId = PSKU.Id  
	 INNER JOIN PRProduct Pr ON Pr.Id = PSKU.ProductId  
	 INNER JOIN (SELECT DISTINCT NVO.ObjectID 
		 FROM NVFilterOutput NVO LEFT JOIN NVFilterExclude NVEx ON NVEx.QueryId = NVO.QueryId AND NVO.ObjectId = NVEx.ObjectId
		 WHERE NVEx.ObjectId IS NULL AND NVO.QueryId = @filterId) NVO ON Pr.Id = NVO.ObjectId  
	WHERE   
	 OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)  
	 AND OrderDate <= dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	 AND O.OrderStatusId in (1,2,11,4)  
	 AND O.SiteId = @ApplicationId
	GROUP BY  
	 Pr.Id,PSKU.Id ,PSKU.SKU,Pr.Title  
 END  
  
Set @virtualCount = @@ROWCOUNT  
  
Declare @grandTotalQuantity decimal(18,2)  
Declare @grandTotalSales money  
  
select @grandTotalQuantity = Sum(A.Quantity) ,  
  @grandTotalSales = Sum(A.ItemTotal)   
from @tmpProductSale As A  
  
if @sortBy is null or @sortBy = 'productskuname asc'  
 Begin  
  Select A.*, case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,    
	 case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales,  
	 dbo.GetProductNavNodesPerSite(A.ProductId,@ApplicationId) as NavigationCategories   
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by ProductSKUName asc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.ProductSKUName ASC  
 end  
else if @sortBy = 'productskuname desc'  
 Begin  
  Select A.*,  case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
	case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales,  
	 dbo.GetProductNavNodesPerSite(A.ProductId,@ApplicationId) as NavigationCategories FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by ProductSKUName desc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.ProductSKUName desc  
 end  
else if @sortBy = 'quantity asc'  
 Begin  
  Select A.*, case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
	case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales,  
   dbo.GetProductNavNodesPerSite(A.ProductId,@ApplicationId) as NavigationCategories FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by Quantity asc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.Quantity asc  
 end  
else if @sortBy = 'quantity desc'  
 Begin  
  Select A.*,   
   case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
   case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales,  
	dbo.GetProductNavNodesPerSite(A.ProductId,@ApplicationId) as NavigationCategories FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by Quantity desc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.Quantity desc  
 end  
else if @sortBy = 'nettotal asc'  
 Begin  
  Select A.*,   
   case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
   case when @grandTotalSales = 0 then 0 else  (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales,  
   dbo.GetProductNavNodesPerSite(A.ProductId,@ApplicationId) as NavigationCategories FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by ItemTotal ASC) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.ItemTotal ASC  
 end   
else if @sortBy = 'nettotal desc'  
 Begin  
  Select A.*,  
	case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
	case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales,  
	 dbo.GetProductNavNodesPerSite(A.ProductId,@ApplicationId) as NavigationCategories FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by ItemTotal DESC) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.ItemTotal DESC  
 end   
  
END  


GO
PRINT 'Altering procedure CommerceReport_SalesByRegion'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesByRegion]
(	
	@startDate datetime,@endDate datetime, @maxReturnCount int,
	@pageSize int = 10,@pageNumber int =null, 
	@sortBy nvarchar(50) = null,
	@stateId uniqueidentifier = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS

--DECLARE @startDate datetime,@endDate datetime, @maxReturnCount int,
--	@pageSize int,@pageNumber int, 
--	@sortBy nvarchar(50),
--	@stateId uniqueidentifier,
--	@virtualCount int,
--	@ApplicationId uniqueidentifier
	
--SET @startDate='2010-09-01 00:00:00:000'
--SET @endDate='2011-01-13 00:00:00:000'
--SET @maxReturnCount=0
--SET @pageSize=10
--SET @pageNumber=1
--SET @sortBy=NULL
--SET @stateId=NULL
--SET @ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'  

BEGIN


if @maxReturnCount = 0
	set @maxReturnCount = null
IF @pageSize IS NULL
	SET @PageSize = 2147483647
IF @endDate IS NULL
	SET @endDate = GetDate()
If @stateId = dbo.GetEmptyGUID()
	SET @stateId = null

declare @tmpRegionOrders table(
	Country nvarchar(255),
	StateId uniqueidentifier,
	StateName nvarchar(255),
	StateCode char(3),
	ItemTotal decimal(18,2),
	DiscountTotal decimal(18,2),
	NetTotal decimal(18,2),
	NumberOfOrders int
	)

IF @stateId IS NULL
BEGIN
	-- I bet this reports incorrectly for Multi Address Shipping. IF in two states, I bet the results double
	INSERT INTO @tmpRegionOrders
	SELECT
		MIN(CountryName) Country,
		GS.Id as StateId,
		GS.State AS StateName,
		GS.StateCode,
		SUM( OI.Price * OI.Quantity) AS ItemTotal,
		SUM(OI.Discount) as DiscountTotal,
		SUM( OI.Price * OI.Quantity) - SUM(OI.Discount) AS NetTotal,
		Count( Distinct O.Id ) as NumberOfOrders
	FROM
		OROrder O
		INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
		INNER JOIN OROrderShipping OS ON OS.Id = OI.OrderShippingId
		INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
		INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
		INNER JOIN GLCountry C on GA.CountryId = C.Id
		LEFT JOIN GLState GS ON GS.Id = GA.StateId
	WHERE
		OI.OrderItemStatusId <> 3
		AND (O.OrderTypeId=1 OR O.OrderTypeId=2) --why now filter here on these two types and everywhere else not?
		AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
		AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
		AND O.OrderStatusId in (1,2,11)
		AND O.SiteId = @ApplicationId
	GROUP BY
		GS.StateCode,GS.State,GS.Id
END
ELSE
BEGIN
	INSERT INTO @tmpRegionOrders
	SELECT
	MIN(CountryName) Country,
		GS.Id as StateId,
		GS.State AS StateName,
		GS.StateCode,
		SUM( OI.Price * OI.Quantity) AS ItemTotal,
		SUM(OI.Discount) as DiscountTotal,
		SUM( OI.Price * OI.Quantity) - SUM(OI.Discount) AS NetTotal,
		Count( Distinct O.Id ) as NumberOfOrders
	FROM
		OROrder O
		INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
		INNER JOIN OROrderShipping OS ON OS.Id = OI.OrderShippingId
		INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
		INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
		INNER JOIN GLState GS ON GS.Id = GA.StateId
		INNER JOIN GLCountry C on GS.CountryId = C.Id
	WHERE
		OI.OrderItemStatusId <> 3
		AND (O.OrderTypeId=1 OR O.OrderTypeId=2) --why now filter here on these two types and everywhere else not?
		AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
		AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
		AND O.OrderStatusId in (1,2,11)
		AND O.SiteId = @ApplicationId
		AND GS.Id = @stateId
	GROUP BY
		GS.StateCode,GS.State,GS.Id
END

SET @virtualCount = @@ROWCOUNT

IF @sortBy is null or (@sortBy)='statename asc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by StateName asc) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.StateName ASC
end
else if (@sortBy) = 'statename desc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by StateName DESC) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.StateName DESC
end
ELSE IF (@sortBy)='statecode asc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by StateCode asc) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.StateCode ASC
end
else if (@sortBy) = 'statecode desc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by StateCode DESC) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.StateCode DESC
end
ELSE IF (@sortBy)='numberoforders asc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by NumberOfOrders asc) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.NumberOfOrders ASC
end
else if (@sortBy) = 'numberoforders desc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by NumberOfOrders DESC) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.NumberOfOrders DESC
end
ELSE IF (@sortBy)='nettotal asc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by NetTotal asc) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.NetTotal ASC
end
else if (@sortBy) = 'nettotal desc'
Begin
	Select A.* 
	FROM
		(
		Select *,  ROW_NUMBER() OVER(Order by NetTotal DESC) as RowNumber
		 from @tmpRegionOrders
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	Order by A.NetTotal DESC
end
END

GO
PRINT 'Altering procedure CommerceReport_SalesByRegionAggregated'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesByRegionAggregated]
(	
	@startDate datetime,@endDate datetime, @maxReturnCount int,
	@pageSize int = 10,@pageNumber int =null, 
	@sortBy nvarchar(50) = null,
	@stateId uniqueidentifier = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS

--DECLARE @startDate datetime,@endDate datetime, @maxReturnCount int,
--	@pageSize int,@pageNumber int, 
--	@sortBy nvarchar(50),
--	@stateId uniqueidentifier,
--	@virtualCount int,
--	@ApplicationId uniqueidentifier
	
--SET @startDate='2010-09-01 00:00:00:000'
--SET @endDate='2011-01-13 00:00:00:000'
--SET @maxReturnCount=0
--SET @pageSize=10
--SET @pageNumber=1
--SET @sortBy=NULL
--SET @stateId=NULL
--SET @ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'  

BEGIN


if @maxReturnCount = 0
	set @maxReturnCount = null
IF @pageSize IS NULL
	SET @PageSize = 2147483647
IF @endDate IS NULL
	SET @endDate = GetDate()
If @stateId = dbo.GetEmptyGUID()
	SET @stateId = null

declare @tmpRegionOrders table(
	Country nvarchar(255),
	CountryCode varchar(3),
	StateId uniqueidentifier,
	StateName nvarchar(255),
	StateCode char(3),
	ItemTotal decimal(18,2),
	DiscountTotal decimal(18,2),
	NetTotal decimal(18,2),
	NumberOfOrders int
	)
	
Declare @tmpCountryOrders table(
	Country nvarchar(255),
	CountryCode varchar(3),
	ItemTotal decimal(18,2),
	DiscountTotal decimal(18,2),
	NetTotal decimal(18,2),
	NumberOfOrders int,
	RowNumber int
	)	

IF @stateId IS NULL
BEGIN
	-- I bet this reports incorrectly for Multi Address Shipping. IF in two states, I bet the results double
	INSERT INTO @tmpRegionOrders
	SELECT
		Min(C.CountryName) Country,
		Min(C.CountryCode) CountryCode,
		GS.Id as StateId,
		GS.State AS StateName,
		GS.StateCode,
		SUM( OI.Price * OI.Quantity) AS ItemTotal,
		SUM(OI.Discount) as DiscountTotal,
		SUM( OI.Price * OI.Quantity) - SUM(OI.Discount) AS NetTotal,
		Count( Distinct O.Id ) as NumberOfOrders
	FROM
		OROrder O
		INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
		INNER JOIN OROrderShipping OS ON OS.Id = OI.OrderShippingId
		INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
		INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
		INNER JOIN GLCountry C on GA.CountryId = C.Id
		LEFT JOIN GLState GS ON GS.Id = GA.StateId
		
	WHERE
		OI.OrderItemStatusId <> 3
		AND (O.OrderTypeId=1 OR O.OrderTypeId=2) --why now filter here on these two types and everywhere else not?
		AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
		AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
		AND O.OrderStatusId in (1,2,11,4)
		AND O.SiteId = @ApplicationId
	GROUP BY
		GS.StateCode,GS.State,GS.Id
END
ELSE
BEGIN
	INSERT INTO @tmpRegionOrders
	SELECT
		Min(C.CountryName) Country,
		Min(C.CountryCode) CountryCode,
		GS.Id as StateId,
		GS.State AS StateName,
		GS.StateCode,
		SUM( OI.Price * OI.Quantity)  AS ItemTotal,
		SUM(OI.Discount) as DiscountTotal,
		SUM( OI.Price * OI.Quantity) - SUM(OI.Discount) AS NetTotal,
		Count( Distinct O.Id ) as NumberOfOrders
	FROM
		OROrder O
		INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
		INNER JOIN OROrderShipping OS ON OS.Id = OI.OrderShippingId
		INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
		INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
		INNER JOIN GLCountry C on GA.CountryId = C.Id
		INNER JOIN GLState GS ON GS.Id = GA.StateId
		
	WHERE
		OI.OrderItemStatusId <> 3
		AND (O.OrderTypeId=1 OR O.OrderTypeId=2) --why now filter here on these two types and everywhere else not?
		AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
		AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
		AND O.OrderStatusId in (1,2,11,4)
		AND O.SiteId = @ApplicationId
		AND GS.Id = @stateId
	GROUP BY
		GS.StateCode,GS.State,GS.Id

END

SET @virtualCount = @@ROWCOUNT

IF @sortBy is null or  (@sortBy)='country asc'
Begin
	INSERT INTO @tmpCountryOrders
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		  Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by Country asc) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	
	
		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by StateName ASC
	
	
end
else if (@sortBy) = 'country desc'
Begin
	INSERT INTO @tmpCountryOrders
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		    Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by Country DESC) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	

		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,				
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by StateName DESC
end
ELSE IF (@sortBy)='countrycode asc'
Begin
	INSERT INTO @tmpCountryOrders
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		    Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by Min(CountryCode) asc) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	
		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by StateCode ASC
	
end
else if (@sortBy) = 'countrycode desc'
Begin
	INSERT INTO @tmpCountryOrders
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		    Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by Min(CountryCode) DESC) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	
	
		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by StateCode DESC
end
ELSE IF (@sortBy)='numberoforders asc'
Begin
INSERT INTO @tmpCountryOrders
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		    Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by sum(NumberOfOrders) asc) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	

		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by NumberOfOrders ASC
end
else if (@sortBy) = 'numberoforders desc'
Begin
	INSERT INTO @tmpCountryOrders
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		   Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by sum(NumberOfOrders) DESC) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	

		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by NumberOfOrders DESC

end
ELSE IF (@sortBy)='nettotal asc'
Begin
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		    Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by sum(NetTotal) asc) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	
	
		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by ItemTotal ASC
end
else if (@sortBy) = 'nettotal desc'
Begin
	Select A.* 
	FROM
		(
		Select Min(Country) Country,
		  Min(CountryCode) CountryCode,
		    Sum(ItemTotal) OrderTotal,
		  SUM(DiscountTotal) DiscountTotal,
		  Sum(NetTotal) NetTotal,
		  Sum(NumberOfOrders) NumberOfOrders,
		ROW_NUMBER() OVER(Order by sum(NetTotal) desc) as RowNumber
		 from @tmpRegionOrders
		  Group By Country
		) A
	where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
		 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
	
	Select * from @tmpCountryOrders
	
		Select CountryCode,
				StateId,
				StateName,
				StateCode,
				ItemTotal,
				DiscountTotal,
				NetTotal,
				NumberOfOrders
		 from @tmpRegionOrders
		 Where CountryCode in (Select CountryCode from @tmpCountryOrders)
	Order by NetTotal DESC
end

Select @virtualCount virtualCount;
END
GO
PRINT 'Altering procedure CommerceReport_SalesBySKU'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesBySKU]  
(   
 @startDate datetime,@endDate datetime, @maxReturnCount int,  
 @pageSize int = 10,@pageNumber int,   
 @sortBy nvarchar(50) = null,  
 @navNodeId uniqueidentifier = null,  
 @virtualCount int output,  
 @ApplicationId uniqueidentifier=null  
)   
AS  

  
--DECLARE @startDate datetime,@endDate datetime, 
-- @maxReturnCount int,  --what's the point of this one?
-- @pageSize int,
-- @pageNumber int,   
-- @sortBy nvarchar(50) ,  
-- @navNodeId uniqueidentifier ,  
-- @virtualCount int ,  
-- @ApplicationId uniqueidentifier
  
--	SET @startDate='2010-09-01 00:00:00:000'
--	SET @endDate='2011-01-13 00:00:00:000'
--	SET @maxReturnCount=0
--	SET @pageSize=NULL
--	SET @pageNumber=1
--	SET @sortBy=N'SaleTotal desc'
--	SET @navNodeId=NULL
--	SET @ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'  
	
BEGIN  
  
if @maxReturnCount = 0  
set @maxReturnCount = null  

IF @pageSize IS NULL  
	SET @PageSize = 2147483647  
IF @endDate IS NULL  
	SET @endDate = GetDate()  

declare @tmpProductSale table(  
	SKUId uniqueidentifier,  
	SKUName nvarchar(1000),  
	SKU nvarchar(255),
	Quantity decimal(18,2),  
	ItemTotal money,  
	DiscountTotal MONEY,
	NetTotal MONEY,
	NumberOfOrders int,
	BundleQuantity decimal(18,2) 
)  

IF  @navNodeId is null Or @navNodeId = dbo.GetEmptyGUID()  
BEGIN  
--  Print '@navNodeId :'   
  INSERT INTO @tmpProductSale  
	SELECT  
	 PSKU.Id as SKUId,  
	 PSKU.Title AS SKUName,   
	 PSKU.SKU AS SKU,  
	 case when MIN(P.IsBundle) =0 then SUM( OI.Quantity ) else 0 end AS Quantity,  
	 SUM( OI.Price * OI.Quantity) AS ItemTotal, 
	  SUM(OI.Discount) as DiscountTotal,
	  SUM( OI.Price * OI.Quantity)  - SUM(OI.Discount) as NetTotal, 
	 COUNT(O.Id ), -- no need for distinct
	 case when MIN(P.IsBundle) !=0 then SUM( OI.Quantity ) else 0 end BundleQuantity  
	FROM   
	 OROrder O     
	 INNER JOIN OROrderItem OI ON OI.OrderId = O.Id AND OI.OrderItemStatusId <> 3
	 INNER JOIN PRProductSKU PSKU ON OI.ProductSKUId = PSKU.Id  
	 INNER JOIN PRProduct P ON PSKU.ProductId =P.Id
	WHERE   
	 OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)  
	 AND OrderDate <= dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	 AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId  
	GROUP BY  
	PSKU.Id, PSKU.SKU, PSKU.Title  
 END  
ELSE  
 BEGIN  
   Declare @filterId uniqueidentifier  
   Select @filterId = QueryId from NVNavNodeNavFilterMap where NavNodeId = @navNodeId  
  
   INSERT INTO @tmpProductSale  
	SELECT  
	 PSKU.Id as SKUId,  
	 PSKU.Title AS SKUName,     
	 PSKU.SKU As SKU,
	 case when MIN(Pr.IsBundle) =0 then SUM( OI.Quantity ) else 0 end AS Quantity,  
	 SUM( OI.Price * OI.Quantity) AS ItemTotal,
	 SUM(OI.Discount) as DiscountTotal,
	 SUM( OI.Price * OI.Quantity)  - SUM(OI.Discount) as NetTotal,  
	 COUNT( O.Id ), -- no need for distinct
	 case when MIN(Pr.IsBundle) !=0 then SUM( OI.Quantity ) else 0 end BundleQuantity  
	FROM   
	 OROrder O     
	 INNER JOIN OROrderItem OI ON OI.OrderId = O.Id AND OI.OrderItemStatusId <> 3
	 INNER JOIN PRProductSKU PSKU ON OI.ProductSKUId = PSKU.Id  
	 INNER JOIN PRProduct Pr ON Pr.Id = PSKU.ProductId  
	 INNER JOIN (SELECT DISTINCT NVO.ObjectID 
		 FROM NVFilterOutput NVO LEFT JOIN NVFilterExclude NVEx ON NVEx.QueryId = NVO.QueryId AND NVO.ObjectId = NVEx.ObjectId
		 WHERE NVEx.ObjectId IS NULL AND NVO.QueryId = @filterId) NVO ON Pr.Id = NVO.ObjectId  
	WHERE   
	 OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)  
	 AND OrderDate <= dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	  
	 AND O.OrderStatusId in (1,2,11,4)  
	AND O.SiteId = @ApplicationId
	GROUP BY  
	 PSKU.Id ,PSKU.SKU,PSKU.Title  
 END  
  
Set @virtualCount = @@ROWCOUNT  
  
Declare @grandTotalQuantity decimal(18,2)  
Declare @grandTotalSales money  
  
select @grandTotalQuantity = Sum(A.Quantity) ,  
  @grandTotalSales = Sum(A.ItemTotal)   
from @tmpProductSale As A  
  
if @sortBy is null or @sortBy = 'skuname asc'  
 Begin  
  Select A.*, case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,    
	 case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales
  FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by SKUName asc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.SKUName ASC  
 end  
else if @sortBy = 'skuname desc'  
 Begin  
  Select A.*,  case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
	case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by SKUName desc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.SKUName desc  
 end  
else if @sortBy = 'quantity asc'  
 Begin  
  Select A.*, case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
	case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by Quantity asc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.Quantity asc  
 end  
else if @sortBy = 'quantity desc'  
 Begin  
  Select A.*,   
   case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
   case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by Quantity desc) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.Quantity desc  
 end  
else if @sortBy = 'nettotal asc'  
 Begin  
  Select A.*,   
   case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
   case when @grandTotalSales = 0 then 0 else  (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by ItemTotal ASC) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.ItemTotal ASC  
 end   
else if @sortBy = 'nettotal desc'  
 Begin  
  Select A.*,  
	case when @grandTotalQuantity = 0 then 0 else (A.Quantity / @grandTotalQuantity) *100 end as PercentOfQuantity,   
	case when @grandTotalSales = 0 then 0 else (A.ItemTotal / @grandTotalSales) * 100 end as PercentOfSales FROM  
   (  
   Select *,  ROW_NUMBER() OVER(Order by ItemTotal DESC) as RowNumber  
	from @tmpProductSale  
   ) A  
   where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
	A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
   Order by A.ItemTotal DESC  
 end   
  
END  

--Common Table Expression

GO