﻿GO

---- START Migraton for Taxonomy for Varaint Site ------


PRINT 'Taxanomy Migration'
GO
Declare @sqlStmt varchar(1000)
PRINT 'Create COTaxonomy Backup table'

IF  NOT EXISTS (Select * from sys.tables Where NAME='COTaxonomyV52_MigrationBackup') 
BEGIN
	Select * into COTaxonomyV52_MigrationBackup from COTaxonomy
END
GO


IF EXISTS (Select 1 FROM SISIte where ParentSIteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000')  -- Check if Variant Site Exist
	BEGIN
		 
		Update COTaxonomy Set ApplicationId = SiteId where ApplicationId <> SiteId AND SiteId IS NOT NULL
		
	END
ELSE
	BEGIN
		Print 'No Variant site exist to migrate'
	
	END
GO

PRINT 'Create Default data for the variant site'
GO

IF EXISTS (Select 1 FROM SISIte where ParentSIteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000')  -- Check if Variant Site Exist
	BEGIN
		--Get all the variant sites and then create default data for all the variant site
			DECLARE @TaxonomySiteDefaultData AS Table 
			(
				SiteId uniqueidentifier,
				IsProcessed bit				
			)
		
		INSERT INTO @TaxonomySiteDefaultData 
		SELECT Id, 0 FROM SISIte WHERE Status = 1 and ParentSIteId IS NOT NULL AND ParentSiteId <> '00000000-0000-0000-0000-000000000000'
		DECLARE @MicroSiteId uniqueidentifier
		
		WHILE (Select count(*) From @TaxonomySiteDefaultData where IsProcessed = 0) > 0
		BEGIN

				SELECT Top 1 @MicroSiteId = SiteId FROM @TaxonomySiteDefaultData where IsProcessed = 0
				--Select @MicroSiteId
				IF NOT EXISTS(SELECT 1 FROM COTaxonomy WHERE ApplicationId=@MicroSiteId)
				BEGIN
					--Create the default node for the INDEX TERMS
					INSERT INTO COTaxonomy
					(
						Id,
						ApplicationId,
						Title,
						Description,
						CreatedDate,
						CreatedBy,
						ModifiedBy,
						ModifiedDate,
						Status,
						LftValue,
						RgtValue,
						ParentId,
						SiteId
					)
					VALUES
					(
						NEWID(),
						@MicroSiteId,
						'Index Terms',
						'Index Terms',
						 GETUTCDATE(),
						(SELECT TOP 1 Id FROM USUser WHERE UserName='iappssystemuser'),
						NULL,
						NULL,
						1,
						1,
						2,
						NULL,
						@MicroSiteId
					)
				END
				UPDATE @TaxonomySiteDefaultData Set IsProcessed = 1 where SiteId = @MicrositeId
		END		
	END
	GO