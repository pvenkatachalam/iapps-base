IF OBJECT_ID(N'[dbo].[Site_GetUserSiteIds]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE Site_GetUserSiteIds
(
	@UserId uniqueidentifier
)
AS
BEGIN

	SELECT DISTINCT SiteId FROM USSiteUser where UserId = @UserId

END
'
ELSE
	PRINT 'SP Site_GetUserSiteIds exists'
	
GO