GO
PRINT 'Altering procedure ContactListUserDto_Save'
GO
ALTER PROCEDURE [dbo].[ContactListUserDto_Save]
    (
      @UserIds XML ,
      @DistributionListId UNIQUEIDENTIFIER ,
      @ContactListSubscriptionType INT
	
    )
AS 
    DELETE  FROM TADistributionListUser
    WHERE   UserId IN ( SELECT  A.n.value('@Id', 'uniqueidentifier')
                        FROM    @UserIds.nodes('/UserIds/users') AS A ( n ) )
            AND DistributionListId = @DistributionListId

    INSERT  INTO dbo.TADistributionListUser
            ( Id ,
              DistributionListId ,
              UserId ,
              ContactListSubscriptionType
            )
            SELECT  NEWID() ,
                    @DistributionListId ,
                    A.n.value('@Id', 'uniqueidentifier') AS UserId ,
                    @ContactListSubscriptionType
            FROM    @UserIds.nodes('/UserIds/users') AS A ( n )

    INSERT  INTO TADistributionListSite
            ( Id ,
              DistributionListId ,
              SiteId
            )
            SELECT  NEWID() ,
                    *
            FROM    ( SELECT    Id ,
                                ApplicationId
                      FROM      TADistributionLists
                      EXCEPT
                      SELECT    DistributionListId ,
                                SiteId
                      FROM      TADistributionListSite
                    ) T

    UPDATE  DLS
    SET     DLS.COUNT = ( SELECT    COUNT(1)
                          FROM      TADistributionListUser
                          WHERE     DistributionListId = @DistributionListId
                                    AND ContactListSubscriptionType != 3
                        )
    FROM    dbo.TADistributionListSite DLS
    WHERE   DLS.DistributionListId = @DistributionListId


GO
PRINT 'Altering procedure Users_ImportBatch'
GO

ALTER PROCEDURE [dbo].[Users_ImportBatch]
    (
      @OverwriteExistingContacts BIT ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @BatchId UNIQUEIDENTIFIER ,
      @BatchUserTable VARCHAR(250) ,
      @BatchMembershipTable VARCHAR(250) ,
      @BatchProfileTable VARCHAR(250) ,
      @BatchSiteUserTable VARCHAR(250) ,
      @BatchUserDistributionListTable VARCHAR(250) ,
      @BatchUSMarketierUserTable VARCHAR(250) ,
      @BatchIndexTermTable VARCHAR(250) ,
      @BatchAddressTable VARCHAR(250) ,
      @OverrideWSUSersAndCustomers BIT = 0
    )
AS 
    BEGIN
        DECLARE @BatchTablePrefix VARCHAR(250) ,
            @BatchIdMapTableName VARCHAR(250) ,
            @StrModifiedBy VARCHAR(36) ,
            @ImportedContacts INT ,
            @AddedToDistributionList INT ,
            @ExistingContacts INT ,
            @UpdatedContacts INT
		
        SET @AddedToDistributionList = 0
        SET @ImportedContacts = 0
        SET @UpdatedContacts = 0

	-- set up temp table name for batch import user id mapping
        SET @BatchTablePrefix = 'ZTMP' + REPLACE(CONVERT(VARCHAR(36), @BatchId),
                                                 '-', '')
        SET @BatchIdMapTableName = @BatchTablePrefix + '_BatchImportIdMap'
        SET @StrModifiedBy = CONVERT(VARCHAR(36), @ModifiedBy)
	
	--start delete duplicate records within the temporary tables if any
        EXEC('delete from ' + @BatchUserTable + '  where ID in (select ID from
        (Select Id, row_number()over(partition by Email order by CreatedDate) as rnum--,Email,FirstName,LastActivityDate
        from ' + @BatchUserTable + '
        Where Email in (
        select Email
        from ' + @BatchUserTable + ' U
        Group by Email
        Having count(*)>1
        )) Duplicates where rnum >1)')
	
        EXEC ('delete from ' + @BatchMembershipTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchProfileTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchSiteUserTable + ' where ContactId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUserDistributionListTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUSMarketierUserTable + ' where UserId not in (select Id from '+@BatchUserTable+')')	
        EXEC ('delete from ' + @BatchAddressTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	--end delete duplicate records within the temporary tables if any


        IF EXISTS ( SELECT  *
                    FROM    sysobjects
                    WHERE   name = @BatchIdMapTableName ) 
            EXEC('Drop Table ' + @BatchIdMapTableName)

	-- Step 1 locate existing records and create mapping
        EXEC(
        'Create Table ' + @BatchIdMapTableName + '(' +
        'importId UniqueIdentifier, ' +
        'actualId UniqueIdentifier, ' +
        'importAddressId UniqueIdentifier, ' +
        'actualAddressId uniqueidentifier ' +
        ')'
        )

        PRINT @BatchIdMapTableName

        EXEC(
        'Create Index ' + @BatchTablePrefix + '_MappingImportId On ' + @BatchIdMapTableName + '(importId)'
        )

        PRINT @BatchTablePrefix 

        PRINT '1a: locate existing users and populate id mapping'
            + @BatchUSMarketierUserTable
	-- 1a: locate existing users and populate id mapping
        EXEC(
        'Insert Into ' + @BatchIdMapTableName + ' (importId, actualId, importAddressId, actualAddressId) ' +
        'Select z.Id, m.UserId, z.AddressId, m.AddressId ' +
        'From ' + @BatchUserTable + ' z ' +			
        'Join vw_contacts m on m.Email = z.Email ' 
			
        )

        PRINT @BatchMembershipTable 
        PRINT '1b: update batch table user id fields to the existing user''s id'

	-- 1b: update batch table user id fields to the existing user's id
        EXEC(
        'Update ' + @BatchUserTable + ' Set ' +
        'ID = m.actualId ' +
        'From ' + @BatchUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.Id'
        )
		
        PRINT @BatchAddressTable
        EXEC(
        'Update ' + @BatchAddressTable + ' Set ' +
        'ID = isnull(m.actualAddressId, m.importAddressId), UserId = m.actualId ' +
        'From ' + @BatchAddressTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 

        EXEC(
        'Update ' + @BatchMembershipTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchMembershipTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 


        EXEC(
        'Update ' + @BatchProfileTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchProfileTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchProfileTable

        EXEC(
        'Update ' + @BatchSiteUserTable + ' Set ' +
        'ContactId = m.actualId ' +
        'From ' + @BatchSiteUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.ContactId'
        )

        PRINT @BatchSiteUserTable

        EXEC(
        'Update ' + @BatchUSMarketierUserTable + ' Set ' +
        'UserId = m.actualId, ' +
        'AddressId = m.actualAddressId ' +
        'From ' + @BatchUSMarketierUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUSMarketierUserTable


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchUserDistributionListTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUserDistributionListTable

	-- Insert Index term
        DECLARE @idxTerms INT
        CREATE TABLE #tmpIdxTerms ( recCount INT )

        EXEC('insert into #tmpIdxTerms (recCount) select count(*) from ' + @BatchIndexTermTable)

        SELECT  @idxTerms = recCount
        FROM    #tmpIdxTerms
        IF @idxTerms > 0 
            BEGIN
                EXEC Users_ImportIndexTerms @BatchIndexTermTable,
                    @BatchSiteUserTable
            END
        DROP TABLE #tmpIdxTerms

	-- To get existing records count
        DECLARE @ParmDefinition NVARCHAR(100) ;
        DECLARE @ExistingContactsStr NVARCHAR(30)
        DECLARE @query NVARCHAR(500)
        SET @query = 'select @result =count(U.Id) from MKContact U inner join '
            + @BatchUserTable + ' z on z.id = u.id'
        SET @ParmDefinition = N'@result varchar(30) OUTPUT' ;	 
        EXEC sp_executesql @query, @ParmDefinition,
            @result = @ExistingContactsStr OUTPUT
        SET @ExistingContacts = CAST(@ExistingContactsStr AS INT)
        PRINT 'remove system users'
-- remove system users
        PRINT 'delete batchusertable system users'
        EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchMembershipTable system users'
        EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchProfileTable system users'
        EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchSiteUserTable system users'
        EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchUserDistributionListTable system users'
        EXEC ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchUSMarketierUserTable system users'
        EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchAddressTable system users'
        EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')

-- remove commerce users
        PRINT 'remove commerce users'
        SELECT  *
        FROM    uscommerceuserprofile
        IF ( @OverrideWSUSersAndCustomers <> 1 ) 
            BEGIN
                PRINT 'DELETE @BatchUserTable'
                EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchMembershipTable'
                EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchProfileTable'
                EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchSiteUserTable'
                EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from vw_contacts where contactType in(1,2))')
--exec ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchUSMarketierUserTable'
                EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchAddressTable'
                EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
            END
	
        PRINT 'Step 2: Update records for existing users'
	-- Step 2: Update records for existing users
        IF ( @OverwriteExistingContacts = 1
             OR @OverrideWSUSersAndCustomers = 1
           ) 
            BEGIN
		-- Step 2a: User Record
                EXEC(
                'Update MKContact Set ' +
                'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
                'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
                'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone,AddressId=z.AddressId, '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From MKContact u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id'
                )
			
                SET @UpdatedContacts = ( SELECT @@ROWCOUNT
                                       )
                SET @ImportedContacts = ( @ImportedContacts + @UpdatedContacts )
                PRINT 'Step 2a Must update User records outside of MKContact'
		--Step 2a Must update User records outside of MKContact
                EXEC(
                'Update USUser Set ' +
                'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
                'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
                'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone, '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From USUser u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id' +
				'LEFT JOIN MKContact MK ON MK.Id = U.Id WHERE MK.Id IS NULL'
                )
			
                SET @UpdatedContacts = ( SELECT @@ROWCOUNT
                                       )
                SET @ImportedContacts = ( @ImportedContacts + @UpdatedContacts )
		
		
		
		--SET @ImportedContacts = (SELECT @@ROWCOUNT)
		-- Step 2b: Profile Record
		-- update profile properties that exist
		
		-- THIS NEEDS TO BE MOVED TO ATTRIBUTE MODEL LOGIC
		
		--Exec(
		--	'Update USMemberProfile Set ' +
		--		'PropertyValueString = z.PropertyValueString, ' +
		--		'PropertyValuesBinary = z.PropertyValuesBinary, ' +
		--		'LastUpdatedDate = z.LastUpdatedDate ' +
		--	'From USMemberProfile u ' +
		--		'Join ' + @BatchProfileTable + ' z on z.userid = u.userid and z.PropertyName = u.PropertyName'
		--	)
		
	
		
		
		
		
                PRINT 'UPDATE GLADDRESS'
                EXEC(
                'Update GLAddress Set ' +
                'AddressType = z.AddressType, ' + 
                'AddressLine1 = z.AddressLine1, ' +
                'AddressLine2 = z.AddressLine2, ' +
                'City = z.City, ' +
                'StateId = s.Id, ' +
                'Zip = z.Zip, ' +
                'CountryId = c.Id, ' +
                'ModifiedDate = z.ModifiedDate, ' +
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'Status = z.Status ' +
                'From GLAddress u ' +
                'join ' + @BatchAddressTable + ' z on z.Id = u.Id ' +
                'join GLState s on s.StateCode = z.State ' +
                'join GLCountry c on c.CountryName = z.Country '
				
                )
		-- Removed by NBOWMAN - new contact batch table should have address ID in it
		--Exec('Update MKContact Set ' +
		--	'AddressId=z.AddressId '+			
		--	'From MKContact u ' +
		--		'join ' + @BatchUSMarketierUserTable + ' z on z.Userid = u.Id')
				
            END
        PRINT 'Insert Records for users that do not exist'
	-- Step 3: Insert Records for users that do not exist
	-- Step 3a: User record
        EXEC(
        'INSERT INTO dbo.MKContact
        ( Id ,
        Email ,
        FirstName ,
        MiddleName ,
        LastName ,
        CompanyName ,
        Gender ,
        BirthDate ,
        HomePhone ,
        MobilePhone ,
        OtherPhone ,
        ImageId ,
        AddressId ,
        Notes ,
        ContactSourceId ,
        Status ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate ,
        LastSynced
        ) ' +
        'select 
        z.Id ,
        z.Email ,
        z.FirstName ,
        z.MiddleName ,
        z.LastName ,
        z.CompanyName ,
        z.Gender ,
        z.BirthDate ,
        z.HomePhone ,
        z.MobilePhone ,
        z.OtherPhone ,
        z.ImageId ,
        z.AddressId ,
        z.Notes ,
        z.ContactSourceId ,
        z.Status ,
        z.CreatedBy ,
        z.CreatedDate ,
        z.ModifiedBy ,
        ISNULL(z.ModifiedDate, Z.CreatedDate) ,
        z.LastSynced ' +
        'from ' + @BatchUserTable + ' z ' +
        'Left Outer Join vw_contacts u on u.UserId = z.Id ' +
        'Where u.UserId is null'
        )
        SET @ImportedContacts = ( @ImportedContacts + ( SELECT
                                                              @@ROWCOUNT
                                                      ) )
        PRINT 'Insert into GLAddress'
        EXEC('insert into GLAddress(Id, AddressType, AddressLine1, AddressLine2, City, StateId, Zip, CountryId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status) ' +
        'select ISNULL(z.Id, NEWID()), z.AddressType, z.AddressLine1, z.AddressLine2, z.City, s.Id, z.Zip, c.Id, z.CreatedDate, z.CreatedBy, null, null, z.Status ' +
        'from ' + @BatchAddressTable + ' z ' +
        'left outer join GLCountry c on c.CountryName = z.Country ' +
        'left outer join GLState s on s.StateCode = z.State and s.CountryId = c.Id ' +
        'left outer join GLAddress a on a.Id = z.Id ' +
        'Where a.Id is null '
        )

        EXEC ('UPDATE MKContact SET AddressId = ISNULL(z.Id, NEWID()) ' + 
        'from  ' + @BatchAddressTable + '  z  '+
        'INNER Join MKContact u on u.Id = z.userId ')

	-- Step 3b: Membership Record
	--Exec(
	--	'insert into USMembership (UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, ' +
	--			'Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved,  ' +
	--			'IsLockedOut, LastLoginDate, LastPasswordChangedDate, LastLockoutDate,  ' +
	--			'FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart,  ' +
	--			'FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart) ' +
	--	'select z.UserId, z.Password, z.PasswordFormat, z.PasswordSalt, z.MobilePIN, ' +
	--			'z.Email, z.LoweredEmail, z.PasswordQuestion, z.PasswordAnswer, z.IsApproved,  ' +
	--			'z.IsLockedOut, z.LastLoginDate, z.LastPasswordChangedDate, z.LastLockoutDate,  ' +
	--			'z.FailedPasswordAttemptCount, z.FailedPasswordAttemptWindowStart,  ' +
	--			'z.FailedPasswordAnswerAttemptCount, z.FailedPasswordAnswerAttemptWindowStart ' +
	--	'from ' + @BatchMembershipTable + ' z ' +
	--		'left outer join USMembership u on u.UserId = z.UserId ' +
	--	'where u.UserId is null '
	--	)

	-- Step 3c: Profile Record
	
	--THIS NEEDS TO BE UPDATED TO ATTRIBUTE MODEL
	
	--Exec(
	--	'insert into USMemberProfile (UserId, PropertyName, PropertyValueString, ' +
	--		'PropertyValuesBinary, LastUpdatedDate) ' +
	--	'select z.UserId, z.PropertyName, z.PropertyValueString, z.PropertyValuesBinary, ' +
	--		'z.LastUpdatedDate ' +
	--	'from ' + @BatchProfileTable + ' z ' +
	--		'left outer join USMemberProfile u on u.UserId = z.UserId and u.PropertyName = z.PropertyName ' +
	--	'where u.PropertyName is null '
	--	)


	---------------------ATTRIBUTE INSERT-----------------------
        EXEC(
        'SELECT * FROM '+@BatchProfileTable+'')
		
             
                    
        CREATE TABLE #tmpContactAttributes
            (
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              VALUE NVARCHAR(4000) ,
              UserId UNIQUEIDENTIFIER
            )
                    
                    
        PRINT 'insert into #tmpContactAttributes'
        INSERT  INTO #tmpContactAttributes
                EXEC
                    ( '	 SELECT  
                A.Id AS AttributeId ,
                AE.Id AS AttributeEnumId ,
                P.PropertyValueString AS Value ,
                P.UserId                 
                FROM   ' + @BatchProfileTable
                      + ' P
                INNER JOIN dbo.ATAttribute A ON A.Title = P.PropertyName
                INNER JOIN dbo.ATAttributeCategoryItem ACI ON ACI.AttributeId = A.Id
                INNER JOIN dbo.ATAttributeCategory AC ON AC.ID = ACI.CategoryId
                AND AC.Name = ''Contact Attributes''
                LEFT JOIN dbo.ATAttributeEnum AE ON AE.AttributeID = A.Id
                AND AE.Title = P.PropertyValueString
                WHERE  ( A.IsEnum = 1
                AND AE.ID IS NOT NULL
                )
                OR A.IsEnum = 0'
                    )
 
 
        DELETE  CAV
        FROM    dbo.ATContactAttributeValue CAV
                INNER JOIN #tmpContactAttributes TCA ON TCA.AttributeId = CAV.AttributeId
                                                        AND TCA.UserId = CAV.ContactId
 
        INSERT  INTO dbo.ATContactAttributeValue
                ( Id ,
                  ContactId ,
                  AttributeId ,
                  AttributeEnumId ,
                  Value ,
                  Notes ,
                  CreatedDate ,
                  CreatedBy
         
                        
                )
                SELECT  NEWID() ,
                        UserId ,
                        AttributeId ,
                        AttributeEnumId ,
                        Value ,
                        '' ,
                        GETUTCDATE() ,
                        '6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
                FROM    #tmpContactAttributes


		
		
		-------------------------------------------------------------




        PRINT ' Step 3d: Site User Record'
	-- Step 3d: Site User Record
        EXEC(
        'insert into MKContactSite (SiteId, ContactId,  Status) ' +
        'select z.SiteId, z.ContactId, 1' +
        'from ' + @BatchSiteUserTable + ' z ' +
        ' inner join MKContact MC ON MC.Id = z.ContactId'+
        ' left outer join MKContactSite u on u.ContactId = z.ContactId ' +
        'where u.ContactId is null '
        )

	-- Step 3e: Distribution List Record

        PRINT 'Update ' + @BatchUserDistributionListTable + ' Set '
            + '  ListId = null ' + 'From ' + @BatchUserDistributionListTable
            + ' z '
            + 'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        ' ListId = null ' +
        ' from  ' + @BatchUserDistributionListTable + ' z ' +
        'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'
        )
        SET @AddedToDistributionList = ( SELECT @@ROWCOUNT
                                       )

        PRINT 'insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '
            + ' select newid(),z.ListId,z.UserId ' + 'from '
            + @BatchUserDistributionListTable + ' z '
            + 'Left Outer Join TADistributionListUser u on u.UserId = z.UserId '
            + 'Where u.UserId is null and z.ListId is not null '


        EXEC ('insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '+
        ' select newid(),z.ListId,z.UserId ' + 
        'from ' + @BatchUserDistributionListTable + ' z ' +
        'Join vw_contacts u on u.userid = z.UserId ' +
        'Where z.ListId is not null ')
        SET @AddedToDistributionList = ( @AddedToDistributionList + ( SELECT
                                                              @@ROWCOUNT
                                                              ) )

        INSERT  INTO TADistributionListSite
                ( Id ,
                  DistributionListId ,
                  SiteId
                )
                SELECT  NEWID() ,
                        *
                FROM    ( SELECT    Id ,
                                    ApplicationId
                          FROM      TADistributionLists
                          EXCEPT
                          SELECT    DistributionListId ,
                                    SiteId
                          FROM      TADistributionListSite
                        ) T


        EXEC (' UPDATE TLS
        SET    TLS.COUNT = TV.ContactCount
        FROM   ( SELECT    DLU.DistributionListId ,
        COUNT(DLU.Id) AS ContactCount
        FROM      dbo.TADistributionListUser DLU
        INNER JOIN dbo.TADistributionListSite TLS ON DLU.DistributionListId = TLS.DistributionListId
        GROUP BY  DLU.DistributionListId
        ) TV
        INNER JOIN dbo.TADistributionListSite TLS ON TLS.DistributionListId = TV.DistributionListId
        INNER JOIN ' + @BatchUserDistributionListTable + ' z ON z.ListId = TV.DistributionListId' )


	

	-- peform cleanup
	--Exec('Drop Table ' + @BatchIdMapTableName)
--
--	-- empty batch tables

	--Exec('truncate table ' + @BatchUserTable)
	--Exec('truncate table ' + @BatchMembershipTable)
	--Exec('truncate table ' + @BatchProfileTable)
	--Exec('truncate table ' + @BatchSiteUserTable)
	--exec('truncate table ' + @BatchAddressTable)
	--exec('truncate table ' + @BatchUserDistributionListTable)
	--exec('truncate table ' + @BatchUSMarketierUserTable)
	--exec('truncate table ' + @BatchIndexTermTable)
	--exec('truncate table ' + @BatchAddressTable)

        UPDATE  UploadContactData
        SET     ImportedContacts = @ImportedContacts ,
                AddedToDistributionList = @AddedToDistributionList ,
                ExistingRecords = @ExistingContacts ,
                UpdatedContacts = @UpdatedContacts
        WHERE   UploadHistoryId = @BatchId
    END

GO

PRINT 'Creating index IX_TADistributionLists_Id_ListType'
GO
IF NOT EXISTS(
SELECT * 
FROM sys.indexes 
WHERE name='IX_TADistributionLists_Id_ListType' AND object_id = OBJECT_ID('TADistributionLists'))
CREATE UNIQUE NONCLUSTERED INDEX [IX_TADistributionLists_Id_ListType] ON [dbo].[TADistributionLists] 
(
	[Id] ASC,
	[ListType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

PRINT 'Handle Bad Data in COTaxonomyTable'
GO
--Make ApplicationId as NULL if LftVale, RgtVale, ParentId and SiteId are null.
Update COTaxonomy Set ApplicationId = NULL where LftValue IS NULL AND RgtValue IS NULL AND ParentID IS NULL AND SiteId IS NULL AND Status = 1
GO

