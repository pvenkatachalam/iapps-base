﻿GO
PRINT 'Updating the version of the products to 5.1.0531.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.0531.1'
GO
PRINT 'Creating table RPTCommerceReport_Category'
GO
IF(OBJECT_ID('RPTCommerceReport_Category','U') IS NULL)
	CREATE TABLE [dbo].[RPTCommerceReport_Category]
	(
		 Id UNIQUEIDENTIFIER ,
		 OrderTotal DECIMAL(18, 2) ,
		 Quantity DECIMAL(18, 2) ,
		 NavNodeId UNIQUEIDENTIFIER ,
		 OrderDate DATETIME
	)
GO

IF not exists(select 1 from sys.indexes where name='IX_RPTCommerceReport_Category_OrderDate')

	CREATE CLUSTERED INDEX IX_RPTCommerceReport_Category_OrderDate
	ON dbo.RPTCommerceReport_Category (OrderDate) ; 

GO
PRINT 'Altering procedure Commerce_GetURLForSiteMap'
 GO
ALTER PROCEDURE [dbo].[Commerce_GetURLForSiteMap]
(@SiteId uniqueidentifier)
as 
 WITH
            ProductsNavigation AS
            (
                  SELECT      p.Id AS ProductId, (case when LOWER(seo.FriendlyUrl) is null then LOWER(dbo.GetProductTypePath(pt.Id) + N'id-' + p.ProductIDUser + '/' + UrlFriendlyTitle)
                              else LOWER(seo.FriendlyUrl) end )AS FriendlyUrl, (case when p.ModifiedDate is null then p.CreatedDate else p.ModifiedDate end) as ModifiedDate
                  FROM 
                  (SELECT DISTINCT ProductId FROM PRProductSKU WHERE IsActive=1 And IsOnline=1 ) AS sku
                          INNER JOIN PRProduct AS p ON p.Id = sku.ProductId
                          INNER JOIN PRProductType AS pt ON p.ProductTypeId = pt.Id
                          LEFT JOIN PRProductSEO AS seo ON p.Id = seo.ProductId where p.SiteId=@SiteId
            )
			,

            ProductsViewCount AS
            (
                  SELECT            ProductId, COUNT(ProductId) AS ViewCount
                  FROM        TRProductView
                  GROUP BY    ProductId
            )
			,
			ProductViewMax AS
			(
			SELECT isnull(Max(ViewCount),1) ViewMax FROM ProductsViewCount
			)
			,
			ProductPriority as
			(
			SELECT pvc.ProductId, cast((case when isnull(pvc.ViewCount,0)=0 then 0.5 else pvc.ViewCount end)/M.ViewMax as decimal(2,1)) as Priority
			FROM ProductsViewCount AS pvc
			CROSS JOIN ProductViewMax M
			)




 --INSERT INTO #tblProduct
  SELECT pn.ProductId as ProductId, CASE CHARINDEX('/',FriendlyUrl) WHEN 1 THEN SUBSTRING(FriendlyUrl,2,LEN(FriendlyUrl)-1) ELSE FriendlyUrl end as url, ModifiedDate LastModifiedDate, 
  case when pp.Priority is null then 0.5 
		when pp.Priority<0.5 then 0.5
		else pp.Priority
		end as Priority,
  case 
   when pn.ModifiedDate > dateadd(dd,-1,getdate()) then 'daily'  
   when pn.ModifiedDate > dateadd(dd,-7,getdate()) then 'weekly' 
   when pn.ModifiedDate > dateadd(mm,-1,getdate()) then 'monthly' 
  else 'weekly'
  end
  as ChangeFrequency
  FROM   ProductsNavigation AS pn 
  LEFT JOIN ProductsViewCount AS pvc ON pvc.ProductId = pn.ProductId
  LEFT JOIN ProductPriority AS pp ON pp.ProductId = pn.ProductId


  --INSERT INTO #tblProduct
  /*SELECT pn.ProductId as ProductId, SUBSTRING(FriendlyUrl,2,LEN(FriendlyUrl)-1) url, ModifiedDate LastModifiedDate, 
  case when pp.Priority is null then 0.5 
		when pp.Priority<0.5 then 0.5
		else pp.Priority
		end as Priority,
  case 
   when pn.ModifiedDate > dateadd(dd,-1,getdate()) then 'daily'  
   when pn.ModifiedDate > dateadd(dd,-7,getdate()) then 'weekly' 
   when pn.ModifiedDate > dateadd(mm,-1,getdate()) then 'monthly' 
  else 'weekly'
  end
  as ChangeFrequency
  FROM   ProductsNavigation AS pn 
  LEFT JOIN ProductsViewCount AS pvc ON pvc.ProductId = pn.ProductId
  LEFT JOIN ProductPriority AS pp ON pp.ProductId = pn.ProductId*/



GO
PRINT 'Altering procedure CMSSite_CreateDefaultData'
GO
alter PROCEDURE [dbo].[CMSSite_CreateDefaultData] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@SiteId uniqueidentifier,
	@CreatedBy uniqueidentifier,
	@CopyFromSiteId uniqueidentifier = null
)
as	
BEGIN
	Declare @Now datetime 
	SET @Now =getutcdate()
	Declare @SiteTitle varchar(512)
	--Get the Site Information
	Select @SiteTitle = Title from SISite Where Id = @SiteId

	Declare @p1 uniqueidentifier
	EXEC Taxonomy_Save 
		@Id=@p1 output,
		@ApplicationId=@SiteId,
		@Title=N'Index Terms', 
		@CreatedBy=@CreatedBy,
		@Status=1

	Insert into PageMapBase (SiteId, ModifiedDate)
		Values(@SiteId, @Now)
	
	DECLARE @pageMapNodeXml NVARCHAR(max)
	DECLARE @pageMapNodeId uniqueidentifier
	SET @pageMapNodeId = @SiteId
	
	--SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
	--					 displayTitle="CommerceNav" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="1" 
	--					 targetId="00000000-0000-0000-0000-000000000000" 
	--					 propogateWorkFlow="True" inheritWorkFlow="False" 
	--					 propogateSecurityLevels="True" inheritSecurityLevels="False" 
	--					 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'
						 
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" description="' + @SiteTitle + '"  
		   displayTitle="' + @SiteTitle + '" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="1"   
		   targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
		   createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
		   propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="True"   
		   inheritRoles="False" roles="" securityLevels="" locationIdentifier="" />'  						 

	EXEC PageMapNode_Save @Id =@pageMapNodeId,
			@SiteId =@SiteId,
			@ParentNodeId = NULL,
			@PageMapNode= @pageMapNodeXml,
			@CreatedBy = @CreatedBy
			
	SET @pageMapNodeId = NEWID()
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
						 displayTitle="Unassigned" friendlyUrl="Unassigned" description="Unassigned" menuStatus="1" 
						 parentId="' + CAST(@SiteId AS CHAR(36)) + '" 
					     targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
						 createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
						 propogateSecurityLevels="True" inheritSecurityLevels="False" 
						 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'

	EXEC PageMapNode_Save @Id = @pageMapNodeId,
			@SiteId = @SiteId,
			@ParentNodeId = @SiteId,
			@PageMapNode = @pageMapNodeXml,
			@CreatedBy = @CreatedBy

	Declare @ProductId uniqueidentifier
    Select @ProductId = Id from iAppsProductSuite where Title like 'Analyzer'
    if(@ProductId is not null)
      exec Membership_CreateDefaultMembership @SiteId,@ProductId
	Select @ProductId = Id from iAppsProductSuite where Title like 'Commerce'
	if(@ProductId is not null)
		exec Membership_CreateDefaultMembership @SiteId,@ProductId
	Select @ProductId = Id from iAppsProductSuite where Title like 'Marketier'
	if(@ProductId is not null)
		exec Membership_CreateDefaultMembership @SiteId,@ProductId

	insert into STSiteSetting
	select 
	@SiteId,
	SettingTypeId,
	Value
	from STSiteSetting where 
	SiteId IN (
		select top 1 S.Id from SISite S Where Status = 1 and Id in 
		(Select distinct SiteId from STSiteSetting)
	)


	-- Otherwise it won't display in workflow author/approver/publisher tab. below it is adding for the site 

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId , Id, 2, 1, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Global Author')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 2, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Global Approver')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 3, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Global Publisher')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 4, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Content Administrator')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 5, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Site Administrator')

	--INSERT INTO  USMemberRoles  (ProductId, ApplicationId,MemberId,MemberType,RoleId,ObjectId,ObjectTypeId,Propagate)
	--SELECT 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', @SiteId, Id, 2, 6, 'CBB702AC-C8F1-4C35-B2DC-839C26E39848', 1,1 FROM USGroup Where Title in ('Installation Administrator')


	-- GLObjectType
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Site')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(1,'Site')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='MenuItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(2,'MenuItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Template')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(3,'Template')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Style')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(4,'Style')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Container')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(5,'Container')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='SiteDirectory')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(6,'SiteDirectory')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Content')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(7,'Content')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Page')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(8,'Page')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='AssetFile')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(9,'AssetFile')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Folder')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(10,'Folder')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='List')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(11,'List')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Taxonomy')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(12,'Taxonomy')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='XMLForm')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(13,'XMLForm')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='User')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(14,'User')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Group')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(15,'Group')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Role')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(16,'Role')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Permission')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(17,'Permission')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Workflow')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(18,'Workflow')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Job')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(19,'Job')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Task')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(20,'Task')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Emails')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(21,'Emails')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='DistributionList')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(22,'DistributionList')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='EmailTemplate')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(23,'EmailTemplate')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='EventType')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(24,'EventType')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='FWFeed')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(25,'FWFeed')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssChannel')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(26,'RssChannel')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssCategory')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(27,'RssCategory')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssCloud')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(28,'RssCloud')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RssImage')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(29,'RssImage')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='RegisteredRssItemHandler')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(30,'RegisteredRssItemHandler')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Links')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(31,'Links')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Version')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(32,'Version')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='AssetImage')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(33,'AssetImage')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Calendar')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(34,'Calendar')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Script')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(35,'Script')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Product')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(36,'Product')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='AudienceSegment')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(37,'AudienceSegment')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Form')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(38,'Form')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Blog')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(39,'Blog')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Post')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(40,'Post')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='FormResponse')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(47,'FormResponse')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Video')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(48,'Video')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Store')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(201,'Store')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Catalog')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(202,'Catalog')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Category')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(203,'Category')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ProductType')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(204,'ProductType')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='CommerceProduct')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(205,'CommerceProduct')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='SKU')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(206,'SKU')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Filter')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(207,'Filter')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ProductImage')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(208,'ProductImage')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='NavItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(209,'NavItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Inventory')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(210,'Inventory')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Customer')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(211,'Customer')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Order')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(212,'Order')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='OrderShipping')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(213,'OrderShipping')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='OrderItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(214,'OrderItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingWishList')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(215,'ShoppingWishList')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingWishListItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(216,'ShoppingWishListItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingCart')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(217,'ShoppingCart')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='ShoppingCartItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(218,'ShoppingCartItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='FeaturedItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(219,'FeaturedItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='CrossSellUpSellItem')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(220,'CrossSellUpSellItem')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Bundle')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(221,'Bundle')  END
	IF NOT EXISTS(SELECT * FROM GLObjectType WHERE Name ='Campaign')  BEGIN   INSERT INTO GLObjectType (Id, Name) values(301,'Campaign')  END

	-- Roles
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =1)  BEGIN  INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal) Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',1,'GlobalAuthor','Author for all ObjectTypes',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =2)  BEGIN  INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal) 	Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',2,'GlobalApprover','Approver for all ObjectTypes',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =3)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',3,'GlobalPublisher','Publisher for all ObjectTypes',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =4)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',4,'ContentAdministrator','Content Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =5)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',5,'SiteAdministrator','Site Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =6)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',6,'InstallationAdministrator','Installation Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =7)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',7,'Author','Represents Author role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =8)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',8,'Approver','Represents Approver role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =9)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',9,'Publisher','Represents Publisher role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =10)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',10,'Archiver','Represents Archiver role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =11)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',11,'NavEditor','Represents Navigational editor role in the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =12)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',12,'Manager','Represents Manage role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =13)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',13,'Viewer','Represents View role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =14)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',14,'CustomPermission','Represents CustomPermission role in the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =15)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',15,'EveryOne','Represents EveryOne role in the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =16)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CAB9FF78-9440-46C3-A960-27F53D743D89',16,'Administrator','Represents Administrator role in the Analytics product',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =17)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CAB9FF78-9440-46C3-A960-27F53D743D89',17,'Analyst','Represents Analyst role in the Analytics product',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =18)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CAB9FF78-9440-46C3-A960-27F53D743D89',18,'AnalystCustomPermission','Represents AnalystCustompermission role in the Analytics product',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =19)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',19,'Merchandising','Merchandising for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =20)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',20,'Fulfillment','Fulfillment for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =21)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',21,'Marketing','Marketing for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =22)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',22,'CSR','CSR for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =23)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',23,'BlogAdministrator','Blog Administrator for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =24)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',24,'MarketierAdmin','Marketier Admin for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =25)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',25,'Marketer','Marketer for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =26)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',26,'LibraryAdmin','Library Admin for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =27)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',27,'GlobalCampaignApprover','Global Campaign Approver',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =28)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',28,'GlobalCampaignPublisher','Global Campaign Publisher',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =29)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',29,'EveryOne','Commerce Everyone for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =30)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CBB702AC-C8F1-4C35-B2DC-839C26E39848',30,'CustomNavEditor','Represents custom Navigational editor role in the system',0)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =31)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E',31,'GlobalCampaignAuthor','Global Campaign Author',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =32)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',32,'CSRManager','CSR Manger for the system',1)  END
	IF NOT EXISTS(SELECT * FROM USRoles WHERE Id =33)  BEGIN   INSERT INTO USRoles (ApplicationId, [Id], [Name], [Description], IsGlobal)    Values('CCF96E1D-84DB-46E3-B79E-C7392061219B',33,'WHManager','Warehouse Manger for the system',1)  END

	EXEC [CMSSite_InsertDefaultPermission] @SiteId
	-- copy all existing main admin users to new site
	--IF NOT EXISTS (SELECT * FROM USMemberGroup WHERE ApplicationId=@SiteId)   
	--BEGIN   
		INSERT INTO USMemberGroup (ApplicationId, MemberId, MemberType, GroupId)
		SELECT distinct @SiteId, MemberId, MemberType, GroupId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
		) AND ApplicationId = @CopyFromSiteId
		
		Except
		
		SELECT distinct @SiteId, MemberId, MemberType, GroupId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
		) AND ApplicationId = @SiteId
		
	--END

	--IF NOT EXISTS (SELECT * FROM USSiteUser WHERE SiteId=@SiteId)   
	--BEGIN
		INSERT INTO USSiteUser (SiteId, UserId, IsSystemUser, ProductId)    
		
		SELECT @SiteId, UserId, IsSystemUser, ProductId FROM USSiteUser WHERE UserId IN (
			SELECT distinct MemberId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
			) AND ApplicationId = @CopyFromSiteId ) 
		AND SiteId = @CopyFromSiteId
		
		Except
		
		SELECT @SiteId, UserId, IsSystemUser, ProductId FROM USSiteUser WHERE UserId IN (
			SELECT distinct MemberId FROM USMemberGroup WHERE GroupId IN 
			(SELECT Id FROM USGroup Where Title in ('Content Administrator','Site Administrator','Installation Administrator','Administrator')
			) AND ApplicationId = @CopyFromSiteId ) 
		AND SiteId = @SiteId
		
	--END

	--- Inserting default SiteSetting Data
	EXEC CMSSite_CreateDefaultSiteSetting @SiteId  --- This will call CodeLibrary_CreateDefaultSiteSetting SP

	EXEC Site_CreateCommerceDefaultMenuAndPages  @SiteId, @SiteTitle, @CreatedBy

	EXEC Site_CopyCommerceDefaultData @CopyFromSiteId, @SiteId ,@SiteTitle, @CreatedBy

	EXEC Site_CreateCommerceDefaultData @SiteId,@SiteTitle, @CreatedBy

	EXEC [Site_CreateTemplateBuilderData] @SiteId


	 INSERT INTO [dbo].[TAEmailTemplate]  
		  ([Id]  
		  ,[Key]  
		  ,[Title]  
		  ,[Description]  
		  ,[Subject]  
		  ,[MimeType]  
		  ,[CreatedBy]  
		  ,[CreatedDate]  
		  ,[ModifiedBy]  
		  ,[ModifiedDate]  
		  ,[Status]  
		  ,[ApplicationId]  
		  ,[SenderDefaultEmail]  
		  ,[MailMergeTags]  
		  ,[ProductId]  
		  ,[Body])  
	 SELECT NEWID()  
		,[Key]  
		,[Title]  
		,[Description]  
		,[Subject]  
		,[MimeType]  
		,[CreatedBy]  
		,GETUTCDATE()  
		,[ModifiedBy]  
		,GETUTCDATE()  
		,[Status]  
		,@SiteId  
		,[SenderDefaultEmail]  
		,[MailMergeTags]  
		,[ProductId]  
		,[Body]  
	   FROM [dbo].[TAEmailTemplate] 
	WHERE Title IN (   
	SELECT  
		[Title]  
	   FROM [dbo].[TAEmailTemplate]  
	  Where ApplicationId = @copyFromSiteId  
	Except
	SELECT  
		[Title]  
	   FROM [dbo].[TAEmailTemplate]  
	  Where ApplicationId = @SiteId  
	)  AND ApplicationId = @copyFromSiteId  
	Order by Title 

	
if not exists (select 1 from COFormStructure Where SiteId=@SiteId )  
Begin  
   
   
 declare @NewFormStructureId uniqueidentifier  
 SET @NewFormStructureId = NEWID()  
 
 --Form library node  
 insert into COFormStructure   
 (  
 Id,  
 ParentId,  
 LftValue,  
 RgtValue,  
 Title,  
 Description,  
 SiteId,  
 [Exists],  
 Attributes,  
 Size,  
 Status,  
 CreatedBy,  
 CreatedDate,  
 ModifiedBy,  
 ModifiedDate,  
 PhysicalPath,  
 VirtualPath,  
 ThumbnailImagePath,  
 FolderIconPath,  
 Keywords,  
 IsSystem,  
 IsMarketierDir,  
 SourceDirId,  
 AllowAccessInChildrenSites  
 )  
 SELECT TOP 1 
 @NewFormStructureId,  
 @SiteId,  
 1,  
 4,  
 Title,  
 Description,  
 @SiteId,  
 [Exists],  
 Attributes,  
 Size,  
 Status,  
 CreatedBy,  
 @Now,  
 ModifiedBy,  
 ModifiedDate,  
 PhysicalPath,  
 VirtualPath,  
 ThumbnailImagePath,  
 FolderIconPath,  
 Keywords,  
 IsSystem,  
 IsMarketierDir,  
 SourceDirId,  
 0  
 FROM COFormStructure  
 WHERE ParentId=@copyFromSiteId  
  
 
 if not exists (select 1 from HSDefaultParent where SiteId=@SiteId And ObjectTypeId=38)  
 begin  
 
 
 
  ----insert into default parent  
  INSERT INTO HSDefaultParent  
  (  
  SiteId,  
  ObjectTypeId,  
  Title,  
  ParentId  
  )  
  VALUES  
  (  
  @SiteId,  
  38,  
  NULL, --form  
  @NewFormStructureId  
  )  
  
 end  
  
  
 
 --For Unassigned node  
 insert into COFormStructure   
 (  
 Id,  
 ParentId,  
 LftValue,  
 RgtValue,  
 Title,  
 Description,  
 SiteId,  
 [Exists],  
 Attributes,  
 Size,  
 Status,  
 CreatedBy,  
 CreatedDate,  
 ModifiedBy,  
 ModifiedDate,  
 PhysicalPath,  
 VirtualPath,  
 ThumbnailImagePath,  
 FolderIconPath,  
 Keywords,  
 IsSystem,  
 IsMarketierDir,  
 SourceDirId,  
 AllowAccessInChildrenSites  
 )  
 SELECT   
 TOP 1  
 newid(),  
 @NewFormStructureId,--THis needs to be hard coded  
 2,  
 3,  
 Title,  
 Description,  
 @SiteId,  
 [Exists],  
 Attributes,  
 Size,  
 Status,  
 CreatedBy,  
 @Now,  
 ModifiedBy,  
 ModifiedDate,  
 PhysicalPath,  
 VirtualPath,  
 ThumbnailImagePath,  
 FolderIconPath,  
 Keywords,  
 IsSystem,  
 IsMarketierDir,  
 SourceDirId,  
 AllowAccessInChildrenSites  
 FROM COFormStructure  
 WHERE SiteId=@copyFromSiteId and Title='Unassigned' AND LftValue=2 and RgtValue=3   
   
  
End  

END
GO
PRINT 'Altering procedure UserManager_GetUserSegment'
GO
ALTER PROCEDURE [dbo].[UserManager_GetUserSegment]  
(  
	@VisitorType int ,  
	@Geo   nvarchar(50),  
	@Resolution  nvarchar(50),  
	@Os    nvarchar(50),  
	@Browser  nvarchar(50),  
	@JavaScriptSupport int,  
	@FlashSupport  int,  
	@SiteId  uniqueidentifier  ,
	@SinceLastVisitDays int = 0,
	@StateCode nvarchar(100) = '',
	@SearchKeyword nvarchar(100)=null,
	@DeviceType int =0,
	@ReferrerType int =0,
	@ReferrerUrl nvarchar(256),
	@EntryPage uniqueidentifier
)  
AS   
BEGIN   
	DECLARE	@ResolutionId		uniqueidentifier,  
			@OsId				uniqueidentifier,  
			@BrowserId			uniqueidentifier,  
			@GeoId				uniqueidentifier,  
			@GeoInt				bigint,  
			@CountryCode		nvarchar(50),  
			@ResolutionOtherId  uniqueidentifier,  
			@OsOtherId			uniqueidentifier,  
			@BrowserOtherId     uniqueidentifier,  
			@GeoOtherId			uniqueidentifier,  
			@Result				xml  
    
	DECLARE @intrinsicFrameworkAccount uniqueidentifier
	SET @intrinsicFrameworkAccount='6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
    
	if(select count(*) from dbo.ALAudienceSegmentCriteria where SegmentType = 0 and CriteriaName = @Browser) =0
	begin 
		insert into dbo.ALAudienceSegmentCriteria values (newid(),@Browser,@Browser,0,@intrinsicFrameworkAccount,getdate(),null,null)
	end
   
	if(select count(*) from dbo.ALAudienceSegmentCriteria where SegmentType =1 and CriteriaName=@Os)=0
	begin 
		insert into dbo.ALAudienceSegmentCriteria values (newid(),@Os,@Os,1,@intrinsicFrameworkAccount,getdate(),null,null)
	end 
    
    if(@ReferrerUrl is null)
		select @ReferrerUrl=''
    
	CREATE TABLE #UserSegment (  
			[SegmentId] [uniqueidentifier] NOT NULL,  
			[Browser] [nvarchar](max) NULL,  
			[OperatingSystems] [nvarchar](max)  NULL,  
			[Resolution] [nvarchar](max) NULL,  
			[Geography] [nvarchar](max) NULL,  
			[IsBrowserFiltered] int default 0 ,  
			[IsOperatingSystemsFiltered] int default 0,  
			[IsResolutionFiltered] int default 0,  
			[IsGeoFiltered] int default 0,
			[SegmentName] nvarchar(100),
			StatesInUs nvarchar(100),
			NoOfDaysSinceLastVisited int,[IsSearchKeywordFiltered] int default 0,
			SearchKeyword nvarchar(100) null,
			PagesVisitedPageGrpCondition int null,
			TotalNoOfPagesVisited int)

	INSERT INTO #UserSegment (SegmentId, Browser ,OperatingSystems,Resolution,Geography,SegmentName,
				StatesInUS,NoOfDaysSinceLastVisited,SearchKeyword,PagesVisitedPageGrpCondition,TotalNoOfPagesVisited)  
	SELECT	a.SegmentId, 
			a.Browser ,
			a.OperatingSystems,
			a.Resolution,
			a.Geography,
			b.SegmentName, 
			a.StatesInUS,
			a.NoOfDaysSinceLastVisited,
			a.SearchKeyword,
			a.TotalPagesVisitedOperator,
			a.TotalNoOfPagesVisited
	FROM dbo.ALAudienceSegmentDetails a, ALAudienceSegment b
	WHERE	(@DeviceType is Null Or a.DeviceType = 0 Or a.DeviceType=@DeviceType) AND a.SegmentId = b.Id and 
			(@ReferrerType is null Or a.ReferrerType=0 Or a.ReferrerType = @ReferrerType) 	
			AND a.VisitorType in (0,3,@VisitorType) -- Not Applicable , Both and identified visitor type  
			 and  a.FlashEnabled in (2,@FlashSupport)  -- Not Applicable and identified flash support  
			and a.JavascriptEnabled in (2,@JavaScriptSupport) -- Not Applicable and identified javascript support  
			and b.SiteId in (select SiteId from [dbo].[GetVariantSites](@siteId))  and b.Status = 1  
			and ( isnull(a.NoOfDaysSinceLastVisited,0) =0 or isnull(a.NoOfDaysSinceLastVisited,0)>=@SinceLastVisitDays) 
			and (a.SegmentId in (select SegmentId from ALAudienceSegmentDetails where (StatesInUs='' or StatesInUs is null or StatesInUs like '%'+ @StateCode +'%')))
			and a.SegmentId in (	
			select ad.SegmentId  from ALAudienceSegmentDetails ad Left join dbo.ALAudienceSegmentReferrers r on ad.SegmentId = r.SegmentId
				where (isnull(r.ReferrerUrl,@ReferrerUrl)='' or r.ReferrerUrl like '%'+ @ReferrerUrl + '%'))
			and a.SegmentId in (select ad.SegmentId  from ALAudienceSegmentDetails ad Left join dbo.ALAudienceSegmentPagesVisited pv on ad.SegmentId = pv.SegmentId
				where (@EntryPage IS NULL OR @EntryPage = dbo.GetEmptyGUID() Or ( pv.PageDefinitionId is null or @EntryPage = pv.PageDefinitionId and pv.IsEntry=1)))
			
			
			---ARUN CODE
						--and (a.SegmentId in (
			--	select SegmentId from ALAudienceSegmentDetails a cross apply dbo.SplitComma(StatesInUS,',') c 
			--	where C.Value in (Select value from dbo.SplitComma(@StateCode,','))) or isnull(StatesInUs,'')='')
			--and (a.SegmentId in (
			--	select ad.SegmentId from ALAudienceSegmentDetails ad ,dbo.ALAudienceSegmentReferrers r 
			--	where (r.ReferrerUrl = @ReferrerUrl )and ad.SegmentId = r.SegmentId ))
			--and (a.SegmentId in (
			--	select ad1.SegmentId from ALAudienceSegmentDetails ad1 cross apply dbo.ALAudienceSegmentPagesVisited pv 
			--	where @EntryPage IS NULL OR @EntryPage = dbo.GetEmptyGUID() Or ( @EntryPage = pv.PageDefinitionId and pv.IsEntry=1)))
			--ARUN CODE 
			
		
	exec GetIPNumber @Geo,@GeoInt OUT  
	select  top 1 @CountryCode = country from dbo.GeoIPCity where @GeoInt between StartIP and EndIP   
  

  
	--GUID of Operating system,Resolution and Browser from ALAudienceSegmentCriteria  
	SELECT @OsId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=1 and AliasCriteriaName=@Os  
	SELECT @ResolutionId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=2 and CriteriaName=@Resolution  
	SELECT @BrowserId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=0 and AliasCriteriaName=@Browser  
	SELECT @GeoId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=3 and AliasCriteriaName=@CountryCode  

	SELECT @ResolutionOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=2 and CriteriaName='Other'  
	SELECT @OsOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=1 and AliasCriteriaName='Other'
	SELECT @BrowserOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=0 and AliasCriteriaName='Other'    
	SELECT @GeoOtherId=Id FROM dbo.ALAudienceSegmentCriteria WHERE SegmentType=3 and AliasCriteriaName='Other'    

	UPDATE #UserSegment Set IsGeoFiltered = 1  
		WHERE (Geography like '%'+ lower(cast(@GeoId as varchar(36))) +'%' or isnull(Geography,'')='')   

	UPDATE #UserSegment Set IsGeoFiltered=1  
		WHERE (Geography like '%'+ lower(cast(@GeoOtherId as varchar(36))) +'%' or isnull(Geography,'')='')   

	DELETE  from #UserSegment Where IsGeoFiltered=0  

	
	UPDATE #UserSegment Set IsBrowserFiltered=1  
		WHERE (Browser like '%'+ lower(cast(@BrowserId as varchar(36))) +'%' or isnull(Browser,'')='')   

	UPDATE #UserSegment Set IsBrowserFiltered=1  
		WHERE (Browser like '%'+ lower(cast(@BrowserOtherId as varchar(36))) +'%' or isnull(Browser,'')='')   

	DELETE  from #UserSegment Where IsBrowserFiltered=0  
	


	Update #UserSegment Set IsOperatingSystemsFiltered=1  
		WHERE (OperatingSystems like '%'+ lower(cast(@OsId as varchar(36))) +'%' or isnull(OperatingSystems,'')='')   

	Update #UserSegment Set IsOperatingSystemsFiltered=1  
		WHERE (OperatingSystems like '%'+ lower(cast(@OsOtherId as varchar(36))) +'%' or isnull(OperatingSystems,'')='')   

	DELETE  from #UserSegment Where IsOperatingSystemsFiltered=0  


	Update #UserSegment Set [IsResolutionFiltered]=1  
		WHERE (Resolution like '%'+ lower(cast(@ResolutionId as varchar(36))) +'%' or isnull(Resolution,'')='')  

	Update #UserSegment Set [IsResolutionFiltered]=1  
		WHERE (Resolution like '%'+ lower(cast(@ResolutionOtherId as varchar(36))) +'%' or isnull(Resolution,'')='')  
  
	DELETE  from #UserSegment Where [IsResolutionFiltered]=0  

	UPDATE #UserSegment SET [IsSearchKeywordFiltered]=0  

	select  + lower(b.value) as Searchvalue ,  a.SegmentId , 0 as filterValue  into #TmpSearch 
		from #UserSegment a cross apply dbo.SplitComma(replace(a.searchkeyword,char(13)+char(10),'|'),'|') b
		where b.value is not null and len(ltrim(rtrim(value)) )>0

		update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,''))=''
		update #UserSegment set [IsSearchKeywordFiltered]=1 WHERE lower(isnull(SearchKeyword,''))=''

	IF(@SearchKeyword <>'')
		BEgin
		update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,'')) like '%'+ lower(isnull(@SearchKeyword,'')) +'%' and  filterValue=0
		END

	--IF(@SearchKeyword <>'')
	--	update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,'')) like '%'+ lower(isnull(@SearchKeyword,'')) +'%'
	--ELSE 
	--begin
	--	update t set filterValue=1 FROM #TmpSearch t where lower(isnull(t.Searchvalue,''))=''
	--	update #UserSegment set [IsSearchKeywordFiltered]=1 WHERE lower(isnull(SearchKeyword,''))=''
	--END 

	update p set [IsSearchKeywordFiltered]=1 
		FROM #TmpSearch t JOIN #UserSegment p on t.segmentid=p.segmentId and filterValue=1
   
	DELETE  from #UserSegment Where [IsSearchKeywordFiltered]=0  
--select * from #TmpSearch
 -- ;	select * from #UserSegment
	set @Result = 
	(SELECT UserSegment.SegmentId , UserSegment.SegmentName,UserSegment.PagesVisitedPageGrpCondition,UserSegment.TotalNoOfPagesVisited,
			cast((SELECT UserSegment.SegmentId , Indexterm.ObjectId as TermId from GetObjectByRelation('O-T',UserSegment.SegmentId)  Indexterm  for xml auto) as xml) ITData,  
			cast((SELECT UserSegment.SegmentId , Watch.Id as WatchId, Watch.Name as WatchName,  Isnull(SegmentWatch.SegmentWatchType,0) as SegmentWatchType, isnull(AssetWatch.ObjectId,'00000000-0000-0000-0000-000000000000') as ObjectId, 
			isnull(AssetWatch.ObjectURL,'') as  ObjectURL
		FROM dbo.ALAudienceSegmentWatch SegmentWatch, dbo.ALWatch Watch   LEFT OUTER JOIN dbo.ALAssetWatch AssetWatch ON AssetWatch.WatchId =Watch .Id    
		where Watch .Id = SegmentWatch.WatchId and UserSegment.SegmentId = SegmentWatch.SegmentId for xml auto) as xml)WData  
		FROM #UserSegment UserSegment  for xml auto)  
  
  select  @Result  
  
  select US.SegmentId, ALPV.PageDefinitionId
		from ALAudienceSegmentPagesVisited ALPV 
			inner join #UserSegment US on ALPV.SegmentId = US.SegmentId
		where ALPV.IsEntry=0
 END   


GO
PRINT 'Altering function GetVariantSitesForUser'
GO
ALTER FUNCTION [dbo].[GetVariantSitesForUser](@UserId uniqueidentifier)
RETURNS 
@SitesTable TABLE 
(
      SiteId uniqueidentifier
)
AS
BEGIN
      --DECLARE @siteTable as SiteTableType 
      
      --INSERT INTO @siteTable
      --SELECT    DISTINCT SiteId AS ObjectId
      --FROM      dbo.USSiteUser US
      --INNER JOIN SISite S ON US.SiteId = S.Id
      --WHERE     UserId = @UserId
      
      --INSERT INTO @SitesTable           
      --SELECT Distinct ObjectId from [dbo].[GetVariantSitesHavingPermission](@siteTable)   
      
      DECLARE @tbSites SiteTableType
      
      -- Master Sites
      DECLARE @tbMasterSites SiteTableType
      INSERT INTO @tbMasterSites
      SELECT SiteId FROM USSiteUser U
            JOIN SISite S ON U.SiteId = S.Id
      WHERE S.Id = S.MasterSiteId AND U.UserId = @UserId
      
      INSERT INTO @tbSites
      SELECT DISTINCT Id FROM SISite S
            JOIN @tbMasterSites T ON S.MasterSiteId = T.ObjectId
      
      -- Variant Sites
      DECLARE @tbVariantSites SiteTableType
      INSERT INTO @tbVariantSites
      SELECT SiteId FROM USSiteUser U
            JOIN SISite S ON U.SiteId = S.Id
      WHERE S.Id != S.MasterSiteId AND U.UserId = @UserId
            AND S.MasterSiteId NOT IN (SELECT ObjectId FROM @tbMasterSites)
      
      --DECLARE @VariantSiteId uniqueidentifier
      --SET @VariantSiteId = (SELECT TOP 1 ObjectId FROM @tbVariantSites)
      --INSERT INTO @tbSites
      --SELECT SiteId FROM dbo.GetAncestorSites(@VariantSiteId) A
      --    EXCEPT (SELECT Id FROM SISite WHERE Id = MasterSiteId)
      INSERT INTO @tbSites
      SELECT A.SiteId FROM @tbVariantSites T
            CROSS APPLY dbo.GetAncestorSites(T.ObjectId) A
      EXCEPT (SELECT Id FROM SISite WHERE Id = MasterSiteId)
      
      
       -- To remove the parent site to be shown in the drop down
        DECLARE @IncludeParentSite BIT  
        SET @IncludeParentSite = 0
        
        SELECT @IncludeParentSite = [VALUE]   
		FROM STSiteSetting SS JOIN STSettingType ST   
		ON ST.Id = SS.SettingTypeId   
		 AND ST.Name='IncludeParentSite'  
     
     
     IF(@IncludeParentSite = 0)
      DELETE from @tbSites where ObjectId IN (
      SELECT ParentSiteId from @tbSites TS
      INNER JOIN SISite  S ON S.Id = TS.ObjectId)
      
      
      -- Direct Sites
      INSERT INTO @tbSites
      SELECT SiteId FROM USSiteUser U
            JOIN SISite S ON U.SiteId = S.Id
      WHERE UserId = @UserId
      
      INSERT INTO @SitesTable
      SELECT DISTINCT ObjectId FROM @tbSites
      
      RETURN 
END
GO
PRINT 'Altering function GetChildSites'
GO
IF(OBJECT_ID('GetChildSites') IS NULL)
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[GetChildSites](@SiteId uniqueidentifier)  
  
RETURNS   
@SITES TABLE   
(  
 [Id]   uniqueidentifier         
 ,[Title]  nvarchar(600)      
 ,[Description] nvarchar(600)       
 ,[URL]  xml      
 ,[VirtualPath]  nvarchar(4000)      
 ,[ParentSiteId] uniqueidentifier       
 ,[Type]      int  
 ,[Keywords]  nvarchar(4000)     
 ,[CreatedBy] uniqueidentifier       
 ,[CreatedDate] datetime       
 ,[ModifiedBy]  uniqueidentifier      
 ,[ModifiedDate] datetime       
 ,[Status]  int      
 ,[LftValue]   int     
 ,[RgtValue]  int      
 ,MasterSiteId uniqueidentifier  
 ,CreatedByFullName nvarchar(4000)  
 ,ModifiedByFullName nvarchar(4000)  
)  
AS  
BEGIN  
  
DECLARE @DeleteStatus int        
SET @DeleteStatus = dbo.GetDeleteStatus()   
  
DECLARE @DraftStatus int  
SET @DraftStatus = dbo.GetSiteDraftStatus()   
    
  
 INSERT INTO @sites   
 SELECT  [Id]        
 ,[Title]        
 ,[Description]        
 ,[URL]        
 ,[VirtualPath]        
 ,[ParentSiteId]        
 ,[Type]        
 ,[Keywords]        
 ,[CreatedBy]        
 ,[CreatedDate]        
 ,[ModifiedBy]        
 ,[ModifiedDate]        
 ,[Status]        
 ,[LftValue]        
 ,[RgtValue]        
 ,MasterSiteId  
 ,CU.UserFullName CreatedByFullName        
 ,MU.UserFullName ModifiedByFullName             
 FROM SISITE S    
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy          
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy  
 WHERE ParentSiteId = @SiteId  
 AND STATUS NOT IN(@DeleteStatus,@DraftStatus)   
  
  
RETURN  
END '

GO
PRINT 'Altering procedure Site_GetAncestorSites'
GO
ALTER PROCEDURE [dbo].[Site_GetAncestorSites]            
(            
@SiteId uniqueIdentifier          
)            
AS            
BEGIN            
    
    
 DECLARE @DeleteStatus int          
 SET @DeleteStatus = dbo.GetDeleteStatus()        
    
 DECLARE @AllowSiblingAccess BIT      
    
 SELECT @AllowSiblingAccess = [VALUE] FROM STSiteSetting SS JOIN STSettingType ST ON ST.Id = SS.SettingTypeId AND ST.Name='AllowSiblingSiteAccess'     
     
    
IF (@AllowSiblingAccess = 1)    
    
BEGIN    
    
 ;WITH siteAncestorHierarchy AS (          
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]          
 ,MasterSiteId          
 FROM SISite S          
 where Id = @SiteId          
     
 UNION ALL           
     
 SELECT S1.[Id]          
 ,S1.[Title]          
 ,S1.[Description]          
 ,S1.[URL]          
 ,S1.[VirtualPath]          
 ,S1.[ParentSiteId]          
 ,S1.[Type]          
 ,S1.[Keywords]          
 ,S1.[CreatedBy]          
 ,S1.[CreatedDate]          
 ,S1.[ModifiedBy]          
 ,S1.[ModifiedDate]          
 ,S1.[Status]          
 ,S1.[LftValue]          
 ,S1.[RgtValue]          
 ,S1.MasterSiteId          
 FROM SISite S1           
 INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId           
 WHERE S1.Status != @DeleteStatus          
 )          
    
    
    
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]          
 ,MasterSiteId      
 ,CU.UserFullName CreatedByFullName          
 ,MU.UserFullName ModifiedByFullName          
          
 FROM siteAncestorHierarchy S1          
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S1.CreatedBy            
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S1.ModifiedBy            
 WHERE Id != @SiteId       
     
 UNION ALL    
     
 SELECT  [Id]          
 ,[TITLE]          
 ,[DESCRIPTION]          
 ,[URL]          
 ,[VIRTUALPATH]          
 ,[PARENTSITEID]          
 ,[TYPE]          
 ,[KEYWORDS]          
 ,[CREATEDBY]          
 ,[CREATEDDATE]          
 ,[MODIFIEDBY]          
 ,[MODIFIEDDATE]          
 ,[STATUS]          
 ,[LFTVALUE]          
 ,[RGTVALUE]    
 ,MASTERSITEID        
 ,CreatedByFullName          
 ,ModifiedByFullName        
 FROM [GetSiblingSites](@SITEID)    
  
 UNION ALL  
   
 SELECT  [Id]          
 ,[TITLE]          
 ,[DESCRIPTION]          
 ,[URL]          
 ,[VIRTUALPATH]          
 ,[PARENTSITEID]          
 ,[TYPE]          
 ,[KEYWORDS]          
 ,[CREATEDBY]          
 ,[CREATEDDATE]          
 ,[MODIFIEDBY]          
 ,[MODIFIEDDATE]          
 ,[STATUS]          
 ,[LFTVALUE]          
 ,[RGTVALUE]    
 ,MASTERSITEID        
 ,CreatedByFullName          
 ,ModifiedByFullName        
 FROM [GetChildSites](@SITEID)              
     
END    
     
ELSE    
     
BEGIN    
  
      
 ;WITH siteAncestorHierarchy AS (          
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]          
 ,MasterSiteId          
 FROM SISite S          
 where Id = @SiteId          
     
 UNION ALL           
     
 SELECT S1.[Id]          
 ,S1.[Title]    
 ,S1.[Description]          
 ,S1.[URL]          
 ,S1.[VirtualPath]          
 ,S1.[ParentSiteId]          
 ,S1.[Type]          
 ,S1.[Keywords]          
 ,S1.[CreatedBy]          
 ,S1.[CreatedDate]          
 ,S1.[ModifiedBy]          
 ,S1.[ModifiedDate]          
 ,S1.[Status]          
 ,S1.[LftValue]          
 ,S1.[RgtValue]          
 ,S1.MasterSiteId          
 FROM SISite S1           
 INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId           
 WHERE S1.Status != @DeleteStatus          
 )          
    
    
    
 SELECT [Id]          
 ,[Title]          
 ,[Description]          
 ,[URL]          
 ,[VirtualPath]          
 ,[ParentSiteId]          
 ,[Type]          
 ,[Keywords]          
 ,[CreatedBy]          
 ,[CreatedDate]          
 ,[ModifiedBy]          
 ,[ModifiedDate]          
 ,[Status]          
 ,[LftValue]          
 ,[RgtValue]      
 ,MasterSiteId      
 ,CU.UserFullName CreatedByFullName          
 ,MU.UserFullName ModifiedByFullName          
 FROM siteAncestorHierarchy S1          
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S1.CreatedBy            
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S1.ModifiedBy            
 where Id != @SiteId     
   
 UNION ALL  
   
 SELECT  [Id]          
 ,[TITLE]          
 ,[DESCRIPTION]          
 ,[URL]          
 ,[VIRTUALPATH]          
 ,[PARENTSITEID]          
 ,[TYPE]          
 ,[KEYWORDS]          
 ,[CREATEDBY]          
 ,[CREATEDDATE]          
 ,[MODIFIEDBY]          
 ,[MODIFIEDDATE]          
 ,[STATUS]          
 ,[LFTVALUE]          Site_CreateMicroSiteData
 ,[RGTVALUE]    
 ,MASTERSITEID
 ,CreatedByFullName          
 ,ModifiedByFullName        
 FROM [GetChildSites](@SITEID)     
     
END    
    
END      
GO
PRINT 'Altering procedure SqlDirectoryProvider_Save'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_Save]
(
	@XmlString		xml,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@VirtualPath	nvarchar(max)  OUT,
	@SiteName		nvarchar(256) OUT
)
AS
BEGIN
	DECLARE @tbDirectory TYDirectory	
	INSERT INTO @tbDirectory(Id,
		Title,
		Description,
		Attributes,
		FolderIconPath,
		PhysicalPath,
		[Size],
		Status,
		ThumbnailImagePath,
		ObjectTypeId,
		ParentId,
		Lft,
		Rgt,
		IsSystem,
		IsMarketierDir,
		AllowAccessInChildrenSites,
		SiteId,
		MicrositeId)						
	SELECT  tab.col.value('@Id[1]','uniqueidentifier') Id,
		tab.col.value('@Title[1]','NVarchar(256)')Title,
		tab.col.value('@Description[1]','Nvarchar(1024)')Description,
		tab.col.value('@Attributes[1]','int')Attributes,
		tab.col.value('@FolderIconPath[1]','nvarchar(4000)')FolderIconPath,
		tab.col.value('@PhysicalPath[1]','nvarchar(4000)')PhysicalPath,
		tab.col.value('@Size[1]','bigint')[Size],
		tab.col.value('@Status[1]','int')Status,
		tab.col.value('@ThumbnailImagePath[1]','nvarchar(4000)')ThumbnailImagePath,
		tab.col.value('@ObjectTypeId[1]','int')ObjectTypeId,
		tab.col.value('@ParentId[1]','uniqueidentifier')ParentId,
		tab.col.value('@Lft[1]','bigint')Lft,
		tab.col.value('@Rgt[1]','bigint')Rgt,
		tab.col.value('@IsSystem[1]','bit')IsSystem,
		tab.col.value('@IsMarketierDir[1]','bit')IsMarketierDir,
		ISNULL(tab.col.value('@AllowAccessInChildrenSites[1]','bit'),0) AllowAccessInChildrenSites,
		@ApplicationId,
		@MicroSiteId
	FROM @XmlString.nodes('/SiteDirectoryCollection/SiteDirectory') tab(col)
	Order By Lft

	-- if one of the node is content then everything is content, no mix and match
	DECLARE @ObjectTypeId int
	SET @ObjectTypeId = (SELECT TOP 1 ObjectTypeId FROM @tbDirectory)
	
	IF (@ObjectTypeId = 7)
		EXEC [dbo].[SqlDirectoryProvider_SaveContentDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 9)
		EXEC [dbo].[SqlDirectoryProvider_SaveFileDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 33)
		EXEC [dbo].[SqlDirectoryProvider_SaveImageDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 38)
		EXEC [dbo].[SqlDirectoryProvider_SaveFormDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE
	BEGIN
		-- All other libraries are shared
		  DECLARE @MasterSiteId uniqueidentifier  
		  SET @MasterSiteId = (SELECT MasterSiteId FROM SISite WHERE Id = @ApplicationId)  
		  IF (@MasterSiteId IS NOT NULL AND @MasterSiteId != dbo.GetEmptyGUID()  AND @ObjectTypeId <> 39)
		  BEGIN
		   SET @ApplicationId = @MasterSiteId  
		   UPDATE @tbDirectory Set SiteId = @ApplicationId
		  END
		EXEC [dbo].[SqlDirectoryProvider_SaveDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	END
	
	SET @VirtualPath = (SELECT Id, dbo.GetVirtualPathByObjectType(Id, @ObjectTypeId) VirtualPath, Attributes, SiteId 
							FROM @tbDirectory SISiteDirectory FOR XML Auto)
	
	SELECT @SiteName = Title FROM SISite WHERE Id = @ApplicationId
END
GO
PRINT 'Altering procedure Site_CreateMicroSiteData'
GO
ALTER procedure [dbo].[Site_CreateMicroSiteData]
(
	@ProductId uniqueidentifier,
	@MasterSiteId uniqueidentifier,
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser bit,
	@PageImportOptions int =2,
	@ImportContentStructure bit =0,
	@ImportFileStructure bit =0,
	@ImportImageStructure bit =0,
	@ImportFormsStructure bit =0
)
AS
BEGIN
	
	--CMSFrontUSer
	IF(@ImportAllWebSiteUser = 1)
	BEGIN			
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId,
			SU.UserId,
			SU.ISSystemUser,
			SU.ProductId
		FROM USUser U 
			INNER JOIN USSiteUser SU on U.Id = SU.UserId  
			INNER JOIN USMembership M on SU.UserId = M.UserId  
		WHERE SU.SiteId = @MasterSiteId AND SU.ProductId = @ProductId  
			AND SU.IsSystemUser = 0 AND  U.Status = 1 
			AND M.IsApproved = 1 AND M.IsLockedOut= 0 AND u.ExpiryDate >= GETUTCDATE()
	END
	
-- This will not insert any record as we have not inserted any record in USSiteuser table
	INSERT INTO USMemberGroup(MemberId, MemberType, GroupId, ApplicationId) 
	SELECT SU.UserId, 1, MG.GroupId, @MicroSiteId  
		FROM USSiteUser SU 
			INNER JOIN USMemberGroup MG on SU.UserId = MG.MemberId 
	WHERE SU.SiteId = @MicroSiteId 
		AND MG.ApplicationId = @MasterSiteId --and MG.MemberType=1
	
	-- This is not required now, as it will inherit permision from parent site
	--****Admin USer****
	IF (@ImportAllAdminUserAndPermission=0)
	BEGIN
		-- Giving permission to Default users. Allow only CMS , Analyzer and Social product default users
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId] 
		FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MasterSiteId AND ProductId in ('CBB702AC-C8F1-4C35-B2DC-839C26E39848','CAB9FF78-9440-46C3-A960-27F53D743D89','CED1F2B6-A3FF-4B82-A856-1583FA811906')
		Except
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId]  FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MicroSiteId 	
			
		INSERT INTO [dbo].[USMemberGroup]
		   ([ApplicationId]
		   ,[MemberId]
		   ,[MemberType]
		   ,[GroupId])   
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MasterSiteId
				AND GroupId IN (SELECT Id FROM USGroup WHERE ApplicationId = ProductId)
		Except
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MicroSiteId				
	END

	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,@PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,7,@ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,9,@ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,33,@ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,38,@ImportFormsStructure
	

	--HSStructure data for Blog
	Declare @hsParentId uniqueidentifier,@SiteDirectoryObjectTypeId int
	Declare @RgtValue bigint , @SiteTitle nvarchar(1000)

	set @hsParentId  = dbo.GetNetEditorId();
	SELECT @RgtValue = RgtValue FROM HSStructure WHERE Id=@hsParentId
	SET @SiteDirectoryObjectTypeId= dbo.GetSiteDirectoryObjectType()

	IF ((@RgtValue IS NOT NULL)
		AND (NOT EXISTS(SELECT 1 FROM HSStructure WHERE Title='Blog' And SiteId = @MicroSiteId )))
	BEGIN
		Set @RgtValue = 1
		
		Select @SiteTitle=Title from sisite where Id=@MicroSiteId

		if not exists(select * from Hsstructure where id=@MicroSiteId)
		begin
			INSERT INTO HSStructure(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@MicroSiteId,@SiteTitle,dbo.GetSiteObjectType(),null,@hsParentId,@RgtValue,@RgtValue+37,@MicroSiteId)
		end
		Declare @L1Id uniqueidentifier
		SELECT @L1Id =newid()
		INSERT INTO HSStructure
		(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L1Id,'Blog',@SiteDirectoryObjectTypeId,null,@MicroSiteId,@RgtValue+35,@RgtValue+36,@MicroSiteId)

		EXEC	[dbo].[SiteDirectory_Save]
		@Id = @L1Id OUTPUT,
		@ApplicationId = @MicroSiteId,
		@Title = N'Blog',
		@Description = NULL,
		@ModifiedBy = @ModifiedBy,
		@ModifiedDate = NULL,
		@ObjectTypeId = 39,
		@PhysicalPath = NULL,
		@VirtualPath = N'~/Blog'	
		-- Inserting to default Parent table
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@MicroSiteId,39,@L1Id)	
	End

	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		
	
	EXEC [Site_CreateTemplateBuilderData] @MicroSiteId

	-- This might be not required now
	--EXEC [CMSSite_InsertDefaultPermission] @MicroSiteId	
		
END
GO

PRINT 'Altering procedure Product_SimpleSearchSKU'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
 ALTER PROCEDURE [dbo].[Product_SimpleSearchSKU]
    (
	@SearchTerm nvarchar(max),
	@IsBundle tinyint = null,
	@pageNumber		int = null,
	@pageSize		int = null,
	@SortColumn VARCHAR(25)=null,
	@ProductTypeId uniqueidentifier=null,
	@ApplicationId uniqueidentifier=null
 )  
AS  
BEGIN  
	DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint,
			@SkuCount				bigint

	SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
	SET @PageUpperBound = @PageLowerBound - Isnull(@pageSize,0) + 1;

	DECLARE @searchText nvarchar(max)  
	SET @searchText = '%' + @SearchTerm + '%'
	IF @SortColumn IS NULL
		SET @SortColumn = 'productname asc'
	SET @SortColumn = LOWER(@SortColumn)
    Create table #tbSku
            (
              RowNumber INT IDENTITY(1, 1) primary key,
              Id UNIQUEIDENTIFIER ,
              ProductId UNIQUEIDENTIFIER ,
              ProductName NVARCHAR(2000) ,
              Qty DECIMAL(18, 4) ,
              AllocatedQty DECIMAL(18, 4) ,
              ProductTypeName NVARCHAR(1000)
            )
			CREATE NONCLUSTERED INDEX CIX_ProductId ON #tbSku(ProductId)
	
	INSERT INTO #tbSku
	SELECT S.Id, P.Id, P.Title, Isnull(dbo.GetBundleQuantityPerSite(I.SKUId,@ApplicationId) ,I.[Quantity]) , ISNULL(A.Quantity,0), T.Title
	FROM PRProduct P 
			JOIN PRProductSku S ON S.ProductId = P.Id
			JOIN PRProductType T ON T.Id = P.ProductTypeId
			Left Join vwInventoryPerSite I ON I.SkuId = S.Id AND I.SiteId=@ApplicationId
			Left Join (Select ProductSKUId SkuId,sum(Quantity)Quantity from vwAllocatedQuantityperWarehouse AL INNER JOIN INWarehouseSite WS on WS.WarehouseId = AL.WarehouseId AND WS.SiteId =@ApplicationId Group By AL.ProductSKUId) A ON A.SkuId = S.Id 
	WHERE	(@ProductTypeId IS NULL OR T.Id = @ProductTypeId) AND
			(@IsBundle IS NULL OR P.IsBundle = @IsBundle) AND
			(P.Title like @searchText 
					OR P.ShortDescription like @searchText 
					OR P.LongDescription like @searchText   
					OR P.Description like @searchText
					OR S.SKU like @searchText)
	ORDER BY
		CASE WHEN @SortColumn = 'productname desc' Then P.Title END DESC, 	
		CASE WHEN @SortColumn = 'productname asc' Then P.Title END ASC, 	
		CASE WHEN @SortColumn = 'sku desc' Then S.SKU END DESC, 	
		CASE WHEN @SortColumn = 'sku asc' Then S.SKU END ASC, 
		CASE WHEN @SortColumn = 'quantity desc' Then I.Quantity END DESC, 	
		CASE WHEN @SortColumn = 'quantity asc' Then I.Quantity  END ASC,
		CASE WHEN @SortColumn = 'availabletosell desc' Then A.Quantity END DESC, 	
		CASE WHEN @SortColumn = 'availabletosell asc' Then A.Quantity END ASC

		Select @SkuCount= Count(*) FROM #tbSku

	delete 
	from	#tbSku
	where	not (@pageSize is null OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))

	SELECT 
			T.Id,
			T.ProductId,
			T.ProductName,
			T.Qty AS Quantity,
			T.Qty - T.AllocatedQty AS AvailableToSell,
			S.Title,
			S.SKU,
			case when T.Qty >=999999 then 1 else S.IsUnlimitedQuantity end IsUnlimitedQuantity,
			S.AvailableForBackOrder,
			S.MaximumDiscountPercentage,			
			ISNULL(S.BackOrderLimit, 0) AS BackOrderLimit,
			S.AvailableForBackOrder,
			CAST(
				CASE 
					WHEN S.IsActive = 1 AND PR.IsActive = 1
						THEN 1
					ELSE 0
				END AS Bit) as IsActive,		
			T.ProductTypeName
		FROM #tbSku T JOIN PrProductSku S ON S.Id = T.Id
		INNER JOIN PRProduct PR ON PR.Id = S.ProductID
		
		declare @activestatus int = dbo.GetActiveStatus()

		SELECT	C.[Id],	C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
		FROM	#tbSku T
				INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
				INNER JOIN CLImage I on I.Id = OI.ImageId 
				INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.ParentImage
				INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType

		UNION ALL

		SELECT	C.[Id],	C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
		FROM	#tbSku T
				INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
				INNER JOIN CLImage I on I.Id = OI.ImageId 
				INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.Id
				INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType

		Order by Sequence--, Title
 
	

	select @SkuCount
END
GO
PRINT 'Altering procedure NavNode_GetNavNodesForFilter'
GO
ALTER PROCEDURE [dbo].[NavNode_GetNavNodesForFilter]
(
	@siteId uniqueidentifier,
	@filterId uniqueidentifier
)
as
begin
	SELECT NavNodeId
	From NVNavNodeNavFilterMap 
	Where QueryId = @filterId
end
GO
PRINT 'Altering procedure Membership_CopyPermission'
GO
ALTER PROCEDURE [dbo].[Membership_CopyPermission]
(
	@ApplicationId		uniqueidentifier,
	@ObjectTypeId		int,
	@SourceMemberId		uniqueidentifier,
	@TargetMemberId		uniqueidentifier,
	@SourceMemberType	smallint,
	@TargetMemberType	smallint,
	@ObjectId			uniqueidentifier = NULL,
	@SourceMemberRoles	xml	= NULL,
	@ProductId 		uniqueidentifier = NULL
)
AS
BEGIN
	DECLARE @tbPermission TABLE(
			ApplicationId	uniqueidentifier, 
			MemberId		uniqueidentifier, 
			MemberType		smallint, 
			RoleId			int, 
			ObjectId		uniqueidentifier, 
			ObjectTypeId	int, 
			Propagate		bit)

	IF (@SourceMemberRoles IS NOT NULL)
	BEGIN
		INSERT INTO @tbPermission
		SELECT	tab.col.value('(ApplicationId/text())[1]','uniqueidentifier') ApplicationId,
		   		tab.col.value('(MemberId/text())[1]','uniqueidentifier') MemberId,
		   		tab.col.value('(MemberType/text())[1]','smallint') MemberType,
		   		tab.col.value('(RoleId/text())[1]','int') RoleId,
				tab.col.value('(ObjectId/text())[1]','uniqueidentifier') ObjectId,	
				tab.col.value('(ObjectTypeId/text())[1]','int') ObjectTypeId,	
				tab.col.value('(Propagate/text())[1]','bit') Propagate
		FROM @SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
	END
		
	IF(@SourceMemberRoles IS NOT NULL AND @ObjectId IS NOT NULL AND @SourceMemberId IS NULL)
	BEGIN
		/*This will copy the entire role information of the given object id to the target memberRole collection.*/

--		--Checks whether the given memberRole collection is having any member role to assign
--		DECLARE @Cnt int
--		SET @Cnt = 0
--		SELECT @Cnt = COUNT(*)
--		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)

		
		DELETE FROM [dbo].[USMemberRoles] 
		--FROM @SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
		WHERE	ApplicationId = @ApplicationId	AND
				ObjectId = @ObjectId	AND
				ObjectTypeId = @ObjectTypeId AND
				MemberType = ISNULL(@SourceMemberType,MemberType) AND
				ProductId = ISNULL(@ProductId, ProductId)	

		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			@ProductId
		FROM @tbPermission
		
	END
	ELSE IF(@SourceMemberRoles IS NOT NULL AND @ObjectId IS NULL AND @SourceMemberId IS NULL)
	BEGIN
		DELETE R FROM @tbPermission T
			LEFT JOIN USMemberRoles R ON T.ApplicationId = R.ApplicationId
				AND R.MemberId = T.MemberId
				AND R.MemberType = T.MemberType
				AND R.ObjectTypeId = T.ObjectTypeId
				AND R.ProductId = @ProductId
		
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			@ProductId
		FROM @tbPermission
				
	END
	ELSE IF (@SourceMemberId IS NULL AND @TargetMemberId IS NULL AND @ObjectId IS NOT NULL AND @SourceMemberRoles IS NULL) --CopyObjectPermissionsToAllUsers
	BEGIN		
		
		/*This will copy all the users and roles to the given object id */
		
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	ApplicationId,
		   		MemberId,
		   		MemberType,
		   		RoleId,
				ObjectId,	
				ObjectTypeId,	
				Propagate,
				ProductId
		FROM	[dbo].[User_GetMembersPermissionByObject(@ApplicationId,@MemberType,@ObjectId,@ObjectTypeId,@Propagate,@ProductId)]
	END			 
	ELSE IF (@SourceMemberId IS NOT NULL AND @TargetMemberId IS NOT NULL AND @SourceMemberRoles IS NULL AND @ObjectId IS NULL) --
	BEGIN

		/* This will trasfer all the roles belonging to one member to another member within a given objectType and application scope*/
		--Deletes all the role information for that user on the given object type.
		DELETE FROM dbo.USMemberRoles 
		WHERE	ApplicationId = @ApplicationId AND
				MemberId = @TargetMemberId AND
				MemberType = @TargetMemberType	AND
				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId) AND
				ProductId = ISNULL(@ProductId, ProductId)
		
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		
		)
		SELECT 	ApplicationId,
		   		MemberId,
		   		MemberType,
		   		RoleId,
				ObjectId,	
				ObjectTypeId,	
				Propagate,
				@ProductId ProductId
		FROM	dbo.USMemberRoles
		WHERE	ApplicationId = @ApplicationId	AND
				MemberId = @SourceMemberId	AND
				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId)	AND
				MemberType = @SourceMemberType AND
				ProductId = ISNULL(@ProductId, ProductId)	
	END
	ELSE IF (@SourceMemberRoles IS NOT NULL AND @SourceMemberId IS NOT NULL AND @ObjectTypeId IS NOT NULL AND @ObjectId IS NULL)
	BEGIN
		--Checks whether the given memberRole collection is having any member role to assign
--		DECLARE @Cnt int
--		SET @Cnt = 0
--		SELECT @Cnt = COUNT(*)
--		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
--		
		
		--Deletes all the roles for the given member
		DELETE FROM dbo.USMemberRoles 
		WHERE	ApplicationId = ISNULL(@ApplicationId,ApplicationId) AND
				MemberId = @SourceMemberId AND
				MemberType = ISNULL(@SourceMemberType,MemberType)	AND
				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId) AND
				ProductId = ISNULL(@ProductId, ProductId)
	
		--Inserts the given memberrole information into the memberrole table.
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			@ProductId
		FROM @tbPermission
		
	END
--	ELSE IF(@SourceMemberRoles IS NOT NULL AND @ObjectId IS NOT NULL AND @ObjectTypeId IS NOT NULL)
--	BEGIN
--		DELETE FROM dbo.USMemberRoles 
--		WHERE	ApplicationId = ISNULL(@ApplicationId,ApplicationId) AND
--				ObjectId = @ObjectId AND
--				MemberType = ISNULL(@SourceMemberType,MemberType)	AND
--				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId)
--
--		--Inserts all the given memberroles 
--		INSERT INTO dbo.USMemberRoles
--		(
--			ApplicationId,
--			MemberId,
--			MemberType,
--			RoleId,
--			ObjectId,
--			ObjectTypeId,
--			Propagate
--		)
--		SELECT	tab.col.value('(ApplicationId/text())[1]','uniqueidentifier') ApplicationId,
--	   			tab.col.value('(MemberId/text())[1]','uniqueidentifier') MemberId,
--	   			tab.col.value('(MemberType/text())[1]','smallint') MemberType,
--	   			tab.col.value('(RoleId/text())[1]','int') RoleId,
--				tab.col.value('(ObjectId/text())[1]','uniqueidentifier') ObjectId,	
--				tab.col.value('(ObjectTypeId/text())[1]','int') ObjectTypeId,	
--				tab.col.value('(Propagate/text())[1]','bit') Propagate
--		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
--	END

	IF (@SourceMemberRoles IS NOT NULL AND @ObjectTypeId = 2)
	BEGIN
		UPDATE P SET P.PropogateRoles = tab.col.value('(Propagate/text())[1]','bit')
			FROM PageMapNode P JOIN @SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
				ON P.PageMapNodeId = tab.col.value('(ObjectId/text())[1]','uniqueidentifier')
	END
END

/*
1. Source Member to Target Member -> All the Role Permissions related to the given ObjectType
2. Source ObjectId to Target ObjectId -> All the Members and roles to the given ObjectType.
*/


GO
PRINT 'Altering procedure CommerceReport_SalesByNavCategory'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesByNavCategory]
    (
      @startDate DATETIME ,
      @endDate DATETIME ,
      @maxReturnCount INT ,
      @pageSize INT = 10 ,
      @pageNumber INT ,
      @sortBy NVARCHAR(50) = NULL ,
      @virtualCount INT OUTPUT ,
      @ApplicationId UNIQUEIDENTIFIER = NULL  
    )
  AS 
    BEGIN  

        IF @maxReturnCount = 0 
            SET @maxReturnCount = NULL  
 
  
  
        IF @pageSize IS NULL 
            SET @PageSize = 2147483647  
        IF @endDate IS NULL 
            SET @endDate = GETDATE()  
        DECLARE @TotalSales DECIMAL(18, 2)
        DECLARE @TotalQuantity DECIMAL(18, 2)
        DECLARE @LoadedOrder DATETIME
           
        SELECT  @LoadedOrder = MAX(OrderDate)
        FROM    RPTCommerceReport_Category

        INSERT  INTO dbo.RPTCommerceReport_Category
                ( Id ,
                  OrderTotal ,
                  Quantity ,
                  NavNodeId ,
                  OrderDate
                                
                )
                SELECT  TV.*
                FROM    RPTCommerceReport_Category LR
                        RIGHT JOIN ( SELECT O.Id ,
                                            SUM(Price * Quantity) AS OrderTotal ,
                                            SUM(Quantity) AS Quantity ,
                                            NV.NavNodeId ,
                                            O.OrderDate
                                     FROM   OROrder O
                                            INNER JOIN dbo.OROrderItem OI ON OI.OrderId = O.Id
                                            INNER JOIN dbo.PRProductSKU PS ON PS.Id = OI.ProductSKUId
                                            INNER JOIN NVFilterOutput NVO ON NVO.ObjectId = PS.ProductId
                                            INNER JOIN NVFilterQuery FQ ON NVO.QueryId = FQ.Id
                                                              AND FQ.SiteId = @ApplicationId
                                            INNER JOIN NVNavNodeNavFilterMap NV ON NVO.QueryId = NV.QueryId
                                            LEFT JOIN NVFilterExclude NVEx ON NVEx.QueryId = FQ.Id
                                                              AND NVO.ObjectId = NVEx.ObjectId
                                     WHERE  NVEx.ObjectId IS NULL
                                            AND OI.OrderItemStatusId <> 3
                                            AND O.OrderStatusId IN ( 1, 2, 11,
                                                              4 )
                                            AND ( @LoadedOrder IS NULL
                                                  OR O.OrderDate > @LoadedOrder
                                                )
                                     GROUP BY O.OrderDate ,
                                            NV.NavNodeId ,
                                            O.Id
                                   ) TV ON TV.Id = LR.Id
                                           AND TV.NavNodeId = LR.NavNodeId
                WHERE   LR.Id IS NULL
                        
                       
                        
                     
                        
                       
                                      
                        
                        
                        
        SELECT  @TotalSales = SUM(OrderTotal) ,
                @TotalQuantity = SUM(Quantity)
        FROM    RPTCommerceReport_Category
        WHERE   OrderDate >= dbo.ConvertTimeToUtc(@startDate, @ApplicationId)
                AND OrderDate < dbo.ConvertTimeToUtc(@endDate, @ApplicationId)
    
     
        SELECT  @virtualCount = COUNT(DISTINCT NavNodeId)
        FROM    RPTCommerceReport_Category
        WHERE   OrderDate >= dbo.ConvertTimeToUtc(@startDate, @ApplicationId)
                AND OrderDate < dbo.ConvertTimeToUtc(@endDate, @ApplicationId)
     
                                                                                            
      
    
           -- INSERT  INTO #tmpCategorySale
        SELECT  *
        FROM    ( SELECT    C.NavNodeId AS NavId ,
                            NavNodeUrl as NavigationCategory ,
                            SUM(Quantity) AS Quantity ,
                            SUM(OrderTotal) AS SaleTotal ,
                            COUNT(Id) AS NumberOfOrders ,
                            ROW_NUMBER() OVER ( ORDER BY CASE WHEN @sortBy = 'navigationcategory asc'
                                                              THEN NavNodeUrl
                                                         END ASC, CASE
                                                              WHEN @sortBy = 'navigationcategory desc'
                                                              THEN NavNodeUrl
                                                              END DESC, CASE
                                                              WHEN @sortBy = 'quantity asc'
                                                              THEN SUM(Quantity)
                                                              END ASC, CASE
                                                              WHEN @sortBy = 'quantity desc'
                                                              THEN SUM(Quantity)
                                                              END DESC, CASE
                                                              WHEN @sortBy = 'saletotal asc'
                                                              THEN SUM(OrderTotal)
                                                              END ASC, CASE
                                                              WHEN @sortBy = 'saletotal desc'
                                                              THEN SUM(OrderTotal)
                                                              END DESC ) AS RowNumber ,
                            ( SUM(Quantity) / @TotalQuantity ) * 100 AS PercentOfQuantity ,
                            ( SUM(OrderTotal) / @TotalSales ) * 100 AS PercentOfSales
                  FROM      RPTCommerceReport_Category C
                            INNER JOIN NVNavNodeNavFilterMap M ON M.NavNodeId = C.NavNodeId
                  WHERE     OrderDate >= dbo.ConvertTimeToUtc(@startDate,
                                                              @ApplicationId)
                            AND OrderDate < dbo.ConvertTimeToUtc(@endDate,
                                                              @ApplicationId)
                  GROUP BY  C.NavNodeId ,
                            NavNodeUrl
                ) TV
        WHERE   RowNumber <= ISNULL(@maxReturnCount, @virtualCount)
                AND RowNumber >= ( @pageSize * @pageNumber ) - ( @pageSize - 1 )
                AND RowNumber <= @pageSize * @pageNumber
        ORDER BY RowNumber
        
     
         
       
     

 
    END
GO

PRINT 'Updating assembly Bridgeline.TimeZone'
GO

PRINT N'Dropping  [ClearCachedTimeZone]...';
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClearCachedTimeZone]') AND (type = 'FS' OR type = 'FT'))
	DROP FUNCTION [dbo].[ClearCachedTimeZone]
GO
PRINT N'Dropping  [ConvertTimeFromUtc]...';
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConvertTimeFromUtc]') AND (type = 'FS' OR type = 'FT'))
	DROP FUNCTION [dbo].[ConvertTimeFromUtc]
GO
PRINT N'Dropping  [ConvertTimeToUtc]...';
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ConvertTimeToUtc]') AND (type = 'FS' OR type = 'FT'))
	DROP FUNCTION [dbo].[ConvertTimeToUtc]
GO
PRINT N'Dropping  [GetLocalDate]...';
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLocalDate]') AND (type = 'FS' OR type = 'FT'))
	DROP FUNCTION [dbo].[GetLocalDate]

GO
PRINT N'Dropping Bridgeline.TimeZone'
GO
IF EXISTS (SELECT 1 FROM sys.assemblies WHERE name = N'Bridgeline.TimeZone')
DROP ASSEMBLY [Bridgeline.TimeZone]
GO
PRINT N'Altering [Bridgeline.TimeZone]...';
GO
CREATE ASSEMBLY [Bridgeline.TimeZone]
    AUTHORIZATION [dbo]
    FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C010300D44587530000000000000000E00002210B010B00001600000006000000000000CE3500000020000000400000000000100020000000020000040000000000000004000000000000000080000000020000000000000300408500001000001000000000100000100000000000001000000000000000000000007C3500004F00000000400000A003000000000000000000000000000000000000006000000C000000443400001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000D4150000002000000016000000020000000000000000000000000000200000602E72737263000000A0030000004000000004000000180000000000000000000000000000400000402E72656C6F6300000C0000000060000000020000001C00000000000000000000000000004000004200000000000000000000000000000000B035000000000000480000000200050080230000C41000000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000133002004700000001000011000F00281100000A0B072D310F00281200000A0F01281300000A2D090F01281400000A2B057E1500000A002806000006281600000A281700000A0A2B087E1800000A0A2B00062A00133002004700000001000011000F00281100000A0B072D310F00281200000A0F01281300000A2D090F01281400000A2B057E1500000A002806000006281900000A281700000A0A2B087E1800000A0A2B00062A00133002003A0000000200001100281A00000A0B1201281B00000A0F00281300000A2D090F00281400000A2B057E1500000A002806000006281600000A281700000A0A2B00062A0000133001002200000003000011000F00281400000A2807000006007201000070281C00000A281D00000A0A2B00062A1E02281E00000A2A00001B3003009400000004000011007E02000004250D281F00000A0000140A7E01000004026F2000000A16FE01130411042D0C7E01000004026F2100000A0A062C27066F0B00000613051205282200000A281A00000A13051205282200000A282300000A16FE012B011600130411042D1D000228080000060B07730A0000060A7E0100000402066F2400000A0000066F0D0000060CDE0809282500000A00DC00082A0110000002000E007B89000800000000133002002000000005000011007E01000004026F2000000A16FE010A062D0C7E01000004026F2600000A262A1B3003008B00000006000011007E2700000A0A140C720B0000700B07732800000A0D723B0000700F00FE16060000016F2900000A723A010070282A00000A130400110409732B00000A1305096F2C00000A0011056F2D00000A6F2900000A0A096F2E00000A0006282F00000A0C00DE05260000DE0000DE16000814FE0116FE01130711072D06283000000A0C00DC000813062B0011062A00011C000000003400306400050100000102003400386C00160000000056733100000A8001000004731E00000A80020000042A7A02281E00000A00000203280E0000060002281A00000A280C00000600002A000000133001000B00000007000011027B030000040A2B00062A2202037D030000042A133001000B00000008000011027B040000040A2B00062A2202037D040000042A42534A4201000100000000000C00000076322E302E35303732370000000005006C000000D0040000237E00003C0500000006000023537472696E6773000000003C0B000040010000235553007C0C00001000000023475549440000008C0C00003804000023426C6F6200000000000000020000015715A2090902000000FA253300160000010000002100000004000000040000000E0000000C0000003200000017000000080000000100000002000000040000000100000001000000030000000200000000000A0001000000000006005E0057000A00860071000A00920071000A00CB00710006000B01F0000600180157000E003E01570006007601570006004302310206005A0231020600770231020600960231020600CE02AF020600E202310206000803310206002103310206003E03310206005703310206007203310206008B0331020600B903A6035700CD0300000600FC03DC0306001C04DC030A0069044E040600C20457000600E504D4040600340557000A0051053B050A006F053B050A008D057A050A009F057A050600E505DC030000000001000000000001000100010010002200000005000100010083011000370000000500010006000300100044000000050003000A0031001D01250031002C012F000100B70155000100D301590050200000000096009A000A000100A420000000009600AD000A000300F820000000009600BE00130005004021000000009600D6001A0006006E21000000008618EA002100070078210000000096004B01320007002822000000009600D6003900080054220000000091005B01320009000823000000009118DE057E030A001E23000000008618EA003F000A0040230000000086087F0145000B0057230000000086088E014A000B0060230000000086089D0150000C007723000000008608AA013F000C00000001000102000002000D02000001001402000002000D02000001000D02000001000D02000001000D02000001000D02000001000D02000001002202000001002B02000001002B024900EA0067005100EA0067005900EA0067006100EA0067006900EA006C007100EA0067007900EA0067008100EA0067008900EA0067009100EA0067009900EA006700A100EA006700A900EA007100B900EA007700C100EA002100C900EA00210011007E04A40111008904450019007E04A40119008904A80131009304AD0139009A00B10111009904BA011100A504C1013900AD00B1014100AA04CB014100B2044500D100CA04FF022100990404030900EA002100D900ED040F030C00F3041D030C00FF042303410008054500410011052A030C001F053203D90028050F030C002D051D03E10093044B03E900EA00670009005F054E03E10068055203F100EA005903F9009A0521000101A9056003F900B70521003900BD0564033900D4056A030C00EA0021000901EA002100200083007C002E001300AA032E001B00AA032E007B0018042E000B0091032E004B00BD032E002B00AA032E003300B0032E004300B0032E005B00FB032E005300D9032E006300AA032E006B0006042E0073000F04400083007C00600083007C0061009301820380008300D701810093018203600193018203800193018203A00193018203C00193018203C501D0010A033A0347036F0387038C03040001000000ED015D000000F801620002000B00030001000C00030002000D00050001000E00050014030480000005000100000000000000000000003A04000002000000000000000000000001004E00000000000200000000000000000000000100650000000000030005000000000000000000010032010000000003000200040003000000003C4D6F64756C653E004272696467656C696E652E54696D655A6F6E652E646C6C0055736572446566696E656446756E6374696F6E73005369746554696D655A6F6E65004C6F63616C44617461006D73636F726C69620053797374656D004F626A6563740053797374656D2E446174610053797374656D2E446174612E53716C54797065730053716C4461746554696D650053716C4775696400436F6E7665727454696D6546726F6D55746300436F6E7665727454696D65546F557463004765744C6F63616C446174650053716C426F6F6C65616E00436C65617243616368656454696D655A6F6E65002E63746F720053797374656D2E436F6C6C656374696F6E732E47656E657269630044696374696F6E617279603200477569640063616368656453657474696E6773005F6C6F636B0053797374656D2E436F72650054696D655A6F6E65496E666F004765745369746554696D655A6F6E650047657454696D655A6F6E6546726F6D5369746553657474696E67004461746554696D65006765745F4C6F6164656454696D65007365745F4C6F6164656454696D65006765745F54696D655A6F6E65007365745F54696D655A6F6E65003C4C6F6164656454696D653E6B5F5F4261636B696E674669656C64003C54696D655A6F6E653E6B5F5F4261636B696E674669656C64004C6F6164656454696D650054696D655A6F6E65007574634461746554696D6500736974654964006C6F63616C4461746554696D650074696D655A6F6E650076616C75650053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C7943756C747572654174747269627574650053797374656D2E52756E74696D652E496E7465726F70536572766963657300436F6D56697369626C6541747472696275746500417373656D626C79496E666F726D6174696F6E616C56657273696F6E41747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C7954726164656D61726B4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C697479417474726962757465004272696467656C696E652E54696D655A6F6E65004D6963726F736F66742E53716C5365727665722E5365727665720053716C46756E6374696F6E417474726962757465006765745F49734E756C6C006765745F56616C756500456D707479006F705F496D706C69636974004E756C6C006765745F4E6F7700546F556E6976657273616C54696D6500436F6E7665727400546F426F6F6C65616E0053797374656D2E546872656164696E67004D6F6E69746F7200456E74657200436F6E7461696E734B6579006765745F4974656D006765745F44617465006F705F496E657175616C697479007365745F4974656D00457869740052656D6F766500537472696E670053797374656D2E446174612E53716C436C69656E740053716C436F6E6E656374696F6E00546F537472696E6700436F6E6361740053716C436F6D6D616E640053797374656D2E446174612E436F6D6D6F6E004462436F6E6E656374696F6E004F70656E004462436F6D6D616E6400457865637574655363616C617200436C6F73650046696E6453797374656D54696D655A6F6E6542794964006765745F4C6F63616C002E6363746F7200436F6D70696C657247656E6572617465644174747269627574650000097400720075006500002F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D0074007200750065000080FD530065006C0065006300740020005B00560061006C00750065005D002000660072006F006D002000530054005300690074006500530065007400740069006E00670020005300200049006E006E006500720020004A006F0069006E00200053005400530065007400740069006E00670054007900700065002000540020004F006E00200053002E00530065007400740069006E0067005400790070006500490064003D0054002E004900640020005700680065007200650020004E0061006D0065003D0027004C006F00630061006C00540069006D0065005A006F006E0065002700200041006E00640020005300690074006500490064003D00270001032700010000E852DFE608B14148921CB2EED977A6340008B77A5C561934E08908000211091109110D0600011109110D0600011111110D032000010906151215021119121002061C060001121D111905000101111905200101121D0420001121052001011121042000121D030611210306121D0428001121042800121D042001010E042001010205200101115904200101088126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730100000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D44617461416363657373010000000320000204200011190306111908000211211121121D06000111091121030611090507021109020400001121060702110911218126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730000000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D4461746141636365737300000000040001020E0500011111020407011111040001011C08151215021119121005200102130006200113011300070002021121112107200201130013010C07061210121D121D1C0211210307010202060E0320000E0600030E0E0E0E062002010E12750320001C050001121D0E040000121D0E07080E0E121D12750E1279121D020300000104010000000407011121040701121D180100134272696467656C696E652E54696D655A6F6E6500000501000000000C010007352E312E302E3000001B0100164272696467656C696E65204469676974616C20496E6300002101001C436F7079726967687420C2A9204272696467656C696E65203230313300000A010005694150505300000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F7773010000000000D445875300000000020000001C010000603400006016000052534453C6BD18E8FBB1154EAA95260CEE19257301000000643A5C69415050535C52656C65617365735C5635315C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E54696D655A6F6E655C6F626A5C44656275675C4272696467656C696E652E54696D655A6F6E652E706462000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000A43500000000000000000000BE350000002000000000000000000000000000000000000000000000B0350000000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF2500200010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058400000480300000000000000000000480334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100010005000000000001000500000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004A8020000010053007400720069006E006700460069006C00650049006E0066006F00000084020000010030003000300030003000340062003000000050001700010043006F006D00700061006E0079004E0061006D006500000000004200720069006400670065006C0069006E00650020004400690067006900740061006C00200049006E00630000000000500014000100460069006C0065004400650073006300720069007000740069006F006E00000000004200720069006400670065006C0069006E0065002E00540069006D0065005A006F006E0065000000300008000100460069006C006500560065007200730069006F006E000000000035002E0031002E0030002E003000000050001800010049006E007400650072006E0061006C004E0061006D00650000004200720069006400670065006C0069006E0065002E00540069006D0065005A006F006E0065002E0064006C006C0000005C001C0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004200720069006400670065006C0069006E0065002000320030003100330000005800180001004F0072006900670069006E0061006C00460069006C0065006E0061006D00650000004200720069006400670065006C0069006E0065002E00540069006D0065005A006F006E0065002E0064006C006C0000002C0006000100500072006F0064007500630074004E0061006D00650000000000690041005000500053000000340008000100500072006F006400750063007400560065007200730069006F006E00000035002E0031002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000035002E0031002E0030002E0030000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003000000C000000D03500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    WITH PERMISSION_SET = UNSAFE;

GO
PRINT N'Creating [dbo].[ClearCachedTimeZone]...';
GO
CREATE FUNCTION [dbo].[ClearCachedTimeZone]
(@siteId UNIQUEIDENTIFIER)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ClearCachedTimeZone]


GO
PRINT N'Creating [dbo].[ConvertTimeFromUtc]...';


GO
CREATE FUNCTION [dbo].[ConvertTimeFromUtc]
(@utcDateTime DATETIME, @siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ConvertTimeFromUtc]


GO
PRINT N'Creating [dbo].[ConvertTimeToUtc]...';


GO
CREATE FUNCTION [dbo].[ConvertTimeToUtc]
(@localDateTime DATETIME, @siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[ConvertTimeToUtc]


GO
PRINT N'Creating [dbo].[GetLocalDate]...';


GO
CREATE FUNCTION [dbo].[GetLocalDate]
(@siteId UNIQUEIDENTIFIER)
RETURNS DATETIME
AS
 EXTERNAL NAME [Bridgeline.TimeZone].[UserDefinedFunctions].[GetLocalDate]
 GO
 PRINT N'Creating [dbo].[Facet_GetFacetsByNavigation]...';

 GO
 ALTER PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(Id)
        FROM    ATFacetRangeProduct_Cache

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN
                SELECT  C.FacetID ,
                        C.FacetValueID ,
                        MIN(DisplayText) DisplayText ,
                        COUNT(*) ProductCount ,
                        MIN(Sequence) FacetSequence ,
                        CASE MIN(SortOrder)
                          WHEN -1 THEN @totProd - COUNT(*)
                          ELSE MIN(SortOrder)
                        END AS SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

                    SELECT  C.FacetID ,
                            C.FacetValueID ,
                            MIN(DisplayText) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            CASE MIN(SortOrder)
                              WHEN -1 THEN @totProd - COUNT(*)
                              ELSE MIN(SortOrder)
                            END AS SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProduct_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
                            WHERE   F.AllowMultiple = 0
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END



GO
PRINT N'Creating [dbo].[Facet_ReloadProductFacetCache]...';

 GO
 ALTER  PROCEDURE [dbo].[Facet_ReloadProductFacetCache]
(
	@ApplicationId uniqueidentifier=null
)
--now only 39 seconds, now 50 seconds with all Enumed Faceted in Shaw
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

--create temp table to load new cache information into 
--(not sure if this is the best way to declare a large temp table - DM)
 
DECLARE @tempCache TABLE 
( 
    Id INT IDENTITY(1,1) Primary Key, 
    FacetId uniqueidentifier,
	AttributeId uniqueidentifier,
	FacetValueID uniqueidentifier,
	DisplayText nvarchar(50),
	ProductId uniqueidentifier,
	SortOrder int Default(0),
	Sequence int Default(0)
)

SELECT 'Insert INTO @TempCache'

--Enumerated Attributes Defined Ranges
INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT FacetID, AttributeID, AttributeEnumId AS FacetValueID, Value AS DisplayText, ProductId
--, AE.Value AS [From], AE.Value AS [To]
FROM 
	vwProductSKUFacetAttributeValue_Enumerated PSFAV 
WHERE IsRange = 0 AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId) 
--30 seconds
SELECT 'Insert INTO @cteValueToId No Range'
-- 47 seconds insert into @TempCache 159k records
-- 22 seconds insert into @TempCache 243k records


SELECT 'Insert INTO @tempCache Numeric'

--Ranges Per Type:
INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
-- select *
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID -- F.IsRange = 1
	INNER JOIN ATFacetRangeInt FRX ON FR.Id = FRX.FacetRangeID 
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON F.AttributeID = PSFAV.AttributeId AND
			PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)  
			AND ISNUMERIC(PSFAV.Value)=1
WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)

SELECT 'Insert INTO @tempCache Date'

INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1 
	INNER JOIN ATFacetRangeDate FRX ON FR.Id = FRX.FacetRangeID
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value) AND ISDATE(PSFAV.Value)=1
    AND F.AttributeID = PSFAV.AttributeId
    WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)
    
--WHERE F.IsRange = 1 --and PSFAV.AttributeEnumId is null
-- 14 seconds
SELECT 'Insert INTO @tempCache Decimal'

INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1 
	INNER JOIN ATFacetRangeDecimal FRX ON FR.Id = FRX.FacetRangeID 
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value) AND ISNUMERIC(PSFAV.Value)=1
    AND F.AttributeID = PSFAV.AttributeId
    WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)
--WHERE F.IsRange = 1 --and PSFAV.AttributeEnumId is null
-- 13 seconds

SELECT 'Insert INTO @tempCache String'

INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --AND F.IsRange = 1 
	INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeID 
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)
    AND F.AttributeID = PSFAV.AttributeId
    WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)
--WHERE F.IsRange = 1 --and PSFAV.AttributeEnumId is null
-- 0 seconds
SELECT 'Updating Sequence'

Update T 
Set T.SortOrder = R.Sequence 
FROM @tempCache T
INNER JOIN ATFacet F on T.FacetID =F.Id
INNER JOIN ATFacetRange R on R.FacetID =F.Id AND T.FacetValueID = R.Id
Where IsRange=1 AND IsManualOrder =1;

with CTEUpdate AS
(
	SELECT  ROW_NUMBER() OVER(ORDER BY DisplayText Asc) RowNumber,
			FacetValueID,
			FacetID,
			ProductId 
	FROM @tempCache T
	INNER JOIN ATFacet F on T.FacetID =F.Id
	Where DisplayOrder =1 AND IsManualOrder=0
)


Update T
Set T.SortOrder =IsNull(C.RowNumber - (SELECT MIN(RowNumber) -1 FROM CTEUpdate R Where R.FacetID =T.FacetID) , 1) 
FROM @tempCache T
INNER JOIN CTEUpdate C ON T.FacetValueID =C.FacetValueID ;
--
--With CTEUpdate2 AS
--(
--	SELECT  ROW_NUMBER() OVER(ORDER BY count(ProductID) Desc) RowNumber,
--			FacetValueID,
--			FacetID
--	FROM @tempCache T
--	INNER JOIN ATFacet F on T.FacetID =F.Id
--	Where DisplayOrder =2 AND IsManualOrder=0
--	Group BY FacetValueID,FacetID
--)

-- if the sort order is supposed to be count then we can not do it here so set a -1
Update T
Set T.SortOrder =-1
FROM @tempCache T
INNER JOIN ATFacet F ON T.FacetID =F.Id
Where F.DisplayOrder = 2 And IsManualOrder = 0

Update T
Set T.Sequence =NF.Sequence
FROM @tempCache T
INNER JOIN NVNavNodeFacet NF ON T.FacetID =NF.FacetId

SELECT 'Insert INTO ATFacetRangeProduct_Cache'

--delete data from existing cache
truncate table dbo.ATFacetRangeProduct_Cache

	--repopulate the real cache
	--(again, best method for this? this will be a LOT of data - DM)
	INSERT INTO dbo.ATFacetRangeProduct_Cache 
	(Id,FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,FacetSequence)
	SELECT newid(),T.FacetId,T.AttributeId,T.FacetValueID,T.DisplayText,T.ProductId,T.SortOrder ,T.Sequence
	FROM @tempCache T

TRUNCATE TABLE ATFacetRangeProductTop_Cache

-- Create Distinct Product Count by Facet.
INSERT INTO ATFacetRangeProductTop_Cache --112,008
SELECT	
	[FacetID],    
    [ProductID]    
FROM 
	ATFacetRangeProduct_Cache --242,572
GROUP BY
	[FacetID]  
  ,[ProductID]

END


GO
PRINT N'Cleaning customer password from log';
Update V SET XMLString.modify('
    replace value of
        (/LogItem/Changes/ChangedItem[ProperyName="Password"]/NewValue/text())[1]
    with
        ("xxxx")
') 
from VEVersion  V
where ObjectTypeId=211
and XMLString.exist('//ProperyName[text()="Password"]')=1


GO
PRINT N'Update complete Bridgeline.TimeZone';
GO
