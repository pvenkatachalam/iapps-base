﻿GO
ALTER Procedure [dbo].[Site_CreateCommerceDefaultData](@ApplicationId uniqueidentifier, @SiteTitle Varchar(1024), @CreatedById uniqueidentifier)
As
BEGIN

	DECLARE @CommerceProductId uniqueidentifier
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'
	DECLARE @Now DateTime, @maxId int 
	DECLARE @maxSecurityId int
	
	SET @Now = GETUTCDATE()

	-- Product Category
	IF NOT EXISTS(SELECT * FROM PRCategory WHERE Title ='Unassigned')
	BEGIN
		INSERT INTO PRCategory (Id, Title, Description, Sequence, CreatedDate, Status)
		Values('6988B7A8-7C72-4B58-9B68-258B940E2363', 'Unassigned', 'Unassigned', 1, @Now, 1)
	END

	--Insert default value for order number
	IF NOT EXISTS(SELECT * FROM GLIdentity WHERE Name ='OrderNumber' )
	BEGIN
		INSERT INTO GLIdentity (Name,Value) Values ('OrderNumber', 0)
	END

	-- Product Import Config - Did not take it.

	-- Coupon Type
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('D1CA3949-EBFA-4DEC-B120-07DAA8961F81',	'% off Order','',		1,	1,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('2C24DC1C-268E-4ACB-933C-618DEF52E434',	'$ off Order',	'',	1,	2,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('C4FE5439-0B9F-4B41-9221-0302C527DEAE',	'$ off Shipping',	'',	1,	32,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('894D41B5-27D9-4C3B-885D-18FFC565DB8B',	'% off Shipping',	'',	1,	4,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='Flat rate shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('004202FA-B429-4AB7-A59F-ED37C74C0421',	'Flat rate shipping','',		1,	5,	1)
	END

	--Customer Group.sql
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Gold')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Gold',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Silver')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Silver',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Bronze')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Bronze',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Frequent Customer')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Frequent Customer',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='B2B')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'B2B',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Friends & Family')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Friends & Family',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Employees')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Employees',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	-- EveryOne Group - We are not using this group as of now
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Everyone')
	BEGIN
		INSERT INTO USGroup (ProductId, ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, 
		ModifiedBy, ModifiedDate, Status, ExpiryDate, GroupType)
		Values
		(@CommerceProductId,	@ApplicationId,	
		NEWID(),	'Everyone',	'everyone',	
		@CreatedById,	@Now,
		@CreatedById,	@Now,	1,	NULL,	2)
	END

	--- Security Level
	SELECT @maxSecurityId = Isnull(Max(Id),0) + 1 FROM USSecurityLevel

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Commerce Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId, 'Commerce Customer', 'Commerce Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Preferred Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+1, 'Preferred Customer', 'Preferred Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Employee')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+2, 'Employee', 'Employee', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Press')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+3, 'Press', 'Press', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	-- Feature Type
	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto Top Seller')
	BEGIN
		Insert into MRFeatureType Values(1,'Auto Top Seller')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto New Products')
	BEGIN
		Insert into MRFeatureType Values(2,'Auto New Products')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Manual')
	BEGIN
		Insert into MRFeatureType Values(3,'Manual')
	END

	-- Feature
	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Top Sellers' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Top Sellers' , 'Top Sellers Auto', 1, 1, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='New Products' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'New Products' , 'New Products Auto', 2,  2, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Moving Brand' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Moving Brand' , 'Moving Brand Manual', 3, 3, 1, newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Stock Clearance' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Stock Clearance' , 'Stock Clearance Manual', 3,  4, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Christmas Sales' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Christmas Sales' , 'Christmas Sales Manual', 3,  5, 2,newId(), GetUTCDate())

	-- AttributeDataType
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Image Library' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Image Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Date' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.DateTime','Date')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Decimal' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.Double','Decimal')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Asset Library File' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Asset Library File')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Page Library' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Page Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='HTML' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','HTML')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='File Upload' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','File Upload')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Integer' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.Int32','Integer')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='String' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','String')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Boolean')  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.Boolean','Boolean')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Content Library' )  BEGIN   INSERT INTO ATAttributeDataType(Id,Type,Title) values(NEWID(),'System.String','Content Library')  END

	-- CommerceStatus 
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Active' )  BEGIN   INSERT INTO CMCommerceStatus values(1,'Active','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Inactive' )  BEGIN   INSERT INTO CMCommerceStatus values(2,'Inactive','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Deleted' )  BEGIN   INSERT INTO CMCommerceStatus values(3,'Deleted','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Approved' )  BEGIN   INSERT INTO CMCommerceStatus values(4,'Approved','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Rejected' )  BEGIN   INSERT INTO CMCommerceStatus values(5,'Rejected','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='PendingForApproval' )  BEGIN   INSERT INTO CMCommerceStatus values(6,'PendingForApproval','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Included' )  BEGIN   INSERT INTO CMCommerceStatus values(7,'Included','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Excluded')  BEGIN   INSERT INTO CMCommerceStatus values(8,'Excluded','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Expired')  BEGIN   INSERT INTO CMCommerceStatus values(9,'Expired','')  END

	-- GLReason
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Damaged')  BEGIN   INSERT INTO GLReason values(NEWID(),'Damaged',210,4,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Restocking')  BEGIN   INSERT INTO GLReason values(NEWID(),'Restocking',210,1,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Bad Inventory')  BEGIN   INSERT INTO GLReason values(NEWID(),'Bad Inventory',210,2,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Returned Item')  BEGIN   INSERT INTO GLReason values(NEWID(),'Returned Item',210,8,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Lost')  BEGIN   INSERT INTO GLReason values(NEWID(),'Lost',210,3,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Physical Count')  BEGIN   INSERT INTO GLReason values(NEWID(),'Physical Count',210,6,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='New Shipment')  BEGIN   INSERT INTO GLReason values(NEWID(),'New Shipment',210,5,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Other')  BEGIN   INSERT INTO GLReason values(NEWID(),'Other',210,7,1)  END

	-- GLAddress
	IF NOT EXISTS (SELECT * FROM GLAddress Where Id = 'A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F')
	BEGIN
		INSERT INTO  GLAddress(
		Id, FirstName, LastName, AddressType, AddressLine1, AddressLine2, AddressLine3, City, StateId, Zip, 
		CountryId, Phone, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, StateName, County)
		Values
		('A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F',
		'Warehouse','Location',0,'Your warehouse address', 250,	NULL	,'City',
		'7E07B39B-BC66-4969-B16A-C62E41043E39',	'30092',	'AD72C013-B99D-4513-A666-A128343CCFF0',	'7819955555',
		'2011-08-01 11:18:32.880',	'E131010D-BE50-4526-B343-B0954E924080',	'2011-08-01 11:18:32.880',	
		'E131010D-BE50-4526-B343-B0954E924080',	1,	'MA','County')	
	END

	-- INWarehouse
	Declare @WarehouseId uniqueidentifier
	DECLARE @CopyWarehouseId uniqueidentifier
		
	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId Where Title = 'Default Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Default Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
			Values(@WarehouseId,	'Default warehouse',	
			'Default warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
			null,	1, 'Internal', 'InternalWarehouseProvider', 1,1)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
		from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
		Where T.IsDownloadableMedia =0 OR T.IsDownloadableMedia is null

	END

	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId  Where Title = 'Digital Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Digital Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
		Values(@WarehouseId,	'Digital Warehouse',	
		'Internal Digital Warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
		Null,	0, Null, 'DigitalWarehouseProvider', 2,0)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
			from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
			Where T.IsDownloadableMedia =1 
	END

	-- Product Type
	--IF NOT EXISTS (SELECT * FROM PRProductType Where SiteId = @ApplicationId)
	IF ((SELECT COUNT(*) FROM PRProductType)=0)
	BEGIN
		Insert into PRProductType(Id,ParentId,LftValue,RgtValue,Title, Description, SiteId, CreatedDate, CreatedBy, Status)
		Values(newId(),@ApplicationId,1,2,@SiteTitle, @SiteTitle ,@ApplicationId, @Now,@CreatedById ,1 )
	END

	-- ORShipper
	DECLARE @Shipper_USPS uniqueidentifier, @Shipper_UPS uniqueidentifier, @Shipper_Fedex uniqueidentifier

	IF EXISTS (SELECT * FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_USPS = (SELECT Id FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId)
	END	
	ELSE
	BEGIN
		SET @Shipper_USPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_USPS,'USPS','USPS','','iAppsUSPSProvider',1,3,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END	
	IF EXISTS (SELECT * FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_UPS = (SELECT Id FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_UPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_UPS,'UPS','UPS','http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum={0}&AgreeToTermsAndConditions=yes&ignore=&track.x=46&track.y=7',
		'iAppsUpsProvider',1,2,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END
	IF EXISTS (SELECT * FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_Fedex = (SELECT Id FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_Fedex = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_Fedex,'Fedex','Fedex','','provider',1,1,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END

	 
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpressSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpressSM',
		'Worldwide ExpressSM','',5,0,0,0,0,1,'07',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
		
	END	
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air Saver','Next Day Air Saver',
		'customid',1,5,0,0,1,0,'13',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='UPS Next Day Air Early A.M. SM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'UPS Next Day Air Early A.M. SM','UPS Next Day Air® Early A.M. SM',
		'',1,0,0,0,0,0,'14',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Next Flight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Next Flight',
		'','',5,0,0,0,0,1,'INTERNATIONAL_FIRST',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Standard','Standard','',8,0,0,0,0,1,'11',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air A.M' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air A.M','Second Day Air A.M','',2,0,0,0,0,0,'59',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First-Class Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'First-Class Mail','1st Class','customid',5,5,0,0,1,0,'0',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International  Economy' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International  Economy','','',7,0,0,0,0,1,'INTERNATIONAL_ECONOMY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Saver','Saver','',4,0,0,0,0,0,'65',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day A.M.' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day A.M.','','',0,0,0,0,0,0,'FEDEX_2_DAY_AM',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpeditedSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpeditedSM','Worldwide ExpeditedSM','',4,0,0,0,0,1,'08',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Three-Day Select' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Three-Day Select','UPS Three-Day Select','customid',3,5,0,0,1,0,'12',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air','UPS Second Day Air','customid',2,5,0,0,1,0,'02',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Priority Overnight','','',1,0,0,0,0,1,'PRIORITY_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Priority' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Priority','','',0,0,0,0,0,1,'INTERNATIONAL_PRIORITY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Priority Mail','Priority Mail','customid',2,5,0,0,1,0,'1',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Express Saver','','',3,0,0,0,0,0,'FEDEX_EXPRESS_SAVER',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'First Overnight','Collège dOccitanie','',4,0,0,0,0,1,'FIRST_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Home Delivery' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Home Delivery','','customid',2,5,0,0,1,0,'GROUND_HOME_DELIVERY',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air','UPS Next Day Air','customid',1,5,0,0,1,0,'01',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide Express PlusSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide Express PlusSM','Worldwide Express PlusSM','',4,0,0,0,0,1,'54',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day','','',5,0,0,0,0,0,'FEDEX_2_DAY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Standard Overnight','','',4,0,0,0,0,0,'STANDARD_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Express','Express','customid',1,5,0,0,1,0,'2',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Ground' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Ground','UPS Ground','customid',5,5,0,0,1,0,'03',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	
	
	
	-- ATAttributeGroup
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='01614e00-86f8-4448-9a48-7daa63171c38')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'01614e00-86f8-4448-9a48-7daa63171c38', N'Features', @ApplicationId, 1, @CreatedById, @Now, @CreatedById, @Now , 1)
	END	
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='b23ffc1b-ab9d-4985-beb4-a5f303b9ef41')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'SKULevel Attributes', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Advanced Details', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'AB730D27-5CCD-49D9-B515-168290438E0A', N'Product Properties', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='3C211CF2-A184-4375-A078-242F4387D1D2')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'3C211CF2-A184-4375-A078-242F4387D1D2', N'Customer Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='9DABE135-467C-459E-ABF2-5FBD64821989')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'9DABE135-467C-459E-ABF2-5FBD64821989', N'Order Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A', N'Advanced Details', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END

	-- ATAttribute
	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0611ed6a-d071-4dff-b6c1-b75a06a17ccf')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as Top Seller', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now,  1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0b94496d-9df4-4ac1-8c5c-48b42f99c4ef')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Discounts', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='2c4983dd-d7eb-40a2-9b05-bfd308e3da62')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Free Shipping', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='4ae05c5a-2375-49d8-8bfb-d131632c0e09')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as New Item', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='7b39ffe5-36b9-44ef-b7a5-08f3c18181aa')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Refundable', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c1a20d3c-985f-49be-9682-df09eaf82fc8')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Freight', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c57665fd-e681-4c13-82ce-d77e60bf9a72')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Tax Exempt', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f4963a96-57dc-4c67-830e-f7426539c8ce')
	--INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	--[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	--VALUES (N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Unlimited Quantity', N'', 1, 
	--NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f68ceb85-922b-4023-a6f9-20025f7fdc94')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'System.String', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Others', 
	NULL, 0, N'AT1', 1, 1, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f7102f66-4242-408e-a816-f8b3e600f4d0')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Shipping Promotions', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='39C2D877-525F-49A6-9E48-D3296989F83B')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('39C2D877-525F-49A6-9E48-D3296989F83B','System.String','AB730D27-5CCD-49D9-B515-168290438E0A','Product Name',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='ECB9E9D1-FC3F-43A5-B40F-3840212F693C')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('ECB9E9D1-FC3F-43A5-B40F-3840212F693C','System.Double','AB730D27-5CCD-49D9-B515-168290438E0A','ListPrice',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	-- ATAttributeEnum
	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='bbf74b61-05d8-49b4-8fee-72147d849814')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'bbf74b61-05d8-49b4-8fee-72147d849814', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='073c66b8-2fc2-4d82-aebe-3165f0138ad5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'073c66b8-2fc2-4d82-aebe-3165f0138ad5', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='51424fd1-0da2-4792-b692-7beef5679bf3')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'51424fd1-0da2-4792-b692-7beef5679bf3', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='f0974b83-4d83-4534-bf87-1f8a8b0237e8')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'f0974b83-4d83-4534-bf87-1f8a8b0237e8', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='0b99d94e-606a-43a7-b72c-d6512fd38bf2')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'0b99d94e-606a-43a7-b72c-d6512fd38bf2', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='235f7a94-52c7-42d8-b870-c0d53906b1e5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'235f7a94-52c7-42d8-b870-c0d53906b1e5', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='11bfcfbd-3ffa-4791-8b39-1f12b80d8524')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'11bfcfbd-3ffa-4791-8b39-1f12b80d8524', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='d8d961c0-5c4a-487b-9913-a920e73cda1a')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'd8d961c0-5c4a-487b-9913-a920e73cda1a', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='3a1e07c0-18d1-4c00-a73c-73a4953d5e41')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'3a1e07c0-18d1-4c00-a73c-73a4953d5e41', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='872eeb49-e8eb-4f73-837c-71ddb20629c4')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'872eeb49-e8eb-4f73-837c-71ddb20629c4', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='ee919400-c7de-4d9c-ae8f-fefd1c768e64')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'ee919400-c7de-4d9c-ae8f-fefd1c768e64', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='49a6b896-2f22-45d1-82fb-e47e8f2519c9')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'49a6b896-2f22-45d1-82fb-e47e8f2519c9', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='8bb63596-8fee-4005-9109-dda014ba147e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'8bb63596-8fee-4005-9109-dda014ba147e', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='70a27f03-06ca-4df2-99bb-5c23b840d465')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'70a27f03-06ca-4df2-99bb-5c23b840d465', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='e5f89737-8879-4e42-9aa1-bfffb05d808e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'e5f89737-8879-4e42-9aa1-bfffb05d808e', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='9b0a9c92-80c8-411f-9037-b0281c755201')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'9b0a9c92-80c8-411f-9037-b0281c755201', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='81a55708-cd4d-4fbd-b3c5-4e3292401511')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'81a55708-cd4d-4fbd-b3c5-4e3292401511', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='a0c6381b-2970-4d09-a582-334b93c51442')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'a0c6381b-2970-4d09-a582-334b93c51442', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'true', N'0', 0, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='691ee3cd-58ff-42d9-a8e8-385e85648cfc')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'691ee3cd-58ff-42d9-a8e8-385e85648cfc', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'false', N'1', 2, N'false', 0, 2, @CreatedById, @Now, @CreatedById, @Now, 1)


	--- CLImageType
	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='396b425c-e197-4f2d-a4a3-25448ca366f8')
	INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('396b425c-e197-4f2d-a4a3-25448ca366f8', N'main', 600, 800, null, N'alt_mainImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 1)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='63cf2418-684a-4d1f-88b2-34968c6f139f')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('63cf2418-684a-4d1f-88b2-34968c6f139f', N'system_thumb', 60, 60, N'systhumb_', null, 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 0)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='2770534e-4675-4764-a620-56776398e03e')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('2770534e-4675-4764-a620-56776398e03e', N'preview', 260, 372, N'prev_', N'alt_PrevImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 3)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='e02ed0f3-1271-4218-b81e-816e458434a1')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('e02ed0f3-1271-4218-b81e-816e458434a1', N'mini', 80, 80, N'mini_', N'alt_miniImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 4)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='ba9b3922-4f9f-4b21-a5cf-e03b26b588ad')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('ba9b3922-4f9f-4b21-a5cf-e03b26b588ad', N'thumbnail', 120, 120, N'thumb_', N'alt_thumbImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 2)

	--OROrderStatus
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='OK')  BEGIN   INSERT INTO OROrderStatus values(1,'OK','Submitted',1,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Shipped')  BEGIN   INSERT INTO OROrderStatus values(2,'Shipped','Shipped',2,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Canceled')  BEGIN   INSERT INTO OROrderStatus values(3,'Canceled','Canceled',3,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Back Ordered')  BEGIN   INSERT INTO OROrderStatus values(4,'Back Ordered','Back Ordered',4,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Approval')  BEGIN   INSERT INTO OROrderStatus values(5,'Pending Approval','On Hold',5,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Customer Registration Approval')  BEGIN   INSERT INTO OROrderStatus values(6,'Pending Customer Registration Approval','Pending Customer Registration Approval',6,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Bad Customer Order Approval')  BEGIN   INSERT INTO OROrderStatus values(7,'Pending Bad Customer Order Approval','Pending Bad Customer Order Approval',7,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Terms Limit')  BEGIN   INSERT INTO OROrderStatus values(8,'Exceeded Terms Limit','Exceeded Terms Limit',8,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Line of Credit')  BEGIN   INSERT INTO OROrderStatus values(9,'Exceeded Line of Credit','Exceeded Line of Credit',9,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Manual Hold')  BEGIN   INSERT INTO OROrderStatus values(10,'Manual Hold','Manual Hold',10,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Partially Shipped')  BEGIN   INSERT INTO OROrderStatus values(11,'Partially Shipped','Partially Shipped',11,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Created')  BEGIN   INSERT INTO OROrderStatus values(12,'Created','Created',12,0)  END

	-- OROrderNextStatus
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderNextStatus values(1,1,10)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderNextStatus values(2,1,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderNextStatus values(3,7,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderNextStatus values(4,7,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderNextStatus values(5,10,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderNextStatus values(6,10,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderNextStatus values(7,5,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =8)  BEGIN   INSERT INTO OROrderNextStatus values(8,5,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =9)  BEGIN   INSERT INTO OROrderNextStatus values(9,6,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =10)  BEGIN   INSERT INTO OROrderNextStatus values(10,6,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =11)  BEGIN   INSERT INTO OROrderNextStatus values(11,12,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =12)  BEGIN   INSERT INTO OROrderNextStatus values(12,11,2)  END

	-- OROrderType
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =1)  BEGIN   INSERT INTO OROrderType values(1,'Web','Web Order',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =2)  BEGIN   INSERT INTO OROrderType values(2,'CSR','Web Service Order',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =3)  BEGIN   INSERT INTO OROrderType values(3,'Brochure Request','Brochure Request',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =4)  BEGIN   INSERT INTO OROrderType values(4,'Recurring Order','Recurring ORder',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =5)  BEGIN   INSERT INTO OROrderType values(5,'Exchange Order','Exchange Order',5)  END

	-- OROrderItemStatus
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderItemStatus values(1,'Ok','Submitted',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderItemStatus values(2,'Shipped','Shipped',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderItemStatus values(3,'Cancelled','Cancelled',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderItemStatus values(4,'Backordered','Backordered',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderItemStatus values(5,'Pending','Pending',5)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderItemStatus values(6,'Released','Released',6)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderItemStatus values(7,'Partially Shipped','Partially Shipped',7)  END

	-- PTCreditCardType
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE Id ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487','American Express','American Express',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='28091FD7-00C2-4E71-A266-7FA718658A66')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('28091FD7-00C2-4E71-A266-7FA718658A66','MasterCard','MasterCard',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='A98F6177-90A5-420D-96B5-A234778F8D96')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('A98F6177-90A5-420D-96B5-A234778F8D96','Visa','Visa',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E','Discover','Discover',@CreatedById, @Now, @CreatedById, @Now)  
	END

	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE CreditCardTypeId ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487' And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='28091FD7-00C2-4E71-A266-7FA718658A66'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('28091FD7-00C2-4E71-A266-7FA718658A66',@ApplicationId,1) 
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='A98F6177-90A5-420D-96B5-A234778F8D96'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('A98F6177-90A5-420D-96B5-A234778F8D96',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E',@ApplicationId,1)  
	END

	-- PTGateway
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =1)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(1,'Website Payments Pro','Website Payments Pro',2,'WebsitePaymentsProGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =2)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(2,'None','None',1,'AutoGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =3)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(3,'Cybersource','Cybersource',3,'CybersourceGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =4)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description],  [Sequence], [DotNetProviderName]) 
		Values(4,'Payflow Pro','Payflow Pro',4,'PayflowProGatewayProvider') 
	END

	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =1)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(1,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =2)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(2,0,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =3)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(3,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =4)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(4,0,1,@ApplicationId) 
	END
	
	-- PTPaymentStatus
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =1)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(1,'Entered','Entered',1,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =2)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(2,'Auth','Authorized',2,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =3)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(3,'Captured','Captured',3,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =4)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(4,'Auth Failed','Authorization Failed',4,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =5)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(5,'Capture Failed','Capture Failed',5,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =6)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(6,'Clear','Clear',6,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =7)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(7,'Manual Cancel','Manual Cancel',7,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =8)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(8,'Partially Captured','Partially Captured',8,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =9)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(9,'Void','Void',9,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =10)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(10,'Refund Entered','Refund Entered',10,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =11)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(11,'Refund Waiting Approval','Refund Waiting Approval',11,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =12)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(12,'Refunded','Refunded',12,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =13)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(13,'Refund Failed','Refund Failed',13,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =14)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(14,'Refund Rejected','Refund Rejected',14,1)  END

	-- PTPaymentType
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =1)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(1,'CreditCard','Credit Card',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =2)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(2,'OnAccount','OnAccount',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =3)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(3,'Paypal','Paypal',5)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =4)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(4,'COD','Cash On Delivery',4)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =5)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(5,'GiftCard','Gift Card',2)  END

	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =1)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(1,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =2)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(2,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =3)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(3,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =4)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(4,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =5)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(5,@ApplicationId,1)  END

	IF (SELECT Count(*) FROM ORReturnReason)=0
	BEGIN

		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES( 
			'Defective')
			
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(  
			'Customer Ordered Wrong Product')
		    
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES('Refused Delivery')


		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Missing Merchandise')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Did not Like')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Changed Mind')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Billing Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Fraudulent Order')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Warehouse Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Returned by Shipper - No Response From Customer')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Other')

	END

	-- INWarehouseType
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =1)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(1,'Internal','iAPPS internal warehouse for Non-Digital Products')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =2)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(2,'Digital','iAPPS Digital warehouse')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =3)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(3,'External','Integrated with external system')  END

	-- INRestockingStatus
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Created'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (1,'Created','Created',1)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Queued'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (2,'Queued','Queued',2)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='In Progress'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (3,'In Progress','In Progress',3)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Canceled'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (4,'Canceled','Canceled',4)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Completed'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (5,'Completed','Completed',5)

	-- ORReturnResolutionType
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] ON
	Select @maxId =Isnull(MAX(Id),0) FROM ORReturnResolutionType
	SET @maxId = @maxId + 1

	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Refund'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Refund','Part or full paymnent is returned', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Replacement'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Replacement','Replacement', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Others'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Others','Others', @maxId)        
		SET @maxId = @maxId + 1
	END
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] OFF

	-- INInventory
	IF(NOT EXISTS(SELECT 1 FROM dbo.INInventory WHERE WarehouseId ='22FAC4BD-4954-415A-9014-3598DAEAB4D0'))  
	BEGIN   
		Declare @Id UniqueIdentifier
		Set @Id= '22FAC4BD-4954-415A-9014-3598DAEAB4D0'

		INSERT INTO [INInventory]
				   ([Id]
				   ,[WarehouseId]
				   ,[SKUId]
				   ,[Quantity]
				   ,[LowQuantityAlert]
				   ,[FirstTimeOrderMinimum]
				   ,[OrderMinimum]
				   ,[OrderIncrement]
				   ,[ReasonId]
				   ,[ReasonDetail]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
					SELECT newId()
					  ,@Id
					  ,Id
					  ,0
					  ,0
					  ,1
					  ,1
					  ,1
					  ,(SELECT Id FROM GLReason Where Title ='Other') As ReasonId
					  ,'Initial Data When SKU is created'
					  ,1
					  ,@CreatedById
					  ,@Now
					  ,@CreatedById
					  ,@Now
				   FROM PRProductSKU WHERE Id NOT IN 
					(SELECT SKUID FROM ININventory WHERE WarehouseId = @Id)
	END
	

	DECLARE @GroupObjectTypeId int
	DECLARE @MerchandisingRoleId int, @FulfillmentRoleId int, @MarketingRoleId int, @CSRRoleId int, 
		@EveryOneRoleId int, @CSRManagerRoleId int, @WHManagerRoleId int
	DECLARE @MerchandisingGroupId uniqueidentifier, @FulfillmentGroupId uniqueidentifier, 
		@MarketingGroupId uniqueidentifier, @CSRGroupId uniqueidentifier, @EveryOneGroupId uniqueidentifier, 
		@CSRManagerGroupId uniqueidentifier, @WHManagerGroupId uniqueidentifier

	SET @GroupObjectTypeId = 15 
	SET @MerchandisingRoleId = 19
	SET @FulfillmentRoleId = 20
	SET @MarketingRoleId = 21
	SET @CSRRoleId = 22
	SET @EveryOneRoleId = 29
	SET @CSRManagerRoleId = 32
	SET @WHManagerRoleId = 33	
	-- if the db has cms and default commerce data was missing 
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MerchandisingGroupId output,@Title = 'Merchandising',
			@Description = 'Merchandising',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
				
	END		
	ELSE
		SET @MerchandisingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')

	IF (@MerchandisingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MerchandisingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MerchandisingGroupId,2,@MerchandisingRoleId, @MerchandisingGroupId,1,1 )				
	END
	
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Fulfillment')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @FulfillmentGroupId output,@Title = 'Fulfillment',
			@Description = 'Fulfillment',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @FulfillmentGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Fulfillment')

	IF (@FulfillmentGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @FulfillmentGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@FulfillmentGroupId,2,@FulfillmentRoleId, @FulfillmentGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Marketing')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MarketingGroupId output,@Title = 'Marketing',
			@Description = 'Marketing',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @MarketingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Marketing')

	IF (@MarketingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MarketingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MarketingGroupId,2,@MarketingRoleId, @MarketingGroupId,1,1 )				
	END

	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSR')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRGroupId output,@Title = 'CSR',
			@Description = 'CSR',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSR')
	
	IF (@CSRGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRGroupId,2,@CSRRoleId, @CSRGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'EveryOne' And GroupType = 1)) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @EveryOneGroupId output,@Title = 'EveryOne',
			@Description = 'EveryOne',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @EveryOneGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'EveryOne')

	IF (@EveryOneGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @EveryOneGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@EveryOneGroupId,2,@EveryOneRoleId, @EveryOneGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSRManager')) 
	BEGIN	
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRManagerGroupId output,@Title = 'CSRManager',
			@Description = 'CSRManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSRManager')

	IF (@CSRManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRManagerGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRManagerGroupId,2,@CSRManagerRoleId, @CSRManagerGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'WHManager')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @WHManagerGroupId output,@Title = 'WHManager',
			@Description = 'WHManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @WHManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'WHManager')

	IF (@WHManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @WHManagerGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@WHManagerGroupId,2,@WHManagerRoleId, @WHManagerGroupId,1,1 )				
	END
			
END
GO