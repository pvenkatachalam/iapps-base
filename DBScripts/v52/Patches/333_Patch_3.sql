GO
PRINT 'Updating the product version as 5.2.0228.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.2.0228.1'
GO
PRINT 'Altering column MKCampaign.Title'
GO
IF (COL_LENGTH('MKCampaign','Title') IS NOT NULL)
ALTER TABLE  MKCampaign ALTER COLUMN Title NVARCHAR (MAX) NULL
GO
PRINT 'Altering column MKCampaign.Description'
GO
IF (COL_LENGTH('MKCampaign','Description') IS NOT NULL)
ALTER TABLE  MKCampaign ALTER COLUMN [Description] NVARCHAR (MAX) NULL
GO
PRINT 'Altering column MKCampaignEmail.EmailSubject'
GO
IF (COL_LENGTH('MKCampaignEmail','EmailSubject') IS NOT NULL)
ALTER TABLE  MKCampaignEmail ALTER COLUMN EmailSubject NVARCHAR (MAX) NULL
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW vwContactPropertiesAndAttributes
AS
SELECT ContactId, AttributeId, AttributeEnumId, Value, Notes, CAV.CreatedDate, CAV.CreatedBy, CAV.CreatedDate AS ModifiedDate, CAV.CreatedBy AS ModifiedBy, ADT.Type as DataType FROM ATContactAttributeValue CAV INNER JOIN ATAttribute A ON A.ID = CAV.AttributeId INNER JOIN ATAttributeDataType ADT ON ADT.Id = A.AttributeDataTypeId WHERE (Value IS NOT NULL OR AttributeEnumId IS NOT NULL) AND ContactId IS NOT NULL
UNION 
SELECT Id AS ContactId,'11111111-0000-0000-0000-000000000000', NULL, FirstName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.MKContact WHERE FirstName IS NOT NULL
UNION
SELECT Id AS ContactId,'22222222-0000-0000-0000-000000000000', NULL, MiddleName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.MKContact WHERE MiddleName IS NOT NULL
UNION
SELECT Id AS ContactId,'33333333-0000-0000-0000-000000000000', NULL, LastName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.MKContact WHERE LastName IS NOT NULL
UNION
SELECT Id AS ContactId,'44444444-0000-0000-0000-000000000000', NULL, Email AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.MKContact WHERE Email IS NOT NULL
UNION
SELECT Id AS ContactId,'55555555-0000-0000-0000-000000000000', NULL, CompanyName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.MKContact WHERE CompanyName IS NOT NULL
UNION 
SELECT Id AS ContactId,'11111111-0000-0000-0000-000000000000', NULL, FirstName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.USUser WHERE FirstName IS NOT NULL
UNION
SELECT Id AS ContactId,'22222222-0000-0000-0000-000000000000', NULL, MiddleName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.USUser WHERE MiddleName IS NOT NULL
UNION
SELECT Id AS ContactId,'33333333-0000-0000-0000-000000000000', NULL, LastName AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.USUser WHERE LastName IS NOT NULL
UNION
SELECT UserId AS ContactId,'44444444-0000-0000-0000-000000000000', NULL, Email AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, 'System.String'  FROM dbo.USMembership M INNER JOIN dbo.USUser U ON U.Id = M.UserId WHERE Email IS NOT NULL

--UNION
--SELECT Id AS ContactId,'20000000-0000-0000-0000-000000000000', NULL, CAST(BirthDate AS nvarchar(500))AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE BirthDate IS NOT NULL
--UNION
--SELECT Id AS ContactId,'50000000-0000-0000-0000-000000000000', NULL, HomePhone AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE HomePhone IS NOT NULL
--UNION
--SELECT Id AS ContactId,'70000000-0000-0000-0000-000000000000', NULL, MobilePhone AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE MobilePhone IS NOT NULL
--UNION
--SELECT Id AS ContactId,'80000000-0000-0000-0000-000000000000', NULL, OtherPhone AS VALUE, NULL AS NOTES, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy  FROM dbo.MKContact WHERE OtherPhone IS NOT NULL
GO

PRINT 'Adding column MKEmailSendActions.ControlId'
GO
IF (COL_LENGTH('MKEmailSendActions','ControlId') IS NULL )
ALTER TABLE MKEmailSendActions ADD  [ControlId] NVARCHAR(255) NULL
GO
IF (OBJECT_ID('Template_GetAllTemplates') IS NOT NULL)
	DROP PROCEDURE Template_GetAllTemplates
GO
PRINT 'Altering procedure ContactDto_Save'
GO
ALTER PROCEDURE [dbo].[ContactDto_Save]
(
	
	@Id				uniqueidentifier = null OUTPUT,
	@SiteId			uniqueidentifier ,
	@Email			nvarchar(max) = null,
	@FirstName		nvarchar(max) = null,
	@MiddleName		nvarchar(max) = null,
	@LastName		nvarchar(max) = null,
	@CompanyName	nvarchar(max) = null,
	@Gender			nvarchar(max) = null,
	@BirthDate		datetime = null,
	@HomePhone		nvarchar(max) = null,
	@MobilePhone	nvarchar(max) = null,
	@OtherPhone		nvarchar(max) = null,
	@ImageId		uniqueidentifier,
	@Notes			nvarchar(max) = null,
	@ContactSourceId int = null,
	@Status			int = NULL,
	@LastSynced		datetime =null,
	@ModifiedBy		uniqueidentifier = null,
	@ChangedLogXml	xml = null,
	@Address		xml = null,
	@PrimarySiteId	uniqueidentifier = null
)
AS
BEGIN
	DECLARE @UtcNow datetime, @IsNewUser bit
	SET @UtcNow = GETUTCDATE()
	
	IF (@ChangedLogXml IS NULL)
		SET @IsNewUser = 1
	ELSE
		SET @IsNewUser = 0

	IF ((@Id IS NULL OR @Id = dbo.GetEmptyGUID() ) AND  @IsNewUser = 1)
	BEGIN
		SET @Id = NEWID()
		
		IF EXISTS (SELECT 1 FROM MKContact Where Email = @Email )
		BEGIN
			 RAISERROR('EXISTS||%s', 16, 1, 'Email')
             RETURN dbo.GetBusinessRuleErrorCode()
		END

		INSERT INTO MKContact
		(
			Id,
			Email,
			FirstName,
			MiddleName,
			LastName,
			CompanyName,
			Gender,
			BirthDate,
			HomePhone,
			MobilePhone,
			OtherPhone,
			ImageId,
			Notes,
			ContactSourceId,
			Status,
			LastSynced,
			CreatedBy,
			CreatedDate,
			ModifiedDate
		)
		VALUES
		(
			@Id,
			@Email,
			@FirstName,
			@MiddleName,
			@LastName,
			@CompanyName,
			@Gender,
			@BirthDate,
			@HomePhone,
			@MobilePhone,
			@OtherPhone,
			@ImageId,
			@Notes,
			@ContactSourceId,
			@Status,
			@LastSynced,
			@ModifiedBy,
			@UtcNow,
			@UtcNow
		)

		--Update the MKContactSite
		INSERT INTO MKContactSite
		(
			ContactId,
			SiteId,
			Status,
			IsPrimarySite
		)
		VALUES
		(
			@Id,
			@SiteId,
			@Status,
			1
		)

	END
	ELSE
	BEGIN

		IF(@IsNewUser = 0 AND @Id IS NULL )
			SELECT TOP 1 @Id =  Id FROM MKContact WHERE LOWER(LTRIM(RTRIM(Email))) = LOWER(LTRIM(RTRIM(@Email)))

		UPDATE MKContact
		SET 
			Email = ISNULL(@Email,Email),
			FirstName = ISNULL(@FirstName,FirstName),
			MiddleName = ISNULL(@MiddleName,MiddleName),
			LastName = ISNULL(@LastName,LastName),
			CompanyName = ISNULL(@CompanyName,CompanyName),
			Gender = ISNULL(@Gender,Gender),
			BirthDate = ISNULL(@BirthDate,BirthDate),
			HomePhone = ISNULL(@HomePhone,HomePhone),
			MobilePhone = ISNULL(@MobilePhone,MobilePhone),
			OtherPhone = ISNULL(@OtherPhone,OtherPhone),
			ImageId = ISNULL(@ImageId,ImageId),
			Notes = ISNULL(@Notes,Notes),
			ContactSourceId = ISNULL(@ContactSourceId,ContactSourceId),
			LastSynced = @LastSynced,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = case when @LastSynced>LastSynced then isnull(ModifiedDate,CreatedDate) else @UtcNow end 
		WHERE Id = @Id 

		--Updates the MKContactNotes table with the changed log
		INSERT INTO MKContactNotes 
		(
			Id,
			ContactId,
			changelog,
			[Version],
			CreatedDate, 
			CreatedBy
		)
		VALUES
		( 
			NEWID(), 
			@Id, 
			ISNULL(@ChangedLogXml,''),
			(SELECT ISNULL(MAX([Version]), 0) + 1 FROM MKContactNotes WHERE ContactId=@Id),
			@UtcNow,
			@ModifiedBy
		)

		IF (@Address IS NOT NULL)
		BEGIN
			DECLARE @AddressId uniqueidentifier
			EXEC [dbo].[AddressDto_Save] 
				@Id = @AddressId OUTPUT,
				@Address = @Address,
				@ModifiedBy = @ModifiedBy

			UPDATE MKContact SET AddressId = @AddressId WHERE Id = @Id
		END

		IF (NOT EXISTS (SELECT 1 FROM MKContactSite WHERE ContactId = @Id AND SiteId = @SiteId))
		BEGIN
		
			--Update previous primary site
			UPDATE MKContactSite
			SET IsPrimarySite = 0
			WHERE ContactId = @Id
		
			--Update the MKContactSite
			INSERT INTO MKContactSite
			(
				ContactId,
				SiteId,
				Status,
				IsPrimarySite
			)
			VALUES
			(
				@Id,
				@SiteId,
				@Status,
				1
			)		
		END 
		ELSE
			UPDATE MKContactSite
			SET Status = @Status
			WHERE ContactId = @Id AND SiteId = @SiteId
		
		
		IF (@PrimarySiteId IS NOT NULL)
		BEGIN
			
			--Update previous primary site
			UPDATE MKContactSite
			SET IsPrimarySite = 0
			WHERE ContactId = @Id
			
			IF (NOT EXISTS (SELECT 1 FROM MKContactSite WHERE ContactId = @Id AND SiteId = @PrimarySiteId))
			BEGIN
			
				--Update the MKContactSite
				INSERT INTO MKContactSite
				(
					ContactId,
					SiteId,
					Status,
					IsPrimarySite
				)
				VALUES
				(
					@Id,
					@PrimarySiteId,
					@Status,
					1
				)		
			
			END
			ELSE 
				UPDATE MKContactSite 
				SET IsPrimarySite = 1
				WHERE ContactId = @Id AND SiteId = @PrimarySiteId
			
		END	
	END
END


GO
PRINT 'Altering procedure Post_GetPost'
GO
ALTER PROCEDURE [dbo].[Post_GetPost]
(
	@ApplicationId		uniqueIdentifier,		
	@Id					uniqueIdentifier = null,		
	@PostMonth			varchar(20) = null,
	@PostYear			varchar(20) = null,
	@PageSize			int = null,
	@PageNumber			int = null,
	@FolderId			uniqueIdentifier = null,
	@BlogId				uniqueIdentifier = null,
	@Status				int = null,
	@CategoryId			uniqueIdentifier = null,
	@Label				nvarchar(1024) = null,
	@Author				nvarchar(1024) = null,
	@IncludeVersion		bit = null,
	@VersionId			uniqueidentifier=null,
	@IgnoreSite			bit = null,
	@ShowOnlyApprovedComments bit = null
)
AS
BEGIN
	DECLARE @IsSticky bit
	IF @Status = 5
		SET @IsSticky = 1

	IF @ShowOnlyApprovedComments IS NULL
		SET @ShowOnlyApprovedComments = 0


	DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)
	;WITH PostIds AS(
		SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo, 
			COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId
		FROM BLPost 
		WHERE (@Id IS NULL OR Id = @Id) AND
			(Status != dbo.GetDeleteStatus()) AND
			(@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND
			(@Status IS NULL OR Status = @Status) AND
			(@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND 
			(@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND 
			(@FolderId IS NULL OR HSId = @FolderId) AND 
			(@BlogId IS NULL OR BlogId = @BlogId) AND
			(@IsSticky IS NULL OR IsSticky = @IsSticky) AND
			(@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND
			(@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND
			(@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))
	)
	
	INSERT INTO @tbPostIds
	SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds
	WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR 
		RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))
	
	SELECT A.Id,			
		A.ApplicationId,
		A.Title,
		A.Description,
		A.ShortDescription,
		A.AuthorName,
		A.AuthorEmail,
		A.Labels,
		A.AllowComments,
		A.PostDate,
		A.Location,
		A.Status,
		A.IsSticky,
		A.CreatedBy,
		A.CreatedDate,
		A.ModifiedBy,
		A.ModifiedDate,
		A.FriendlyName,
		A.PostContentId,
		A.ContentId,
		A.ImageUrl,
		A.TitleTag,
		A.H1Tag,
		A.KeywordMetaData,
		A.DescriptiveMetaData,
		A.OtherMetaData,
		B.BlogListPageId,
		B.Id AS BlogId,
		A.WorkflowState,
		A.PublishDate,
		A.PublishCount,
		A.INWFFriendlyName,
		A.HSId AS MonthId
	FROM BLPost A 
		JOIN @tbPostIds T ON A.Id = T.Id
		JOIN BLBlog B ON A.BlogId = B.Id
	ORDER BY T.RowNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.Text, 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		sDir.VirtualPath As FolderPath
	FROM COContent C
		INNER JOIN @tbPostIds T ON C.Id = T.PostContentId
		INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		
	SELECT Top 1 TotalRows FROM @tbPostIds

	SELECT A.Id,
		ApplicationId,
		Title,
		Description,
		BlogNodeType,
		MenuId,
		ContentDefinitionId,
		ShowPostDate,
		DisplayAuthorName,
		EmailBlogOwner,
		ApproveComments,
		RequiresUserInfo,
		Status,
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		ModifiedDate,
		BlogListPageId,
		BlogDetailPageId,
		ContentDefinitionId
	FROM BLBlog A 
		INNER JOIN @tbPostIds T ON T.BlogId = A.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
	
	SELECT T.Id,
		I.Title,
		C.TaxonomyId
	FROM COTaxonomyObject C
		JOIN @tbPostIds T ON T.Id = C.ObjectId
		JOIN COTaxonomy I ON I.Id = C.TaxonomyId

	IF (@ShowOnlyApprovedComments = 1)
	BEGIN
		SELECT Count(*) AS NoOfComments,
			T.Id
		FROM BLComments C
			JOIN @tbPostIds T ON C.ObjectId = T.Id
		WHERE C.Status = 4 --For approved comments
		GROUP BY T.Id
	END
	ELSE
	BEGIN
		SELECT Count(*) AS NoOfComments,
			T.Id
		FROM BLComments C
			JOIN @tbPostIds T ON C.ObjectId = T.Id
		GROUP BY T.Id
	END

	IF @IncludeVersion = 1
	BEGIN
		DECLARE @tbVersionIds TABLE(Id uniqueidentifier)
		IF @VersionId IS NULL
		BEGIN
			;WITH CTEVersion AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,
					V.Id FROM VEVersion V
					JOIN BLPost T On T.Id = V.ObjectId
			)
			INSERT INTO @tbVersionIds
			SELECT Id FROM CTEVersion WHERE VerRank = 1
		END
		ELSE
		BEGIN
			INSERT INTO @tbVersionIds VALUES (@VersionId)
		END

		SELECT V.Id AS VersionId,
			V.ObjectId AS PostId,
			V.XMLString
		FROM VEVersion V
			JOIN @tbVersionIds T On T.Id = V.Id

		SELECT 
			V.PostVersionId AS VersionId,
			C.ContentId,
			C.XMLString
		FROM VEPostContentVersion V
			JOIN @tbVersionIds T On T.Id = V.PostVersionId
			JOIN VEContentVersion C ON C.Id = V.ContentVersionId
	END
END


GO
PRINT 'Creating procedure Template_GetAllTemplates'
GO
IF (OBJECT_ID('Template_GetAllTemplates') IS NOT NULL)
DROP PROCEDURE Template_GetAllTemplates
GO
CREATE PROCEDURE [dbo].[Template_GetAllTemplates]
(
	@Id				uniqueIdentifier=null ,
	@ApplicationId	uniqueidentifier=null,
	@Title			nvarchar(256)=null,
	@FileName		nvarchar(4000)=null,
	@Status			int=null,
	@PageIndex		int=1,
	@PageSize		int=null,
	@Type			int=null 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueidentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN

		SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type,ApplicationId,PagePart,LayoutTemplateId,ZonesXml,
		(SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO) StyleId,
		(SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO) ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
		[dbo].[GetParentDirectoryVirtualPath](S.Id) as ParentVirtualPath, CodeFile, H.ParentId
		FROM SITemplate S 
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		INNER JOIN HSStructure H on S.Id = H.Id
		WHERE	S.Id = isnull(@Id, S.Id) and
			isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
			isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
			 (@ApplicationId IS NULL OR S.ApplicationId=@ApplicationId OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) and      
			S.Type=isnull(@Type,S.Type) and
			S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
			AND (S.IsNotShared IS NULL OR S.IsNotShared = 0 )  
	END
	
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, 
				ModifiedDate, FileName,ImageURL,Type,ApplicationId,
				(SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO ) ScriptId,
				[dbo].[GetParentDirectoryVirtualPath](Id) as ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml
			FROM SITemplate S
			WHERE S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				isnull(upper(S. FileName),'') = isnull(upper(@FileName), isnull(upper(S.FileName),'')) and
				isnull(S.ApplicationId,@EmptyGUID) = isnull(@ApplicationId, isnull(S.ApplicationId,@EmptyGUID)) and
				S.Type=isnull(@Type,S.Type) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

		SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type,ApplicationId, StyleId,ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, H.ParentId
		FROM ResultEntries S
		INNER JOIN HSStructure H on S.Id = H.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END
GO
PRINT 'Altering procedure Product_SaveProductAndAttributes'
GO
ALTER PROCEDURE [dbo].[Product_SaveProductAndAttributes]
(
	@Id uniqueidentifier output ,
	@ProductCode varchar(50),
	@Title	nvarchar(255),
	@ShortDescription nvarchar(255)= '',
	@LongDescription nvarchar(max)= '',
	@Description nvarchar(200),
    @Keywords nvarchar(900)= '',
    @Status int,
    @ProductTypeId uniqueidentifier,
    @ApplicationId uniqueidentifier,
    @ModifiedBy uniqueidentifier,	
    @ProductAttributes xml,
    @SystemAttributes xml,
	@UrlFriendlyTitle nvarchar(255)=null
)
as
begin
	Declare @CurrentDate datetime
	set @CurrentDate = getdate() 

	if (@UrlFriendlyTitle is null OR @UrlFriendlyTitle ='')
	Begin
		set @UrlFriendlyTitle = dbo.MakeFriendlyProductTitle(@Title)
	End

	if(@Id is null OR @Id = dbo.GetEmptyGUID())
	Begin		
		Set @Id = newid()
        insert into PRProduct 
			([Id]
			,[ProductIDUser]
			,[Title], UrlFriendlyTitle
			,[ShortDescription]
			,[LongDescription]
			,[Description]
			,[Keyword]
			,[Status]
			,[ProductTypeID]
			,[SiteId]
			,[CreatedDate]
			,[CreatedBy]
			)
		 values
			(@Id,
			@ProductCode,
			@Title, @UrlFriendlyTitle,
			@ShortDescription,
			@LongDescription,
			@Description,
			@Keywords,
			@Status,
			@ProductTypeId,
			@ApplicationId,
			@CurrentDate,
			@ModifiedBy)

		
		declare @defaultCategoryId uniqueidentifier
		declare @prCategorySequence int
		select @defaultCategoryId = Id from PRCategory where Title='Unassigned'
		select @prCategorySequence = max(Sequence) from PRProductCategory  
		
		insert into PRProductCategory (Id,ProductId, CategoryId,Sequence) 
					values(newid(), @Id,@defaultCategoryId,@prCategorySequence+1)
		
	end
	else
	begin
		
		update PRProduct 
		set	 [ProductIDUser]=@ProductCode
			,[Title] = @Title
			,[UrlFriendlyTitle] = @UrlFriendlyTitle
			,[ShortDescription] =@ShortDescription
			,[LongDescription] =@LongDescription
			,[Description] =@Description
			,[Keyword] =@Keywords
			,[Status] =@Status
			,[ProductTypeID] =@ProductTypeId
			,[SiteId]=@ApplicationId
			,[ModifiedDate]=@CurrentDate
			,[ModifiedBy]=@ModifiedBy
		where Id =@Id
		
	
	End
		
	-- Updating the System Attribtues

	Declare @tempProductAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier)

	Insert into @tempProductAttribute(AttributeId)
	Select tab.col.value('text()[1]','uniqueidentifier')
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	UNION ALL
	Select T.c.value('(AttributeId)[1]','uniqueidentifier')
	From @ProductAttributes.nodes('//AttributeItem') T(c)
	
	if (select count(*) from @tempProductAttribute)>0
	begin
		delete from PRProductAttributeValue where ProductId =@Id
		delete from PRProductAttribute where ProductId =@Id and IsSKULevel=0
	end

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel)
	Select newid(),@Id,AttributeId,Sequence,0
	From @tempProductAttribute
	

	Insert into PRProductAttributeValue(Id,ProductAttributeId,ProductId,AttributeId,AttributeEnumId,Value,CreatedDate,CreatedBy,Status)
	SELECT newid(),  
		PA.Id,
		@Id,
       T.c.value('(AttributeId)[1]','uniqueidentifier'),
       CASE WHEN T.c.value('(AttributeEnumId)[1]', 'uniqueidentifier') = '00000000-0000-0000-0000-000000000000' THEN NULL ELSE T.c.value('(AttributeEnumId)[1]', 'uniqueidentifier') END,    
       T.c.value('(Value)[1]','varchar(max)'),  
       @CurrentDate,  
       @ModifiedBy,  
       T.c.value('(Status)[1]','int')  
	FROM @ProductAttributes.nodes('//AttributeItem') T(c)
	INNER JOIN PRProductAttribute PA ON PA.ProductId = @Id AND AttributeId =  T.c.value('(AttributeId)[1]','uniqueidentifier')
	
	Insert into PRProductAttributeValue(Id,ProductAttributeId,ProductId,AttributeId,AttributeEnumId,Value,CreatedDate,CreatedBy,Status)
	Select	
		newid(),
		PA.Id,
		@Id,
		tab.col.value('(key/guid/text())[1]','uniqueidentifier'),
		E.Id,
		E.Value,
		@CurrentDate,
		@ModifiedBy	,
		dbo.GetActiveStatus()
		From @SystemAttributes.nodes('dictionary/SerializableDictionary/item') tab(col)
		Inner Join ATAttributeEnum E 
			ON tab.col.value('(key/guid/text())[1]','uniqueidentifier') = E.AttributeID
		
		INNER JOIN PRProductAttribute PA ON PA.ProductId = @Id AND PA.AttributeId = tab.col.value('(key/guid/text())[1]','uniqueidentifier')	

		Where (tab.col.value('(value/boolean/text())[1]','varchar(5)')= 'true' AND E.Code='1')
			OR ((tab.col.value('(value/boolean/text())[1]','varchar(5)'))= 'false' AND E.Code='0')
end

GO
PRINT 'Altering procedure Membership_UpdateUser'
GO
ALTER PROCEDURE [dbo].[Membership_UpdateUser]
--********************************************************************************
-- PARAMETERS
	@ProductId			  uniqueidentifier = NULL, 
	@ApplicationId		  uniqueidentifier,
	@UserId				  uniqueidentifier,	
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @IsApproved           bit,
    @UniqueEmail          int,
	@UserProfile		  xml=NULL,
	@EmailNotification						[bit]= NULL,
	@ExpiryDate								[datetime]= NULL,
	@TimeZone								[nvarchar](100)= NULL,
	@FirstName								[nvarchar](256)= NULL,
	@MiddleName								[nvarchar](256)= NULL,
	@LastName								[nvarchar](256)= NULL,
	@ReportRangeSelection					[varchar](50)= NULL,
	@ReportStartDate						[datetime] =NULL,
	@ReportEndDate							[datetime] =NULL,
	@BirthDate								[datetime]= NULL,
	@CompanyName							[nvarchar](1024)= NULL,
	@Gender									[nvarchar](50)= NULL,
	@HomePhone								[varchar](50) =NULL,
	@MobilePhone							[varchar](50)= NULL,
	@OtherPhone								[varchar](50) =NULL,
	@ImageId				uniqueidentifier = null,
	@DashboardData			xml,
	@UpdateProfile	 bit=1
--********************************************************************************
    
AS
BEGIN
    DECLARE	@CurrentTimeUtc			datetime

	SET @CurrentTimeUtc = GetUtcDate()
	
	IF (@UserId IS NULL AND @UserName IS NOT NULL)
	BEGIN
		SET @UserId = dbo.USGetUserIdForSite(@UserName , @ApplicationId)
	END

	--User not found
    IF (@UserId IS NULL)
	BEGIN
		DECLARE @ErrMsg nvarchar(256)
		SET @ErrMsg = ISNULL(@UserName,'UserId')
        RAISERROR ('NOTEXISTS||%s', 16, 1, @ErrMsg)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	-- Unique Email is enabled
    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.USMembership m WITH (UPDLOCK, HOLDLOCK) inner join dbo.USSiteUser us WITH (UPDLOCK, HOLDLOCK) on m.UserId=us.UserId inner join dbo.USUser u WITH (UPDLOCK, HOLDLOCK) on us.UserId=u.Id
                    WHERE SiteId = @ApplicationId  AND @UserId <> m.UserId AND LoweredEmail = LOWER(@Email) and u.status <>dbo.GetDeleteStatus()))  
        BEGIN
            RAISERROR ('ISEXISTS||%s', 16, 1, 'Email')
			RETURN dbo.GetBusinessRuleErrorCode()
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    IF @ExpiryDate IS NULL
		SET @ExpiryDate = '9999-12-31'
    
    if(@ReportRangeSelection is null)
    begin 
    set @ReportRangeSelection = 'MonthToDate'
    set @ReportStartDate= GETDATE() 
    set @ReportEndDate= GETDATE()
    end
    
    IF @UpdateProfile =1   
    UPDATE dbo.USUser WITH (ROWLOCK)
    SET
         LastActivityDate = @CurrentTimeUtc --Last Activity date
             ,EmailNotification=@EmailNotification
	,ExpiryDate=@ExpiryDate
	,TimeZone=@TimeZone
	,FirstName=@FirstName
	,MiddleName=@MiddleName
	,LastName=@LastName
	,ReportRangeSelection=@ReportRangeSelection
	,ReportStartDate=@ReportStartDate
	,ReportEndDate=@ReportEndDate
	,BirthDate=@BirthDate
	,CompanyName=@CompanyName
	,Gender=@Gender
	,HomePhone=@HomePhone
	,MobilePhone=@MobilePhone
	,OtherPhone =@OtherPhone
	,ImageId	=@ImageId
	,DashboardData = @DashboardData
	,ModifiedDate = @CurrentTimeUtc
    WHERE
       @UserId = Id
	ELSE
	UPDATE dbo.USUser WITH (ROWLOCK)
    SET
         LastActivityDate = @CurrentTimeUtc --Last Activity date
    WHERE
       @UserId = Id

    IF( @@ERROR <> 0 )
	BEGIN
        RAISERROR ('DBERROR||%s', 16, 1, 'Update')
		GOTO Cleanup
	END

	--update membership table
    UPDATE dbo.USMembership WITH (ROWLOCK)
    SET
         Email            = ISNULL(@Email,''),
         LoweredEmail     = LOWER(ISNULL(@Email,'')),
         IsApproved       = ISNULL(@IsApproved,IsApproved),
         LastLoginDate    = @CurrentTimeUtc -- Last login date
    WHERE
       @UserId = UserId


    IF( @@ERROR <> 0 )
	BEGIN
		RAISERROR ('DBERROR||%s', 16, 1, 'Update')
		GOTO Cleanup
	END
	
	--IF(@ProductId IS NOT NULL)
	--BEGIN
	--	UPDATE dbo.USSiteUser WITH (ROWLOCK)
	--	SET
	--		 ProductId = @ProductId
	--	WHERE
	--	   @UserId = UserId
	--END
	
	--IF( @@ERROR <> 0 )
	--BEGIN
	--	RAISERROR ('DBERROR||%s', 16, 1, 'Update')
	--	GOTO Cleanup
	--END
	
--  Update the member profile data using the xml parameter
	IF (@UserProfile IS NOT NULL AND @UpdateProfile =1  )
	BEGIN
		EXEC [ProfileManager_UpdateUserProfileFromXml] @UserId,@UserProfile,@CurrentTimeUtc
	END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @@ROWCOUNT

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN dbo.GetDataBaseErrorCode()
END
--***********************************************************************************


--***********************************************************************************


GO
PRINT 'Altering procedure EmailAction_Save'
GO
ALTER PROCEDURE [dbo].[EmailAction_Save]  
(  
 @Id uniqueidentifier out,  
 @SendId uniqueidentifier,  
 @ActionType int,  
 @ActionDate datetime,  
 @LinkType int,  
 @LinkContainerId uniqueidentifier = null,  
 @ControlId nvarchar(255) = null,  
 @TargetId uniqueidentifier,  
 @SessionInfo_Id uniqueidentifier = null,  
 @ApplicationId uniqueidentifier,  
 @MicroSiteId uniqueidentifier=null  
)  
AS  
Begin  
  
  
if(@MicroSiteId <> dbo.GetEmptyGUID()and @MicroSiteId is not null)  
  set @ApplicationId = @MicroSiteId  
    
 set @ActionDate = getutcdate()  
 If not exists(select * from MKEmailSendActions Where Id = @Id)  
 Begin  
  IF @Id is null  
   Set @Id = newid()  
  -- insert  
  Insert Into MKEmailSendActions(  
   Id,  
   SendId,  
   ActionType,  
   ActionDate,  
   LinkType,  
   LinkContainerId,  
   ControlId,  
   TargetId,  
   SessionInfo_Id  
   )  
  Values (  
   @Id,  
   @SendId,  
   @ActionType,  
   @ActionDate,  
   @LinkType,  
   @LinkContainerId,  
   @ControlId,  
   @TargetId,  
   @SessionInfo_Id  
   )  
 End  
 Else  
 Begin  
  -- update  
  Update MKEmailSendActions Set  
   ActionType = @ActionType,  
   ActionDate = @ActionDate,  
   LinkType = @LinkType,  
   LinkContainerId = @LinkContainerId,  
   ControlId = @ControlId,  
   TargetId = @TargetId,  
   SessionInfo_Id = @SessionInfo_Id  
  Where Id = @Id  
 End  
  
 If @ActionType = 1  
 Begin  
  Update MKEmailSendLog Set  
   Opened = 1  
  Where Id = @SendId  
 End  
  
 If @ActionType = 2  
 Begin  
  Update MKEmailSendLog Set  
   Clicked = 1  
  Where Id = @SendId  
 End  
  
 If @ActionType = 3  
 Begin  
  Update MKEmailSendLog Set  
   Unsubscribed = 1  
  Where Id = @SendId  
 End  
  
 If @ActionType = 4  
 Begin  
  Update MKEmailSendLog Set  
   Bounced = 1  
  Where Id = @SendId  
 End  
  
 If @ActionType = 5  
 Begin  
  Update MKEmailSendLog Set  
   TriggeredWatch = 1  
  Where Id = @SendId  
 End  
  
End

GO
PRINT 'Altering procedure CampaignRunHistory_GetOverlayData'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetOverlayData](@CampaignId uniqueidentifier, @CampaignRunId uniqueidentifier)  
As  
Begin  
  
 select ESA.TargetId, dbo.GetEmptyGuid() as SendId,ESA.LinkContainerId,ControlId,   
 count(*) as LinkClicks from MKEmailSendActions ESA   
     inner join MKEmailSendLog ESL on ESA.SendId=ESL.Id  
 where ActionType=2 and   
   ESL.CampaignRunId=@CampaignRunId and ESL.CampaignId = @CampaignId  
 group by ESA.LinkContainerId,TargetId, ControlId  
  
 select sum(Sends) as Sends from MKCampaignRunHistory where Id = @CampaignRunId  
  
End

GO
PRINT 'Altering procedure CampaignRunHistory_GetContactsForLink'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistory_GetContactsForLink]    
 @CampaignRunId  uniqueidentifier,    
 @LinkContainerId uniqueidentifier,    
 @TargetId   uniqueidentifier,  
 @ControlId nvarchar(255) = null  
AS    
BEGIN    
 SET NOCOUNT ON;    
    
 SELECT DISTINCT ESL.UserId AS Id    
 FROM MKEmailSendActions AS ESA    
   INNER JOIN MKEmailSendLog AS ESL ON ESL.Id = ESA.SendId    
 WHERE (LinkContainerId = @LinkContainerId OR ControlId = @ControlId) AND    
   TargetId = @TargetId AND    
   SendId IN (    
    SELECT Id    
    FROM MKEmailSendLog    
    WHERE CampaignRunId = @CampaignRunId    
   )    
END

GO
PRINT 'Altering procedure Product_Save'
GO
ALTER PROCEDURE [dbo].[Product_Save]
(
	@ApplicationId uniqueidentifier,
	@ProductXml xml,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier,	
	@Id uniqueidentifier output
)
as
begin
	Declare @CurrentDate datetime
	Declare @strId char(36)
	set @strId = @ProductXml.value('(Product/Id)[1]','char(36)')
	set @CurrentDate = GetUTCDate() 

	if(@strId = '00000000-0000-0000-0000-000000000000' OR @strId='')
	Begin		
		Set @Id = newid()
		
        insert into PRProduct 
			(Id,
			 ProductIDUser,
			 Title, UrlFriendlyTitle,
			 ShortDescription,
			 Description,
			 ProductTypeID,
			 IsActive,
			 IsBundle,
			 SiteId,
			 CreatedDate,
			 CreatedBy,
			 ModifiedDate,
			 ModifiedBy,
			 Status)
		 values
			(@Id,
			 @ProductXml.value('(Product/ProductCode)[1]','nvarchar(50)'),
			 @ProductXml.value('(Product/Title)[1]','nvarchar(255)'),
			 @ProductXml.value('(Product/UrlFriendlyTitle)[1]','nvarchar(255)'),
			@ProductXml.value('(Product/ShortDescription)[1]','nvarchar(555)'),
			 @ProductXml.value('(Product/Description)[1]','nvarchar(max)'),
			 @ProductXml.value('(Product/ProductType/Id)[1]','uniqueidentifier'),
			 @ProductXml.value('(Product/IsActive)[1]','bit'),
			 @ProductXml.value('(Product/IsBundle)[1]','bit'),
			 @ApplicationId,
			 @CurrentDate,
		     @CreatedBy,
			 @CurrentDate,
		     @ModifiedBy,
			 @ProductXml.value('(Product/Status)[1]','int'))

		
		declare @defaultCategoryId uniqueidentifier
		declare @prCategorySequence int
		select @defaultCategoryId = Id from PRCategory where Title='Unassigned'
		select @prCategorySequence = max(Sequence) from PRProductCategory  
		
		insert into PRProductCategory (Id,ProductId, CategoryId,Sequence) 
					values(newid(), @Id,@defaultCategoryId,@prCategorySequence+1)
		
	end
	else
	begin
		set @Id= @ProductXml.value('(Product/Id)[1]','uniqueidentifier')
		
		update PRProduct 
		set	 Title=@ProductXml.value('(Product/Title)[1]','nvarchar(255)'),
			 UrlFriendlyTitle=@ProductXml.value('(Product/UrlFriendlyTitle)[1]','nvarchar(255)'),
			 ProductIDUser=@ProductXml.value('(Product/ProductCode)[1]','nvarchar(50)'),
			 ShortDescription=@ProductXml.value('(Product/ShortDescription)[1]','nvarchar(255)'),
			 Description=@ProductXml.value('(Product/Description)[1]','nvarchar(max)'),
			 ProductTypeID=@ProductXml.value('(Product/ProductType/Id)[1]','uniqueidentifier'),
			 IsActive=@ProductXml.value('(Product/IsActive)[1]','bit'),
			 IsBundle=@ProductXml.value('(Product/IsBundle)[1]','bit'),
			 SiteId=@ApplicationId,			
			 ModifiedDate =@CurrentDate,
			 ModifiedBy=@ModifiedBy,
			 Status = @ProductXml.value('(Product/Status)[1]','int')
		where Id =@Id
		
		delete from PRProductAttributeValue where ProductId =@Id
		delete from PRProductAttribute where ProductId =@Id and IsSKULevel=0
	End
		
	--Associate attributes to the Product and insert attribute values 
	declare @iCount int
	declare @i int
	declare @attributeId varchar(36)
    declare @ProductAttributeId uniqueidentifier		

	--process normal product attributes
	set @iCOunt = @ProductXml.value('count(/Product/Attributes/ProductAttribute)','int')
	set @i = 1
	
	while (@i <= @iCOunt)
	begin
		set  @attributeId = @ProductXml.value('(/Product/Attributes/ProductAttribute[sql:variable("@i")]/Id)[1]','uniqueidentifier')
		set @ProductAttributeId = newid()

	   insert into PRProductAttribute
		(Id, ProductId, AttributeId,Sequence,IsSKULevel)
	   Values( @ProductAttributeId,	@Id, @attributeId, @i, 0)		
		
			
		insert into PRProductAttributeValue
		(Id, ProductAttributeId, ProductId, AttributeId,AttributeEnumId,[Value],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
		SELECT newid(), @ProductAttributeId, 
			   @Id,
			   T.c.value('(AttributeId)[1]','uniqueidentifier'),		
			   CASE WHEN  T.c.value('(AttributeEnumId)[1]','uniqueidentifier') ='00000000-0000-0000-0000-000000000000' then NULL ELSE T.c.value('(AttributeEnumId)[1]','uniqueidentifier')END,		
			   T.c.value('(Value)[1]','varchar(50)'),
			   @CurrentDate,
			   @CreatedBy,
			   @CurrentDate,
			   @ModifiedBy,
			   T.c.value('(Status)[1]','varchar(50)')
		FROM @ProductXml.nodes('/Product/Attributes/ProductAttribute[sql:variable("@i")]/Values/AttributeItem') T(c)

		SET @i = @i + 1
	end

	--process system level product attributes
	set @iCOunt = @ProductXml.value('count(/Product/SystemAttributes/ProductAttribute)','int')
	set @i = 1
	
	while (@i <= @iCOunt)
	begin
		set  @attributeId = @ProductXml.value('(/Product/SystemAttributes/ProductAttribute[sql:variable("@i")]/Id)[1]','uniqueidentifier')
		set @ProductAttributeId = newid()

	   insert into PRProductAttribute
		(Id, ProductId, AttributeId,Sequence,IsSKULevel)
	   Values( @ProductAttributeId,	@Id, @attributeId, @i, 0)		
		
			
		insert into PRProductAttributeValue
		(Id, ProductAttributeId, ProductId, AttributeId,AttributeEnumId,[Value],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
		SELECT newid(), @ProductAttributeId, 
			   @Id,
			   T.c.value('(AttributeId)[1]','uniqueidentifier'),
			   CASE WHEN  T.c.value('(AttributeEnumId)[1]','uniqueidentifier') ='00000000-0000-0000-0000-000000000000' then NULL ELSE T.c.value('(AttributeEnumId)[1]','uniqueidentifier')END,		
			   T.c.value('(Value)[1]','varchar(50)'),
			   @CurrentDate,
			   @CreatedBy,
			   @CurrentDate,
			   @ModifiedBy,
			   T.c.value('(Status)[1]','varchar(50)')
		FROM @ProductXml.nodes('/Product/SystemAttributes/ProductAttribute[sql:variable("@i")]/Values/AttributeItem') T(c)

		SET @i = @i + 1
	end

end
GO
PRINT 'Altering procedure Site_CreateMicrositeDataForMarketier'
GO
ALTER PROCEDURE [dbo].[Site_CreateMicrositeDataForMarketier]
(
	@MicroSiteId uniqueidentifier
	--@PageImportOptions
)
AS
BEGIN

--Pointer used for text / image updates. This might not be needed, but is declared here just in case
	DECLARE @pv binary(16)
	DECLARE @ApplicationId uniqueidentifier
	SET @ApplicationId = @MicroSiteId

	DECLARE @CommerceProductId uniqueidentifier
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'

	DECLARE @MarketierProductId uniqueidentifier
	SET @MarketierProductId='CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E'

	--DECLARE @UnsubscribePageId  uniqueidentifier
	--SET @UnsubscribePageId='18F27A17-03B2-4482-B375-3C48577B5702'

	--if not exists (select 1 from COContentStructure where VirtualPath='~/Content Library/MarketierLibrary' and SiteId=@MicroSiteId)
	--begin
	BEGIN TRANSACTION

	--========== Creating ManageContentLibrary folder for marketier
		DECLARE @ContentLibraryId uniqueidentifier, @MarketierContentLibrary uniqueidentifier, @unassignedFoldeId uniqueidentifier
		SELECT @ContentLibraryId = Id from COContentStructure where Title = 'Content Library' AND SiteId=@MicroSiteId
		SET @ContentLibraryId = Isnull(@ContentLibraryId, @ApplicationId)
		SET @MarketierContentLibrary = newid()--'E196A2C5-C070-4904-B93D-DFECED4ADABF'
		SET @unassignedFoldeId = newid() --'B843F49B-1427-48D9-A439-CBD265B36077'


		
			declare @p1 xml
			set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="' + cast(@MarketierContentLibrary as char(36)) + '" Title="MarketierLibrary" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@ContentLibraryId as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
			declare @p5 nvarchar(max)
			set @p5=N'<SISiteDirectory Id="'+ cast(@MarketierContentLibrary as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary" Attributes="0"/>'
			declare @p6 nvarchar(256)
			set @p6=N'ACME'
			IF NOT EXISTS (Select 1 from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)
			exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@ContentLibraryId,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
			ELSE
				SET @MarketierContentLibrary = (Select top 1 Id  from COContentStructure Where Title ='MarketierLibrary' And SiteId=@MicroSiteId)

		  --UPDATING SITE SETTINGS FOR MARKETIER CONTENT LIBRARY
		  declare @MarketierContentLibSettingTypeId int 
		  SELECT @MarketierContentLibSettingTypeId = Id  FROM STSettingType Where Name = 'MarketierContentLibraryId'

		  IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		  BEGIN
			IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierContentLibSettingTypeId) )
			BEGIN
				INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
				VALUES (@MicroSiteId,@MarketierContentLibSettingTypeId,@MarketierContentLibrary)
			END 
			ELSE
			BEGIN
				update [dbo].[STSiteSetting] set Value = @MarketierContentLibrary where SettingTypeId=@MarketierContentLibSettingTypeId AND SiteId=@MicroSiteId
			END
		 END
		
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
		--declare @p1 xml
		set @p1=convert(xml,N'<SiteDirectoryCollection><SiteDirectory Id="'+ cast(@unassignedFoldeId as char(36)) + '" Title="Unassigned" Description="" CreatedBy="00000000-0000-0000-0000-000000000000" CreatedDate="" ModifiedBy="00000000-0000-0000-0000-000000000000" ModifiedDate="" Status="0" CreatedByFullName="" ModifiedByFullName="" ObjectTypeId="7" PhysicalPath="" VirtualPath="" Attributes="0" ThumbnailImagePath="" FolderIconPath="" Exists="0" Size="0" NumberOfFiles="0" CompleteURL="" IsSystem="True" IsMarketierDir="True" AccessRoles="" ParentId="' + cast(@MarketierContentLibrary as char(36)) + '" Lft="1" Rgt="2"/></SiteDirectoryCollection>')
		--declare @p5 nvarchar(max)
		set @p5=N'<SISiteDirectory Id="'+cast(@unassignedFoldeId as char(36)) +'" VirtualPath="~/Content Library/MarketierLibrary/Unassigned" Attributes="0"/>'
		--declare @p6 nvarchar(256)
		set @p6=N'ACME'
		IF NOT EXISTS (Select 1 from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		exec SqlDirectoryProvider_Save @XmlString=@p1,@ParentId=@MarketierContentLibrary,@ApplicationId=@MicroSiteId,@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MicroSiteId=null,@VirtualPath=@p5 output,@SiteName=@p6 output
		ELSE 
			SET @unassignedFoldeId =( Select top 1 Id   from COContentStructure Where Title ='Unassigned' And SiteId=@MicroSiteId)
		--select @p5, @p6
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	--============= Creating Maketier PageMapNodes 
		DECLARE @MarkierPageMapNodeRootId uniqueidentifier, @MarkierPageMapNodeId uniqueidentifier
		DECLARE @pmnXml xml
		DECLARE	@return_value int

		set @MarkierPageMapNodeRootId = newid() 
		print @MarkierPageMapNodeRootId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" description="MARKETIERPAGES" displayTitle="MARKETIERPAGES" friendlyUrl="MARKETIERPAGES" menuStatus="1" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@ApplicationId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  1:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels="" locationIdentifier="MARKETIERPAGES"/>'

		IF NOT EXISTS (Select 1 from PageMapNode Where description ='MARKETIERPAGES' AND displayTitle='MARKETIERPAGES' 
		AND friendlyUrl='MARKETIERPAGES' And SiteId=@MicroSiteId)
		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeRootId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @ApplicationId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		
		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
 
		set @MarkierPageMapNodeId = newid() 
		
		Select @MarkierPageMapNodeRootId = PageMapNodeId from PageMapNode Where description ='MARKETIERPAGES' AND displayTitle='MARKETIERPAGES' 
		AND friendlyUrl='MARKETIERPAGES' And SiteId=@MicroSiteId

		set @pmnXml= '<pageMapNode id="' + convert(nvarchar(36),@MarkierPageMapNodeId) +'" description="MARKETIERCAMPAIGN" displayTitle="MARKETIERCAMPAIGN" friendlyUrl="MARKETIERCAMPAIGN" menuStatus="0" targetId="00000000-0000-0000-0000-000000000000" parentId="' + convert(nvarchar(36),@MarkierPageMapNodeRootId) +'" target="0" targetUrl="" createdBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" createdDate="Feb  9 2010  1:18PM" modifiedBy="C8C20320-0DB4-41C4-81AC-7717E95B273A" modifiedDate="Feb  9 2010  6:18PM" propogateWorkFlow="True" inheritWorkFlow="False" propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="False" inheritRoles="False" roles="" securityLevels=""/>'
		
		IF NOT EXISTS (Select 1 from PageMapNode Where description ='MARKETIERCAMPAIGN' AND displayTitle='MARKETIERCAMPAIGN' 
		AND friendlyUrl='MARKETIERCAMPAIGN' And SiteId=@MicroSiteId)
		EXEC @return_value = [dbo].[PageMapNode_Save]
					@Id=@MarkierPageMapNodeId,
					@SiteId = @ApplicationId,
					@ParentNodeId = @MarkierPageMapNodeRootId,
					@PageMapNode = @pmnXml,
					@CreatedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedBy='798613EE-7B85-4628-A6CC-E17EE80C09C5',
					@ModifiedDate=null
		SELECT 'Return Value' = @return_value


		IF @@ERROR<>0 
		Begin
			ROLLBACK TRANSACTION
			GOTO QuitWithErrors
		END

		--UPDATING SITE SETTINGS FOR MARKETIER Campaign Page map node id
		declare @MarketierPageMapNodeSettingTypeId int 
		SELECT @MarketierPageMapNodeSettingTypeId = Id  FROM STSettingType Where Name = 'CampaignPageMapNodeId'
		
		Select @MarkierPageMapNodeId = PageMapNodeId from PageMapNode Where description ='MARKETIERCAMPAIGN' AND displayTitle='MARKETIERCAMPAIGN' 
		AND friendlyUrl='MARKETIERCAMPAIGN' And SiteId=@MicroSiteId

		IF (@MarketierContentLibSettingTypeId IS NOT NULL)
		BEGIN
		IF ( NOT EXISTS (SELECT 1 FROM STSiteSetting WHERE SiteId=@MicroSiteId AND SettingTypeId=@MarketierPageMapNodeSettingTypeId) )
		BEGIN
			INSERT INTO STSiteSetting (SiteId,SettingTypeId,Value)
			VALUES (@MicroSiteId,@MarketierPageMapNodeSettingTypeId,@MarkierPageMapNodeId)
		END 
		ELSE
		BEGIN
			update [dbo].[STSiteSetting] set Value = @MarkierPageMapNodeId where SettingTypeId=@MarketierPageMapNodeSettingTypeId AND SiteId=@MicroSiteId
		END
		END
	--end
	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END

	--===== Updaging existing commerce group 
		-- Before Commerce custom settings group was 2(i.e Custom) and now it changed to 4 (i.e CommerceCustomSettings)
		--update STSettingType set SettingGroupId=(select Id from STSettingGroup where Name='CommerceCustomSettings') where SettingGroupId=2

	IF @@ERROR<>0 
	Begin
		ROLLBACK TRANSACTION
		GOTO QuitWithErrors
	END
	
IF @@ERROR<>0 
Begin
	ROLLBACK TRANSACTION
	GOTO QuitWithErrors
END
ELSE
BEGIN
	COMMIT TRANSACTION
END
-- Creating default email group
Declare @CreatedBy UNIQUEIDENTIFIER
SET @CreatedBy = (select top 1 Id from USUser Where UserName like 'IAppsSystemUser')

IF NOT EXISTS(Select 1 from MKCampaignGroup Where Name ='My first email group' and ApplicationId=@MicroSiteId)
INsert into MKCampaignGroup(Id,Name,CreatedBy,CreatedDate,ApplicationId)
Select newid(),'My first email group',@createdBy,getdate(),@MicroSiteId


QuitWithErrors:

END

GO

PRINT 'Altering procedure Product_Save'
GO
ALTER PROCEDURE [dbo].[Product_Save]
(
	@ApplicationId uniqueidentifier,
	@ProductXml xml,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier,	
	@Id uniqueidentifier output
)
as
begin
	Declare @CurrentDate datetime
	Declare @strId char(36)
	set @strId = @ProductXml.value('(Product/Id)[1]','char(36)')
	set @CurrentDate = GetUTCDate() 

	if(@strId = '00000000-0000-0000-0000-000000000000' OR @strId='')
	Begin		
		Set @Id = newid()
		
        insert into PRProduct 
			(Id,
			 ProductIDUser,
			 Title, UrlFriendlyTitle,
			 ShortDescription,
			 Description,
			 ProductTypeID,
			 IsActive,
			 IsBundle,
			 SiteId,
			 CreatedDate,
			 CreatedBy,
			 ModifiedDate,
			 ModifiedBy,
			 Status)
		 values
			(@Id,
			 @ProductXml.value('(Product/ProductCode)[1]','nvarchar(50)'),
			 @ProductXml.value('(Product/Title)[1]','nvarchar(255)'),
			 @ProductXml.value('(Product/UrlFriendlyTitle)[1]','nvarchar(255)'),
			@ProductXml.value('(Product/ShortDescription)[1]','nvarchar(555)'),
			 @ProductXml.value('(Product/Description)[1]','nvarchar(max)'),
			 @ProductXml.value('(Product/ProductType/Id)[1]','uniqueidentifier'),
			 @ProductXml.value('(Product/IsActive)[1]','bit'),
			 @ProductXml.value('(Product/IsBundle)[1]','bit'),
			 @ApplicationId,
			 @CurrentDate,
		     @CreatedBy,
			 @CurrentDate,
		     @ModifiedBy,
			 @ProductXml.value('(Product/Status)[1]','int'))

		
		declare @defaultCategoryId uniqueidentifier
		declare @prCategorySequence int
		select @defaultCategoryId = Id from PRCategory where Title='Unassigned'
		select @prCategorySequence = max(Sequence) from PRProductCategory  
		
		insert into PRProductCategory (Id,ProductId, CategoryId,Sequence) 
					values(newid(), @Id,@defaultCategoryId,@prCategorySequence+1)
		
	end
	else
	begin
		set @Id= @ProductXml.value('(Product/Id)[1]','uniqueidentifier')
		
		update PRProduct 
		set	 Title=@ProductXml.value('(Product/Title)[1]','nvarchar(255)'),
			 UrlFriendlyTitle=@ProductXml.value('(Product/UrlFriendlyTitle)[1]','nvarchar(255)'),
			 ProductIDUser=@ProductXml.value('(Product/ProductCode)[1]','nvarchar(50)'),
			 ShortDescription=@ProductXml.value('(Product/ShortDescription)[1]','nvarchar(255)'),
			 Description=@ProductXml.value('(Product/Description)[1]','nvarchar(max)'),
			 ProductTypeID=@ProductXml.value('(Product/ProductType/Id)[1]','uniqueidentifier'),
			 IsActive=@ProductXml.value('(Product/IsActive)[1]','bit'),
			 IsBundle=@ProductXml.value('(Product/IsBundle)[1]','bit'),
			 SiteId=@ApplicationId,			
			 ModifiedDate =@CurrentDate,
			 ModifiedBy=@ModifiedBy,
			 Status = @ProductXml.value('(Product/Status)[1]','int')
		where Id =@Id
		
		delete from PRProductAttributeValue where ProductId =@Id
		delete from PRProductAttribute where ProductId =@Id and IsSKULevel=0
	End
		
	--Associate attributes to the Product and insert attribute values 
	declare @iCount int
	declare @i int
	declare @attributeId varchar(36)
    declare @ProductAttributeId uniqueidentifier		

	--process normal product attributes
	set @iCOunt = @ProductXml.value('count(/Product/Attributes/ProductAttribute)','int')
	set @i = 1
	
	while (@i <= @iCOunt)
	begin
		set  @attributeId = @ProductXml.value('(/Product/Attributes/ProductAttribute[sql:variable("@i")]/Id)[1]','uniqueidentifier')
		Select @ProductAttributeId = Id
			FROM PRProductAttribute 
			Where ProductId=@Id And AttributeId=@attributeId And IsSKULevel=0
			IF @ProductAttributeId is null
			BEGIN
				set @ProductAttributeId = newid()
			   insert into PRProductAttribute
				(Id, ProductId, AttributeId,Sequence,IsSKULevel)
			   Values( @ProductAttributeId,	@Id, @attributeId, @i, 0)		
		END

	   insert into PRProductAttribute
		(Id, ProductId, AttributeId,Sequence,IsSKULevel)
	   Values( @ProductAttributeId,	@Id, @attributeId, @i, 0)		
		
			
		insert into PRProductAttributeValue
		(Id, ProductAttributeId, ProductId, AttributeId,AttributeEnumId,[Value],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
		SELECT newid(), @ProductAttributeId, 
			   @Id,
			   T.c.value('(AttributeId)[1]','uniqueidentifier'),		
			   CASE WHEN  T.c.value('(AttributeEnumId)[1]','uniqueidentifier') ='00000000-0000-0000-0000-000000000000' then NULL ELSE T.c.value('(AttributeEnumId)[1]','uniqueidentifier')END,		
			   T.c.value('(Value)[1]','varchar(50)'),
			   @CurrentDate,
			   @CreatedBy,
			   @CurrentDate,
			   @ModifiedBy,
			   T.c.value('(Status)[1]','varchar(50)')
		FROM @ProductXml.nodes('/Product/Attributes/ProductAttribute[sql:variable("@i")]/Values/AttributeItem') T(c)

		SET @i = @i + 1
	end

	--process system level product attributes
	set @iCOunt = @ProductXml.value('count(/Product/SystemAttributes/ProductAttribute)','int')
	set @i = 1
	
	while (@i <= @iCOunt)
	begin
		set  @attributeId = @ProductXml.value('(/Product/SystemAttributes/ProductAttribute[sql:variable("@i")]/Id)[1]','uniqueidentifier')
		set @ProductAttributeId = newid()

	   insert into PRProductAttribute
		(Id, ProductId, AttributeId,Sequence,IsSKULevel)
	   Values( @ProductAttributeId,	@Id, @attributeId, @i, 0)		
		
			
		insert into PRProductAttributeValue
		(Id, ProductAttributeId, ProductId, AttributeId,AttributeEnumId,[Value],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)
		SELECT newid(), @ProductAttributeId, 
			   @Id,
			   T.c.value('(AttributeId)[1]','uniqueidentifier'),
			   CASE WHEN  T.c.value('(AttributeEnumId)[1]','uniqueidentifier') ='00000000-0000-0000-0000-000000000000' then NULL ELSE T.c.value('(AttributeEnumId)[1]','uniqueidentifier')END,		
			   T.c.value('(Value)[1]','varchar(50)'),
			   @CurrentDate,
			   @CreatedBy,
			   @CurrentDate,
			   @ModifiedBy,
			   T.c.value('(Status)[1]','varchar(50)')
		FROM @ProductXml.nodes('/Product/SystemAttributes/ProductAttribute[sql:variable("@i")]/Values/AttributeItem') T(c)

		SET @i = @i + 1
	end

end
GO


PRINT 'Rebuilding Contact table with User data'
GO
 EXEC dbo.ContactDto_RebuildContactData
 GO