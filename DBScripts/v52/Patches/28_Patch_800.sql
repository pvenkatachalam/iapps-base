﻿--Update the product version:
GO
PRINT 'Updating the version of the products to 5.1.0628.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.0628.1'
GO
PRINT 'Adding index to CPCouponCode.CouponId'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE  name='IX_CPCouponCode_CouponId' AND object_id = OBJECT_ID(N'[dbo].[CPCouponCode]'))
	CREATE INDEX [IX_CPCouponCode_CouponId] ON [dbo].[CPCouponCode] ([CouponId])
GO
PRINT 'Altering column CPCouponCode.CouponId'
GO
IF COL_LENGTH(N'[dbo].[PageRedirect_Import]', N'OldURL') IS NOT NULL
	ALTER TABLE PageRedirect_Import ALTER COLUMN [OldURL] NVARCHAR (max)  NOT NULL
GO
PRINT 'Altering column PageRedirect_Import.NewURL'
GO
IF COL_LENGTH(N'[dbo].[PageRedirect_Import]', N'NewURL') IS NOT NULL
	ALTER TABLE PageRedirect_Import ALTER COLUMN NewURL NVARCHAR (max)  NOT NULL
GO

PRINT 'Altering procedure Facet_ReloadProductFacetCache'
GO
-- Copyright ⌐ 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To reload the flattened product/facet/facetrange cache table for frontend loookups 
-- Author: Devin
-- Created Date: 08-04-2009
-- Created By: Devin
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   
--********************************************************************************
ALTER PROCEDURE [dbo].[Facet_ReloadProductFacetCache]
(
	@ApplicationId uniqueidentifier=null
)
--now only 39 seconds, now 50 seconds with all Enumed Faceted in Shaw
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

--create temp table to load new cache information into 
--(not sure if this is the best way to declare a large temp table - DM)
 
DECLARE @tempCache TABLE 
( 
    Id INT IDENTITY(1,1) Primary Key, 
    FacetId uniqueidentifier,
	AttributeId uniqueidentifier,
	FacetValueID uniqueidentifier,
	DisplayText nvarchar(50),
	ProductId uniqueidentifier,
	SortOrder int Default(0),
	Sequence int Default(0)
)

SELECT 'Insert INTO @TempCache'

--Enumerated Attributes Defined Ranges
INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT FacetID, AttributeID, AttributeEnumId AS FacetValueID, Value AS DisplayText, ProductId
--, AE.Value AS [From], AE.Value AS [To]
FROM 
	vwProductSKUFacetAttributeValue_Enumerated PSFAV 
WHERE IsRange = 0 AND (@ApplicationId IS NULL OR PSFAV.SiteId = @ApplicationId) 
--30 seconds
SELECT 'Insert INTO @cteValueToId No Range'
-- 47 seconds insert into @TempCache 159k records
-- 22 seconds insert into @TempCache 243k records


SELECT 'Insert INTO @tempCache Numeric'

--Ranges Per Type:
INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
-- select *
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID -- F.IsRange = 1
	INNER JOIN ATFacetRangeInt FRX ON FR.Id = FRX.FacetRangeID 
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON F.AttributeID = PSFAV.AttributeId AND
			PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)  
			AND ISNUMERIC(PSFAV.Value)=1
WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)

SELECT 'Insert INTO @tempCache Date'

INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1 
	INNER JOIN ATFacetRangeDate FRX ON FR.Id = FRX.FacetRangeID
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value) AND ISDATE(PSFAV.Value)=1
    AND F.AttributeID = PSFAV.AttributeId
    WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)
    
--WHERE F.IsRange = 1 --and PSFAV.AttributeEnumId is null
-- 14 seconds
SELECT 'Insert INTO @tempCache Decimal'

INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --and  F.IsRange = 1 
	INNER JOIN ATFacetRangeDecimal FRX ON FR.Id = FRX.FacetRangeID 
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value) AND ISNUMERIC(PSFAV.Value)=1
    AND F.AttributeID = PSFAV.AttributeId
    WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)
--WHERE F.IsRange = 1 --and PSFAV.AttributeEnumId is null
-- 13 seconds

SELECT 'Insert INTO @tempCache String'

INSERT INTO @tempCache (FacetID,AttributeID,FacetValueID,DisplayText,ProductID)
SELECT DISTINCT F.Id AS FacetID, F.AttributeID, FR.Id AS FacetValueID, FR.DisplayText, PSFAV.ProductId
--, FRX.[From], FRX.[To]
FROM 
	ATFacet F 
	INNER JOIN ATFacetRange FR ON F.Id = FR.FacetID --AND F.IsRange = 1 
	INNER JOIN ATFacetRangeString FRX ON FR.Id = FRX.FacetRangeID 
	INNER JOIN vwProductSKUFacetAttributeValue_RANGE PSFAV ON PSFAV.Value BETWEEN Isnull(FRX.[From], PSFAV.Value) AND IsNull(FRX.[To],PSFAV.Value)
    AND F.AttributeID = PSFAV.AttributeId
    WHERE (@ApplicationId IS NULL OR F.SiteId = @ApplicationId)
--WHERE F.IsRange = 1 --and PSFAV.AttributeEnumId is null
-- 0 seconds
SELECT 'Updating Sequence'

Update T 
Set T.SortOrder = R.Sequence 
FROM @tempCache T
INNER JOIN ATFacet F on T.FacetID =F.Id
INNER JOIN ATFacetRange R on R.FacetID =F.Id AND T.FacetValueID = R.Id
Where IsRange=1 AND IsManualOrder =1;

with CTEUpdate AS
(
	SELECT  ROW_NUMBER() OVER(ORDER BY DisplayText Asc) RowNumber,
			FacetValueID,
			FacetID,
			ProductId 
	FROM @tempCache T
	INNER JOIN ATFacet F on T.FacetID =F.Id
	Where DisplayOrder =1 AND IsManualOrder=0
)


Update T
Set T.SortOrder =IsNull(C.RowNumber - (SELECT MIN(RowNumber) -1 FROM CTEUpdate R Where R.FacetID =T.FacetID) , 1) 
FROM @tempCache T
INNER JOIN CTEUpdate C ON T.FacetValueID =C.FacetValueID ;
--
--With CTEUpdate2 AS
--(
--	SELECT  ROW_NUMBER() OVER(ORDER BY count(ProductID) Desc) RowNumber,
--			FacetValueID,
--			FacetID
--	FROM @tempCache T
--	INNER JOIN ATFacet F on T.FacetID =F.Id
--	Where DisplayOrder =2 AND IsManualOrder=0
--	Group BY FacetValueID,FacetID
--)

-- if the sort order is supposed to be count then we can not do it here so set a -1
Update T
Set T.SortOrder =-1
FROM @tempCache T
INNER JOIN ATFacet F ON T.FacetID =F.Id
Where F.DisplayOrder = 2 And IsManualOrder = 0

Update T
Set T.Sequence =NF.Sequence
FROM @tempCache T
INNER JOIN NVNavNodeFacet NF ON T.FacetID =NF.FacetId

SELECT 'Insert INTO ATFacetRangeProduct_Cache'

--delete data from existing cache
truncate table dbo.ATFacetRangeProduct_Cache

	--repopulate the real cache
	--(again, best method for this? this will be a LOT of data - DM)
	INSERT INTO dbo.ATFacetRangeProduct_Cache 
	(Id,FacetID,AttributeID,FacetValueID,DisplayText,ProductID,SortOrder,FacetSequence)
	SELECT newid(),T.FacetId,T.AttributeId,T.FacetValueID,T.DisplayText,T.ProductId,T.SortOrder ,T.Sequence
	FROM @tempCache T

TRUNCATE TABLE ATFacetRangeProductTop_Cache

-- Create Distinct Product Count by Facet.
INSERT INTO ATFacetRangeProductTop_Cache --112,008
SELECT	
	[FacetID],    
    [ProductID]    
FROM 
	ATFacetRangeProduct_Cache --242,572
GROUP BY
	[FacetID]  
  ,[ProductID]

END
GO

PRINT 'Altering procedure Product_SimpleSearchSKU'
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  ALTER PROCEDURE [dbo].[Product_SimpleSearchSKU]
    (
	@SearchTerm nvarchar(max),
	@IsBundle tinyint = null,
	@pageNumber		int = null,
	@pageSize		int = null,
	@SortColumn VARCHAR(25)=null,
	@ProductTypeId uniqueidentifier=null,
	@ApplicationId uniqueidentifier=null
 )  
AS  
BEGIN  
	DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint,
			@SkuCount				bigint

	SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
	SET @PageUpperBound = @PageLowerBound - Isnull(@pageSize,0) + 1;

	DECLARE @searchText nvarchar(max)  
	SET @searchText = '%' + @SearchTerm + '%'
	IF @SortColumn IS NULL
		SET @SortColumn = 'productname asc'
	SET @SortColumn = LOWER(@SortColumn)
    Create table #tbSku
            (
              RowNumber INT IDENTITY(1, 1) primary key,
              Id UNIQUEIDENTIFIER ,
              ProductId UNIQUEIDENTIFIER ,
              ProductName NVARCHAR(2000) ,
              Qty DECIMAL(18, 4) ,
              AllocatedQty DECIMAL(18, 4) ,
              ProductTypeName NVARCHAR(1000)
            )
			CREATE NONCLUSTERED INDEX CIX_ProductId ON #tbSku(ProductId)
	
	INSERT INTO #tbSku
	SELECT S.Id, P.Id, P.Title, Isnull(dbo.GetBundleQuantityPerSite(I.SKUId,@ApplicationId) ,I.[Quantity]) , ISNULL(A.Quantity,0), T.Title
	FROM PRProduct P 
			JOIN PRProductSku S ON S.ProductId = P.Id
			JOIN PRProductType T ON T.Id = P.ProductTypeId
			Left Join vwInventoryPerSite I ON I.SkuId = S.Id AND I.SiteId=@ApplicationId
			Left Join (Select ProductSKUId SkuId,sum(Quantity)Quantity from vwAllocatedQuantityperWarehouse AL INNER JOIN INWarehouseSite WS on WS.WarehouseId = AL.WarehouseId AND WS.SiteId =@ApplicationId Group By AL.ProductSKUId) A ON A.SkuId = S.Id 
	WHERE	(@ProductTypeId IS NULL OR T.Id = @ProductTypeId) AND
			(@IsBundle IS NULL OR P.IsBundle = @IsBundle) AND
			(P.Title like @searchText 
					OR P.ShortDescription like @searchText 
					OR P.LongDescription like @searchText   
					OR P.Description like @searchText
					OR S.SKU like @searchText)
	ORDER BY
		CASE WHEN @SortColumn = 'productname desc' Then P.Title END DESC, 	
		CASE WHEN @SortColumn = 'productname asc' Then P.Title END ASC, 	
		CASE WHEN @SortColumn = 'sku desc' Then S.SKU END DESC, 	
		CASE WHEN @SortColumn = 'sku asc' Then S.SKU END ASC, 
		CASE WHEN @SortColumn = 'quantity desc' Then I.Quantity END DESC, 	
		CASE WHEN @SortColumn = 'quantity asc' Then I.Quantity  END ASC,
		CASE WHEN @SortColumn = 'availabletosell desc' Then A.Quantity END DESC, 	
		CASE WHEN @SortColumn = 'availabletosell asc' Then A.Quantity END ASC

		Select @SkuCount= Count(*) FROM #tbSku

	delete 
	from	#tbSku
	where	not (@pageSize is null OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))

	SELECT 
			T.Id,
			T.ProductId,
			T.ProductName,
			T.Qty AS Quantity,
			T.Qty - T.AllocatedQty AS AvailableToSell,
			S.Title,
			S.SKU,
			case when T.Qty >=999999 then 1 else S.IsUnlimitedQuantity end IsUnlimitedQuantity,
			S.AvailableForBackOrder,
			S.MaximumDiscountPercentage,			
			ISNULL(S.BackOrderLimit, 0) AS BackOrderLimit,
			S.AvailableForBackOrder,
			CAST(
				CASE 
					WHEN S.IsActive = 1 AND PR.IsActive = 1
						THEN 1
					ELSE 0
				END AS Bit) as IsActive,		
			T.ProductTypeName
		FROM #tbSku T JOIN PrProductSku S ON S.Id = T.Id
		INNER JOIN PRProduct PR ON PR.Id = S.ProductID
		
		declare @activestatus int = dbo.GetActiveStatus()

		SELECT	C.[Id],	C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
		FROM	#tbSku T
				INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
				INNER JOIN CLImage I on I.Id = OI.ImageId 
				INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.ParentImage
				INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType

		UNION ALL

		SELECT	C.[Id],	C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
		FROM	#tbSku T
				INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
				INNER JOIN CLImage I on I.Id = OI.ImageId 
				INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.Id
				INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType

		Order by Sequence--, Title
 
	

	select @SkuCount
END
GO
PRINT 'Altering procedure Membership_GetAllUserPermissions'
GO
ALTER PROCEDURE [dbo].[Membership_GetAllUserPermissions]
(
	@ProductId uniqueidentifier,
	@ApplicationId uniqueidentifier = null,
	@UserId uniqueidentifier = null,
	@IsSystemUser bit =null
)
AS
BEGIN
		
		SELECT	DISTINCT SiteId, S.Title, US.UserId
		FROM	dbo.USSiteUser US
		INNER JOIN SISite S ON US.SiteId = S.Id AND S.Status <> dbo.GetDeleteStatus()
		WHERE	
			(@ProductId is null OR (@ProductId is not null AND ProductId = @ProductId))
			AND (@UserId is null OR (@UserId is not null AND UserId = @UserId))
			AND (@IsSystemUser is null  OR  IsSystemUser = @IsSystemUser)

		SELECT DISTINCT	
				G.ProductId,
				MG.ApplicationId AS SiteId,
				MG.MemberId AS UserId,
				G.Id,
				G.Title,
				G.Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				Status,
				ExpiryDate,
				'' as CreatedByFullName,
				'' as ModifiedByFullName,
				G.GroupType
		FROM	dbo.USGroup G 
				INNER JOIN dbo.USMemberGroup MG ON G.Id = MG.GroupId AND MG.MemberType = 1	
				INNER JOIN USSiteUser US on MG.MemberId = US.UserId
				WHERE
				( @ProductId is null OR (@ProductId Is not null AND G.ProductId = @ProductId) )
				--AND ( @ApplicationId  is null OR (@ApplicationId is not null AND MG.ApplicationId   = @ApplicationId ) )
				AND ( @ApplicationId  is null OR MG.ApplicationId IN (Select SiteId From dbo.GetAncestorSites(@ApplicationId) ))
				AND ( @UserId  is null OR (@UserId is not null AND MG.MemberId   = @UserId ) )
				AND (@IsSystemUser is null  OR  IsSystemUser = @IsSystemUser)
END
GO
PRINT 'Altering procedure Post_GetPost'
GO
ALTER PROCEDURE [dbo].[Post_GetPost]
(
	@ApplicationId		uniqueIdentifier,		
	@Id					uniqueIdentifier = null,		
	@PostMonth			varchar(20) = null,
	@PostYear			varchar(20) = null,
	@PageSize			int = null,
	@PageNumber			int = null,
	@FolderId			uniqueIdentifier = null,
	@BlogId				uniqueIdentifier = null,
	@Status				int = null,
	@CategoryId			uniqueIdentifier = null,
	@Label				nvarchar(1024) = null,
	@Author				nvarchar(1024) = null,
	@IncludeVersion		bit = null,
	@VersionId			uniqueidentifier=null,
	@IgnoreSite			bit = null
)
AS
BEGIN
	DECLARE @IsSticky bit
	IF @Status = 5
		SET @IsSticky = 1

	DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)
	;WITH PostIds AS(
		SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo, 
			COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId
		FROM BLPost 
		WHERE (@Id IS NULL OR Id = @Id) AND
			(Status != dbo.GetDeleteStatus()) AND
			(@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND
			(@Status IS NULL OR Status = @Status) AND
			(@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND 
			(@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND 
			(@FolderId IS NULL OR HSId = @FolderId) AND 
			(@BlogId IS NULL OR BlogId = @BlogId) AND
			(@IsSticky IS NULL OR IsSticky = @IsSticky) AND
			(@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND
			(@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND
			(@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))
	)
	
	INSERT INTO @tbPostIds
	SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds
	WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR 
		RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))
	
	SELECT A.Id,			
		A.ApplicationId,
		A.Title,
		A.Description,
		A.ShortDescription,
		A.AuthorName,
		A.AuthorEmail,
		A.Labels,
		A.AllowComments,
		A.PostDate,
		A.Location,
		A.Status,
		A.IsSticky,
		A.CreatedBy,
		A.CreatedDate,
		A.ModifiedBy,
		A.ModifiedDate,
		A.FriendlyName,
		A.PostContentId,
		A.ContentId,
		A.ImageUrl,
		A.TitleTag,
		A.H1Tag,
		A.KeywordMetaData,
		A.DescriptiveMetaData,
		A.OtherMetaData,
		B.BlogListPageId,
		B.Id AS BlogId,
		A.WorkflowState,
		A.PublishDate,
		A.PublishCount,
		A.INWFFriendlyName,
		A.HSId AS MonthId
	FROM BLPost A 
		JOIN @tbPostIds T ON A.Id = T.Id
		JOIN BLBlog B ON A.BlogId = B.Id
	ORDER BY T.RowNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.Text, 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		sDir.VirtualPath As FolderPath
	FROM COContent C
		INNER JOIN @tbPostIds T ON C.Id = T.PostContentId
		INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		
	SELECT Top 1 TotalRows FROM @tbPostIds

	SELECT A.Id,
		ApplicationId,
		Title,
		Description,
		BlogNodeType,
		MenuId,
		ContentDefinitionId,
		ShowPostDate,
		DisplayAuthorName,
		EmailBlogOwner,
		ApproveComments,
		RequiresUserInfo,
		Status,
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		ModifiedDate,
		BlogListPageId,
		BlogDetailPageId,
		ContentDefinitionId
	FROM BLBlog A 
		INNER JOIN @tbPostIds T ON T.BlogId = A.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
	
	SELECT T.Id,
		I.Title,
		C.TaxonomyId
	FROM COTaxonomyObject C
		JOIN @tbPostIds T ON T.Id = C.ObjectId
		JOIN COTaxonomy I ON I.Id = C.TaxonomyId

	SELECT Count(*) AS NoOfComments,
		T.Id
	FROM BLComments C
		JOIN @tbPostIds T ON C.ObjectId = T.Id and C.status <> dbo.GetDeleteStatus() 
	GROUP BY T.Id

	IF @IncludeVersion = 1
	BEGIN
		DECLARE @tbVersionIds TABLE(Id uniqueidentifier)
		IF @VersionId IS NULL
		BEGIN
			;WITH CTEVersion AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,
					V.Id FROM VEVersion V
					JOIN BLPost T On T.Id = V.ObjectId
			)
			INSERT INTO @tbVersionIds
			SELECT Id FROM CTEVersion WHERE VerRank = 1
		END
		ELSE
		BEGIN
			INSERT INTO @tbVersionIds VALUES (@VersionId)
		END

		SELECT V.Id AS VersionId,
			V.ObjectId AS PostId,
			V.XMLString
		FROM VEVersion V
			JOIN @tbVersionIds T On T.Id = V.Id

		SELECT 
			V.PostVersionId AS VersionId,
			C.ContentId,
			C.XMLString
		FROM VEPostContentVersion V
			JOIN @tbVersionIds T On T.Id = V.PostVersionId
			JOIN VEContentVersion C ON C.Id = V.ContentVersionId
	END
END
GO
GO
PRINT 'Altering procedure Country_Get'
GO
ALTER PROCEDURE [dbo].[Country_Get](@Id uniqueidentifier=null,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
	IF(@Id IS NULL)
		BEGIN
				SELECT 
					C.Id,
					CountryName Title,
					CountryCode,
					CreatedBy,
					CreatedDate,
					ModifiedDate,
					ModifiedBy,
					R.Name Region,
					Status,
					IsTerritory
					FROM GLCountry C
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					Order By C.CountryName

					SELECT 
					S.Id,
					S.State Title,
					S.StateCode,
					S.CountryId,
					S.CreatedBy,
					S.CreatedDate,
					S.ModifiedDate,
					S.ModifiedBy,
					R.Name Region 
					FROM GLState S
					INNER JOIN GLCountry C ON S.CountryId=C.Id
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					Where S.Status = 1	
		END
	ELSE
		BEGIN
					SELECT	C.Id,
					CountryName Title,
					CountryCode,
					CreatedBy,
					CreatedDate,
					ModifiedDate,
					ModifiedBy,
					R.Name Region,
					Status,
					IsTerritory
					FROM GLCountry C
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE C.Id = @Id

						SELECT 
					S.Id,
					S.State Title,
					S.StateCode,
					S.CountryId,
					S.CreatedBy,
					S.CreatedDate,
					S.ModifiedDate,
					S.ModifiedBy,
					R.Name Region 
					FROM GLState S
					INNER JOIN GLCountry C ON S.CountryId=C.Id
					Inner JoIN GLCountryRegion R On C.RegionId = R.Id
					WHERE (@Id is null or C.Id = @Id) AND S.Status = 1	
		END
END
GO
PRINT 'Inserting US Territories into GLState table'



IF NOT EXISTS (Select 1 from GLState Where StateCode='VI' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
VALUES ('673CABFD-2EC2-479B-A22F-0C6F6FA3E0F1','Virgin Islands, U.S.','VI','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','outlying territory')


IF NOT EXISTS (Select 1 from GLState Where StateCode='PR' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
     VALUES
('25C6D970-648C-47CF-90F5-22DAB494CE9D','Puerto Rico','PR','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','outlying territory')

IF NOT EXISTS (Select 1 from GLState Where StateCode='GU' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
     VALUES
('8458B993-ABEA-410F-AAC5-4306215E4839','Guam','GU','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','outlying territory')

IF NOT EXISTS (Select 1 from GLState Where StateCode='MP' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
     VALUES
('3FA50BF0-05D8-4CB5-A591-4D831C6648CC','Northern Mariana Islands','MP','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','outlying territory')

IF NOT EXISTS (Select 1 from GLState Where StateCode='AS' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
     VALUES
('BAD75D82-4BBD-44FD-9B54-66F68EEB611C','AMERICAN SAMOA','AS','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','outlying territory')

IF NOT EXISTS (Select 1 from GLState Where StateCode='UM' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
     VALUES
('2D171F46-B8E4-4422-99C9-D3E5B277CC98','United States Minor Outlying Islands','UM','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','outlying territory')

IF NOT EXISTS (Select 1 from GLState Where StateCode='DC' AND CountryId='AD72C013-B99D-4513-A666-A128343CCFF0')
INSERT INTO [dbo].[GLState]
           ([Id]
           ,[State]
           ,[StateCode]
           ,[CountryId]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[PrimaryLevelName])
     VALUES
('AF516933-BABE-475D-81D1-E512FEEC02AA','District of Columbia','DC','AD72C013-B99D-4513-A666-A128343CCFF0','2009-06-02 00:00:00.000','798613EE-7B85-4628-A6CC-E17EE80C09C5',NULL,NULL,'1','district')

GO
PRINT 'Updating incorrect state codes'
GO

UPDATE GLState SET StateCode ='FL' Where Id='AF76A287-B17F-4554-9E62-82795E0799F9'
UPDATE GLState SET StateCode ='MD' Where Id='36A5884C-A4D0-4617-95A9-DDAC07000EE2'
UPDATE GLState SET StateCode ='MT' Where Id='D6FC14D3-9E51-465C-8B83-E709083F3CFD'


GO

GO
PRINT 'Updating Territories which are entered as countries into states'
GO
;with Territory AS
(
select C.Id TerritoryId, S.Id StateId, S.CountryId
from GLState S 
INNER JOIN GLCountry C on S.State = C.CountryName
Where C.Status=3 and IsTerritory=1
)


UPDATE A Set A.CountryId =T.CountryId,A.StateId = T.StateId,A.StateName=null
from GLAddress A 
INNER JOIN Territory T on A.CountryId = T.TerritoryId

GO

PRINT 'Altering procedure UPSUserProfile_Save'
GO

ALTER PROCEDURE [dbo].[UPSUserProfile_Save]
(
	@Id     uniqueidentifier = null OUT ,
	@UPSUserName nvarchar(max),
	@UPSPassword nvarchar(max) ,
	@AccessLicenseNumber nvarchar(max) = null,	
	@IsHostingProvider bit,
	@ApplicationId  uniqueidentifier,
	@AddressId uniqueidentifier,
	@ContactName nvarchar(max) = null,
	@Title nvarchar(max)  = null,
	@CompanyName nvarchar(max)  = null,
	@EmailAddress  nvarchar(max) ,
	@Status int, 
	@Sequence int = null,
	@IsDefault bit = 0,		
	@CreatedBy [uniqueidentifier] ,
	@ModifiedBy [uniqueidentifier] = null	
)
AS
BEGIN
 DECLARE @NewId uniqueidentifier  
 DECLARE @Now dateTime  
 DECLARE @Stmt VARCHAR(36)   
 DECLARE @Error int  

 SET @Now = getutcdate()  

 IF (@Id is null)  
 Begin  
	set @NewId = newid()  
	set @Id  =@NewId   
	Declare @currentSequence int       
	Select @currentSequence = Isnull(max(Sequence),0) from dbo.UPSUserProfile
	SET @Stmt = 'Insert UPSUserProfile'  

	INSERT INTO UPSUserProfile
	(
		Id,
		UPSUserName,
		[UPSPassword],
		IsHostingProvider ,
		AccessLicenseNumber,	
		SiteId,
		AddressId,
		ContactName,
		Title,
		CompanyName,
		EmailAddress,		
		[Status],
		Sequence,
		[CreatedBy],
		[CreatedDate],
		IsDefault
	)
	VALUES
	(
		@NewId,
		@UPSUserName,
		@UPSPassword,
		@IsHostingProvider ,
		@AccessLicenseNumber,	
		@ApplicationId,
		@AddressId,
		@ContactName,
		@Title,
		@CompanyName,
		@EmailAddress,
		@Status,
		@currentSequence+1 ,
		@CreatedBy,
		@Now,
		@IsDefault		
	)		  

	SELECT @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN  
		RAISERROR('DBERROR||%s',16,1,@Stmt)  
		RETURN dbo.GetDataBaseErrorCode()  
	END  
	SET @Id =  @NewId 
 End  
 Else
 Begin
	IF (NOT EXISTS(SELECT 1 FROM  UPSUserProfile WHERE  Id = @Id))  
	BEGIN     
		set @Stmt = convert(varchar(36),@Id)  
		RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
		RETURN dbo.GetBusinessRuleErrorCode()  
	END   

	SET @Stmt = 'Update UPSUserProfile' 
	UPDATE UPSUserProfile
	SET
		UPSUserName		= @UPSUserName
		,[UPSPassword] = @UPSPassword
		,IsHostingProvider = @IsHostingProvider
		,AccessLicenseNumber	 = @AccessLicenseNumber
		,AddressId = @AddressId
		,SiteId	= @ApplicationId
		,ContactName	= @ContactName
		,Title			= @Title
		,CompanyName	= @CompanyName		
		,EmailAddress	= @EmailAddress
		,[Status]		= @Status
		,Sequence		= @Sequence
		,[ModifiedBy]			= @ModifiedBy  
		,[ModifiedDate]			= @Now
		,IsDefault 			= @IsDefault
	WHERE Id = @Id

	SELECT @Error = @@ERROR  
	IF @Error <> 0  
	BEGIN  
		RAISERROR('DBERROR||%s',16,1,@Stmt)  
		RETURN dbo.GetDataBaseErrorCode()  
	END  

 End
 IF (@IsDefault=1)
 BEGIN
	UPDATE UPSUserProfile SET IsDefault = 0 WHERE Id <> @Id
	IF (Exists(SELECT Id FROM STSettingType Where Name like 'UPSLicenseNumber'))
	BEGIN
		UPDATE STSiteSetting SET Value =@AccessLicenseNumber WHERE SettingTypeId IN (SELECT Id FROM STSettingType Where Name like 'UPSLicenseNumber') AND SiteId = @ApplicationId
	END
	IF (Exists(SELECT Id FROM STSettingType Where Name like 'UPSUserName'))
	BEGIN
		UPDATE STSiteSetting SET Value =@UPSUserName WHERE SettingTypeId IN (SELECT Id FROM STSettingType Where Name like 'UPSUserName') AND SiteId = @ApplicationId
	END
	IF (Exists(SELECT Id FROM STSettingType Where Name like 'UPSPassword'))
	BEGIN
		UPDATE STSiteSetting SET Value =@UPSPassword WHERE SettingTypeId IN (SELECT Id FROM STSettingType Where Name like 'UPSPassword') AND SiteId = @ApplicationId
	END
 END

END

GO
PRINT 'Altering procedure Product_SimpleSearchSKU'
GO

ALTER PROCEDURE [dbo].[Product_SimpleSearchSKU]
    (
    @SearchTerm nvarchar(max),
    @IsBundle tinyint = null,
    @pageNumber        int = null,
    @pageSize        int = null,
    @SortColumn VARCHAR(25)=null,
    @ProductTypeId uniqueidentifier=null,
    @ApplicationId uniqueidentifier=null
 )  
AS  
BEGIN  
    DECLARE
            @PageLowerBound            bigint,
            @PageUpperBound            bigint,
            @SkuCount                bigint
 
    SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
    SET @PageUpperBound = @PageLowerBound - Isnull(@pageSize,0) + 1;
 
    DECLARE @searchText nvarchar(max)  
    SET @searchText = '%' + @SearchTerm + '%'
    IF @SortColumn IS NULL
        SET @SortColumn = 'productname asc'
    SET @SortColumn = LOWER(@SortColumn)
    Create table #tbSku
            (
              RowNumber INT IDENTITY(1, 1) primary key,
              Id UNIQUEIDENTIFIER ,
              ProductId UNIQUEIDENTIFIER ,
              ProductName NVARCHAR(2000) ,
              Qty DECIMAL(18, 4) ,
              AllocatedQty DECIMAL(18, 4) ,
              ProductTypeName NVARCHAR(1000)
            )
            CREATE NONCLUSTERED INDEX CIX_ProductId ON #tbSku(ProductId)
 
    INSERT INTO #tbSku
    SELECT S.Id, P.Id, P.Title, Isnull(dbo.GetBundleQuantityPerSite(I.SKUId,@ApplicationId) ,I.[Quantity]) , ISNULL(A.Quantity,0), T.Title
    FROM PRProduct P 
            JOIN PRProductSku S ON S.ProductId = P.Id
            JOIN PRProductType T ON T.Id = P.ProductTypeId
            Left Join vwInventoryPerSite I ON I.SkuId = S.Id AND I.SiteId=@ApplicationId
            Left Join (Select ProductSKUId SkuId,sum(Quantity)Quantity from vwAllocatedQuantityperWarehouse AL INNER JOIN INWarehouseSite WS on WS.WarehouseId = AL.WarehouseId AND WS.SiteId =@ApplicationId Group By AL.ProductSKUId) A ON A.SkuId = S.Id 
    WHERE    (@ProductTypeId IS NULL OR T.Id = @ProductTypeId) AND
            (@IsBundle IS NULL OR P.IsBundle = @IsBundle) AND
            (P.Title like @searchText 
                    OR P.ShortDescription like @searchText 
                    OR P.LongDescription like @searchText   
                    OR P.Description like @searchText
                    OR S.SKU like @searchText)
    ORDER BY
        CASE WHEN @SortColumn = 'productname desc' Then P.Title END DESC,     
        CASE WHEN @SortColumn = 'productname asc' Then P.Title END ASC,     
        CASE WHEN @SortColumn = 'sku desc' Then S.SKU END DESC,     
        CASE WHEN @SortColumn = 'sku asc' Then S.SKU END ASC, 
        CASE WHEN @SortColumn = 'quantity desc' Then I.Quantity END DESC,     
        CASE WHEN @SortColumn = 'quantity asc' Then I.Quantity  END ASC,
        CASE WHEN @SortColumn = 'availabletosell desc' Then A.Quantity END DESC,     
        CASE WHEN @SortColumn = 'availabletosell asc' Then A.Quantity END ASC
 
        Select @SkuCount= Count(*) FROM #tbSku
 
    delete 
    from    #tbSku
    where    not (@pageSize is null OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
 
    SELECT 
            T.Id,
            T.ProductId,
            T.ProductName,
            T.Qty AS Quantity,
            T.Qty - T.AllocatedQty AS AvailableToSell,
            S.Title,
            S.SKU,
            case when T.Qty >=999999 then 1 else S.IsUnlimitedQuantity end IsUnlimitedQuantity,
            S.AvailableForBackOrder,
            S.MaximumDiscountPercentage,            
            ISNULL(S.BackOrderLimit, 0) AS BackOrderLimit,
            S.AvailableForBackOrder,
            CAST(
                CASE 
                    WHEN S.IsActive = 1 AND PR.IsActive = 1
                        THEN 1
                    ELSE 0
                END AS Bit) as IsActive,        
            T.ProductTypeName
        FROM #tbSku T JOIN PrProductSku S ON S.Id = T.Id
        INNER JOIN PRProduct PR ON PR.Id = S.ProductID
 
        declare @activestatus int = dbo.GetActiveStatus()
 
        SELECT    C.[Id],    C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
        FROM    #tbSku T
                INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
                INNER JOIN CLImage I on I.Id = OI.ImageId 
                INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.ParentImage
                INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType
 
        UNION ALL
 
        SELECT    C.[Id],    C.[Title], C.[Description], C.[FileName], C.[Size], C.[Attributes], C.[RelativePath], C.[AltText], C.[Height], C.[Width], C.[Status], C.[SiteId], OI.[ObjectId] AS ProductId, OI.[ObjectType], OI.[IsPrimary], OI.[Sequence], C.[ParentImage], C.ImageType, OI.ObjectId as ProductId
        FROM    #tbSku T
                INNER JOIN CLObjectImage OI ON OI.ObjectType = 205 and T.ProductId = OI.ObjectId
                INNER JOIN CLImage I on I.Id = OI.ImageId 
                INNER JOIN CLImage C ON i.status = @activestatus and i.id is not null and I.Id=C.Id
                INNER JOIN CLImageType IT ON IT.TypeNumber = 0 AND IT.Id = C.ImageType
 
        Order by Sequence--, Title
 
 
 
    select @SkuCount
END
GO
IF (OBJECT_ID('iAppsForm_ExtractResponse') IS NOT NULL)
	DROP PROCEDURE iAppsForm_ExtractResponse
GO
CREATE PROCEDURE [dbo].[iAppsForm_ExtractResponse]
(	
	@FormsId			uniqueidentifier,
	@TableSuffix		nvarchar(200),
	@StartDate			DateTime = null,
	@EndDate			DateTime = null
)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SiteId uniqueidentifier
	SET @SiteId = (SELECT TOP 1 ApplicationId FROM Forms WHERE Id = @FormsId)

	IF OBJECT_ID('tempdb..#FormResponse_Export_Temp') IS NOT NULL
		DROP TABLE #FormResponse_Export_Temp
		
	SELECT 
		Id, 
		T2.X.value('(@label)[1]', 'nvarchar(max)') AS Title, 
		T2.X.value('(@value)[1]', 'nvarchar(max)') AS Value
	INTO #FormResponse_Export_Temp
	FROM FormsResponse
		CROSS APPLY FormsResponseXml.nodes('/FormElements/FormElement') as T2(X)
	WHERE FormsId = @FormsId
		AND (@StartDate IS NULL OR @EndDate IS NULL OR ResponseDate BETWEEN @StartDate AND @EndDate)
		
	DECLARE @Columns AS nvarchar(max), @Query  AS nvarchar(max)
    
	DECLARE @ExportTableName nvarchar(500)
	SET @ExportTableName = 'FormResponse_BulkExport_' + @TableSuffix

	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@ExportTableName) AND TYPE IN (N'U'))
	BEGIN
		SET @Query = 'DROP TABLE ' + @ExportTableName
		EXEC(@Query)
	END 
	
	SELECT @Columns = STUFF((SELECT ',' + QUOTENAME(Title) FROM #FormResponse_Export_Temp
					GROUP BY Title
				FOR XML PATH(''), TYPE
				).value('.', 'NVARCHAR(MAX)') 
			,1,1,'')
	
	SET @Query = 'SELECT * INTO ' + @ExportTableName + ' FROM (
		SELECT Id, ' + @Columns + ' FROM 
         (
            SELECT Id, Value, Title FROM #FormResponse_Export_Temp
        ) x
        pivot 
        (
            max(value)
            FOR Title in (' + @Columns + ')
        ) p) y    
                  
        SELECT T.*,
			dbo.ConvertTimeFromUtc(ResponseDate, ''' + CAST(@SiteId AS varchar(36)) + ''') AS ResponseDate,
			IPAddress
         FROM ' + @ExportTableName + ' T
			JOIN FormsResponse R on T.Id = R.Id 
	'		

	EXEC(@Query)
	
	IF OBJECT_ID('tempdb..#FormResponse_Export_Temp') IS NOT NULL
		DROP TABLE #FormResponse_Export_Temp
END
GO
IF (OBJECT_ID('iAppsForm_DeleteBulkTable') IS NOT NULL)
	DROP PROCEDURE iAppsForm_DeleteBulkTable
GO
CREATE PROCEDURE [dbo].[iAppsForm_DeleteBulkTable]
(	
	@TableSuffix		nvarchar(200)
)
AS
BEGIN
	DECLARE @Query  AS nvarchar(max)
    
    DECLARE @ExportTableName nvarchar(500)
	SET @ExportTableName = 'FormResponse_BulkExport_' + @TableSuffix

	IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(@ExportTableName) AND TYPE IN (N'U'))
	BEGIN
		SET @Query = 'DROP TABLE ' + @ExportTableName
		EXEC(@Query)
	END 
END
GO
ALTER PROCEDURE [dbo].[iAppsForm_GetFormsResponse]
(	
	@FormsId			Uniqueidentifier,
	@UserId				Uniqueidentifier = null,
	@StartDate			DateTime = null,
	@EndDate			DateTime = null
)
AS
BEGIN
	DECLARE @ApplicationId	Uniqueidentifier
	SET @ApplicationId = (SELECT TOP 1 ApplicationId FROM Forms WHERE Id = @FormsId)
	
	IF (@StartDate IS NOT NULL AND @EndDate IS NOT NULL)
	BEGIN
		SET @StartDate = dbo.ConvertTimeToUtc(@StartDate, @ApplicationId)
		SET @EndDate = dbo.ConvertTimeToUtc(@EndDate, @ApplicationId)
	
		SELECT
			R.Id,
			R.FormsId,
			R.FormsResponseXML,
			dbo.ConvertTimeFromUtc(R.ResponseDate, @ApplicationId) AS ResponseDate,
			R.UserId,
			FN.UserFullName CreatedByFullName,
			--M.LoweredEmail,
			R.IPAddress,
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') ElementId,
			R.FormsResponseXML.value('(/FormElements/FormElement/@value)[1]','varchar(256)') ElementValue
		FROM 
			FormsResponse R	LEFT JOIN Forms F ON R.FormsId = F.Id AND
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') = F.XMLString.value('(/FormElements/FormElement/@id)[1]','varchar(256)') AND F.Status	= 1	
			--JOIN USUser U ON U.Id = R.UserId AND U.Status	= 1 JOIN USMembership M ON U.Id = M.UserId
			LEFT JOIN VW_UserFullName FN on FN.UserId = R.UserId
		WHERE R.FormsId = @FormsId				
			AND R.ResponseDate BETWEEN @StartDate AND @EndDate 
		ORDER BY  
			R.ResponseDate DESC	
	END
	ELSE
	BEGIN
		SELECT
			R.Id,
			R.FormsId,
			R.FormsResponseXML,
			dbo.ConvertTimeFromUtc(R.ResponseDate, @ApplicationId)  AS ResponseDate,
			R.UserId,
			FN.UserFullName CreatedByFullName,
			--M.LoweredEmail,
			R.IPAddress,
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') ElementId,
			R.FormsResponseXML.value('(/FormElements/FormElement/@value)[1]','varchar(256)') ElementValue
		FROM 
			FormsResponse R	LEFT JOIN Forms F ON R.FormsId = F.Id AND
			R.FormsResponseXML.value('(/FormElements/FormElement/@ID)[1]','varchar(256)') = F.XMLString.value('(/FormElements/FormElement/@id)[1]','varchar(256)') AND F.Status	= 1	
			--JOIN USUser U ON U.Id = R.UserId AND U.Status	= 1 JOIN USMembership M ON U.Id = M.UserId
			LEFT JOIN VW_UserFullName FN on FN.UserId = R.UserId
		WHERE				
			R.FormsId = @FormsId
		ORDER BY  
			R.ResponseDate DESC	
	END
END
GO
ALTER PROCEDURE [dbo].[PageMap_GetReDirect]
--********************************************************************************
-- PARAMETERS
		@ApplicationId	uniqueidentifier,
		@IsGenerated bit = NULL,
		@Keyword nvarchar(max) = NULL,
		@PageSize	int = NULL,
		@PageNumber	int = NULL
--********************************************************************************
AS
BEGIN
		--select Id,OldURL,NewURL,LastURLId from PageRedirect Where ApplicationId=@ApplicationId And Status = 1
		IF @Keyword is not null 
			Set @Keyword = '%' + @Keyword + '%'
		
		;WITH CTE (RowNo, OldUrl, NewUrl, IsGenerated) AS(
			SELECT ROW_NUMBER() OVER(ORDER BY OldURL),
				OldUrl, 
				NewUrl, 
				IsGenerated 
			FROM PageRedirect
			WHERE ApplicationId = @ApplicationId 
				AND Status = 1 
				AND (@Keyword IS NULL OR NewURL like @Keyword OR OldURL like @Keyword)
				AND OldURL IS NOT NULL AND NewURL IS NOT NULL
				AND (@IsGenerated IS NULL OR IsGenerated = @IsGenerated)
			)
		
		SELECT * FROM CTE
			WHERE @PageSize IS NULL OR @PageNumber IS NULL 
				OR (RowNo between (@PageNumber - 1) * @PageSize + 1 and @PageNumber * @PageSize)
		
		SELECT COUNT(*) FROM PageRedirect
			WHERE ApplicationId = @ApplicationId AND Status = 1 AND (@Keyword IS NULL OR NewURL like @Keyword OR OldURL like @Keyword)
				AND (@IsGenerated IS NULL OR IsGenerated = @IsGenerated)
END

GO
ALTER PROCEDURE [dbo].[PageMap_ReDirectDelete]
--********************************************************************************
-- PARAMETERS
		@ApplicationId	uniqueidentifier,
		@OldURL			nvarchar(max),
		@NewURL			nvarchar(max)
--********************************************************************************
AS
BEGIN

	-- @NewURL is menu url
	Declare @DeleteIds Table(Id uniqueidentifier)
	Insert into @DeleteIds
	Select Id from PageRedirect WHERE LOWER(NewURL) = LOWER(@NewURL) AND LOWER(OldURL) = LOWER(@OldURL)

	UPDATE PageRedirect SET Status = 0 WHERE Id in (select Id from @DeleteIds) or LastUrlId in (select Id from @DeleteIds)

END

GO
PRINT 'Altering procedure PageMap_ReDirectSave'
 GO
ALTER PROCEDURE [dbo].[PageMap_ReDirectSave]  
--********************************************************************************  
-- PARAMETERS  
	@ApplicationId	uniqueidentifier,  
	@OldURL			nvarchar(max),  
	@NewURL			nvarchar(max),
	@IsGenerated	bit = 1
--********************************************************************************  
AS  
BEGIN  
  
	DECLARE @Id				uniqueidentifier  
	DECLARE @NewId			uniqueidentifier  
	DECLARE @OldLastURLId	uniqueidentifier  

	IF @IsGenerated IS NULL
		SET @IsGenerated = 1

	IF EXISTS(SELECT 1 FROM PageRedirect WHERE lower(OldURL) = lower(@OldURL) AND ApplicationId = @ApplicationId AND Status=1)
		UPDATE PageRedirect SET NewURL = @NewURL WHERE OldURL = @OldURL AND ApplicationId = @ApplicationId
	ELSE
	BEGIN	
		SET @NewId = newid()  
	  
		SELECT @Id = Id, 
				@OldLastURLId = LastURLId 
			FROM PageRedirect WHERE lower(NewURL) = lower(@OldURL) AND ApplicationId = @ApplicationId  
		
		IF (@Id IS NOT NULL)
		BEGIN
			UPDATE PageRedirect SET LastURLId = @NewId, NewURL = @NewURL WHERE Id = @Id 
			UPDATE PageRedirect SET LastURLId = @NewId, NewURL = @NewURL WHERE LastURLId = @Id
		END
		
		INSERT INTO PageRedirect(Id, ApplicationId, OldURL, NewURL, LastURLId, CreatedDate, IsGenerated) 
				VALUES(@NewId, @ApplicationId, @OldURL, @NewURL, null, getdate(), @IsGenerated)  
	END
  
END  
 GO
PRINT 'Altering procedure ProfileManager_SetProperties'
 GO
 ALTER PROCEDURE [dbo].[ProfileManager_SetProperties]
--********************************************************************************
-- PARAMETERS
		@ApplicationId			uniqueidentifier,
		@UserName				nvarchar(256),
		@UserId					uniqueidentifier,
		@UserProfile			xml,
		@IsUserAnonymous		bit, --This can be used to create the user.
		@FirstName              nvarchar(256) =null,
		@LastName				nvarchar(256) =null,
		@MiddleName             nvarchar(256) =null,
		@ExpiryDate             datetime =null,
		@EmailNotification      bit =0,
		@TimeZone				nvarchar(100) =null,
		@ReportRangeSelection   varchar(50) =null,
		@ReportStartDate		datetime =null,
		@ReportEndDate			datetime=null, 
		@BirthDate			  datetime=null,
		@CompanyName			  nvarchar(1024)=null,
		@Gender               nvarchar(50)=null,
		@HomePhone			varchar(50)=null,
		@MobilePhone			varchar(50)=null,
		@OtherPhone			varchar(50)=null
--********************************************************************************
AS
BEGIN
	
	DECLARE @CurrentTimeUtc DATETIME
	SET @CurrentTimeUtc = GetUtcDate()

	--Gets the user id for the given site.
	IF (@UserId IS NULL AND @UserName IS NOT NULL)
	BEGIN
		SELECT @UserId = [dbo].[USGetUserIdForSite](@UserName, @ApplicationId)
	END
	
	IF (@UserId IS NULL)
	BEGIN
		RAISERROR ('NOTEXISTS||%s' ,16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	Update USUser set 
		FirstName=@FirstName, 
		LastName=@LastName,
		MiddleName=@MiddleName,
		ExpiryDate=ISNULL(@ExpiryDate,ExpiryDate),
		EmailNotification= @EmailNotification,
		TimeZone =@TimeZone,
		ReportRangeSelection=@ReportRangeSelection,
		ReportStartDate=@ReportStartDate,
		ReportEndDate=@ReportEndDate,
		BirthDate=@BirthDate,
		CompanyName=@CompanyName,
		Gender=@Gender,
		HomePhone=@HomePhone,
		MobilePhone=@MobilePhone,
		OtherPhone=@OtherPhone
		
		 where Id=@UserId
		
	--Updates the profile info present in the xml.
	EXEC [ProfileManager_UpdateUserProfileFromXml] @UserId, @UserProfile, @CurrentTimeUtc

	--If properties are found then update the lastactivity date of the user.
	IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.USUser
        SET    LastActivityDate = @CurrentTimeUtc
        WHERE  Id = @UserId
    END
END
--********************************************************************************


GO
PRINT 'Changing group Navigational editor to Global navigational Editor'
 GO
Update USGroup set Title = 'Global Navigational Editor' , Description ='Global Navigational Editor Group' where Title= 'Navigational Editor'
GO