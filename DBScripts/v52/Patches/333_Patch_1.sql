﻿GO
PRINT 'Updating the product version as 5.2.1213.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.2.1213.1'

GO
PRINT 'Dropping a FOREIGN KEY constraint FK_CPCouponNavCategory_NVNavNodeNavFilterMap'
GO
IF OBJECT_ID('FK_CPCouponNavCategory_NVNavNodeNavFilterMap') IS NOT NULL
	ALTER TABLE [dbo].[CPCouponNavCategory] DROP CONSTRAINT FK_CPCouponNavCategory_NVNavNodeNavFilterMap 
GO
PRINT 'Adding a FOREIGN KEY constraint FK_CPCouponNavCategory_PageMapNode'
GO
IF OBJECT_ID('FK_CPCouponNavCategory_PageMapNode') IS  NULL 
ALTER TABLE [dbo].[CPCouponNavCategory]  WITH CHECK ADD CONSTRAINT [FK_CPCouponNavCategory_PageMapNode] FOREIGN KEY([NavCategoryId]) REFERENCES [dbo].[PageMapNode] ([PageMapNodeId])

GO
IF COL_LENGTH('MKCampaignRunWorkTable','CreatedDate') IS NULL
 ALTER TABLE MKCampaignRunWorkTable ADD [CreatedDate] DATETIME NULL DEFAULT getutcdate()
GO
PRINT 'Adding a column for ScheduledTimeZone'
GO
IF COL_LENGTH('MKCampaign','ScheduledTimeZone') IS NULL
	ALTER TABLE MKCampaign Add [ScheduledTimeZone] NVARCHAR(MAX) NULL
GO
PRINT 'Altering view vw_contactSite'
GO
ALTER VIEW [dbo].[vw_contactSite]
	AS 

	SELECT Distinct UserId,SiteId,SU.IsPrimarySite as IsPrimary
	FROM [dbo].USSiteUser SU 
	INNER JOIN USUser U on U.Id =SU.UserId
	Where U.Status!=3
	
	UNION 
	
	SELECT ContactId AS UserId, SiteId,IsPrimarySite as IsPrimary
	FROM [dbo].MKContactSite CS

GO
PRINT N'Altering [dbo].[vw_contacts]...';
GO
ALTER VIEW [dbo].[vw_contacts]
AS
SELECT     
	U.Id UserId, 
	U.[FirstName],
	U.[MiddleName], 
	U.[LastName],
	U.[CompanyName],
	U.[BirthDate],
	U.[Gender],
	U.[AddressId],
	[Status], U.[HomePhone],
	U.[MobilePhone], 
	U.[OtherPhone], 
	U.[ImageId], 
	U.[Notes], 
	U.Email,
	0 ContactType,
	ContactSourceId,
	CreatedBy,
	CreatedDate,
	ModifiedBy,
	ModifiedDate
FROM         
	MKContact U 
UNION ALL
SELECT     
	CP.[Id] UserId,
	U.[FirstName],
	U.[MiddleName],
	U.[LastName],
	U.[CompanyName],
	U.[BirthDate],
	U.[Gender],
	UA.[AddressId], 
	CASE WHEN CP.IsActive = 1 
	THEN U.[Status] 
	ELSE 2 END/* if Not Active return inactive*/,
	U.[HomePhone],
	U.[MobilePhone],
	U.[OtherPhone],
	U.[ImageId],
	NULL Notes, 
	M.Email, 
	1 ContactType,
	9 ContactSourceId,
	U.CreatedBy,
	U.CreatedDate,
	U.ModifiedBy,
	U.ModifiedDate
FROM        USUser U
	
	INNER JOIN  [USCommerceUserProfile] CP  ON CP.Id = U.Id 
	INNER JOIN USMembership M ON M.UserId = U.Id 
	LEFT JOIN (select top 1 * from USUserShippingAddress where UserId=Id )UA ON UA.UserId = U.Id AND UA.IsPrimary = 1
	LEFT JOIN dbo.USSiteUser SU ON SU.UserId = U.Id AND SU.IsSystemUser = 1
WHERE   
	U.Status !=3  and SU.UserId IS NULL
UNION ALL
SELECT 
DISTINCT 
	U.[Id] 'UserId',
	U.[FirstName], 
	U.[MiddleName], 
	U.[LastName],
	U.[CompanyName],
	U.BirthDate,
	U.[Gender],
	NULL AddressId,
	U.[Status],
	U.[HomePhone], 
	U.[MobilePhone],
	U.[OtherPhone], 
	U.[ImageId],
	NULL Notes, 
	M.Email, 
	(CASE WHEN Isnull(SU.IsSystemUser, 0) = 1 THEN 3 ELSE 2 END) ContactType, 
	(CASE WHEN Isnull(SU.IsSystemUser, 0) = 1 THEN 7 ELSE 8 END) ContactSourceId,
	U.CreatedBy,
	U.CreatedDate,
	U.ModifiedBy,
	U.ModifiedDate
FROM        
	USUser U 
	INNER JOIN USMemberShip M ON M.UserId = U.Id 
	LEFT JOIN USSiteUser SU ON U.Id = SU.UserId
	LEFT JOIN dbo.USCommerceUserProfile CUP ON CUP.Id = U.Id 
WHERE  U.Status !=3  and   CUP.Id IS NULL
GO
PRINT 'Altering procedure Campaign_GetCampaignHistoryByUser'
GO
ALTER PROCEDURE [dbo].[Campaign_GetCampaignHistoryByUser]
(
	 @UserId UNIQUEIDENTIFIER 
	,@LastNoOfDays INT
	,@PageSize INT = NULL
	,@PageNumber INT = NULL
	,@SortColumn NVARCHAR(25) = NULL
	,@ApplicationId UNIQUEIDENTIFIER
)
AS

	IF(ISNULL(@PageSize,0)= 0 AND ISNULL(@PageNumber,0) =0)
	BEGIN 
		
		SELECT Row_Id,newid() Id, a.CampaignId,b.Title, a.Sends, a.Opens, a.Clicks, a.LastSendDate, a.IsUnsubscribed
		FROM (
			SELECT ROW_NUMBER() OVER(ORDER BY a.CampaignId) as Row_Id ,a.CampaignId, COUNT(*) 'Sends', SUM(a.Opens) 'Opens', SUM(a.Clicks) 'Clicks', MAX(a.SendDate) 'LastSendDate', CAST(CASE WHEN uu.Id IS NULL OR r.Id IS NOT NULL THEN 0 ELSE 1 END AS BIT) 'IsUnsubscribed'
			FROM MKEmailSendLog a  
				LEFT JOIN USUserUnsubscribe uu on a.UserId = uu.UserId AND (uu.CampaignId IS NULL OR uu.CampaignId = a.CampaignId)
				LEFT JOIN USResubscribe r on a.UserId = r.UserId AND a.CampaignId = r.CampaignId
			WHERE a.UserId=@UserId AND (@ApplicationId IS NULL OR a.ApplicationId=@ApplicationId)
			GROUP BY a.CampaignId, uu.Id, r.Id
		) a , MKCampaign b 
		WHERE a.CampaignId = b.Id  AND (@ApplicationId IS NULL OR b.ApplicationId=@ApplicationId) 
		ORDER BY CASE ISNULL(@SortColumn ,'')
			WHEN 'Name' THEN 3  WHEN 'Sends' THEN Sends WHEN 'Opens' THEN Opens WHEN 'Clicks' THEN Clicks ELSE 3 END

	END
	ELSE 
	BEGIN

		DECLARE @RecordCount INT
		DECLARE @HistroyTable Table (
			 Row_id INT
			,Id UNIQUEIDENTIFIER
			,CampaignId UNIQUEIDENTIFIER
			,Name NVARCHAR(1024)
			,Sends INT
			,Opens INT
			,Clicks INT
		)


		INSERT INTO @HistroyTable 
		SELECT Row_Id, NEWID() Id, a.CampaignId, b.Title, a.Sends, a.Opens,a.Clicks FROM (
			SELECT ROW_NUMBER() OVER(ORDER BY a.CampaignId) AS Row_Id, a.CampaignId, COUNT(*) 'Sends', SUM(a.Opens) 'Opens', SUM(a.Clicks) 'Clicks'
			FROM MkEmailSendLog a 
			WHERE a.SendDate >= DATEADD(DAY, -(@LastNoOfDays), GETUTCDATE()) AND a.UserId = @UserId  AND (@ApplicationId IS NULL OR a.ApplicationId=@ApplicationId)
			GROUP BY a.CampaignId
		) a, MKCampaign b 
		WHERE a.CampaignId = b.Id AND (@ApplicationId IS NULL OR b.ApplicationId=@ApplicationId)
		
		SELECT @RecordCount = COUNT(*) FROM @HistroyTable

		SELECT *, @RecordCount AS RecordCount 
		FROM @HistroyTable 
		WHERE Row_ID >= (@PageSize * @PageNumber) - (@PageSize -1) AND Row_ID <= @PageSize * @PageNumber
		ORDER BY CASE ISNULL(@SortColumn ,'')
			WHEN 'Name' THEN 3  WHEN 'Sends' THEN Sends WHEN 'Opens' THEN Opens WHEN 'Clicks' THEN Clicks ELSE 3 END

	END


GO
PRINT 'Altering procedure CampaignTestEmailDto_Get'
GO
ALTER procedure CampaignTestEmailDto_Get
as
begin
	--delete from MKCampaignTestEmail where ProcessedDateTime is not null
	
	declare @TabEmails as table (id uniqueidentifier)
	insert into @TabEmails
	select Id from MKCampaignTestEmail where ProcessedDateTime is null

	update MKCampaignTestEmail set ProcessedDateTIme= GETUTCDATE() where Id in (select Id from @TabEmails)

	SELECT * from MKCampaignTestEmail where Id in (select Id from @TabEmails)
	
	
end
GO
PRINT 'Altering procedure CampaignEmail_GetBySendId'
GO
ALTER PROCEDURE [dbo].[CampaignEmail_GetBySendId]
	@SendId		uniqueidentifier,
	@IsTestEmail bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	if(@IsTestEmail = 1)
	Begin
		SELECT		Id CampaignRunId, ContactId UserId, EmailSubject, EmailHtml
		FROM		MKCampaignTestEmail 
		WHERE		Id = @SendId
	end
	else
	begin
		SELECT		sl.CampaignRunId, sl.UserId, rh.EmailSubject, rh.EmailHtml
		FROM		MKEmailSendLog AS sl
					INNER JOIN MKCampaignRunHistory AS rh ON rh.Id = sl.CampaignRunId
		WHERE		sl.Id = @SendId
	end
END

GO
PRINT 'Altering procedure Campaign_IsScheduleCompleted'
GO
ALTER PROCEDURE [dbo].[Campaign_IsScheduleCompleted]
(
	@Id uniqueidentifier
)
AS
Begin
	Declare
		@ScheduleId uniqueidentifier,
		@RunCount int,
		@MaxOccurrence int,
		@NextRunTime datetime,
		@IsCompleted bit

	Set @IsCompleted = 0

	Select @ScheduleId = ScheduleId From MKCampaignAdditionalInfo where CampaignId = @Id
	Select @MaxOccurrence = MaxOccurrence, @NextRunTime = NextRunTime From TASchedule where Id = @ScheduleId

	SET @RunCount = (SELECT	COUNT(Id) AS RunCount FROM	MKCampaignRunHistory WHERE	CampaignId = @id)

	If IsNull(@MaxOccurrence, 0) > 0 And @RunCount >= IsNull(@MaxOccurrence, 0)
		Set @IsCompleted = 1
	If @IsCompleted = 0 And @NextRunTime is null
		Set @IsCompleted = 1

	Select @IsCompleted 'IsCompleted'
End

GO
PRINT 'Altering procedure CampaignContact_Fill'
GO
ALTER PROCEDURE [dbo].[CampaignContact_Fill]
(
	@SiteId					uniqueidentifier,
	@CampaignId				uniqueidentifier,
	@CampaignRunId			uniqueidentifier,
	@EmailPerCampaignLimit	int
)
AS
BEGIN
	DECLARE @UniqueRecipientsOnly bit,
		@AutoUpdateLists bit,
		@LastPublishDate datetime,
		@listRecs int,
		@totalRecs int,
		@listId uniqueidentifier,
		@GetCurrentRecipients bit,
		@SendToNotTriggered bit,
		@CurrentUtcDateTime datetime, 
		@UseLocalTimeZoneProcessing bit

	DECLARE @CanPopulateWorkTable INT
	EXEC CampaignContact_VerifyFillable @CampaignId,@CampaignRunId, @CanPopulateWorkTable OUTPUT

	IF (@CanPopulateWorkTable = 0)
	BEGIN
		PRINT 'CampaignContact_Fill No valid campaign or response to fill contacts'
		RETURN;
	END

	DECLARE @TabProcessableSites TABLE(SiteId uniqueidentifier, Id int identity(1,1))
	SET @CurrentUtcDateTime = GetUtcDate()
	SET @UseLocalTimeZoneProcessing = 0

	IF EXISTS (SELECT 1 FROM MKCampaignAdditionalInfo Where UseLocalTimeZoneToSend = 1 AND CampaignId=@CampaignId)
	BEGIN
		PRINT 'Timezone is set to 1 for campaign '
		SET @UseLocalTimeZoneProcessing = 1
	END

	IF (@UseLocalTimeZoneProcessing = 1)
	BEGIN
		PRINT 'Gettings sites from timezone table'
		--Get the list of processable sites so that only contact belonging to those sites will be retrived
		INSERT INTO @TabProcessableSites (SiteId)
		SELECT SiteId FROM MKCampaignRunTimeZoneSites
			WHERE CampaignRunId = @CampaignRunId AND StartTime <= @CurrentUtcDateTime AND IsProcessed = 0

		--Updating the status to IsProcessed for those sites in the timezone table
		UPDATE MKCampaignRunTimeZoneSites SET IsProcessed = 1 
			WHERE SiteId in (SELECT SiteId from @TabProcessableSites) AND CampaignRunId=@CampaignRunId
	END

	print 'getting contacts for sites'
	DECLARE @tblContacts TABLE
	(
		Id uniqueidentifier,
		UserId uniqueidentifier,
		FirstName varchar(1024) null,
		MiddleName varchar(1024) null,
		LastName varchar(1024) null,
		CompanyName varchar(1024) null,
		BirthDate datetime null,
		Gender varchar(1024) null,
		AddressId uniqueidentifier null,
		Status int null,
		HomePhone varchar(1024) null,
		MobilePhone varchar(1024) null,
		OtherPhone varchar(1024) null,
		ImageId uniqueidentifier null,
		Notes varchar(max) null,
		Email varchar(256),
		ContactType int,
		ContactSourceId int,
		TotalRecords int null
	)

	--Populate the worktable
	SET @GetCurrentRecipients = 1

	--If the campaign is shared and to be sent to contacts of a variant site, then this should be changed to resolve the applications
	--select @ApplicationId = ApplicationId from mkcampaign Where Id = @CampaignId

	-- get the options for list selection
	SELECT TOP 1 @UniqueRecipientsOnly = UniqueRecipientsOnly,
		@AutoUpdateLists = AutoUpdateLists, 
		@LastPublishDate = IsNull(LastPublishDate, '2009-01-01'),
		@SendToNotTriggered = SendToNotTriggered
	FROM MKCampaignAdditionalInfo
		WHERE CampaignId = @CampaignId

	IF @AutoUpdateLists = 1
	BEGIN
		--TODO: For the timezone specific site we need to verify whether any sent data is available.
		IF EXISTS (SELECT TOP 1 * From MKEmailSendLog WHERE CampaignId = @CampaignId And SendDate > @LastPublishDate)
			SET @GetCurrentRecipients = 0
	END

	IF @GetCurrentRecipients = 1
	BEGIN
		--Get the distribution lists either the campaign always updates or this is the first time

		--Get the contacts for Manual Lists
		INSERT INTO  @tblContacts 
		SELECT C.[UserId] Id
			,C.[UserId]
			,C.[FirstName]
			,C.[MiddleName]
			,C.[LastName]
			,C.[CompanyName]
			,dbo.ConvertTimeFromUtc(BirthDate, @SiteId)BirthDate 
			,C.[Gender]
			,C.[AddressId]
			,C.[Status]
			,C.[HomePhone]
			,C.[MobilePhone]
			,C.[OtherPhone]
			,C.ImageId 
			,C.[Notes]
			,C.[Email]
			,C.[ContactType]
			,C.[ContactSourceId]
			,0  
		FROM MKCampaignDistributionList CDL 
			JOIN TADistributionListUser DLU ON CDL.DistributionListId=DLU.DistributionListId and ContactListSubscriptionType != 3
			JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
			JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id
			JOIN vw_contactsite CS ON CS.UserId = C.UserId
		WHERE CDL.CampaignId = @CampaignId AND DL.ListType = 0
			AND (@UseLocalTimeZoneProcessing = 0 OR CS.SiteId in (select SiteId from  @TabProcessableSites))
		--TODO: Check for primary site??

		--Get all the contacts for AUTO LIST
		DECLARE @autoListIds TABLE (Id uniqueidentifier, RowNum int)

		INSERT INTO @autoListIds
		SELECT DistributionListId, ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
			FROM MKCampaignDistributionList CDL 
				JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			WHERE CampaignId = @CampaignId  AND DL.ListType = 1 --needs to be verified.
		
		DECLARE @cnt int, @TempId uniqueidentifier, @ProcessableSiteCount int, @LocalSiteCounter int,@LocalSiteId uniqueidentifier
		SELECT @cnt = MAX(RowNum) FROM @autoListIds
		SELECT @ProcessableSiteCount = Count(1) FROM @TabProcessableSites

		WHILE (@cnt >0)
		BEGIN
			SELECT @TempId = Id FROM @autoListIds WHERE RowNum = @cnt

			DECLARE @queryXml xml, @TotalRecords int 
			SELECT @queryXml = SearchXml FROM dbo.TADistributionListSearch a, CTSearchQuery b  
					WHERE b.Id = a.SearchQueryId AND a.DistributionListId = @TempId 
				
			IF (@UseLocalTimeZoneProcessing = 1 )
			BEGIN
				SET @LocalSiteCounter = @ProcessableSiteCount
				WHILE (@LocalSiteCounter > 0)
				BEGIN
					print 'getting contacts for dynamic list with timezone'
					--Get the site id to process for dynamic list
					SELECT @LocalSiteId = SiteId FROM @TabProcessableSites WHERE @LocalSiteCounter = Id

					INSERT INTO @tblContacts
					EXEC Contact_SearchContact 
						@Xml = @queryXml,
						@ApplicationId = @LocalSiteId, 
						@MaxRecords = 0, 
						@ContactListId = @TempId, 
						@TotalRecords = @TotalRecords output,  
						@IncludeAddress = 0

					SET @LocalSiteCounter = @LocalSiteCounter - 1
				END
			END
			ELSE
			BEGIN
					print 'getting contacts for dynamic list without timezone'
					INSERT INTO @tblContacts
					EXEC Contact_SearchContact  
						@Xml = @queryXml,
						@ApplicationId = @SiteId, 
						@MaxRecords = 0, 
						@ContactListId = @TempId, 
						@TotalRecords=@TotalRecords output,  
						@IncludeAddress = 0
			END

			SET @cnt = @cnt - 1
		end
		
		print 'getting contacts that are manually added to campaign'
		-- Get all the contacts that are manually added to the campaign
		INSERT @tblContacts
		(
			Id
			,UserId
			,FirstName
			,MiddleName
			,LastName
			,CompanyName
			,BirthDate
			,Gender
			,AddressId
			,Status
			,HomePhone
			,MobilePhone
			,OtherPhone
			,ImageId
			,Notes
			,Email
			,ContactType
			,ContactSourceId
		)
		SELECT NewId(), 
			c.UserId, 
			c.FirstName, 
			c.MiddleName, 
			c.LastName, 
			c.CompanyName, 
			c.BirthDate,
			c.Gender, 
			c.AddressId, 
			c.Status, 
			c.HomePhone, 
			c.MobilePhone, 
			c.OtherPhone, 
			c.ImageId, 
			c.Notes, 
			c.Email, 
			c.ContactType,
			c.ContactSourceId
		FROM vw_contacts c
			JOIN MKCampaignUser cu on cu.UserId = c.UserId AND cu.CampaignId = @CampaignId
			JOIN vw_contactsite CS ON CS.UserId = C.UserId
		WHERE (@UseLocalTimeZoneProcessing = 0 OR CS.SiteId in (SELECT SiteId FROM @TabProcessableSites))
	END
	ELSE
	BEGIN
		INSERT @tblContacts
		(
			Id
			,UserId
			,FirstName
			,MiddleName
			,LastName
			,CompanyName
			,BirthDate
			,Gender
			,AddressId
			,Status
			,HomePhone
			,MobilePhone
			,OtherPhone
			,ImageId
			,Notes
			,Email
			,ContactType
			,ContactSourceId
		)
		SELECT NewId(), 
			c.UserId, 
			c.FirstName, 
			c.MiddleName, 
			c.LastName, 
			c.CompanyName, 
			c.BirthDate,
			c.Gender, 
			c.AddressId, 
			c.Status, 
			c.HomePhone, 
			c.MobilePhone, 
			c.OtherPhone,
			c.ImageId, 
			c.Notes, 
			c.Email, 
			c.ContactType,
			c.ContactSourceId
		FROM vw_contacts c
			JOIN vw_contactsite CS ON CS.UserId = C.UserId
			JOIN mkEmailSendLog sl ON sl.UserId = c.UserId 
				AND sl.CampaignId = @CampaignId
				AND sl.SendDate > @LastPublishDate
		WHERE (@UseLocalTimeZoneProcessing = 0 OR CS.SiteId in (SELECT SiteId FROM @TabProcessableSites))
	END

	-- remove users that have triggered watches
	IF IsNull(@SendToNotTriggered, 0) = 1
	BEGIN
		DELETE @tblContacts WHERE UserId IN (
			SELECT DISTINCT l.UserId FROM mkemailsendactions a
				JOIN mkemailsendlog l ON a.SendId = l.Id
				JOIN mkcampaignrelevantwatches w ON w.CampaignId = l.CampaignId AND w.WatchId = a.TargetId
			WHERE a.actiontype = 5 AND l.CampaignId = @CampaignId)

			print 'deleting contact from @tblContacts where @SendToNotTriggered is 1'
	END

	print 'populating the worktable'

	;WITH CTE AS
	(
		SELECT ROW_NUMBER() OVER(PARTITION BY C.UserId ORDER BY [Email]) AS RowNumber,
			C.UserId, 
			FirstName , 
			MiddleName ,
			LastName ,
			CompanyName ,
			BirthDate, 
			Gender,
			AddressId, 
			Status,
			HomePhone,
			MobilePhone, 
			OtherPhone, 
			Notes,
			Email
		FROM @tblContacts C 
			LEFT OUTER JOIN MKEmailSendLog L on L.UserId = C.UserId AND L.CampaignId = @CampaignId
			LEFT OUTER JOIN USUserUnsubscribe U on U.UserId = C.UserId AND (@CampaignId IS NULL OR U.CampaignId = @CampaignId)
			LEFT OUTER JOIN USResubscribe R on R.UserId = C.UserId AND R.CampaignId = @CampaignId
		WHERE (U.Id IS NULL OR R.Id IS NOT NULL) AND
			C.Status = 1 AND
			(@UniqueRecipientsOnly = 0 OR (@UniqueRecipientsOnly = 1 and L.Id is null)) AND
			C.UserId  NOT IN (SELECT UserId from MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId)
	)

	INSERT INTO MKCampaignRunWorkTable 
	(
		CampaignRunId, 
		UserId, 
		FirstName, 
		MiddleName, 
		LastName,
		CompanyName, 
		BirthDate, 
		Gender, 
		AddressId, 
		Status, 
		HomePhone, 
		MobilePhone, 
		OtherPhone,
		Notes, 
		Email
	)
	SELECT  TOP(@EmailPerCampaignLimit) 
		@CampaignRunId,
		UserId, 
		FirstName , 
		MiddleName ,
		LastName ,
		CompanyName ,
		BirthDate, 
		Gender,
		AddressId, 
		Status,
		HomePhone,
		MobilePhone, 
		OtherPhone, 
		Notes,
		Email
	FROM CTE
	WHERE RowNumber = 1
		
	--Remove contacts that are already sent for the campaignRunId in case of partiallysent campaigns
	DELETE FROM MKCampaignRunWorkTable WHERE UserId IN
		(SELECT UserId FROM MKEmailSendLog WHERE CampaignRunId = @CampaignRunId)
			
	--select count(*) 'TotalContacts' from MKCampaignRunWorkTable where CampaignRunId = @CampaignRunId
END



GO
PRINT 'Altering procedure Users_ImportBatch'
GO
ALTER PROCEDURE [dbo].[Users_ImportBatch]
    (
      @OverwriteExistingContacts BIT ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @BatchId UNIQUEIDENTIFIER ,
      @BatchUserTable VARCHAR(250) ,
      @BatchMembershipTable VARCHAR(250) ,
      @BatchProfileTable VARCHAR(250) ,
      @BatchSiteUserTable VARCHAR(250) ,
      @BatchUserDistributionListTable VARCHAR(250) ,
      @BatchUSMarketierUserTable VARCHAR(250) ,
      @BatchIndexTermTable VARCHAR(250) ,
      @BatchAddressTable VARCHAR(250) ,
      @OverrideWSUSersAndCustomers BIT = 0
    )
AS 
    BEGIN
        DECLARE @BatchTablePrefix VARCHAR(250) ,
            @BatchIdMapTableName VARCHAR(250) ,
            @StrModifiedBy VARCHAR(36) ,
            @ImportedContacts INT ,
            @AddedToDistributionList INT ,
            @ExistingContacts INT ,
            @UpdatedContacts INT
		
        SET @AddedToDistributionList = 0
        SET @ImportedContacts = 0
        SET @UpdatedContacts = 0

		-- Clean up any records that may have have been marked active from a bad import from launch
		UPDATE  U
		SET     U.STATUS = 3
		FROM    dbo.USUser U
        INNER JOIN dbo.MKContact C ON C.Id = U.Id
		WHERE   U.Status = 1

	-- set up temp table name for batch import user id mapping
        SET @BatchTablePrefix = 'ZTMP' + REPLACE(CONVERT(VARCHAR(36), @BatchId),
                                                 '-', '')
        SET @BatchIdMapTableName = @BatchTablePrefix + '_BatchImportIdMap'
        SET @StrModifiedBy = CONVERT(VARCHAR(36), @ModifiedBy)
	
	--start delete duplicate records within the temporary tables if any
        EXEC('delete from ' + @BatchUserTable + '  where ID in (select ID from
        (Select Id, row_number()over(partition by Email order by CreatedDate) as rnum--,Email,FirstName,LastActivityDate
        from ' + @BatchUserTable + '
        Where Email in (
        select Email
        from ' + @BatchUserTable + ' U
        Group by Email
        Having count(*)>1
        )) Duplicates where rnum >1)')
	
        EXEC ('delete from ' + @BatchMembershipTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchProfileTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchSiteUserTable + ' where ContactId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUserDistributionListTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
        EXEC ('delete from ' + @BatchUSMarketierUserTable + ' where UserId not in (select Id from '+@BatchUserTable+')')	
        EXEC ('delete from ' + @BatchAddressTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	--end delete duplicate records within the temporary tables if any


        IF EXISTS ( SELECT  *
                    FROM    sysobjects
                    WHERE   name = @BatchIdMapTableName ) 
            EXEC('Drop Table ' + @BatchIdMapTableName)

	-- Step 1 locate existing records and create mapping
        EXEC(
        'Create Table ' + @BatchIdMapTableName + '(' +
        'importId UniqueIdentifier, ' +
        'actualId UniqueIdentifier, ' +
        'importAddressId UniqueIdentifier, ' +
        'actualAddressId uniqueidentifier ' +
        ')'
        )

        PRINT @BatchIdMapTableName

        EXEC(
        'Create Index ' + @BatchTablePrefix + '_MappingImportId On ' + @BatchIdMapTableName + '(importId)'
        )

        PRINT @BatchTablePrefix 

        PRINT '1a: locate existing users and populate id mapping'
            + @BatchUSMarketierUserTable
	-- 1a: locate existing users and populate id mapping
        EXEC(
        'Insert Into ' + @BatchIdMapTableName + ' (importId, actualId, importAddressId, actualAddressId) ' +
        'Select z.Id, m.UserId, z.AddressId, m.AddressId ' +
        'From ' + @BatchUserTable + ' z ' +			
        'Join vw_contacts m on m.Email = z.Email ' 
			
        )

        PRINT @BatchMembershipTable 
        PRINT '1b: update batch table user id fields to the existing user''s id'

	-- 1b: update batch table user id fields to the existing user's id
        EXEC(
        'Update ' + @BatchUserTable + ' Set ' +
        'ID = m.actualId ' +
        'From ' + @BatchUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.Id'
        )
		
        PRINT @BatchAddressTable
        EXEC(
        'Update ' + @BatchAddressTable + ' Set ' +
        'ID = isnull(m.actualAddressId, m.importAddressId), UserId = m.actualId ' +
        'From ' + @BatchAddressTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 

        EXEC(
        'Update ' + @BatchMembershipTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchMembershipTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchIdMapTableName 


        EXEC(
        'Update ' + @BatchProfileTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchProfileTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchProfileTable

        EXEC(
        'Update ' + @BatchSiteUserTable + ' Set ' +
        'ContactId = m.actualId ' +
        'From ' + @BatchSiteUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.ContactId'
        )

        PRINT @BatchSiteUserTable

        EXEC(
        'Update ' + @BatchUSMarketierUserTable + ' Set ' +
        'UserId = m.actualId, ' +
        'AddressId = m.actualAddressId ' +
        'From ' + @BatchUSMarketierUserTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUSMarketierUserTable


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        'UserId = m.actualId ' +
        'From ' + @BatchUserDistributionListTable + ' z ' +
        'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
        )

        PRINT @BatchUserDistributionListTable

	-- Insert Index term
        DECLARE @idxTerms INT
        CREATE TABLE #tmpIdxTerms ( recCount INT )

        EXEC('insert into #tmpIdxTerms (recCount) select count(*) from ' + @BatchIndexTermTable)

        SELECT  @idxTerms = recCount
        FROM    #tmpIdxTerms
        IF @idxTerms > 0 
            BEGIN
                EXEC Users_ImportIndexTerms @BatchIndexTermTable,
                    @BatchSiteUserTable
            END
        DROP TABLE #tmpIdxTerms

	-- To get existing records count
        DECLARE @ParmDefinition NVARCHAR(100) ;
        DECLARE @ExistingContactsStr NVARCHAR(30)
        DECLARE @query NVARCHAR(500)
        
        SET @query = 'select @result =count(U.Id) from MKContact U inner join '
            + @BatchUserTable + ' z on z.id = u.id'
        SET @ParmDefinition = N'@result varchar(30) OUTPUT' ;	 
        EXEC sp_executesql @query, @ParmDefinition,
            @result = @ExistingContactsStr OUTPUT
        SET @ExistingContacts = CAST(@ExistingContactsStr AS INT)
        
        PRINT 'remove system users'
-- remove system users
        PRINT 'delete batchusertable system users'
        EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchMembershipTable system users'
        EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchProfileTable system users'
        EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchSiteUserTable system users'
        EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchUserDistributionListTable system users'
        EXEC ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchUSMarketierUserTable system users'
        EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
        PRINT 'delete @BatchAddressTable system users'
        EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')

-- remove commerce users
        PRINT 'remove commerce users'
        SELECT  *
        FROM    uscommerceuserprofile
        IF ( @OverrideWSUSersAndCustomers <> 1 ) 
            BEGIN
                PRINT 'DELETE @BatchUserTable'
                EXEC ('Delete ' + @BatchUserTable + ' Where Id in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchMembershipTable'
                EXEC ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchProfileTable'
                EXEC ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchSiteUserTable'
                EXEC ('Delete ' + @BatchSiteUserTable + ' Where ContactId in(select userid from vw_contacts where contactType in(1,2))')
--exec ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchUSMarketierUserTable'
                EXEC ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
                PRINT 'DELETE @BatchAddressTable'
                EXEC ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
            END
	
        PRINT 'Step 2: Update records for existing users'
	-- Step 2: Update records for existing users
        IF ( @OverwriteExistingContacts = 1
             OR @OverrideWSUSersAndCustomers = 1
           ) 
            BEGIN
		-- Step 2a: User Record
                EXEC(
                'Update MKContact Set ' +
                'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
                'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
                'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone,AddressId=z.AddressId, '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From MKContact u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id'
                )
			
                SET @UpdatedContacts = (SELECT @@ROWCOUNT)
            
                PRINT 'Step 2a Must update User records outside of MKContact'
		--Step 2a Must update User records outside of MKContact
                EXEC(
                'Update USUser Set ' +
                'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
                'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
                'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone, '+
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
                'From USUser u ' +
                'join ' + @BatchUserTable + ' z on z.id = u.id ' +
				'LEFT JOIN MKContact MK ON MK.Id = U.Id WHERE MK.Id IS NULL'
                )
			
                SET @UpdatedContacts = ( SELECT @@ROWCOUNT) + @UpdatedContacts
                SET @ImportedContacts = ( @ImportedContacts + @UpdatedContacts )
		
		
		
		--SET @ImportedContacts = (SELECT @@ROWCOUNT)
		-- Step 2b: Profile Record
		-- update profile properties that exist
		
		-- THIS NEEDS TO BE MOVED TO ATTRIBUTE MODEL LOGIC
		
		--Exec(
		--	'Update USMemberProfile Set ' +
		--		'PropertyValueString = z.PropertyValueString, ' +
		--		'PropertyValuesBinary = z.PropertyValuesBinary, ' +
		--		'LastUpdatedDate = z.LastUpdatedDate ' +
		--	'From USMemberProfile u ' +
		--		'Join ' + @BatchProfileTable + ' z on z.userid = u.userid and z.PropertyName = u.PropertyName'
		--	)
		
	
		
		
		
		
                PRINT 'UPDATE GLADDRESS'
                EXEC(
                'Update GLAddress Set ' +
                'AddressType = z.AddressType, ' + 
                'AddressLine1 = z.AddressLine1, ' +
                'AddressLine2 = z.AddressLine2, ' +
                'City = z.City, ' +
                'StateId = s.Id, ' +
                'Zip = z.Zip, ' +
                'CountryId = c.Id, ' +
                'ModifiedDate = z.ModifiedDate, ' +
                'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
                'Status = z.Status ' +
                'From GLAddress u ' +
                'join ' + @BatchAddressTable + ' z on z.Id = u.Id ' +
                'join GLState s on s.StateCode = z.State ' +
                'join GLCountry c on c.CountryName = z.Country '
				
                )
		-- Removed by NBOWMAN - new contact batch table should have address ID in it
		--Exec('Update MKContact Set ' +
		--	'AddressId=z.AddressId '+			
		--	'From MKContact u ' +
		--		'join ' + @BatchUSMarketierUserTable + ' z on z.Userid = u.Id')
				
            END
        PRINT 'Insert Records for users that do not exist'
	-- Step 3: Insert Records for users that do not exist
	-- Step 3a: User record
        EXEC(
        'INSERT INTO dbo.MKContact
        ( Id ,
        Email ,
        FirstName ,
        MiddleName ,
        LastName ,
        CompanyName ,
        Gender ,
        BirthDate ,
        HomePhone ,
        MobilePhone ,
        OtherPhone ,
        ImageId ,
        AddressId ,
        Notes ,
        ContactSourceId ,
        Status ,
        CreatedBy ,
        CreatedDate ,
        ModifiedBy ,
        ModifiedDate ,
        LastSynced
        ) ' +
        'select 
        z.Id ,
        z.Email ,
        z.FirstName ,
        z.MiddleName ,
        z.LastName ,
        z.CompanyName ,
        z.Gender ,
        z.BirthDate ,
        z.HomePhone ,
        z.MobilePhone ,
        z.OtherPhone ,
        z.ImageId ,
        z.AddressId ,
        z.Notes ,
        z.ContactSourceId ,
        z.Status ,
        z.CreatedBy ,
        z.CreatedDate ,
        z.ModifiedBy ,
        ISNULL(z.ModifiedDate, Z.CreatedDate) ,
        z.LastSynced ' +
        'from ' + @BatchUserTable + ' z ' +
        'Left Outer Join vw_contacts u on u.UserId = z.Id ' +
        'Where u.UserId is null'
        )
        SET @ImportedContacts = ( @ImportedContacts + ( SELECT
                                                              @@ROWCOUNT
                                                      ) )
        PRINT 'Insert into GLAddress'
        EXEC('insert into GLAddress(Id, AddressType, AddressLine1, AddressLine2, City, StateId, Zip, CountryId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status) ' +
        'select ISNULL(z.Id, NEWID()), z.AddressType, z.AddressLine1, z.AddressLine2, z.City, s.Id, z.Zip, c.Id, z.CreatedDate, z.CreatedBy, null, null, z.Status ' +
        'from ' + @BatchAddressTable + ' z ' +
        'left outer join GLCountry c on c.CountryName = z.Country ' +
        'left outer join GLState s on s.StateCode = z.State and s.CountryId = c.Id ' +
        'left outer join GLAddress a on a.Id = z.Id ' +
        'Where a.Id is null '
        )

        EXEC ('UPDATE MKContact SET AddressId = ISNULL(z.Id, NEWID()) ' + 
        'from  ' + @BatchAddressTable + '  z  '+
        'INNER Join MKContact u on u.Id = z.userId ')

	-- Step 3b: Membership Record
	--Exec(
	--	'insert into USMembership (UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, ' +
	--			'Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved,  ' +
	--			'IsLockedOut, LastLoginDate, LastPasswordChangedDate, LastLockoutDate,  ' +
	--			'FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart,  ' +
	--			'FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart) ' +
	--	'select z.UserId, z.Password, z.PasswordFormat, z.PasswordSalt, z.MobilePIN, ' +
	--			'z.Email, z.LoweredEmail, z.PasswordQuestion, z.PasswordAnswer, z.IsApproved,  ' +
	--			'z.IsLockedOut, z.LastLoginDate, z.LastPasswordChangedDate, z.LastLockoutDate,  ' +
	--			'z.FailedPasswordAttemptCount, z.FailedPasswordAttemptWindowStart,  ' +
	--			'z.FailedPasswordAnswerAttemptCount, z.FailedPasswordAnswerAttemptWindowStart ' +
	--	'from ' + @BatchMembershipTable + ' z ' +
	--		'left outer join USMembership u on u.UserId = z.UserId ' +
	--	'where u.UserId is null '
	--	)

	-- Step 3c: Profile Record
	
	--THIS NEEDS TO BE UPDATED TO ATTRIBUTE MODEL
	
	--Exec(
	--	'insert into USMemberProfile (UserId, PropertyName, PropertyValueString, ' +
	--		'PropertyValuesBinary, LastUpdatedDate) ' +
	--	'select z.UserId, z.PropertyName, z.PropertyValueString, z.PropertyValuesBinary, ' +
	--		'z.LastUpdatedDate ' +
	--	'from ' + @BatchProfileTable + ' z ' +
	--		'left outer join USMemberProfile u on u.UserId = z.UserId and u.PropertyName = z.PropertyName ' +
	--	'where u.PropertyName is null '
	--	)


	---------------------ATTRIBUTE INSERT-----------------------
        EXEC(
        'SELECT * FROM '+@BatchProfileTable+'')
		
             
                    
        CREATE TABLE #tmpContactAttributes
            (
              AttributeId UNIQUEIDENTIFIER ,
              AttributeEnumId UNIQUEIDENTIFIER ,
              VALUE NVARCHAR(4000) ,
              UserId UNIQUEIDENTIFIER
            )
                    
                    
        PRINT 'insert into #tmpContactAttributes'
        INSERT  INTO #tmpContactAttributes
                EXEC
                    ( '	 SELECT  
                A.Id AS AttributeId ,
                AE.Id AS AttributeEnumId ,
                P.PropertyValueString AS Value ,
                P.UserId                 
                FROM   ' + @BatchProfileTable
                      + ' P
                INNER JOIN dbo.ATAttribute A ON A.Title = P.PropertyName
                INNER JOIN dbo.ATAttributeCategoryItem ACI ON ACI.AttributeId = A.Id
                INNER JOIN dbo.ATAttributeCategory AC ON AC.ID = ACI.CategoryId
                AND AC.Name = ''Contact Attributes''
                LEFT JOIN dbo.ATAttributeEnum AE ON AE.AttributeID = A.Id
                AND AE.Title = P.PropertyValueString
                WHERE  ( A.IsEnum = 1
                AND AE.ID IS NOT NULL
                )
                OR A.IsEnum = 0'
                    )
 
 
        DELETE  CAV
        FROM    dbo.ATContactAttributeValue CAV
                INNER JOIN #tmpContactAttributes TCA ON TCA.AttributeId = CAV.AttributeId
                                                        AND TCA.UserId = CAV.ContactId
 
        INSERT  INTO dbo.ATContactAttributeValue
                ( Id ,
                  ContactId ,
                  AttributeId ,
                  AttributeEnumId ,
                  Value ,
                  Notes ,
                  CreatedDate ,
                  CreatedBy
         
                        
                )
                SELECT  NEWID() ,
                        UserId ,
                        AttributeId ,
                        AttributeEnumId ,
                        Value ,
                        '' ,
                        GETUTCDATE() ,
                        '6CB2B06E-C6D6-455B-BBEA-E4162A7692D3'
                FROM    #tmpContactAttributes


		
		
		-------------------------------------------------------------




        PRINT ' Step 3d: Site User Record'
	-- Step 3d: Site User Record
        EXEC(
        'insert into MKContactSite (SiteId, ContactId,  Status) ' +
        'select z.SiteId, z.ContactId, 1' +
        'from ' + @BatchSiteUserTable + ' z ' +
        ' inner join MKContact MC ON MC.Id = z.ContactId'+
        ' left outer join MKContactSite u on u.ContactId = z.ContactId ' +
        'where u.ContactId is null '
        )

	-- Step 3e: Distribution List Record

        PRINT 'Update ' + @BatchUserDistributionListTable + ' Set '
            + '  ListId = null ' + 'From ' + @BatchUserDistributionListTable
            + ' z '
            + 'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'


        EXEC(
        'Update ' + @BatchUserDistributionListTable + ' Set ' +
        ' ListId = null ' +
        ' from  ' + @BatchUserDistributionListTable + ' z ' +
        'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'
        )
        SET @AddedToDistributionList = ( SELECT @@ROWCOUNT
                                       )

        PRINT 'insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '
            + ' select newid(),z.ListId,z.UserId ' + 'from '
            + @BatchUserDistributionListTable + ' z '
            + 'Left Outer Join TADistributionListUser u on u.UserId = z.UserId '
            + 'Where u.UserId is null and z.ListId is not null '


        EXEC ('insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '+
        ' select newid(),z.ListId,z.UserId ' + 
        'from ' + @BatchUserDistributionListTable + ' z ' +
        'Join vw_contacts u on u.userid = z.UserId ' +
        'Where z.ListId is not null ')
        SET @AddedToDistributionList = ( @AddedToDistributionList + ( SELECT
                                                              @@ROWCOUNT
                                                              ) )

        INSERT  INTO TADistributionListSite
                ( Id ,
                  DistributionListId ,
                  SiteId
                )
                SELECT  NEWID() ,
                        *
                FROM    ( SELECT    Id ,
                                    ApplicationId
                          FROM      TADistributionLists
                          EXCEPT
                          SELECT    DistributionListId ,
                                    SiteId
                          FROM      TADistributionListSite
                        ) T


        EXEC (' UPDATE TLS
        SET    TLS.COUNT = TV.ContactCount
        FROM   ( SELECT    DLU.DistributionListId ,
        COUNT(DLU.Id) AS ContactCount
        FROM      dbo.TADistributionListUser DLU
        INNER JOIN dbo.TADistributionListSite TLS ON DLU.DistributionListId = TLS.DistributionListId
        GROUP BY  DLU.DistributionListId
        ) TV
        INNER JOIN dbo.TADistributionListSite TLS ON TLS.DistributionListId = TV.DistributionListId
        INNER JOIN ' + @BatchUserDistributionListTable + ' z ON z.ListId = TV.DistributionListId' )

		SET @query = 'select @result =count(U.Id) from USUser U inner join '
            + @BatchUserTable + ' z on z.id = u.id'
        SET @ParmDefinition = N'@result varchar(30) OUTPUT' ;	 
        EXEC sp_executesql @query, @ParmDefinition,
            @result = @ExistingContactsStr OUTPUT
        SET @ExistingContacts = CAST(@ExistingContactsStr AS INT) +@ExistingContacts
	

	-- peform cleanup
	--Exec('Drop Table ' + @BatchIdMapTableName)
--
--	-- empty batch tables

	--Exec('truncate table ' + @BatchUserTable)
	--Exec('truncate table ' + @BatchMembershipTable)
	--Exec('truncate table ' + @BatchProfileTable)
	--Exec('truncate table ' + @BatchSiteUserTable)
	--exec('truncate table ' + @BatchAddressTable)
	--exec('truncate table ' + @BatchUserDistributionListTable)
	--exec('truncate table ' + @BatchUSMarketierUserTable)
	--exec('truncate table ' + @BatchIndexTermTable)
	--exec('truncate table ' + @BatchAddressTable)

        UPDATE  UploadContactData
        SET     ImportedContacts = @ImportedContacts ,
                AddedToDistributionList = @AddedToDistributionList ,
                ExistingRecords = @ExistingContacts ,
                UpdatedContacts = @UpdatedContacts
        WHERE   UploadHistoryId = @BatchId
    END
	GO
	PRINT 'Altering procedure CampaignContact_FillResponse'
GO
ALTER PROCEDURE [dbo].[CampaignContact_FillResponse]
(
	@SiteId				uniqueidentifier,
	@CampaignId			uniqueidentifier,
	@CampaignRunId		uniqueidentifier, 
	@CampaignEmailId	uniqueidentifier
)
AS
BEGIN
	DECLARE @ResponseProviderName varchar(250) 
	DECLARE @ResponseOptions xml 
	
	DECLARE @CanPopulateWorkTable INT
	EXEC CampaignContact_VerifyFillable @CampaignId,@CampaignRunId, @CanPopulateWorkTable OUTPUT

	IF (@CanPopulateWorkTable = 0)
		RETURN;

	SELECT @ResponseProviderName = ResponseProviderName, 
		@ResponseOptions= ResponseProviderOptions  
	FROM MKCampaignResponseInfo 
	WHERE CampaignId = @CampaignId
	
	--Populate the response worktable from which the email processor worktable will be populated.
	IF(@ResponseProviderName = 'LoginResponseProvider')
		EXEC Response_PopulateLoggedIn @CampaignId,@ResponseOptions
	ELSE IF(@ResponseProviderName = 'TriggeredWatchResponseProvider')
		EXEC Response_PopulateTriggeredWatches @CampaignId,@ResponseOptions
	ELSE IF(@ResponseProviderName = 'ProductPurchaseResponseProvider')
		EXEC Response_PopulatePurchasedProducts @CampaignId,@ResponseOptions
	ELSE IF(@ResponseProviderName = 'SubmittedFormResponseProvider')
		EXEC Response_PopulateFormsSubmitted @CampaignId,@ResponseOptions


	;WITH CTE AS
	(
		SELECT ROW_NUMBER() OVER(PARTITION BY C.UserId ORDER BY [Email]) AS RowNumber,
			C.UserId, 
			FirstName , 
			MiddleName ,
			LastName ,
			CompanyName ,
			BirthDate, 
			Gender,
			AddressId, 
			Status,
			HomePhone,
			MobilePhone, 
			OtherPhone, 
			Notes,
			Email
		FROM MKResponseFlow R 
		JOIN vw_contacts C ON C.UserId = R.UserId
	WHERE R.ResponseId = @CampaignId AND
		R.Completed IS NULL AND 
		([dbo].[Campaign_GetResponseSendDate](R.DateStarted, @CampaignRunId) <= GETUTCDATE() AND 
		(R.LastSent IS NULL OR [dbo].[Campaign_GetResponseSendDate](R.DateStarted, @CampaignRunId) > R.LastSent)) AND
		C.UserId  NOT IN (SELECT UserId from MKCampaignRunWorkTable WHERE CampaignRunId = @CampaignRunId) 
	)
	INSERT INTO MKCampaignRunWorkTable 
	(
		CampaignRunId, 
		UserId, 
		FirstName, 
		MiddleName, 
		LastName,
		CompanyName, 
		BirthDate, 
		Gender, 
		AddressId, 
		Status, 
		HomePhone, 
		MobilePhone, 
		OtherPhone,
		Notes, 
		Email
	)
	SELECT   
		@CampaignRunId,
		UserId, 
		ISNULL(FirstName, '') FirstName , 
		ISNULL(MiddleName, '') MiddleName ,
		ISNULL(LastName, '') LastName ,
		ISNULL(CompanyName, '') CompanyName ,
		 ISNULL(BirthDate, '') BirthDate, 
		ISNULL(Gender, '') Gender,
		AddressId, 
		Status,
		ISNULL(HomePhone, '') HomePhone,
		ISNULL(MobilePhone, '') MobilePhone, 
		ISNULL(OtherPhone, '') OtherPhone, 
		ISNULL(Notes, '') Notes,
		ISNULL(Email, '') Email
	FROM CTE
	WHERE RowNumber = 1

END


GO
PRINT 'Creating procedure Campaign_GetCampaignPreviewDataForDashboard'
GO
IF (OBJECT_ID('Campaign_GetCampaignPreviewDataForDashboard') IS NOT NULL)
	DROP PROCEDURE Campaign_GetCampaignPreviewDataForDashboard

GO
CREATE PROCEDURE [dbo].[Campaign_GetCampaignPreviewDataForDashboard]
	@CampaignId UNIQUEIDENTIFIER
AS
BEGIN

	SELECT c.Id, c.Title, c.ModifiedDate, g.Name AS GroupName
	FROM MKCampaign c
		LEFT JOIN MKCampaignGroup g on c.CampaignGroupId = g.Id
	WHERE c.Id=@CampaignId
	
END
GO
PRINT 'Altering procedure CampaignRunHistoryDto_UpdateResponse'
GO
ALTER PROCEDURE [dbo].[CampaignRunHistoryDto_UpdateResponse]
	(
		@Id uniqueidentifier --CampaignRunId
		--This is dependent on MKCampaignRunResponseWorktable. So the response status should be updated on this worktable
	)
AS
BEGIN
	
	Declare @CurrentTimeUtc datetime
	set @CurrentTimeUtc = GetUtcDate()

	;With ResponseCTE (StatusToUpdate,ProcessingCountToUpdate,ContactId,LogMessage)
	As
	(
		SELECT CASE WHEN ResponseStatus = 3 AND C.ProcessingCount<3   THEN 0
		 WHEN ResponseStatus = 2 OR ResponseStatus = 4 THEN ResponseStatus
		 WHEN  C.ProcessingCount>=3  THEN ResponseStatus END StatusToUpdate,
		0,
		 -- CASE WHEN ResponseStatus = 3 AND C.ProcessingCount<3   THEN C.ProcessingCount+1
		 --WHEN ResponseStatus = 2  THEN C.ProcessingCount END  ProcessingCountToUpdate,
		WT.ContactId,
		WT.LogMessage 
		FROM MKCampaignRunResponseWorktable WT JOIN MKCampaignRunWorkTable C ON C.CampaignRunId=WT.CampaignRunId
		WHERE WT.CampaignRunId = @Id

	)
	UPDATE C 
	SET ProcessingStatus = StatusToUpdate,
	C.LogMessage = R.LogMessage
	--ProcessingCount = ProcessingCountToUpdate
	FROM MKCampaignRunWorkTable C JOIN ResponseCTE R ON R.ContactId = C.UserId

	--Updates the run history with sends count
	;With RunHistory_Cte(Sends, CampaignRunId)
	AS
	(
		--Update the campaign run history send count
		SELECT COUNT(ContactId),CampaignRunId FROM MKCampaignRunResponseWorktable 
		Where ResponseStatus = 2 --success response
		Group By CampaignRunId	
	)
	Update H SET H.Sends= H.Sends+C.Sends 
	FROM MKCampaignRunHistory H JOIN RunHistory_Cte C ON H.Id = C.CampaignRunId
	Where C.Sends > 0
	
	--Get the campaign id for the run id
	declare @CampaignId uniqueidentifier ,@type int

	--Get the campaignid for the campaign run id if the type if reponse.
	select top 1 @type = C.Type,@CampaignId = C.Id from MKCampaign C Join MKCampaignRunHistory  CH On C.Id = CH.CampaignId Where CH.Id = @Id

	if(@type = 2) --response
	begin
		--for successful sends and hard error update the last sent date on MKResponseFlow table
		update MKResponseFlow set LastSent = @CurrentTimeUtc,
		Completed = case when dbo.Campaign_VerifyAutoReponsesCompleted(ResponseId,UserId,@CurrentTimeUtc) = 1 then @CurrentTimeUtc  else Completed end
		where UserId in (select UserId from MKCampaignRunWorkTable where CampaignRunId = @Id and (ProcessingStatus = 2 or ProcessingStatus = 4 OR ProcessingCount = 3)) and ResponseId = @CampaignId and Completed is null

	end

	--Update the errored records from the MKCampaignRunWorkTable to MKCampaignRunErrorLog table; success emails are already logged by different process
	INSERT INTO MKCampaignRunErrorLog (Id,
	CampaignId,
	CampaignRunId,
	ContactId,
	ErrorMessage)
	SELECT NEWID(),@CampaignId,@Id,UserId,LogMessage FROM MKCampaignRunWorkTable WHERE CampaignRunId=@Id AND (ProcessingStatus = 4 OR ProcessingCount = 3)

	--in case of AUTO Responders and campaigns , REMOVE all the processed contacts; 
	--this deletes all the contacts those are errored out OR those are processed with error after 3 reties OR successfullt sent
	-- Successfully sent statuses are getting updated to MKEmailSendLog table using different procedure. 
	DELETE FROM  MKCampaignRunWorkTable WHERE CampaignRunId=@Id AND (ProcessingStatus = 2 OR ProcessingStatus = 4 OR ProcessingCount = 3)

	--Cleanup the ResponseWorktable
	DELETE FROM MKCampaignRunResponseWorktable WHERE CampaignRunId=@Id

	SELECT @@ROWCOUNT
END
GO
PRINT 'Altering procedure ContactDto_Get'
GO
/*
* This procedure is used for the following purposes
* 1. Gettting contacts (directly from MKContact)
* 2. Getting Contacts for the CampaignRun (email processor core logic to get contacts to send emails)
* 3. Getting Contacts for the given ContactList (distribution list); for Manual as well as dynamic lists
* 4. Verifying logic for CampaignRun for email processor;in this case only CampaignRun Id is required.
*/
ALTER PROCEDURE [dbo].[ContactDto_Get]
(
	@Id UNIQUEIDENTIFIER = NULL,
	@Ids varchar(max)=NULL,
	@Status			int = NULL ,
	@SiteId			uniqueidentifier = NULL,
	@CampaignId		uniqueidentifier = null, --Pass only campaignRun id to verify whether all the batches are complete for email processor.
	@CampaignRunId uniqueidentifier = null, -- Pass campaign id along with run id to get the next batches of contacts.
	@EmailPerCampaignLimit INT = NULL, -- Need to be passed;this is obsolete. 
	@ContactListId uniqueidentifier = null, --Will get the contacts for the given list(DL)
	@FormId			uniqueidentifier = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL,
	@MaxRecords		int = NULL,
	@SortBy			nvarchar(100) = NULL,  
	@SortOrder		nvarchar(10) = NULL,
	@Query	NVARCHAR(MAX) = NULL,
	@BatchSize INT = NULL,
	@Email nvarchar(max) = null
)
AS
BEGIN

DECLARE @LoweredEmail nvarchar(max)

IF (@Email IS NOT NULL AND LTRIM(RTRIM(@Email)) <> '')
	SET @LoweredEmail = LOWER(LTRIM(RTRIM(@Email)))
IF (@Ids is NOT NULL)
BEGIN
	IF (@PageNumber IS NOT NULL AND @PageSize IS NOT NULL)
	BEGIN
	WITH CTE AS(
		SELECT ROW_NUMBER() OVER (ORDER BY Email) As RowNumber,
			COUNT(Id) OVER () AS TotalRecords,
			T.*
		FROM(	
		SELECT C.UserId Id, C.*
		FROM 
		vw_contacts C JOIN dbo.SplitGUID(@Ids,',') T ON T.Items = C.UserId
		)T
		)
	Select * 
	FROM CTE
	WHERE RowNumber BETWEEN ((@PageNumber-1) * @PageSize)+1 AND ((@PageNumber) * @PageSize)
	ORDER BY RowNumber
	END
	ELSE
		SELECT C.UserId Id, C.*
		FROM 
		vw_contacts C JOIN dbo.SplitGUID(@Ids,',') T ON T.Items = C.UserId

END
ELSE IF (@CampaignRunId IS NULL OR @CampaignRunId = dbo.GetEmptyGuid())
	BEGIN
		DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1

	
		DECLARE @EmptyGuid uniqueidentifier
		SET @EmptyGuid = dbo.GetEmptyGUID()
	
		DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
		IF @SortBy IS NULL AND @Query IS NOT NULL
		BEGIN
			IF (@ContactListId IS NULL OR @ContactListId =  dbo.GetEmptyGuid())
			INSERT INTO @tbIds
			EXEC sp_executesql @Query
		END
		ELSE IF @Ids is NOT NULL
		BEGIN 
			INSERT INTO @tbIds
			Select Items,0 from dbo.SplitGUID(@Ids,',')
		END
		ELSE
		BEGIN
			-- if the request is to get the contacts for the given list then this temp table will be populated below
			IF (@ContactListId IS NULL OR @ContactListId =  dbo.GetEmptyGuid())
			BEGIN
				IF (@SortOrder IS NULL) SET @SortOrder = 'ASC'
				IF (@SortBy IS NOT NULL)
					SET @SortClause = UPPER(@SortBy + ' ' + @SortOrder)
			
				INSERT INTO @tbIds
				SELECT	Id, ROW_NUMBER() OVER (ORDER BY
							CASE WHEN @SortClause = 'FirstName ASC' THEN FirstName END ASC,
							CASE WHEN @SortClause = 'FirstName DESC' THEN FirstName END DESC,
							CASE WHEN @SortClause = 'LastName ASC' THEN LastName END ASC,
							CASE WHEN @SortClause = 'LastName DESC' THEN LastName END DESC,
							CASE WHEN @SortClause = 'Email ASC' THEN Email END ASC,
							CASE WHEN @SortClause = 'Email DESC' THEN Email END DESC,
							CASE WHEN @SortClause = 'CreatedDate ASC' THEN CreatedDate END ASC,
							CASE WHEN @SortClause = 'CreatedDate DESC' THEN CreatedDate END DESC
						) AS RowNumber
				FROM	MKContact WHERE @Status is null or status = @Status
			END
		END
			
		DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
		--If the contact list id is passed then get the contacts(including the customers,etc) for the list
		IF (@ContactListId IS NOT NULL AND @ContactListId <>  dbo.GetEmptyGuid())
		BEGIN
			--Get all the contacts for the given contact list
			Declare @ListType int
			SELECT @ListType = ListType from TADistributionLists where Id=@ContactListId

			IF (@ListType = 0) --For manual
			BEGIN

			IF @Query IS NOT NULL
			BEGIN
			-- IMP NOTE: this will not work for getting contacts as it should be joined with vwcontacts rather than just MKContact. This has to be fixed at the framework level
				INSERT INTO @tbIds
				EXEC sp_executesql @Query
			END
			--ELSE
			--BEGIN
			--	-- if the request is to get the contacts for the given list then this temp table will be populated below
			--	--INSERT INTO @tbIds
			--	--SELECT UserId Id, 0 FROM vw_contacts
			
			--END

			;WITH CTE AS(
				SELECT ROW_NUMBER() OVER (ORDER BY DL.Id) As RowNumber,
					COUNT(C.UserId) OVER () AS TotalRecords,
					C.UserId AS Id			
				from TADistributionListUser DLU JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
				--JOIN @tbIds T ON T.Id = DLU.UserId 
				JOIN TADistributionLists DL ON DLU.DistributionListId = DL.Id
				WHERE DL.Id = @ContactListId AND DL.ListType = @ListType AND ContactListSubscriptionType != 3
			)
			INSERT INTO @tbPagedResults
			SELECT Id, RowNumber, TotalRecords FROM CTE
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
				
			END
			ELSE IF (@ListType = 1) --For auto list
			BEGIN
				Declare @queryXml xml,@TotalRecords int 
				SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@ContactListId 
					
					Declare @tblContacts Table
					(
						Id uniqueidentifier,
						UserId uniqueidentifier,
						FirstName varchar(1024) null,
						MiddleName varchar(1024) null,
						LastName varchar(1024) null,
						CompanyName varchar(1024) null,
						BirthDate datetime null,
						Gender varchar(1024) null,
						AddressId uniqueidentifier null,
						Status int null,
						HomePhone varchar(1024) null,
						MobilePhone varchar(1024) null,
						OtherPhone varchar(1024) null,
						ImageId uniqueidentifier null,
						Notes varchar(max) null,
						Email varchar(256),
						ContactType int,
						ContactSourceId int,
						TotalRecords int null
					)
				
				insert into @tblContacts(Id,UserId,FirstName,MiddleName,LastName,CompanyName,BirthDate,Gender,AddressId,Status,HomePhone,MobilePhone,OtherPhone,ImageId,Notes,
				Email,ContactType,ContactSourceId,TotalRecords)
				exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@SiteId, @MaxRecords=0, @ContactListId = @ContactListId,@TotalRecords=@TotalRecords output, @IncludeAddress = 0
				
				INSERT INTO @tbIds
				SELECT UserId Id, 0 FROM @tblContacts
				
				;WITH CTE AS(
				SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) As RowNumber,
					COUNT(T.Id) OVER () AS TotalRecords,T.Id AS Id			
				from @tbIds T
					)
					INSERT INTO @tbPagedResults
					SELECT Id, RowNumber, TotalRecords FROM CTE
					WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
						OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
						AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)

			END

			SELECT DISTINCT U.UserId Id, U.[FirstName], U.[MiddleName], U.[LastName], U.[CompanyName], U.[BirthDate], U.[Gender], U.[AddressId],   
                      [Status]/* if active return marketier status else return 2 inactive*/ , U.[HomePhone], U.[MobilePhone],   
                      U.[OtherPhone], U.[ImageId], U.[Notes], U.Email, 0 ContactType, ContactSourceId  ,
				CU.UserFullName AS CreatedByFullName,
				MU.UserFullName AS ModifiedByFullName,
				T.RowNumber,
				T.TotalRecords,
				CS.SiteId 
			from @tbPagedResults T JOIN vw_Contacts U ON U.UserId = T.Id
				JOIN vw_ContactSite CS on CS.UserId = U.UserId and CS.IsPrimary = 1
				LEFT JOIN VW_UserFullName CU on CU.UserId = U.CreatedBy
				LEFT JOIN VW_UserFullName MU on MU.UserId = U.ModifiedBy
			ORDER BY RowNumber
		END
		ELSE
		BEGIN --FOR GETTING CONTACTS
			;WITH CTE AS(
				SELECT ROW_NUMBER() OVER (ORDER BY T.RowNumber) As RowNumber,
					COUNT(C.Id) OVER () AS TotalRecords,
					C.Id AS Id			
				FROM MKContact C JOIN MKContactSite SC ON SC.ContactId = C.Id
					 INNER JOIN @tbIds T ON T.Id = C.Id
				WHERE (@Id IS NULL OR C.Id = @Id)
					AND (@SiteId IS NULL OR SC.SiteId = @SiteId)
					AND (@Status IS NULL OR C.Status = @Status)
					AND (@LoweredEmail IS NULL OR LOWER(C.Email) = @LoweredEmail)
					AND (@FormId IS NULL OR @FormId = dbo.GetEmptyGuid() OR C.Id IN (
							SELECT	DISTINCT UserId
							FROM	FormsResponse
							WHERE	FormsId = @FormId
						))
			)
			INSERT INTO @tbPagedResults
			SELECT Id, RowNumber, TotalRecords FROM CTE
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
		

			SELECT DISTINCT C.Id,
				C.Email,
				C.FirstName,
				C.MiddleName,
				C.LastName,
				C.CompanyName,
				C.Gender,
				C.BirthDate,
				C.HomePhone,
				C.MobilePhone,
				C.OtherPhone,
				C.ImageId,
				C.AddressId,
				C.Notes,
				C.ContactSourceId,
				C.Status,
				C.CreatedBy,
				dbo.ConvertTimeFromUtc(C.CreatedDate, @SiteId) CreatedDate,
				C.ModifiedBy,
				dbo.ConvertTimeFromUtc(C.ModifiedDate, @SiteId) ModifiedDate,
				C.LastSynced,
				CU.UserFullName AS CreatedByFullName,
				MU.UserFullName AS ModifiedByFullName,
				T.RowNumber,
				T.TotalRecords,
				CS.SiteId
			FROM 
				MKContact C JOIN @tbPagedResults T ON T.Id = C.Id
				JOIN vw_ContactSite CS on CS.UserId = C.Id and CS.IsPrimary = 1
				LEFT JOIN VW_UserFullName CU on CU.UserId = C.CreatedBy
				LEFT JOIN VW_UserFullName MU on MU.UserId = C.ModifiedBy
			WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
				OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
				AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
			ORDER BY RowNumber

			SELECT A.*
			from vw_Address A
				JOIN MKContact C ON C.AddressId = A.Id
				Join @tbPagedResults T on T.Id = C.Id
		END

	END
	ELSE IF (@CampaignRunId IS NOT NULL AND @CampaignRunId <> dbo.GetEmptyGuid())
	BEGIN

		DECLARE @VerifyBatchCompletion BIT
		IF (@CampaignId IS NULL OR @CampaignId = dbo.GetEmptyGuid())
			SET @VerifyBatchCompletion = 1

		EXEC CampaignContact_Get @SiteId,@CampaignId,@CampaignRunId,@EmailPerCampaignLimit,@BatchSize,@VerifyBatchCompletion
	END
	

END
GO
PRINT 'Altering procedure CampaignContact_Get'
GO

ALTER PROCEDURE [dbo].[CampaignContact_Get]
(
	@SiteId uniqueidentifier = null,
	@CampaignId uniqueidentifier = null,
	@CampaignRunId UNIQUEIDENTIFIER = null,
	@EmailPerCampaignLimit INT = NULL,
	@BatchSize INT,
	@VerifyBatchCompletion BIT = NULL,
	@OnlyCount BIT = NULL --This is used along with the campaignid to know the count

)
AS
BEGIN

/*
PART 1: Getting contacts for the campaign
PART 2: Verifying whether a email campaign is finished processing by the email processor
PART 3: Getting contacts for the Email processor to send emails using the batch logic; this also includes the AUTO RESPONDER contact processing
CampaignRunHistoryDto_UpdateResponse is used to update the response back to the email processor worktable
*/

	/*
	Create columns for ProcessingCount , Message, Status
• For the given Campaign Run Id, Batch size do the following
◦Verify whether the work table has data for this campaign and the campaign is still in processing status
◦If no contacts found , get the contacts for the campaign and insert into worktable
◦Get the contacts for the NEXT batch and return it.
◾The contacts should not have Processing count > 0
◦Mark the batch as processing by increasing the ProcessingCount by 1
◦If there is no contacts with ProcessingCount 0, then USE retry by sending Processing count starting from 1 to 3
◦If no contacts found EVEN after retry with ProcessingCount < 3 send empty result set.

	*/
	/*
	CAMPAIGN STATUS
	---------------------
		Draft = 1,
        Active = 2,
        Completed = 3,
        Archived = 4,
        Running = 5,
        Cancelled = 6,
        PartiallySent = 7,
	
	*/
	--PART 1: This is to get the contacts for the given campaign;THIS IS NOT USED BY EMAIL PROCESSOR
	IF ( (@CampaignRunId IS NULL OR @CampaignRunId = dbo.GetEmptyGuid()) AND @CampaignId IS NOT NULL AND @CampaignId <> dbo.GetEmptyGuid() )
	BEGIN
		
		Declare @tblContacts as Table
		(
			Id uniqueidentifier,
			UserId uniqueidentifier,
			FirstName varchar(1024) null,
			MiddleName varchar(1024) null,
			LastName varchar(1024) null,
			CompanyName varchar(1024) null,
			BirthDate datetime null,
			Gender varchar(1024) null,
			AddressId uniqueidentifier null,
			Status int null,
			HomePhone varchar(1024) null,
			MobilePhone varchar(1024) null,
			OtherPhone varchar(1024) null,
			ImageId uniqueidentifier null,
			Notes varchar(max) null,
			Email varchar(256),
			ContactType int,
			ContactSourceId int,
			TotalRecords int null
		)

		--Get the direct list attached to the campaign
		insert into @tblContacts
		select C.[UserId] Id
				,C.[UserId]
				,C.[FirstName]
				,C.[MiddleName]
				,C.[LastName]
				,C.[CompanyName]
				,dbo.ConvertTimeFromUtc(BirthDate, @SiteId)BirthDate 
				,C.[Gender]
				,C.[AddressId]
				,C.[Status]
				,C.[HomePhone]
				,C.[MobilePhone]
				,C.[OtherPhone]
				,C.ImageId 
				,C.[Notes]
				,C.[Email]
				,C.[ContactType]
				,C.[ContactSourceId] ,0
			from MKCampaignDistributionList CDL JOIN TADistributionListUser DLU ON CDL.DistributionListId=DLU.DistributionListId AND ContactListSubscriptionType != 3
			JOIN dbo.vw_contacts C ON C.UserId = DLU.UserId
			JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id
			WHERE CDL.CampaignId = @CampaignId AND DL.ListType = 0

		--Get all the contacts for AUTO LIST
			declare @autoListIds table (Id uniqueidentifier, RowNum int)

			insert into @autoListIds
			select DistributionListId,
			ROW_NUMBER() OVER (ORDER BY DL.Id) RowNum
			From MKCampaignDistributionList CDL JOIN TADistributionLists DL ON CDL.DistributionListId = DL.Id 
			Where CampaignId = @CampaignId  AND DL.ListType = 1 --needs to be verified.
		
			declare @cnt int,  @TempId uniqueidentifier
			select @cnt = MAX(RowNum) from @autoListIds
		 
			while (@cnt >0)
			begin
					
				SELECT @TempId = Id FROM @autoListIds WHERE RowNum = @cnt

				Declare @queryXml xml,@TotalRecords int 
				SELECT @queryXml = SearchXml from dbo.TADistributionListSearch a,CTSearchQuery b where 
						b.Id = a.SearchQueryId and a.DistributionListId=@TempId 
				
				IF (@queryXml IS NOT NULL AND @SiteId IS NOT NULL)
				BEGIN
					--It is a must to have a valid site id 
					insert into @tblContacts
					exec Contact_SearchContact @Xml=@queryXml,@ApplicationId=@SiteId, @MaxRecords=0,@ContactListId = @TempId, @TotalRecords=@TotalRecords output,  @IncludeAddress = 0
				END

				SET @cnt = @cnt - 1
			end

			--Get the contacts attached directly to the campaign
			insert @tblContacts
			(	Id
				,UserId
				,FirstName
				,MiddleName
				,LastName
				,CompanyName
				,BirthDate
				,Gender
				,AddressId
				,Status
				,HomePhone
				,MobilePhone
				,OtherPhone
				,ImageId
				,Notes
				,Email
				,ContactType
				,ContactSourceId)
			select c.UserId, c.UserId, c.FirstName, c.MiddleName, c.LastName, c.CompanyName, c.BirthDate,
				c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone, c.OtherPhone, c.ImageId, c.Notes, 
				c.Email, c.ContactType,c.ContactSourceId
			from vw_contacts c
				join MKCampaignUser cu on cu.UserId = c.UserId and cu.CampaignId = @CampaignId

		IF (@OnlyCount = 1)
			SELECT COUNT(DISTINCT(UserId)),@CampaignId CampaignId FROM @tblContacts 
		ELSE
			SELECT C.*,CS.SiteId FROM @tblContacts C JOIN vw_ContactSite CS on Cs.UserId = C.Id and CS.SiteId = @SiteId

		RETURN;
	END 

	DECLARE @CurrentUtcDateTime datetime

	SET @CurrentUtcDateTime = GetUtcDate()

	--PART 2:THIS IS USED BY EMAIL PROCESSOR to verify whether a campaign is completed fully
	IF (@VerifyBatchCompletion = 1)
	BEGIN
		DECLARE @RVal BIT
		SET @RVal = 0

		IF (
		NOT EXISTS(SELECT 1 FROM MKCampaignRunTimeZoneSites WHERE CampaignRunId = @CampaignRunId AND IsProcessed=0 AND StartTime >= @CurrentUtcDateTime )  
		AND NOT EXISTS(SELECT 1 FROM MKCampaignRunWorkTable WHERE (ProcessingStatus = 0 OR ProcessingStatus = 1) AND CampaignRunId = @CampaignRunId)
		
		 )
		BEGIN
			SET  @RVal = 1
			--Update the completion date only if there are some contacts.
			--IF (EXISTS (SELECT 1 FROM MKCampaignRunWorkTable WHERE CampaignRunId=@CampaignRunId))
			--BEGIN
				UPDATE MKCampaignRunHistory SET EndDateTime=@CurrentUtcDateTime WHERE Id= @CampaignRunId
			--END
			--cleanup the worktable; this is moved to update batch status process; this is to ensure that Verify should not remove contacts JUST after the status updates.
			--DELETE FROM MKCampaignRunWorkTable WHERE CampaignRunId=@CampaignRunId
		END

		SELECT @RVal

		RETURN;
	END --Ends the process.


	--PART 3: THIS IS USED BY CAMPAIGN PROCESSOR to fill the contacts and select contacts by batch; both for campaign and auto responders
	DECLARE @CanPopulateWorkTable int --Decides whether to populate the worktable or not
	DECLARE @IsRunnableCampaign bit -- Decides whether the given campaign is in running state still.
	declare @IsAutoResponder bit 
	Declare @TabContactIds as table (ContactId uniqueidentifier,AddressId uniqueidentifier)


	SET @CanPopulateWorkTable = 0 --by default return the contacts from worktable
	SET @IsRunnableCampaign = 0
	SET @IsAutoResponder = 0

	
	Declare @IsRetry bit
	SET @IsRetry = 0

	--If the campaign is in processing state then proceed;otherwise DO NOT Proceed
	IF ((SELECT COUNT(1) FROM MKCampaignRunWorkTable WHERE @CampaignRunId=CampaignRunId) > 0)
	BEGIN
		PRINT 'Getting contacts from worktable'
		
		;with ContactId_Cte(ContactId,AddressId)
		AS
		(
			SELECT UserId,AddressId--,ROW_NUMBER() OVER (ORDER BY CampaignRunId) RowNum 
			FROM MKCampaignRunWorkTable
			WHERE @CampaignRunId = CampaignRunId AND ProcessingCount = 0 AND ProcessingStatus = 0 
		)
		insert into @TabContactIds (ContactId,AddressId)
		Select TOP (@BatchSize) ContactId,AddressId from ContactId_Cte

		--Updates the selected contacts as PROCESSING
		UPDATE MKCampaignRunWorkTable 
		SET ProcessingCount = 1 , ProcessingStatus = 1 
		WHERE UserId in (Select ContactId from @TabContactIds)

		--For retry
		IF ((SELECT COUNT(1) FROM @TabContactIds) = 0)
		BEGIN
			PRINT 'Processing RETRY'

			;with ContactId_Cte(ContactId,AddressId)
			AS
			(
				SELECT UserId,AddressId--,ROW_NUMBER() OVER (ORDER BY CampaignRunId) RowNum 
				FROM MKCampaignRunWorkTable
				WHERE @CampaignRunId = CampaignRunId AND ProcessingCount > 0 AND ProcessingCount <= 3 AND ProcessingStatus = 0 
			)
			insert into @TabContactIds (ContactId,AddressId)
			Select TOP (@BatchSize) ContactId,AddressId from ContactId_Cte

			--Updates the selected contacts as PROCESSING
			UPDATE MKCampaignRunWorkTable 
			SET ProcessingCount = ProcessingCount + 1 , ProcessingStatus = 1 
			WHERE UserId in (Select ContactId from @TabContactIds)

		END
	END

	-- Contacts
	Select DISTINCT
		--Row,
		CampaignRunId,
		WT.UserId Id,
		FirstName ,
		MiddleName ,
		LastName,
		CompanyName ,
		BirthDate ,
		Gender ,
		WT.AddressId ,
		Status ,
		HomePhone ,
		MobilePhone ,
		OtherPhone ,
		Email ,
		Notes ,
		ContactType ,
		CS.SiteId
	From @TabContactIds T JOIN MKCampaignRunWorkTable WT ON T.ContactId = WT.UserId AND WT.CampaignRunId = @CampaignRunId
	JOIN vw_ContactSite CS on CS.UserId = T.ContactId and CS.SiteId = @SiteId


	-- Address
	select	a.Id
			,a.FirstName
			,a.LastName
			,a.AddressType
			,a.AddressLine1
			,a.AddressLine2
			,a.AddressLine3
			,a.City
			,a.StateId
			,a.Zip			
			,a.CountryId
			,a.Phone
			,a.CreatedDate
			,a.CreatedBy
			,a.ModifiedDate
			,a.ModifiedBy
			,a.Status
			,a.County
			,a.CountryName
			,a.CountryCode
			,a.StateCode
			,a.StateName
	from vw_Address a
		Join @TabContactIds c on c.AddressId = a.Id

	--	-- Profile
	--select * 
	--from UsMemberProfile p
	--	join @tblContacts c on c.UserId = p.UserId

		/*
		Status
		0 - Processing NOT YET started;contacts available for 1st time processing
		1 - Processing Started;contacts sent for email processor
		2 - Processing Complete; contacts successfully processed by the email processor
		3 - Processing Complete with Error or Number of retries exceeded the limit (3)
		*/

	
END	
GO
PRINT 'Altering procedure Template_Save'
GO
ALTER PROCEDURE [dbo].[Template_Save]
(
	@Id					uniqueidentifier =null OUT,
	@ApplicationId		uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	@Description        nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier=null,
	@ModifiedDate    	datetime=null,
	@FileName            nvarchar(4000)=null,
	@ImageURL	         nvarchar(4000)=null,
	@CodeFile			 nvarchar(4000)=null,
	@TemplateType        int=null,
	@Status				 int=null,
	@PagePart			 bit=0,
	@LayoutTemplateId		 uniqueidentifier=null,
	@ZonesXml			xml = null,
	@ThemeId			UNIQUEIDENTIFIER = null,
	@EditorOptionsStyleSheet nvarchar(256) = null,
	@EditorContentStyleSheet nvarchar(256) = null,
	@IsNotShared bit = null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
BEGIN
--********************************************************************************
-- code
--********************************************************************************

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	/* IF @Id specified, ensure exists */
   IF (@Id is not null)
       IF((SELECT count(*) FROM  SITemplate WHERE Id = @Id AND Status != dbo.GetDeleteStatus() 
			AND ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) = 0)
       BEGIN
			RAISERROR('NOTEXISTS||Id', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
       END

	SET @Now = getdate()
	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	/*
	Sankar(05/01/2014) : 
	1. Upload a template with DEFAULT Option => Insert a row in the sitemplate with IsThemed = 0
	2. Upload a template with SELECTED Theme => Insert a row in the sitemplate with IsThemed = 1
	
	*/


	--For insert, no need to worry about the theme as we always upload the default template 1st. There can not be a Theme without a default template.
	IF (@Id is null)			-- New INSERT
	BEGIN
		SET @stmt = 'TEMPLATE INSERT'
		SET @Id = newid();
		INSERT INTO SITemplate(
					   Id,	
					   ApplicationId,
					   Title,
                       Description,
					   CreatedBy,
                       CreatedDate,
					   Status,
                       FileName,
						ImageURL,
						CodeFile,	
                       Type,
						PagePart,
						LayoutTemplateId,
						ZonesXml,
						EditorOptionsStyleSheet,
						EditorContentStyleSheet,
						IsNotShared) 
					values (
						@Id,
						@ApplicationId,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@FileName,
						@ImageURL,
						@CodeFile,
						@TemplateType,
						@PagePart,
						@LayoutTemplateId,
						@ZonesXml,
						@EditorOptionsStyleSheet,
						@EditorContentStyleSheet,
						@IsNotShared)

		SELECT @error = @@error
		IF @error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END
    END
    ELSE			-- update
         BEGIN
			
			--We need to update the SIObjectTheme table if we have a theme id greater than 0; Themeid 0 is for default template and it should go to SITemplate table
			SET @stmt = 'TEMPLATE UPDATE'
			IF ( @ThemeId = dbo.GetEmptyGUID() OR @TemplateType = 1)
			BEGIN
				UPDATE	SITemplate  WITH (rowlock)	SET 
					ApplicationId=@ApplicationId,
					Title =@Title,
					Description=@Description,
					ModifiedBy=@ModifiedBy,
					ModifiedDate=@Now,
					ImageURL=@ImageURL,
					CodeFile=@CodeFile,
					FileName=@FileName,
					--Type=@TemplateTyp ,
					Status =@Status,
					PagePart =@PagePart,
					LayoutTemplateId = ISNULL(@LayoutTemplateId, LayoutTemplateId),
					ZonesXml = ISNULL(@ZonesXml, ZonesXml),
					EditorOptionsStyleSheet = ISNULL(@EditorOptionsStyleSheet, EditorOptionsStyleSheet),
					EditorContentStyleSheet = ISNULL(@EditorContentStyleSheet, EditorContentStyleSheet),
					IsNotShared = ISNULL(@IsNotShared,IsNotShared)
 				WHERE 	Id    = @Id 
				--AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

			END
			ELSE
			BEGIN
				
				--Update the SIObjectTheme table with the properties
				DELETE FROM SIObjectTheme WHERE ObjectId=@Id AND ObjectTypeId=3 AND ThemeId=@ThemeId
				
				INSERT INTO SIObjectTheme(Id,ObjectId,ThemeId,FileName, CodeFile, ObjectTypeId,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,Status,Title)
				VALUES (NEWID(),@Id,@ThemeId,@FileName,@CodeFile,3,@ModifiedBy,@Now,NULL,NULL,1,@Title) -- 3 for template

				UPDATE SITemplate SET IsThemed=1 WHERE Id=@Id
			END
			SELECT @error = @@error, @rowcount = @@rowcount

			IF @error <> 0
			BEGIN
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END
END
GO
PRINT 'Altering procedure CampaignDto_Save'
GO
ALTER PROCEDURE [dbo].[CampaignDto_Save]
(
	
	@Id UNIQUEIDENTIFIER = NULL OUT,
	@Title NVARCHAR(MAX) = NULL,
	@Description NVARCHAR(MAX) = NULL,
	@Type INT ,
	@CampaignGroupId UNIQUEIDENTIFIER = NULL,
	@Status INT = NULL,
	@SenderName NVARCHAR(MAX) = NULL,
	@SenderEmail NVARCHAR(MAX) = NULL,
	@ConfirmationEmail NVARCHAR(MAX) = NULL,
	@CreatedDate DATETIME,
	@CreatedBy NVARCHAR(MAX) = NULL,
	@CampaignId UNIQUEIDENTIFIER = NULL,
	@SiteId UNIQUEIDENTIFIER ,
	@EmailId UNIQUEIDENTIFIER = NULL,
	@Emails XML = NULL,
	@ScheduleId UNIQUEIDENTIFIER = NULL,
	@WorkflowId UNIQUEIDENTIFIER = NULL,
	@WorkflowStatus INT = NULL,
	@UniqueRecipientsOnly BIT = NULL,
	@AutoUpdateLists BIT = NULL,
	@SendToNotTriggered BIT = NULL,	
	@UseLocalTimeZoneToSend BIT =0,
	@SenderNameProperty nvarchar(250) =null,
	@SenderEmailProperty nvarchar(250) =null,
	@ScheduledTimeZone nvarchar(max) =null
)
AS
BEGIN
	DECLARE @UtcNow datetime,@ModifiedBy uniqueidentifier
	SET @UtcNow = GETUTCDATE()
	SET @ModifiedBy = NULL
	
	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID())
	BEGIN
		SET @Id = NEWID()
		
		INSERT INTO MKCampaign
		(
			Id,
			Title,
			Description,
			Type,
			CampaignGroupId,
			Status,
			SenderName,
			SenderEmail,
			ConfirmationEmail,
			CreatedDate,
			CreatedBy,
			ModifiedDate,
			ModifiedBy,			
			ApplicationId,
			SenderNameProperty,
			SenderEmailProperty,
			ScheduledTimeZone
				
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			@Type,
			@CampaignGroupId,
			@Status,
			@SenderName,
			@SenderEmail,
			@ConfirmationEmail,
			@CreatedDate,
			@CreatedBy,
			@UtcNow,
			@ModifiedBy,
			@SiteId,
			@SenderNameProperty,
			@SenderEmailProperty,
			@ScheduledTimeZone
		)
		
		INSERT INTO MKCampaignAdditionalInfo
		(
			Id,
			CampaignId,
			CostPerEmail,
			ScheduleId,
			WorkflowStatus,
			UniqueRecipientsOnly,
			AutoUpdateLists,
			LastPublishDate,
			WorkflowId,
			AuthorId,
			SendToNotTriggered,
			UseLocalTimeZoneToSend
		)
		VALUES
		(
			NEWID(),
			@Id,
			NULL,
			@ScheduleId,
			@WorkflowStatus,
			@UniqueRecipientsOnly,
			@AutoUpdateLists,
			NULL,
			@WorkflowId,
			NULL,
			@SendToNotTriggered,
			@UseLocalTimeZoneToSend
		)		

		INSERT INTO MKCampaignEmail
		(
			Id,
			CampaignId,
			EmailSubject,
			EmailHtml,
			CMSPageId,
			EmailText,
			Sequence,
			TimeValue,
			TimeMeasurement,
			EmailHtmlPageVersion,
			PageMapNodeId,
			EmailContentType,
			EmailContentTypePriority
		)
		SELECT
			NEWID(),
			@Id,
			Emails.email.value('@EmailSubject', 'nvarchar(max)'),
			Emails.email.value('@EmailHtml', 'nvarchar(max)'),
			Emails.email.value('@CMSPageId', 'uniqueidentifier'),
			Emails.email.value('@EmailText', 'nvarchar(max)'),
			ISNULL(Emails.email.value('@Sequence', 'int'), 0),
			Emails.email.value('@TimeValue', 'int'),
			Emails.email.value('@TimeMeasurement', 'nvarchar(max)'),
			Emails.email.value('@EmailHtmlPageVersion', 'nvarchar(max)'),
			Emails.email.value('@PageMapNodeId', 'uniqueidentifier'),
			Emails.email.value('@EmailContentType', 'int'),
			Emails.email.value('@EmailContentTypePriority', 'int')
		FROM @Emails.nodes('Emails/Email') AS Emails(email)
	END
	
	ELSE
	BEGIN
		UPDATE MKCampaign
	    SET [Title] = @Title
			,[Description] = @Description
			,[Type] = @Type
			,[CampaignGroupId] = @CampaignGroupId
			,[Status] = @Status
			,[SenderName] = @SenderName
			,[SenderEmail] = @SenderEmail
		    ,[ConfirmationEmail] = @ConfirmationEmail
		    ,[CreatedBy] =@CreatedBy
		    ,[CreatedDate] = @CreatedDate
		    ,[ModifiedBy] = @ModifiedBy
		    ,[ModifiedDate] = @UtcNow
		    ,[ApplicationId] = @SiteId
		    ,[SenderNameProperty] = @SenderNameProperty
			,[SenderEmailProperty] = @SenderEmailProperty
			,[ScheduledTimeZone] = @ScheduledTimeZone
			
		WHERE Id = @Id	
	  
		UPDATE MKCampaignAdditionalInfo
		SET CostPerEmail = NULL,
			ScheduleId = @ScheduleId,
			WorkflowStatus = @WorkflowStatus,
			UniqueRecipientsOnly = @UniqueRecipientsOnly,
			AutoUpdateLists = @AutoUpdateLists,
			LastPublishDate = NULL,
			WorkflowId = @WorkflowId,
			AuthorId = NULL,
			SendToNotTriggered = @SendToNotTriggered
			,[UseLocalTimeZoneToSend] = @UseLocalTimeZoneToSend
		WHERE CampaignId = @Id
		
		;WITH EmailXML AS
		(
			SELECT 
				Emails.email.value('@EmailSubject', 'nvarchar(max)') AS EmailSubject,
				Emails.email.value('@EmailHtml', 'nvarchar(max)') AS EmailHtml,
				Emails.email.value('@CMSPageId', 'uniqueidentifier') AS CMSPageId,
				Emails.email.value('@EmailText', 'nvarchar(max)') AS EmailText,
				Emails.email.value('@Sequence', 'int') AS Sequence,
				Emails.email.value('@TimeValue', 'int') AS TimeValue,
				Emails.email.value('@TimeMeasurement', 'nvarchar(max)') AS TimeMeasurement,
				Emails.email.value('@EmailHtmlPageVersion', 'nvarchar(max)') as EmailHtmlPageVersion,
				Emails.email.value('@PageMapNodeId', 'uniqueidentifier') as PageMapNodeId,
				Emails.email.value('@EmailContentType', 'int') AS EmailContentType,
				Emails.email.value('@EmailContentTypePriority', 'int') AS EmailContentTypePriority
			FROM @Emails.nodes('Emails/Email') AS Emails(email)
		)
		UPDATE e
		SET EmailSubject = ISNULL(x.EmailSubject, e.EmailSubject),
			EmailHtml = ISNULL(x.EmailHtml, e.EmailHtml),
			CMSPageId = ISNULL(x.CMSPageId, e.CMSPageId),
			EmailText = ISNULL(x.EmailText, e.EmailText),
			Sequence = ISNULL(x.Sequence, e.Sequence),
			TimeValue = ISNULL(x.TimeValue, e.TimeValue),
			TimeMeasurement = ISNULL(x.TimeMeasurement, e.TimeMeasurement),
			EmailHtmlPageVersion = ISNULL(x.EmailHtmlPageVersion, e.EmailHtmlPageVersion),
			PageMapNodeId = ISNULL(x.PageMapNodeId, e.PageMapNodeId),
			EmailContentType = ISNULL(x.EmailContentType, e.EmailContentType),
			EmailContentTypePriority = ISNULL(x.EmailContentTypePriority, e.EmailContentTypePriority)
		FROM MKCampaignEmail e, EmailXML x
	    WHERE CampaignId = @Id
	END
END

GO
PRINT 'Altering procedure Customer_GetCustomers'
GO
ALTER PROCEDURE [dbo].[Customer_GetCustomers]
    (
      @IsActive BIT = NULL ,
      @Searchkeyword NVARCHAR(4000) = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @PageSize INT = NULL ,
      @PageNumber INT = 1 ,
      @ApplicationId UNIQUEIDENTIFIER = NULL
    )
AS 
    BEGIN  
  


  
        DECLARE @searchText NVARCHAR(MAX) ,
            @IsactiveCondition NVARCHAR(100)  
  
        IF @IsActive IS NULL 
            SET @IsactiveCondition = ''  
        ELSE 
            SET @IsactiveCondition = ' IsActive = '
                + CAST(@IsActive AS VARCHAR(1)) + ' AND '  
   
        IF @SearchKeyword IS NULL 
            SET @SearchKeyword = '%'  
        ELSE 
            SET @SearchKeyword = '%' + ( @Searchkeyword ) + '%'  
        SET @searchText = '''' + @Searchkeyword + ''''  
  
  
        IF @PageSize IS NULL 
            SET @PageSize = 2147483647  
  
        IF @SortColumn IS NULL 
            SET @SortColumn = 'LastName ASC'  
  
        DECLARE @SQL NVARCHAR(MAX) ,
            @params NVARCHAR(100)  
  
        SET @params = N'@SIZE INT, @nbr INT,  @Sort VARCHAR(25), @ApplicationId uniqueidentifier'  
  
        SET @SQL = N'  
  
;WITH PagingCTE (
	Pages
	,Row_ID  
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone
   ,ImageId  
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy
   ,County
   ,ExternalId
   ,ExternalProfileId
    )  
  
AS  
  
(  
  
SELECT   
Count(*) OVER()  as Pages,
     ROW_NUMBER()   
  
            OVER(ORDER BY ' + @SortColumn
            + '  
  
--             CASE WHEN @Sort=''LastName DESC'' THEN LastName END DESC,  
--  
--             CASE WHEN @Sort=''LastName ASC''  THEN LastName END ASC,  
--  
--       CASE WHEN @Sort=''FirstName DESC'' THEN FirstName END DESC,  
--  
--             CASE WHEN @Sort=''FirstName ASC''  THEN FirstName END ASC,  
--           
--    CASE WHEN @Sort=''Email DESC'' THEN Email END DESC,  
--  
--             CASE WHEN @Sort=''Email ASC''  THEN Email END ASC,  
--  
--    CASE WHEN @Sort=''AddressLine1 DESC'' THEN AddressLine1 END DESC,  
--  
--             CASE WHEN @Sort=''AddressLine1 ASC''  THEN AddressLine1 END ASC,  
--  
--       CASE WHEN @Sort=''AddressLine2 DESC'' THEN AddressLine2 END DESC,  
--  
--             CASE WHEN @Sort=''AddressLine2 ASC''  THEN AddressLine2 END ASC,  
--           
--    CASE WHEN @Sort=''City DESC'' THEN City END DESC,  
--  
--             CASE WHEN @Sort=''City ASC''  THEN City END ASC,  
--  
--    CASE WHEN @Sort=''Phone DESC'' THEN Phone END DESC,  
--  
--             CASE WHEN @Sort=''PHONE ASC''  THEN Phone END ASC,  
--  
--    CASE WHEN @Sort=''Status DESC'' THEN Status END DESC,  
--  
--             CASE WHEN @Sort=''Status ASC''  THEN Status END ASC  
           
           
            ) AS [Row_ID]  
  
     
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State 
	,StateCode 
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM vwSearchCustomer  
Where SiteId = @ApplicationId AND ' + @IsactiveCondition + '   
 (  	
  (LastName) Like ' + @searchText + '  
  OR (FirstName) Like ' + @searchText + '  
  OR (Email) Like ' + @searchText + '  
  OR (AddressLine1) Like ' + @searchText + '  
  OR (City) Like ' + @searchText + '  
  OR (State) Like ' + @searchText + '  
  OR (Zip) Like ' + @searchText + '  
  OR (HomePhone) Like ' + @searchText + '  
  OR (AccountNumber) Like ' + @searchText + ' 
  OR (CompanyName) Like ' + @searchText + ' 
  OR case when Status =1 then ''active'' else ''deactive'' end Like '
            + @searchText + '  
 )  
)   
  
SELECT   
Pages
 ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
   ,CreatedBy  
   ,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId) CreatedDate  
   ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId) ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM PagingCTE pcte  
WHERE Row_ID >= (@SIZE * @nbr) - (@SIZE -1) AND Row_ID <= @SIZE * @nbr'  
  
        PRINT @SQL  
        EXEC sp_executesql @SQL, @params, @SIZE = @PageSize,
            @nbr = @PageNumber, @Sort = @SortColumn,
            @ApplicationId = @ApplicationId 
        DECLARE @RowCount INT
        SELECT  @RowCount = @@ROwCOUNT
      

        SELECT  1 


    END

GO
PRINT 'Altering procedure CampaignDto_Get'

GO
ALTER PROCEDURE [dbo].[CampaignDto_Get]  
(  
	@Id UNIQUEIDENTIFIER = NULL,  
	@SiteId UNIQUEIDENTIFIER = NULL,
	@IncludeVariantSites BIT = 0,
	@Status	INT = NULL,
	@Type INT = NULL,
	@CampaignGroupId UNIQUEIDENTIFIER = NULL,
	@AwaitingSendOnly BIT = 0,
	@PageSize INT = NULL,   
	@PageNumber INT = NULL ,
	@MaxRecords	INT = NULL,
	@Query NVARCHAR(MAX) = NULL,
	@IsRun BIT = 0,
	@IgnoreDetails BIT = NULL	
)  
AS  
BEGIN
	DECLARE @PageLowerBound int, @PageUpperBound int, @SortClause nvarchar(50)
	SET @PageLowerBound = @PageSize * @PageNumber
	
	IF (@PageLowerBound IS NOT NULL AND @PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	
	DECLARE @tbIds TABLE (Id uniqueidentifier, RowNumber int)
	
	IF @Query IS NULL
	BEGIN
		IF(@IsRun = 0)
			INSERT INTO @tbIds
			SELECT Id, 0 FROM MKCampaign
		ELSE
			INSERT INTO @tbIds
			SELECT Distinct M.Id, 0 FROM MKCampaign M
			INNER JOIN MKCampaignRunHistory MR ON M.Id = MR.CampaignId
	END
	ELSE
	BEGIN
		INSERT INTO @tbIds
		EXEC sp_executesql @Query
	END
	
	
	DECLARE @EmptyGuid uniqueidentifier
	SET @EmptyGuid = dbo.GetEmptyGUID()
			
	DECLARE @tbPagedResults TABLE (Id uniqueidentifier, RowNumber int, TotalRecords int)
	
	;WITH CTE AS(
		SELECT	ROW_NUMBER() OVER (	
					ORDER BY T.RowNumber	
				) AS RowNumber, COUNT(C.Id) OVER () AS TotalRecords,
				C.Id AS Id			
		FROM	MKCampaign AS C
				LEFT JOIN MKCampaignAdditionalInfo AS A ON C.Id = A.CampaignId
				LEFT JOIN TASchedule AS S ON S.Id = A.ScheduleId
				JOIN @tbIds AS T ON T.Id = C.Id
		WHERE	(@Id IS NULL OR C.Id = @Id) AND
				(@Status IS NULL OR C.Status = @Status) AND
				(@CampaignGroupId IS NULL OR @CampaignGroupId ='00000000-0000-0000-0000-000000000000' OR C.CampaignGroupId = @CampaignGroupId) AND
				(@SiteId IS NULL OR 
					((@IncludeVariantSites = 0 AND C.ApplicationId = @SiteId) OR 
					(@IncludeVariantSites = 1 AND C.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId))))) AND
				(@Type IS NULL OR C.Type = @Type) AND 
				(@AwaitingSendOnly = 0 OR (
					C.Status = 2 AND
					S.NextRunTime < GETUTCDATE() AND
					(ISNULL(S.MaxOccurrence, 0) <= 0 OR S.RunCount < S.MaxOccurrence)) OR
				 C.Status = 7 or C.Status=5)
	)
	
	INSERT INTO @tbPagedResults
	SELECT Id, RowNumber, TotalRecords FROM CTE
	WHERE (@PageLowerBound IS NULL OR @PageUpperBound IS NULL
		OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		AND (@MaxRecords IS NULL OR @MaxRecords = 0 OR RowNumber <= @MaxRecords)
	
	SELECT C.ApplicationId, 
	       C.CampaignGroupId,
	       C.ConfirmationEmail,
	       C.CreatedBy,
	       dbo.ConvertTimeFromUtc(C.CreatedDate, @SiteId)CreatedDate,	      
	       C.Description,
		   C.Id,	       
	       C.ModifiedBy,
	       dbo.ConvertTimeFromUtc(C.ModifiedDate, @SiteId)ModifiedDate,
	       C.SenderEmail,
	       C.SenderEmailProperty,
	       C.SenderName,
	       C.SenderNameProperty,
	       C.Status,
	       C.Title,
	       C.Type,	
	       C.ScheduledTimeZone, 
		   A.UseLocalTimeZoneToSend,       
	       A.AuthorId,
	       A.AutoUpdateLists,
	       A.CampaignId,
	       A.CostPerEmail,	      
	       dbo.ConvertTimeFromUtc(A.LastPublishDate, @SiteId)LastPublishDate,
	       A.LastPublishDate,
	       A.ScheduleId,
	       A.SendToNotTriggered,
	       A.UniqueRecipientsOnly,
	       A.WorkflowId,
	       A.WorkflowStatus,
		   T.TotalRecords
	FROM MKCampaign C
		JOIN @tbPagedResults T ON T.Id = C.Id
		LEFT JOIN MKCampaignAdditionalInfo A on A.CampaignId = C.Id
	ORDER BY T.RowNumber
	
	SELECT E.[Id]
      ,[CampaignId]
      ,[EmailSubject]
      ,CASE WHEN @IgnoreDetails = 1 THEN '' ELSE [EmailHtml] END AS EmailHtml
      ,[CMSPageId]
      ,[EmailText]
      ,[Sequence]
      ,[TimeValue]
      ,[TimeMeasurement]
      ,[EmailHtmlPageVersion]
      ,[PageMapNodeId]
      ,[EmailContentType]
      ,[EmailContentTypePriority]
  FROM [MKCampaignEmail] E
	JOIN @tbPagedResults T ON T.Id = E.CampaignId
		
--	--Getting the contact count for each campaign
	Declare @tabCampaignListCount as table (CampaignId uniqueidentifier,ContactListId uniqueidentifier,ContactListTitle nvarchar(max),ContactCount int,RowNum int)

	INSERT INTO @tabCampaignListCount(CampaignId,ContactListId,ContactListTitle,ContactCount,RowNum)
	Select R.Id,CL.DistributionLIstId,D.Title,ISNULL(DC.Count,0),ROW_NUMBER() over(PARTITION BY R.Id,CL.DistributionListId ORDER BY CL.DistributionListId ASC)
	FROM @tbPagedResults R
	INNER JOIN (Select AI.CampaignId,CDL.DistributionListId FROM
				MKCampaignAdditionalInfo AI 
				INNER JOIN MKCampaignDistributionListDraft CDL ON (CDL.CampaignId = AI.CampaignId AND AI.WorkflowStatus =1)
				UNION ALL
				Select AJ.CampaignId,CPL.DistributionListId
				FROM MKCampaignAdditionalInfo AJ 
				LEFT JOIN MKCampaignDistributionListDraft CPL ON (CPL.CampaignId = AJ.CampaignId AND AJ.WorkflowStatus <>1)) CL
		ON CL.CampaignId = R.Id
		INNER JOIN TADistributionLists D on D.Id = CL.DistributionListId
		LEFT JOIN TADistributionListSite DC ON DC.DistributionListid =CL.DistributionListId AND (@SiteId IS NULL OR 
					((@IncludeVariantSites = 0 AND DC.SiteId = @SiteId) OR 
					(@IncludeVariantSites = 1 AND DC.SiteId IN (SELECT SiteId FROM dbo.GetVariantSites(@SiteId)))))


	SELECT CampaignId,sum(ContactCount) ContactCount FROM @tabCampaignListCount
	Group By CampaignId
	
	SELECT * FROM @tabCampaignListCount

END


GO
Print 'Altering Schedule_UpdateLastRun'

GO
ALTER PROCEDURE [dbo].[Schedule_UpdateLastRun] 
(
			@Id  uniqueidentifier
		   ,@LastRunTime DateTime=NULL
		   ,@ModifiedBy uniqueidentifier
		   ,@ModifiedDate datetime
		   ,@ApplicationId uniqueidentifier
		   ,@IncrementRunCount bit = NULL
)
AS
BEGIN
	
	SET @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId) 
	Declare @Type int,
			@NextRunTime dateTime,
			@OldLastRunTime dateTime,
			@StartDate Datetime,
			@StartTime DateTime,
			@EndDate DateTime,
			@EndTime DateTime,
			@MaxOccurence int,
			@Runcount int,	
			@ScheduleTypeId uniqueidentifier,
			@OldNextRunTime dateTime

	SET @NextRunTime =NULL

	SELECT @StartDate =[StartDate]
		  ,@EndDate=[EndDate]
		  ,@StartTime =[StartTime]
		  ,@EndTime =[EndTime]
		  ,@MaxOccurence=[MaxOccurrence]
		  ,@Type=[Type]
		  ,@ScheduleTypeId=[ScheduleTypeId]
		  ,@NextRunTime=[NextRunTime]
		  ,@OldLastRunTime=[LastRunTime]
		  ,@Runcount=[RunCount]
	  FROM [dbo].[TASchedule]
	  Where Id=@Id AND Status=dbo.GetActiveStatus()
			--AND ApplicationId=@ApplicationId
	
	--if(@LastRunTime is null and @NextRunTime is not null) set @LastRunTime=GETUTCDATE()

	IF @LastRunTime is not null
	Set @OldLastRunTime =@LastRunTime
	--If @LastRunTime is null
	--	Select @LastRunTime = LastRunTime From TASchedule Where Id = @Id


	
	IF @Type=1
	BEGIN
	-- Do Daily calculations
	
	
	--	Select @LastRunTime = LastRunTime From TASchedule Where Id = @Id
		Declare @OccursEvery int, @WeekDaysOnly bit
		
		Select @OccursEvery=OccursEvery,@WeekDaysOnly=isnull(WeekDaysOnly,0) From TAScheduleDaily
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()

		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() 
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate 
			SET @OccursEvery =0
		END	
			IF @WeekDaysOnly=1
			BEGIN 
				IF datepart(weekday,@LastRunTime + @OccursEvery) =7
				BEGIN
					SET @NextRunTime = @LastRunTime + @OccursEvery + 2
				END
				ELSE IF datepart(weekday,@LastRunTime + @OccursEvery) =1
				BEGIN
					SET @NextRunTime = @LastRunTime + @OccursEvery + 1
				END
				ELSE
				BEGIN
					SET @NextRunTime = @LastRunTime + @OccursEvery
				END
			END
			ELSE
			BEGIN
				set @NextRunTime = @LastRunTime + @OccursEvery
			END

	END
	ELSE IF @Type=2
	BEGIN
	-- Do weekly Calculations
		
		Declare @Sunday bit,@Monday bit,@Tuesday bit,@Wednesday  bit,@Thursday  bit,@Friday bit,@Saturday bit
		Declare @Weekday int,@CurrentWeekday int,@Increment int,@TempInc int
		
		Select @OccursEvery=isnull(@OccursEvery,1),@Sunday=Sunday,@Monday=Monday,@Tuesday=Tuesday,@Wednesday=Wednesday,@Thursday=Thursday,@Friday=Friday,@Saturday=Saturday 
		From TAScheduleWeekly
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()
		
		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() -1
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate -1
			SET @OccursEvery =1
		END	
		
		SET @CurrentWeekday = datepart(weekday,@LastRunTime)
		SET @Weekday = datepart(weekday,@LastRunTime + 1)
		SET @Increment= (7 * @OccursEvery) 
		IF(@Sunday=1)
		BEGIN
			SET @Increment = (7 * @OccursEvery) + 1 - @CurrentWeekday
		END
		IF(@Monday=1)
		BEGIN
			IF @CurrentWeekday <2
				SET @TempInc =2 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery)+ 2 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Tuesday=1)
		BEGIN
			IF @CurrentWeekday <3
				SET @TempInc =3 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 3 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Wednesday=1)
		BEGIN
			IF @CurrentWeekday <4
				SET @TempInc =4 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 4 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc

		END
		IF(@Thursday=1)
		BEGIN
			IF @CurrentWeekday <5
				SET @TempInc =5 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 5 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Friday=1)
		BEGIN
			IF @CurrentWeekday <6
				SET @TempInc =6 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery)+ 6 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		IF(@Saturday=1)
		BEGIN
			IF @CurrentWeekday <7
				SET @TempInc =7 - @CurrentWeekday
			ELSE 
				SET @TempInc =(7 * @OccursEvery) + 7 - @CurrentWeekday
			IF @TempInc < @Increment
				SET @Increment =@TempInc
		END
		--IF @Increment < (7 * @OccursEvery) + 1
			SET @NextRunTime = @LastRunTime + @Increment

	END
	ELSE IF @Type=3
	BEGIN
	-- Do Monthly Calculations
		
		Declare @Day int,@NthWeek int,@DayofWeek int,@MonthType int
		Select @Day=Day,@OccursEvery =isnull(OccursEvery,1),@NthWeek=NthWeek,@DayofWeek=DayofWeek,@MonthType=Type 
		From TAScheduleMonthly
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()
		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() -1
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate -1
			SET @OccursEvery =1
		END	
		
		IF @MonthType=1
		BEGIN
			IF datepart(day,@LastRunTime)< @Day
				SET @NextRunTime = @LastRunTime + @Day - datepart(day,@LastRunTime)
			ELSE
			BEGIN		
				IF datepart(month,@LastRunTime) + @OccursEvery <=12
					SET @NextRunTime = cast(datepart(month,@LastRunTime) + @OccursEvery as varchar(2)) + '/' + cast(@Day as varchar(2)) + '/' +  cast(datepart(year,@LastRunTime) as varchar(4))
				ELSE
				BEGIN
					Declare @YearPart int,@MonthPart int
					SET @MonthPart = (datepart(month,@LastRunTime) + @OccursEvery) % 12
					SET @Yearpart =  datepart(year,@LastRunTime)  +(datepart(month,@LastRunTime)+ @OccursEvery) / 12
					SET @NextRunTime = cast(@MonthPart as varchar(2)) + '/' + cast(@Day  as varchar(2))  + '/' + cast(@YearPart  as varchar(4)) 
				END
			END
		END
		ELSE
		BEGIN
			Declare @LastDayOfMonth DateTime, @NumberOfWeek int
			Declare @weekFirstDay int
			Declare @interMediateDate datetime
			Declare @FirstWeekDay DateTime
			Declare @LastWeekDay DateTime
			Declare @weekdayoffset int
			
			--@NthWeek is the firt dropdown in second row. -- first-1, Second-2, Third-3,Fourth-4, Last-5
			--@DayOfWeek second dropdown, Sunday-1 Saturday -7 Day-8, Weekday -9 weekends-10
			SET @LastDayOfMonth =DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@LastRunTime)+@OccursEvery + 1,0))
			--Set @FirstWeekDay = cast(cast(datepart(month,@LastRunTime) + @OccursEvery + 1 as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
			Set @FirstWeekDay =cast(cast( DATEPART(MONTH,@LASTDayOfMonth) as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
			Set @WeekFirstDay =datepart(weekday,@FirstWeekDay)
			if(@WeekFirstDay<= @DayofWeek)
				SET @weekdayoffset = @DayofWeek - @WeekFirstDay
			ELSE
				SET @weekdayoffset = @DayofWeek + 7 - @WeekFirstDay

			if(@DayOfWeek between 1 and 7)
			BEGIN
				Declare @currentWeek int
				Set @currentWeek = datepart(week,@LastRunTime)
				IF @NthWeek =5	
				BEGIN
					SET @NthWeek = datepart(week,@LastDayOfMonth) - datePart(week,@FirstWeekDay)
				END
				
				IF(datepart(month,@LastRunTime) = datepart(month,@FirstWeekDay))
				IF((datepart(week,@LastRunTime) - datepart(week,@FirstWeekDay)) < @NthWeek)
					Set @OccursEvery =0
				ELSE IF((datepart(week,@LastRunTime) - datepart(week,@FirstWeekDay)) = @NthWeek AND datepart(weekday,@LastRunTime)< @weekdayoffset)	
					Set @OccursEvery =0
					
					Set @NextRunTime = cast(cast(datepart(month,@LastRunTime) + @OccursEvery as varchar(2)) + '/' + cast((@NthWeek * 7) -6 + @weekdayoffset as varchar(2)) +'/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
					
			END
			ELSE IF @DayOfWeek = 8 -- Nth Day
			BEGIN 
				Set @NextRunTime = cast(cast(datepart(month,@LastRunTime) + @OccursEvery + 1 as varchar(2)) + '/' + cast(@NthWeek as varchar(2)) +'/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
			END
			ELSE IF @DayOfWeek = 9 -- Nth Week day
			BEGIN 
				IF (@NthWeek!=5)
				BEGIN
					
					
					Set @weekFirstDay = datepart(week,@FirstWeekDay)
					if(@weekFirstDay =1)
						SET @FirstWeekDay = @FirstWeekDay + 1
					if(@weekFirstDay =7)
						SET @FirstWeekDay = @FirstWeekDay + 2
					SET @NextRunTime = @FirstWeekDay + @NthWeek
					Set @weekFirstDay = datepart(week,@NextRunTime)
					if(@weekFirstDay =1)
						SET @NextRunTime = @NextRunTime + 1
					if(@weekFirstDay =7)
						SET @NextRunTime = @NextRunTime + 2
				END
				ELSE
				BEGIN
					SET @LastWeekDay = datepart(week,@LastDayOfMonth)
					IF(@LastWeekDay =1) 
						SET @NextRunTime = @LastDayOfMonth -2
					ELSE IF (@LastWeekDay=7)
						SET @NextRunTime = @LastDayOfMonth -1
					ELSE
						SET @NextRunTime = @LastDayOfMonth
				END
			END
			ELSE IF @DayOfWeek = 10 -- Nth week end day
			BEGIN 
				IF (@NthWeek!=5)
				BEGIN
				Set @FirstWeekDay = cast(cast(datepart(month,@LastRunTime) + @OccursEvery  as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
				print @FirstWeekDay 
					Set @weekFirstDay = datepart(weekday,@FirstWeekDay)
					print @weekFirstDay
					IF(@weekFirstDay !=1)
						Set @FirstWeekDay = @FirstWeekDay + (7 - @WeekFirstDay) -- going to next sarturday
					IF(@NthWeek=1)
						SET @NextRunTime = @FirstWeekDay
					IF(@NthWeek=2)
						IF @WeekFirstDay =1
							SET @NextRunTime = @FirstWeekDay + 6
						ELSE 
							SET @NextRunTime = @FirstWeekDay + 1
					IF(@NthWeek=3)
							SET @NextRunTime = @FirstWeekDay + 7
					IF(@NthWeek=4)
						IF @WeekFirstDay =1
							SET @NextRunTime = @FirstWeekDay + 13
						ELSE 
							SET @NextRunTime = @FirstWeekDay + 8
						
				END
				ELSE
				BEGIN
					SET @LastWeekDay = datepart(week,@LastDayOfMonth)
					IF(@LastWeekDay =1 OR @LastWeekDay=6)
						SET @NextRunTime = @LastDayOfMonth
					ELSE
						SET @NextRunTime = @LastDayOfMonth - (@LastWeekDay - 1)
				END
			END
		END

	END
	ELSE IF @Type=4
	BEGIN
	-- Do Yearly Calculations
	Declare @Month int,@NthMonth int,@YearType int
		Select @Month=Month,@Day=Day,@NthWeek=NthWeek,@DayofWeek =DayofWeek, @NthMonth=NthMonth,@YearType=Type 
		From TAScheduleYearly
		Where ScheduleId=@Id AND Status=dbo.GetActiveStatus()
		If @LastRunTime is null
		BEGIN
			Set @LastRunTime = getutcdate() -1
			IF @StartDate > @LastRuntime
				Set @LastRunTime = @StartDate -1
			SET @OccursEvery =1
		END	
		IF @YearType =1 
		BEGIN
			IF DatePart(Month,@LastRunTime)<@Month OR (DatePart(Month,@LastRunTime)=@Month AND
				DatePart(Day,@LastRunTime)<@Day)
				  SET @NextRunTime = cast(@Month as varchar(2)) + '/' + cast(@Day as varchar(2)) + '/' +  CAST(DatePart(Year,@LastRunTime) as varchar(4))
			ELSE
			BEGIN
				  SET @NextRunTime = cast(@Month as varchar(2)) + '/' + cast(@Day as varchar(2)) + '/' +  CAST(DatePart(Year,@LastRunTime) +1 as varchar(4))
			END
			
		END
		ELSE
		BEGIN
			Declare @FirstDayOfMonth DateTime
			
			If(DatePart(month,@LastRunTime)< @NthMonth)
				SET @FirstDayOfMonth =Cast(@NthMonth as varchar(2)) +  '/1/'  +cast( datepart(year,@LastRunTime) as varchar(4))
			else
				SET @FirstDayOfMonth =Cast(@NthMonth as varchar(2)) +  '/1/'  +cast( datepart(year,@LastRunTime) + 1 as varchar(4))
			SET @LastDayOfMonth =DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@FirstDayOfMonth) + 1,0))
			Set @WeekFirstDay =datepart(weekday,@FirstDayOfMonth) -- first week day ie sunday=1 ,, sat=7 for first day of month
			if(@FirstWeekDay <= @DayofWeek)
				SET @weekdayoffset = @DayofWeek - @WeekFirstDay
			ELSE
				SET @weekdayoffset = @DayofWeek + 7 - @WeekFirstDay

			if(@DayOfWeek between 1 and 7)
				BEGIN
					
					IF @NthWeek =5	
					BEGIN
						SET @NthWeek = datepart(weekday,@LastDayOfMonth)
					END
						Set @NextRunTime = cast(cast(datepart(month,@FirstDayOfMonth) as varchar(2)) + '/' + cast((@NthWeek * 7) -6 + @weekdayoffset as varchar(2)) +'/' + cast(datepart(year,@FirstDayOfMonth) as varchar(4)) as datetime)
				END
				ELSE IF @DayOfWeek = 8 -- Nth Day
				BEGIN 
					Set @NextRunTime = cast(cast(datepart(month,@FirstDayOfMonth)  as varchar(2)) + '/' + cast(@NthWeek as varchar(2)) +'/' + cast(datepart(year,@FirstDayOfMonth) as varchar(4)) as datetime)
				END
				ELSE IF @DayOfWeek = 9 -- Nth Week day
				BEGIN 
					IF (@NthWeek!=5)
					BEGIN
						
						Set @FirstWeekDay = cast(cast(datepart(month,@FirstDayOfMonth) as varchar(2)) + '/1/' + cast(datepart(year,@LastRunTime) as varchar(4)) as datetime)
						Set @weekFirstDay = datepart(week,@FirstWeekDay)
						if(@weekFirstDay =1)
							SET @FirstWeekDay = @FirstWeekDay + 1
						if(@weekFirstDay =7)
							SET @FirstWeekDay = @FirstWeekDay + 2
						SET @NextRunTime = @FirstWeekDay + @NthWeek
						Set @weekFirstDay = datepart(week,@NextRunTime)
						if(@weekFirstDay =1)
							SET @NextRunTime = @NextRunTime + 1
						if(@weekFirstDay =7)
							SET @NextRunTime = @NextRunTime + 2
					END
					ELSE
					BEGIN
						SET @LastWeekDay = datepart(week,@LastDayOfMonth)
						IF(@LastWeekDay =1) 
							SET @NextRunTime = @LastDayOfMonth -2
						ELSE IF (@LastWeekDay=7)
							SET @NextRunTime = @LastDayOfMonth -1
						ELSE
							SET @NextRunTime = @LastDayOfMonth
					END
				END
				ELSE IF @DayOfWeek = 10 -- Nth week end day
				BEGIN 
					IF (@NthWeek!=5)
					BEGIN
						Set @FirstWeekDay = cast(cast(datepart(month,@FirstDayOfMonth) as varchar(2)) + '/1/' + cast(datepart(year,@FirstDayOfMonth) as varchar(4)) as datetime)
						Set @weekFirstDay = datepart(week,@FirstWeekDay)
						IF(@weekFirstDay !=1)
							Set @FirstWeekDay = @FirstWeekDay + (7 - @WeekFirstDay) -- going to next sarturday
						IF(@NthWeek=1)
							SET @NextRunTime = @FirstWeekDay
						IF(@NthWeek=2)
							IF @WeekFirstDay =1
								SET @NextRunTime = @FirstWeekDay + 6
							ELSE 
								SET @NextRunTime = @FirstWeekDay + 1
						IF(@NthWeek=3)
								SET @NextRunTime = @FirstWeekDay + 7
						IF(@NthWeek=4)
							IF @WeekFirstDay =1
								SET @NextRunTime = @FirstWeekDay + 13
							ELSE 
								SET @NextRunTime = @FirstWeekDay + 8
							
					END
					ELSE
					BEGIN
						SET @LastWeekDay = datepart(week,@LastDayOfMonth)
						IF(@LastWeekDay =1 OR @LastWeekDay=6)
							SET @NextRunTime = @LastDayOfMonth
						ELSE
							SET @NextRunTime = @LastDayOfMonth - (@LastWeekDay - 1)
					END
				END

		END

	END

	Set @NextRunTime = CAST(FLOOR(CAST(@NextRunTime AS FLOAT ))AS DateTime)
	IF @NextRunTime > @EndDate AND @EndDate is not null
		SET @NextRunTime =NULL	
	ELSE
		Set @NextRunTime = @NextRunTime  + CONVERT(VARCHAR(8),@StartTime,108)
	
	IF @IncrementRunCount = 1
		SET @Runcount = @Runcount + 1

	Update [dbo].[TASchedule]
		SET LastRunTime=@OldLastRunTime,
			NextRunTime=@NextRunTime,
			ModifiedBy =@ModifiedBy,
			ModifiedDate =@ModifiedDate,
			RunCount = @Runcount
			Where Id=@Id


END

GO
PRINT 'Updating the created date in the XMLVersion for all the OLD campaigns'
GO
if exists (
select 1 FROM VEVersion v
	INNER JOIN MKCampaign c on v.ObjectId = c.Id
WHERE v.XMLString.value('(/Campaign/CreatedDate)[1]', 'nvarchar(max)') = '0001-01-01T00:00:00'
)
UPDATE VEVersion
SET XMLString = REPLACE(REPLACE(CAST(v.XMLString AS NVARCHAR(MAX)), '<CreatedBy>00000000-0000-0000-0000-000000000000</CreatedBy>', '<CreatedBy>' + CAST(c.CreatedBy AS NVARCHAR(MAX)) + '</CreatedBy>'), '<CreatedDate>0001-01-01T00:00:00</CreatedDate>', '<CreatedDate>' + CONVERT(NVARCHAR(MAX), c.CreatedDate, 126) + '</CreatedDate>')
FROM VEVersion v
	INNER JOIN MKCampaign c on v.ObjectId = c.Id
WHERE v.XMLString.value('(/Campaign/CreatedDate)[1]', 'nvarchar(max)') = '0001-01-01T00:00:00'


GO
PRINT 'Updating assembly Bridgeline.CLRFunctions'


GO
PRINT N'Dropping [dbo].[GenerateXml]...';


GO
IF OBJECT_ID(N'[dbo].[GenerateXml]', 'AF') IS NOT NULL
DROP AGGREGATE [dbo].[GenerateXml];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedImageProperty]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedImageProperty]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedFileProperty]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedFileProperty]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetContentLocation]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetContentLocation]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetContentLocation];


GO
PRINT N'Dropping [dbo].[RegexSelectAll]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegexSelectAll]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[RegexSelectAll];


GO
PRINT N'Dropping [dbo].[Regex_ReplaceMatches]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_ReplaceMatches]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_ReplaceMatches];


GO
PRINT N'Dropping [dbo].[Regex_IsMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_IsMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_IsMatch];


GO
PRINT N'Dropping [dbo].[Regex_GetMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_GetMatch];


GO
PRINT N'Dropping [dbo].[NavFilter_GetQuery]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NavFilter_GetQuery]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[NavFilter_GetQuery];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsTextContentMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsTextContentMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsTextContentMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsPagePropertiesMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsPageNameMatch]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPageNameMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsPageNameMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsLinkInContentDefinition]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsImagePropertiesMatch]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsImagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsFilePropertiesMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsFilePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_IsContentDefinitionMatch]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsContentDefinitionMatch]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedTextContentText]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedTextContentText]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedPageProperties]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageProperties]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedPageName]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageName]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName];


GO
PRINT N'Dropping [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]...';


GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition];


GO
PRINT N'Dropping [dbo].[Regex_GetMatches]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatches]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[Regex_GetMatches];


GO
PRINT N'Dropping [dbo].[GetRelationTable]...';


GO
IF  EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRelationTable]') AND (type = 'FS' OR type = 'FT'))
DROP FUNCTION [dbo].[GetRelationTable];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_Save]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Save]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_Save];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_RemoveContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_RemoveContainer]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_AddContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_AddContainer]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_AddContainer];


GO
PRINT N'Dropping [dbo].[CLRPageMap_SavePageDefinition]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMap_SavePageDefinition]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMap_SavePageDefinition];


GO
PRINT N'Dropping [dbo].[FindAndReplace_UpdateLinks]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_UpdateLinks]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[FindAndReplace_UpdateLinks];


GO
PRINT N'Dropping [dbo].[FindAndReplace_Replace]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Replace]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[FindAndReplace_Replace];


GO
PRINT N'Dropping [dbo].[FindAndReplace_Find]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Find]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[FindAndReplace_Find];


GO
PRINT N'Dropping [dbo].[Contact_SearchCLR]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_SearchCLR]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[Contact_SearchCLR];


GO
PRINT N'Dropping [dbo].[Contact_GetAutoDistributionListContactCount]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_GetAutoDistributionListContactCount]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount];


GO
PRINT N'Dropping [dbo].[CLSPageMapNode_AttachWorkflow]...';


GO
IF OBJECT_ID(N'[dbo].[CLSPageMapNode_AttachWorkflow]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow];


GO
PRINT N'Dropping [dbo].[CLRPageMapNode_Update]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Update]', 'PC') IS NOT NULL
DROP PROCEDURE [dbo].[CLRPageMapNode_Update];


GO
PRINT N'Dropping [Bridgeline.CLRFunctions]...';


GO
IF EXISTS (SELECT 1 FROM sys.assemblies WHERE name = N'Bridgeline.CLRFunctions')
DROP ASSEMBLY [Bridgeline.CLRFunctions];


GO
PRINT N'Creating [Bridgeline.CLRFunctions]...';

GO
CREATE ASSEMBLY [Bridgeline.CLRFunctions]
    AUTHORIZATION [dbo]
    FROM 0x4D5A90000300000004000000FFFF0000B800000000000000400000000000000000000000000000000000000000000000000000000000000000000000800000000E1FBA0E00B409CD21B8014CCD21546869732070726F6772616D2063616E6E6F742062652072756E20696E20444F53206D6F64652E0D0D0A2400000000000000504500004C01030047787F540000000000000000E00002210B010B00006401000006000000000000AE8201000020000000A001000000001000200000000200000400000000000000040000000000000000E001000002000000000000030040850000100000100000000010000010000000000000100000000000000000000000548201005700000000A00100E80300000000000000000000000000000000000000C001000C0000001C8101001C0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000200000080000000000000000000000082000004800000000000000000000002E74657874000000B4620100002000000064010000020000000000000000000000000000200000602E72737263000000E803000000A001000004000000660100000000000000000000000000400000402E72656C6F6300000C00000000C0010000020000006A010000000000000000000000000040000042000000000000000000000000000000009082010000000000480000000200050048BD0000D4C300000100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000360002731500000A7D010000042A0000133003002500000001000011000F01281600000A0A062D18027B0100000472010000700F01281700000A6F1800000A262A5600027B010000040F017B010000046F1900000A262A0013300300390000000200001100027B010000041672230000706F1A00000A26027B0100000472570000706F1B00000A26027B010000046F1C00000A731D00000A0A2B00062A4E0002036F1E00000A731F00000A7D010000042A520003027B010000046F1C00000A6F2000000A002A00001B300700C609000003000011007E2200000A0B0F02FE16050000016F1C00000A282300000A131811182D7E000F02FE16050000016F1C00000A728D000070282400000A16FE01131811182D0A0072990000700B002B520F02FE16050000016F1C00000A722C010070282400000A16FE01131811182D0A0072400100700B002B280F02FE16050000016F1C00000A72EB010070282400000A16FE01131811182D080072030200700B0000721D020070028C09000001070E048C09000001282500000A0C72A6020070028C09000001070E048C09000001282500000A0D728B0300700A72BB030070130406732600000A130500732700000A1306732700000A130772F10300700F00282800000A13191219FE16110000016F1C00000A7220060070282900000A130811081105732A00000A130911056F2B00000A00110911086F2C00000A001109732D00000A130A110A11066F2E00000A26724406007072030900707217090070282F00000A1308110911086F2C00000A00110A11076F2E00000A2611066F3000000A6F3100000A11076F3000000A6F3100000A5817588D36000001130B110B167E2200000AA27E2200000A130E16130F722F0900700F01281700000A722F090070282900000A130E0F01281700000A6F3200000A13102B6800110E722F0900701210283300000A283400000A72330900701210283300000A7239090070282900000A6F3500000A130E110E723F0900701210283300000A283400000A72430900701210283300000A7239090070282900000A6F3500000A130E00111017591310111017FE0416FE01131811182D8A7249090070110E283400000A130E1713110011066F3000000A6F3600000A131A381F060000111A6F3700000A7440000001131200111272510900706F3800000AA53E000001130C1112726D0900706F3800000AA53E000001130D000011076F3000000A6F3600000A131B38A3050000111B6F3700000A7440000001131500111272890900706F3800000A6F1C00000A6F3900000A111572A10900706F3800000A6F1C00000A6F3900000A282400000A2C2C111272AB0900706F3800000A6F1C00000A111572BB0900706F3800000A6F1C00000A282400000A16FE012B011700131811183A2905000000111572C10900706F3800000A6F1C00000A72E509007072F50900706F3500000A6F3A00000A1314111272F70900706F3800000A6F1C00000A72090A0070720D0A00706F3500000A1313111272130A00706F3800000A6F1C00000A6F3A00000A6F3900000A721B0A0070282400000A16FE01131811182D1372250A0070111372250A0070282900000A1313111572290A00706F3800000A6F1C00000A723F0A0070282400000A16FE01131811183AB201000000111472430A0070282400000A2D11111472510A0070282400000A16FE012B011600131811182D7E00110B11111D8D36000001131C111C1672590A0070A2111C17111572A10900706F3800000A6F1C00000A72090A0070720D0A00706F3500000AA2111C1872650A0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A722F090070A2111C1B1113A2111C1C722F090070A2111C283B00000AA20038000100001114727D0A0070283C00000A16FE01131811182D7200110B11111C8D36000001131C111C16111572A10900706F3800000A6F1C00000A72090A0070720D0A00706F3500000AA2111C17722F090070A2111C18111272130A00706F3800000A6F1C00000AA2111C19728F0A0070A2111C1A1113A2111C1B72090A0070A2111C283B00000AA2002B7900110B11111D8D36000001131C111C1672950A0070A2111C17111572A10900706F3800000A6F1C00000A72090A0070720D0A00706F3500000AA2111C1872B90A0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A72C90A0070A2111C1B1113A2111C1C72110B0070A2111C283B00000AA2001111175813110038C4020000111572290A00706F3800000A6F1C00000A72230B0070282400000A16FE01131811183A9D0200000017130F111472510A0070282400000A2D11111472430A0070282400000A16FE012B011600131811183AC700000000110E72270B00706F3D00000A16FE01131811182D3C00110B1111722F0B0070111272AB0900706F3800000A6F1C00000A111272130A00706F3800000A6F1C00000A11136F3A00000A282500000AA2002B6F00110B11111D8D36000001131C111C16721E0C0070A2111C17111272AB0900706F3800000A6F1C00000AA2111C18723C0C0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A722F090070A2111C1B11136F3A00000AA2111C1C727A0C0070A2111C283B00000AA2000038A10100001114727D0A0070283C00000A16FE01131811183AC700000000110E72270B00706F3D00000A16FE01131811182D3C00110B1111727E0C0070111272AB0900706F3800000A6F1C00000A111272130A00706F3800000A6F1C00000A11136F3A00000A282500000AA2002B6F00110B11111D8D36000001131C111C16721E0C0070A2111C17111272AB0900706F3800000A6F1C00000AA2111C1872530D0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A728F0A0070A2111C1B11136F3A00000AA2111C1C72710D0070A2111C283B00000AA2000038C200000000110E72270B00706F3D00000A16FE01131811182D3C00110B111172770D0070111272AB0900706F3800000A6F1C00000A111272130A00706F3800000A6F1C00000A11136F1C00000A282500000AA2002B6F00110B11111D8D36000001131C111C16721E0C0070A2111C17111272AB0900706F3800000A6F1C00000AA2111C1872EC0E0070A2111C19111272130A00706F3800000A6F1C00000AA2111C1A72C90A0070A2111C1B11136F3A00000AA2111C1C72580F0070A2111C283B00000AA20000111117581311000000111B6F3E00000A131811183A4DFAFFFFDE1D111B7541000001131D111D14FE01131811182D08111D6F3F00000A00DC000000111A6F3E00000A131811183AD1F9FFFFDE1D111A7541000001131D111D14FE01131811182D08111D6F3F00000A00DC007E2200000A13160F02FE16050000016F1C00000A282300000A131811182D2D00726C0F00700F02FE16050000016F1C00000A722F0900700F03FE16050000016F1C00000A284000000A131600110F2C11110E72270B00706F3D00000A16FE012B011600131811182D3F001B8D36000001131C111C1672800F0070A2111C1708A2111C18110E110B284100000AA2111C1972B40F0070A2111C1A1116A2111C283B00000A1304002B3D001B8D36000001131C111C1672800F0070A2111C1709A2111C18110E110B284100000AA2111C1972B40F0070A2111C1A1116A2111C283B00000A13040000DE14110514FE01131811182D0811056F3F00000A00DC001104284200000A13172B0011172A0000414C000002000000B1020000BA0500006B0800001D000000000000000200000067020000360600009D0800001D0000000000000002000000E4000000BF080000A30900001400000000000000133002003700000005000011000F00281600000A0C082D25000F01281700000A0F02284300000A734400000A0A060F00281700000A6F4500000A0B2B04160B2B00072A00133002005100000006000011000F00281600000A0C082D3F000F01281700000A0F02284300000A734400000A0A060F00281700000A6F4500000A16FE010C082D15060F00281700000A6F4600000A6F4700000A0B2B0500140B2B00072A000000133002004C00000007000011000F00281600000A0C082D3A000F01281700000A0F02284300000A734400000A0A060F00281700000A6F4500000A16FE010C082D10060F00281700000A6F4800000A0B2B0500140B2B00072A62000302740E0000016F4700000A284200000A81050000012A000000133003003E00000006000011000F00281600000A0C082D2C000F01281700000A0F02284300000A734400000A0A060F00281700000A0F03281700000A6F4900000A0B2B04140B2B00072A000013300400FD0000000800001100731500000A0A72C00F0070732600000A0B076F4A00000A0C0872F00F00706F2C00000A00086F4B00000A72571600701F0E6F4C00000A0F00282800000A8C110000016F4D00000A00086F4B00000A725F1600701F0E6F4C00000A0F01282800000A8C110000016F4D00000A00076F2B00000A00086F4E00000A0D096F4F00000A16FE01130511052D2E002B1E0006726F16007009727D1600706F5000000A74360000016F1800000A2600096F5100000A130511052DD600096F5200000A00076F5300000A00066F5400000A19FE0216FE01130511052D1900066F1C00000A16066F5400000A19596F5500000A13042B097E2200000A13042B0011042A0000001B3004006601000009000011001A0F01281700000A0F02285600000A0F03285600000A28790000060A170F01281700000A0F02285600000A0F03285600000A28780000060B0006026F5700000A6F4800000A6F5800000A130838DB00000011086F3700000A740E0000010C00086F4700000A287E0000060D07096F4500000A16FE01130911092D08171307DDDE0000000F04285600000A16FE01130911093A95000000001714161628790000061304180F01281700000A0F02285600000A0F03285600000A28790000061305001104096F4800000A6F5800000A130A2B2C110A6F3700000A740E000001130600110511066F4700000A6F4500000A16FE01130911092D05171307DE6500110A6F3E00000A130911092DC7DE1D110A7541000001130B110B14FE01130911092D08110B6F3F00000A00DC00000011086F3E00000A130911093A15FFFFFFDE1D11087541000001130B110B14FE01130911092D08110B6F3F00000A00DC001613072B000011072A0000011C00000200D0003D0D011D0000000002004D00F23F011D000000001E02285900000A2A1E02285900000A2A1E02285900000A2A13300500330000000A0000110003027B200000047B1F000004027B210000047C1D000004281700000A027B22000004027B2300000428110000060A2B00062A0013300300220000000A0000110003027B1F000004027B1E0000047C1D000004281700000A28100000060A2B00062A000013300500070100000B000011738300000613051105047D1D00000400026F5A00000A130711073AE0000000140D73840000061304110411057D1E000004001A0F01281700000A0F03285600000A0F04285600000A28790000060A1104170F01281700000A0F03285600000A0F04285600000A28780000067D1F0000040F05285600000A16FE01130711072D5773860000060C0811047D200000040811057D2100000400081714161628790000067D2200000408180F01281700000A0F03285600000A0F04285600000A28790000067D2300000408FE0687000006735B00000A0B002B1700092D101104FE0685000006735B00000A0D2B00090B0006026F5700000A076F5C00000A13062B051413062B0011062A0013300300230000000C00001100026F4700000A287E0000060A0306046F4900000A0A06287F0000060A060B2B00072A1E02285900000A2A0013300300180000000A0000110003027B25000004027B24000004281C0000060A2B00062A13300300530000000D00001173880000060C08047D24000004080E047D2500000400026F4700000A287E0000060A0306087B240000046F4900000A0A08FE0689000006735B00000A0B0506076F5C00000A0A06287F0000060A060D2B00092A00133004009300000005000011001F200F03281700000A0F04285600000A0F05285600000A28780000060A0F00281600000A2D12060F00281700000A6F4500000A16FE012B0117000C082D04170B2B4E0F01281600000A2D12060F01281700000A6F4500000A16FE012B0117000C082D04170B2B290F02281600000A2D12060F02281700000A6F4500000A16FE012B0117000C082D04170B2B04160B2B00072A00133004004700000006000011000F00281600000A0C082D35001F200F01281700000A0F03285600000A0F04285600000A28780000060A060F00281700000A0F02281700000A6F4900000A0B2B04140B2B00072A00133004009300000005000011001F100F03281700000A0F04285600000A0F05285600000A28780000060A0F00281600000A2D12060F00281700000A6F4500000A16FE012B0117000C082D04170B2B4E0F01281600000A2D12060F01281700000A6F4500000A16FE012B0117000C082D04170B2B290F02281600000A2D12060F02281700000A6F4500000A16FE012B0117000C082D04170B2B04160B2B00072A00133004004700000006000011000F00281600000A0C082D35001F100F01281700000A0F03285600000A0F04285600000A28780000060A060F00281700000A0F02281700000A6F4900000A0B2B04140B2B00072A00133004002F0000000E000011001A0F01281700000A0F02285600000A0F03285600000A28780000060A060F00281700000A6F4500000A0B2B00072A00133004004600000006000011000F00281600000A0C082D34001A0F01281700000A0F03285600000A0F04285600000A28780000060A060F00281700000A0F02281700000A6F4900000A0B2B04140B2B00072A0000133004002E0000000E000011001E0F01281700000A0F02285600000A0F03285600000A28780000060A06026F5700000A6F4500000A0B2B00072A000013300400440000000600001100026F5A00000A0C082D33001E0F01281700000A0F03285600000A0F04285600000A28780000060A06026F5700000A0F02281700000A6F4900000A0B2B04140B2B00072A1B300400FE0000000F000011000F00281600000A130511053AE400000000170F01281700000A0F02285600000A0F03285600000A28780000060A060F00281700000A6F4500000A16FE01130511052D0817130438AE0000000F04285600000A16FE01130511053A95000000001714161628790000060B180F01281700000A0F02285600000A0F03285600000A28790000060C00070F00281700000A6F4800000A6F5800000A13062B2911066F3700000A740E0000010D0008096F4700000A6F4500000A16FE01130511052D05171304DE350011066F3E00000A130511052DCADE1D110675410000011307110714FE01130511052D0811076F3F00000A00DC0000001613042B000011042A00000110000002009B003AD5001D000000001E02285900000A2A1E02285900000A2A13300300220000000A0000110003027B28000004027B270000047C26000004281700000A281C0000060A2B00062A000013300500CB00000010000011738A00000613051105047D26000004000F00281600000A130711073AA3000000000F00281700000A0A170F01281700000A0F03285600000A0F04285600000A28780000060B070611057C26000004281700000A6F4900000A0A0F05285600000A16FE01130711072D55738B0000061304110411057D27000004001714161628790000060C1104180F01281700000A0F03285600000A0F04285600000A28790000067D280000041104FE068C000006735B00000A0D0806096F5C00000A0A000613062B051413062B0011062A0013300300130000000A0000110003026F4700000A046F4900000A0A2B00062A001B3004009B00000011000011001A14161628790000060A0F01281700000A0F02285600000A287B0000060B0006026F5700000A6F4800000A6F5800000A13052B3011056F3700000A740E0000010C00086F4700000A287E0000060D07096F4500000A16FE01130611062D05171304DE330011056F3E00000A130611062DC3DE1D110575410000011307110714FE01130611062D0811076F3F00000A00DC001613042B000011042A0001100000020033004174001D000000001B300500130100001200001100026F5A00000A130811083AFB000000001A14161628790000060A0F01281700000A0F03285600000A287B0000060B731500000A0C160D0006026F5700000A6F4800000A6F5800000A13092B6711096F3700000A740E00000113040011046F4700000A287E00000613050711050F02281700000A6F4900000A287F000006130608026F5700000A0911046F5D00000A09596F5500000A1106283400000A6F1B00000A2611046F5D00000A11046F5E00000A580D0011096F3E00000A130811082D8CDE1D11097541000001130A110A14FE01130811082D08110A6F3F00000A00DC0008026F5700000A09026F5700000A6F3200000A09596F5500000A6F1B00000A26086F1C00000A13072B051413072B0011072A000110000002004B0078C3001D000000001B300500720400001300001100728B0300700A06732600000A0B732700000A0C732700000A0D7289160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A007293160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A00729D160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072A7160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072B1160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072BB160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072C5160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072CF160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072D9160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072E3160070D011000001285F00000A736000000A1304096F6100000A11046F6200000A0072EF16007002286300000A1306110607732A00000A1307076F2B00000A0011076F6400000A6F1C00000A13051105178D4E00000113101110161F2D9D11106F6500000A130811088E6918FE0116FE01131111113A8B020000000F01286600000A131111112D560072BE1700701108169A1108179A0F01FE16090000016F1C00000A282500000A1306110711066F2C00000A0011076F6400000A6F1C00000A13051105178D4E00000113101110161F2D9D11106F6500000A1308002B17001108161108169A286700000A176A58286800000AA20011088E6918FE0116FE01131111113AFC0100000072C51800701108169A1108179A282F00000A1306110711066F2C00000A001107732D00000A13091109086F2E00000A26076F5300000A00086F3000000A6F3100000A736900000A130A082C13086F3000000A6F3100000A16FE0216FE012B011700131111113A9001000000086F3000000A6F3600000A1312384D01000011126F3700000A7440000001130C00110C72501900706F3800000AA551000001110C72581900706F3800000AA55100000159176AFE0116FE01131111112D7B00110A110C6F6A00000A00110A6F6B00000A130D096F6C00000A130B17130E2B2D00110B110E1759110D110D8E69110E599A744000000172601900706F3800000A6F6D00000A0000110E1758130E110E110D8E69FE0216FE01131111112DC2096F3000000A110B6F6E00000A00110A6F6F00000A26003892000000002B7100110A6F7000000A7440000001130B110B72581900706F3800000AA551000001110C72581900706F3800000AA5510000012F29110B72501900706F3800000AA551000001110C72581900706F3800000AA551000001FE0216FE012B011700131111112D022B1E00110A6F6F00000A260000110A6F7100000A16FE02131111113A7CFFFFFF110A110C6F6A00000A00000011126F3E00000A131111113AA3FEFFFFDE1D111275410000011313111314FE01131111112D0811136F3F00000A00DC000000096F3000000A130F2B00110F2A0000411C000002000000E102000064010000450400001D00000000000000133003008E03000014000011000214FE010B073A0A030000000274400000010A06166F7200000A7E7300000A2E1306166F7200000A6F1C00000A282300000A2B0117000B072D190306166F7200000AA511000001287400000A81090000012B0B037E7500000A810900000106176F7200000A7E7300000A2E1306176F7200000A6F1C00000A282300000A2B0117000B072D190406176F7200000AA511000001287400000A81090000012B0B047E7500000A810900000106186F7200000A7E7300000A2E1306186F7200000A6F1C00000A282300000A2B0117000B072D190506186F7200000AA511000001287400000A81090000012B0B057E7500000A810900000106196F7200000A7E7300000A2E1306196F7200000A6F1C00000A282300000A2B0117000B072D1A0E0406196F7200000AA511000001287400000A81090000012B0C0E047E7500000A8109000001061A6F7200000A7E7300000A2E13061A6F7200000A6F1C00000A282300000A2B0117000B072D1A0E05061A6F7200000AA511000001287400000A81090000012B0C0E057E7500000A8109000001061B6F7200000A7E7300000A2E13061B6F7200000A6F1C00000A282300000A2B0117000B072D1A0E06061B6F7200000AA511000001287400000A81090000012B0C0E067E7500000A8109000001061C6F7200000A7E7300000A2E13061C6F7200000A6F1C00000A282300000A2B0117000B072D1A0E07061C6F7200000AA511000001287400000A81090000012B0C0E077E7500000A8109000001061D6F7200000A7E7300000A2E13061D6F7200000A6F1C00000A282300000A2B0117000B072D1A0E08061D6F7200000AA511000001287400000A81090000012B0C0E087E7500000A8109000001061E6F7200000A7E7300000A2E13061E6F7200000A6F1C00000A282300000A2B0117000B072D1A0E09061E6F7200000AA511000001287400000A81090000012B0C0E097E7500000A8109000001061F096F7200000A7E7300000A2E14061F096F7200000A6F1C00000A282300000A2B0117000B072D1B0E0A061F096F7200000AA511000001287400000A81090000012B0C0E0A7E7500000A8109000001002B7700037E7500000A8109000001047E7500000A8109000001057E7500000A81090000010E047E7500000A81090000010E057E7500000A81090000010E067E7500000A81090000010E077E7500000A81090000010E087E7500000A81090000010E097E7500000A81090000010E0A7E7500000A8109000001002A1E02285900000A2A00001B300800D50000001500001100170A7E7800000A0B0F04287900000A130611062D0A000F04285600000A0A000F05286600000A130611062D0A000F05282800000A0B000516737A00000A810A000001026F5A00000A130611063A8200000000160C7E2200000A0D151304728B030070732600000A13050011056F2B00000A001105283000000600020304120412021203110507282900000600051104737A00000A810A0000010F01282800000A08090F02284300000A110506283200000600110528310000060000DE14110514FE01130611062D0811056F3F00000A00DC00002A0000000110000002006A0054BE0014000000001B3008008600000016000011000516737A00000A810A000001026F5A00000A130411042D6C00160A7E2200000A0B150C728B030070732600000A0D00096F2B00000A0009283000000600020304120212001201097E7800000A282900000600060928240000060C0928310000060000DE120914FE01130411042D07096F3F00000A00DC000508737A00000A810A000001002A00000110000002002F00366500120000000013300300440000001700001100178D130000010A737B00000A0B0772721900706F7C00000A00071E6F7D00000A0007028C3E0000016F4D00000A00061607A27280190070060328420000060C2B00082A133003006C0000001800001100188D130000010A737B00000A0B0772B21900706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272E4190070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772141A00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272241A0070060428420000060D2B00092A13300300450000001700001100178D130000010A737B00000A0B0772C81900706F7C00000A00071F0E6F7D00000A0007028C110000016F4D00000A00061607A272501A0070060328420000060C2B00082A000000133003006C0000001800001100188D130000010A737B00000A0B0772881A00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272A61A0070060428420000060D2B00092A1B300400790700001900001100737E00000A0A06026F5700000A6F7F00000A000E0416540E0572F509007051051554000E0628440000066F8000000A130F381F070000120F288100000A0C00054A2D1D087272190070283C00000A2C100872E01A0070283C00000A16FE012B011700131011102D0538E8060000060828450000060D092C0E096F8200000A16FE0116FE012B011600131011102D0538C20600000813111111399C060000FE137E290000043A160100001F15738300000A2572881A007016288400000A2572F41A007017288400000A2572B219007018288400000A2572141A007019288400000A2572121B00701A288400000A2572341B00701B288400000A25724A1B00701C288400000A2572681B00701D288400000A2572861B00701E288400000A2572961B00701F09288400000A2572BA1B00701F0A288400000A2572E81B00701F0B288400000A25720C1C00701F0C288400000A2572381C00701F0D288400000A2572541C00701F0E288400000A2572681C00701F0F288400000A25727A1C00701F10288400000A25728E1C00701F11288400000A2572A41C00701F12288400000A2572721900701F13288400000A2572E01A00701F14288400000AFE138029000004FE137E2900000411111212288500000A3965050000111245150000000500000027000000490000006600000083000000A0000000BD000000DF000000FC0000001B0100003D0100005F01000081010000A3010000C20100002B02000094020000960300007D040000E3040000F904000038050500000972881A007028530000060B05070F01282800000A0E0628280000065438E30400000972C01C007028530000060B05070F01282800000A0E06282F0000065438C10400000928520000060B05070F01282800000A0E0628250000065438A40400000928520000060B05070F01282800000A0E0628260000065438870400000928520000060B05070F01282800000A0E06284100000654386A0400000928520000060B05070F01282800000A0E06284000000654384D0400000972C01C007028530000060B05070F01282800000A0E06283F00000654382B0400000928520000060B05070F01282800000A0E06283E00000654380E04000009284800000613040511040F01282800000A0E0628380000065438EF0300000972961B007028530000060B05070F01282800000A0E06283C0000065438CD0300000972BA1B007028530000060B05070F01282800000A0E06282B0000065438AB0300000972E81B007028530000060B05070F01282800000A0E06283A00000654388903000009720C1C007028530000060B05070F01282800000A0E06283B00000654386703000009284900000613050511050F01282800000A0E06283900000654384803000009284A000006130609166F8600000A6F8700000A72F01C00706F8800000A2C2A09166F8600000A6F8700000A72F01C00706F8800000A6F8900000A7E2200000A283C00000A16FE012B011700131011102D120511060F01282800000A0E0628370000065438DF02000009284B000006130709166F8600000A6F8700000A72F01C00706F8800000A2C2A09166F8600000A6F8700000A72F01C00706F8800000A6F8900000A7E2200000A283C00000A16FE012B011700131011102D120511070F01282800000A0E06283300000654387602000009284C000006130816738A00000A130909166F8600000A6F8700000A72FE1C00706F8800000A2C2409166F8600000A6F8700000A72FE1C00706F8800000A6F8900000A1209288B00000A2B011700131011102D0A0016738A00000A1309007E8C00000A288D00000A130A09166F8600000A6F8700000A72181D00706F8800000A2C2409166F8600000A6F8700000A72181D00706F8800000A6F8900000A120A288B00000A2B011700131011102D0E007E8C00000A288D00000A130A00110916738A00000A288E00000A2C16110A7E8C00000A288D00000A288E00000A16FE012B011700131011102D14000511080F01282800000A0E0628340000065400387401000009284D000006130B16130C09166F8600000A6F8700000A72321D00706F8800000A2C2409166F8600000A6F8700000A72321D00706F8800000A6F8900000A120C288F00000A2B011700131011102D050016130C007E9000000A289100000A130D09166F8600000A6F8700000A724C1D00706F8800000A2C2409166F8600000A6F8700000A724C1D00706F8800000A6F8900000A120D288F00000A2B011700131011102D0E007E9000000A289100000A130D00110C2C10110D7E9000000A289100000AFE012B011700131011102D140005110B0F01282800000A0E0628350000065400388D00000009284E000006130E09166F8600000A6F8700000A72F01C00706F8800000A2C2A09166F8600000A6F8700000A72F01C00706F8800000A6F8900000A7E2200000A283C00000A16FE012B011700131011102D1205110E0F01282800000A0E062836000006542B2709166F8600000A6F9200000A0E04288F00000A262B110E0509166F8600000A6F9200000A512B00050E070E06282A00000654050F01282800000A0E0628270000065400120F289300000A131011103AD1F8FFFFDE0F120FFE160200001B6F3F00000A00DC002A000000411C0000020000003200000036070000680700000F0000000000000013300300450000001700001100178D130000010A737B00000A0B0772661D00706F7C00000A00071F0E6F7D00000A0007028C110000016F4D00000A00061607A272821D0070060328420000060C2B00082A000000133003006C0000001800001100188D130000010A737B00000A0B0772B61D00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272EA1D0070060428420000060D2B00092A13300300720000001800001100198D130000010A737B00000A0B0772B21900706F7C00000A00071F196F7D00000A00070F007B030000046F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272E4190070060428420000060D2B00092A00001B300400880100001A000011001200FE150500000200026F9400000A1306383901000011066F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613071207289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D02000004739600000A0C076F8700000A72481E00706F8800000A2C1C076F8700000A72481E00706F8800000A6F8900000A282300000A2B011700130811083A860000000000076F8700000A72481E00706F8800000A6F8900000A178D4E00000113091109161F209D11096F6500000A130A16130B2B35110A110B9A0D00096F3200000A1F24FE0116FE01130811082D1400120409289700000A000811046F9800000A000000110B1758130B110B110A8E69FE04130811082DBD12000828510000067D03000004002B0E0012007E2200000A7D03000004000011066F3E00000A130811083AB7FEFFFFDE1D11067541000001130C110C14FE01130811082D08110C6F3F00000A00DC000613052B0011052A411C0000020000001200000050010000620100001D00000000000000133003005B0000001B000011001B8D360000010C081672601E0070A2081702A20818729E1E0070A208190F01283300000AA2081A727A0C0070A208283B00000A0A739900000A0B07046F9A00000A0007066F2C00000A0007176F9B00000A00076F9C00000A262A00133003006C0000001800001100188D130000010A737B00000A0B0772F41A00706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272A41E0070060428420000060D2B00092A133002002D0000001C0000110072DC1E00700A739900000A0B07026F9A00000A0007066F2C00000A0007176F9B00000A00076F9C00000A262A000000133002002D0000001C0000110072B32000700A739900000A0B07026F9A00000A0007066F2C00000A0007176F9B00000A00076F9C00000A262A000000133002004B0100001D00001100737B00000A0A0672C81900706F7C00000A00061F0E6F7D00000A0006028C110000016F4D00000A00737B00000A0B0772721900706F7C00000A00071E6F7D00000A0007038C3E0000016F4D00000A00737B00000A0C0872E01A00706F7C00000A00081F166F7D00000A00081F196F9D00000A0008046F4D00000A00737B00000A0D0972FB2000706F7C00000A00091E6F7D00000A0009058C3E0000016F4D00000A00737B00000A1304110472112100706F7C00000A001104186F7D00000A0011040E058C5A0000016F4D00000A00739900000A130511050E046F9A00000A001105722F2100706F2C00000A0011051A6F9B00000A0011056F4B00000A066F9E00000A2611056F4B00000A076F9E00000A2611056F4B00000A086F9E00000A2611056F4B00000A096F9E00000A2611056F4B00000A11046F9E00000A26289F00000A11056FA000000A002A0013300300DA0000001E000011001A8D130000010A737B00000A0B0772281E00706F7C00000A0007186F7D00000A00070F007B0400000428A100000A8C5A0000016F4D00000A00061607A2737B00000A0C08726F2100706F7C00000A00081A6F7D00000A00080F007B070000046F4D00000A00061708A2737B00000A0D0972832100706F7C00000A00091A6F7D00000A00090F007B080000046F4D00000A00061809A2737B00000A1304110472C81900706F7C00000A0011041F0E6F7D00000A001104038C110000016F4D00000A0006191104A272932100700604284200000613052B0011052A000013300300170100001F000011001B8D130000010A737B00000A0B0772FE1C00706F7C00000A00071F096F7D00000A00070F007B090000048C1B0000016F4D00000A00061607A2737B00000A0C0872181D00706F7C00000A00081F096F7D00000A00080F007B0A0000048C1B0000016F4D00000A00061708A2737B00000A0D09726F2100706F7C00000A00091A6F7D00000A00090F007B0B0000048C170000016F4D00000A00061809A2737B00000A1304110472832100706F7C00000A0011041A6F7D00000A0011040F007B0C0000048C170000016F4D00000A0006191104A2737B00000A1305110572C81900706F7C00000A0011051F0E6F7D00000A001105038C110000016F4D00000A00061A1105A272C92100700604284200000613062B0011062A0013300300150100001F000011001B8D130000010A737B00000A0B0772321D00706F7C00000A00071E6F7D00000A00070F007B0D0000048C0A0000016F4D00000A00061607A2737B00000A0C08724C1D00706F7C00000A00081E6F7D00000A00080F007B0E0000048C0A0000016F4D00000A00061708A2737B00000A0D09726F2100706F7C00000A00091A6F7D00000A00090F007B0F0000048C170000016F4D00000A00061809A2737B00000A1304110472832100706F7C00000A0011041A6F7D00000A0011040F007B100000048C170000016F4D00000A0006191104A2737B00000A1305110572C81900706F7C00000A0011051F0E6F7D00000A001105038C110000016F4D00000A00061A1105A272F92100700604284200000613062B0011062A000000133003009F0000002000001100198D130000010A737B00000A0B07726F2100706F7C00000A00071A6F7D00000A00070F007B070000046F4D00000A00061607A2737B00000A0C0872832100706F7C00000A00081A6F7D00000A00080F007B080000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A2722B2200700604284200000613042B0011042A00133003009F0000002000001100198D130000010A737B00000A0B07726F2100706F7C00000A00071A6F7D00000A00070F007B070000046F4D00000A00061607A2737B00000A0C0872832100706F7C00000A00081A6F7D00000A00080F007B080000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A272632200700604284200000613042B0011042A0013300300AA0000002000001100198D130000010A737B00000A0B0772281E00706F7C00000A0007186F7D00000A00070F007B0200000428A100000A8C5A0000016F4D00000A00061607A2737B00000A0C0872932200706F7C00000A00081F196F7D00000A00080F007B030000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A272A92200700604284200000613042B0011042A000013300300AA0000002000001100198D130000010A737B00000A0B0772281E00706F7C00000A0007186F7D00000A00070F007B0200000428A100000A8C5A0000016F4D00000A00061607A2737B00000A0C0872D52200706F7C00000A00081F196F7D00000A00080F007B030000046F4D00000A00061708A2737B00000A0D0972C81900706F7C00000A00091F0E6F7D00000A0009038C110000016F4D00000A00061809A272E52200700604284200000613042B0011042A0000133003006C0000001800001100188D130000010A737B00000A0B07721F2300706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27235230070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772752300706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27293230070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772DB2300706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27205240070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772932200706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272A9220070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772392400706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A2725D240070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772972400706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A272B9240070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B0772F12400706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A2720D250070060428420000060D2B00092A133003006C0000001800001100188D130000010A737B00000A0B07723F2500706F7C00000A00071F196F7D00000A0007026F4D00000A00061607A2737B00000A0C0872C81900706F7C00000A00081F0E6F7D00000A0008038C110000016F4D00000A00061708A27267250070060428420000060D2B00092A13300200790000002100001100732700000A0A739900000A0B07046F9A00000A0007026F2C00000A00071A6F9B00000A0007166FA200000A00000313041613052B25110411059A0C000814FE01130611062D0D076F4B00000A086F9E00000A2600110517581305110511048E69FE04130611062DCD076F6400000AA53E0000010D2B00092A00000013300200920000002200001100732700000A0A728B0300700B07732600000A0C739900000A0D09086F9A00000A0009026F2C00000A00091A6F9B00000A0009166FA200000A00000313071613082B28110711089A130400110414FE01130911092D0E096F4B00000A11046F9E00000A2600110817581308110811078E69FE04130911092DCA09732D00000A13051105066F2E00000A260613062B0011062A00001B300300B1000000230000110073A300000A0A732700000A0B72A52500700C0802732A00000A0D09732D00000A13041104076F2E00000A2600076F3000000A6F3600000A13072B2811076F3700000A7440000001130500061105723A2600706F3800000A6F1C00000A6FA400000A000011076F3E00000A130811082DCBDE1D110775410000011309110914FE01130811082D0811096F3F00000A00DC000672721900706FA400000A000672E01A00706FA400000A000613062B0011062A0000000110000002003A003973001D00000000133004006803000024000011000313161116394B030000FE137E2A0000043A160100001F15738300000A2572881A007016288400000A2572F41A007017288400000A2572B219007018288400000A2572141A007019288400000A2572121B00701A288400000A2572341B00701B288400000A25724A1B00701C288400000A2572681B00701D288400000A2572861B00701E288400000A2572961B00701F09288400000A2572BA1B00701F0A288400000A2572E81B00701F0B288400000A25720C1C00701F0C288400000A2572381C00701F0D288400000A2572541C00701F0E288400000A2572681C00701F0F288400000A25727A1C00701F10288400000A25728E1C00701F11288400000A2572A41C00701F12288400000A2572721900701F13288400000A2572E01A00701F14288400000AFE13802A000004FE137E2A00000411161217288500000A39140200001117451500000005000000190000002D00000041000000550000006B0000008100000097000000AD000000C3000000D9000000EF000000050100001B01000031010000470100005A0100006D0100008001000093010000A601000038B40100000272442600706FA500000A0A06131538AF0100000272A62600706FA500000A0B071315389B0100000272E82600706FA500000A0C08131538870100000272242700706FA500000A0D091315387301000002725A2700706FA500000A130411041315385D0100000272A22700706FA500000A13051105131538470100000272DE2700706FA500000A13061106131538310100000272202800706FA500000A130711071315381B0100000272642800706FA500000A13081108131538050100000272B42800706FA500000A13091109131538EF00000002721A2900706FA500000A130A110A131538D900000002728C2900706FA500000A130B110B131538C30000000272E62900706FA500000A130C110C131538AD0000000272482A00706FA500000A130D110D131538970000000272A42A00706FA500000A130E110E131538810000000272D42A00706FA500000A130F110F13152B6E0272022B00706FA500000A1310111013152B5B0272322B00706FA500000A1311111113152B480272642B00706FA500000A1312111213152B3502729C2B00706FA500000A1313111313152B220272C62B00706FA500000A1314111413152B0F0272F62B00706FA500000A13152B0011152A1B300200620000002500001100739600000A0A00026F9400000A0D2B20096F3700000A74180000010B0006076F9200000A739700000A6F9800000A0000096F3E00000A130411042DD4DE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A00000110000002000F00303F001C000000001B30020062000000260000110073A600000A0A00026F9400000A0D2B20096F3700000A74180000010B0006076F9200000A28A700000A6FA800000A0000096F3E00000A130411042DD4DE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A00000110000002000F00303F001C000000001B300400880100001A000011001200FE150500000200026F9400000A1306383901000011066F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613071207289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D02000004739600000A0C076F8700000A72481E00706F8800000A2C1C076F8700000A72481E00706F8800000A6F8900000A282300000A2B011700130811083A860000000000076F8700000A72481E00706F8800000A6F8900000A178D4E00000113091109161F209D11096F6500000A130A16130B2B35110A110B9A0D00096F3200000A1F24FE0116FE01130811082D1400120409289700000A000811046F9800000A000000110B1758130B110B110A8E69FE04130811082DBD12000828510000067D03000004002B0E0012007E2200000A7D03000004000011066F3E00000A130811083AB7FEFFFFDE1D11067541000001130C110C14FE01130811082D08110C6F3F00000A00DC000613052B0011052A411C0000020000001200000050010000620100001D000000000000001B300400880100001A000011001200FE150500000200026F9400000A1306383901000011066F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613071207289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D02000004739600000A0C076F8700000A72481E00706F8800000A2C1C076F8700000A72481E00706F8800000A6F8900000A282300000A2B011700130811083A860000000000076F8700000A72481E00706F8800000A6F8900000A178D4E00000113091109161F209D11096F6500000A130A16130B2B35110A110B9A0D00096F3200000A1F24FE0116FE01130811082D1400120409289700000A000811046F9800000A000000110B1758130B110B110A8E69FE04130811082DBD12000828510000067D03000004002B0E0012007E2200000A7D03000004000011066F3E00000A130811083AB7FEFFFFDE1D11067541000001130C110C14FE01130811082D08110C6F3F00000A00DC000613052B0011052A411C0000020000001200000050010000620100001D000000000000001B3003005C01000027000011001200FE150600000200026F9400000A0D3812010000096F3700000A74180000010B00120072541C00707D050000041200076F8700000A72122C00706F8800000A2C17076F8700000A72122C00706F8800000A6F8900000A2B0572242C0070007D060000041200076F8700000A72F01C00706F8800000A2C33076F8700000A72F01C00706F8800000A6F8900000A282300000A2D17076F8700000A72F01C00706F8800000A6F8900000A2B147EA900000A13041204FE16170000016F1C00000A007D070000041200076F8700000A72342C00706F8800000A2C33076F8700000A72342C00706F8800000A6F8900000A282300000A2D17076F8700000A72342C00706F8800000A6F8900000A2B147EAA00000A13041204FE16170000016F1C00000A007D0800000400096F3E00000A130511053ADFFEFFFFDE1C0975410000011306110614FE01130511052D0811066F3F00000A00DC00060C2B00082A411C0000020000001100000028010000390100001C000000000000001B300300B301000028000011001200FE150600000200026F9400000A0D3869010000096F3700000A74180000010B001200076F8700000A72281E00706F8800000A2C1C076F8700000A72281E00706F8800000A6F8900000A282300000A2C0C1613041204289500000A2B15076F8700000A72281E00706F8800000A6F8900000A007D04000004120072681C00707D050000041200076F8700000A72122C00706F8800000A2C17076F8700000A72122C00706F8800000A6F8900000A2B0572242C0070007D060000041200076F8700000A72F01C00706F8800000A2C33076F8700000A72F01C00706F8800000A6F8900000A282300000A2D17076F8700000A72F01C00706F8800000A6F8900000A2B147EA900000A13051205FE16170000016F1C00000A007D070000041200076F8700000A72342C00706F8800000A2C33076F8700000A72342C00706F8800000A6F8900000A282300000A2D17076F8700000A72342C00706F8800000A6F8900000A2B147EAA00000A13051205FE16170000016F1C00000A007D0800000400096F3E00000A130611063A88FEFFFFDE1C0975410000011307110714FE01130611062D0811076F3F00000A00DC00060C2B00082A00411C000002000000110000007F010000900100001C000000000000001B300300F101000029000011001200FE150700000200026F9400000A0D38A7010000096F3700000A74180000010B00076F8700000A72FE1C00706F8800000A2C1C076F8700000A72FE1C00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72FE1C00706F8800000A6F8900000A28AB00000A7D090000042B11120072422C007028AB00000A7D09000004076F8700000A72181D00706F8800000A2C1C076F8700000A72181D00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72181D00706F8800000A6F8900000A28AB00000A7D0A0000042B0C12007E8C00000A7D0A000004076F8700000A726F2100706F8800000A2C1C076F8700000A726F2100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A726F2100706F8800000A6F8900000A28AC00000A7D0B0000042B0C12007EA900000A7D0B000004076F8700000A72832100706F8800000A2C1C076F8700000A72832100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72832100706F8800000A6F8900000A28AC00000A7D0C0000042B0C12007EAA00000A7D0C00000400096F3E00000A130411043A4AFEFFFFDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A000000411C00000200000011000000BD010000CE0100001C000000000000001B300300ED0100002A000011001200FE150800000200026F9400000A0D38A3010000096F3700000A74180000010B00076F8700000A72321D00706F8800000A2C1C076F8700000A72321D00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72321D00706F8800000A6F8900000A28AD00000A7D0D0000042B0D12001628AE00000A7D0D000004076F8700000A724C1D00706F8800000A2C1C076F8700000A724C1D00706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A724C1D00706F8800000A6F8900000A28AD00000A7D0E0000042B0C12007E9000000A7D0E000004076F8700000A726F2100706F8800000A2C1C076F8700000A726F2100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A726F2100706F8800000A6F8900000A28AC00000A7D0F0000042B0C12007EA900000A7D0F000004076F8700000A72832100706F8800000A2C1C076F8700000A72832100706F8800000A6F8900000A282300000A2B011700130411042D231200076F8700000A72832100706F8800000A6F8900000A28AC00000A7D100000042B0C12007EAA00000A7D1000000400096F3E00000A130411043A4EFEFFFFDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A000000411C00000200000011000000B9010000CA0100001C000000000000001B3003005C01000027000011001200FE150600000200026F9400000A0D3812010000096F3700000A74180000010B00120072A41C00707D050000041200076F8700000A72122C00706F8800000A2C17076F8700000A72122C00706F8800000A6F8900000A2B0572242C0070007D060000041200076F8700000A72F01C00706F8800000A2C33076F8700000A72F01C00706F8800000A6F8900000A282300000A2D17076F8700000A72F01C00706F8800000A6F8900000A2B147EA900000A13041204FE16170000016F1C00000A007D070000041200076F8700000A72342C00706F8800000A2C33076F8700000A72342C00706F8800000A6F8900000A282300000A2D17076F8700000A72342C00706F8800000A6F8900000A2B147EAA00000A13041204FE16170000016F1C00000A007D0800000400096F3E00000A130511053ADFFEFFFFDE1C0975410000011306110614FE01130511052D0811066F3F00000A00DC00060C2B00082A411C0000020000001100000028010000390100001C000000000000001B300300C70000002B0000110073AF00000A0A00026F9400000A13042B7F11046F3700000A74180000010B001202FE1506000002120272541C00707D050000041202076F8700000A72122C00706F8800000A6F8900000A7D060000041202076F8700000A72F01C00706F8800000A6F8900000A7D070000041202076F8700000A72342C00706F8800000A6F8900000A7D0800000406086FB000000A000011046F3E00000A130511053A71FFFFFFDE1D110475410000011306110614FE01130511052D0811066F3F00000A00DC00060D2B00092A00011000000200100093A3001D000000001B3004008A0000002C00001100731500000A0A06724C2C00706F1B00000A2600026F3000000A6F3600000A0D2B26096F3700000A74400000010B0006720100007007166F7200000A6F1C00000A6F1800000A2600096F3E00000A130411042DCEDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC000672822C00706F1B00000A26066F1C00000A0C2B00082A000001100000020020003656001C000000001B3003007D0000002D00001100731500000A0A06724C2C00706F1B00000A260214FE010D092D4B00026FB100000A13042B23120428B200000A0B000672010000701201FE16110000016F1C00000A6F1800000A2600120428B300000A0D092DD2DE0F1204FE160700001B6F3F00000A00DC000672822C00706F1B00000A26066F1C00000A0C2B00082A00000001100000020024003256000F0000000013300200110000000A000011000272BA2C007028530000060A2B00062A0000001B3003007C0000002E00001100731500000A0A0672EC2C0070036F1800000A2600026F9400000A0D2B1B096F3700000A74180000010B0006076FB400000A6F1B00000A2600096F3E00000A130411042DD9DE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC000672F82C0070036F1800000A26066F1C00000A0C2B00082A0110000002001C002B47001C000000001B3003006D0000002F00001100739600000A0A00026F3000000A6F3600000A0D2B26096F3700000A74400000010B000607166F7200000A6F1C00000A739700000A6F9800000A0000096F3E00000A130411042DCEDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00060C2B00082A0000000110000002001400364A001C000000001B300200690000003000001100737E00000A0A0672062D00706F7F00000A0000026F9400000A0D2B16096F3700000A74180000010B0006076FB500000A2600096F3E00000A130411042DDEDE1C0975410000011305110514FE01130411042D0811056F3F00000A00DC00066FB400000A0C2B00082A0000000110000002001B002641001C00000000133005003D010000310000110072462D00701F11734400000A0A140B060F01281700000A6F4500000A16FE010D092D13060F01281700000A6F4600000A6F4700000A0B731500000A0C0872542D00701813041204283300000A7E7800000A8C110000010F00282800000A13051205FE16110000016F1C00000A6FB600000A260872612E0070168D030000016FB700000A260872812E00700F00282800000A13051205FE16110000016F1C00000A6F1800000A2607282300000A0D092D0D08720A2F0070076F1800000A260872702F00701A8D0300000113061106160F01281700000AA21106170F02285600000A28B800000A8C3E000001A21106180F03285600000A28B800000A8C3E000001A21106190F04285600000A28B800000A8C3E000001A211066FB700000A2608721D300070168D030000016FB700000A26086F1C00000A2875000006002A00000013300500A20000003200001100731500000A0A067243300070168D030000016FB700000A260672673000701B8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2071A0F06285600000A28B800000A8C3E000001A2076FB700000A2606724A310070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500CB0000003300001100731500000A0A0672B33300701F200B1201283300000A7E7800000A8C110000010F00282800000A0C1202FE16110000016F1C00000A6FB600000A260672E4340070168D030000016FB700000A260672643500701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A260672D1360070168D030000016FB700000A26066F1C00000A2875000006002A00133005001A0100003200001100731500000A0A067243300070168D030000016FB700000A260672FD3600701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A2606726E380070168D030000016FB700000A26066F1C00000A036F5700000A287600000600731500000A0A0672CF3A0070168D030000016FB700000A260672ED3A00701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A260672A23B0070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500CB0000003300001100731500000A0A0672B33300701F100B1201283300000A7E7800000A8C110000010F00282800000A0C1202FE16110000016F1C00000A6FB600000A260672E4340070168D030000016FB700000A260672273E00701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A260672D1360070168D030000016FB700000A26066F1C00000A2875000006002A00133005001A0100003200001100731500000A0A067243300070168D030000016FB700000A260672983F00701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A2606720D410070168D030000016FB700000A26066F1C00000A036F5700000A287600000600731500000A0A0672CF3A0070168D030000016FB700000A260672704300701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A26067227440070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500C00000003300001100731500000A0A0672AE4600701A0B1201283300000A0F00282800000A0C1202FE16110000016F1C00000A6FB900000A26067285470070168D030000016FB700000A260672B04800701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A26067269490070168D030000016FB700000A26066F1C00000A2875000006002A133005008E0000003200001100731500000A0A067295490070168D030000016FB700000A260672C34900701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A260672684A0070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500D20000003300001100731500000A0A06721D4D00701E0B1201283300000A0F00282800000A0C1202FE16110000016F1C00000A6FB900000A260672F64D0070168D030000016FB700000A260672B54E0070168D030000016FB700000A260672B44F00701A8D030000010D09160F00282800000A0C1202FE16110000016F1C00000AA209170F01281700000AA209180F02285600000A28B800000A8C3E000001A209190F03285600000A28B800000A8C3E000001A2096FB700000A26067293500070168D030000016FB700000A26066F1C00000A2875000006002A0000133005008E0000003200001100731500000A0A0672C1500070168D030000016FB700000A260672E95000701A8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2076FB700000A260672BA510070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A0000133005003D010000310000110072375400701F11734400000A0A140B060F01281700000A6F4500000A16FE010D092D13060F01281700000A6F4600000A6F4700000A0B731500000A0C0872455400701713041204283300000A7E7800000A8C110000010F00282800000A13051205FE16110000016F1C00000A6FB600000A260872612E0070168D030000016FB700000A260872525500700F00282800000A13051205FE16110000016F1C00000A6F1800000A2607282300000A0D092D0D0872D9550070076F1800000A2608720B5600701A8D0300000113061106160F01281700000AA21106170F02285600000A28B800000A8C3E000001A21106180F03285600000A28B800000A8C3E000001A21106190F04285600000A28B800000A8C3E000001A211066FB700000A2608721D300070168D030000016FB700000A26086F1C00000A2875000006002A00000013300500A20000003200001100731500000A0A067243300070168D030000016FB700000A260672A65600701B8D030000010B07160F02281700000AA207170F03281700000AA207180F04285600000A28B800000A8C3E000001A207190F05285600000A28B800000A8C3E000001A2071A0F06285600000A28B800000A8C3E000001A2076FB700000A26067267570070168D030000016FB700000A26066F1C00000A036F5700000A2876000006002A000013300500C500000034000011000F01284300000A0A28740000060B289F00000A076FBA00000A0006175F17FE0116FE010C082D0E02050E040E050E0628600000060006185F18FE0116FE010C082D0E02050E040E050E06285600000600061A5F1AFE0116FE010C082D0C02040E040E05285C00000600061E5F1EFE0116FE010C082D0C02040E040E05285E00000600061F105F1F10FE0116FE010C082D0C02040E040E05285A00000600061F205F1F20FE0116FE010C082D0C02040E040E05285800000600289F00000A6FBB00000A002A00000013300700ED0000003500001100737E00000A0A06036F5700000A6F7F00000A00140B0672C25900706FBC00000A0B0714FE010C082D110203050E050E060E070E082861000006000672FA5900706FBC00000A0B0714FE010C082D110203050E050E060E070E082857000006000672405A00706FBC00000A0B0714FE010C082D0F0203040E040E060E07285D000006000672745A00706FBC00000A0B0714FE010C082D0F0203040E040E060E07285F000006000672B25A00706FBC00000A0B0714FE010C082D0F0203040E040E060E07285B000006000672F25A00706FBC00000A0B0714FE010C082D0F0203040E040E060E072859000006002A000000133005003701000036000011000F01281700000A0F03285600000A287B0000060A731500000A0B077243300070168D030000016FB700000A260772305B0070066F1C00000A066FBD00000A8C3E0000010F02281700000A6FB600000A260772B55B00700F00282800000A0C1202FE16110000016F1C00000A6F1800000A260772445C0070066F1C00000A066FBD00000A8C3E0000016FB900000A26076F1C00000A287700000600731500000A0B077243300070168D030000016FB700000A260772985C00700F01281700000A0F02281700000A0F03285600000A28B800000A8C3E0000016FB600000A260772615D00700F00282800000A0C1202FE16110000016F1C00000A0F01281700000A6FB900000A260772565E00700F01281700000A0F03285600000A28B800000A8C3E0000016FB900000A26076F1C00000A2877000006002A00133007001E00000037000011007E7800000A0A027E7800000A037E7800000A040512002869000006002A4A00020304050E040E050E062869000006002A0000001B300500DD0400003800001100026F3200000A16310E036F3200000A16FE0216FE012B011700131B111B3AB90400000072C00F0070732600000A0A737E00000A0B737E00000A0C000372090A00706FBE00000A2C100372090A00706FBF00000A16FE012B011700131B111B2D18070317036F3200000A18596F5500000A6F7F00000A002B0807036F7F00000A000272090A00706FBE00000A2C100272090A00706FBF00000A16FE012B011700131B111B2D18080217026F3200000A18596F5500000A6F7F00000A002B0808026F7F00000A00086FC000000A72E75E00706FA500000A0D096F8200000A73C100000A130400096F9400000A131C2B29111C6F3700000A7418000001130500110472235F007011056F9200000A6FC200000A6F1800000A2600111C6F3E00000A131B111B2DCADE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0011046F1C00000A130611066F3200000A17FE0216FE01131B111B2D1311061611066F3200000A17596F5500000A1306732700000A130772315F00701106286300000A1308110806732A00000A13091109166FA200000A00066F2B00000A001109732D00000A130A110A11076F2E00000A26066F5300000A007E2200000A130B14130C7E2200000A130D7E2200000A130E7E2200000A130F0011076F3000000A6F3600000A131C38A9010000111C6F3700000A74400000011310007E2200000A130F111072006000706F3800000A6F1C00000A130D111072246000706F3800000A6F1C00000A130E737E00000A130C110C111072006000706F3800000A6F1C00000A6F7F00000A00076FC000000A72E75E00706FA500000A1311110C6FC000000A723E6000706FBC00000A131211126F8700000A72666000706F8800000A0F02FE16110000016F1C00000A6FC300000A0011126F8700000A727C6000706F8800000A28C400000A131E121E729660007028C500000A178D4E000001131F111F161F5A9D111F6FC600000A6FC300000A000011116F9400000A13202B6111206F3700000A741800000113130011136F9200000A6FC200000A1314110E11146F3A00000A6F3D00000A2D10110E11146FC200000A6F3D00000A2B011700131B111B2D1B00729A6000701114286300000A1315110F1115283400000A130F000011206F3E00000A131B111B2D92DE1D11207541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0011107200600070110C6FB400000A6FC700000A0011107224600070110E110F283400000A6FC700000A0000111C6F3E00000A131B111B3A47FEFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC00066F2B00000A00724562007013167E2200000A13171413180011076F3000000A6F3600000A131C2B7B111C6F3700000A74400000011319001116111972006000706F3800000A6F1C00000A72090A0070720D0A00706F3500000A111972246000706F3800000A6F1C00000A111972106300706F3800000A6F1C00000A282500000A1317111706732A00000A13181118166FA200000A00289F00000A11186FA000000A0000111C6F3E00000A131B111B3A75FFFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC00066F5300000A0000DE10131A007232630070111A73C800000A7A00DE2000062C0B066FC900000A16FE012B011700131B111B2D07066F5300000A0000DC00002A0000004194000002000000ED0000003A000000270100001D0000000000000002000000DE02000072000000500300001D0000000000000002000000EB010000C0010000AB0300001D0000000000000002000000F00300008F0000007F0400001D00000000000000000000003B0000006C040000A7040000100000005F000001020000003B0000007F040000BA04000020000000000000001B300500D80500003900001100026F3200000A16310E036F3200000A16FE0216FE012B011700131C111C3AB40500000072C00F0070732600000A0A737E00000A0B737E00000A0C000372090A00706FBE00000A2C100372090A00706FBF00000A16FE012B011700131C111C2D18070317036F3200000A18596F5500000A6F7F00000A002B0807036F7F00000A000272090A00706FBE00000A2C100272090A00706FBF00000A16FE012B011700131C111C2D18080217026F3200000A18596F5500000A6F7F00000A002B0808026F7F00000A00086FC000000A72E75E00706FA500000A0D096F8200000A73C100000A130400096F9400000A131D2B29111D6F3700000A7418000001130500110472235F007011056F9200000A6FC200000A6F1800000A2600111D6F3E00000A131C111C2DCADE1D111D7541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC0011046F1C00000A130611066F3200000A17FE0216FE01131C111C2D1311061611066F3200000A17596F5500000A1306732700000A130772586300701106286300000A1308110806732A00000A13091109166FA200000A00066F2B00000A001109732D00000A130A110A11076F2E00000A26066F5300000A007E2200000A130B14130C14130D0011076F3000000A6F3600000A131D38B6020000111D6F3700000A7440000001130E00737E00000A130C110C110E72006000706F3800000A6F1C00000A6F7F00000A00737E00000A130D110D7223640070110E72246000706F3800000A6F1C00000A723D640070282900000A6F7F00000A00076FC000000A72E75E00706FA500000A130F110C6FC000000A723E6000706FBC00000A131011106F8700000A72666000706F8800000A0F02FE16110000016F1C00000A6FC300000A0011106F8700000A727C6000706F8800000A28C400000A131F121F729660007028C500000A178D4E00000113201120161F5A9D11206FC600000A6FC300000A0000110F6F9400000A1321383B01000011216F3700000A741800000113110011116F9200000A6FC200000A1312110D6FC000000A72596400701112286300000A6FA500000A1313110D6FC000000A725964007011126F3A00000A286300000A6FA500000A131411136F8200000A2D0F11146F8200000A16FE0216FE012B011700131C111C2D041114131311136F8200000A16310F11146F8200000A16FE0116FE012B011700131C111C2D0411131314161315388200000000111517FE0116FE01131C111C2D04111413130011136F9400000A13222B2F11226F3700000A741800000113160011166FCA00000A14FE01131C111C2D0F11166FCA00000A11166FCB00000A260011226F3E00000A131C111C2DC4DE1D11227541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC0000111517581315111517FE0216FE01131C111C3A6DFFFFFF0011216F3E00000A131C111C3AB5FEFFFFDE1D11217541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC00110E7200600070110C6FB400000A6FC700000A00110E7224600070110D6FCC00000A72236400707E2200000A6F3500000A723D6400707E2200000A6F3500000A72876400707E2200000A6F3500000A6FC700000A0000111D6F3E00000A131C111C3A3AFDFFFFDE1D111D7541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC00066F2B00000A0072A364007013177E2200000A13181413190011076F3000000A6F3600000A131D2B7B111D6F3700000A7440000001131A001117111A72006000706F3800000A6F1C00000A72090A0070720D0A00706F3500000A111A72246000706F3800000A6F1C00000A111A72106300706F3800000A6F1C00000A282500000A1318111806732A00000A13191119166FA200000A00289F00000A11196FA000000A0000111D6F3E00000A131C111C3A75FFFFFFDE1D111D7541000001131E111E14FE01131C111C2D08111E6F3F00000A00DC00066F5300000A0000DE10131B007232630070111B73C800000A7A00DE2000062C0B066FC900000A16FE012B011700131C111C2D07066F5300000A0000DC00002A41AC000002000000ED0000003A000000270100001D00000000000000020000009703000040000000D70300001D0000000000000002000000CE02000052010000200400001D0000000000000002000000D9010000CD020000A60400001D0000000000000002000000EB0400008F0000007A0500001D00000000000000000000003B00000067050000A2050000100000005F000001020000003B0000007A050000B505000020000000000000001B300700B30300003A0000110072C00F0070732600000A0A140B0E067E7800000A8111000001737E00000A0C737E00000A0D000008046F5700000A6F7F00000A00726E6500700F00FE16110000016F1C00000A286300000A1304110406732A00000A13051105166FA200000A00066F2B00000A0011056F6400000A6F1C00000A0B066F5300000A0009076F7F00000A007E2200000A1306141307086FC000000A6F8700000A72D26500706F8800000A13087E7800000A1309110814FE01130D110D2D21000011086F8900000A739700000A130900DE0C26007E7800000A130900DE0000000F017E7800000A28CD00000A16FE01130D110D3AEB0000000012097E7800000A28CD00000A16FE01130D110D2D0C0072D865007073CE00000A7A72406600701209FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A1307110714FE0116FE01130D110D2D2F0072406600701209FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130700110714FE01130D110D2D4E000E040E05086FC000000A11070973CF00000A17286E00000600086FC000000A6FD000000A16FE01130D110D2D1600086FC000000A11070E040E050914286C00000600000E06110981110000010000380C0100000012097E7800000A28CD00000A16FE01130D110D3AF20000000072406600700F01FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A130A110A14FE0116FE01130D110D2D2F0072406600700F01FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130A00110A14FE0116FE01130D110D2D0C00727266007073CE00000A7A72B6660070050E05086FC000000A0917286D0000061307110A11076FB500000A26086FC000000A6FD000000A16FE01130D110D2D1500086FC000000A1107050E050914286C00000600000E0611076F8700000A72D26500706F8800000A6F8900000A739700000A81110000010000110714FE01130D110D2D6700091107286B0000060072CE660070096FB400000A72090A0070720D0A00706F3500000A0F00FE16110000016F1C00000A282F00000A1304110406732A00000A13051105166FA200000A00066F2B00000A00289F00000A11056FA000000A00066F5300000A000000DE10130B007232630070110B73C800000A7A0000DE10130C007232630070110C73C800000A7A00DE2E00062C0B066FC900000A16FE012B011700130D110D2D07066F5300000A00086FD100000A00096FD100000A0000DC002A004164000000000000B800000012000000CA0000000C000000030000010000000027000000350300005C030000100000005F00000100000000260000004A03000070030000100000005F00000102000000260000005D030000830300002E000000000000001B3005006D0600003B00001100036F3200000A16310E046F3200000A16FE0216FE012B011700131D111D3A490600000072C00F0070732600000A0A737E00000A0B737E00000A0C000472090A00706FBE00000A2C100472090A00706FBF00000A16FE012B011700131D111D2D18070417046F3200000A18596F5500000A6F7F00000A002B0807046F7F00000A000372090A00706FBE00000A2C100372090A00706FBF00000A16FE012B011700131D111D2D18080317036F3200000A18596F5500000A6F7F00000A002B0808036F7F00000A00086FC000000A72E75E00706FA500000A0D096F8200000A73C100000A130400096F9400000A131E2B29111E6F3700000A7418000001130500110472235F007011056F9200000A6FC200000A6F1800000A2600111E6F3E00000A131D111D2DCADE1D111E7541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC0011046F1C00000A130611066F3200000A17FE0216FE01131D111D2D1311061611066F3200000A17596F5500000A1306732700000A130772406700700F00FE16110000016F1C00000A1106282F00000A1308110806732A00000A13091109166FA200000A00066F2B00000A001109732D00000A130A110A11076F2E00000A26066F5300000A007E2200000A130B14130C0011076F3000000A6F3600000A131E3855030000111E6F3700000A7440000001130D00737E00000A130C110C110D72006000706F3800000A6F1C00000A6F7F00000A00076FC000000A72E75E00706FA500000A130E110C6FC000000A723E6000706FBC00000A130F110F6F8700000A72666000706F8800000A0F03FE16110000016F1C00000A6FC300000A00110F6F8700000A727C6000706F8800000A28C400000A13201220729660007028C500000A178D4E00000113211121161F5A9D11216FC600000A6FC300000A0000110E6F9400000A1322384A02000011226F3700000A741800000113100011106F9200000A6FC200000A13110E041728D200000A28D300000A28D400000A16FE01131D111D3A4201000000110C177213680070146FD500000A1312110C72D26500706FD600000A1313111311116FC300000A0011126F8700000A11136FD700000A26110C72276800706FD600000A131311137E7800000A13231223FE16110000016F1C00000A6FC200000A6FC300000A0011126F8700000A11136FD700000A26110C723B6800706FD600000A131311137E7800000A13231223FE16110000016F1C00000A6FC200000A6FC300000A0011126F8700000A11136FD700000A26110C72576800706FD600000A1313111372230B00706FC300000A0011126F8700000A11136FD700000A26110C72736800706FD600000A1313111372896800706FC300000A0011126F8700000A11136FD700000A26110C72956800706FD600000A1313111372896800706FC300000A0011126F8700000A11136FD700000A26110F11126FB500000A260038CC00000000110F72596400701111286300000A6FA500000A1314110F725964007011116F3A00000A286300000A6FA500000A1315161316388200000000111617FE0116FE01131D111D2D04111513140011146F9400000A13242B2F11246F3700000A741800000113170011176FCA00000A14FE01131D111D2D0F11176FCA00000A11176FCB00000A260011246F3E00000A131D111D2DC4DE1D11247541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC0000111617581316111617FE0216FE01131D111D3A6DFFFFFF000011226F3E00000A131D111D3AA6FDFFFFDE1D11227541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC00110D7200600070110C6FB400000A6FC700000A0000111E6F3E00000A131D111D3A9BFCFFFFDE1D111E7541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC00066F2B00000A0072A968007013187E2200000A131914131A0011076F3000000A6F3600000A131E2B6A111E6F3700000A7440000001131B001118111B72006000706F3800000A6F1C00000A72090A0070720D0A00706F3500000A111B72106300706F3800000A6F1C00000A282F00000A1319111906732A00000A131A111A166FA200000A00289F00000A111A6FA000000A0000111E6F3E00000A131D111D2D89DE1D111E7541000001131F111F14FE01131D111D2D08111F6F3F00000A00DC00066F5300000A0000DE10131C007232630070111C73C800000A7A00DE2000062C0B066FC900000A16FE012B011700131D111D2D07066F5300000A0000DC00002A00000041AC000002000000ED0000003A000000270100001D00000000000000020000008004000040000000C00400001D0000000000000002000000A9020000610200000A0500001D0000000000000002000000E30100006C0300004F0500001D0000000000000002000000940500007B0000000F0600001D00000000000000000000003B000000FC05000037060000100000005F000001020000003B0000000F0600004A060000200000000000000013300400CF0600003C00001100036F8700000A724A6900706F8800000A0A036F8700000A727A6900706F8800000A0B036F8700000A72A66900706F8800000A0C036F8700000A72C46900706F8800000A0D036F8700000A72E26900706F8800000A1304036F8700000A72FC6900706F8800000A1305062C0D066F8900000A28A100000A2B0116001306072C0D076F8900000A28A100000A2B0116001307082C08086F8900000A2B057E2200000A001308092C0D096F8900000A28A100000A2B011600130911042C0E11046F8900000A28A100000A2B011600130A11052C0911056F8900000A2B057E2200000A00130B036F8700000A72D26500706F8800000A6F8900000A130C72086A0070110C286300000A130D026FC000000A110D6FBC00000A130E110E14FE01132C112C3AA905000000723A6A0070130D110E110D6FA500000A130F16131038DE02000000110F11106F8600000A1311110616FE01132C112C3A6F0100000011116F8700000A72A66900706F8800000A1312111214FE01132C112C3A24010000001108282300000A132C112C3A100100000011126F8900000A282300000A16FE01132C112C2D1100111211086FC300000A000038E8000000001108178D4E000001132D112D161F2C9D112D6F6500000A131311126F8900000A178D4E000001132D112D161F2C9D112D6F6500000A131473CF00000A1315001114132E16132F2B19112E112F9A131600111511166FD800000A2600112F1758132F112F112E8E69FE04132C112C2DD9001113132E16132F2B37112E112F9A131700111511176FD900000A2D091117282300000A2B011700132C112C2D0C00111511176FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB1112726A6A00701115D036000001285F00000A6FDA00000A740800001B28DB00000A6FC300000A000000002B28000272A66900706FD600000A1312111211086FC300000A0011116F8700000A11126FD700000A260000110916FE01132C112C3A400100000011116F8700000A72FC6900706F8800000A1318111814FE01132C112C3AF500000000110B282300000A132C112C3AE10000000011186F8900000A282300000A16FE01132C112C2D11001118110B6FC300000A000038B900000000110B178D4E000001132D112D161F2C9D112D6F6500000A131911186F8900000A178D4E000001132D112D161F2C9D112D6F6500000A131A111A28DC00000A131B001119132E16132F2B37112E112F9A131C00111B111C6FD900000A2D09111C282300000A2B011700132C112C2D0C00111B111C6FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB1118726A6A0070111BD036000001285F00000A6FDA00000A740800001B28DB00000A6FC300000A000000002B28000272FC6900706FD600000A13181118110B6FC300000A0011116F8700000A11186FD700000A260000001110175813101110110F6F8200000AFE04132C112C3A0EFDFFFF036FCA00000A131D111D6F8700000A72A66900706F8800000A131E111D6F8700000A72FC6900706F8800000A131F111E2C09111E6F8900000A2B057E2200000A001320111F2C09111F6F8900000A2B057E2200000A001321110716FE01132C112C3A13010000001120282300000A132C112C3A01010000001108178D4E000001132D112D161F2C9D112D6F6500000A13221120178D4E000001132D112D161F2C9D112D6F6500000A1323112228DC00000A1324001123132E16132F2B37112E112F9A132500112411256FD900000A2D091125282300000A2B011700132C112C2D0C00112411256FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB0814FE01132C112C2D3000036F8700000A72A66900706F8800000A726A6A007011246FDD00000A740800001B28DB00000A6FC300000A00002B3B000272A66900706FD600000A13261126726A6A007011246FDD00000A740800001B28DB00000A6FC300000A00036F8700000A11266FD700000A26000000110A16FE01132C112C3A14010000001121282300000A132C112C3A0201000000110B178D4E000001132D112D161F2C9D112D6F6500000A13271121178D4E000001132D112D161F2C9D112D6F6500000A1328112728DC00000A1329001128132E16132F2B37112E112F9A132A001129112A6FD900000A2D09112A282300000A2B011700132C112C2D0C001129112A6FD800000A260000112F1758132F112F112E8E69FE04132C112C2DBB110514FE01132C112C2D3000036F8700000A72FC6900706F8800000A726A6A007011296FDD00000A740800001B28DB00000A6FC300000A00002B3B000272A66900706FD600000A132B112B726A6A007011296FDD00000A740800001B28DB00000A6FC300000A00036F8700000A112B6FD700000A26000000002A001B300700ED0100003D0000110000026FDE00000A6F9400000A130738A801000011076F3700000A74180000010A00066FDF00000A6F3A00000A0B066F8700000A72D26500706F8800000A6F8900000A6FC200000A0C7E2200000A0D1413041413051613060713081108391F0100001108726E6A0070282400000A2D27110872866A0070282400000A3A88000000110872A46A0070282400000A3AB300000038EA00000072CC6A007008286300000A0D03096FBC00000A1304110414FE01130911092D390004050611040E0473CF00000A17286E0000060011041005066FD000000A16FE01130911092D0F06110404050E041104286C00000600002B170017130672B66600700405060E0417286D0000061305002B7872086B007008286300000A0D03096FBC00000A1304110414FE0116FE01130911092D1700171306724A6B00700405060E0417286D0000061305002B3C72686B007008286300000A0D03096FBC00000A1304110414FE0116FE01130911092D170017130672B46B00700405060E0416286D0000061305002B0011062C07110514FE012B011700130911092D27000E0514FE01130911092D0F000311050E056FE000000A26002B0B000311056FB500000A2600000011076F3E00000A130911093A48FEFFFFDE1D11077541000001130A110A14FE01130911092D08110A6F3F00000A00DC002A000000411C0000020000000F000000BF010000CE0100001D000000000000001B300500EE0100003E000011000E04026FE100000A0A00056F8700000A6FE200000A13072B3711076F3700000A74580000010B000E04076FDF00000A6FD600000A0C08076F8900000A6FC300000A00066F8700000A086FD700000A260011076F3E00000A130811082DBCDE1D110775410000011309110914FE01130811082D0811096F3F00000A00DC000E0516FE01130811083A5A0100000028E300000A0D066F8700000A72D26500706F8800000A1203FE16110000016F1C00000A6FC200000A6FC300000A00066F8700000A72DC6B00706F8800000A14FE01130811082D2C00066F8700000A72DC6B00706F8800000A0F01FE16110000016F1C00000A6FC200000A6FC300000A00002B38000E0472DC6B00706FD600000A130411040F01FE16110000016F1C00000A6FC200000A6FC300000A00066F8700000A11046FD700000A2600066F8700000A72F06B00706F8800000A14FE01130811082D4400066F8700000A72F06B00706F8800000A0F0228E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00002B50000E0472F06B00706FD600000A130511050F0228E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00066F8700000A11056FD700000A2600000613062B0011062A000001100000020018004860001D000000001B300500830200003F0000110000046F8700000A6FE200000A1307380401000011076F3700000A74580000010A000E05066FDF00000A6FD900000A130811083ADF00000000066F8900000A0B7E7800000A0C0007739700000A0C00DE0B26007E7800000A0C00DE0000056F8700000A066FDF00000A6F8800000A0D0914FE01130811082D4000087E7800000A28E500000A16FE01130811082D1B091202FE16110000016F1C00000A6FC200000A6FC300000A002B0D09066F8900000A6FC300000A00002B5D000E04066FDF00000A6FD600000A1304087E7800000A28E500000A16FE01130811082D1C11041202FE16110000016F1C00000A6FC200000A6FC300000A002B0E1104066F8900000A6FC300000A00056F8700000A11046FD700000A2600000011076F3E00000A130811083AECFEFFFFDE1D110775410000011309110914FE01130811082D0811096F3F00000A00DC000E0616FE01130811083A2C01000000056F8700000A72666000706F8800000A14FE01130811082D2C00056F8700000A72666000706F8800000A0F00FE16110000016F1C00000A6FC200000A6FC300000A00002B38000E0472666000706FD600000A130511050F00FE16110000016F1C00000A6FC200000A6FC300000A00056F8700000A11056FD700000A2600056F8700000A727C6000706F8800000A14FE01130811082D4400056F8700000A727C6000706F8800000A0F0128E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00002B50000E04727C6000706FD600000A130611060F0128E400000A130A120A729660007028C500000A178D4E000001130B110B161F5A9D110B6FC600000A6FC300000A00056F8700000A11066FD700000A2600002A004134000000000000460000000B000000510000000B00000065000001020000000F0000001B0100002A0100001D000000000000001B3007008A040000400000110072086C0070168D03000001284100000A0A03066FA500000A0B722A6C0070168D03000001284100000A0C03086FA500000A0D72446C0070168D03000001284100000A13040311046FA500000A130500076F9400000A131738B500000011176F3700000A741800000113060011066F8700000A72D26500706F8800000A6F8900000A6F1C00000A130772606C007011076FC200000A286300000A13080211086FBC00000A1309110914FE0116FE01131811182D1F0072606C007011076F3A00000A286300000A13080211086FBC00000A130900110914FE01131811182D28007E7800000A7EA900000A110911060473CF00000A16286E000006000211096FCB00000A26002B0B000311066FCB00000A26000011176F3E00000A131811183A3BFFFFFFDE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC0002066FA500000A130A00110A6F9400000A13172B3311176F3700000A7418000001130B0072136800707E7800000A7EA900000A110B0416286D000006130C03110C6FB500000A260011176F3E00000A131811182DC0DE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC0000096F9400000A131738B500000011176F3700000A7418000001130D00110D6F8700000A72D26500706F8800000A6F8900000A6F1C00000A130772986C007011076FC200000A286300000A13080211086FBC00000A130E110E14FE0116FE01131811182D1F0072986C007011076F3A00000A286300000A13080211086FBC00000A130E00110E14FE01131811182D28007E7800000A7EA900000A110E110D0473CF00000A16286E0000060002110E6FCB00000A26002B0B0003110D6FCB00000A26000011176F3E00000A131811183A3BFFFFFFDE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC0002086FA500000A130F00110F6F9400000A13172B3311176F3700000A741800000113100072C86C00707E7800000A7EA900000A11100416286D00000613110311116FB500000A260011176F3E00000A131811182DC0DE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC000011056F9400000A131738B500000011176F3700000A741800000113120011126F8700000A72D26500706F8800000A6F8900000A6F1C00000A130772D46C007011076FC200000A286300000A13080211086FBC00000A1313111314FE0116FE01131811182D1F0072D46C007011076F3A00000A286300000A13080211086FBC00000A131300111314FE01131811182D28007E7800000A7EA900000A111311120473CF00000A16286E000006000211136FCB00000A26002B0B000311126FCB00000A26000011176F3E00000A131811183A3BFFFFFFDE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC000211046FA500000A13140011146F9400000A13172B3311176F3700000A741800000113150072066D00707E7800000A7EA900000A11150416286D00000613160311166FB500000A260011176F3E00000A131811182DC0DE1D111775410000011319111914FE01131811182D0811196F3F00000A00DC002A0000014C000002005800CC24011D00000000020055014499011D000000000200C001CC8C021D000000000200BD024401031D0000000002002903CCF5031D0000000002002704446B041D0000000013300500CE0100004100001100026F8700000A72146D00706F8800000A14FE01130511052D3F00026F8700000A72146D00706F8800000A6F8900000A28E600000A0A0617580B026F8700000A72146D00706F8800000A0728E700000A6FC300000A00002B27000372146D00706FD600000A0C0872230B00706FC300000A00026F8700000A086FD700000A2600026F8700000A722E6D00706F8800000A14FE01130511052D4200026F8700000A722E6D00706F8800000A28C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00002B4A0003722E6D00706FD600000A0D0928C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00026F8700000A096FD700000A2600026F8700000A72466D00706F8800000A14FE01130511052D4200026F8700000A722E6D00706F8800000A28C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00002B4D000372466D00706FD600000A1304110428C400000A13061206729660007028C500000A178D4E00000113071107161F5A9D11076FC600000A6FC300000A00026F8700000A11046FD700000A26002A00001B300700D403000042000011000E057E7800000A811100000172C00F0070732600000A0A140B00737E00000A0C737E00000A0D726E6500700F00FE16110000016F1C00000A286300000A1304110406732A00000A13051105166FA200000A00066F2B00000A0011056F6400000A6F1C00000A0B066F5300000A0009076F7F00000A0072406600700F01FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A1307110714FE0116FE01131111112D2F0072406600700F01FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130700110714FE0116FE01131111112D0C00726A6D007073CE00000A7A00080E046F5700000A6F7F00000A0000DE042600FE1A007E7800000A1308161309086FC000000A6F8700000A72D26500706F8800000A130A110A14FE01131111112D5A00110A6F8900000A282300000A16FE01131111112D0700171309002B3C0000110A6F8900000A739700000A130800DE0D260072BC6D007073CE00000A7A0012087E7800000A28CD00000A16FE01131111112D05001713090000001109131111113A490100000072F06D00701208FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FA500000A130B110B2C0F110B6F8200000A16FE0116FE012B011600131111112D2F0072F06D00701208FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FA500000A130B00110B2C0F110B6F8200000A16FE0216FE012B011700131111113AA40000000016130C388600000000110B110C6F8600000A130D73CF00000A130E110E72146D00706FD800000A26110E722E6D00706FD800000A26110E72466D00706FD800000A260528E800000A28E900000A086FC000000A110D09110E17286E000006000E0628D400000A16FE01131111112D09110D09287000000600086FC000000A110D09286F0000060000110C1758130C110C110B6F8200000AFE04131111113A66FFFFFF000E051108811100000100388200000000724A6B00700428E800000A28E900000A086FC000000A0917286D000006130F110F14FE01131111112D56001107110F6FB500000A260E0628D400000A16FE01131111112D09110F09287000000600086FC000000A110F09286F000006000E05110F6F8700000A72D26500706F8800000A6F8900000A739700000A8111000001000072CE660070096FB400000A72090A0070720D0A00706F3500000A0F00FE16110000016F1C00000A282F00000A1304110406732A00000A13051105166FA200000A00066F2B00000A00289F00000A11056FA000000A00066F5300000A0000DE0613100011107A002A414C000000000000FA000000120000000C0100000400000003000001000000005B010000120000006D0100000D00000003000001000000001A000000B2030000CC030000060000005F0000011B300600D2050000430000110072C00F0070732600000A0A140B00737E00000A0C737E00000A0D726E6500700F00FE16110000016F1C00000A286300000A1304110406732A00000A13051105166FA200000A00066F2B00000A0011056F6400000A6F1C00000A0B066F5300000A0009076F7F00000A0072406600700F01FE16110000016F1C00000A6FC200000A286300000A1306096FC000000A11066FBC00000A1307110714FE0116FE01131B111B2D2F0072406600700F01FE16110000016F1C00000A6F3A00000A286300000A1306096FC000000A11066FBC00000A130700110714FE0116FE01131B111B2D0C0072286E007073CE00000A7A725E6E00701306110711066FA500000A13080E0528EA00000A28D400000A16FE01131B111B2D55000011086F9400000A131C2B1A111C6F3700000A7418000001130900110711096FCB00000A2600111C6F3E00000A131B111B2DD9DE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC000073EB00000A130A110711066FA500000A13080011086F9400000A131C2B30111C6F3700000A7418000001130B00110A110B6F8700000A72D26500706F8800000A6F8900000A110B6FEC00000A0000111C6F3E00000A131B111B2DC3DE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0016130C0E0428D400000A16FE01131B111B3AF60000000011076FCA00000A11066FA500000A130D00110D6F9400000A131C38A5000000111C6F3700000A7418000001130E00110E6F8700000A72D26500706F8800000A6F8900000A130F110A110F6FC200000A6FED00000A130C110C131B111B2D1200110A110F6F3A00000A6FED00000A130C00110C131B111B2D4B0072B46B00707E7800000A7EA900000A110E0916286D0000061310110711106FB500000A26110A11106F8700000A72D26500706F8800000A6F8900000A6FC200000A11106FEC00000A000000111C6F3E00000A131B111B3A4BFFFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0000000872946E0070046F5700000A72AC6E0070282900000A6F7F00000A0000DE0613110011117A0000086FC000000A6FDE00000A6F9400000A131C38A5000000111C6F3700000A741800000113120011126F8700000A72D26500706F8800000A6F8900000A1313110A11136FC200000A6FED00000A130C110C131B111B2D1200110A11136F3A00000A6FED00000A130C00110C131B111B2D4B0072B46B00707E7800000A7EA900000A11120916286D0000061314110711146FB500000A26110A11146F8700000A72D26500706F8800000A6F8900000A6FC200000A11146FEC00000A000000111C6F3E00000A131B111B3A4BFFFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC000528D400000A16FE01131B111B3A5101000000723A6A00701306110711066FA500000A13150011156F9400000A131C38FE000000111C6F3700000A741800000113160000110A6FEE00000A6FEF00000A131E38AA000000111E6F3700000A743600000113170072686B007011176F1C00000A6FC200000A286300000A1306111611066FBC00000A1318111814FE0116FE01131B111B2D250072686B007011176F1C00000A6F3A00000A286300000A1306111611066FBC00000A131800111814FE0116FE01131B111B2D3600110A11176FF000000A7418000001131972B46B00707E7800000A7EA900000A11190916286D000006131A1116111A6FB500000A260000111E6F3E00000A131B111B3A46FFFFFFDE1D111E7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC0000111C6F3E00000A131B111B3AF2FEFFFFDE1D111C7541000001131D111D14FE01131B111B2D08111D6F3F00000A00DC000014130A72CE660070096FB400000A72090A0070720D0A00706F3500000A0F00FE16110000016F1C00000A282F00000A1304110406732A00000A13051105166FA200000A00066F2B00000A00289F00000A11056FA000000A00066F5300000A0000DE0613110011117A002A000041C4000002000000200100002B0000004B0100001D00000000000000020000008601000041000000C70100001D000000000000000200000016020000BC000000D20200001D0000000000000000000000F10200002000000011030000060000005F000001020000002B030000BC000000E70300001D000000000000000200000057040000C1000000180500001D00000000000000020000003404000015010000490500001D00000000000000000000000E000000BC050000CA050000060000005F0000011E02285900000A2A133005006300000044000011001B8D680000010C081672BB0900701F0E73F100000AA20817727D1600701F0C156A73F200000AA2081872C66E00701E73F100000AA2081972D86E00701F0E73F100000AA2081A72EA6E00701F0C156A73F200000AA20873F300000A0A060B2B00072A0013300400EC000000450000110072FC6E0070732600000A0A066F4A00000A0B07026F2C00000A00066F2B00000A00076F4E00000A0C28740000060D086F4F00000A16FE01130411043A9C0000000038860000000009160872BB0900706F5000000AA5110000016FF400000A00091708727D1600706F5000000A74360000016FF500000A0009180872C66E00706F5000000AA53E0000016FF600000A0009190872D86E00706F5000000AA5110000016FF400000A00091A0872EA6E00706F5000000A74360000016FF500000A00289F00000A096FF700000A0000086F5100000A130411043A6BFFFFFF00086F5200000A00066F5300000A002A133003004A000000460000110072FC6E0070732600000A0A066F4A00000A0B07026F2C00000A00076F4B00000A72306F00701F196F4C00000A036F4D00000A00066F2B00000A00076F9C00000A26066F5300000A002A00001330020031000000460000110072FC6E0070732600000A0A066F4A00000A0B07026F2C00000A00066F2B00000A00076F9C00000A26066F5300000A002A000000133003009B00000047000011000314FE0116FE010C082D077E2200000A10010328F800000A10010516FE010C082D1272466F00700372466F0070282900000A1001140A020D091A300A09172E16091A2E202B3D091E2E1E091F102E2C091F202E2B2B2D724C6F007003283400000A0A2B2A030A2B2672866F00700372CE6F0070282900000A0A2B13030A2B0F030A2B0B721270007073CE00000A7A0604287A0000060B2B00072A00133003007E00000048000011000314FE0116FE010C082D077E2200000A10010328F800000A10010516FE010C082D1272466F00700372466F0070282900000A1001140A020D0917594504000000020000000A000000160000000E0000002B1472347000700A2B17030A2B13727E7000700A2B0B721571007073CE00000A7A0604287A0000060B2B00072A0000133002001E0000000E00001100030B072D0B021F11734400000A0A2B0B021F10734400000A0A2B00062A0000133002003000000049000011000316FE010C082D0E723971007002286300000A0A2B0C72B571007002286300000A0A061F11734400000A0B2B00072A133003001A0000000C00001100020A0672090A0070720D0A00706F3500000A0A060B2B00072A0000133003001A0000000C00001100020A06723572007072397200706F3500000A0A060B2B00072A000013300300B10000004A00001100020A06282300000A0C083A9B0000000006723972007072357200706F3500000A0A06724572007072397200706F3500000A0A06725572007072637200706F3500000A0A067263720070726D7200706F3500000A0A067271720070727B7200706F3500000A0A06727F72007072997200706F3500000A0A06729972007072A77200706F3500000A0A0672AB72007072090A00706F3500000A0A0672B972007072C97200706F3500000A0A00060B2B00072A000000133003008C0000004A00001100020A06282300000A0C082D79000672C972007072B97200706F3500000A0A0672090A007072AB7200706F3500000A0A0672A7720070727F7200706F3500000A0A06726D72007072557200706F3500000A0A06723972007072457200706F3500000A0A06723572007072397200706F3500000A0A06727B72007072717200706F3500000A0A00060B2B00072A13300200680000004B00001100037E1C000004734400000A0A06026F4600000A0B731500000A0C2B2F0008076F4700000A6F1B00000A26076FF900000A0B076FFA00000A16FE01130411042D0A0008046F1B00000A260000076FFA00000A130411042DC5086F1C00000A731D00000A0D2B00092A221F23801C0000042A1E02285900000A2A00000042534A4201000100000000000C00000076322E302E35303732370000000005006C000000481F0000237E0000B41F00007C1E000023537472696E677300000000303E0000D07200002355530000B1000010000000234755494400000010B10000C412000023426C6F620000000000000002000001571F02080902000000FA2533001600000100000069000000140000002A0000008C000000A901000001000000FA000000090000003D0000004B0000000800000001000000040000000C00000000000A0001000000000006000D0106010A003E01230106004F0106010600560106010A00750160010600A6019A010600C501BB010600D701BB010A00EA0160010A000502600106003E022B020A00AB0260010A00B20260010E00380319030E003E0319030A00B805A2050600D70506011200910686060A003508A2050A0050081701060086086B0812009E0886060A009F0A60011200490B86061200870B860606009F0B2B020A00620C60010A00440D23010E00E70D19030A00550E23010600B20E930E06001C130A13060033130A13060050130A1306006F130A1306008813930E06009C130A130600C2130A130600DB130A130600F8130A13060011140A1306002C140A13060045140A13060073146014B300871400000600B61496140600D614961406000C1506010A00221523010A004315230106004A15930E06006015930E0A00AF1523010600C41506010A00F215A2050A001016FD150A002216FD150A003C16A2050A004B16FD150A005E1617010A00791617010600A91606010600B7162B020A00DD16170106001B1706010E00431719030E004B1719030A007117A2050A00971717010A00A517FD150A00BB17A2050A00D717FD150E00CD1819030600841906010600891906010A00AD1917010A00B81917010600D91906010600E41906010600F4192B020600FA1906010600261A06010600321A96140A004D1A230157008B1A00000600DB1A6B081200131B86061200391B86060600461B060106007A1B06010A00911B17010A00B61B23010A00C11B23010600B11C06010600CA1C06010A00D41C17011200411D86060600681D06010600731D2B021200B31D86060600CB1D06010600F41D2B0206000A1E2B020A001F1E23010E00110E19030000000001000000000001000100092110002600000005000100010001001000320000000D000200070001001000470000000D00020022000A011000580000000500020074000A011000730000000500040074000A011000860000000500090074000A0110009E00000005000D00740081011000B800C4000D001100740002010000D600000011001100740002010000DF00000011001800740081011000E900C4000D001C00740001001000F10000000D001C00800003011000001800000D001D00830003011000131800000D001E00840003011000701800000D002000860003011000DC1800000D002400880003011000151900000D0026008A0003011000281900000D0027008B0000000000961A00000D0029008D000100B4011F000600230C79030600330C79030600230C790306003F0C790306004B0C79030600540C790306005B0C790306006B0C7C030600780C7C030600850C800306008F0C80030600970C84030600A40C84030600850C800306008F0C80030606B10C88035680B90C8B035680C50C8B035680D70C8B035680E00C8B035680EF0C8B035680FF0C8B030606B10C880356800E0DAD0356801B0DAD0356802D0DAD033600F40DE1030600130F2E080600261832080600250F360806008318400806002618320806002F0F360806003E0F36080600130F790306003E0F36080600130F2E0806003B19910806003E0F36081300E81A520B1300FA1B520B50200000000086005B010A00010060200000000086007F010E00010091200000000086008A0114000200A82000000000860090011A000300ED2000000000E601D20123000300012100000000E601E401290004001821000000009600F2012F000500382B0000000096000E023E000A007C2B0000000096001C0248000D00DC2B0000000096004A0252001000342C0000000096005B025D001300502C0000000096007402650015009C2C000000009600890271001A00A82D000000009600BD0279001C00C02F000000009600E50287002100D43000000000910044039700270030310000000091004403A0002A0090310000000096006403AD002F0030320000000096008903BD0035008432000000009600AF03AD003A002433000000009600D503BD0040007833000000009600FC03CB004500B4330000000096001B04BD00490008340000000096003D04D7004E0044340000000096006204E300520094340000000096008A04F1005700F035000000009600AC04FF005C00C836000000009100D50497006200E836000000009600EB040F016500A037000000009600140519016800D038000000009600450525016C006C3D00000000910056052D016E0006410000000086185E050A00790010410000000096006405500179000442000000009600760561017F00A842000000009100C6056E018300F842000000009100DC05750185007043000000009100ED0575018800E843000000009100FC057E018B003C44000000009100180675018D00B4440000000091002E0686019000584C00000000910044067E019800AC4C000000009100560675019A00244D00000000910074069A019D00A44D0000000091009D06A401A000544F000000009100AB06AB01A100BC4F000000009100B7067501A4003450000000009100CC06B301A7007050000000009100E706B301A800AC500000000091000007B901A90004520000000091001307C501AF00EC520000000091002307CF01B20010540000000091003407D901B50034550000000091004607C501B800E0550000000091005B07C501BB008C560000000091006C079A01BE0044570000000091007B079A01C100FC5700000000910090077501C4007458000000009100A9077501C700EC58000000009100C6077501CA0064590000000091006C077501CD00DC59000000009100DF077501D000545A000000009100F5077501D300CC5A0000000091000B087501D600445B0000000091001D087501D900BC5B0000000091004208E301DC00445C0000000091005A08ED01DF00E45C0000000091008D08F601E100B45D000000009100AA080002E2002861000000009100B3080802E400A861000000009100BC081302E5002862000000009100C408A401E600D863000000009100CF08A401E7008865000000009100E0081D02E8000C67000000009100ED081D02E900E868000000009100F9082402EA00046B00000000910006092B02EB001C6D00000000910014091D02EC00A06E00000000910025093202ED00846F0000000091003B093D02EE002C700000000091003B094302EF00C8700000000091004A094D02F000E8700000000091004A095302F1008071000000009100B3085A02F3000C7200000000910059094D02F400947200000000960075096502F500E0730000000096008B097302FA009074000000009600A409850201016875000000009600B609910205019076000000009600CB0985020B016877000000009600DE0991020F019078000000009600F409850215015C79000000009600010A91021901F879000000009600110A85021F01D87A000000009600230A91022301747B000000009600380A65022901C07C000000009600470A73022E01707D000000009600590AA1023501447E0000000096006D0AB3023C01407F000000009600840AC90245018480000000009600AB0AD5024901AE80000000009600C10AE1024D01C480000000009600D50AF40254014486000000009600F10AF4025701D48C000000009100100BE1025A01F8900000000091002A0BFC0261012098000000009100510B08036601FC9E000000009100770B1003680114A1000000009100920B20036E0120A3000000009100A90B2F037401E4A5000000009100B70B40037B01C8AA000000009100D30B4A037E01A4AC000000009600E70B52038001D0B0000000009600050C6503870174B70000000086185E050A008F017CB7000000009600520DB1038F01ECB7000000009600630DB6038F01E4B8000000009600740DBB0390013CB9000000009600830DB60392017CB9000000009600930DC103930124BA000000009600930DCB039701B0BA000000009100930DD5039B01DCBA0000000096009C0DD5039D0118BB000000009600A90DDC039F0140BB000000009600BA0DDC03A00168BB000000009600C50DDC03A10128BC000000009600D60DDC03A201C0BC000000009600FC0DE503A3013DBD0000000086185E050A00A60134BD000000009118721E1A12A601382F0000000086185E050A00A601402F0000000086185E050A00A601902F00000000860036183A08A601482F0000000086185E050A00A701502F00000000860093183A08A70103310000000086185E050A00A8010C31000000008600EF183A08A801B0350000000086185E050A00A901B8350000000086185E050A00A901C0350000000086004B193A08A901000001000B0E00000100110E00000100170E00000100190E000001001B0E00000200230E000003002D0E000004003D0E00000500470E000001004E0E00000200670E00000300740E000001004E0E00000200670E00000300740E000001004E0E00000200670E00000300740E00000100810E020002008D0E000000000000000001004E0E00000200670E00000300740E00000400BF0E00000100CB0E00000200D50E000001004E0E00000200DC0E00000300E70E00000400F10E00000500FC0E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E00000600FC0E000001001F0F00000200250F00000300130F000001001F0F00000200250F00000300130F000004002F0F000005003E0F00000100520F00000200580F00000300640F00000400DC0E00000500E70E00000600F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E00000100520F00000200580F00000300640F00000400DC0E00000500E70E00000600F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E000001004E0E00000200DC0E00000300E70E00000400F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E000001004E0E00000200DC0E00000300E70E00000400F10E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E000001004E0E00000200DC0E00000300E70E00000400F10E00000500FC0E000001004E0E00000200DC0E00000300130F00000400E70E00000500F10E00000600FC0E000001008D0E000002003E0F00000300130F000001004E0E000002006C0F00000300730F000001004E0E000002006C0F00000300820F00000400730F00000100890F00000200960F000001009F0F02000200A30F02000300A80F02000400AD0F02000500B20F02000600B70F02000700BC0F02000800C10F02000900C60F02000A00CB0F02000B00D00F00000100D60F00000200DA0F00000300E80F02000400F30F000005000010000006000F1000000100D60F00000200DA0F00000300E80F02000400F30F000001001D10000002002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001003C10000002002410000001002F10000002003C1000000300241000000100D60F00000200DA0F00000300E80F02000400F30F020005001D10020006004A10000007002410000008005410000001005410000002002410000001002F10000002003C10000003002410000001006210000002003C10000003002410000001007010000001007910000002008310000003002410000001002F10000002003C10000003002410000001002410000001002410000001003C10000002001D10000003004A1000000400E80F000005002410000006000010000001009010000002003C10000003002410000001009C10000002003C10000003002410000001009C10000002003C10000003002410000001009010000002003C10000003002410000001009010000002003C1000000300241000000100A910000002003C1000000300241000000100A910000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C10000003002410000001002F10000002003C1000000300241000000100B410000002003C1000000300241000000100C510000002003C10000003002410000001002F10000002003C1000000300241000000100D31000000200DF1000000300EA1000000100D31000000200DF1000000100EA1000000100EF1000000200F810000001007010000001007010000001007010000001007010000001007010000001007010000001000111000001000111000001007010000001007010000001000711000001000F1100000100011100000100011100000200181100000100071100000100211100000100D50E00000200DC0E00000300E70E00000400F10E00000500FC0E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000700FC0E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000100D50E00000200DC0E00000300E70E00000400F10E00000500FC0E00000100D50E00000200261100000300DC0E00000400130F00000500E70E00000600F10E00000700FC0E00000100D50E00000200301100000300DC0E00000400391100000500E70E00000600F10E00000700FC0E00000100D50E00000200261100000300DC0E00000400391100000500130F000006004F1100000700E70E00000800F10E00000900FC0E00000100D50E000002006C0F00000300820F00000400730F00000100D50E000002006611000003007211000004007D1100000100D50E000002008A11000003006611000004009711000005007211000006007D1102000700A11100000100A41100000200AF1100000300721100000100A41100000200AF1100000300721100000100D50E000002008A11000003006611000004009711000005007211000006007D1102000700A11100000100D50E00000200BC1100000300AF1100000400721100000500C81100000100D91100000200DF1100000100E61100000200DF1100000300ED11000004007D1100000500D91100000600FA1100000100F81000000200ED11000003007D1100000400E61100000500D91100000600081200000100ED11000002007D1100000300E61100000400DF1100000500D91100000600151200000700081200000100241200000200311200000300D911000001003F1200000200D91100000100D50E000002008A11000003004A12000004005412000005005F1202000600711200000700741200000100D50E000002008012000003008E1200000400A21200000500AC1200000600B412000007007211000008007D1100000100CE1200000100CE1200000200261100000100CE1200000100DD1200000200E21200000300E70E00000400F10E00000100DD1200000200E21200000300E70E00000400F10E00000100670E00000200E70E00000100E91200000200730F00000100ED1200000100ED1200000100ED1200000100ED1200000100ED1200000200F31200000300FB12000001008D0E000001008D0E000001008D0E000001008D0E02000900F1005E050A00F9005E050A0001015E05000409015E05000411015E05000419015E05000421015E05050429015E05000431015E05000439015E05000441015E05000449015E05000451015E05000459015E05000461015E050A0471015E05110479015E050A0081015E050A0089015E05160499015E05A40431005E050A0029006B15AB0429007615AF0431008015B30431008D15BE0431009415C40431008D15CB0419009B15AF0429005E0500043900A415AF0431005E0500044100E4010004A9015E050A00B101CB157903B101D115FE05B101DF150306B1014315090681005E050004A1005E050A00490076151106B101EB151606B9015E051D06C1011D160A00C9012C160004D1015E052406D90159162B06B10143153106A10070163806E90194163E06B1019E163E06F1019B15AF04B101EB154206B101AF164806E901C3164E06F901D11654060102E5165806B101EE16AF04B101F316AF04B101EB155D06B101FB160306B10109176306F9011217AB04090227170A00B101EB156806B1014315700629002F177706510076153E0679005E05B30679003B17630679003803C10611027615AF0479005B1726077900AF16480681006317DE07B9018817E4072102A117EA073102B117F307B901C917F8074102E417AB044102E51658064102D201AB044102F0170A00C101F0170A0031009E163E06B101F617FE0769007615AB0461007615AF041902C3164E0619005E050A0061006B15AB0449025E0548087900AF164E0811027A193E0611029E163E0651029B196B0A61025E05740AA100CD197C0A6902A117820AB1014315890AC90142085406B101DE198F0A49006B15AB047902EC19960A79029B159B0A81025E0511048102001AF3078102051AA00AA1000D1AA50A0102141AAB0AE101A117B10A81021D1A54068102211A5406810294163E060102E516E60A91020B0EEB0A49002F17F00A49002D1AF70A99025E050A00A1025E050A008900CB15020B69006B15AB0451005E05110499005E050A003102631A00049900751A1B0BB1005E050A00B100831A00040C00C3163C0B1400D1164D0B910094163E061C005E0511041C00A117630B1C00FC1A6B0B9100081B740BC1002A1B7A0BB902081B800BC1007615AF04C9025E051104C9024E1B870BD900571B7C03D900601B900BC902FB16980BF1014E1BA20B5100571B84035100601BA90BC1006C1BAF0414001217AB049100C3164E06D1029B15AF0424005E050A0089005E0500042400A117DE0BB9015E050A00B901821B030CC9019D1B090CC901830D3E063102AD1B11042102A117200CE102C91B270CE902D21B2406D102E11BFE05C901E71B11040C005E050A000C00A117DE0BC1000E1CB10C2C005E050A00F101E11B060D2C00A117DE0BB9001A1C8003B900571B8003D900E11B460DB900E11B4C0D5100E11B620D51002F17680D34005E050A003400A117DE0B2400C3163C0B3C00D1164D0B3C001217AB04C100231CAF04C100301CEF0D31008015050E310080150E0E79023C1C160E310080153A0EE902441C420EE902551C0A00C100641C500E7900751C5E0EB101811C6306B1018C1C6306B100951C710E31005E051104B101A91CAF04C100B1170004F102BA1C760EF1029B157C0EB101C21C810E0102141A870EF9025E058D0EC101E41C950EC100EE1CDF0EC100FD1CEF0DC100091DAF048900161D300FF9025E050004D1005E050A00C1001D1DAB04C1002F1D0A0069002F17550F6900DF155B0F6900391D640FB1004D1D6A0FB100581D800BB9028D15740FD100A117CE0FD1000917D30FD100051AD80FB1016E1DE40FD100791DEB0FD100051AA00AC100811D5010C100901DAF04C100991D5510B100A51D73102103C3164E068900C31D7910B90076157E108900FB16A51079023C1C060D79029B150111F102DB1D760EB9002F171A116900E61D461131035E050A003103A1174D113103FE1DD30F3103161E53115900C3164E063103E516591141035E059D1141035E05A511E1005E05AE11E1002B1EC111E100331EC811E1003D1ECE11E902461E420E7900551EDC0371005C1E09124903661EAB04080048008F0308004C009403080050009903080054009E0308005800A30308005C00A803080064008F0308006800940308006C0099032E00830099122E002B0034122E007B0090122E008B00A2122E001B001E122E0023001E122E003B0034122E0043003A122E0053003A122E005B0047122E00630063122E006B0085122E007300341243009B001D04E0000B01D60400010B018F0320010B018F0340010B01CE0644010B00ED0380010B013507A0010B014D07A4010B00ED03C0010B018F03C301B3038F03E0010B018F03E301B3038F030302B3038F0304020B00ED032302B3038F0340020B018F034302B3038F0360020B018F036302B3038F0380020B018F038302B3038F03A0020B018F03A4020B00ED03C0020B018F03C4020B00ED03E0020B018F0300030B018F0320030B018F0340030B018F0360030B018F03A0030B018F03C0030B018F03E0030B01CE084004BB038F036004BB038F03E40A0B00ED03840B0B00ED03400CBB038F03600CBB038F03800CBB038F03A00CBB038F03C00CBB038F03E00CBB038F03000DBB038F03200EBB038F03400EBB038F0300100B018F03BA04D1047D067903BA06C7062D07040813084408560869086E0878087E089508A608B808B80AFB0A060B120B220B2B0BAF0BE40B100C190C2D0C3D0C4C0C5D0C6A0C7A0C910CB70CE80C0B0D210D330D520D6E0D850D9F0DB70DC70DD60DF60D1B0E290E300E480E560E630E6C0E9B0EE40E360F7D0FF30F5E108410AD10CE10061122115E11B611D411E211EA11F311FC1103120E12360B460B5B0BD70B000D7E0DAF0DE10F048000000500020000000000000000000000F41400000200000000000000000000000100FD0000000000020000000000000000000000010017010000000002000000000000000000000001000601000000000200000000000000000000000100860600000000050004000600040007000400080004000A0009000B0009000E0003000F000300100003001100030012000300130003000000003C4D6F64756C653E004272696467656C696E652E434C5246756E6374696F6E732E646C6C0047656E6572617465586D6C0055736572446566696E656446756E6374696F6E730053746F72656450726F63656475726573004E6567617461626C65436F6C6C656374696F6E4F6647756964730050726F70657274795365617263684974656D0044617465416E644D6F6E657952616E67655365617263680044617465416E64496E746567657252616E676553656172636800456E756D657261746F72730046696E64416E645265706C616365434C52004974656D5479706500526567657854797065005574696C697479005265676578536561726368006D73636F726C69620053797374656D0056616C7565547970650053797374656D2E44617461004D6963726F736F66742E53716C5365727665722E536572766572004942696E61727953657269616C697A65004F626A65637400456E756D00496E69740053797374656D2E446174612E53716C54797065730053716C537472696E6700416363756D756C617465004D65726765005465726D696E6174650053797374656D2E5465787400537472696E674275696C64657200737472586D6C0053797374656D2E494F0042696E61727952656164657200526561640042696E6172795772697465720057726974650053716C47756964004E617646696C7465725F47657451756572790053716C496E7433320052656765785F49734D617463680052656765785F4765744D617463680053797374656D2E436F6C6C656374696F6E730049456E756D657261626C650052656765785F4765744D6174636865730052656765785F4765744D6174636865735F46696C6C526F770052656765785F5265706C6163654D6174636865730046696E64416E645265706C6163655F476574436F6E74656E744C6F636174696F6E0053716C586D6C0053716C426F6F6C65616E0046696E64416E645265706C6163655F4973436F6E74656E74446566696E6974696F6E4D617463680046696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E670053797374656D2E546578742E526567756C617245787072657373696F6E73004D6174636800526567657800436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F720046696E64416E645265706C6163655F497346696C6550726F706572746965734D617463680046696E64416E645265706C6163655F4765745570646174656446696C6550726F70657274790046696E64416E645265706C6163655F4973496D61676550726F706572746965734D617463680046696E64416E645265706C6163655F47657455706461746564496D61676550726F70657274790046696E64416E645265706C6163655F4973506167654E616D654D617463680046696E64416E645265706C6163655F47657455706461746564506167654E616D650046696E64416E645265706C6163655F49735061676550726F706572746965734D617463680046696E64416E645265706C6163655F476574557064617465645061676550726F706572746965730046696E64416E645265706C6163655F497354657874436F6E74656E744D617463680046696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E7454657874005461674174747269627574654576616C7561746F720046696E64416E645265706C6163655F49734C696E6B496E436F6E74656E74446566696E6974696F6E0046696E64416E645265706C6163655F476574557064617465644C696E6B496E436F6E74656E74446566696E6974696F6E0047657452656C6174696F6E5461626C650046696C6C526F77002E63746F7200436F6E746163745F536561726368434C5200436F6E746163745F4765744175746F446973747269627574696F6E4C697374436F6E74616374436F756E740053797374656D2E446174612E53716C436C69656E740053716C436F6E6E656374696F6E00476574436F756E744279537461747573004775696400457865637574655369746547726F75700045786563757465536974654964730045786563757465427943757272656E7453697465436F6E746578740045786563757465536974654174747269627574657300526574726965766556616C75657346726F6D584D4C004765744D616E75616C436F6E74616374730045786563757465417474726962757465436F6E746163745365617263680045786563756974655369746547726F75700053797374656D2E586D6C00586D6C4E6F64654C697374004765745369746547726F7570730057726974654F75747075740045786563757465436F6E74616374536F757263650043726561746554656D706F726172794F75747075745461626C650044726F7054656D706F726172794F75747075745461626C650045786563757465476574436F6E74616374730045786563757465507572636861736500457865637574654F7264657253697A6500457865637574654F72646572436F756E7400457865637574654162616E646F6E65644361727400457865637574654C6173744C6F67696E0045786563757465576174636865730045786563757465466F726D5375626D6974746564004578656375746550726F6475637473507572636861736564004578656375746550726F647563745479706573507572636861736564004578656375746557656273697465557365725365617263680045786563757465437573746F6D657247726F757073004578656375746553656375697274794C6576656C730045786563757465496E6465785465726D730045786563757465446973747269627574696F6E4C6973740053716C506172616D6574657200457865637574655363616C617200446174615461626C650045786563757465446174615461626C650053797374656D2E436F6C6C656374696F6E732E47656E65726963004C697374603100476574436F6E66696775726174696F6E00586D6C446F63756D656E74006765744E6F64657300476574477569647300476574496E7473004765745761746368657300476574466F726D5375626D6974746564004765744C6173744C6F67696E004765745075726368617365004765744F7264657253697A65004765744F72646572436F756E74004765744162616E646F6E6564436172740047657450726F70657274795365617263684974656D00476574586D6C466F72477569647300476574586D6C466F724E6F64657300476574586D6C466F7250726F70657274795365617263684974656D00476574436F6E74656E74446566696E6974696F6E7300557064617465436F6E74656E74446566696E6974696F6E730047657446696C6550726F706572746965730055706461746546696C6550726F7065727469657300476574496D61676550726F7065727469657300557064617465496D61676550726F7065727469657300476574506167654E616D657300557064617465506167654E616D6573004765745061676550726F70657274696573005570646174655061676550726F706572746965730047657454657874436F6E74656E740055706461746554657874436F6E74656E740046696E64416E645265706C6163655F46696E640046696E64416E645265706C6163655F5265706C6163650046696E64416E645265706C6163655F5570646174654C696E6B730053716C4461746554696D6500434C52506167654D61704E6F64655F55706461746500434C52506167654D61704E6F64655F5361766500434C52506167654D61704E6F64655F416464436F6E7461696E657200434C52506167654D61704E6F64655F52656D6F7665436F6E7461696E657200434C52506167654D61704E6F64655F5361766555706461746500434C52506167654D61704E6F64655F557064617465436F6E7461696E657200586D6C4E6F646500536574757050726F7061676174696F6E416E64496E6865726974616E63654174747269627300497465726174654368696C6472656E00586D6C456C656D656E740053657475704E65774E6F64650041727261794C697374005365744174747269627574657300536574757050616765446566696E6974696F6E4368696C6472656E0053657475705075626C69736844657461696C7300434C52506167654D61705F5361766550616765446566696E6974696F6E00434C53506167654D61704E6F64655F417474616368576F726B666C6F77004E656761746553656C656374696F6E004C6973744F6647756964730050726F706572794E616D65004F70657261746F720056616C7565310056616C7565320053716C4D6F6E6579004D696E696D756D56616C7565004D6178696D756D56616C75650053746172744461746500456E6444617465004D696E696D756D436F756E74004D6178696D756D436F756E740076616C75655F5F0054657874436F6E74656E7400436F6E74656E74446566696E6974696F6E00506167654E616D65005061676550726F7065727469657300496D61676550726F706572746965730046696C6550726F70657274696573005461674174747269627574650054616741747472696275746556616C756500436F6E74656E74446566696E6974696F6E4669656C640053716C446174615265636F72640047657453716C446174615265636F726400476574536561726368526573756C747300506572666F726D5265706C61636500457865637574654E6F6E5175657279004765745265676578004765744C696E6B526567657800466F726D61745365617263685465726D0048746D6C456E636F646500436F6E76657274586D6C546F48746D6C00436F6E7665727448746D6C546F586D6C0052656765784F7074696F6E73004F7074696F6E7300526567657853656C656374416C6C0056616C75650047726F757000720077007175657279496400636F6E646974696F6E004F72646572427950726F706572747900536F72744F726465720053697465496400736F757263650053716C46616365744174747269627574650072656765785061747465726E0072656765784F7074696F6E73006D617463684F626A656374006D617463680053797374656D2E52756E74696D652E496E7465726F705365727669636573004F7574417474726962757465007265706C6163656D656E7400636F6E74656E74496400736974654964007365617263685465726D006D61746368436173650077686F6C65576F72647300696E636C75646541747472696275746556616C756573007265706C6163655465726D006669656C640068746D6C52656765780061747472696275746552656765780061747472696275746556616C75655265676578007469746C65006465736372697074696F6E00616C7454657874006F6C6455726C006D6174636857686F6C654C696E6B006E657755726C0072656C6174696F6E54797065006F626A6563744964006F626A00636F6C3100636F6C3200636F6C3300636F6C3400636F6C3500636F6C3600636F6C3700636F6C3800636F6C3900636F6C313000786D6C004170706C69636174696F6E4964004D61785265636F72647300546F74616C5265636F72647300696E636C7564654164647265737300436F6E746163744C69737449640073746174757300636F6E6E656374696F6E006F7065726174696F6E586D6C006170706C69636174696F6E496400736F72744F7264657200636F6E746163744C6973744964007369746547726F75704974656D006E6F64654C697374004F7065726174696F6E00746F74616C5265636F72647300705365617263684974656D0073656172636856616C756573007365617263684974656D0073656375726974794C6576656C586D6C00696E6465785465726D73586D6C00636F6D6D616E644E616D6500706172616D657465727300636F6E6E00646F63756D656E74006E6F64654E616D65006E6F646573006474477569647300636F6C477569647300726F6F744E6F6465006C697374006F626A65637449647300736561726368496E0068746D6C456E636F6465645365617263685465726D0068746D6C456E636F6465645265706C6163655465726D00706167654D61704E6F6465006D6F6469666965644279006D6F6469666965644461746500706172656E744E6F64654964006372656174656442790069640074656D706C617465496400636F6E7461696E65724964730074656D706C61746549647300696E7365727444656C657465466C6167006462446F630064624E6F646500696E4E6F6465006D6F64696669656442794964006C617374466F756E644E6F6465007365744578747261496E666F006578636C7564654174747269627300696E70757450616765446566006F6E655061676544624E6F6465006462506167654E6F646500437265617465644279004D6F64696669656442790070616765446566696E6974696F6E586D6C004964007075626C6973685061676500706167654D61704E6F6465496400706167654D61704E6F6465576F726B466C6F770070726F706F6761746500696E686572697400617070656E64546F4578697374696E67576F726B666C6F77730073716C436F6D6D616E64546578740074797065007365617263680075726C00696E707574007061747465726E006D6174636844656C696D697465720053797374656D2E5265666C656374696F6E00417373656D626C795469746C6541747472696275746500417373656D626C794465736372697074696F6E41747472696275746500417373656D626C79436F6E66696775726174696F6E41747472696275746500417373656D626C7943756C7475726541747472696275746500436F6D56697369626C6541747472696275746500417373656D626C79496E666F726D6174696F6E616C56657273696F6E41747472696275746500417373656D626C7956657273696F6E41747472696275746500417373656D626C7946696C6556657273696F6E41747472696275746500417373656D626C79436F6D70616E7941747472696275746500417373656D626C79436F7079726967687441747472696275746500417373656D626C7950726F6475637441747472696275746500417373656D626C7954726164656D61726B4174747269627574650053797374656D2E446961676E6F73746963730044656275676761626C6541747472696275746500446562756767696E674D6F6465730053797374656D2E52756E74696D652E436F6D70696C6572536572766963657300436F6D70696C6174696F6E52656C61786174696F6E734174747269627574650052756E74696D65436F6D7061746962696C697479417474726962757465004272696467656C696E652E434C5246756E6374696F6E730053657269616C697A61626C654174747269627574650053716C55736572446566696E656441676772656761746541747472696275746500466F726D6174005374727563744C61796F7574417474726962757465004C61796F75744B696E64006765745F49734E756C6C006765745F56616C756500417070656E64466F726D617400417070656E6400496E7365727400546F537472696E670052656164537472696E670053716C46756E6374696F6E41747472696275746500537472696E6700456D7074790049734E756C6C4F72456D707479006F705F457175616C69747900436F6E6361740053716C436F6D6D616E640053797374656D2E446174612E436F6D6D6F6E004462436F6E6E656374696F6E004F70656E004462436F6D6D616E64007365745F436F6D6D616E64546578740053716C446174614164617074657200446244617461416461707465720046696C6C0044617461526F77436F6C6C656374696F6E006765745F526F777300496E7465726E616C44617461436F6C6C656374696F6E42617365006765745F436F756E74006765745F4C656E67746800496E743332005265706C6163650049456E756D657261746F7200476574456E756D657261746F72006765745F43757272656E740044617461526F77006765745F4974656D005472696D00546F4C6F776572006F705F496E657175616C69747900436F6E7461696E73004D6F76654E6578740049446973706F7361626C6500446973706F7365006F705F496D706C696369740049734D617463680043617074757265004D61746368436F6C6C656374696F6E004D61746368657300437265617465436F6D6D616E640053716C506172616D65746572436F6C6C656374696F6E006765745F506172616D65746572730053716C44625479706500416464004462506172616D65746572007365745F56616C75650053716C44617461526561646572004578656375746552656164657200446244617461526561646572006765745F486173526F777300436C6F736500537562737472696E67003C3E635F5F446973706C6179436C61737332003C3E635F5F446973706C6179436C61737335004353243C3E385F5F6C6F63616C7333003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F31003C3E635F5F446973706C6179436C61737337004353243C3E385F5F6C6F63616C7336003C46696E64416E645265706C6163655F47657455706461746564436F6E74656E74446566696E6974696F6E586D6C537472696E673E625F5F30004D617463684576616C7561746F72003C3E635F5F446973706C6179436C61737361003C436F6E74656E74446566696E6974696F6E4669656C644576616C7561746F723E625F5F39003C3E635F5F446973706C6179436C61737364003C3E635F5F446973706C6179436C61737366004353243C3E385F5F6C6F63616C7365003C46696E64416E645265706C6163655F4765745570646174656454657874436F6E74656E74546578743E625F5F63006765745F496E64657800547970650052756E74696D655479706548616E646C65004765745479706546726F6D48616E646C650044617461436F6C756D6E0044617461436F6C756D6E436F6C6C656374696F6E006765745F436F6C756D6E7300436861720053706C697400436F6E7665727400546F496E74363400537461636B00496E743634005075736800546F4172726179004E6577526F77007365745F4974656D00506F70005065656B0044424E756C6C004E756C6C00436F6D70696C657247656E6572617465644174747269627574650053716C50726F636564757265417474726962757465007365745F506172616D657465724E616D65007365745F53716C446254797065004C6F6164586D6C00456E756D657261746F72003C50726976617465496D706C656D656E746174696F6E44657461696C733E7B30334536444332462D464136312D344630302D393341302D3042304236303335374442347D0044696374696F6E61727960320024246D6574686F643078363030303032392D310054727947657456616C7565006765745F4974656D4F6600586D6C417474726962757465436F6C6C656374696F6E006765745F4174747269627574657300586D6C41747472696275746500446563696D616C005472795061727365004D617856616C7565006F705F4578706C69636974006765745F496E6E65725465787400426F6F6C65616E007365745F436F6E6E656374696F6E00436F6D6D616E6454797065007365745F436F6D6D616E6454797065007365745F53697A650053716C436F6E746578740053716C50697065006765745F506970650045786563757465416E6453656E64005061727365007365745F436F6D6D616E6454696D656F75740024246D6574686F643078363030303034352D310053656C6563744E6F646573004D696E56616C7565006765745F4F75746572586D6C00417070656E644368696C6400546F496E7433320053656E64526573756C747353746172740053656E64526573756C7473456E640053656C65637453696E676C654E6F6465006765745F4F7074696F6E73005374617274735769746800456E647357697468006765745F446F63756D656E74456C656D656E7400546F5570706572004461746554696D65006765745F4E6F77005472696D456E6400457863657074696F6E00436F6E6E656374696F6E5374617465006765745F5374617465006765745F506172656E744E6F64650052656D6F76654368696C64006765745F496E6E6572586D6C00457175616C73006765745F4861734368696C644E6F6465730052656D6F7665416C6C006F705F5472756500586D6C4E6F646554797065004372656174654E6F646500437265617465417474726962757465004172726179004A6F696E00494C6973740041646170746572006765745F4368696C644E6F646573006765745F4E616D6500496E73657274416674657200437265617465456C656D656E7400586D6C4E616D65644E6F64654D6170004E65774775696400466F726D6174457863657074696F6E006765745F5574634E6F77006F705F4C6F676963616C4E6F7400486173687461626C6500436F6E7461696E734B65790049436F6C6C656374696F6E006765745F4B6579730053716C4D65746144617461005365744775696400536574537472696E6700536574496E7433320053656E64526573756C7473526F7700457363617065004E6578744D61746368006765745F53756363657373002E6363746F720000000000213C0067007500690064003E007B0030007D003C002F0067007500690064003E0000333C00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064003E0000353C002F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064003E00000B500072006900630065000080912C002800730065006C0065006300740020006D0069006E0028004C006900730074005000720069006300650029002000660072006F006D00200050005200500072006F00640075006300740053004B0055002000770068006500720065002000500072006F006400750063007400490064003D0050002E00490064002900200061007300200050007200690063006500001354006F007000530065006C006C00650072000080A92C002800730065006C006500630074002000730075006D002800500072006500760069006F007500730053006F006C00640043006F0075006E00740029002000660072006F006D00200050005200500072006F00640075006300740053004B0055002000770068006500720065002000500072006F006400750063007400490064003D0050002E00490064002900200061007300200054006F007000530065006C006C006500720000174300720065006100740065006400440061007400650000192C0043007200650061007400650064004400610074006500008087530065006C006500630074002000440069007300740069006E0063007400200027007B0030007D0027002000610073002000460069006C007400650072004900640020007B0031007D002C00200050002E00490064002000660072006F006D00200050005200500072006F0064007500630074002000500020005700680065007200650020000180E3530065006C006500630074002000440069007300740069006E0063007400200027007B0030007D00270020002000610073002000460069006C007400650072004900640020007B0031007D002C00200050002E00490064002000660072006F006D00200050005200500072006F00640075006300740020005000200049006E006E006500720020004A006F0069006E00200056005700500072006F0064007500630074004100740074007200690062007500740065002000560020006F006E00200050002E00490064003D0056002E00490064002000570068006500720065002000012F63006F006E007400650078007400200063006F006E006E0065006300740069006F006E003D0074007200750065000035730065006C00650063007400200074006F00700031002000660072006F006D00200050005200500072006F00640075006300740000822D530065006C00650063007400200053006E006F002C004C00660074004F0070006E0064002C005200670074004F0070006E0064002C004C00660074004F0070006E0064004E0061006D0065002C005200670074004F0070006E0064004E0061006D0065002C004F00700072002C004C00660074004F0062006A0065006300740054007900700065002C005200670074004F0062006A0065006300740054007900700065002C0056002E00560061006C00750065002000520067007400560061006C007500650020002000660072006F006D0020004E005600460069006C007400650072004900740065006D00200046004900200049006E006E006500720020004A006F0069006E0020004E005600460069006C007400650072004900740065006D005100750065007200790020004900510020006F006E002000460049002E004900640020003D00490051002E004900740065006D0049006400200049006E006E006500720020004A006F0069006E0020004E005600460069006C00740065007200510075006500720079002000510020006F006E002000490051002E0051007500650072007900490064003D0051002E0049006400200049006E006E006500720020004A006F0069006E0020004E005600460069006C00740065007200560061006C00750065002000560020006F006E002000460049002E005200670074004F0070006E0064003D0056002E0049006400200057006800650072006500200051002E00490064003D0027000123270020004F0072006400650072002000420079002000460049002E0053006E006F000182BD530065006C006500630074002000640069007300740069006E006300740020002700300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D0030003000300030003000300030003000300030003000300027002000490064002C0043002E006E0061006D0065002C0054002E006E0061006D0065002000410074007400720069006200750074006500440061007400610054007900700065002C00310020004900730050006800790073006900630061006C0020002000660072006F006D0020007300790073002E0063006F006C0075006D006E00730020004300200049006E006E006500720020006A006F0069006E0020007300790073002E006F0062006A00650063007400730020004F0020006F006E0020004F002E006F0062006A006500630074005F00690064003D0043002E006F0062006A006500630074005F0069006400200069006E006E006500720020006A006F0069006E00200020007300790073002E00740079007000650073002000540020006F006E00200043002E00730079007300740065006D005F0074007900700065005F00690064003D0054002E00730079007300740065006D005F0074007900700065005F0069006400200020005700680065007200650020004F002E006E0061006D00650020003D0027007B0030007D002700200041006E00640020004F002E0074007900700065003D00270055002700200061006E006400200054002E006E0061006D006500200021003D0027007300790073006E0061006D0065002700200055006E0069006F006E00200041006C006C002000530065006C006500630074002000490064002C005400690074006C0065002C00410074007400720069006200750074006500440061007400610054007900700065002C00300020002000660072006F006D0020007B0031007D00011350005200500072006F00640075006300740000174100540041007400740072006900620075007400650000032000000520007B0000057D00200000032800000528007B0000077B0030007D00001B4C00660074004F0062006A006500630074005400790070006500001B5200670074004F0062006A00650063007400540079007000650000174C00660074004F0070006E0064004E0061006D00650000096E0061006D006500000F4C00660074004F0070006E006400000549006400002341007400740072006900620075007400650044006100740061005400790070006500000F530079007300740065006D002E0000010011520067007400560061006C00750065000003270001052700270001074F007000720000096C0069006B0065000003250000154900730050006800790073006900630061006C0000033100000D64006F00750062006C006500000769006E007400000B63006100730074002800001720006100730020006D006F006E00650079002900200000116400610074006500740069006D0065000005200027000123200063006F006E007600650072007400280076006100720063006800610072002C00000F2C002000310030003100290020000047200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027000111270029002C002000310030003100290001033000000741004E0044000080ED280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200056005700500072006F00640075006300740041007400740072006900620075007400650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E0064002000430041005300540028005B00560061006C00750065005D0020006100730020004D004F004E0045005900290020007B0031007D0020007B0032007D0029002900011D2800410074007400720069006200750074006500490064003D002700013D2700200061006E0064002000430041005300540028005B00560061006C00750065005D0020006100730020004D004F004E0045005900290020002000010329000080D3280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200056005700500072006F00640075006300740041007400740072006900620075007400650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E00640020005B00560061006C00750065005D0020007B0031007D00200027007B0032007D00270029002900011D2700200061006E00640020005B00560061006C00750065005D002000010527002900018173280045005800490053005400530020002800530045004C00450043005400200031002000460052004F004D00200056005700500072006F00640075006300740041007400740072006900620075007400650020005600200057004800450052004500200050002E00490064003D0056002E0049006400200061006E0064002000410074007400720069006200750074006500490064003D0027007B0030007D002700200061006E006400200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C005B00560061006C00750065005D0029002C003100300031002900200020007B0031007D00200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C0027007B0032007D00270029002C002000310030003100290029002900016B2700200061006E006400200063006F006E007600650072007400280076006100720063006800610072002C0063006F006E00760065007200740028006400610074006500740069006D0065002C005B00560061006C00750065005D0029002C00310030003100290020000113270029002C0020003100300031002900290001136F00720064006500720020006200790020000033530065006C006500630074002000460069006C00740065007200490064002C00490064002000660072006F006D0020002800000B2900200046004F002000002F43006F006E007400650078007400200043006F006E006E0065006300740069006F006E003D00740072007500650000866557004900540048000900520065006C0061007400650064004E006F0064006500730020002800490064002C00200050006100720065006E007400490064002C0020005400690074006C0065002C0020005B004C006500760065006C005D0029002000410053000D000A000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200028000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900490064002C00200050006100720065006E007400490064002C0020005400690074006C0065002C00200030000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900480053005300740072007500630074007500720065000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020005700480045005200450009004900640020003D0020004000490064000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000090009000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200055004E0049004F004E00200041004C004C000D000A002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000090009000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400090070006100720065006E0074002E00490064002C00200070006100720065006E0074002E0050006100720065006E007400490064002C00200070006100720065006E0074002E005400690074006C0065002C0020005B004C006500760065006C005D0020002B00200031000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900520065006C0061007400650064004E006F0064006500730020004100530020006300680069006C0064002C00200048005300530074007200750063007400750072006500200041005300200070006100720065006E0074000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200057004800450052004500090070006100720065006E0074002E004900640020003D0020006300680069006C0064002E0050006100720065006E00740049006400200041004E0044000D000A000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200070006100720065006E0074002E004900640020003C003E002000400053006900740065004900640020000D000A000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000D000A000D000A0009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C00450043005400090009005400690074006C0065000D000A0009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0009000900520065006C0061007400650064004E006F006400650073000D000A00090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020004F00520044004500520020004200590009005B004C006500760065006C005D00200044004500530043000007400049006400000F4000530069007400650049006400000D7B0030007D0020003E002000000B5400690074006C006500000963006F006C003100000963006F006C003200000963006F006C003300000963006F006C003400000963006F006C003500000963006F006C003600000963006F006C003700000963006F006C003800000963006F006C003900000B63006F006C00310030000080CD530065006C006500630074002000630061007300740028004C0066007400200061007300200076006100720063006800610072002800310030002900290020002B00200027002D00270020002B0020006300610073007400280052006700740020006100730020007600610072006300680061007200280031003000290029002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004E006F006400650054007900700065003D0027007B0030007D002700018105530065006C006500630074002000630061007300740028004C0066007400200061007300200076006100720063006800610072002800310030002900290020002B00200027002D00270020002B0020006300610073007400280052006700740020006100730020007600610072006300680061007200280031003000290029002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004C006600740020003E0020007B0030007D00200061006E006400200052006700740020003C0020007B0031007D00200061006E00640020004F0062006A00650063007400490064003D0027007B0032007D002700018089530065006C0065006300740020002A002000660072006F006D00200052004D00520065006C006100740069006F006E00730020005700680065007200650020004C006600740020004200650074007700650065006E0020007B0030007D00200061006E00640020007B0031007D0020004F00720064006500720020006200790020004C0066007400000752006700740000074C006600740000114F0062006A0065006300740049006400000D530074006100740075007300003143006F006E0074006100630074005F0047006500740043006F0075006E0074004200790053007400610074007500730000155300690074006500470072006F00750070007300001B4100700070006C00690063006100740069006F006E0049006400002F43006F006E0074006100630074005F005300650061007200630068005300690074006500470072006F0075007000000F5300690074006500490064007300002B43006F006E0074006100630074005F005300650061007200630068005300690074006500490064007300003743006F006E0074006100630074005F00460069006C0074006500720042007900530069007400650043006F006E007400650078007400001D53006900740065004100740074007200690062007500740065007300003943006F006E0074006100630074005F0053006500610072006300680053006900740065004100740074007200690062007500740065007300001353006F00720074004F007200640065007200001D43006F006E00740061006300740053006F0075007200630065007300002144006900730074007200690062007500740069006F006E004C00690073007400001549006E006400650078005400650072006D007300001D530065006300750072006900740079004C006500760065006C007300001D43007500730074006F006D0065007200470072006F00750070007300000F5700610074006300680065007300002357006500620073006900740065005500730065007200530065006100720063006800002D43006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068000023500072006F0064007500630074007300500075007200630068006100730065006400002B500072006F00640075006300740054007900700065007300500075007200630068006100730065006400001B46006F0072006D005300750062006D006900740074006500640000134C006100730074004C006F00670069006E0000115000750072006300680061007300650000134F007200640065007200530069007A00650000154F00720064006500720043006F0075006E007400001B4100620061006E0064006F006E00650064004300610072007400002F470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660049006E0074007300000D560061006C0075006500310000194D0069006E0069006D0075006D00560061006C007500650000194D006100780069006D0075006D00560061006C007500650000194D0069006E0069006D0075006D0043006F0075006E00740000194D006100780069006D0075006D0043006F0075006E007400001B43006F006E0074006100630074004C0069007300740049006400003343006F006E0074006100630074005F004100640064004D0061006E00750061006C0043006F006E0074006100630074007300003343006F006E00740061006300740041007400740072006900620075007400650053006500610072006300680058006D006C00003D43006F006E0074006100630074005F0043006F006E007400610063007400410074007400720069006200750074006500530065006100720063006800001F4E0065006700610074006500530065006C0065006300740069006F006E0000174C006900730074004F00660047007500690064007300003D49006E007300650072007400200069006E0074006F002000740065007300740043006F0075006E0074002000760061006C0075006500730028002700010527002C00013743006F006E0074006100630074005F0053006500610072006300680043006F006E00740061006300740053006F0075007200630065000081D54900460020004500580049005300540053002800530065006C0065006300740020002A002000660072006F006D002000740065006D007000640062002E00640062006F002E007300790073006F0062006A00650063007400730020006F0020007700680065007200650020006F002E0078007400790070006500200069006E00200028002700550027002900090061006E00640009006F002E006900640020003D0020006F0062006A006500630074005F0069006400280020004E002700740065006D007000640062002E002E002300740065006D00700043006F006E0074006100630074005300650061007200630068004F007500740070007500740027002900290020005400720075006E00630061007400650020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400200045004C0053004500200063007200650061007400650020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F0075007400700075007400280049006400200075006E0069007100750065006900640065006E0074006900660069006500720029000147440072006F00700020007400610062006C00650020002300740065006D00700043006F006E0074006100630074005300650061007200630068004F007500740070007500740000154D00610078005200650063006F00720064007300001D49006E0063006C007500640065004100640064007200650073007300003F43006F006E0074006100630074005F0047006500740043006F006E00740061006300740042007900490064007300460072006F006D00540065006D0070000013530074006100720074004400610074006500000F45006E0064004400610074006500003543006F006E0074006100630074005F005300650061007200630068004C0061007300740050007500720063006800610073006500002F43006F006E0074006100630074005F005300650061007200630068004F007200640065007200530069007A006500003143006F006E0074006100630074005F005300650061007200630068004F00720064006500720043006F0075006E007400003743006F006E0074006100630074005F005300650061007200630068004100620061006E0064006F006E00650064004300610072007400002F43006F006E0074006100630074005F005300650061007200630068004C006100730074004C006F00670069006E0000155700610074006300680065007300490064007300002B43006F006E0074006100630074005F005300650061007200630068005700610074006300680065007300000F46006F0072006D00490064007300003943006F006E0074006100630074005F0053006500610072006300680046006F0072006D0073005300750062006D00690074007400650064000015500072006F006400750063007400490064007300003F43006F006E0074006100630074005F00530065006100720063006800500072006F0064007500630074007300500075007200630068006100730065006400001D500072006F0064007500630074005400790070006500490064007300004743006F006E0074006100630074005F00530065006100720063006800500072006F0064007500630074005400790070006500730050007500720063006800610073006500640000295700650062007300690074006500550073006500720053006500610072006300680058006D006C00003343006F006E0074006100630074005F0053006500610072006300680057006500620073006900740065005500730065007200002343007500730074006F006D0065007200470072006F00750070007300490064007300003943006F006E0074006100630074005F0053006500610072006300680043007500730074006F006D0065007200470072006F007500700073000021530065006300750072006900740079004C006500760065006C00490064007300003743006F006E0074006100630074005F00530065006100720063006800530065006300750072006900740079004C006500760065006C00001B49006E006400650078005400650072006D007300490064007300003143006F006E0074006100630074005F0053006500610072006300680049006E006400650078005400650072006D007300002744006900730074007200690062007500740069006F006E004C00690073007400490064007300003D43006F006E0074006100630074005F0053006500610072006300680044006900730074007200690062007500740069006F006E004C00690073007400008093530065006C0065006300740020004E0061006D0065002000660072006F006D0020004300540043006F006E00740061006300740053006500610072006300680043006F006E006600690067002000570068006500720065002000490073004100630074006900760065003D00310020004F0072006400650072002000420079002000530065007100750065006E006300650000094E0061006D006500006143006F006E0074006100630074005300650061007200630068002F00530069007400650041007400740072006900620075007400650073002F004100740074007200690062007500740065005300650061007200630068004900740065006D00004143006F006E0074006100630074005300650061007200630068002F0043006F006E00740061006300740053006F00750072006300650073002F0069006E007400003B43006F006E0074006100630074005300650061007200630068002F005300690074006500470072006F007500700073002F006700750069006400003543006F006E0074006100630074005300650061007200630068002F0053006900740065004900640073002F006700750069006400004743006F006E0074006100630074005300650061007200630068002F0044006900730074007200690062007500740069006F006E004C006900730074002F006700750069006400003B43006F006E0074006100630074005300650061007200630068002F0049006E006400650078005400650072006D0073002F006700750069006400004143006F006E0074006100630074005300650061007200630068002F00530065006300750072006900740079004C006500760065006C0073002F0069006E007400004343006F006E0074006100630074005300650061007200630068002F0043007500730074006F006D0065007200470072006F007500700073002F006700750069006400004F43006F006E0074006100630074005300650061007200630068002F0057006100740063006800650073005B0040004C006900730074004F0066004700750069006400730021003D00220022005D00006543006F006E0074006100630074005300650061007200630068002F00570065006200730069007400650055007300650072005300650061007200630068002F00500072006F00700065007200740079005300650061007200630068004900740065006D00007143006F006E0074006100630074005300650061007200630068002F0043006F006E0074006100630074004100740074007200690062007500740065005300650061007200630068002F004100740074007200690062007500740065005300650061007200630068004900740065006D00005943006F006E0074006100630074005300650061007200630068002F00500072006F00640075006300740073005000750072006300680061007300650064002F004B0065007900560061006C00750065005000610069007200006143006F006E0074006100630074005300650061007200630068002F00500072006F006400750063007400540079007000650073005000750072006300680061007300650064002F004B0065007900560061006C00750065005000610069007200005B43006F006E0074006100630074005300650061007200630068002F0046006F0072006D005300750062006D00690074007400650064005B0040004C006900730074004F0066004700750069006400730021003D00220022005D00002F43006F006E0074006100630074005300650061007200630068002F004C006100730074004C006F00670069006E00002D43006F006E0074006100630074005300650061007200630068002F0050007500720063006800610073006500002F43006F006E0074006100630074005300650061007200630068002F004F007200640065007200530069007A006500003143006F006E0074006100630074005300650061007200630068002F004F00720064006500720043006F0075006E007400003743006F006E0074006100630074005300650061007200630068002F004100620061006E0064006F006E00650064004300610072007400002943006F006E0074006100630074005300650061007200630068002F00530074006100740075007300002F43006F006E0074006100630074005300650061007200630068002F0053006F00720074004F007200640065007200001B43006F006E00740061006300740053006500610072006300680000114F00700065007200610074006F007200000F4200650074007700650065006E00000D560061006C00750065003200000930002E003000300000353C00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F006600470075006900640073003E0000373C002F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F006600470075006900640073003E000031470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064007300000B3C007B0030007D003E00000D3C002F007B0030007D003E00003F3C0043006F006E0074006100630074005300650061007200630068003E003C002F0043006F006E0074006100630074005300650061007200630068003E00000D5C0077007B0032002C007D0000810B530045004C004500430054002000490064002C0020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280050006100720065006E007400490064002C00200027007B0031007D0027002900200041005300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A00011F460052004F004D00200043004F0043006F006E00740065006E0074000A000080875700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D00200031003300200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D0027002000016541004E00440020004300410053005400280058006D006C0053007400720069006E00670020004100530020006E00760061007200630068006100720028004D00410058002900290020004C0049004B0045002000270025007B0030007D002500270020000180AB41004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730043006F006E00740065006E00740044006500660069006E006900740069006F006E004D006100740063006800280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C0020007B0031007D002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A0001254F00520044004500520020004200590020005400690074006C00650020004100530043000023550050004400410054004500200043004F0043006F006E00740065006E0074000A000080E1530045005400200058006D006C0053007400720069006E00670020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640043006F006E00740065006E00740044006500660069006E006900740069006F006E0058006D006C0053007400720069006E006700280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D002C0020007B0034007D0029000A0001826757004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0043006F006E00740065006E00740044006500660069006E006900740069006F006E0073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001812F530045004C00450043005400200063006F002E00490064002C00200063006F002E005400690074006C00650020004100730020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280063006F002E0050006100720065006E007400490064002C00200027007B0031007D0027002900200061007300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A00017F460052004F004D00200043004F0043006F006E00740065006E007400200063006F00200049004E004E004500520020004A004F0049004E00200043004F00460069006C00650020006600690020004F004E002000660069002E0043006F006E00740065006E0074004900640020003D00200063006F002E00490064000A0000816B57004800450052004500200063006F002E005B005300740061007400750073005D0020003D0020003100200041004E004400200063006F002E004F0062006A0065006300740054007900700065004900640020003D0020003900200041004E004400200063006F002E004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0049007300460069006C006500500072006F0070006500720074006900650073004D006100740063006800280063006F002E005400690074006C0065002C00200063006F002E005B004400650073006300720069007000740069006F006E005D002C002000660069002E0041006C00740054006500780074002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012B4F005200440045005200200042005900200063006F002E005400690074006C006500200041005300430000816F53004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F007000650072007400790028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029002C0020005B004400650073006300720069007000740069006F006E005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F007000650072007400790028005B004400650073006300720069007000740069006F006E005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001825F57004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900011D550050004400410054004500200043004F00460069006C0065000A000080B3530045005400200041006C007400540065007800740020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400460069006C006500500072006F0070006500720074007900280041006C00740054006500780074002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001828357004800450052004500090043006F006E00740065006E00740049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200066002E0043006F006E00740065006E007400490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F00460069006C006500200041005300200066000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200066002E0043006F006E00740065006E007400490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001816F57004800450052004500200063006F002E005B005300740061007400750073005D0020003D0020003100200041004E004400200063006F002E004F0062006A0065006300740054007900700065004900640020003D00200033003300200041004E004400200063006F002E004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730049006D00610067006500500072006F0070006500720074006900650073004D006100740063006800280063006F002E005400690074006C0065002C00200063006F002E005B004400650073006300720069007000740069006F006E005D002C002000660069002E0041006C00740054006500780074002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A0001817353004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F007000650072007400790028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029002C0020005B004400650073006300720069007000740069006F006E005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F007000650072007400790028005B004400650073006300720069007000740069006F006E005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001826157004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180B5530045005400200041006C007400540065007800740020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640049006D00610067006500500072006F0070006500720074007900280041006C00740054006500780074002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001828557004800450052004500090043006F006E00740065006E00740049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200066002E0043006F006E00740065006E007400490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F00460069006C006500200041005300200066000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200066002E0043006F006E00740065006E007400490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180D5530045004C004500430054002000500064002E00500061006700650044006500660069006E006900740069006F006E00490064002000410053002000490064002C002000500064002E005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C00200050006E002E0050006100670065004D00610070004E006F006400650049006400200061007300200046006F006C00640065007200490064002C0020002700270020004100530020004C006F0063006100740069006F006E000A00018129460052004F004D002000500061006700650044006500660069006E006900740069006F006E002000500064002000430052004F005300530020004100500050004C00590020002800530045004C00450043005400200054004F00500020003100200050006E0064002E0050006100670065004D00610070004E006F0064006500490064002000660072006F006D00200050006100670065004D00610070004E006F00640065005000610067006500440065006600200050006E006400200077006800650072006500200050006E0064002E00500061006700650044006500660069006E006900740069006F006E004900640020003D002000500064002E00500061006700650044006500660069006E006900740069006F006E00490064002900200050006E000A000080B757004800450052004500200053006900740065004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004900730050006100670065004E0061006D0065004D0061007400630068002800500064002E005400690074006C0065002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012B4F0052004400450052002000420059002000500064002E005400690074006C0065002000410053004300002D5500500044004100540045002000500061006700650044006500660069006E006900740069006F006E000A000080A353004500540020005400690074006C00650020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00470065007400550070006400610074006500640050006100670065004E0061006D00650028005400690074006C0065002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A000182B3570048004500520045000900500061006700650044006500660069006E006900740069006F006E0049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200070002E00500061006700650044006500660069006E006900740069006F006E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D000900500061006700650044006500660069006E006900740069006F006E00200041005300200070000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F0050006100670065004E0061006D00650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200070002E00500061006700650044006500660069006E006900740069006F006E00490064000D000A0009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200029000180D7530045004C0045004300540020006400650074002E005000610067006500490064002000410053002000490064002C0020006400650066002E005400690074006C00650020004100530020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C00200050006E002E0050006100670065004D00610070004E006F006400650049006400200041007300200046006F006C00640065007200490064002C0020002700270020004100530020004C006F0063006100740069006F006E000A000180BD460052004F004D0020005000610067006500440065007400610069006C0073002000410053002000640065007400200049004E004E004500520020004A004F0049004E002000500061006700650044006500660069006E006900740069006F006E00200041005300200064006500660020004F004E0020006400650066002E00500061006700650044006500660069006E006900740069006F006E004900640020003D0020006400650074002E005000610067006500490064000A000080FD430052004F005300530020004100500050004C00590020002800530045004C00450043005400200054004F00500020003100200050006E0064002E0050006100670065004D00610070004E006F0064006500490064002000660072006F006D00200050006100670065004D00610070004E006F00640065005000610067006500440065006600200050006E006400200077006800650072006500200050006E0064002E00500061006700650044006500660069006E006900740069006F006E004900640020003D0020006400650066002E00500061006700650044006500660069006E006900740069006F006E00490064002900200050006E000A000080DD5700480045005200450020006400650066002E0053006900740065004900640020003D00200027007B0030007D002700200041004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00490073005000610067006500500072006F0070006500720074006900650073004D00610074006300680028006400650074002E005000610067006500440065007400610069006C0058004D004C002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A00012D4F00520044004500520020004200590020006400650066002E005400690074006C0065002000410053004300002755005000440041005400450020005000610067006500440065007400610069006C0073000A000080CF53004500540020005000610067006500440065007400610069006C0058006D006C0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740055007000640061007400650064005000610067006500500072006F00700065007200740069006500730028005000610067006500440065007400610069006C0058006D006C002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D0029000A0001827B570048004500520045000900500061006700650049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200070002E005000610067006500490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D0009005000610067006500440065007400610069006C007300200041005300200070000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F005000610067006500500072006F0070006500720074006900650073002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200070002E005000610067006500490064000D000A000900090020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002900010D5C0077007B0033002C007D0000810B530045004C004500430054002000490064002C0020005400690074006C0065002C0020007B0030007D0020004100530020004900740065006D0054007900700065002C002000490053004E0055004C004C00280050006100720065006E007400490064002C00200027007B0031007D0027002900200061007300200046006F006C00640065007200490064002C002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740043006F006E00740065006E0074004C006F0063006100740069006F006E002800490064002C00200027007B0032007D002700290020004100530020004C006F0063006100740069006F006E000A000180855700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D0020003700200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D0027002000013141004E00440020005B0054006500780074005D0020004C0049004B0045002000270025007B0030007D0025002700200001809941004E0044002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0049007300540065007800740043006F006E00740065006E0074004D00610074006300680028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D002C0020007B0032007D002C0020007B0033007D00290020003D00200031000A000180BF53004500540020005B0054006500780074005D0020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F004700650074005500700064006100740065006400540065007800740043006F006E00740065006E007400540065007800740028005B0054006500780074005D002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D002C0020007B0033007D002C0020007B0034007D0029000A0001825957004800450052004500090049006400200049004E00200028000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000530045004C004500430054000900440049005300540049004E0043005400200063002E00490064000D000A000900090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000460052004F004D00090043004F0043006F006E00740065006E007400200041005300200063000D000A0009000900090009000900200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200049004E004E004500520020004A004F0049004E00200040004F0062006A006500630074004900640073002E006E006F00640065007300280027002F00460069006E00640041006E0064005200650070006C006100630065002F00540065007800740043006F006E00740065006E0074002F0049006400270029002000410053002000540028006300290020004F004E00200054002E0063002E00760061006C0075006500280027002E0027002C002000270075006E0069007100750065006900640065006E007400690066006900650072002700290020003D00200063002E00490064000D000A00090009002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000200020002000290001372F00460069006E00640041006E0064005200650070006C006100630065002F00540065007800740043006F006E00740065006E00740000452F00460069006E00640041006E0064005200650070006C006100630065002F0043006F006E00740065006E00740044006500660069006E006900740069006F006E00730000332F00460069006E00640041006E0064005200650070006C006100630065002F0050006100670065004E0061006D0065007300003D2F00460069006E00640041006E0064005200650070006C006100630065002F005000610067006500500072006F007000650072007400690065007300003F2F00460069006E00640041006E0064005200650070006C006100630065002F0049006D00610067006500500072006F007000650072007400690065007300003D2F00460069006E00640041006E0064005200650070006C006100630065002F00460069006C006500500072006F00700065007200740069006500730000808353004500540020005B0054006500780074005D0020003D002000640062006F002E00520065006700650078005F005200650070006C006100630065004D0061007400630068006500730028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D002C00200027007B0032007D00270029000A0001808D5700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D0020003700200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E00440020000153640062006F002E00520065006700650078005F00490073004D00610074006300680028005B0054006500780074005D002C00200027007B0030007D0027002C0020007B0031007D00290020003D00200031000180C7530045005400200058006D006C0053007400720069006E00670020003D002000640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F0047006500740055007000640061007400650064004C0069006E006B0049006E0043006F006E00740065006E00740044006500660069006E006900740069006F006E00280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C00200027007B0031007D0027002C0020007B0032007D0029000A000180F35700480045005200450020005B005300740061007400750073005D0020003D0020003100200041004E00440020004F0062006A0065006300740054007900700065004900640020003D00200031003300200041004E00440020004100700070006C00690063006100740069006F006E004900640020003D00200027007B0030007D002700200041004E00440020004300410053005400280058006D006C0053007400720069006E00670020004100530020006E00760061007200630068006100720028004D00410058002900290020004C0049004B0045002000270025007B0031007D0025002700200041004E004400200001808F640062006F002E00460069006E00640041006E0064005200650070006C006100630065005F00490073004C0069006E006B0049006E0043006F006E00740065006E00740044006500660069006E006900740069006F006E00280058006D006C0053007400720069006E0067002C00200027007B0030007D0027002C0020007B0031007D00290020003D0020003100013B2F00470065006E00650072006900630043006F006C006C0065006300740069006F006E004F00660047007500690064002F006700750069006400000D27007B0030007D0027002C000180CD530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002C0043006F006E007400610069006E006500720058006D006C0020002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E0020005700680065007200650020002000540065006D0070006C0061007400650049006400200069006E00200028007B0030007D0029000023500061006700650044006500660069006E006900740069006F006E0058006D006C00001943006F006E007400610069006E006500720058006D006C0000272F002F00700061006700650044006500660069006E006900740069006F006E005B0031005D0000156D006F006400690066006900650064004200790000196D006F006400690066006900650064004400610074006500000375000081A93C0063006F006E007400610069006E00650072002000690064003D0022007B0030007D002200200063006F006E00740065006E007400490064003D002200300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D003000300030003000300030003000300030003000300030002200200069006E005700460043006F006E00740065006E007400490064003D002200300030003000300030003000300030002D0030003000300030002D0030003000300030002D0030003000300030002D003000300030003000300030003000300030003000300030002200200063006F006E00740065006E0074005400790070006500490064003D002200300022002000690073004D006F006400690066006900650064003D002200460061006C0073006500220020006900730054007200610063006B00650064003D002200460061006C007300650022002000760069007300690062006C0065003D00220054007200750065002200200069006E0057004600560069007300690062006C0065003D002200540072007500650022002F003E000180C95500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002C0043006F006E007400610069006E006500720058006D006C0020003D004E0027007B0031007D0027002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0032007D0027000121500061006700650044006500660069006E006900740069006F006E0049006400002550006100670065004D00610070004E006F00640065005F005500700064006100740065000080C9530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002C0043006F006E007400610069006E006500720058006D006C002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E002000570068006500720065002000540065006D0070006C0061007400650049006400200069006E00200028007B0030007D00290000193C0043006F006E007400610069006E006500720073003E00001B3C002F0043006F006E007400610069006E006500720073003E00002D2F002F0063006F006E007400610069006E00650072005B004000690064003D0022007B0030007D0022005D00001B3C0043006F006E007400610069006E006500720073002F003E000080C95500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002C0043006F006E007400610069006E006500720058006D006C003D004E0027007B0031007D00270020002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0032007D0027000163530065006C00650063007400200070006100670065004D006100700058006D006C002000460052004F004D00200050006100670065004D006100700020005700680065007200650020007300690074006500490064003D0027007B0030007D002700010569006400006750006100670065004D00610070004E006F0064006500200064006F006500730020006E006F00740020006500780069007300740020006F007200200069006E00760061006C0069006400200050006100670065004D00610070004E006F00640065004900640000312F002F0070006100670065004D00610070004E006F00640065005B004000690064003D0022007B0030007D0022005D00004350006100720065006E007400200050006100670065004D00610070004E006F0064006500200064006F006500730020006E006F007400200065007800690073007400001770006100670065004D00610070004E006F00640065000071550070006400610074006500200050006100670065004D00610070002000530065007400200070006100670065004D006100700058006D006C0020003D00200027007B0030007D00270020005700680065007200650020007300690074006500490064003D0027007B0031007D0027000180D1530065006C006500630074002000500061006700650044006500660069006E006900740069006F006E00490064002C00500061006700650044006500660069006E006900740069006F006E0058006D006C002000460052004F004D002000500061006700650044006500660069006E006900740069006F006E0020005700680065007200650020005300690074006500490064003D0027007B0030007D002700200061006E0064002000540065006D0070006C0061007400650049006400200069006E00200028007B0031007D002900011363006F006E007400610069006E0065007200001363006F006E00740065006E00740049006400001B69006E005700460043006F006E00740065006E00740049006400001B63006F006E00740065006E0074005400790070006500490064000015690073004D006F00640069006600690065006400000B460061006C007300650000136900730054007200610063006B006500640000809F5500700064006100740065002000500061006700650044006500660069006E006900740069006F006E0020005300650074002000500061006700650044006500660069006E006900740069006F006E0058006D006C003D004E0027007B0030007D0027002000570068006500720065002000500061006700650044006500660069006E006900740069006F006E00490064003D0027007B0031007D002700012F700072006F0070006F006700610074006500530065006300750072006900740079004C006500760065006C007300002B69006E0068006500720069007400530065006300750072006900740079004C006500760065006C007300001D730065006300750072006900740079004C006500760065006C007300001D700072006F0070006F00670061007400650052006F006C0065007300001969006E006800650072006900740052006F006C0065007300000B72006F006C006500730000312F002F0070006100670065004D00610070004E006F00640065005B004000690064003D0027007B0030007D0027005D00012F640065007300630065006E00640061006E0074003A003A0070006100670065004D00610070004E006F006400650000032C00001770006100670065006D00610070006E006F0064006500001D700061006700650064006500660069006E006900740069006F006E00002770006100670065006D00610070006E006F006400650077006F0072006B0066006C006F007700003B6300680069006C0064003A003A0070006100670065004D00610070004E006F00640065005B004000690064003D0022007B0030007D0022005D0000416300680069006C0064003A003A00700061006700650044006500660069006E006900740069006F006E005B004000690064003D0022007B0030007D0022005D00001D700061006700650044006500660069006E006900740069006F006E00004B6300680069006C0064003A003A0070006100670065004D00610070004E006F006400650057006F0072006B0046006C006F0077005B004000690064003D0022007B0030007D0022005D00002770006100670065004D00610070004E006F006400650057006F0072006B0046006C006F007700001363007200650061007400650064004200790000176300720065006100740065006400440061007400650000216300680069006C0064003A003A0063006F006E007400610069006E006500720000196300680069006C0064003A003A007300740079006C006500001B6300680069006C0064003A003A0073006300720069007000740000376300680069006C0064003A003A0063006F006E007400610069006E00650072005B004000690064003D0022007B0030007D0022005D00002F6300680069006C0064003A003A007300740079006C0065005B004000690064003D0022007B0030007D0022005D00000B7300740079006C00650000316300680069006C0064003A003A007300630072006900700074005B004000690064003D0022007B0030007D0022005D00000D73006300720069007000740000197000750062006C0069007300680043006F0075006E00740000177000750062006C00690073006800440061007400650000237300740061007400750073004300680061006E006700650064004400610074006500005150006100720065006E00740020004E006F006400650020002D00200050006100670065004D00610070004E006F0064006500200064006F006500730020006E006F007400200065007800690073007400013349006E00760061006C00690064002000500061006700650044006500660069006E006900740069006F006E0020004900440000372F002F00700061006700650044006500660069006E006900740069006F006E005B004000690064003D0022007B0030007D0022005D00003550006100670065004D00610070004E006F0064006500200064006F006500730020006E006F00740020006500780069007300740000356300680069006C0064003A003A0070006100670065004D00610070004E006F006400650057006F0072006B0046006C006F00770000173C0077006F0072006B0066006C006F00770073003E0000193C002F0077006F0072006B0066006C006F00770073003E0000114900740065006D005400790070006500001146006F006C006400650072004900640000114C006F0063006100740069006F006E00003343006F006E007400650078007400200043006F006E006E0065006300740069006F006E0020003D0020007400720075006500001540004F0062006A0065006300740049006400730000055C006200003928003F003A0028003F003C0021005B003C005D002E002A003F0029007C0028003F003C003D003E005B005E003C005D002A003F0029002900004728003F003C003D003C007000610067006500440065007400610069006C0073003E002E002A003F0028003F003A003C0028005C0077002B0029003E0029002E002A003F002900004328003F003D0028003F003A002E002A003F003C002F005C0031003E0029002E002A003F003C002F007000610067006500440065007400610069006C0073003E002900002149006E00760061006C006900640020004900740065006D005400790070006500004928003F003C003D003C005C0077002B002E002A003F005C0062005C0077002B003D0028005B00220027005D002900290028003F003A0028003F0021005C00310029002E0029002A0001809528003F003C003D003C0063006F006E00740065006E00740044006500660069006E006900740069006F006E005C0073002B00540065006D0070006C00610074006500490064003D0022005B005E0022005D007B00330036007D0022002E002A005C00620028003F003C00500072006F00700065007200740079003E005C0077002B0029003D00220029005B005E0022005D002A00002349006E00760061006C00690064002000520065006700650078005400790070006500007B28003F003C003D003C0061005B005C0073005D002B005B005E003E005D002A0028003F003A0068007200650066007C00720065007400750072006E00760061006C007500650029003D00280022007C0027002700290029007B0030007D0028003F003D0028003F003A005C0031007C005B002C005D0029002900017F28003F003C003D003C0061005B005C0073005D002B005B005E003E005D002A0028003F003A0068007200650066007C00720065007400750072006E00760061006C007500650029003D00280022007C0027002700290029007B0030007D0028003F003D0028003F003A0028003F0021005C00310029002E0029002A00290001032600000B260061006D0070003B00000F26007300700061006D0070003B00000D2600730070006C0074003B00000926006C0074003B0000033C0000092600670074003B0000033E000019260064006F00750062006C006500710075006F0074003B00000D2600710075006F0074003B0000032200000D2600610070006F0073003B00000F260073006C006100730068003B0000035C00000000002FDCE60361FA004F93A00B0B60357DB40008B77A5C561934E0890320000105200101111505200101110804200011150306121905200101121D0520010112210E0005111511251115111511151125090003021115111511290900030E1115111511290A0003122D111511151129070002011C1011150B00040E11151115112911150700020E112511250D000502123111151135113511350F00060E1231111511151135113511350800030E1239123D0E0C00050E1239123D0E123D123D0F0006021115111511151115113511350D00050E111511151115113511350B00040211151115113511350B00040212311115113511350D00050E123111151115113511350D000502111511151135113511350F00060E111511151115113511351135090003021231111511350B00040E1231111511151135070002122D0E112522000B011C10112510112510112510112510112510112510112510112510112510112510000601123111251129101129113511250C00040112311125112910112906000208081241080003080E1145124107000208114512411300080112311125112910081008100E124111450900030811141145124106000111141249070003010E0812410500010112410B0006011145080E081241020900030811181145124109000308111C1145124109000308112011451241090003080E1D124D124108000212510E1D124D090001151255010E1241070002124912590E0A000115125501114512490900011512550108124906000111181249060001111C1249060001112012490A000115125501111812490500010E12510900010E1512550111450500010E12490600020E12490E0A000115125501114512510D000501112511151135113511351100070111251231111511151135113511350B00040111251115113511350F000601112512311115111511351135110007011125112911151115113511351135150009011125123111151115111511151135113511350B00040111251115111511350B000401114512311145115D1200070111451145123111451145115D101145070003010E0E11450B00050111450E0E1145113507000201125912610F000601126112611145115D125912610E000612650E1145115D1261125902100007011145115D12611261125912690209000301126512611259070002011261125912000701114511451145114512311011451135130008011145114512311135113511351145115D02060E0306116D0306115D03061129020608030611280401000000040200000004040000000408000000041000000004200000000306112C0400001271040001010E050002010E0E090004123D11280E0202090004123D112C0E0202060002123D0E020400010E0E0306117507000311150E0E0E12010001005408074D617853697A65FFFFFFFF042001010E0420010102062001011180B50420010108062001011180C9808501000200000006005402174973496E76617269616E74546F4475706C696361746573005402124973496E76617269616E74546F4E756C6C73015402124973496E76617269616E74546F4F726465720054020D49734E756C6C4966456D7074790154080B4D61784279746553697A65401F0000540E044E616D650B47656E6572617465586D6C062001011180D1032000020320000E06200212190E1C0307010205200112191C0620021219080E05200112190E04070111158126010002005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A446174614163636573730100000054557F4D6963726F736F66742E53716C5365727665722E5365727665722E53797374656D446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038391053797374656D4461746141636365737301000000040001020E050002020E0E0700040E0E1C1C1C04200011450600030E0E0E0E062002010E1241062001011280DD0520010812510600030E0E1C1C0520001280F1032000080500020E0E0E0520020E0E0E0520001280FD0320001C0420011C0E0500010E1D0E042001020E0700040E0E0E0E0E0600020E0E1D1C05000111150E35071E0E0E0E0E0E1241125112510E1280DD1280E91D0E08080E0208081281010E0E1281010E11150211451280FD1280FD1D0E128105062002010E1175060703123D020205200112390E060703123D0E025701000200540E1146696C6C526F774D6574686F644E616D651852656765785F4765744D6174636865735F46696C6C526F77540E0F5461626C65446566696E6974696F6E134D61746368206E76617263686172284D41582906200112810D0E070703123D122D02170100010054020F497344657465726D696E697374696301808F010001005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A44617461416363657373010000000520001280DD052000128111082002124D0E118115042001011C05200012811D0520020E08080E0706121912411280DD12811D0E021A070C123D123D12390E123D123D1239021280FD021280FD12810503061115030612380306123D0520010E12390306123C0307010E052002011C180720020E0E128125120708123D1281251240128125123C12380E020407020E0E0907040E12812512440E050702123D02120708123D123D123D123902021280FD128105030612481007080E123D123D128125124C12480E02110708123D123D12390E021280FD0212810515070B123D123D12190812390E0E0E021280FD128105819B010003005455794D6963726F736F66742E53716C5365727665722E5365727665722E446174614163636573734B696E642C2053797374656D2E446174612C2056657273696F6E3D322E302E302E302C2043756C747572653D6E65757472616C2C205075626C69634B6579546F6B656E3D623737613563353631393334653038390A4461746141636365737301000000540E1146696C6C526F774D6574686F644E616D650746696C6C526F77540E0F5461626C65446566696E6974696F6E80DC636F6C3120756E697175656964656E7469666965722C636F6C3220756E697175656964656E7469666965722C636F6C3320756E697175656964656E7469666965722C636F6C3420756E697175656964656E7469666965722C636F6C3520756E697175656964656E7469666965722C636F6C3620756E697175656964656E7469666965722C636F6C3720756E697175656964656E7469666965722C636F6C3820756E697175656964656E7469666965722C636F6C3920756E697175656964656E7469666965722C636F6C313020756E697175656964656E74696669657208000112812911812D072002010E128129052000128135062001011281310500020E0E1C0620011D0E1D030400010A0E0400010E0A0420001D1C05200012810105200201081C062001011281012D07140E1241125112511281310E0E1280DD1D0E1280E91281411281011281011D1C08122D1D03021280FD1281050420011C080406128149060001112511450306112506070212810102030611450B0707021145080E08124102080705080E08124102062001011181150807031D124D124D080A07041D124D124D124D0805151255010E092000151181550113000615118155010E0420001300080615128159020E080715128159020E08072002011300130108200202130010130105200112610805200012815D0620011281610E080002020E10118165070001118165116D09000202118165118165060002020E100805000108112927071312590E0E12491114111411181118111C11816511816511200808111815118155010E020E08061512550111450520010113001E070D111412611512550111450E114511141280FD02021D031D0E081281050520010112410620010111816D0807030E1280DD1D0E0607020E1280DD062001124D124D0500001281750F0706124D124D124D124D124D1280DD0E07061D124D124D124D124D124D081007071D124D124D124D124D124D124D080C07051D124D124D124D124D080F070712511280DD124D081D124D080216070A12510E12411280DD124D1280E912511D124D08021F070A151255010E12510E1280DD1280E9128101151255010E1280FD0212810505200112490E30071812491249124912491249124912491249124912491249124912491249124912491249124912491249124912490E0817070615125501114512611512550111451280FD02128105051512550108040001080E1507061512550108126115125501081280FD021281051107071118126111181280FD115D021281051207081118126111181280FD02115D02128105050001116D0E050001115D0E0F0706111C1261111C1280FD0212810505000111290E0500011129080F07061120126111201280FD0212810506151255011118190707151255011118126111181512550111181280FD021281050F070612191281010E1280FD0212810507151181550111450F0705121911450E02151181550111450E0706121912610E1280FD021281051807061512550111451281011512550111451280FD02128105062001126112610E0706125912610E1280FD0212810508200412190E1C1C1C07200212190E1D1C04000108020D0707123D0E1219020811451D1C06070212191D1C09070412190811451D1C07200312190E1C1C052001011271070703112812710205200112610E07070312591261020420001175080703123D12191145040701114504200012650500001181790420010E0E0520010E1D03052002010E1C072002010E12817D0520001181814307211241125912591249121912610E12510E1280DD1280E90E12590E0E0E1281011249126112610E0E0E0E1280DD12810112817D021280FD1281051181791D031280FD04200012614B07231241125912591249121912610E12510E1280DD1280E90E125912591281011249126112610E124912490812610E0E1280DD12810112817D021280FD1281051181791D031280FD1280FD0520010211451E070E12410E125912590E1280DD0E12611281611145126112817D12817D0205000111350208000211351135113505000102113509200312611181850E0E0820011281611281615007251241125912591249121912610E12510E1280DD1280E90E12591281011249126112610E1261128161124912490812610E0E1280DD12810112817D021280FD1281051181791D031280FD11451280FD042001081C042001021C082001128189128129021D0E0600020E0E1D0E070001126912818D5C073012816112816112816112816112816112816102020E02020E0E0E126112490812611281611D0E1D0E12690E0E1281611D0E1D0E12690E12611281611281610E0E1D0E1D0E12690E1281611D0E1D0E12690E128161021D031D0E08042000124908200212611261126114070B12610E0E0E12611265021280FD0E0212810505200112650E040000114505200011817920070C1265128161128161114512816112816112651280FD021281051181791D03070002021145114520070C1281610E11451281611281611281611281611280FD021281051181791D0332071A0E12490E12490E124912610E0E126112491261126512611261124912611265126112611249126112651280FD021281050400010E081307080808128161128161128161021181791D03070001115D11817923071212410E125912590E1280DD0E126111450212816112490812611269126512817D0206000111351135052002011C1C05200012819D0420011C1C3E071F12410E125912590E1280DD0E126112491261128199126102124912610E126512817D12610E1265124912610E126112611265021280FD1281051280FD072002010E118115082003010E1181150A072001011D1281A10A0703127112711D1281A10620020108114505200201080E0520020108080D070512411280DD12811D12710207070212411280DD0807040E123D0211280807040E123D02112C0607030E123D020507030E0E0204200012390B0705123D1239121911150203000001150100104657434C5353746F72656450726F637300000501000000000C010007352E322E302E3000001B0100164272696467656C696E65204469676974616C20496E6300002101001C436F7079726967687420C2A9204272696467656C696E65203230313300000A010005694150505300000801000701000000000801000800000000001E01000100540216577261704E6F6E457863657074696F6E5468726F7773010000000000000047787F5400000000020000001C01000038810100386301005253445308F3697DE46A5C45B3914AA7D030129801000000643A5C69415050535C52656C65617365735C5635325C4272696467656C696E652E69415050532E44617461626173655C4272696467656C696E652E434C5246756E6374696F6E735C6F626A5C44656275675C4272696467656C696E652E434C5246756E6374696F6E732E706462000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007C82010000000000000000009E820100002000000000000000000000000000000000000000000000908201000000000000000000000000000000000000005F436F72446C6C4D61696E006D73636F7265652E646C6C0000000000FF2500200010000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000100100000001800008000000000000000000000000000000100010000003000008000000000000000000000000000000100000000004800000058A00100900300000000000000000000900334000000560053005F00560045005200530049004F004E005F0049004E0046004F0000000000BD04EFFE00000100020005000000000002000500000000003F000000000000000400000002000000000000000000000000000000440000000100560061007200460069006C00650049006E0066006F00000000002400040000005400720061006E0073006C006100740069006F006E00000000000000B004F0020000010053007400720069006E006700460069006C00650049006E0066006F000000CC02000001003000300030003000300034006200300000003C001100010043006F006D006D0065006E007400730000004600570043004C005300530074006F00720065006400500072006F00630073000000000050001700010043006F006D00700061006E0079004E0061006D006500000000004200720069006400670065006C0069006E00650020004400690067006900740061006C00200049006E006300000000004C0011000100460069006C0065004400650073006300720069007000740069006F006E00000000004600570043004C005300530074006F00720065006400500072006F006300730000000000300008000100460069006C006500560065007200730069006F006E000000000035002E0032002E0030002E003000000058001C00010049006E007400650072006E0061006C004E0061006D00650000004200720069006400670065006C0069006E0065002E0043004C005200460075006E006300740069006F006E0073002E0064006C006C0000005C001C0001004C006500670061006C0043006F007000790072006900670068007400000043006F0070007900720069006700680074002000A90020004200720069006400670065006C0069006E00650020003200300031003300000060001C0001004F0072006900670069006E0061006C00460069006C0065006E0061006D00650000004200720069006400670065006C0069006E0065002E0043004C005200460075006E006300740069006F006E0073002E0064006C006C0000002C0006000100500072006F0064007500630074004E0061006D00650000000000690041005000500053000000340008000100500072006F006400750063007400560065007200730069006F006E00000035002E0032002E0030002E003000000038000800010041007300730065006D0062006C0079002000560065007200730069006F006E00000035002E0032002E0030002E0030000000000000000000000000000000000000000000000000000000008001000C000000B03200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    WITH PERMISSION_SET = UNSAFE

GO
PRINT N'Creating [dbo].[GenerateXml]...';


GO
IF OBJECT_ID(N'[dbo].[GenerateXml]', 'AF') IS NULL
CREATE AGGREGATE [dbo].[GenerateXml](@Value NVARCHAR (4000))
    RETURNS NVARCHAR (4000)
    EXTERNAL NAME [Bridgeline.CLRFunctions].[GenerateXml];


GO
PRINT N'Creating [dbo].[FindAndReplace_GetContentLocation]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetContentLocation]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[FindAndReplace_GetContentLocation]
(@contentId UNIQUEIDENTIFIER, @siteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetContentLocation]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
(@source XML, @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedFileProperty]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedFileProperty]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedFileProperty]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedImageProperty]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedImageProperty]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedImageProperty]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (4000), @newUrl NVARCHAR (4000), @matchWholeLink BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedLinkInContentDefinition]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageName]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageName]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageName]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageProperties]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedPageProperties]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties]
(@source XML, @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageProperties]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedTextContentText]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_GetUpdatedTextContentText]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedTextContentText]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_IsContentDefinitionMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsContentDefinitionMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch]
(@source XML, @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsContentDefinitionMatch]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_IsFilePropertiesMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsFilePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch]
(@title NVARCHAR (4000), @description NVARCHAR (4000), @altText NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsFilePropertiesMatch]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_IsImagePropertiesMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsImagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch]
(@title NVARCHAR (4000), @description NVARCHAR (4000), @altText NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsImagePropertiesMatch]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_IsLinkInContentDefinition]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsLinkInContentDefinition]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (4000), @matchWholeLink BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsLinkInContentDefinition]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_IsPageNameMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPageNameMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsPageNameMatch]
(@source NVARCHAR (4000), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPageNameMatch]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_IsPagePropertiesMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsPagePropertiesMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch]
(@source XML, @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPagePropertiesMatch]
 '

GO
PRINT N'Creating [dbo].[FindAndReplace_IsTextContentMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FindAndReplace_IsTextContentMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[FindAndReplace_IsTextContentMatch]
(@source NVARCHAR (MAX), @searchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsTextContentMatch]
 '

GO
PRINT N'Creating [dbo].[NavFilter_GetQuery]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NavFilter_GetQuery]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[NavFilter_GetQuery]
(@queryId UNIQUEIDENTIFIER, @condition NVARCHAR (4000), @OrderByProperty NVARCHAR (4000), @SortOrder NVARCHAR (4000), @SiteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[NavFilter_GetQuery]
 '

GO
PRINT N'Creating [dbo].[Regex_GetMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[Regex_GetMatch]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (4000), @regexOptions INT)
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatch]
 '

GO
PRINT N'Creating [dbo].[Regex_IsMatch]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_IsMatch]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[Regex_IsMatch]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (4000), @regexOptions INT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_IsMatch]
 '

GO
PRINT N'Creating [dbo].[Regex_ReplaceMatches]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_ReplaceMatches]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[Regex_ReplaceMatches]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (4000), @regexOptions INT, @replacement NVARCHAR (4000))
RETURNS NVARCHAR (MAX)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_ReplaceMatches]
 '

GO
PRINT N'Creating [dbo].[RegexSelectAll]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RegexSelectAll]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[RegexSelectAll]
(@input NVARCHAR (4000), @pattern NVARCHAR (4000), @matchDelimiter NVARCHAR (4000))
RETURNS NVARCHAR (4000)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[RegexSearch].[RegexSelectAll]
 '

GO
PRINT N'Creating [dbo].[GetRelationTable]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRelationTable]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[GetRelationTable]
(@relationType NVARCHAR (4000), @objectId UNIQUEIDENTIFIER)
RETURNS 
     TABLE (
        [col1]  UNIQUEIDENTIFIER NULL,
        [col2]  UNIQUEIDENTIFIER NULL,
        [col3]  UNIQUEIDENTIFIER NULL,
        [col4]  UNIQUEIDENTIFIER NULL,
        [col5]  UNIQUEIDENTIFIER NULL,
        [col6]  UNIQUEIDENTIFIER NULL,
        [col7]  UNIQUEIDENTIFIER NULL,
        [col8]  UNIQUEIDENTIFIER NULL,
        [col9]  UNIQUEIDENTIFIER NULL,
        [col10] UNIQUEIDENTIFIER NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[GetRelationTable]
 '

GO
PRINT N'Creating [dbo].[Regex_GetMatches]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Regex_GetMatches]') AND (type = 'FS' OR type = 'FT'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[Regex_GetMatches]
(@source NVARCHAR (MAX), @regexPattern NVARCHAR (4000), @regexOptions INT)
RETURNS 
     TABLE (
        [Match] NVARCHAR (MAX) NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatches]
 '

GO
PRINT N'Creating [dbo].[CLRPageMap_SavePageDefinition]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMap_SavePageDefinition]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[CLRPageMap_SavePageDefinition]
@siteId UNIQUEIDENTIFIER, @parentNodeId UNIQUEIDENTIFIER, @CreatedBy UNIQUEIDENTIFIER, @ModifiedBy UNIQUEIDENTIFIER, @pageDefinitionXml XML, @Id UNIQUEIDENTIFIER OUTPUT, @publishPage BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMap_SavePageDefinition]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_AddContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_AddContainer]', 'PC') IS  NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[CLRPageMapNode_AddContainer]
@templateId NVARCHAR (4000), @containerIds NVARCHAR (4000), @modifiedBy UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_AddContainer]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_RemoveContainer]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_RemoveContainer]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer]
@templateId NVARCHAR (4000), @containerIds NVARCHAR (4000), @modifiedBy UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_RemoveContainer]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_Save]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Save]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[CLRPageMapNode_Save]
@siteId UNIQUEIDENTIFIER, @parentNodeId UNIQUEIDENTIFIER, @pageMapNode XML, @createdBy UNIQUEIDENTIFIER, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME, @id UNIQUEIDENTIFIER OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Save]
'

GO
PRINT N'Creating [dbo].[CLRPageMapNode_Update]...';


GO
IF OBJECT_ID(N'[dbo].[CLRPageMapNode_Update]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[CLRPageMapNode_Update]
@siteId UNIQUEIDENTIFIER, @pageMapNode XML, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Update]
'

GO
PRINT N'Creating [dbo].[CLSPageMapNode_AttachWorkflow]...';


GO
IF OBJECT_ID(N'[dbo].[CLSPageMapNode_AttachWorkflow]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow]
@siteId UNIQUEIDENTIFIER, @pageMapNodeId UNIQUEIDENTIFIER, @pageMapNodeWorkFlow XML, @propogate BIT, @inherit BIT, @appendToExistingWorkflows BIT, @modifiedBy UNIQUEIDENTIFIER, @modifiedDate DATETIME
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLSPageMapNode_AttachWorkflow]
'

GO
PRINT N'Creating [dbo].[Contact_GetAutoDistributionListContactCount]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_GetAutoDistributionListContactCount]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @MaxRecords INT, @TotalRecords INT OUTPUT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_GetAutoDistributionListContactCount]
'

GO
PRINT N'Creating [dbo].[Contact_SearchCLR]...';


GO
IF OBJECT_ID(N'[dbo].[Contact_SearchCLR]', 'PC') IS  NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[Contact_SearchCLR]
@xml XML, @ApplicationId UNIQUEIDENTIFIER, @MaxRecords INT, @TotalRecords INT OUTPUT, @includeAddress BIT, @ContactListId UNIQUEIDENTIFIER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_SearchCLR]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_Find]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Find]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[FindAndReplace_Find]
@siteId UNIQUEIDENTIFIER, @searchIn INT, @searchTerm NVARCHAR (4000), @htmlEncodedSearchTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Find]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_Replace]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_Replace]', 'PC') IS  NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[FindAndReplace_Replace]
@siteId UNIQUEIDENTIFIER, @objectIds XML, @searchTerm NVARCHAR (4000), @htmlEncodedSearchTerm NVARCHAR (4000), @replaceTerm NVARCHAR (4000), @htmlEncodedReplaceTerm NVARCHAR (4000), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Replace]
'

GO
PRINT N'Creating [dbo].[FindAndReplace_UpdateLinks]...';


GO
IF OBJECT_ID(N'[dbo].[FindAndReplace_UpdateLinks]', 'PC') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[FindAndReplace_UpdateLinks]
@siteId UNIQUEIDENTIFIER, @oldUrl NVARCHAR (4000), @newUrl NVARCHAR (4000), @matchWholeLink BIT
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_UpdateLinks]
'
GO
PRINT N'Update complete for assembly Bridgeline.CLRFunctions.';

GO