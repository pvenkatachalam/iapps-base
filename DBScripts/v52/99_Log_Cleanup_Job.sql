USE [msdb]
GO

IF  NOT EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'LGLog Backup Job')
BEGIN
	BEGIN TRANSACTION
	DECLARE @ReturnCode INT
	SELECT @ReturnCode = 0
	IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'iAPPS Maintenance' AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'iAPPS Maintenance'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	END

	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'LGLog Backup Job', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'No description available.', 
			@category_name=N'iAPPS Maintenance', 
			@owner_login_name=N'sa', @job_id = @jobId OUTPUT
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Move LGLog to History and Truncate', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, @subsystem=N'TSQL', 
			@command=N'DECLARE @DBName nvarchar(500)

	DECLARE LOG_CURSOR Cursor 
	FOR
	SELECT name FROM master.sys.databases WHERE name <> ''master'' AND state_desc LIKE ''online''
	OPEN LOG_CURSOR 
	FETCH NEXT FROM LOG_CURSOR INTO @DBName

	While (@@FETCH_STATUS <> -1)
	BEGIN
		DECLARE @Query nvarchar(max)
		
		SELECT @Query = ''
			IF EXISTS(SELECT * FROM ['' + @DBName + ''].sys.tables WHERE name = ''''LGLog'''')
				AND EXISTS(SELECT * FROM ['' + @DBName + ''].sys.tables WHERE name = ''''LGLogHistory'''')
			BEGIN
				INSERT INTO ['' + @DBName + ''].dbo.LGCategoryLogHistory
				SELECT * FROM ['' + @DBName + ''].dbo.LGCategoryLog

				TRUNCATE TABLE ['' + @DBName + ''].dbo.LGCategoryLog

				INSERT INTO ['' + @DBName + ''].dbo.LGExceptionLogHistory
				SELECT * FROM ['' + @DBName + ''].dbo.LGExceptionLog

				TRUNCATE TABLE ['' + @DBName + ''].dbo.LGExceptionLog

				INSERT INTO ['' + @DBName + ''].dbo.LGLogHistory
				SELECT * FROM ['' + @DBName + ''].dbo.LGLog

				TRUNCATE TABLE ['' + @DBName + ''].dbo.LGLog
			END''
		
		EXEC (@query)
		FETCH NEXT FROM LOG_CURSOR INTO @DBName 		
	END

	CLOSE LOG_CURSOR
	DEALLOCATE LOG_CURSOR

	', 
			@database_name=N'master', 
			@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Weekly', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20131119, 
		@active_end_date=99991231, 
		@active_start_time=0, 
		@active_end_time=235959, 
		@schedule_uid = N'790964c3-927a-4992-bfbf-8d567733d8db'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION
	GOTO EndSave
	QuitWithRollback:
		IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
	EndSave:
END
GO


