--insert attribute group items ; in some of the databases these values are missing
IF NOT EXISTS (SELECT 1 FROM [dbo].[ATAttributeGroup] WHERE Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
BEGIN
	INSERT INTO dbo.ATAttributeGroup
	        ( Id ,
	          Title ,
	          SiteId ,
	          IsSystem ,
	          CreatedDate ,
	          CreatedBy ,
	          ModifiedDate ,
	          ModifiedBy ,
	          Status
	        )
	VALUES  ( 'AB730D27-5CCD-49D9-B515-168290438E0A' , -- Id - uniqueidentifier
	          N'Product Properties' , -- Title - nvarchar(255)
	          '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' , -- SiteId - uniqueidentifier
	          1 , -- IsSystem - bit
	          '2013-10-22 16:27:49' , -- CreatedDate - datetime
	          '798613EE-7B85-4628-A6CC-E17EE80C09C5' , -- CreatedBy - uniqueidentifier
	          '2013-10-22 16:27:49' , -- ModifiedDate - datetime
	          NULL , -- ModifiedBy - uniqueidentifier
	          1  -- Status - int
	        )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[ATAttributeGroup] WHERE Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
BEGIN
	INSERT INTO dbo.ATAttributeGroup
	        ( Id ,
	          Title ,
	          SiteId ,
	          IsSystem ,
	          CreatedDate ,
	          CreatedBy ,
	          ModifiedDate ,
	          ModifiedBy ,
	          Status
	        )
	VALUES  ( 'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A' , -- Id - uniqueidentifier
	          N'Advanced Details' , -- Title - nvarchar(255)
	          '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' , -- SiteId - uniqueidentifier
	          1 , -- IsSystem - bit
	          '2013-10-22 16:27:49' , -- CreatedDate - datetime
	          '798613EE-7B85-4628-A6CC-E17EE80C09C5' , -- CreatedBy - uniqueidentifier
	          '2013-10-22 16:27:49' , -- ModifiedDate - datetime
	          NULL , -- ModifiedBy - uniqueidentifier
	          1  -- Status - int
	        )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[ATAttributeGroup] WHERE Id ='01614E00-86F8-4448-9A48-7DAA63171C38')
BEGIN
	INSERT INTO dbo.ATAttributeGroup
	        ( Id ,
	          Title ,
	          SiteId ,
	          IsSystem ,
	          CreatedDate ,
	          CreatedBy ,
	          ModifiedDate ,
	          ModifiedBy ,
	          Status
	        )
	VALUES  ( '01614E00-86F8-4448-9A48-7DAA63171C38' , -- Id - uniqueidentifier
	          N'Features' , -- Title - nvarchar(255)
	          '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' , -- SiteId - uniqueidentifier
	          1 , -- IsSystem - bit
	          '2013-10-22 16:27:49' , -- CreatedDate - datetime
	          '798613EE-7B85-4628-A6CC-E17EE80C09C5' , -- CreatedBy - uniqueidentifier
	          '2013-10-22 16:27:49' , -- ModifiedDate - datetime
	          NULL , -- ModifiedBy - uniqueidentifier
	          1  -- Status - int
	        )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[ATAttributeGroup] WHERE Id ='B23FFC1B-AB9D-4985-BEB4-A5F303B9EF41')
BEGIN
	INSERT INTO dbo.ATAttributeGroup
	        ( Id ,
	          Title ,
	          SiteId ,
	          IsSystem ,
	          CreatedDate ,
	          CreatedBy ,
	          ModifiedDate ,
	          ModifiedBy ,
	          Status
	        )
	VALUES  ( 'B23FFC1B-AB9D-4985-BEB4-A5F303B9EF41' , -- Id - uniqueidentifier
	          N'SKULevel Attributes' , -- Title - nvarchar(255)
	          '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' , -- SiteId - uniqueidentifier
	          1 , -- IsSystem - bit
	          '2013-10-22 16:27:49' , -- CreatedDate - datetime
	          '798613EE-7B85-4628-A6CC-E17EE80C09C5' , -- CreatedBy - uniqueidentifier
	          '2013-10-22 16:27:49' , -- ModifiedDate - datetime
	          NULL , -- ModifiedBy - uniqueidentifier
	          1  -- Status - int
	        )
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[ATAttribute] WHERE Id ='4EEF2AE9-6B5E-4819-9481-DBDBBAE03D38')
INSERT INTO [dbo].[ATAttribute]
           ([Id]
           ,[AttributeDataType]
           ,[AttributeDataTypeId]
           ,[AttributeGroupId]
           ,[Title]
           ,[Description]
           ,[Sequence]
           ,[Code]
           ,[IsFaceted]
           ,[IsSearched]
           ,[IsSystem]
           ,[IsDisplayed]
           ,[IsEnum]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[IsPersonalized])
     VALUES
           ('4EEF2AE9-6B5E-4819-9481-DBDBBAE03D38'
           	,'System.String'
           	,'5C41E8EB-AA6E-4D5F-A299-DC16695B92E8'	
           	,'AB730D27-5CCD-49D9-B515-168290438E0A'	
           	,'SKU Filter Attributes'
           	,'These Attributes are used in Multi-SKU Selection control'
           	,1
           	,NULL
           	,0
           	,0
           	,1
           	,0
           	,0
           	,'2013-08-06 16:15:09.297'
           	,'798613EE-7B85-4628-A6CC-E17EE80C09C5'	
           	,NULL
           	,NULL
           	,1
           	,0
           	)





DECLARE @MaxId INT	

IF NOT EXISTS (Select 1 from PTGateway Where Name='Authorize.Net')
BEGIN

SELECT @MaxId = MAX(Id)+1 FROM PTGateway

INSERT INTO PTGateway(Id,Name,Description,Sequence,DotNetProviderName)
Values(@MaxId,'Authorize.Net','Authorize.net',@MaxId,'AuthorizeNetGatewayProvider')

END


IF NOT EXISTS (Select 1 from PTGateway Where Name='TokenizedAuthorize.Net')
BEGIN

SELECT @MaxId = MAX(Id)+1 FROM PTGateway

INSERT INTO PTGateway(Id,Name,Description,Sequence,DotNetProviderName)
Values(@MaxId,'TokenizedAuthorize.Net','TokenizedAuthorize.Net',@MaxId,'TokenizedAuthorizeNetGateway')

END

IF NOT EXISTS (Select 1 from PTGateway Where Name='TokenizedCybersource')
BEGIN

SELECT @MaxId = MAX(Id)+1 FROM PTGateway

INSERT INTO PTGateway(Id,Name,Description,Sequence,DotNetProviderName)
Values(@MaxId,'TokenizedCybersource','Tokenized Cybersource',@MaxId,'TokenizedCybersourceGateway')

END

--THis is to update the existing settings on staging to reflect the new one. ONLY FOR STAGING
update PTGatewaySite set IsActive = 0 where GatewayId in (select Id from PTGateway where Name in ('Authorize.Net','TokenizedAuthorize.Net','TokenizedCybersource'))

INSERT INTO PTGatewaySite(GatewayId,SiteId,IsActive,PaymentTypeId)
Select  T.Id GatewayId,T.SiteId,0 IsActive,1 PaymentTypeId
from
(
Select G.Id,S.SiteId from PTGateway G
Cross Join (select Distinct SiteId from PTGatewaySite)S
Except
Select GatewayId,SiteId from PTGatewaySite
)T

