
IF COL_LENGTH(N'[dbo].[BLBlog]', N'ContentDefinitionId') IS NULL
ALTER TABLE [dbo].[BLBlog] ADD [ContentDefinitionId] [UNIQUEIDENTIFIER] NULL
GO

GO
PRINT N'Altering [dbo].[BLPost]'
GO
IF COL_LENGTH(N'[dbo].[BLPost]', N'PostContentId') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[PostContentId] [UNIQUEIDENTIFIER] NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'TitleTag') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[TitleTag] [NVARCHAR] (2000) NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'H1Tag') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[H1Tag] [NVARCHAR] (2000) NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'KeywordMetaData') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[KeywordMetaData] [NVARCHAR] (2000) NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'DescriptiveMetaData') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[DescriptiveMetaData] [NVARCHAR] (2000) NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'OtherMetaData') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[OtherMetaData] [NVARCHAR] (2000) NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'IsSticky') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[IsSticky] [BIT] NULL CONSTRAINT [DF__BLPost__IsSti__1EEF32DB] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[BLPost]', N'PublishDate') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[PublishDate] [DATETIME] NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'PublishCount') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[PublishCount] [INT] NULL CONSTRAINT [DF__BLPost__Publi__3BFB850B] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[BLPost]', N'WorkflowState') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[WorkflowState] [INT] NULL CONSTRAINT [DF__BLPost__Workf__18A3A71A] DEFAULT ((1))
IF COL_LENGTH(N'[dbo].[BLPost]', N'INWFFriendlyName') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[INWFFriendlyName] [NVARCHAR] (max) NULL
IF COL_LENGTH(N'[dbo].[BLPost]', N'StatusChangeDate') IS NULL
ALTER TABLE [dbo].[BLPost] ADD[StatusChangeDate] [DATETIME] NULL
GO
IF COL_LENGTH(N'[dbo].[BLPost]', N'PostXMLString') IS NOT NULL
ALTER TABLE [dbo].[BLPost] DROP COLUMN [PostXMLString]
IF COL_LENGTH(N'[dbo].[BLPost]', N'PostXSLTString') IS NOT NULL
ALTER TABLE [dbo].[BLPost] DROP COLUMN [PostXSLTString]


GO
PRINT N'Altering [dbo].[COContentStructure]...';


GO
IF COL_LENGTH(N'[dbo].[COContentStructure]', N'MicroSiteId') IS NOT NULL
ALTER TABLE [dbo].[COContentStructure] DROP COLUMN [MicroSiteId];


GO
IF COL_LENGTH(N'[dbo].[COContentStructure]', N'SourceDirId') IS NULL
ALTER TABLE [dbo].[COContentStructure]
    ADD [SourceDirId]                UNIQUEIDENTIFIER NULL,
        [AllowAccessInChildrenSites] BIT              DEFAULT ((0)) NOT NULL;


GO
PRINT N'Altering [dbo].[COFileStructure]...';


GO
IF COL_LENGTH(N'[dbo].[COFileStructure]', N'MicroSiteId') IS NOT NULL
ALTER TABLE [dbo].[COFileStructure] DROP COLUMN [MicroSiteId];


GO
IF COL_LENGTH(N'[dbo].[COFileStructure]', N'SourceDirId') IS NULL
ALTER TABLE [dbo].[COFileStructure]
    ADD [SourceDirId]                UNIQUEIDENTIFIER NULL,
        [AllowAccessInChildrenSites] BIT              DEFAULT ((0)) NOT NULL;


GO
PRINT N'Altering [dbo].[COImageStructure]...';


GO
IF COL_LENGTH(N'[dbo].[COImageStructure]', N'MicroSiteId') IS NOT NULL
ALTER TABLE [dbo].[COImageStructure] DROP COLUMN [MicroSiteId];


GO
IF COL_LENGTH(N'[dbo].[COImageStructure]', N'SourceDirId') IS NULL
ALTER TABLE [dbo].[COImageStructure]
    ADD [SourceDirId]                UNIQUEIDENTIFIER NULL,
        [AllowAccessInChildrenSites] BIT              DEFAULT ((0)) NOT NULL;


GO
PRINT N'Altering [dbo].[CPOrderCouponCode]...';


GO
IF COL_LENGTH(N'[dbo].[CPOrderCouponCode]', N'Id') IS NULL
ALTER TABLE [dbo].[CPOrderCouponCode]
    ADD [Id] UNIQUEIDENTIFIER DEFAULT newid() NOT NULL;


GO
PRINT N'Creating PK_CPOrderCouponCode...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE type=1 AND object_id = OBJECT_ID(N'[dbo].[CPOrderCouponCode]'))
ALTER TABLE [dbo].[CPOrderCouponCode]
    ADD CONSTRAINT [PK_CPOrderCouponCode] PRIMARY KEY CLUSTERED ([Id] ASC);


GO
PRINT N'Altering [dbo].[FFOrderShipmentItem]...';


GO
IF COL_LENGTH(N'[dbo].[FFOrderShipmentItem]', N'ExternalReferenceNumber') IS NULL
ALTER TABLE [dbo].[FFOrderShipmentItem]
    ADD [ExternalReferenceNumber] NVARCHAR (50) NULL;


GO
PRINT N'Altering [dbo].[Forms]...';


GO
IF COL_LENGTH(N'[dbo].[Forms]', N'ParentId') IS NULL
ALTER TABLE [dbo].[Forms]
    ADD [ParentId] UNIQUEIDENTIFIER NULL;


GO
PRINT N'Altering [dbo].[GLAddress]...';


GO
IF COL_LENGTH(N'[dbo].[GLAddress]', N'County') IS NULL
ALTER TABLE [dbo].[GLAddress]
    ADD [County] NVARCHAR (255) NULL;


GO
PRINT N'Altering [dbo].[OROrder]...';


GO
IF COL_LENGTH(N'[dbo].[OROrder]', N'RefundTotal') IS NULL
ALTER TABLE [dbo].[OROrder]
    ADD [RefundTotal] MONEY DEFAULT ((0)) NOT NULL;


GO
PRINT N'Altering [dbo].[OROrderItem]...';

GO

GO
IF COL_LENGTH(N'[dbo].[OROrderItem]', N'Discount') IS NOT NULL
BEGIN
--This is update the DISCOUNT column values that are null. This is a rare scenario; so kept it here.

DECLARE @sqlUpdate nvarchar(max) 
set @sqlUpdate = 'UPDATE dbo.OROrderItem SET Discount = 0.00 WHERE Discount IS NULL'

EXEC sys.sp_executesql @sqlUpdate

ALTER TABLE [dbo].[OROrderItem] ALTER COLUMN [Discount] MONEY NOT NULL;

END
ELSE 
ALTER TABLE [dbo].[OROrderItem] ADD  [Discount] MONEY NOT NULL DEFAULT(0.00);



GO
PRINT N'Altering [dbo].[OROrderShipping]...';


GO
IF COL_LENGTH(N'[dbo].[OROrderShipping]', N'IsHandlingIncluded') IS NULL
ALTER TABLE [dbo].[OROrderShipping]
    ADD [IsHandlingIncluded] BIT NULL;


GO
PRINT N'Starting rebuilding table [dbo].[PageDefinitionContainer]...';

GO
IF COL_LENGTH(N'[dbo].[PageDefinitionContainer]', N'ContainerName') IS NULL
ALTER TABLE [dbo].PageDefinitionContainer
    ADD [ContainerName] [NVARCHAR] (100) NULL;

GO
PRINT N'Creating [dbo].[PageDefinitionContainer].[IX_PageDefinitionContainer_PageDefinitionId]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE  name='IX_PageDefinitionContainer_PageDefinitionId' AND object_id = OBJECT_ID(N'[dbo].[PageDefinitionContainer]'))
CREATE NONCLUSTERED INDEX [IX_PageDefinitionContainer_PageDefinitionId]
    ON [dbo].[PageDefinitionContainer]([PageDefinitionId] ASC);


GO
PRINT N'Creating [dbo].[PageDefinitionContainer].[IX_PageDefinitionContainer_PageDefinitionSegmentId]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE  name='IX_PageDefinitionContainer_PageDefinitionSegmentId' AND object_id = OBJECT_ID(N'[dbo].[PageDefinitionContainer]'))
CREATE NONCLUSTERED INDEX [IX_PageDefinitionContainer_PageDefinitionSegmentId]
    ON [dbo].[PageDefinitionContainer]([PageDefinitionSegmentId] ASC);


GO
PRINT N'Creating [dbo].[PageDefinitionContainer].[PageDefinitionContainer_ContentTypeId]...';


GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE  name='PageDefinitionContainer_ContentTypeId' AND object_id = OBJECT_ID(N'[dbo].[PageDefinitionContainer]'))
CREATE NONCLUSTERED INDEX [PageDefinitionContainer_ContentTypeId]
    ON [dbo].[PageDefinitionContainer]([ContentTypeId] ASC)
    INCLUDE([PageDefinitionId], [ContainerId], [ContentId], [InWFContentId]);


GO
PRINT N'Altering [dbo].[PTPayment]...';


GO
IF COL_LENGTH(N'[dbo].[PTPayment]', N'IsRefund') IS NULL
ALTER TABLE [dbo].[PTPayment]
    ADD [IsRefund] TINYINT DEFAULT 0 NOT NULL;


GO
PRINT N'Altering [dbo].[PTPaymentCapture]...';


GO
IF COL_LENGTH(N'[dbo].[PTPaymentCapture]', N'TransactionId') IS NULL
ALTER TABLE [dbo].[PTPaymentCapture]
    ADD [TransactionId]          NVARCHAR (255)   NULL,
        [ParentPaymentCaptureId] UNIQUEIDENTIFIER NULL;


GO
PRINT N'Altering [dbo].[PTPaymentCreditCard]...';


GO
IF COL_LENGTH(N'[dbo].[PTPaymentCreditCard]', N'ExternalProfileId') IS NULL
ALTER TABLE [dbo].[PTPaymentCreditCard]
    ADD [ExternalProfileId] NVARCHAR (255) NULL;


GO
PRINT N'Altering [dbo].[SISite]...';


GO
IF COL_LENGTH(N'[dbo].[SISite]', N'ImportContentStructure') IS NULL
ALTER TABLE [dbo].[SISite]
    ADD [LftValue]               INT              CONSTRAINT [DF__SISite__LftValue__1F9386F4] DEFAULT ((1)) NOT NULL,
        [RgtValue]               INT              CONSTRAINT [DF__SISite__RgtValue__2087AB2D] DEFAULT ((2)) NOT NULL,
        [ImportContentStructure] BIT              NULL,
        [ImportFileStructure]    BIT              NULL,
        [ImportImageStructure]   BIT              NULL,
        [ImportFormsStructure]   BIT              NULL,
        [MasterSiteId]           UNIQUEIDENTIFIER NULL,
        [PrimarySiteUrl]         NVARCHAR (1024)  NULL;


GO
PRINT N'Altering [dbo].[SISiteDirectory]...';


GO
IF COL_LENGTH(N'[dbo].[SISiteDirectory]', N'SourceDirId') IS NULL
ALTER TABLE [dbo].[SISiteDirectory]
    ADD [SourceDirId] UNIQUEIDENTIFIER NULL;


GO
PRINT N'Altering [dbo].[SYRssChannel]...';


GO
IF COL_LENGTH(N'[dbo].[SYRssChannel]', N'ChannelType') IS NULL
ALTER TABLE [dbo].[SYRssChannel]
    ADD [ChannelType] INT NULL;


GO
PRINT N'Altering [dbo].[UploadContactData]...';

GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'TotalRecords') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD TotalRecords INT NULL;
GO
IF COL_LENGTH(N'[dbo].[UploadContactData]', N'DuplicateRecords') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD [DuplicateRecords] INT NULL;
GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'ErrorRecords') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD [ErrorRecords] INT NULL;
GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'ExistingRecords') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD ExistingRecords INT NULL;
GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'UpdatedContacts') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD [UpdatedContacts] INT NULL;
GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'CreatedBy') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD CreatedBy [uniqueidentifier] NULL
GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'CreatedDate') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD [CreatedDate] [datetime] NULL
GO

IF COL_LENGTH(N'[dbo].[UploadContactData]', N'ModifiedBy') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD ModifiedBy [uniqueidentifier] NULL
GO
IF COL_LENGTH(N'[dbo].[UploadContactData]', N'ModifiedDate') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD ModifiedDate [datetime] NULL
GO
IF COL_LENGTH(N'[dbo].[UploadContactData]', N'ErrorMessage') IS NULL
ALTER TABLE [dbo].[UploadContactData]
    ADD ErrorMessage [nvarchar](max) NULL;
GO

PRINT N'Altering [dbo].[USCommerceUserProfile]...';


GO
IF COL_LENGTH(N'[dbo].[USCommerceUserProfile]', N'ExternalId') IS NULL
ALTER TABLE [dbo].[USCommerceUserProfile]
    ADD [ExternalProfileId] NVARCHAR (255) NULL,
        [ExternalId]        NVARCHAR (255) NULL;


GO
PRINT N'Altering [dbo].[USUserPaymentInfo]...';


GO
IF COL_LENGTH(N'[dbo].[USUserPaymentInfo]', N'ExternalProfileId') IS NULL
ALTER TABLE [dbo].[USUserPaymentInfo]
    ADD [ExternalProfileId] NVARCHAR (255) NULL,
        [Nickname]          NVARCHAR (255) NULL;


GO
PRINT N'Altering [dbo].[WFWorkflow]...';


GO
IF COL_LENGTH(N'[dbo].[WFWorkflow]', N'IsShared') IS NULL
ALTER TABLE [dbo].[WFWorkflow]
    ADD [IsShared] BIT CONSTRAINT [DF_WFWorkflow_IsShared] DEFAULT ((0)) NOT NULL;
GO
IF COL_LENGTH(N'[dbo].[USSiteUser]', N'SessionId') IS NULL
ALTER TABLE [dbo].[USSiteUser]
    ADD [SessionId] VARCHAR(10) NULL
