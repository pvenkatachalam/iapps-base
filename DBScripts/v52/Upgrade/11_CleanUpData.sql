﻿-- Groups are not needed in HSStructure. Earlier it was inserted
DELETE FROM HSStructure WHERE ObjectTypeId = 15

IF OBJECT_ID(N'[dbo].[Membership_GetAllUsersInProduct]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetAllUsersInProduct]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetAllUsersInProduct_Count]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetAllUsersInProduct_Count]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetAllUsers_Count]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetAllUsers_Count]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetAllUsersForIndexing]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetAllUsersForIndexing]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetAllUsersWithProfile]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetAllUsersWithProfile]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetExpiredMembers]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetExpiredMembers]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetExpiredMembers_Count]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetExpiredMembers_Count]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetExpiredUsersWithProfile]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetExpiredUsersWithProfile]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetAllAnalyticsUsersForIndexing]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetAllAnalyticsUsersForIndexing]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetUsersFromXml]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Membership_GetUsersFromXml]    
GO

IF OBJECT_ID(N'[dbo].[User_GetAllUsersWithPaging]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[User_GetAllUsersWithPaging]    
GO

IF OBJECT_ID(N'[dbo].[User_GetExpiredUsersWithPagination]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[User_GetExpiredUsersWithPagination]    
GO
IF OBJECT_ID(N'[dbo].[User_GetUsersInRoleWithPagination]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[User_GetUsersInRoleWithPagination]    
GO

IF OBJECT_ID(N'[dbo].[Group_GetMembersInRole_Count]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[Group_GetMembersInRole_Count]    
GO

IF OBJECT_ID(N'[dbo].[User_GetUsersInGroupWithPaging]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[User_GetUsersInGroupWithPaging]    
GO

IF OBJECT_ID(N'[dbo].[Membership_GetUsersInGroup_Count]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[Membership_GetUsersInGroup_Count]    
GO
IF OBJECT_ID(N'[dbo].[GetAllParentsForRange]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[GetAllParentsForRange]    
GO