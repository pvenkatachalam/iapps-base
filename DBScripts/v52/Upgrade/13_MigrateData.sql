-- Updating the MasterSiteId
Update SISite SET MasterSiteId = case when ParentSiteId ='00000000-0000-0000-0000-000000000000' then Id else ParentSiteId end 
where MasterSiteId is null
GO
--Updating the Hierarchy for all the sites in the table
Declare @SiteId uniqueidentifier
DECLARE Site_cursor CURSOR FOR 
select Id from SISite Where Status =1 
and ParentSiteId = '00000000-0000-0000-0000-000000000000'

OPEN Site_cursor

FETCH NEXT FROM Site_cursor
INTO @SiteId

WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC RegenerateLftRgt @SiteId=@SiteId,@Key=8
	
	
	FETCH NEXT FROM Site_cursor
    INTO @SiteId
 END 
CLOSE Site_cursor;
DEALLOCATE Site_cursor;
GO

PRINT 'Making copy of Content,Image,File and Form library for all the variant sites'

GO
-- Migration library folders from V5.0 to V5.1
-- Migration script to create independent Content, Image, File and Forms Folder for varient sites.

Declare @SiteId uniqueidentifier, @ParentSiteId uniqueidentifier
Declare @Lft bigint, @Rgt bigint,@FormsRoot uniqueidentifier	
Declare @dRgt bigint
DECLARE Site_cursor CURSOR FOR 
select Id,ParentSiteId from SISite Where Status =1 
and ParentSiteId <> '00000000-0000-0000-0000-000000000000'

Declare @tmpTable table(Id uniqueidentifier,SourceId uniqueidentifier)
		Declare @tempHS table
			([Id] uniqueidentifier
           ,[Title] nvarchar(256)
           ,[ObjectTypeId] int
           ,[Keywords] nvarchar(1000)
           ,[ParentId]  uniqueidentifier
           ,[LftValue] bigint
           ,[RgtValue] bigint
           ,[SiteId]  uniqueidentifier
           ,[MicroSiteId]  uniqueidentifier)
			
OPEN Site_cursor

FETCH NEXT FROM Site_cursor
INTO @SiteId,@ParentSiteId

WHILE @@FETCH_STATUS = 0
BEGIN
Delete FROM @tmpTable
-- copy content directories
	IF NOT EXISTS(Select 1 from COContentStructure Where SiteId =@SiteId)
	BEGIN
			INSERT INTO @tmpTable(Id,SourceId)
			SELECT NEWID(),Id FROM COContentStructure Where SiteId=@ParentSiteId
			
			INSERT INTO [dbo].[COContentStructure]
					   ([Id]
					   ,[ParentId]
					   ,[LftValue]
					   ,[RgtValue]
					   ,[Title]
					   ,[Description]
					   ,[SiteId]
					   ,[Exists]
					   ,[Attributes]
					   ,[Size]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[PhysicalPath]
					   ,[VirtualPath]
					   ,[ThumbnailImagePath]
					   ,[FolderIconPath]
					   ,[Keywords]
					   ,[IsSystem]
					   ,[IsMarketierDir]
					   ,[SourceDirId]
					   )
				 SELECT T.Id
					   ,ISNULL(P.Id,@SiteId)
					   ,[LftValue]
					   ,[RgtValue]
					   ,[Title]
					   ,[Description]
					   ,@SiteId
					   ,[Exists]
					   ,[Attributes]
					   ,[Size]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[PhysicalPath]
					   ,[VirtualPath]
					   ,[ThumbnailImagePath]
					   ,[FolderIconPath]
					   ,[Keywords]
					   ,[IsSystem]
					   ,[IsMarketierDir]
					   ,T.[SourceId]
				FROM COContentStructure S 
				INNER JOIN @tmpTable T on S.Id = T.SourceId
				LEFT JOIN @tmpTable P on S.ParentId= P.SourceId


			-- update the parent id in the content table
			UPDATE C SET C.ParentId = T.Id
			FROM COContent C	
			INNER JOIN @tmpTable T ON C.ParentId = T.SourceId
			WHERE ApplicationId = @SiteId
			
			DECLARE @ObjectTypeParentId UNIQUEIDENTIFIER
			SET @ObjectTypeParentId = (SELECT TOP 1 Id FROM COContentStructure WHERE LftValue=1 AND SiteId=@SiteId )
			
			--insert the parent id of the Content library in the HSDefaultParent table
			IF (NOT EXISTS (SELECT TOP 1 1 FROM dbo.HSDefaultParent WHERE ParentId=@ObjectTypeParentId)  AND @ObjectTypeParentId IS NOT NULL)
			BEGIN
				INSERT INTO dbo.HSDefaultParent 
				        ( SiteId ,
				          ObjectTypeId ,
				          Title ,
				          ParentId
				        )
				VALUES  ( @SiteId , -- SiteId - uniqueidentifier
				          7 , -- ObjectTypeId - int
				          N'Content Library' , -- Title - nvarchar(256)
				          @ObjectTypeParentId  -- ParentId - uniqueidentifier
				        )
			END
			
			
	END
	
	Delete FROM @tmpTable
	-- copy image directories
	IF NOT EXISTS(Select 1 from COImageStructure Where SiteId =@SiteId)
	BEGIN
			INSERT INTO @tmpTable(Id,SourceId)
			SELECT NEWID(),Id FROM COImageStructure Where SiteId=@ParentSiteId
			
			INSERT INTO [dbo].[COImageStructure]
				   ([Id]
				   ,[ParentId]
				   ,[LftValue]
				   ,[RgtValue]
				   ,[Title]
				   ,[Description]
				   ,[SiteId]
				   ,[Exists]
				   ,[Attributes]
				   ,[Size]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate]
				   ,[PhysicalPath]
				   ,[VirtualPath]
				   ,[ThumbnailImagePath]
				   ,[FolderIconPath]
				   ,[Keywords]
				   ,[IsSystem]
				   
				   ,[InheritSecurityLevels]
				   ,[PropagateSecurityLevels]
				   ,[SourceDirId])
			 SELECT
				   T.[Id]
				   ,ISNULL(P.Id,@SiteId)
				   ,[LftValue]
				   ,[RgtValue]
				   ,[Title]
				   ,[Description]
				   ,@SiteId
				   ,[Exists]
				   ,[Attributes]
				   ,[Size]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate]
				   ,[PhysicalPath]
				   ,[VirtualPath]
				   ,[ThumbnailImagePath]
				   ,[FolderIconPath]
				   ,[Keywords]
				   ,[IsSystem]
				   
				   ,[InheritSecurityLevels]
				   ,[PropagateSecurityLevels]
				   ,T.[SourceId]
				FROM COImageStructure S 
				INNER JOIN @tmpTable T on S.Id = T.SourceId
				LEFT JOIN @tmpTable P on S.ParentId= P.SourceId

			-- update the parent id in the content table
			UPDATE C SET C.ParentId = T.Id
			FROM COContent C	
			INNER JOIN @tmpTable T ON C.ParentId = T.SourceId 
			WHERE ApplicationId = @SiteId
			
			
			
			SET @ObjectTypeParentId = (SELECT TOP 1 Id FROM [COImageStructure] Where LftValue=1 AND SiteId=@SiteId)
			
			--insert the parent id of the Content library in the HSDefaultParent table
			IF (NOT EXISTS (SELECT TOP 1 1 FROM dbo.HSDefaultParent WHERE ParentId=@ObjectTypeParentId) AND @ObjectTypeParentId IS NOT NULL)
			BEGIN
				INSERT INTO dbo.HSDefaultParent 
				        ( SiteId ,
				          ObjectTypeId ,
				          Title ,
				          ParentId
				        )
				VALUES  ( @SiteId , -- SiteId - uniqueidentifier
				          33 , -- ObjectTypeId - int
				          N'Image Library' , -- Title - nvarchar(256)
				          @ObjectTypeParentId  -- ParentId - uniqueidentifier
				        )
			END
			
	END
	Delete FROM @tmpTable
	-- copy file directories
	IF NOT EXISTS(Select 1 from COFileStructure Where SiteId =@SiteId)
	BEGIN
			INSERT INTO @tmpTable(Id,SourceId)
			SELECT NEWID(),Id FROM COFileStructure Where SiteId=@ParentSiteId
			
			INSERT INTO [dbo].[COFileStructure]
			   ([Id]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[Title]
			   ,[Description]
			   ,[SiteId]
			   ,[Exists]
			   ,[Attributes]
			   ,[Size]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PhysicalPath]
			   ,[VirtualPath]
			   ,[ThumbnailImagePath]
			   ,[FolderIconPath]
			   ,[Keywords]
			   ,[IsSystem]
			   
			   ,[InheritSecurityLevels]
			   ,[PropagateSecurityLevels]
			   ,[SourceDirId])
			 SELECT
			   T.[Id]
			  ,ISNULL(P.Id,@SiteId)
			   ,[LftValue]
			   ,[RgtValue]
			   ,[Title]
			   ,[Description]
			   ,@SiteId
			   ,[Exists]
			   ,[Attributes]
			   ,[Size]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PhysicalPath]
			   ,[VirtualPath]
			   ,[ThumbnailImagePath]
			   ,[FolderIconPath]
			   ,[Keywords]
			   ,[IsSystem]
			   
			   ,[InheritSecurityLevels]
			   ,[PropagateSecurityLevels]
			   ,T.[SourceId]
				FROM COFileStructure S 
				INNER JOIN @tmpTable T on S.Id = T.SourceId
				LEFT JOIN @tmpTable P on S.ParentId= P.SourceId

			-- update the parent id in the content table
			UPDATE C SET C.ParentId = T.Id
			FROM COContent C	
			INNER JOIN @tmpTable T ON C.ParentId = T.SourceId
			WHERE ApplicationId = @SiteId
			
			
			SET @ObjectTypeParentId = (SELECT TOP 1 Id FROM [COFileStructure] Where LftValue=1 AND SiteId=@SiteId)
			
			--insert the parent id of the Content library in the HSDefaultParent table
			IF (NOT EXISTS (SELECT TOP 1 1 FROM dbo.HSDefaultParent WHERE ParentId=@ObjectTypeParentId) AND @ObjectTypeParentId IS NOT NULL)
			BEGIN
				INSERT INTO dbo.HSDefaultParent 
				        ( SiteId ,
				          ObjectTypeId ,
				          Title ,
				          ParentId
				        )
				VALUES  ( @SiteId , -- SiteId - uniqueidentifier
				          9 , -- ObjectTypeId - int
				          N'File Library' , -- Title - nvarchar(256)
				          @ObjectTypeParentId  -- ParentId - uniqueidentifier
				        )
			END
			
	END
	Delete FROM @tmpTable
-- copy forms directories
	IF NOT EXISTS(Select 1 from SISiteDirectory Where ApplicationId =@SiteId and ObjectTypeId=38)
	BEGIN
		Declare @sHId uniqueidentifier,@dHId uniqueidentifier
		Declare @Title nvarchar(256)
			INSERT INTO @tmpTable(Id,SourceId)
			SELECT NEWID(),Id  from SISiteDirectory Where ApplicationId =@ParentSiteId and ObjectTypeId=38
			
			INSERT INTO [dbo].[SISiteDirectory]
				   ([Id]
				   ,[ApplicationId]
				   ,[Title]
				   ,[Description]
				   ,[ObjectTypeId]
				   ,[PhysicalPath]
				   ,[VirtualPath]
				   ,[Attributes]
				   ,[ThumbnailImagePath]
				   ,[FolderIconPath]
				   ,[Exists]
				   ,[Size]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate]
				   ,[Status]
				   ,[IsSystem]
				   ,[IsMarketierDir])
			 SELECT
			   T.[Id]
					,@SiteId
				   ,[Title]
				   ,[Description]
				   ,[ObjectTypeId]
				   ,[PhysicalPath]
				   ,[VirtualPath]
				   ,[Attributes]
				   ,[ThumbnailImagePath]
				   ,[FolderIconPath]
				   ,[Exists]
				   ,[Size]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate]
				   ,[Status]
				   ,[IsSystem]
				   ,[IsMarketierDir]
				FROM SISiteDirectory S 
				INNER JOIN @tmpTable T on S.Id = T.SourceId 
										
			
		 	select top 1 @Lft=LftValue,@Rgt=RgtValue,@FormsRoot=S.Id 
		 	from HSStructure S 
			INNER JOIN @tmpTable T on S.Id = T.SourceId	
			Where SiteId=@ParentSiteId
			Order By LftValue
				
			select @dRgt = max(RgtValue) FROM HSStructure
			Where SiteId=@SiteId
			
			-- Create space 
			
			Update HSStructure SET LftValue = CASE WHEN LftValue>@dRgt then LftValue + (@Rgt -@Lft)+1 ELSE LftValue END,
									RgtValue = CASE WHEN RgtValue>=@dRgt then RgtValue + (@Rgt -@Lft) +1 ELSE RgtValue END
			Where SiteId=@SiteId
			
	
			Delete from @tempHS
			
			INSERT INTO @tempHS
           ([Id]
           ,[Title]
           ,[ObjectTypeId]
           ,[Keywords]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[MicroSiteId])
           SELECT
           T.[Id]
           ,[Title]
           ,[ObjectTypeId]
           ,[Keywords]
           ,case when [ParentId] = @ParentSiteId then @SiteId else ParentId end
           ,@dRgt + LftValue - @Lft 
           ,@dRgt + RgtValue - @Lft 
           ,@SiteId
           ,[MicroSiteId]
			FROM HSStructure S
			Inner JOIN @tmpTable T on S.Id = T.SourceId		
			Where LftValue Between @Lft and @Rgt 
			and SiteId = @ParentSiteId
			and ObjectTypeId=6
			
			
			
			
			Update H SET H.ParentId = T.Id
			FROM @tempHS H 
			INNER JOIN @tmpTable T on H.ParentId = T.SourceId
			
			
			
			INSERT INTO HSStructure
           ([Id]
           ,[Title]
           ,[ObjectTypeId]
           ,[Keywords]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[MicroSiteId])
           Select 
           [Id]
           ,[Title]
           ,[ObjectTypeId]
           ,[Keywords]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[MicroSiteId]
           FROM
           @tempHS
			
		           
			Declare @FormId uniqueidentifier
			DECLARE forms_cursor CURSOR FOR 
			select Id,Title from Forms where ApplicationId = @SiteId
			
			OPEN forms_cursor
			FETCH NEXT FROM forms_cursor INTO @FormId,@Title


			WHILE @@FETCH_STATUS = 0
			BEGIN

			select top 1 @sHId = ParentId from HSStructure Where Id=@FormId
			select top 1 @dHId = Id From @tmpTable Where SourceId = @sHId
			IF @dHId is not null
			BEGIN
				EXEC HierarchyNode_DeleteNode @FormId
				EXEC HierarchyNode_Save @Id=@FormId OUTPUT,@Title =@Title, @ParentId=@dHId, @Keywords=null,@ObjectTypeId=38,@ApplicationId=@SiteId
			END
				
				FETCH NEXT FROM forms_cursor INTO @FormId,@Title
			 END

			CLOSE forms_cursor
			DEALLOCATE forms_cursor

	END

    FETCH NEXT FROM Site_cursor
    INTO @SiteId,@ParentSiteId
 END 
CLOSE Site_cursor;
DEALLOCATE Site_cursor;

GO

PRINT 'Updating the Site table indicating that libraries are MOVED for variant sites'
GO
--Updates the site table indicating that libraries are imported as we are making copy for the variant sites
UPDATE dbo.SISite 
SET ImportContentStructure=1 ,
ImportImageStructure=1,
ImportFileStructure=1,
ImportFormsStructure=1 WHERE  Status =1 
and ParentSiteId <> '00000000-0000-0000-0000-000000000000'

GO