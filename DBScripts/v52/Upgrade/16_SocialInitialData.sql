﻿-- Add Social Default Data
DECLARE @SocialProductId uniqueidentifier
SET @SocialProductId ='CED1F2B6-A3FF-4B82-A856-1583FA811906'

IF NOT EXISTS (SELECT * FROM iAppsProductSuite WHERE Id = @SocialProductId)
	INSERT [dbo].[iAppsProductSuite] ([Id], [Title], [Description], [Url], [ProductVersion])  
	VALUES (@SocialProductId, N'Social', N'iAPPS Social', N'/SocialAdmin', N'5.1.0.0')

DECLARE @User uniqueidentifier
SET @User = 'B10CB05D-7000-49AD-B115-872C19F5700B' --This has to be modified to InstallAdmin

Declare @Now datetime
SET @Now = GETDATE()

DECLARE @MasterSiteId uniqueidentifier
SET @MasterSiteId = '8039ce09-e7da-47e1-bcec-df96b5e411f4'

DECLARE @SiteId uniqueidentifier
SET @SiteId = '8039ce09-e7da-47e1-bcec-df96b5e411f4'

DECLARE @ESRoleId int
SET @ESRoleId = (SELECT TOP 1 Id FROM USRoles WHERE Name = 'EngagementSpecialist')
IF @ESRoleId IS NULL
BEGIN
	SELECT @ESRoleId = MAX(Id) + 1 FROM USRoles
	INSERT INTO USRoles (ApplicationId,Id,[Name],Description,IsGlobal) 
	VALUES (@SocialProductId, @ESRoleId, 'EngagementSpecialist','ES', 1)
END

DECLARE @ESGroupId uniqueidentifier
SET @ESGroupId = (SELECT TOP 1 Id FROM USGroup WHERE Title = 'Engagement Specialist')
IF @ESGroupId IS NULL
BEGIN
	EXEC Group_Save  @SocialProductId,
		@ApplicationId = @SocialProductId, 
		@Id = @ESGroupId output,
		@Title = 'Engagement Specialist',
		@Description = 'ES',
		@CreatedBy = @User,
		@ModifiedBy=NULL,
		@ExpiryDate=NULL,
		@Status=NULL,
		@GroupType=1	
END

IF NOT EXISTS (SELECT * FROM USMemberRoles WHERE MemberId = @ESGroupId AND RoleId = @ESRoleId)
	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
	Values(@SocialProductId, @MasterSiteId, @ESGroupId, 2, @ESRoleId, @SocialProductId, 1, 1)	

DECLARE @SocialAdminUserId uniqueidentifier 
SELECT @SocialAdminUserId = (SELECT TOP 1 Id FROM USUser Where UserName like 'SocialAdmin')
IF @SocialAdminUserId IS NULL
	EXEC Membership_CreateUser @SocialProductId,@ApplicationId=@SiteId,@UserName='SocialAdmin',
        @Password='pass!@#',@PasswordSalt=NULL,@Email='SocialAdmin@xyz.com',@PasswordQuestion=NULL,
        @PasswordAnswer=NULL,@IsApproved=1,@UniqueEmail=0,@PasswordFormat=0,@CreatedBy=@User,
        @IsSystemUser=1,@UserProfile=NULL,@UserId=@SocialAdminUserId output,@CreatedDate=NULL,
        @ExpiryDate='9999-12-31 23:59:59.997', @FirstName='SocialAdmin', @LastName='SocialAdmin',
        @ReportStartDate=@Now, @ReportEndDate=@Now, @ReportRangeSelection='MonthToDate'

IF NOT EXISTS (SELECT * FROM USSiteUser WHERE UserId = @SocialAdminUserId AND SiteId = @SiteId AND ProductId = @SocialProductId)
	INSERT INTO USSiteUser (SiteId, UserId, IsSystemUser, ProductId)
	values (@SiteId,@SocialAdminUserId, 1, @SocialProductId )

IF NOT EXISTS (SELECT * FROM USMemberGroup Where MemberId = @SocialAdminUserId AND ApplicationId = @SiteId AND GroupId = @ESGroupId)
	INSERT INTO USMemberGroup(MemberId,MemberType,GroupId, ApplicationId) 
	VALUES (@SocialAdminUserId, 1 ,@ESGroupId ,@SiteId) -- global group	

DECLARE @BloggerRoleId int
SET @BloggerRoleId = (SELECT TOP 1 Id FROM USRoles WHERE Name = 'Blogger')
IF @BloggerRoleId IS NULL
BEGIN
	SELECT @BloggerRoleId = MAX(Id) + 1 FROM USRoles
	INSERT INTO USRoles (ApplicationId,Id,[Name],Description,IsGlobal) 
	VALUES (@SocialProductId, @BloggerRoleId, 'Blogger','Blogger', 1)
END

DECLARE @BloggerGroupId uniqueidentifier
SET @BloggerGroupId = (SELECT TOP 1 Id FROM USGroup WHERE Title = 'Blogger')
IF @BloggerGroupId IS NULL
BEGIN
	EXEC Group_Save  @SocialProductId,
		@ApplicationId = @SocialProductId, 
		@Id = @BloggerGroupId output,
		@Title = 'Blogger',
		@Description = 'Blogger',
		@CreatedBy = @User,
		@ModifiedBy=NULL,
		@ExpiryDate=NULL,
		@Status=NULL,
		@GroupType=1	
END

IF NOT EXISTS (SELECT * FROM USMemberRoles WHERE MemberId = @BloggerGroupId AND RoleId = @BloggerRoleId)
	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
	Values(@SocialProductId, @MasterSiteId, @BloggerGroupId, 2, @BloggerRoleId, @SocialProductId, 1, 1)	

IF NOT EXISTS (SELECT * FROM SMSocialMedia Where Provider = 'facebook')
	INSERT INTO SMSocialMedia Values(1, 'Facebook', 'facebook', 1)
IF NOT EXISTS (SELECT * FROM SMSocialMedia Where Provider = 'twitter')
	INSERT INTO SMSocialMedia Values(2, 'Twitter', 'twitter', 1)
IF NOT EXISTS (SELECT * FROM SMSocialMedia Where Provider = 'linkedin')
	INSERT INTO SMSocialMedia Values(3, 'LinkedIn', 'linkedin', 1)

