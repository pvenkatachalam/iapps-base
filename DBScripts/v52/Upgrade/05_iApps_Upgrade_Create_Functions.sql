/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:36:11 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT 'Dropping All new functions'
GO
IF OBJECT_ID('[dbo].[GetSiteDraftStatus]') IS NOT NULL DROP FUNCTION[dbo].[GetSiteDraftStatus]
IF OBJECT_ID('[dbo].[GetSiblingSites]') IS NOT NULL DROP FUNCTION[dbo].[GetSiblingSites]
IF OBJECT_ID('[dbo].[GetAncestorSitesForSystemUser]') IS NOT NULL DROP FUNCTION[dbo].[GetAncestorSitesForSystemUser]
IF OBJECT_ID('[dbo].[GetAncestorSites]') IS NOT NULL DROP FUNCTION[dbo].[GetAncestorSites]
IF OBJECT_ID('[dbo].[User_GetUsersInRole]') IS NOT NULL DROP FUNCTION[dbo].[User_GetUsersInRole]
IF OBJECT_ID('[dbo].[User_GetUsersInGroup]') IS NOT NULL DROP FUNCTION[dbo].[User_GetUsersInGroup]
IF OBJECT_ID('[dbo].[GetVariantSitesHavingPermission]') IS NOT NULL DROP FUNCTION[dbo].[GetVariantSitesHavingPermission]
IF OBJECT_ID('[dbo].[GetSiteHierarchyPath]') IS NOT NULL DROP FUNCTION[dbo].[GetSiteHierarchyPath]
IF OBJECT_ID('[dbo].[Order_GetRefundTotal]') IS NOT NULL DROP FUNCTION[dbo].[Order_GetRefundTotal]
IF OBJECT_ID('[dbo].[GetVariantDirectoryId]') IS NOT NULL DROP FUNCTION[dbo].[GetVariantDirectoryId]
IF OBJECT_ID('[dbo].[GetDomainLoweredUrl]') IS NOT NULL DROP FUNCTION[dbo].[GetDomainLoweredUrl]
IF OBJECT_ID('[dbo].[GetVariantSitesForUser]') IS NOT NULL DROP FUNCTION[dbo].[GetVariantSitesForUser]
IF OBJECT_ID('[dbo].[GetChildSites]') IS NOT NULL DROP FUNCTION[dbo].[GetChildSites]

GO
PRINT N'Creating [dbo].[GetSiteDraftStatus]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSiteDraftStatus]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetSiteDraftStatus
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Karthick Nagarajan
-- Create date: 6/09/2013
-- Description:	returns the draft status of a site
--============================================

CREATE Function [dbo].[GetSiteDraftStatus]()
RETURNS int
AS
begin
return 2
end


'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT 'Creating [dbo].[GetChildSites]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetChildSites]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[GetChildSites](@SiteId uniqueidentifier)  
  
RETURNS   
@SITES TABLE   
(  
 [Id]   uniqueidentifier         
 ,[Title]  nvarchar(600)      
 ,[Description] nvarchar(600)       
 ,[URL]  xml      
 ,[VirtualPath]  nvarchar(4000)      
 ,[ParentSiteId] uniqueidentifier       
 ,[Type]      int  
 ,[Keywords]  nvarchar(4000)     
 ,[CreatedBy] uniqueidentifier       
 ,[CreatedDate] datetime       
 ,[ModifiedBy]  uniqueidentifier      
 ,[ModifiedDate] datetime       
 ,[Status]  int      
 ,[LftValue]   int     
 ,[RgtValue]  int      
 ,MasterSiteId uniqueidentifier  
 ,CreatedByFullName nvarchar(4000)  
 ,ModifiedByFullName nvarchar(4000)  
)  
AS  
BEGIN  
  
DECLARE @DeleteStatus int        
SET @DeleteStatus = dbo.GetDeleteStatus()   
  
DECLARE @DraftStatus int  
SET @DraftStatus = dbo.GetSiteDraftStatus()   
    
  
 INSERT INTO @sites   
 SELECT  [Id]        
 ,[Title]        
 ,[Description]        
 ,[URL]        
 ,[VirtualPath]        
 ,[ParentSiteId]        
 ,[Type]        
 ,[Keywords]        
 ,[CreatedBy]        
 ,[CreatedDate]        
 ,[ModifiedBy]        
 ,[ModifiedDate]        
 ,[Status]        
 ,[LftValue]        
 ,[RgtValue]        
 ,MasterSiteId  
 ,CU.UserFullName CreatedByFullName        
 ,MU.UserFullName ModifiedByFullName             
 FROM SISITE S    
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy          
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy  
 WHERE ParentSiteId = @SiteId  
 AND STATUS NOT IN(@DeleteStatus,@DraftStatus)   
  
  
RETURN  
END  '
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Creating [dbo].[GetSiblingSites]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSiblingSites]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetSiblingSites
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Karthick Nagarajan
-- Create date: 9/06/2013
-- Description:	Get all the sibling sites

--- =======================================================


CREATE FUNCTION [dbo].[GetSiblingSites](@SITEID uniqueidentifier)

RETURNS 
@SITES TABLE 
(
	[Id]   uniqueidentifier       
	,[Title]  nvarchar(600)    
	,[Description] nvarchar(600)     
	,[URL]  xml    
	,[VirtualPath]  nvarchar(4000)    
	,[ParentSiteId] uniqueidentifier     
	,[Type]      int
	,[Keywords]  nvarchar(4000)   
	,[CreatedBy] uniqueidentifier     
	,[CreatedDate] datetime     
	,[ModifiedBy]  uniqueidentifier    
	,[ModifiedDate] datetime     
	,[Status]  int    
	,[LftValue]   int   
	,[RgtValue]  int    
	,MasterSiteId uniqueidentifier
	,CreatedByFullName nvarchar(4000)
	,ModifiedByFullName nvarchar(4000)
)
AS
BEGIN

DECLARE @DeleteStatus int      
SET @DeleteStatus = dbo.GetDeleteStatus() 

DECLARE @DraftStatus int
SET @DraftStatus = dbo.GetSiteDraftStatus() 
  
DECLARE @PARENTID UNIQUEIDENTIFIER;
SELECT @PARENTID = PARENTSITEID FROM SISITE WHERE ID = @SITEID

IF @PARENTID != dbo.GetEmptyGuid() AND 
(SELECT TOP 1 ParentSiteId FROM SISite WHERE Id = @PARENTID) != dbo.GetEmptyGuid()
BEGIN
	insert into @sites	
	SELECT  [Id]      
	,[Title]      
	,[Description]      
	,[URL]      
	,[VirtualPath]      
	,[ParentSiteId]      
	,[Type]      
	,[Keywords]      
	,[CreatedBy]      
	,[CreatedDate]      
	,[ModifiedBy]      
	,[ModifiedDate]      
	,[Status]      
	,[LftValue]      
	,[RgtValue]      
	,MasterSiteId
	,CU.UserFullName CreatedByFullName      
	,MU.UserFullName ModifiedByFullName    	 	    
	FROM SISITE S  
	LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy        
	LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy
	WHERE (PARENTSITEID = @PARENTID) AND ID != @SiteId
	AND STATUS NOT IN(@DeleteStatus,@DraftStatus) 
END

RETURN
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetAncestorSitesForSystemUser]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAncestorSitesForSystemUser]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetAncestorSitesForSystemUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Vaibhav Sinha
-- Create date: 7/10/2013
-- Description:	Get all the ancestor site of a variant site
-- CHANGE LOG
---------------------------------------
-- Modified By: Sankar Raj
-- Date :  
-- Reason : To include the sibiling site access functionality 

-- =============================================
--select * from GetAncestorSites(''CDD38906-6071-4DD8-9FB6-508C38848C61'')

CREATE FUNCTION [dbo].[GetAncestorSitesForSystemUser](@ApplicationId uniqueidentifier, @IsSystemUser bit = null, @IsShared bit = null)

RETURNS 
@Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	IF(@IsSystemUser IS NULL)
		SET @IsSystemUser = 1

	IF(@IsShared IS NULL)
		SET @IsShared = 0
			
	IF @ApplicationId is NOT NULL
	BEGIN
	Declare @ImportAllAdminPermission bit
	SELECT @ImportAllAdminPermission = ImportAllAdminPermission FROM SISite where Id = @ApplicationId
	IF((@ImportAllAdminPermission = 1 AND @IsSystemUser = 1) OR @IsShared = 1)
	BEGIN
	
		DECLARE @AllowSiblingAccess BIT
		
		SELECT @AllowSiblingAccess = [VALUE] 
		FROM STSiteSetting SS JOIN STSettingType ST 
				ON ST.Id = SS.SettingTypeId 
					AND ST.Name=''AllowSiblingSiteAccess''

		DECLARE @PARENTID UNIQUEIDENTIFIER;    
		SELECT @PARENTID = PARENTSITEID FROM SISITE WHERE ID = @ApplicationId


		;WITH siteAncestorHierarchy AS (
			SELECT [Id]
					,[ParentSiteId]
					  FROM SISite S
					  where Id = @ApplicationId
			UNION ALL 
			SELECT S1.[Id]
					  ,S1.[ParentSiteId]
					  FROM SISite S1 
					INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId 
					WHERE S1.Status != 3
			)

				INSERT INTO @Sites 
				SELECT [Id]
				FROM siteAncestorHierarchy S1
				--where Id != @ApplicationId
			
			IF (@AllowSiblingAccess = 1)
			BEGIN
				INSERT INTO @Sites
				SELECT [ID] SITEID FROM [GetSiblingSites](@ApplicationId)
			END
	END
	ELSE
		BEGIN
			INSERT INTO @Sites 
					SELECT @ApplicationId
		END	
	END
	
	RETURN 
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetAncestorSites]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAncestorSites]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetAncestorSites
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Vaibhav Sinha
-- Create date: 7/10/2013
-- Description:	Get all the ancestor site of a variant site
-- =============================================
--select * from GetAncestorSites(''CDD38906-6071-4DD8-9FB6-508C38848C61'')
CREATE FUNCTION [dbo].[GetAncestorSites](@ApplicationId uniqueidentifier)

RETURNS 
@Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	INSERT INTO @Sites Select SiteId from dbo.GetAncestorSitesForSystemUser(@ApplicationId,null,0)	
	RETURN 
	
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[User_GetUsersInRole]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_GetUsersInRole]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[User_GetUsersInRole]      
(      
 @Id   int,      
 @MemberType smallint,      
 @ApplicationId uniqueidentifier = NULL,      
 @IsSystemUser  bit = 0,      
 @ObjectTypeId  int=null,      
 @ProductId  uniqueidentifier = NULL      
      
)      
RETURNS @ResultTable TABLE       
(      
 Id      uniqueidentifier,      
 UserName    nvarchar(256),        
 Email     nvarchar(512),      
 PasswordQuestion  nvarchar(512),        
 IsApproved    bit,      
 CreatedDate    datetime,      
 LastLoginDate   datetime,      
 LastActivityDate  datetime,      
 LastPasswordChangedDate datetime,      
 IsLockedOut    bit,      
 LastLockoutDate   datetime,    
 FirstName    nvarchar(256),    
 LastName    nvarchar(256),    
 MiddleName    nvarchar(256) ,    
 ExpiryDate    datetime ,    
 EmailNotification  bit ,    
 TimeZone    nvarchar(100),    
 ReportRangeSelection varchar(50),    
 ReportStartDate   datetime ,    
 ReportEndDate   datetime,     
 BirthDate    datetime,    
 CompanyName    nvarchar(1024),    
 Gender     nvarchar(50),    
 HomePhone    varchar(50),    
 MobilePhone    varchar(50),    
 OtherPhone    varchar(50),    
 ImageId     uniqueidentifier,  
 Status     int,
 ProductSuite   varchar(500)    
)      
AS      
BEGIN     
   
 DECLARE @tbUserIds Table (Id uniqueidentifier)  
 INSERT INTO @tbUserIds  
    SELECT DISTINCT G.MemberId   
 FROM USMemberGroup G  
  INNER JOIN USMemberRoles R ON G.GroupId = R.MemberId  
   AND R.RoleId = @Id AND R.MemberType = 2  
 WHERE  
  (@ObjectTypeId IS NULL OR R.ObjectTypeId = @ObjectTypeId) AND  
  (@ProductId IS NULL OR R.ProductId = @ProductId) AND  
  (@ApplicationId IS NULL OR G.ApplicationId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0)))   
  
 UNION  
  
 SELECT DISTINCT U.Id  
 FROM dbo.USUser U  
  INNER JOIN dbo.USMemberRoles R ON R.MemberId = U.Id   
   AND R.RoleId = @Id AND R.MemberType = 1  
 WHERE   
  (@ObjectTypeId IS NULL OR R.ObjectTypeId = @ObjectTypeId)   
  
 DECLARE @tbUser TYUser  
 INSERT INTO @tbUser  
 SELECT DISTINCT   
  U.Id,  
  U.UserName,  
  M.Email,   
  M.PasswordQuestion,    
  M.IsApproved,  
  U.CreatedDate,  
  M.LastLoginDate,   
  U.LastActivityDate,  
  M.LastPasswordChangedDate,   
  M.IsLockedOut,  
  M.LastLockoutDate,  
  U.FirstName,  
  U.LastName,  
  U.MiddleName,  
  U.ExpiryDate,  
  U.EmailNotification,  
  U.TimeZone,  
  U.ReportRangeSelection,  
  U.ReportStartDate,  
  U.ReportEndDate,  
  U.BirthDate,  
  U.CompanyName,  
  U.Gender,  
  U.HomePhone,  
  U.MobilePhone,  
  U.OtherPhone,  
  U.ImageId,  
  U.Status,
  I.Title  
 FROM dbo.USUser U  
  INNER JOIN @tbUserIds T ON U.Id = T.Id  
  INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
  INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
   (@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser) AND  
   (@ApplicationId IS NULL OR  
    S.SiteId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0))   
   )
   INNER JOIN iAppsProductSuite I ON I.Id = S.ProductId   
 WHERE   
  U.ExpiryDate IS NULL OR U.ExpiryDate >= GETUTCDATE() AND  
  M.IsApproved = 1 AND M.IsLockedOut = 0 AND U.Status != 3 AND  
  (@ProductId IS NULL OR S.ProductId = @ProductId)  
  
 INSERT INTO @ResultTable  
 SELECT * FROM @tbUser  
  
 RETURN     
END 

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[User_GetUsersInGroup]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User_GetUsersInGroup]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'
CREATE FUNCTION [dbo].[User_GetUsersInGroup]  
(  
 @ProductId    uniqueidentifier = NULL,   
 @ApplicationId   uniqueidentifier,  
 @Id            uniqueidentifier,   -- GroupId  
 @MemberType    smallint,  
 @MemberId    uniqueidentifier = NULL, -- Either userId or groupid.  
 @IsSystemUser   bit = 0  
)  
RETURNS @ResultTable TABLE       
(      
 Id      uniqueidentifier,      
 UserName    nvarchar(256),        
 Email     nvarchar(512),      
 PasswordQuestion  nvarchar(512),        
 IsApproved    bit,      
 CreatedDate    datetime,      
 LastLoginDate   datetime,      
 LastActivityDate  datetime,      
 LastPasswordChangedDate datetime,      
 IsLockedOut    bit,      
 LastLockoutDate   datetime,    
 FirstName    nvarchar(256),    
 LastName    nvarchar(256),    
 MiddleName    nvarchar(256) ,    
 ExpiryDate    datetime ,    
 EmailNotification  bit ,    
 TimeZone    nvarchar(100),    
 ReportRangeSelection varchar(50),    
 ReportStartDate   datetime ,    
 ReportEndDate   datetime,     
 BirthDate    datetime,    
 CompanyName    nvarchar(1024),    
 Gender     nvarchar(50),    
 HomePhone    varchar(50),    
 MobilePhone    varchar(50),    
 OtherPhone    varchar(50),    
 ImageId     uniqueidentifier,  
 Status     int ,
 ProductSuite  varchar(500)
)   
AS  
BEGIN  
  
 DECLARE @tbUser TYUser  
 INSERT INTO @tbUser  
 SELECT DISTINCT   
  U.Id,  
  U.UserName,  
  M.Email,   
  M.PasswordQuestion,    
  M.IsApproved,  
  U.CreatedDate,  
  M.LastLoginDate,   
  U.LastActivityDate,  
  M.LastPasswordChangedDate,   
  M.IsLockedOut,  
  M.LastLockoutDate,  
  U.FirstName,  
  U.LastName,  
  U.MiddleName,  
  U.ExpiryDate,  
  U.EmailNotification,  
  U.TimeZone,  
  U.ReportRangeSelection,  
  U.ReportStartDate,  
  U.ReportEndDate,  
  U.BirthDate,  
  U.CompanyName,  
  U.Gender,  
  U.HomePhone,  
  U.MobilePhone,  
  U.OtherPhone,  
  U.ImageId,  
  U.Status,
  I.Title  
 FROM dbo.USUser U  
  INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
  INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
   (@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser)   
  INNER JOIN dbo.USMemberGroup G ON G.MemberId = U.Id AND  
   (@ApplicationId IS NULL OR  
    G.ApplicationId IN (Select SiteId From dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser, 0))   
   )   
   INNER JOIN iAppsProductSuite I ON I.Id = S.ProductId
 WHERE   
  G.GroupId = @Id AND  
  (U.ExpiryDate IS NULL OR U.ExpiryDate >= GETUTCDATE()) AND  
  M.IsApproved = 1 AND M.IsLockedOut = 0 AND U.Status != 3 AND  
  (@ProductId IS NULL OR S.ProductId = @ProductId) AND  
  (@MemberId IS NULL OR G.MemberId = @MemberId)  
   
 INSERT INTO @ResultTable  
 SELECT * FROM @tbUser  
  
 RETURN  
END
'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetVariantSitesHavingPermission]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVariantSitesHavingPermission]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetVariantSitesHavingPermission
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Vaibhav Sinha
-- Create date: 7/10/2013
-- Description:	Get all the Variant site having permission 
-- =============================================

CREATE FUNCTION [dbo].[GetVariantSitesHavingPermission](@sites SiteTableType READONLY)

RETURNS 
@SitesHavingPermission TABLE 
(
	ObjectId uniqueidentifier
)
AS
BEGIN
			INSERT INTO @SitesHavingPermission SELECT ObjectId FROM @sites
					
			 ;With cteSiteTableChild
				AS
				(
					Select Id AS ObjectId from SISIte where Id NOT IN (SELECT ObjectId from @sites)
				),
				cte_SiteTemp AS
				(
					Select ObjectId, SiteId from 
					cteSiteTableChild
					CROSS APPLY 
					dbo.GetAncestorSites(ObjectId)
				)
						
				INSERT INTO @SitesHavingPermission
				Select Distinct C.ObjectId from cte_SiteTemp C
				INNER JOIN @sites S ON C.SiteId = S.ObjectId
	
	RETURN 
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetSiteHierarchyPath]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSiteHierarchyPath]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetSiteHierarchyPath
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE FUNCTION [dbo].[GetSiteHierarchyPath]
	(@SiteId uniqueidentifier, @MasterSiteId uniqueidentifier)
RETURNS nvarchar(4000)
AS  
BEGIN   

DECLARE @String varchar(8000)
SET @String = ''''

SELECT @String = S2.Title + '' > '' +@String FROM SISite S1, SISite S2 
		WHERE S1.LftValue BETWEEN S2.LftValue AND S2.RgtValue
		AND S1.Id = @SiteId AND S2.MasterSiteId = @MasterSiteId
		ORDER BY S2.LftValue Desc

SET @String = LTRIM(RTRIM(@String))
IF(charindex(''>'',@String,LEN(@String)) = LEN(@String))
BEGIN
	set @String = SUBSTRING(@String,0,len(@String))
END

RETURN LTRIM(RTRIM(@String))	

END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Order_GetRefundTotal]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Order_GetRefundTotal]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : Order_GetRefundTotal
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Bowman
-- Create date: 7/15/2009
-- Description:	Totals the Refund amount for the given order
-- =============================================
CREATE Function [dbo].[Order_GetRefundTotal]
(
	@OrderId uniqueidentifier
)
RETURNS money
AS
BEGIN

	DECLARE @paymentTotal money

	Select @paymentTotal = sum(isnull(OP.Amount,0)) 
    from PTOrderPayment OP
	INNER JOIN PTPayment P ON OP.PaymentId=P.Id
	Where P.PaymentStatusId in (12) and P.IsRefund = 1
	AND OP.OrderId=@OrderId

	RETURN isnull(@paymentTotal,0)
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetVariantDirectoryId]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVariantDirectoryId]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetVariantDirectoryId
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE FUNCTION [dbo].[GetVariantDirectoryId](
	@SourceContentId	uniqueidentifier,
	@ObjectTypeId		int,
	@ApplicationId		uniqueidentifier
) RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @DirectoryId uniqueidentifier
	
	IF @ObjectTypeId = 9
	BEGIN
		SET @DirectoryId = (SELECT TOP 1 Id FROM COFileStructure WHERE SourceDirId IN 
				(SELECT TOP 1 ParentId FROM COContent WHERE Id = @SourceContentId))
			
		IF @DirectoryId IS NULL OR @DirectoryId = dbo.GetEmptyGUID()
			SET @DirectoryId = (SELECT TOP 1 Value FROM STSiteSetting WHERE 
				SettingTypeId IN (SELECT Id FROM STSettingType WHERE Name like ''FileLibrary.DefaultImportDirectory'')
					AND SiteId = @ApplicationId)
	END
	ELSE IF @ObjectTypeId = 33
	BEGIN
		SET @DirectoryId = (SELECT TOP 1 Id FROM COImageStructure WHERE SourceDirId IN 
				(SELECT TOP 1 ParentId FROM COContent WHERE Id = @SourceContentId))
			
		IF @DirectoryId IS NULL OR @DirectoryId = dbo.GetEmptyGUID()
			SET @DirectoryId = (SELECT TOP 1 Value FROM STSiteSetting WHERE 
				SettingTypeId IN (SELECT Id FROM STSettingType WHERE Name like ''ImageLibrary.DefaultImportDirectory'')
					AND SiteId = @ApplicationId)
	END
	ELSE
	BEGIN
		SET @DirectoryId = (SELECT TOP 1 Id FROM COContentStructure WHERE SourceDirId IN 
				(SELECT TOP 1 ParentId FROM COContent WHERE Id = @SourceContentId))
			
		IF @DirectoryId IS NULL OR @DirectoryId = dbo.GetEmptyGUID()
			SET @DirectoryId = (SELECT TOP 1 Value FROM STSiteSetting WHERE 
				SettingTypeId IN (SELECT Id FROM STSettingType WHERE Name like ''ContentLibrary.DefaultImportDirectory'')
					AND SiteId = @ApplicationId)
	END
	
	RETURN @DirectoryId
END


'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetDomainLoweredUrl]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDomainLoweredUrl]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetDomainLoweredUrl
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE FUNCTION [dbo].[GetDomainLoweredUrl](
	@Url nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @LoweredUrl nvarchar(max)
	SET @LoweredUrl = replace(replace(LOWER(@Url), ''http://'', ''''), ''https://'', '''')
	IF RIGHT(@LoweredUrl, 1) = ''/''
		SET @LoweredUrl = LEFT(@LoweredUrl, LEN(@LoweredUrl) - 1)
	
	RETURN @LoweredUrl
END


'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetVariantSitesForUser]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVariantSitesForUser]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetVariantSitesForUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE FUNCTION [dbo].[GetVariantSitesForUser](@UserId uniqueidentifier)
RETURNS 
@SitesTable TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	DECLARE @siteTable as SiteTableType 
	
	INSERT INTO @siteTable
	SELECT	DISTINCT SiteId AS ObjectId
	FROM	dbo.USSiteUser US
	INNER JOIN SISite S ON US.SiteId = S.Id
	WHERE	UserId = @UserId
	
	INSERT INTO @SitesTable		
	SELECT Distinct ObjectId from [dbo].[GetVariantSitesHavingPermission](@siteTable)		
	
	RETURN 
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
