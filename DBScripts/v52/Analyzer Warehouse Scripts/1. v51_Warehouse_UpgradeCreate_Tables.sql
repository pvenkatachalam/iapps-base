
/* VARIANT SITE ROLLUP TABLE CHANGES */
PRINT 'Begin Variant Site Rollup Changes'
GO

IF COL_LENGTH('[dbo].[DimSite]', '[ParentOriginalID]') IS NULL
BEGIN
	PRINT 'Adding Parent Original Id Column to Dim Site'
	ALTER TABLE [dbo].[DimSite] ADD [ParentOriginalID] [uniqueidentifier] NULL 
END

GO

IF COL_LENGTH('[dbo].[DimSite]', '[SK_ParentSiteId]') IS NULL
BEGIN
	PRINT 'Adding SK_ParentSiteId Column to Dim Site'
	ALTER TABLE [dbo].[DimSite] ADD	[SK_ParentSiteID] [INT] NULL
END
	
GO

PRINT 'End Variant Site Rollup Changes'
GO

/* Files Downloaded / Images Served data fix related to HS Structure table */
PRINT 'Begin Asset Processing Changes'
GO

IF COL_LENGTH('[dbo].[COContent]', '[ParentId]') IS NULL
BEGIN
	PRINT 'Adding ParentId Column to COContent'
	ALTER TABLE [dbo].[COContent] ADD	[ParentId] [UNIQUEIDENTIFIER] NULL
END
	
GO

IF OBJECT_ID(N'[dbo].[COContentStructure]', 'U') IS NULL BEGIN

PRINT 'Creating COContentStructure Table'

CREATE TABLE [dbo].[COContentStructure](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Keywords] [ntext] NULL,
	[ParentId] [uniqueidentifier] NULL,
	[LftValue] [bigint] NOT NULL,
	[RgtValue] [bigint] NOT NULL,
	[SiteId] [uniqueidentifier] NULL,
	 CONSTRAINT [pk_COContentStructure_Id] PRIMARY KEY NONCLUSTERED ([Id] ASC))

END

GO

IF OBJECT_ID(N'[dbo].[COFileStructure]', 'U') IS NULL BEGIN

PRINT 'Creating COFileStructure Table'

CREATE TABLE [dbo].[COFileStructure](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Keywords] [ntext] NULL,
	[ParentId] [uniqueidentifier] NULL,
	[LftValue] [bigint] NOT NULL,
	[RgtValue] [bigint] NOT NULL,
	[SiteId] [uniqueidentifier] NULL,
	CONSTRAINT [pk_COFileStructure_Id] PRIMARY KEY NONCLUSTERED ([Id] ASC))
	
END

IF OBJECT_ID(N'[dbo].[COFormStructure]', 'U') IS NULL BEGIN

PRINT 'Creating COFormStructure Table'

CREATE TABLE [dbo].[COFormStructure](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Keywords] [ntext] NULL,
	[ParentId] [uniqueidentifier] NULL,
	[LftValue] [bigint] NOT NULL,
	[RgtValue] [bigint] NOT NULL,
	[SiteId] [uniqueidentifier] NULL,
	 CONSTRAINT [pk_COFormStructure_Id] PRIMARY KEY NONCLUSTERED ([Id] ASC))

END

IF OBJECT_ID(N'[dbo].[COImageStructure]', 'U') IS NULL BEGIN

PRINT 'Creating COImageStructure Table'

CREATE TABLE [dbo].[COImageStructure](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [nvarchar](256) NULL,
	[Keywords] [ntext] NULL,
	[ParentId] [uniqueidentifier] NULL,
	[LftValue] [bigint] NOT NULL,
	[RgtValue] [bigint] NOT NULL,
	[SiteId] [uniqueidentifier] NULL,
	CONSTRAINT [pk_COImageStructure_Id] PRIMARY KEY NONCLUSTERED ([Id] ASC))

END


PRINT 'End Asset Processing Changes'
GO