SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

PRINT 'Dropping All new procedures'
GO

/****** Object:  StoredProcedure [dbo].[Utility_VisitorConsolidated]    Script Date: 12/05/2013 16:17:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[Utility_VisitorConsolidated]') IS NOT NULL DROP PROCEDURE[dbo].[Utility_VisitorConsolidated]
IF OBJECT_ID('[dbo].[Utility_GetSiteSurrogateKey]') IS NOT NULL DROP PROCEDURE[dbo].[Utility_GetSiteSurrogateKey]
IF OBJECT_ID('[etl].[TRSessionINfo_Incremental]') IS NOT NULL DROP PROCEDURE[etl].[TRSessionINfo_Incremental]

GO


PRINT 'Creating [Utility_GetSiteSurrogateKey] Stored Procedure'
GO
--Utility Get Site Surrogate Key Procedure
CREATE PROCEDURE [dbo].[Utility_GetSiteSurrogateKey]
	@SiteId uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

	SELECT SK_SiteId FROM DimSite WHERE OriginalId = @SiteId
END

GO

PRINT 'Creating [Utility_VisitorConsolidated] Stored Procedure'
GO
--[Utility_VisitorConsolidated] Procedure
CREATE PROCEDURE [dbo].[Utility_VisitorConsolidated] ( @SiteId UNIQUEIDENTIFIER ,
													   @StartDate DATETIME ,
													   @EndDate DATETIME,
													   @RollupChildData BIT
													 )
AS 
 BEGIN  
        DECLARE @ReturnVisitors INT

        CREATE TABLE #TempTab 
            (
              sk_userid INT ,
              noofvisits INT ,
              NoOfPagesVisits BIGINT ,
              Duration INT ,
              VisitsType VARCHAR(12)
            )
        DECLARE @Start_Sk_Date_Id INT ,
				@End_Sk_Date_Id INT ,
				@Sk_Site_Id INT ,
				@TotalVisits INT ,
				@TotalDuration INT ,
				@TotalPageVisits INT 
		
		DECLARE @tblSites TABLE(Sk_Site_Id int)

        SELECT  @Start_Sk_Date_Id = Sk_DateId
        FROM    dimdate WITH(NOLOCK)
        WHERE   SqlDateTimestamp = @StartDate ;
        SELECT  @End_Sk_Date_Id = Sk_DateId
        FROM    dimdate WITH(NOLOCK)
        WHERE   SqlDateTimestamp = @EndDate ;
        
        IF (@RollupChildData = 0) 
        BEGIN
			INSERT INTO @tblSites(Sk_Site_Id)
			SELECT  SK_SiteId
			FROM    dimsite WITH(NOLOCK)
			WHERE   OriginalId = @SiteId ; 
        END
        ELSE BEGIN
			;WITH ChildSites AS (
			SELECT 
				SK_SiteId,
				OriginalId,
				ParentOriginalID
				FROM DimSite AS DS
				WHERE OriginalId = @SiteId
			UNION ALL
			
			SELECT 	
				DSR.SK_SiteId,
				DSR.OriginalId,
				DSR.ParentOriginalID
			FROM DimSite AS DSR
			INNER JOIN ChildSites CS ON CS.OriginalId = DSR.ParentOriginalID
			) INSERT INTO @tblSites
				SELECT SK_SiteId FROM DimSite
					WHERE OriginalId in (SELECT OriginalId FROM ChildSites)
		END       
		
		
		SELECT  sk_userid,
				NoOfPageVisits,
				Duration
		INTO    #current_users
		FROM    factsession a WITH(NOLOCK)
        JOIN    dimHostGeography b  WITH(NOLOCK) ON a.sk_hostgeographyid = b.sk_hostgeographyid and b.status=1
								  AND	IsExcluded = 0 
		JOIN	@tblSites t ON t.Sk_Site_Id = a.SK_SiteId
		WHERE   SK_DateId BETWEEN @Start_Sk_Date_Id  AND     @End_Sk_Date_Id
		  --AND	Sk_SiteId = @Sk_Site_Id 
   
        SELECT  sk_userid
		  INTO  #VisitorsReturnType
          FROM  factsession WITH(NOLOCK)
         WHERE  SK_DateId < @Start_Sk_Date_Id
           AND	SK_DateId > DATEADD(dd, -20, @Start_Sk_Date_Id)
           AND	sk_userid IN (
								SELECT  sk_userid
								FROM    #current_users)
	
        INSERT  INTO #TempTab
        SELECT  sk_userid ,
                COUNT(*) noofvisits ,
                SUM(NoOfPageVisits) NoOfPagesVisits ,
                SUM(duration) AS Duration ,
                ( CASE WHEN COUNT(*) = 1 THEN 'New'
                       ELSE 'Repeat'
                  END ) VisitsType
         FROM  #current_users
		where  sk_userid NOT IN (
									SELECT  sk_userid
									FROM    factsession a WITH(NOLOCK)
								    JOIN    dimHostGeography b  WITH(NOLOCK) ON a.sk_hostgeographyid = b.sk_hostgeographyid
								    JOIN	@tblSites t ON t.Sk_Site_Id = a.SK_SiteId
									WHERE   SK_DateId < @Start_Sk_Date_Id
											--AND Sk_SiteId = @Sk_Site_Id
											AND IsExcluded = 0 )
        GROUP BY sk_userid
        UNION ALL
        SELECT  u.sk_userid ,
                COUNT(DISTINCT u.sk_userid) noofvisits ,
                SUM(NoOfPageVisits) ,
                SUM(Duration) ,
                'Return' VisitsType
        FROM   #current_users u
	   WHERE   u.sk_userid in (SELECT sk_userid FROM #VisitorsReturnType) 
    GROUP BY  u.sk_userid  


		-- total visitors
        SELECT  @TotalVisits = SUM(noofvisits) ,
                @TotalDuration = SUM(Duration) ,
                @TotalPageVisits = SUM(NoOfPagesVisits)
        FROM    #TempTab
        WHERE   VisitsType <> 'Repeat' 

        INSERT  INTO #TempTab
        VALUES  ( 0, @TotalVisits, @TotalPageVisits, @TotalDuration, 'Total' ) ; 

        SELECT  VisitsType AS VisitorType ,
                SUM(noofvisits) Visits ,
                CONVERT(DECIMAL(12, 2), SUM(Duration) / SUM(NoOfPagesVisits
                                                            + .0)) AS Average ,
                CONVERT(DECIMAL(12, 2), SUM(Duration) / SUM(noofvisits + .0)) AS duration ,
                CONVERT(DECIMAL(12, 2), SUM(NoOfPagesVisits) / SUM(noofvisits
                                                              + .0)) AS AVGPageViews ,
                ( CASE VisitsType
                    WHEN 'New' THEN 2
                    WHEN 'Return' THEN 5
                    WHEN 'Repeat' THEN 4
                    WHEN 'Total' THEN 1
                  END ) AS OrderBy ,
                ( ROW_NUMBER() OVER ( PARTITION BY VisitsType ORDER BY VisitsType ) ) AS RowNumber
        FROM    #TempTab
        GROUP BY VisitsType 

     
	DROP TABLE #current_users
	DROP TABLE #TempTab
	DROP TABLE #VisitorsReturnType
END

GO


--TRSessionInfo Stored Procedure for HSStructure updates

PRINT 'Creating [TRSessionINfo_Incremental] Stored Procedure'
GO

CREATE PROCEDURE [etl].[TRSessionINfo_Incremental](@logicaldate DATETIME)
AS
BEGIN

SET NOCOUNT ON

 

		-- Declare Variables
		DECLARE @sql NVARCHAR(MAX)
		DECLARE @Origin NVARCHAR(200)
		declare @PrimaryDBServerName nvarchar(200)
		
		-- Get Origin Database
		SELECT @Origin =  SUBSTRING(configuredvalue,PATINDEX('%Initial Catalog=%',configuredvalue)+16,PATINDEX('%;Provider%',configuredvalue) -(PATINDEX('%Initial Catalog=%',configuredvalue)+16))
		  FROM admin.Configuration
		 WHERE ConfigurationFilter = 'LocalOrigin'	  
		
	 	SELECT @PrimaryDBServerName=SUBSTRING(configuredvalue,PATINDEX('%Data Source=%',configuredvalue)+12,PATINDEX('%;User ID%',configuredvalue) -(PATINDEX('%Data Source=%',configuredvalue)+12))
		  FROM admin.Configuration
		 WHERE ConfigurationFilter = 'LocalOrigin'	 
	 --SELECT *  FROM OPENROWSET('SQLOLEDB', 'Server=iapps-db3;Trusted_Connection=yes;',  'SELECT *  FROM NLC.dbo.PageDefinition')
print getdate()

		-- Add Tables to be refreshed to temp table
		CREATE TABLE #refreshedTables (tablename VARCHAR(MAX)) 
		INSERT INTO #refreshedTables SELECT 'ALClientIPExclusion'
		INSERT INTO #refreshedTables SELECT 'COContent'  
		INSERT INTO #refreshedTables SELECT 'COFile'  
		INSERT INTO #refreshedTables SELECT 'HSStructure'
		INSERT INTO #refreshedTables SELECT 'SISiteDirectory'
		INSERT INTO #refreshedTables SELECT 'TREventType'  
		INSERT INTO #refreshedTables SELECT 'ALAudienceSegmentCriteria'
		INSERT INTO #refreshedTables SELECT 'IASearchEngine'
		INSERT INTO #refreshedTables SELECT 'SISite'
		INSERT INTO #refreshedTables SELECT 'ALWatch'
		INSERT INTO #refreshedTables SELECT 'ALAssetWatch'
		INSERT INTO #refreshedTables SELECT 'ALClientBrowserWatch'
		INSERT INTO #refreshedTables SELECT 'ALTopViewWatch'
		INSERT INTO #refreshedTables SELECT 'USSecurityLevel' 
		INSERT INTO #refreshedTables SELECT 'USSiteUser' 
		INSERT INTO #refreshedTables SELECT 'USMembership' 
		INSERT INTO #refreshedTables SELECT 'USUserSecurityLevel' 
		INSERT INTO #refreshedTables SELECT 'USUser' 
		INSERT INTO #refreshedTables SELECT 'USRolePermission' 
		INSERT INTO #refreshedTables SELECT 'USGroup' 
		INSERT INTO #refreshedTables SELECT 'USRoles'
		INSERT INTO #refreshedTables SELECT 'USMemberRoles'
		INSERT INTO #refreshedTables SELECT 'USMemberGroup'
		INSERT INTO #refreshedTables SELECT 'ALClientBrowserWatch'
		INSERT INTO #refreshedTables SELECT 'GLStatus'
		INSERT INTO #refreshedTables SELECT 'COContentStructure'
		INSERT INTO #refreshedTables SELECT 'COImageStructure'
		INSERT INTO #refreshedTables SELECT 'COFileStructure'
		INSERT INTO #refreshedTables SELECT 'COFormStructure'
		--INSERT INTO #refreshedTables SELECT 'PageDefinition'
--	
		
		-- Loop Through tables to be refreshed
		DECLARE @table VARCHAR(MAX)
		DECLARE @columns VARCHAR(MAX)
		DECLARE @columns2 VARCHAR(MAX)
		
		WHILE EXISTS (SELECT 1 FROM #refreshedTables)
		BEGIN
		
			-- Get Table Name
			SELECT TOP 1 @table = tablename FROM #refreshedTables 			 
			DELETE FROM #refreshedTables WHERE tablename = @table
			
		    -- Get Column list
			SELECT @columns =''
			SELECT @columns = @columns + '' + case when system_type_id =241 then 'cast(['+ NAME +'] as nvarchar(max))' else  '[' +  NAME  + ']' end + '' + + ','  FROM sys.columns WHERE object_id = object_id(@table)						
			SELECT @columns = LEFT(@columns,LEN(@columns)-1)
			
			-- Get Column list
			SELECT @columns2 =''
			SELECT @columns2 = @columns2 + '[' +  NAME  + ']' + + ','  FROM sys.columns WHERE object_id = object_id(@table)						
			SELECT @columns2 = LEFT(@columns2,LEN(@columns2)-1)
			
			BEGIN TRY      
				-- Refresh table
				SELECT @sql = 'TRUNCATE TABLE dbo.' + @table 
				  EXEC sp_executesql @sql		
				--SELECT @sql =  ' INSERT INTO ' + 'dbo.' + @table + '(' + @columns + ') SELECT ' + @columns + ' FROM ' + @Origin + '.dbo.' + @table 
				SELECT @sql =  ' INSERT INTO ' + 'dbo.' + @table + '(' + @columns2 + ') SELECT *  FROM OPENROWSET(''SQLOLEDB'', ''Server=' + @PrimaryDBServerName +';Trusted_Connection=yes;'',  ''SELECT ' + @columns + '  FROM '+ @Origin  +'.dbo.' + @table + ' with (nolock) '')'
				print @sql
				  EXEC sp_executesql @sql
				  print getdate()
		    END TRY 
		    BEGIN CATCH
				DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int 
				 
				-- Raise an error with the details of the exception 
				SELECT @ErrMsg = '[etl].[TRSessionINfo_Incremental] FAILED WHILE LOADING  : ' + @table,
				@ErrSeverity  = ERROR_SEVERITY() 
				RAISERROR(@ErrMsg, @ErrSeverity, 1)
				RETURN 
		    END CATCH 
		END	
		-- Drop Temp table
		DROP TABLE #refreshedTables
		--Union HSStructure
		INSERT INTO HSStructure(Id, Title, ObjectTypeId, Keywords, ParentId, LftValue, RgtValue, SiteId)
		
		--We need the "Where" clause because old HS Structure records still exist in the source database
		SELECT Id, Title, 6, Keywords, ParentId, LftValue, RgtValue, SiteId  FROM COContentStructure
		WHERE Id NOT IN (SELECT Id from HSStructure)
		UNION ALL
			SELECT Id, Title, 6, Keywords, ParentId, LftValue, RgtValue, SiteId  FROM COImageStructure
			WHERE Id NOT IN (SELECT Id from HSStructure)
		UNION ALL
			SELECT Id, Title, 6, Keywords, ParentId, LftValue, RgtValue, SiteId  FROM COFileStructure
			WHERE Id NOT IN (SELECT Id from HSStructure)
		UNION ALL
			SELECT Id, Title, 6, Keywords, ParentId, LftValue, RgtValue, SiteId  FROM COFormStructure
			WHERE Id NOT IN (SELECT Id from HSStructure)
		UNION ALL
			SELECT Id, Title, ObjectTypeId, Keywords, ParentId, 0, 0, ApplicationId FROM COContent CC	
			WHERE Id NOT IN (SELECT Id from HSStructure)	

		-- Process tables that require only incremental load
		
		-- TRSessionInfo
		TRUNCATE TABLE TRSessionInfo		
		SELECT @sql =  'SELECT *  FROM OPENROWSET(''SQLOLEDB'', ''Server=' + @PrimaryDBServerName +';Trusted_Connection=yes;'',  ''SELECT * FROM ' + @Origin + '.dbo.TRSessionInfo  with (nolock) WHERE StartTime > ''''' + CONVERT(VARCHAR(11),@logicaldate,101) + '''''   and StartTime < ''''' + CONVERT(VARCHAR(11),DATEADD(d,1,@logicaldate),101) + ''''''')'
		print @sql		
		INSERT INTO dbo.TRSessionInfo		
		  EXEC sp_executesql @sql		
		 print getdate()
		-- TREventInfo
		TRUNCATE TABLE TREventInfo		
		SELECT @sql =  'SELECT *  FROM OPENROWSET(''SQLOLEDB'', ''Server=' + @PrimaryDBServerName +';Trusted_Connection=yes;'',  ''SELECT * FROM ' + @Origin + '.dbo.TREventInfo  with (nolock) WHERE EventTime > ''''' + CONVERT(VARCHAR(11),@logicaldate,101) + '''''   and EventTime < ''''' + CONVERT(VARCHAR(11),DATEADD(d,1,@logicaldate),101) + ''''''')'
		INSERT INTO dbo.TREventInfo		
		  EXEC sp_executesql @sql
		        print getdate()
	    -- TRUniqueVisitorInfo
		TRUNCATE TABLE TRUniqueVisitorInfo		
		SELECT @sql =  'SELECT *  FROM OPENROWSET(''SQLOLEDB'', ''Server=' + @PrimaryDBServerName +';Trusted_Connection=yes;'',  ''SELECT * FROM ' + @Origin + '.dbo.TRUniqueVisitorInfo  with (nolock) where Id   IN (  SELECT UniqueVisitorInfo_Id from  ' + @Origin + '.dbo.TRSessionInfo)'')'
		print @sql
		INSERT INTO dbo.TRUniqueVisitorInfo		
		  EXEC sp_executesql @sql
		  print getdate()
		-- Process tables that contain Identity Columns
		
		-- IAPhysicalGeography
--		TRUNCATE TABLE  IAPhysicalGeography
--		SET IDENTITY_INSERT IAPhysicalGeography ON
--		SELECT @sql =  'SELECT ID,Country,StateORProvince,City,ZipORPostalCode,ZipORPostalCodeExtension,Region,CreatedDate,ModifiedDate FROM ' + @Origin + '.dbo.IAPhysicalGeography '
--		INSERT INTO dbo.IAPhysicalGeography(ID,Country,StateORProvince,City,ZipORPostalCode,ZipORPostalCodeExtension,Region,CreatedDate,ModifiedDate)		
--		  EXEC sp_executesql @sql 	
--		SET IDENTITY_INSERT IAPhysicalGeography OFF
		
		 -- TRBrowserCaps
		TRUNCATE TABLE TRBrowserCaps
		SET IDENTITY_INSERT TRBrowserCaps ON
		SELECT @sql =  'SELECT *  FROM OPENROWSET(''SQLOLEDB'', ''Server=' + @PrimaryDBServerName +';Trusted_Connection=yes;'',  ''SELECT SessionID,BrowserFlashSupport,BrowserFlashVersion,ScreenResolution,CreatedDate,IDS FROM ' + @Origin + '.dbo.TRBrowserCaps  with (nolock)'')'
		INSERT INTO dbo.TRBrowserCaps(SessionID,BrowserFlashSupport,BrowserFlashVersion,ScreenResolution,CreatedDate,IDS)	
		  EXEC sp_executesql @sql 
		SET IDENTITY_INSERT TRBrowserCaps OFF		
	
print getdate()
print 'delete'
		-- Custom actions
		DELETE FROM TRSessionINfo WHERE IP in ('::1')       
		DELETE FROM CubeProcessedStatus;INSERT INTO CubeProcessedStatus VALUES('Not Processed')
		
		-- Remove fact data for date key provided
		DECLARE @Sk_dateid INT
		SELECT @Sk_dateid = Sk_dateId FROM dimdate WHERE SqlDateTimeStamp IN (SELECT ConfiguredValue FROM admin.Configuration WHERE ConfigurationFilter='LogicalDate')

		DELETE FROM dbo.FactContentActivity  WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactOutBound WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactPageActivity WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactSession WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactSessionPagePathActivity WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactSessionPathActivity WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactUniqueVisitor WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactUserActivitySiteHits WHERE sk_dateid=@Sk_dateid
		DELETE FROM dbo.FactWatchActivity WHERE sk_dateid=@Sk_dateid
		DELETE from dbo.FactSiteActivity  WHERE SK_SessionId in (select sk_sessionid from dbo.Dimsession  WHERE starttime between @logicaldate and @logicaldate +1)
		DELETE from dbo.FactInternalSearch  WHERE sk_dateid=@Sk_dateid
		DELETE from dbo.Dimsession  WHERE starttime between @logicaldate and @logicaldate +1
		
print getdate()
print 'FillBrowser'
		
		exec dbo.FillBrowserCaps_Daily @logicaldate

select * into #TempEvent 
from (
Select Id, (ROW_NUMBER() OVER(PARTITION BY [SessionINfo_Id] ORDER BY [EventTime])) as rn, ReferrerObjectId,ReferrerObjectURL ,ReferrerObjectTypeId from TREventINfo 
where EventType_EventCode=100100) X where X.rn=1 and (ReferrerObjectId is not null)
Update TREventINfo set ReferrerObjectURL=null,ReferrerObjectId=null,REferrerObjectTypeId=null where Id in (Select Id from #TempEvent )


select * into #TempEvent1 from (
Select Id, (ROW_NUMBER() OVER(PARTITION BY [SessionINfo_Id] ORDER BY [EventTime])) as rn, ReferrerObjectId,ReferrerObjectURL ,ReferrerObjectTypeId from TREventINfo 
where EventType_EventCode=100100) X where X.rn=1 and ReferrerObjectId is null and ReferrerObjectTypeId=8 and ReferrerObjectURL is not null
and lower(substring(ReferrerObjectURL,1,4)) not in ('http')

Update TREventINfo set ReferrerObjectURL=null,ReferrerObjectId=null,REferrerObjectTypeId=null where Id in (Select Id from #TempEvent1 )	
PRINT 'IP '
print getdate()
--IP Filter
--
--DECLARE @BotFilterDatabase NVARCHAR(1000)
--DECLARE @BotFilterDataServer NVARCHAR(1000)
--
--DECLARE @tempList TABLE (IPAddress NVARCHAR(1024))
--SET @BotFilterDatabase = (SELECT REPLACE(SUBSTRING(configuredvalue,PATINDEX('%database=%',configuredvalue)+9, 
--(PATINDEX('%SERVER%',configuredvalue)-(PATINDEX('%database=%',configuredvalue)+9))),';','')
--		  FROM admin.Configuration
--		 WHERE ConfigurationFilter = 'BotFilterDataSource')
--
--SET @BotFilterDataServer = (		 
--SELECT SUBSTRING(configuredvalue,PATINDEX('%'+@BotFilterDatabase+'%' , configuredvalue)+LEN(@BotFilterDatabase)+1, 
--LEN(configuredvalue) - (PATINDEX('%'+@BotFilterDatabase+'%' , configuredvalue)+LEN(@BotFilterDatabase)+1))
--		  FROM admin.Configuration
--		 WHERE ConfigurationFilter = 'BotFilterDataSource' )


--SET @SQL='SELECT a.* FROM OPENROWSET(''SQLOLEDB'', ''' +  @BotFilterDataServer + ''',''select IPAddress from ' + @BotFilterDatabase +'.dbo.ExcludeIPAddressList'') AS a;'
--SELECT a.* FROM OPENROWSET('SQLOLEDB', '@BotFilterDataServer','select IPAddress from +@BotFilterDatabase+.dbo.ExcludeIPAddressList') AS a;
--SELECT a.* INTO #tempList  FROM OPENROWSET('SQLOLEDB', 'SERVER=blproddevdb1;UID=aladmin;PWD=Bridgeline1','select IPAddress from iAppsBotFilter.dbo.ExcludeIPAddressList') AS a;

--Insert into @tempList  EXEC sp_executesql @sql

--PRINT @sql
print 'Exclusion'
				 
DECLARE @UserId UNIQUEIDENTIFIER  
SET @UserId=NEWID()

DECLARE @SiteId uniqueidentifier

	  
	  

			insert into ALClientIPExclusion 
			select 
				newid(),originalid,'iApps Bot Filter', IPaddress,IPaddress,
				'iApps Bot Filter',1,@UserId,DATEADD(hour,4, @logicaldate),null,null 
			From ExcludeIPAddressList a
	  cross join (select originalid,min(sk_siteid)sk_siteid from DimSite where SK_SiteId>1 group by OriginalId )b
			where not exists (select 1  from dimclientIPexclusion WHERE sk_siteid = b.sk_siteid and a.IPAddress = rtrim(ltrim(IP)))
		 
print getdate()
--IPFilter 

--Bot Filter 
print 'Bot Filter'
print getdate()

DECLARE @UserAgentToBeExcluded NVARCHAR(max)
DECLARE @KeywordList TABLE (UserAgent NVARCHAR(max))

--SET @SQL='SELECT a.* FROM OPENROWSET(''SQLOLEDB'', ''' +  @BotFilterDataServer + ''',''select '+@BotFilterDatabase+'.dbo.GetUserAgentLisTobeExcluded()'') AS a;'
--INSERT INTO @KeywordList EXEC sp_executesql @sql

SET @UserAgentToBeExcluded = ISNULL((SELECT  dbo.GetUserAgentLisTobeExcluded()),'')
--if(len(@UserAgentToBeExcluded)>1)
--SET @UserAgentToBeExcluded = RTRIM(LTRIM(SUBSTRING(@UserAgentToBeExcluded,2,LEN(@UserAgentToBeExcluded)-1)))

PRINT @UserAgentToBeExcluded


IF(@UserAgentToBeExcluded <>'')
begin
	--DELETE FROM dbo.TRSessionInfo WHERE dbo.Regex_IsMatch(useragent, @UserAgentToBeExcluded, 1) = 1
	DELETE FROM dbo.TRSessionInfo WHERE UserAgent IN (select UserAgent from ExcludeUserAgentList)

	DELETE FROM DBO.TREventInfo WHERE SessionInfo_Id NOT IN (SELECT Id FROM dbo.TRSessionInfo)
	DELETE FROM DBO.TRBrowserCaps WHERE SessionID NOT IN (SELECT Id FROM dbo.TRSessionInfo)

	--SELECT  distinct useragent
	--into #useragents 
	--FROM dbo.FactSession 
	 
	--select useragent,dbo.Regex_IsMatch(useragent, @UserAgentToBeExcluded, 1)excluded
	--into #excluded 
	--from #useragents 

	--delete from #excluded where excluded = 0

	SELECT SK_HostGeographyId into #SK_HostGeographyId FROM dbo.FactSession f 
	WHERE  UserAgent in   (select UserAgent from ExcludeUserAgentList)
	--exists (select 1 from #excluded e where e.UserAgent = f.useragent )
 
	-- Undo previous bug
	UPDATE dbo.DimHostGeography SET IsExcluded = 0
		
	UPDATE dbo.DimHostGeography SET IsExcluded = 1 
	 WHERE SK_HostGeographyId IN (
	SELECT SK_HostGeographyId FROM #SK_HostGeographyId )
	 
END --End Bot Filter 
update TREventInfo Set SearchKeyword= REPLACE(searchkeyword,'*','') where EventType_EventCode ='100117'

print 'End'
print getdate()

update dimplatform set devicename='Unknown' where devicename is null
update dimplatform set IsDevice=0 where devicename='Unknown' 


Declare @referrerObjectId uniqueidentifier 
Declare @ObjectURL nvarchar(200)
Declare @ReferrerObjectURL nvarchar(200)
--declare @PrimaryDBServerName nvarchar(200)
--declare @Origin nvarchar(200)
--declare @logicaldate datetime
--declare @sql nvarchar(max)

--set @PrimaryDBServerName ='iapps-db3'
--set @Origin  = 'refrigiwear'
--set @logicaldate='03/22/2012'
--drop table #TmpSessionInfo
select top 1 @referrerObjectId=referrerObjectId,@ObjectURL=ObjectURL,@ReferrerObjectURL=ReferrerObjectURL  from dbo.TREventInfo where EventType_EventCode='100116' order by EventTime desc 

truncate table dbo.[OROrder]
SELECT @sql =  'SELECT *  FROM OPENROWSET(''SQLOLEDB'', ''Server=' + @PrimaryDBServerName +';Trusted_Connection=yes;'', 
 ''select Id,OrderDate,CustomerId from   ' + @Origin + '.dbo.OROrder  WHERE OrderDate > ''''' + CONVERT(VARCHAR(11),@logicaldate,101) + '''''   and OrderDate < ''''' + CONVERT(VARCHAR(11),DATEADD(d,1,@logicaldate),101) + ''''''')'
 
insert into dbo.[OROrder] EXEC sp_executesql @sql 
		  
delete from dbo.[OROrder] where Id in (select objectid from dbo.TREventInfo where EventSubCode='100116' and ObjectId is not null)
select * into #TmpSessionInfo from dbo.TRSessionInfo where RegisteredUserId in (Select CustomerId from OROrder)

insert into dbo.TREventInfo
select NEWID() as Id,'Order placed' as Details, a.orderdate as Eventtime,'' as MachineName,a.Id as ObjectId,212 as ObjectTypeId,@referrerObjectId as ReferrerObjectId,8 as ReferrerObjectTypeId,
@ObjectURL as ObjectURL,@ReferrerObjectURL as ReferrerObjectURL , null as SearchKeyword, 100116 as EventType_EventCode, b.Id as SessionInfo_Id , 100116 as EventSubCode , 0 as IntAdditionalInfo,
null as strAdditionalInfo
   from dbo.OROrder a , dbo.TRSessionInfo b where a.customerid=b.RegisteredUserId 


if (select COUNT(*) from dbo.DimSession where OriginalId='00000000-0000-0000-0000-000000000000') =0
Begin
insert into dbo.DimSession (OriginalId,SessionIdString,SK_UserId,SessionType , SessionStatus,FirstSession, PreviousVisit,Duration,NoOfPageVisits,TimePath,CountPath,                                        SK_HostGeographyID, PageWatchPath,TimeOfDayPath,PagePath,                                        ActivityPath,Path,SK_PathID,BrowserName, BrowserLanguage,ScreenResolution,                                   FlashVersion, FlashEnabled , JavascriptSupport, StartTime)
values ('00000000-0000-0000-0000-000000000000',	'Unknown',                 	1	,'Unknown',	'Unknown',	0,	'-1'	,'-1'	,'-1'	,'-',	'-',	NULL,	'-',	'-',	'-1-',
'-1-',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL)
end

END
GO