GO
IF (OBJECT_ID('VerifyForPatch','P') IS NOT NULL )
	DROP PROCEDURE VerifyForPatch
GO
--This is to clean up the traces of iAPPS_GetDBFIlesToExecute if any....
IF (OBJECT_ID('iAPPS_GetDBFIlesToExecute') IS NOT NULL)
	DROP PROCEDURE iAPPS_GetDBFIlesToExecute
GO
IF (OBJECT_ID('iAPPS_VerifyForPatch','P') IS NOT NULL )
	DROP PROCEDURE iAPPS_VerifyForPatch
GO

/*
 * This is the master file that gives list of files to run based on the current version of the database
 * This applies for upgrade as well as patch
 * ONLY for 5.0 to 5.1 upgrade and later upgrades and patches
 * This will be called by the upgrader to decide which files to execute
*/

CREATE PROCEDURE VerifyForPatch
AS
BEGIN
	
	/*this has to changed. need to maintain all the db related scripts in xml for all the versions and patches*/
	--verifies whether it is 5.0 DB
	if(OBJECT_ID(N'[dbo].[SMSocialMedia]', 'U') IS NULL)
		BEGIN
			--For upgrade
			RETURN 0
		END
		ELSE --ONLY for patches
		BEGIN
			-- For patches	
			RETURN 1
		END
			
	-- verifies the patch; this logic should be changed after updating each version details
	
END

