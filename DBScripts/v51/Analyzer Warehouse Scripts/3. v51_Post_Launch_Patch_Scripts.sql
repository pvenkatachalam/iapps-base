SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

PRINT 'Dropping All new procedures'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('[dbo].[AnalyzerReport_GetReferrerStats]') IS NOT NULL DROP PROCEDURE[dbo].[AnalyzerReport_GetReferrerStats]

GO


PRINT 'Creating [AnalyzerReport_GetReferrerStats] Stored Procedure'
GO
--Utility Get Site Surrogate Key Procedure

CREATE PROCEDURE [dbo].[AnalyzerReport_GetReferrerStats](@StartDate DATETIME, @EndDate DATETIME, @SiteID uniqueidentifier, @RollupChildData bit = 0 )
AS
BEGIN

/*
	-- Run to allow distributed queris
	
	sp_configure 'show advanced options', 1   
	RECONFIGURE   
	GO   
	sp_configure 'ad hoc distributed queries', 1   
	RECONFIGURE   
	GO  
	allow in process in msolap provider properties
	
	-- Also need to set ServerObjects/Linked Servers/MSOLAP/Properties/Allow In-Process to true
	
	--Test Call
	exec [AnalyzerReport_GetReferrerStats]  '5/18/2011','5/18/2011','8039CE09-E7DA-47E1-BCEC-DF96B5E411F4' 
*/
SET NOCOUNT ON

		-- Declare Variables
		DECLARE @sql NVARCHAR(MAX),@params NVARCHAR(MAX), @SK_DateID INT, @cube VARCHAR(100)
		DECLARE @Origin NVARCHAR(200) , @mdxConfiguration VARCHAR(max), @start VARCHAR(50), @end VARCHAR(50), @server varchar(100)
		DECLARE @SiteMdxQuery NVARCHAR(100)
		
		-- Get Origin Database
				SELECT @server= SUBSTRING(configuredvalue,PATINDEX('%Data Source=%',configuredvalue) +12,PATINDEX('%Initial Catalog=%',configuredvalue) -(PATINDEX('%Data Source=%',configuredvalue)+13))
				  FROM admin.Configuration
				 WHERE ConfigurationFilter = 'ConnectionsASxx'	
				 
				 -- Get Origin Database
				SELECT @Origin =  SUBSTRING(configuredvalue,PATINDEX('%Initial Catalog=%',configuredvalue)+16,PATINDEX('%;Provider%',configuredvalue) -(PATINDEX('%Initial Catalog=%',configuredvalue)+16))
				  FROM admin.Configuration
				 WHERE ConfigurationFilter = 'LocalOrigin'	
				 
				SELECT @cube =  ConfiguredValue
				  FROM admin.Configuration
				 WHERE ConfigurationFilter = 'CubeName'	
 
		-- Set date strings
		SELECT @start = CONVERT(VARCHAR(50),@startdate,126),
			   @end = CONVERT(VARCHAR(50),@EndDate,126)
			   
		IF @RollupChildData = 0 BEGIN
		 SET @SiteMdxQuery = '[Dim Site].[Original Id].&[{' + CAST(@SiteID AS CHAR(36)) + '}]'
		END
		ELSE BEGIN
			DECLARE @SK_SiteId INT
			SELECT @SK_SiteId = SK_SiteId FROM Dimsite where OriginalId = @SiteID
			SET @SiteMdxQuery = '[Dim Site].[Parent].&[' + CAST(@SK_SiteId AS VARCHAR(100)) + ']'
		END 
			   
		-- Set MDX Configuration string
		SELECT @mdxConfiguration = 'DATASOURCE=' + @server + '; Initial Catalog=' + @cube + ';' 
		
			Declare @tempTable table
		(
				TotalTraffic nvarchar(50), 
				DirectTraffic  nvarchar(50),
				[DirectTraffic%] nvarchar(50),
				Search nvarchar(500),
				[Search%] nvarchar(50),
				Campaign nvarchar(500),
				[Campaign%] nvarchar(50),				
				Referrers nvarchar(500),			
				[Referrers%] nvarchar(50)		
		)
 
		SELECT @sql = 'with b(
				TotalTraffic, 
				DirectTraffic,
				[DirectTraffic%],
				Search,
				[Search%],
				Campaign,
				[Campaign%],				
				Referrers,			
				[Referrers%]				
				)
		as(
		SELECT *  FROM OpenRowset(''MSOLAP'',''' + @mdxConfiguration + ''',
		''
	 
	with member [Measures].[TotalTraffic] as [Measures].[Distinct Sessions] 
	 //direct Referrer:  All direct traffic to the site irrespective of it being associated with a Goal should be included � Email  [Already Known from old measure] � [New Email related data]
	 member measures.directreferrer as sum(( 
                 Filter([Dim Referrer Geography].[Referred URL].[Referred URL],
                 Left([Dim Referrer Geography].[Referred URL].currentmember.Properties("Caption"),len("Unknown")) <> "Unknown"),[Dim Referrer Geography].[External Internal Flag].&[0] ,[Dim Referrer Geography].[SK Search Engine Id].&[1]),[Measures].[Page Visits] )
                
	 member measures.newemaildata as sum(	[Dim Campaign Link].[Campaign Medium].&[Email],[Measures].[SiteVisits])	
	 member measures.DirectTraffic as 	measures.directreferrer-measures.newemaildata 						
	 //External Referrer:   All external Referrers � PPC � Banner Ads , [Already known from Old report] � [New PPC data] � [New Banner Ads Data]
	 member measures.externalreferrer as sum(crossjoin([Dim Referrer Geography].[External Internal Flag].&[1],
											[Dim Referrer Geography].[SK Search Engine Id].&[1]),[Measures].[Distinct Sessions])	
	 member measures.ppcbanner as 	sum({[Dim Campaign Link].[Campaign Medium].&[PPC],[Campaign Medium].&[Text Link],[Dim Campaign Link].[Campaign Medium].&[Banner]},[Measures].[SiteVisits]) 							 
	 member measures.Referrers as measures.externalreferrer - measures.ppcbanner
	 //Search Referrer:	 PPC  + Organic Search,  [New PPC data] + [Already known from old report]
	 member measures.searchreferrer as sum(except(except(except([Dim Referrer Geography].[SK Search Engine Id].members,
											[Dim Referrer Geography].[SK Search Engine Id].[All]   ),
											[Dim Referrer Geography].[SK Search Engine Id].&[1]),
											[Dim Referrer Geography].[SK Search Engine Id].[All].UNKNOWNMEMBER),
				[Measures].[Distinct Sessions])	
	 member measures.newppc as sum([Dim Campaign Link].[Campaign Medium].&[PPC],[Measures].[SiteVisits])		
	 member measures.Search as 			 measures.newppc	+measures.searchreferrer 	 
	 //Campaigns:            Email + Banner External  [All new Data] 
 
	 member measures.unknown as sum([Dim Campaign Link].[Campaign Medium].&[Unknown],[Measures].[SiteVisits])	
	 member [Measures].[Campaign] as sum({[Dim Campaign Link].[Campaign Medium].&[Banner],[Dim Campaign Link].[Campaign Medium].&[Email],[Campaign Medium].&[Text Link]},[Measures].[SiteVisits])  

     member [Measures].[DirectTraffic%]as measures.DirectTraffic /[Measures].[SiteVisits] * 100
     member [Measures].[Referrers%]as  measures.Referrers/[Measures].[SiteVisits] * 100
     member [Measures].[Search%]as measures.Search/[Measures].[SiteVisits] * 100
     member [Measures].[Campaign%]as  [Measures].[Campaign]/[Measures].[SiteVisits] * 100 
select {[Measures].[TotalTraffic],
		[Measures].[DirectTraffic],
		[Measures].[DirectTraffic%],
		[Measures].[Search],
		[Measures].[Search%],
		[Measures].[Campaign],
		[Measures].[Campaign%],
		[Measures].[Referrers],
		[Measures].[Referrers%] }  on 0
from [i Apps Warehouse]
      where  
       ([Dim Date].[SqlDateTimestamp].&[' + @start + ']:[Dim Date].[SqlDateTimestamp].&[' + @End + '],' + @SiteMdxQuery + ',[Dim Host Geography].[Hierarchy Exclusion].[Is Excluded].[0])
     '' ))
        Select * from b'
     BEGIN TRY
		Insert into @tempTable    
		EXEC sp_executesql @sql
	END TRY
	BEGIN CATCH
	END CATCH
--PRINT @sql 
Select 
	TotalTraffic, 
				DirectTraffic,
				[DirectTraffic%],
				Search,
				CAST (CAST ([Search%] AS float(24)) AS decimal(18, 13))[Search%], 
				Campaign,
				[Campaign%],				
				Referrers,			
				[Referrers%]
from @tempTable
		 		 
END

