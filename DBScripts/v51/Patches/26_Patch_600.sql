﻿GO
DECLARE @SiteId UNIQUEIDENTIFIER,@SettingTypeId INT,@Sequence INT

DECLARE SITE_CURSOR__ Cursor 
FOR
SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId -- Only Master sites

Open SITE_CURSOR__ 
Fetch NEXT FROM SITE_CURSOR__ INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN

	SET @SettingTypeId =(SELECT TOP 1 Id FROM STSettingType WHERE name ='Social.FBTokenExpiredMessage')
	IF NOT EXISTS(SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			 SELECT @Sequence =MAX(sequence)FROM dbo.STSettingType
			 INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			 VALUES('Social.FBTokenExpiredMessage','Social.FBTokenExpiredMessage',1, 1, @Sequence)
		END

		SET @SettingTypeId =ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting( SiteId, SettingTypeId, Value )
		VALUES( @SiteId,
				@SettingTypeId,
				N'As part of Facebook security procedures, the user needs to reauthorize their account periodically.<br />Your account is due for reauthorization. Please click the link below to reconnect your facebook account.'  
		)   
	END
	
	SET @SettingTypeId =(SELECT TOP 1 Id FROM STSettingType WHERE name ='Social.FBNoInsightPermission')
	IF NOT EXISTS(SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			 SELECT @Sequence =MAX(sequence)FROM dbo.STSettingType
			 INSERT INTO STSettingType(Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			 VALUES('Social.FBNoInsightPermission','Social.FBNoInsightPermission',1, 1, @Sequence)
		END

		SET @SettingTypeId =ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting( SiteId, SettingTypeId, Value )
		VALUES( @SiteId,
				@SettingTypeId,
				N'Due to improvements in iAPPS Social, your facebook account need to be reauthorized. <br />Please click the link below to reconnect your facebook account.'  
		)   
	END
	
	FETCH NEXT FROM SITE_CURSOR__ INTO @SiteId 		
END

CLOSE SITE_CURSOR__
DEALLOCATE SITE_CURSOR__
GO
PRINT 'Updating the version of the products to 5.1.0426.1'
GO
UPDATE iAppsProductSuite SET ProductVersion='5.1.0426.1'
GO

PRINT 'Updating Pagedefinition for Exclude from search = 1 and enable output cache = 0'
GO
UPDATE PageDefinition 
SET ExcludeFromSearch = 1, 
	EnableOutputCache = 0 
WHERE title LIKE '%template builder%'

GO
PRINT N'Creating [dbo].[ProcessQueue]...';

GO
IF OBJECT_ID('ProcessQueue','U') IS NULL
CREATE TABLE [dbo].[ProcessQueue] (
    [Id]                  UNIQUEIDENTIFIER NOT NULL,
    [Title]               NVARCHAR (500)   NULL,
    [TotalItems]          INT              NOT NULL,
    [CompletedItems]      INT              NULL,
    [PercentageCompleted] INT              NULL,
    [CreatedBy]           UNIQUEIDENTIFIER NOT NULL,
    [CreatedDate]         DATETIME         NOT NULL,
    [ModifiedBy]          UNIQUEIDENTIFIER NULL,
    [ModifiedDate]        DATETIME         NULL,
    [ApplicationId]       UNIQUEIDENTIFIER NOT NULL,
    [Status]              INT              NULL,
    [ProcessTypeId]       INT              NOT NULL,
    CONSTRAINT [PK_ProcessQueue] PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY]
) ON [PRIMARY];



GO
PRINT N'Creating [dbo].[ProcessType]...';

GO
IF OBJECT_ID('ProcessType','U') IS NULL
CREATE TABLE [dbo].[ProcessType] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [Title]       NVARCHAR (500)  NOT NULL,
    [Description] NVARCHAR (1024) NULL,
    CONSTRAINT [PK_ProcessType] PRIMARY KEY CLUSTERED ([Id] ASC) ON [PRIMARY]
) ON [PRIMARY];


GO
PRINT N'Creating FK_ProcessQueue_ProcessType...';


GO
IF OBJECT_ID('FK_ProcessQueue_ProcessType') IS NULL
ALTER TABLE [dbo].[ProcessQueue] WITH NOCHECK
    ADD CONSTRAINT [FK_ProcessQueue_ProcessType] FOREIGN KEY ([ProcessTypeId]) REFERENCES [dbo].[ProcessType] ([Id]);


GO


PRINT 'Inserting default values for Process Type'
GO
IF NOT EXISTS (SELECT 1 FROM [ProcessType] WHERE Title='AdminIndex')
BEGIN
	INSERT INTO [ProcessType] ([Title] ,[Description])  VALUES ('AdminIndex' ,'AdminIndex')
	INSERT INTO [ProcessType] ([Title] ,[Description])  VALUES ('FrontEndIndex' ,'FrontEndIndex')
	INSERT INTO [ProcessType] ([Title] ,[Description])  VALUES ('SiteCreation','SiteCreation')
END

GO
PRINT N'Dropping Procedure [dbo].[Content_BulkImport]...';
GO
IF OBJECT_ID('Content_BulkImport','P') IS NOT NULL
	DROP PROCEDURE Content_BulkImport
GO
PRINT N'Dropping Procedure [dbo].[ProcessQueue_Delete]...';
GO
IF OBJECT_ID('ProcessQueue_Delete','P') IS NOT NULL
	DROP PROCEDURE ProcessQueue_Delete

GO
PRINT N'Dropping Procedure [dbo].[ProcessQueue_GetProcessQueue]...';
GO
IF OBJECT_ID('ProcessQueue_GetProcessQueue','P') IS NOT NULL
	DROP PROCEDURE ProcessQueue_GetProcessQueue
GO
PRINT N'Dropping Procedure [dbo].[ProcessQueue_Save]...';
GO
IF OBJECT_ID('ProcessQueue_Save','P') IS NOT NULL
	DROP PROCEDURE ProcessQueue_Save
GO
PRINT N'Dropping Procedure [dbo].[SocialCredentials_Get]...';
GO
IF OBJECT_ID('SocialCredentials_Get','P') IS NOT NULL
	DROP PROCEDURE SocialCredentials_Get
GO
PRINT N'Creating [dbo].[BulkUpload_Content]...';

GO
IF OBJECT_ID('BulkUpload_Content','U') IS NULL
CREATE TABLE [dbo].[BulkUpload_Content] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [ApplicationId] UNIQUEIDENTIFIER NULL,
    [TemplateId]    UNIQUEIDENTIFIER NULL,
    [ParentId]      UNIQUEIDENTIFIER NULL,
    [Title]         NVARCHAR (256)   NOT NULL,
    [Description]   NVARCHAR (2048)  NULL,
    [Text]          NTEXT            NULL,
    [ObjectTypeId]  INT              NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NOT NULL,
    [Status]        INT              NOT NULL,
    [XMLString]     XML              NULL,
    [BatchId]       NVARCHAR (50)    NULL,
    [IsNew]         BIT              NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Altering [dbo].[Content_GetUrls]...';


GO
ALTER PROCEDURE [dbo].[Content_GetUrls]   
(
	@ContentIds		nvarchar(max) = NULL, 
	@PageIds		nvarchar(max) = NULL
)
AS  
BEGIN  
	DECLARE   
		@id uniqueidentifier,  
		@text varchar(max),  
		@SiteId uniqueidentifier,
		@ModifiedDate DateTime, @MaxModifiedDate DateTime, @ObjectTypeId int

	DECLARE @curSQL nvarchar(max)
  
	IF (@PageIds is not null)
	BEGIN
		DELETE FROM Content_AssetLinks Where PageId IN (@PageIds)
	END

	SELECT @MaxModifiedDate = ISNULL(Min(ContentModifiedDate), '1900-01-01') FROM Content_AssetLinks
	IF (@ContentIds is not null AND @ContentIds<>'')
	BEGIN
		SET @curSQL ='SELECT Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
			From COContent 
			where Id IN (SELECT VALUE FROM dbo.SplitComma(''' + @ContentIds + ''', '',''))'
	END
	ELSE
	BEGIN
		SET @curSQL ='SELECT Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
			From COContent 
			where Status!= 3 and Text is not null and Text not like  ''''  
			And ( ( Isnull(ModifiedDate, CreatedDate) >=  @MaxModifiedDate  And ObjectTypeId IN (7,13)) 
				) ORDER BY ModifiedDate'
	END

	SET @curSQL='Declare textCursor cursor for '+ @curSQL
	EXEC sp_executesql  @curSQL, N'@MaxModifiedDate DateTime', @MaxModifiedDate
	
	OPEN textCursor  
	FETCH NEXT FROM textCursor INTO @id, @text, @SiteId, @ModifiedDate, @ObjectTypeId  
	WHILE @@FETCH_STATUS = 0      
	BEGIN      
		DELETE FROM Content_AssetLinks WHERE ContentId = @id
		
		IF (@text IS NOT NULL AND @text <> '')
		BEGIN 
			DECLARE @link nvarchar(max), @links nvarchar(max), @hreflinks  nvarchar(max), @srclinks  nvarchar(max)

			SELECT @hreflinks = dbo.RegexSelectAll(@text,'href\s*=\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))' ,'|')
			SELECT @srclinks = dbo.RegexSelectAll(@text,'src\s*=\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))' ,'|')
			SELECT @links = @hreflinks  			
			IF (@srclinks IS NOT NULL AND @srclinks <> '') 
				SELECT @links = @links + '|' + @srclinks

			IF (SELECT charindex('|', @links)) = 1
				SET @links = SUBSTRING(@links, 2, len(@links) -1)
				
			DECLARE @tblLinks Table(link nvarchar(max))
			INSERT INTO @tblLinks
				SELECT Value FROM dbo.SplitComma(@links, '|')

			WHILE EXISTS(SELECT * FROM @tblLinks)  
			BEGIN  
				SET @link = (SELECT TOP 1 link FROM @tblLinks)			
				
				INSERT INTO Content_AssetLinks
					(SiteId, 
					ContentId, 
					AssetLink, 
					ContentModifiedDate, 
					PageId, 
					PageMapNodeId, 
					PageSiteId, 
					ObjectTypeId) 
				SELECT 
					@SiteId, 
					@id, 
					dbo.UrlDecode(@link), 
					@ModifiedDate, 
					pg.PageDefinitionId, 
					m.PageMapNodeId, 
					pg.SiteId As PageSiteId, 
					@ObjectTypeId
				FROM PageDefinition pg
					INNER JOIN PageMapNodePageDef m on pg.PageDefinitionId = m.PageDefinitionId
					INNER JOIN vw_PageContainerXmlDetails vg on vg.PageDefinitionId = pg.PageDefinitionId
				WHERE vg.ContentId =  @id  
						
				DELETE FROM @tblLinks WHERE link = @link
			END
		END
		FETCH NEXT FROM textCursor INTO  @id, @text, @SiteId, @ModifiedDate, @ObjectTypeId  
	END  
	CLOSE textCursor  
	DEALLOCATE textCursor  

END
GO
PRINT N'Creating [dbo].[Content_BulkImport]...';


GO
CREATE Procedure [dbo].[Content_BulkImport]  
(  
	@OverwriteExisting bit,
	@ModifiedBy Uniqueidentifier,
	@BatchId nvarchar(50)
)  
As  
Begin  
  
	-- Step 1: Update records for existing content  
	if @OverwriteExisting = 1  
	Begin  
		-- Step 1a: 
		UPDATE COContent SET Title = bk.Title, Description = bk.Description, Text = bk.Text, 
			ObjectTypeId = bk.ObjectTypeId, XMLString = bk.XMLString, TemplateId = bk.TemplateId,
			ModifiedDate = GetUTCDate(), ModifiedBy = @ModifiedBy
		FROM COContent co
			JOIN BulkUpload_Content bk on co.Id = bk.Id
		WHERE bk.BatchId = @BatchId
	end

	-- Step 2: Insert Records for content that do not exist  
	-- Step 2a: Content record  			
	DECLARE @OrderNumber int
	SELECT @OrderNumber = Max(OrderNo) + 1 FROM COContent;

	INSERT INTO COContent (Id, Title, Description, Text, ObjectTypeId, XMLString, TemplateId, Status, ApplicationId, CreatedDate, CreatedBy
						 ,ParentId  , OrderNo)
	SELECT bk.Id, bk.Title, bk.Description, bk.Text, bk.ObjectTypeId, bk.XMLString, bk.TemplateId, bk.Status, bk.ApplicationId, GetUTCDate(), CreatedBy
				, bk.ParentId	, (@OrderNumber + row_number() over (order by bk.Id)) AS OrderNo
	FROM BulkUpload_Content bk		
	--LEFT Outer Join COContent co on co.Id = bk.Id 
	WHERE bk.Isnew = 1 AND bk.BatchId = @BatchId	

	truncate table BulkUpload_Content
	--select dbo.GetEmptyGuid()
End
GO
PRINT N'Creating [dbo].[ProcessQueue_Delete]...';


GO
CREATE PROCEDURE [dbo].[ProcessQueue_Delete] 
(

--********************************************************************************
-- Parameters
--********************************************************************************
	@Id				uniqueidentifier,
	@ModifiedBy    	uniqueidentifier,
	@Status			int	OUTPUT
)
	
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************

DECLARE
	
	@CurrentUTCDate	datetime,
	@Stmt 			nvarchar(50),
	@Error 	        int,
	@DeleteStatus	int

BEGIN	
		-- if Id is specified, ensure exists 
		IF (@Id IS NOT NULL)
			IF (NOT EXISTS(SELECT 1 FROM   BLBlog
				WHERE  [Id] = @Id AND Status != dbo.GetDeleteStatus()))
			BEGIN
				RAISERROR ( 'NOTEXISTS||Id' , 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
			END
		
		--Set the variables 
		SET @CurrentUTCDate = getUTCdate()
		SET @Error = 0
		
		BEGIN
			
			SET @Stmt = 'Updating the status of ProcessQueue'
			SET @DeleteStatus = dbo.GetDeleteStatus()
		
				UPDATE ProcessQueue
				SET		Status	= @DeleteStatus 
				WHERE	[Id]	= @Id


				set @Status = @DeleteStatus


			SET @Error = @@ERROR
			IF @Error <> 0
			BEGIN
				--Database Error
				RAISERROR('DBERROR||%s',16,1,@Stmt)
				RETURN dbo.GetDataBaseErrorCode()			
			END	
		END	
		
END
GO
PRINT N'Creating [dbo].[ProcessQueue_GetProcessQueue]...';


GO
CREATE PROCEDURE [dbo].[ProcessQueue_GetProcessQueue]
(
	@Id					uniqueIdentifier = null,
	@ApplicationId		uniqueIdentifier = null,
	@ProcessTypeId		int = null
	
)
AS
BEGIN

SELECT Id
      ,Title
      ,TotalItems
      ,CompletedItems
      ,PercentageCompleted
      ,CreatedDate
	  ,CreatedBy
	  ,ModifiedBy
	  ,FN.UserFullName CreatedByFullName
	  ,MN.UserFullName ModifiedByFullName
	  ,ModifiedDate
      ,ApplicationId
      ,Status
      ,ProcessTypeId
  FROM [ProcessQueue] A
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE
			(@Id IS NULL OR Id = @Id) AND
			(@ApplicationId IS NULL OR @ApplicationId = ApplicationId) AND
			(@ProcessTypeId IS NULL OR @ProcessTypeId = ProcessTypeId) AND
			Status <> dbo.GetDeleteStatus()


		
END
GO
PRINT N'Creating [dbo].[ProcessQueue_Save]...';


GO
CREATE PROCEDURE [dbo].[ProcessQueue_Save] 
(
	@Id						uniqueidentifier=null OUT ,
	@ApplicationId			uniqueidentifier,	
 	@Title					nvarchar(256)=null,
 	@TotalItems				int=0,
 	@CompletedItems			int=0,
 	@ModifiedBy				uniqueidentifier,
 	@ModifiedDate			datetime=null,
 	@Status					int=null,
 	@ProcessTypeId			int
)
AS
BEGIN
	DECLARE
		@NewId		uniqueidentifier,
		@RowCount	INT,
		@Stmt 		VARCHAR(25),
		@Now 		datetime,
		@Error 		int	

	SET @Now = getutcdate()

	IF NOT EXISTS (SELECT * FROM ProcessQueue WHERE Id = @Id)
	BEGIN
		SET @Stmt = 'Create ProcessQueue'
		set @Status = dbo.GetStatus(8,'Create')
		SET @Id = newid()
		INSERT INTO ProcessQueue  WITH (ROWLOCK)(
			 	 Id,
				 ApplicationId,	
 				 Title,
 				 TotalItems,
 				 CreatedBy,
 				 CreatedDate,
 				 Status,
 				 ProcessTypeId 	
		)
		values
			(
				@Id,
				@ApplicationId,	
 				@Title,
 				@TotalItems,
 				@ModifiedBy,
 				@Now,
 				@Status,
 				@ProcessTypeId
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END

		SELECT @Id 
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify Process Queue'
		
		UPDATE ProcessQueue WITH (rowlock) SET
		Title = ISNULL(@Title, Title),
		TotalItems = ISNULL(@TotalItems, TotalItems),
		CompletedItems =  ISNULL(@CompletedItems, CompletedItems), --(CASE WHEN  @CompletedItems IS NULL then  CompletedItems else @CompletedItems end),
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @Now,
		Status = ISNULL(@Status, Status),
		ProcessTypeId = ISNULL(@ProcessTypeId, ProcessTypeId)
		WHERE
			Id = @Id
		

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		

END
GO
PRINT N'Creating [dbo].[SocialCredentials_Get]...';


GO
CREATE PROCEDURE [dbo].[SocialCredentials_Get](
	@SocialMedia	nvarchar(1024) = NULL,
	@UserId			uniqueidentifier
)
AS
BEGIN
	DECLARE @SocialMediaId int
	SET @SocialMediaId = (SELECT Id FROM SMSocialMedia WHERE name like @SocialMedia AND IsActive = 1)
	
	SELECT  
		SocialMediaId,
		(SELECT name FROM SMSocialMedia WHERE Id = SocialMediaId) AS ChannelName,
		ExternalId,
		Token,
		Secret,
		UserId
	FROM SMSocialCredentials 
	WHERE UserId = @UserId AND 
		(@SocialMediaId IS NULL OR SocialMediaId = @SocialMediaId)
END
GO
PRINT N'Altering [dbo].[Cache_SaveSiteFlushUrls]...';

GO
ALTER PROCEDURE Cache_SaveSiteFlushUrls
	@SiteUrls XML,
	@PublicSiteUrl NVARCHAR(MAX) = null
AS
BEGIN

	DELETE FROM GLInvalidateCache WHERE PublicSiteUrl = ISNULL(@PublicSiteUrl, PublicSiteUrl)

	INSERT INTO GLInvalidateCache (SiteId, Url, PublicSiteUrl)
			SELECT 
				T.C.value('SiteId[1]', 'UNIQUEIDENTIFIER'),
				T.C.value('Url[1]', 'NVARCHAR(MAX)'),
				T.C.value('PublicSiteUrl[1]', 'NVARCHAR(MAX)')
			 FROM @SiteUrls.nodes('(/DSUrlMappings/Mapping)') AS T(C)
END
GO
PRINT N'Altering [dbo].[Site_Delete]...';

GO
ALTER PROCEDURE [dbo].[Site_Delete]
(
	@Id uniqueidentifier,
	@ModifiedBy	uniqueidentifier,
	@Status		int OUT
)

AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Now	  datetime,
		@LocalStatus int
BEGIN
--********************************************************************************
-- code
--********************************************************************************

SET @LocalStatus = dbo.GetDeleteStatus()

	/*Check if @Id specified, ensure exists */
	IF (len(rtrim(isnull(@Id,'')))=0 )
	BEGIN
		RAISERROR('ISNULL||Id', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END   
    ELSE
    BEGIN
		IF (SELECT count(*) FROM  SISite WHERE  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0
		BEGIN
			RAISERROR('NOTEXISTS||Id', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END
	SET @Now = getdate()


        UPDATE	SISite  WITH (rowlock)	SET 
				ModifiedBy=@ModifiedBy,
				ModifiedDate=@Now,
				Status=@LocalStatus
        WHERE Id  = @Id 
	

	--UPDATE THE URL SO THAT ORIGINAL URLs CAN BE USED LATER
	declare @tab as table(url nvarchar(max))
declare @results nvarchar(max)
declare @formattedModifiedDate varchar(max), @URLxml xml
set @formattedModifiedDate = REPLACE(CONVERT(VARCHAR(max),@Now ,114),':','-') --This is to make sure the URL is valid

--Constructs the modified url collection
SELECT @URLxml = URL FROM SISite WHERE Id=@Id

insert into @tab
SELECT '<URL>'+tab.col.value('(text())[1]','nvarchar(max)')+'_DELETE_'+@formattedModifiedDate+'</URL>' 
from @URLxml.nodes('URLCollection/URL') tab(col)  


select @results = coalesce(@results + '', '') +  convert(nvarchar(max),url)
from @tab
--order by url

select @results = '<URLCollection>'+@results+'</URLCollection>'

UPDATE SISite
SET URL=@results,
PrimarySiteURL = PrimarySiteURL+'_DELETE_'+@formattedModifiedDate,
Title = Title+'_DELETE_'+@formattedModifiedDate
WHERE Id = @Id


		IF @@error <> 0
		BEGIN
			RAISERROR('DBERROR||Delete Site',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END

	END
		
	SET @Status =@LocalStatus
END
GO
PRINT 'Updating Channel Type on SYRssChannel Where ChannelType is null to 1 (for page channel)'
GO

update SYRssChannel set ChannelType = 1 where ChannelType is null
GO
PRINT 'Altering Procedure FWFeed_AddChannel'
GO
ALTER PROCEDURE [dbo].[FWFeed_AddChannel](

--********************************************************************************
-- Parameters
--********************************************************************************
	@RssChannelId	uniqueidentifier,
	@FWFeedId		uniqueidentifier
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

DECLARE

	@Stmt 		nvarchar(50),
	@Error 		int	


BEGIN
	
	--Set the variable
	SET @Error = 0
	SET @Stmt = 'Adding RssChannel to FWFeed.'
	
	--Check whether RssChannelId is already exists for FeedId then raise an error
	IF (EXISTS(SELECT 1 FROM   SYFWFeedRssChannel 
	WHERE  FWFeedId= @FWFeedId and RssChannelId=@RssChannelId))
	BEGIN
			RAISERROR ( 'ISEXISTS||Id' , 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	END
IF (EXISTS(SELECT 1 FROM   SYFWFeedRssChannel     
 WHERE  FWFeedId= @FWFeedId))    
   
 BEGIN  
-- Updating RssChannel to FWFeed in the SYFWFeedRssChannel table   
UPDATE SYFWFeedRssChannel SET RssChannelId = @RssChannelId WHERE FWFeedId= @FWFeedId  

 END  
   
ELSE   
      
BEGIN  

	--Adding RssChannel to FWFeed in the SYFWFeedRssChannel table
	INSERT INTO SYFWFeedRssChannel(
		FWFeedId,
		RssChannelId) 
	VALUES (
		@FWFeedId,
		@RssChannelId)

END  
	
	SET @Error = @@ERROR
	IF @Error <> 0
	BEGIN
		--Database error
		RAISERROR('DBERROR||%s',16,1,@Stmt)
		RETURN dbo.GetDataBaseErrorCode()			
	END	


END

GO
PRINT 'Altering Function GetChildrenByHierarchyType'
GO
ALTER Function [dbo].[GetChildrenByHierarchyType](@Id uniqueidentifier,@Level int,@ObjectTypeId int)
RETURNS @Results TABLE (Id uniqueidentifier,
		ObjectTypeId int,
		Title nvarchar(256),
		Keywords nvarchar(1024),
		ParentId uniqueidentifier,
		LftValue bigint,
		RgtValue bigint,
		VirtualPath nvarchar(max),
		SiteId uniqueidentifier)
AS
Begin
--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @Lft bigint,@Rgt bigint,@SiteId uniqueidentifier
Declare @isIdSite bit
IF EXISTS(Select 1 from SISite where Id=@Id) 
	SET @isIdSite = 1
ELSE 
	SET @isIdSite = 0
--********************************************************************************
-- code
--********************************************************************************
IF @ObjectTypeId =7
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				@ObjectTypeId ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COContentStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE Id = @Id OR @isIdSite = 1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COContentStructure WHERE Id=@Id
	ELSE
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COContentStructure Where SiteId = @Id Order By LftValue
		
		INSERT INTO @Results 
		SELECT  S.Id,
				7 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,	
				VirtualPath,
				SiteId
			FROM COContentStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END
ELSE IF @ObjectTypeId =9 
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				9 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COFileStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE S.Id = @Id OR @isIdSite =1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFileStructure WHERE Id=@Id
	else
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFileStructure Where SiteId = @Id Order By LftValue
			
		INSERT INTO @Results 
		SELECT  S.Id,
				9 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,	
				SiteId
			FROM COFileStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END
ELSE IF @ObjectTypeId =33 
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				33 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COImageStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE S.Id = @Id or @isIdSite = 1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COImageStructure WHERE Id=@Id
	ELSE
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COImageStructure Where SiteId = @Id Order By LftValue
			
		INSERT INTO @Results 
		SELECT  S.Id,
				33 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,	
				VirtualPath,
				SiteId
			FROM COImageStructure  S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END
ELSE IF @ObjectTypeId =38 
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				38 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COFormStructure S
			WHERE S.Id = @Id or @isIdSite = 1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFormStructure WHERE Id=@Id
	ELSE
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFormStructure Where SiteId = @Id Order By LftValue
			
		INSERT INTO @Results 
		SELECT  S.Id,
				38 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,	
				VirtualPath,
				SiteId
			FROM COFormStructure  S
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END

ELSE
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  H.Id,
				H.ObjectTypeId,
				H.Title,
				Keywords,
				ParentId,
				LftValue,
				RgtValue,
				S.VirtualPath AS VirtualPath,
				SiteId
			FROM HSStructure H
			INNER JOIN SISiteDirectory S ON H.ParentId = S.Id
			WHERE ParentId = @Id
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM HSStructure WHERE Id=@Id
		INSERT INTO @Results 
		SELECT  H.Id,
				H.ObjectTypeId,
				H.Title,
				Keywords,
				ParentId,
				LftValue,
				RgtValue,	
				S.VirtualPath AS VirtualPath,
				SiteId
			FROM HSStructure H
			INNER JOIN SISiteDirectory S ON H.ParentId = S.Id
			WHERE LftValue > @Lft and RgtValue < @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
	
END
	
Return 
End

GO
PRINT 'Altering Procedure [Facet_GetFacetsByNavigation]'
GO
ALTER PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(Id)
        FROM    ATFacetRangeProduct_Cache

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN

                SELECT 
			C.FacetID,
			C.FacetValueID,
			MIN(C.DisplayText) DisplayText,
			Count(*) ProductCount,
			MIN(NF.Sequence) FacetSequence,
			Case FR.Sequence 
				WHEN NULL  THEN @totProd - COUNT(*)
				ELSE MIN(FR.Sequence)
			END AS SortOrder,
			MIN(AF.AllowMultiple) AllowMultiple
		FROM 
			ATFacetRangeProduct_Cache C 
			INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
			INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
			INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
			LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
			LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

				SELECT 
					C.FacetID,
					C.FacetValueID,
					MIN(C.DisplayText) DisplayText,
					Count(*) ProductCount,
					MIN(NF.Sequence) FacetSequence,
					Case FR.Sequence 
								WHEN NULL  THEN @totProd - COUNT(*)
								ELSE MIN(FR.Sequence)
							END AS SortOrder,
					MIN(AF.AllowMultiple) AllowMultiple
				FROM 
					ATFacetRangeProduct_Cache C 
					INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
					INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
					INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
					INNER JOIN #FilterProductIds P ON P.ID=C.ProductID
					LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
					LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
                            WHERE   F.AllowMultiple = 0
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END
GO
PRINT 'Altering Procedure [Payment_Save]'
GO
ALTER PROCEDURE [dbo].[Payment_Save]
    (
      @Id UNIQUEIDENTIFIER OUTPUT ,
      @PaymentInfoId UNIQUEIDENTIFIER OUTPUT ,
      @OrderId UNIQUEIDENTIFIER ,
      @PaymentTypeId INT ,
      @PaymentStatusId INT = 1 ,
      @CardTypeId UNIQUEIDENTIFIER ,
      @CardNumber VARCHAR(MAX) ,
      @ExpirationMonth INT ,
      @ExpirationYear INT ,
      @NameOnCard NVARCHAR(256) ,
      @LastName NVARCHAR(256) = NULL ,
      @AuthCode NVARCHAR(256) ,
      @Description NVARCHAR(MAX) ,
      @AddressLine1 NVARCHAR(256) ,
      @AddressLine2 NVARCHAR(256) ,
      @City NVARCHAR(256) ,
      @StateId UNIQUEIDENTIFIER ,
      @StateName NVARCHAR(255) ,
      @CountryId UNIQUEIDENTIFIER ,
      @Zip NVARCHAR(50) ,
      @Phone NVARCHAR(50) ,
      @Amount MONEY ,
      @CustomerId UNIQUEIDENTIFIER ,
      @UpdateCustomerPayment BIT ,
      @SavedCardId UNIQUEIDENTIFIER ,
      @ModifiedBy UNIQUEIDENTIFIER ,
      @OrderPaymentId UNIQUEIDENTIFIER OUTPUT ,
      @IsRefund TINYINT = 0 ,
      @ExternalProfileId NVARCHAR(255)
    )
AS 
    BEGIN    -- 1   
  
        DECLARE @CreatedDate DATETIME ,
            @ModifiedDate DATETIME
--Declare @PaymentInfoId uniqueidentifier
        DECLARE @AddressId UNIQUEIDENTIFIER
        DECLARE @error INT
        DECLARE @stmt VARCHAR(256)	
        DECLARE @sequence INT
        IF ( @Id IS NULL
             OR @Id = dbo.GetEmptyGUID()
           ) 
            BEGIN --2
                SET @Id = NEWID()  
                SET @PaymentInfoId = NEWID()
                SET @CreatedDate = GETUTCDATE()
                SET @AddressId = NEWID()


                INSERT  INTO PTPayment
                        ( Id ,
                          PaymentTypeId ,
                          PaymentStatusId ,
                          CustomerId ,
                          Amount ,
                          Description ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          AuthCode ,
                          Status ,
                          IsRefund
	                  )
                VALUES  ( @Id ,
                          @PaymentTypeId ,
                          @PaymentStatusId ,
                          @CustomerId ,
                          @Amount ,
                          @Description ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @AuthCode ,
                          dbo.GetActiveStatus() ,
                          @IsRefund
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN 
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END  

-- open the symmetric key 
                OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
                OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection ;

                INSERT  INTO PTPaymentInfo
                        ( Id ,
                          CardType ,
                          Number ,
                          ExpirationMonth ,
                          ExpirationYear ,
                          NameOnCard ,
                          CreatedBy ,
                          CreatedDate ,
                          ModifiedBy ,
                          ModifiedDate ,
                          Status
	                  )
                VALUES  ( @PaymentInfoId ,
                          @CardTypeId ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'), @CardNumber) ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                       CAST(@ExpirationMonth AS VARCHAR(2))) ,
                          ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                       CAST(@ExpirationYear AS VARCHAR(4))) ,
                          @NameOnCard ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          dbo.GetActiveStatus()
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
		-- close the symmetric key
                        CLOSE SYMMETRIC KEY [key_DataShare] ;
                        CLOSE MASTER KEY
		
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                INSERT  INTO GLAddress
                        ( Id ,
                          FirstName ,
                          LastName ,
                          AddressType ,
                          AddressLine1 ,
                          AddressLine2 ,
                          City ,
                          StateId ,
                          StateName ,
                          CountryId ,
                          Zip ,
                          Phone ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status
	                  )
                VALUES  ( @AddressId ,
                          @NameOnCard ,
                          @LastName ,
                          0 ,
                          @AddressLine1 ,
                          @AddressLine2 ,
                          @City ,
                          @StateId ,
                          @StateName ,
                          @CountryId ,
                          @Zip ,
                          @Phone ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          @CreatedDate ,
                          @ModifiedBy ,
                          dbo.GetActiveStatus()
                        )
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
	
	
                INSERT  INTO PTPaymentCreditCard
                        ( Id ,
                          PaymentInfoId ,
                          ParentPaymentInfoId ,
                          PaymentId ,
                          BillingAddressId ,
                          Phone ,
                          ExternalProfileId
	                  )
                VALUES  ( NEWID() ,
                          @PaymentInfoId ,
                          @SavedCardId ,
                          @Id ,
                          @AddressId ,
                          @Phone ,
                          @ExternalProfileId
	                  )
	
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                SELECT  @sequence = MAX(ISNULL(Sequence, 0)) + 1
                FROM    PTOrderPayment
                WHERE   OrderId = @OrderId


                SET @OrderPaymentId = NEWID()
                INSERT  INTO PTOrderPayment
                        ( Id ,
                          OrderId ,
                          PaymentId ,
                          Amount ,
                          Sequence
                        )
                VALUES  ( @OrderPaymentId ,
                          @OrderId ,
                          @Id ,
                          @Amount ,
                          @sequence
                        )

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

            END  --2
        ELSE 
            BEGIN --3
                SET @CreatedDate = GETUTCDATE()
                SET @AddressId = NEWID()


                UPDATE  PTPayment
                SET     PaymentTypeId = @PaymentTypeId ,
                        PaymentStatusId = @PaymentStatusId ,
                        CustomerId = @CustomerId ,
                        Amount = @Amount ,
                        Description = @Description ,
                        ModifiedBy = @ModifiedBy ,
                        ModifiedDate = @CreatedDate ,
                        AuthCode = @AuthCode ,
                        Status = dbo.GetActiveStatus() ,
                        IsRefund = @IsRefund
                WHERE   Id = @Id


                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

	-- open the symmetric key 
                OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
                OPEN SYMMETRIC KEY [key_DataShare] 
				DECRYPTION BY CERTIFICATE cert_keyProtection ;

                UPDATE  PTPaymentInfo
                SET     CardType = @CardTypeId ,
                        Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                              @CardNumber) ,
                        ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                       CAST(@ExpirationMonth AS VARCHAR(2))) ,
                        ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                      CAST(@ExpirationYear AS VARCHAR(4))) ,
                        NameOnCard = @NameOnCard ,
                        ModifiedBy = @ModifiedBy ,
                        ModifiedDate = GETUTCDATE() ,
                        Status = dbo.GetActiveStatus()
                WHERE   Id = @PaymentInfoId
				
                
		

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
			-- close the symmetric key
                        CLOSE SYMMETRIC KEY [key_DataShare] ;
                        CLOSE MASTER KEY
			
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

				UPDATE  dbo.PTPaymentCreditCard
                SET     ExternalProfileId = @ExternalProfileId
                WHERE   PaymentInfoId = @PaymentInfoId

                SELECT  @AddressId = BillingAddressId
                FROM    PTPaymentCreditCard
                WHERE   PaymentId = @Id
		
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

                UPDATE  GLAddress
                SET     FirstName = @NameOnCard ,
                        LastName = @LastName ,
                        AddressType = 0 ,
                        AddressLine1 = @AddressLine1 ,
                        AddressLine2 = @AddressLine2 ,
                        City = @City ,
                        StateId = @StateId ,
                        StateName = @StateName ,
                        CountryId = @CountryId ,
                        Zip = @Zip ,
                        Phone = @Phone ,
                        ModifiedDate = GETUTCDATE() ,
                        ModifiedBy = @ModifiedBy ,
                        Status = dbo.GetActiveStatus()
                WHERE   Id = @AddressId
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END	


                UPDATE  PTOrderPayment
                SET     Amount = @Amount
                WHERE   Id = @OrderPaymentId
		
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
		
                    END
            END  --3

        IF @UpdateCustomerPayment = 1 
            BEGIN --4
                DECLARE @NewAddressId UNIQUEIDENTIFIER
	
                SET @NewAddressId = NEWID()
                INSERT  INTO GLAddress
                        ( Id ,
                          FirstName ,
                          LastName ,
                          AddressType ,
                          AddressLine1 ,
                          AddressLine2 ,
                          City ,
                          StateId ,
                          StateName ,
                          CountryId ,
                          Zip ,
                          Phone ,
                          CreatedDate ,
                          CreatedBy ,
                          ModifiedDate ,
                          ModifiedBy ,
                          Status
	                  )
                        SELECT  @NewAddressId ,
                                FirstName ,
                                LastName ,
                                AddressType ,
                                AddressLine1 ,
                                AddressLine2 ,
                                City ,
                                StateId ,
                                StateName ,
                                CountryId ,
                                Zip ,
                                Phone ,
                                CreatedDate ,
                                CreatedBy ,
                                ModifiedDate ,
                                ModifiedBy ,
                                Status
                        FROM    GLAddress
                        WHERE   Id = @AddressId

                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
                DECLARE @ExistingPaymentInfo UNIQUEIDENTIFIER
                SELECT  @ExistingPaymentInfo = PaymentInfoId
                FROM    USUserPaymentInfo
                WHERE   Id = @SavedCardId
                DECLARE @NewPaymentInfoId UNIQUEIDENTIFIER
                SET @NewPaymentInfoId = NEWID()
                IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
                    BEGIN
                        UPDATE  PTPaymentInfo
                        SET     CardType = @CardTypeId ,
                                Number = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                      @CardNumber) ,
                                ExpirationMonth = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                              CAST(@ExpirationMonth AS VARCHAR(2))) ,
                                ExpirationYear = ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                                              CAST(@ExpirationYear AS VARCHAR(4))) ,
                                NameOnCard = @NameOnCard ,
                                ModifiedBy = @ModifiedBy ,
                                ModifiedDate = @CreatedDate
                        WHERE   Id = @ExistingPaymentInfo
                    END
                ELSE 
                    BEGIN
                        INSERT  INTO PTPaymentInfo
                                ( Id ,
                                  CardType ,
                                  Number ,
                                  ExpirationMonth ,
                                  ExpirationYear ,
                                  NameOnCard ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  Status ,
                                  ModifiedBy ,
                                  ModifiedDate
		                    )
                        VALUES  ( @NewPaymentInfoId ,
                                  @CardTypeId ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               @CardNumber) ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               CAST(@ExpirationMonth AS VARCHAR(2))) ,
                                  ENCRYPTBYKEY(KEY_GUID('key_DataShare'),
                                               CAST(@ExpirationYear AS VARCHAR(4))) ,
                                  @NameOnCard ,
                                  @ModifiedBy ,
                                  @CreatedDate ,
                                  1 ,
                                  @ModifiedBy ,
                                  @CreatedDate
			              )
                    END
	
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END
                IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
                    BEGIN
                        UPDATE  USUserPaymentInfo
                        SET     BillingAddressId = @NewAddressId ,
                                ExternalProfileId = @ExternalProfileId
                        WHERE   PaymentInfoId = @ExistingPaymentInfo
                                AND UserId = @CustomerId
		
                    END 
                ELSE 
                    BEGIN 
                        INSERT  INTO USUserPaymentInfo
                                ( Id ,
                                  UserId ,
                                  PaymentInfoId ,
                                  BillingAddressId ,
                                  Sequence ,
                                  IsPrimary ,
                                  CreatedBy ,
                                  CreatedDate ,
                                  Status ,
                                  ModifiedDate ,
                                  ModifiedBy ,
                                  ExternalProfileId
		                    )
                        VALUES  ( NEWID() ,
                                  @CustomerId ,
                                  @NewPaymentInfoId ,
                                  @AddressId ,
                                  1 ,
                                  0 ,
                                  @ModifiedBy ,
                                  @CreatedDate ,
                                  1 ,
                                  @CreatedDate ,
                                  @ModifiedBy ,
                                  @ExternalProfileId		
		                    )
                    END
                SELECT  @error = @@error
                IF @error <> 0 
                    BEGIN
                        RAISERROR('DBERROR||%s',16,1,@stmt)
                        RETURN dbo.GetDataBaseErrorCode()	
                    END

            END --4


        UPDATE  OROrder
        SET     PaymentTotal = dbo.Order_GetPaymentTotal(@OrderId) ,
                RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
        WHERE   Id = @OrderId

  
  
    END
GO
PRINT 'Altering Procedure [Comments_GetByObjectId]'
GO
ALTER PROCEDURE [dbo].[Comments_GetByObjectId]
(
	@ObjectId		uniqueidentifier,
	@Status			int = null,
	@PageNumber		int = 1,
	@PageSize		int = 10,
	@SortOrder		nvarchar(10) = null,
	@VirtualCount	int output
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

BEGIN
--********************************************************************************
-- code
--********************************************************************************
	IF @PageNumber is NUll
		set @PageNumber = 1
	IF @SortOrder is null
		set @SortOrder = 'desc'
	Declare @commentsRootId uniqueidentifier
	SET @commentsRootId =(Select top 1 Id from BLComments Where LftValue=1)
	
	DECLARE @tbComments table (RowNumber int identity(1,1), Id uniqueidentifier, LftValue bigint, RgtValue bigint)
	IF (lower(@SortOrder) = 'desc')
		Insert into @tbComments
			Select Id, LftValue, RgtValue from BLComments Where Level = 0 And ObjectId = @ObjectId And (@Status IS NULL OR Status = @Status) AND (ParentId is NULL OR ParentId=@commentsRootId)
			Order by LftValue desc
	ELSE
		Insert into @tbComments
			Select Id, LftValue, RgtValue from BLComments Where Level = 0 And ObjectId = @ObjectId And (@Status IS NULL OR Status = @Status) AND (ParentId is NULL OR ParentId=@commentsRootId)
			Order by LftValue
			

	SET @VirtualCount = @@ROWCOUNT
	
	;With ResultEntries As(
		Select ROW_NUMBER() over (Order by P.RowNumber) As RowNumber,
			C.Id, C.ParentId, C.ObjectId, C.PostedBy, C.Email, C.Title, 
			C.Comments, C.UserId, C.Status, C.CreatedDate, C.Level,
			C.CreatedBy, FN.FirstName, FN.LastName
			from @tbComments P
				Inner Join BLComments C On C.LftValue Between P.LftValue AND P.RgtValue 
				LEFT JOIN USUser FN on FN.Id = C.CreatedBy
		Where @PageSize IS NULL OR (RowNumber >= (@PageSize * @PageNumber) - (@PageSize - 1) AND RowNumber <= @PageSize * @PageNumber)
		AND (@Status IS NULL OR C.Status = @Status)
	)
	
	
	SELECT RE.*,R.Id as RatingId, R.RatingValue, R.ObjectId, R.IPAddress, R.CreatedDate as RatingCreatedDate
		FROM ResultEntries RE Left Join BLCommentRating CR on RE.Id= CR.CommentId 
				Left Join BLRatings R on CR.RatingId = R.Id
		
END
GO
PRINT 'Parameter Length changes for FindAndReplace_GetContentLocation'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetContentLocation]
(@contentId UNIQUEIDENTIFIER, @siteId UNIQUEIDENTIFIER)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetContentLocation]

GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedContentDefinitionXmlString]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
(@source XML, @searchTerm NVARCHAR (max), @replaceTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedFileProperty]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty]
(@source NVARCHAR (max), @searchTerm NVARCHAR (max), @replaceTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedFileProperty]

GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedImageProperty]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty]
(@source NVARCHAR (max), @searchTerm NVARCHAR (max), @replaceTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedImageProperty]

GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedLinkInContentDefinition]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (max), @newUrl NVARCHAR (max), @matchWholeLink BIT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedLinkInContentDefinition]

GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedPageName]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName]
(@source NVARCHAR (max), @searchTerm NVARCHAR (max), @replaceTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageName]

GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedPageProperties]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties]
(@source XML, @searchTerm NVARCHAR (max), @replaceTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageProperties]

GO
PRINT 'Parameter Length changes for [FindAndReplace_GetUpdatedTextContentText]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText]
(@source [nvarchar](max), @searchTerm [nvarchar](max), @replaceTerm [nvarchar](max), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedTextContentText]

GO
PRINT 'Parameter Length changes for [FindAndReplace_IsContentDefinitionMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch]
(@source XML, @searchTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT, @includeAttributeValues BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsContentDefinitionMatch]

GO
PRINT 'Parameter Length changes for [FindAndReplace_IsFilePropertiesMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch]
(@title NVARCHAR (max), @description NVARCHAR (max), @altText NVARCHAR (max), @searchTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsFilePropertiesMatch]

GO
PRINT 'Parameter Length changes for [FindAndReplace_IsImagePropertiesMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch]
(@title NVARCHAR (max), @description NVARCHAR (max), @altText NVARCHAR (max), @searchTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsImagePropertiesMatch]

GO
PRINT 'Parameter Length changes for [FindAndReplace_IsLinkInContentDefinition]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition]
(@source XML, @oldUrl NVARCHAR (max), @matchWholeLink BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsLinkInContentDefinition]

GO
PRINT 'Parameter Length changes for [FindAndReplace_IsPageNameMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsPageNameMatch]
(@source NVARCHAR (max), @searchTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPageNameMatch]
GO
PRINT 'Parameter Length changes for [FindAndReplace_IsPagePropertiesMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch]
(@source XML, @searchTerm NVARCHAR (max), @matchCase BIT, @wholeWords BIT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPagePropertiesMatch]

GO
PRINT 'Parameter Length changes for FindAndReplace_IsTextContentMatch'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsTextContentMatch](@source [nvarchar](max), @searchTerm [nvarchar](max), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsTextContentMatch]

GO
PRINT 'Altering procedure [SqlDirectoryProvider_GetDefaultContentDirectories]'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultContentDirectories]
(
 @SiteId				uniqueIdentifier,  
 @Level					nvarchar(4000),  
 @UserId				uniqueIdentifier,
 @AccessibleNodesOnly	bit = 0,
 @onlySharedNode bit = 0,
 @ParentId				uniqueIdentifier = null
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 7
	
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COContentStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COContentStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,
			D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COContentStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId = @ParentId OR D.Id = @ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT DISTINCT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			7 ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath,  
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,S.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,ISNULL(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites   
		FROM COContentStructure S 
			left outer join  @objectRole R on R.ObjectId = S.Id  
			left outer join @objectCount FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COContentStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites 
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COContentStructure  D 
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1 From COContentStructure M   
				WHERE M.LftValue Between @Lft and @Rgt  
					And D.LftValue Between M.LftValue and M.RgtValue  
					And M.LftValue Not in (D.LftValue ,@Lft))  
					AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT 'Altering procedure [SqlDirectoryProvider_GetDefaultFileDirectories]'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultFileDirectories]
(
 @SiteId				uniqueIdentifier,  
 @Level					nvarchar(4000),  
 @UserId				uniqueIdentifier,
 @AccessibleNodesOnly	bit = 0,
 @onlySharedNode bit = 0,
 @ParentId				uniqueIdentifier = null
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 9
   
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COFileStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFileStructure   
		Where Id = @ParentId AND SiteId = @SiteId
 
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFileStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <= -1  
	BEGIN  
		SELECT DISTINCT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			@ObjectType ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath, S.InheritSecurityLevels, S.PropagateSecurityLevels,
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFileStructure S 
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFileStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFileStructure  D 
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
				And D.LftValue Between M.LftValue and M.RgtValue  
				And M.LftValue Not in (D.LftValue ,@Lft))  
				AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT 'Altering procedure [SqlDirectoryProvider_GetDefaultFormDirectories]'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]
(
 @SiteId    uniqueIdentifier,      
 @Level     nvarchar(4000),      
 @UserId    uniqueIdentifier,    
 @AccessibleNodesOnly bit = 0,  
 @onlySharedNode bit = 0,
 @ParentId    uniqueIdentifier = null 
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 38
	
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COFormStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFormStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,
			D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFormStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId = @ParentId OR D.Id = @ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT DISTINCT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			38 ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath,  
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,S.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,ISNULL(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites   
		FROM COFormStructure S 
			left outer join  @objectRole R on R.ObjectId = S.Id  
			left outer join @objectCount FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFormStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites 
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFormStructure  D 
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1 From COFormStructure M   
				WHERE M.LftValue Between @Lft and @Rgt  
					And D.LftValue Between M.LftValue and M.RgtValue  
					And M.LftValue Not in (D.LftValue ,@Lft))  
					AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END

GO
PRINT 'Altering procedure [SqlDirectoryProvider_GetDefaultImageDirectories]'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultImageDirectories]
(
	@SiteId					uniqueIdentifier,  
	@Level					nvarchar(4000),  
	@UserId					uniqueIdentifier,
	@AccessibleNodesOnly	bit = 0,
	@onlySharedNode bit = 0,
	@ParentId				uniqueIdentifier = null
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 33
   
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COImageStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COImageStructure   
		Where Id = @ParentId AND SiteId = @SiteId
 
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)   AllowAccessInChildrenSites
		FROM COImageStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT DISTINCT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			@ObjectType ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath, S.InheritSecurityLevels, S.PropagateSecurityLevels,
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure S 
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure  D 
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
				And D.LftValue Between M.LftValue and M.RgtValue  
				And M.LftValue Not in (D.LftValue ,@Lft))  
				AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT 'Altering procedure [SqlDirectoryProvider_GetDefaultSharedDirectories]'
GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultSharedDirectories]
(
	@SiteId			uniqueIdentifier,
	@ObjectType		int,
	@Level			nvarchar(4000),
	@UserId			uniqueIdentifier,
	@ParentId		uniqueIdentifier = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Lft bigint,  
	@Rgt bigint
BEGIN  
--********************************************************************************
-- code
--********************************************************************************
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId))
				And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM HSStructure where Id = @ParentId)

	SELECT @Lft = LftValue , @Rgt = RgtValue from HSStructure   
		Where Id = @ParentId AND SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId))

	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (S.ParentId =@ParentId OR S.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT DISTINCT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,
			D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,
			D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		--AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
		--	(Select ObjectId from @objectRole) OR S.ParentId IN 
		--	(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id   
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.Id=@ParentId And D.Status != dbo.GetDeleteStatus() And D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM HSStructure  C  Inner Join SISiteDirectory D ON C.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = C.Id  
		left outer join @objectCount FC ON FC.Id=C.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE C.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR C.Id=@ParentId)  
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
			  And C.LftValue Between M.LftValue and M.RgtValue  
			  And M.LftValue Not in (C.LftValue ,@Lft))  
				AND C. SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END
END
GO
ALTER PROCEDURE [dbo].[Facet_GetFacetsByNavigation]
    (
      @Id UNIQUEIDENTIFIER ,
      @ExistingFacetValueIds XML = NULL ,
      @ExistingFacets XML = NULL ,
      @UseLimit BIT = 1
    )
AS 
    BEGIN

        DECLARE @filterId UNIQUEIDENTIFIER
        SELECT  @filterId = QueryId
        FROM    NVNavNodeNavFilterMap
        WHERE   NavNodeId = @Id

        DECLARE @totProd INT
        SELECT  @totProd = COUNT(Id)
        FROM    ATFacetRangeProduct_Cache

        SET @UseLimit = ISNULL(@UseLimit, 0)
        IF ( @ExistingFacetValueIds IS NULL
             AND @ExistingFacets IS NULL
           ) 
            BEGIN

                SELECT 
			C.FacetID,
			C.FacetValueID,
			MIN(C.DisplayText) DisplayText,
			Count(*) ProductCount,
			MIN(NF.Sequence) FacetSequence,
			Case FR.Sequence 
				WHEN NULL  THEN @totProd - COUNT(*)
				ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
			END AS SortOrder,
			MIN(AF.AllowMultiple) AllowMultiple
		FROM 
			ATFacetRangeProduct_Cache C 
			INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
			INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
			INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
			LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
			LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetValueID ,
                        C.FacetID
                UNION ALL
                SELECT  C.FacetID ,
                        NULL FacetValueID ,
                        MIN(AF.Title) DisplayText ,
                        COUNT(*) ,
                        MIN(Sequence) FacetSequence ,
                        0 SortOrder ,
                        MIN(AF.AllowMultiple) AllowMultiple
                FROM    ATFacetRangeProduct_Cache C
                        INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                        INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                        INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                        LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                         AND NFE.QueryId = NFO.QueryId
                WHERE   NF.NavNodeId = @Id
                        AND NFE.QueryId IS NULL
                        AND NFO.QueryId = @filterId
                GROUP BY C.FacetID 
            END
        ELSE 
            IF ( @ExistingFacetValueIds IS NOT NULL ) 
                BEGIN
 

                    CREATE TABLE #FilterProductIds
                        (
                          Id UNIQUEIDENTIFIER
                            CONSTRAINT [PK_#cteValueToIdValue]
                            PRIMARY KEY CLUSTERED ( [Id] ASC )
                            WITH ( PAD_INDEX = OFF,
                                   STATISTICS_NORECOMPUTE = OFF,
                                   IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                   ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
                        )
                    ON  [PRIMARY]

                    INSERT  INTO #FilterProductIds
                            SELECT  C.ProductID
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab ( col ) ON tab.col.value('text()[1]',
                                                              'uniqueidentifier') = C.FacetValueID
                            GROUP BY C.ProductID
                            HAVING  COUNT(C.FacetValueID) >= ( SELECT
                                                              COUNT(1)
                                                              FROM
                                                              @ExistingFacetValueIds.nodes('/GenericCollectionOfGuid/guid') tab1 ( col1 )
                                                             )

				SELECT 
					C.FacetID,
					C.FacetValueID,
					MIN(C.DisplayText) DisplayText,
					Count(*) ProductCount,
					MIN(NF.Sequence) FacetSequence,
					Case FR.Sequence 
								WHEN NULL  THEN @totProd - COUNT(*)
								ELSE COALESCE(MIN(FR.Sequence), @totProd - COUNT(*))
							END AS SortOrder,
					MIN(AF.AllowMultiple) AllowMultiple
				FROM 
					ATFacetRangeProduct_Cache C 
					INNER JOIN NVNavNodeFacet NF ON C.FacetID= NF.FacetId
					INNER JOIN ATFacet AF ON AF.Id = NF.FacetId	
					INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
					INNER JOIN #FilterProductIds P ON P.ID=C.ProductID
					LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId and NFE.QueryId = NFO.QueryId
					LEFT JOIN ATFacetRange FR on FR.Id = C.FacetValueID
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetValueID ,
                            C.FacetID
                    UNION ALL
                    SELECT  C.FacetID ,
                            NULL FacetValueID ,
                            MIN(AF.Title) DisplayText ,
                            COUNT(*) ProductCount ,
                            MIN(Sequence) FacetSequence ,
                            0 SortOrder ,
                            MIN(AF.AllowMultiple) AllowMultiple
                    FROM    ATFacetRangeProductTop_Cache C
                            INNER JOIN NVNavNodeFacet NF ON C.FacetID = NF.FacetId
                            INNER JOIN ATFacet AF ON AF.Id = NF.FacetId
                            INNER JOIN NVFilterOutput NFO ON NFO.ObjectId = C.ProductID --P.Id 
                            INNER JOIN #FilterProductIds P ON P.ID = C.ProductID
                            LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = NFO.ObjectId
                                                             AND NFE.QueryId = NFO.QueryId
                    WHERE   NF.NavNodeId = @Id
                            AND NFE.QueryId IS NULL
                            AND NFO.QueryId = @filterId
                    GROUP BY C.FacetID 


                    DROP TABLE #FilterProductIds

                END
            ELSE 
                BEGIN
                   ------------------NEW---------------------------------
                    DECLARE @FacetValues TABLE
                        (
                          FacetValueID UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER
                        )
                    INSERT  INTO @FacetValues
                            SELECT  tab.col.value('(value/guid/text())[1]',
                                                  'uniqueidentifier') ,
                                    tab.col.value('(key/guid/text())[1]',
                                                  'uniqueidentifier')
                            FROM    @ExistingFacets.nodes('/GenericCollectionOfKeyValuePairOfGuidGuid/KeyValuePairOfGuidGuid/KeyValuePair/item') tab ( col ) ;
                 

                 
                 

                    DECLARE @FilteredProducts TABLE
                        (
                          ProductId UNIQUEIDENTIFIER
                        )           
                    
                    DECLARE @RequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
       
                    INSERT  INTO @RequiredProduct
                            SELECT DISTINCT
                                    C.ProductId ,
                                    Row_Number() OVER ( PARTITION BY ProductId ORDER BY ProductID ) AS RowNumber
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 0 

                    DELETE  FROM @RequiredProduct
                    WHERE   RowNumber NOT IN ( SELECT   MAX(RowNumber)
                                               FROM     @RequiredProduct )


                    DECLARE @MultiRequiredProduct TABLE
                        (
                          ProductId UNIQUEIDENTIFIER ,
                          FacetId UNIQUEIDENTIFIER ,
                          RowNumber INT
                        )
                     INSERT  INTO @MultiRequiredProduct
                            SELECT  DISTINCT
                                    ProductID ,
                                    FacetID ,
                                    Row_Number() OVER ( PARTITION BY ProductID ORDER BY ProductID ) AS RowNumber
                                    
                                    FROM (
                                    SELECT DISTINCt C.ProductID,
                                    C.FacetID
                                    
                            FROM    ATFacetRangeProduct_Cache C
                                    INNER JOIN @FacetValues FV ON FV.FacetValueID = C.FacetValueID
                                    INNER JOIN dbo.ATFacet F ON F.Id = FV.FacetId
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                            WHERE   F.AllowMultiple = 1)TV
                           



                    IF NOT EXISTS ( SELECT  *
                                    FROM    @RequiredProduct ) 
                        BEGIN
                            INSERT  INTO @FilteredProducts
                                    SELECT  ProductId
                                    FROM    @MultiRequiredProduct
                                    WHERE   RowNumber IN (
                                            SELECT  MAX(RowNumber)
                                            FROM    @MultiRequiredProduct )
                       
                        END
                    ELSE 
                        IF NOT EXISTS ( SELECT  *
                                        FROM    @MultiRequiredProduct ) 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  ProductId
                                        FROM    @RequiredProduct
                            END
                        ELSE 
                            BEGIN
                                INSERT  INTO @FilteredProducts
                                        SELECT  RP.ProductId
                                        FROM    @RequiredProduct RP
                                                INNER JOIN ( SELECT
                                                              ProductId
                                                             FROM
                                                              @MultiRequiredProduct
                                                             WHERE
                                                              RowNumber IN (
                                                              SELECT
                                                              MAX(RowNumber)
                                                              FROM
                                                              @MultiRequiredProduct )
                                                           ) TV ON TV.ProductId = RP.ProductId


                            END

                    DECLARE @FacetResults TABLE
                        (
                          FacetId UNIQUEIDENTIFIER ,
                          FacetValueId UNIQUEIDENTIFIER ,
                          DisplayText NVARCHAR(250) ,
                          FacetName NVARCHAR(250) ,
                          ProductCount INT ,
                          FacetSequence INT ,
                          SortOrder INT ,
                          AllowMultiple INT ,
                          ENABLED INT
                        )
                    INSERT  INTO @FacetResults
                            ( FacetId ,
                              FacetValueId ,
                              DisplayText ,
                              FacetName ,
                              ProductCount ,
                              FacetSequence ,
                              SortOrder ,
                              AllowMultiple ,
                              ENABLED
                            )
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @FilteredProducts RP ON RP.ProductId = C.ProductID
                            WHERE   F.AllowMultiple = 0
                                    AND ( RP.ProductId IS NOT NULL
                                          OR NOT EXISTS ( SELECT
                                                              ProductId
                                                          FROM
                                                              @FilteredProducts )
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
--Close now we need to get the other multi values that are not currently in the required products but would be filtered by any required products. 
--This query should be the multi facets that have no products currently showing in the grid. 
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( 
																		--MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
                                                              ( SELECT
                                                              COUNT(DISTINCT FacetId)
                                                              FROM
                                                              @MultiRequiredProduct
                                                              ) = 1 )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                        )
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
                            UNION
                            SELECT  C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    F.Title ,
                                    COUNT(C.ProductID) AS ProductCount ,
                                    C.FacetSequence ,
                                    C.SortOrder ,
                                    F.AllowMultiple ,
                                    1 AS [Enabled]
                            FROM    dbo.ATFacetRangeProduct_Cache C
                                    INNER JOIN dbo.ATFacet F ON F.Id = C.FacetID
                                    INNER JOIN NVFilterOutput FO ON C.ProductID = FO.ObjectId
                                                              AND FO.QueryId = @FilterId
                                    INNER JOIN NVNavNodeFacet NF ON C.FacetId = NF.FacetId
                                                              AND NF.NavNodeId = @Id
                                    LEFT JOIN @RequiredProduct RP ON RP.ProductId = C.ProductID
                                    LEFT JOIN @MultiRequiredProduct MRP ON ( MRP.ProductId = C.ProductID
                                                              AND ( MRP.FacetId != C.FacetID
																  ---This is crazy expensive
																		--OR 
																		--( SELECT
																		--COUNT(DISTINCT FacetId)
																		--FROM
																		--@MultiRequiredProduct
																		--) = 1
                                                              )
																	---End of expensive
                                                              )
                            WHERE   F.AllowMultiple = 1
                                    AND ( ( ( RP.ProductId IS NOT NULL
                                              OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @RequiredProduct )
                                            )
                                            AND ( MRP.ProductId IS NOT NULL
                                                  OR NOT EXISTS ( SELECT
                                                              ProductId
                                                              FROM
                                                              @MultiRequiredProduct )
                                                )
                                          )
                                          OR ( SELECT   COUNT(*)
                                               FROM     @FacetValues
                                               WHERE    FacetId != C.FacetId
                                             ) = 0
                                             
                                        )
                                        
                            GROUP BY C.FacetID ,
                                    C.FacetValueID ,
                                    DisplayText ,
                                    FacetSequence ,
                                    SortOrder ,
                                    AllowMultiple ,
                                    F.Title
        
                        
        
        
        
                    SELECT  FacetId ,
                            FacetValueId ,
                            DisplayText ,
                            ProductCount ,
                            FacetSequence ,
                            SortOrder ,
                            AllowMultiple ,
                            ENABLED
                    FROM    @FacetResults
                    UNION
                    SELECT  FacetId ,
                            NULL ,
                            FacetName AS DisplayText ,
                            SUM(ProductCount) ,
                            MAX(FacetSequence) ,
                            MAX(SortOrder) ,
                            MAX(AllowMultiple) ,
                            MAX(ENABLED)
                    FROM    @FacetResults
                    GROUP BY FacetId ,
                            FacetName
                    ORDER BY FacetValueId DESC
                   
                   
                   
                   
                   ----------------END OF NEW------------------------------
                        
                        
                        
                        
                        

                END
    END
GO