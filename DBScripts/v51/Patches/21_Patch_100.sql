﻿--This script is to update the patches for v5.1
/*
List of changed objects
1. PRProductSKU is added with a new column [MaximumDiscountPercentage] money null
2. Customer_Get
3. OrderItem_GetSKUAttributeValues
4. Product_GetProductAndSkuByXmlGuidsForProductIndexer
5. Product_GetEffectivePrice
6. Bundle_GetBundleItems
7. BundleItem_Get
8. Product_GetItemMasterUpdates
9. Product_GetProductAndSkuByType
10. Product_GetProductAndSkuByXmlGuids
11. Product_GetProductSKUList
12. Product_GetProductSKUListP
13. Product_GetSKU
14. Product_SimpleSearchSKU
15. ProductType_GetSkus
16. Sku_GetByXml
17. SKU_GetSKU
18. Sku_GetSKUByGuid
19. Sku_GetSkuProperties
20. SKU_Save
21. SKU_SaveMulti
22. Sku_SaveSKUAndAttributes
23. Sku_Update


*/

GO
--updating the incorrect value for the Maximum Order amount
update STSiteSetting set Value='2147483647' where SettingTypeId in (select Id from STSettingType where Name='MaximumOrderAmount')

GO

UPDATE STSiteSetting SET Value = 'VersionedImages\Workflow' WHERE SettingTypeId in (
SELECT Id FROM STSettingType WHERE Name like 'WFScreenshotLocation')

GO

DECLARE @SettingTypeId INT,@Sequence INT

SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'EnforceMaxDiscountPercentageOnCoupons')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('EnforceMaxDiscountPercentageOnCoupons', 1, 1, @Sequence)		
END

SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'EnforceMaxDiscountPercentageOnPriceSets')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('EnforceMaxDiscountPercentageOnPriceSets', 1, 1, @Sequence)		
END

SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'EnforceMaxDiscountPercentageOnSalePriceSets')
IF @SettingTypeId IS NULL 
BEGIN
    SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
    INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
    VALUES('EnforceMaxDiscountPercentageOnSalePriceSets', 1, 1, @Sequence)		
END
GO

IF COL_LENGTH(N'[dbo].[PRProductSKU]', N'MaximumDiscountPercentage') IS NULL
ALTER TABLE [dbo].[PRProductSKU] ADD  [MaximumDiscountPercentage] MONEY NULL

GO
ALTER PROCEDURE [dbo].[Customer_Get]
(
      @Id uniqueidentifier=null,
      @CustomerIds xml=null,
      @UserId uniqueidentifier = null,
      @UserName nvarchar(256) =null,
      @Email      nvarchar(256)=null,
	  @AccountNumber nvarchar(256)=null,
      @isExpress  bit =null,
      @ApplicationId uniqueidentifier =null
)
as
begin
      IF @UserName is null AND @isExpress is null AND @Email is null AND @UserId is null and @AccountNumber is null
            BEGIN
            IF @CustomerIds is null
                  SELECT CP.[Id]
                          ,CP.[Id] as UserId
                          ,U.[FirstName]
                          ,U.[LastName]
                          ,U.[MiddleName]
                          ,U.[CompanyName]
                          ,U.[BirthDate]
                          ,U.[Gender]
                          ,CP.[IsBadCustomer]
                          ,CP.[IsActive]
                          ,CP.[IsOnMailingList]
                          ,CP.[Status]
                          ,CP.[CSRSecurityQuestion]
                          ,CP.[CSRSecurityPassword]
                          ,U.[HomePhone]
                          ,U.[MobilePhone]
                          ,U.[OtherPhone]
                          ,U.ImageId
                          ,CP.[AccountNumber]
                          ,CP.[IsExpressCustomer]
                          ,CP.ExternalId
                          ,CP.ExternalProfileId
                          ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                          , (Select count(*) from CSCustomerLogin where CustomerId= Isnull(@Id,CP.Id)) +1 as NumberOfLoginAccounts
                          , (Select count(*) from PTLineOfCreditInfo where CustomerId = IsnuLL(@Id, CP.Id) 
                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                   FROM [USCommerceUserProfile] CP
                   INNER JOIN USUser U on CP.Id = U.Id
            WHERE --CP.Id = Isnull(@Id,CP.Id)
				(@Id is null or CP.Id = @Id)
				 AND U.Status !=3
			ELSE
						  SELECT CP.[Id]
                          ,CP.[Id] as UserId
                          ,U.[FirstName]
                          ,U.[LastName]
                          ,U.[MiddleName]
                          ,U.[CompanyName]
                          ,U.[BirthDate]
                          ,U.[Gender]
                          ,CP.[IsBadCustomer]
                          ,CP.[IsActive]
                          ,CP.[IsOnMailingList]
                          ,CP.[Status]
                          ,CP.[CSRSecurityQuestion]
                          ,CP.[CSRSecurityPassword]
                          ,U.[HomePhone]
                          ,U.[MobilePhone]
                          ,U.[OtherPhone]
                          ,U.ImageId
                          ,CP.[AccountNumber]
                          ,CP.[IsExpressCustomer]
                          ,CP.ExternalId
                          ,CP.ExternalProfileId
                          ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                          , (Select count(*) from CSCustomerLogin where CustomerId= CP.Id) +1 as NumberOfLoginAccounts
                          , (Select count(*) from PTLineOfCreditInfo where CustomerId =  CP.Id 
                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                   FROM [USCommerceUserProfile] CP
                   INNER JOIN USUser U ON CP.Id = U.Id
                   INNER JOIN @CustomerIds.nodes('/GenericCollectionOfGuid/guid')tab(col) ON CP.Id =tab.col.value('text()[1]','uniqueidentifier')
                   Where U.Status !=3
            
            END
      ELSE
            BEGIN
                  if @UserId is null
                        begin
                                    select  @UserId = U.Id from USUser U 
                                    INNER JOIN USMembership M on M.UserId =U.Id
                                    INNER JOIN USSiteUser US ON US.UserId = U.Id
									LEFT JOIN USCommerceUserProfile UCP ON UCP.Id = U.Id
                                     WHERE --(U.UserName) = (isnull(@UserName,U.UserName))
												(@UserName is null or U.UserName = @UserName)
                                                AND 
                                                (@Email is null OR M.Email = @Email)
												AND
												(@AccountNumber is null OR UCP.AccountNumber = @AccountNumber)
												and U.Status <> dbo.GetDeleteStatus() 
                                                AND (@ApplicationId is null OR US.SiteID = @ApplicationId)
                                                
                                     
                        end
						
                  
                  if(@UserId is not null)
                        begin
                              SELECT CP.[Id]
                                      ,@UserId as UserId
                                      ,U.[FirstName]
                                      ,U.[LastName]
                                      ,U.[MiddleName]
                                      ,U.[CompanyName]
                                      ,U.[BirthDate]
                                      ,U.[Gender]
                                      ,CP.[IsBadCustomer]
                                      ,CP.[IsActive]
                                      ,CP.[IsOnMailingList]
                                      ,CP.[Status]
                                      ,CP.[CSRSecurityQuestion]
                                      ,CP.[CSRSecurityPassword]
                                      ,U.[HomePhone]
                                      ,U.[MobilePhone]
                                      ,U.[OtherPhone]
                                      ,U.ImageId
                                      ,CP.[AccountNumber]
                                      ,CP.[IsExpressCustomer]
                                      ,CP.[ModifiedDate]
                                      ,CP.ExternalId
									  ,CP.ExternalProfileId
                                      ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                                        , (Select count(*) from CSCustomerLogin where CustomerId= Isnull(@Id,CP.Id)) +1 as NumberOfLoginAccounts
                                       , (Select count(*) from PTLineOfCreditInfo where CustomerId = IsnuLL(@Id, CP.Id)
                                                                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                               FROM [USCommerceUserProfile] CP
                               INNER JOIN USUser U ON CP.Id = U.Id
                               WHERE (CP.Id = (Select CustomerId from CSCustomerLogin where UserId = @UserId) OR CP.Id = @UserId )
                                    --AND IsExpressCustomer =isnull(@isExpress,IsExpressCustomer) 
									AND (@isExpress is null or IsExpressCustomer = @isExpress)
									AND U.Status != 3
                        end
            END  
end

GO
ALTER PROCEDURE [dbo].[OrderItem_GetSKUAttributeValues]
    (
      @OrderId UNIQUEIDENTIFIER = NULL ,
      @OrderItemId UNIQUEIDENTIFIER = NULL ,
      @CartId UNIQUEIDENTIFIER = NULL ,
      @CartItemId UNIQUEIDENTIFIER = NULL ,
      @ApplicationId UNIQUEIDENTIFIER
    )
AS 
    BEGIN

        IF ( @OrderId = '00000000-0000-0000-0000-000000000000' ) 
            SET @OrderId = NULL

        IF @OrderItemId = '00000000-0000-0000-0000-000000000000' 
            SET @OrderItemId = NULL

        IF ( @CartId = '00000000-0000-0000-0000-000000000000' ) 
            SET @CartId = NULL

        IF @CartItemId = '00000000-0000-0000-0000-000000000000' 
            SET @CartItemId = NULL

        SELECT  [OrderId] ,
                [OrderItemId] ,
                [SKUId] ,
                [ProductId] ,
                [CustomerId] ,
                [ProductName] ,
                [AttributeId] ,
                [AttributeName] ,
                [AttributeEnumId] ,
                [Value] ,
                [Code] ,
                [NumericValue] ,
                [Sequence] ,
                [IsDefault] ,
                [CartId] ,
                [CartItemID] ,
                [AttributeGroupId] ,
                [IsFaceted] ,
                [IsPersonalized] ,
                [IsSearched] ,
                [IsSystem] ,
                [IsDisplayed] ,
                [IsEnum] ,
                [AttributeDataTypeId]
        FROM    [dbo].[VWOrderItemPersonalizedAttribute]
        WHERE   ( ( OrderId = @OrderId
                    AND OrderId IS NOT NULL
                  )
                  OR @OrderId IS NULL
                )
                AND ( ( OrderItemId = @OrderItemId
                        AND OrderItemID IS NOT NULL
                      )
                      OR @OrderItemId IS NULL
                    )
                AND ( ( CartId = @CartId
                        AND CartId IS NOT NULL
                      )
                      OR @CartId IS NULL
                    )
                AND ( ( CartItemID = @CartItemID
                        AND CartItemId IS NOT NULL
                      )
                      OR @CartItemId IS NULL
                    )


    END
GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: get a collection of products by a given list of IDs  
-- Author:   
-- Created Date:03-23-2009  
-- Created By:Prakash  
  
-- Modified Date: 03-27-2009  
-- Modified By: Devin  
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  267181a5-2608-4bed-b59c-005baf3476ac
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuidsForProductIndexer]      
(      
		@ApplicationId uniqueidentifier=null
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      
      
Insert Into @tempXml(ProductId)      
Select distinct ProductId as Id from PRProductSKU where IsActive =1 and IsOnline=1  
      
 SELECT   
 DISTINCT     
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 [PrdSeo].FriendlyUrl as SEOFriendlyUrl,
 [S].TaxCategoryID        
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
  LEFT JOIN PRProductSEO PrdSeo ON S.Id = PrdSeo.ProductId   
--Order By T.Sno      
  
  
Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,[SiteId]    
      ,[SKU]    
   ,[Title]    
      ,[Description]    
      ,[Sequence]    
      ,[IsActive]    
      ,[IsOnline]    
      ,[ListPrice]    
      ,[UnitCost]    
      ,[PreviousSoldCount]    
   ,[CreatedBy]    
   ,[CreatedDate]    
   ,[ModifiedBy]    
   ,[ModifiedDate]    
   ,[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges]  
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]    
    ,[AvailableForBackOrder]
	,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  FROM [PRProductSKU] S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  
  
Select   
 SA.Id,  
 SA.SKUId,  
 PA.ProductId,
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
  
  
  
  
select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
	A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=dbo.GetActiveStatus()    and  A.IsSystem=0 and A.IsFaceted=1
  

SELECT P.Id AS ProductId,	CASE WHEN Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice) WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
 ELSE Min(ListPrice) END  as Price ,  cast( SUM(PS.PreviousSoldCount) as int) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
					LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
									) PP ON P.Id= PP.ProductId 	
					INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
					PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser 

  
END


GO

ALTER PROCEDURE [dbo].[Product_GetEffectivePrice](
			 @CustomerId uniqueidentifier = null,
			 @Quantity float = 1,
			 @ProductId uniqueidentifier = null,
			 @ProductIdList Xml = null,
			 @ProductIdQuantityList XML =null,
			 @ApplicationId uniqueidentifier=null
										)

AS

BEGIN
--<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>
declare @DefaultGroupId uniqueidentifier
Declare @tempProductTable table(Sno int Identity(1,1),ProductId uniqueidentifier)
Declare @Today DateTime
SET @Today=  cast(convert(varchar(12),dbo.GetLocalDate(@ApplicationId)) as datetime)
SET @Today = dbo.ConvertTimeToUtc(@Today,@ApplicationId)
IF @ProductIdQuantityList is null
BEGIN

	if @ProductIdList is not null
		begin
				Insert Into @tempProductTable(ProductId)
					Select tab.col.value('text()[1]','uniqueidentifier')
					FROM @ProductIdList.nodes('/ProductCollection/Product')tab(col)
		end
	else if(@ProductId is not null)
		begin
			
			Insert Into @tempProductTable(ProductId)
			Select @ProductId

		end

	if @@ROWCOUNT = 0 
		BEGIN
			RAISERROR ( 'No Product Id supplied' , 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			
			 Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
				(@ApplicationId is null or STS.SiteId = @ApplicationId))
				--STS.SiteId=IsNull(@ApplicationId,STS.SiteId))

			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId, IsSale ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join @tempProductTable inProduct on PSC.ProductId = inProduct.ProductId
				Where 
					CGPS.CustomerGroupId = @DefaultGroupId and 
					ps.SiteId = @ApplicationId and
					((@Quantity is null and MinQuantity <= 1 and MaxQuantity >= 1) OR
						(@Quantity is not null and MinQuantity <= @Quantity and MaxQuantity >= @Quantity))
					--(MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))	
					AND @Today Between PS.StartDate AND PS.EndDate 
					
			) EFPS
			WHERE EFPS.Sno=1	
		End
	else
		Begin
			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId, IsSale ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					inner join @tempProductTable inProduct on PSC.ProductId = inProduct.ProductId
				Where USMG.MemberId = @CustomerId  and
					ps.SiteId = @ApplicationId 
					and ((@Quantity is null and MinQuantity <= 1 and MaxQuantity >= 1) OR
						(@Quantity is not null and MinQuantity <= @Quantity and MaxQuantity >= @Quantity))
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1				
		End
		-- in any scenario - return List Price for all the SKUs		
		Select prSku.ProductId, Id as SkuId, ListPrice 
			From PRProductSKU prSku 
			where prSku.ProductId in(Select ProductId from @tempProductTable)
	
	END
	ELSE
	BEGIN
--<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>
	Declare @tableproductIdQnty	table(Sno int identity(1,1),ProductId uniqueidentifier,Quantity decimal(18,2))
	--Declare @ProductIdQuantityList XML 
	--Set @ProductIdQuantityList ='<dictionary><SerializableDictionary><item><key><guid>75610a9c-cc9d-4da5-893a-05fd0056d7cd</guid></key><value><double>2</double></value></item><item><key><guid>c15dd7bf-ce90-4a7b-be78-20b24324957e</guid></key><value><double>1</double></value></item><item><key><guid>1cc48359-eec5-44b4-9518-0c06398f5b92</guid></key><value><double>1</double></value></item><item><key><guid>0ae206dc-1b24-4f75-a174-56cdb59d5346</guid></key><value><double>2</double></value></item></SerializableDictionary></dictionary>'
	Insert into @tableproductIdQnty(ProductId,Quantity)
	Select tab.col.value('(key/guid/text())[1]','uniqueidentifier'),tab.col.value('(value/double/text())[1]','decimal(18,2)')
	From @ProductIdQuantityList.nodes('dictionary/SerializableDictionary/item') tab(col)
	
	if(@CustomerId is null Or @CustomerId='00000000-0000-0000-0000-000000000000')
		Begin
			Set @DefaultGroupId= (SELECT top 1 [Value] from STSiteSetting STS inner join STSettingType STT on STS.SettingTypeId = STT.Id
			where STT.Name='CommerceEveryOneGroupId' and 
					(@ApplicationId is null or STS.SiteId = @ApplicationId))
					--STS.SiteId=IsNull(@ApplicationId,STS.SiteId))

			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join @tableproductIdQnty inProduct on PSC.ProductId = inProduct.ProductId AND inProduct.Quantity between minQuantity and maxQuantity
				Where 
					CGPS.CustomerGroupId = @DefaultGroupId 
					and PS.SiteId = @ApplicationId 
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1	
		End
		else
		Begin
			Select EFPS.* 
			FROM
			(
				Select PSC.SkuId,PSC.ProductId,PSC.MinQuantity, PSC.MaxQuantity, PSC.PriceSetId,
					PS.IsSale, PSC.EffectivePrice,
					(ROW_NUMBER() OVER(PARTITION BY PSC.SkuId ORDER BY PSC.EffectivePrice ASC))  as Sno
				from dbo.vwEffectivePriceSets PSC 
					inner join dbo.CSCustomerGroupPriceSet CGPS on PSC.PriceSetId = CGPS.PriceSetId 
					inner join PSPriceSet PS on PSC.PriceSetId = PS.Id
					inner join USMemberGroup USMG on CGPS.CustomerGroupId = USMG.GroupId
					inner join @tableproductIdQnty inProduct on PSC.ProductId = inProduct.ProductId AND inProduct.Quantity between minQuantity and maxQuantity
				Where USMG.MemberId = @CustomerId 
				and PS.SiteId = @ApplicationId 
					--and (MinQuantity <= isnull(@Quantity,1) and MaxQuantity >= isnull(@Quantity,1))		
					AND @Today Between PS.StartDate AND PS.EndDate 		
			) EFPS
			WHERE EFPS.Sno=1				
		End
-- in any scenario - return List Price for all the SKUs		
		Select prSku.ProductId, Id as SkuId, ListPrice 
			From PRProductSKU prSku 
			where prSku.ProductId in(Select ProductId from @tableproductIdQnty)
	
	END

END
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets the items associated with a bundle Id  
-- Author:   
-- Created Date:05/1/2009  
-- Created By:Devin   
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  
ALTER PROCEDURE [dbo].[Bundle_GetBundleItems](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @Id uniqueidentifier = null  
   ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
 SELECT s.[Id]  
      ,s.[ProductId]  
      ,s.[SiteId]  
      ,s.[SKU]  
	,s.[Id] As SkuId
   ,s.[Title]  
      ,s.[Description]  
      ,s.[Sequence]  
      ,s.[IsActive]  
      ,s.[IsOnline]  
      ,s.[ListPrice]  
      ,s.[UnitCost]  
      ,s.[PreviousSoldCount]  
   ,s.[CreatedBy]  
   ,s.[CreatedDate]
   ,s.[ModifiedBy]  
   ,s.ModifiedDate
   ,s.[Status]  
   ,s.[Measure]  
   ,s.[OrderMinimum]  
   ,s.[OrderIncrement] 
   ,s.[Length]
   ,s.[Height]
   ,s.[Width]
   ,s.[Weight]
   ,s.[SelfShippable]  
   ,s.AvailableForBackOrder
   ,s.MaximumDiscountPercentage
   ,s.BackOrderLimit
	,s.IsUnlimitedQuantity
   ,bs.[BundleItemId]  
   ,bi.[BundleProductId]  
   ,bs.[ChildSkuId]  
   ,bi.[ChildSkuQuantity]  
   ,bi.[CreatedBy] as [BundleSkuCreatedBy]  
   ,bi.[CreatedDate] as [BundleSkuCreatedDate]  
   ,bi.[ModifiedBy] as [BundleSkuModifiedBy]  
   ,bi.[ModifiedDate] as [BundleSkuModifiedDate],
   (SELECT 
		
		
		sum(Quantity)
	From 
		[dbo].[vwAllocatedQuantityPerWarehouse] A INNER JOIN INWarehouseSite SI on A.WarehouseId = SI.WarehouseId Where ProductSKUId = s.[Id] AND SI.SiteId=@ApplicationId )
	 as AllocatedQuantity,
   (SELECT		
		CASE s.IsUnlimitedQuantity
				WHEN 1 then 9999999
				ELSE sum(Quantity)
			END		
	From 
		INInventory I Inner join INWarehouseSite SI on I.WarehouseId = SI.WarehouseId Where SKUId = s.[Id]  AND SI.SiteId=@ApplicationId)
	 as QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
 FROM  PRBundleItemSku bs left join PRProductSKU s on bs.[ChildSkuId]=s.[Id] left join PRBundleItem bi on bs.[BundleItemId]=bi.Id 
 WHERE  
  bi.[BundleProductId]=@Id  
  AND  
  Status = dbo.GetActiveStatus() -- Select only Active Attributes  
 Order By bi.[Id]  
END
GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets the items associated with a bundle Id  
-- Author:   
-- Created Date:05/1/2009  
-- Created By:Devin   
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  
ALTER PROCEDURE [dbo].[BundleItem_Get](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @Id uniqueidentifier = null  
   ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
 SELECT s.[Id]  
      ,s.[ProductId]  
      ,s.[SiteId]  
      ,s.[SKU]  
	  ,s.[Id] As SkuId
	  ,s.[Title]  
      ,s.[Description]  
      ,s.[Sequence]  
      ,s.[IsActive]  
      ,s.[IsOnline]  
      ,s.[ListPrice]  
      ,s.[UnitCost]  
      ,s.[PreviousSoldCount]  
   ,s.[CreatedBy]  
   ,s.[CreatedDate]
   ,s.[ModifiedBy]  
   ,s.ModifiedDate
   ,s.[Status]  
   ,s.[Measure]  
   ,s.[OrderMinimum]  
   ,s.[OrderIncrement]  
   ,s.[Length]
	  ,s.[Height]
	  ,s.[Width]
	  ,s.[Weight]
	  ,s.[SelfShippable]  
	  ,s.AvailableForBackOrder
	  ,s.MaximumDiscountPercentage
	  ,s.BackOrderLimit
	,s.IsUnlimitedQuantity
   ,bs.[BundleItemId]  
   ,bi.[BundleProductId]  
   ,bs.[ChildSkuId]  
   ,bi.[ChildSkuQuantity]  
   ,bi.[CreatedBy] as [BundleSkuCreatedBy]  
   ,dbo.ConvertTimeFromUtc(bi.[CreatedDate],@ApplicationId) as [BundleSkuCreatedDate]  
   ,bi.[ModifiedBy] as [BundleSkuModifiedBy]  
   ,dbo.ConvertTimeFromUtc(bi.[ModifiedDate] ,@ApplicationId)as [BundleSkuModifiedDate],
   (SELECT sum(Quantity) From vwAllocatedQunatity Where ProductSKUId = s.[Id] ) as AllocatedQuantity,
   (SELECT sum(Quantity) From INInventory Where SKUId = s.[Id] ) as QuantityOnHand    --- sum of all warehouse -- find bundle item wise minimum in API or helper
 FROM  PRBundleItemSku bs left join PRProductSKU s on bs.[ChildSkuId]=s.[Id] left join PRBundleItem bi on bs.[BundleItemId]=bi.Id 
 WHERE  
  bi.[Id]=@Id  
  AND  
  Status = dbo.GetActiveStatus() -- Select only Active Attributes  
 Order By bs.[Id]  
END


GO
ALTER PROCEDURE [dbo].[Product_GetItemMasterUpdates]
AS
BEGIN
	DECLARE @LastExported datetime
	SET @LastExported = (SELECT top 1 ExportDate FROM PRItemMasterExportHistory Order By ExportDate DESC)
	
	Declare @tbSkus TABLE (Id uniqueidentifier, ProductId uniqueidentifier, Status int)
	
	INSERT INTO @tbSkus
	Select DISTINCT S.Id, P.Id, 1
	FROM PRProductSku S JOIN PRProduct P ON S.ProductId = P.Id
	WHERE (@LastExported IS NULL OR S.CreatedDate >= @LastExported)
	
	UNION ALL

	Select DISTINCT S.Id, P.Id, 2
	FROM PRProductSku S JOIN PRProduct P ON S.ProductId = P.Id
	WHERE @LastExported IS NOT NULL AND 
		((S.CreatedDate < @LastExported AND S.ModifiedDate >= @LastExported) OR
		(P.CreatedDate < @LastExported AND P.ModifiedDate >= @LastExported))
	
	SELECT * FROM @tbSkus
	
	SELECT DISTINCT     
		[Prd].[Id],    
		[Prd].ProductIDUser,    
		[Prd].[Title],    
		[Prd].[Keyword],    
		[Prd].[IsActive],    
		[Prd].[ProductTypeID],    
		[Prd].[ProductStyle],    
		[Prd].Status  
	FROM PRProduct Prd INNER JOIN @tbSkus T on [T].ProductId = Prd.Id   
	
	SELECT 
		S.Id,  
		S.ProductId,  
		S.SiteId,  
		S.Title,  
		S.SKU,
		S.Description,  
		S.Sequence,  
		S.IsActive,  
		S.IsOnline,  
		S.ListPrice,  
		S.UnitCost,  
		S.Measure,
		S.OrderMinimum,
		S.OrderIncrement,	
		S.HandlingCharges, 
		S.PreviousSoldCount,  
		S.CreatedBy,  
		S.ModifiedBy,
		S.[Length],
		S.[Height],
		S.[Width],
		S.[Weight],
		S.[SelfShippable] ,
		S.[AvailableForBackOrder],
		S.MaximumDiscountPercentage,
		S.[BackOrderLimit],
		S.[IsUnlimitedQuantity]
	FROM PRProductSKU S INNER JOIN @tbSkus T on [T].Id = S.Id   

END
GO

IF OBJECT_ID('[dbo].[Product_GetProductAndSkuByType]') IS  NULL
EXEC sp_executesql N'/*****************************************************
* Name : Product_GetProductAndSkuByType
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Product_GetProductAndSkuByType]      
	(      
	 @ProductTypeID uniqueidentifier
	,@ApplicationId uniqueidentifier=null

	)      
	AS      

	--********************************************************************************      
	-- Variable declaration      
	--********************************************************************************      
	BEGIN      
	--********************************************************************************      
	-- code      
	--********************************************************************************      
	   

	 SELECT        
	 S.[Id],      
	 S.[ProductIDUser],      
	 S.[Title],      
	 S.[ShortDescription],      
	 S.[LongDescription],      
	 S.[Description],      
	 S.[Keyword],      
	 S.[IsActive],      
	 S.[ProductTypeID],      
	 vPT.[ProductTypeName] as ProductTypeName,      
	 vPT.[ProductTypePath] + N''id-'' + S.ProductIDUser + ''/'' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
	vPT.[IsDownloadableMedia], S.[ProductStyle],      
	 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
	 S.CreatedDate,      
	 S.CreatedBy,      
	 S.ModifiedDate,      
	 S.ModifiedBy,      
	 S.Status,      
	 S.IsBundle,      
		dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
	 [PrdSeo].FriendlyUrl as SEOFriendlyUrl,
	S.TaxCategoryId  
	 FROM PRProduct S       
	  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
	  LEFT JOIN PRProductSEO PrdSeo ON S.Id = PrdSeo.ProductId      
	  Where S.ProductTypeID =@ProductTypeID
	--Order By T.Sno      


	Select   
	 SA.Id,  
	 SA.AttributeId,  
	 SA.AttributeEnumId,  
	 AE.NumericValue,  
	 IsNull(AE.[IsDefault], 0) as [IsDefault],  
	 AE.Sequence,  
	 SA.Value,  
	 AE.Code,  
	 SA.CreatedDate,  
	 SA.CreatedBy,  
	 SA.ModifiedDate,  
	 SA.ModifiedBy,  
	 SA.Status,
	 SA.ProductId
	from PRProduct t   
	Inner Join PRProductAttributeValue SA  On SA.ProductId=t.Id  
	Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
	Where SA.Status = dbo.GetActiveStatus()  
	AND t.ProductTypeID =@ProductTypeID


	SELECT S.[Id]    
		  ,S.[ProductId]    
		  ,S.[SiteId]    
		  ,[SKU]    
	   ,S.[Title]    
		  ,S.[Description]    
		  ,S.[Sequence]    
		  ,S.[IsActive]    
		  ,[IsOnline]    
		  ,[ListPrice]    
		  ,[UnitCost]    
		  ,[PreviousSoldCount]    
	   ,S.[CreatedBy]    
	   ,S.[CreatedDate]    
	   ,S.[ModifiedBy]    
	   ,S.[ModifiedDate]    
	   ,S.[Status]    
	   ,[Measure]    
	   ,[OrderMinimum]    
	   ,[OrderIncrement]  
	   ,[HandlingCharges] 
		,[Length]
		  ,[Height]
		  ,[Width]
		  ,[Weight]
		  ,[SelfShippable]    
		  ,[AvailableForBackOrder]
		  ,MaximumDiscountPercentage
		  ,[BackOrderLimit]
		,[IsUnlimitedQuantity]
	  FROM [PRProductSKU] S   
	  Inner Join PRProduct t on S.ProductId=t.Id  
	  Where t.ProductTypeID =@ProductTypeID



	Select   
	 SA.Id,  
	 SA.SKUId,  
	 SA.AttributeId,  
	 SA.AttributeEnumId,  
	 AE.NumericValue,  
	 IsNull(AE.[IsDefault], 0) as [IsDefault],  
	 AE.Sequence,  
	 SA.Value,  
	 AE.Code,  
	 SA.CreatedDate,  
	 SA.CreatedBy,  
	 SA.ModifiedDate,  
	 SA.ModifiedBy,  
	 SA.Status  
	from PRProduct t   
	Inner Join PRProductAttribute PA  On PA.ProductId=t.Id  
	Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
	Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
	Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
	Where IsSKULevel=1   
	AND SA.Status = dbo.GetActiveStatus()  
	and t.ProductTypeID =@ProductTypeID
	--Order By SA.AttributeId  




	select  
	  A.Id,  
	  A.AttributeGroupId,  
	  A.AttributeDataType,  
	  A.Title,  
	  A.Description,  
	  A.Sequence,  
	  A.Code,  
	  A.IsFaceted,  
	  A.IsSearched,  
	  A.IsSystem,  
	  A.IsDisplayed,  
	  A.IsEnum,    
	  A.IsPersonalized,
	  A.CreatedDate,  
	  A.CreatedBy,  
	  A.ModifiedDate,  
	  A.ModifiedBy,  
	  A.Status,  
	  PA.IsSKULevel,  
	  PA.ProductId,  
	  A.IsSystem   , A.IsPersonalized
	from PRProduct t   
	Inner Join PRProductAttribute PA  On PA.ProductId=t.Id   
	Inner Join ATAttribute A on PA.AttributeId = A.Id   
	where  A.Status=dbo.GetActiveStatus()     
	and t.ProductTypeID =@ProductTypeID

	END

'
	
GO

ALTER PROCEDURE [dbo].[Product_GetProductAndSkuByType]      
(      
 @ProductTypeID uniqueidentifier
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
   

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 [PrdSeo].FriendlyUrl as SEOFriendlyUrl,
S.TaxCategoryId  
 FROM PRProduct S       
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
  LEFT JOIN PRProductSEO PrdSeo ON S.Id = PrdSeo.ProductId      
  Where S.ProductTypeID =@ProductTypeID
--Order By T.Sno      


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from PRProduct t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.Id  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
AND t.ProductTypeID =@ProductTypeID


SELECT S.[Id]    
      ,S.[ProductId]    
      ,S.[SiteId]    
      ,[SKU]    
   ,S.[Title]    
      ,S.[Description]    
      ,S.[Sequence]    
      ,S.[IsActive]    
      ,[IsOnline]    
      ,[ListPrice]    
      ,[UnitCost]    
      ,[PreviousSoldCount]    
   ,S.[CreatedBy]    
   ,S.[CreatedDate]    
   ,S.[ModifiedBy]    
   ,S.[ModifiedDate]    
   ,S.[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges] 
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]    
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  FROM [PRProductSKU] S   
  Inner Join PRProduct t on S.ProductId=t.Id  
  Where t.ProductTypeID =@ProductTypeID



Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from PRProduct t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.Id  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
and t.ProductTypeID =@ProductTypeID
--Order By SA.AttributeId  




select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from PRProduct t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.Id   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=dbo.GetActiveStatus()     
and t.ProductTypeID =@ProductTypeID

END
GO

-- Stored Procedure

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  

-- Purpose: get a collection of products by a given list of IDs  
-- Author:   
-- Created Date:03-23-2009  
-- Created By:Prakash  

-- Modified Date: 03-27-2009  
-- Modified By: Devin  
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  


ALTER PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuids]      
(      
 @Id Xml  
,@ApplicationId uniqueidentifier=null

)      
AS      

--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      

Insert Into @tempXml(ProductId)      
Select tab.col.value('text()[1]','uniqueidentifier')      
 FROM @Id.nodes('/ProductCollection/Product')tab(col)      

 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
vPT.[IsDownloadableMedia], S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 [PrdSeo].FriendlyUrl as SEOFriendlyUrl,
S.TaxCategoryId  
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
  LEFT JOIN PRProductSEO PrdSeo ON S.Id = PrdSeo.ProductId      
--Order By T.Sno      


Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  


SELECT [Id]    
      ,S.[ProductId]    
      ,[SiteId]    
      ,[SKU]    
   ,[Title]    
      ,[Description]    
      ,[Sequence]    
      ,[IsActive]    
      ,[IsOnline]    
      ,[ListPrice]    
      ,[UnitCost]    
      ,[PreviousSoldCount]    
   ,[CreatedBy]    
   ,[CreatedDate]    
   ,[ModifiedBy]    
   ,[ModifiedDate]    
   ,[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges] 
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]    
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  FROM [PRProductSKU] S   

  Inner Join @tempXml t on S.ProductId=t.ProductId  



Select   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On ( PS.Id = SA.SKUId  AND SA.ProductAttributeID = PA.ID)  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  




select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=dbo.GetActiveStatus()     


END
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of Product based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 08-04-2009
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Product_GetProductSKUList 
--			@ApplicationId	= uniqueidentifier null ,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Product_GetProductSKUList](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@PageIndex		int = null,
			@PageSize		int = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - Isnull(@PageSize,0) + 1;
	
		WITH ResultEntries AS
		(
          SELECT 
					ROW_NUMBER() OVER (ORDER BY SKU.Title) AS RowNumber,
					SKU.Id, SKU.ProductId, SKU.SiteId, SKU.Title, SKU.SKU, SKU.Description, 
					SKU.Sequence, SKU.IsActive, case SKU.IsActive WHEN 1 THEN 'Active' ELSE 'Inactive' END  As IsActiveDescription ,  SKU.IsOnline, SKU.ListPrice, SKU.UnitCost, 
					SKU.ListPrice As SalePrice, SKU.WholesalePrice As WholesalePrice, 
					inv.Quantity As Quantity,
					SKU.HandlingCharges,  SKU.Length,SKU.Height,SKU.Width,SKU.Weight,SKU.SelfShippable,SKU.AvailableForBackOrder, SKU.MaximumDiscountPercentage, SKU.BackOrderLimit,
					Product.Title As ProductTitle, Type.Title	As TypeTitle,  dbo.GetProductCategoryTitleCollection(Product.Id)  As CategoryTitle, dbo.GetProductCategoryCollection(Product.Id)  As CategoryIdsAndTitles,
					IsUnlimitedQuantity as IsUnlimited
					--dbo.IsSkuUnlimited(SKU.Id) as IsUnlimited
			FROM PRProductSKU As SKU
			INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN [vwInventoryPerSite] inv on SKU.Id = inv.SKUId AND inv.SiteId=@ApplicationId
            WHERE	
					Product.IsBundle = 0
		)

		SELECT * 
		FROM ResultEntries 
		WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))  Order by Sequence

END
GO

ALTER PROCEDURE [dbo].[Product_GetProductSKUListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(50)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null	
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 


--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		
set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
SET @SortColumn = lower(rtrim(ltrim((@SortColumn))))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256)) +1
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256)) + 1



--SET @SearchKeyword = ''

DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	
  
ELSE IF @SortColumn = 'sku desc'
	SET @SortColumn = 'SKU.SKU DESC'  
ELSE IF @SortColumn = 'sku asc'
	SET @SortColumn = 'SKU.SKU ASC'  

ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'listprice desc'
	SET @SortColumn = 'SKU.ListPrice DESC'  
ELSE IF @SortColumn = 'listprice asc'
	SET @SortColumn = 'SKU.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'inv.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'inv.Quantity ASC'

ELSE IF @SortColumn = 'availabletosell desc'
	SET @SortColumn = 'avail.Quantity DESC'  
ELSE IF @SortColumn = 'availabletosell asc'
	SET @SortColumn = 'avail.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.SKU) Like ''%''+'''+@SearchKeyword+'''+''%''
					) '
					
IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';


SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					SKU.Id, 
					SKU.ProductId,
					SKU.SiteId, 
					SKU.Title, 
					SKU.SKU, 
					SKU.Description, 
					SKU.Sequence, 
					SKU.IsActive, 
					case SKU.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,  
					SKU.IsOnline, 
					SKU.ListPrice, 
					SKU.UnitCost, 
					SKU.ListPrice As SalePrice, 
					SKU.WholesalePrice As WholesalePrice, 
					IsNull(inv.Quantity,0) as Quantity,
					avail.Quantity as AvailableToSell,
					SKU.HandlingCharges,
					Product.Title As ProductTitle, 
					Product.ProductIDUser As ProductIDUser,
					Type.Title	As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  
					As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  
					 As CategoryIdsAndTitles,
					IsUnlimitedQuantity AS IsUnlimited,
					SKU.Length,SKU.Height,SKU.Width,SKU.Weight,SKU.SelfShippable,SKU.AvailableForBackOrder, SKU.MaximumDiscountPercentage,SKU.BackOrderLimit,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia
			FROM PRProductSKU As SKU
			INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN [vwInventoryPerSite] inv on SKU.Id = inv.SKUId AND inv.SiteId=''' + @ApplicationIdVar + '''
			LEFT JOIN vwAvailableToSellPerWarehouse avail ON SKU.Id = avail.SKUId AND avail.SiteId=''' + @ApplicationIdVar + '''
            WHERE	
				Product.IsBundle = 0		
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'


EXEC sp_executesql   @DynamicSQL 

SET @DynamicSQL = N'
SELECT  
	Count(*)
FROM 
	PRProductSKU As SKU
	INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
	INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
WHERE	
		Product.IsBundle = 0
		'+@DynamicWhereClause

EXEC sp_executesql   @DynamicSQL 

END

GO

ALTER PROCEDURE [dbo].[Product_GetSKU]  
(  
 @ProductId uniqueidentifier  = null
 ,@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
IF @ProductId is not null
select  
  Id,  
  ProductId,  
  SiteId,  
  Title,  
  SKU,  
  Description,  
  Sequence,  
  IsActive,  
  IsOnline,  
  ListPrice,  
  UnitCost,  
  Measure,
  OrderMinimum,
  OrderIncrement,	HandlingCharges, 
  PreviousSoldCount,  
  dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,  
  CreatedBy,  
  dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,  
  ModifiedBy
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable] 
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  from PRProductSKU 
  where ProductId=@ProductId and Status=dbo.GetActiveStatus()  
  Order by Sequence, Title  
ELSE
 select  
  Id,  
  ProductId,  
  SiteId,  
  Title,  
  SKU,  
  Description,  
  Sequence,  
  IsActive,  
  IsOnline,  
  ListPrice,  
  UnitCost,  
  Measure,
  OrderMinimum,
  OrderIncrement,	HandlingCharges, 
  PreviousSoldCount,  
  dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,  
  CreatedBy,  
  dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,  
  ModifiedBy  
,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable] 
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  from PRProductSKU where 
		--ProductId=Isnull(@ProductId,ProductId) 
		(@ProductId is null or ProductId = @ProductId)
		and Status=dbo.GetActiveStatus()  
  Order by Sequence, Title  
END

GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: gets All the catalog in the system  
-- Author:   
-- Created Date:04/22/2009  
-- Created By:Arun  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  ALTER PROCEDURE [dbo].[Product_SimpleSearchSKU]  
(
	@SearchTerm nvarchar(max),
	@IsBundle tinyint = null,
	@pageNumber		int = null,
	@pageSize		int = null,
	@SortColumn VARCHAR(25)=null,
	@ProductTypeId uniqueidentifier=null,
	@ApplicationId uniqueidentifier=null
 )  
AS  
BEGIN  
	DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 

	SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
	SET @PageUpperBound = @PageLowerBound - Isnull(@pageSize,0) + 1;

	DECLARE @searchText nvarchar(max)  
	SET @searchText = '%' + @SearchTerm + '%'
	IF @SortColumn IS NULL
		SET @SortColumn = 'productname asc'
	SET @SortColumn = LOWER(@SortColumn)

	DECLARE @tbSku Table(RowNumber int identity(1,1), Id uniqueidentifier, ProductId uniqueidentifier, ProductName nvarchar(2000), 
			Qty decimal(18,4), AllocatedQty decimal(18,4), ProductTypeName nvarchar(1000))
	
	INSERT INTO @tbSku
	SELECT S.Id, P.Id, P.Title, Isnull(dbo.GetBundleQuantityPerSite(I.SKUId,@ApplicationId) ,I.[Quantity]) , ISNULL(A.Quantity,0), T.Title
	FROM PRProduct P 
			JOIN PRProductSku S ON S.ProductId = P.Id
			JOIN PRProductType T ON T.Id = P.ProductTypeId
			Left Join vwInventoryPerSite I ON I.SkuId = S.Id AND I.SiteId=@ApplicationId
			Left Join (Select ProductSKUId SkuId,sum(Quantity)Quantity from vwAllocatedQuantityperWarehouse AL INNER JOIN INWarehouseSite WS on WS.WarehouseId = AL.WarehouseId AND WS.SiteId =@ApplicationId Group By AL.ProductSKUId) A ON A.SkuId = S.Id 
	WHERE
		--P.SiteId = @ApplicationId AND
		(@ProductTypeId IS NULL OR T.Id = @ProductTypeId) AND
		(@IsBundle IS NULL OR P.IsBundle = @IsBundle) AND
		(P.Title like @searchText 
				OR P.ShortDescription like @searchText 
				OR P.LongDescription like @searchText   
				OR P.Description like @searchText
				OR S.SKU like @searchText)
	ORDER BY
		CASE WHEN @SortColumn = 'productname desc' Then P.Title END DESC, 	
		CASE WHEN @SortColumn = 'productname asc' Then P.Title END ASC, 	
		CASE WHEN @SortColumn = 'sku desc' Then S.SKU END DESC, 	
		CASE WHEN @SortColumn = 'sku asc' Then S.SKU END ASC, 
		CASE WHEN @SortColumn = 'quantity desc' Then I.Quantity END DESC, 	
		CASE WHEN @SortColumn = 'quantity asc' Then I.Quantity  END ASC,
		CASE WHEN @SortColumn = 'availabletosell desc' Then A.Quantity END DESC, 	
		CASE WHEN @SortColumn = 'availabletosell asc' Then A.Quantity END ASC

	SELECT 
			T.Id,
			T.ProductId,
			T.ProductName,
			T.Qty AS Quantity,
			T.Qty - T.AllocatedQty AS AvailableToSell,
			S.Title,
			S.SKU,
			case when T.Qty >=999999 then 1 else S.IsUnlimitedQuantity end IsUnlimitedQuantity,
			S.AvailableForBackOrder,
			S.MaximumDiscountPercentage,			
			ISNULL(S.BackOrderLimit, 0) AS BackOrderLimit,
			S.AvailableForBackOrder,
			CAST(
				CASE 
					WHEN S.IsActive = 1 AND PR.IsActive = 1
						THEN 1
					ELSE 0
				END AS Bit) as IsActive,		
			T.ProductTypeName
		FROM @tbSku T JOIN PrProductSku S ON S.Id = T.Id
		INNER JOIN PRProduct PR ON PR.Id = S.ProductID
		WHERE @pageSize is null OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)

	SELECT 		
		C.[Id],
		C.[Title],
		C.[Description],
		C.[FileName],
		C.[Size],
		C.[Attributes],
		C.[RelativePath],
		C.[AltText],
		C.[Height],
		C.[Width],
		C.[Status],
		C.[SiteId],
		OI.[ObjectId] AS ProductId,
		OI.[ObjectType],
		OI.[IsPrimary],
		OI.[Sequence],
		C.[ParentImage],
		C.ImageType,
		OI.ObjectId as ProductId
	FROM
		CLObjectImage OI INNER JOIN CLImage I on I.Id = OI.ImageId
		INNER JOIN CLImage C on (I.Id=C.ParentImage or I.Id=C.Id)
		INNER JOIN CLImageType IT ON IT.Id = C.ImageType
		INNER JOIN @tbSku T on T.ProductId = OI.ObjectId
	WHERE  
		--(@ApplicationId is null or I.SiteId = @ApplicationId)AND 
		I.Status = dbo.GetActiveStatus() AND I.Id is not null 
		AND ObjectType = 205 AND IT.TypeNumber = 0
		AND ((@pageSize is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		Order by OI.Sequence, C.Title
 
	Select Count(*) FROM @tbSku
END

GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
   
-- Purpose: To get the details of ProductType based on the parameters from the database.   
-- Author: Martin M V  
-- Created Date: 02-04-2009  
-- Created By: Martin M V  
--********************************************************************************  
-- List of all the changes with date and reason  
--********************************************************************************  
-- Modified Date:  
-- Modified By:  
-- Reason:  
-- Changes:  
  
--********************************************************************************  
-- Warm-up Script:  
--   exec [dbo].ProductType_GetSkus   
--          @Id = 'uniqueidentifier'  null,  
--   @ApplicationId = uniqueidentifier null
--********************************************************************************  
ALTER PROCEDURE [dbo].[ProductType_GetSkus](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @ProductTypeId    uniqueidentifier = null,  
   @ApplicationId uniqueidentifier = null  
)  
AS  
--********************************************************************************  
-- Procedure Body  
--********************************************************************************  
BEGIN  
		SELECT 
			sku.Id,
			sku.ProductId,
			sku.SiteId,
			sku.Title,
			sku.SKU,
			sku.Description,
			sku.Sequence,
			sku.IsActive,
			sku.IsOnline,
			sku.ListPrice,
			sku.UnitCost,
			sku.WholesalePrice,
			--sku.Quantity,
			sku.PreviousSoldCount,
			dbo.ConvertTimeFromUtc(sku.CreatedDate,@ApplicationId)CreatedDate,
			sku.CreatedBy,
			dbo.ConvertTimeFromUtc(sku.ModifiedDate,@ApplicationId)ModifiedDate,
			sku.ModifiedBy,
			sku.Status,
			sku.Measure,
			sku.OrderMinimum,
			sku.OrderIncrement,
			sku.HandlingCharges,
			sku.[Length],
			sku.[Height],
			sku.[Width],
			sku.[Weight],
			sku.[SelfShippable],  
			sku.AvailableForBackOrder,
			sku.MaximumDiscountPercentage,
			sku.BackOrderLimit,
			sku.IsUnlimitedQuantity,
			Product.Title As ProductTitle
		FROM PRProductSKU sku
		INNER JOIN PRProduct Product ON sku.ProductId = Product.Id
		INNER JOIN PRProductType ON Product.ProductTypeID =  PRProductType.Id
        WHERE 
				--PRProductType.[Id]   = Isnull(@ProductTypeId,PRProductType.[Id])   AND   
				--sku.SiteId  = Isnull(@ApplicationId, sku.SiteId)  AND  
				(@ProductTypeId is null or PRProductType.[Id] = @ProductTypeId) AND
				(@ApplicationId is null or sku.SiteId = @ApplicationId) AND
				sku.Status  = dbo.GetActiveStatus() 
		Order by  Product.Title, sku.SKU	  
END



GO
ALTER PROCEDURE [dbo].[Sku_GetByXml](@SKUs xml=null,@SKUString xml=null,@ApplicationId uniqueidentifier=null)  
  
AS  
  
  IF @SKUs is not null
SELECT [Id]  
      ,[ProductId]  
      ,[SiteId]  
      ,[SKU]  
	  ,[Title]  
      ,[Description]  
      ,[Sequence]  
      ,[IsActive]  
      ,[IsOnline]  
      ,[ListPrice]  
      ,[UnitCost]  
      ,[PreviousSoldCount]  
	  ,[CreatedBy]  
	  ,CreatedDate AS [CreatedDate]  
	  ,[ModifiedBy]  
	  ,ModifiedDate AS [ModifiedDate]  
	  ,[Status]  
	  ,[Measure]  
	  ,[OrderMinimum]  
	  ,[OrderIncrement] 
	  ,[HandlingCharges] 
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	  ,[IsUnlimitedQuantity]
  FROM [PRProductSKU] S  
Inner Join @SKUs.nodes('/GenericCollectionOfGuid/guid')tab(col)   
ON  S.Id =tab.col.value('text()[1]','uniqueidentifier')  
ELSE
SELECT [Id]  
      ,[ProductId]  
      ,[SiteId]  
      ,[SKU]  
	  ,[Title]  
      ,[Description]  
      ,[Sequence]  
      ,[IsActive]  
      ,[IsOnline]  
      ,[ListPrice]  
      ,[UnitCost]  
      ,[PreviousSoldCount]  
	  ,[CreatedBy]  
	 ,CreatedDate AS [CreatedDate]  
	  ,[ModifiedBy]  
	  ,ModifiedDate AS [ModifiedDate]  
	  ,[Status]  
	  ,[Measure]  
	  ,[OrderMinimum]  
	  ,[OrderIncrement] 
	  ,[HandlingCharges] 
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	  ,[AvailableForBackOrder]
	  ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	  ,[IsUnlimitedQuantity]
  FROM [PRProductSKU] S   
Inner Join @SKUString.nodes('/GenericCollectionOfString/string')tab(col)   
ON  S.SKU=tab.col.value('text()[1]','nvarchar(256)')

GO

ALTER PROCEDURE [dbo].[SKU_GetSKU](@productId uniqueidentifier,@ApplicationId uniqueidentifier=null)

AS

SELECT [ID]
      ,[ProductID]
      ,[SiteID]
      ,[SKU]
	  ,[Title]
      ,[Description]
      ,[Sequence]
      ,[IsActive]
      ,[IsOnline]
      ,[ListPrice]
      ,[UnitCost]
      ,[PreviousSoldCount]
	  ,[CreatedBy]
	  ,CreatedDate AS CreatedDate
	  ,[ModifiedBy]
	  ,ModifiedDate AS ModifiedDate
	  ,[Status]
	  ,[HandlingCharges]
	  ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]
	 ,[AvailableForBackOrder]
	 ,MaximumDiscountPercentage
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  FROM [ProductSKU]
Where ProductID = @productId

GO

ALTER PROCEDURE [dbo].[Sku_GetSKUByGuid](@skuId uniqueidentifier,@ApplicationId uniqueidentifier=null)  
  
AS  
  
SELECT [Id]  
		,[ProductId]  
		,[SiteId]  
		,[SKU]  
		,[Title]  
		,[Description]  
		,[Sequence]  
		,[IsActive]  
		,[IsOnline]  
		,[ListPrice]  
		,[UnitCost]  
		,[PreviousSoldCount]  
		,[CreatedBy]  
		,CreatedDate AS CreatedDate  
		,[ModifiedBy]  
		, ModifiedDate AS ModifiedDate  
		,[Status]  
		,[Measure]  
		,[OrderMinimum]  
		,[OrderIncrement]
		,[HandlingCharges]  
		,[Length]
		,[Height]
		,[Width]
		,[Weight]
		,[SelfShippable]
		,[AvailableForBackOrder]
		,MaximumDiscountPercentage
		,[BackOrderLimit]
		,[IsUnlimitedQuantity]
  FROM [PRProductSKU]  
  Where Id = @skuId

GO

ALTER PROCEDURE [dbo].[Sku_GetSkuProperties]( @SkuIds		xml,@ApplicationId uniqueidentifier=null)
AS
SELECT S.[Id]
      ,S.[ProductId]
      ,S.[SiteId]
      ,S.[SKU]
	  ,S.[Title]
      ,S.[Description]
      ,S.[Sequence]
      ,S.[IsActive]
      ,S.[IsOnline]
      ,S.[ListPrice]
      ,S.[UnitCost]
      ,S.[PreviousSoldCount]
	  ,S.[Measure]
	  ,S.[OrderMinimum]
	  ,S.[OrderIncrement]
	  ,S.[CreatedBy]
	  ,S.CreatedDate AS CreatedDate
	  ,S.[ModifiedBy]
	  ,S.ModifiedDate AS  ModifiedDate
	  ,S.[Status]
	  ,S.[HandlingCharges]
	  ,S.[Length]
	  ,S.[Height]
	  ,S.[Width]
	  ,S.[Weight]
	  ,S.[SelfShippable]
	  ,S.[AvailableForBackOrder]
	  ,S.MaximumDiscountPercentage
	  ,S.[BackOrderLimit]
	  ,S.[IsUnlimitedQuantity]
   FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKU S on S.Id =tab.col.value('text()[1]','uniqueidentifier')

GO



ALTER PROCEDURE [dbo].[SKU_Save]  
(  
	 @ApplicationId uniqueidentifier,  
	 @ProductId uniqueidentifier,  
	 @ProductSKUXml xml,  
	 @CreatedBy uniqueidentifier,  
	 @ModifiedBy uniqueidentifier,   
	 @Id uniqueidentifier output  
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  
-- Declare @WarehouseId uniqueidentifier
--
-- Select top 1 @WarehouseId =Id from INWarehouse
 set @strId = @ProductSKUXml.value('(Sku/Id)[1]','char(36)')  
 set @CurrentDate = getutcdate()   
  
 if(@strId = '00000000-0000-0000-0000-000000000000' OR @strId='')  
 Begin    
  Set @Id = newid()  
  declare @currentSequence int  
  --select @currentSequence= max(Sequence) from PRProductSKU where ProductId=@ProductId  
  
   insert into PRProductSKU  
   (Id,  
    ProductId,  
    SiteId,  
	SKU, 
    Title,  
    Description, 
--    Sequence,  
    ListPrice,  
    UnitCost,  
    CreatedDate,  
    CreatedBy,  
    ModifiedDate,  
    ModifiedBy,  
    Status, 
	Sequence, 
	Measure,
	OrderMinimum,
	OrderIncrement, 
	HandlingCharges,
	IsActive,
	IsOnline,
	Length,
	Height,
	Width,
	Weight,
	SelfShippable,
	AvailableForBackOrder,
	MaximumDiscountPercentage,
	BackOrderLimit,
	IsUnlimitedQuantity )  
   values  
   (@Id,  
    @ProductId,  
    @ApplicationId,  
	Isnull(@ProductSKUXml.value('(Sku/SKU)[1]','varchar(max)'), ' '),  
    Isnull(@ProductSKUXml.value('(Sku/Title)[1]','char(36)'), ' '),  
    Isnull(@ProductSKUXml.value('(Sku/Description)[1]','char(36)'), ' '),  
--@currentSequence+1,  
    @ProductSKUXml.value('(Sku/ListPrice)[1]','money'),  
    @ProductSKUXml.value('(Sku/UnitPrice)[1]','money'),  
    @CurrentDate,  
       @CreatedBy,  
    @CurrentDate,  
       @ModifiedBy,  
    @ProductSKUXml.value('(Sku/Status)[1]','int'), 
	(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
	Isnull(@ProductSKUXml.value('(Sku/Measure)[1]','varchar(max)'), ' '),  
    @ProductSKUXml.value('(Sku/OrderMinimum)[1]','money'),  
    @ProductSKUXml.value('(Sku/OrderIncrement)[1]','money'), 
    @ProductSKUXml.value('(Sku/HandlingCharges)[1]','money'), 
	case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	case when lower(@ProductSKUXml.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end, 
	@ProductSKUXml.value('(Sku/Length)[1]','decimal(18,3)'),
	@ProductSKUXml.value('(Sku/Height)[1]','decimal(18,3)'),
	@ProductSKUXml.value('(Sku/Width)[1]','decimal(18,3)'),
	@ProductSKUXml.value('(Sku/Weight)[1]','decimal(18,3)'),
	case when lower(@ProductSKUXml.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	 @ProductSKUXml.value('(Sku/MaximumDiscountPercentage)[1]','money'), 
	case when  lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else @ProductSKUXml.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	case when lower(@ProductSKUXml.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end
	)   

  --Associate attributes to the Product SKU and insert attribute values   
  declare @iCount int  
  declare @i int  
  declare @attributeId varchar(36)  
  
  set @iCOunt = @ProductSKUXml.value('count(/Sku/Attributes/ProductAttribute)','int')  
  set @i = 1  
    
  while (@i <= @iCOunt)  
  begin  
   set  @attributeId = @ProductSKUXml.value('(/Sku/Attributes/ProductAttribute[sql:variable("@i")]/Id)[1]','uniqueidentifier')  
     
     insert into PRProductAttribute  
   (Id, ProductId, AttributeId,Sequence,IsSKULevel)  
     Values( newid(), @Id, @attributeId, @i, 1)    
     
      
   insert into PRProductSKUAttributeValue  
   (Id, SKUId, AttributeId,AttributeEnumId,[Value],CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,Status)  
   SELECT newid(),  
       @Id,  
       @attributeId,  
       T.c.value('(AttributeEnumId)[1]','varchar(36)'),    
--		case A.AttributeDataType
--		 When 'System.DateTime' 
--		  Then	
--			Case ISDate(T.c.value('(Value)[1]','varchar(max)')) When  1
--				THEN  convert(varchar(max),dbo.ConvertTimeToUtc(T.c.value('(Value)[1]','varchar(max)'),@ApplicationId),126)
--			    ELSE T.c.value('(Value)[1]','varchar(max)')
--				END
--		   Else	
		T.c.value('(Value)[1]','varchar(max)')
		--End	
		,    
       @CurrentDate,  
       @CreatedBy,  
       @CurrentDate,  
       @ModifiedBy,  
       T.c.value('(Status)[1]','varchar(50)')  
   FROM @ProductSKUXml.nodes('/Sku/Attributes/ProductAttribute[sql:variable("@i")]/Values/AttributeItem') T(c)  
	---INNER JOIN ATAttribute A on A.Id = T.c.value('(AttributeId)[1]','uniqueidentifier')
  
   SET @i = @i + 1  
  end   
 end  
 else  
 begin  
  set @Id= @ProductSKUXml.value('(Sku/Id)[1]','uniqueidentifier')  
    
  update PRProductSKU   
  set  ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	SKU = Isnull(@ProductSKUXml.value('(Sku/SKU)[1]','varchar(max)'), ' '), 
    Title= Isnull(@ProductSKUXml.value('(Sku/Title)[1]','char(36)'), ' '),  
    Description= Isnull(@ProductSKUXml.value('(Sku/Description)[1]','char(36)'), ' '),  
    Sequence=@ProductSKUXml.value('(Sku/Sequence)[1]','int'),  
    ListPrice =@ProductSKUXml.value('(Sku/ListPrice)[1]','money'),  
    UnitCost=@ProductSKUXml.value('(Sku/UnitPrice)[1]','money'),     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = @ProductSKUXml.value('(Sku/Status)[1]','int'), 
	Measure = Isnull(@ProductSKUXml.value('(Sku/Measure)[1]','varchar(max)'), ' ') ,
	OrderMinimum = @ProductSKUXml.value('(Sku/OrderMinimum)[1]','money'), 
	OrderIncrement = @ProductSKUXml.value('(Sku/OrderIncrement)[1]','money'),
	HandlingCharges = @ProductSKUXml.value('(Sku/HandlingCharges)[1]','money'),
	IsActive=case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	IsOnline=case when lower(@ProductSKUXml.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end,
	Length=@ProductSKUXml.value('(Sku/Length)[1]','decimal(18,3)'),	
	Height=@ProductSKUXml.value('(Sku/Height)[1]','decimal(18,3)'),
	Width=@ProductSKUXml.value('(Sku/Width)[1]','decimal(18,3)'),
	Weight=@ProductSKUXml.value('(Sku/Weight)[1]','decimal(18,3)'),
	SelfShippable =case when lower(@ProductSKUXml.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	AvailableForBackOrder =case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	MaximumDiscountPercentage =  @ProductSKUXml.value('(Sku/MaximumDiscountPercentage)[1]','money'), 
	BackOrderLimit=case when  lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else @ProductSKUXml.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	IsUnlimitedQuantity=case when lower(@ProductSKUXml.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end
	
  where Id =@Id  
    		-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProduct 
	set IsActive=
			case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end 
	where IsBundle =1 and Id=@ProductId
  --delete from PRProductSKUAttributeValue where SKUId =@Id  
  --delete from PRProductAttribute where ProductId =@ProductId and IsSKULevel=1  
 End      
   
EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy	
end

GO

ALTER PROCEDURE [dbo].[SKU_SaveMulti]  
(  
	 @ApplicationId uniqueidentifier,  
	 @ProductSKUXml xml,  
	 @SkuIds		xml,	
	 @ProductId		uniqueidentifier,
	 @ModifiedBy uniqueidentifier   
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  
-- Declare @WarehouseId uniqueidentifier
--
-- Select top 1 @WarehouseId =Id from INWarehouse

 set @CurrentDate = getutcdate()   
	
	Declare @InsertSkus table(SkuId uniqueidentifier)
	Declare @UpdateSkus table(SkuId uniqueidentifier)
	Declare @InsertAttributes table(Id uniqueidentifier,SKUId uniqueidentifier,AttributeId uniqueidentifier)
	Declare @UpdateAttributes table(Id uniqueidentifier,SKUId uniqueidentifier,AttributeId uniqueidentifier)
	

	Insert into @UpdateSkus(SkuId)
	Select Distinct S.Id
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKU S on S.Id =tab.col.value('text()[1]','uniqueidentifier')
	

	Insert into @InsertSkus
	Select CASE WHEN tab.col.value('text()[1]','uniqueidentifier') = dbo.GetEmptyGUID() THEN newid() else tab.col.value('text()[1]','uniqueidentifier') END
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	except
	Select SkuId from @UpdateSkus
	

 
   insert into PRProductSKU  
   (Id,  
    ProductId,  
    SiteId,  
	SKU, 
    Title,  
    Description, 
    ListPrice,  
    UnitCost,  
    CreatedDate,  
    CreatedBy,  
    Status, 
	Sequence, 
	Measure,
	OrderMinimum,
	OrderIncrement,
	HandlingCharges,
	IsActive,
	IsOnline,
	Length,
	Height,
	Width,
	Weight,
	SelfShippable,
	AvailableForBackOrder,
	MaximumDiscountPercentage,
	BackOrderLimit,
	IsUnlimitedQuantity)  
   Select   
	I.SkuId,  
    @ProductId,  
    @ApplicationId,  
	Isnull(tab.col.value('(SKU)[1]','varchar(max)'), ' '),  
    Isnull(tab.col.value('(Title)[1]','char(36)'), ' '),  
    Isnull(tab.col.value('(Description)[1]','char(36)'), ' '),  
    tab.col.value('(ListPrice)[1]','money'),  
    tab.col.value('(UnitPrice)[1]','money'),  
    @CurrentDate,  
    @ModifiedBy,  
    tab.col.value('(Status)[1]','int'), 
	(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
	Isnull(tab.col.value('(Measure)[1]','varchar(max)'), ' '),  
    tab.col.value('(OrderMinimum)[1]','money'),  
    tab.col.value('(OrderIncrement)[1]','money'),
    tab.col.value('(HandlingCharges)[1]','money'),    
	case when (tab.col.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	case when (tab.col.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end,
	tab.col.value('(Sku/Length)[1]','decimal(18,3)'),
	tab.col.value('(Sku/Height)[1]','decimal(18,3)'),
	tab.col.value('(Sku/Width)[1]','decimal(18,3)'),
	tab.col.value('(Sku/Weight)[1]','decimal(18,3)'),
	case when lower(tab.col.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	case when lower(tab.col.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	tab.col.value('(MaximumDiscountPercentage)[1]','money'),  
	case when lower(tab.col.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else tab.col.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	case when lower(tab.col.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end
	From @InsertSkus I 
	INNER JOIN @ProductSKUXml.nodes('/Sku')tab(col)	
				ON  I.SkuId =tab.col.value('(/Id)[1]','uniqueidentifier')

  update S   
  set  ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	SKU = Isnull(tab.col.value('(SKU)[1]','varchar(max)'), ' '), 
    Title= Isnull(tab.col.value('(Title)[1]','char(36)'), ' '),  
    Description= Isnull(tab.col.value('(Description)[1]','char(36)'), ' '),  
    Sequence=tab.col.value('(Sequence)[1]','int'),  
    ListPrice =tab.col.value('(ListPrice)[1]','money'),  
    UnitCost=tab.col.value('(UnitPrice)[1]','money'),     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = tab.col.value('(Status)[1]','int'), 
	Measure = Isnull(tab.col.value('(Measure)[1]','varchar(max)'), ' ') ,
	OrderMinimum = tab.col.value('(OrderMinimum)[1]','money'), 
	OrderIncrement = tab.col.value('(OrderIncrement)[1]','money'),
	HandlingCharges = tab.col.value('(HandlingCharges)[1]','money'),
	IsActive = case when (tab.col.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end, 
	IsOnline = case when (tab.col.value('(Sku/IsOnline)[1]','varchar(5)'))='false' then 0 else 1 end,
	Length=tab.col.value('(Sku/Length)[1]','decimal(18,3)'),	
	Height=tab.col.value('(Sku/Height)[1]','decimal(18,3)'),
	Width=tab.col.value('(Sku/Width)[1]','decimal(18,3)'),
	Weight=tab.col.value('(Sku/Weight)[1]','decimal(18,3)'),
	SelfShippable =case when lower(tab.col.value('(Sku/SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,    
	AvailableForBackOrder =case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	MaximumDiscountPercentage = tab.col.value('(MaximumDiscountPercentage)[1]','money'),
	BackOrderLimit=case when lower(@ProductSKUXml.value('(Sku/AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else @ProductSKUXml.value('(Sku/BackOrderLimit)[1]','decimal(18,3)') end,
	IsUnlimitedQuantity =case when lower(tab.col.value('(Sku/IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end
	From PRProductSKU S
	Inner Join @UpdateSkus U on S.Id = U.SkuId 
	INNER JOIN @ProductSKUXml.nodes('/Sku')tab(col)	
				ON  U.SkuId =tab.col.value('(Id)[1]','uniqueidentifier')
  
  
  		-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProduct 
	set IsActive=
			case when lower(@ProductSKUXml.value('(Sku/IsActive)[1]','varchar(5)'))='false' then 0 else 1 end 
	where IsBundle =1 and Id=@ProductId


--  Now @InsertSkus has all the sku Id's
	Insert into @InsertSkus
	Select SkuId from @UpdateSkus

	Delete from PRProductSKUAttributeValue
	Where SKUId in (Select SkuId from @UpdateSkus)

	Delete from PRProductAttribute
	Where ProductId=@ProductId and IsSKULevel=1	
	AND AttributeId in 
	(Select T.c.value('(AttributeId)[1]','uniqueidentifier') FROM @ProductSKUXml.nodes('//AttributeItem') T(c))
	
	Declare @tempProductAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsRequired bit)
	Insert into @tempProductAttribute(AttributeId,IsRequired)
	Select T.c.value('(AttributeId)[1]','uniqueidentifier'),case when (T.c.value('(IsRequired)[1]','varchar(5)'))='false' then 0 else 1 end 
	From @ProductSKUXml.nodes('//AttributeItem') T(c)

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel,IsRequired)
	Select newid(),@ProductId,AttributeId,Sequence,1,IsRequired
	From @tempProductAttribute
	

	Insert into PRProductSKUAttributeValue(Id,SKUId,AttributeId,AttributeEnumId,Value,CreatedDate,CreatedBy,Status)
	SELECT newid(),  
       S.SkuId,  
        T.c.value('(AttributeId)[1]','uniqueidentifier'),
       T.c.value('(AttributeEnumId)[1]','uniqueidentifier'),    
--      case A.AttributeDataType
--		 When 'System.DateTime' 
--		  Then	
--			Case ISDate(T.c.value('(Value)[1]','varchar(max)')) When  1
--				THEN  convert(varchar(max),dbo.ConvertTimeToUtc(T.c.value('(Value)[1]','varchar(max)'),@ApplicationId),126)
--			    ELSE T.c.value('(Value)[1]','varchar(max)')
--				END
--		   Else	
		T.c.value('(Value)[1]','varchar(max)')
--		End	
		,   
       @CurrentDate,  
       @ModifiedBy,  
       T.c.value('(Status)[1]','int')  
	FROM @ProductSKUXml.nodes('//AttributeItem') T(c)
	--INNER JOIN ATAttribute A on A.Id = T.c.value('(AttributeId)[1]','uniqueidentifier')
	CROSS JOIN @InsertSkus S
	
EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy
end


GO

ALTER PROCEDURE [dbo].[Sku_SaveSKUAndAttributes]  
(  
	@Id				uniqueidentifier OUTPUT,  
    @ProductId		uniqueidentifier,
    @ApplicationId uniqueidentifier,  
	@SKU			nvarchar(255)=null, 
    @Title			nvarchar(255)=null,  
    @Description	text='', 
    @ListPrice		money,		
    @UnitPrice		money,  
    @Status			int, 
	@Measure		nvarchar(50),
	@OrderMinimum	money,
	@OrderIncrement	money,
	@HandlingCharges money = 0,
	@SystemAttributes xml,  
	@SKUAttributes xml,  
	@IsActive bit,
	@IsOnline bit,
	@SkuIds		xml out,	
	@ModifiedBy uniqueidentifier,
	@Length	decimal(18,3)=null,   
	@Height	decimal(18,3)=null,   
	@Width	decimal(18,3)=null,
	@Weight decimal(18,3)=null,
	@SelfShippable bit=null,
	@AvailableForBackOrder bit =null,
	@MaximumDiscountPercentage money = null,
	@BackOrderLimit decimal(18,3)=null,
	@IsUnlimitedQuantity bit = null
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  

 set @CurrentDate = getutcdate()   
	
	Declare @InsertSkus table(SkuId uniqueidentifier)
	Declare @UpdateSkus table(SkuId uniqueidentifier)
	Declare @tempProductSKUAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsSKULevel bit,IsSystem bit,IsRequired bit)

	Insert into @UpdateSkus(SkuId)
	Select Distinct S.Id
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	Inner Join PRProductSKU S on S.Id =tab.col.value('text()[1]','uniqueidentifier')
	

	Insert into @InsertSkus
	Select CASE WHEN tab.col.value('text()[1]','uniqueidentifier') = dbo.GetEmptyGUID() THEN newid() else tab.col.value('text()[1]','uniqueidentifier') END
	FROM @SkuIds.nodes('/GenericCollectionOfGuid/guid')tab(col)	
	except
	Select SkuId from @UpdateSkus
	
	IF (Select count(*) From @InsertSkus)>0
	BEGIN
		IF Exists(Select Id from PRProductSKU Where (SKU) =(@SKU))
		BEGIN
			RAISERROR('Sku already exists, Please choose a different Sku.', 16, 1)
			Return
		END		
	END
   insert into PRProductSKU  
   (Id,  
    ProductId,  
    SiteId,  
	SKU, 
    Title,  
    Description, 
    ListPrice,  
    UnitCost,  
    CreatedDate,  
    CreatedBy,  
    Status, 
	Sequence, 
	Measure,
	OrderMinimum,
	OrderIncrement,
	HandlingCharges,
	IsActive,
	IsOnline,
	Length,
	Height,
	Width,
	Weight,
	SelfShippable,
	AvailableForBackOrder,
	MaximumDiscountPercentage,
	BackOrderLimit,
	IsUnlimitedQuantity)  
   Select   
	I.SkuId,  
    @ProductId,  
    @ApplicationId,  
	@SKU,  
    @Title,  
    @Description,  
    @ListPrice,  
    @UnitPrice,  
    @CurrentDate,  
    @ModifiedBy,  
    @Status, 
	dbo.GetMaxSKUSequence(@ProductId) + 1,
	--(Isnull(dbo.GetMaxSKUSequence(@ProductId),0) + 1),
	@Measure,  
    @OrderMinimum,  
    @OrderIncrement,
    @HandlingCharges,
	@IsActive,
	@IsOnline,
	@Length,   
	@Height,   
	@Width,
	@Weight,
	@SelfShippable,   
	@AvailableForBackOrder, 
	@MaximumDiscountPercentage,
	case when @AvailableForBackOrder=0 then 0 else @BackOrderLimit end,
	@IsUnlimitedQuantity
	From @InsertSkus I 
	
  
  update S   
  set  ProductId=@ProductId,  
    SiteId=@ApplicationId,  
	SKU = case when @SKU is not null then @SKU else sku end,
	--isnull(@SKU,SKU), 
    Title = case when @Title is not null then @Title else Title end, 
	--isnull(@Title,Title),  
    Description=@Description,  
    ListPrice =@ListPrice,  
    UnitCost=@UnitPrice,     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = @Status, 
	Measure = @Measure,
	OrderMinimum = @OrderMinimum, 
	OrderIncrement = @OrderIncrement,
	HandlingCharges = @HandlingCharges,
	IsActive=@IsActive,
	IsOnline=@IsOnline,
	Length=@Length,
	Height=@Height,
	Width= @width,
	Weight=@Weight,
	SelfShippable=@SelfShippable,
	AvailableForBackOrder=@AvailableForBackOrder,
	MaximumDiscountPercentage = @MaximumDiscountPercentage,
	BackOrderLimit=case when @AvailableForBackOrder=0 then 0 else @BackOrderLimit end,
	IsUnlimitedQuantity=@IsUnlimitedQuantity
	From PRProductSKU S
	Inner Join @UpdateSkus U on S.Id = U.SkuId 
	
	-- make product active/inactive if the sku is that of bundle i.e. parent is a bundle
	Update PRProduct 
	set IsActive=@IsActive
	where IsBundle =1 and Id=@ProductId
	
--  Now @InsertSkus has all the sku Id's
	Insert into @InsertSkus
	Select SkuId from @UpdateSkus

	

	delete from PRProductSKUAttributeValue
	Where SKUId in (Select SkuId from @UpdateSkus)
	AND AttributeId in -- to not delete the attribute value when 'Multi' is selected
	(
		Select T.c.value('(@Id)[1]','uniqueidentifier')
		FROM @SKUAttributes.nodes('//ProductAttribute') T(c)
		UNION ALL
		Select tab.col.value('text()[1]','uniqueidentifier')
		From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	)


	Insert into @tempProductSKUAttribute(AttributeId,IsSKULevel,IsSystem,IsRequired)
	Select tab.col.value('text()[1]','uniqueidentifier'),1,1,0
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	UNION ALL
	Select Distinct T.c.value('(@Id)[1]','uniqueidentifier'),
	1,0,case when T.c.value('(@IsRequired)[1]','varchar(5)') ='true' then 1 else 0 end 
	From @SKUAttributes.nodes('//ProductAttribute') T(c) 
	---WHERE T.c.value('(IsSKULevel)[1]','varchar(5)') ='true'  
	 

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel,IsRequired)
	Select newid(),@ProductId,AttributeId,Sequence,IsSKULevel,IsRequired
	From @tempProductSKUAttribute
	WHERE IsSKULevel=1 AND IsSystem=0
	AND AttributeId not in 
		(Select AttributeId from PRProductAttribute Where ProductId=@ProductId AND IsSKULevel=1)

	
	Declare @tempProductAttribute table(Sequence int identity(1,1),AttributeId Uniqueidentifier,IsRequired bit)

	Insert into @tempProductAttribute(AttributeId,IsRequired)
	Select tab.col.value('text()[1]','uniqueidentifier'),0
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item/key/guid') tab(col)
	
	UNION ALL
	Select Distinct T.c.value('(@Id)[1]','uniqueidentifier'),
	case when T.c.value('(@IsRequired)[1]','varchar(5)') ='true' then 1 else 0 end 
	From @SKUAttributes.nodes('//ProductAttribute') T(c) 
	--WHERE T.c.value('(IsSKULevel)[1]','varchar(5)') ='true'  

	insert into PRProductAttribute(Id, ProductId, AttributeId,Sequence,IsSKULevel,IsRequired)
	Select newid(),@ProductId,AttributeId,Sequence,1,IsRequired
	From @tempProductAttribute Where  AttributeId not in 
		(Select AttributeId from PRProductAttribute Where ProductId=@ProductId AND IsSKULevel=1)
	
	Insert into PRProductSKUAttributeValue
	(
		Id,
		ProductAttributeId,
		SKUId,
		AttributeId,
		AttributeEnumId,
		Value,
		CreatedDate,
		CreatedBy,
		Status)
	SELECT newid(), 
			Id,
			SkuId,
			AttributeId,
			AttributeEnumId,
			Value,
	@CurrentDate,  
       @ModifiedBy,  
       [Status]
	From
		(Select distinct
		PA.Id, 
       S.SkuId,  
       T.c.value('(@AttributeId)[1]','uniqueidentifier') AttributeId,
       case when T.c.value('(@AttributeEnumId)[1]','uniqueidentifier')=dbo.GetEmptyGUID() Then null ELSE T.c.value('(@AttributeEnumId)[1]','uniqueidentifier') END AttributeEnumId ,    
		T.c.value('(@Value)[1]','varchar(max)') Value,    
       1 Status--T.c.value('(Status)[1]','int')    Status
	FROM @SKUAttributes.nodes('//ProductAttribute') T(c)
	INNER JOIN PRProductAttribute PA ON (PA.ProductId = @ProductId AND PA.AttributeId = T.c.value('(@AttributeId)[1]','uniqueidentifier') AND IsSKULevel=1)
	--INNER JOIN ATAttribute A on A.Id = T.c.value('(AttributeId)[1]','uniqueidentifier')
	CROSS JOIN @InsertSkus S
	where T.c.value('(@Value)[1]','varchar(max)') IS NOT NULL
	--Where T.c.value('(Value)[1]','varchar(max)') is not null
	--AND  T.c.value('(parent::*/parent::*/IsSystem)[1]','varchar(5)')  ='false' 
	--And T.c.value('(parent::*/parent::*/IsSKULevel)[1]','varchar(5)') ='true'
	)VV

Insert into PRProductSKUAttributeValue
	(
	Id,
	ProductAttributeId,
	SKUId,
	AttributeId,
	AttributeEnumId,
	Value,
	CreatedDate,
	CreatedBy,
	Status
	)
	Select	
	newid(),
	PA.Id,
	S.SkuId,
	tab.col.value('(key/guid/text())[1]','uniqueidentifier'),
	E.Id,
	E.Value,
	@CurrentDate,
	@ModifiedBy	,
	dbo.GetActiveStatus()
	From @SystemAttributes.nodes('dictionary/SerializableDictionary/item') tab(col)
		Inner Join ATAttributeEnum E 
			ON tab.col.value('(key/guid/text())[1]','uniqueidentifier') = E.AttributeID
		INNER JOIN PRProductAttribute PA ON (PA.ProductId = @ProductId AND PA.AttributeId = tab.col.value('(key/guid/text())[1]','uniqueidentifier') AND IsSKULevel=1)
		CROSS JOIN @InsertSkus S
		Where (tab.col.value('(value/boolean/text())[1]','varchar(5)')= 'true' AND E.Code='1')
			OR ((tab.col.value('(value/boolean/text())[1]','varchar(5)'))= 'false' AND E.Code='0')
	
Declare @char varchar(max)
Set @char ='<GenericCollectionOfGuid>'
Select @char = @char + '<guid>' + cast(SkuId as char(36))  + '</guid>' from @InsertSkus
Set @char = @char + '</GenericCollectionOfGuid>'
Set @SkuIds = cast(@char as xml)

EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy

end

GO

ALTER PROCEDURE [dbo].[Sku_Update]  
(  
	 @ApplicationId uniqueidentifier,  
	 @ProductSKUXml xml,  
	 @SkuIds		xml,	
	 @ProductId		uniqueidentifier,
	 @ModifiedBy uniqueidentifier   
)  
as  
begin  
 Declare @CurrentDate datetime  
 Declare @strId char(36)  

 set @CurrentDate = GetUTCDate()   
	
  update S   
  set  ProductId=tab.col.value('(ParentProductId)[1]','uniqueidentifier'),  
    SiteId=@ApplicationId,  
	SKU = Isnull(tab.col.value('(SKU)[1]','varchar(max)'), ' '), 
    Title= Isnull(tab.col.value('(Title)[1]','char(36)'), ' '),  
    Description= Isnull(tab.col.value('(Description)[1]','char(36)'), ' '),  
    ListPrice =tab.col.value('(ListPrice)[1]','money'),  
    UnitCost=tab.col.value('(UnitPrice)[1]','money'),     
    ModifiedDate =@CurrentDate,  
    ModifiedBy=@ModifiedBy,  
    Status = tab.col.value('(Status)[1]','int'), 
	Measure = Isnull(tab.col.value('(Measure)[1]','varchar(max)'), ' ') ,
	OrderMinimum = tab.col.value('(OrderMinimum)[1]','money'), 
	OrderIncrement = tab.col.value('(OrderIncrement)[1]','money'),
	HandlingCharges  = tab.col.value('(HandlingCharges)[1]','money'),
	Length = tab.col.value('(Length)[1]','decimal(18,3)'),
	Height = tab.col.value('(Height)[1]','decimal(18,3)'),
	Width = tab.col.value('(Width)[1]','decimal(18,3)'),
	Weight = tab.col.value('(Weight)[1]','decimal(18,3)'),
	SelfShippable = case when lower(tab.col.value('(SelfShippable)[1]','varchar(5)'))='false' then 0 else 1 end,
	AvailableForBackOrder = case when lower(tab.col.value('(AvailableForBackOrder)[1]','varchar(5)'))='false' then 0 else 1 end,
	MaximumDiscountPercentage = tab.col.value('(MaximumDiscountPercentage)[1]','money'), 
	BackOrderLimit = tab.col.value('(BackOrderLimit)[1]','decimal(18,3)'),
	IsUnlimitedQuantity = case when lower(tab.col.value('(IsUnlimitedQuantity)[1]','varchar(5)'))='false' then 0 else 1 end
	
	From PRProductSKU S
	INNER JOIN @SkuIds.nodes('/GenericCollectionOfGuid/guid')TA(c)	
		ON  S.Id =TA.c.value('text()[1]','uniqueidentifier')
	CROSS JOIN @ProductSKUXml.nodes('/Sku')tab(col)	
			
  Delete from PRProductSKUAttributeValue
Where AttributeId in (Select T.c.value('(Id)[1]','uniqueidentifier') from     @ProductSKUXml.nodes('//ProductAttribute') T(c))
AND 
SKUId in (Select  tab.col.value('(Id)[1]','uniqueidentifier') From @ProductSKUXml.nodes('/Sku')tab(col)	)

	Insert into PRProductSKUAttributeValue(Id,ProductAttributeId,SKUId,AttributeId,AttributeEnumId,Value,CreatedDate,CreatedBy,Status)
	SELECT newid(),temp.*
	FROM (Select Distinct  
		PA.Id ProductAttributeId,
       X.C.value('text()[1]','uniqueidentifier') SKUId,  
        T.c.value('(AttributeId)[1]','uniqueidentifier')AttributeId,
      case when T.c.value('(AttributeEnumId)[1]','uniqueidentifier')=dbo.GetEmptyGUID() Then null ELSE T.c.value('(AttributeEnumId)[1]','uniqueidentifier') END AttributeEnumId,    
       T.c.value('(Value)[1]','varchar(max)') Value,  
       @CurrentDate ModifiedDate,  
       @ModifiedBy ModifiedBy,  
       T.c.value('(Status)[1]','int') Status 
	FROM @ProductSKUXml.nodes('//AttributeItem') T(c)
	INNER JOIN PRProductAttribute PA ON 
		(PA.ProductId = @ProductId 
			AND PA.AttributeId = T.c.value('(AttributeId)[1]','uniqueidentifier')
			AND PA.IsSKULevel=1
			)	
	CROSS JOIN @SkuIds.nodes('/GenericCollectionOfGuid/guid')X(C)
		)temp
	EXEC [dbo].[Product_UpdateProductProperties] @ProductId=@ProductId,@ModifiedBy=@ModifiedBy
END


