﻿--This script is to update the patches for v5.1
/*
List of changed objects
1. 

*/
/*
Run this script on:

        rnd-d-dev.copy_www_iapps_trunk    -  This database will be modified

to synchronize it with:

        rnd-d-dev.v51_iapps_cms

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 1/13/2014 4:24:40 PM

*/


GO
PRINT N'Altering [dbo].[SISite]...';


GO
IF COL_LENGTH(N'[dbo].[SISite]', N'CustomAttribute') IS NULL
ALTER TABLE [dbo].[SISite]
    ADD [CustomAttribute] XML NULL;
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[Regex_ReplaceMatches]'
GO
ALTER FUNCTION [dbo].[Regex_ReplaceMatches] (@source [nvarchar] (max), @regexPattern [nvarchar] (max), @regexOptions [int], @replacement [nvarchar] (max))
RETURNS [nvarchar] (max)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_ReplaceMatches]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[NavFilter_GetQuery]'
GO
ALTER FUNCTION [dbo].[NavFilter_GetQuery] (@queryId [uniqueidentifier], @condition [nvarchar] (4000), @OrderByProperty [nvarchar] (4000), @SortOrder [nvarchar] (4000), @SiteId [uniqueidentifier])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[NavFilter_GetQuery]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[RegexSelectAll]'
GO
ALTER FUNCTION [dbo].[RegexSelectAll] (@input [nvarchar] (max), @pattern [nvarchar] (max), @matchDelimiter [nvarchar] (max))
RETURNS [nvarchar] (max)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[RegexSearch].[RegexSelectAll]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsContentDefinitionMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsContentDefinitionMatch] (@source [xml], @searchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsContentDefinitionMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedLinkInContentDefinition] (@source [xml], @oldUrl [nvarchar] (4000), @newUrl [nvarchar] (4000), @matchWholeLink [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedLinkInContentDefinition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[GenerateXml]'
GO
IF EXISTS (select 1 from sys.objects where name='GenerateXml' and type='AF')
DROP AGGREGATE [dbo].[GenerateXml]
GO
PRINT N'Creating [dbo].[GenerateXml]'
GO
CREATE AGGREGATE [dbo].[GenerateXml] (@Value [nvarchar] (max))
RETURNS [nvarchar] (max)
EXTERNAL NAME [Bridgeline.CLRFunctions].[GenerateXml]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Regex_GetMatches]'
GO
ALTER FUNCTION [dbo].[Regex_GetMatches] (@source [nvarchar] (max), @regexPattern [nvarchar] (max), @regexOptions [int])
RETURNS TABLE (
[Match] [nvarchar] (max) NULL)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatches]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetRelationTable]'
GO
ALTER FUNCTION [dbo].[GetRelationTable] (@relationType [nvarchar] (4000), @objectId [uniqueidentifier])
RETURNS TABLE (
[col1] [uniqueidentifier] NULL,
[col2] [uniqueidentifier] NULL,
[col3] [uniqueidentifier] NULL,
[col4] [uniqueidentifier] NULL,
[col5] [uniqueidentifier] NULL,
[col6] [uniqueidentifier] NULL,
[col7] [uniqueidentifier] NULL,
[col8] [uniqueidentifier] NULL,
[col9] [uniqueidentifier] NULL,
[col10] [uniqueidentifier] NULL)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[GetRelationTable]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsPagePropertiesMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsPagePropertiesMatch] (@source [xml], @searchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPagePropertiesMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedFileProperty]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedFileProperty] (@source [nvarchar] (4000), @searchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedFileProperty]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedContentDefinitionXmlString] (@source [xml], @searchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedContentDefinitionXmlString]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsPageNameMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsPageNameMatch] (@source [nvarchar] (4000), @searchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsPageNameMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsLinkInContentDefinition]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsLinkInContentDefinition] (@source [xml], @oldUrl [nvarchar] (4000), @matchWholeLink [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsLinkInContentDefinition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetContentLocation]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetContentLocation] (@contentId [uniqueidentifier], @siteId [uniqueidentifier])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetContentLocation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageName]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedPageName] (@source [nvarchar] (4000), @searchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageName]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedImageProperty]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedImageProperty] (@source [nvarchar] (4000), @searchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedImageProperty]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedPageProperties]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedPageProperties] (@source [xml], @searchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedPageProperties]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsFilePropertiesMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsFilePropertiesMatch] (@title [nvarchar] (4000), @description [nvarchar] (4000), @altText [nvarchar] (4000), @searchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsFilePropertiesMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Regex_GetMatch]'
GO
ALTER FUNCTION [dbo].[Regex_GetMatch] (@source [nvarchar] (max), @regexPattern [nvarchar] (max), @regexOptions [int])
RETURNS [nvarchar] (max)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Regex_IsMatch]'
GO
ALTER FUNCTION [dbo].[Regex_IsMatch] (@source [nvarchar] (max), @regexPattern [nvarchar] (max), @regexOptions [int])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_IsMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsImagePropertiesMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsImagePropertiesMatch] (@title [nvarchar] (4000), @description [nvarchar] (4000), @altText [nvarchar] (4000), @searchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsImagePropertiesMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_IsTextContentMatch]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_IsTextContentMatch] (@source [nvarchar] (max), @searchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_IsTextContentMatch]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_GetUpdatedTextContentText]'
GO
ALTER FUNCTION [dbo].[FindAndReplace_GetUpdatedTextContentText] (@source [nvarchar] (max), @searchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[FindAndReplace_GetUpdatedTextContentText]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Contact_GetAutoDistributionListContactCount]'
GO
ALTER PROCEDURE [dbo].[Contact_GetAutoDistributionListContactCount] (@xml [xml], @ApplicationId [uniqueidentifier], @MaxRecords [int], @TotalRecords [int] OUTPUT)
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_GetAutoDistributionListContactCount]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CLRPageMapNode_AddContainer]'
GO
ALTER PROCEDURE [dbo].[CLRPageMapNode_AddContainer] (@templateId [nvarchar] (4000), @containerIds [nvarchar] (4000), @modifiedBy [uniqueidentifier])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_AddContainer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CLRPageMap_SavePageDefinition]'
GO
ALTER PROCEDURE [dbo].[CLRPageMap_SavePageDefinition] (@siteId [uniqueidentifier], @parentNodeId [uniqueidentifier], @CreatedBy [uniqueidentifier], @ModifiedBy [uniqueidentifier], @pageDefinitionXml [xml], @Id [uniqueidentifier] OUTPUT, @publishPage [bit])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMap_SavePageDefinition]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CLRPageMapNode_Save]'
GO
ALTER PROCEDURE [dbo].[CLRPageMapNode_Save] (@siteId [uniqueidentifier], @parentNodeId [uniqueidentifier], @pageMapNode [xml], @createdBy [uniqueidentifier], @modifiedBy [uniqueidentifier], @modifiedDate [datetime], @id [uniqueidentifier] OUTPUT)
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Save]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CLRPageMapNode_RemoveContainer]'
GO
ALTER PROCEDURE [dbo].[CLRPageMapNode_RemoveContainer] (@templateId [nvarchar] (4000), @containerIds [nvarchar] (4000), @modifiedBy [uniqueidentifier])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_RemoveContainer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Contact_SearchCLR]'
GO
ALTER PROCEDURE [dbo].[Contact_SearchCLR] (@xml [xml], @ApplicationId [uniqueidentifier], @MaxRecords [int], @TotalRecords [int] OUTPUT)
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[Contact_SearchCLR]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CLSPageMapNode_AttachWorkflow]'
GO
ALTER PROCEDURE [dbo].[CLSPageMapNode_AttachWorkflow] (@siteId [uniqueidentifier], @pageMapNodeId [uniqueidentifier], @pageMapNodeWorkFlow [xml], @propogate [bit], @inherit [bit], @appendToExistingWorkflows [bit], @modifiedBy [uniqueidentifier], @modifiedDate [datetime])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLSPageMapNode_AttachWorkflow]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CLRPageMapNode_Update]'
GO
ALTER PROCEDURE [dbo].[CLRPageMapNode_Update] (@siteId [uniqueidentifier], @pageMapNode [xml], @modifiedBy [uniqueidentifier], @modifiedDate [datetime])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[CLRPageMapNode_Update]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_Replace]'
GO
ALTER PROCEDURE [dbo].[FindAndReplace_Replace] (@siteId [uniqueidentifier], @objectIds [xml], @searchTerm [nvarchar] (4000), @htmlEncodedSearchTerm [nvarchar] (4000), @replaceTerm [nvarchar] (4000), @htmlEncodedReplaceTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Replace]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_Find]'
GO
ALTER PROCEDURE [dbo].[FindAndReplace_Find] (@siteId [uniqueidentifier], @searchIn [int], @searchTerm [nvarchar] (4000), @htmlEncodedSearchTerm [nvarchar] (4000), @matchCase [bit], @wholeWords [bit], @includeAttributeValues [bit])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_Find]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FindAndReplace_UpdateLinks]'
GO
ALTER PROCEDURE [dbo].[FindAndReplace_UpdateLinks] (@siteId [uniqueidentifier], @oldUrl [nvarchar] (4000), @newUrl [nvarchar] (4000), @matchWholeLink [bit])
WITH EXECUTE AS CALLER
AS EXTERNAL NAME [Bridgeline.CLRFunctions].[StoredProcedures].[FindAndReplace_UpdateLinks]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
IF OBJECT_ID('vwAssetFileUrls') IS NOT NULL
	DROP VIEW vwAssetFileUrls
GO
CREATE VIEW [dbo].[vwAssetFileUrls]
WITH SCHEMABINDING
AS
SELECT C.Id AS Id, C.ApplicationId AS SiteId, RelativePath + '/' + FileName AS Url 
	FROM dbo.COFile F
		JOIN dbo.COContent C ON C.Id = F.ContentId
GO

CREATE UNIQUE CLUSTERED INDEX [IX_vwAssetFileUrls_Id_SiteId] ON [dbo].[vwAssetFileUrls] 
(
	[Id] ASC,
	[SiteId] ASC
)

GO
ALTER VIEW [dbo].[vwSearchOrder]
AS
SELECT  O.Id ,
        O.PurchaseOrderNumber ,
        O.ExternalOrderNumber ,
        O.OrderTypeId ,
        O.OrderDate ,
        US.FirstName ,
        US.MiddleName ,
        US.LastName ,
        M.Email ,
        O.OrderStatusId ,
        O.TotalTax ,
        O.CODCharges ,
        O.TotalShippingCharge AS ShippingTotal ,
        O.TotalShippingCharge ,
		O.PaymentRemaining ,
        O.RefundTotal ,
        O.SiteId ,
        dbo.Order_GetQuantity(O.Id) AS Quantity ,
        ISNULL(( SELECT SUM(ISNULL(dbo.OROrderItem.Quantity, 0)) AS Expr1
                 FROM   dbo.OROrderItem
                        INNER JOIN dbo.PRProductSKU AS PS ON dbo.OROrderItem.ProductSKUId = PS.Id
                        INNER JOIN dbo.PRProduct AS P ON PS.ProductId = P.Id
                 WHERE  ( dbo.OROrderItem.OrderId = O.Id )
                        AND ( P.IsBundle = 0 )
               ), 0) AS NumSkus ,
        O.OrderTotal ,
        O.TotalDiscount ,
        O.TaxableOrderTotal ,
        O.GrandTotal ,
        O.CustomerId ,
        O.TotalShippingDiscount ,
        O.CreatedBy ,
        FN.UserFullName AS CreatedByFullName ,
        CASE WHEN EXISTS ( SELECT   *
                           FROM     FFOrderShipment F
                                    INNER JOIN dbo.OROrderShipping AS OS ON F.OrderShippingId = OS.Id
                           WHERE    OS.OrderId = O.Id
                                    AND ( ShipmentStatus = 3
                                          OR ShipmentStatus = 9
                                        ) ) THEN 3
             ELSE 0
        END AS ExternalShippingStatus
FROM    dbo.OROrder AS O
        INNER JOIN dbo.USCommerceUserProfile AS U ON O.CustomerId = U.Id
        INNER JOIN dbo.USUser US ON US.Id = U.Id
        INNER JOIN dbo.USMembership M ON M.UserId = U.Id
        LEFT OUTER JOIN dbo.VW_CustomerFullName AS FN ON FN.UserId = O.CreatedBy
GO
PRINT N'Altering [dbo].[Site_GetSite]...';


GO
ALTER PROCEDURE [dbo].[Site_GetSite]  
(  
	@Id				uniqueIdentifier = null ,  
	@Title			nvarchar(256) = null,  
	@ParentSiteId	uniqueIdentifier = null,  
	@Status			int = null,  
	@PageIndex		int,  
	@PageSize		int,
	@ForceAllSites	bit = null,
	@MasterSiteId   uniqueIdentifier = null,
	@SiteGroupId	uniqueIdentifier = null
 )  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	IF @PageSize is Null  
	BEGIN  
		SELECT S.Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId, 
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName, 
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			NotifyOnTranslationError, 
			TranslationEnabled, 
			TranslatePagePropertiesByDefault, 
			AutoImportTranslatedSubmissions, 
			DefaultTranslateToLanguage,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
			MasterSiteId,
			CustomAttribute
		FROM SISite S  
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id
		WHERE 
			(@Id IS NULL OR S.Id = @Id) AND
			(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
			(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
			(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
			(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus)
			
	END  
	ELSE  
	BEGIN  
		WITH ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
				S.Id, 
				Title, 
				Description, 
				Status, 
				CreatedBy, 
				CreatedDate, 
				ModifiedBy,   
				ModifiedDate, 
				URL,
				Type,
				ParentSiteId,
				Keywords,
				VirtualPath,
				HostingPackageInstanceId, 
				LicenseLevel, 
				SendNotification, 
				SubmitToTranslation,
				ImportAllAdminPermission,
				AllowMasterWebSiteUser,
				Prefix, 
				UICulture, 
				LoginURL, 
				DefaultURL,
				NotifyOnTranslationError, 
				TranslationEnabled, 
				TranslatePagePropertiesByDefault, 
				AutoImportTranslatedSubmissions, 
				DefaultTranslateToLanguage,
				PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteURL,
				dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
				MasterSiteId,
				CustomAttribute
			FROM SISite S
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id 
			WHERE 
				(@Id IS NULL OR S.Id = @Id) AND
				(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
				(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
				(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
				(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
				(@Status IS NULL OR S.Status = @Status) AND
				(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus) 
		)
		
		SELECT 
			Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId,  
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			SitePath,
			MasterSiteId,
			CustomAttribute
		FROM ResultEntries  A  
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize  
	END  
END
GO
PRINT N'Altering [dbo].[Site_Save]...';


GO
ALTER PROCEDURE [dbo].[Site_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@URL				xml,
	@VirtualPath		nvarchar(4000)=null,
	@ParentSiteId		uniqueidentifier=null,
	@SiteType 	        int=null,
	@Keywords	        nvarchar(4000)=null,
	@Status				int=null,
	@SendNotification	bit=null,
	@SubmitToTranslation bit=null,
	@AllowMasterWebSiteUser bit=null,
	@ImportAllAdminPermission bit=null,
	@Prefix			nvarchar(5) = null, 
	@UICulture			nvarchar(10) = null,
	@LoginUrl		nvarchar(max) =null,
	@DefaultURL		nvarchar(max) =null,
	@NotifyOnTranslationError bit = null,
	@TranslationEnabled bit = null,
	@TranslatePagePropertiesByDefault bit = null,
	@AutoImportTranslatedSubmissions bit = null,
	@DefaultTranslateToLanguage nvarchar(10) = null,
	@PageImportOptions int =null,
	@ImportContentStructure bit =null,
	@ImportFileStructure bit=null,
	@ImportImageStructure bit=null,
	@ImportFormsStructure bit=null,
	@SiteGroups nvarchar(4000) = null,
	@MasterSiteId		uniqueidentifier=null,
	@PrimarySiteUrl nvarchar(max) =null,
	@CustomAttribute xml = null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
Declare @temp table(url1 nvarchar(max))
Declare @displayTitle varchar(512), @pageMapNodeDescription varchar(512), @pageMapNodeXml xml, @pageMapNodeId uniqueidentifier
Declare @LftValue int,@RgtValue int
Begin
--********************************************************************************
-- code
--********************************************************************************
	
	SET @displayTitle = @Title
	SET @pageMapNodeDescription = @Title

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  SISite where  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end

	set @Now = getdate()
	--set @Status= dbo.GetActiveStatus()



	if (@Id is null)			-- new insert
	begin
	
	IF (@ParentSiteId is not null AND @ParentSiteId <> '00000000-0000-0000-0000-000000000000' )  
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue
		from SISite
		Where Id=@ParentSiteId and MasterSiteId=@MasterSiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END

		Update SISite Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
								RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 


		set @stmt = 'Site Insert'
		set @Id = newid();
		IF @MasterSiteId IS NULL OR @MasterSiteId = dbo.GetEmptyGUID()
			SET @MasterSiteId = @Id

		insert into SISite  (
					   Id,	
					   Title,
                       Description,
                       CreatedBy,
                       CreatedDate,
					   Status,
                       URL,
                       VirtualPath,
					   ParentSiteId,
					   Type,
                       Keywords,
                       HostingPackageInstanceId,
                       SendNotification,
                       SubmitToTranslation,
                       AllowMasterWebSiteUser,
                       ImportAllAdminPermission,
                       Prefix, 
                       UICulture, 
                       LoginUrl, 
                       DefaultURL,
                       NotifyOnTranslationError,
                       TranslationEnabled,
                       TranslatePagePropertiesByDefault,
                       AutoImportTranslatedSubmissions,
                       DefaultTranslateToLanguage,
                       PageImportOptions,
                       ImportContentStructure,
                       ImportFileStructure,
                       ImportImageStructure,
                       ImportFormsStructure,
                       MasterSiteId,
                       PrimarySiteUrl,
                       LftValue,
                       RgtValue,
                       CustomAttribute) 
					values (
						@Id,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@URL,
						@VirtualPath,
						@ParentSiteId,
						@SiteType,
						@Keywords,
						0,
						@SendNotification,
						@SubmitToTranslation,
						@AllowMasterWebSiteUser,
						@ImportAllAdminPermission, 
						@Prefix, 
						@UICulture, 
						@LoginUrl, 
						@DefaultURL,
						@NotifyOnTranslationError,
						@TranslationEnabled,
						@TranslatePagePropertiesByDefault,
						@AutoImportTranslatedSubmissions,
						@DefaultTranslateToLanguage,
						@PageImportOptions,
						@ImportContentStructure,
						@ImportFileStructure,
						@ImportImageStructure,
						@ImportFormsStructure,
						@MasterSiteId,
						@PrimarySiteUrl,
						@RgtValue,
						@RgtValue + 1,
						@CustomAttribute)

		-- Inserting in SiteGroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')


			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			

			
			
         end
         else			-- update
         begin
			set @stmt = 'Site Update'
			
			Update HSStructure Set Title=@Title Where Id=@Id
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			--SELECT @pageMapNodeXml =  cast((PageMapNodexml + '</pageMapNode>') as xml), @pageMapNodeId = PageMapNodeId FROM PageMapNode Where SiteId = PageMapNodeId And SiteId = @Id								

			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@displayTitle)[1]
			--				  with
			--				(sql:variable("@displayTitle"))')
			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@description)[1]
			--				  with
			--				(sql:variable("@pageMapNodeDescription"))')			
			
			Update PageMapNode set displaytitle=@Title
			Where PageMapNodeId = @pageMapNodeId ANd SiteId = @Id
			-- Here we are not updating the friendly url
						
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			set @rowcount= 0
			
			select * from SISite where Id=@Id
			update	SISite  with (rowlock)	set 
            Title =@Title,
            Description=@Description,
            ModifiedBy=@ModifiedBy,
            ModifiedDate=@Now,
            Status=@Status,
            URL=@URL,
            VirtualPath=@VirtualPath,
            ParentSiteId=@ParentSiteId,
            Type=@SiteType,
            Keywords= @Keywords,
            SendNotification = @SendNotification,
            SubmitToTranslation = @SubmitToTranslation,
            AllowMasterWebSiteUser=@AllowMasterWebSiteUser,
			ImportAllAdminPermission=@ImportAllAdminPermission,
			Prefix = @Prefix, 
			UICulture = @UICulture,
			LoginUrl = @LoginUrl, 
			DefaultURL =@DefaultURL,
			NotifyOnTranslationError = @NotifyOnTranslationError,
			TranslationEnabled = @TranslationEnabled,
			TranslatePagePropertiesByDefault = @TranslatePagePropertiesByDefault,
			AutoImportTranslatedSubmissions = @AutoImportTranslatedSubmissions,
			DefaultTranslateToLanguage = @DefaultTranslateToLanguage,	
			PageImportOptions =	@PageImportOptions,
			ImportContentStructure=@ImportContentStructure,
			ImportFileStructure=@ImportFileStructure,
			ImportImageStructure=@ImportImageStructure,
			ImportFormsStructure=@ImportFormsStructure,
			MasterSiteId = @MasterSiteId,
			PrimarySiteUrl = @PrimarySiteUrl,
			CustomAttribute = @CustomAttribute
 	where 	Id    = @Id --and
	--isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
	
	select @error = @@error, @rowcount = @@rowcount
	
			--Deleting from SiteGroups
			DELETE FROM SISiteGroup WHERE SiteId = @id
			-- Insert into sitegroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')
		
			
	

if @error <> 0
  begin
      raiserror('DBERROR||%s',16,1,@stmt)
      return dbo.GetDataBaseErrorCode()
   end
if @rowcount = 0
  begin
    raiserror('CONCURRENCYERROR',16,1)
     return dbo.GetDataConcurrencyErrorCode()			-- concurrency error
end	
end
End
GO
PRINT N'Altering [dbo].[Membership_GetAllUsers]...';


GO
ALTER PROCEDURE [dbo].[Membership_GetAllUsers]  
(  
 @ProductId				uniqueidentifier = NULL,  
 @ProductIds			xml = NULL,  
 @ExcludedProductIds	xml = NULL,  
 @ApplicationId			uniqueidentifier = NULL,  
 @Status				int = NULL,  
 @IsSystemUser			bit = NULL,  
 @IsApproved			bit = NULL,  
 @IsLockedOut			bit = NULL,  
 @PageNumber			int = NULL,  
 @PageSize				int = NULL,  
 @IncludeExpiredUsers	bit = NULL,  
 @SortBy				nvarchar(100) = NULL,  
 @SortOrder				nvarchar(10) = NULL,  
 @FilterBy				nvarchar(500) = NULL,  
 @IncludeVariantUsers	bit = NULL,  
 @IgnoreStatus			bit = NULL,  
 @SinceExpired			datetime = NULL,  
 @UserIds				xml = NULL,  
 @UserId				uniqueidentifier = NULL,  
 @UserName				nvarchar(256) = NULL,
 @UserNameOnly			bit = NULL    
)  
AS  
BEGIN  
	DECLARE @tbVariantSiteIds TABLE (Id uniqueidentifier)  
	IF @ApplicationId IS NOT NULL  
		INSERT INTO @tbVariantSiteIds SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId)  
  
	DECLARE @tbProductIds TABLE (Id uniqueidentifier)  
	IF @ProductIds IS NOT NULL  
		INSERT INTO @tbProductIds   
		SELECT tab.col.value('@Id','uniqueidentifier') FROM @ProductIds.nodes('/Products/Product') tab(col)  
	ELSE IF @ProductId IS NOT NULL  
		INSERT INTO @tbProductIds VALUES (@ProductId)  
  
	DECLARE @tbUserIds TABLE (Id uniqueidentifier)  
	IF @UserIds IS NOT NULL  
		INSERT INTO @tbUserIds   
		SELECT tab.col.value('@Id','uniqueidentifier') FROM @UserIds.nodes('/Users/User') tab(col)  
  
	DECLARE @tbExcludedUserIds TABLE (Id uniqueidentifier primary key)  
	IF @ExcludedProductIds IS NOT NULL  
		INSERT INTO @tbExcludedUserIds  
		SELECT DISTINCT UserId FROM @ExcludedProductIds.nodes('/Products/Product') tab1(col)  
			JOIN USSiteUser S ON tab1.col.value('@Id','uniqueidentifier') = S.ProductId  
  
	IF @IncludeVariantUsers IS NULL SET @IncludeVariantUsers = 0  
	IF @Status < 0 SET @Status = NULL  
   
	IF(@UserId IS NULL AND @UserName IS NOT NULL)    
		SELECT @UserId = Id FROM dbo.USUser U   
			INNER JOIN dbo.USSiteUser S ON U.Id = S.UserId   
		WHERE LOWER(u.UserName) = LOWER(@UserName) AND    
			(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))) AND  
			(@ProductId IS NULL OR @ProductId = S.ProductId)     
   
	DECLARE @tbUser TYUser  
	INSERT INTO @tbUser  
	SELECT DISTINCT   
		U.Id,   
		U.UserName,    
		M.Email,  
		M.PasswordQuestion,   
		M.IsApproved,  
		U.CreatedDate,  
		M.LastLoginDate,  
		U.LastActivityDate,  
		M.LastPasswordChangedDate,  
		M.IsLockedOut,  
		M.LastLockoutDate,  
		U.FirstName,  
		U.LastName,  
		U.MiddleName,  
		U.ExpiryDate,  
		U.EmailNotification,  
		U.TimeZone,  
		U.ReportRangeSelection,  
		U.ReportStartDate,  
		U.ReportEndDate,  
		U.BirthDate,  
		U.CompanyName,  
		U.Gender,  
		U.HomePhone,  
		U.MobilePhone,  
		U.OtherPhone,  
		U.ImageId,  
		U.Status
	FROM dbo.USUser U  
		INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
		INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
		(@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser) AND  
		(@ApplicationId IS NULL OR  
		(@IncludeVariantUsers = 0 AND S.SiteId = @ApplicationId) OR  
		(@IncludeVariantUsers = 1 AND S.SiteId IN (Select Id FROM @tbVariantSiteIds))   
		)   
		INNER JOIN @tbProductIds P ON P.Id = S.ProductId  
	WHERE (  
		(@Status IS NULL AND U.Status != 3) OR  
		(@Status IS NOT NULL AND @Status = U.Status )  
		) AND  
		(  
		@IgnoreStatus = 1 OR  
		(         
			(@IsApproved IS NULL OR M.IsApproved = @IsApproved) AND  --Checks whether the user is approved.  
			(@IsLockedOut IS NULL OR M.IsLockedOut = @IsLockedOut) AND -- Checks whether the user is locked out.  
			(@IncludeExpiredUsers = 1 OR U.ExpiryDate IS NULL OR U.ExpiryDate >= getutcdate()) AND  
			(@SinceExpired IS NULL OR U.ExpiryDate < @SinceExpired)  
		)  
		) AND  
		U.Id NOT IN (SELECT Id FROM @tbExcludedUserIds) AND  
		(  
		@FilterBy IS NULL OR   
		U.UserName like @FilterBy OR U.FirstName like @FilterBy OR U.LastName like @FilterBy  
		) AND  
		(@UserIds IS NULL OR U.Id IN (SELECT Id FROM @tbUserIds)) AND  
		(@UserId IS NULL OR U.Id = @UserId)  
   
	IF @UserNameOnly = 1
		SELECT Id AS UserId, UserName, FirstName, LastName
			FROM @tbUser
	ELSE
		EXEC [dbo].[Membership_BuildUser]   
			@tbUser = @tbUser,   
			@SortBy = @SortBy,   
			@SortOrder = @SortOrder,  
			@PageNumber = @PageNumber,  
			@PageSize = @PageSize  
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultSharedDirectories]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultSharedDirectories]
(
	@SiteId			uniqueIdentifier,
	@ObjectType		int,
	@Level			nvarchar(4000),
	@UserId			uniqueIdentifier,
	@ParentId		uniqueIdentifier = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Lft bigint,  
	@Rgt bigint
BEGIN  
--********************************************************************************
-- code
--********************************************************************************
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId))
				And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM HSStructure where Id = @ParentId)

	SELECT @Lft = LftValue , @Rgt = RgtValue from HSStructure   
		Where Id = @ParentId AND SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId))

	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (S.ParentId =@ParentId OR S.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,
			D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,
			D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		--AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
		--	(Select ObjectId from @objectRole) OR S.ParentId IN 
		--	(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id   
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.Id=@ParentId And D.Status != dbo.GetDeleteStatus() And D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM HSStructure  C  Inner Join SISiteDirectory D ON C.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = C.Id  
		left outer join @objectCount FC ON FC.Id=C.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE C.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR C.Id=@ParentId)  
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
			  And C.LftValue Between M.LftValue and M.RgtValue  
			  And M.LftValue Not in (C.LftValue ,@Lft))  
				AND C. SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier,
	@ObjectType		int
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM HSStructure where Id = @ParentId)
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from HSStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from HSStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (S.ParentId =@ParentId OR S.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId )
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id 
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.Id=@ParentId And D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) AND D.Status != dbo.GetDeleteStatus()
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure  C  Inner Join SISiteDirectory D ON C.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=C.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE C.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR C.Id=@ParentId)
			And @ChildrenLevel>=(Select count(*) + 1 
				From HSStructure M 
				WHERE M.LftValue Between @Lft and @Rgt
				And C.LftValue Between M.LftValue and M.RgtValue
				And M.LftValue Not in (C.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND C.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultContentDirectories]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultContentDirectories]
(
 @SiteId				uniqueIdentifier,  
 @Level					nvarchar(4000),  
 @UserId				uniqueIdentifier,
 @AccessibleNodesOnly	bit = 0,
 @onlySharedNode bit = 0,
 @ParentId				uniqueIdentifier = null
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 7
	
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COContentStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COContentStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,
			D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COContentStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId = @ParentId OR D.Id = @ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			7 ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath,  
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,S.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,ISNULL(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites   
		FROM COContentStructure S 
			left outer join  @objectRole R on R.ObjectId = S.Id  
			left outer join @objectCount FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COContentStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites 
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COContentStructure  D 
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1 From COContentStructure M   
				WHERE M.LftValue Between @Lft and @Rgt  
					And D.LftValue Between M.LftValue and M.RgtValue  
					And M.LftValue Not in (D.LftValue ,@Lft))  
					AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultFileDirectories]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultFileDirectories]
(
 @SiteId				uniqueIdentifier,  
 @Level					nvarchar(4000),  
 @UserId				uniqueIdentifier,
 @AccessibleNodesOnly	bit = 0,
 @onlySharedNode bit = 0,
 @ParentId				uniqueIdentifier = null
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 9
   
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COFileStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFileStructure   
		Where Id = @ParentId AND SiteId = @SiteId
 
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFileStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <= -1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			@ObjectType ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath, S.InheritSecurityLevels, S.PropagateSecurityLevels,
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFileStructure S 
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFileStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFileStructure  D 
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
				And D.LftValue Between M.LftValue and M.RgtValue  
				And M.LftValue Not in (D.LftValue ,@Lft))  
				AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]
(
 @SiteId    uniqueIdentifier,      
 @Level     nvarchar(4000),      
 @UserId    uniqueIdentifier,    
 @AccessibleNodesOnly bit = 0,  
 @onlySharedNode bit = 0,
 @ParentId    uniqueIdentifier = null 
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 38
	
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COFormStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFormStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,
			D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFormStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId = @ParentId OR D.Id = @ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			38 ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath,  
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,S.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,ISNULL(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites   
		FROM COFormStructure S 
			left outer join  @objectRole R on R.ObjectId = S.Id  
			left outer join @objectCount FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFormStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites 
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFormStructure  D 
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1 From COFormStructure M   
				WHERE M.LftValue Between @Lft and @Rgt  
					And D.LftValue Between M.LftValue and M.RgtValue  
					And M.LftValue Not in (D.LftValue ,@Lft))  
					AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultImageDirectories]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultImageDirectories]
(
	@SiteId					uniqueIdentifier,  
	@Level					nvarchar(4000),  
	@UserId					uniqueIdentifier,
	@AccessibleNodesOnly	bit = 0,
	@onlySharedNode bit = 0,
	@ParentId				uniqueIdentifier = null
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 33
   
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COImageStructure where Id = @ParentId)
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COImageStructure   
		Where Id = @ParentId AND SiteId = @SiteId
 
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)   AllowAccessInChildrenSites
		FROM COImageStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			@ObjectType ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath, S.InheritSecurityLevels, S.PropagateSecurityLevels,
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure S 
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure  D 
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
				And D.LftValue Between M.LftValue and M.RgtValue  
				And M.LftValue Not in (D.LftValue ,@Lft))  
				AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 9

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COFileStructure where Id = @ParentId)
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFileStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COFileStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COFileStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 38

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COFormStructure where Id = @ParentId)
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFormStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COFormStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COFormStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]...';


GO
ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 33

	IF @SiteId IS NULL AND @ParentId IS NOT NULL
		SET @SiteId = (SELECT TOP 1 SiteId FROM COImageStructure where Id = @ParentId)
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COImageStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COImageStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COImageStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
GO
PRINT N'Altering [dbo].[Order_Get]...';


GO
ALTER PROCEDURE [dbo].[Order_Get]
    (
      @Searchkeyword NVARCHAR(4000) = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @PageSize INT = NULL ,
      @PageIndex INT = 1 ,
      @CustomerId UNIQUEIDENTIFIER = NULL ,
      @OrderDate DATETIME = NULL ,
      @StartDate DATETIME = NULL ,
      @EndDate DATETIME = NULL ,
      @OrderStatus XML = NULL ,
      @ExternalShippingStatus INT = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL    
    )
AS 
    BEGIN    
        DECLARE @tblOrderStatus TABLE ( OrderStatus INT )    
        IF @OrderStatus IS NOT NULL 
            BEGIN
                INSERT  @tblOrderStatus
                        ( OrderStatus
                        )
                        SELECT  tab.col.value('text()[1]', 'int')
                        FROM    @OrderStatus.nodes('GenericCollectionOfInt32/int') tab ( col )
            END
    
        IF @SearchKeyword IS NULL 
            SET @SearchKeyword = '%'    
        ELSE 
            SET @SearchKeyword = '%' + ( @Searchkeyword ) + '%'    
    
    
        IF @PageSize IS NULL 
            SET @PageSize = 2147483647    
    
        IF @SortColumn IS NULL 
            SET @SortColumn = 'OrderDate Desc'    
    
        SET @SortColumn = LOWER(@SortColumn)    
    
    	 
        DECLARE @lOrderDate DATETIME
        IF @OrderDate IS NULL 
            BEGIN
                DECLARE @lStartDate DATETIME
                IF @StartDate IS NULL 
                    SET @lStartDate = '01-01-1970'
                ELSE 
                    SET @lStartDate = @StartDate
			
                DECLARE @lEndDate DATETIME
                IF @EndDate IS NULL 
                    SET @lEndDate = '01-01-3000'
                ELSE 
                    SET @lEndDate = @EndDate
            END
        ELSE 
            BEGIN
                SET @lStartDate = @OrderDate
                SET @lEndDate = DATEADD(d, 1, @OrderDate)
            END


        SET @lStartDate = dbo.ConvertTimeToUtc(@lStartDate, @ApplicationId)
        SET @lEndDate = dbo.ConvertTimeToUtc(@lEndDate, @ApplicationId)

		 
        DECLARE @tblOrders TABLE
            (
              Row_ID INT PRIMARY KEY ,
              Id UNIQUEIDENTIFIER ,
              PurchaseOrderNumber NVARCHAR(100) ,
              ExternalOrderNumber NVARCHAR(100) ,
              OrderTypeId INT ,
              OrderDate DATETIME ,
              FirstName NVARCHAR(255) ,
              MiddleName NVARCHAR(255) ,
              LastName NVARCHAR(255) ,
              OrderStatusId INT ,
              Quantity MONEY ,
              NumSkus DECIMAL(38, 2) ,
              OrderTotal MONEY ,
              CODCharges MONEY ,
              TotalDiscount MONEY ,
              RefundTotal MONEY ,
              TaxableOrderTotal MONEY ,
              GrandTotal MONEY ,
              TotalTax MONEY ,
              ShippingTotal MONEY ,
              TotalShippingCharge MONEY ,
			  PaymentRemaining MONEY,
              SiteId UNIQUEIDENTIFIER ,
              CustomerId UNIQUEIDENTIFIER ,
              TotalShippingDiscount MONEY ,
              CreatedByFullName NVARCHAR(3074) ,
              Email NVARCHAR(255)
            ) ;
        WITH    PagingCTE ( Row_ID, Id, PurchaseOrderNumber, ExternalOrderNumber, OrderTypeId, OrderDate, FirstName, MiddleName, LastName, OrderStatusId, Quantity, NumSkus, OrderTotal, CODCharges, RefundTotal, TaxableOrderTotal, GrandTotal, TotalTax, ShippingTotal, TotalShippingCharge,PaymentRemaining, SiteId, CustomerId, TotalDiscount, TotalShippingDiscount, CreatedByFullName, Email )
                  AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN @SortColumn = 'purchaseordernumber desc'
                                                              THEN PurchaseOrderNumber
                                                             END DESC, CASE
                                                              WHEN @SortColumn = 'purchaseordernumber asc'
                                                              OR @SortColumn = 'purchaseordernumber'
                                                              THEN PurchaseOrderNumber
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'ordertypeid desc'
                                                              THEN CAST(OrderTypeId AS VARCHAR(25))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'ordertypeid asc'
                                                              OR @SortColumn = 'ordertypeid'
                                                              THEN CAST(OrderTypeId AS VARCHAR(25))
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'orderdate desc'
                                                              THEN CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'orderdate asc'
                                                              OR @SortColumn = 'orderdate'
                                                              THEN CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'firstname desc'
                                                              OR @SortColumn = 'customer.firstname desc'
                                                              THEN FirstName
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'firstname asc'
                                                              OR @SortColumn = 'firstname'
                                                              OR @SortColumn = 'customer.firstname asc'
                                                              THEN FirstName
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'createdByfullname desc'
                                                              OR @SortColumn = 'customer.createdByfullname desc'
                                                              THEN FirstName
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'createdByfullname asc'
                                                              OR @SortColumn = 'createdByfullname'
                                                              OR @SortColumn = 'customer.createdByfullname asc'
                                                              THEN FirstName
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'orderstatusid desc'
                                                              OR @SortColumn = 'orderstatus desc'
                                                              THEN CAST(OrderStatusId AS INT)
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'orderstatusid asc'
                                                              OR @SortColumn = 'orderstatusid'
                                                              OR @SortColumn = 'orderstatus asc'
                                                              THEN CAST(OrderStatusId AS INT)
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'quantity desc'
                                                              THEN Quantity
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'quantity asc'
                                                              OR @SortColumn = 'quantity'
                                                              THEN Quantity
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'ordertotal desc'
                                                              THEN OrderTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'ordertotal asc'
                                                              OR @SortColumn = 'ordertotal'
                                                              THEN OrderTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totaldiscount desc'
                                                              THEN TotalDiscount
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totaldiscount asc'
                                                              OR @SortColumn = 'totaldiscount'
                                                              THEN TotalDiscount
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totaltax desc'
                                                              THEN TotalTax
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totaltax asc'
                                                              OR @SortColumn = 'totaltax'
                                                              THEN TotalTax
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'taxableordertotal desc'
                                                              THEN TaxableOrderTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'taxableordertotal asc'
                                                              OR @SortColumn = 'taxableordertotal'
                                                              THEN TaxableOrderTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'grandtotal desc'
                                                              THEN GrandTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'grandtotal asc'
                                                              OR @SortColumn = 'grandtotal'
                                                              THEN GrandTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'numskus desc'
                                                              THEN GrandTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'numskus asc'
                                                              OR @SortColumn = 'numskus'
                                                              THEN NumSkus
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totalshippingcharge desc'
                                                              THEN TotalShippingCharge
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totalshippingcharge asc'
                                                              OR @SortColumn = 'totalshippingcharge'
                                                              THEN TotalShippingCharge
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totalshippingdiscount desc'
                                                              THEN TotalShippingDiscount
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totalshippingdiscount asc'
                                                              OR @SortColumn = 'totalshippingdiscount'
                                                              THEN TotalShippingDiscount
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'customerid desc'
                                                              THEN CAST(CustomerId AS CHAR(36))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'email desc'
                                                              THEN Email
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'email asc'
                                                              OR @SortColumn = 'email'
                                                              THEN Email
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'customerid asc'
                                                              OR @SortColumn = 'customerid'
                                                              THEN CAST(CustomerId AS CHAR(36))
                                                              ELSE CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END ASC ) AS [Row_ID] ,
                                Id ,
                                PurchaseOrderNumber ,
                                ExternalOrderNumber ,
                                OrderTypeId ,
                                OrderDate ,
                                FirstName ,
                                MiddleName ,
                                LastName ,
                                OrderStatusId ,
                                Quantity ,
                                NumSkus ,
                                OrderTotal ,
                                CODCharges ,
                                RefundTotal ,
                                TaxableOrderTotal ,
                                GrandTotal ,
                                TotalTax ,
                                ShippingTotal ,
                                TotalShippingCharge ,
								PaymentRemaining,
                                SiteId ,
                                CustomerId ,
                                TotalDiscount ,
                                TotalShippingDiscount ,
                                CreatedByFullName ,
                                Email
                       FROM     vwSearchOrder
                       WHERE    ( @CustomerId IS NULL
                                  OR CustomerId = @CustomerId
                                )
                                AND ( SiteId = @ApplicationId )
                                AND OrderDate BETWEEN @lStartDate AND @lEndDate
                                AND ( @OrderStatus IS NULL
                                      OR OrderStatusId IN ( SELECT
                                                              OrderStatus
                                                            FROM
                                                              @tblOrderStatus )
                                    )
                                AND ( @Searchkeyword = '%'
                                      OR ( PurchaseOrderNumber ) LIKE @SearchKeyword
                                      OR ( ExternalOrderNumber ) LIKE @SearchKeyword
                                      OR ( FirstName ) LIKE @SearchKeyword
                                      OR ( LastName ) LIKE @SearchKeyword
                                      OR ( MiddleName ) LIKE @SearchKeyword
                                      OR ( Email ) LIKE @SearchKeyword
                                    )
                                AND ( @ExternalShippingStatus IS NULL
                                      OR ExternalShippingStatus = @ExternalShippingStatus
                                    )
                     )
            INSERT  INTO @tblOrders
                    SELECT  Row_ID ,
                            Id ,
                            PurchaseOrderNumber ,
                            ExternalOrderNumber ,
                            OrderTypeId ,
                            dbo.ConvertTimeFromUtc(OrderDate, @ApplicationId) OrderDate ,
                            FirstName ,
                            MiddleName ,
                            LastName ,
                            OrderStatusId ,
                            Quantity ,
                            NumSkus ,
                            OrderTotal ,
                            CODCharges ,
                            TotalDiscount ,
                            RefundTotal ,
                            TaxableOrderTotal ,
                            GrandTotal ,
                            TotalTax ,
                            ShippingTotal ,
                            TotalShippingCharge , 
							PaymentRemaining,
                            SiteId ,
                            CustomerId ,
                            TotalShippingDiscount ,
                            CreatedByFullName ,
                            Email
                    FROM    PagingCTE pcte    



        SELECT  *
        FROM    @tblOrders
        WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                AND Row_ID <= @PageSize * @PageIndex    
    
        SELECT  COUNT(*) AS [Count] ,
                ISNULL(SUM(GrandTotal), 0) Total
        FROM    @tblOrders 

        SELECT  OS.[Id] ,
                OS.[OrderId] ,
                OS.[ShippingOptionId] ,
                OS.[OrderShippingAddressId] ,
                dbo.ConvertTimeFromUtc(OS.[ShipOnDate], @ApplicationId) ShipOnDate ,
                dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate], @ApplicationId) ShipDeliveryDate ,
                OS.[TaxPercentage] ,
                OS.[TaxPercentage] ,
                OS.[ShippingTotal] ,
                OS.[Sequence] ,
                OS.[IsManualTotal] ,
                OS.[Greeting] ,
                OS.[Tax] ,
                OS.[SubTotal] ,
                OS.[TotalDiscount] ,
                OS.[ShippingCharge] ,
                OS.[Status] ,
                OS.[CreatedBy] ,
                dbo.ConvertTimeFromUtc(OS.[CreatedDate], @ApplicationId) CreatedDate ,
                OS.[ModifiedBy] ,
                dbo.ConvertTimeFromUtc(OS.[ModifiedDate], @ApplicationId) ModifiedDate ,
                OS.[ShippingDiscount] ,
                OS.ShipmentHash ,
                OS.IsBackOrder ,
                OS.IsHandlingIncluded ,
                OS.ShippingChargeTax
        FROM    OROrderShipping OS
                JOIN @tblOrders T ON T.Id = OS.OrderId
        WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                AND Row_ID <= @PageSize * @PageIndex ;
 
 
 
 
        WITH    cteShipmentItemQuantity
                  AS ( SELECT   SUM(SI.Quantity) Quantity ,
                                OI.Id
                       FROM     FFOrderShipmentItem SI
                                INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId = S.Id
                                INNER JOIN OROrderItem OI ON SI.OrderItemId = OI.Id
                                INNER JOIN @tblOrders T ON T.Id = OI.OrderId
                       WHERE    ShipmentStatus = 2
                       GROUP BY OI.Id
                     )
            SELECT  OI.[Id] ,
                    OI.[OrderId] ,
                    OI.[OrderShippingId] ,
                    OI.[OrderItemStatusId] ,
                    OI.[ProductSKUId] ,
                    OI.[Quantity] ,
                    ISNULL(SQ.Quantity, 0) AS ShippedQuantity ,
                    OI.[Price] ,
                    OI.[UnitCost] ,
                    OI.[IsFreeShipping] ,
                    OI.[Sequence] ,
                    dbo.ConvertTimeFromUtc(OI.CreatedDate, @ApplicationId) CreatedDate ,
                    OI.[CreatedBy] ,
                    FN.UserFullName CreatedByFullName ,
                    dbo.ConvertTimeFromUtc(OI.ModifiedDate, @ApplicationId) ModifiedDate ,
                    OI.[ModifiedBy] ,
                    OI.[ParentOrderItemId] ,
                    OI.[HandlingCharge] ,
                    OI.[BundleItemId] ,
                    OI.[CartItemId] ,
                    OI.[Tax] ,
                    OI.[Discount]
            FROM    OROrderItem OI
                    INNER JOIN @tblOrders T ON T.Id = OI.OrderId
                    LEFT JOIN cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
                    LEFT JOIN VW_UserFullName FN ON FN.UserId = OI.CreatedBy
            WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                    AND Row_ID <= @PageSize * @PageIndex    

 
    
    END
GO
PRINT N'Altering [dbo].[Order_GetRandomOrderNumber]...';


GO

ALTER PROCEDURE [dbo].[Order_GetRandomOrderNumber]  
(  
 @OrderNumberPrefix nvarchar(25)='ON:'  
)  
As  
Begin  
 Declare @digit int  
 Declare @inc int  
 Declare @orderNumber decimal(18)  
 Set @digit = (SELECT top 1 cast(Value as int) FROM GLIdentity Where Name = 'OrderNumberDigits')  
 Set @digit =isnull(@digit,4)  
 Set @inc = 0  
 while (@inc<=10)  
 begin  
  Set @OrderNumber = cast(rand() * power(10,@digit) as decimal(18))  
  if exists(Select 1 from OROrder Where PurchaseOrderNumber = isnull(@OrderNumberPrefix,'') + cast(@OrderNumber as varchar(18)))  
   Set @inc =@inc + 1  
  else  
  begin  
     Select @OrderNumber  
     return  
   end  
 if(@inc=10)  
 begin   
  Set @digit =@digit + 1  
  Set @inc=0  
  Delete from GLIdentity Where Name = 'OrderNumberDigits'  
  Insert into GLIdentity(Id,Name,Value)values (newid(),'OrderNumberDigits',@digit)  
 end  
 end  
  RAISERROR('INVALID||unable to generate order number', 16, 1)  
  RETURN dbo.GetBusinessRuleErrorCode()  
End
GO
PRINT N'Altering [dbo].[PageDefinition_Get]...';


GO
ALTER PROCEDURE [dbo].[PageDefinition_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageDefinitionIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageDefinitionIds
	SELECT PageDefinitionId FROM PageDefinition
		WHERE SiteId = @siteId AND PageStatus != 3 AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT M.PageDefinitionId, M.PageMapNodeId, SiteId, M.DisplayOrder, --PageDefinitionXml,
		TemplateId, Title, Description, PageStatus, WorkflowState, PublishCount, PublishDate, FriendlyName,
		WorkflowId, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, ArchivedDate, StatusChangedDate, 
		AuthorId, EnableOutputCache, OutputCacheProfileName, ExcludeFromSearch, IsDefault, IsTracked,
		HasForms, TextContentCounter, CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,
		ScheduledPublishDate, ScheduledArchiveDate, NextVersionToPublish
	FROM PageDefinition D 
		JOIN PageMapNodePageDef M ON D.PageDefinitionId = M.PageDefinitionId
		JOIN @PageDefinitionIds T ON D.PageDefinitionId = T.Id
	ORDER BY DisplayOrder ASC

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId
	FROM USSecurityLevelObject S
		JOIN @PageDefinitionIds T ON S.ObjectId = T.Id
	WHERE ObjectTypeId = 8
		
	SELECT PageDefinitionId, 
		StyleId, CustomAttributes 
	FROM SIPageStyle S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id
		
	SELECT PageDefinitionId, 
		ScriptId, CustomAttributes 
	FROM SIPageScript S
		JOIN @PageDefinitionIds T ON S.PageDefinitionId = T.Id

	SELECT P.PageDefinitionId, 
		P.PageMapNodeId, P.DisplayOrder 
	FROM PageMapNodePageDef P
		JOIN @PageDefinitionIds T ON P.PageDefinitionId = T.Id
END
GO
PRINT N'Altering [dbo].[PageMapNode_Get]...';


GO
ALTER PROCEDURE [dbo].[PageMapNode_Get]
(
	@siteId		uniqueidentifier,
	@startDate	datetime = null
)
AS
BEGIN
	DECLARE @PageMapNodeIds TABLE(Id uniqueidentifier)
	INSERT INTO @PageMapNodeIds
	SELECT PageMapNodeId FROM PageMapNode
		WHERE SiteId = @siteId AND (@startDate IS NULL OR ModifiedDate > @startDate)

	SELECT PageMapNodeId,ParentId,LftValue,RgtValue, SiteId, ExcludeFromSiteMap, --PageMapNodeXml, 
		Description, DisplayTitle, FriendlyUrl, MenuStatus, TargetId, Target, TargetUrl, CreatedBy,
		CreatedDate, ModifiedBy, ModifiedDate, PropogateWorkflow, InheritWorkflow, PropogateSecurityLevels,
		InheritSecurityLevels, PropogateRoles, InheritRoles, Roles, LocationIdentifier, HasRolloverImages,
		RolloverOnImage, RolloverOffImage, IsCommerceNav, AssociatedQueryId, DefaultContentId, 
		AssociatedContentFolderId, CustomAttributes, HiddenFromNavigation, SourcePageMapNodeId
	FROM PageMapNode P 
		JOIN @PageMapNodeIds T ON P.PageMapNodeId = T.Id
	ORDER BY LftValue

	SELECT S.ObjectId, 
		S.ObjectTypeId, S.SecurityLevelId 
	FROM USSecurityLevelObject S
		JOIN @PageMapNodeIds T ON S.ObjectId = T.Id
	WHERE S.ObjectTypeId = 2

	SELECT W.Id, W.PageMapNodeId, 
		W.WorkflowId, W.CustomAttributes 
	FROM PageMapNodeWorkflow W
		JOIN @PageMapNodeIds T ON W.PageMapNodeId = T.Id
	
	SELECT M.PageDefinitionId, 
		M.PageMapNodeId, M.DisplayOrder 
	FROM PageMapNodePageDef M
		JOIN @PageMapNodeIds T ON M.PageMapNodeId = T.Id

	SELECT P.PageMapNodeId, C.ContentId, RelativePath, FileName FROM COFile C
		JOIN PageMapNode P ON C.ContentId = P.TargetId
		JOIN @PageMapNodeIds T ON P.PageMapNodeId = T.Id
END
GO
PRINT N'Altering [dbo].[PageMapNode_ReOrder]...';


GO
ALTER PROCEDURE [dbo].[PageMapNode_ReOrder] 
(
	@SiteId uniqueidentifier,	
	@PageMapNodeId uniqueidentifier,
	@Index int
)	
AS 
Begin	
	Declare @CurrentLft int,@CurrentRgt int,@NewLft int,@Width int,@Change int,@SibRgt int
	Declare @parentId uniqueidentifier
	
	if(@Index is null)
	Begin
		RAISERROR ('ISNULL||Index',16,1)
		return(dbo.GetBusinessRuleErrorCode())
	end	

	if(@PageMapNodeId is null)
	Begin
		RAISERROR ('ISNULL||PageMapNodeId',16,1)
		return(dbo.GetBusinessRuleErrorCode())
	end	

	Select @CurrentLft = LftValue, @CurrentRgt = RgtValue from PageMapNode 
		Where PageMapNodeId = @PageMapNodeId and SiteId = @SiteId

	Select @parentId = ParentId	from PageMapNode 
		Where PageMapNodeId = @PageMapNodeId and SiteId = @SiteId

	if(@Index <0)
	Begin
			RAISERROR ('INVALID||Index',16,1)
			return(dbo.GetBusinessRuleErrorCode())
	end	

	If @Index =0 
	begin
		Select @sibRgt =LftValue from PageMapNode 
			Where PageMapNodeId = @parentId and SiteId = @SiteId
	end
	Else
	begin
		Declare @CurrentIndex int
		Select @CurrentIndex = count(*)  from PageMapNode
			Where ParentId = @ParentId and LftValue < @CurrentLft and SiteId = @SiteId

		Set @Index= @Index + CASE When @CurrentIndex < @Index Then 1 Else 0 End
		
		Select top (@Index) @sibRgt = RgtValue from PageMapNode
			Where ParentId = @parentId and SiteId = @SiteId Order by LftValue
	End

	Set @Width = @CurrentRgt-@CurrentLft
	Set @NewLft =  CASE WHEN @SibRgt > @CurrentLft Then @SibRgt - @Width ELSE @SibRgt + 1 END
	Set @Change =   @NewLft - @CurrentLft

	select @CurrentLft as CurrentLft, @CurrentRgt as CurrentRgt, @Change as Change, 
		@SibRgt as SibRgt, @Width as Width

	Update PageMapNode Set LftValue = 
			 CASE WHEN LftValue Between @CurrentLft and @CurrentRgt THEN LftValue + @Change
				ELSE CASE WHEN LftValue Between @CurrentRgt and @SibRgt 
						THEN LftValue - @Width -1
						ELSE CASE WHEN LftValue  Between @SibRgt +1 and @CurrentLft -1 
							THEN LftValue + @Width + 1  ELSE LftValue END 
					END
			 END ,
		RgtValue = 
			 CASE WHEN RgtValue Between @CurrentLft and @CurrentRgt THEN RgtValue + @Change
				ELSE CASE WHEN (RgtValue Between @CurrentRgt + 1 and @SibRgt) 
						THEN RgtValue - @Width -1
						ELSE CASE WHEN RgtValue Between @SibRgt and @CurrentLft
							THEN RgtValue + @Width + 1 ELSE RgtValue END 
						END
			 END
		Where ((LftValue Between @CurrentLft and @SibRgt) or (LftValue Between @SibRgt and @CurrentRgt))
			And SiteId=@SiteId

	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @parentId, 2, 2, null, 1

end
GO
PRINT N'Creating [dbo].[UploadContactDataLog_Get]...';

GO
IF (Object_Id('UploadContactDataLog_Get') IS NOT NULL)
	DROP PROCEDURE UploadContactDataLog_Get
GO
GO
CREATE PROCEDURE [dbo].[UploadContactDataLog_Get]
	@Id uniqueidentifier
AS
BEGIN
	select Id, UploadHistoryId, ErrorType, ErrorMessage from UploadContactDataLog where @Id is null or Id=@Id
END
GO
PRINT N'Creating [dbo].[UploadContactDataLog_Save]...';
GO
IF (Object_Id('UploadContactDataLog_Save') IS NOT NULL)
	DROP PROCEDURE UploadContactDataLog_Save
GO
CREATE PROCEDURE [dbo].[UploadContactDataLog_Save]
(
	@Id uniqueidentifier output,
	@UploadHistoryId uniqueidentifier,
	@ErrorType int,
	@ErrorMessage nvarchar(max)	
)
AS
BEGIN
	if(@Id is null or @Id= dbo.GetEmptyGUID())
	begin
		set @Id = newid()
		INSERT INTO [dbo].[UploadContactDataLog]
           ([Id]
           ,[UploadHistoryId]
           ,[ErrorType]
           ,[ErrorMessage])
		 VALUES
			   (@Id
			   ,@UploadHistoryId 
			   ,@ErrorType 
			   ,@ErrorMessage)

	end
	else
	begin
		Update [dbo].[UploadContactDataLog]
		set UploadHistoryId=@UploadHistoryId, ErrorType=@ErrorType,ErrorMessage=@ErrorMessage where ID =@Id 
	end
END
GO
ALTER Procedure [dbo].[Site_CreateCommerceDefaultData](@ApplicationId uniqueidentifier, @SiteTitle Varchar(1024), @CreatedById uniqueidentifier)
As
BEGIN

	DECLARE @CommerceProductId uniqueidentifier 
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'
	DECLARE @Now DateTime, @maxId int 
	DECLARE @maxSecurityId int
	
	SET @Now = GETUTCDATE()

	-- Product Category
	IF NOT EXISTS(SELECT * FROM PRCategory WHERE Title ='Unassigned')
	BEGIN
		INSERT INTO PRCategory (Id, Title, Description, Sequence, CreatedDate, Status)
		Values('6988B7A8-7C72-4B58-9B68-258B940E2363', 'Unassigned', 'Unassigned', 1, @Now, 1)
	END

	--Insert default value for order number
	IF NOT EXISTS(SELECT * FROM GLIdentity WHERE Name ='OrderNumber' )
	BEGIN
		INSERT INTO GLIdentity (Name,Value) Values ('OrderNumber', 0)
	END

	-- Product Import Config - Did not take it.

	-- Coupon Type
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('D1CA3949-EBFA-4DEC-B120-07DAA8961F81',	'% off Order','',		1,	1,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('2C24DC1C-268E-4ACB-933C-618DEF52E434',	'$ off Order',	'',	1,	2,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('C4FE5439-0B9F-4B41-9221-0302C527DEAE',	'$ off Shipping',	'',	1,	32,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('894D41B5-27D9-4C3B-885D-18FFC565DB8B',	'% off Shipping',	'',	1,	4,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='Flat rate shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('004202FA-B429-4AB7-A59F-ED37C74C0421',	'Flat rate shipping','',		1,	5,	1)
	END

	--Customer Group.sql
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Gold')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Gold',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Silver')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Silver',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Bronze')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Bronze',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Frequent Customer')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Frequent Customer',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='B2B')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'B2B',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Friends & Family')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Friends & Family',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Employees')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Employees',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	-- EveryOne Group - We are not using this group as of now
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Everyone')
	BEGIN
		INSERT INTO USGroup (ProductId, ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, 
		ModifiedBy, ModifiedDate, Status, ExpiryDate, GroupType)
		Values
		(@CommerceProductId,	@ApplicationId,	
		NEWID(),	'Everyone',	'everyone',	
		@CreatedById,	@Now,
		@CreatedById,	@Now,	1,	NULL,	2)
	END

	--- Security Level
	SELECT @maxSecurityId = Isnull(Max(Id),0) + 1 FROM USSecurityLevel

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Commerce Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId, 'Commerce Customer', 'Commerce Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Preferred Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+1, 'Preferred Customer', 'Preferred Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Employee')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+2, 'Employee', 'Employee', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Press')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+3, 'Press', 'Press', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	-- Feature Type
	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto Top Seller')
	BEGIN
		Insert into MRFeatureType Values(1,'Auto Top Seller')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto New Products')
	BEGIN
		Insert into MRFeatureType Values(2,'Auto New Products')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Manual')
	BEGIN
		Insert into MRFeatureType Values(3,'Manual')
	END

	-- Feature
	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Top Sellers' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Top Sellers' , 'Top Sellers Auto', 1, 1, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='New Products' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'New Products' , 'New Products Auto', 2,  2, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Moving Brand' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Moving Brand' , 'Moving Brand Manual', 3, 3, 1, newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Stock Clearance' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Stock Clearance' , 'Stock Clearance Manual', 3,  4, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Christmas Sales' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Christmas Sales' , 'Christmas Sales Manual', 3,  5, 2,newId(), GetUTCDate())

	-- AttributeDataType
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Image Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Image Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Date' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.DateTime','Date')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Decimal' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Double','Decimal')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Asset Library File' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Asset Library File')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Page Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Page Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='HTML' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','HTML')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='File Upload' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','File Upload')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Integer' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Int32','Integer')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='String' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','String')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Boolean')  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Boolean','Boolean')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Content Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Content Library')  END

	-- CommerceStatus 
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Active' )  BEGIN   INSERT INTO CMCommerceStatus values(1,'Active','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Inactive' )  BEGIN   INSERT INTO CMCommerceStatus values(2,'Inactive','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Deleted' )  BEGIN   INSERT INTO CMCommerceStatus values(3,'Deleted','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Approved' )  BEGIN   INSERT INTO CMCommerceStatus values(4,'Approved','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Rejected' )  BEGIN   INSERT INTO CMCommerceStatus values(5,'Rejected','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='PendingForApproval' )  BEGIN   INSERT INTO CMCommerceStatus values(6,'PendingForApproval','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Included' )  BEGIN   INSERT INTO CMCommerceStatus values(7,'Included','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Excluded')  BEGIN   INSERT INTO CMCommerceStatus values(8,'Excluded','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Expired')  BEGIN   INSERT INTO CMCommerceStatus values(9,'Expired','')  END

	-- GLReason
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Damaged')  BEGIN   INSERT INTO GLReason values(NEWID(),'Damaged',210,4,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Restocking')  BEGIN   INSERT INTO GLReason values(NEWID(),'Restocking',210,1,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Bad Inventory')  BEGIN   INSERT INTO GLReason values(NEWID(),'Bad Inventory',210,2,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Returned Item')  BEGIN   INSERT INTO GLReason values(NEWID(),'Returned Item',210,8,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Lost')  BEGIN   INSERT INTO GLReason values(NEWID(),'Lost',210,3,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Physical Count')  BEGIN   INSERT INTO GLReason values(NEWID(),'Physical Count',210,6,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='New Shipment')  BEGIN   INSERT INTO GLReason values(NEWID(),'New Shipment',210,5,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Other')  BEGIN   INSERT INTO GLReason values(NEWID(),'Other',210,7,1)  END

	-- GLAddress
	IF NOT EXISTS (SELECT * FROM GLAddress Where Id = 'A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F')
	BEGIN
		INSERT INTO  GLAddress(
		Id, FirstName, LastName, AddressType, AddressLine1, AddressLine2, AddressLine3, City, StateId, Zip, 
		CountryId, Phone, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, StateName, County)
		Values
		('A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F',
		'Warehouse','Location',0,'Your warehouse address', 250,	NULL	,'City',
		'7E07B39B-BC66-4969-B16A-C62E41043E39',	'30092',	'AD72C013-B99D-4513-A666-A128343CCFF0',	'7819955555',
		'2011-08-01 11:18:32.880',	'E131010D-BE50-4526-B343-B0954E924080',	'2011-08-01 11:18:32.880',	
		'E131010D-BE50-4526-B343-B0954E924080',	1,	'MA','County')	
	END

	-- INWarehouse
	Declare @WarehouseId uniqueidentifier
	DECLARE @CopyWarehouseId uniqueidentifier
		
	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId Where Title = 'Default Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Default Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
			Values(@WarehouseId,	'Default warehouse',	
			'Default warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
			null,	1, 'Internal', 'InternalWarehouseProvider', 1,1)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
		from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
		Where T.IsDownloadableMedia =0 OR T.IsDownloadableMedia is null

	END

	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId  Where Title = 'Digital Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Digital Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
		Values(@WarehouseId,	'Digital Warehouse',	
		'Internal Digital Warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
		Null,	0, Null, 'DigitalWarehouseProvider', 2,0)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
			from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
			Where T.IsDownloadableMedia =1 
	END

	-- Product Type
	--IF NOT EXISTS (SELECT * FROM PRProductType Where SiteId = @ApplicationId)
	IF ((SELECT COUNT(*) FROM PRProductType)=0)
	BEGIN
		Insert into PRProductType(Id,ParentId,LftValue,RgtValue,Title, Description, SiteId, CreatedDate, CreatedBy, Status)
		Values(newId(),@ApplicationId,1,2,@SiteTitle, @SiteTitle ,@ApplicationId, @Now,@CreatedById ,1 )
	END

	-- ORShipper
	DECLARE @Shipper_USPS uniqueidentifier, @Shipper_UPS uniqueidentifier, @Shipper_Fedex uniqueidentifier

	IF EXISTS (SELECT * FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_USPS = (SELECT Id FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId)
	END	
	ELSE
	BEGIN
		SET @Shipper_USPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_USPS,'USPS','USPS','','iAppsUSPSProvider',1,3,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END	
	IF EXISTS (SELECT * FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_UPS = (SELECT Id FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_UPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_UPS,'UPS','UPS','http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum={0}&AgreeToTermsAndConditions=yes&ignore=&track.x=46&track.y=7',
		'iAppsUpsProvider',1,2,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END
	IF EXISTS (SELECT * FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_Fedex = (SELECT Id FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_Fedex = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_Fedex,'Fedex','Fedex','','provider',1,1,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END

	 
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpressSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpressSM',
		'Worldwide ExpressSM','',5,0,0,0,0,1,'07',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
		
	END	
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air Saver','Next Day Air Saver',
		'customid',1,5,0,0,1,0,'13',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='UPS Next Day Air Early A.M. SM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'UPS Next Day Air Early A.M. SM','UPS Next Day Air® Early A.M. SM',
		'',1,0,0,0,0,0,'14',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Next Flight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Next Flight',
		'','',5,0,0,0,0,1,'INTERNATIONAL_FIRST',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Standard','Standard','',8,0,0,0,0,1,'11',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air A.M' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air A.M','Second Day Air A.M','',2,0,0,0,0,0,'59',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First-Class Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'First-Class Mail','1st Class','customid',5,5,0,0,1,0,'0',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International  Economy' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International  Economy','','',7,0,0,0,0,1,'INTERNATIONAL_ECONOMY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Saver','Saver','',4,0,0,0,0,0,'65',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day A.M.' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day A.M.','','',0,0,0,0,0,0,'FEDEX_2_DAY_AM',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpeditedSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpeditedSM','Worldwide ExpeditedSM','',4,0,0,0,0,1,'08',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Three-Day Select' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Three-Day Select','UPS Three-Day Select','customid',3,5,0,0,1,0,'12',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air','UPS Second Day Air','customid',2,5,0,0,1,0,'02',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Priority Overnight','','',1,0,0,0,0,1,'PRIORITY_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Priority' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Priority','','',0,0,0,0,0,1,'INTERNATIONAL_PRIORITY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Priority Mail','Priority Mail','customid',2,5,0,0,1,0,'1',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Express Saver','','',3,0,0,0,0,0,'FEDEX_EXPRESS_SAVER',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'First Overnight','Collège dOccitanie','',4,0,0,0,0,1,'FIRST_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Home Delivery' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Home Delivery','','customid',2,5,0,0,1,0,'GROUND_HOME_DELIVERY',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air','UPS Next Day Air','customid',1,5,0,0,1,0,'01',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide Express PlusSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide Express PlusSM','Worldwide Express PlusSM','',4,0,0,0,0,1,'54',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day','','',5,0,0,0,0,0,'FEDEX_2_DAY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Standard Overnight','','',4,0,0,0,0,0,'STANDARD_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Express','Express','customid',1,5,0,0,1,0,'2',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Ground' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Ground','UPS Ground','customid',5,5,0,0,1,0,'03',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	
	
	
	-- ATAttributeGroup
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='01614e00-86f8-4448-9a48-7daa63171c38')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'01614e00-86f8-4448-9a48-7daa63171c38', N'Features', @ApplicationId, 1, @CreatedById, @Now, @CreatedById, @Now , 1)
	END	
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='b23ffc1b-ab9d-4985-beb4-a5f303b9ef41')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'SKULevel Attributes', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Advanced Details', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'AB730D27-5CCD-49D9-B515-168290438E0A', N'Product Properties', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='3C211CF2-A184-4375-A078-242F4387D1D2')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'3C211CF2-A184-4375-A078-242F4387D1D2', N'Customer Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='9DABE135-467C-459E-ABF2-5FBD64821989')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'9DABE135-467C-459E-ABF2-5FBD64821989', N'Order Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A', N'Advanced Details', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END

	-- ATAttribute
	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0611ed6a-d071-4dff-b6c1-b75a06a17ccf')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as Top Seller', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now,  1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0b94496d-9df4-4ac1-8c5c-48b42f99c4ef')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Discounts', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='2c4983dd-d7eb-40a2-9b05-bfd308e3da62')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Free Shipping', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='4ae05c5a-2375-49d8-8bfb-d131632c0e09')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as New Item', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='7b39ffe5-36b9-44ef-b7a5-08f3c18181aa')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Refundable', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c1a20d3c-985f-49be-9682-df09eaf82fc8')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Freight', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c57665fd-e681-4c13-82ce-d77e60bf9a72')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Tax Exempt', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f4963a96-57dc-4c67-830e-f7426539c8ce')
	--INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	--[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	--VALUES (N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Unlimited Quantity', N'', 1, 
	--NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f68ceb85-922b-4023-a6f9-20025f7fdc94')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'System.String', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Others', 
	NULL, 0, N'AT1', 1, 1, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f7102f66-4242-408e-a816-f8b3e600f4d0')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Shipping Promotions', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='39C2D877-525F-49A6-9E48-D3296989F83B')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('39C2D877-525F-49A6-9E48-D3296989F83B','System.String','AB730D27-5CCD-49D9-B515-168290438E0A','Product Name',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='ECB9E9D1-FC3F-43A5-B40F-3840212F693C')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('ECB9E9D1-FC3F-43A5-B40F-3840212F693C','System.Double','AB730D27-5CCD-49D9-B515-168290438E0A','ListPrice',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	-- ATAttributeEnum
	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='bbf74b61-05d8-49b4-8fee-72147d849814')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'bbf74b61-05d8-49b4-8fee-72147d849814', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='073c66b8-2fc2-4d82-aebe-3165f0138ad5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'073c66b8-2fc2-4d82-aebe-3165f0138ad5', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='51424fd1-0da2-4792-b692-7beef5679bf3')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'51424fd1-0da2-4792-b692-7beef5679bf3', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='f0974b83-4d83-4534-bf87-1f8a8b0237e8')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'f0974b83-4d83-4534-bf87-1f8a8b0237e8', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='0b99d94e-606a-43a7-b72c-d6512fd38bf2')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'0b99d94e-606a-43a7-b72c-d6512fd38bf2', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='235f7a94-52c7-42d8-b870-c0d53906b1e5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'235f7a94-52c7-42d8-b870-c0d53906b1e5', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='11bfcfbd-3ffa-4791-8b39-1f12b80d8524')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'11bfcfbd-3ffa-4791-8b39-1f12b80d8524', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='d8d961c0-5c4a-487b-9913-a920e73cda1a')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'd8d961c0-5c4a-487b-9913-a920e73cda1a', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='3a1e07c0-18d1-4c00-a73c-73a4953d5e41')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'3a1e07c0-18d1-4c00-a73c-73a4953d5e41', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='872eeb49-e8eb-4f73-837c-71ddb20629c4')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'872eeb49-e8eb-4f73-837c-71ddb20629c4', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='ee919400-c7de-4d9c-ae8f-fefd1c768e64')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'ee919400-c7de-4d9c-ae8f-fefd1c768e64', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='49a6b896-2f22-45d1-82fb-e47e8f2519c9')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'49a6b896-2f22-45d1-82fb-e47e8f2519c9', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='8bb63596-8fee-4005-9109-dda014ba147e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'8bb63596-8fee-4005-9109-dda014ba147e', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='70a27f03-06ca-4df2-99bb-5c23b840d465')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'70a27f03-06ca-4df2-99bb-5c23b840d465', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='e5f89737-8879-4e42-9aa1-bfffb05d808e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'e5f89737-8879-4e42-9aa1-bfffb05d808e', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='9b0a9c92-80c8-411f-9037-b0281c755201')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'9b0a9c92-80c8-411f-9037-b0281c755201', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='81a55708-cd4d-4fbd-b3c5-4e3292401511')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'81a55708-cd4d-4fbd-b3c5-4e3292401511', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='a0c6381b-2970-4d09-a582-334b93c51442')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'a0c6381b-2970-4d09-a582-334b93c51442', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'true', N'0', 0, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='691ee3cd-58ff-42d9-a8e8-385e85648cfc')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'691ee3cd-58ff-42d9-a8e8-385e85648cfc', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'false', N'1', 2, N'false', 0, 2, @CreatedById, @Now, @CreatedById, @Now, 1)


	--- CLImageType
	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='396b425c-e197-4f2d-a4a3-25448ca366f8')
	INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('396b425c-e197-4f2d-a4a3-25448ca366f8', N'main', 600, 800, null, N'alt_mainImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 1)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='63cf2418-684a-4d1f-88b2-34968c6f139f')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('63cf2418-684a-4d1f-88b2-34968c6f139f', N'system_thumb', 60, 60, N'systhumb_', null, 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 0)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='2770534e-4675-4764-a620-56776398e03e')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('2770534e-4675-4764-a620-56776398e03e', N'preview', 260, 372, N'prev_', N'alt_PrevImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 3)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='e02ed0f3-1271-4218-b81e-816e458434a1')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('e02ed0f3-1271-4218-b81e-816e458434a1', N'mini', 80, 80, N'mini_', N'alt_miniImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 4)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='ba9b3922-4f9f-4b21-a5cf-e03b26b588ad')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('ba9b3922-4f9f-4b21-a5cf-e03b26b588ad', N'thumbnail', 120, 120, N'thumb_', N'alt_thumbImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 2)

	--OROrderStatus
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='OK')  BEGIN   INSERT INTO OROrderStatus values(1,'OK','Submitted',1,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Shipped')  BEGIN   INSERT INTO OROrderStatus values(2,'Shipped','Shipped',2,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Canceled')  BEGIN   INSERT INTO OROrderStatus values(3,'Canceled','Canceled',3,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Back Ordered')  BEGIN   INSERT INTO OROrderStatus values(4,'Back Ordered','Back Ordered',4,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Approval')  BEGIN   INSERT INTO OROrderStatus values(5,'Pending Approval','On Hold',5,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Customer Registration Approval')  BEGIN   INSERT INTO OROrderStatus values(6,'Pending Customer Registration Approval','Pending Customer Registration Approval',6,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Bad Customer Order Approval')  BEGIN   INSERT INTO OROrderStatus values(7,'Pending Bad Customer Order Approval','Pending Bad Customer Order Approval',7,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Terms Limit')  BEGIN   INSERT INTO OROrderStatus values(8,'Exceeded Terms Limit','Exceeded Terms Limit',8,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Line of Credit')  BEGIN   INSERT INTO OROrderStatus values(9,'Exceeded Line of Credit','Exceeded Line of Credit',9,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Manual Hold')  BEGIN   INSERT INTO OROrderStatus values(10,'Manual Hold','Manual Hold',10,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Partially Shipped')  BEGIN   INSERT INTO OROrderStatus values(11,'Partially Shipped','Partially Shipped',11,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Created')  BEGIN   INSERT INTO OROrderStatus values(12,'Created','Created',12,0)  END

	-- OROrderNextStatus
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderNextStatus values(1,1,10)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderNextStatus values(2,1,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderNextStatus values(3,7,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderNextStatus values(4,7,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderNextStatus values(5,10,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderNextStatus values(6,10,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderNextStatus values(7,5,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =8)  BEGIN   INSERT INTO OROrderNextStatus values(8,5,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =9)  BEGIN   INSERT INTO OROrderNextStatus values(9,6,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =10)  BEGIN   INSERT INTO OROrderNextStatus values(10,6,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =11)  BEGIN   INSERT INTO OROrderNextStatus values(11,12,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =12)  BEGIN   INSERT INTO OROrderNextStatus values(12,11,2)  END

	-- OROrderType
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =1)  BEGIN   INSERT INTO OROrderType values(1,'Web','Web Order',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =2)  BEGIN   INSERT INTO OROrderType values(2,'CSR','Web Service Order',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =3)  BEGIN   INSERT INTO OROrderType values(3,'Brochure Request','Brochure Request',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =4)  BEGIN   INSERT INTO OROrderType values(4,'Recurring Order','Recurring ORder',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =5)  BEGIN   INSERT INTO OROrderType values(5,'Exchange Order','Exchange Order',5)  END

	-- OROrderItemStatus
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderItemStatus values(1,'Ok','Submitted',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderItemStatus values(2,'Shipped','Shipped',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderItemStatus values(3,'Cancelled','Cancelled',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderItemStatus values(4,'Backordered','Backordered',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderItemStatus values(5,'Pending','Pending',5)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderItemStatus values(6,'Released','Released',6)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderItemStatus values(7,'Partially Shipped','Partially Shipped',7)  END

	-- PTCreditCardType
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE Id ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487','American Express','American Express',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='28091FD7-00C2-4E71-A266-7FA718658A66')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('28091FD7-00C2-4E71-A266-7FA718658A66','MasterCard','MasterCard',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='A98F6177-90A5-420D-96B5-A234778F8D96')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('A98F6177-90A5-420D-96B5-A234778F8D96','Visa','Visa',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E','Discover','Discover',@CreatedById, @Now, @CreatedById, @Now)  
	END

	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE CreditCardTypeId ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487' And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='28091FD7-00C2-4E71-A266-7FA718658A66'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('28091FD7-00C2-4E71-A266-7FA718658A66',@ApplicationId,1) 
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='A98F6177-90A5-420D-96B5-A234778F8D96'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('A98F6177-90A5-420D-96B5-A234778F8D96',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E',@ApplicationId,1)  
	END

	-- PTGateway
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =1)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(1,'Website Payments Pro','Website Payments Pro',2,'WebsitePaymentsProGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =2)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(2,'None','None',1,'AutoGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =3)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(3,'Cybersource','Cybersource',3,'CybersourceGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =4)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description],  [Sequence], [DotNetProviderName]) 
		Values(4,'Payflow Pro','Payflow Pro',4,'PayflowProGatewayProvider') 
	END

	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =1)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(1,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =2)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(2,0,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =3)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(3,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =4)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(4,0,1,@ApplicationId) 
	END
	
	-- PTPaymentStatus
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =1)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(1,'Entered','Entered',1,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =2)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(2,'Auth','Authorized',2,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =3)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(3,'Captured','Captured',3,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =4)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(4,'Auth Failed','Authorization Failed',4,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =5)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(5,'Capture Failed','Capture Failed',5,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =6)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(6,'Clear','Clear',6,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =7)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(7,'Manual Cancel','Manual Cancel',7,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =8)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(8,'Partially Captured','Partially Captured',8,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =9)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(9,'Void','Void',9,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =10)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(10,'Refund Entered','Refund Entered',10,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =11)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(11,'Refund Waiting Approval','Refund Waiting Approval',11,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =12)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(12,'Refunded','Refunded',12,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =13)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(13,'Refund Failed','Refund Failed',13,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =14)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(14,'Refund Rejected','Refund Rejected',14,1)  END

	-- PTPaymentType
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =1)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(1,'CreditCard','Credit Card',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =2)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(2,'OnAccount','OnAccount',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =3)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(3,'Paypal','Paypal',5)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =4)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(4,'COD','Cash On Delivery',4)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =5)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(5,'GiftCard','Gift Card',2)  END

	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =1)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(1,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =2)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(2,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =3)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(3,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =4)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(4,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =5)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(5,@ApplicationId,1)  END

	IF (SELECT Count(*) FROM ORReturnReason)=0
	BEGIN

		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES( 
			'Defective')
			
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(  
			'Customer Ordered Wrong Product')
		    
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES('Refused Delivery')


		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Missing Merchandise')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Did not Like')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Changed Mind')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Billing Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Fraudulent Order')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Warehouse Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Returned by Shipper - No Response From Customer')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Other')

	END

	-- INWarehouseType
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =1)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(1,'Internal','iAPPS internal warehouse for Non-Digital Products')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =2)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(2,'Digital','iAPPS Digital warehouse')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =3)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(3,'External','Integrated with external system')  END

	-- INRestockingStatus
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Created'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (1,'Created','Created',1)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Queued'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (2,'Queued','Queued',2)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='In Progress'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (3,'In Progress','In Progress',3)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Canceled'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (4,'Canceled','Canceled',4)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Completed'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (5,'Completed','Completed',5)

	-- ORReturnResolutionType
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] ON
	Select @maxId =Isnull(MAX(Id),0) FROM ORReturnResolutionType
	SET @maxId = @maxId + 1

	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Refund'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Refund','Part or full paymnent is returned', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Replacement'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Replacement','Replacement', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Others'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Others','Others', @maxId)        
		SET @maxId = @maxId + 1
	END
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] OFF

	-- INInventory
	IF(NOT EXISTS(SELECT 1 FROM dbo.INInventory WHERE WarehouseId ='22FAC4BD-4954-415A-9014-3598DAEAB4D0'))  
	BEGIN   
		Declare @Id UniqueIdentifier
		Set @Id= '22FAC4BD-4954-415A-9014-3598DAEAB4D0'

		INSERT INTO [INInventory]
				   ([Id]
				   ,[WarehouseId]
				   ,[SKUId]
				   ,[Quantity]
				   ,[LowQuantityAlert]
				   ,[FirstTimeOrderMinimum]
				   ,[OrderMinimum]
				   ,[OrderIncrement]
				   ,[ReasonId]
				   ,[ReasonDetail]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
					SELECT newId()
					  ,@Id
					  ,Id
					  ,0
					  ,0
					  ,1
					  ,1
					  ,1
					  ,(SELECT Id FROM GLReason Where Title ='Other') As ReasonId
					  ,'Initial Data When SKU is created'
					  ,1
					  ,@CreatedById
					  ,@Now
					  ,@CreatedById
					  ,@Now
				   FROM PRProductSKU WHERE Id NOT IN 
					(SELECT SKUID FROM ININventory WHERE WarehouseId = @Id)
	END
	

	DECLARE @GroupObjectTypeId int
	DECLARE @MerchandisingRoleId int, @FulfillmentRoleId int, @MarketingRoleId int, @CSRRoleId int, 
		@EveryOneRoleId int, @CSRManagerRoleId int, @WHManagerRoleId int
	DECLARE @MerchandisingGroupId uniqueidentifier, @FulfillmentGroupId uniqueidentifier, 
		@MarketingGroupId uniqueidentifier, @CSRGroupId uniqueidentifier, @EveryOneGroupId uniqueidentifier, 
		@CSRManagerGroupId uniqueidentifier, @WHManagerGroupId uniqueidentifier

	SET @GroupObjectTypeId = 15 
	SET @MerchandisingRoleId = 19
	SET @FulfillmentRoleId = 20
	SET @MarketingRoleId = 21
	SET @CSRRoleId = 22
	SET @EveryOneRoleId = 29
	SET @CSRManagerRoleId = 32
	SET @WHManagerRoleId = 33	
	-- if the db has cms and default commerce data was missing 
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MerchandisingGroupId output,@Title = 'Merchandising',
			@Description = 'Merchandising',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
				
	END		
	ELSE
		SET @MerchandisingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')

	IF (@MerchandisingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MerchandisingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MerchandisingGroupId,2,@MerchandisingRoleId, @MerchandisingGroupId,1,1 )				
	END
	
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Fulfillment')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @FulfillmentGroupId output,@Title = 'Fulfillment',
			@Description = 'Fulfillment',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @FulfillmentGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Fulfillment')

	IF (@FulfillmentGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @FulfillmentGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@FulfillmentGroupId,2,@FulfillmentRoleId, @FulfillmentGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Marketing')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MarketingGroupId output,@Title = 'Marketing',
			@Description = 'Marketing',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @MarketingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Marketing')

	IF (@MarketingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MarketingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MarketingGroupId,2,@MarketingRoleId, @MarketingGroupId,1,1 )				
	END

	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSR')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRGroupId output,@Title = 'CSR',
			@Description = 'CSR',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSR')
	
	IF (@CSRGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRGroupId,2,@CSRRoleId, @CSRGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'EveryOne' And GroupType = 1)) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @EveryOneGroupId output,@Title = 'EveryOne',
			@Description = 'EveryOne',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @EveryOneGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'EveryOne')

	IF (@EveryOneGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @EveryOneGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@EveryOneGroupId,2,@EveryOneRoleId, @EveryOneGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSRManager')) 
	BEGIN	
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRManagerGroupId output,@Title = 'CSRManager',
			@Description = 'CSRManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @CSRManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSRManager')

	IF (@CSRManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRManagerGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRManagerGroupId,2,@CSRManagerRoleId, @CSRManagerGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'WHManager')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @WHManagerGroupId output,@Title = 'WHManager',
			@Description = 'WHManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
	END		
	ELSE
		SET @WHManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'WHManager')

	IF (@WHManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @WHManagerGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@WHManagerGroupId,2,@WHManagerRoleId, @WHManagerGroupId,1,1 )				
	END
			
END
GO
ALTER PROCEDURE [dbo].[Product_GetProductIdByUrl](@url nvarchar(4000), @productIDUser nvarchar(50) = null)  
AS  
BEGIN



if @productIDUser is null
	begin	
	
			set @url=lower(@url)	
			
		
			declare @revUrl nvarchar(max)
			declare @lastSlashPosition int
			declare @afterLastSlash nvarchar(100)
			Declare @ProdIdUser nvarchar(50)
			Declare @secobdLastSlashProsition int

			DECLARE @IdPosition int
			set @revUrl = Reverse(@url) + '/'
			set @lastSlashPosition =CHARINDEX ( '/' , @revUrl,0) 
			SET @IdPosition = PATINDEX('%-di', @revUrl)
			PRINT @IdPosition
			IF(@IdPosition < @lastSlashPosition AND @lastSlashPosition > 1 AND @IdPosition > 0)
			BEGIN 
			SET @productIDUser = SUBSTRING(@revUrl,0,@IdPosition)
			SET @productIDUser = REVERSE(@productIDUser)
			END
			ELSE if @lastSlashPosition  > 0
				begin
					set @secobdLastSlashProsition = CHARINDEX('/',@revUrl,@lastSlashPosition+1)
					if @secobdLastSlashProsition > @lastSlashPosition
					BEGIN
					
						Set @productIDUser = Substring(@revUrl,@lastSlashPosition+1,@secobdLastSlashProsition-@lastSlashPosition-1)
						
						set @productIDUser = Reverse(@productIDUser)
			
					
						if PATINDEX('id-%',@productIDUser) > 0 
									and len(@productIDUser) > 4 
							BEGIN
						
								set @productIDUser = Substring(@productIDUser,4,len(@productIDUser)-2)
							
							end
						-- ATL EDIT START --
						else
							begin
								set @productIDUser = NULL
								
							end
						-- ATL EDIT END --
					end
				end
	
	end
	if @productIDUser is null OR NOT EXISTS(Select 1 from PRProduct Where ProductIDUser=@productIDUser)
	begin
		;with cteUrl as
		(
		SELECT prseo.ProductId as Id,[prseo].[FriendlyUrl] As FriendlyUrl 
		FROM PRProductSEO prseo 
		)
		Select Id,lower(FriendlyUrl ) FriendlyUrl
		from (Select Distinct ProductId From PRProductSKU Where IsOnline=1 ) PS inner join 
		cteUrl cte On cte.Id = PS.ProductId
		WHERE [FriendlyUrl]=@url   OR [FriendlyUrl]= '/' + @url 
		--IF @@ROWCOUNT >0
		--	RETURN
	end
	else
	begin


		;with cteUrl as
		(
		SELECT   
		P.Id as Id,lower(dbo.GetProductTypePath(PT.Id)  + N'id-' + P.ProductIDUser + '/' + UrlFriendlyTitle)As VirtualPath ,
		lower([prseo].[FriendlyUrl]) As FriendlyUrl 
		FROM 
		(Select Distinct ProductId From PRProductSKU Where IsOnline=1 ) PS
			INNER JOIN PRProduct P ON P.Id = PS.ProductId
			Inner Join PRProductType PT on P.ProductTypeID=PT.Id
			 Left Join PRProductSEO prseo ON P.[Id]=[prseo].[ProductId]  
			WHERE
			P.ProductIDUser = @productIDUser
		 )
		Select Id,VirtualPath,FriendlyUrl 
		from cteUrl
		WHERE VirtualPath = @url OR [FriendlyUrl]=@url

	END
end
GO
PRINT N'Altering [dbo].[Snippet_Get]...';


GO
ALTER PROCEDURE [dbo].[Snippet_Get]
	@SnippetId	uniqueidentifier,
	@SiteId		uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

    SELECT	Name, [Content]
    FROM	COSnippet
    WHERE	SnippetId = @SnippetId 
		-- AND SiteId = @SiteId
END
GO
PRINT N'Altering [dbo].[Snippet_GetAll]...';


GO
ALTER PROCEDURE [dbo].[Snippet_GetAll]
	@SiteId	uniqueidentifier
AS
BEGIN
	SET NOCOUNT ON;

    SELECT		SnippetId, Name, [Content]
    FROM		COSnippet
    --WHERE		SiteId = @SiteId
    ORDER BY	Name ASC
END
GO
ALTER PROCEDURE [dbo].[AssetFile_GetAssetFiles] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id uniqueidentifier = null,
	@ApplicationId uniqueidentifier = null,
	@SourceContentId uniqueidentifier=NUll,
	@ObjectTypeId int =null,
	@Status int = null,
	@Extension nvarchar(10)= null,	
	@PageSize int= null,
	@PageIndex int = null,
	@MimeType nvarchar(256) = null,
	@FileName nvarchar(256) =null,
	@Url nvarchar(1200) =null
)
as
Begin
	If (@PageSize is null and @PageIndex is null) 
	Begin
	if @Status  IS NULL
		set @Status= dbo.GetActiveStatus()
		
		select  C.Id,
				C.ApplicationId,
				C.Title,
				C.Description, 
				C.Text, 
				C.URL,
				C.URLType, 
				C.ObjectTypeId, 
				C.CreatedDate,
				C.CreatedBy,
				C.ModifiedBy,
				C.ModifiedDate, 
				C.Status,
				C.RestoreDate, 
				C.BackupDate, 
				C.StatusChangedDate,
				C.PublishDate, 
				C.ArchiveDate, 
				C.XMLString,
				C.BinaryObject, 
				C.Keywords,
				C.SourceContentId,
				F.FileName, 
				F.FileSize, 
				F.Extension, 
				F.FolderName, 
				F.Attributes, 
				F.RelativePath, 
				F.PhysicalPath,
				F.MimeType,
				F.AltText,
				F.FileIcon, 
				F.DescriptiveMetadata, 
				F.ImageHeight, F.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				C.OrderNo,
				C.ParentId							
		from COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		INNER JOIN vwAssetFileUrls V ON V.Id = C.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		where (@Id is NUll Or C.Id = @Id)
			  and
			  (@ApplicationId Is NUll Or C.ApplicationId = @ApplicationId)
			  and
			  (@Extension is null Or @Extension = F.Extension)
			  and
			 (@MimeType is null Or F.MimeType = @MimeType)
			  and
			  (@FileName is Null or F.FileName =@FileName )
			  and
			  (@ObjectTypeId IS NULL Or C.ObjectTypeId = @ObjectTypeId)
			  and
			  C.Status = @Status
			  And 
			  (@SourceContentId IS NULL Or C.SourceContentId = @SourceContentId)
			  And
			  (@Url IS NULL OR V.Url like @Url)
	end
	else		
	Begin
		select  Id,
				ApplicationId,
				Title,
				Description, 
				Text, 
				URL,
				URLType, 
				ObjectTypeId, 
				CreatedDate,
				CreatedBy,
				ModifiedBy,
				ModifiedDate, 
				Status,
				RestoreDate, 
				BackupDate, 
				StatusChangedDate,
				SourceContentId,
				PublishDate, 
				ArchiveDate, 
				XMLString,
				BinaryObject, 
				Keywords,
				FileName, 
				FileSize, 
				Extension, 
				FolderName, 
				Attributes, 
				RelativePath, 
				PhysicalPath,
				MimeType,
				AltText,
				FileIcon, 
				DescriptiveMetadata, 
				ImageHeight,
				ImageWidth,
				CreatedByFullName,
				ModifiedByFullName,
				OrderNo,
				ParentId									
		From (select ROW_NUMBER() over (order by CreatedDate) as RowId,
					C.Id,
					C.ApplicationId,
					C.Title,
					C.Description, 
					C.Text, 
					C.URL,
					C.URLType, 
					C.ObjectTypeId, 
					C.CreatedDate,
					C.CreatedBy,
					C.ModifiedBy,
					C.ModifiedDate, 
					C.Status,
					C.RestoreDate, 
					C.BackupDate, 
					C.StatusChangedDate,
					C.SourceContentId,
					C.PublishDate, 
					C.ArchiveDate, 
					C.XMLString,
					C.BinaryObject, 
					C.Keywords,
					F.FileName, 
					F.FileSize, 
					F.Extension, 
					F.FolderName, 
					F.Attributes, 
					F.RelativePath, 
					F.PhysicalPath,
					F.MimeType,
					F.AltText,
					F.FileIcon, 
					F.DescriptiveMetadata, 
					F.ImageHeight,
					F.ImageWidth,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName,
					C.OrderNo,
				C.ParentId										
			from COContent C 
			INNER JOIN COFile F ON C.Id = F.ContentId
			INNER JOIN vwAssetFileUrls V ON V.Id = C.Id
			LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
			where (@Id is NUll Or C.Id = @Id)
			  and
			  (@ApplicationId Is NUll Or C.ApplicationId = @ApplicationId)
			  and
			  (@Extension is null Or @Extension = F.Extension)
			  and
			 (@MimeType is null Or F.MimeType = @MimeType)
			  and
			  (@FileName is Null or F.FileName =@FileName )
			  and
			  (@ObjectTypeId IS NULL Or C.ObjectTypeId = @ObjectTypeId)
			  and
			  C.Status = @Status
			  And 
			  (@SourceContentId IS NULL Or C.SourceContentId = @SourceContentId)
			  And
			  (@Url IS NULL OR V.Url = @Url))
			as FileWithRowNumbers
		where RowId between ((@PageIndex -1) * @PageSize +1) and (@PageIndex * @PageSize)
		
	end
end
GO