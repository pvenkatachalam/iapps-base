﻿go
Update SISite SET MasterSiteId = case when ParentSiteId ='00000000-0000-0000-0000-000000000000' then Id else ParentSiteId end
where MasterSiteId is null
go

-- Clean up jquery settings names
UPDATE STSettingType SET NAME = 'JQuery.Include' WHERE Name like 'IncludeJQuery'  
UPDATE STSettingType SET NAME = 'JQuery.UseCDN' WHERE Name like 'GoogleCDN'  
UPDATE STSettingType SET NAME = 'JQuery.CDNPath' WHERE Name like 'GoogleCDNFile'  
UPDATE STSettingType SET NAME = 'JQuery.CDNAdminPath' WHERE Name like 'jQueryGoogleCDN'  
UPDATE STSettingType SET NAME = 'JQueryUI.CDNPath' WHERE Name like 'jQueryUICDNURL'
UPDATE STSettingType SET NAME = 'JQueryUI.CDNAdminPath' WHERE Name like 'FormsLibrary.JQueryUIUrl'

GO
DECLARE @SiteId UNIQUEIDENTIFIER,@SettingTypeId INT,@Sequence INT

DECLARE SITE_CURSOR_ Cursor 
FOR
SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId -- Only Master sites

Open SITE_CURSOR_ 
Fetch NEXT FROM SITE_CURSOR_ INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Blogs.DefaultContentDefinitionId')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Blogs.DefaultContentDefinitionId', 'Blogs DefaultContentDefinitionId',1, 1, @Sequence)
		END
    
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
				  @SettingTypeId, -- SettingTypeId - int
				  N'68D6F3BA-D3B8-41DD-90F4-0176140B0DD6'  -- Value - nvarchar(max)
				  )	
    		
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'JQuery.UseCDN')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('JQuery.UseCDN', 'JQuery UserCDN',1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'true'  -- Value - nvarchar(max)
					)		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'JQuery.CDNAdminPath')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('JQuery.CDNAdminPath', 'JQuery CDNAdminPath',1, 1, @Sequence)	
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'  -- Value - nvarchar(max)
					)	
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'JQueryUI.CDNAdminPath')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('JQueryUI.CDNAdminPath', 'JQueryUI CDNAdminPath',1, 1, @Sequence)		
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.js'  -- Value - nvarchar(max)
					)	
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'IndexName')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('IndexName', 'IndexName',1, 1, @Sequence)	
		END
     
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N''  -- Value - nvarchar(max)
					)			
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'ISYSVersion')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('ISYSVersion', 'ISYSVersion',1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'ISYS Version 9.7'  -- Value - nvarchar(max)
					)		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'SocialAdminVDName')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('SocialAdminVDName', 'SocialAdminVDName',1, 1, @Sequence)		
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'SocialAdmin'  -- Value - nvarchar(max)
					)	
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'SocialProduct.Key')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('SocialProduct.Key', 'Social Product Key',1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'Social'  -- Value - nvarchar(max)
					)			
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'SocialProductId')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('SocialProductId', 'Social Product Id',1, 1, @Sequence)	
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO dbo.STSiteSetting ( SiteId, SettingTypeId, Value ) 
		VALUES  ( @SiteId, -- SiteId - uniqueidentifier
					@SettingTypeId, -- SettingTypeId - int
					N'CED1F2B6-A3FF-4B82-A856-1583FA811906'  -- Value - nvarchar(max)
					)			
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'JQueryUI.Include')
	IF @SettingTypeId IS NULL 
	BEGIN
		SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
		VALUES('JQueryUI.Include', 1, 1, @Sequence)		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'JQueryUI.CDNPath')
	IF @SettingTypeId IS NULL 
	BEGIN
		SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
		VALUES('JQueryUI.CDNPath', 1, 1, @Sequence)		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'JQueryUI.UseCDN')
	IF @SettingTypeId IS NULL 
	BEGIN
		SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
		VALUES('JQueryUI.UseCDN', 1, 1, @Sequence)		
	END

	--- GWS Settings
	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'GWSPickTicketProfileID')
	IF @SettingTypeId IS NULL 
	BEGIN
		SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
		VALUES('GWSPickTicketProfileID', 1, 1, @Sequence)		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'GWSPackingSlipType')
	IF @SettingTypeId IS NULL 
	BEGIN
		SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
		VALUES('GWSPackingSlipType', 1, 1, @Sequence)		
	END
	
	SET @SettingTypeId = (SELECT Id FROM STSettingType WHere Name = 'GWSOrderType')
	IF @SettingTypeId IS NULL 
	BEGIN
		SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
		VALUES('GWSOrderType', 1, 1, @Sequence)		
	END	


	SET @SettingTypeId = (SELECT Id FROM STSettingType WHere Name = 'JQueryUI.CDNAdminPath')
	IF NOT EXISTS(SELECT * FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
		INSERT INTO dbo.STSiteSetting (SiteId, SettingTypeId, Value)
		VALUES  (@SiteId, @SettingTypeId, 'ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.js')
			  			  

	SET @SettingTypeId = (SELECT Id FROM STSettingType WHere Name = 'Editor.CustomCKEditorConfigFile')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
			Values('Editor.CustomCKEditorConfigFile','Editor.CustomCKEditorConfigFile',1,1,@Sequence)
		END
	
		SET @SettingTypeId =ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')
	END

	SET @SettingTypeId = (SELECT Id FROM STSettingType WHere Name = 'Editor.CustomToolsFile')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
			Values('Editor.CustomToolsFile','Editor.CustomToolsFile',1,1,@Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')
	END



	--SOCIAL TOKENS
	-- Social Tokens
	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.APIURL')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
	IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.APIURL', 1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.TwitterAppKey')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.TwitterAppKey', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.TwitterAppSecret')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.TwitterAppSecret', 1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.LinkedInAppKey')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.LinkedInAppKey', 1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.LinkedInAppSecret')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId)
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.LinkedInAppSecret', 1, 1, @Sequence)
		END

		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.FacebookAppKey')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
	IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.FacebookAppKey', 1, 1, @Sequence)
		END
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'Social.FacebookAppSecret')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('Social.FacebookAppSecret', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'')		
	END	

	--For cybersource entries
	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'cybs.secretKey')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
			Values('cybs.secretKey','cybs.secretKey',1,1,@Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'a8a93117315e4b76a2e0b2f9590756fdb336129f086b4558a8f23aab15bf486f4fca140ed45c4ad4a165de9f2272f779ac400e3079034dbca1425556f851d7fa4f85ca3adf974c039604f804fb746db4183b7110f12f491194de4b98c1c133934422cccaada74124b9f24bda8aa8dd46210272b2c80e442d8eb58863b0c55afb')		
	END	

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'cybs.accessKey')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
			Values('cybs.accessKey','cybs.accessKey',1,1,@Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'52c4058b72e1373e94a55b7302097758')		
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'cybs.profileId')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
			Values('cybs.profileId','cybs.profileId',1,1,@Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'iappsOP')		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'AllowSiblingSiteAccess')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('AllowSiblingSiteAccess', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,1)		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'AuthorizeNetAPILogin')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('AuthorizeNetAPILogin', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'5T6p6FgZY')		
	END

	
	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'AuthorizeNetTransactionKey')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('AuthorizeNetTransactionKey', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'6k3C3Ngg67c8V6N3')		
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'MinimumOrderAmount')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('MinimumOrderAmount', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'0')		
	END

	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'MaximumOrderAmount')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('MaximumOrderAmount', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'1.79769313486232E+308')		
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'MinimumOrderAmountAction')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('MinimumOrderAmountAction', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'4')		
	END


	SET @SettingTypeId = (SELECT TOP 1 Id FROM STSettingType WHERE name = 'MaximumOrderAmountAction')
	IF NOT EXISTS (SELECT 1 FROM STSiteSetting Where SettingTypeId = @SettingTypeId And SiteId = @SiteId) 
	BEGIN
		IF @SettingTypeId IS NULL
		BEGIN
			SELECT @Sequence = MAX(sequence) FROM dbo.STSettingType
			INSERT INTO STSettingType (Name, SettingGroupId, IsSiteOnly,Sequence)
			VALUES('MaximumOrderAmountAction', 1, 1, @Sequence)
		END
		
		SET @SettingTypeId = ISNULL(@SettingTypeId,@@Identity)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@SettingTypeId,'4')		
	END

	Fetch NEXT FROM SITE_CURSOR_ INTO @SiteId 		
	END

CLOSE SITE_CURSOR_
DEALLOCATE SITE_CURSOR_


GO


-- Update admin jQuery to 10.2
UPDATE STSiteSetting SET Value = 'ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js' WHERE SettingTypeId = (
SELECT TOP 1 Id FROM STSettingType WHERE Name like 'JQuery.CDNAdminPath')

UPDATE STSiteSetting SET Value = 'ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.js' WHERE SettingTypeId = (
SELECT TOP 1 Id FROM STSettingType WHERE Name like 'JQueryUI.CDNAdminPath')


