﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


--Update product version on iAPPSProductSuite table
UPDATE iAppsProductSuite SET ProductVersion='5.1.0.0'
GO

--update the primarysite url column with the primary site url
UPDATE SISite set PrimarySiteURL = URL.value('(URLCollection/URL)[1]','nvarchar(100)' ) where PrimarySiteURL is null OR LTRIM(RTRIM(PrimarySiteUrl)) ='' 

GO

Declare @siteSettingTypeId int
Declare @seq int
IF NOT EXISTS (Select * from STSettingType Where name like 'Translation.FromEmail')
BEGIN
SELECT @seq = MAX(sequence) FROM dbo.STSettingType
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
	Values('Translation.FromEmail','Translation.FromEmail',1,1,@seq)
	SET @siteSettingTypeId =@@Identity
	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	Values('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@siteSettingTypeId,'admin@blinedigital.com')
END

IF NOT EXISTS (Select * from STSettingType Where name like 'Translation.ToEmail')
BEGIN
SELECT @seq = MAX(sequence) FROM dbo.STSettingType
	INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
	Values('Translation.ToEmail','Translation.ToEmail',1,1,@seq)
	SET @siteSettingTypeId =@@Identity
	INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
	Values('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@siteSettingTypeId,'vsinha@blinedigital.com')
END
GO
declare @sql varchar(max)
DECLARE @TypeId int, @SiteId uniqueidentifier

DECLARE MY_CURSOR Cursor 
FOR
SELECT Id FROM SISite Where Status = 1

Open My_Cursor 
Fetch NEXT FROM MY_Cursor INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN

SET @TypeId = (SELECT Id FROM STSettingType WHere Name = 'Translation.FromEmail')
	IF NOT EXISTS(SELECT * FROM STSiteSetting Where SettingTypeId = @TypeId And SiteId = @SiteId)
				INSERT INTO dbo.STSiteSetting
			( SiteId, SettingTypeId, Value )
			VALUES  ( @SiteId,  
			  @TypeId, 
			   'admin@blinedigital.com'
			  )

SET @TypeId = (SELECT Id FROM STSettingType WHere Name = 'Translation.ToEmail')
	IF NOT EXISTS(SELECT * FROM STSiteSetting Where SettingTypeId = @TypeId And SiteId = @SiteId)
				INSERT INTO dbo.STSiteSetting
			( SiteId, SettingTypeId, Value )
			VALUES  ( @SiteId,  
			  @TypeId, 
			   'vsinha@blinedigital.com'
			  )
Fetch NEXT FROM MY_Cursor INTO @SiteId 		
END

CLOSE MY_CURSOR
DEALLOCATE MY_CURSOR
GO

--Update all pyments less than 0 dollars as refunds
UPDATE  dbo.PTPayment SET     IsRefund = 1
WHERE   Amount < 0

DELETE FROM STSiteSetting Where SettingTypeId IN (Select Id FROM STSettingType Where Name = 'WFScreenshotUrl')

DELETE FROM STSettingType Where Name = 'WFScreenshotUrl'

UPDATE STSiteSetting Set Value = '\CommonLogin\Executables\WebScreenshot\WebScreenshot.exe' Where SettingTypeId IN (Select Id FROM STSettingType Where Name = 'WFScreenshotUtilityLocation')

IF NOT EXISTS (SELECT 1 FROM [dbo].[ATAttribute] WHERE Id ='4EEF2AE9-6B5E-4819-9481-DBDBBAE03D38')
INSERT INTO [dbo].[ATAttribute]
           ([Id]
           ,[AttributeDataType]
           ,[AttributeDataTypeId]
           ,[AttributeGroupId]
           ,[Title]
           ,[Description]
           ,[Sequence]
           ,[Code]
           ,[IsFaceted]
           ,[IsSearched]
           ,[IsSystem]
           ,[IsDisplayed]
           ,[IsEnum]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
           ,[IsPersonalized])
     VALUES
           ('4EEF2AE9-6B5E-4819-9481-DBDBBAE03D38'
           	,'System.String'
           	,'5C41E8EB-AA6E-4D5F-A299-DC16695B92E8'	
           	,'AB730D27-5CCD-49D9-B515-168290438E0A'	
           	,'SKU Filter Attributes'
           	,'These Attributes are used in Multi-SKU Selection control'
           	,1
           	,NULL
           	,0
           	,0
           	,1
           	,0
           	,0
           	,'2013-08-06 16:15:09.297'
           	,'798613EE-7B85-4628-A6CC-E17EE80C09C5'	
           	,NULL
           	,NULL
           	,1
           	,0
           	)

--Remove refund coupons
DELETE  OCC
FROM    dbo.CPCoupon c
        INNER JOIN dbo.CPCouponCode CC ON CC.CouponId = C.ID
        INNER JOIN dbo.CPOrderCouponCode OCC ON OCC.CouponCodeId = CC.Id
WHERE   CouponTypeId = 'FC3BDCB0-C1D3-4A2C-815F-12E81E54B72F'


UPDATE  O
SET     O.TotalDiscount = O.TotalDiscount + RefundAmount ,
        O.REfundTotal = RefundAmount
FROM    ( SELECT    O.PurchaseOrderNumber ,
                    O.Id ,
                    O.TotalDiscount ,
                    OS.TotalDiscount shippingdiscount ,
                    SUM(OP.Amount) AS RefundAmount
          FROM      dbo.OROrder O
                    INNER JOIN dbo.OROrderShipping OS ON OS.OrderId = O.ID
                    INNER JOIN dbo.PTOrderPayment OP ON OP.OrderId = O.Id
                    INNER JOIN dbo.PTPayment P ON P.Id = OP.PaymentId
          WHERE     OP.Amount < 0
                    AND P.PaymentStatusId IN ( 12 )
          GROUP BY  O.PurchaseOrderNumber ,
                    O.TotalDiscount ,
                    OS.TotalDiscount ,
                    O.Id
        ) TV
        INNER JOIN OROrder O ON O.Id = TV.Id

UPDATE PTPayment 
SET IsRefund = 1 where PaymentStatusId IN (10,11,12,13,14)                    

UPDATE  dbo.OROrder
SET     TotalDiscount = 0
WHERE   TotalDiscount < 0

UPDATE PC
SET PC.ContainerName = CN.Title
FROM PageDefinitionContainer PC
JOIN SIContainer CN ON PC.ContainerId = CN.Id

GO
--Update SYRssChannel Set ChannelType = 1
IF NOT EXISTS(Select 1 from OROrderType Where Id=1)
INSERT INTO OROrderType
Values(1,'Web','Web Order',1)

IF NOT EXISTS(Select 1 from OROrderType Where Id=2)
INSERT INTO OROrderType
Values(2,'CSR','CSR',2)

IF NOT EXISTS(Select 1 from OROrderType Where Id=3)
INSERT INTO OROrderType
Values(3,'Brochure Request','Brochure Request',3)

IF NOT EXISTS(Select 1 from OROrderType Where Id=4)
INSERT INTO OROrderType
Values(4,'Recurring Order','Recurring Order',4)

IF NOT EXISTS(Select 1 from OROrderType Where Id=5)
INSERT INTO OROrderType
Values(5,'Exchange Order','Exchange Order',5)

IF NOT EXISTS( Select Name from GLAction where Name = 'ReOrder')
BEGIN
	INSERT INTO GLAction VALUES (21,'ReOrder','')
END

GO
-- Script to remove Credit card number from PaymentLog
Declare @Id uniqueidentifier
Declare @log xml
Declare @logData nvarchar(max)
Declare @startIndex int,@endIndex int
DECLARE payment_cursor CURSOR FOR 
select Id,Log from PTPaymentLog Where cast(Log as nvarchar(max)) like '%cybersource-com%'

OPEN payment_cursor

FETCH NEXT FROM payment_cursor 
INTO @Id,@log

WHILE @@FETCH_STATUS = 0
BEGIN

select @logData= @log.value('(PaymentLogItem/XMLRequestMessage/text())[1]','nvarchar(max)')
SET @startIndex = CHARINDEX('accountNumber',@logData);
SET @endIndex =CHARINDEX('/cvNumber',@logData,@startIndex)
	IF @startIndex >0 and @endIndex >0
	BEGIN
		select @logData = LEFT(@logData,@startIndex-2)+ RIGHT(@logData, len(@logData)-@endIndex-9)
		select @logData
		SET     @log.modify('
				replace value of (PaymentLogItem/XMLRequestMessage/text())[1]
				with sql:variable("@logData")')
		Update PTPaymentLog SET Log =@log
		Where Id=@Id	
	END	
 FETCH NEXT FROM payment_cursor 
    INTO @Id,@log
END 
CLOSE payment_cursor;
DEALLOCATE payment_cursor;	
GO
IF (COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludePageTranslationProperties') IS NOT NULL AND COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludeDescription') IS NOT NULL)
BEGIN

Declare @sql as nvarchar(max)
Set @sql = 'UPDATE TLAssetInfo 
	 SET IncludePageTranslationProperties = 
	 (
		CASE 
			WHEN IncludeDescription = 0
			THEN 
				0
			ELSE
				7
		END
	 )
--DROP Include Description column
ALTER TABLE TLAssetInfo Drop column IncludeDescription'

exec (@sql)
	
END

GO
IF (COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludeMenuTranslationProperties') IS NOT NULL AND COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludeMenu') IS NOT NULL )
BEGIN
Declare @sql as nvarchar(max)
Set @sql = 'UPDATE TLAssetInfo 
	 SET IncludeMenuTranslationProperties = 
	 (
		CASE 
			WHEN IncludeMenu = 0
			THEN 
				0
			ELSE
				7
		END
	 )
	 --DROP Include Menu column
	Alter Table TLAssetInfo Drop column IncludeMenu'

	exec (@sql)
END
GO
IF NOT EXISTS (SELECT 1 FROM STSettingType WHERE Name = 'EnableAuthenticationForSecureObjects')
BEGIN
Declare @seq int
SELECT @seq = MAX(sequence) FROM dbo.STSettingType
SET @seq = @seq + 1
Declare @siteSettingTypeId int

	INSERT INTO STSettingType (Name, FriendlyName, SettingGroupId, IsSiteOnly, Sequence)
	VALUES ('EnableAuthenticationForSecureObjects', 'Enable Authentication For Secure Objects', 1, 1, @seq)

	SET @siteSettingTypeId =@@Identity

	INSERT INTO STSiteSetting (SiteId, SettingTypeId, Value) 
	VALUES ('8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', @siteSettingTypeId, 'false')
END
GO
Declare @CustomPermissionGrpId uniqueidentifier
Set @CustomPermissionGrpId = (Select top 1 Id from USGroup where 
ProductId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848' And ApplicationId = 'CBB702AC-C8F1-4C35-B2DC-839C26E39848'
AND Title = 'Custom Permission') 

Update STSiteSetting set value= @CustomPermissionGrpId
where SettingTypeId in (select Id from STSettingType where Name = 'CustomPermissionGroupId')


IF (COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludePageTranslationProperties') IS NOT NULL AND COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludeDescription') IS NOT NULL)
BEGIN

Declare @sql as nvarchar(max)
Set @sql = 'UPDATE TLAssetInfo 
	 SET IncludePageTranslationProperties = 
	 (
		CASE 
			WHEN IncludeDescription = 0
			THEN 
				0
			ELSE
				7
		END
	 )
--DROP Include Description column
ALTER TABLE TLAssetInfo Drop column IncludeDescription'

exec (@sql)
	
END

GO
IF (COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludeMenuTranslationProperties') IS NOT NULL AND COL_LENGTH(N'[dbo].[TLAssetInfo]', N'IncludeMenu') IS NOT NULL )
BEGIN
Declare @sql as nvarchar(max)
Set @sql = 'UPDATE TLAssetInfo 
	 SET IncludeMenuTranslationProperties = 
	 (
		CASE 
			WHEN IncludeMenu = 0
			THEN 
				0
			ELSE
				7
		END
	 )
	 --DROP Include Menu column
	Alter Table TLAssetInfo Drop column IncludeMenu'

	exec (@sql)
END

--This is to trim the state code 
Update GLState SET StateCode = rtrim(StateCode)