/*
DECLARE @PageId uniqueidentifier
DECLARE PAGE_CURSOR Cursor 
FOR
SELECT PageDefinitionId FROM PageDefinition Where PageStatus = 8

Open PAGE_CURSOR 
Fetch NEXT FROM PAGE_CURSOR INTO @PageId

While (@@FETCH_STATUS <> -1)
BEGIN
	EXEC PageDefinition_UpdateAssetReference @PageId
			  			  
	Fetch NEXT FROM PAGE_CURSOR INTO @PageId 		
END

CLOSE PAGE_CURSOR
DEALLOCATE PAGE_CURSOR
*/