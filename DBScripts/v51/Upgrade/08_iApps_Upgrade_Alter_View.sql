/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:45:35 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[vw_PageContainerXmlDetails]'
GO
IF OBJECT_ID(N'[dbo].[vw_PageContainerXmlDetails]', 'V') IS NOT NULL
EXEC sp_executesql N'/*****************************************************
* Name : vw_PageContainerXmlDetails
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER VIEW [dbo].[vw_PageContainerXmlDetails]
AS 


SELECT PDF.PageDefinitionId, PDFC.ContainerId, PDFC.ContainerName,
CO.Id AS ContentId, PDFC.inWFContentId, CO.TemplateId, CO.ObjectTypeId AS ContentTypeId
FROM PageDefinitionContainer PDFC
INNER JOIN PageDefinition PDF ON PDFC.PageDefinitionId = PDF.PageDefinitionId
INNER JOIN COContent CO ON CO.Id = PDFC.ContentId




'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[VWDirectoryIds]'
GO
IF OBJECT_ID(N'[dbo].[VWDirectoryIds]', 'V') IS NOT NULL
EXEC sp_executesql N'/*****************************************************
* Name : VWDirectoryIds
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/




ALTER VIEW [dbo].[VWDirectoryIds] WITH SCHEMABINDING 
as
SELECT [Id]
      ,case when [ObjectTypeId] =0 then 6 ELSE ObjectTypeId END ObjectTypeId
  FROM [dbo].[SISiteDirectory]
  Where ObjectTypeId not in (7,9,33,38)
UNION ALL
Select [Id]
      ,7
  FROM [dbo].[COContentStructure]
  UNION ALL
Select [Id]
      ,9
  FROM [dbo].[COFileStructure]
UNION ALL
Select [Id]
      ,33
FROM [dbo].[COImageStructure]
UNION ALL  
Select [Id]    
      ,38  
FROM [dbo].[COFORMStructure]
  
  



'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[vwSearchOrder]'
GO
IF OBJECT_ID(N'[dbo].[vwSearchOrder]', 'V') IS NOT NULL
EXEC sp_executesql N'/*****************************************************
* Name : vwSearchOrder
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER VIEW [dbo].[vwSearchOrder]
AS
SELECT     O.Id, O.PurchaseOrderNumber, O.ExternalOrderNumber, O.OrderTypeId, O.OrderDate, US.FirstName, US.MiddleName, US.LastName, O.OrderStatusId, O.TotalTax, 
                      O.CODCharges, O.TotalShippingCharge AS ShippingTotal, O.TotalShippingCharge, O.RefundTotal, O.SiteId, dbo.Order_GetQuantity(O.Id) AS Quantity, ISNULL
                          ((SELECT     SUM(ISNULL(dbo.OROrderItem.Quantity, 0)) AS Expr1
                              FROM         dbo.OROrderItem INNER JOIN
                                                    dbo.PRProductSKU AS PS ON dbo.OROrderItem.ProductSKUId = PS.Id INNER JOIN
                                                    dbo.PRProduct AS P ON PS.ProductId = P.Id
                              WHERE     (dbo.OROrderItem.OrderId = O.Id) AND (P.IsBundle = 0)), 0) AS NumSkus, O.OrderTotal, O.TotalDiscount, O.TaxableOrderTotal, O.GrandTotal, 
                      O.CustomerId, O.TotalShippingDiscount, O.CreatedBy, FN.UserFullName AS CreatedByFullName, CASE WHEN EXISTS
                          (SELECT     *
                            FROM          FFOrderShipment F INNER JOIN
                                                   dbo.OROrderShipping AS OS ON F.OrderShippingId = OS.Id
                            WHERE      OS.OrderId = O.Id AND (ShipmentStatus = 3 OR ShipmentStatus = 9)) THEN 3 
                            ELSE 0 END AS ExternalShippingStatus
FROM         dbo.OROrder AS O 
					INNER JOIN dbo.USCommerceUserProfile AS U ON O.CustomerId = U.Id 
					INNER JOIN dbo.USUser US on US.Id = U.Id
                    LEFT OUTER JOIN dbo.VW_CustomerFullName AS FN ON FN.UserId = O.CreatedBy

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[vwSearchCustomer]'
GO
IF OBJECT_ID(N'[dbo].[vwSearchCustomer]', 'V') IS NOT NULL
EXEC sp_executesql N'/*****************************************************
* Name : vwSearchCustomer
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER  VIEW [dbo].[vwSearchCustomer]
AS
with SU as
(Select distinct UserId,SiteId,IsSystemUser
FROM USSiteUser)
SELECT      U.Id AS UserId, U.UserName, M.Email, CP.Id, U.FirstName, U.LastName, U.MiddleName, U.CompanyName, U.BirthDate, U.Gender, 
                      CP.IsBadCustomer, CP.IsActive, CP.IsOnMailingList, CP.Status, CP.CSRSecurityQuestion, CP.CSRSecurityPassword, U.HomePhone, U.MobilePhone, 
                      U.OtherPhone,U.ImageId,  A.Id AS ShippingAddressId, A.FirstName + '' '' + A.LastName [Name], A.AddressType, A.AddressLine1, A.AddressLine2, A.AddressLine3, A.City, A.StateId, A.Zip, 
                      A.CountryId,C.CountryName Country, C.CountryCode, A.County, A.Phone, UA.Id AS AddressId, UA.NickName, UA.IsPrimary, UA.Sequence, U.CreatedBy, U.CreatedDate, U.ModifiedDate, U.ModifiedBy, 
                      S.State, S.StateCode, CP.AccountNumber, CP.IsExpressCustomer, SU.SiteId AS SiteId
                      ,CP.ExternalId
                      ,CP.ExternalProfileId
FROM         dbo.USUser AS U INNER JOIN
					SU ON U.Id = SU.UserId INNER JOIN
                      dbo.USMembership AS M ON U.Id = M.UserId INNER JOIN
                      dbo.USCommerceUserProfile AS CP ON CP.Id = U.Id LEFT OUTER JOIN
                      dbo.USUserShippingAddress AS UA ON UA.UserId = U.Id AND UA.IsPrimary = 1 LEFT OUTER JOIN
                      dbo.GLAddress AS A ON A.Id = UA.AddressId LEFT OUTER JOIN
                      dbo.GLState AS S ON A.StateId = S.Id LEFT OUTER JOIN
                      dbo.GLCountry AS C on A.CountryId = C.Id
'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
