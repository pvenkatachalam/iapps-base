/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:42:50 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[OROrderItemCouponCode]'
GO
IF OBJECT_ID(N'[dbo].[OROrderItemCouponCode]', 'U') IS NULL
CREATE TABLE [dbo].[OROrderItemCouponCode]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[OrderItemId] [UNIQUEIDENTIFIER] NOT NULL,
[CouponCodeId] [UNIQUEIDENTIFIER] NOT NULL,
[Amount] [MONEY] NOT NULL DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__OROrderItemCoupo__3A5167A9] on [dbo].[OROrderItemCouponCode]'
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE  object_id = OBJECT_ID(N'[dbo].[OROrderItemCouponCode]') AND type=1)
ALTER TABLE [dbo].[OROrderItemCouponCode] ADD PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_OROrderItemCouponCode_OrderItemId] on [dbo].[OROrderItemCouponCode]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_OROrderItemCouponCode_OrderItemId' AND object_id = OBJECT_ID(N'[dbo].[OROrderItemCouponCode]'))
CREATE NONCLUSTERED INDEX [IX_OROrderItemCouponCode_OrderItemId] ON [dbo].[OROrderItemCouponCode] ([OrderItemId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SMSocialMedia]'
GO
IF OBJECT_ID(N'[dbo].[SMSocialMedia]', 'U') IS NULL
CREATE TABLE [dbo].[SMSocialMedia]
(
[Id] [int] NOT NULL,
[Name] [nvarchar] (1024) NOT NULL,
[Provider] [nvarchar] (1024) NOT NULL,
[IsActive] [bit] NOT NULL DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SMSocialMedia] on [dbo].[SMSocialMedia]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_SMSocialMedia' AND object_id = OBJECT_ID(N'[dbo].[SMSocialMedia]'))
ALTER TABLE [dbo].[SMSocialMedia] ADD CONSTRAINT [PK_SMSocialMedia] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END


GO
PRINT N'Creating [dbo].[ORRefundItem]...';

GO
IF OBJECT_ID(N'[dbo].[ORRefundItem]', 'U') IS NULL
CREATE TABLE [dbo].[ORRefundItem] (
    [Id]               UNIQUEIDENTIFIER NOT NULL,
    [RefundId]         UNIQUEIDENTIFIER NOT NULL,
    [ReturnItemId]     UNIQUEIDENTIFIER NULL,
    [OrderItemId]      UNIQUEIDENTIFIER NOT NULL,
    [Quantity]         DECIMAL (18, 2)  NULL,
    [Amount]           AS               ((([ItemTotal] + [Shipping]) + [Tax]) + [AdditionalRefund]),
    [ItemTotal]        MONEY            NOT NULL DEFAULT(0),
    [Shipping]         MONEY            NOT NULL DEFAULT(0),
	ItemTax			MONEY				NOT NULL DEFAULT(0),
	ShippingTax		MONEY				NOT NULL DEFAULT(0),
    [Tax]              MONEY            NOT NULL DEFAULT(0),
    [AdditionalRefund] MONEY            NOT NULL DEFAULT(0),
    [CreatedDate]      DATETIME         NOT NULL,
    [CreatedBy]        UNIQUEIDENTIFIER NOT NULL,
    [ModifiedDate]     DATETIME         NULL,
    [ModifiedBy]       UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_ORRefundItem] PRIMARY KEY CLUSTERED ([Id] ASC)
) ON [PRIMARY];
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ORRefundItem_ORRefund]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[ORRefundItem]', 'U'))
ALTER TABLE [dbo].[ORRefundItem]  WITH CHECK ADD  CONSTRAINT [FK_ORRefundItem_ORRefund] FOREIGN KEY([RefundId]) REFERENCES [dbo].[ORRefund] ([Id])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ORRefundItem_ORReturnItem]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[ORRefundItem]', 'U'))
ALTER TABLE [dbo].[ORRefundItem]  WITH CHECK ADD  CONSTRAINT [FK_ORRefundItem_ORReturnItem] FOREIGN KEY([ReturnItemId]) REFERENCES [dbo].[ORReturnItem] ([Id])

GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
PRINT N'Creating [dbo].[COFormStructure]'
GO
IF OBJECT_ID(N'[dbo].[COFormStructure]', 'U') IS NULL
CREATE TABLE [dbo].[COFormStructure]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[ParentId] [UNIQUEIDENTIFIER] NULL,
[LftValue] [BIGINT] NOT NULL,
[RgtValue] [BIGINT] NOT NULL,
[Title] [NVARCHAR] (256) NOT NULL,
[Description] [NVARCHAR] (1024) NULL,
[SiteId] [UNIQUEIDENTIFIER] NULL,
[Exists] [BIT] NULL,
[Attributes] [INT] NULL,
[Size] [DECIMAL] (9, 0) NULL,
[Status] [INT] NULL,
[CreatedBy] [UNIQUEIDENTIFIER] NOT NULL,
[CreatedDate] [DATETIME] NOT NULL,
[ModifiedBy] [UNIQUEIDENTIFIER] NULL,
[ModifiedDate] [DATETIME] NULL,
[PhysicalPath] [NVARCHAR] (4000) NULL,
[VirtualPath] [NVARCHAR] (4000) NULL,
[ThumbnailImagePath] [NVARCHAR] (4000) NULL,
[FolderIconPath] [NVARCHAR] (4000) NULL,
[Keywords] [NTEXT] NULL,
[IsSystem] [BIT] NOT NULL DEFAULT ((0)),
[IsMarketierDir] [BIT] NOT NULL DEFAULT ((0)),
[SourceDirId] [UNIQUEIDENTIFIER] NULL,
[AllowAccessInChildrenSites] [BIT] NOT NULL DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_COFormStructure] on [dbo].[COFormStructure]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_COFormStructure' AND object_id = OBJECT_ID(N'[dbo].[COFormStructure]'))
ALTER TABLE [dbo].[COFormStructure] ADD CONSTRAINT [PK_COFormStructure] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_COFormStructure] on [dbo].[COFormStructure]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_COFormStructure' AND object_id = OBJECT_ID(N'[dbo].[COFormStructure]'))
CREATE NONCLUSTERED INDEX [IX_COFormStructure] ON [dbo].[COFormStructure] ([LftValue], [RgtValue])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[USSocialUserProfile]'
GO
IF OBJECT_ID(N'[dbo].[USSocialUserProfile]', 'U') IS NULL
CREATE TABLE [dbo].[USSocialUserProfile]
(
[Id] [uniqueidentifier] NOT NULL,
[UserId] [uniqueidentifier] NOT NULL,
[SiteId] [uniqueidentifier] NOT NULL,
[ExternalId] [nvarchar] (500) NULL,
[ExternalPassword] [nvarchar] (500) NULL,
[APIToken] [nvarchar] (500) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_USSocialSettings] on [dbo].[USSocialUserProfile]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_USSocialSettings' AND object_id = OBJECT_ID(N'[dbo].[USSocialUserProfile]'))
ALTER TABLE [dbo].[USSocialUserProfile] ADD CONSTRAINT [PK_USSocialSettings] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SIGroup]'
GO
IF OBJECT_ID(N'[dbo].[SIGroup]', 'U') IS NULL
CREATE TABLE [dbo].[SIGroup]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[Title] [NVARCHAR] (256) NOT NULL,
[Description] [NVARCHAR] (1024) NULL,
[Status] [INT] NOT NULL,
[CreatedBy] [UNIQUEIDENTIFIER] NOT NULL,
[CreatedDate] [DATETIME] NOT NULL,
[ModifiedBy] [UNIQUEIDENTIFIER] NULL,
[ModifiedDate] [DATETIME] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SIGroup] on [dbo].[SIGroup]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_SIGroup' AND object_id = OBJECT_ID(N'[dbo].[SIGroup]'))
ALTER TABLE [dbo].[SIGroup] ADD CONSTRAINT [PK_SIGroup] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SISiteGroup]'
GO
IF OBJECT_ID(N'[dbo].[SISiteGroup]', 'U') IS NULL
CREATE TABLE [dbo].[SISiteGroup]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[SiteId] [UNIQUEIDENTIFIER] NOT NULL,
[GroupId] [UNIQUEIDENTIFIER] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SISiteGroup] on [dbo].[SISiteGroup]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_SISiteGroup' AND object_id = OBJECT_ID(N'[dbo].[SISiteGroup]'))
ALTER TABLE [dbo].[SISiteGroup] ADD CONSTRAINT [PK_SISiteGroup] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[UploadContactDataLog]'
GO
IF OBJECT_ID(N'[dbo].[UploadContactDataLog]', 'U') IS NULL
CREATE TABLE [dbo].[UploadContactDataLog]
(
[Id] [uniqueidentifier] NOT NULL,
[UploadHistoryId] [uniqueidentifier] NOT NULL,
[ErrorType] [int] NOT NULL,
[ErrorMessage] [nvarchar] (max) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGExceptionLog]'
GO
IF OBJECT_ID(N'[dbo].[LGExceptionLog]', 'U') IS NULL
CREATE TABLE [dbo].[LGExceptionLog]
(
[Id] [INT] NOT NULL IDENTITY(1, 1),
[LogId] [INT] NOT NULL,
[StackTrace] [NTEXT] NOT NULL,
[InnerException] [XML] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LGExceptionLog] on [dbo].[LGExceptionLog]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_LGExceptionLog' AND object_id = OBJECT_ID(N'[dbo].[LGExceptionLog]'))
ALTER TABLE [dbo].[LGExceptionLog] ADD CONSTRAINT [PK_LGExceptionLog] PRIMARY KEY CLUSTERED  ([Id])
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_LGExceptionLog_LogId' AND object_id = OBJECT_ID(N'[dbo].[LGExceptionLog]'))
CREATE  NONCLUSTERED INDEX IX_LGExceptionLog_LogId ON dbo.LGExceptionLog([LogId])
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGLog]'
GO
IF OBJECT_ID(N'[dbo].[LGLog]', 'U') IS NULL
CREATE TABLE [dbo].[LGLog]
(
[Id] [INT] NOT NULL IDENTITY(1, 1),
[SiteId] [UNIQUEIDENTIFIER] NOT NULL,
[EventId] [INT] NOT NULL,
[Priority] [INT] NOT NULL,
[Severity] [NVARCHAR] (100) NOT NULL,
[CreatedDate] [DATETIME] NOT NULL,
[MachineName] [NVARCHAR] (100) NOT NULL,
[ProcessId] [NVARCHAR] (256) NOT NULL,
[ActivityId] [UNIQUEIDENTIFIER] NOT NULL,
[Message] [NTEXT] NULL,
[AdditionalInfo] [NTEXT] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LGLog_1] on [dbo].[LGLog]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_LGLog_1' AND object_id = OBJECT_ID(N'[dbo].[LGLog]'))
ALTER TABLE [dbo].[LGLog] ADD CONSTRAINT [PK_LGLog_1] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGCategoryLog]'
GO
IF OBJECT_ID(N'[dbo].[LGCategoryLog]', 'U') IS NULL
CREATE TABLE [dbo].[LGCategoryLog]
(
[Id] [INT] NOT NULL IDENTITY(1, 1),
[LogId] [INT] NOT NULL,
[CategoryId] [INT] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LGCategoryLog_1] on [dbo].[LGCategoryLog]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_LGCategoryLog_1' AND object_id = OBJECT_ID(N'[dbo].[LGCategoryLog]'))
ALTER TABLE [dbo].[LGCategoryLog] ADD CONSTRAINT [PK_LGCategoryLog_1] PRIMARY KEY CLUSTERED  ([Id] ASC)
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IX_LGCategoryLog_LogId' AND object_id = OBJECT_ID(N'[dbo].[LGCategoryLog]'))
CREATE  NONCLUSTERED INDEX IX_LGCategoryLog_LogId ON dbo.LGCategoryLog([LogId])
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGCategory]'
GO
IF OBJECT_ID(N'[dbo].[LGCategory]', 'U') IS NULL
CREATE TABLE [dbo].[LGCategory]
(
[Id] [INT] NOT NULL IDENTITY(1, 1),
[Title] [NVARCHAR] (500) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_LGCategory] on [dbo].[LGCategory]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_LGCategory' AND object_id = OBJECT_ID(N'[dbo].[LGCategory]'))
ALTER TABLE [dbo].[LGCategory] ADD CONSTRAINT [PK_LGCategory] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SMSocialCredentials]'
GO
IF OBJECT_ID(N'[dbo].[SMSocialCredentials]', 'U') IS NULL
CREATE TABLE [dbo].[SMSocialCredentials]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[SocialMediaId] [INT] NOT NULL,
[ExternalId] [NVARCHAR] (50) NULL,
[Token] [NVARCHAR] (1024) NOT NULL,
[Secret] [NVARCHAR] (1024) NULL,
[UserId] [UNIQUEIDENTIFIER] NOT NULL,
[CreatedDate] [DATETIME] NOT NULL,
[CreatedBy] [UNIQUEIDENTIFIER] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__SMSocialCredenti__5522C40F] on [dbo].[SMSocialCredentials]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE type=1 AND object_id = OBJECT_ID(N'[dbo].[SMSocialCredentials]'))
ALTER TABLE [dbo].[SMSocialCredentials] ADD PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VEPostContentVersion]'
GO
IF OBJECT_ID(N'[dbo].[VEPostContentVersion]', 'U') IS NULL
CREATE TABLE [dbo].[VEPostContentVersion]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[PostVersionId] [UNIQUEIDENTIFIER] NOT NULL,
[ContentVersionId] [UNIQUEIDENTIFIER] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__VEPostContentVer__B71FF44] on [dbo].[VEPostContentVersion]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE type=1 AND object_id = OBJECT_ID(N'[dbo].[VEPostContentVersion]'))
ALTER TABLE [dbo].[VEPostContentVersion] ADD PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GLInvalidateCache]'
GO
IF OBJECT_ID(N'[dbo].[GLInvalidateCache]', 'U') IS NULL
CREATE TABLE [dbo].[GLInvalidateCache]
(
[SiteId] [uniqueidentifier] NOT NULL,
[Url] [nvarchar] (max) NOT NULL
)
GO
IF COL_LENGTH(N'[dbo].[GLInvalidateCache]', N'PublicSiteUrl') IS NULL
	ALTER TABLE GLInvalidateCache  ADD [PublicSiteUrl] [nvarchar](max) NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[USMemberGroupVariantSite]'
GO
IF OBJECT_ID(N'[dbo].[USMemberGroupVariantSite]', 'U') IS NULL
CREATE TABLE [dbo].[USMemberGroupVariantSite]
(
[ApplicationId] [UNIQUEIDENTIFIER] NULL,
[MemberId] [UNIQUEIDENTIFIER] NOT NULL,
[MemberType] [SMALLINT] NOT NULL,
[GroupId] [UNIQUEIDENTIFIER] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[HSStructureBKUP]'
GO
IF OBJECT_ID(N'[dbo].[HSStructureBKUP]', 'U') IS NULL
CREATE TABLE [dbo].[HSStructureBKUP]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[Title] [NVARCHAR] (256) NULL,
[ObjectTypeId] [INT] NULL,
[Keywords] [NTEXT] NULL,
[ParentId] [UNIQUEIDENTIFIER] NULL,
[LftValue] [BIGINT] NOT NULL,
[RgtValue] [BIGINT] NOT NULL,
[SiteId] [UNIQUEIDENTIFIER] NULL,
[MicroSiteId] [UNIQUEIDENTIFIER] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SISiteBkUp]'
GO
IF OBJECT_ID(N'[dbo].[SISiteBkUp]', 'U') IS NULL
CREATE TABLE [dbo].[SISiteBkUp]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[Title] [NVARCHAR] (256) NOT NULL,
[Description] [NVARCHAR] (1024) NULL,
[URL] [XML] NULL,
[VirtualPath] [NVARCHAR] (4000) NULL,
[ParentSiteId] [UNIQUEIDENTIFIER] NULL,
[Type] [INT] NOT NULL,
[Keywords] [NVARCHAR] (4000) NULL,
[CreatedBy] [UNIQUEIDENTIFIER] NOT NULL,
[CreatedDate] [DATETIME] NOT NULL,
[ModifiedBy] [UNIQUEIDENTIFIER] NULL,
[ModifiedDate] [DATETIME] NULL,
[Status] [INT] NOT NULL,
[LicenseKey] [NVARCHAR] (100) NULL,
[HostingPackageInstanceId] [INT] NOT NULL,
[LicenseLevel] [INT] NOT NULL,
[SendNotification] [BIT] NULL,
[SubmitToTranslation] [BIT] NULL,
[AllowMasterWebSiteUser] [BIT] NULL,
[ImportAllAdminPermission] [BIT] NULL,
[UICulture] [NVARCHAR] (10) NULL,
[Prefix] [NVARCHAR] (5) NULL,
[LoginURL] [NVARCHAR] (1024) NULL,
[DefaultURL] [NVARCHAR] (1024) NULL,
[NotifyOnTranslationError] [BIT] NULL,
[TranslationEnabled] [BIT] NULL,
[TranslatePagePropertiesByDefault] [BIT] NULL,
[AutoImportTranslatedSubmissions] [BIT] NULL,
[DefaultTranslateToLanguage] [NVARCHAR] (10) NULL,
[PageImportOptions] [INT] NULL,
[LftValue] [INT] NOT NULL,
[RgtValue] [INT] NOT NULL,
[ImportContentStructure] [BIT] NULL,
[ImportFileStructure] [BIT] NULL,
[ImportImageStructure] [BIT] NULL,
[ImportFormsStructure] [BIT] NULL,
[MasterSiteId] [UNIQUEIDENTIFIER] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[USSiteUserVariantSite]'
GO
IF OBJECT_ID(N'[dbo].[USSiteUserVariantSite]', 'U') IS NULL
CREATE TABLE [dbo].[USSiteUserVariantSite]
(
[SiteId] [UNIQUEIDENTIFIER] NOT NULL,
[UserId] [UNIQUEIDENTIFIER] NOT NULL,
[IsSystemUser] [BIT] NOT NULL,
[ProductId] [UNIQUEIDENTIFIER] NOT NULL,
[UniqueNo] [INT] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGLogHistory]'
GO
IF OBJECT_ID(N'[dbo].[LGLogHistory]', 'U') IS NULL
CREATE TABLE [dbo].[LGLogHistory](
	[Id] [int] NOT NULL,
	[SiteId] [uniqueidentifier] NOT NULL,
	[EventId] [int] NOT NULL,
	[Priority] [int] NOT NULL,
	[Severity] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[MachineName] [nvarchar](100) NOT NULL,
	[ProcessId] [nvarchar](256) NOT NULL,
	[ActivityId] [uniqueidentifier] NOT NULL,
	[Message] [ntext] NULL,
	[AdditionalInfo] [ntext] NULL
) 
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGCategoryLogHistory]'
GO
IF OBJECT_ID(N'[dbo].[LGCategoryLogHistory]', 'U') IS NULL
CREATE TABLE [dbo].[LGCategoryLogHistory] (
    [Id]         INT NOT NULL,
    [LogId]      INT NOT NULL,
    [CategoryId] INT NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[LGExceptionLogHistory]'
GO
IF OBJECT_ID(N'[dbo].[LGExceptionLogHistory]', 'U') IS NULL
CREATE TABLE [dbo].[LGExceptionLogHistory] (
    [Id]             INT   NOT NULL,
    [LogId]          INT   NOT NULL,
    [StackTrace]     NTEXT NOT NULL,
    [InnerException] XML   NULL
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[USMemberRolesVariantSite]'
GO
IF OBJECT_ID(N'[dbo].[USMemberRolesVariantSite]', 'U') IS NULL
CREATE TABLE [dbo].[USMemberRolesVariantSite]
(
[ProductId] [UNIQUEIDENTIFIER] NOT NULL,
[ApplicationId] [UNIQUEIDENTIFIER] NOT NULL,
[MemberId] [UNIQUEIDENTIFIER] NOT NULL,
[MemberType] [SMALLINT] NOT NULL,
[RoleId] [INT] NOT NULL,
[ObjectId] [UNIQUEIDENTIFIER] NOT NULL,
[ObjectTypeId] [INT] NOT NULL,
[Propagate] [BIT] NOT NULL,
[UniqueNo] [INT] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SMUserChannel]'
GO
IF OBJECT_ID(N'[dbo].[SMUserChannel]', 'U') IS NULL
CREATE TABLE [dbo].[SMUserChannel]
(
[Id] [UNIQUEIDENTIFIER] NOT NULL,
[SocialMediaId] [INT] NOT NULL,
[UserId] [UNIQUEIDENTIFIER] NOT NULL,
[Token] [NVARCHAR] (1024) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
Go
PRINT N'Creating [dbo].[PageRedirect_Import]'
GO

IF OBJECT_ID(N'[dbo].[PageRedirect_Import]', 'U') IS NULL
CREATE TABLE [dbo].[PageRedirect_Import] (
    [Id]     UNIQUEIDENTIFIER NOT NULL,
    [OldURL] NVARCHAR (1000)  NOT NULL,
    [NewURL] NVARCHAR (1000)  NOT NULL
);
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__SMUserChannel__35855C79] on [dbo].[SMUserChannel]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE type=1 AND object_id = OBJECT_ID(N'[dbo].[SMUserChannel]'))
ALTER TABLE [dbo].[SMUserChannel] ADD PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[USSocialUserProfile]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USSocialSettings_USUser]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[USSocialUserProfile]', 'U'))
ALTER TABLE [dbo].[USSocialUserProfile] ADD CONSTRAINT [FK_USSocialSettings_USUser] FOREIGN KEY ([UserId]) REFERENCES [dbo].[USUser] ([Id])
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USSocialSettings_SISite]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[USSocialUserProfile]', 'U'))
ALTER TABLE [dbo].[USSocialUserProfile] ADD CONSTRAINT [FK_USSocialSettings_SISite] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[SISite] ([Id])
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USSocialUserProfile_SISite]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[USSocialUserProfile]', 'U'))
ALTER TABLE [dbo].[USSocialUserProfile] ADD CONSTRAINT [FK_USSocialUserProfile_SISite] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[SISite] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[OROrderItemCouponCode]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ORCouponCodeOrderItem_OROrderItem]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[OROrderItemCouponCode]', 'U'))
ALTER TABLE [dbo].[OROrderItemCouponCode] ADD CONSTRAINT [FK_ORCouponCodeOrderItem_OROrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [dbo].[OROrderItem] ([Id])
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ORCouponCodeOrderItem_ORCouponCode]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[OROrderItemCouponCode]', 'U'))
ALTER TABLE [dbo].[OROrderItemCouponCode] ADD CONSTRAINT [FK_ORCouponCodeOrderItem_ORCouponCode] FOREIGN KEY ([CouponCodeId]) REFERENCES [dbo].[CPCouponCode] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SISiteGroup]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SISiteGroup_SISite]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[SISiteGroup]', 'U'))
ALTER TABLE [dbo].[SISiteGroup] ADD CONSTRAINT [FK_SISiteGroup_SISite] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[SISite] ([Id])
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SISiteGroup_SIGroup]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[SISiteGroup]', 'U'))
ALTER TABLE [dbo].[SISiteGroup] ADD CONSTRAINT [FK_SISiteGroup_SIGroup] FOREIGN KEY ([GroupId]) REFERENCES [dbo].[SIGroup] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
