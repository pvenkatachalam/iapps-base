/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:47:34 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating types'
GO
IF TYPE_ID(N'[dbo].[SiteTableType]') IS NULL
CREATE TYPE [dbo].[SiteTableType] AS TABLE
(
[ObjectId] [UNIQUEIDENTIFIER] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF TYPE_ID(N'[dbo].[TYDirectory]') IS NULL
CREATE TYPE [dbo].[TYDirectory] AS TABLE
(
[Id] [UNIQUEIDENTIFIER] NULL,
[Title] [NVARCHAR] (256) NULL,
[Description] [NVARCHAR] (1024) NULL,
[Attributes] [INT] NULL,
[Exists] [BIT] NULL,
[FolderIconPath] [NVARCHAR] (4000) NULL,
[PhysicalPath] [NVARCHAR] (4000) NULL,
[Size] [BIGINT] NULL,
[Status] [INT] NULL,
[ThumbnailImagePath] [NVARCHAR] (4000) NULL,
[VirtualPath] [NVARCHAR] (4000) NULL,
[ObjectTypeId] [INT] NULL,
[ParentId] [UNIQUEIDENTIFIER] NULL,
[Lft] [BIGINT] NULL,
[Rgt] [BIGINT] NULL,
[IsSystem] [BIT] NULL,
[IsMarketierDir] [BIT] NULL,
[AllowAccessInChildrenSites] [bit] NULL,
[SiteId] [UNIQUEIDENTIFIER] NULL,
[MicrositeId] [UNIQUEIDENTIFIER] NULL,
UNIQUE NONCLUSTERED  ([Rgt]),
UNIQUE NONCLUSTERED  ([Lft])
)
GO
--DROP DEPENDECNCIES OF TYUSer
IF OBJECT_ID(N'[dbo].[Membership_BuildUser]', 'P') IS NOT NULL
	DROP PROCEDURE Membership_BuildUser
GO
IF TYPE_ID(N'[dbo].[TYUser]') IS NOT NULL
DROP TYPE [TYUser]
GO
IF TYPE_ID(N'[dbo].[TYUser]') IS NULL
CREATE TYPE [dbo].[TYUser] AS TABLE
(
	Id						uniqueidentifier, 
	UserName				nvarchar(256),  
	Email					nvarchar(512),
	PasswordQuestion		nvarchar(512),  
	IsApproved				bit,
	CreatedDate				datetime,
	LastLoginDate			datetime,
	LastActivityDate		datetime,
	LastPasswordChangedDate	datetime,
	IsLockedOut				bit,
	LastLockoutDate			datetime,
	FirstName				nvarchar(256),
	LastName				nvarchar(256),
	MiddleName				nvarchar(256) ,
	ExpiryDate				datetime ,
	EmailNotification		bit ,
	TimeZone				nvarchar(100),
	ReportRangeSelection	varchar(50),
	ReportStartDate			datetime ,
	ReportEndDate			datetime, 
	BirthDate				datetime,
	CompanyName				nvarchar(1024),
	Gender					nvarchar(50),
	HomePhone				varchar(50),
	MobilePhone				varchar(50),
	OtherPhone				varchar(50),
	ImageId					uniqueidentifier,
	Status					int,
	ProductSuite			varchar(500)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
