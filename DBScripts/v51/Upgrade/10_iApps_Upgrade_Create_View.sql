/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:45:06 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[vwSiteUrls]'
GO
IF OBJECT_ID(N'[dbo].[vwSiteUrls]', 'V') IS NOT NULL
	DROP VIEW vwSiteUrls
GO
IF OBJECT_ID(N'[dbo].[vwSiteUrls]', 'V') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : vwSiteUrls
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE VIEW [dbo].[vwSiteUrls]
AS

SELECT 
	Id, PrimarySiteUrl AS Url
FROM SISite
	WHERE PrimarySiteUrl IS NOT NULL AND PrimarySiteUrl != ''''
UNION
SELECT 
	Id, S.c.value(''.'', ''nvarchar(1024)'') 
FROM (SELECT Id, Url FROM SISite) X
	CROSS APPLY Url.nodes(''//URL'') AS S(c)

'
GO

PRINT N'Creating [dbo].[vwTaxRefund]'
GO
IF OBJECT_ID(N'[dbo].[vwTaxRefund]', 'V') IS NOT NULL
	DROP VIEW vwTaxRefund
GO
IF OBJECT_ID(N'[dbo].[vwTaxRefund]', 'V') IS NULL
EXEC sp_executesql N'CREATE VIEW [dbo].[vwTaxRefund]
	AS 
	SELECT   P.PaymentStatusId
		 ,RI.Tax
		 ,P.ModifiedDate
		 ,R.Id
		 ,OP.OrderId
		 ,RI.OrderItemId
		 ,RI.Quantity
		 ,OI.OrderShippingId
		 ,case when OI.Tax >0 then  RI.ItemTotal ELSE 0 END TaxableTotal
FROM         dbo.ORRefund R
			INNER JOIN dbo.PTRefundPayment RP ON R.Id = RP.RefundId
			INNER JOIN dbo.PTOrderPayment OP ON RP.RefundOrderPaymentId = OP.Id 
			INNER JOIN dbo.ORRefundItem RI ON R.Id = RI.RefundId 
			INNER JOIN dbo.PTPayment P ON OP.PaymentId = P.Id
			INNER JOIN dbo.OROrderItem OI on RI.OrderItemId= OI.Id
			WHERE P.IsRefund =1 and P.PaymentStatusId = 12
'

GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[vwFormattedSiteUrls]'
GO
IF OBJECT_ID(N'[dbo].[vwFormattedSiteUrls]', 'V') IS NOT NULL
	DROP VIEW vwFormattedSiteUrls
GO
IF OBJECT_ID(N'[dbo].[vwFormattedSiteUrls]', 'V') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : vwFormattedSiteUrls
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE VIEW [dbo].[vwFormattedSiteUrls]
AS

SELECT 
	 Id, dbo.GetDomainLoweredUrl(Url) AS Url
FROM vwSiteUrls


'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
