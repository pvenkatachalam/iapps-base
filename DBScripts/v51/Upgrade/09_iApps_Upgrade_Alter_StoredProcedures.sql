/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:31:25 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetAssetFolderSecurityLevelMap]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetAssetFolderSecurityLevelMap
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetAssetFolderSecurityLevelMap]
	@ApplicationId uniqueidentifier
AS
BEGIN
select F.Id, F.VirtualPath,S.SecurityLevelId, 33 as AssetObjectTypeId from COImageStructure F 
	Inner Join USSecurityLevelObject S on F.Id=S.ObjectId  AND ObjectTypeId = 6
	INNER JOIN USSecurityLevel SL on S.SecurityLevelId = SL.Id
Where F.SiteId = @ApplicationId AND SL.Status = 1  --Don't grab security levels that are deleted
union
select F.Id, F.VirtualPath,S.SecurityLevelId, 9 as AssetObjectTypeId from COFileStructure F 
	Inner Join USSecurityLevelObject S on F.Id=S.ObjectId  AND ObjectTypeId = 6
	INNER JOIN USSecurityLevel SL on S.SecurityLevelId = SL.Id
Where F.SiteId = @ApplicationId AND SL.Status = 1  --Don't grab security levels that are deleted
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_CheckUser]'
GO
/*****************************************************
* Name : Membership_CheckUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- select * from usmembership where email = 'pkalani@blinedigital.com'
-- [Membership_CheckUser] 'pkalani11@bridgelinesw.com', 'pkalani11@bridgelinesw.com', 1
ALTER PROCEDURE [dbo].[Membership_CheckUser]
(
	@UserName nvarchar(256)
	, @Email nvarchar(256)
	, @MatchedOnEmail int output
	, @ActualUsername nvarchar(256) output
	, @ApplicationId uniqueidentifier
)
as
begin

set @MatchedOnEmail = 0
set @ActualUsername = ''

declare @UserType int
set @UserType = 0

declare @UserId uniqueidentifier

select @UserId = U.Id from USUser U JOIN USSiteUser S ON U.Id = S.UserId 
where U.Status = 1 and U.UserName = @UserName and S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))

if(@UserId is null)
begin
	set @MatchedOnEmail = 1
	select @UserId = U.Id from USUser U JOIN USSiteUser S ON U.Id = S.UserId 
	where U.Status = 1 and U.UserName = @Email and S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
end

if(@UserId is null)
begin
	set @MatchedOnEmail = 2
	select @UserId = U.Id, @ActualUsername = U.UserName from USUser U inner join
	usmembership um on um.UserId = U.Id
	JOIN USSiteUser S ON U.Id = S.UserId  
	where U.Status = 1 and um.email = @Email and S.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
end

if(@UserId is not null)
begin
	set @UserType = 2
	
	if(OBJECT_ID ('USMarketierUserProfile','U') is not null)
	begin	
		select @UserType = 1 from USMarketierUserProfile
		where UserId = @UserId
	end
	
	if(OBJECT_ID ('USCommerceUserProfile','U') is not null)
	begin
		select @UserType = 3 from USCommerceUserProfile
		where Id = @UserId and IsExpressCustomer = 1
		
		select @UserType = 4 from USCommerceUserProfile
		where Id = @UserId and (IsExpressCustomer is null or IsExpressCustomer = 0)
	END
end
--select @userid
--select @UserType
return @UserType

end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [dbo].[PageMap_GetReDirect]'
GO
ALTER PROCEDURE [dbo].[PageMap_GetReDirect]
--********************************************************************************
-- PARAMETERS
		@ApplicationId	uniqueidentifier,
		@IsGenerated bit = NULL,
		@Keyword nvarchar(100) = NULL,
		@PageSize	int = NULL,
		@PageNumber	int = NULL
--********************************************************************************
AS
BEGIN
		--select Id,OldURL,NewURL,LastURLId from PageRedirect Where ApplicationId=@ApplicationId And Status = 1
		IF @Keyword is not null 
			Set @Keyword = '%' + @Keyword + '%'
		
		;WITH CTE (RowNo, OldUrl, NewUrl, IsGenerated) AS(
			SELECT ROW_NUMBER() OVER(ORDER BY OldURL),
				OldUrl, 
				NewUrl, 
				IsGenerated 
			FROM PageRedirect
			WHERE ApplicationId = @ApplicationId 
				AND Status = 1 
				AND (@Keyword IS NULL OR NewURL like @Keyword OR OldURL like @Keyword)
				AND OldURL IS NOT NULL AND NewURL IS NOT NULL
				AND (@IsGenerated IS NULL OR IsGenerated = @IsGenerated)
			)
		
		SELECT * FROM CTE
			WHERE @PageSize IS NULL OR @PageNumber IS NULL 
				OR (RowNo between (@PageNumber - 1) * @PageSize + 1 and @PageNumber * @PageSize)
		
		SELECT COUNT(*) FROM PageRedirect
			WHERE ApplicationId = @ApplicationId AND Status = 1 AND (@Keyword IS NULL OR NewURL like @Keyword OR OldURL like @Keyword)
				AND (@IsGenerated IS NULL OR IsGenerated = @IsGenerated)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END

GO
PRINT N'Altering [dbo].[UploadContactData_GetUploadStatus]'
GO
ALTER PROCEDURE [dbo].[UploadContactData_GetUploadStatus] 
(
--********************************************************************************
	@UploadHistoryId uniqueidentifier
--********************************************************************************
)
AS


select Status, ErrorMessage from UploadContactData where  UploadHistoryId=@UploadHistoryId

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[UploadContactData_GetUploadStatus]'
GO
ALTER PROCEDURE [dbo].[UploadContactData_GetUploadStatus] 
(
--********************************************************************************
	@UploadHistoryId uniqueidentifier
--********************************************************************************
)
AS
BEGIN

select Status, ErrorMessage from UploadContactData where  UploadHistoryId=@UploadHistoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Address_Save]'
GO
/*****************************************************
* Name : Address_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[Address_Save] (
	@Id UNIQUEIDENTIFIER = NULL OUT
	,@Title NVARCHAR(256) = NULL
	,@Description NVARCHAR(1024) = NULL
	,@ModifiedBy UNIQUEIDENTIFIER
	,@ModifiedDate DATETIME = NULL
	,@Status INT = NULL
	,@FirstName NVARCHAR(255) = NULL
	,@LastName NVARCHAR(255) = NULL
	,@AddressType INT = NULL
	,@AddressLine1 NVARCHAR(255) = NULL
	,@AddressLine2 NVARCHAR(255) = NULL
	,@AddressLine3 NVARCHAR(255) = NULL
	,@City NVARCHAR(255) = NULL
	,@StateId UNIQUEIDENTIFIER = NULL
	,@StateName NVARCHAR(255) = NULL
	,@County NVARCHAR(255) = NULL
	,@CountryId UNIQUEIDENTIFIER = NULL
	,@Phone NVARCHAR(50) = NULL
	,@Zip NVARCHAR(50) = NULL
	,@ApplicationId UNIQUEIDENTIFIER
	)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @reccount INT
	,@error INT
	,@Now DATETIME
	,@stmt VARCHAR(256)
	,@rowcount INT
	,@AddressId UNIQUEIDENTIFIER

BEGIN
	--********************************************************************************
	-- code
	--********************************************************************************
	IF (@Status = dbo.GetDeleteStatus())
	BEGIN
		RAISERROR (
				'INVALID||Status'
				,16
				,1
				)

		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF (@Status IS NULL OR @Status = 0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	/* if @Id specified, ensure exists */
	IF (@Id IS NOT NULL AND @Id != dbo.GetEmptyGUID())
		IF (
				(
					SELECT count(*)
					FROM GLAddress
					WHERE Id = @Id AND STATUS != dbo.GetDeleteStatus()
					) = 0
				)
		BEGIN
			RAISERROR (
					'NOTEXISTS||Id'
					,16
					,1
					)

			RETURN dbo.GetBusinessRuleErrorCode()
		END

	SET @Now = GetUTCDate()

	IF @ModifiedDate IS NULL
		SET @ModifiedDate = @Now
	ELSE
		SET @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate, @ApplicationId)

	IF (@CountryId IS NULL)
		SET @CountryId = (
				SELECT CountryId
				FROM GLState
				WHERE Id = @StateId
				)

	IF (@Id IS NULL OR @Id = dbo.GetEmptyGUID()) -- new insert
	BEGIN
		SET @stmt = 'Address Insert'
		SET @Id = newid();

		INSERT INTO [dbo].[GLAddress] (
			[Id]
			,[FirstName]
			,[LastName]
			,[AddressType]
			,[AddressLine1]
			,[AddressLine2]
			,[AddressLine3]
			,[City]
			,[StateId]
			,[StateName]
			,[Zip]
			,[CountryId]
			,[Phone]
			,[CreatedDate]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBy]
			,[Status]
			,[County]
			)
		VALUES (
			@Id
			,@FirstName
			,@LastName
			,@AddressType
			,@AddressLine1
			,@AddressLine2
			,@AddressLine3
			,@City
			,@StateId
			,@StateName
			,@Zip
			,@CountryId
			,@Phone
			,@ModifiedDate
			,@ModifiedBy
			,@ModifiedDate
			,@ModifiedBy
			,@Status
			,@County
			)

		SELECT @error = @@error

		IF @error <> 0
		BEGIN
			RAISERROR (
					'DBERROR||%s'
					,16
					,1
					,@stmt
					)

			RETURN dbo.GetDataBaseErrorCode()
		END
	END
	ELSE -- update
	BEGIN
		SET @stmt = 'Address Update'

		UPDATE [dbo].[GLAddress]
		WITH (ROWLOCK)

		SET [FirstName] = @FirstName
			,[LastName] = @LastName
			,[AddressType] = @AddressType
			,[AddressLine1] = @AddressLine1
			,[AddressLine2] = @AddressLine2
			,[AddressLine3] = @AddressLine3
			,[City] = @City
			,[StateId] = @StateId
			,[StateName] = @StateName
			,[Phone] = @Phone
			,[Zip] = @Zip
			,[CountryId] = @CountryId
			,[ModifiedDate] = @Now
			,[ModifiedBy] = @ModifiedBy
			,[Status] = @Status
			,[County] = @County
		WHERE Id = @Id --AND ((@ModifiedDate IS NULL AND ModifiedDate IS NULL) OR (@ModifiedDate IS NULL AND ModifiedDate = @Now) OR (@ModifiedDate IS NOT NULL AND ModifiedDate IS NULL AND @ModifiedDate = @Now) OR (@ModifiedDate IS NOT NULL AND ModifiedDate IS NOT NULL AND ModifiedDate = @ModifiedDate))

		--isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
		SELECT @error = @@error
			,@rowcount = @@rowcount

		IF @error <> 0
		BEGIN
			RAISERROR (
					'DBERROR||%s'
					,16
					,1
					,@stmt
					)

			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @rowcount = 0
		BEGIN
			RAISERROR (
					'CONCURRENCYERROR'
					,16
					,1
					)

			RETURN dbo.GetDataConcurrencyErrorCode() -- concurrency error
		END
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_UpdateUserInfo]'
GO
/*****************************************************
* Name : Membership_UpdateUserInfo
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_UpdateUserInfo]
    
--********************************************************************************
-- PARAMETERS
	@ProductId						uniqueidentifier = NULL,
	@ApplicationId					uniqueidentifier,
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************
	DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime
	
	DECLARE @CurrentTimeUtc							datetime
	SET		@CurrentTimeUtc = GetUTCDate()

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

	IF (@ProductId = '00000000-0000-0000-0000-000000000000')
	BEGIN
		SET @ProductId = null
	END

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.Id,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m WITH ( UPDLOCK )
    WHERE   --s.SiteId = @ApplicationId	AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
            u.Id = m.UserId AND
			u.Id = s.UserId AND 
			s.ProductId = ISNULL(@ProductId,s.ProductId) AND
            LOWER(@UserName) = u.LoweredUserName

    --User not found
	IF ( @@rowcount = 0 )
    BEGIN
        RAISERROR ('NOTEXISTS||%s', 16, 1, @UserName)
        GOTO Cleanup
    END

    --User is locked out
	IF( @IsLockedOut = 1 )
    BEGIN
        RAISERROR ('USERLOCKED||%s', 16, 1, @UserName)
		GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            SET @LastLockoutDate = CONVERT( datetime, '17540101', 112 )
        END
    END

	-- update the last activity date and last login date if the password is correct and update last login activity is set ot true
    IF( @UpdateLastLoginActivityDate = 1 AND @IsPasswordCorrect = 1)
    BEGIN
        UPDATE  dbo.USUser
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = [Id]

        IF( @@ERROR <> 0 )
        BEGIN
            RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'LastLoginActivityDate Update')
            GOTO Cleanup
        END

        UPDATE  dbo.USMembership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'LastLoginDate Update')
            GOTO Cleanup
        END
    END


    UPDATE dbo.USMembership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'Membership Update')
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN dbo.GetBusinessRuleErrorCode()

END
--***********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Schedule_Save]'
GO
/*****************************************************
* Name : Schedule_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Schedule_Save]  
(  
      @Id  uniqueidentifier OUT  
     ,@Title nvarchar(256)  
     ,@Description nvarchar(max)  
     ,@StartDate datetime=null  
     ,@EndDate datetime  
     ,@StartTime datetime =null
     ,@EndTime datetime  
     ,@MaxOccurrence int  
     ,@Type int  
     ,@LastRunTime DateTime  
     ,@NextRunTime DateTime  
     ,@ScheduleTypeId uniqueidentifier  
     ,@ModifiedBy uniqueidentifier  
     ,@ModifiedDate datetime  
     ,@ApplicationId uniqueidentifier  
)  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @reccount int,   
  @error   int,  
  @stmt   varchar(256),  
  @rowcount int,  
  @Status  int  
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
  

	/*  Timezone Convert */ 
	SET @StartTime = dbo.ConvertTimeToUtc(@StartTime,@ApplicationId) 
    SET @StartDate = cast(convert(varchar(12), @StartTime,101) as DateTime)
	IF (@EndTime Is not Null)
	BEGIN
		SET @EndTime = dbo.ConvertTimeToUtc(@EndTime,@ApplicationId) 
		SET @EndDate = Cast(convert(varchar(12), @EndTime,101) as DateTime)
	END


 /* IF @Id specified, ensure exists */  
   IF (@Id is not null)  
       IF((SELECT count(*) FROM  [TASchedule] WHERE Id = @Id AND Status != dbo.GetDeleteStatus() And ApplicationId=@ApplicationId) = 0)  
       BEGIN  
   RAISERROR('NOTEXISTS||Id', 16, 1)  
   RETURN dbo.GetBusinessRuleErrorCode()  
       END  

SET @ModifiedDate = getUTCdate()  

		
 SET @Status= dbo.GetActiveStatus()  
  
 IF (@Id is null)   -- New INSERT  
 BEGIN  
  SET @stmt = 'Container INSERT'  
  SET @Id = newid();  
  INSERT INTO [dbo].[TASchedule]  
       ([Id]  
       ,[Title]  
       ,[Description]   
       ,[StartDate]  
       ,[EndDate]  
       ,[StartTime]  
       ,[EndTime]  
       ,[MaxOccurrence]  
       ,[Type]  
       ,[LastRuntime]  
       ,[NextRunTime]  
       ,[ScheduleTypeId]  
       ,[Status]  
       ,[CreatedBy]  
       ,[CreatedDate]  
       ,[ApplicationId])  
    VALUES  
       (@Id  
       ,@Title  
       ,@Description   
       ,@StartDate  
       ,@EndDate  
       ,@StartTime  
       ,@EndTime  
       ,@MaxOccurrence  
       ,@Type  
       ,@LastRuntime  
       ,@NextRunTime  
       ,@ScheduleTypeId   
       ,@Status  
       ,@ModifiedBy  
       ,@ModifiedDate  
       ,@ApplicationId  
     )  
  
  SELECT @error = @@error  
  IF @error <> 0  
  BEGIN  
   RAISERROR('DBERROR||%s',16,1,@stmt)  
   RETURN dbo.GetDataBaseErrorCode()   
  END  
    END  
    ELSE   -- update  
         BEGIN  
   SET @stmt = 'Container UPDATE'  
 UPDATE[dbo].[TASchedule]  
    SET [Id] = @Id  
    ,[Title]=@Title  
    ,[Description]=@Description   
    ,[StartDate] = @StartDate  
    ,[EndDate] = @EndDate  
    ,[StartTime] = @StartTime  
    ,[EndTime] = @EndTime  
    ,[MaxOccurrence] = @MaxOccurrence  
    ,[Type] = @Type  
    ,[ScheduleTypeId] =@ScheduleTypeId  
    ,[Status] = @Status  
    ,[LastRuntime] =@LastRuntime  
    ,[NextRunTime]=@NextRunTime  
    ,[ModifiedBy] = @ModifiedBy  
    ,[ModifiedDate] = @ModifiedDate  
    ,[ApplicationId] = @ApplicationId  
    WHERE  Id    = @Id AND  
   isnull(ModifiedDate,getutcdate())<= getutcdate()  
  
   SELECT @error = @@error, @rowcount = @@rowcount  
  
   IF @error <> 0  
   BEGIN  
    RAISERROR('DBERROR||%s',16,1,@stmt)  
    RETURN dbo.GetDataBaseErrorCode()  
   END  
  
   IF @rowcount = 0  
   BEGIN  
    RAISERROR('CONCURRENCYERROR',16,1)  
    RETURN dbo.GetDataConcurrencyErrorCode()   -- concurrency error  
   END   
  END  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [dbo].[ProfileManager_GetNumberOfInactiveProfiles]'
GO
/*****************************************************
* Name : ProfileManager_GetNumberOfInactiveProfiles
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[ProfileManager_GetNumberOfInactiveProfiles]
-- *************************************************************************
-- PARAMETERS
	@ApplicationId			uniqueidentifier,
	@AuthenticationOption	int = 0,
	@InactiveSinceDate		DATETIME 

--**************************************************************************
AS
BEGIN
    SELECT  COUNT(*)
    FROM    dbo.USUser u, dbo.USMemberProfile p, dbo.USSiteUser s
    WHERE   --s.SiteId = @ApplicationId AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
			u.Id = p.UserId  AND
			u.Id = p.UserId  AND
	       (LastActivityDate <= @InactiveSinceDate) AND
			 (
                (@AuthenticationOption = 2)
                OR (@AuthenticationOption = 0 AND IsAnonymous = 1)
                OR (@AuthenticationOption = 1 AND IsAnonymous = 0)
            )
			AND u.Status <> dbo.GetDeleteStatus()
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PaymentCapture_Save]'
GO
/*****************************************************
* Name : PaymentCapture_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[PaymentCapture_Save]
(
	@CapturedAmount money,
	@PaymentId uniqueidentifier,
	@Id  uniqueidentifier output,
	@TransactionId nvarchar(255) = null,
	@ParentPaymentCaptureId uniqueidentifier = null
)
AS 
BEGIN
DECLARE @CreatedDate datetime
DECLARE @TotalCapturedAmount money
SET @CreatedDate = GetUTCDate()
SET @Id = newid()
INSERT INTO	PTPaymentCapture(
	Id,
	PaymentId,
	CapturedAmount,
	CreatedDate,
	TransactionId,
	ParentPaymentCaptureId
)
VALUES
(
	@Id,
	@PaymentId,
	@CapturedAmount,
	@CreatedDate,
	@TransactionId,
	@ParentPaymentCaptureId
)
SELECT @TotalCapturedAmount = SUM(CapturedAmount) from PTPaymentCapture WHERE PaymentId = @PaymentID
UPDATE PTPayment Set CapturedAmount = @TotalCapturedAmount where Id = @PaymentID
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShipping_Get]'
GO
/*****************************************************
* Name : OrderShipping_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[OrderShipping_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
 SELECT [Id]
  ,[OrderId]
  ,[ShippingOptionId]
  ,[OrderShippingAddressId]
  ,dbo.ConvertTimeFromUtc(ShipOnDate,@ApplicationId)[ShipOnDate]
  ,dbo.ConvertTimeFromUtc(ShipDeliveryDate,@ApplicationId)[ShipDeliveryDate]
  ,[TaxPercentage]
  ,[ShippingTotal]
  ,[Sequence]
  ,[IsManualTotal]
  ,[Greeting]
  ,[Tax]
  ,[SubTotal]
  ,[TotalDiscount]
  ,[ShippingCharge]
  ,[Status]
  ,[CreatedBy]
  ,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate
  ,[ModifiedBy]
  ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
  ,[ShippingDiscount]
  ,ShipmentHash
  ,IsBackOrder
  ,IsHandlingIncluded
 FROM OROrderShipping OS where [Id]=@Id

 SELECT O.* FROM ORShippingOption O
 INNER JOIN OROrderShipping OS ON OS.ShippingOptionId =O.Id
 where OS.[Id]=@Id

 SELECT S.* 
 FROM ORShippingOption O
 INNER Join ORShipper S on O.ShipperId = S.Id
 INNER JOIN OROrderShipping OS ON OS.ShippingOptionId =O.Id
 where OS.[Id]=@Id
 
 
SELECT [Id]
      ,[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(ShippedDate,@ApplicationId)ShippedDate
      ,[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(CreateDate,@ApplicationId)CreateDate
      ,[CreatedBy]
      ,[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
   ,[EstimatedShipmentTotal]
   ,ShipmentStatus
   ,WarehouseId
 ,ExternalReferenceNumber
FROM [dbo].[FFOrderShipment]
Where OrderShippingId =@Id
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
Where OrderShippingId =@Id


SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,SP.[ShippingContainerId]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
Where OrderShippingId =@Id

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetPasswordWithFormat]'
GO
/*****************************************************
* Name : Membership_GetPasswordWithFormat
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetPasswordWithFormat]
--********************************************************************************
-- PARAMETERS

	@ApplicationId							uniqueidentifier,
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit

--********************************************************************************

AS
BEGIN

--********************************************************************************
-- Variable declaration
--********************************************************************************

	DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    DECLARE @CurrentTimeUtc						datetime
	SET @CurrentTimeUtc		 =GetUTCDate()
					
	SELECT  @UserId          = NULL

	DECLARE @DeletedSatus INT
	SET @DeletedSatus = dbo.GetDeleteStatus()
	
    SELECT  @UserId = u.Id, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s
    WHERE   
			--s.SiteId = @ApplicationId AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
            u.Id = m.UserId AND s.UserId = u.Id AND
            LOWER(@UserName) = u.LoweredUserName AND u.Status <> @DeletedSatus	

    --User not found
	IF (@UserId IS NULL)
	BEGIN
        RAISERROR ('NOTEXISTS||%s', 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

    --User is locked out
	IF (@IsLockedOut = 1)
	BEGIN
        RAISERROR ('USERLOCKED||%s', 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	DECLARE @CNT int
	SET @CNT = 0
	
	SELECT @CNT = COUNT(*) 
	FROM USUser
	WHERE	Id = @UserId and 	(Isnull(ExpiryDate,'12/31/9999')  <GETUTCDATE())
	
	IF (@CNT > 0)
	BEGIN
		RAISERROR ('USEREXPIRED||%s', 16, 1, @UserName)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
				
    SELECT   @Password Password, @PasswordFormat PasswordFormat, @PasswordSalt PasswordSalt, @FailedPasswordAttemptCount FailedAttemptCount,
             @FailedPasswordAnswerAttemptCount FailedAnswerAttemptCount, @IsApproved IsApproved, @LastLoginDate LastLoginDate, @LastActivityDate LastActivityDate

    --Update based on the 
	IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.USMembership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId        

        UPDATE  dbo.USUser
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = Id
    END


    RETURN 0
END

--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_Save]'
GO
/*****************************************************
* Name : Customer_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Customer_Save]
(	
	@ApplicationId							uniqueidentifier,
	@Id                                 uniqueidentifier,
	@FirstName								nvarchar(255),
	@LastName								nvarchar(255),
	@MiddleName								nvarchar(255) =null,
	@CompanyName							nvarchar(255),
	@BirthDate								DateTime =null,
	@Gender									nvarchar(50)=null,
	@IsBadCustomer							bit=null,
	@IsActive								bit=null,
	@IsOnMailingList						bit=null,
	@Status									int,
	@CSRSecurityQuestion					nvarchar(max)=null,
	@CSRSecurityPassword					varbinary(max) =null,
	@HomePhone								nvarchar(50)=null,
	@MobilePhone							nvarchar(50)=null,
	@OtherPhone								nvarchar(50)=null,
	@ImageId								uniqueidentifier=null,
	@AccountNumber							nvarchar(255)=null,
	@IsExpressCustomer						bit=null,
	@AddedAttributes						xml=null,
	@ExternalProfileId						nvarchar(255)=null,
	@ExternalId								nvarchar(255)
)

AS
BEGIN     
	
	Declare @createdBy uniqueidentifier
	
	IF (NOT EXISTS(SELECT 1 FROM  USCommerceUserProfile WHERE  Id = @Id))
	begin
		INSERT INTO USCommerceUserProfile
           ([Id]
           ,[IsBadCustomer]
           ,[IsActive]
           ,[IsOnMailingList]
		   ,[IsExpressCustomer]
           ,[Status]           
           ,[CSRSecurityQuestion]
           ,[CSRSecurityPassword]
		   ,[AccountNumber]
		   ,[ExternalProfileId] 
		   ,[ExternalId] 
		   )
     VALUES
           (@Id
           ,@IsBadCustomer
           ,@IsActive
           ,@IsOnMailingList
			,@IsExpressCustomer
           ,dbo.GetActiveStatus()          
           ,@CSRSecurityQuestion
           ,@CSRSecurityPassword
		   ,@AccountNumber
		   ,@ExternalProfileId 
		   ,@ExternalId
		  )

		--Assign customer to everyone group
		declare @EveryOneGroupId uniqueidentifier, @CustomerSecurityLevelId int
	    Set @EveryOneGroupId= ( select top 1 Value from STSiteSetting SV, STSettingType ST where 
			SV.SettingTypeId=ST.Id and ST.Name='CommerceEveryOneGroupId' and SV.SiteId = @ApplicationId)
		Exec Group_AddUser @ApplicationId,@Id,'1', @EveryOneGroupId, Null
		
		SET @CustomerSecurityLevelId = (select  top 1 Value from STSiteSetting SV, STSettingType ST where 
			SV.SettingTypeId=ST.Id and ST.Name='CustomerSecurityLevelId' and SV.SiteId = @ApplicationId)
		INSERT INTO USUserSecurityLevel (UserId, SecurityLevelId, MemberType) Values(@Id,@CustomerSecurityLevelId,1)
		
		
		if @AddedAttributes is not null
			begin
				select @createdBy = CreatedBy from USUser where Id=@Id								 
				Insert into CSCustomerAttributeValue(Id, CustomerId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
				Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), getUTCDate(),@createdBy
				From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple') tab(col)

			end
	end
	else
	begin
		update USCommerceUserProfile
		set 
            IsBadCustomer = @IsBadCustomer,
            IsActive=@IsActive,
            IsOnMailingList=@IsOnMailingList,
            Status =@Status,           
            CSRSecurityQuestion = @CSRSecurityQuestion,
			CSRSecurityPassword=@CSRSecurityPassword,
			AccountNumber = @AccountNumber,
			IsExpressCustomer=@IsExpressCustomer,
			ExternalProfileId =@ExternalProfileId,
			ExternalId =@ExternalId,
			ModifiedDate = getUTCDate()
			where Id=@Id
			
			if @AddedAttributes is not null
				begin
					select @createdBy = IsNull(ModifiedBy, CreatedBy) from USUser where Id=@Id								 
	
					Delete From dbo.CSCustomerAttributeValue where CustomerId= @Id 
					
					Insert into CSCustomerAttributeValue(Id, CustomerId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
					Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), getUTCDate(),@createdBy
					From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple')
					 tab(col)
				end
	end
	
	-- delete user entry from marketier if exists
	if(OBJECT_ID ('USMarketierUserProfile','U') is not null)
	begin	
		delete from USMarketierUserProfile
		where UserId = @Id
	end
	

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetUserMembership]'
GO
/*****************************************************
* Name : Membership_GetUserMembership
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Membership_GetUserMembership @ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@ApplicationId=NULL,@UserId='798613EE-7B85-4628-A6CC-E17EE80C09C5',@RoleId=NULL,@ObjectTypeId=NULL,@ParentSiteId=NULL
--exec Membership_GetUserMembership @ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@ApplicationId=NULL,@UserId='4FCBE87A-E452-4041-9523-9DEFA9E0422D',@RoleId=NULL,@ObjectTypeId=NULL,@ParentSiteId=NULL, @OnlyParentSite = 1

ALTER PROCEDURE [dbo].[Membership_GetUserMembership]
(
	@ProductId		uniqueidentifier = NULL,
	@ApplicationId	uniqueidentifier = NULL, --This is for getting applicationIds for the user and role.
	@UserId			uniqueidentifier,
	@RoleId			int = NULL,
	@ObjectTypeId	int,
	@ParentSiteId   uniqueidentifier = NULL,
	@OnlyParentSite bit=null
	
)
AS
BEGIN

Declare @Local_ProductId		uniqueidentifier,
	@Local_ApplicationId	uniqueidentifier, --This is for getting applicationIds for the user and role.
	@Local_UserId			uniqueidentifier,
	@Local_RoleId			int,
	@Local_ObjectTypeId	int,
	@Local_ParentSiteId   uniqueidentifier,
	@Local_OnlyParentSite bit
	Declare @siteTable as SiteTableType 
	
	IF(@OnlyParentSite IS NULL)
		SET @OnlyParentSite = 0
		
	Set @Local_ProductId = @ProductId 
	Set @Local_ApplicationId = @ApplicationId
	Set	@Local_UserId= @UserId
	Set	@Local_RoleId = @RoleId
	Set	@Local_ObjectTypeId	= @ObjectTypeId
	Set	@Local_ParentSiteId = @ParentSiteId
	Set @Local_OnlyParentSite = @OnlyParentSite
	--If Role Id is null then get all the sites in which the given user is having membership.
	IF (@Local_RoleId IS NULL)
	BEGIN
	Declare @EmptyGuid Uniqueidentifier
	Set @EmptyGuid = '00000000-0000-0000-0000-000000000000'
	--IF (@ParentSiteId IS NULL)
	--	SET @ParentSiteId = @EmptyGuid
	
		INSERT INTO @siteTable
		SELECT	DISTINCT SiteId AS ObjectId
		FROM	dbo.USSiteUser US
		INNER JOIN SISite S ON US.SiteId = S.Id
		WHERE	UserId = @Local_UserId AND  
				ProductId = ISNULL(@Local_ProductId, ProductId)
				AND ((@Local_OnlyParentSite = 0 AND S.ParentSiteId = ISNULL(@Local_ParentSiteId, S.ParentSiteId)) 
				OR (@Local_OnlyParentSite = 1 AND S.ParentSiteId = @EmptyGuid))
				
			
			IF(@Local_OnlyParentSite = 0)
				SELECT * from [dbo].[GetVariantSitesHavingPermission](@siteTable)
			ELSE	
				SELECT  * from @siteTable
		
	END
	ELSE IF (@Local_RoleId IS NOT NULL AND @Local_UserId IS NOT NULL) --Gets all the sites in which the user is having the given role
	BEGIN
--		SELECT	DISTINCT ObjectId
--		FROM	dbo.USMemberRoles MR
--		WHERE	ApplicationId = ISNULL(@ApplicationId,ApplicationId)	AND
--				RoleId = @RoleId	AND
--				MemberId = @UserId
--		UNION
--
--		SELECT DISTINCT	ObjectId
--		FROM	dbo.USMemberGroup	MG, dbo.USMemberRoles MR
--		WHERE	--MR.ApplicationId = ISNULL(@ApplicationId,MR.ApplicationId)	AND
--				MR.RoleId = @RoleId	AND
--				MR.MemberId	= MG.GroupId
		INSERT INTO @siteTable
		SELECT DISTINCT SU.SiteId  AS ObjectId
		FROM	USMemberRoles MR,
				USMemberGroup MG, 
				USSiteUser SU 
		WHERE	RoleId = @Local_RoleId AND 
				MR.MemberId = MG.GroupId AND 
				MG.MemberId = SU.UserId	AND
				SU.UserId = @Local_UserId	AND
				MR.ObjectTypeId = ISNULL(@Local_ObjectTypeId,MR.ObjectTypeId) AND
				MG.ApplicationId = SU.SiteId  AND  
				SU.ProductId = ISNULL(@Local_ProductId, SU.ProductId)
				
			SELECT * from [dbo].[GetVariantSitesHavingPermission](@siteTable)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Post_IsPostNameAvailable]'
GO
/*****************************************************
* Name : Post_IsPostNameAvailable
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Stored procedure
ALTER PROCEDURE [dbo].[Post_IsPostNameAvailable]
(
	@BlogId   uniqueIdentifier=null ,
	@PostId   uniqueIdentifier=null ,
	@FriendlyName	nvarchar(256)=null
)
as
Begin
declare @Count int

SET @Count=0
	
	if(@PostId is null)
	SELECT @Count=count(*)	FROM BLPost where lower(FriendlyName) =lower(@FriendlyName) and BlogId=@BlogId AND Status <> dbo.GetDeleteStatus()

	else

	SELECT @Count=count(*)	FROM BLPost where lower(FriendlyName) =lower(@FriendlyName) and Id not in (@PostId) AND Status <> dbo.GetDeleteStatus()
	and BlogId=@BlogId


select @Count
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ImageStructure_MoveTree]'
GO
/*****************************************************
* Name : ImageStructure_MoveTree
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[ImageStructure_MoveTree] 
(
	@SourceNodeId 	[uniqueidentifier],  
	@DestinationNodeId 	[uniqueidentifier]
	
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Source_Lft bigint,
	@Source_Rgt bigint,
	@Destination_rgt bigint,		
	@ApplicationId uniqueidentifier,
	@DestApplicationId uniqueidentifier
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@SourceNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@SourceNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||SourceNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	IF (@DestinationNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@DestinationNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||DestinationNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	select @Source_Lft=LftValue,@Source_Rgt=RgtValue,@ApplicationId=SiteId
	from COContentStructure
	Where Id=@SourceNodeId
			
	Select @Destination_rgt=RgtValue,@DestApplicationId=SiteId 
	from COContentStructure 
	Where Id=@DestinationNodeId

	IF (@DestApplicationId != @ApplicationId)
	BEGIN
		RAISERROR('NOTSUPPORTED application1',16,1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF @Destination_rgt between @Source_Lft and @Source_Rgt
		BEGIN
			RAISERROR('NOTSUPPORTED application rgt',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	Update COContentStructure
		Set LftValue
		   = LftValue 
			+ case 
			when @Destination_rgt <@Source_Lft
			Then CASE

				When LftValue Between @Source_Lft and @Source_Rgt
				Then @Destination_rgt-@Source_Lft
				When LftValue Between @Destination_rgt
					And @Source_Lft - 1
				Then @Source_Rgt - @Source_Lft + 1
				Else 0 End
			When @Destination_rgt > @Source_Rgt
			Then CASE
				When LftValue Between @Source_Lft
					And @Source_Rgt
			Then @Destination_rgt - @Source_Rgt - 1
			
			When LftValue Between @Source_Rgt + 1
					And @Destination_rgt - 1
			Then @Source_Lft - @Source_Rgt - 1
			Else 0 End
			Else 0 End,
	RgtValue
	  =RgtValue
		+ Case
		  When @Destination_rgt < @Source_Lft
		  Then CASE
	
			When RgtValue Between @Source_Lft
				AND @Source_Rgt
			Then @Destination_rgt - @Source_Lft
			When RgtValue Between @Destination_rgt And @Source_Lft -1
			Then @Source_Rgt - @Source_Lft + 1
			Else 0 End
			When @Destination_rgt>@Source_Rgt
			THEN CASE
				When RgtValue Between @Source_Lft
					And @Source_Rgt
				Then @Destination_rgt - @Source_Rgt - 1
				When RgtValue Between @Source_Rgt + 1
					And @Destination_rgt - 1
				Then @Source_Lft - @Source_Rgt - 1
				Else 0 End
			Else 0 End,
	ParentId
	  =Case When Id=@SourceNodeId 
		Then @DestinationNodeId
		Else ParentId
		End
		Where SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||MoveNode',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
			--Update the ParentId of the Moved Node
		Update COContentStructure Set ParentId =@DestinationNodeId Where Id = @SourceNodeId
	AND SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||UpdateParentId',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[FileStructure_MoveTree]'
GO
/*****************************************************
* Name : FileStructure_MoveTree
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[FileStructure_MoveTree] 
(
	@SourceNodeId 	[uniqueidentifier],  
	@DestinationNodeId 	[uniqueidentifier]
	
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Source_Lft bigint,
	@Source_Rgt bigint,
	@Destination_rgt bigint,		
	@ApplicationId uniqueidentifier,
	@DestApplicationId uniqueidentifier
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@SourceNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@SourceNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||SourceNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	IF (@DestinationNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@DestinationNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||DestinationNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	select @Source_Lft=LftValue,@Source_Rgt=RgtValue,@ApplicationId=SiteId
	from COContentStructure
	Where Id=@SourceNodeId
			
	Select @Destination_rgt=RgtValue,@DestApplicationId=SiteId 
	from COContentStructure 
	Where Id=@DestinationNodeId

	IF (@DestApplicationId != @ApplicationId)
	BEGIN
		RAISERROR('NOTSUPPORTED application1',16,1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF @Destination_rgt between @Source_Lft and @Source_Rgt
		BEGIN
			RAISERROR('NOTSUPPORTED application rgt',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	Update COContentStructure
		Set LftValue
		   = LftValue 
			+ case 
			when @Destination_rgt <@Source_Lft
			Then CASE

				When LftValue Between @Source_Lft and @Source_Rgt
				Then @Destination_rgt-@Source_Lft
				When LftValue Between @Destination_rgt
					And @Source_Lft - 1
				Then @Source_Rgt - @Source_Lft + 1
				Else 0 End
			When @Destination_rgt > @Source_Rgt
			Then CASE
				When LftValue Between @Source_Lft
					And @Source_Rgt
			Then @Destination_rgt - @Source_Rgt - 1
			
			When LftValue Between @Source_Rgt + 1
					And @Destination_rgt - 1
			Then @Source_Lft - @Source_Rgt - 1
			Else 0 End
			Else 0 End,
	RgtValue
	  =RgtValue
		+ Case
		  When @Destination_rgt < @Source_Lft
		  Then CASE
	
			When RgtValue Between @Source_Lft
				AND @Source_Rgt
			Then @Destination_rgt - @Source_Lft
			When RgtValue Between @Destination_rgt And @Source_Lft -1
			Then @Source_Rgt - @Source_Lft + 1
			Else 0 End
			When @Destination_rgt>@Source_Rgt
			THEN CASE
				When RgtValue Between @Source_Lft
					And @Source_Rgt
				Then @Destination_rgt - @Source_Rgt - 1
				When RgtValue Between @Source_Rgt + 1
					And @Destination_rgt - 1
				Then @Source_Lft - @Source_Rgt - 1
				Else 0 End
			Else 0 End,
	ParentId
	  =Case When Id=@SourceNodeId 
		Then @DestinationNodeId
		Else ParentId
		End
		Where SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||MoveNode',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
			--Update the ParentId of the Moved Node
		Update COContentStructure Set ParentId =@DestinationNodeId Where Id = @SourceNodeId
	AND SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||UpdateParentId',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ContentStructure_MoveTree]'
GO
/*****************************************************
* Name : ContentStructure_MoveTree
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[ContentStructure_MoveTree] 
(
	@SourceNodeId 	[uniqueidentifier],  
	@DestinationNodeId 	[uniqueidentifier]
	
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Source_Lft bigint,
	@Source_Rgt bigint,
	@Destination_rgt bigint,		
	@ApplicationId uniqueidentifier,
	@DestApplicationId uniqueidentifier
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@SourceNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@SourceNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||SourceNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	IF (@DestinationNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@DestinationNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||DestinationNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	select @Source_Lft=LftValue,@Source_Rgt=RgtValue,@ApplicationId=SiteId
	from COContentStructure
	Where Id=@SourceNodeId
			
	Select @Destination_rgt=RgtValue,@DestApplicationId=SiteId 
	from COContentStructure 
	Where Id=@DestinationNodeId

	IF (@DestApplicationId != @ApplicationId)
	BEGIN
		RAISERROR('NOTSUPPORTED application1',16,1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF @Destination_rgt between @Source_Lft and @Source_Rgt
		BEGIN
			RAISERROR('NOTSUPPORTED application rgt',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	Update COContentStructure
		Set LftValue
		   = LftValue 
			+ case 
			when @Destination_rgt <@Source_Lft
			Then CASE

				When LftValue Between @Source_Lft and @Source_Rgt
				Then @Destination_rgt-@Source_Lft
				When LftValue Between @Destination_rgt
					And @Source_Lft - 1
				Then @Source_Rgt - @Source_Lft + 1
				Else 0 End
			When @Destination_rgt > @Source_Rgt
			Then CASE
				When LftValue Between @Source_Lft
					And @Source_Rgt
			Then @Destination_rgt - @Source_Rgt - 1
			
			When LftValue Between @Source_Rgt + 1
					And @Destination_rgt - 1
			Then @Source_Lft - @Source_Rgt - 1
			Else 0 End
			Else 0 End,
	RgtValue
	  =RgtValue
		+ Case
		  When @Destination_rgt < @Source_Lft
		  Then CASE
	
			When RgtValue Between @Source_Lft
				AND @Source_Rgt
			Then @Destination_rgt - @Source_Lft
			When RgtValue Between @Destination_rgt And @Source_Lft -1
			Then @Source_Rgt - @Source_Lft + 1
			Else 0 End
			When @Destination_rgt>@Source_Rgt
			THEN CASE
				When RgtValue Between @Source_Lft
					And @Source_Rgt
				Then @Destination_rgt - @Source_Rgt - 1
				When RgtValue Between @Source_Rgt + 1
					And @Destination_rgt - 1
				Then @Source_Lft - @Source_Rgt - 1
				Else 0 End
			Else 0 End,
	ParentId
	  =Case When Id=@SourceNodeId 
		Then @DestinationNodeId
		Else ParentId
		End
		Where SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||MoveNode',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
			--Update the ParentId of the Moved Node
		Update COContentStructure Set ParentId =@DestinationNodeId Where Id = @SourceNodeId
	AND SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||UpdateParentId',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultSharedDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultSharedDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultSharedDirectories]
(
	@SiteId			uniqueIdentifier,
	@ObjectType		int,
	@Level			nvarchar(4000),
	@UserId			uniqueIdentifier,
	@ParentId		uniqueIdentifier = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Lft bigint,  
	@Rgt bigint
BEGIN  
--********************************************************************************
-- code
--********************************************************************************
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId))
				And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  

	SELECT @Lft = LftValue , @Rgt = RgtValue from HSStructure   
		Where Id = @ParentId AND SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId))

	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (S.ParentId =@ParentId OR S.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,
			D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,
			D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		--AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
		--	(Select ObjectId from @objectRole) OR S.ParentId IN 
		--	(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id   
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.Id=@ParentId And D.Status != dbo.GetDeleteStatus() And D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId)  
		AND S.SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM HSStructure  C  Inner Join SISiteDirectory D ON C.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = C.Id  
		left outer join @objectCount FC ON FC.Id=C.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE C.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR C.Id=@ParentId)  
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
			  And C.LftValue Between M.LftValue and M.RgtValue  
			  And M.LftValue Not in (C.LftValue ,@Lft))  
				AND C. SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId) )  
		ORDER BY LftValue  
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultBlogDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultBlogDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultBlogDirectories]
(
      @SiteId		uniqueIdentifier,
      @Level        nvarchar(4000),
      @UserId       uniqueIdentifier,
      @ParentId		uniqueidentifier = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************
-- code
--********************************************************************************
	SET @ObjectType = 39
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId
				And (ObjectTypeId = @ObjectType OR ObjectTypeId = 0) 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from HSStructure   
		Where Id = @ParentId AND SiteId = @SiteId
		
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	DECLARE @IsSystemUser bit
	SET @IsSystemUser = (SELECT TOP 1 IsSystemUser FROM USSiteUser WHERE UserId = @UserId AND SiteId = @SiteId)
	IF @IsSystemUser IS NULL SET @IsSystemUser = 0
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			dbo.GetTotalBlogFiles(D.Id,@IsSystemUser) TotalFiles 
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (S.ParentId =@ParentId OR S.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId=@SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,
			D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,
			D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			dbo.GetTotalBlogFiles(D.Id,@IsSystemUser) TotalFiles 			
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)  
		AND S.SiteId=@SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			dbo.GetTotalBlogFiles(D.Id,@IsSystemUser) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id   
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE S.Id=@ParentId And D.Status != dbo.GetDeleteStatus() And D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId)  
		AND S.SiteId=@SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			dbo.GetTotalBlogFiles(D.Id,@IsSystemUser) TotalFiles 
		FROM HSStructure  C  Inner Join SISiteDirectory D ON C.Id = D.Id  
		left outer join  @objectRole R on R.ObjectId = C.Id  
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE C.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR C.Id=@ParentId)  
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
			  And C.LftValue Between M.LftValue and M.RgtValue  
			  And M.LftValue Not in (C.LftValue ,@Lft))  
				AND C. SiteId =@SiteId
		ORDER BY LftValue  
	END  

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultImageDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultImageDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultImageDirectories]
(
	@SiteId					uniqueIdentifier,  
	@Level					nvarchar(4000),  
	@UserId					uniqueIdentifier,
	@AccessibleNodesOnly	bit = 0,
	@onlySharedNode bit = 0,
	@ParentId				uniqueIdentifier = null
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 33
   
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COImageStructure   
		Where Id = @ParentId AND SiteId = @SiteId
 
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)   AllowAccessInChildrenSites
		FROM COImageStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			@ObjectType ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath, S.InheritSecurityLevels, S.PropagateSecurityLevels,
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure S 
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COImageStructure  D 
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
				And D.LftValue Between M.LftValue and M.RgtValue  
				And M.LftValue Not in (D.LftValue ,@Lft))  
				AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultFileDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultFileDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultFileDirectories]
(
 @SiteId				uniqueIdentifier,  
 @Level					nvarchar(4000),  
 @UserId				uniqueIdentifier,
 @AccessibleNodesOnly	bit = 0,
 @onlySharedNode bit = 0,
 @ParentId				uniqueIdentifier = null
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 9
   
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFileStructure   
		Where Id = @ParentId AND SiteId = @SiteId
 
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels, 
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFileStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <= -1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			@ObjectType ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath, S.InheritSecurityLevels, S.PropagateSecurityLevels,
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,isnull(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFileStructure S 
		left outer join  @objectRole R on R.ObjectId = S.Id  
		left outer join @objectCount FC ON FC.Id=S.Id
		left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFileStructure D
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath, D.InheritSecurityLevels, D.PropagateSecurityLevels,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,0 IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFileStructure  D 
		left outer join  @objectRole R on R.ObjectId = D.Id  
		left outer join @objectCount FC ON FC.Id=D.Id
		left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
		left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		  WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1   
			  From HSStructure M   
			  WHERE M.LftValue Between @Lft and @Rgt  
				And D.LftValue Between M.LftValue and M.RgtValue  
				And M.LftValue Not in (D.LftValue ,@Lft))  
				AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultContentDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultContentDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultContentDirectories]
(
 @SiteId				uniqueIdentifier,  
 @Level					nvarchar(4000),  
 @UserId				uniqueIdentifier,
 @AccessibleNodesOnly	bit = 0,
 @onlySharedNode bit = 0,
 @ParentId				uniqueIdentifier = null
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 7
	
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COContentStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,
			D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COContentStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId = @ParentId OR D.Id = @ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			7 ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath,  
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,S.IsMarketierDir,
			ISNULL(R.[Role],'') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,ISNULL(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites   
		FROM COContentStructure S 
			left outer join  @objectRole R on R.ObjectId = S.Id  
			left outer join @objectCount FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COContentStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,7 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites 
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COContentStructure  D 
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1 From COContentStructure M   
				WHERE M.LftValue Between @Lft and @Rgt  
					And D.LftValue Between M.LftValue and M.RgtValue  
					And M.LftValue Not in (D.LftValue ,@Lft))  
					AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDefaultDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultDirectories]
(
 @SiteId		uniqueIdentifier,  
 @ObjectType	int,  
 @Level			nvarchar(4000),  
 @UserId		uniqueIdentifier,
 @AccessibleNodesOnly bit = 0,
 @onlySharedNode bit = 0
 )  
AS  
BEGIN  
IF @ObjectType = 7 -- content    
  EXEC dbo.[SqlDirectoryProvider_GetDefaultContentDirectories] @SiteId, @Level, @UserId, @AccessibleNodesOnly, @onlySharedNode  
 ELSE IF @ObjectType = 9 -- file    
  EXEC dbo.[SqlDirectoryProvider_GetDefaultFileDirectories] @SiteId, @Level, @UserId, @AccessibleNodesOnly, @onlySharedNode  
 ELSE IF @ObjectType = 33 -- image    
  EXEC dbo.[SqlDirectoryProvider_GetDefaultImageDirectories] @SiteId, @Level, @UserId, @AccessibleNodesOnly, @onlySharedNode   
 ELSE IF @ObjectType = 38 -- Form     
  EXEC dbo.[SqlDirectoryProvider_GetDefaultFormDirectories] @SiteId, @Level, @UserId,@AccessibleNodesOnly, @onlySharedNode  
ELSE IF @ObjectType = 39 -- Blog 
		EXEC dbo.[SqlDirectoryProvider_GetDefaultBlogDirectories] @SiteId, @Level, @UserId
	ELSE -- Others
		EXEC dbo.[SqlDirectoryProvider_GetDefaultSharedDirectories] @SiteId, @ObjectType, @Level, @UserId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_Save]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_Save]
(
	@XmlString		xml,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier,
	@MicroSiteId	uniqueidentifier,
	@VirtualPath	nvarchar(max)  OUT,
	@SiteName		nvarchar(256) OUT
)
AS
BEGIN
	DECLARE @tbDirectory TYDirectory	
	INSERT INTO @tbDirectory(Id,
		Title,
		Description,
		Attributes,
		FolderIconPath,
		PhysicalPath,
		[Size],
		Status,
		ThumbnailImagePath,
		ObjectTypeId,
		ParentId,
		Lft,
		Rgt,
		IsSystem,
		IsMarketierDir,
		AllowAccessInChildrenSites,
		SiteId,
		MicrositeId)						
	SELECT  tab.col.value('@Id[1]','uniqueidentifier') Id,
		tab.col.value('@Title[1]','NVarchar(256)')Title,
		tab.col.value('@Description[1]','Nvarchar(1024)')Description,
		tab.col.value('@Attributes[1]','int')Attributes,
		tab.col.value('@FolderIconPath[1]','nvarchar(4000)')FolderIconPath,
		tab.col.value('@PhysicalPath[1]','nvarchar(4000)')PhysicalPath,
		tab.col.value('@Size[1]','bigint')[Size],
		tab.col.value('@Status[1]','int')Status,
		tab.col.value('@ThumbnailImagePath[1]','nvarchar(4000)')ThumbnailImagePath,
		tab.col.value('@ObjectTypeId[1]','int')ObjectTypeId,
		tab.col.value('@ParentId[1]','uniqueidentifier')ParentId,
		tab.col.value('@Lft[1]','bigint')Lft,
		tab.col.value('@Rgt[1]','bigint')Rgt,
		tab.col.value('@IsSystem[1]','bit')IsSystem,
		tab.col.value('@IsMarketierDir[1]','bit')IsMarketierDir,
		tab.col.value('@AllowAccessInChildrenSites[1]','bit') AllowAccessInChildrenSites,
		@ApplicationId,
		@MicroSiteId
	FROM @XmlString.nodes('/SiteDirectoryCollection/SiteDirectory') tab(col)
	Order By Lft

	-- if one of the node is content then everything is content, no mix and match
	DECLARE @ObjectTypeId int
	SET @ObjectTypeId = (SELECT TOP 1 ObjectTypeId FROM @tbDirectory)
	
	IF (@ObjectTypeId = 7)
		EXEC [dbo].[SqlDirectoryProvider_SaveContentDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 9)
		EXEC [dbo].[SqlDirectoryProvider_SaveFileDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 33)
		EXEC [dbo].[SqlDirectoryProvider_SaveImageDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE IF (@ObjectTypeId = 38)
		EXEC [dbo].[SqlDirectoryProvider_SaveFormDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	ELSE
	BEGIN
		-- All other libraries are shared
		DECLARE @MasterSiteId uniqueidentifier
		SET @MasterSiteId = (SELECT ParentSiteId FROM SISite WHERE Id = @ApplicationId)
		IF @MasterSiteId IS NOT NULL AND @MasterSiteId != dbo.GetEmptyGUID()
			SET @ApplicationId = @MasterSiteId
		EXEC [dbo].[SqlDirectoryProvider_SaveDirectory] @tbDirectory, @ParentId, @ApplicationId, @ModifiedBy
	END
	
	SET @VirtualPath = (SELECT Id, dbo.GetVirtualPathByObjectType(Id, @ObjectTypeId) VirtualPath, Attributes, SiteId 
							FROM @tbDirectory SISiteDirectory FOR XML Auto)
	
	SELECT @SiteName = Title FROM SISite WHERE Id = @ApplicationId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderPayment_Split]'
GO
/*****************************************************
* Name : OrderPayment_Split
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderPayment_Split]
(
	@OrderPaymentId	uniqueidentifier,
	@PaymentAmount money,
	@NewOrderPaymentId uniqueidentifier output
)
AS
BEGIN

--DECLARE @OrderPaymentId uniqueidentifier
--SET @OrderPaymentId = '358AFC04-82A2-4DCE-8DB0-0EC4AE54FACD'
--
--DECLARE @PaymentAmount money
--Set @PaymentAmount = 15

DECLARE @NewPaymentId uniqueidentifier
Set @NewPaymentId = newid()

--DECLARE @NewOrderPaymentId uniqueidentifier
Set @NewOrderPaymentId = newid()

DECLARE @NewPaymentCreditCardId uniqueidentifier
Set @NewPaymentCreditCardId = newid()

DECLARE @NewPaymentInfoId uniqueidentifier
Set @NewPaymentInfoId = newid()

DECLARE @NewBillingAddressId uniqueidentifier
Set @NewBillingAddressId = newid()


UPDATE 
	PTOrderPayment 
SET
	Amount = Amount - @PaymentAmount 
WHERE 
	Id = @OrderPaymentId

UPDATE 
	P 
SET 
	P.Amount = P.Amount - @PaymentAmount  
FROM 
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE 
	OP.Id = @OrderPaymentId




INSERT INTO PTPayment (
	Id,
	PaymentTypeId,
	PaymentStatusId,
	CustomerId,
	Amount,
	Description,
	FailureDate,
	FailureDescription,
	CreatedDate,
	CreatedBy,
	ClearDate,
	ProcessorTransactionId,
	AuthCode,
	Status,
	ModifiedDate,
	ModifiedBy,
	IsRefund
	
)

SELECT 
	@NewPaymentId,
	P.PaymentTypeId,
	1,--Enterned status
	P.CustomerId,
	@PaymentAmount,
	NULL,
	NULL,
	NULL,
	P.CreatedDate,
	P.CreatedBy,
	NULL,
	NULL,
	NULL,
	P.Status, 
	GetUTCDate(),
	P.ModifiedBy,
	P.IsRefund
	
 FROM 
	PTOrderPayment OP
	INNER JOIN PTPayment P ON P.Id = OP.PaymentId
	INNER JOIN PTPaymentCreditCard PCC On PCC.PaymentId = P.Id
WHERE 
	OP.Id = @OrderPaymentId



INSERT INTO PTOrderPayment (
	Id,
	OrderId,
	PaymentId,
	Amount,
	Sequence)
SELECT	
	@NewOrderPaymentId,
	OP.OrderId,
	@NewPaymentId,
	@PaymentAmount,
	1
FROM 
	PTOrderPayment OP
	INNER JOIN PTPayment P ON P.Id = OP.PaymentId
	INNER JOIN PTPaymentCreditCard PCC On PCC.PaymentId = P.Id
WHERE 
	OP.Id = @OrderPaymentId



INSERT INTO PTPaymentInfo(
	Id,
	CardType,
	Number,
	ExpirationMonth,
	ExpirationYear,
	NameOnCard,
	CreatedBy,
	CreatedDate,
	Status,
	ModifiedBy,
	ModifiedDate
)

SELECT 
	@NewPaymentInfoId,
	PIF.CardType,
	PIF.Number,
	PIF.ExpirationMonth,
	PIF.ExpirationYear,
	PIF.NameOnCard,
	PIF.CreatedBy,
	PIF.CreatedDate,
	PIF.Status,
	PIF.ModifiedBy,
	GetUTCDate()
FROM 
	PTOrderPayment OP
	INNER JOIN PTPayment P ON P.Id = OP.PaymentId
	INNER JOIN PTPaymentCreditCard PCC On PCC.PaymentId = P.Id
	INNER JOIN PTPaymentInfo PIF ON PIF.Id = PCC.PaymentInfoId
WHERE 
	OP.Id = @OrderPaymentId



INSERT INTO GLAddress(
	Id,
	FirstName,
	LastName,
	AddressType,
	AddressLine1,
	AddressLine2,
	AddressLine3,
	City,
	StateId,
	Zip,
	CountryId,
	Phone,
	CreatedDate,
	CreatedBy,
	ModifiedDate,
	ModifiedBy,
	Status,
	County
)

SELECT
	@NewBillingAddressId,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.Zip,
	A.CountryId,
	A.Phone,
	A.CreatedDate,
	A.CreatedBy,
	GetUTCDate(),
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	PTOrderPayment OP
	INNER JOIN PTPayment P ON P.Id = OP.PaymentId
	INNER JOIN PTPaymentCreditCard PCC On PCC.PaymentId = P.Id
	INNER JOIN GLAddress A ON A.Id = PCC.BillingAddressId
WHERE 
	OP.Id = @OrderPaymentId



INSERT INTO PTPaymentCreditCard (
	Id,
	PaymentInfoId,
	ParentPaymentInfoId,
	PaymentId,
	BillingAddressId,
	Phone
)
SELECT 
	@NewPaymentCreditCardId,
	@NewPaymentInfoId,
	PCC.ParentPaymentInfoId,
	@NewPaymentId,
	@NewBillingAddressId,
	PCC.Phone
 FROM 
	PTOrderPayment OP
	INNER JOIN PTPayment P ON P.Id = OP.PaymentId
	INNER JOIN PTPaymentCreditCard PCC On PCC.PaymentId = P.Id
WHERE 
	OP.Id = @OrderPaymentId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflows]'
GO
/*****************************************************
* Name : Workflow_GetWorkflows
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflows 
--          @WorkflowId = 'uniqueidentifier'  null,
--          @ObjectTypeId = 'int'  null,
--			@ApplicationId	= uniqueidentifier null ,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflows](
--********************************************************************************
-- Parameters
--********************************************************************************
			@WorkflowId		uniqueidentifier = null,
			@ObjectTypeId	int = null,
			@ApplicationId	uniqueidentifier = null,
			@ProductId		uniqueidentifier = null,
			@Title			nvarchar(512) = null,
			@PageIndex		int = null,
			@PageSize		int = null,
			@IsGlobal		bit = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint --,
			--@TotalRecords			bigint
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1;
	
		WITH ResultEntries AS
		(
            SELECT 
					ROW_NUMBER() OVER (ORDER BY A.Title) AS RowNumber,
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.IsGlobal,
					A.IsShared,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName
            FROM	dbo.WFWorkflow A
            LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
            WHERE	A.[Id]			=	Isnull(@WorkflowId,[Id])			AND 
					A.ObjectTypeId	=	Isnull(@ObjectTypeId,ObjectTypeId)	AND
					(@ApplicationId IS NULL OR (A.ApplicationId = @ApplicationId OR A.IsShared = 1 AND A.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, 1, 1)))) AND
					A.ProductId		=	Isnull(@ProductId, ProductId)		AND
					A.Title			=	Isnull(@Title, Title)			AND
					A.Status = dbo.GetActiveStatus()	AND
					A.IsGlobal = Isnull(@IsGlobal, A.IsGlobal)
		)

		SELECT * 
		FROM ResultEntries 
		WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))

--		SELECT  @TotalRecords = COUNT(*)
--		FROM    ResultEntries

		--RETURN @TotalRecords
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageMap_SavePageDefinition]'
GO
/*****************************************************
* Name : PageMap_SavePageDefinition
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[PageMap_SavePageDefinition]
(
	@Id					uniqueidentifier output,
	@SiteId				uniqueidentifier,
	@ParentNodeId		uniqueidentifier,
	@CreatedBy			uniqueidentifier,
	@ModifiedBy			uniqueidentifier,
	@PublishPage		bit,
	@PageDefinitionXml	xml,
	@ContainerXml		xml = null,
	@UnmanagedContentXml xml = null,
	@UpdatePageName bit = null
)
AS
BEGIN
	
	IF(@PublishPage = 1)
		SET @UpdatePageName = 1
		
	IF(@UpdatePageName IS NULL)
		SET @UpdatePageName = 0
	
		
	IF NOT EXISTS(Select 1 from PageMapNode Where PageMapNodeId = @ParentNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	DECLARE @pageDefinitionSegmentIds table(Id uniqueidentifier)	
	DECLARE @PublishCount int
	DECLARE @PublishDate datetime
	DECLARE @PageDefinitionSegmentId uniqueidentifier 
	
	IF @PublishPage = 1
		SET @PublishDate = GETUTCDATE()
	ELSE
		SET @PublishDate = @PageDefinitionXml.value('(pageDefinition/@publishDate)[1]','datetime')
		
	DECLARE @ActionType int
	SET @ActionType = 2
	IF @Id IS NULL OR (Select count(*) FROM PageDefinition Where PageDefinitionId=@Id)=0 -- New Page
	BEGIN
		SET @ActionType = 1
		-- get the highest value of the DisplayOrder currently in the DB for this menuId
		DECLARE @CurrentDisplayOrder int
		SELECT @CurrentDisplayOrder = ISNULL(MAX(DisplayOrder),0) FROM PageMapNodePageDef M WHERE M.PageMapNodeId = @ParentNodeId
		IF @CurrentDisplayOrder IS NULL
			SET @CurrentDisplayOrder = 1
		
		IF @PublishPage = 1
			SET @PublishCount = 1
		ELSE
			SET @PublishCount = 0
		IF @Id IS NULL 	
			SET @Id = NEWID()

		IF NOT EXISTS (Select 1 from PageDefinition Where PageDefinitionId =@Id)
			INSERT INTO PageDefinition(PageDefinitionId, SiteId, TemplateId, Title, Description, 
						PublishDate, PageStatus, WorkflowState, PublishCount, --ContainerXml,UnmanagedContentXml,
						FriendlyName, WorkflowId,
						CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
						OutputCacheProfileName, ExcludeFromSearch, IsDefault, IsTracked, HasForms, TextContentCounter, 
						CustomAttributes, SourcePageDefinitionId, IsInGroupPublish,ScheduledPublishDate,ScheduledArchiveDate,NextVersionToPublish)
			VALUES(@Id,  @SiteId, 
					@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)'), 
					@PublishDate,
					@PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
					@PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'),
					@PublishCount,
					--@ContainerXml,
					--isnull(@UnmanagedContentXml,'<unmanagedPageContents/>'),
					@PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)'),
					@PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
					@CreatedBy,
					GETUTCDATE(),
					@PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
					@PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(1000)'),
					@PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
					@PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
					dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
					@PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
					isnull(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
					@PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
					@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier')
				)
				
		IF NOT EXISTS (Select 1 from PageMapNodePageDef Where PageDefinitionId =@Id AND PageMapNodeId=@ParentNodeId)
			INSERT INTO PageMapNodePageDef(PageDefinitionId,PageMapNodeId,DisplayOrder)
			VALUES (@Id,@ParentNodeId,@CurrentDisplayOrder + 1)
			
			
		INSERT INTO PageDefinitionSegment(
			Id
			,PageDefinitionId
			,DeviceId
			,AudienceSegmentId
			,UnmanagedXml
			)
			 output inserted.Id into @pageDefinitionSegmentIds
		VALUES (
			NEWID()
			,@Id
			,dbo.GetDesktopDeviceId(@SiteId)
			,dbo.GetDesktopAudienceSegmentId(@SiteId)
			,isnull(@UnmanagedContentXml,'<unmanagedPageContents/>')
			)
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			INSERT INTO [PageDefinitionContainer]
				   (
					[PageDefinitionSegmentId]
				   ,[PageDefinitionId]
				   ,[ContainerId]
				   ,[ContentId]
				   ,[InWFContentId]
				   ,[ContentTypeId]
				   ,[IsModified]
				   ,[IsTracked]
				   ,[Visible]
				   ,[InWFVisible]
				   ,[DisplayOrder]
				   ,[InWFDisplayOrder]
				   ,[CustomAttributes]
				   ,ContainerName)
			 SELECT
					S.Id -- as this would be called only for desktop version, we can come in a better way
					,@Id
					,X.value('(@id)[1]','uniqueidentifier') as ContainerId
					,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
					,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
					,X.value('(@contentTypeId)[1]','int') as contentTypeId
					,X.value('(@isModified)[1]','bit') as isModified
					,X.value('(@isTracked)[1]','bit') as isTracked
					,X.value('(@visible)[1]','bit') as Visible
					,X.value('(@inWFVisible)[1]','bit') as InWFVisible
					,X.value('(@displayOrder)[1]','int') as DisplayOrder
					,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
					,dbo.GetCustomAttributeXml(X.query('.'),'container')
					,X.value('(@name)[1]','nvarchar(100)') as ContainerName
			From @ContainerXml.nodes('/Containers/container') temp(X)
			CROSS JOIN @pageDefinitionSegmentIds S
		END
	END
	ELSE
	BEGIN
		SELECT @PublishCount = PublishCount FROM PageDefinition WHERE PageDefinitionId = @Id
		IF @PublishPage = 1
			SELECT @PublishCount = @PublishCount + 1
			
		UPDATE PageDefinition Set 
			Title = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@title)[1]','nvarchar(max)') ELSE title END,
			TemplateId =@PageDefinitionXml.value('(pageDefinition/@templateId)[1]','uniqueidentifier'),
			description = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@description)[1]','nvarchar(max)') ELSE description END, 
			publishDate = @PublishDate, 
			PageStatus = @PageDefinitionXml.value('(pageDefinition/@status)[1]','int'), 
			WorkflowState = @PageDefinitionXml.value('(pageDefinition/@workflowState)[1]','int'), 
			PublishCount = @PublishCount,
			--ContainerXml = @containerXml,
			--UnmanagedContentXml = isnull(@unmanagedContentXml, UnmanagedContentXml),
			FriendlyName = CASE WHEN @UpdatePageName = 1 THEN @PageDefinitionXml.value('(pageDefinition/@friendlyName)[1]','nvarchar(max)') ELSE FriendlyName END,
			WorkflowId = @PageDefinitionXml.value('(pageDefinition/@workflowId)[1]','uniqueidentifier'),
			ModifiedBy = @ModifiedBy,
			ModifiedDate = GETUTCDATE(),
			ArchivedDate = @PageDefinitionXml.value('(pageDefinition/@archiveDate)[1]','datetime'),
			StatusChangedDate = @PageDefinitionXml.value('(pageDefinition/@statusChangedDate)[1]','datetime'),
			AuthorId = @PageDefinitionXml.value('(pageDefinition/@authorId)[1]','uniqueidentifier'),
			EnableOutputCache = @PageDefinitionXml.value('(pageDefinition/@EnableOutputCache)[1]','bit'),
			OutputCacheProfileName = @PageDefinitionXml.value('(pageDefinition/@OutputCacheProfileName)[1]','nvarchar(100)'),
			ExcludeFromSearch = @PageDefinitionXml.value('(pageDefinition/@ExcludeFromSearch)[1]','bit'),
			IsDefault = @PageDefinitionXml.value('(pageDefinition/@IsDefault)[1]','bit'),
			IsTracked = @PageDefinitionXml.value('(pageDefinition/@IsTracked)[1]','bit'),
			HasForms = @PageDefinitionXml.value('(pageDefinition/@HasForms)[1]','bit'),
			TextContentCounter = @PageDefinitionXml.value('(pageDefinition/@TextContentCounter)[1]','int'),
			--SourcePageDefinitionId = @PageDefinitionXml.value('(pageDefinition/@sourcePageDefinitionId)[1]','uniqueidentifier'),
			CustomAttributes = dbo.GetCustomAttributeXml(@PageDefinitionXml.query('.'), 'pagedefinition'),
			IsInGroupPublish  =ISNULL(@PageDefinitionXml.value('(pageDefinition/@isInGroupPublish)[1]','bit'),0),
			ScheduledPublishDate = @PageDefinitionXml.value('(pageDefinition/@scheduledPublishDate)[1]','datetime'),
			ScheduledArchiveDate = @PageDefinitionXml.value('(pageDefinition/@scheduledArchiveDate)[1]','datetime'),
			NextVersionToPublish =@PageDefinitionXml.value('(pageDefinition/@nextVersionToPublish)[1]','uniqueidentifier')
		WHERE PageDefinitionId = @Id AND SiteId = @SiteId
		
		DECLARE @DeskTopDeviceId uniqueidentifier
		DECLARE @DeskTopAudienceId uniqueidentifier
		SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@SiteId)
		SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@SiteId)
		
		SELECT @PageDefinitionSegmentId = Id 
			FROM PageDefinitionSegment WHERE DeviceId = @DeskTopDeviceId AND 
				AudienceSegmentId = @DeskTopAudienceId AND
				PageDefinitionId = @Id
		
		UPDATE PageDefinitionSegment Set UnmanagedXml = isnull(@unmanagedContentXml, UnmanagedXml)
		WHERE PageDefinitionId = @Id AND Id = @PageDefinitionSegmentId
		
		IF(@ContainerXml IS NOT NULL)
		BEGIN
			UPDATE [PageDefinitionContainer] SET 
			   [ContentId] = X.value('(@contentId)[1]','uniqueidentifier')
			  ,[InWFContentId] = X.value('(@inWFContentId)[1]','uniqueidentifier')
			  ,[ContentTypeId] = X.value('(@contentTypeId)[1]','int') 
			  ,[IsModified] = X.value('(@isModified)[1]','bit')
			  ,[IsTracked] = X.value('(@isTracked)[1]','bit')
			  ,[Visible] = X.value('(@visible)[1]','bit')
			  ,[InWFVisible] = X.value('(@inWFVisible)[1]','bit')
			  ,[CustomAttributes] = dbo.GetCustomAttributeXml(X.query('.'),'container')
			  ,[DisplayOrder] = X.value('(@displayOrder)[1]','int')
			  ,[InWFDisplayOrder] = X.value('(@inWFDisplayOrder)[1]','int')
			From @ContainerXml.nodes('/Containers/container') temp(X)
			WHERE [PageDefinitionId] = @Id 
			AND PageDefinitionSegmentId = @PageDefinitionSegmentId
			AND [ContainerId] = X.value('(@id)[1]','uniqueidentifier')
			--If any containers added to pagedefinition insert those(ex. marketier template switching)
			INSERT INTO [PageDefinitionContainer]
						   (
							[PageDefinitionSegmentId]
						   ,[PageDefinitionId]
						   ,[ContainerId]
						   ,[ContentId]
						   ,[InWFContentId]
						   ,[ContentTypeId]
						   ,[IsModified]
						   ,[IsTracked]
						   ,[Visible]
						   ,[InWFVisible]
						   ,[DisplayOrder]
						   ,[InWFDisplayOrder]
						   ,[CustomAttributes]
						   ,[ContainerName])
					 SELECT
							@PageDefinitionSegmentId
							,@Id
							,X.value('(@id)[1]','uniqueidentifier') as ContainerId
							,X.value('(@contentId)[1]','uniqueidentifier') as ContentId
							,X.value('(@inWFContentId)[1]','uniqueidentifier') as inWFContentId
							,X.value('(@contentTypeId)[1]','int') as contentTypeId
							,X.value('(@isModified)[1]','bit') as isModified
							,X.value('(@isTracked)[1]','bit') as isTracked
							,X.value('(@visible)[1]','bit') as Visible
							,X.value('(@inWFVisible)[1]','bit') as InWFVisible
							,X.value('(@displayOrder)[1]','int') as DisplayOrder
							,X.value('(@inWFDisplayOrder)[1]','int') as InWFDisplayOrder
							,dbo.GetCustomAttributeXml(X.query('.'),'container')
							,X.value('(@name)[1]','nvarchar(100)') as ContainerName
					From @ContainerXml.nodes('/Containers/container') temp(X)					
					where X.value('(@id)[1]','uniqueidentifier') not in (select ContainerId from PageDefinitionContainer where PageDefinitionSegmentId=@PageDefinitionSegmentId and PageDefinitionId=@Id)
		
		END
		
	END
	
	DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
	INSERT INTO USSecurityLevelObject 
		SELECT @Id, 8, SL.Id
	FROM dbo.SplitComma(@PageDefinitionXml.value('(pageDefinition/@securityLevels)[1]','nvarchar(max)'), ',') S 
		JOIN USSecurityLevel SL ON S.Value = SL.Id
	
	DELETE FROM SIPageStyle WHERE PageDefinitionId = @Id
	INSERT INTO SIPageStyle 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'style')
	FROM @PageDefinitionXml.nodes('/pageDefinition/style') T(C) JOIN SIStyle S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	DELETE FROM SIPageScript WHERE PageDefinitionId = @Id
	INSERT INTO SIPageScript 
		SELECT @Id, S.Id, dbo.GetCustomAttributeXml(T.C.query('.'), 'script')
	FROM @PageDefinitionXml.nodes('/pageDefinition/script') T(C) JOIN SIScript S ON
		T.C.value('(@id)[1]','uniqueidentifier') = S.Id
	
	EXEC [PageDefinition_AdjustDisplayOrder] @ParentNodeId

	EXEC PageMapBase_UpdateLastModification @SiteId
	EXEC PageMap_UpdateHistory @SiteId, @Id, 8, @ActionType

	EXEC [PageDefinition_UpdateAssetReference] @Id
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[HierarchyNode_CreateSite]'
GO
/*****************************************************
* Name : HierarchyNode_CreateSite
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[HierarchyNode_CreateSite] 
(
	 @SiteId  [uniqueidentifier],
	 @SiteTitle [nvarchar](256),
	 @ParentId [uniqueidentifier] = NULL OUT	
)
AS
Declare @reccount int ,
		@Error int,
		@RgtValue bigint,
		@L1Id uniqueidentifier,
		@L2Id uniqueidentifier,
		@ParentSiteId uniqueidentifier,
		@SiteDirectoryObjectTypeId int,
		@ModifiedBy	uniqueidentifier

BEGIN
	IF Exists (SELECT * FROM HSStructure Where Id = @SiteId )
	BEGIN
		RAISERROR('EXISTS||SiteId',16,1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	SET @SiteDirectoryObjectTypeId = dbo.GetSiteDirectoryObjectType()
	IF @ParentId IS NULL
		SET @ParentId = dbo.GetNetEditorId()
		
	SET @RgtValue = 1	    	    
	SELECT @ModifiedBy = CreatedBy FROM SISite Where Id = @SiteId
	SELECT @RgtValue = RgtValue FROM HSStructure WHERE Id = @ParentId
	IF @RgtValue IS NOT NULL
	BEGIN	    			
		
		
		-- Check to see if this is variant site. For Variants following are shared.
		SET @ParentSiteId = (SELECT ParentSiteId FROM SISite WHERE Id = @SiteId)
		IF (@ParentSiteId IS NULL OR @ParentSiteId = dbo.GetEmptyGUID())
		BEGIN
			-- START FILE LIBRARY
		SELECT @L1Id =newid()
		
		INSERT INTO COFileStructure
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
		Values(@L1Id,'File Library',null,@SiteId,1,4,@SiteId,1, @ModifiedBy, GETUTCDATE(),'~/File Library')
			
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@SiteId,9,@L1Id)
		
		SELECT @L2Id =newid()
						
		INSERT INTO COFileStructure
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
		Values(@L2Id,'Unassigned',null,@L1Id,2,3,@SiteId,1, @ModifiedBy, GETUTCDATE(),'~/File Library/Unassigned')
		-- END FILE LIBRARY
				
		-- START CONTENT LIBRARY
		SELECT @L1Id =newid()
			
		INSERT INTO COContentStructure 
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
		Values(@L1Id,'Content Library',null,@SiteId,1,4,@SiteId,1, @ModifiedBy, GETUTCDATE(),'~/Content Library')					
					
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@SiteId,7,@L1Id)

		SELECT @L2Id =newid()
						
		INSERT INTO COContentStructure
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
		Values(@L2Id,'Unassigned',null,@L1Id,2,3,@SiteId,1, @ModifiedBy, GETUTCDATE(),'~/Content Library/Unassigned')
		-- END CONTENT LIBRARY	
			
		-- START IMAGE LIBRARY	
		SELECT @L1Id =newid()
			
		INSERT INTO COImageStructure 
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath)
		Values(@L1Id,'Image Library',null,@SiteId,1,4,@SiteId,1, @ModifiedBy, GETUTCDATE(),'~/Image Library')

		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@SiteId,33,@L1Id)

		SELECT @L2Id =newid()
				
		INSERT INTO COImageStructure
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath)
		Values(@L2Id,'Unassigned',null,@L1Id,2,3,@SiteId,1, @ModifiedBy, GETUTCDATE(),'~/Image Library/Unassigned')
		-- END IMAGE LIBRARY
		
		-- START FORM LIBRARY	
		SELECT @L1Id =newid()
			
		INSERT INTO HSStructure
			(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L1Id,'Form Library',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+4,@SiteId)
				
		EXEC [dbo].[SiteDirectory_Save]
			@Id = @L1Id OUTPUT,
			@ApplicationId = @SiteId,
			@Title = N'Form Library',
			@Description = NULL,
			@ModifiedBy = @ModifiedBy,
			@ModifiedDate = NULL,
			@ObjectTypeId = 38,
			@PhysicalPath = NULL,
			@VirtualPath = N'~/Form Library'	
			
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@SiteId,38,@L1Id)

		SELECT @L2Id =newid()
		
		INSERT INTO HSStructure
			(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L2Id,'Unassigned',@SiteDirectoryObjectTypeId,null,@L1Id,@RgtValue+2,@RgtValue+3,@SiteId)
		
		EXEC [dbo].[SiteDirectory_Save]
			@Id = @L2Id OUTPUT,
			@ApplicationId = @SiteId,
			@Title = N'Unassigned',
			@Description = NULL,
			@ModifiedBy = @ModifiedBy,
			@ModifiedDate = NULL,
			@ObjectTypeId = 38,
			@PhysicalPath = NULL,
			@VirtualPath = N'~/Form Library/Unassigned'	
		
		SET @RgtValue = @RgtValue + 4	    	    
		-- END FORM LIBRARY					
											
			-- START CONTENT DEFINITION LIBRARY			
			SELECT @L1Id =newid()
				
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L1Id,'Content Definition Library',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+4,@SiteId)
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L1Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Content Definition Library',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 13,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Content Definition Library'	
				
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
			VALUES(@SiteId,13,@L1Id)

			SELECT @L2Id =newid()
					
			INSERT INTO HSStructure
			(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L2Id,'Unassigned',@SiteDirectoryObjectTypeId,null,@L1Id,@RgtValue+2,@RgtValue+3,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L2Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Unassigned',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 13,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Content Definition Library/Unassigned'		
			
			SET @RgtValue = @RgtValue + 4	    	    
			-- END CONTENT DEFINITION LIBRARY									
									
			-- START PAGE TEMPLATE LIBRARY					
			SELECT @L1Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L1Id,'Page Template Library',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+4,@SiteId)
					
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L1Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Page Template Library',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 3,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Page Template Library'	
				
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
			VALUES(@SiteId,3,@L1Id)

			SELECT @L2Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L2Id,'Unassigned',@SiteDirectoryObjectTypeId,null,@L1Id,@RgtValue+2,@RgtValue+3,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L2Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Unassigned',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 3,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Page Template Library/Unassigned'
			
			SET @RgtValue = @RgtValue + 4	    	    
			-- END PAGE TEMPLATE LIBRARY	
				
			-- START STYLE LIBRARY	
			SELECT @L1Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L1Id,'Style Library',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+4,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L1Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Style Library',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 4,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Style Library'	
						
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
			VALUES(@SiteId,4,@L1Id)

			SELECT @L2Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L2Id,'Unassigned',@SiteDirectoryObjectTypeId,null,@L1Id,@RgtValue+2,@RgtValue+3,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L2Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Unassigned',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 4,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Style Library/Unassigned'	
			
			SET @RgtValue = @RgtValue + 4	    	    
			-- END STYLE LIBRARY		
									
			-- START SCRIPT LIBRARY						
			SELECT @L1Id =newid()
				
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L1Id,'Script Library',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+4,@SiteId)
				
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L1Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Script Library',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 35,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Script Library'	
				
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
			VALUES(@SiteId,35,@L1Id)

			SELECT @L2Id =newid()
			
			INSERT INTO HSStructure
			(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L2Id,'Unassigned',@SiteDirectoryObjectTypeId,null,@L1Id,@RgtValue+2,@RgtValue+3,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L2Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Unassigned',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 35,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Script Library/Unassigned'
			
			SET @RgtValue = @RgtValue + 4	    	    
			-- END SCRIPT LIBRARY																															
				
			-- START VIDEO LIBRARY
			SELECT @L1Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L1Id,'Video Library',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+4,@SiteId)
				
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L1Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Video Library',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 48,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Video Library'	
					
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
			VALUES(@SiteId,48,@L1Id)

			SELECT @L2Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L2Id,'Unassigned',@SiteDirectoryObjectTypeId,null,@L1Id,@RgtValue+2,@RgtValue+3,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L2Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Unassigned',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 48,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Video Library/Unassigned'	
			
			SET @RgtValue = @RgtValue + 4	    	    
			-- END VIDEO LIBRARY
			
			-- START INDEX TERMS
			SELECT @L1Id =newid()
			
			INSERT INTO HSStructure
				(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
			Values(@L1Id,'Index Terms',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+2,@SiteId)
			
			EXEC [dbo].[SiteDirectory_Save]
				@Id = @L1Id OUTPUT,
				@ApplicationId = @SiteId,
				@Title = N'Index Terms',
				@Description = NULL,
				@ModifiedBy = @ModifiedBy,
				@ModifiedDate = NULL,
				@ObjectTypeId = 12,
				@PhysicalPath = NULL,
				@VirtualPath = N'~/Index Terms'	
				
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
			VALUES(@SiteId,12,@L1Id)
			
			SET @RgtValue = @RgtValue + 2	    	    
			-- END INDEX TERM		
		END
		-- START BLOG LIBRARY							
		SELECT @L1Id =newid()
			
		INSERT INTO HSStructure
			(Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@L1Id,'Blog',@SiteDirectoryObjectTypeId,null,@SiteId,@RgtValue+1,@RgtValue+2,@SiteId)

		EXEC [dbo].[SiteDirectory_Save]
			@Id = @L1Id OUTPUT,
			@ApplicationId = @SiteId,
			@Title = N'Blog',
			@Description = NULL,
			@ModifiedBy = @ModifiedBy,
			@ModifiedDate = NULL,
			@ObjectTypeId = 39,
			@PhysicalPath = NULL,
			@VirtualPath = N'~/Blog'	
				
		INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)
		VALUES(@SiteId,39,@L1Id)	
		
		SET @RgtValue = @RgtValue + 2	    	    
		-- END BLOG LIBRARY	
		
		-- START SITE 
		INSERT INTO HSStructure (Id,Title,ObjectTypeId,Keywords,ParentId,LftValue,RgtValue,SiteId)
		Values(@SiteId,@SiteTitle,dbo.GetSiteObjectType(),null,@ParentId,1,@RgtValue+1,@SiteId)
		
		EXEC [dbo].[SiteDirectory_Save]
			@Id = @SiteId OUTPUT,
			@ApplicationId = @SiteId,
			@Title = @SiteTitle,
			@Description = NULL,
			@ModifiedBy = @ModifiedBy,
			@ModifiedDate = NULL,
			@ObjectTypeId = 1,
			@PhysicalPath = NULL,
			@VirtualPath = N'~'
		--END SITE		
	END
	ELSE
	BEGIN 
		RAISERROR('NOTEXISTS||ProductId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	SELECT @Error = @@Error
	IF @Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,'Insert Site')
		RETURN dbo.GetDataBaseErrorCode()
	END

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Group_GetUserGroups]'
GO
/*****************************************************
* Name : Group_GetUserGroups
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Group_GetUserGroups]
--********************************************************************************
	@ProductId		uniqueidentifier = NULL,
	@ApplicationId	uniqueidentifier,
	@UserId			uniqueidentifier
--********************************************************************************
AS
BEGIN 
	SELECT DISTINCT	G.ProductId,
			G.ApplicationId,
			G.Id,
			Title,
			G.Description,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			Status,
			ExpiryDate,
			'' as CreatedByFullName,
			'' as ModifiedByFullName,
		--	dbo.User_GetUserFullName(CreatedBy) CreatedByFullName,
		--	dbo.User_GetUserFullName(ModifiedBy) ModifiedByFullName,
			G.GroupType
FROM		dbo.USGroup G,dbo.USMemberGroup MG, dbo.USRoles R
WHERE		MG.MemberId = @UserId	AND
			MG.MemberType = 1	AND
			G.Id = MG.GroupId	AND
			--MG.ApplicationId = @ApplicationId AND
			MG.ApplicationId IN (Select SiteId From dbo.GetAncestorSites(@ApplicationId)) AND
			G.ProductId = ISNULL(@ProductId,G.ProductId)
--			(
--				G.ApplicationId = @ApplicationId	
--					OR
--				(
--					G.ApplicationId = R.ApplicationId	AND
--					R.IsGlobal = 1
--				)
--			)
	
--	FROM	dbo.USGroup g INNER JOIN dbo.USMemberGroup mr
--			ON	mr.GroupId = g.Id	AND
--				(
--					mr.MemberId = @UserId	AND
--					mr.MemberType = 1
--				)	AND 
--				(
--					ApplicationId=ISNULL(@ApplicationId,ApplicationId)
--							
--					
--				)
--OR //Group Membership
--				(
--					mr.MemberId = @UserId AND
--					mr.GroupId in (	SELECT MemberId FROM dbo.USMemberGroup )
			
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_Rename]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_Rename
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[SqlDirectoryProvider_Rename] 
(
	@Id 				[uniqueidentifier],  
	@Name			 	[NVarchar](265),
	@ModifiedBy			[uniqueidentifier],
	@ObjectTypeId		[int]=NULL,
	@VirtualPath		[Nvarchar](max) OUTPUT,
	@SiteName			[NVarchar](256) OUTPUT
	
)
as

--********************************************************************************
-- Variable declaration
Declare @ApplicationId uniqueidentifier,@Lft bigint,@Rgt bigint
--********************************************************************************
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	
	if @ObjectTypeId is null
		Set @ObjectTypeId = (Select ObjectTypeId from vwDirectoryIds where Id=@Id)
		
	IF (@ObjectTypeId=7 OR @ObjectTypeId = 13)
	BEGIN
	IF EXISTS(Select * from COContentStructure 
	Where ParentId=(Select ParentId from HSStructure Where Id=@Id)
	And Id!=@Id	
	And LOWER(Title)=Lower(@Name))
	BEGIN
			RAISERROR('ISDUPLICATE||Title', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	END
		Update COContentStructure SET Title =@Name,ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath=dbo.GetVirtualPathByObjectType(@Id,@ObjectTypeId) 
		Where Id=@Id
		Select @Lft=LftValue,@Rgt=RgtValue,@ApplicationId=SiteId from COContentStructure Where Id=@Id

		Update COContentStructure SET ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath =dbo.GetVirtualPathByObjectType(Id,@ObjectTypeId)
		where LftValue Between @Lft and @Rgt AND SiteId=@ApplicationId

			SELECT @VirtualPath =VirtualPath,@ApplicationId=SiteId from COContentStructure Where Id = @Id
			Select @SiteName = Title From SISite Where Id=@ApplicationId
	END
	ELSE IF @ObjectTypeId=9
	BEGIN
		Update COFileStructure SET Title =@Name,ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath=dbo.GetVirtualPathByObjectType(@Id,@ObjectTypeId) 
		Where Id=@Id
		Select @Lft=LftValue,@Rgt=RgtValue,@ApplicationId=SiteId from COFileStructure Where Id=@Id

		Update COFileStructure SET ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath =dbo.GetVirtualPathByObjectType(Id,@ObjectTypeId)
		where LftValue Between @Lft and @Rgt AND SiteId=@ApplicationId

			SELECT @VirtualPath =VirtualPath,@ApplicationId=SiteId from COFileStructure Where Id = @Id
			Select @SiteName = Title From SISite Where Id=@ApplicationId
	END
	ELSE IF @ObjectTypeId=33
	BEGIN
		Update COImageStructure SET Title =@Name,ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath=dbo.GetVirtualPathByObjectType(@Id,@ObjectTypeId) 
		Where Id=@Id
		Select @Lft=LftValue,@Rgt=RgtValue,@ApplicationId=SiteId from COImageStructure Where Id=@Id

		Update COImageStructure SET ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath =dbo.GetVirtualPathByObjectType(Id,@ObjectTypeId)
		where LftValue Between @Lft and @Rgt AND SiteId=@ApplicationId

		SELECT @VirtualPath =VirtualPath,@ApplicationId=SiteId from COImageStructure Where Id = @Id
		Select @SiteName = Title From SISite Where Id=@ApplicationId
	END
	ELSE IF @ObjectTypeId=38
	BEGIN
		Update COFormStructure SET Title =@Name,ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath=dbo.GetVirtualPathByObjectType(@Id,@ObjectTypeId) 
		Where Id=@Id
		Select @Lft=LftValue,@Rgt=RgtValue,@ApplicationId=SiteId from COFormStructure Where Id=@Id

		Update COFormStructure SET ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath =dbo.GetVirtualPathByObjectType(Id,@ObjectTypeId)
		where LftValue Between @Lft and @Rgt AND SiteId=@ApplicationId

		SELECT @VirtualPath =VirtualPath,@ApplicationId=SiteId from COFormStructure Where Id = @Id
		Select @SiteName = Title From SISite Where Id=@ApplicationId
	END

	ELSE
	BEGIN
	IF EXISTS(Select * from HSStructure 
	Where ParentId=(Select ParentId from HSStructure Where Id=@Id)
	And Id!=@Id	
	And LOWER(Title)=Lower(@Name))
	BEGIN
			RAISERROR('ISDUPLICATE||Title', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	END
		Update HSStructure SET Title =@Name Where Id=@Id
		Update SISiteDirectory SET Title =@Name,ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath=dbo.GetVirtualPath(@Id) Where Id=@Id

		Select @Lft=LftValue,@Rgt=RgtValue,@ApplicationId=SiteId from HSStructure Where Id=@Id

		Update SISiteDirectory SET ModifiedBy=@ModifiedBy,ModifiedDate=getutcdate(),VirtualPath =dbo.GetVirtualPath(SISiteDirectory.Id)
		From HSStructure S Where LftValue Between @Lft and @Rgt And SISiteDirectory.Id =S.Id AND SiteId=@ApplicationId

			SELECT @VirtualPath =VirtualPath,@ApplicationId=ApplicationId from SISiteDirectory Where Id = @Id
			Select @SiteName = Title From SISite Where Id=@ApplicationId
	END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[AssetImage_SaveImage]'
GO
/*****************************************************
* Name : AssetImage_SaveImage
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[AssetImage_SaveImage] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id  uniqueidentifier,
	@FileName  nvarchar(256),
	@FileSize  int = null,
	@Extension  nvarchar(10),
	@FolderName  nvarchar(256) = null,
	@Attributes  nvarchar(256) = null,
	@RelativePath  nvarchar(4000) = null,
	@PhysicalPath  nvarchar(4000)  = null,
	@MimeType nvarchar(256) = null,
    @AltText nvarchar(1024) = null,
    @FileIcon nvarchar(1024)= null, 
    @DescriptiveMetadata nvarchar(4000)= null,
    @ImageHeight int = null,
    @ImageWidth int = null
)
as
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
	@Stmt 		VARCHAR(50),	
	@Error 		int

--********************************************************************************
-- code
--********************************************************************************
BEGIN

	IF @RelativePath IS NULL OR @RelativePath = ''
	BEGIN
		DECLARE @ParentId uniqueidentifier
		DECLARE @ObjectTypeId int
		SELECT @ParentId = ParentId, @ObjectTypeId = ObjectTypeId FROM COContent WHERE Id = @Id
		
		SET @RelativePath = dbo.GetVirtualPathByObjectType(@ParentId, @ObjectTypeId)
	END
	
    IF (EXISTS(SELECT 1 FROM COFile WHERE  ContentId = @Id))
	BEGIN			
		SET @Stmt = 'Update AssetImage'
		UPDATE COFile WITH (ROWLOCK)
		SET		[FileName] = @FileName,
				FileSize = @FileSize,
				Extension = @Extension,
				FolderName = @FolderName,
				Attributes = @Attributes,
				RelativePath = @RelativePath,
				PhysicalPath = @PhysicalPath, 
				MimeType = @MimeType,
				AltText = @AltText,
				FileIcon = @FileIcon, 
				DescriptiveMetadata = @DescriptiveMetadata,
				ImageHeight = @ImageHeight,
				ImageWidth = @ImageWidth	
		WHERE	ContentId = @Id
	END		
	ELSE
	BEGIN		
		SET @Stmt = 'Create Asset Image'
		INSERT INTO COFile(
				ContentId,
				FileName,
				FileSize,
				Extension,
				FolderName,
				Attributes,
				RelativePath,
				PhysicalPath, 
				MimeType,
				AltText,
				FileIcon, 
				DescriptiveMetadata,
				ImageHeight,
				ImageWidth
		)
		values
		(
			@Id,
			@FileName,
			@FileSize,
			@Extension,
			@FolderName,
			@Attributes,
			@RelativePath,
			@PhysicalPath,
			@MimeType,
			@AltText,
			@FileIcon, 
			@DescriptiveMetadata,
			@ImageHeight,
			@ImageWidth		
		)
	END

	SELECT @Error = @@ERROR
	IF @Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,@Stmt)
		RETURN dbo.GetDataBaseErrorCode()
	END
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Role_GetRole]'
GO
/*****************************************************
* Name : Role_GetRole
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Role_GetRole @ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@Id=NULL,@MemberId='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MemberType=1,@ObjectId=NULL,@ObjectTypeId=NULL,@OnlyGlobal=1,@ProductId=NULL
--exec Role_GetRole @ApplicationId='CDD38906-6071-4DD8-9FB6-508C38848C61',@Id=NULL,@MemberId='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MemberType=1,@ObjectId=NULL,@ObjectTypeId=NULL,@OnlyGlobal=1,@ProductId=NULL
--exec Role_GetRole @ApplicationId='CDD38906-6071-4DD8-9FB6-508C38848C61',@Id=NULL,@MemberId='798613EE-7B85-4628-A6CC-E17EE80C09C5',@MemberType=1,@ObjectId=NULL,@ObjectTypeId=NULL,@OnlyGlobal=1,@ProductId=NULL

ALTER PROCEDURE [dbo].[Role_GetRole]
--********************************************************************************
-- PARAMETERS
	@ApplicationId	uniqueidentifier,
	@Id				int,
	@MemberId		uniqueidentifier,
	@MemberType		smallint,
	@ObjectId		uniqueidentifier,
	@ObjectTypeId	int,
	@OnlyGlobal		bit,
	@ProductId		uniqueidentifier = NULL
--********************************************************************************

AS
BEGIN
	IF (@Id IS NOT NULL)
	BEGIN	
		SELECT  ApplicationId,
				Id,
				[Name],
				Description,
				IsGlobal
		FROM	dbo.USRoles
		WHERE	Id = @Id
		
		IF(@@ROWCOUNT = 0)
		BEGIN
			RAISERROR ('NOTEXISTS||Role', 16, 1)
			RETURN dbo.GetBusinessRulErrorCode()
		END
	END
	ELSE 
		SELECT * FROM 
			dbo.User_GetMemberRoles(@ApplicationId,@Id,@MemberId,
			@MemberType,@ObjectId,@ObjectTypeId,@OnlyGlobal,@ProductId) 
		WHERE (@MemberType = 2 OR ProductId IN (SELECT ProductId FROM USSiteUser WHERE SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND UserId = @MemberId))
		--WHERE (@MemberType = 2 OR ProductId IN (SELECT ProductId FROM USSiteUser WHERE SiteId = @ApplicationId AND UserId = @MemberId))
END
--********************************************************************************
GO


IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_RemoveOrderShipping]'
GO
ALTER PROCEDURE [dbo].[Order_RemoveOrderShipping]  
(   
 @OrderId uniqueidentifier,  
 @OrderShippingId uniqueidentifier
)  
  
AS  
BEGIN       
  DELETE FROM  OROrderItemAttributeValue where  OrderId=@OrderId and OrderItemId in (
		Select Id from OROrderItem WHERE OrderShippingId=@OrderShippingId)
-- delete from ordershipmentitem
DELETE FROM FFOrderShipmentItem WHERE OrderShipmentId IN (SELECT Id FROM FFOrderShipment WHERE OrderShippingId = @OrderShippingId)
-- delete from FFOrderShipmentPackage
DELETE FROM FFOrderShipmentPackage WHERE OrderShipmentId IN (SELECT Id FROM FFOrderShipment WHERE OrderShippingId = @OrderShippingId)
-- delete from ordershipment		
DELETE FROM FFOrderShipment WHERE OrderShippingId = @OrderShippingId
DELETE FROM OROrderItemCouponCode Where OrderItemId IN(SELECT Id FROM dbo.OROrderItem WHERE OrderShippingId=@OrderShippingId)
DELETE FROM OROrderItem WHERE OrderShippingId=@OrderShippingId;
DELETE FROM OROrderShipping WHERE OrderId=@OrderId and Id=@OrderShippingId;
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_Remove]'
GO
ALTER PROCEDURE [dbo].[Order_Remove]  
(  
 @OrderId uniqueidentifier 
)  
AS  
BEGIN  
DECLARE @CreateOrderStatusId int  
DECLARE @OrderStatus int

SELECT  
	@CreateOrderStatusId = Id 
FROM 
	OROrderStatus 
WHERE 
	[Name] = 'Created'

SELECT 
	@OrderStatus = OrderStatusId 
FROM 
	OROrder 
WHERE 
	Id = @OrderId

if @CreateOrderStatusId <> @OrderStatus
	begin
		raiserror('DBERROR||%s',16,1,'Order must be in created status to delete')
		return dbo.GetDataBaseErrorCode()	
	end

DELETE FROM OROrderItemAttributeValue where  OrderId=@OrderId
DELETE FROM dbo.OROrderAttributeValue where  OrderId=@OrderId

Declare @tmp table(OrderShipmentId uniqueidentifier)
Insert into @tmp
SELECT Id FROM FFOrderShipment WHERE OrderShippingId in(Select Id FROM dbo.OROrderShipping WHERE OrderId = @OrderId)
IF (select count(*) from @tmp) >0
BEGIN
	DELETE FROM FFOrderShipmentItem WHERE OrderShipmentId IN (Select OrderShipmentId from @tmp)
	DELETE FROM FFOrderShipmentPackage WHERE OrderShipmentId IN (Select OrderShipmentId from @tmp)
END
DELETE FROM OROrderItemCouponCode Where OrderItemId IN(SELECT Id FROM dbo.OROrderItem WHERE OrderId = @OrderId)
DELETE FROM dbo.OROrderItem WHERE 	OrderId = @OrderId
DELETE FROM FFOrderShipment WHERE OrderShippingId in(Select Id FROM dbo.OROrderShipping WHERE OrderId = @OrderId)
DELETE FROM dbo.OROrderShipping WHERE OrderId = @OrderId
Delete from PTOrderPayment Where OrderId=@OrderId
DELETE FROM OROrder WHERE 	Id = @OrderId

END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShipping_RemoveOrderItem]'
GO
ALTER PROCEDURE [dbo].[OrderShipping_RemoveOrderItem]  
(   
 @OrderShippingId uniqueidentifier,  
 @OrderItemId uniqueidentifier
)  
  
AS  
BEGIN       
	DELETE FROM  OROrderItemAttributeValue where  OrderItemId=@OrderItemId 
	DELETE FROM dbo.FFOrderShipmentItem WHERE OrderItemId IN( SELECT Id FROM dbo.OROrderItem WHERE ParentOrderItemId =@OrderItemId)
	DELETE FROM FFOrderShipmentItem WHERE OrderItemId=@OrderItemId 
	DELETE FROM OROrderItemCouponCode Where OrderItemId =@OrderItemId
	DELETE FROM OROrderItem WHERE OrderShippingId=@OrderShippingId and (Id=@OrderItemId or ParentOrderItemId = @OrderItemId);

END
  

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_GetOrders]'
GO
/*****************************************************
* Name : Order_GetOrders
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Order_GetOrders](@OrderStatusId int,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

Declare @tblOrders table
(
Id    uniqueidentifier
,CartId uniqueidentifier
,OrderTypeId    INT
,OrderStatusId    int
,PurchaseOrderNumber nvarchar(100)
,ExternalOrderNumber    nvarchar(100)
,SiteId    uniqueidentifier
,CustomerId     uniqueidentifier
,TotalTax    money
,OrderTotal   money
,CODCharges money
,TaxableOrderTotal   money
,GrandTotal   money
,TotalShippingCharge money
,TotalDiscount    money
,PaymentTotal    money
,RefundTotal money
,PaymentRemaining    money
,OrderDate    dateTime
,CreatedDate  dateTime  
,CreatedBy    uniqueidentifier
,CreatedByFullName nvarchar(3074)
,ModifiedDate   dateTime 
,ModifiedBy uniqueidentifier
,TotalShippingDiscount  money
);

with cteOrders
AS
(
SELECT     
 O.[Id],    
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy
WHERE
O.SiteId = @ApplicationId AND
O.OrderStatusId = Isnull(@OrderStatusId, O.OrderStatusId)
)


Insert into @tblOrders
SELECT * FROM cteOrders 

SELECT * FROM @tblOrders
Order by OrderDate;

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId IN (SELECT Id FROM @tblOrders) AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.Tax,
	OI.Discount
FROM OROrderItem OI
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
WHERE OI.OrderId IN (SELECT Id FROM @tblOrders);

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder
FROM OROrderShipping OS
WHERE OS.OrderId IN (SELECT Id FROM @tblOrders);

select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  P.OrderId 
from PRProductAttribute PA  
Inner join 
	(Select ProductId, OI.OrderId  FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId IN (SELECT Id FROM @tblOrders)
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[AttributeGroupId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId IN (SELECT Id FROM @tblOrders)

--- order shipments
SELECT F.[Id]
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	  ,OS.OrderId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
      ,OS.OrderId
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)



SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
      ,OS.OrderId
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId IN (SELECT Id FROM @tblOrders)

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDirectories]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDirectories]    
(    
	@DirectoryId	uniqueIdentifier,    
	@ApplicationId	uniqueidentifier,    
	@ObjectType		int,    
	@Level			nvarchar(4000),    
	@UserId			uniqueIdentifier
 )    
AS    
BEGIN  
	IF @ObjectType IS NULL
		SET @ObjectType =(Select top 1 ObjectTypeId from VWDirectoryIds Where Id = @DirectoryId)

	IF @ObjectType = 7 -- content
	  EXEC dbo.[SqlDirectoryProvider_GetDefaultContentDirectories] @ApplicationId, @Level, @UserId, 0,0, @DirectoryId    
	ELSE IF @ObjectType = 9 -- file    
	  EXEC dbo.[SqlDirectoryProvider_GetDefaultFileDirectories] @ApplicationId, @Level, @UserId, 0,0, @DirectoryId    
	ELSE IF @ObjectType = 33 -- image    
	  EXEC dbo.[SqlDirectoryProvider_GetDefaultImageDirectories] @ApplicationId, @Level, @UserId, 0,0, @DirectoryId    
	ELSE IF @ObjectType = 38 -- Form     
	  EXEC dbo.[SqlDirectoryProvider_GetDefaultFormDirectories] @ApplicationId, @Level, @UserId,0,0, @DirectoryId 
	ELSE IF @ObjectType = 39 -- Blog 
		EXEC dbo.[SqlDirectoryProvider_GetDefaultBlogDirectories] @ApplicationId, @Level, @UserId, @DirectoryId
	ELSE -- Others
		EXEC dbo.[SqlDirectoryProvider_GetDefaultSharedDirectories] @ApplicationId, @ObjectType, @Level, @UserId, @DirectoryId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageDefinition_SavePageDefinitionSegment]'
GO
/*****************************************************
* Name : PageDefinition_SavePageDefinitionSegment
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[PageDefinition_SavePageDefinitionSegment]
(
	@PageDefinitionId   uniqueidentifier,
	@ApplicationId		uniqueidentifier
	
)
AS
BEGIN
	DECLARE @ContainerContentXml XML
	DECLARE @PageDefinitionSegmentId uniqueidentifier
	
	IF NOT EXISTS(Select 1 from PageDefinition Where PageDefinitionId = @PageDefinitionId and SiteId = @ApplicationId)
	BEGIN
		RAISERROR('NOTEXISTS||PageDefinitionId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	DECLARE @DeskTopDeviceId uniqueidentifier
	DECLARE @DeskTopAudienceId uniqueidentifier
	SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@ApplicationId)
	SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@ApplicationId)
	
	DECLARE @VersionId uniqueidentifier
	SELECT TOP (1) @VersionId = Id  from VEVersion A
	WHERE ObjectId=@PageDefinitionId and ObjectTypeId=8 and ApplicationId = @ApplicationId
	ORDER BY RevisionNumber DESC, VersionNumber DESC	
	
	-- Insert into PageDefinitionSegment if there is no record for the pagedefintionSegment 
	--for that Device and Audience Segment, if its present update the record
	Declare @pageDefSegmentInsertIds table(Id uniqueidentifier)	
	Declare @pageDefSegmentDeleteIds table(Id uniqueidentifier)	
	Declare @pageDefSegmentIds table(Id uniqueidentifier)	
	--Getting the latest page definition segment version for all the device
	
	;With PageDefSegmentDeleteCTE AS
	(	
		SELECT [Id],DeviceID,AudienceSegmentId
			  ,ROW_NUMBER() over (Partition BY DeviceId, AudienceSegmentId Order by DeviceId, AudienceSegmentId) as pdfRank
		  FROM PageDefinitionSegment PS
		  WHERE PageDefinitionId = @PageDefinitionId
		  AND NOT EXISTS (SELECT Id from [VEPageDefSegmentVersion] VPS where 
		  VPS.PageDefinitionId = PS.PageDefinitionId AND 
		  VPS.DeviceID = PS.DeviceId AND
		  VPS.AudienceSegmentId = PS.AudienceSegmentId)
	)
	
	DELETE FROM PageDefinitionSegment WHERE Id IN(
	SELECT Id FROM PageDefSegmentDeleteCTE P1 where pdfRank = 1
	AND Id NOT IN (SELECT Id FROM PageDefSegmentDeleteCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId))
	
	
	;With PageDefSegmentCTE AS
	(	
		SELECT [Id],DeviceID,AudienceSegmentId
			  ,ROW_NUMBER() over (Partition BY DeviceId, AudienceSegmentId Order by MinorVersionNumber Desc) as pdfRank
		  FROM [VEPageDefSegmentVersion] VES
		  WHERE VersionId = @VersionId
		  AND NOT EXISTS (SELECT Id from PageDefinitionSegment PDFS where 
		  VES.PageDefinitionId = PDFS.PageDefinitionId AND 
		  VES.DeviceID = PDFS.DeviceId AND
		  VES.AudienceSegmentId = PDFS.AudienceSegmentId)
	)

	INSERT INTO @pageDefSegmentInsertIds  
	SELECT Id FROM PageDefSegmentCTE P1 where pdfRank = 1
	AND Id NOT IN (SELECT Id FROM PageDefSegmentCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId)
	
	--select * from @pageDefSegmentInsertIds
	-- @pageDefSegmentInsertIds will not have row which are already in PageDefinitionSegment table
	
	INSERT INTO PageDefinitionSegment
	SELECT NEWID()
		  ,VES.[PageDefinitionId]
		  ,VES.[DeviceId]
		  ,VES.[AudienceSegmentId]
		  ,VES.[UnmanagedXml]
	FROM [VEPageDefSegmentVersion] VES
	WHERE VES.Id IN (SELECT Id from @pageDefSegmentInsertIds)
	
	;With PageDefSegmentUpdateCTE AS
	(	
		SELECT [Id],DeviceID,AudienceSegmentId
			  ,ROW_NUMBER() over (Partition BY DeviceId, AudienceSegmentId Order by MinorVersionNumber Desc) as pdfRank
		  FROM [VEPageDefSegmentVersion] VES
		  WHERE VersionId = @VersionId
	)
	
	INSERT INTO @pageDefSegmentIds  
	SELECT Id FROM PageDefSegmentUpdateCTE P1 where pdfRank = 1
			AND Id NOT IN (SELECT Id FROM PageDefSegmentUpdateCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId)
			
	--select * from @pageDefSegmentIds		
	
	-- Update the PageDefintionSegment which is not inserted
	UPDATE PDFS Set
	PDFS.UnmanagedXml = VES.UnmanagedXml
	FROM PageDefinitionSegment PDFS
	INNER JOIN VEPageDefSegmentVersion VES ON PDFS.PageDefinitionId = VES.PageDefinitionId 
	AND PDFS.DeviceID = VES.DeviceID
	AND PDFS.AudienceSegmentId = VES.AudienceSegmentId
	AND VES.Id IN (SELECT Id from @pageDefSegmentIds)
	AND VES.Id NOT IN (SELECT Id from @pageDefSegmentInsertIds)
	
	--Insert in PageDefinitionContainer if there is no record for PageDefinitionSegmentId else update it
	--select * from @pageDefSegmentIds
				
	INSERT INTO [PageDefinitionContainer]
		   (
			[PageDefinitionSegmentId]
		   ,[PageDefinitionId]
		   ,[ContainerId]
		   ,[ContentId]
		   ,[InWFContentId]
		   ,[ContentTypeId]
		   ,[IsModified]
		   ,[IsTracked]
		   ,[Visible]
		   ,[InWFVisible]
		   ,[DisplayOrder]
		   ,[InWFDisplayOrder]
		   ,[CustomAttributes]
		   ,[ContainerName])
		Select Id,PageDefinitionId
			,P.n.value('@id','uniqueidentifier') as ContainerId
			,P.n.value('@contentId','uniqueidentifier') as ContentId
			,P.n.value('@inWFContentId','uniqueidentifier') as inWFContentId
			,P.n.value('@contentTypeId','int') as contentTypeId
			,P.n.value('@isModified','bit') as isModified
			,P.n.value('@isTracked','bit') as isTracked
			,P.n.value('@visible','bit') as Visible
			,P.n.value('@inWFVisible','bit') as InWFVisible
			,P.n.value('@displayOrder','int') as DisplayOrder
			,P.n.value('@inWFDisplayOrder','int') as InWFDisplayOrder
			,dbo.GetCustomAttributeXml(P.n.query('.'),'container') as CustomAttributes
			,P.n.value('@name','nvarchar(100)') as ContainerName
		FROM
		(
			SELECT PDFS.Id,PDFS.PageDefinitionId,VES.ContainerContentXML
			from VEPageDefSegmentVersion VES
			INNER JOIN PageDefinitionSegment PDFS ON PDFS.PageDefinitionId = VES.PageDefinitionId
			AND PDFS.DeviceId = VES.DeviceID
			AND PDFS.AudienceSegmentId = VES.AudienceSegmentId
			WHERE
			VES.Id IN (SELECT Id from @pageDefSegmentIds)
			AND PDFS.Id NOT IN (Select PageDefinitionSegmentId from PageDefinitionContainer)
		) X
		CROSS APPLY ContainerContentXML.nodes('/Containers/container') AS P(n)
		
		
		
		UPDATE PC Set  
		--Select 
		   [ContentId] = P.n.value('@contentId','uniqueidentifier')
		  ,[InWFContentId] = P.n.value('@inWFContentId','uniqueidentifier')
		  ,[ContentTypeId] = P.n.value('@contentTypeId','int') 
		  ,[IsModified] = P.n.value('@isModified','bit')
		  ,[IsTracked] = P.n.value('@isTracked','bit')
		  ,[Visible] = P.n.value('@visible','bit')
		  ,[InWFVisible] = P.n.value('@inWFVisible','bit')
		  ,[CustomAttributes] = dbo.GetCustomAttributeXml(P.n.query('.'),'container')
		  ,[DisplayOrder] = P.n.value('@displayOrder','int')
		  ,[InWFDisplayOrder] = P.n.value('@inWFDisplayOrder','int')
		  FROM 
		  (
			SELECT PDFS.Id,PDFS.PageDefinitionId,VES.ContainerContentXML from VEPageDefSegmentVersion VES
			INNER JOIN PageDefinitionSegment PDFS ON PDFS.PageDefinitionId = VES.PageDefinitionId
			AND PDFS.DeviceId = VES.DeviceID
			AND PDFS.AudienceSegmentId = VES.AudienceSegmentId
			WHERE
			VES.Id IN (SELECT Id from @pageDefSegmentIds)
			AND PDFS.Id NOT IN (Select Id from @pageDefSegmentInsertIds)
		  ) X
		CROSS APPLY ContainerContentXml.nodes('/Containers/container') P(n)
		INNER JOIN PageDefinitionContainer PC ON PC.PageDefinitionId = X.PageDefinitionId AND PC.PageDefinitionSegmentId = X.Id
		WHERE [ContainerId] = P.n.value('@id','uniqueidentifier')

		EXEC [PageDefinition_UpdateAssetReference] @PageDefinitionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[List_GetAutoList]'
GO
/*****************************************************
* Name : List_GetAutoList
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright � 2000-2006 Bridgeline Digital, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
-- Purpose: This Stored is used to retrieve the list information based on the input parameters  
-- Author: P. Jeevan  
-- Created Date:   
-- Created By:   
--********************************************************************************  
-- List of all the changes with date and reason  
--********************************************************************************  
-- Modified Date:  
-- Modified By:  
-- Reason:  
-- Changes:  
--********************************************************************************  
-- Warm-up Script:  
-- exec  dbo.List_GetAutoList 'uniqueidentifier'  
--********************************************************************************  
ALTER PROCEDURE [dbo].[List_GetAutoList](  
@SiteId uniqueidentifier,  
@ListId uniqueidentifier,  
@IsTemporary bit = 0  
--@Taxonomyxml xml = null,  
--@FindInChildren bit = 0  
)  
as  
Begin  
Declare  
@PublishDateType int,  
@FromPublishDate datetime, 
@ToPublishDate datetime,  
@DaysOlder int,  
@ListObjectType int,  
@OrderBy varchar(256),  
@OrderType int,  
@DirId UniqueIdentifier,  
@StrSelect nvarchar(max),  
@StrWhereClause varchar(max),  
@StrOrderClause varchar(max),  
@StrSql varchar(max),  
@Id uniqueidentifier,  
@Today datetime,  
@EmptyGuid uniqueidentifier  

set @Today=GETUTCDATE()  
set @StrWhereClause=''  
set @StrOrderClause=''  
set @EmptyGuid = @EmptyGuid;  


IF (@IsTemporary = 1)  
 select @PublishDateType=PublishDateType,@FromPublishDate=FromPublishDate,@ToPublishDate=ToPublishDate,@DaysOlder=DaysOlder,  
     @ListObjectType=ObjectTypeId,@OrderBy=OrderBy,@OrderType=OrderType FROM COTempList WHERE Id=@ListId and Status =dbo.GetActiveStatus()  
ELSE  
 select @PublishDateType=PublishDateType,@FromPublishDate=FromPublishDate,@ToPublishDate=ToPublishDate,@DaysOlder=DaysOlder,  
     @ListObjectType=ObjectTypeId,@OrderBy=OrderBy,@OrderType=OrderType FROM COList WHERE Id=@ListId and Status =dbo.GetActiveStatus()  

IF (@IsTemporary = 1)  
 select Id, Title, Description, CreatedDate, CreatedBy, ModifiedBy, ModifiedDate, ObjectTypeId,  
    ListType, MaxDisplayItems, OrderBy, OrderType, PublishDateType, FromPublishDate, ToPublishDate,  
    DaysOlder, ParentId,Status,
    FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
    from COTempList A
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
    where Id=@ListId 
    and Status =dbo.GetActiveStatus()  
ELSE  
 select Id, Title, Description, CreatedDate, CreatedBy, ModifiedBy, ModifiedDate, ObjectTypeId,  
    ListType, MaxDisplayItems, OrderBy, OrderType, PublishDateType, FromPublishDate, ToPublishDate,  
    DaysOlder, Status,
    FN.UserFullName CreatedByFullName,
	MN.UserFullName ModifiedByFullName
    from COList  A
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
     where Id=@ListId and Status =dbo.GetActiveStatus()  

select @DirId=ObjectId from GetObjectByRelation('L-H',@ListId)  

If(@ListObjectType=8 OR @ListObjectType=39)  
 Begin  
	If(@ListObjectType=8)
		exec  dbo.GetAutoPageList @SiteId,@ListId,@DirId,@PublishDateType,@FromPublishDate,@ToPublishDate,@DaysOlder,@OrderBy,@OrderType,null  
	else If(@ListObjectType=39)
		exec  dbo.GetAutoBlogList @SiteId,@ListId,@DirId,@PublishDateType,@FromPublishDate,@ToPublishDate,@DaysOlder,@OrderBy,@OrderType  
  
 End   
else  
Begin  
If(@ListObjectType=7) --'7' is ObjectType of Content  
 Begin  
  set @StrSelect = 'declare @Pagedef varchar(max),  
       @Taxonomyxml xml,  
       @Lft bigint,  
       @Rgt bigint,  
       @ActiveStatus int  
       declare  
       @Taxonomy_table table(  
       TaxonomyId uniqueidentifier,  
       ObjectId uniqueidentifier  
       )  
       set @Taxonomyxml  = (select ObjectId from GetObjectByRelation(''L-T'',''' + convert(varchar(36),@ListId) + ''') for XMl Auto, root)  
       set @ActiveStatus = dbo.GetActiveStatus()  
       if(convert(nvarchar(max),@Taxonomyxml) != '''')  
        Begin  
         insert into @Taxonomy_table Select  tab.col.value(''@ObjectId'',''uniqueidentifier'') TaxonomyId,T.ObjectId  
         from @Taxonomyxml.nodes(''/root/GetObjectByRelation'') tab(col)   
                inner join COTaxonomy H on tab.col.value(''@ObjectId'',''uniqueidentifier'')  = H.Id  
                INNER JOIN COTaxonomyObject T on H.Id = T.TaxonomyId
                
          Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
          C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
          C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,   
          FN.UserFullName CreatedByFullName,
		  MN.UserFullName ModifiedByFullName, 
          C.OrderNo, C.SourceContentId   
          from COContent C INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H   
          on C.ParentId=H.Id INNER JOIN @Taxonomy_table T on C.Id = T.ObjectId
          LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy 
			 where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and H.ObjectTypeId in (7,13) and C.Status=@ActiveStatus  
        End  
       Else  
          Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
          C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
          C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,   
          FN.UserFullName CreatedByFullName,
		  MN.UserFullName ModifiedByFullName,
           C.OrderNo, C.SourceContentId  
          from COContent C 
          INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H   
          on C.ParentId=H.Id 
          LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
			where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId in (7,13) and C.Status= @ActiveStatus
          '  
 End  
else If(@ListObjectType=9) --'9' is ObjectType of File  
 Begin  
  set @StrSelect = 'declare @Pagedef varchar(max),  
       @Taxonomyxml xml,  
       @Lft bigint,  
       @Rgt bigint,  
       @ActiveStatus int  
       declare  
       @Taxonomy_table table(  
       TaxonomyId uniqueidentifier,  
       ObjectId uniqueidentifier  
       )  
       set @Taxonomyxml  = (select ObjectId from GetObjectByRelation(''L-T'',''' + convert(varchar(36),@ListId) + ''') for XMl Auto, root)  
       set @ActiveStatus = dbo.GetActiveStatus()  
       if(convert(nvarchar(max),@Taxonomyxml) != '''')  
        Begin  
           insert into @Taxonomy_table Select  tab.col.value(''@ObjectId'',''uniqueidentifier'') TaxonomyId,T.ObjectId  
         from @Taxonomyxml.nodes(''/root/GetObjectByRelation'') tab(col)   
                inner join COTaxonomy H on tab.col.value(''@ObjectId'',''uniqueidentifier'')  = H.Id  
                INNER JOIN COTaxonomyObject T on H.Id = T.TaxonomyId
                Where T.ObjectTypeId=9
                      
           Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
           C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
           C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,   
           FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,  
           Asset.FileName, Asset.FileSize, Asset.Extension, Asset.FolderName, Asset.Attributes, Asset.RelativePath,   
           Asset.PhysicalPath, Asset.MimeType, Asset.AltText, Asset.FileIcon, Asset.DescriptiveMetadata, C.OrderNo, C.SourceContentId    
           from COContent C INNER JOIN COFile Asset on C.Id=Asset.ContentId   
           INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H    
           on C.ParentId=H.Id INNER JOIN @Taxonomy_table T on C.Id = T.ObjectId 
           LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
           where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId=9 and C.Status=@ActiveStatus  
          End  
       Else  
           Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
           C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
           C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,   
           FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,  
           Asset.FileName, Asset.FileSize, Asset.Extension, Asset.FolderName, Asset.Attributes, Asset.RelativePath,   
           Asset.PhysicalPath, Asset.MimeType, Asset.AltText, Asset.FileIcon, Asset.DescriptiveMetadata  , C.OrderNo, C.SourceContentId  
           from COContent C INNER JOIN COFile Asset on C.Id=Asset.ContentId   
           INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H    
           on C.ParentId=H.Id 
           LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
           where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId=9 and C.Status=@ActiveStatus'  
 End  
else If(@ListObjectType=33) --'33' is ObjectType of Image  
  Begin  
  set @StrSelect = 'declare @Pagedef varchar(max),  
       @Taxonomyxml xml,  
       @Lft bigint,  
       @Rgt bigint,  
       @ActiveStatus int  
       declare  
       @Taxonomy_table table(  
       TaxonomyId uniqueidentifier,  
       ObjectId uniqueidentifier  
       )  
       set @Taxonomyxml  = (select ObjectId from GetObjectByRelation(''L-T'',''' + convert(varchar(36),@ListId) + ''') for XMl Auto, root)  
       set @ActiveStatus = dbo.GetActiveStatus()  
       if(convert(nvarchar(max),@Taxonomyxml) != '''')  
        Begin  
        insert into @Taxonomy_table Select  tab.col.value(''@ObjectId'',''uniqueidentifier'') TaxonomyId,T.ObjectId  
         from @Taxonomyxml.nodes(''/root/GetObjectByRelation'') tab(col)   
                inner join COTaxonomy H on tab.col.value(''@ObjectId'',''uniqueidentifier'')  = H.Id  
                INNER JOIN COTaxonomyObject T on H.Id = T.TaxonomyId
                Where T.ObjectTypeId=33
                  
         Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
           C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
           C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,   
           FN.UserFullName CreatedByFullName,
		   MN.UserFullName ModifiedByFullName,
           Asset.FileName, Asset.FileSize, Asset.Extension, Asset.FolderName, Asset.Attributes, Asset.RelativePath,   
           Asset.PhysicalPath, Asset.MimeType, Asset.AltText, Asset.FileIcon, Asset.DescriptiveMetadata, Asset.ImageHeight, Asset.ImageWidth  , C.OrderNo , C.SourceContentId 
           from COContent C 
           INNER JOIN COFile Asset on C.Id=Asset.ContentId   
           INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H   
           on C.ParentId=H.Id INNER JOIN @Taxonomy_table T on C.Id = T.ObjectId 
           LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
           where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId=33 and C.Status=@ActiveStatus  
          End  
       Else  
         Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
           C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
           C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,   
           FN.UserFullName CreatedByFullName,
		  MN.UserFullName ModifiedByFullName,
          Asset.FileName, Asset.FileSize, Asset.Extension, Asset.FolderName, Asset.Attributes, Asset.RelativePath,   
           Asset.PhysicalPath, Asset.MimeType, Asset.AltText, Asset.FileIcon, Asset.DescriptiveMetadata, Asset.ImageHeight, Asset.ImageWidth  , C.OrderNo, C.SourceContentId  
           from COContent C INNER JOIN COFile Asset on C.Id=Asset.ContentId   
           INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H   
           on C.ParentId=H.Id 
           LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
           where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId=33 and C.Status=@ActiveStatus'  
 End  
else If(@ListObjectType=13) --'13' is ObjectType of XMLForm  
 Begin  
  set @StrSelect = 'declare @Pagedef varchar(max),  
       @Taxonomyxml xml,  
       @Lft bigint,  
       @Rgt bigint,  
       @ActiveStatus int  
       declare  
       @Taxonomy_table table(  
       TaxonomyId uniqueidentifier,  
       ObjectId uniqueidentifier  
       )  
       set @Taxonomyxml  = (select ObjectId from GetObjectByRelation(''L-T'',''' + convert(varchar(36),@ListId) + ''') for XMl Auto, root)  
       set @ActiveStatus = dbo.GetActiveStatus()  
       if(convert(nvarchar(max),@Taxonomyxml) != '''')  
        Begin  
           insert into @Taxonomy_table Select  tab.col.value(''@ObjectId'',''uniqueidentifier'') TaxonomyId,T.ObjectId  
         from @Taxonomyxml.nodes(''/root/GetObjectByRelation'') tab(col)   
                inner join COTaxonomy H on tab.col.value(''@ObjectId'',''uniqueidentifier'')  = H.Id  
                INNER JOIN COTaxonomyObject T on H.Id = T.TaxonomyId
                Where T.ObjectTypeId=13
          Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
            C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
            C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,  
            FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
            from COContent C INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H   
            on C.ParentId=H.Id INNER JOIN @Taxonomy_table T on C.Id = T.ObjectId 
            LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
            where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId=13 and C.Status=@ActiveStatus  
        End  
       Else  
          Select C.Id, C.ApplicationId, C.Title, C.Description, C.Text, C.URL, C.URLType, C.ObjectTypeId,   
            C.CreatedDate, C.CreatedBy, C.ModifiedBy, C.ModifiedDate, C.Status, C.RestoreDate,  C.BackupDate,   
            C.StatusChangedDate, C.PublishDate, C.ArchiveDate, C.XMLString, C.BinaryObject, C.Keywords,  
            FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName 
            from COContent C INNER JOIN GetChildrenByHierarchyType(''' + convert(varchar(36),isnull(@DirId,@EmptyGuid)) + ''',0,'+convert(varchar(5),@ListObjectType) + ') H   
            on C.ParentId=H.Id 
            LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
            where C.ApplicationId= ''' +CONVERT(varchar(36),@SiteId) +''' and  H.ObjectTypeId=13 and C.Status=@ActiveStatus'  
 End  
if(@PublishDateType=1) --PublishDateType '1' is FromDate to ToDate  
 Begin   
  set @StrWhereClause = ' And convert(varchar(10), CreatedDate,121) between '  
  if @FromPublishDate is null  
   set @StrWhereClause  =   @StrWhereClause  + 'C.CreatedDate '   
  else  
   set @StrWhereClause  =   @StrWhereClause  +  '''' + convert(varchar(10), @FromPublishDate,121)  
  if @ToPublishDate is null  
   set @StrWhereClause  =   @StrWhereClause  + ' And C.CreatedDate '  
  else  
   set @StrWhereClause  =   @StrWhereClause  + ''' And ''' + convert(varchar(10), @ToPublishDate,121) + ''''  
 End  
if(@PublishDateType=2) --PublishDateType '2' is from TodayDate to TodayDate-DaysOlder  
  Begin  
  set @StrWhereClause = ' And convert(varchar(10), CreatedDate,121) between ''' + convert(varchar(10), @Today- @DaysOlder,121) +  
          ''' and ''' +  convert(varchar(10), @Today,121) +  ''''  
  End  
 set @StrOrderClause = ''  
 If @OrderBy is null or @OrderBy = ''
	Set @OrderBy = 'OrderNo'
	
if (@OrderBy is not null and @OrderBy != '')  
  Begin   
 set @StrOrderClause = ' Order By '  
   if (@OrderType = 1) -- '1' is ascending  
   set @StrOrderClause = @StrOrderClause + convert(varchar(30),@OrderBy) + ' ' + 'asc'  
   else  
  set @StrOrderClause = @StrOrderClause + convert(varchar(30),@OrderBy) + ' ' + 'desc'  
    End  
set @StrSql = @StrSelect + @StrWhereClause + @StrOrderClause  
--print @StrSql  
EXEC(@StrSql)  
End  
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_ByPaypal_Save]'
GO
/*****************************************************
* Name : Payment_ByPaypal_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Payment_ByPaypal_Save]  
(   
 @Id uniqueidentifier output,
 @PaypalInfoId uniqueidentifier,	
 @OrderId uniqueidentifier,
 @PaymentTypeId int,
 @PaymentStatusId int =1,
 @Amount money,
 @CustomerId uniqueidentifier,
 @Token nvarchar(50)=null,
 @PaypalId nvarchar(255)=null,
 @Description nvarchar(255),
 @ModifiedBy uniqueidentifier  ,
 @BillingAddressId uniqueidentifier,
 @OrderPaymentId uniqueidentifier output,
 @IsRefund tinyint = 0
)  
  
AS  
BEGIN       
  
DECLARE @CreatedDate datetime
  set @CreatedDate = GetUTCDate()

Declare @error	  int
Declare @stmt	  varchar(256)	
Declare @sequence int
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
			  SET @Id = newid()  

				Insert into PTPayment 
				(Id
				,PaymentTypeId
				,PaymentStatusId
				,CustomerId
				,Amount
				,Description
				,CreatedDate
				,CreatedBy
				,Status
				,IsRefund
				)
				Values
				(
				@Id
				,@PaymentTypeId
				,@PaymentStatusId
				,@CustomerId
				,@Amount
				,@Description
				,@CreatedDate
				,@ModifiedBy
				,dbo.GetActiveStatus()
				,@IsRefund
			)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end


	if(@PaypalInfoId is null OR @PaypalInfoId = dbo.GetEmptyGUID())  
	begin  
			  SET @PaypalInfoId= newid()  

		INSERT INTO [dbo].[PTPaypalInfo]
				   ([Id]
				   ,[CustomerId]
				   ,[Token]
				   ,[PaypalId]
				   ,[Status]
				   ,[CreatedDate]
				   ,[CreatedBy]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
			 VALUES
				   (@PaypalInfoId
				   ,@CustomerId
				   ,@Token
				   ,@PaypalId
				   ,1
				   ,@CreatedDate
				   ,@ModifiedBy
				   ,@ModifiedBy
				   ,@CreatedDate)

		
				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end
		end

				Insert into PTPaymentPaypal
				(Id,
				 PaymentId,
				 PaypalInfoId
				)
				Values
				(newId(),
				  @Id,
				  @PaypalInfoId
				)

					select @error = @@error
							if @error <> 0
							begin
								raiserror('DBERROR||%s',16,1,@stmt)
								return dbo.GetDataBaseErrorCode()	
							end

				



				Select @sequence = max(isnull(Sequence,0)) + 1
				From PTOrderPayment
				Where OrderId=@OrderId


				SET @OrderPaymentId  = newId()
				Insert Into PTOrderPayment(Id,OrderId,PaymentId,Amount,Sequence)
				Values(@OrderPaymentId,@OrderId,@Id,@Amount,@sequence)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

	end  
ELSE
	BEGIN
		set @CreatedDate = GetUTCDate()

		UPDATE PTPayment SET 
		PaymentTypeId = @PaymentTypeId, 
		PaymentStatusId = @PaymentStatusId,
		CustomerId = @CustomerId,
		Amount = @Amount,
		Description = @Description,
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @CreatedDate,	
		Status = dbo.GetActiveStatus(),
		IsRefund = @IsRefund
		WHERE Id = @Id


		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end


		UPDATE PTOrderPayment SET Amount = @Amount
		WHERE Id= @OrderPaymentId
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end
		
	UPDATE [dbo].[PTPaypalInfo]
	   SET [CustomerId] = @CustomerId
		  ,[BillingAddressId] = @BillingAddressId
		  ,[Token] = @Token
		  ,[PaypalId] = @PaypalId
		  ,[Status] = dbo.GetActiveStatus()
		  ,[ModifiedBy] = @ModifiedBy
		  ,[ModifiedDate] = @CreatedDate
	 WHERE Id=@PaypalInfoId
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		END
END  

Update OROrder Set PaymentTotal =dbo.Order_GetPaymentTotal(@OrderId), RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
Where Id=@OrderId
  
  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageDefinition_GetPageVersion]'
GO
/*****************************************************
* Name : PageDefinition_GetPageVersion
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[PageDefinition_GetPageVersion]
--********************************************************************************
-- Input Output Parameters
--********************************************************************************
	@ApplicationId uniqueidentifier,
	@VersionId uniqueidentifier=null,
	@PageId  uniqueidentifier=null,
	@VersionNumber int=null,
	@RevisionNumber  int=null,
	@DeviceId uniqueidentifier = NULL,
	@AudienceSegmentId varchar(max)=null 
	
AS
 
BEGIN

	IF @VersionId IS NULL
		Select Top 1 @VersionId =Id
			FROM VEVersion Where ObjectId=@PageId AND VersionNumber=@VersionNumber AND RevisionNumber= @RevisionNumber

	IF @VersionId IS NULL
	BEGIN
		return;
	END

	IF(@PageId IS NULL)
		SELECT @PageId = ObjectId FROM VEVersion WHERE Id = @VersionId

	IF (@PageId IS NULL)
	BEGIN
			RAISERROR('DBERROR||%s',16,1,'Cannot find page')
				RETURN dbo.GetDataBaseErrorCode()	
	END

	-- Page Version
	SELECT TOP (1) Id, VersionNumber, ObjectTypeId, ObjectId, CreatedDate, 
				CreatedBy, RevisionNumber, XMLString, VersionStatus,ObjectStatus, Comments, Status, 
				FN.UserFullName CreatedByFullName,
				TriggeredObjectId, TriggeredObjectTypeId, ApplicationId, D.PageDetailXML
		FROM VEVersion A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN PageDetails D WITH(NOLOCK) on A.ObjectId=D.PageId
		WHERE (@ApplicationId IS NULL OR ApplicationId = @ApplicationId)
		AND Id=@VersionId AND (@PageId IS NULL OR ObjectId = @PageId)

	DECLARE @SegmentVersionId uniqueidentifier

	SELECT @SegmentVersionId = dbo.GetPageDefinitionVersionSegmentId(@PageId,@VersionId,@DeviceId,@AudienceSegmentId)

	-- UnManaged XML and Container Content Version
	SELECT [Id]
		  ,[VersionId]
		  ,[PageDefinitionId]
		  ,[DeviceId]
		  ,[AudienceSegmentId]
		  ,[ContainerContentXML]
		  ,[UnmanagedXml]
	  FROM [VEPageDefSegmentVersion]
	  WHERE Id = @SegmentVersionId
	  
	 	
	  -- Page Contents 
	SELECT CV.[Id]
		  ,[ContentId]
		  ,[ContentTypeId]
		  ,[XMLString]
		  ,[Status]
		  ,CV.[CreatedBy]
		  ,CV.[CreatedDate]
		  ,[ModifiedBy]
		  ,[ModifiedDate]
		  ,[ContentVersionId]
		  ,[IsChanged]
		  ,[PageDefSegmentVersionId]
	  FROM [VEContentVersion] CV 
	  INNER JOIN [VEPageContentVersion] PCV ON CV.Id = PCV.ContentVersionId
	  INNER JOIN [VEPageDefSegmentVersion] PDFV ON PDFV.Id = PCV.PageDefSegmentVersionId
	  WHERE PDFV.Id	= @SegmentVersionId
	  
	SELECT
		C.Id AS ContainerId,
		C.Title AS ContainerName
	FROM PageDefinition P
		JOIN SITemplateContainer TC ON TC.TemplateId = P.TemplateId
		JOIN SIContainer C ON C.Id = TC.ContainerId
	WHERE P.PageDefinitionId = @PageId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflow]'
GO
/*****************************************************
* Name : Workflow_GetWorkflow
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright � 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflow 
--          @Id = 'uniqueidentifier'  null,
--          @ObjectTypeId = 'int'  null,
--			@ApplicationId	= uniqueidentifier null ,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflow](
--********************************************************************************
-- Parameters
--********************************************************************************
			@Id				uniqueidentifier = null,
			@ObjectTypeId	int = null,
			@ApplicationId	uniqueidentifier = null,				
			@ProductId		uniqueidentifier = null,
			@PageIndex		int = null,
			@PageSize		int = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint --,
			--@TotalRecords			bigint
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1;
	
		WITH ResultEntries AS
		(
            SELECT 
					ROW_NUMBER() OVER (ORDER BY A.Title) AS RowNumber,
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.IsGlobal,
					A.IsShared
            FROM	dbo.WFWorkflow A
            WHERE	A.[Id]			=	Isnull(@Id,[Id])			AND 
					A.ObjectTypeId	=	Isnull(@ObjectTypeId, ObjectTypeId)	AND
					A.ApplicationId	=	Isnull(@ApplicationId, ApplicationId) AND
					A.ProductId		=	Isnull(@ProductId, ProductId) AND
					A.Status = dbo.GetActiveStatus()
		)

		SELECT *,FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))

--		SELECT  @TotalRecords = COUNT(*)
--		FROM    ResultEntries

		--RETURN @TotalRecords
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_GetCustomersInGroup]'
GO
/*****************************************************
* Name : Customer_GetCustomersInGroup
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Customer_GetCustomersInGroup]    
--********************************************************************************    
--PARAMETERS    
(    
 @ApplicationId  uniqueidentifier,    
 @GroupId   xml, --GroupId    
 @IsActive   bit = null,   
 @IsApproved  bit = 1,   
 @IsLockedOut  bit = 0,   
 @PageIndex    int,    
 @PageSize    int    
)    
--********************************************************************************    
AS    
BEGIN    
 -- Set the page bounds    
 DECLARE @IsSystemUser bit  
    DECLARE @PageLowerBound int    
    DECLARE @PageUpperBound int    
    DECLARE @TotalRecords   int    
 DECLARE @MemberType int  
   
 SET @IsSystemUser = 0  
 SET @MemberType = 1  
    SET @PageLowerBound = @PageSize * @PageIndex    
 IF (@PageIndex > 0)    
  SET @PageUpperBound = @PageLowerBound - @PageSize + 1    
 ELSE    
  SET @PageUpperBound = 0;    
        
   
 WITH ResultEntries AS    
 (    
  SELECT     
   ROW_NUMBER() OVER (ORDER BY u.UserName) AS RowNumber,    
   u.UserName,    
   dbo.ConvertTimeFromUtc(u.CreatedDate,@ApplicationId)CreatedDate,    
   m.Email,     
   m.PasswordQuestion,      
   m.IsApproved,    
   dbo.ConvertTimeFromUtc(m.LastLoginDate,@ApplicationId)LastLoginDate,     
   dbo.ConvertTimeFromUtc(LastActivityDate,@ApplicationId)LastActivityDate,    
   dbo.ConvertTimeFromUtc(LastPasswordChangedDate,@ApplicationId)LastPasswordChangedDate,     
   u.Id,     
   m.IsLockedOut,    
   dbo.ConvertTimeFromUtc(LastLockoutDate,@ApplicationId)LastLockoutDate,  
   u.[FirstName],    
   u.[LastName]  ,  
   u.[MiddleName] ,   
   u.[CompanyName] ,   
   u.[BirthDate]  ,  
   u.[Gender]  ,  
   p.[IsBadCustomer]  ,  
   p.[IsActive]  ,  
   p.[IsOnMailingList]  ,  
   p.[Status]  ,  
   p.[CSRSecurityQuestion]  ,  
   p.[CSRSecurityPassword]  ,  
   u.[HomePhone]  ,  
   u.[MobilePhone] ,   
   u.[OtherPhone] ,  
   u.ImageId,
   p.[AccountNumber],  
   ug.MemberType  ,
   ExternalId,
   ExternalProfileId   
  FROM    
   dbo.USMemberGroup ug, 
   dbo.USUser u,   
   dbo.USSiteUser s, 
   dbo.USMembership m, 
   dbo.USCommerceUserProfile p,   
   @GroupId.nodes('/GenericCollectionOfGuid/guid')tab(col)    
  WHERE u.Status <> dbo.GetDeleteStatus() AND    
   ug.GroupId = tab.col.value('text()[1]','uniqueidentifier') AND     
   ug.MemberId = u.Id AND  ug.MemberType = @MemberType AND  
   u.Id = m.UserId AND   --- u.Id = p.Id AND  
   m.UserId = s.UserId AND    
   --s.SiteId = ISNULL(@ApplicationId,s.SiteId) AND    
	(@ApplicationId is null or s.SiteId = @ApplicationId) AND
   s.IsSystemUser = @IsSystemUser AND    
   m.IsApproved =  @IsApproved AND -- Checks whether the user is approved.    
   m.IsLockedOut = @IsLockedOut AND -- Checks whether the user is locked out.    
   --p.IsActive  = isnull(@IsActive,p.IsActive) AND  
	(@IsActive is null or p.IsActive = @IsActive) AND
   u.Id = p.Id AND    
   --s.ProductId = ISNULL(@ProductId,s.ProductId)    
	s.ProductId = 'CCF96E1D-84DB-46E3-B79E-C7392061219B')
 SELECT    
   UserName,    
   CreatedDate,    
   Email,     
   PasswordQuestion,      
   IsApproved,    
   LastLoginDate,     
   LastActivityDate,    
   LastPasswordChangedDate,     
   Id,     
   IsLockedOut,    
   LastLockoutDate,  
   [FirstName],    
   [LastName]  ,  
   [MiddleName] ,   
   [CompanyName] ,   
   [BirthDate]  ,  
   [Gender]  ,  
   [IsBadCustomer]  ,  
   [IsActive]  ,  
   [IsOnMailingList]  ,  
   [Status]  ,  
   [CSRSecurityQuestion]  ,  
   [CSRSecurityPassword]  ,  
   [HomePhone]  ,  
   [MobilePhone] ,   
   [OtherPhone] ,  
   ImageId,
   [AccountNumber],  
   MemberType,
   ExternalId,
   ExternalProfileId      
 FROM ResultEntries     
 WHERE (@PageIndex = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)    
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_FindUsersByName]'
GO
/*****************************************************
* Name : Membership_FindUsersByName
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_FindUsersByName]
--********************************************************************************
-- PARAMETERS
	@ApplicationId			uniqueidentifier,
    @UserNameToMatch		nvarchar(256),
	@IsSystemUser			bit = 0,
    @PageIndex				int,
    @PageSize				int
--********************************************************************************

AS
BEGIN
    
--********************************************************************************
-- Variable declaration
--********************************************************************************

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET		@PageLowerBound = @PageSize * @PageIndex
    IF (@PageIndex > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	ELSE
		SET @PageUpperBound = 0;

	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc	= GetUtcDate();
    
	WITH ResultEntries AS
	(
		SELECT 
		ROW_NUMBER() OVER (ORDER BY m.LoweredEmail) AS RowNumber,
		u.Id, 
		u.UserName,  
		m.Email,
		m.PasswordQuestion, 
		m.IsApproved,
		u.CreatedDate,
		m.LastLoginDate,
		u.LastActivityDate,
		m.LastPasswordChangedDate,
		m.IsLockedOut,
		m.LastLockoutDate
		FROM   dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s
		WHERE	s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND 
				m.UserId = u.Id AND 
				s.UserId = m.UserId AND
				s.IsSystemUser = @IsSystemUser	AND
				u.Status <> dbo.GetDeleteStatus() AND
				u.LoweredUserName LIKE LOWER(@UserNameToMatch) AND
				(u.ExpiryDate is null OR u.ExpiryDate >= getutcdate())
	)
	
	SELECT	
			Id, 
			UserName,  
			Email,
			PasswordQuestion, 
			IsApproved,
			CreatedDate,
			LastLoginDate,
			LastActivityDate,
			LastPasswordChangedDate,
			IsLockedOut,
			LastLockoutDate 
	FROM	ResultEntries 
	WHERE	(@PageIndex = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)

    RETURN 0
END
--***********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShipment_Get]'
GO
/*****************************************************
* Name : OrderShipment_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderShipment_Get]
(@OrderShipmentId uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN

SELECT [Id]
      ,[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(ShippedDate,@ApplicationId)ShippedDate
      ,[ShippingOptionId]
      ,CreateDate 
      ,[CreatedBy]
      ,[ModifiedBy]
      ,ModifiedDate
	  ,[EstimatedShipmentTotal]
	  ,ShipmentStatus
	  ,WarehouseId
	,ExternalReferenceNumber
FROM [dbo].[FFOrderShipment]
Where Id =@OrderShipmentId
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,SI.[CreateDate] AS CreateDate
      ,SI.[CreatedBy]
      ,SI.[ModifiedDate] AS ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
Where S.Id=@OrderShipmentId


SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,SP.[ShippingContainerId]
      ,SP.CreatedDate AS CreatedDate
      ,SP.[ChangedBy]
      ,SP.ModifiedDate AS ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
Where S.Id=@OrderShipmentId

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShipping_GetOrderShipments]'
GO
/*****************************************************
* Name : OrderShipping_GetOrderShipments
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderShipping_GetOrderShipments]
(@OrderShippingId uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN

SELECT [Id]
      ,[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(ShippedDate,@ApplicationId)ShippedDate
      ,[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(CreateDate,@ApplicationId)CreateDate
      ,[CreatedBy]
      ,[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate
	  ,[EstimatedShipmentTotal]
	  ,ShipmentStatus
	  ,WarehouseId
	,ExternalReferenceNumber
FROM [dbo].[FFOrderShipment]
Where OrderShippingId =@OrderShippingId
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
Where OrderShippingId =@OrderShippingId


SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
		,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
Where OrderShippingId =@OrderShippingId

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ResponseFlow_InsertFromWorkTable]'
GO
/*****************************************************
* Name : ResponseFlow_InsertFromWorkTable
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[ResponseFlow_InsertFromWorkTable]  
 @WorkerId uniqueidentifier,  
 @ResponseId uniqueidentifier  
AS  
Begin  

	-- remove users that are unsubscribed from this response campaign
	Delete MKResponseFlowWorkTable
	Where ResponseId = @ResponseId
		And UserId In(select UserId from UsUserUnsubscribe Where IsNull(CampaignId, @ResponseId) = @ResponseId)
		And UserId Not In(select UserId from USResubscribe Where CampaignId = @ResponseId)

	-- only insert the records that arent already in the ResponseFlow  
	insert into MKResponseFlow  
	Select Distinct isnull(rf.ResponseId, @ResponseId), trf.UserId, trf.DateStarted, trf.LastSent, trf.Completed  
	From MKResponseFlowWorkTable trf  
		Left Outer Join MKResponseFlow rf  on rf.ResponseId = trf.ResponseId   
		And rf.UserId = trf.UserId  
		and rf.DateStarted = trf.DateStarted  
	Where trf.WorkerId = @WorkerId  
		And  
		((rf.ResponseId is null)  
		Or (rf.ResponseId is not null and rf.DateStarted > trf.DateStarted))  
	Group By rf.ResponseId, trf.UserId, trf.DateStarted, trf.LastSent, trf.Completed  
	order by trf.DateStarted Desc

	-- update the existing response flow records to complete for unsubscribed users
	Update MKResponseFlow Set
		Completed = GetUtcDate()
	Where ResponseId = @ResponseId
		And UserId in(select UserId from UsUserUnsubscribe Where IsNull(CampaignId, @ResponseId) = @ResponseId)
   
	Delete MKResponseFlowWorkTable Where WorkerId = @WorkerId  
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_Get]'
GO
/*****************************************************
* Name : Payment_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[Payment_Get]    
(    
 @Id uniqueidentifier=null,
 @SortColumn VARCHAR(25)=null,    
 @PagingSize INT=null,     
 @PagingIndex INT=1,    
 @CustomerId uniqueidentifier=null,    
 @PaymentTypeId int=null,
 @PaymentStatusId int=null,
 @ProcessorTxnId nvarchar(255) =null,
 @PaymentDateFrom DateTime =null,
 @PaymentDateTo DateTime =null,
 @ClearedDateFrom DateTime =null,
 @ClearedDateTo DateTime =null,
 @RefundOnly bit = null,
 @ApplicationId uniqueidentifier=null
)    
AS    
BEGIN    
    
IF @Id is not null 
BEGIN
 SELECT P.[Id] ,
        P.[PaymentTypeId] ,
        P.[PaymentStatusId] ,
        P.[CustomerId] ,
        U.[AccountNumber] ,
        P.[Amount] ,
        P.[CapturedAmount] ,
        P.[Description] ,
        dbo.ConvertTimeFromUtc(P.[FailureDate], @ApplicationId) FailureDate ,
        P.[FailureDescription] ,
        dbo.ConvertTimeFromUtc(P.[CreatedDate], @ApplicationId) CreatedDate ,
        P.[CreatedBy] ,
        dbo.ConvertTimeFromUtc(P.[ClearDate], @ApplicationId) ClearDate ,
        P.[ProcessorTransactionId] ,
        P.[AuthCode] ,
        P.[Status] ,
        dbo.ConvertTimeFromUtc(P.ModifiedDate, @ApplicationId) ModifiedDate ,
        P.[ModifiedBy] ,
        O.[PurchaseOrderNumber] ,
        O.[Id] OrderId ,
        OP.[Amount] OrderPaymentAmount ,
        OP.[Id] OrderPaymentId,
		P.IsRefund
 FROM   [dbo].[PTPayment] P
        INNER JOIN PTOrderPayment OP ON P.Id = OP.PaymentId
        INNER JOIN OROrder O ON OP.OrderId = O.Id
        INNER JOIN USCommerceUserProfile U ON P.CustomerId = U.Id
 WHERE  P.Id = @Id

 SELECT PC.Id ,
        PC.PaymentID ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate , @ApplicationId) as CreatedDate,
        PC.TransactionId ,
        PC.ParentPaymentCaptureID,
		SUM(ISNULL(PC2.CapturedAmount,0)) as TotalRefundAmount
 FROM   dbo.PTPaymentCapture PC
		LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
 WHERE  PC.PaymentId = @Id
 GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId


END
ELSE
BEGIN


SET @PaymentDateFrom = dbo.ConvertTimeToUtc(isnull(@PaymentDateFrom,'01-01-1970'),@ApplicationId)
SET @PaymentDateTo = dbo.ConvertTimeToUtc(isnull(@PaymentDateTo,'01-01-3000'),@ApplicationId)

IF @ClearedDateFrom IS NOT NULL
	SET @ClearedDateFrom = dbo.ConvertTimeToUtc(@ClearedDateFrom,@ApplicationId)
IF @ClearedDateTo IS NOT NULL
	SET @ClearedDateTo = dbo.ConvertTimeToUtc(@ClearedDateTo,@ApplicationId)


 IF @PagingSize IS NULL    
  SET @PagingSize = 2147483647    
    
 IF @SortColumn IS NULL    
  SET @SortColumn = 'cleardate Asc'    
    
 Set @SortColumn = (@SortColumn)    
    
 ;WITH PagingCTE (Row_ID    
 ,[Id]
  ,[PaymentTypeId]
  ,[PaymentStatusId]
  ,[CustomerId]
  ,[AccountNumber]
  ,[Amount]
  ,[CapturedAmount]
  ,[Description]
  , FailureDate
  ,[FailureDescription]
  ,CreatedDate
  ,[CreatedBy]
  ,ClearDate
  ,[ProcessorTransactionId]
  ,[AuthCode]
  ,[Status]
  ,ModifiedDate
  ,[ModifiedBy]
  ,[PurchaseOrderNumber]
  ,OrderId
  ,OrderPaymentAmount
  ,OrderPaymentId
  ,IsRefund)    
 AS    
 (    
 SELECT     
    ROW_NUMBER()     
 OVER(ORDER BY     
 Case When @SortColumn='paymenttypeid desc' Then PaymentTypeId END DESC,    
 Case When @SortColumn='paymenttypeid asc' or @SortColumn='paymenttypeid' Then PaymentTypeId END Asc,    
    
 Case When @SortColumn='paymentstatusid desc' Then cast(PaymentStatusId as varchar(25)) END DESC,    
 Case When @SortColumn='paymentstatusid asc' or @SortColumn='paymentstatusid' Then cast(PaymentStatusId as varchar(25)) END Asc,    
 
 Case When @SortColumn='amount desc' Then P.Amount  END DESC,    
 Case When @SortColumn='amount asc' or @SortColumn='amount' Then P.Amount END Asc,    
 
 Case When @SortColumn='description desc' Then cast(P.Description as varchar(25)) END DESC,    
 Case When @SortColumn='description asc' or @SortColumn='description' Then cast(P.Description as varchar(25)) END Asc,    
 
 Case When @SortColumn='failuredate desc' Then cast(FailureDate as varchar(25)) END DESC,    
 Case When @SortColumn='failuredate asc' or @SortColumn='failuredate' Then cast(FailureDate as varchar(25)) END Asc,    
 
 Case When @SortColumn='failuredescription desc' Then cast(FailureDescription as varchar(25)) END DESC,    
 Case When @SortColumn='failuredescription asc' or @SortColumn='failuredescription' Then cast(FailureDescription as varchar(25)) END Asc,    
 
 Case When @SortColumn='createddate desc' Then cast(P.CreatedDate as varchar(25)) END DESC,    
 Case When @SortColumn='createddate asc' or @SortColumn='createddate' Then cast(P.CreatedDate as varchar(25)) END Asc,       

 Case When @SortColumn='createdby desc' Then cast(P.CreatedBy as varchar(25)) END DESC,    
 Case When @SortColumn='createdby asc' or @SortColumn='createdby' Then cast(P.CreatedBy as varchar(25)) END Asc,       


 Case When @SortColumn='modifieddate desc' Then cast(P.ModifiedDate as varchar(25)) END DESC,    
 Case When @SortColumn='modifieddate asc' or @SortColumn='modifieddate' Then cast(P.ModifiedDate as varchar(25)) END Asc,       

 Case When @SortColumn='modifiedby desc' Then cast(P.ModifiedBy as varchar(25)) END DESC,    
 Case When @SortColumn='modifiedby asc' or @SortColumn='modifiedby' Then cast(P.ModifiedBy as varchar(25)) END Asc,       
 
 Case When @SortColumn='cleardate desc' Then cast(P.ClearDate as varchar(25)) END DESC,    
 Case When @SortColumn='cleardate asc' or @SortColumn='cleardate' Then cast(P.ClearDate as varchar(25)) END Asc,       

 Case When @SortColumn='processortransactionid desc' Then cast(ProcessorTransactionId as varchar(25)) END DESC,    
 Case When @SortColumn='processortransactionid asc' or @SortColumn='processortransactionid' Then cast(ProcessorTransactionId as varchar(25)) END Asc,       

 Case When @SortColumn='authcode desc' Then cast(P.AuthCode as varchar(25)) END DESC,    
 Case When @SortColumn='authcode asc' or @SortColumn='authcode' Then cast(P.AuthCode as varchar(25)) END Asc,       

 Case When @SortColumn='purchaseordernumber desc' Then cast(O.[PurchaseOrderNumber] as varchar(100)) END DESC,    
 Case When @SortColumn='purchaseordernumber asc' or @SortColumn='purchaseordernumber' Then cast(O.[PurchaseOrderNumber] as varchar(100)) END Asc,    

 Case When @SortColumn='status desc' Then cast(P.Status as varchar(25)) END DESC,    
 Case When @SortColumn='status asc' or @SortColumn='status' Then cast(P.Status as varchar(25))       
     
 Else P.CreatedDate End Asc    
          ) AS [Row_ID]    
,P.[Id]
      ,P.[PaymentTypeId]
      ,P.[PaymentStatusId]
      ,P.[CustomerId]
	  ,U.[AccountNumber]
      ,P.[Amount]
	  ,P.[CapturedAmount]
      ,P.[Description]
      ,dbo.ConvertTimeFromUtc(P.[FailureDate],@ApplicationId)FailureDate
      ,P.[FailureDescription]
      ,dbo.ConvertTimeFromUtc(P.[CreatedDate],@ApplicationId)CreatedDate
      ,P.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(P.[ClearDate],@ApplicationId)ClearDate
      ,P.[ProcessorTransactionId]
      ,P.[AuthCode]
      ,P.[Status]
      ,dbo.ConvertTimeFromUtc(P.[ModifiedDate],@ApplicationId)ModifiedDate
      ,P.[ModifiedBy]
	  ,O.[PurchaseOrderNumber]
	  ,O.[Id] OrderId
	  ,OP.[Amount] OrderPaymentAmount
	  ,OP.[Id] OrderPaymentId
	  ,P.IsRefund
  FROM [dbo].[PTPayment] P 
Inner Join PTOrderPayment OP On P.Id =OP.PaymentId
Inner Join OROrder O on OP.OrderId =O.Id    
Inner Join USCommerceUserProfile U on P.CustomerId = U.Id
 Where     
  (@CustomerId IS NULL OR P.CustomerId = @CustomerId)
   AND (@PaymentDateFrom is null or P.CreatedDate >= @PaymentDateFrom)
   AND (@PaymentDateTo is null or P.CreatedDate <= @PaymentDateTo)
	AND ((@ClearedDateFrom is null or ClearDate >= @ClearedDateFrom)
			OR (@ClearedDateFrom is null AND ClearDate is null))   
	AND ((@ClearedDateTo is null or ClearDate <= @ClearedDateTo) 
			OR (@ClearedDateTo is null AND ClearDate is null))     
	AND (@PaymentTypeId IS NULL OR P.PaymentTypeId = @PaymentTypeId)
   AND (@PaymentStatusId IS NULL OR P.PaymentStatusId = @PaymentStatusId)
   AND (@ProcessorTxnId  is null OR ProcessorTransactionId like isnull('%'+(@ProcessorTxnId)+'%',ProcessorTransactionId))
   AND (@RefundOnly is null OR @RefundOnly = 0 OR P.Amount < 0)
   AND (@ApplicationId IS NULL OR O.SiteId = @ApplicationId)
)     

SELECT     
Row_ID    
,[Id]
      ,[PaymentTypeId]
      ,[PaymentStatusId]
      ,[CustomerId]
	  ,[AccountNumber]
      ,[Amount]
	  ,[CapturedAmount]
      ,[Description]
      ,[FailureDate]
      ,[FailureDescription]
      ,[CreatedDate]
      ,[CreatedBy]
      ,[ClearDate]
      ,[ProcessorTransactionId]
      ,[AuthCode]
      ,[Status]
      ,[ModifiedDate]
      ,[ModifiedBy]
	  ,[PurchaseOrderNumber]
	  ,[OrderId]
	  ,[OrderPaymentAmount]		  
	  ,[OrderPaymentId]
	  ,IsRefund
FROM PagingCTE pcte    
WHERE Row_ID >= (@PagingSize * @PagingIndex) - (@PagingSize -1) AND Row_ID <= @PagingSize * @PagingIndex    
    
SELECT Count(*)[[Count]
FROM [dbo].[PTPayment] P
Inner Join PTOrderPayment OP On P.Id =OP.PaymentId
Inner Join OROrder O on OP.OrderId =O.Id    
Inner Join USCommerceUserProfile U on P.CustomerId = U.Id
 Where
	(@CustomerId IS NULL OR P.CustomerId = @CustomerId)
	AND (@PaymentDateFrom is null or P.CreatedDate >= @PaymentDateFrom)    
	AND (@PaymentDateTo is null or P.CreatedDate <= @PaymentDateTo)    
	AND ((@ClearedDateFrom is null or ClearDate >= @ClearedDateFrom)
			OR (@ClearedDateFrom is null AND ClearDate is null))   
	AND ((@ClearedDateTo is null or ClearDate <= @ClearedDateTo) 
			OR (@ClearedDateTo is null AND ClearDate is null))  
	AND (@PaymentTypeId is null OR PaymentTypeId = @PaymentTypeId)
	AND (@PaymentStatusId is null OR PaymentStatusId = @PaymentStatusId)
	AND (@ProcessorTxnId  is null OR ProcessorTransactionId like '%'+ @ProcessorTxnId +'%')
	AND (@RefundOnly is null OR @RefundOnly = 0 OR P.Amount < 0)
	AND (@ApplicationId IS NULL OR O.SiteId = @ApplicationId)

 END 

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_GetOrder]'
GO
/*****************************************************
* Name : Order_GetOrder
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Order_GetOrder](@Id uniqueidentifier = null, @CartId uniqueidentifier = null,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

IF @Id IS NULL AND @CartId IS NULL 
	begin
		raiserror('DBERROR||%s',16,1,'OrderId or CartId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],   
 o.[Id] OrderId, 
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.[PaymentRemaining],   
 O.RefundTotal,
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O  
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy  
WHERE
O.SiteId = @ApplicationId AND
(@Id is null or @Id = O.Id) AND
((@CartId is null or @CartId = CartId) OR (CartId IS NULL AND @CartId IS NULL))
--O.Id = ISnull(@Id,Id) AND (CartId = Isnull(@CartId,CartId) OR (CartId IS NULL AND @CartId IS NULL)) ;    


IF @Id IS NULL
	SELECT  @Id = Id FROM OROrder WHERE CartId = @CartId;

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId=@Id AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount]
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy  
WHERE OI.OrderId = @Id;

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded
FROM OROrderShipping OS
WHERE OS.OrderId = @Id;

select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized
from PRProductAttribute PA  
Inner join 
	(Select ProductId FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId = @Id
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[AttributeGroupId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId =@Id

--- order shipments
SELECT F.[Id]
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId =@Id
Order By CreateDate DESC

SELECT SI.[Id]
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId =@Id



SELECT SP.[Id]
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId=@Id

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_UpdateStatus]'
GO
/*****************************************************
* Name : Payment_UpdateStatus
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Payment_UpdateStatus]
(
	@PaymentId uniqueidentifier,
	@ProcessorTransactionId nvarchar(256)=null,
	@AuthCode				nvarchar(256)=null,
	@PaymentStatusId int=null,
	@FailureDate	DateTime,
	@FailureDescription nvarchar(max)=null,
	@ClearDate DateTime=null,
	@Amount money=null,
	@ModifiedBy uniqueidentifier,
	@ApplicationId uniqueidentifier,
	@IsRefund tinyint = 0
)AS
BEGIN

Declare @ModifiedDate DateTime
Declare @error	  int
Declare @stmt	  varchar(256)	
Declare @sequence int
DECLARE @OrderId uniqueidentifier
DECLARE @OrderPaymentAmount money

Set @ModifiedDate = GetUTCDate()

Update PTPayment 
	SET ProcessorTransactionId=isnull(@ProcessorTransactionId ,ProcessorTransactionId)
	,AuthCode=isnull(@AuthCode,AuthCode)
	,PaymentStatusId =isnull(@PaymentStatusId,PaymentStatusId)
	,FailureDate =dbo.ConvertTimeToUtc(@FailureDate,@ApplicationId)
	,FailureDescription=isnull(@FailureDescription,FailureDescription) 
	,ClearDate =isnull(dbo.ConvertTimeToUtc(@ClearDate,@ApplicationId),ClearDate)
	,Amount=isnull(@Amount,Amount)
	,ModifiedBy =@ModifiedBy
	,ModifiedDate =@ModifiedDate
	,IsRefund =@IsRefund
	
Where Id=@PaymentId	


SELECT TOP 1 @OrderId = OrderId FROM PTOrderPayment where PaymentId = @PaymentId

Update OROrder Set PaymentTotal =dbo.Order_GetPaymentTotal(@OrderId), RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
Where Id=@OrderId

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_SetExpiryDate]'
GO
/*****************************************************
* Name : Membership_SetExpiryDate
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_SetExpiryDate]
--********************************************************************************
-- PARAMETERS
	@ProductId			uniqueidentifier = NULL,
	@ApplicationId		uniqueidentifier,
	@UserId				uniqueidentifier, 
	@ExpiryDate			datetime
--********************************************************************************

AS
BEGIN
	
	DECLARE @Count INT
	DECLARE @CurrentTimeUtc DATETIME

	SET @CurrentTimeUtc = GetUtcDate()

	IF(@ApplicationId IS NOT NULL)
	BEGIN
		IF ( (SELECT COUNT(Id) FROM USUser u, USSiteUser s WHERE u.Id = s.UserId AND s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND s.UserId = @UserId AND u.Status <> dbo.GetDeleteStatus() AND s.ProductId = ISNULL(@ProductId,s.ProductId)) > 0)
		BEGIN
			
				UPDATE dbo.USUser
				SET	ExpiryDate =@ExpiryDate,LastActivityDate = @CurrentTimeUtc
				WHERE Id = @UserId
			
			
		END
		ELSE
		BEGIN
			RAISERROR ('NOTEXISTS||User', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END
	END
	ELSE
	BEGIN
		RAISERROR ('ISNULL||ApplicationId', 16, 1 )
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	RETURN @@ROWCOUNT	
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[AssetFile_SaveFile]'
GO
/*****************************************************
* Name : AssetFile_SaveFile
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[AssetFile_SaveFile] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id  uniqueidentifier,
	@FileName  nvarchar(256),
	@FileSize  int = null,
	@Extension  nvarchar(10),
	@FolderName  nvarchar(256) = null,
	@Attributes  nvarchar(256) = null,
	@RelativePath  nvarchar(4000) = null,
	@PhysicalPath  nvarchar(4000)  = null,
	@MimeType nvarchar(256) = null,
    @AltText nvarchar(1024)= null, 
    @FileIcon nvarchar(1024)= null, 
    @DescriptiveMetadata nvarchar(4000)= null,
    @SecurityLevels nvarchar(max) = null
)
as
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE	
	@Stmt 		VARCHAR(50),	
	@Error 		int

--********************************************************************************
-- code
--********************************************************************************
BEGIN
	IF @RelativePath IS NULL OR @RelativePath = ''
	BEGIN
		DECLARE @ParentId uniqueidentifier
		DECLARE @ObjectTypeId int
		SELECT @ParentId = ParentId, @ObjectTypeId = ObjectTypeId FROM COContent WHERE Id = @Id
		
		SET @RelativePath = dbo.GetVirtualPathByObjectType(@ParentId, @ObjectTypeId)
	END
	
    IF (EXISTS(SELECT 1 FROM COFile WHERE  ContentId = @Id))
	BEGIN			
		SET @Stmt = 'Update AssetFile'
		UPDATE COFile WITH (ROWLOCK)
		SET		[FileName] = @FileName,
				FileSize = @FileSize,
				Extension = @Extension,
				FolderName = @FolderName,
				Attributes = @Attributes,
				RelativePath = @RelativePath,
				PhysicalPath = @PhysicalPath, 
				MimeType = @MimeType,
				AltText = @AltText,
				FileIcon = @FileIcon, 
				DescriptiveMetadata = @DescriptiveMetadata
		WHERE	ContentId = @Id
		
		IF @SecurityLevels IS NOT NULL
			BEGIN
				DELETE FROM USSecurityLevelObject WHERE ObjectId = @Id
			END
	END		
	ELSE
	BEGIN
		SET @Stmt = 'Create AssetFile'
		INSERT INTO COFile (
				ContentId,
				FileName,
				FileSize,
				Extension,
				FolderName,
				Attributes,
				RelativePath,
				PhysicalPath, 
				MimeType,
				AltText,
				FileIcon, 
				DescriptiveMetadata
		)
		values
		(
			@Id,
			@FileName,
			@FileSize,
			@Extension,
			@FolderName,
			@Attributes,
			@RelativePath,
			@PhysicalPath,
			@MimeType,
			@AltText,
			@FileIcon, 
			@DescriptiveMetadata		
		)
	END
	
	IF @SecurityLevels IS NOT NULL
	BEGIN
			INSERT INTO USSecurityLevelObject (ObjectId,ObjectTypeId,SecurityLevelId)
			SELECT @Id,9,Value
			FROM SplitComma(@SecurityLevels,',')
			WHERE LEN(Value)>0
			EXCEPT
			SELECT @Id,ObjectTypeId,SecurityLevelId FROM USSecurityLevelObject
			WHERE ObjectId=@Id
	END

	SELECT @Error = @@ERROR
	IF @Error <> 0
	BEGIN
		RAISERROR('DBERROR||%s',16,1,@Stmt)
		RETURN dbo.GetDataBaseErrorCode()
	END
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Group_GetGroup]'
GO
/*****************************************************
* Name : Group_GetGroup
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--sp_helptext group_getgroup

ALTER PROCEDURE [dbo].[Group_GetGroup]    
(    
--********************************************************************************    
 @ProductId   uniqueidentifier,    
 @ApplicationId  uniqueidentifier,    
 @Id     uniqueidentifier = null,    
 @Status    int = NULL,    
 @GroupType   int = 0,    
 @ExcludeGroupInThisRoles nvarchar(max) = null    
--********************************************************************************    
)    
AS    
BEGIN    
 DECLARE @CurrentTimeUtc datetime    
 SET @CurrentTimeUtc = GetUtcDate()    
     
 DECLARE @Exclude table    
 (    
  GroupId uniqueidentifier    
 )     
     
 IF (@ExcludeGroupInThisRoles IS NOT NULL)    
 BEGIN    
  INSERT INTO @Exclude     
  SELECT distinct MemberId FROM USMemberRoles     
   Where RoleId IN (SELECT Value FROM dbo.SplitComma(@ExcludeGroupInThisRoles, ',')) AND MemberType = 2    
 END    
     
 IF (@Id IS NOT NULL)    
 BEGIN    
  SELECT  ProductId,    
    ApplicationId,    
    Id,    
    Title,    
    Description,    
    CreatedBy,    
    CreatedDate,    
    ModifiedBy,    
    ModifiedDate,    
    Status,    
    ExpiryDate,    
    FN.UserFullName CreatedByFullName,    
    MN.UserFullName ModifiedByFullName,    
    GroupType        
  FROM dbo.USGroup A    
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy    
  LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy    
  WHERE Id = @Id AND    
    Status <> dbo.GetDeleteStatus()    
    AND Id NOT IN (SELECT GroupId FROM @Exclude)    
 END    
 ELSE IF (@Status IS NOT NULL)    
 BEGIN    
  SELECT   ProductId,            
    ApplicationId,            
    Id,            
    Title,            
    Description,            
    CreatedBy,            
    CreatedDate,            
    ModifiedBy,            
    ModifiedDate,            
    Status,            
    ExpiryDate,            
    FN.UserFullName CreatedByFullName,    
    MN.UserFullName ModifiedByFullName,    
    GroupType        
  FROM dbo.USGroup A    
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy    
  LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy    
  WHERE Status = @Status AND      
    (@ApplicationId is null or ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) and    
    --ApplicationId = ISNULL(@ApplicationId,ApplicationId) AND      
    ProductId = ISNULL(@ProductId,ProductId) AND    
    ISNULL(ExpiryDate,CONVERT( datetime, '99991231', 112 )) >= @CurrentTimeUtc  AND          
    ( ( @GroupType = 0 AND (GroupType = 1  or GroupType =2 OR GroupType = 3) )     
    OR (@GroupType <> 0 AND (GroupType = @GroupType ) ))    
    AND Id NOT IN (SELECT GroupId FROM @Exclude)    
    
 END    
 ELSE      
  BEGIN   --  This is for GroupType overload applicable to ApplicationID overload    
  SELECT   ProductId,            
    ApplicationId,            
    Id,            
    Title,            
    Description,            
    CreatedBy,            
    CreatedDate,            
    ModifiedBy,            
    ModifiedDate,            
    Status,            
    ExpiryDate,            
    FN.UserFullName CreatedByFullName,    
    MN.UserFullName ModifiedByFullName,    
    GroupType        
  FROM dbo.USGroup A    
  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy    
  LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy    
  WHERE Status <>  dbo.GetDeleteStatus()AND      
    (@ApplicationId is null or ApplicationId in (select SiteId from dbo.GetVariantSites(@ApplicationId)) OR  ProductId = @ApplicationId ) and    
    --ApplicationId = ISNULL(@ApplicationId,ApplicationId) AND      
         ProductId = ISNULL(@ProductId,ProductId) AND    
    ISNULL(ExpiryDate,CONVERT( datetime, '99991231', 112 )) >= @CurrentTimeUtc  AND          
    ( ( @GroupType = 0 AND (GroupType = 1  or GroupType =2 OR GroupType = 3) )     
    OR (@GroupType <> 0 AND (GroupType = @GroupType ) ))    
    AND Id NOT IN (SELECT GroupId FROM @Exclude)    
    
 END      
    
     
 RETURN @@ROWCOUNT    
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_FindUsersByEmail_Count]'
GO
/*****************************************************
* Name : Membership_FindUsersByEmail_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_FindUsersByEmail_Count]
--********************************************************************************
-- PARAMETERS

	@ApplicationId			uniqueidentifier,
    @EmailToMatch			nvarchar(256),
	@IsSystemUser			bit = 0
--********************************************************************************
AS
BEGIN
    
--********************************************************************************
-- Variable declaration
--********************************************************************************

	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc	= GetUtcDate()

	SELECT  COUNT(*)
	FROM    dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s
	WHERE   s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
			AND m.UserId = u.Id AND 
			s.UserId = m.UserId AND 
			s.IsSystemUser = @IsSystemUser	AND
			u.Status <> dbo.GetDeleteStatus() AND
			(
				(@EmailToMatch IS NULL AND m.Email IS NULL)
						OR
				(@EmailToMatch IS NOT NULL	AND	m.LoweredEmail LIKE LOWER(@EmailToMatch))
			)	AND
			(u.ExpiryDate is null OR u.ExpiryDate >= getutcdate())
    RETURN 0
END
--***********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShipmentItem_Save]'
GO
/*****************************************************
* Name : OrderShipmentItem_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderShipmentItem_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@OrderShipmentId	uniqueidentifier,
	@OrderItemId		uniqueidentifier,
	@Quantity			money,
	@OrderShipmentContainerId uniqueidentifier=null,
	@ExternalReferenceNumber nvarchar(50) = null,
	@ModifiedBy       	uniqueidentifier
	
)
AS
BEGIN

Declare @OrderItemQuantity money
Declare @OrderShipmentItemQuantity money
Declare @OrderId uniqueidentifier
Declare @ProductSkuId uniqueidentifier
Declare @OrderStatusId int
Declare @count1 int
Declare @count2 int

IF @OrderShipmentContainerId ='00000000-0000-0000-0000-000000000000'
SET @OrderShipmentContainerId=NULL

	IF (@Id is null OR @Id =dbo.GetEmptyGUID())
	BEGIN
		Set @Id =newid()
	END
	
	IF Not Exists(SELECT Id FROM FFOrderShipmentItem Where Id = @Id)
	BEGIN
		INSERT INTO [dbo].[FFOrderShipmentItem]
			   ([Id]
			   ,[OrderShipmentId]
			   ,[OrderItemId]
			   ,[Quantity]
			   ,[OrderShipmentContainerId]
			   ,externalreferencenumber
			   ,[CreateDate]
			   ,[CreatedBy])
		 VALUES
			   (@Id
			   ,@OrderShipmentId
			   ,@OrderItemId
				,@Quantity
				,@OrderShipmentContainerId
				,@ExternalReferenceNumber
			   ,GetUTCDate()
			   ,@ModifiedBy)
	END
	ELSE 
	BEGIN
		UPDATE [dbo].[FFOrderShipmentItem]
		SET
			   [OrderShipmentId] = @OrderShipmentId
			   ,[OrderItemId] = @OrderItemId
			   ,[Quantity] = @Quantity
			   ,[OrderShipmentContainerId] = @OrderShipmentContainerId
			   ,[ModifiedDate] = GetUTCDate()
			   ,[ModifiedBy] = @ModifiedBy
			   ,externalreferencenumber = @ExternalReferenceNumber
		WHERE  [Id]  = @Id
	END
	

	if @@error <> 0
	begin
		raiserror('DBERROR||%s',16,1,'Insert OrderShipmentItem')
		return dbo.GetDataBaseErrorCode()	
	end


	Select @OrderItemQuantity = Quantity
	FROM  OROrderItem  
	Where Id =@OrderItemId

	Select @OrderShipmentItemQuantity = sum(Quantity)
	FROM  FFOrderShipmentItem   SI	
	INNER JOIN FFOrderShipmentPackage  S ON SI.OrderShipmentContainerId=S.Id
	Where OrderItemId =@OrderItemId  AND ShipmentStatus=2
	Group By OrderItemId
	If(@OrderItemQuantity = @OrderShipmentItemQuantity OR @OrderShipmentItemQuantity>0.0)
	BEGIN
	
		Update OROrderItem 
		Set OrderItemStatusId=Case When @OrderItemQuantity = @OrderShipmentItemQuantity Then 2 Else 7 End
		Where  Id =@OrderItemId

		if @@error <> 0
		begin
			raiserror('DBERROR||%s',16,1,'Update Status Shipped OrderShipmentItem')
			return dbo.GetDataBaseErrorCode()	
		end
	END


	DECLARE @estimatedShipmentTotal money

	Select @estimatedShipmentTotal = [dbo].[Order_AmountToCaptureForShipment](@OrderId,'',1, @OrderShipmentId)
	
	UPDATE FFOrderShipment SET EstimatedShipmentTotal = @estimatedShipmentTotal WHERE Id = @OrderShipmentId


END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_Save]'
GO
/*****************************************************
* Name : Site_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Site_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@URL				xml,
	@VirtualPath		nvarchar(4000)=null,
	@ParentSiteId		uniqueidentifier=null,
	@SiteType 	        int=null,
	@Keywords	        nvarchar(4000)=null,
	@Status				int=null,
	@SendNotification	bit=null,
	@SubmitToTranslation bit=null,
	@AllowMasterWebSiteUser bit=null,
	@ImportAllAdminPermission bit=null,
	@Prefix			nvarchar(5) = null, 
	@UICulture			nvarchar(10) = null,
	@LoginUrl		nvarchar(max) =null,
	@DefaultURL		nvarchar(max) =null,
	@NotifyOnTranslationError bit = null,
	@TranslationEnabled bit = null,
	@TranslatePagePropertiesByDefault bit = null,
	@AutoImportTranslatedSubmissions bit = null,
	@DefaultTranslateToLanguage nvarchar(10) = null,
	@PageImportOptions int =null,
	@ImportContentStructure bit =null,
	@ImportFileStructure bit=null,
	@ImportImageStructure bit=null,
	@ImportFormsStructure bit=null,
	@SiteGroups nvarchar(4000) = null,
	@MasterSiteId		uniqueidentifier=null,
	@PrimarySiteUrl nvarchar(max) =null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int
Declare @temp table(url1 nvarchar(max))
Declare @displayTitle varchar(512), @pageMapNodeDescription varchar(512), @pageMapNodeXml xml, @pageMapNodeId uniqueidentifier
Declare @LftValue int,@RgtValue int
Begin
--********************************************************************************
-- code
--********************************************************************************
	
	SET @displayTitle = @Title
	SET @pageMapNodeDescription = @Title

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  SISite where  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end

	set @Now = getdate()
	--set @Status= dbo.GetActiveStatus()



	if (@Id is null)			-- new insert
	begin
	
	IF (@ParentSiteId is not null) 
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue
		from SISite
		Where Id=@ParentSiteId and MasterSiteId=@MasterSiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END

		Update SISite Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
								RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 


		set @stmt = 'Site Insert'
		set @Id = newid();
		insert into SISite  (
					   Id,	
					   Title,
                       Description,
                       CreatedBy,
                       CreatedDate,
					   Status,
                       URL,
                       VirtualPath,
					   ParentSiteId,
					   Type,
                       Keywords,
                       HostingPackageInstanceId,
                       SendNotification,
                       SubmitToTranslation,
                       AllowMasterWebSiteUser,
                       ImportAllAdminPermission,
                       Prefix, 
                       UICulture, 
                       LoginUrl, 
                       DefaultURL,
                       NotifyOnTranslationError,
                       TranslationEnabled,
                       TranslatePagePropertiesByDefault,
                       AutoImportTranslatedSubmissions,
                       DefaultTranslateToLanguage,
                       PageImportOptions,
                       ImportContentStructure,
                       ImportFileStructure,
                       ImportImageStructure,
                       ImportFormsStructure,
                       MasterSiteId,
                       PrimarySiteUrl,
                       LftValue,
                       RgtValue) 
					values (
						@Id,
						@Title,
						@Description,
						@ModifiedBy,
						@Now,
						@Status,
						@URL,
						@VirtualPath,
						@ParentSiteId,
						@SiteType,
						@Keywords,
						0,
						@SendNotification,
						@SubmitToTranslation,
						@AllowMasterWebSiteUser,
						@ImportAllAdminPermission, 
						@Prefix, 
						@UICulture, 
						@LoginUrl, 
						@DefaultURL,
						@NotifyOnTranslationError,
						@TranslationEnabled,
						@TranslatePagePropertiesByDefault,
						@AutoImportTranslatedSubmissions,
						@DefaultTranslateToLanguage,
						@PageImportOptions,
						@ImportContentStructure,
						@ImportFileStructure,
						@ImportImageStructure,
						@ImportFormsStructure,
						@MasterSiteId,
						@PrimarySiteUrl,
						@RgtValue,
						@RgtValue + 1)

		-- Inserting in SiteGroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')


			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			

			
			
         end
         else			-- update
         begin
			set @stmt = 'Site Update'
			
			Update HSStructure Set Title=@Title Where Id=@Id
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			--SELECT @pageMapNodeXml =  cast((PageMapNodexml + '</pageMapNode>') as xml), @pageMapNodeId = PageMapNodeId FROM PageMapNode Where SiteId = PageMapNodeId And SiteId = @Id								

			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@displayTitle)[1]
			--				  with
			--				(sql:variable("@displayTitle"))')
			--set @pageMapNodeXml.modify('replace value of (pageMapNode/@description)[1]
			--				  with
			--				(sql:variable("@pageMapNodeDescription"))')			
			
			Update PageMapNode set displaytitle=@Title
			Where PageMapNodeId = @pageMapNodeId ANd SiteId = @Id
			-- Here we are not updating the friendly url
						
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()
			end
			
			set @rowcount= 0
			
			select * from SISite where Id=@Id
			update	SISite  with (rowlock)	set 
            Title =@Title,
            Description=@Description,
            ModifiedBy=@ModifiedBy,
            ModifiedDate=@Now,
            Status=@Status,
            URL=@URL,
            VirtualPath=@VirtualPath,
            ParentSiteId=@ParentSiteId,
            Type=@SiteType,
            Keywords= @Keywords,
            SendNotification = @SendNotification,
            SubmitToTranslation = @SubmitToTranslation,
            AllowMasterWebSiteUser=@AllowMasterWebSiteUser,
			ImportAllAdminPermission=@ImportAllAdminPermission,
			Prefix = @Prefix, 
			UICulture = @UICulture,
			LoginUrl = @LoginUrl, 
			DefaultURL =@DefaultURL,
			NotifyOnTranslationError = @NotifyOnTranslationError,
			TranslationEnabled = @TranslationEnabled,
			TranslatePagePropertiesByDefault = @TranslatePagePropertiesByDefault,
			AutoImportTranslatedSubmissions = @AutoImportTranslatedSubmissions,
			DefaultTranslateToLanguage = @DefaultTranslateToLanguage,	
			PageImportOptions =	@PageImportOptions,
			ImportContentStructure=@ImportContentStructure,
			ImportFileStructure=@ImportFileStructure,
			ImportImageStructure=@ImportImageStructure,
			ImportFormsStructure=@ImportFormsStructure,
			MasterSiteId = @MasterSiteId,
			PrimarySiteUrl = @PrimarySiteUrl
 	where 	Id    = @Id --and
	--isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
	
	select @error = @@error, @rowcount = @@rowcount
	
			--Deleting from SiteGroups
			DELETE FROM SISiteGroup WHERE SiteId = @id
			-- Insert into sitegroups
			INSERT INTO SISiteGroup
			SELECT newid(),@Id ,Items FROM dbo.SplitGuid(@SiteGroups,',')
		
			
	

if @error <> 0
  begin
      raiserror('DBERROR||%s',16,1,@stmt)
      return dbo.GetDataBaseErrorCode()
   end
if @rowcount = 0
  begin
    raiserror('CONCURRENCYERROR',16,1)
     return dbo.GetDataConcurrencyErrorCode()			-- concurrency error
end	
end
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageMap_MovePage]'
GO
/*****************************************************
* Name : PageMap_MovePage
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[PageMap_MovePage]  
(  
	@p_siteId uniqueidentifier,  
	@p_pageId uniqueidentifier,  
	@p_fromNodeId uniqueidentifier,  
	@p_toNodeId uniqueidentifier,  
	@p_copy int,  
	@p_outputPageId uniqueidentifier output  
)   
AS  
BEGIN  
	IF NOT EXISTS(Select 1 from PageMapNode where PageMapNodeId = @p_fromNodeId OR PageMapNodeId = @p_toNodeId AND SiteId = @p_siteId)  
	BEGIN  
		RAISERROR('NOTEXISTS||Source / Destination Node Id', 16, 1)  
		RETURN dbo.GetBusinessRuleErrorCode()  
	END
	
	DECLARE @DisplayOrder int
	SELECT @DisplayOrder = ISNULL(MAX(M.DisplayOrder), 0) + 1 FROM PageDefinition D INNER JOIN PageMapNodePageDef M on D.PageDefinitionId =M.PageDefinitionId WHERE M.PageMapNodeId = @p_toNodeId  And SiteId = @p_siteId  
	DECLARE @p_NewPageId uniqueidentifier
	SET @p_NewPageId = @p_pageId
	IF @p_copy = 2 OR @p_copy = 1 -- copy as new or copy  
	BEGIN  
		DECLARE @TitlePrefix nvarchar(25)
		DECLARE @UrlPrefix nvarchar(25)
		IF @p_copy = 2  
		BEGIN
			SET @p_NewPageId = newid()  
			SET @TitlePrefix = 'Copy Of '
			SET @UrlPrefix = 'copy-of-'
		END	
		IF @p_copy=2
		BEGIN
		 		 
			INSERT INTO PageDefinition(PageDefinitionId, SiteId, TemplateId, Title, Description, 
			PublishDate, PageStatus, WorkflowState, PublishCount, FriendlyName, WorkflowId,
			CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
			OutputCacheProfileName, ExcludeFromSearch, IsDefault, IsTracked, HasForms, TextContentCounter, CustomAttributes)
			SELECT @p_NewPageId,  @p_siteId, TemplateId, ISNULL(@TitlePrefix, '') + Title, Description,
				PublishDate, PageStatus, WorkflowState, PublishCount,ISNULL(@UrlPrefix, '') + FriendlyName, WorkflowId,
				CreatedBy, CreatedDate, ArchivedDate, StatusChangedDate, AuthorId, EnableOutputCache,
				OutputCacheProfileName, ExcludeFromSearch, IsDefault, IsTracked, HasForms, TextContentCounter, CustomAttributes 
			FROM PageDefinition D
			INNER JOIN PageMapNodePageDef M ON D.PageDefinitionId =M.PageDefinitionId
			WHERE M.PageDefinitionId = @p_pageId AND M.PageMapNodeId =@p_fromNodeId AND SiteId = @p_siteId 
		 
			INSERT INTO [dbo].[PageDetails]([PageId],[PageDetailXML])SELECT @p_NewPageId ,[PageDetailXML] FROM [dbo].[PageDetails] where PageId =@p_pageId
		 
			create table  #tempIds (OldId uniqueidentifier, NewId uniqueidentifier)
			insert into #tempIds SELECT Id, NEWID() FROM [PageDefinitionSegment] WHERE PageDefinitionId = @p_pageId
		 
			INSERT INTO [dbo].[PageDefinitionSegment]
				([Id]
				,[PageDefinitionId]
				,[DeviceId]
				,[AudienceSegmentId]
				,[UnmanagedXml])
			SELECT T.NewId 
				,@p_NewPageId
				,[DeviceId]
				,[AudienceSegmentId]
				,[UnmanagedXml]
			FROM [dbo].[PageDefinitionSegment] PS
				INNER JOIN #tempIds T on PS.Id= T.OldId 
 
			INSERT INTO [PageDefinitionContainer]
			   (
			    [Id]
			   ,PageDefinitionSegmentId 
			   ,[PageDefinitionId]
			   ,[ContainerId]
			   ,[ContentId]
			   ,[InWFContentId]
			   ,[ContentTypeId]
			   ,[IsModified]
			   ,[IsTracked]
			   ,[Visible]
			   ,[InWFVisible]
			   ,DisplayOrder 
			   ,InWFDisplayOrder 
			   ,[CustomAttributes]
			   ,[ContainerName])
			SELECT
				NewId() 
				,T.NewId 
				,@p_NewPageId
				,ContainerId
				,ContentId
				,InWFContentId
				,ContentTypeId
				,IsModified
				,IsTracked
				,Visible
				,InWFVisible
				,DisplayOrder 
			   ,InWFDisplayOrder 
				,CustomAttributes
				,ContainerName
			FROM PageDefinitionContainer PC
				inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId 
			WHERE PageDefinitionId = @p_pageId			
							
			drop table #tempIds
		END
	END
	ELSE IF @p_copy = 0 -- move 
	BEGIN	
		DELETE FROM PageMapNodePageDef Where PageMapNodeId=@p_fromNodeId and PageDefinitionId=@p_pageId	
		
		UPDATE PageMapNode SET TargetId = dbo.GetEmptyGUID(), Target = 0, TargetUrl = ''
			WHERE PageMapNodeId = @p_fromNodeId And SiteId = @p_siteId AND TargetId = @p_pageId		
	END
	IF(not exists(SELECT * FROM PageMapNodePageDef WHERE PageMapNodeId=@p_toNodeId and PageDefinitionId=@p_NewPageId))
		INSERT INTO [dbo].[PageMapNodePageDef]([PageMapNodeId],[PageDefinitionId], DisplayOrder) values( @p_toNodeId, @p_NewPageId, @DisplayOrder)
       
    EXEC [PageDefinition_AdjustDisplayOrder] @p_fromNodeId
    EXEC [PageDefinition_AdjustDisplayOrder] @p_toNodeId
    
	SET @p_outputPageId = @p_NewPageId  
	
	EXEC PageMapBase_UpdateLastModification @p_siteId
	IF @p_copy = 0 -- move 
		EXEC PageMap_UpdateHistory @p_siteId, @p_fromNodeId, 2, 2
		
	EXEC PageMap_UpdateHistory @p_siteId, @p_outputPageId, 8, 1
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetUsersInGroup]'
GO
/*****************************************************
* Name : Membership_GetUsersInGroup
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetUsersInGroup]
(
	@ProductId				uniqueidentifier = NULL,
	@ApplicationId			uniqueidentifier,
	@Id				        uniqueidentifier,
	@MemberType				smallint,
	@IsSystemUser			bit = 0,
	@MemberId				uniqueidentifier = NULL, --either userid or groupid.
	@WithProfile			bit = 0,
	@PageNumber				int,
	@PageSize				int
)
AS
BEGIN

	IF(@Id IS NULL)
	BEGIN
		RAISERROR ('ISNULL||GroupId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	IF @PageNumber IS NULL SET @PageNumber = 0
		
	IF (@MemberType = 1)
	BEGIN
		DECLARE @tbUser TYUser
		INSERT INTO @tbUser
		SELECT * FROM [dbo].[User_GetUsersInGroup] 
			(
				@ProductId, 
				@ApplicationId,
				@Id,
				@MemberType,
				@MemberId,
				@IsSystemUser
			)

		EXEC [dbo].[Membership_BuildUser] 
			@tbUser = @tbUser, 
			@SortBy = NULL, 
			@SortOrder = NULL,
			@PageNumber = @PageNumber,
			@PageSize = @PageSize
	END
	ELSE IF (@MemberType = 2) -- Group
	BEGIN
		DECLARE @PageLowerBound int
		DECLARE @PageUpperBound int
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1
		ELSE
			SET @PageUpperBound = 0

		;WITH ResultEntries AS
		(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY g.Title) AS RowNumber,
				g.ProductId,
				ug.ApplicationId,
				Id,
				Title,
				Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				Status,
				ExpiryDate,
				g.GroupType
			FROM dbo.USMemberGroup ug 
				INNER JOIN dbo.USGroup g ON g.Id = ug.MemberId	
			WHERE Status <> dbo.GetDeleteStatus() AND
				ug.GroupId = @Id AND 
				ug.MemberId = ISNULL(@MemberId,ug.MemberId)	AND
				(g.ApplicationId = g.ProductId OR g.ApplicationId IN (Select SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, @IsSystemUser,0))) AND
				(g.ExpiryDate is null OR g.ExpiryDate >= GETUTCDATE()) AND
				g.ProductId = ISNULL(@ProductId,g.ProductId)
		)

		SELECT DISTINCT ProductId,
			ApplicationId,
			Id,
			Title,
			Description,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			Status,
			ExpiryDate,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			GroupType
		FROM ResultEntries g
			LEFT JOIN VW_UserFullName FN on FN.UserId = g.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = g.ModifiedBy
		WHERE (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
	END 
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[UploadContactData_GetContactsAdded]'
GO
/*****************************************************
* Name : UploadContactData_GetContactsAdded
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[UploadContactData_GetContactsAdded] 
(
--********************************************************************************
	@UploadHistoryId uniqueidentifier
--********************************************************************************
)
AS


Select ImportedContacts, AddedToDistributionList, TotalRecords, DuplicateRecords, ErrorRecords,ExistingRecords, UpdatedContacts from UploadContactData where  UploadHistoryId=@UploadHistoryId


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_FindUsersByProfileProperty]'
GO
/*****************************************************
* Name : Membership_FindUsersByProfileProperty
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Stored procedure
--select * from USMemberProfile
ALTER PROCEDURE [dbo].[Membership_FindUsersByProfileProperty]
--********************************************************************************
-- PARAMETERS

	@ProductId				uniqueidentifier = NULL,
	@ApplicationId			uniqueidentifier,
	@Status					int = NULL,
	@IsSystemUser			bit = 1,
	@PropertyName			nvarchar(256),
	@PropertyValue			nvarchar(1024),
	@DataType				nvarchar(50)
--********************************************************************************
AS
BEGIN
    
--********************************************************************************
-- Variable declaration
--********************************************************************************
	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc = GetUtcDate();
	Declare @tempUsers TABLE 
		(
				Id	uniqueidentifier, 
				UserName nvarchar(256),  
				Email nvarchar(512),
				PasswordQuestion	nvarchar(512),  
				IsApproved	bit,
				CreatedDate	datetime,
				LastLoginDate datetime,
				LastActivityDate	datetime,
				LastPasswordChangedDate	datetime,
				IsLockedOut	bit,
				LastLockoutDate datetime
		)


	Insert into @tempUsers
	SELECT 
			u.Id, 
			u.UserName,  
			m.Email,
			m.PasswordQuestion, 
			m.IsApproved,
			u.CreatedDate,
			m.LastLoginDate,
			u.LastActivityDate,
			m.LastPasswordChangedDate,
			m.IsLockedOut,
			m.LastLockoutDate
			FROM   dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s,dbo.USMemberProfile p 
			WHERE	
					Isnull(CONVERT(varchar,u.ExpiryDate,101),convert(varchar,'12/31/9999',101))  >=CONVERT(varchar,getutcdate(),101) AND
					(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))) AND 
					--s.SiteId = ISNULL(@ApplicationId,SiteId) AND 
					s.IsSystemUser	= @IsSystemUser	AND
					(
						(@Status IS NULL AND u.Status <> dbo.GetDeleteStatus()) OR
						(@Status IS NOT NULL AND @Status = u.Status )
					)	
					AND
					u.Id = p.UserId AND
					u.Id = m.UserId AND
					u.Id = s.UserId AND
					(
						LOWER(p.PropertyName) = LOWER(@PropertyName)	AND 
						(CASE 
							WHEN LOWER(@DataType)='int' THEN p.PropertyValueString
							WHEN LOWER(@DataType)='datetime' THEN CONVERT(varchar(10), p.PropertyValueString,110)
							ELSE LOWER(p.PropertyValueString)
						 END) = 
						(CASE 
							WHEN LOWER(@DataType)='int' THEN @PropertyValue
							WHEN LOWER(@DataType)='datetime' THEN CONVERT(varchar(10), @PropertyValue,110)
							ELSE LOWER(@PropertyValue)
						 END)
					)
					AND
					s.ProductId = ISNULL(@ProductId,s.ProductId)

		SELECT	UserId ,             
				PropertyName,
				PropertyValueString, 
				PropertyValuesBinary,
				LastUpdatedDate
		FROM 	dbo.USMemberProfile p, @tempUsers t
		WHERE	p.UserId = t.Id AND LOWER(p.PropertyName) = LOWER(@PropertyName)


	RETURN 0
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PaymentCreditCard_GetByPaymentId]'
GO
/*****************************************************
* Name : PaymentCreditCard_GetByPaymentId
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[PaymentCreditCard_GetByPaymentId](@PaymentId uniqueidentifier)
AS
BEGIN
	SELECT	
		Id,
		PaymentInfoId,
		ParentPaymentInfoId,
		PaymentId,
		BillingAddressId,
		Phone,
		ExternalProfileId
	FROM PTPaymentCreditCard
	WHERE PaymentId=@PaymentId

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetUsersInGroup_Count]'
GO
/*****************************************************
* Name : Membership_GetUsersInGroup_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetUsersInGroup_Count]
(
--********************************************************************************
-- PARAMETERS
	@ProductId				uniqueidentifier = NULL,
	@ApplicationId			uniqueidentifier,
	@Id				        uniqueidentifier,
	@MemberType				smallint,
	@IsSystemUser			bit = 0,
	@MemberId				uniqueidentifier = NULL
--********************************************************************************
)
AS
BEGIN

	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc = GetUtcDate();

	IF (@MemberType = 1)
	BEGIN
		SELECT 
				COUNT(u.Id)
		FROM
				dbo.USMemberGroup ug, dbo.USUser u, dbo.USSiteUser s, dbo.USMembership m
		WHERE	ug.GroupId = @Id AND 
				ug.MemberId = u.Id AND
				u.Id = m.UserId AND
				m.UserId = s.UserId	AND
				--s.SiteId = @ApplicationId AND
				s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
				ug.MemberId = ISNULL(@MemberId,ug.MemberId) AND
				s.IsSystemUser = @IsSystemUser	AND
				m.IsApproved = 1	AND -- Checks whether the user is approved.
				m.IsLockedOut = 0	AND -- Checks whether the user is locked out.
				(u.ExpiryDate is null OR u.ExpiryDate >= @CurrentTimeUtc	)AND
				s.ProductId = ISNULL(@ProductId, s.ProductId)
	END
	ELSE IF(@MemberType = 2)
	BEGIN
		SELECT COUNT(g.Id)
		FROM	dbo.USGroup g, dbo.USMemberGroup ug
		WHERE	ug.MemberId = g.Id	AND
				ug.MemberId = ISNULL(@MemberId,ug.MemberId) AND
				ug.GroupId = @Id	AND
				g.ApplicationId = @ApplicationId	AND
				(g.ExpiryDate is null OR g.ExpiryDate >= @CurrentTimeUtc	) AND
				g.ProductId = ISNULL(@ProductId, g.ProductId)
				
	END
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetExpiryDate]'
GO
/*****************************************************
* Name : Membership_GetExpiryDate
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Membership_GetExpiryDate 'CDD38906-6071-4DD8-9FB6-508C38848C61','798613EE-7B85-4628-A6CC-E17EE80C09C5'
--exec Membership_GetExpiryDate '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4','798613EE-7B85-4628-A6CC-E17EE80C09C5'

ALTER PROCEDURE [dbo].[Membership_GetExpiryDate]
--********************************************************************************
-- PARAMETERS
	@ApplicationId		uniqueidentifier,
	@UserId				uniqueidentifier
--********************************************************************************

AS
BEGIN
	DECLARE @ExpiryDate DATETIME
	IF(@ApplicationId IS NOT NULL)
	BEGIN
		SELECT @ExpiryDate = u.ExpiryDate
		FROM USUser u inner join dbo.USSiteUser s on u.Id=s.UserId
		WHERE u.Id = @UserId	 and s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
		
		SELECT @ExpiryDate
	END
	ELSE
	BEGIN
		RAISERROR ('ISNULL||ApplicationId', 16, 1 )
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	RETURN @@ROWCOUNT
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_CopyMenusAndPages]'
GO
/*****************************************************
* Name : Site_CopyMenusAndPages
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[Site_CopyMenusAndPages]
(@SiteId uniqueidentifier,
@MicroSiteId uniqueidentifier,
@ModifiedBy  uniqueidentifier,
@CopiedAllUsersFromMaster bit,
@PageImportOptions int =2
)
AS
BEGIN

if(@PageImportOptions = 0) -- Create default nodes root node and unassigned when selected dont import menus and pages
begin
	Insert into PageMapBase (SiteId, ModifiedDate)
		Values(@MicroSiteId, GETUTCDATE())
	
	DECLARE @pageMapNodeXml NVARCHAR(max)
	DECLARE @pageMapNodeId uniqueidentifier
	SET @pageMapNodeId = @MicroSiteId
	declare @SiteTitle nvarchar(512)
	set @SiteTitle = (select DisplayTitle  from PageMapNode where PageMapNodeId =@SiteId)
							 
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" description="' + @SiteTitle + '"  
		   displayTitle="' + @SiteTitle + '" friendlyUrl="' + replace(replace(replace(@SiteTitle,' ',''),'''',''),'"','') + '" menuStatus="0"   
		   targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
		   createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
		   modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
		   propogateSecurityLevels="True" inheritSecurityLevels="False" propogateRoles="True"   
		   inheritRoles="False" roles="" securityLevels="" locationIdentifier="" />'  						 

	EXEC PageMapNode_Save @Id =@pageMapNodeId,
			@SiteId =@MicroSiteId,
			@ParentNodeId = NULL,
			@PageMapNode= @pageMapNodeXml,
			@CreatedBy = @ModifiedBy
			
	SET @pageMapNodeId = NEWID()
	SET @pageMapNodeXml = '<pageMapNode id="' + CAST(@pageMapNodeId AS CHAR(36)) + '" 
						 displayTitle="Unassigned" friendlyUrl="Unassigned" description="Unassigned" menuStatus="0" 
						 parentId="' + CAST(@MicroSiteId AS CHAR(36)) + '" 
					     targetId="00000000-0000-0000-0000-000000000000"  target="0" targetUrl=""   
						 createdBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     createdDate="2012-05-31 12:13:57.073" modifiedBy="798613EE-7B85-4628-A6CC-E17EE80C09C5"   
					     modifiedDate="2012-05-31 12:13:57.073" propogateWorkFlow="True" inheritWorkFlow="False"   
						 propogateSecurityLevels="True" inheritSecurityLevels="False" 
						 propogateRoles="True" inheritRoles="False" roles="" securityLevels="" />'

	EXEC PageMapNode_Save @Id = @pageMapNodeId,
			@SiteId = @MicroSiteId,
			@ParentNodeId = @MicroSiteId,
			@PageMapNode = @pageMapNodeXml,
			@CreatedBy = @ModifiedBy


end
else
begin


	declare @ImportMenusOnly bit 
	Set @ImportMenusOnly =0
	declare @MakePageStatusAsDraft bit 
	Set @MakePageStatusAsDraft = 1

	if(@PageImportOptions = 1) set @ImportMenusOnly = 1  -- Import only menus
	if(@PageImportOptions = 3) set @MakePageStatusAsDraft = 0 -- @PageImportOptions =3 Import menus and pages, page status same as master site page status, @PageImportOptions =2 import all pages as created page status and workflow state as draft

	IF (Select count(*)  FROM [dbo].[PageMapNode]   Where SiteId=@MicroSiteId ) =0
	BEGIN

		INSERT INTO [dbo].[PageMapNode]
			   ([PageMapNodeId]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
	           
			   ,[SiteId]
			   ,[ExcludeFromSiteMap]
			   ,[Description]
			   ,[DisplayTitle]
			   ,[FriendlyUrl]
			   ,[MenuStatus]
			   ,[TargetId]
			   ,[Target]
			   ,[TargetUrl]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PropogateWorkflow]
			   ,[InheritWorkflow]
			   ,[PropogateSecurityLevels]
			   ,[InheritSecurityLevels]
			   ,[PropogateRoles]
			   ,[InheritRoles]
			   ,[Roles]
			   ,[LocationIdentifier]
			   ,[HasRolloverImages]
			   ,[RolloverOnImage]
			   ,[RolloverOffImage]
			   ,[IsCommerceNav]
			   ,[AssociatedQueryId]
			   ,[DefaultContentId]
			   ,[AssociatedContentFolderId]
			   ,[CustomAttributes]
			   ,[HiddenFromNavigation]
			   ,SourcePageMapNodeId)
			   SELECT  case when parentId is null then @MicroSiteId else NEWID()end
		  ,[ParentId]
		  ,[LftValue]
		  ,[RgtValue]
	      
		  ,@MicroSiteId
		  ,[ExcludeFromSiteMap]
		  ,[Description]
		  ,[DisplayTitle]
		  ,[FriendlyUrl]
		  ,[MenuStatus]
		  ,[TargetId]
		  ,[Target]
		  ,[TargetUrl]
		  ,@ModifiedBy
		  ,GETUTCDATE()
		  ,@ModifiedBy
		  ,GETUTCDATE()
		  ,[PropogateWorkflow]
		  ,[InheritWorkflow]
		  ,[PropogateSecurityLevels]
		  ,[InheritSecurityLevels]
		  ,[PropogateRoles]
		  ,[InheritRoles]
		  ,[Roles]
		  ,[LocationIdentifier]
		  ,[HasRolloverImages]
		  ,[RolloverOnImage]
		  ,[RolloverOffImage]
		  ,[IsCommerceNav]
		  ,[AssociatedQueryId]
		  ,[DefaultContentId]
		  ,[AssociatedContentFolderId]
		  ,[CustomAttributes]
		  ,[HiddenFromNavigation]
		  ,PageMapNodeId
	  FROM [dbo].[PageMapNode]
	   Where SiteId=@SiteId
	   AND PageMapNodeId not in    
	   (select distinct P.PageMapNodeId 
	 FROM [dbo].[PageMapNode] P
	 cross  join PageMapNode DP 
	   Where P.SiteId=@SiteId and P.LftValue between DP.LftValue and DP.RgtValue
	   AND DP.PageMapNodeId   in 
	   (
		   select top 4 Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		Where Name in ('CommerceNavRootNodeId', 'ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId
		And Value is not null
		AND rtrim(ltrim(Value)) !=''
		union 
		select PageMapNodeId from  PageMapNode where lower(DisplayTitle) = 'storefront' and SiteId=@SiteId 
	   )   
	   )
	  
	  
	    
	--remove commerceproduct type and marketierpages menu group   
	delete from PageMapNode where SiteId = @MicroSiteId  and  SourcePageMapNodeId in 
	(select ParentId from PageMapNode where PageMapNodeId in 
		(select Top 2 Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		Where Name in ('ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId
		And Value is not null
		AND rtrim(ltrim(Value)) !=''))
		
		--remove commerceproduct type  and marketierpages menu group   subnodes if any exist(generally it should not)
	delete from PageMapNode where SiteId = @MicroSiteId  and  ParentId in 
	(select ParentId from PageMapNode where PageMapNodeId in 
		(select Top 2 Cast(Value as Uniqueidentifier) from STSettingType T
		INNER JOIN STSiteSetting S on T.Id = S.SettingTypeId
		Where Name in ('ProductTypePageMapNodeId','CampaignPageMapNodeId')
		AND SiteId=@SiteId
		And Value is not null
		AND rtrim(ltrim(Value)) !=''))

		
	    
	   Update  M Set M.ParentId = P.PageMapNodeId
	   FROM PageMapNode M 
	   INNER JOIN PageMapNode P ON P.SourcePageMapNodeId = M.ParentId
	   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
	   
	   UPdate PN SET PN.TargetId=T.PageMapNodeId
		FROM PageMapNode PN
		INNER JOIN PageMapNode T ON PN.TargetId = T.SourcePageMapNodeId
		Where PN.Target='2' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

	 --  if(@CopiedAllUsersFromMaster = 1)
	 --  begin
		--   INSERT INTO [dbo].[PageMapNodeWorkflow]
		--		   ([Id]
		--		   ,[PageMapNodeId]
		--		   ,[WorkflowId]
		--		   ,[CustomAttributes])
		--SELECT NEWID()
		--	  ,P.[PageMapNodeId]
		--	  ,[WorkflowId]
		--	  ,W.[CustomAttributes]
		--  FROM [dbo].[PageMapNodeWorkflow] W
		--  INNER JOIN PageMapNode P ON W.PageMapNodeId = P.SourcePageMapNodeId
		--  Where P.SiteId = @MicroSiteId
	 -- end
	  
	  INSERT INTO [dbo].[USSecurityLevelObject]
			   ([ObjectId]
			   ,[ObjectTypeId]
			   ,[SecurityLevelId])
	  SELECT P.PageMapNodeId
			   ,ObjectTypeId
			   ,SecurityLevelId
	FROM [USSecurityLevelObject] SO
	INNER JOIN PageMapNode P ON SO.ObjectId = P.SourcePageMapNodeId
	 Where P.SiteId = @MicroSiteId

	 --import shared workflow mapping of pagemapnode and workflow
	INSERT INTO [dbo].[PageMapNodeWorkflow]
           ([Id]
           ,[PageMapNodeId]
           ,[WorkflowId]
           ,[CustomAttributes])
	select NEWID(),
		P.PageMapNodeId,
		PW.WorkflowId,
		null
	FROM [dbo].[PageMapNodeWorkflow] PW
		INNER JOIN PageMapNode P ON PW.PageMapNodeId = P.SourcePageMapNodeId
		inner join WFWorkflow W on PW.WorkflowId = W.Id 
		 Where P.SiteId = @MicroSiteId  and W.IsShared=1 



	if(@CopiedAllUsersFromMaster = 1)
	   begin
	   -- This is updating the custome permission page map node id
		  INSERT INTO [dbo].[USMemberRoles]
				   ([ProductId]
				   ,[ApplicationId]
				   ,[MemberId]
				   ,[MemberType]
				   ,[RoleId]
				   ,[ObjectId]
				   ,[ObjectTypeId]
				   ,[Propagate]
				   )
		SELECT Distinct [ProductId]
			  ,@MicroSiteId
			  ,[MemberId]
			  ,[MemberType]
			  ,[RoleId]
			  ,P.PageMapNodeId
			  ,[ObjectTypeId]
			  ,[Propagate]
		  FROM [dbo].[USMemberRoles]M
		INNER JOIN PageMapNode P ON M.ObjectId = P.SourcePageMapNodeId
		 Where P.SiteId = @MicroSiteId 
		 and MemberId not in(select Id from USGroup where GroupType=2) -- Excluding custom groups, custom groups are not shared with variant sites
	 end 
	 
	 
	 if exists(select 1 from  [dbo].PageMapBase B INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId Where P.SiteId = @MicroSiteId)
	begin
	 INSERT INTO [dbo].[PageMapBase]
					   ([SiteId]
					   ,[HomePageDefinitionId]
					   ,[ModifiedDate]
					   ,[ModifiedBy]
					   ,[LastPageMapModificationDate])
			SELECT top 1 @MicroSiteId
				  ,P.PageMapNodeId
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,[LastPageMapModificationDate]
			FROM [dbo].PageMapBase B
			INNER JOIN PageMapNode P ON B.HomePageDefinitionId= P.SourcePageMapNodeId
			 Where P.SiteId = @MicroSiteId
			 end
		else	
			Insert into PageMapBase (SiteId, ModifiedDate,ModifiedBy)	Values(@MicroSiteId, GETUTCDATE(),@ModifiedBy) 
	 
	 if(@ImportMenusOnly = 0)
	 begin
	 
			INSERT INTO [dbo].[PageDefinition]
					   ([PageDefinitionId]
					   ,[TemplateId]
					   ,[SiteId]
					   ,[Title]
					   ,[Description]
					   ,[PageStatus]
					   ,[WorkflowState]
					   ,[PublishCount]
					   ,[PublishDate]
					   --,[DisplayOrder]
					   ,[FriendlyName]
					   ,[WorkflowId]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate]
					   ,[ArchivedDate]
					   ,[StatusChangedDate]
					   ,[AuthorId]
					   ,[EnableOutputCache]
					   ,[OutputCacheProfileName]
					   ,[ExcludeFromSearch]
					   ,[IsDefault]
					   ,[IsTracked]
					   ,[HasForms]
					   ,[TextContentCounter]
					   ,[SourcePageDefinitionId]
					   ,[CustomAttributes]
					   )
			SELECT NEWID()
				  ,[TemplateId]
				  ,@MicroSiteId
				  ,[Title]
				  ,D.[Description]
				  ,case when @MakePageStatusAsDraft=1 then dbo.GetActiveStatus()  else [PageStatus]  end
				  ,case when @MakePageStatusAsDraft=1 then  2  else case when WorkflowState =1 then WorkflowState else 2 end end
				  ,case when @MakePageStatusAsDraft=1 then 0 else 1 end
				  ,[PublishDate]
				  --,[DisplayOrder]
				  ,[FriendlyName]
				  ,dbo.GetEmptyGUID()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,[ArchivedDate]
				  ,[StatusChangedDate]
				  ,@ModifiedBy
				  ,[EnableOutputCache]
				  ,[OutputCacheProfileName]
				  ,[ExcludeFromSearch]
				  ,[IsDefault]
				  ,[IsTracked]
				  ,[HasForms]
				  ,[TextContentCounter]
				  ,PageDefinitionId
				  ,D.[CustomAttributes]
			      
			  FROM [dbo].[PageDefinition] D
			Where D.SiteId =@SiteId and PageDefinitionId in (select PageDefinitionId  from PageMapNodePageDef PMD inner join PageMapNode PM on PMD.PageMapNodeId = PM.SourcePageMapNodeId where PM.SiteId =@MicroSiteId)


			insert into PageMapNodePageDef
			select distinct  c.PageMapNodeId ,a.PageDefinitionId, b.DisplayOrder from [PageDefinition] a , PageMapNodePageDef b  , PageMapNode c
			where a.SiteId  = @MicroSiteId and a.SourcePageDefinitionId = b.PageDefinitionId 
			and c.SourcePageMapNodeId = b.PageMapNodeId and 
			c.SiteId = @MicroSiteId



			UPdate PN SET PN.TargetId=T.PageDefinitionId
			FROM PageMapNode PN
			INNER JOIN PageDefinition T ON PN.TargetId = T.SourcePageDefinitionId
			Where PN.Target='1' AND PN.SiteId =@MicroSiteId AND T.SiteId =@MicroSiteId

			create table  #tempIds (OldId uniqueidentifier, NewId uniqueidentifier)
			insert into #tempIds SELECT PS.Id, NEWID() FROM  [dbo].[PageDefinitionSegment] PS INNER JOIN PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 	  
			 
			 INSERT INTO [dbo].[PageDefinitionSegment]
						   ([Id]
						   ,[PageDefinitionId]
						   ,[DeviceId]
						   ,[AudienceSegmentId]
						   ,[UnmanagedXml])
						   SELECT T.NewId 
							  ,P.PageDefinitionId 
							  ,[DeviceId]
							  ,[AudienceSegmentId]
							  ,[UnmanagedXml]
						  FROM [dbo].[PageDefinitionSegment] PS 
						  inner join #tempIds T on PS.Id= T.OldId 
						  inner join PageDefinition P ON PS.PageDefinitionId=P.SourcePageDefinitionId
						  Where P.SiteId = @MicroSiteId
						  
						  INSERT INTO [PageDefinitionContainer]
							   (
								[Id]
							   ,PageDefinitionSegmentId 
							   ,[PageDefinitionId]
							   ,[ContainerId]
							   ,[ContentId]
							   ,[InWFContentId]
							   ,[ContentTypeId]
							   ,[IsModified]
							   ,[IsTracked]
							   ,[Visible]
							   ,[InWFVisible]
							   ,DisplayOrder 
							   ,InWFDisplayOrder 
							   ,[CustomAttributes]
							   ,[ContainerName])
							SELECT
								 NewId() 
								 ,T.NewId 
								,P.PageDefinitionId
								,ContainerId
								,ContentId
								,InWFContentId
								,ContentTypeId
								,IsModified
								,PC.IsTracked
								,Visible
								,InWFVisible
								,PC.DisplayOrder 
							   ,PC.InWFDisplayOrder 
								,PC.CustomAttributes
								,ContainerName
							FROM PageDefinitionContainer PC
							inner join #tempIds T on PC.PageDefinitionSegmentId= T.OldId
							inner join PageDefinition P ON PC.PageDefinitionId=P.SourcePageDefinitionId 
							Where P.SiteId = @MicroSiteId
						  
			truncate table #tempIds

				

			INSERT INTO [dbo].[PageDetails]
					   ([PageId]
					   ,[PageDetailXML])
			  SELECT 
				 P.PageDefinitionId
					   ,[PageDetailXML]
			  FROM [dbo].[PageDetails] PD
			INNER JOIN PageDefinition P ON P.SourcePageDefinitionId = PD.PageId
			 Where P.SiteId = @MicroSiteId

			  INSERT INTO [dbo].[USSecurityLevelObject]
					   ([ObjectId]
					   ,[ObjectTypeId]
					   ,[SecurityLevelId])
			  SELECT P.PageDefinitionId
					   ,ObjectTypeId
					   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN PageDefinition P ON SO.ObjectId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageScript]
					   ([PageDefinitionId]
					   ,[ScriptId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[ScriptId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageScript] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[SIPageStyle]
					   ([PageDefinitionId]
					   ,[StyleId]
					   ,[CustomAttributes])
				SELECT P.[PageDefinitionId]
				  ,[StyleId]
				  ,PS.[CustomAttributes]
			  FROM [dbo].[SIPageStyle] PS
			INNER JOIN PageDefinition P ON PS.PageDefinitionId = P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId

			INSERT INTO [dbo].[COTaxonomyObject]
					   ([Id]
					   ,[TaxonomyId]
					   ,[ObjectId]
					   ,[ObjectTypeId]
					   ,[SortOrder]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
			 SELECT NEWID()
				  ,[TaxonomyId]
				  ,P.PageDefinitionId
				  ,[ObjectTypeId]
				  ,[SortOrder]
				  ,[Status]
				  ,@ModifiedBy
				  ,GETUTCDATE()
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[COTaxonomyObject] OT
			INNER JOIN PageDefinition P ON OT.ObjectId= P.SourcePageDefinitionId
			 Where P.SiteId = @MicroSiteId
			 
			 
			 
			 
			 --create microsite pages version from master pages latest version
			insert into #tempIds select Id, NEWID() from 
			(Select distinct Id,ObjectId
			from (
			Select ROW_NUMBER () Over (Partition By ObjectId order by CreatedDate desc) RowNum,ObjectId,Id,XMLString, ObjectStatus, VersionStatus,Comments, TriggeredObjectId,TriggeredObjectTypeId 
			FROM VEVersion 
			Where ObjectTypeId =8
			)V
			Where V.RowNum =1) LV inner  join PageDefinition pd on LV.ObjectId =pd.SourcePageDefinitionId where pd.SiteId=@MicroSiteId




			INSERT INTO [dbo].[VEVersion]
					   ([Id]
					   ,[ApplicationId]
					   ,[ObjectTypeId]
					   ,[ObjectId]
					   ,[CreatedDate]
					   ,[CreatedBy]
					   ,[VersionNumber]
					   ,[RevisionNumber]
					   ,[Status]
					   ,[ObjectStatus]
					   ,[VersionStatus]
					   ,[XMLString]
					   ,[Comments]
					   ,[TriggeredObjectId]
					   ,[TriggeredObjectTypeId])          
					   select T.NewId,
					   @MicroSiteId ,
					   8, 
					   pd.PageDefinitionId, 
					   GETUTCDATE(),
					   @ModifiedBy ,
					   1 ,
					   0,
					   'false' ,
					   LV.ObjectStatus,
					   LV.VersionStatus, 
					   case when @MakePageStatusAsDraft=1 then dbo.UpdatePageXml(LV.XMLString, pd.PageDefinitionId,pd.WorkflowId,2,dbo.GetActiveStatus(),0, pd.CreatedBy,pd.CreatedDate, pd.AuthorId,pd.SiteId,pd.SourcePageDefinitionId)
					   else dbo.UpdatePageXml(LV.XMLString, pd.PageDefinitionId,pd.WorkflowId, case when pd.WorkflowState =1 then pd.WorkflowState else 2 end,pd.PageStatus,1 ,pd.CreatedBy,pd.CreatedDate, pd.AuthorId,pd.SiteId,pd.SourcePageDefinitionId) end, 
					   LV.Comments,
					   LV.TriggeredObjectId,
					   LV.TriggeredObjectTypeId       
			  from VEVersion LV
			 inner join #tempIds T on LV.Id= T.OldId 
			 inner  join PageDefinition pd on LV.ObjectId =pd.SourcePageDefinitionId 
			 
			  where pd.SiteId=@MicroSiteId
			 
			 create table #VPSIds (OldId uniqueidentifier, NewId uniqueidentifier)
			 
			 insert into #VPSIds select Id, NEWID() from [dbo].[VEPageDefSegmentVersion] VPS inner join #tempIds T on VPS.VersionId= T.OldId
			  

			    
			 
			 INSERT INTO [dbo].[VEPageDefSegmentVersion]
					   ([Id]
					   ,[VersionId]
					   ,[PageDefinitionId]
					   ,[MinorVersionNumber]
					   ,[DeviceId]
					   ,[AudienceSegmentId]   
					   ,[ContainerContentXML]        
					   ,[UnmanagedXml]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   )
				SELECT TPS.NewId 
				  ,V.Id
				  ,V.ObjectId 
				  ,[MinorVersionNumber]
				  ,[DeviceId]
				  ,[AudienceSegmentId]
				  ,[ContainerContentXML]  
				  ,[UnmanagedXml]
				  ,@ModifiedBy
				  ,GETUTCDATE()
			  FROM [dbo].[VEPageDefSegmentVersion] VPS
			  inner join #VPSIds TPS on VPS.Id= TPS.OldId 
			  inner join #tempIds T on VPS.VersionId = T.OldId 
			  inner join VEVersion V on T.NewId = V.Id  
			  

			 truncate table #tempIds 

			 insert into #tempIds select VC.Id, newId() from dbo.VEContentVersion VC inner join [dbo].[VEPageContentVersion] VPC on VC.Id= VPC.ContentVersionId  inner join #VPSIds VPS on VPC.PageDefSegmentVersionId = VPS.OldId



			 INSERT INTO [dbo].[VEContentVersion]
					   ([Id]
					   ,[ContentId]
					   ,[XMLString]
					   ,[Status]
					   ,[CreatedBy]
					   ,[CreatedDate]
					   ,[ModifiedBy]
					   ,[ModifiedDate])
				 SELECT T.NewId 
				  ,[ContentId]
				  ,[XMLString]
				  ,[Status]
				  ,@ModifiedBy 
				  ,GETUTCDATE()
				  ,@ModifiedBy 
				  ,GETUTCDATE()
			  FROM [dbo].[VEContentVersion] VC
			  inner join #tempIds T on VC.Id= T.OldId 


			INSERT INTO [dbo].[VEPageContentVersion]
					   ([Id]
					   ,[ContentVersionId]
					   ,[IsChanged]
					   ,[PageDefSegmentVersionId])
			   
			SELECT NewId()
				  ,T.NewId
				  ,[IsChanged]
				  ,VPS.NewId
			  FROM [dbo].[VEPageContentVersion] VPC
			inner join #VPSIds VPS on VPC.PageDefSegmentVersionId = VPS.OldId 
			inner join #tempIds T on VPC.ContentVersionId = T.OldId

			drop table #VPSIds 
			drop table #tempIds
		End

	 END
	
 end

END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[RegenerateLftRgt]'
GO
/*****************************************************
* Name : RegenerateLftRgt
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[RegenerateLftRgt](@SiteId uniqueidentifier=null,@key int=2,@backup bit =1)
as
BEGIN
-- 1 PageMapNode
-- 2 COContentStructure
-- 3 COFileStructure
-- 4 COImageStructure
-- 9 COFormStructure
IF @SiteId is NULL
	Set @SiteId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'
Declare @sqlStmt varchar(1000)
Declare @tblName varchar(100)

IF @Key =1
	SET @tblName = 'PageMapNode'
ELSE IF @Key=2
	SET @tblName='COContentStructure'
ELSE IF @Key =3
	SET @tblName='COFileStructure'
ELSE IF @Key =4
	SET @tblName='COImageStructure'
ELSE IF @Key =5
	SET @tblName='HSStructure'
ELSE IF @key =6
	SET @tblName='PRProductType'
ELSE IF @Key =7
	SET @tblName='COTaxonomy'	
ELSE IF @Key =8
	SET @tblName='SISite'		
ELSE IF @Key =9
	SET @tblName='COFormStructure'		
	
if(@backup =1)
begin
if  exists (Select * from sys.tables Where NAME='Regen_backup_'+  replace(replace(replace(cast(getdate() as datetime),'-','_'),' ','_'),':','_')) 
BEGIN
	SET @sqlStmt = 'Drop table Regen_backup_' +  replace(replace(replace(cast(getdate() as datetime),'-','_'),' ','_'),':','_')
	Exec (@sqlStmt)
END
SET @sqlStmt='Select  * into Regen_backup_' +  replace(replace(replace(cast(getdate() as datetime),'-','_'),' ','_'),':','_') +' from '+ @tblName 
EXEC   (@sqlStmt)
end

if  exists (Select * from sys.tables Where NAME='Regen_Work')
	Drop table Regen_Work

	CREATE TABLE Regen_Work
( Id Uniqueidentifier NOT NULL,
 LftValue Bigint NOT NULL,
 RgtValue bigint NOT NULL,
 ParentId uniqueidentifier);

 IF @tblName = 'PageMapNode'
 INSERT INTO Regen_Work
	Select  PageMapNodeId,LftValue,RgtValue,ParentId from PageMapNode  Where SiteId=@SiteId
 ELSE IF @tblName = 'COContentStructure'
	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COContentStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'COFileStructure'
 	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId from COFileStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'COImageStructure'
 	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COImageStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'HSStructure'
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from HSStructure  Where SiteId=@SiteId
 ELSE IF @tblName = 'PRProductType'
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from PRProductType
ELSE IF @tblName = 'COTaxonomy'
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COTaxonomy  Where SiteId=@SiteId	
ELSE IF @tblName = 'SISite'
BEGIN
 INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,case when ParentSiteId IS NULL then '00000000-0000-0000-0000-000000000000' else ParentSiteId END ParentId  
	from SISite Where MasterSiteId=@SiteId
	Order By Title
END	
ELSE IF @tblName = 'COFormStructure'
BEGIN
	INSERT INTO Regen_Work
	Select  Id,LftValue,RgtValue,ParentId  from COFormStructure  Where SiteId=@SiteId
END	


if exists (Select * from sys.tables Where NAME='Regen_Temp')
DROP TABLE Regen_Temp;

CREATE TABLE Regen_Temp
(stack_top bigint NOT NULL,
 Id Uniqueidentifier NOT NULL,
 lft Bigint NOT NULL,
 rgt bigint);


BEGIN

DECLARE @lft_rgt bigint, @stack_pointer bigint, @max_lft_rgt bigint;

SET @max_lft_rgt = 2 * (SELECT COUNT(*) FROM Regen_Work);

INSERT INTO Regen_Temp
SELECT top 1 1, Id, 1, @max_lft_rgt 
  FROM Regen_Work
 Order By LftValue,ParentId;

SET @lft_rgt = 2;
SET @Stack_pointer = 1;

DELETE FROM Regen_Work 
 WHERE Id in (select Id from Regen_Temp)

-- The Regen_Temp is now loaded and ready to use

WHILE (@lft_rgt < @max_lft_rgt)
BEGIN 
 IF EXISTS (SELECT *
              FROM Regen_Temp AS S1, Regen_Work AS T1
             WHERE S1.Id = T1.ParentId
               AND S1.stack_top = @stack_pointer)
    BEGIN -- push when stack_top has subordinates and set lft value
      INSERT INTO Regen_Temp
      SELECT top 1 (@stack_pointer + 1), T1.Id, @lft_rgt, NULL
        FROM Regen_Temp AS S1, Regen_Work AS T1
       WHERE S1.Id = T1.ParentId
         AND S1.stack_top = @stack_pointer
		
		Order By T1.LftValue;

      -- remove this row from Regen_Work 
      DELETE FROM Regen_Work
       WHERE Id IN (SELECT Id
                        FROM Regen_Temp
WHERE stack_top = @stack_pointer + 1 );
      SET @stack_pointer = @stack_pointer + 1;
    END -- push
    ELSE
    BEGIN  -- pop the Regen_Temp and set rgt value
      UPDATE Regen_Temp
         SET rgt = @lft_rgt,
             stack_top = -stack_top
       WHERE stack_top = @stack_pointer 
		--AND SiteId =@SiteId
      SET @stack_pointer = @stack_pointer - 1;
    END; -- pop
  SET @lft_rgt = @lft_rgt + 1;
  END; -- if
END; -- while
 IF @tblName = 'PageMapNode'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from PageMapNode Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM PageMapNode M
		INNER JOIN Regen_Temp R ON M.PageMapNodeId = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating PageMapNode in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'COContentStructure'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COContentStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COContentStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COContentStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'COFileStructure'
 	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COFileStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COFileStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating  COFileStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'COImageStructure'
 	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COImageStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COImageStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COImageStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'HSStructure'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from HSStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM HSStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating HSStructure in Site ' + cast(@SiteId as char(36))
	END
 ELSE IF @tblName = 'PRProductType'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from PRProductType))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM PRProductType M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
	END
	ELSE
	BEGIN
		print 'Error Updating PRProductType in Site ' + cast(@SiteId as char(36))
	END	
	ELSE IF @tblName = 'COTaxonomy'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COTaxonomy Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COTaxonomy M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COTaxonomy in Site ' + cast(@SiteId as char(36))
	END
	ELSE IF @tblName = 'SISite'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from SISite Where MasterSiteId=@SiteId ))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM SISite M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.MasterSiteId=@SiteId
	END
	ELSE
	BEGIN
	select * from Regen_Temp
		print 'Error Updating SISite in Site ' 
	END
	ELSE IF @tblName = 'COFormStructure'
	IF((Select COUNT(*) from Regen_Temp) = (Select COUNT(*) from COFormStructure Where SiteId=@SiteId))
	BEGIN
		Update M SET M.LftValue = R.lft,M.RgtValue = R.rgt
		FROM COFormStructure M
		INNER JOIN Regen_Temp R ON M.Id = R.Id
		Where M.SiteId =@SiteId
	END
	ELSE
	BEGIN
		print 'Error Updating COFormStructure in Site ' + cast(@SiteId as char(36))
	END
Drop Table Regen_Temp
Drop table Regen_Work;

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_ByLOC_Save]'
GO
/*****************************************************
* Name : Payment_ByLOC_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Payment_ByLOC_Save]  
(   
 @Id uniqueidentifier output,
 @LOCId uniqueidentifier,	
 @OrderId uniqueidentifier,
 @PaymentTypeId int,
 @PaymentStatusId int =1,
 @Amount money,
 @CustomerId uniqueidentifier,
 @Description nvarchar(255),
 @ModifiedBy uniqueidentifier  ,
 @OrderPaymentId uniqueidentifier output,
 @IsRefund tinyint = 0
)  
  
AS  
BEGIN       
  
DECLARE @CreatedDate datetime
  set @CreatedDate = GetUTCDate()

Declare @error	  int
Declare @stmt	  varchar(256)	
Declare @sequence int
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
			  SET @Id = newid()  

				Insert into PTPayment 
				(Id
				,PaymentTypeId
				,PaymentStatusId
				,CustomerId
				,Amount
				,Description
				,CreatedDate
				,CreatedBy
				,Status
				,IsRefund
				)
				Values
				(
				@Id
				,@PaymentTypeId
				,@PaymentStatusId
				,@CustomerId
				,@Amount
				,@Description
				,@CreatedDate
				,@ModifiedBy
				,dbo.GetActiveStatus()
				,@IsRefund
			)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

				Insert into PTPaymentLineOfCredit
				(Id,
				 PaymentId,
				 LOCId
				)
				Values
				(newId(),
				  @Id,
				  @LOCId
				)

					select @error = @@error
							if @error <> 0
							begin
								raiserror('DBERROR||%s',16,1,@stmt)
								return dbo.GetDataBaseErrorCode()	
							end

				

				Select @sequence = max(isnull(Sequence,0)) + 1
				From PTOrderPayment
				Where OrderId=@OrderId


				SET @OrderPaymentId  = newId()
				Insert Into PTOrderPayment(Id,OrderId,PaymentId,Amount,Sequence)
				Values(@OrderPaymentId,@OrderId,@Id,@Amount,@sequence)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

	end  
ELSE
	BEGIN
		set @CreatedDate = GetUTCDate()

		UPDATE PTPayment SET 
		PaymentTypeId = @PaymentTypeId, 
		PaymentStatusId = @PaymentStatusId,
		CustomerId = @CustomerId,
		Amount = @Amount,
		Description = @Description,
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @CreatedDate,	
		Status = dbo.GetActiveStatus(),
		IsRefund = @IsRefund
		WHERE Id = @Id


		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end


		UPDATE PTOrderPayment SET Amount = @Amount
		WHERE Id= @OrderPaymentId
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		
		END
END  

Update OROrder Set PaymentTotal =dbo.Order_GetPaymentTotal(@OrderId), RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
Where Id=@OrderId

  
  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_GetCustomersForExport]'
GO
/*****************************************************
* Name : Customer_GetCustomersForExport
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Customer_GetCustomersForExport]  
(  
	@Searchkeyword nvarchar(4000)=null,  
    @SortColumn VARCHAR(25)=null,  
    @PageSize INT=null,   
	@PageNumber INT=1  ,
	@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
  
	Declare @searchText nvarchar(max)
   
	IF @SearchKeyword is null  
		SET @SearchKeyword ='%'  
	ELSE  
		SET @SearchKeyword ='%' + (@Searchkeyword) + '%'  
	SET @searchText = '''' + @Searchkeyword + ''''  
  
	IF @PageSize IS NULL  
		SET @PageSize = 2147483647  
  
	IF @SortColumn IS NULL  
		SET @SortColumn = 'LastName ASC'  
  
	DECLARE @SQL nvarchar(max),@params nvarchar(100)  
  
	SET @params = N'@SIZE INT, @nbr INT,  @Sort VARCHAR(25), @ApplicationId uniqueidentifier'  
  
	SET @SQL = N'  
  
;WITH PagingCTE (Row_ID  
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone
   ,ImageId  
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
	,Country
	,CountryCode
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy
   ,County 
   ,ExternalId
   ,ExternalProfileId
   )  
  
AS  
  
(  
  
SELECT   
     ROW_NUMBER()   
  
            OVER(ORDER BY ' + @SortColumn + '  
           
            ) AS [Row_ID]  
  
     
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State 
   	,StateCode 
	,Country
	,CountryCode
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM vwSearchCustomer  
Where SiteId = @ApplicationId AND 
 (  	
  (LastName) Like ' + @searchText + '  
  OR (FirstName) Like ' + @searchText + '  
  OR (Email) Like ' + @searchText + '  
  OR (AddressLine1) Like ' + @searchText + '  
  OR (City) Like ' + @searchText + '  
  OR (State) Like ' + @searchText + '  
  OR (Zip) Like ' + @searchText + '  
  OR (HomePhone) Like ' + @searchText + '  
  OR (AccountNumber) Like ' + @searchText + ' 
  OR (CompanyName) Like ' +@searchText + ' 
  OR case when Status =1 then ''active'' else ''deactive'' end Like ' + @searchText + '  
 )  
)   
  
SELECT   
	UserName  
   ,Email   
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,State  
   ,StateCode
   ,Zip  
   ,Country  
   ,CountryCode
   ,Phone  
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM PagingCTE pcte  
WHERE Row_ID >= (@SIZE * @nbr) - (@SIZE -1) AND Row_ID <= @SIZE * @nbr'  
  
--print @SQL  
EXEC sp_executesql   
      @SQL,  
      @params,  
      @SIZE = @PageSize,  
      @nbr = @PageNumber,  
      @Sort = @SortColumn ,
      @ApplicationId=@ApplicationId 
  
   
Select count(1) [Count]  
 FROM vwSearchCustomer  
Where SiteId = @ApplicationId
 AND (  
  (LastName) Like  @SearchKeyword   
  OR (FirstName) Like  @SearchKeyword   
  OR (Email) Like  @SearchKeyword   
  OR (AddressLine1) Like @SearchKeyword   
  OR (City) Like @SearchKeyword   
  OR (State) Like @SearchKeyword  
  OR (Zip) Like  @SearchKeyword   
  OR (HomePhone) Like  @SearchKeyword   
  OR (AccountNumber) Like @SearchKeyword   
   OR (CompanyName) Like @SearchKeyword   
  OR case when Status =1 then 'active' else 'deactive' end Like @SearchKeyword  
 )  
  
 END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageDefinition_GetContainerXmls]'
GO
/*****************************************************
* Name : PageDefinition_GetContainerXmls
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[PageDefinition_GetContainerXmls]
(
	@pageIds varchar(max),
	@renderingDeviceId uniqueidentifier = null,
	@audienceSegmentId nvarchar(max) 
)  
AS  
BEGIN  
	DECLARE @PageDefSegmentIds table (PageDefinitionSegmentId uniqueidentifier) 
	INSERT INTO @PageDefSegmentIds (PageDefinitionSegmentId)
	SELECT dbo.GetPageDefinitionSegmentId(P.Value, @renderingDeviceId,@audienceSegmentId)
		FROM dbo.SplitComma(@pageIds,',') P
 	
	SELECT 
		pdfc.[PageDefinitionId]
		,pdfc.[ContainerId]
		,pdfc.[ContentId]
		,pdfc.[InWFContentId]
		,pdfc.[ContentTypeId]
		,pdfc.[IsModified]
		,pdfc.[IsTracked]
		,pdfc.[Visible]
		,pdfc.[InWFVisible]
		,pdfc.[DisplayOrder]
		,pdfc.[InWFDisplayOrder]
		,pdfc.[CustomAttributes]
		,pdfc.[PageDefinitionSegmentId]
		,pdfc.ContainerName
	FROM	
		PageDefinitionContainer pdfc INNER JOIN @PageDefSegmentIds P ON P.PageDefinitionSegmentId = pdfc.PageDefinitionSegmentId

	SELECT 
		Id, 
		PageDefinitionId,
		DeviceId,
		AudienceSegmentId,
		UnmanagedXml AS UnmanagedContentXml
	FROM 
		PageDefinitionSegment PDFS
		INNER JOIN @PageDefSegmentIds P ON P.PageDefinitionSegmentId = PDFS.Id
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetNumberOfUsersOnline]'
GO
/*****************************************************
* Name : Membership_GetNumberOfUsersOnline
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetNumberOfUsersOnline]
--********************************************************************************
-- PARAMETERS

	@ApplicationId				uniqueidentifier,
    @MinutesSinceLastInActive   int,
	@IsSystemUser				bit = 0 
--********************************************************************************
AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************

	DECLARE	@CurrentTimeUtc     datetime
    DECLARE @DateActive			datetime
 
	SET		@CurrentTimeUtc	 =	GetDate()

	SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    
	SELECT  @NumOnline = COUNT(*)
    FROM    dbo.USUser u(NOLOCK),
            dbo.USSiteUser s(NOLOCK),
            dbo.USMembership m(NOLOCK)
    WHERE   --s.SiteId = @ApplicationId AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
            LastActivityDate > @DateActive  AND
            u.Id = m.UserId AND
			u.Status <> dbo.GetDeleteStatus()	AND
			s.IsSystemUser = @IsSystemUser
    
	RETURN(@NumOnline)
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetMembersAndRoles]'
GO
/*****************************************************
* Name : Membership_GetMembersAndRoles
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetMembersAndRoles]
--********************************************************************************
-- PARAMETERS
	@ProductId		uniqueidentifier = NULL,
	@ApplicationId  uniqueidentifier,
	@MemberType		smallint,
	@ObjectId		uniqueidentifier,
	@ObjectTypeId	int,
	@IsSystemUser	bit = 0
--********************************************************************************
AS
BEGIN 
	IF (@MemberType = 1)
	BEGIN
		IF(@ObjectId IS NOT NULL)
		BEGIN
						
			Declare @memberTable table(Id uniqueidentifier,RoleId int,RoleName nvarchar(256),ObjectTypeId int,ProductId uniqueidentifier,Propagate int, SiteId uniqueidentifier)
			Declare @objTable table(ObjectId uniqueidentifier)	
			Declare @groupTable table(Id uniqueidentifier)

			Insert into @objTable 
			values(@ObjectId)
			Insert into @objTable 
			SELECT Distinct P.ObjectId FROM dbo.GetAllParents(@ObjectId) P
			Inner JOIN dbo.USMemberRoles MR on P.ObjectId = MR.ObjectId
			Where Propagate = 1  
						
			Insert into @memberTable(Id,RoleId,RoleName,ObjectTypeId,ProductId,Propagate, SiteId)
			Select MemberId,MR.RoleId,R.Name,MR.ObjectTypeId,MR.ProductId ,Propagate, MR.ApplicationId
			from USMemberRoles MR
			Inner JOIN @objTable OT on MR.ObjectId = OT.ObjectId
			Inner Join dbo.USRoles R on R.Id =MR.RoleId
			INNER JOIN dbo.USSiteUser SU on MR.MemberId =SU.UserId
			Where MR.MemberType =1 
			AND SU.IsSystemUser =@IsSystemUser
			AND (@ProductId IS NULL OR SU.ProductId = @ProductId)
			

			;with cteChildGroup(GroupId)
			aS
			(Select MemberId from USMemberRoles   MR
			Inner JOIN @objTable OT on MR.ObjectId = OT.ObjectId
			Where MR.MemberType =2
			)
			Insert into @GroupTable
			Select GroupId from cteChildGroup 
			UNION
			Select MemberId from USMemberGroup Where GroupId in (Select GroupId from cteChildGroup)
			AND MemberType =2

			--Select * from @GroupTable T
			--Inner Join USGroup G  on T.Id =G.Id

			Insert into @memberTable(Id,RoleId,RoleName,ObjectTypeId,ProductId,Propagate,SiteId)
			Select MemberId ,MR.RoleId,R.Name,MR.ObjectTypeId,MR.ProductId ,Propagate,MR.ApplicationId
			from USMemberRoles MR
			Inner JOIN @objTable OT on MR.ObjectId = OT.ObjectId
			Inner Join dbo.USRoles R on R.Id =MR.RoleId
			Where MR.MemberType =1
			UNION
			Select G.MemberId ,MR.RoleId,R.Name,MR.ObjectTypeId,MR.ProductId ,Propagate,MR.ApplicationId
			from USMemberGroup G
			Left Join USMemberRoles MR on G.GroupId = MR.MemberId
			Inner JOIN @objTable OT on MR.ObjectId = OT.ObjectId
			Left Join USRoles R on R.Id = MR.RoleId
			Where GroupId in (Select Id from @groupTable)



			Select  DISTINCT     
					U.Id,
					U.UserName,
					FN.UserFullName FullName,
					U.Status,
					T.RoleId,
					T.[RoleName] Name,
					t.Propagate,
					T.ObjectTypeId,
					t.ProductId
			FROM @memberTable T
			Inner Join USUser U on T.Id = U.Id
			INNER JOIN dbo.USSiteUser SU on U.Id =SU.UserId
			LEFT JOIN VW_UserFullName FN on FN.UserId = U.Id
			Where IsSystemUser=@IsSystemUser AND( @ObjectTypeId is null OR T.ObjectTypeId =@ObjectTypeId)
			AND (@ProductId IS NULL OR SU.ProductId = @ProductId)
			AND (@ProductId IS NULL OR T.ProductId = @ProductId)
			AND 
			--SU.SiteId = @ApplicationId 
			SU.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId,@IsSystemUser, 0))
			AND T.SiteId = @ApplicationId
		END
		ELSE IF(@ObjectId IS NULL) --This is not resolving the group permission and propagation. This can be modified according to the need.
		BEGIN	
			SELECT DISTINCT --CHECK FOR ITS PARENT HAVING PROPAGATE SET TO TRUE 
					U.Id,
					UserName,
					FN.UserFullName FullName,
					U.Status,
					MR.RoleId,
					R.[Name],
					MR.Propagate,
					MR.ObjectTypeId,
					MR.ProductId
			FROM USRoles R 
			INNER JOIN USMemberRoles MR ON R.[Id] = MR.RoleId
			INNER JOIN dbo.USUser U ON U.Id = MR.MemberId 
			INNER JOIN dbo.USSiteUser SU ON SU.UserId = MR.MemberId
			LEFT JOIN VW_UserFullName FN on FN.UserId =U.Id
			WHERE	(@MemberType IS NULL OR MR.MemberType = @MemberType)	AND
					MR.ObjectTypeId = @ObjectTypeId AND
					--SU.SiteId = @ApplicationId	AND
					SU.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId,@IsSystemUser,0)) AND
					(@ProductId IS NULL OR SU.ProductId = @ProductId) AND
					SU.IsSystemUser = @IsSystemUser 
					--AND
					--MR.ProductId = ISNULL(@ProductId , MR.ProductId)
		END
	END
	ELSE
	BEGIN
		IF (@MemberType = 2)
		BEGIN
		
		SELECT DISTINCT     
				G.Id,
				Title,
				G.Status,
				MR.RoleId,
				R.[Name],
				MR.Propagate,
				MR.ObjectTypeId,
				MR.ProductId
		FROM	dbo.USMemberRoles MR,
				dbo.USRoles  R,
				dbo.USGroup G--,  
				--dbo.GetAllParents(@ObjectId) T  
		  WHERE   --MR.ProductId = ISNULL(@ProductId , MR.ProductId) AND
			MR.ApplicationId = @ApplicationId AND
			(@ObjectTypeId IS NULL OR MR.ObjectTypeId = @ObjectTypeId) AND
			(
				(@ObjectId IS NULL OR MR.ObjectId = @ObjectId) 
--				OR
--				MR.ObjectId = T.ObjectId 
				OR   
				 (  
					--This is costly. Consider removing this and try some other way.   
					MR.ObjectId IN (SELECT ObjectId FROM dbo.GetAllParents(@ObjectId)) AND MR.Propagate = 1   --AND R.Id = MR.RoleId AND MR.MemberId = G.Id
					--T.ObjectId = MR.ObjectId AND MR.Propagate = 1  --If ObjectId's any one of the parent has the role and propagate is set to true  
				 ) 
				
			)
			AND 
			R.Id = MR.RoleId 
			AND
			(
				(MR.MemberId = G.Id  AND G.Status!= 2 AND G.Status!=3) --2 means deactivated.
				OR 
				(
					G.Id in (SELECT GROUPID FROM dbo.User_GetGroupChildren(MR.MemberId, @ApplicationId,@ProductId)) 
					
				)
			)
			AND
			G.Status!= 2 AND G.Status!=3--This is to filter the deactivated groups.
			AND G.GroupType = 1 -- for System Group alone
		END
	END
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Group_GetMembersInRole]'
GO
/*****************************************************
* Name : Group_GetMembersInRole
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Group_GetMembersInRole]
(
	@ProductId			uniqueidentifier,
	@Id					int,
	@Name				nvarchar(256),
	@MemberType			smallint,
	@ApplicationId		uniqueidentifier = NULL,
	@IsSystemUser		bit = 0,
	@ObjectTypeId	    int = null,
	@WithProfile		bit = 0,
	@PageNumber			int,
	@PageSize			int
)
AS
BEGIN 
	IF(@Id IS NULL)
		SELECT @Id = Id FROM dbo.USRoles WHERE LOWER([Name]) = LOWER(@Name)

	IF (@Id IS NULL)
	BEGIN
		RAISERROR('NOTEXIST||Role', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END		

	IF @PageNumber IS NULL SET @PageNumber = 0
	
	IF (@MemberType = 1) -- User
	BEGIN
		DECLARE @tbUser TYUser
		INSERT INTO @tbUser
		SELECT * FROM [dbo].[User_GetUsersInRole] 
			(
				@Id, 
				@MemberType,
				@ApplicationId,
				@IsSystemUser,
				@ObjectTypeId,
				@ProductId
			)

		EXEC [dbo].[Membership_BuildUser] 
			@tbUser = @tbUser, 
			@SortBy = NULL, 
			@SortOrder = NULL,
			@PageNumber = @PageNumber,
			@PageSize = @PageSize
	END
	ELSE IF(@MemberType = 2) --- Group 
	BEGIN
		DECLARE @PageLowerBound int
		DECLARE @PageUpperBound int
		SET @PageLowerBound = @PageSize * @PageNumber
		IF (@PageNumber > 0)
			SET @PageUpperBound = @PageLowerBound - @PageSize + 1
		ELSE
			SET @PageUpperBound = 0

		;WITH ResultEntries AS (
			SELECT  ROW_NUMBER() OVER (ORDER BY Title) AS RowNumber,
				g.ProductId,
				g.ApplicationId,
				Id,
				Title,
				Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				Status,
				ExpiryDate,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				GroupType
			FROM dbo.USGroup g
				INNER JOIN dbo.User_GetGroupChildrenInRole(@ApplicationId, @Id,@MemberType) ur ON ur.ChildGroupId = g.Id
				LEFT JOIN VW_UserFullName FN on FN.UserId = g.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = g.ModifiedBy
			WHERE Status != 3 AND
				ur.ObjectTypeId = Isnull(@ObjectTypeId, ur.ObjectTypeId) AND
				(g.ExpiryDate is null OR g.ExpiryDate >= GETUTCDATE())AND
				g.Status = dbo.GetActiveStatus() AND
				g.ProductId = ISNULL(@ProductId,g.ProductId)
				AND g.GroupType  = 1 -- for System Group alone
		)

		SELECT DISTINCT ProductId,
			ApplicationId,
			Id,
			Title,
			Description,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			Status,
			ExpiryDate,
			CreatedByFullName,
			ModifiedByFullName,
			GroupType
		FROM ResultEntries
		WHERE (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
	END
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShippingAddress_Save]'
GO
/*****************************************************
* Name : OrderShippingAddress_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderShippingAddress_Save]
(
	@Id uniqueidentifier output,
	@OrderShippingId uniqueidentifier,
	@AddressId uniqueidentifier,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@FirstName			nvarchar(255)=null,
	@LastName			nvarchar(255)=null,
	@AddressType		int =null,
	@AddressLine1		nvarchar(255)=null,
	@AddressLine2		nvarchar(255)=null,
	@AddressLine3		nvarchar(255)=null,
	@City				nvarchar(255)=null,
	@StateId			uniqueidentifier=null,
	@StateName			nvarchar(255)=null,
	@CountryId			uniqueidentifier=null,
	@Phone				nvarchar(50)=null,
	@Zip				nvarchar(50)=null,
	@County				nvarchar(255)=null
)
as
begin
	DECLARE @Now dateTime
	DECLARE @Stmt VARCHAR(36)	
	DECLARE @Error int	
	DECLARE @NewAddressId uniqueidentifier	
	
		SET @Now = GetUTCDate()	
		
	
IF(@Id IS NOT NULL AND @Id != dbo.GetEmptyGUID())
BEGIN

SELECT @AddressId = AddressId FROM OROrderShippingAddress WHERE Id =@Id

UPDATE [dbo].[GLAddress] WITH (ROWLOCK)
			SET [FirstName] = @FirstName
			  ,[LastName] = @LastName
			  ,[AddressType] = @AddressType
			  ,[AddressLine1] = @AddressLine1
			  ,[AddressLine2] = @AddressLine2
			  ,[AddressLine3] = @AddressLine3
			  ,[City] = @City
			  ,[StateId] = @StateId
			  ,[StateName]=@StateName
			  ,[Phone] = @Phone
			  ,[Zip] = @Zip
			  ,[CountryId] = @CountryId
			  ,[ModifiedDate] = @Now
			  ,[ModifiedBy] = @ModifiedBy
			  ,[Status] = @Status	
			  ,[County] = @County		 
			WHERE Id=@AddressId
END
ELSE
BEGIN
		set @Id = newId()
		Set @NewAddressId = newId()
		SET @Stmt = 'Insert OrderShippingAddress'
		if (@AddressId is not null and @AddressId != dbo.GetEmptyGUID())
		begin
		INSERT INTO[dbo].[GLAddress]
           ([Id]
           ,[FirstName]
		   ,[LastName]
           ,[AddressType]
           ,[AddressLine1]
           ,[AddressLine2]
		   ,[AddressLine3]	
		   ,[City]
           ,[StateId]
           ,[StateName]
           ,[Zip]
           ,[CountryId]
		   ,[Phone]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status]
		   ,[County]
		   )
		select	@NewAddressId,				
				GA.FirstName,
				GA.LastName,
				GA.AddressType, 
				GA.AddressLine1,
				GA.AddressLine2, 
				GA.AddressLine3,						
				GA.City,
				GA.StateId,
				GA.StateName,
				GA.Zip,
				GA.CountryId,
				GA.Phone ,
				@Now, 
				@CreatedBy,	
				@Now,
				@CreatedBy,			 
				GA.Status ,
				@County
		 from  GLAddress GA WHERE GA.Id = @AddressId
		END
		ELSE 
		BEGIN
			INSERT INTO[dbo].[GLAddress]
			   ([Id]
			   ,[FirstName]
			   ,[LastName]
			   ,[AddressType]
			   ,[AddressLine1]
			   ,[AddressLine2]
			   ,[AddressLine3]					
			   ,[City]
			   ,[StateId]
			   ,[StateName]
			   ,[Zip]
			   ,[CountryId]
			   ,[Phone]
			   ,[CreatedDate]
			   ,[CreatedBy]
			   ,[ModifiedDate]
			   ,[ModifiedBy]
			   ,[Status]
			   ,[County])
			VALUES
				(@NewAddressId
				,@FirstName
				,@LastName
				,@AddressType
				,@AddressLine1
				,@AddressLine2
				,@AddressLine3					
				,@City
				,@StateId
				,@StateName
				,@Zip
				,@CountryId
				,@Phone
				,@Now
				,@CreatedBy
				,@Now
				,@CreatedBy
				,1,
				@County)
	END
	INSERT INTO OROrderShippingAddress
	   ([Id]
	   ,[AddressId]
	   ,[ParentAddressId]
	   ,[CreatedDate]
	   ,[CreatedBy]         
	   ,[Status])
	select @Id,
			@NewAddressId,
			@AddressId,
			@Now, 
			@CreatedBy,				 
			1 --Status
END


Update OROrderShipping Set OrderShippingAddressId =@Id,ModifiedDate=@Now,ModifiedBy=@ModifiedBy
Where Id=@OrderShippingId
	
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[iAppsForm_GetFormsByHierarchy]'
GO
/*****************************************************
* Name : iAppsForm_GetFormsByHierarchy
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[iAppsForm_GetFormsByHierarchy]  
(  
 @Id     uniqueIdentifier=null ,  
 @ApplicationId  uniqueidentifier=null,  
 @Level    int=null,  
 @PageIndex   int=1,  
 @PageSize   int=null   
)  
AS  
BEGIN  
   
 SELECT    
  C.Id,      
  C.ApplicationId,  
  C.Title,  
  C.Description,    
  C.FormType,     
  H.ObjectTypeId,   
  C.CreatedDate,  
  C.CreatedBy,  
  C.ModifiedBy,  
  C.ModifiedDate,  
  C.XMLString,  
  C.XSLTString,  
  C.XSLTFileName,   
  C.Status,  
  C.SendEmailYesNo,  
  C.Email,  
  C.ThankYouURL,  
  C.SubmitButtonText,    
  C.PollResultType,   
  C.PollVotingType,  
  C.AddAsContact,  
  C.ContactEmailField,  
  C.WatchId,  
  C.EmailSubjectText,  
  C.EmailSubjectField,  
  C.SourceFormId,  
  C.ParentId,
  FN.UserFullName CreatedByFullName,  
  MN.UserFullName ModifiedByFullName,  
  dbo.iAppsForm_GetResponseCountByFormId(C.Id) ResponseCount,  
  dbo.iAppsForm_GetResponseDateByFormId(C.Id) ResponseDate  
 FROM Forms C   
 INNER JOIN dbo.GetChildrenByHierarchyType(@Id,@Level,38) H ON    
  C.ParentId = H.Id       AND  
  C.Status != dbo.GetDeleteStatus() AND   
  H.ObjectTypeId IN (38)    AND      
  (@ApplicationId IS NULL OR C.ApplicationId = @ApplicationId)  
 LEFT OUTER JOIN FormsResponse F ON   
  FormsId = C.Id      AND  
  F.ResponseDate IS NOT NULL AND F.ResponseDate = NULL    
  LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy  
  LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy  
 ORDER By C.Title    
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Blog_Save]'
GO
/*****************************************************
* Name : Blog_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Blog_Save] 
(
	@Id						uniqueidentifier=null OUT ,
	@ApplicationId			uniqueidentifier,	
 	@Title					nvarchar(256)=null,
 	@Description			nvarchar(1024)=null,
 	@BlogNodeType			int=0,
 	@MenuId 				uniqueidentifier=null,
	@ContentDefinitionId	uniqueidentifier=null,
 	@ShowPostDate 			bit=true,
 	@DisplayAuthorName 		bit=false,
 	@EmailBlogOwner 		bit=true,
 	@ApproveComments 		bit=true,
 	@RequiresUserInfo 		bit=true,
 	@ModifiedBy				uniqueidentifier,
 	@ModifiedDate			datetime=null, 	
 	@Status					int=null,
	@BlogListPageId			uniqueidentifier=null,
	@BlogDetailPageId		uniqueidentifier=null
)
AS
BEGIN
	DECLARE
		@NewId		uniqueidentifier,
		@RowCount	INT,
		@Stmt 		VARCHAR(25),
		@Now 		datetime,
		@Error 		int	

	SET @Now = getutcdate()

	IF NOT EXISTS (SELECT * FROM BLBlog WHERE Id = @Id)
	BEGIN
		SET @Stmt = 'Create Blog'
		set @Status = dbo.GetStatus(8,'Create')
		INSERT INTO BLBlog  WITH (ROWLOCK)(
			 	 Id,
				 ApplicationId,	
 				 Title,
 				 Description,
 				 BlogNodeType,
 				 MenuId, 		
				 ContentDefinitionId,		 
 				 ShowPostDate,
 				 DisplayAuthorName,
 				 EmailBlogOwner,
 				 ApproveComments,
 				 RequiresUserInfo,
 				 CreatedBy,
 				 CreatedDate, 	
 				 Status
		)
		values
			(
				@Id,
				@ApplicationId,	
 				@Title,
 				@Description,
 				@BlogNodeType,
 				@MenuId, 			
				@ContentDefinitionId,	
				isnull(@ShowPostDate,1),
 				isnull(@DisplayAuthorName,0),
 				isnull(@EmailBlogOwner,1),
 				isnull(@ApproveComments,1),
				isnull(@RequiresUserInfo,1),
 				@ModifiedBy,
 				@Now, 	
 				@Status		
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END

		SELECT @Id 
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify Blog'
		
		UPDATE	BLBlog  WITH (rowlock)	SET 
			Title = @Title,
			MenuId = @MenuId,
 			ShowPostDate = @ShowPostDate,
 			DisplayAuthorName=@DisplayAuthorName,
 			EmailBlogOwner=@EmailBlogOwner,
 			ApproveComments=@ApproveComments,
 			RequiresUserInfo=@RequiresUserInfo,
 			ModifiedBy = @ModifiedBy,
			ModifiedDate=@Now,
			BlogListPageId= @BlogListPageId,
			BlogDetailPageId = @BlogDetailPageId,
			ContentDefinitionId=@ContentDefinitionId
        WHERE
			Id = @Id

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PaymentInfo_GetByPaymentCreditCardId]'
GO
/*****************************************************
* Name : PaymentInfo_GetByPaymentCreditCardId
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[PaymentInfo_GetByPaymentCreditCardId](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	-- open the symmetric key 
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	SELECT 
		P.Id,
		P.CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		CreatedBy,
		dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId)CreatedDate,
		Status,
		ModifiedBy,
		dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId)ModifiedDate,
		ExternalProfileId
	FROM PTPaymentInfo P
	Inner Join PTPaymentCreditCard PCC ON P.Id=PCC.PaymentInfoId
	WHERE PCC.Id=@Id

	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageDefinition_GetPageLatestVersionForAllSegment]'
GO
/*****************************************************
* Name : PageDefinition_GetPageLatestVersionForAllSegment
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


--exec PageDefinition_GetPageLatestVersionForAllSegment '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4', 
--'dd606ea2-3a2d-461c-bcfd-0d3f46f466a6', 8
ALTER PROCEDURE [dbo].[PageDefinition_GetPageLatestVersionForAllSegment]
--********************************************************************************
-- Input Output Parameters
--********************************************************************************
	@ApplicationId uniqueidentifier = null,
	@PageId uniqueidentifier,
	@ObjectTypeId  int
	
AS
 
BEGIN
	DECLARE @VersionId uniqueidentifier
	-- Setting the VersionId to fetch the UnManaged XML and Content from Version
	SELECT TOP (1) @VersionId = Id  from VEVersion A
	WHERE ObjectId=@PageId and ObjectTypeId=@ObjectTypeId and ApplicationId = @ApplicationId
	ORDER BY RevisionNumber DESC, VersionNumber DESC
	
	DECLARE @DeskTopDeviceId uniqueidentifier
	DECLARE @DeskTopAudienceId uniqueidentifier
	SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@ApplicationId)
	SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@ApplicationId)

	DECLARE @pageDefSegmentIds TABLE(Id uniqueidentifier)	

	;With PageDefSegmentCTE AS
	(	
				SELECT [Id],DeviceID, AudienceSegmentId
					  ,ROW_NUMBER() over (Partition BY DeviceId,AudienceSegmentId Order by MinorVersionNumber Desc) as pdfRank
				  FROM [VEPageDefSegmentVersion]
				  WHERE VersionId = @VersionId
	)

	INSERT INTO @pageDefSegmentIds 
		SELECT Id FROM PageDefSegmentCTE P1 WHERE pdfRank = 1
		AND Id NOT IN (SELECT Id FROM PageDefSegmentCTE P2 
		WHERE P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId 
		)
			
	-- Getting the latest page definition segment version for all the device
	SELECT	   [Id]
			  ,[VersionId]
			  ,[PageDefinitionId]
			  ,[DeviceId]
			  ,[AudienceSegmentId]
			  ,[ContainerContentXML]
			  ,[UnmanagedXml]
	FROM [VEPageDefSegmentVersion] 
	WHERE Id IN (SELECT Id from @pageDefSegmentIds)
	 		  

	  -- Page Contents 
	SELECT CV.[Id]
		   [ContentId]
		  ,[ContentTypeId]
		  ,[XMLString]
		  ,[Status]
		  ,CV.[CreatedBy]
		  ,CV.[CreatedDate]
		  ,[ModifiedBy]
		  ,[ModifiedDate]
		  ,[ContentVersionId]
		  ,[IsChanged]
		  ,[PageDefSegmentVersionId]
	  FROM [VEContentVersion] CV 
	  INNER JOIN [VEPageContentVersion] PCV ON CV.Id = PCV.ContentVersionId
	  INNER JOIN [VEPageDefSegmentVersion] PDFV ON PDFV.Id = PCV.PageDefSegmentVersionId
	  WHERE PageDefSegmentVersionId IN (SELECT Id from @pageDefSegmentIds)
	  --AND (@AudienceSegmentId IS NULL OR AudienceSegmentId = @AudienceSegmentId) -- TO DO: We may have to handle multiple AS

	SELECT
		C.Id AS ContainerId,
		C.Title AS ContainerName
	FROM PageDefinition P
		JOIN SITemplateContainer TC ON TC.TemplateId = P.TemplateId
		JOIN SIContainer C ON C.Id = TC.ContainerId
	WHERE P.PageDefinitionId = @PageId

END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_ByCOD_Save]'
GO
/*****************************************************
* Name : Payment_ByCOD_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Payment_ByCOD_Save]  
(   
 @Id uniqueidentifier output,
 @CODId uniqueidentifier output,	
 @OrderId uniqueidentifier,
 @PaymentTypeId int,
 @PaymentStatusId int =1,
 @Amount money,
 @CustomerId uniqueidentifier,
 @Description nvarchar(255),
 @ModifiedBy uniqueidentifier  ,
 @BillingAddressId uniqueidentifier,
 @OrderPaymentId uniqueidentifier output,
 @IsRefund tinyint = 0
)  
  
AS  
BEGIN       
  
DECLARE @CreatedDate datetime
  set @CreatedDate = GetUTCDate()

Declare @error	  int
Declare @stmt	  varchar(256)	
Declare @sequence int
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
			  SET @Id = newid()  

				Insert into PTPayment 
				(Id
				,PaymentTypeId
				,PaymentStatusId
				,CustomerId
				,Amount
				,Description
				,CreatedDate
				,CreatedBy
				,Status
				,IsRefund
				)
				Values
				(
				@Id
				,@PaymentTypeId
				,@PaymentStatusId
				,@CustomerId
				,@Amount
				,@Description
				,@CreatedDate
				,@ModifiedBy
				,dbo.GetActiveStatus()
				,@IsRefund
			)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

		IF (@CODId is null or @CODId =dbo.GetEmptyGUID())
		BEGIN
	IF Exists (Select AddressId from OROrderShippingAddress Where Id=@BillingAddressId)
	Select @BillingAddressId =AddressId from OROrderShippingAddress Where Id=@BillingAddressId
			Select @CODId =newid()
			Insert into PTCODInfo
						(Id
						 ,CustomerId
						 ,BillingAddressId
						 ,Status
						 ,CreatedDate
						 ,CreatedBy
						 ,ModifiedDate
						 ,ModifiedBy
						)
				Values
				(@CODId
				,@CustomerId
				,@BillingAddressId
				,1
				,@CreatedDate
				,@ModifiedBy
				,@CreatedDate
				,@ModifiedBy
				)

	
		END

				Insert into PTPaymentCOD
				(Id,
				 PaymentId,
				 CODId
				)
				Values
				(newId(),
				  @Id,
				  @CODId
				)

					select @error = @@error
							if @error <> 0
							begin
								raiserror('DBERROR||%s',16,1,@stmt)
								return dbo.GetDataBaseErrorCode()	
							end

				

				Select @sequence = max(isnull(Sequence,0)) + 1
				From PTOrderPayment
				Where OrderId=@OrderId


				SET @OrderPaymentId  = newId()
				Insert Into PTOrderPayment(Id,OrderId,PaymentId,Amount,Sequence)
				Values(@OrderPaymentId,@OrderId,@Id,@Amount,@sequence)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

	end  
ELSE
	BEGIN
		set @CreatedDate = GetUTCDate()

		UPDATE PTPayment SET 
		PaymentTypeId = @PaymentTypeId, 
		PaymentStatusId = @PaymentStatusId,
		CustomerId = @CustomerId,
		Amount = @Amount,
		Description = @Description,
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @CreatedDate,	
		Status = dbo.GetActiveStatus(),
		IsRefund = @IsRefund
		WHERE Id = @Id


		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end


		UPDATE PTOrderPayment SET Amount = @Amount
		WHERE Id= @OrderPaymentId
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		
		END
END  

Update OROrder Set PaymentTotal =dbo.Order_GetPaymentTotal(@OrderId), RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
Where Id=@OrderId

  
  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetUser]'
GO
/*****************************************************
* Name : Membership_GetUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


--exec Membership_GetUser @ApplicationId='CDD38906-6071-4DD8-9FB6-508C38848C61',@UserId=NULL,@UserName=N'iappsuser',@UpdateLastActivity=NULL
--exec Membership_GetUser @ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@UserId=NULL,@UserName=N'iappsuser',@UpdateLastActivity=NULL


ALTER PROCEDURE [dbo].[Membership_GetUser]
--********************************************************************************
-- PARAMETERS
	@ApplicationId						uniqueidentifier,
    @UserId						uniqueidentifier,
	@UserName					nvarchar(256),
    @UpdateLastActivity			bit = 0
--********************************************************************************
AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************
	DECLARE @CurrentTimeUtc     datetime
	SET @CurrentTimeUtc = GetUTCDate()
	DECLARE @DeletedSatus INT
	SET @DeletedSatus = dbo.GetDeleteStatus()
	
	IF (@ApplicationId = dbo.GetEmptyGUID())
		SET @ApplicationId = NULL
			
	IF ( @UserName IS NOT NULL )
	BEGIN
		IF ( @UpdateLastActivity = 1 )
		BEGIN
			UPDATE   dbo.USUser
			SET      LastActivityDate = @CurrentTimeUtc
			FROM     dbo.USUser u, dbo.USSiteUser s
			WHERE	 u.LoweredUserName	= LOWER(@UserName) AND
					(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))) AND 
					-- s.SiteId	= ISNULL(@ApplicationId,s.SiteId) AND
					 u.Status <> @DeletedSatus	
			
			IF ( @@ROWCOUNT = 0 ) -- User ID not found
			BEGIN
				RAISERROR ('NOTEXISTS||%s', 16, 1, 'User')
				RETURN dbo.GetBusinessRuleErrorCode()
			END
		
		END
		SET @UserName = LOWER(@UserName)
		
		SELECT  u.UserName,u.CreatedDate,
				m.Email, m.PasswordQuestion,  m.IsApproved,
				m.LastLoginDate, u.LastActivityDate,
				m.LastPasswordChangedDate, u.Id, m.IsLockedOut,
				m.LastLockoutDate
		FROM    dbo.USUser u, dbo.USMembership m, USSiteUser s
		WHERE   u.LoweredUserName	=@UserName AND 
				u.Id = m.UserId		   AND
				m.UserId = s.UserId    AND
				u.Status <> @DeletedSatus	AND
				--s.SiteId = @ApplicationId   
				(@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
	END
	ELSE
	BEGIN
		IF ( @UpdateLastActivity = 1 )
		BEGIN
			UPDATE   dbo.USUser
			SET      LastActivityDate = @CurrentTimeUtc
			FROM     dbo.USUser
			WHERE    @UserId = Id

			IF ( @@ROWCOUNT = 0 ) -- User ID not found
			BEGIN
				RAISERROR ('NOTEXISTS||%s', 16, 1, 'User')
				RETURN dbo.GetBusinessRuleErrorCode()
			END
		
		END

		SELECT  u.UserName,u.CreatedDate,
				m.Email, m.PasswordQuestion,  m.IsApproved,
				m.LastLoginDate, u.LastActivityDate,
				m.LastPasswordChangedDate, u.Id, m.IsLockedOut,
				m.LastLockoutDate
		FROM    dbo.USUser u, dbo.USMembership m
		WHERE   @UserId = u.Id AND u.Id = m.UserId
	END
    
	IF ( @@ROWCOUNT = 0 ) -- User ID not found
    BEGIN
            RAISERROR ('NOTEXISTS||%s', 16, 1, 'User')
			RETURN dbo.GetBusinessRuleErrorCode()
	END   

    RETURN 0
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [dbo].[iAppsForm_Save]'
GO
/*****************************************************
* Name : iAppsForm_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[iAppsForm_Save] (
	@Id UNIQUEIDENTIFIER out
	,@ApplicationId UNIQUEIDENTIFIER
	,@Title NVARCHAR(1024)
	,@Description NVARCHAR(1024)
	,@XMLString XML
	,@XSLTString TEXT
	,@XSLTFileName VARCHAR(100)
	,@ThankYouURL NVARCHAR(1024)
	,@Email NVARCHAR(max)
	,@SendEmailYesNo INT
	,@Status INT
	,@CreatedBy UNIQUEIDENTIFIER
	,@ModifiedBy UNIQUEIDENTIFIER
	,@SubmitButtonText NVARCHAR(100)
	,@FormType INT
	,@PollResultType INT
	,@PollVotingType INT
	,@AddAsContact INT
	,@ContactEmailField NVARCHAR(256)
	,@WatchId UNIQUEIDENTIFIER
	,@EmailSubjectText NVARCHAR(256)
	,@EmailSubjectField NVARCHAR(256)
	,@SourceFormId UNIQUEIDENTIFIER = NULL
	,@ParentId  uniqueidentifier = null
	)
AS
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @reccount INT
	,@error INT
	,@Now DATETIME
	,@stmt VARCHAR(256)
	,@rowcount INT

BEGIN
	--********************************************************************************  
	-- code  
	--********************************************************************************  
	IF (@Status = dbo.GetDeleteStatus())
	BEGIN
		RAISERROR (
				'INVALID||Status'
				,16
				,1
				)

		RETURN dbo.GetBusinessRuleErrorCode()
	END

	/* IF @Id specified, ensure exists */
	IF (@Id IS NOT NULL)
		IF (
				(
					SELECT count(*)
					FROM Forms
					WHERE Id = @Id AND STATUS != dbo.GetDeleteStatus() AND ApplicationId = @ApplicationId
					) = 0
				)
		BEGIN
			RAISERROR (
					'NOTEXISTS||Id'
					,16
					,1
					)

			RETURN dbo.GetBusinessRuleErrorCode()
		END

	SET @Now = getutcdate()

	IF (@Status IS NULL OR @Status = 0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

	IF (@Id IS NULL) -- New INSERT  
	BEGIN
		SET @stmt = 'INSERT'
		SET @Id = newid();

		INSERT INTO Forms (
			Id
			,ApplicationId
			,Title
			,Description
			,FormType
			,XMLString
			,XSLTString
			,XSLTFileName
			,STATUS
			,CreatedBy
			,CreatedDate
			,ModifiedBy
			,ModifiedDate
			,ThankYouURL
			,Email
			,SendEmailYesNo
			,SubmitButtonText
			,PollResultType
			,PollVotingType
			,AddAsContact
			,ContactEmailField
			,WatchId
			,EmailSubjectText
			,EmailSubjectField
			,SourceFormId
			,ParentId
			)
		VALUES (
			@Id
			,@ApplicationId
			,@Title
			,@Description
			,@FormType
			,@XMLString
			,@XSLTString
			,@XSLTFileName
			,@Status
			,@CreatedBy
			,@Now
			,@ModifiedBy
			,@Now
			,@ThankYouURL
			,@Email
			,@SendEmailYesNo
			,@SubmitButtonText
			,@PollResultType
			,@PollVotingType
			,@AddAsContact
			,@ContactEmailField
			,@WatchId
			,@EmailSubjectText
			,@EmailSubjectField
			,@SourceFormId
			,@ParentId
			)

		SELECT @error = @@error

		IF @error <> 0
		BEGIN
			RAISERROR (
					'DBERROR||%s'
					,16
					,1
					,@stmt
					)

			RETURN dbo.GetDataBaseErrorCode()
		END
	END
	ELSE -- update  
	BEGIN
		SET @stmt = 'UPDATE'
		IF @WatchId = dbo.GetEmptyGuid()
			SET @WatchId = NULL
		
		UPDATE Forms
		WITH (ROWLOCK)

		SET ApplicationId = @ApplicationId
			,Title = isnull(@Title, Title)
			,Description = isnull(@Description, Description)
			,FormType = @FormType
			,XMLString = @XMLString
			,XSLTString = @XSLTString
			,XSLTFileName = @XSLTFileName
			,ThankYouURL = @ThankYouURL
			,Email = @Email
			,SendEmailYesNo = @SendEmailYesNo
			,STATUS = @Status
			,ModifiedBy = @ModifiedBy
			,ModifiedDate = @Now
			,SubmitButtonText = @SubmitButtonText
			,PollResultType = @PollResultType
			,PollVotingType = @PollVotingType
			,AddAsContact = @AddAsContact
			,ContactEmailField = @ContactEmailField
			,WatchId = @WatchId
			,EmailSubjectText=@EmailSubjectText
			,EmailSubjectField=@EmailSubjectField
			,SourceFormId=ISNULL(@SourceFormId, SourceFormId)
			,ParentId = @ParentId
		WHERE Id = @Id

		--update title to HSStructure table--  
		--UPDATE HSStructure
		--SET Title = @Title
		--WHERE Id = @Id

		--------------------------------------------------  
		SELECT @error = @@error
			,@rowcount = @@rowcount

		IF @error <> 0
		BEGIN
			RAISERROR (
					'DBERROR||%s'
					,16
					,1
					,@stmt
					)

			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @rowcount = 0
		BEGIN
			RAISERROR (
					'CONCURRENCYERROR'
					,16
					,1
					)

			RETURN dbo.GetDataConcurrencyErrorCode() -- concurrency error  
		END
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[AssetFile_GetAssetFilesByCommaDelimitedGuids]'
GO
/*****************************************************
* Name : AssetFile_GetAssetFilesByCommaDelimitedGuids
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/




ALTER PROCEDURE [dbo].[AssetFile_GetAssetFilesByCommaDelimitedGuids] 
--********************************************************************************
--Parameters
--********************************************************************************
(
@commaDelimitedGuids nvarchar(max)
)
AS
BEGIN
		SELECT  C.Id,
				C.ApplicationId,
				C.Title,
				C.Description, 
				C.Text, 
				C.URL,
				C.URLType, 
				C.ObjectTypeId, 
				C.CreatedDate,
				C.CreatedBy,
				C.ModifiedBy,
				C.ModifiedDate, 
				C.Status,
				C.RestoreDate, 
				C.BackupDate, 
				C.StatusChangedDate,
				C.SourceContentId,
				C.PublishDate, 
				C.ArchiveDate, 
				C.XMLString,
				C.BinaryObject, 
				C.Keywords,
				F.FileName, 
				F.FileSize, 
				F.Extension, 
				F.FolderName, 
				F.Attributes, 
				F.RelativePath, 
				F.PhysicalPath,
				F.MimeType,
				F.AltText,
				F.FileIcon, 
				F.DescriptiveMetadata, 
				F.ImageHeight, F.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				C.OrderNo	,
				C.ParentId						
		FROM COContent C 
			INNER JOIN dbo.SplitGuid(@commaDelimitedGuids,',') M ON C.Id= M.Items
			INNER JOIN COFile F ON C.Id = F.ContentId
			LEFT JOIN VW_UserFullName FN ON FN.UserId = C.CreatedBy
			LEFT JOIN VW_UserFullName MN ON MN.UserId = C.ModifiedBy
		WHERE Status != 3
		ORDER BY OrderNo
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[AssetFile_GetAssetFile]'
GO

ALTER PROCEDURE [dbo].[AssetFile_GetAssetFile] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id uniqueidentifier
)
as
Begin	

		select  C.Id,
				C.ApplicationId,
				C.Title,
				C.Description, 
				C.Text, 
				C.URL,
				C.URLType, 
				C.ObjectTypeId, 
				C.CreatedDate,
				C.CreatedBy,
				C.ModifiedBy,
				C.ModifiedDate, 
				C.Status,
				C.RestoreDate, 
				C.BackupDate, 
				C.StatusChangedDate,
				C.PublishDate, 
				C.ArchiveDate, 
				C.XMLString,
				C.BinaryObject, 
				C.Keywords,
				F.FileName, 
				F.FileSize, 
				F.Extension, 
				F.FolderName, 
				F.Attributes, 
				F.RelativePath, 
				F.PhysicalPath,
				F.MimeType,
				F.AltText,
				F.FileIcon, 
				F.DescriptiveMetadata,
				F.ImageHeight,
				F.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				C.OrderNo,
				C.ParentId
		from COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		where F.ContentId = @Id 			  
	
end

GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[AssetImage_GetAssetImage]'
GO
ALTER PROCEDURE [dbo].[AssetImage_GetAssetImage] 
--********************************************************************************
-- Parameters
--********************************************************************************
(
	@Id uniqueidentifier
)
as
Begin
		select  C.Id,
				C.ApplicationId,
				C.Title,
				C.Description, 
				C.Text, 
				C.URL,
				C.URLType, 
				C.ObjectTypeId, 
				C.CreatedDate,
				C.CreatedBy,
				C.ModifiedBy,
				C.ModifiedDate, 
				C.Status,
				C.RestoreDate, 
				C.BackupDate, 
				C.StatusChangedDate,
				C.PublishDate, 
				C.ArchiveDate, 
				C.XMLString,
				C.BinaryObject, 
				C.Keywords,
				F.FileName, 
				F.FileSize, 
				F.Extension, 
				F.FolderName, 
				F.Attributes, 
				F.RelativePath, 
				F.PhysicalPath,
				F.MimeType,
				F.AltText,
				F.FileIcon, 
				F.DescriptiveMetadata,
				F.ImageHeight,
				F.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				C.OrderNo,
				C.ParentId			
		from COContent C
		INNER JOIN COFile F ON C.Id = F.ContentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		where F.ContentId = @Id 			  
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderShippingAddress_Get]'
GO
/*****************************************************
* Name : OrderShippingAddress_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderShippingAddress_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	SELECT	OSA.[Id]
			,GA.Id AddressId
			,OSA.ParentAddressId
			,[FirstName]
			,[LastName]
			,[AddressType]
			,[AddressLine1]
			,[AddressLine2]
			,[AddressLine3]
			,[City]
			,[StateId]
			,[StateName]
			,[Zip]
			,[CountryId]
			,[Phone]
			,dbo.ConvertTimeFromUtc(OSA.[CreatedDate],@ApplicationId)CreatedDate
			,OSA.[CreatedBy]
			,OSA.[ModifiedBy]
			,dbo.ConvertTimeFromUtc(OSA.ModifiedDate,@ApplicationId)ModifiedDate
			,OSA.[Status]
			,County
	FROM OROrderShippingAddress OSA
	INNER JOIN GLAddress GA ON GA.Id = OSA.AddressId
	WHERE OSA.Id=@Id
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_GetOrderForPayment]'
GO
/*****************************************************
* Name : Order_GetOrderForPayment
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Order_GetOrderForPayment](@PaymentId uniqueidentifier,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN

IF @PaymentId IS NULL 
	begin
		raiserror('DBERROR||%s',16,1,'OrderId or CartId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],    
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],    
 O.[ExternalOrderNumber],
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
 O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId)OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
INNER JOIN PTOrderPayment OP ON O.Id=OP.OrderId
WHERE OP.PaymentId = @PaymentId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflowsWithSequenceFromXml]'
GO
/*****************************************************
* Name : Workflow_GetWorkflowsWithSequenceFromXml
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright ? 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:
--- xml format should be ("<workflows><workflow Id=Guid/><workflow Id=Guid/></workflows>" and case sensitive)
--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflowsWithSequenceFromXml 
--		@XmlGuids		xml 
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflowsWithSequenceFromXml](
--********************************************************************************
-- Parameters
--********************************************************************************
			@XmlGuids		xml 
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
            SELECT 
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName,
					B.Id As SequenceId, 
					B.RoleId,
					B.ActorId,
					B.ActorType,
					B.[Order],
					dbo.User_GetMemberName(B.ActorId,B.ActorType) As ActorName ,
					dbo.User_GetRoleName(B.RoleId) As RoleName,
					A.IsGlobal,
					A.IsShared
            FROM	dbo.WFWorkflow	A INNER JOIN dbo.WFSequence	B ON A.[Id] = B.WorkflowId 
				LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
            WHERE	A.[Id] IN (SELECT T.c.value('@Id','uniqueidentifier') as id      
							FROM   @XmlGuids.nodes('/workflows/workflow') T(c))		AND
					A.Status = dbo.GetActiveStatus()													
			ORDER BY A.[Id], B.[Order] 

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderItem_Save]'
GO
/*****************************************************
* Name : OrderItem_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderItem_Save]  
(   
 @Id uniqueidentifier output,  
 @OrderId uniqueidentifier,  
 @OrderShippingId uniqueidentifier,  
 @OrderItemStatusId int,  
 @ProductSKUId uniqueidentifier,  
 @Quantity decimal(18,2),  
 @Price money,  
 @UnitCost money,    
 @IsFreeShipping bit,  
 @Sequence int,  
 @Status int,  
 --@CreatedDate datetime,  
 @CreatedBy uniqueidentifier,  
 --@ModifiedDate datetime,  
 @ModifiedBy uniqueidentifier,
@HandlingCharge money,
@ParentOrderItemId uniqueidentifier,
@BundleItemId uniqueidentifier,
@CartItemId uniqueidentifier,
@Tax money,
@Discount money
)  
  
AS  
BEGIN       

  DECLARE @CreatedDate datetime, @ModifiedDate DateTime
IF(@OrderShippingId is null OR @OrderShippingId = dbo.GetEmptyGUID())
BEGIN
	SET @OrderShippingId = null
END
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
  set @Id = newid()  
  set @CreatedDate = getUTCDate()
  INSERT INTO OROrderItem  
           ([Id]  
   ,[OrderId]  
   ,[OrderShippingId]  
   ,[OrderItemStatusId]  
   ,[ProductSKUId]  
   ,[Quantity]  
   ,[Price]  
   ,[UnitCost]  
   ,[IsFreeShipping]  
   ,[Sequence]  
   ,[Status]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[HandlingCharge]
   ,[ParentOrderItemId]
   ,[BundleItemId]
	,[CartItemId]
	,[Tax]
	,[Discount])  
     VALUES  
           (@Id,   
   @OrderId ,  
   @OrderShippingId ,  
   @OrderItemStatusId ,  
   @ProductSKUId ,  
   @Quantity ,  
   @Price ,  
   @UnitCost ,  
   @IsFreeShipping ,  
   @Sequence ,  
   @Status ,  
   @CreatedDate ,  
   @CreatedBy ,  
   @CreatedDate ,  
   @ModifiedBy, 
   @HandlingCharge,
   @ParentOrderItemId,
   @BundleItemId,
	@CartItemId,
	@Tax,
	@Discount)  

 end  
 else  
 begin  
  --set @ModifiedDate= getUTCDate()
  update OROrderItem  
  set   
   [OrderId]=@OrderId ,  
   [OrderShippingId]=@OrderShippingId ,  
   [OrderItemStatusId]=@OrderItemStatusId ,  
   [ProductSKUId]=@ProductSKUId ,  
   [Quantity]=@Quantity ,  
   [Price]=@Price ,  
   [UnitCost]=@UnitCost ,  
   [IsFreeShipping]=@IsFreeShipping ,  
   [Sequence]=@Sequence ,  
   [Status]=@Status ,  
   --[CreatedDate]=@CreatedDate ,  
   [CreatedBy]=@CreatedBy ,  
   [ModifiedDate]=GetUTCDate() ,  
   [ModifiedBy]=@ModifiedBy ,
   [HandlingCharge] = @HandlingCharge,
   [ParentOrderItemId]=@ParentOrderItemId,
   [BundleItemId]=@BundleItemId,
   [CartItemId]=@CartItemId,
	[Tax] = @Tax,
	[Discount] = @Discount
  where Id=@Id  
     


 end  


Update OROrderItemAttributeValue set OrderId=b.OrderId , OrderItemId=b.Id from OROrderItemAttributeValue a, 
	OROrderItem b where 
	a.CartItemId = b.CartItemId
	and a.OrderItemId is null and b.CartItemId=@CartItemId


	DECLARE @OrderItemQuantity money
	DECLARE @OrderShipmentItemQuantity money
	DECLARE @OrderStatusId int
	DECLARE @count1 int
	DECLARE @count2 int

	Select @OrderItemQuantity = Quantity
	FROM  OROrderItem  
	Where Id = @Id

	Select @OrderShipmentItemQuantity = sum(Quantity)
	FROM  FFOrderShipmentItem  SI
	INNER JOIN FFOrderShipmentPackage  S ON SI.OrderShipmentContainerId=S.Id
	Where OrderItemId =@Id AND ShipmentStatus=2
	Group By OrderItemId

	If(@OrderItemQuantity = @OrderShipmentItemQuantity OR @OrderShipmentItemQuantity>0.0)
	BEGIN
	
		Update OROrderItem 
		Set OrderItemStatusId=Case When @OrderItemQuantity = @OrderShipmentItemQuantity Then 2 Else 7 End
		Where  Id =@Id

		if @@error <> 0
		begin
			raiserror('DBERROR||%s',16,1,'Update Status Shipped OrderShipmentItem')
			return dbo.GetDataBaseErrorCode()	
		end
	END

--	SELECT TOP 1 @OrderId=OrderId 
--	FROM OROrderItem	
--	WHERE Id=@Id
--
--
--	Select @OrderStatusId =OrderStatusId 
--	from OROrder
--	Where Id=@OrderId
--
--	
--	IF @OrderStatusId=1 OR @OrderStatusId=11
--	BEGIN
--		Select @count1 = count(*) 
--		from OROrderItem
--		Where OrderId=@OrderId
--			
--		Select @count2=  count(*)
--		from OROrderItem
--		Where OrderId=@OrderId AND OrderItemStatusId=2
--		
--		
--	IF(@count1 = @count2)
--		BEGIN
--			Update OROrder Set OrderStatusId=2
--			Where Id=@OrderId	
--
--		END
--		ELSE IF(@count2>0 OR (Select count(*) 
--			from OROrderItem
--			Where OrderId=@OrderId AND OrderItemStatusId=7) >0)
--		BEGIN
--			Update OROrder Set OrderStatusId=11
--			Where Id=@OrderId
--		END
--	END



END  


  
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_FindUsersByName_Count]'
GO
/*****************************************************
* Name : Membership_FindUsersByName_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_FindUsersByName_Count]
--********************************************************************************
-- PARAMETERS
	@ApplicationId			uniqueidentifier,
    @UserNameToMatch		nvarchar(256),
	@IsSystemUser			bit = 0

--********************************************************************************
AS
BEGIN
    
--********************************************************************************
-- Variable declaration
--********************************************************************************
	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc	= GetUtcDate()
	
	SELECT  COUNT(*)
	FROM    dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s
	WHERE   s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))	AND 
			m.UserId = u.Id		AND 
			s.UserId = m.UserId AND
			s.IsSystemUser = @IsSystemUser	AND
			u.Status <> dbo.GetDeleteStatus() AND
			u.LoweredUserName LIKE LOWER(@UserNameToMatch) AND
			(u.ExpiryDate is null OR u.ExpiryDate >= getutcdate())

    RETURN 0
END
--***********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_GetSite_Count]'
GO
/*****************************************************
* Name : Site_GetSite_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Site_GetSite_Count]
(
	@Id   uniqueIdentifier =null,
	@Title  nvarchar(256)=null,
--	@URL     nvarchar(4000)=null,
	@ParentSiteId  uniqueIdentifier=null,
	@Status        int=null,
	@MasterSiteId   uniqueIdentifier = null,
	@SiteGroupId	uniqueIdentifier = null
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

Begin
--********************************************************************************
-- code
--********************************************************************************

select count(*)   from SISite S
LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id
where	S.Id = isnull(@Id, S.Id) and
isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
--isnull(upper(S. URL),'') = isnull(upper(@URL), isnull(upper(S.URL),'')) and
isnull(S.ParentSiteId,'00000000-0000-0000-0000-000000000000') = isnull(@ParentSiteId, isnull(S.ParentSiteId,'00000000-0000-0000-0000-000000000000')) and
S.Status = isnull(@Status, S.Status)and 
(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
AND (@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) 


End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_SaveWorkflow]'
GO
/*****************************************************
* Name : Workflow_SaveWorkflow
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: This procedure is used to create workflow. In case of modification, 
-- if the workflow is executed already for any other object then modification is not allowed. 
-- Those type of checkings are doing in Workflow_Save procedure, because this procedure is calling 
-- from Workflow_Save stored procedure. It is not calling directly from anywhere.
-- Author: Martin M V
-- Created Date: 23-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].[Workflow_SaveWorkflow] 
--          @Id = 'uniqueidentifier' or null,
--          @Title = 'value',
--          @Description = 'value',
--          @CreatedBy = 'uniqueidentifier',
--          @ModifiedBy = 'uniqueidentifier',
--          @Status = 1 (Active) or 0 (InActive),
--			@SkipAfterDays = 0,
--          @ObjectTypeId = 1,
--          @RootId = null,
--			@ApplicationId	uniqueidentifier 
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_SaveWorkflow](
---********************************************************************************
-- Parameters
--********************************************************************************
            @Id				uniqueidentifier OUTPUT,
            @Title			nvarchar(256),
            @Description	nvarchar(1024),
            @CreatedBy		uniqueidentifier,
            @ModifiedBy		uniqueidentifier,
            @Status			int,
            @SkipAfterDays	int,
            @ObjectTypeId	int,
            @RootId			uniqueidentifier = null OUTPUT,
            @ApplicationId	uniqueidentifier,
            @ProductId		uniqueidentifier ,
            @IsGlobal		bit = null,
            @IsShared       bit = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
            @Count 		bigint,
            @CurrentDate	DateTime,
            @NewGuid		uniqueidentifier,
            @RowCount       	int
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
            /* Set Default values to Variable*/	
            SET @Count		= 0
            SET @RowCount	= 0	    
            SET @CurrentDate 	= getUTCDate()

            /* new insert */
            IF (@Id IS NULL)            
            BEGIN
            			IF @Status Is null 
						BEGIN
							set @Status = dbo.GetStatus(@ObjectTypeId,'Create')
						END
						IF @Status Is null 
						BEGIN
							set @Status = dbo.GetStatus(@ObjectTypeId,'Created')
						END						
                        SET @NewGuid = newid();
                        INSERT INTO WFWorkflow (
                                    [Id],
                                    [Title],
                                    [Description],
                                    [CreatedBy],
                                    [CreatedDate],
                                    [RootId],
                                    [Status],
                                    [SkipAfterDays],
                                    [ObjectTypeId],
                                    [ApplicationId],
                                    [ProductId],
                                    [IsGlobal],
                                    [IsShared]
                                    ) 

                        VALUES (
                                    @NewGuid,
                                    @Title,
                                    @Description,
                                    @CreatedBy,
                                    @CurrentDate,
                                    @RootId,
                                    @Status,
                                    @SkipAfterDays,
                                    @ObjectTypeId,
                                    @ApplicationId,
                                    @ProductId,
                                    @IsGlobal,
                                    @IsShared
                                    )

                        IF @@ERROR <> 0
                        BEGIN
                                    RETURN dbo.GetDataBaseErrorCode()         
                        END
                        SELECT @Id = @NewGuid;
            END

            /* Update */
            ELSE    
            BEGIN
                        UPDATE	WFWorkflow  WITH (ROWLOCK)
                        SET     [Title] 		= 	@Title, 
                                [Description] 	= 	@Description,
                                [ModifiedBy] 	= 	@ModifiedBy,
                                [ModifiedDate]	= 	@CurrentDate,
                                [RootId]		= 	@RootId,
                                [Status]		=	@Status,
                                [SkipAfterDays]	=	@SkipAfterDays,
                                [ObjectTypeId]	=	@ObjectTypeId,
                                [ApplicationId]	=	@ApplicationId,
                                [ProductId]		=	@ProductId,
                                [IsGlobal]		=	@IsGlobal,
                                [IsShared]      =   @IsShared
                        WHERE   [Id]           	=	@Id 

                        SELECT  @RowCount = @@ROWCOUNT
                        IF @@ERROR <> 0
                        BEGIN
                                    RETURN dbo.GetDataBaseErrorCode()
                        END

                        IF @RowCount = 0
                        BEGIN
                                    /* concurrency error */
                                    RAISERROR('DBCONCURRENCY',16,1)
                                    RETURN dbo.GetDataConcurrencyErrorCode()                                   
                        END     
            END
            
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflowsFromXml]'
GO
/*****************************************************
* Name : Workflow_GetWorkflowsFromXml
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright ? 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:
--- xml format should be ("<workflows><workflow Id=Guid/><workflow Id=Guid/></workflows>" and case sensitive)
--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflowsFromXml 
--		@XmlGuids		xml 
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflowsFromXml](
--********************************************************************************
-- Parameters
--********************************************************************************
			@XmlGuids		xml 
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
            SELECT 
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName,
					A.IsGlobal,
					A.IsShared
            FROM	dbo.WFWorkflow A
				LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
				LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
            WHERE	A.[Id] IN (SELECT T.c.value('@Id','uniqueidentifier') as id      
		FROM   @XmlGuids.nodes('/workflows/workflow') T(c))		AND
		A.Status = dbo.GetActiveStatus()

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_GetSiteByXmlGuids]'
GO
/*****************************************************
* Name : Site_GetSiteByXmlGuids
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Site_GetSiteByXmlGuids]
(
	@Id Xml
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

BEGIN
--********************************************************************************
-- code
--********************************************************************************
		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, 
				URL,Type,ParentSiteId,Keywords,VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName ,Prefix, UICulture, LoginURL, DefaultURL
				,NotifyOnTranslationError, TranslationEnabled, TranslatePagePropertiesByDefault, AutoImportTranslatedSubmissions, DefaultTranslateToLanguage,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteUrl,
				MasterSiteId
		FROM @Id.nodes('/guidCollection/guid')tab(col)
		INNER JOIN SISite S on S.Id=tab.col.value('text()[1]','uniqueidentifier')
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE S.Status != dbo.GetDeleteStatus()
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_Save]'
GO
/*****************************************************
* Name : Payment_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Payment_Save]  
(   
 @Id uniqueidentifier output,
 @PaymentInfoId uniqueidentifier output,	
 @OrderId uniqueidentifier,
 @PaymentTypeId int,
 @PaymentStatusId int =1,
 @CardTypeId uniqueidentifier,
 @CardNumber varchar(max),
 @ExpirationMonth int,
 @ExpirationYear int,
 @NameOnCard nvarchar(256),
 @LastName nvarchar(256)=null,
 @AuthCode		nvarchar(256),
 @Description nvarchar(max),
 @AddressLine1 nvarchar(256),
 @AddressLine2 nvarchar(256),
 @City nvarchar(256),
 @StateId uniqueidentifier,
 @StateName nvarchar(255),
 @CountryId uniqueidentifier,
 @Zip	nvarchar(50),
 @Phone nvarchar(50),
 @Amount money,
 @CustomerId uniqueidentifier,
 @UpdateCustomerPayment bit,
 @SavedCardId uniqueidentifier,
 @ModifiedBy uniqueidentifier  ,
 @OrderPaymentId uniqueidentifier output,
 @IsRefund tinyint = 0,
 @ExternalProfileId nvarchar(255)
)  
  
AS  
BEGIN    -- 1   
  
DECLARE @CreatedDate datetime, @ModifiedDate DateTime
--Declare @PaymentInfoId uniqueidentifier
Declare @AddressId uniqueidentifier
Declare @error	  int
Declare @stmt	  varchar(256)	
Declare @sequence int
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin --2
  SET @Id = newid()  
  SET @PaymentInfoId =newid()
  set @CreatedDate = GetUTCDate()
  SET @AddressId = newid()


	Insert into PTPayment 
	(Id
	,PaymentTypeId
	,PaymentStatusId
	,CustomerId
	,Amount
	,Description
	,CreatedDate
	,CreatedBy
	,ModifiedDate
	,ModifiedBy
	,AuthCode
	,Status
	,IsRefund
	)
	Values
	(
	@Id
	,@PaymentTypeId
	,@PaymentStatusId
	,@CustomerId
	,@Amount
	,@Description
	,@CreatedDate
	,@ModifiedBy
	,@CreatedDate
	,@ModifiedBy
	,@AuthCode
	,dbo.GetActiveStatus()
	,@IsRefund
)

	select @error = @@error
	if @error <> 0
	begin 
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end  

-- open the symmetric key 
		OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
		OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection;

	Insert into PTPaymentInfo
	(Id
	,CardType
	,Number
	,ExpirationMonth
	,ExpirationYear
	,NameOnCard
	,CreatedBy
	,CreatedDate
	,ModifiedBy
	,ModifiedDate
	,Status
	)
	Values
	(@PaymentInfoId
	,@CardTypeId
    ,EncryptByKey(Key_Guid('key_DataShare'), @CardNumber)
	,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
	,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
	,@NameOnCard
	,@ModifiedBy
	,@CreatedDate
	,@ModifiedBy
	,@CreatedDate
	,dbo.GetActiveStatus()
	)

	select @error = @@error
	if @error <> 0
	begin
		-- close the symmetric key
		CLOSE SYMMETRIC KEY [key_DataShare];
		CLOSE MASTER KEY
		
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end

	Insert Into GLAddress
	(Id
	 ,FirstName
	 ,LastName
	 ,AddressType
	 ,AddressLine1
	 ,AddressLine2
	 ,City
	 ,StateId
	 ,StateName
	 ,CountryId
	 ,Zip
	 ,Phone
	 ,CreatedDate
	 ,CreatedBy
	 ,ModifiedDate
	 ,ModifiedBy
	 ,Status
	)
	Values
	(
	@AddressId
	,@NameOnCard
    ,@LastName
	,0
	,@AddressLine1
	,@AddressLine2
	,@City
	,@StateId
	,@StateName
	,@CountryId
	,@Zip
	,@Phone
	,@CreatedDate
	,@ModifiedBy
	,@CreatedDate
	,@ModifiedBy
	,dbo.GetActiveStatus()
	)
	select @error = @@error
	if @error <> 0
	begin
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end
	
	
	Insert Into PTPaymentCreditCard
	(Id
	,PaymentInfoId
	,ParentPaymentInfoId
	,PaymentId
	,BillingAddressId
	,Phone
	,ExternalProfileId
	)
	Values
	(newid()
	,@PaymentInfoId
	,@SavedCardId
	,@Id
	,@AddressId
	,@Phone
	,@ExternalProfileId
	)
	
	select @error = @@error
	if @error <> 0
	begin
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end

	Select @sequence = max(isnull(Sequence,0)) + 1
	From PTOrderPayment
	Where OrderId=@OrderId


	SET @OrderPaymentId  = newId()
	Insert Into PTOrderPayment(Id,OrderId,PaymentId,Amount,Sequence)
	Values(@OrderPaymentId,@OrderId,@Id,@Amount,@sequence)

	select @error = @@error
	if @error <> 0
	begin
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end

 end  --2
ELSE
	BEGIN --3
		set @CreatedDate = GetUTCDate()
		SET @AddressId = newid()


		UPDATE PTPayment SET 
		PaymentTypeId = @PaymentTypeId, 
		PaymentStatusId = @PaymentStatusId,
		CustomerId = @CustomerId,
		Amount = @Amount,
		Description = @Description,
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @CreatedDate,	
		AuthCode = @AuthCode,
		Status = dbo.GetActiveStatus(),
		IsRefund = @IsRefund
		WHERE Id = @Id


		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end

	-- open the symmetric key 
			OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
			OPEN SYMMETRIC KEY [key_DataShare] 
				DECRYPTION BY CERTIFICATE cert_keyProtection;

		UPDATE PTPaymentInfo SET	
		CardType = @CardTypeId
		,Number = EncryptByKey(Key_Guid('key_DataShare'),@CardNumber)
		,ExpirationMonth = EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
		,ExpirationYear = EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
		,NameOnCard = @NameOnCard	
		,ModifiedBy = @ModifiedBy
		,ModifiedDate = GetUTCDate()
		,Status = dbo.GetActiveStatus()
		WHERE Id = @PaymentInfoId
		
		

		select @error = @@error
		if @error <> 0
		begin
			-- close the symmetric key
			CLOSE SYMMETRIC KEY [key_DataShare];
			CLOSE MASTER KEY
			
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end

		
		SELECT @AddressId =  BillingAddressId FROM PTPaymentCreditCard where PaymentId = @Id
		
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end

		UPDATE GLAddress SET	
		 FirstName =  @NameOnCard
		 ,LastName =@LastName
		 ,AddressType = 0
		 ,AddressLine1 = @AddressLine1
		 ,AddressLine2 = @AddressLine2
		 ,City = @City
		 ,StateId = @StateId
		 ,StateName = @StateName
		 ,CountryId = @CountryId
		 ,Zip = @Zip
		 ,Phone = @Phone	
		 ,ModifiedDate = GetUTCDate()
		 ,ModifiedBy = @ModifiedBy
		 ,Status = dbo.GetActiveStatus()
		WHERE Id = @AddressId
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end	


		UPDATE PTOrderPayment SET Amount = @Amount
		WHERE Id= @OrderPaymentId
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		
		END
END  --3

If @UpdateCustomerPayment= 1
BEGIN --4
	Declare @NewAddressId uniqueidentifier
	
	Set @NewAddressId = newid()
	Insert into GLAddress(Id
	 ,FirstName
	 ,LastName
	 ,AddressType
	 ,AddressLine1
	 ,AddressLine2
	 ,City
	 ,StateId
	 ,StateName
	 ,CountryId
	 ,Zip
	 ,Phone
	 ,CreatedDate
	 ,CreatedBy
	 ,ModifiedDate
	 ,ModifiedBy
	 ,Status
	)
	Select	
	@NewAddressId
	,FirstName
	,LastName
	 ,AddressType
	 ,AddressLine1
	 ,AddressLine2
	 ,City
	 ,StateId
	 ,StateName
	 ,CountryId
	 ,Zip
	 ,Phone
	 ,CreatedDate
	 ,CreatedBy
	 ,ModifiedDate
	 ,ModifiedBy
	 ,Status
	 FROM GLAddress
	WHERE Id = @AddressId

	select @error = @@error
	if @error <> 0
	begin
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end
	DECLARE @ExistingPaymentInfo UNIQUEIDENTIFIER
	SELECT 	@ExistingPaymentInfo = PaymentInfoId FROM USUserPaymentInfo Where Id = @SavedCardId
	DECLARE @NewPaymentInfoId UNIQUEIDENTIFIER
	SET @NewPaymentInfoId = newid()
	IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000'
	BEGIN
		UPDATE PTPaymentInfo
		SET
		CardType=@CardTypeId
		,Number=EncryptByKey(Key_Guid('key_DataShare'), @CardNumber)
		,ExpirationMonth=EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
		,ExpirationYear=EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
		,NameOnCard=@NameOnCard
		,ModifiedBy=@ModifiedBy
		,ModifiedDate=@CreatedDate	
		Where Id = @ExistingPaymentInfo
	END
	ELSE
	BEGIN
		INSERT INTO PTPaymentInfo(
			Id,
			CardType,
			Number,
			ExpirationMonth,
			ExpirationYear,
			NameOnCard,
			CreatedBy,
			CreatedDate,
			Status,
			ModifiedBy,
			ModifiedDate
		)VALUES(
			@NewPaymentInfoId,
			@CardTypeId,
			EncryptByKey(Key_Guid('key_DataShare'), @CardNumber),
			EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2))),
			EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4))),
			@NameOnCard,
			@ModifiedBy,
			@CreatedDate,
			1,	
			@ModifiedBy,
			@CreatedDate
			)
	END
	
	select @error = @@error
	if @error <> 0
	begin
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end
	IF @ExistingPaymentInfo != '00000000-0000-0000-0000-000000000000' 
	BEGIN
		Update USUserPaymentInfo
		SET BillingAddressId = @NewAddressId,
			ExternalProfileId= @ExternalProfileId
		Where PaymentInfoId=@ExistingPaymentInfo AND UserId=@CustomerId
		
	END 
	ELSE
	BEGIN 
		INSERT INTO USUserPaymentInfo(
			Id,
			UserId,
			PaymentInfoId,
			BillingAddressId,
			Sequence,
			IsPrimary,
			CreatedBy,
			CreatedDate,
			Status,
			ModifiedDate,
			ModifiedBy,
			ExternalProfileId
		)VALUES
		(
			newid(),
			@CustomerId,
			@NewPaymentInfoId,
			@AddressId,
			1,
			0,	
			@ModifiedBy,
			@CreatedDate,
			1,
			@CreatedDate,
			@ModifiedBy,
			@ExternalProfileId		
		)
	END
	select @error = @@error
	if @error <> 0
	begin
		raiserror('DBERROR||%s',16,1,@stmt)
		return dbo.GetDataBaseErrorCode()	
	end

END --4


Update OROrder Set PaymentTotal =dbo.Order_GetPaymentTotal(@OrderId), RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
Where Id=@OrderId

  
  
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[CustomerPaymentInfo_Get]'
GO
/*****************************************************
* Name : CustomerPaymentInfo_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[CustomerPaymentInfo_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	-- open the symmetric key 
	OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

	Declare @BillingAddressId uniqueidentifier

	SELECT @BillingAddressId = BillingAddressId	
	FROM USUserPaymentInfo 
	WHERE Id=@Id

	SELECT UP.Id,
		P.Id PaymentInfoId,
		P.CardType,
		CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],
		CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,
		CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,
		P.NameOnCard,
		UP.UserId,
		UP.BillingAddressId,
		UP.Sequence,
		UP.IsPrimary,
		UP.CreatedBy,
		dbo.ConvertTimeFromUtc(UP.CreatedDate,@ApplicationId)CreatedDate,
		UP.ModifiedBy,
		dbo.ConvertTimeFromUtc(UP.ModifiedDate,@ApplicationId)ModifiedDate,
		UP.[Status],
		UP.ExternalProfileId,
		UP.Nickname
	FROM PTPaymentInfo P
	Inner Join USUserPaymentInfo UP ON P.Id=UP.PaymentInfoId
	WHERE UP.Id=@Id
	AND UP. [Status] != dbo.GetDeleteStatus()

	SELECT 	 
	A.Id 
	,A.FirstName
	,A.LastName
	,A.AddressType
	,A.AddressLine1
	,A.AddressLine2
	,A.AddressLine3
	,A.City
	,A.StateId
	,A.StateName
	,A.Zip
	,A.CountryId
	,A.Phone
	,A.CreatedBy
	,dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate
	,dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate
	,A.ModifiedBy 
--	,UA.Id AddressId
--	,UA.NickName
--	,UA.IsPrimary
--	,UA.Sequence
	,A.County
	FROM GLAddress A
	--Left Outer Join USUserShippingAddress UA ON A.Id=UA.AddressId
	Where A.Id=@BillingAddressId
	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetAllUserPermissions]'
GO
/*****************************************************
* Name : Membership_GetAllUserPermissions
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetAllUserPermissions]
(
	@ProductId uniqueidentifier,
	@ApplicationId uniqueidentifier = null,
	@UserId uniqueidentifier = null
)
AS
BEGIN
		
		SELECT	DISTINCT SiteId, S.Title, US.UserId
		FROM	dbo.USSiteUser US
		INNER JOIN SISite S ON US.SiteId = S.Id AND S.Status <> dbo.GetDeleteStatus()
		WHERE	
			(@ProductId is null OR (@ProductId is not null AND ProductId = @ProductId))
			AND (@UserId is null OR (@UserId is not null AND UserId = @UserId))
				
		SELECT DISTINCT	
				G.ProductId,
				MG.ApplicationId AS SiteId,
				MG.MemberId AS UserId,
				G.Id,
				G.Title,
				G.Description,
				CreatedBy,
				CreatedDate,
				ModifiedBy,
				ModifiedDate,
				Status,
				ExpiryDate,
				'' as CreatedByFullName,
				'' as ModifiedByFullName,
				G.GroupType
		FROM	dbo.USGroup G JOIN dbo.USMemberGroup MG ON G.Id = MG.GroupId
				AND MG.MemberType = 1	
				AND ( @ProductId is null OR (@ProductId Is not null AND G.ProductId = @ProductId) )
				--AND ( @ApplicationId  is null OR (@ApplicationId is not null AND MG.ApplicationId   = @ApplicationId ) )
				AND ( @ApplicationId  is null OR MG.ApplicationId IN (Select SiteId From dbo.GetAncestorSites(@ApplicationId) ))
				AND ( @UserId  is null OR (@UserId is not null AND MG.MemberId   = @UserId ) )
		
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_GetSiteByRelation]'
GO
/*****************************************************
* Name : Site_GetSiteByRelation
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Site_GetSiteByRelation]
(
	@RelationName		varchar(256)=null,
	@ObjectId			varchar(8000)=null,
	@ReturnObjectType	int ,
	@Id					uniqueidentifier=null,
	@Title				nvarchar(256)=null,
	--@URL				nvarchar(4000),
	@ParentSiteId		uniqueidentifier=null,
	@Status			    int,
	@PageIndex			int = 1,
	@PageSize			int 
	)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE @EmptyGUID uniqueIdentifier

BEGIN
--********************************************************************************
-- code
--********************************************************************************
SET @EmptyGUID = dbo.GetEmptyGUID()
	IF @PageSize is Null
	BEGIN
		
		SELECT S.Id, S.Title, S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate, 
				S.URL,S.Type,S.ParentSiteId,S.Keywords,S.VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName ,Prefix, UICulture, LoginURL, DefaultURL
				,NotifyOnTranslationError, TranslationEnabled, TranslatePagePropertiesByDefault, AutoImportTranslatedSubmissions, DefaultTranslateToLanguage,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure
		FROM SISite S  
		INNER JOIN dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R ON S.Id = R.ObjectId 
		LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
		WHERE 	S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				--isnull(upper(S. URL),'') = isnull(upper(@URL), isnull(upper(S.URL),'')) and
				isnull(S.ParentSiteId,@EmptyGUID) = isnull(@ParentSiteId, isnull(S.ParentSiteId,@EmptyGUID)) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())
	END
	ELSE
	BEGIN
		WITH ResultEntries AS ( 
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, S.Id, S.Title, S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, S.ModifiedDate, 
				S.URL,S.Type,S.ParentSiteId,S.Keywords,S.VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation ,Prefix, UICulture, LoginURL, DefaultURL,NotifyOnTranslationError, TranslationEnabled, TranslatePagePropertiesByDefault, AutoImportTranslatedSubmissions, DefaultTranslateToLanguage,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure
		FROM SISite S  
		INNER JOIN dbo.GetObjectByRelationType(@RelationName,@ObjectId,@ReturnObjectType) R ON S.Id = R.ObjectId 
		WHERE 
				S.Id = isnull(@Id, S.Id) and
				isnull(upper(S.Title),'') = isnull(upper(@Title), isnull(upper(S.Title),'')) and
				--isnull(upper(S. URL),'') = isnull(upper(@URL), isnull(upper(S.URL),'')) and
				isnull(S.ParentSiteId,@EmptyGUID) = isnull(@ParentSiteId, isnull(S.ParentSiteId,@EmptyGUID)) and
				S.Status = isnull(@Status, S.Status)and 
				(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))

		SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, 
				URL,Type,ParentSiteId,Keywords,VirtualPath,HostingPackageInstanceId, LicenseLevel, SendNotification, SubmitToTranslation,PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName ,Prefix, UICulture, LoginURL, DefaultURL
		FROM ResultEntries A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE Row between 
			(@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize

	END

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[UploadContactData_Status]'
GO
/*****************************************************
* Name : UploadContactData_Status
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[UploadContactData_Status] 
(
--********************************************************************************
@Status	 int ,
	@UploadHistoryId uniqueidentifier,
	@Id uniqueidentifier OUTPUT,
	@TotalRecords int=null,
	@DuplicateRecords int=null,
	@ErrorRecords int=null,
	@ErrorMessage nvarchar(max)=null
--********************************************************************************
)
AS
declare 
@stmt	  varchar(256)

		IF (@UploadHistoryId IS NOT NULL)
		BEGIN
			IF (NOT EXISTS(SELECT 1 FROM   UploadContactData WHERE  UploadHistoryId = @UploadHistoryId))
			BEGIN			
					SET @Stmt = convert(varchar(36),@UploadHistoryId)
					RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			END	
			ELSE
			BEGIN
				update UploadContactData set [Status]=@Status, [TotalRecords]=@TotalRecords,  DuplicateRecords=@DuplicateRecords ,ErrorRecords=@ErrorRecords, ErrorMessage=@ErrorMessage	where UploadHistoryId = @UploadHistoryId;
				set @Id=@UploadHistoryId;
			END	
		END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_GetOrderShippings]'
GO
/*****************************************************
* Name : Order_GetOrderShippings
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order shippings with the given order id
-- Author: 
-- Created Date:03/25/2009
-- Created By:Devin

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderShippings](@Id uniqueidentifier,@ApplicationId uniqueidentifier =null)
AS
SELECT 
 OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId) ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId) ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
OS.[CreatedDate] AS  CreatedDate,
OS.[ModifiedBy],
OS.[ModifiedDate] AS   ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder,
OS.IsHandlingIncluded
FROM OROrderShipping OS
WHERE OS.OrderId = @Id;
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetImageFolderSecurityLevelMap]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetImageFolderSecurityLevelMap
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetImageFolderSecurityLevelMap]
	@ApplicationId uniqueidentifier
AS
BEGIN
select F.Id, F.VirtualPath,S.SecurityLevelId from COImageStructure F 
	Inner Join USSecurityLevelObject S on F.Id=S.ObjectId  AND ObjectTypeId = 6
	INNER JOIN USSecurityLevel SL on S.SecurityLevelId = SL.Id
Where F.SiteId = @ApplicationId AND SL.Status = 1 --Don't grab security levels that are deleted
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Product_GetProductAndSkuByXmlGuidsForProductIndexer]'
GO
/*****************************************************
* Name : Product_GetProductAndSkuByXmlGuidsForProductIndexer
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose: get a collection of products by a given list of IDs  
-- Author:   
-- Created Date:03-23-2009  
-- Created By:Prakash  
  
-- Modified Date: 03-27-2009  
-- Modified By: Devin  
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  267181a5-2608-4bed-b59c-005baf3476ac
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Product_GetProductAndSkuByXmlGuidsForProductIndexer]      
(      
		@ApplicationId uniqueidentifier=null
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
 Declare @tempXml table(Sno int Identity(1,1),ProductId uniqueidentifier)      
      
Insert Into @tempXml(ProductId)      
Select distinct ProductId as Id from PRProductSKU where IsActive =1 and IsOnline=1  
      
 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] as ProductTypeName,      
 vPT.[ProductTypePath] + N'id-' + S.ProductIDUser + '/' + S.UrlFriendlyTitle as DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (Select Count(*) From PRProductSKU where ProductId= S.[Id]) as NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) as BundleCompositionLastModified,      
 [PrdSeo].FriendlyUrl as SEOFriendlyUrl,
 [S].TaxCategoryID        
  FROM @tempXml T      
  INNER JOIN PRProduct S on S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT on [S].ProductTypeID = vPT.ProductTypeId      
  LEFT JOIN PRProductSEO PrdSeo ON S.Id = PrdSeo.ProductId   
--Order By T.Sno      
  
  
Select   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
from @tempXml t   
Inner Join PRProductAttributeValue SA  On SA.ProductId=t.ProductId  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,[SiteId]    
      ,[SKU]    
   ,[Title]    
      ,[Description]    
      ,[Sequence]    
      ,[IsActive]    
      ,[IsOnline]    
      ,[ListPrice]    
      ,[UnitCost]    
      ,[PreviousSoldCount]    
   ,[CreatedBy]    
   ,[CreatedDate]    
   ,[ModifiedBy]    
   ,[ModifiedDate]    
   ,[Status]    
   ,[Measure]    
   ,[OrderMinimum]    
   ,[OrderIncrement]  
   ,[HandlingCharges]  
    ,[Length]
	  ,[Height]
	  ,[Width]
	  ,[Weight]
	  ,[SelfShippable]    
    ,[AvailableForBackOrder]
	  ,[BackOrderLimit]
	,[IsUnlimitedQuantity]
  FROM [PRProductSKU] S   
  Inner Join @tempXml t on S.ProductId=t.ProductId  
  
  
Select   
 SA.Id,  
 SA.SKUId,  
 PA.ProductId,
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId  
Inner Join PRProductSKU PS on PA.ProductId = PS.ProductId  
Inner Join PRProductSKUAttributeValue SA  On (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
Left Join ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
Where IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
  
  
  
  
select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
	A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
from @tempXml t   
Inner Join PRProductAttribute PA  On PA.ProductId=t.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=dbo.GetActiveStatus()    and  A.IsSystem=0 and A.IsFaceted=1
  

SELECT P.Id AS ProductId,	CASE WHEN Min(MinimumEffectivePrice) IS NULL THEN MIN(ListPrice) WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
 ELSE Min(ListPrice) END  as Price ,  cast( SUM(PS.PreviousSoldCount) as int) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
					LEFT JOIN (
										SELECT * from cache_ProductMinEffectivePrice MP 
									) PP ON P.Id= PP.ProductId 	
					INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
					PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser 

  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflowsWithSequence_Count]'
GO
/*****************************************************
* Name : Workflow_GetWorkflowsWithSequence_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflows with its sequences based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflowsWithSequence_Count 
--          @WorkflowId = 'uniqueidentifier'  null,
--          @ObjectTypeId = 'int'  null,
--			@ApplicationId	= uniqueidentifier null ,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflowsWithSequence_Count](
--********************************************************************************
-- Parameters
--********************************************************************************
			@WorkflowId		uniqueidentifier = null,
			@ObjectTypeId	int = null,
			@ApplicationId	uniqueidentifier = null,				
			@ProductId		uniqueidentifier = null,
			@PageIndex		int = null,
			@PageSize		int = null
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
        SELECT 
				Count(*)
        FROM	dbo.WFWorkflow A,
				dbo.WFSequence B
        WHERE	A.[Id]			=	Isnull(@WorkflowId,A.[Id])			AND 
				A.ObjectTypeId	=	Isnull(@ObjectTypeId,ObjectTypeId)	AND
				A.[Id]			=	B.WorkflowId						AND
				(@ApplicationId IS NULL OR (A.ApplicationId = @ApplicationId OR (A.IsShared = 1 AND A.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, 1, 1))))) AND
				A.ProductId		=	Isnull(@ProductId, ProductId)		AND
				A.Status = dbo.GetActiveStatus()
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetDirectoriesByLevel]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetDirectoriesByLevel
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetDirectoriesByLevel]
(
	@DirectoryId	uniqueIdentifier,
	@ApplicationId	uniqueidentifier=null,
	@ObjectType		int,
	@ParentLevel	int,
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
)
AS
BEGIN
	
	IF @DirectoryId IS NULL
	BEGIN
		RAISERROR ( 'ISMANDATORY||Id' , 16, 1 )
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	IF @ObjectType IS NULL
		SET @ObjectType =(Select top 1 ObjectTypeId from VWDirectoryIds Where Id = @DirectoryId)

	IF @ObjectType =7 
		EXEC [dbo].[SqlDirectoryProvider_GetContentDirectoriesByLevel] @DirectoryId, @ApplicationId, @ParentLevel, @ChildrenLevel, @UserId
	ELSE IF @ObjectType =9 
		EXEC [dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel] @DirectoryId, @ApplicationId, @ParentLevel, @ChildrenLevel, @UserId
	ELSE IF @ObjectType =33 
		EXEC [dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel] @DirectoryId, @ApplicationId, @ParentLevel, @ChildrenLevel, @UserId
	ELSE IF @ObjectType =38 
		EXEC [dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel] @DirectoryId, @ApplicationId, @ParentLevel, @ChildrenLevel, @UserId
	ELSE
		EXEC [dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel] @DirectoryId, @ApplicationId, @ParentLevel, @ChildrenLevel, @UserId, @ObjectType
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderPayment_Get]'
GO
/*****************************************************
* Name : OrderPayment_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderPayment_Get]
	(@OrderPaymentId	uniqueidentifier 
	,@ApplicationId uniqueidentifier=null)
AS
BEGIN
OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

DECLARE @PaymentId uniqueidentifier ;
SELECT 
	Id,
	OrderId,
	PaymentId,
	Amount,
	Sequence
FROM
	PTOrderPayment
WHERE
	Id = @OrderPaymentId

SELECT 
	P.Id,
	P.PaymentTypeId,
	P.PaymentStatusId,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Description,
	dbo.ConvertTimeFromUtc(P.FailureDate,@ApplicationId)FailureDate,
	P.FailureDescription,
	dbo.ConvertTimeFromUtc(P.CreatedDate,@ApplicationId)CreatedDate,
	P.CreatedBy,
	dbo.ConvertTimeFromUtc(P.ClearDate,@ApplicationId)ClearDate,
	P.ProcessorTransactionId,
	P.AuthCode,
	P.Status,
	dbo.ConvertTimeFromUtc(P.ModifiedDate,@ApplicationId)ModifiedDate,
	P.ModifiedBy ,
	P.IsRefund 
FROM
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId


SELECT 
	PCC.Id,
	PCC.PaymentInfoId,
	PCC.ParentPaymentInfoId,
	PCC.PaymentId,
	PCC.BillingAddressId,
	PCC.Phone,
	PCC.ExternalProfileId
FROM 
	PTPaymentCreditCard PCC
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

SELECT 
	PINFO.Id,
	PINFO.CardType,
	CONVERT(varchar(max), DecryptByKey(PINFO.Number))  [Number],
	CAST(CONVERT(varchar(2), DecryptByKey(PINFO.ExpirationMonth)) AS INT) ExpirationMonth  ,
	CAST(CONVERT(varchar(4), DecryptByKey(PINFO.ExpirationYear)) AS INT) ExpirationYear,
	PINFO.NameOnCard,
	PINFO.CreatedBy,
	dbo.ConvertTimeFromUtc(PINFO.CreatedDate,@ApplicationId)CreatedDate,
	PINFO.Status,
	PINFO.ModifiedBy,
	dbo.ConvertTimeFromUtc(PINFO.ModifiedDate,@ApplicationId)ModifiedDate
FROM
	PTPaymentInfo PINFO
	INNER JOIN PTPaymentCreditCard PCC ON PCC.PaymentInfoId = PINFO.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

SELECT 
	A.Id,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.StateName,
	A.Zip,
	A.CountryId,
	A.Phone,
	dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
	A.CreatedBy,
	dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	GLAddress A	
	INNER JOIN PTPaymentCreditCard PCC ON PCC.BillingAddressId  = A.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.Id = @OrderPaymentId

	
SELECT  PC.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate, @ApplicationId) CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId ,
        SUM(ISNULL(PC2.CapturedAmount,0)) AS TotalRefundAmount
FROM    PTPaymentCapture PC
        INNER JOIN PTOrderPayment OP ON OP.PaymentId = PC.PaymentId
        LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
WHERE   OP.Id = @OrderPaymentId
GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId
ORDER BY PC.CreatedDate ASC

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_CreateCommerceDefaultData]'
GO
/*****************************************************
* Name : Site_CreateCommerceDefaultData
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Procedure [dbo].[Site_CreateCommerceDefaultData](@ApplicationId uniqueidentifier, @SiteTitle Varchar(1024), @CreatedById uniqueidentifier)
As
BEGIN

	DECLARE @CommerceProductId uniqueidentifier
	SET @CommerceProductId='CCF96E1D-84DB-46E3-B79E-C7392061219B'
	DECLARE @Now DateTime, @maxId int 
	DECLARE @maxSecurityId int
	
	SET @Now = GETUTCDATE()

	-- Product Category
	IF NOT EXISTS(SELECT * FROM PRCategory WHERE Title ='Unassigned')
	BEGIN
		INSERT INTO PRCategory (Id, Title, Description, Sequence, CreatedDate, Status)
		Values('6988B7A8-7C72-4B58-9B68-258B940E2363', 'Unassigned', 'Unassigned', 1, @Now, 1)
	END

	--Insert default value for order number
	IF NOT EXISTS(SELECT * FROM GLIdentity WHERE Name ='OrderNumber' )
	BEGIN
		INSERT INTO GLIdentity (Name,Value) Values ('OrderNumber', 0)
	END

	-- Product Import Config - Did not take it.

	-- Coupon Type
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('D1CA3949-EBFA-4DEC-B120-07DAA8961F81',	'% off Order','',		1,	1,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Order')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('2C24DC1C-268E-4ACB-933C-618DEF52E434',	'$ off Order',	'',	1,	2,	0)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='$ off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('C4FE5439-0B9F-4B41-9221-0302C527DEAE',	'$ off Shipping',	'',	1,	32,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='% off Shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('894D41B5-27D9-4C3B-885D-18FFC565DB8B',	'% off Shipping',	'',	1,	4,	1)
	END
	IF NOT EXISTS(SELECT * FROM CPCouponType WHERE Title ='Flat rate shipping')
	BEGIN
		INSERT INTO CPCouponType (Id, Title, Description, Status, Sequence, IsShippingRequired)
		Values ('004202FA-B429-4AB7-A59F-ED37C74C0421',	'Flat rate shipping','',		1,	5,	1)
	END

	--Customer Group.sql
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Gold')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Gold',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Silver')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Silver',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Bronze')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Bronze',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Frequent Customer')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Frequent Customer',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='B2B')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'B2B',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Friends & Family')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Friends & Family',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Employees')
	BEGIN
		INSERT INTO USGroup (Id, Title, ProductId, ApplicationId, CreatedDate, CreatedBy, Status, GroupType)
		Values(newId(), 'Employees',@CommerceProductId,@ApplicationId, @Now, @CreatedById,1, 2)
	END

	-- EveryOne Group - We are not using this group as of now
	IF NOT EXISTS(SELECT * FROM USGroup WHERE ProductId = @CommerceProductId And ApplicationId = @ApplicationId And GroupType = 2 AND Title ='Everyone')
	BEGIN
		INSERT INTO USGroup (ProductId, ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, 
		ModifiedBy, ModifiedDate, Status, ExpiryDate, GroupType)
		Values
		(@CommerceProductId,	@ApplicationId,	
		NEWID(),	'Everyone',	'everyone',	
		@CreatedById,	@Now,
		@CreatedById,	@Now,	1,	NULL,	2)
	END

	--- Security Level
	SELECT @maxSecurityId = Isnull(Max(Id),0) + 1 FROM USSecurityLevel

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Commerce Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId, 'Commerce Customer', 'Commerce Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Preferred Customer')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+1, 'Preferred Customer', 'Preferred Customer', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Employee')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+2, 'Employee', 'Employee', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	IF NOT EXISTS(SELECT * FROM USSecurityLevel WHERE ApplicationId = @ApplicationId AND Title ='Press')
	BEGIN
		INSERT INTO USSecurityLevel(ApplicationId, Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, Status, IsSystem)
		Values(@ApplicationId, @maxSecurityId+3, 'Press', 'Press', @CreatedById, @Now,@CreatedById, @Now,1,1)
	END

	-- Feature Type
	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto Top Seller')
	BEGIN
		Insert into MRFeatureType Values(1,'Auto Top Seller')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Auto New Products')
	BEGIN
		Insert into MRFeatureType Values(2,'Auto New Products')
	END

	IF NOT EXISTS(SELECT * FROM MRFeatureType WHERE Title ='Manual')
	BEGIN
		Insert into MRFeatureType Values(3,'Manual')
	END

	-- Feature
	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Top Sellers' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Top Sellers' , 'Top Sellers Auto', 1, 1, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='New Products' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'New Products' , 'New Products Auto', 2,  2, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Moving Brand' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Moving Brand' , 'Moving Brand Manual', 3, 3, 1, newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Stock Clearance' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Stock Clearance' , 'Stock Clearance Manual', 3,  4, 1,newId(), GetUTCDate())

	IF NOT EXISTS(SELECT * FROM MRFeature WHERE Title ='Christmas Sales' And SiteId = @ApplicationId)
		INSERT INTO MRFeature (Id, SiteId, Title, Description, FeatureTypeId, Sequence, Status, CreatedBy, CreatedDate)
		Values(NewId(), @ApplicationId,'Christmas Sales' , 'Christmas Sales Manual', 3,  5, 2,newId(), GetUTCDate())

	-- AttributeDataType
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Image Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Image Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Date' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.DateTime','Date')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Decimal' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Double','Decimal')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Asset Library File' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Asset Library File')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Page Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Page Library')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='HTML' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','HTML')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='File Upload' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','File Upload')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Integer' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Int32','Integer')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='String' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','String')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Boolean')  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.Boolean','Boolean')  END
	IF NOT EXISTS(SELECT * FROM ATAttributeDataType WHERE Title ='Content Library' )  BEGIN   INSERT INTO ATAttributeDataType values(NEWID(),'System.String','Content Library')  END

	-- CommerceStatus 
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Active' )  BEGIN   INSERT INTO CMCommerceStatus values(1,'Active','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Inactive' )  BEGIN   INSERT INTO CMCommerceStatus values(2,'Inactive','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Deleted' )  BEGIN   INSERT INTO CMCommerceStatus values(3,'Deleted','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Approved' )  BEGIN   INSERT INTO CMCommerceStatus values(4,'Approved','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Rejected' )  BEGIN   INSERT INTO CMCommerceStatus values(5,'Rejected','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='PendingForApproval' )  BEGIN   INSERT INTO CMCommerceStatus values(6,'PendingForApproval','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Included' )  BEGIN   INSERT INTO CMCommerceStatus values(7,'Included','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Excluded')  BEGIN   INSERT INTO CMCommerceStatus values(8,'Excluded','')  END
	IF NOT EXISTS(SELECT * FROM CMCommerceStatus WHERE Title ='Expired')  BEGIN   INSERT INTO CMCommerceStatus values(9,'Expired','')  END

	-- GLReason
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Damaged')  BEGIN   INSERT INTO GLReason values(NEWID(),'Damaged',210,4,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Restocking')  BEGIN   INSERT INTO GLReason values(NEWID(),'Restocking',210,1,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Bad Inventory')  BEGIN   INSERT INTO GLReason values(NEWID(),'Bad Inventory',210,2,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Returned Item')  BEGIN   INSERT INTO GLReason values(NEWID(),'Returned Item',210,8,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Lost')  BEGIN   INSERT INTO GLReason values(NEWID(),'Lost',210,3,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Physical Count')  BEGIN   INSERT INTO GLReason values(NEWID(),'Physical Count',210,6,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='New Shipment')  BEGIN   INSERT INTO GLReason values(NEWID(),'New Shipment',210,5,1)  END
	IF NOT EXISTS(SELECT * FROM GLReason WHERE Title ='Other')  BEGIN   INSERT INTO GLReason values(NEWID(),'Other',210,7,1)  END

	-- GLAddress
	IF NOT EXISTS (SELECT * FROM GLAddress Where Id = 'A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F')
	BEGIN
		INSERT INTO  GLAddress(
		Id, FirstName, LastName, AddressType, AddressLine1, AddressLine2, AddressLine3, City, StateId, Zip, 
		CountryId, Phone, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, StateName, County)
		Values
		('A8F664CD-5CC4-4FDD-B3FA-763BDE0A070F',
		'Warehouse','Location',0,'Your warehouse address', 250,	NULL	,'City',
		'7E07B39B-BC66-4969-B16A-C62E41043E39',	'30092',	'AD72C013-B99D-4513-A666-A128343CCFF0',	'7819955555',
		'2011-08-01 11:18:32.880',	'E131010D-BE50-4526-B343-B0954E924080',	'2011-08-01 11:18:32.880',	
		'E131010D-BE50-4526-B343-B0954E924080',	1,	'MA','County')	
	END

	-- INWarehouse
	Declare @WarehouseId uniqueidentifier
	DECLARE @CopyWarehouseId uniqueidentifier
		
	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId Where Title = 'Default Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Default Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
			Values(@WarehouseId,	'Default warehouse',	
			'Default warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
			null,	1, 'Internal', 'InternalWarehouseProvider', 1,1)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
		from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
		Where T.IsDownloadableMedia =0 OR T.IsDownloadableMedia is null

	END

	IF NOT EXISTS (SELECT * FROM INWarehouse W INNER JOIN INWarehouseSite S on W.Id = S.WarehouseId  Where Title = 'Digital Warehouse' And S.SiteId = @ApplicationId)
	BEGIN
		SET @CopyWarehouseId = (SELECT TOP 1 Id FROM INWarehouse where Title = 'Digital Warehouse')
		SET @WarehouseId = NEWID()
		INSERT INTO INWarehouse(Id,  Title, Description, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status, AddressId, IsDefault, WarehouseCode, DotNetProviderName, TypeId,RestockingDelay)
		Values(@WarehouseId,	'Digital Warehouse',	
		'Internal Digital Warehouse',	'2009-01-01 00:00:00.000',	@CreatedById,	NULL,	NULL,	1,	
		Null,	0, Null, 'DigitalWarehouseProvider', 2,0)
		
		INSERT INTO [dbo].[INWarehouseSite]
			([WarehouseId]
			,[SiteId]
			,[Status])
		VALUES
			(@WarehouseId
			,@ApplicationId
			,1)

		INSERT INTO [dbo].[INInventory]
           ([Id]
           ,[WarehouseId]
           ,[SKUId]
           ,[Quantity]
           ,[LowQuantityAlert]
           ,[FirstTimeOrderMinimum]
           ,[OrderMinimum]
           ,[OrderIncrement]
           ,[ReasonId]
           ,[ReasonDetail]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate])

		Select newid()
			,@WarehouseId
			,I.[SKUId]
           ,0
           ,I.[LowQuantityAlert]
           ,I.[FirstTimeOrderMinimum]
           ,I.[OrderMinimum]
           ,I.[OrderIncrement]
           ,I.[ReasonId]
           ,I.[ReasonDetail]
           ,I.[Status]
           ,I.[CreatedBy]
           ,I.[CreatedDate]
           ,I.[ModifiedBy]
           ,I.[ModifiedDate]
			from INInventory I
			Inner join PRProductSKU S on I.SKUId =S.Id AND I.WarehouseId = @CopyWarehouseId
			Inner join PRProduct P on S.ProductId = P.Id
			Inner Join PRProductType T on P.ProductTypeID =T.Id
			Where T.IsDownloadableMedia =1 
	END

	-- Product Type
	--IF NOT EXISTS (SELECT * FROM PRProductType Where SiteId = @ApplicationId)
	IF ((SELECT COUNT(*) FROM PRProductType)=0)
	BEGIN
		Insert into PRProductType(Id,ParentId,LftValue,RgtValue,Title, Description, SiteId, CreatedDate, CreatedBy, Status)
		Values(newId(),@ApplicationId,1,2,@SiteTitle, @SiteTitle ,@ApplicationId, @Now,@CreatedById ,1 )
	END

	-- ORShipper
	DECLARE @Shipper_USPS uniqueidentifier, @Shipper_UPS uniqueidentifier, @Shipper_Fedex uniqueidentifier

	IF EXISTS (SELECT * FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_USPS = (SELECT Id FROM ORShipper Where Name ='USPS' And SiteId =@ApplicationId)
	END	
	ELSE
	BEGIN
		SET @Shipper_USPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_USPS,'USPS','USPS','','iAppsUSPSProvider',1,3,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END	
	IF EXISTS (SELECT * FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_UPS = (SELECT Id FROM ORShipper Where Name ='UPS' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_UPS = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_UPS,'UPS','UPS','http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&tracknum={0}&AgreeToTermsAndConditions=yes&ignore=&track.x=46&track.y=7',
		'iAppsUpsProvider',1,2,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END
	IF EXISTS (SELECT * FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId) 
	BEGIN
		SET @Shipper_Fedex = (SELECT Id FROM ORShipper Where Name ='Fedex' And SiteId =@ApplicationId)
	END
	ELSE
	BEGIN
		SET @Shipper_Fedex = NEWID()
		INSERT INTO [dbo].[ORShipper]([Id],[Name],[Description],[TrackingURL],[DotNetProviderName] ,[Status],[Sequence],
			[CreatedBy],[CreatedDate] ,[ModifiedBy],[ModifiedDate],[SiteId])            
		VALUES (@Shipper_Fedex,'Fedex','Fedex','','provider',1,1,
		@CreatedById,@Now,@CreatedById,@Now,@ApplicationId)
	END

	 
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpressSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpressSM',
		'Worldwide ExpressSM','',5,0,0,0,0,1,'07',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
		
	END	
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air Saver','Next Day Air Saver',
		'customid',1,5,0,0,1,0,'13',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='UPS Next Day Air Early A.M. SM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'UPS Next Day Air Early A.M. SM','UPS Next Day Air® Early A.M. SM',
		'',1,0,0,0,0,0,'14',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Next Flight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Next Flight',
		'','',5,0,0,0,0,1,'INTERNATIONAL_FIRST',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Standard','Standard','',8,0,0,0,0,1,'11',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air A.M' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air A.M','Second Day Air A.M','',2,0,0,0,0,0,'59',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First-Class Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'First-Class Mail','1st Class','customid',5,5,0,0,1,0,'0',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International  Economy' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International  Economy','','',7,0,0,0,0,1,'INTERNATIONAL_ECONOMY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Saver','Saver','',4,0,0,0,0,0,'65',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day A.M.' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day A.M.','','',0,0,0,0,0,0,'FEDEX_2_DAY_AM',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide ExpeditedSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide ExpeditedSM','Worldwide ExpeditedSM','',4,0,0,0,0,1,'08',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Three-Day Select' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Three-Day Select','UPS Three-Day Select','customid',3,5,0,0,1,0,'12',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Second Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Second Day Air','UPS Second Day Air','customid',2,5,0,0,1,0,'02',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Priority Overnight','','',1,0,0,0,0,1,'PRIORITY_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='International Priority' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'International Priority','','',0,0,0,0,0,1,'INTERNATIONAL_PRIORITY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Priority Mail' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Priority Mail','Priority Mail','customid',2,5,0,0,1,0,'1',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express Saver' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Express Saver','','',3,0,0,0,0,0,'FEDEX_EXPRESS_SAVER',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='First Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'First Overnight','Collège dOccitanie','',4,0,0,0,0,1,'FIRST_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Home Delivery' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Home Delivery','','customid',2,5,0,0,1,0,'GROUND_HOME_DELIVERY',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Next Day Air' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Next Day Air','UPS Next Day Air','customid',1,5,0,0,1,0,'01',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Worldwide Express PlusSM' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Worldwide Express PlusSM','Worldwide Express PlusSM','',4,0,0,0,0,1,'54',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='2Day' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'2Day','','',5,0,0,0,0,0,'FEDEX_2_DAY',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null)
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Standard Overnight' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_Fedex,'Standard Overnight','','',4,0,0,0,0,0,'STANDARD_OVERNIGHT',0.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Express' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,
		ExternalShipCode)
		Values (NEWID(),@Shipper_USPS,'Express','Express','customid',1,5,0,0,1,0,'2',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	IF NOT EXISTS (SELECT * FROM ORShippingOption where Name='Ground' And SiteId = @ApplicationId)
	BEGIN 
		INSERT INTO ORShippingOption(Id,ShipperId,Name,Description,CustomId,NumberOfDaysToDeliver,CutOffHour,SaturdayDelivery,
		AllowShippingCoupon,IsPublic,IsInternational,ServiceCode,BaseRate,SiteId,Status,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate, 
		ExternalShipCode)
		Values (NEWID(),@Shipper_UPS,'Ground','UPS Ground','customid',5,5,0,0,1,0,'03',5.00,
		@ApplicationId,1, @CreatedById, @Now, @CreatedById, @Now , null) 
	END
	
	
	
	-- ATAttributeGroup
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='01614e00-86f8-4448-9a48-7daa63171c38')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'01614e00-86f8-4448-9a48-7daa63171c38', N'Features', @ApplicationId, 1, @CreatedById, @Now, @CreatedById, @Now , 1)
	END	
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='b23ffc1b-ab9d-4985-beb4-a5f303b9ef41')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'SKULevel Attributes', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Advanced Details', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='AB730D27-5CCD-49D9-B515-168290438E0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'AB730D27-5CCD-49D9-B515-168290438E0A', N'Product Properties', @ApplicationId, 1, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='3C211CF2-A184-4375-A078-242F4387D1D2')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'3C211CF2-A184-4375-A078-242F4387D1D2', N'Customer Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='9DABE135-467C-459E-ABF2-5FBD64821989')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'9DABE135-467C-459E-ABF2-5FBD64821989', N'Order Attributes', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END
	IF NOT EXISTS (SELECT * FROM ATAttributeGroup Where Id ='ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A')
	BEGIN
		INSERT INTO [ATAttributeGroup] ([Id], [Title], [SiteId], [IsSystem], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
		VALUES (N'ED30EF67-09A6-4A29-9BB8-610A6FBD6F0A', N'Advanced Details', @ApplicationId, 0, 
		@CreatedById, @Now, @CreatedById, @Now , 1)
	END

	-- ATAttribute
	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0611ed6a-d071-4dff-b6c1-b75a06a17ccf')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as Top Seller', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now,  1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='0b94496d-9df4-4ac1-8c5c-48b42f99c4ef')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Discounts', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='2c4983dd-d7eb-40a2-9b05-bfd308e3da62')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Free Shipping', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='4ae05c5a-2375-49d8-8bfb-d131632c0e09')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'System.Boolean', N'01614e00-86f8-4448-9a48-7daa63171c38', N'Promote as New Item', N'', 1, 
	NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='7b39ffe5-36b9-44ef-b7a5-08f3c18181aa')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Refundable', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c1a20d3c-985f-49be-9682-df09eaf82fc8')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Freight', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='c57665fd-e681-4c13-82ce-d77e60bf9a72')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Tax Exempt', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f4963a96-57dc-4c67-830e-f7426539c8ce')
	--INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	--[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	--VALUES (N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'System.Boolean', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Unlimited Quantity', N'', 1, 
	--NULL, 0, 0, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f68ceb85-922b-4023-a6f9-20025f7fdc94')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'System.String', N'b23ffc1b-ab9d-4985-beb4-a5f303b9ef41', N'Others', 
	NULL, 0, N'AT1', 1, 1, 1, 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='f7102f66-4242-408e-a816-f8b3e600f4d0')
	INSERT INTO [ATAttribute] ([Id], [AttributeDataType], [AttributeGroupId], [Title], [Description], [Sequence], [Code], [IsFaceted], [IsSearched], 
	[IsSystem], [IsDisplayed], [IsEnum], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) 
	VALUES (N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'System.Boolean', N'ed30ef67-09a6-4a29-9bb8-610a6fbd6f0a', N'Exclude from Shipping Promotions', N'', 1, 
	NULL, 0, 0, 1, 0, 0, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='39C2D877-525F-49A6-9E48-D3296989F83B')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('39C2D877-525F-49A6-9E48-D3296989F83B','System.String','AB730D27-5CCD-49D9-B515-168290438E0A','Product Name',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	IF NOT EXISTS (SELECT * FROM ATAttribute Where Id ='ECB9E9D1-FC3F-43A5-B40F-3840212F693C')
	INSERT INTO [ATAttribute] (Id,AttributeDataType,AttributeGroupId,Title,Description,Sequence,Code,IsFaceted,IsSearched,
	IsSystem,IsDisplayed,IsEnum,CreatedBy,CreatedDate,[ModifiedBy], [ModifiedDate],Status)
	Values('ECB9E9D1-FC3F-43A5-B40F-3840212F693C','System.Double','AB730D27-5CCD-49D9-B515-168290438E0A','ListPrice',
	null,1,null,1,0,1,0,0,@CreatedById, @Now, @CreatedById, @Now,1)

	-- ATAttributeEnum
	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='bbf74b61-05d8-49b4-8fee-72147d849814')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'bbf74b61-05d8-49b4-8fee-72147d849814', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='073c66b8-2fc2-4d82-aebe-3165f0138ad5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'073c66b8-2fc2-4d82-aebe-3165f0138ad5', 
	N'7b39ffe5-36b9-44ef-b7a5-08f3c18181aa', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='51424fd1-0da2-4792-b692-7beef5679bf3')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'51424fd1-0da2-4792-b692-7beef5679bf3', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	--IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='f0974b83-4d83-4534-bf87-1f8a8b0237e8')
	--INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	--[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'f0974b83-4d83-4534-bf87-1f8a8b0237e8', 
	--N'f4963a96-57dc-4c67-830e-f7426539c8ce', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='0b99d94e-606a-43a7-b72c-d6512fd38bf2')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'0b99d94e-606a-43a7-b72c-d6512fd38bf2', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='235f7a94-52c7-42d8-b870-c0d53906b1e5')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'235f7a94-52c7-42d8-b870-c0d53906b1e5', 
	N'c57665fd-e681-4c13-82ce-d77e60bf9a72', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='11bfcfbd-3ffa-4791-8b39-1f12b80d8524')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'11bfcfbd-3ffa-4791-8b39-1f12b80d8524', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='d8d961c0-5c4a-487b-9913-a920e73cda1a')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'd8d961c0-5c4a-487b-9913-a920e73cda1a', 
	N'0611ed6a-d071-4dff-b6c1-b75a06a17ccf', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='3a1e07c0-18d1-4c00-a73c-73a4953d5e41')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'3a1e07c0-18d1-4c00-a73c-73a4953d5e41', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='872eeb49-e8eb-4f73-837c-71ddb20629c4')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'872eeb49-e8eb-4f73-837c-71ddb20629c4', 
	N'0b94496d-9df4-4ac1-8c5c-48b42f99c4ef', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='ee919400-c7de-4d9c-ae8f-fefd1c768e64')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'ee919400-c7de-4d9c-ae8f-fefd1c768e64', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='49a6b896-2f22-45d1-82fb-e47e8f2519c9')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'49a6b896-2f22-45d1-82fb-e47e8f2519c9', 
	N'2c4983dd-d7eb-40a2-9b05-bfd308e3da62', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='8bb63596-8fee-4005-9109-dda014ba147e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'8bb63596-8fee-4005-9109-dda014ba147e', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='70a27f03-06ca-4df2-99bb-5c23b840d465')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'70a27f03-06ca-4df2-99bb-5c23b840d465', 
	N'f7102f66-4242-408e-a816-f8b3e600f4d0', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='e5f89737-8879-4e42-9aa1-bfffb05d808e')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'e5f89737-8879-4e42-9aa1-bfffb05d808e', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'true', N'1', 1, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'7d9f8d8e-f979-41e9-8f0d-c8fa5e77624d', 
	N'c1a20d3c-985f-49be-9682-df09eaf82fc8', N'false', N'0', 0, N'false', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='9b0a9c92-80c8-411f-9037-b0281c755201')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'9b0a9c92-80c8-411f-9037-b0281c755201', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'true', N'1', 1, N'true', 1, 2, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='81a55708-cd4d-4fbd-b3c5-4e3292401511')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'81a55708-cd4d-4fbd-b3c5-4e3292401511', 
	N'4ae05c5a-2375-49d8-8bfb-d131632c0e09', N'false', N'0', 0, N'false', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='a0c6381b-2970-4d09-a582-334b93c51442')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'a0c6381b-2970-4d09-a582-334b93c51442', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'true', N'0', 0, N'true', 0, 1, @CreatedById, @Now, @CreatedById, @Now, 1)

	IF NOT EXISTS (SELECT * FROM ATAttributeEnum Where Id ='691ee3cd-58ff-42d9-a8e8-385e85648cfc')
	INSERT INTO [ATAttributeEnum] ([Id], [AttributeID], [Title], [Code], [NumericValue], [Value], [IsDefault], [Sequence], 
	[CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status]) VALUES (N'691ee3cd-58ff-42d9-a8e8-385e85648cfc', 
	N'f68ceb85-922b-4023-a6f9-20025f7fdc94', N'false', N'1', 2, N'false', 0, 2, @CreatedById, @Now, @CreatedById, @Now, 1)


	--- CLImageType
	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='396b425c-e197-4f2d-a4a3-25448ca366f8')
	INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('396b425c-e197-4f2d-a4a3-25448ca366f8', N'main', 600, 800, null, N'alt_mainImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 1)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='63cf2418-684a-4d1f-88b2-34968c6f139f')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('63cf2418-684a-4d1f-88b2-34968c6f139f', N'system_thumb', 60, 60, N'systhumb_', null, 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 0)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='2770534e-4675-4764-a620-56776398e03e')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('2770534e-4675-4764-a620-56776398e03e', N'preview', 260, 372, N'prev_', N'alt_PrevImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 3)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='e02ed0f3-1271-4218-b81e-816e458434a1')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('e02ed0f3-1271-4218-b81e-816e458434a1', N'mini', 80, 80, N'mini_', N'alt_miniImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 4)

	IF NOT EXISTS (SELECT * FROM CLImageType Where Id ='ba9b3922-4f9f-4b21-a5cf-e03b26b588ad')
		INSERT INTO [dbo].[CLImageType] ([Id], [Title], [Height], [Width], [Prefix], [NotAvailableImage], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [SiteId], [TypeNumber]) 
		VALUES('ba9b3922-4f9f-4b21-a5cf-e03b26b588ad', N'thumbnail', 120, 120, N'thumb_', N'alt_thumbImage.jpg', 1, @CreatedById, @Now, @CreatedById, @Now, @ApplicationId, 2)

	--OROrderStatus
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='OK')  BEGIN   INSERT INTO OROrderStatus values(1,'OK','Submitted',1,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Shipped')  BEGIN   INSERT INTO OROrderStatus values(2,'Shipped','Shipped',2,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Canceled')  BEGIN   INSERT INTO OROrderStatus values(3,'Canceled','Canceled',3,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Back Ordered')  BEGIN   INSERT INTO OROrderStatus values(4,'Back Ordered','Back Ordered',4,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Approval')  BEGIN   INSERT INTO OROrderStatus values(5,'Pending Approval','On Hold',5,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Customer Registration Approval')  BEGIN   INSERT INTO OROrderStatus values(6,'Pending Customer Registration Approval','Pending Customer Registration Approval',6,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Pending Bad Customer Order Approval')  BEGIN   INSERT INTO OROrderStatus values(7,'Pending Bad Customer Order Approval','Pending Bad Customer Order Approval',7,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Terms Limit')  BEGIN   INSERT INTO OROrderStatus values(8,'Exceeded Terms Limit','Exceeded Terms Limit',8,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Exceeded Line of Credit')  BEGIN   INSERT INTO OROrderStatus values(9,'Exceeded Line of Credit','Exceeded Line of Credit',9,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Manual Hold')  BEGIN   INSERT INTO OROrderStatus values(10,'Manual Hold','Manual Hold',10,5)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Partially Shipped')  BEGIN   INSERT INTO OROrderStatus values(11,'Partially Shipped','Partially Shipped',11,0)  END
	IF NOT EXISTS(SELECT * FROM OROrderStatus WHERE Name ='Created')  BEGIN   INSERT INTO OROrderStatus values(12,'Created','Created',12,0)  END

	-- OROrderNextStatus
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderNextStatus values(1,1,10)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderNextStatus values(2,1,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderNextStatus values(3,7,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderNextStatus values(4,7,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderNextStatus values(5,10,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderNextStatus values(6,10,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderNextStatus values(7,5,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =8)  BEGIN   INSERT INTO OROrderNextStatus values(8,5,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =9)  BEGIN   INSERT INTO OROrderNextStatus values(9,6,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =10)  BEGIN   INSERT INTO OROrderNextStatus values(10,6,3)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =11)  BEGIN   INSERT INTO OROrderNextStatus values(11,12,1)  END
	IF NOT EXISTS(SELECT * FROM OROrderNextStatus WHERE Id =12)  BEGIN   INSERT INTO OROrderNextStatus values(12,11,2)  END

	-- OROrderType
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =1)  BEGIN   INSERT INTO OROrderType values(1,'Web','Web Order',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =2)  BEGIN   INSERT INTO OROrderType values(2,'CSR','Web Service Order',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =3)  BEGIN   INSERT INTO OROrderType values(3,'Brochure Request','Brochure Request',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =4)  BEGIN   INSERT INTO OROrderType values(4,'Recurring Order','Recurring ORder',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderType Where Id =5)  BEGIN   INSERT INTO OROrderType values(5,'Exchange Order','Exchange Order',5)  END

	-- OROrderItemStatus
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =1)  BEGIN   INSERT INTO OROrderItemStatus values(1,'Ok','Submitted',1)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =2)  BEGIN   INSERT INTO OROrderItemStatus values(2,'Shipped','Shipped',2)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =3)  BEGIN   INSERT INTO OROrderItemStatus values(3,'Cancelled','Cancelled',3)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =4)  BEGIN   INSERT INTO OROrderItemStatus values(4,'Backordered','Backordered',4)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =5)  BEGIN   INSERT INTO OROrderItemStatus values(5,'Pending','Pending',5)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =6)  BEGIN   INSERT INTO OROrderItemStatus values(6,'Released','Released',6)  END
	IF NOT EXISTS(SELECT * FROM OROrderItemStatus WHERE Id =7)  BEGIN   INSERT INTO OROrderItemStatus values(7,'Partially Shipped','Partially Shipped',7)  END

	-- PTCreditCardType
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE Id ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487','American Express','American Express',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='28091FD7-00C2-4E71-A266-7FA718658A66')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('28091FD7-00C2-4E71-A266-7FA718658A66','MasterCard','MasterCard',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='A98F6177-90A5-420D-96B5-A234778F8D96')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('A98F6177-90A5-420D-96B5-A234778F8D96','Visa','Visa',@CreatedById, @Now, @CreatedById, @Now)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardType WHERE  Id ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E')  
	BEGIN   
		INSERT INTO PTCreditCardType(Id,Title,Description,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E','Discover','Discover',@CreatedById, @Now, @CreatedById, @Now)  
	END

	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE CreditCardTypeId ='F6DDE79A-A4E2-485D-8B98-2D8A5218B487' And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('F6DDE79A-A4E2-485D-8B98-2D8A5218B487',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='28091FD7-00C2-4E71-A266-7FA718658A66'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('28091FD7-00C2-4E71-A266-7FA718658A66',@ApplicationId,1) 
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='A98F6177-90A5-420D-96B5-A234778F8D96'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('A98F6177-90A5-420D-96B5-A234778F8D96',@ApplicationId,1)  
	END
	IF NOT EXISTS(SELECT * FROM PTCreditCardTypeSite WHERE  CreditCardTypeId ='8ACBD7F2-48DF-4129-B1D1-E8F24D01229E'  And SiteId =@ApplicationId)  
	BEGIN   
		INSERT INTO PTCreditCardTypeSite(CreditCardTypeId,SiteId,Status)
		values('8ACBD7F2-48DF-4129-B1D1-E8F24D01229E',@ApplicationId,1)  
	END

	-- PTGateway
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =1)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(1,'Website Payments Pro','Website Payments Pro',2,'WebsitePaymentsProGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =2)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(2,'None','None',1,'AutoGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =3)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description], [Sequence], [DotNetProviderName]) 
		Values(3,'Cybersource','Cybersource',3,'CybersourceGatewayProvider')  
	END
	IF NOT EXISTS(SELECT * FROM PTGateway WHERE Id =4)  
	BEGIN   
		INSERT INTO PTGateway ([Id], [Name], [Description],  [Sequence], [DotNetProviderName]) 
		Values(4,'Payflow Pro','Payflow Pro',4,'PayflowProGatewayProvider') 
	END

	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =1)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(1,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =2)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(2,0,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =3)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(3,1,1,@ApplicationId)  
	END
	IF NOT EXISTS(SELECT * FROM PTGatewaySite WHERE GatewayId =4)  
	BEGIN   
		INSERT INTO PTGatewaySite ([GatewayId], [IsActive], PaymentTypeId,SiteId) 
		Values(4,0,1,@ApplicationId) 
	END
	
	-- PTPaymentStatus
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =1)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(1,'Entered','Entered',1,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =2)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(2,'Auth','Authorized',2,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =3)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(3,'Captured','Captured',3,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =4)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(4,'Auth Failed','Authorization Failed',4,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =5)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(5,'Capture Failed','Capture Failed',5,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =6)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(6,'Clear','Clear',6,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =7)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(7,'Manual Cancel','Manual Cancel',7,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =8)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(8,'Partially Captured','Partially Captured',8,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =9)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(9,'Void','Void',9,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =10)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(10,'Refund Entered','Refund Entered',10,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =11)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(11,'Refund Waiting Approval','Refund Waiting Approval',11,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =12)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(12,'Refunded','Refunded',12,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =13)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(13,'Refund Failed','Refund Failed',13,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentStatus WHERE Id =14)  BEGIN   INSERT INTO PTPaymentStatus ([Id], [Name], [Description], [Sequence], [Status]) Values(14,'Refund Rejected','Refund Rejected',14,1)  END

	-- PTPaymentType
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =1)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(1,'CreditCard','Credit Card',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =2)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(2,'OnAccount','OnAccount',1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =3)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(3,'Paypal','Paypal',5)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =4)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(4,'COD','Cash On Delivery',4)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentType WHERE Id =5)  BEGIN   INSERT INTO PTPaymentType ([Id], [Name], [Description], [Sequence]) Values(5,'GiftCard','Gift Card',2)  END

	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =1)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(1,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =2)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(2,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =3)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(3,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =4)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(4,@ApplicationId,1)  END
	IF NOT EXISTS(SELECT * FROM PTPaymentTypeSite WHERE PaymentTypeId =5)  BEGIN   INSERT INTO PTPaymentTypeSite ([PaymentTypeId], [SiteId], [Status]) Values(5,@ApplicationId,1)  END

	IF (SELECT Count(*) FROM ORReturnReason)=0
	BEGIN

		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES( 
			'Defective')
			
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(  
			'Customer Ordered Wrong Product')
		    
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES('Refused Delivery')


		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Missing Merchandise')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Did not Like')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Customer Changed Mind')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Billing Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Fraudulent Order')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Warehouse Shipping Error')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Returned by Shipper - No Response From Customer')
		INSERT INTO dbo.ORReturnReason
				(Title)
		VALUES(    'Other')

	END

	-- INWarehouseType
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =1)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(1,'Internal','iAPPS internal warehouse for Non-Digital Products')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =2)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(2,'Digital','iAPPS Digital warehouse')  END
	IF NOT EXISTS(SELECT * FROM INWarehouseType WHERE Id =3)  BEGIN   INSERT INTO INWarehouseType ([Id], [Title], [Description]) Values(3,'External','Integrated with external system')  END

	-- INRestockingStatus
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Created'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (1,'Created','Created',1)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Queued'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (2,'Queued','Queued',2)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='In Progress'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (3,'In Progress','In Progress',3)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Canceled'))
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (4,'Canceled','Canceled',4)
	IF(NOT EXISTS(SELECT 1 FROM dbo.INRestockingStatus WHERE Name='Completed'))	
		INSERT INTO INRestockingStatus (Id, Name, CustomerFriendlyName, Sequence) Values (5,'Completed','Completed',5)

	-- ORReturnResolutionType
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] ON
	Select @maxId =Isnull(MAX(Id),0) FROM ORReturnResolutionType
	SET @maxId = @maxId + 1

	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Refund'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Refund','Part or full paymnent is returned', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Replacement'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Replacement','Replacement', @maxId)        
		SET @maxId = @maxId + 1
	END
	IF(NOT EXISTS(SELECT 1 FROM dbo.ORReturnResolutionType WHERE Title='Others'))  
	BEGIN   
		INSERT INTO dbo.ORReturnResolutionType(Id, Title, Description, SortOrder) VALUES (@maxId,'Others','Others', @maxId)        
		SET @maxId = @maxId + 1
	END
	SET IDENTITY_INSERT [dbo].[ORReturnResolutionType] OFF

	-- INInventory
	IF(NOT EXISTS(SELECT 1 FROM dbo.INInventory WHERE WarehouseId ='22FAC4BD-4954-415A-9014-3598DAEAB4D0'))  
	BEGIN   
		Declare @Id UniqueIdentifier
		Set @Id= '22FAC4BD-4954-415A-9014-3598DAEAB4D0'

		INSERT INTO [INInventory]
				   ([Id]
				   ,[WarehouseId]
				   ,[SKUId]
				   ,[Quantity]
				   ,[LowQuantityAlert]
				   ,[FirstTimeOrderMinimum]
				   ,[OrderMinimum]
				   ,[OrderIncrement]
				   ,[ReasonId]
				   ,[ReasonDetail]
				   ,[Status]
				   ,[CreatedBy]
				   ,[CreatedDate]
				   ,[ModifiedBy]
				   ,[ModifiedDate])
					SELECT newId()
					  ,@Id
					  ,Id
					  ,0
					  ,0
					  ,1
					  ,1
					  ,1
					  ,(SELECT Id FROM GLReason Where Title ='Other') As ReasonId
					  ,'Initial Data When SKU is created'
					  ,1
					  ,@CreatedById
					  ,@Now
					  ,@CreatedById
					  ,@Now
				   FROM PRProductSKU WHERE Id NOT IN 
					(SELECT SKUID FROM ININventory WHERE WarehouseId = @Id)
	END
	

	DECLARE @GroupObjectTypeId int
	DECLARE @MerchandisingRoleId int, @FulfillmentRoleId int, @MarketingRoleId int, @CSRRoleId int, 
		@EveryOneRoleId int, @CSRManagerRoleId int, @WHManagerRoleId int
	DECLARE @MerchandisingGroupId uniqueidentifier, @FulfillmentGroupId uniqueidentifier, 
		@MarketingGroupId uniqueidentifier, @CSRGroupId uniqueidentifier, @EveryOneGroupId uniqueidentifier, 
		@CSRManagerGroupId uniqueidentifier, @WHManagerGroupId uniqueidentifier

	SET @GroupObjectTypeId = 15 
	SET @MerchandisingRoleId = 19
	SET @FulfillmentRoleId = 20
	SET @MarketingRoleId = 21
	SET @CSRRoleId = 22
	SET @EveryOneRoleId = 29
	SET @CSRManagerRoleId = 32
	SET @WHManagerRoleId = 33	
	-- if the db has cms and default commerce data was missing 
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MerchandisingGroupId output,@Title = 'Merchandising',
			@Description = 'Merchandising',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@MerchandisingGroupId output, @Title=N'Merchandising',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @MerchandisingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Merchandising')

	IF (@MerchandisingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MerchandisingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MerchandisingGroupId,2,@MerchandisingRoleId, @MerchandisingGroupId,1,1 )				
	END
	
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Fulfillment')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @FulfillmentGroupId output,@Title = 'Fulfillment',
			@Description = 'Fulfillment',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@FulfillmentGroupId output, @Title=N'Fulfillment',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @FulfillmentGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Fulfillment')

	IF (@FulfillmentGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @FulfillmentGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@FulfillmentGroupId,2,@FulfillmentRoleId, @FulfillmentGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'Marketing')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @MarketingGroupId output,@Title = 'Marketing',
			@Description = 'Marketing',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@MarketingGroupId output, @Title=N'Marketing',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @MarketingGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'Marketing')

	IF (@MarketingGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @MarketingGroupId)
	BEGIN		
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@MarketingGroupId,2,@MarketingRoleId, @MarketingGroupId,1,1 )				
	END

	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSR')) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRGroupId output,@Title = 'CSR',
			@Description = 'CSR',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@CSRGroupId output, @Title=N'CSR',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @CSRGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSR')
	
	IF (@CSRGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRGroupId,2,@CSRRoleId, @CSRGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'EveryOne' And GroupType = 1)) 
	BEGIN		
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @EveryOneGroupId output,@Title = 'EveryOne',
			@Description = 'EveryOne',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@EveryOneGroupId output, @Title=N'EveryOne',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @EveryOneGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'EveryOne')

	IF (@EveryOneGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @EveryOneGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@EveryOneGroupId,2,@EveryOneRoleId, @EveryOneGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'CSRManager')) 
	BEGIN	
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @CSRManagerGroupId output,@Title = 'CSRManager',
			@Description = 'CSRManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@CSRManagerGroupId output, @Title=N'CSRManager',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @CSRManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'CSRManager')

	IF (@CSRManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @CSRManagerGroupId)
	BEGIN				
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@CSRManagerGroupId,2,@CSRManagerRoleId, @CSRManagerGroupId,1,1 )				
	END
	
	IF(NOT EXISTS(SELECT 1 FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId  And Title = 'WHManager')) 
	BEGIN
		EXEC Group_Save  @CommerceProductId,@ApplicationId=@CommerceProductId, @Id = @WHManagerGroupId output,@Title = 'WHManager',
			@Description = 'WHManager',@CreatedBy=@CreatedById,@ModifiedBy=NULL,@ExpiryDate=NULL,@Status=NULL,@GroupType=1	
			
		EXEC HierarchyNode_Save @Id=@WHManagerGroupId output, @Title=N'WHManager',@ParentId=NULL,@Keywords=NULL,
			@ObjectTypeId=@GroupObjectTypeId, @ApplicationId=@ApplicationId		
	END		
	ELSE
		SET @WHManagerGroupId = (SELECT Id FROM USGroup  WHERE ProductId = @CommerceProductId AND ApplicationId =@CommerceProductId And Title = 'WHManager')

	IF (@WHManagerGroupId IS NOT NULL ) AND NOT EXISTS(
		SELECT 1 FROM USMemberRoles WHERE ApplicationId = @ApplicationId AND MemberId = @WHManagerGroupId)
	BEGIN			
		INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
			Values(@CommerceProductId, @ApplicationId,@WHManagerGroupId,2,@WHManagerRoleId, @WHManagerGroupId,1,1 )				
	END
			
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[NavFilter_GetObjectsP]'
GO
/*****************************************************
* Name : NavFilter_GetObjectsP
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--Paging for NavFilter
ALTER PROCEDURE [dbo].[NavFilter_GetObjectsP]
(@FilterId uniqueidentifier
,@ObjectType int
,@OutputStatus int=1
,@ApplicationId uniqueidentifier
,@PageSize int
,@PageNumber int
,@SortColumn nvarchar(255) = null
) --@ObjectType 0 - Get the record from Result table 
-- 1 - If not latest Executes the Query and get the result 
-- 2 - Returns the record if it is latest or no records
-- 3 - Executes the query always
AS
BEGIN
----
--DECLARE @PageSize int
--SET @PageSize = 10
--DECLARE @PageNumber int
--SET @PageNumber = 1
--DECLARE @FilterId Uniqueidentifier
--SET @FilterId = '328BF7FE-A8F1-4136-84A3-33BD734D068A'
--DECLARE @OutputStatus int
--set @OutputStatus = 1
--DECLARE @ApplicationId uniqueidentifier
--DECLARE @SortColumn varchar(64)
--SET @SortColumn = 'status desc'

	declare @isLatest bit
	Select @IsLatest =IsLatest from NVFilterQuery Where Id=@FilterId And 
		(@ApplicationId is null or SiteId = @ApplicationId)
		--SiteId=ISNULL(@ApplicationId, SiteId)

	if ((@IsLatest=0 AND @OutputStatus =1) OR @OutputStatus=3)
	Begin
		Exec NavFilter_ExecuteQuery @FilterId 
	End


DECLARE @SQL nvarchar(max)
SET @SortColumn = (@SortColumn)
IF @SortColumn = 'name asc'
	SET @SortColumn = 'P.Title ASC'
ELSE IF @SortColumn = 'name desc'
	SET @SortColumn = 'P.Title DESC'
ELSE IF @SortColumn = 'lastmodifieddate asc'
	SET @SortColumn = 'P.CreatedDate ASC'
ELSE IF @SortColumn = 'lastmodifieddate desc'
	SET @SortColumn = 'P.CreatedDate DESC'
ELSE IF @SortColumn = 'status asc'
	SET @SortColumn = 'NFE.ObjectId ASC'
ELSE IF @SortColumn = 'status desc'
	SET @SortColumn = 'NFE.ObjectId DESC'
ELSE
	SET @SortColumn = 'P.Title ASC'
SET @SQL = N'
SELECT * FROM (
	Select 
		FO.ObjectId, 
		Row_Number() Over(Order By '+@SortColumn+') as Row_ID,   
		CASE COALESCE(NFE.ObjectId,''00000000-0000-0000-0000-000000000000'')
			WHEN ''00000000-0000-0000-0000-000000000000'' THEN 0
			ELSE 1 END
		as Exclude 
FROM NVFilterOutput FO
INNER JOIN PRProduct P ON FO.ObjectId = P.Id
LEFT JOIN NVFilterExclude NFE ON NFE.ObjectId = FO.ObjectId and NFE.QueryId = '''+cast(@FilterId as varchar(64))+'''
 Where FO.QueryId='''+cast(@FilterId as varchar(64))+'''


)TV


WHERE Row_ID >= ('+cast(@PageSize as varchar(32))+' * '+cast(@PageNumber as varchar(32))+') - ('+cast(@PageSize as varchar(32))+' -1) AND Row_ID <= '+cast(@PageSize as varchar(32))+' * '+cast(@PageNumber as varchar(32))+'
'

print @SQL
EXEC sp_executesql   @SQL 


	Select COUNT(*) as [Count] from NVFilterOutput Where QueryId=@FilterId
	-- and ObjectId not in (Select ObjectId from NVFilterExclude Where QueryId=@FilterId)
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[UserShippingAddress_Save]'
GO
/*****************************************************
* Name : UserShippingAddress_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[UserShippingAddress_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@NickName			nvarchar(255) =null,
	@IsPrimary			bit,
	@Sequence			int =null,
	@FirstName				nvarchar(255)=null,
	@LastName				nvarchar(255)=null,
	@AddressType		int =null,
	@AddressLine1		nvarchar(255)=null,
	@AddressLine2		nvarchar(255)=null,
	@AddressLine3		nvarchar(255)=null,
	@City				nvarchar(255)=null,
	@StateId			uniqueidentifier=null,
	@StateName			nvarchar(255)=null,
	@County				nvarchar(255) = null,
	@CountryId			uniqueidentifier=null,
	@Phone				nvarchar(50)=null,
	@Zip				nvarchar(50)=null,
	@UserId				uniqueidentifier,
	@ApplicationId		uniqueidentifier
	
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int,
		@hasPrimary int,
		@AddressId uniqueidentifier
Begin
--********************************************************************************
-- code
--********************************************************************************

	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

		UPDATE USUserShippingAddress SET IsPrimary = 0 FROM USUserShippingAddress
		WHERE [UserId]=@UserId	and @IsPrimary =1
/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  USUserShippingAddress where Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end  

	
		if ((select count(*) from USUserShippingAddress where  Status != dbo.GetDeleteStatus() AND (LTRIM(RTRIM(@NickName))) = (LTRIM(RTRIM(NickName))) AND @UserId = UserId AND ((@Id IS NOT NULL AND @Id != Id ) OR @Id IS NULL)  ) != 0)
		begin
			raiserror('Nick Name must be unique||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
		end
	

	set @Now = GetUTCDate()
	IF @ModifiedDate is null
		SET @ModifiedDate =@Now
	ELSE
	Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)

	IF (@Id is null)			-- new insert
	BEGIN
		SET @stmt = 'Address Insert'
		SET @Id = newid();
		
		Set @AddressId = newid()

		INSERT INTO[dbo].[GLAddress]
           ([Id]
           ,[FirstName]
		   ,[LastName]
           ,[AddressType]
           ,[AddressLine1]
           ,[AddressLine2]
		   ,[AddressLine3]
		   ,[City]
           ,[StateId]
           ,[StateName]
           ,[Zip]
		   ,[County]
           ,[CountryId]
		   ,[Phone]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status])
		VALUES
           (@AddressId
           ,@FirstName
		   ,@LastName
           ,@AddressType
           ,@AddressLine1
           ,@AddressLine2
		   ,@AddressLine3
           ,@City
           ,@StateId
           ,@StateName
           ,@Zip
		   ,@County
           ,@CountryId
			,@Phone
           ,@ModifiedDate
           ,@ModifiedBy
		   ,@ModifiedDate
           ,@ModifiedBy
           ,@Status)

			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end





	
		INSERT INTO [dbo].[USUserShippingAddress]
           ([Id]
           ,[NickName]
           ,[AddressId]
			,[UserId]
           ,[IsPrimary]
           ,[Sequence]
           ,[CreatedDate]
           ,[CreatedBy]
           ,[ModifiedDate]
           ,[ModifiedBy]
           ,[Status])
		VALUES
           (@Id
           ,@NickName
           ,@AddressId
		   ,@UserId
           ,@IsPrimary
	       ,@Sequence
           ,@ModifiedDate
           ,@ModifiedBy
           ,@ModifiedDate
           ,@ModifiedBy
           ,@Status)
			
			select @error = @@error
			if @error <> 0
			begin
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
        END
        ELSE			-- update
        BEGIN
			SET @stmt = 'Address Update'
			
		

			UPDATE [dbo].[USUserShippingAddress] WITH (ROWLOCK)
			SET [NickName] = @NickName
			  ,[UserId] = @UserId
			  ,[IsPrimary] = @IsPrimary
			  ,[Sequence] = @Sequence
			  ,[ModifiedDate] = @Now
			  ,[ModifiedBy] = @ModifiedBy
			  ,[Status] = @Status
			WHERE Id=@Id
		--	AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				RAISERROR('CONCURRENCYERROR USERSHIPPINGADDRESS',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
			SET @rowcount= 0	
			
			SELECT @AddressId = AddressId FROM USUserShippingAddress WHERE Id = @Id

			UPDATE [dbo].[GLAddress] WITH (ROWLOCK)
			SET [FirstName] = @FirstName
			 ,[LastName] = @LastName
			  ,[AddressType] = @AddressType
			  ,[AddressLine1] = @AddressLine1
			  ,[AddressLine2] = @AddressLine2
			  ,[AddressLine3] = @AddressLine3
			  ,[City] = @City
			  ,[StateId] = @StateId
			  ,[StateName]=@StateName
			  ,[Phone] = @Phone
			  ,[Zip] = @Zip
			  ,[County] = @County
			  ,[CountryId] = @CountryId
			  ,[ModifiedDate] = @Now
			  ,[ModifiedBy] = @ModifiedBy
			  ,[Status] = @Status
			WHERE Id=@AddressId
			--AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
					
			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				RAISERROR('CONCURRENCYERROR GLADDRESS',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Users_ImportBatch]'
GO
/*****************************************************
* Name : Users_ImportBatch
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Users_ImportBatch]
(
	@OverwriteExistingContacts bit,
	@ModifiedBy Uniqueidentifier,
	@BatchId UniqueIdentifier,
	@BatchUserTable varchar(250),
	@BatchMembershipTable varchar(250),
	@BatchProfileTable varchar(250),
	@BatchSiteUserTable varchar(250),
	@BatchUserDistributionListTable varchar(250),
	@BatchUSMarketierUserTable  varchar(250),
	@BatchIndexTermTable varchar(250) ,
	@BatchAddressTable varchar(250),
	@OverrideWSUSersAndCustomers bit =0
)
As
Begin
	declare
		@BatchTablePrefix varchar(250),
		@BatchIdMapTableName varchar(250),
		@StrModifiedBy varchar(36),
		@ImportedContacts int ,
		@AddedToDistributionList int ,
		@ExistingContacts int,
		@UpdatedContacts int
		
		SET @AddedToDistributionList = 0
		SET @ImportedContacts =  0
		SET @UpdatedContacts =  0

	-- set up temp table name for batch import user id mapping
	Set @BatchTablePrefix = 'ZTMP' + Replace(Convert(varchar(36), @BatchId), '-', '')
	set @BatchIdMapTableName = @BatchTablePrefix + '_BatchImportIdMap'
	set @StrModifiedBy = Convert(varchar(36), @ModifiedBy)
	
	--start delete duplicate records within the temporary tables if any
	exec('delete from ' + @BatchUserTable + '  where ID in (select ID from
	(Select Id, row_number()over(partition by UserName order by CreatedDate) as rnum--,UserName,FirstName,LastActivityDate
	from ' + @BatchUserTable + '
	Where UserName in (
	select UserName
	from ' + @BatchUserTable + ' U
	Group by UserName
	Having count(*)>1
	)) Duplicates where rnum >1)')
	
	exec ('delete from ' + @BatchMembershipTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	exec ('delete from ' + @BatchProfileTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	exec ('delete from ' + @BatchSiteUserTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	exec ('delete from ' + @BatchUserDistributionListTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	exec ('delete from ' + @BatchUSMarketierUserTable + ' where UserId not in (select Id from '+@BatchUserTable+')')	
	exec ('delete from ' + @BatchAddressTable + ' where UserId not in (select Id from '+@BatchUserTable+')')
	--end delete duplicate records within the temporary tables if any


	if exists(select * from sysobjects where name = @BatchIdMapTableName)
		Exec('Drop Table ' + @BatchIdMapTableName)

	-- Step 1 locate existing records and create mapping
	Exec(
		'Create Table ' + @BatchIdMapTableName + '(' +
			'importId UniqueIdentifier, ' +
			'actualId UniqueIdentifier, ' +
			'importAddressId UniqueIdentifier, ' +
			'actualAddressId uniqueidentifier ' +
		')'
		)

print @BatchIdMapTableName

	Exec(
		'Create Index ' + @BatchTablePrefix + '_MappingImportId On ' + @BatchIdMapTableName + '(importId)'
	)

print @BatchTablePrefix 


	-- 1a: locate existing users and populate id mapping
	Exec(
		'Insert Into ' + @BatchIdMapTableName + ' (importId, actualId, importAddressId, actualAddressId) ' +
		'Select z.UserId, m.UserId, zu.AddressId, mp.AddressId ' +
		'From ' + @BatchMembershipTable + ' z ' +
			'Join ' + @BatchUSMarketierUserTable + ' zu on zu.UserId = z.UserId ' +
			'Join USMembership m on m.email = z.email ' +
			'left outer Join USMarketierUserProfile mp on mp.UserId = m.UserId '
		)

print  @BatchMembershipTable 

	-- 1b: update batch table user id fields to the existing user's id
	Exec(
		'Update ' + @BatchUserTable + ' Set ' +
			'ID = m.actualId ' +
		'From ' + @BatchUserTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importId = z.Id'
		)
		
print @BatchAddressTable
	Exec(
		'Update ' + @BatchAddressTable + ' Set ' +
			'ID = isnull(m.actualAddressId, m.importAddressId) ' +
		'From ' + @BatchAddressTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importAddressId = z.Id'
		)

print  @BatchIdMapTableName 

	Exec(
		'Update ' + @BatchMembershipTable + ' Set ' +
			'UserId = m.actualId ' +
		'From ' + @BatchMembershipTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
		)

print @BatchIdMapTableName 


	Exec(
		'Update ' + @BatchProfileTable + ' Set ' +
			'UserId = m.actualId ' +
		'From ' + @BatchProfileTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
		)

print @BatchProfileTable

	Exec(
		'Update ' + @BatchSiteUserTable + ' Set ' +
			'UserId = m.actualId ' +
		'From ' + @BatchSiteUserTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
		)

print @BatchSiteUserTable

Exec(
		'Update ' + @BatchUSMarketierUserTable + ' Set ' +
			'UserId = m.actualId, ' +
			'AddressId = m.actualAddressId ' +
		'From ' + @BatchUSMarketierUserTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
		)

print @BatchUSMarketierUserTable


Exec(
		'Update ' + @BatchUserDistributionListTable + ' Set ' +
			'UserId = m.actualId ' +
		'From ' + @BatchUserDistributionListTable + ' z ' +
			'Join ' + @BatchIdMapTableName + ' m on m.importId = z.UserId'
		)

print @BatchUserDistributionListTable

	-- Insert Index term
	declare @idxTerms int
	create table #tmpIdxTerms (recCount int)

	Exec('insert into #tmpIdxTerms (recCount) select count(*) from ' + @BatchIndexTermTable)

	select @idxTerms = recCount from #tmpIdxTerms
	if @idxTerms > 0
	Begin
		exec Users_ImportIndexTerms @BatchIndexTermTable,@BatchSiteUserTable
	End
	drop table #tmpIdxTerms

	-- To get existing records count
	 DECLARE @ParmDefinition nvarchar(100);
	 declare @ExistingContactsStr nvarchar(30)
	 declare @query nvarchar(500)
	 set @query= 'select @result =count(U.Id) from USUser U inner join ' + @BatchUserTable + ' z on z.id = u.id'
	 SET @ParmDefinition = N'@result varchar(30) OUTPUT';	 
	 exec sp_executesql  @query,@ParmDefinition, @result=@ExistingContactsStr OUTPUT
	 set @ExistingContacts= CAST(@ExistingContactsStr as int)

-- remove system users
exec ('Delete ' + @BatchUserTable + ' Where Id in(select userid from ussiteuser where isSystemUser = 1)')
exec ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
exec ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
exec ('Delete ' + @BatchSiteUserTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
exec ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
exec ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')
exec ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from ussiteuser where isSystemUser = 1)')

-- remove commerce users

select * from uscommerceuserprofile
if(@OverrideWSUSersAndCustomers <> 1)
begin
exec ('Delete ' + @BatchUserTable + ' Where Id in(select userid from vw_contacts where contactType in(1,2))')
exec ('Delete ' + @BatchMembershipTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
exec ('Delete ' + @BatchProfileTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
exec ('Delete ' + @BatchSiteUserTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
--exec ('Delete ' + @BatchUserDistributionListTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
exec ('Delete ' + @BatchUSMarketierUserTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
exec ('Delete ' + @BatchAddressTable + ' Where UserId in(select userid from vw_contacts where contactType in(1,2))')
end
	

	-- Step 2: Update records for existing users
	if (@OverwriteExistingContacts = 1 or @OverrideWSUSersAndCustomers =1)
	Begin
		-- Step 2a: User Record
		Exec(
			'Update USUser Set ' +
				'FirstName = z.FirstName,MiddleName=z.Middlename,LastName=Z.LastName,'+
				'CompanyName=z.CompanyName,BirthDate=z.BirthDate,Gender=z.Gender,' +
				'HomePhone=z.HomePhone,MobilePhone=z.MobilePhone,OtherPhone=z.OtherPhone, '+
				'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
				'ModifiedDate = GetDate(), Status=dbo.GetActiveStatus() ' + 
			'From USUser u ' +
				'join ' + @BatchUserTable + ' z on z.id = u.id'
			)
			
			set @UpdatedContacts = (SELECT @@ROWCOUNT)
		SET @ImportedContacts = (@ImportedContacts + @UpdatedContacts)
		--SET @ImportedContacts = (SELECT @@ROWCOUNT)
		-- Step 2b: Profile Record
		-- update profile properties that exist
		Exec(
			'Update USMemberProfile Set ' +
				'PropertyValueString = z.PropertyValueString, ' +
				'PropertyValuesBinary = z.PropertyValuesBinary, ' +
				'LastUpdatedDate = z.LastUpdatedDate ' +
			'From USMemberProfile u ' +
				'Join ' + @BatchProfileTable + ' z on z.userid = u.userid and z.PropertyName = u.PropertyName'
			)

		Exec(
			'Update GLAddress Set ' +
				'AddressType = z.AddressType, ' + 
				'AddressLine1 = z.AddressLine1, ' +
				'AddressLine2 = z.AddressLine2, ' +
				'City = z.City, ' +
				'StateId = s.Id, ' +
				'Zip = z.Zip, ' +
				'CountryId = c.Id, ' +
				'ModifiedDate = z.ModifiedDate, ' +
				'ModifiedBy = ''' + @StrModifiedBy + ''', ' +
				'Status = z.Status ' +
			'From GLAddress u ' +
				'join ' + @BatchAddressTable + ' z on z.Id = u.Id ' +
				'join GLState s on s.StateCode = z.State ' +
				'join GLCountry c on c.CountryName = z.Country '
				
			)

		Exec('Update USMarketierUserProfile Set ' +
			'AddressId=z.AddressId '+			
			'From USMarketierUserProfile u ' +
				'join ' + @BatchUSMarketierUserTable + ' z on z.Userid = u.Userid')
				
	End

	-- Step 3: Insert Records for users that do not exist
	-- Step 3a: User record
	Exec(
		'insert into USUser (Id, Title, Description, CreatedBy, CreatedDate, ModifiedBy, ' +
				'ModifiedDate, Status, UserName, LoweredUserName, MobileAlias, IsAnonymous, ' +
				'LastActivityDate,EmailNotification,ExpiryDate,TimeZone,FirstName,MiddleName,LastName,ReportRangeSelection,'+
                'ReportStartDate,ReportEndDate,BirthDate,CompanyName,Gender,HomePhone,MobilePhone,OtherPhone) ' +
		'select z.Id, z.Title, z.Description, z.CreatedBy, z.CreatedDate, z.ModifiedBy, ' +
				'z.ModifiedDate, z.Status, z.UserName, z.LoweredUserName, z.MobileAlias, ' +
				'z.IsAnonymous, z.LastActivityDate,z.EmailNotification,ISNULL(z.ExpiryDate,''9999-12-31 23:59:59.000''),z.TimeZone,z.FirstName,z.MiddleName,z.LastName,z.ReportRangeSelection,'+
                'z.ReportStartDate,z.ReportEndDate,z.BirthDate,z.CompanyName,z.Gender,z.HomePhone,z.MobilePhone,z.OtherPhone ' +
		'from ' + @BatchUserTable + ' z ' +
			'Left Outer Join USUser u on u.Id = z.Id ' +
		'Where u.Id is null'
		)
	SET @ImportedContacts = (@ImportedContacts + (SELECT @@ROWCOUNT))
	
	Exec('insert into GLAddress(Id, AddressType, AddressLine1, AddressLine2, City, StateId, Zip, CountryId, CreatedDate, CreatedBy, ModifiedDate, ModifiedBy, Status) ' +
		'select z.Id, z.AddressType, z.AddressLine1, z.AddressLine2, z.City, s.Id, z.Zip, c.Id, z.CreatedDate, z.CreatedBy, null, null, z.Status ' +
		'from ' + @BatchAddressTable + ' z ' +
			'left outer join GLCountry c on c.CountryName = z.Country ' +
			'left outer join GLState s on s.StateCode = z.State and s.CountryId = c.Id ' +
			'left outer join GLAddress a on a.Id = z.Id ' +
		'Where a.Id is null '
		)

	Exec ('insert into USMarketierUserProfile([UserId] ,[AddressId],	[Status], [Notes], [ContactSourceId])'+	
	' select z.UserId ,	z.AddressId,	1,	z.Notes, z.ContactSourceId ' + 
		'from  ' + @BatchUSMarketierUserTable + '  z  '+
			'Left Outer Join USMarketierUserProfile u on u.userId = z.userId  Where u.userId is null')

	-- Step 3b: Membership Record
	Exec(
		'insert into USMembership (UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, ' +
				'Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved,  ' +
				'IsLockedOut, LastLoginDate, LastPasswordChangedDate, LastLockoutDate,  ' +
				'FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart,  ' +
				'FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart) ' +
		'select z.UserId, z.Password, z.PasswordFormat, z.PasswordSalt, z.MobilePIN, ' +
				'z.Email, z.LoweredEmail, z.PasswordQuestion, z.PasswordAnswer, z.IsApproved,  ' +
				'z.IsLockedOut, z.LastLoginDate, z.LastPasswordChangedDate, z.LastLockoutDate,  ' +
				'z.FailedPasswordAttemptCount, z.FailedPasswordAttemptWindowStart,  ' +
				'z.FailedPasswordAnswerAttemptCount, z.FailedPasswordAnswerAttemptWindowStart ' +
		'from ' + @BatchMembershipTable + ' z ' +
			'left outer join USMembership u on u.UserId = z.UserId ' +
		'where u.UserId is null '
		)

	-- Step 3c: Profile Record
	Exec(
		'insert into USMemberProfile (UserId, PropertyName, PropertyValueString, ' +
			'PropertyValuesBinary, LastUpdatedDate) ' +
		'select z.UserId, z.PropertyName, z.PropertyValueString, z.PropertyValuesBinary, ' +
			'z.LastUpdatedDate ' +
		'from ' + @BatchProfileTable + ' z ' +
			'left outer join USMemberProfile u on u.UserId = z.UserId and u.PropertyName = z.PropertyName ' +
		'where u.PropertyName is null '
		)




	-- Step 3d: Site User Record
	Exec(
		'insert into USSiteUser (SiteId, UserId, IsSystemUser, ProductId) ' +
		'select z.SiteId, z.UserId, z.IsSystemUser, z.ProductId ' +
		'from ' + @BatchSiteUserTable + ' z ' +
			'left outer join USSiteUser u on u.UserId = z.UserId ' +
		'where u.UserId is null '
	)

	-- Step 3e: Distribution List Record

print 		'Update ' + @BatchUserDistributionListTable + ' Set ' +
			'  ListId = null ' +
		'From ' + @BatchUserDistributionListTable + ' z ' +
			'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'


		Exec(
		'Update ' + @BatchUserDistributionListTable + ' Set ' +
			' ListId = null ' +
		' from  ' + @BatchUserDistributionListTable + ' z ' +
			'Join  TADistributionListUser  m on m.UserId = z.UserId and m.DistributionListId=z.ListId'
		)
		SET @AddedToDistributionList = (SELECT @@ROWCOUNT)

print 'insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '+
	' select newid(),z.ListId,z.UserId ' + 
		'from ' + @BatchUserDistributionListTable + ' z ' +
			'Left Outer Join TADistributionListUser u on u.UserId = z.UserId ' +
		'Where u.UserId is null and z.ListId is not null '


	Exec ('insert into dbo.TADistributionListUser(Id,DistributionListId,UserId) '+
	' select newid(),z.ListId,z.UserId ' + 
		'from ' + @BatchUserDistributionListTable + ' z ' +
			'Join USUser u on u.id = z.UserId ' +
		'Where z.ListId is not null ')
		SET @AddedToDistributionList = (@AddedToDistributionList + (SELECT @@ROWCOUNT))
	

	-- peform cleanup
	Exec('Drop Table ' + @BatchIdMapTableName)
--
--	-- empty batch tables

	--Exec('truncate table ' + @BatchUserTable)
	--Exec('truncate table ' + @BatchMembershipTable)
	--Exec('truncate table ' + @BatchProfileTable)
	--Exec('truncate table ' + @BatchSiteUserTable)
	--exec('truncate table ' + @BatchAddressTable)
	--exec('truncate table ' + @BatchUserDistributionListTable)
	--exec('truncate table ' + @BatchUSMarketierUserTable)
	--exec('truncate table ' + @BatchIndexTermTable)
	--exec('truncate table ' + @BatchAddressTable)

	UPDATE UploadContactData SET ImportedContacts = @ImportedContacts, AddedToDistributionList = @AddedToDistributionList, ExistingRecords=@ExistingContacts , UpdatedContacts=@UpdatedContacts where UploadHistoryId = @BatchId
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Content_Save]'
GO
/*****************************************************
* Name : Content_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Content_Save] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier output,
	@ApplicationId uniqueidentifier,
	@Title nvarchar(256),
	@Description nvarchar(256)= null,
	@Text ntext = null,
	@URL nvarchar(4000) = null,
	@URLType nvarchar(256) = null,
	@ObjectTypeId int,
	@CreatedBy uniqueidentifier,
	@ModifiedBy uniqueidentifier = null,	
	@PublishDate Datetime =null,
	@XMLString xml = null,
	@BinaryObject image = null,
	@Keywords nvarchar(4000) = null,
	@ParentId  uniqueidentifier = null,
	@TemplateId uniqueidentifier=null,
	@SourceContentId uniqueidentifier=null,
	@Status int = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
	@NewId		uniqueidentifier,
	@RowCount		INT,
	@Stmt 		VARCHAR(36),
	@Now 			datetime,
	@Error 		int,
	@OldParentId	uniqueidentifier
	
	 
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM   COContent WHERE  Id = @Id))
		BEGIN			
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END	
		
	IF (@ParentId IS NULL OR @ParentId = dbo.GetEmptyGUID())
		SET @ParentId = [dbo].[GetVariantDirectoryId](@SourceContentId, @ObjectTypeId, @ApplicationId)
	
	IF((@ParentId IS NULL OR @ParentId = dbo.GetEmptyGUID())AND @Id IS NOT NULL)
		SELECT @ParentId = ParentId, @OldParentId = ParentId FROM COContent where Id = @Id
	ELSE IF (@Id IS NOT NULL)
		SELECT @OldParentId = ParentId FROM COContent where Id = @Id
	
	IF @TemplateId is null and @XmlString is not null
	 Set @TemplateId = @XMLString.value('(contentDefinition/@TemplateId)[1]','uniqueidentifier')
	
	IF @SourceContentId = dbo.GetEmptyGUID()
		SET @SourceContentId = null
	 
	SET @Now = getutcdate()	
		
	IF (@Id IS NULL)			-- new insert
	BEGIN
		SET @Stmt = 'Create Content'
		Select @NewId =newid()
						
		set @Status = 1;-- dbo.GetStatus(@ObjectTypeId,'Create')
		
		DECLARE @OrderNo INT
		SELECT @OrderNo = Isnull(Count(Title),0) + 1 FROM COContent Where ParentId = @ParentId  and ApplicationId=@ApplicationId
		  	
		INSERT INTO COContent  WITH (ROWLOCK)(
			Id,
			ApplicationId,
			Title,
			Description,
			Text,
			URL,
			URLType,
			ObjectTypeId,
			CreatedDate,
			CreatedBy,			
			Status,
			PublishDate,
			XMLString,
			BinaryObject,
			Keywords,
			OrderNo,
			TemplateId,
			SourceContentId,
			ParentId,
			ModifiedBy,
			ModifiedDate
		)
		values
			(
				@NewId,
				@ApplicationId,
				@Title,
				@Description,
				@Text,
				@URL,
				@URLType,
				@ObjectTypeId,
				@Now,
				@CreatedBy,				
				@Status,
				@PublishDate,
				@XMLString,
				@BinaryObject,
				@Keywords,
				@OrderNo,
				@TemplateId,
				@SourceContentId,
				@ParentId,
				@CreatedBy,
				@Now			
			)
		SELECT @Error = @@ERROR
	 	IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		SELECT @Id = @NewId
	END
	ELSE			-- Update
	BEGIN
		SET @Stmt = 'Modify Content'
		IF (@OrderNo = 0)
		BEGIN
			SET @OrderNo = null
		END
		
		Update COContent WITH (ROWLOCK)
		set 
		   	Title = @Title,
			Description = @Description,
			Text = @Text,
			URL = @URL,
			URLType = @URLType,
			ObjectTypeId = @ObjectTypeId,
			ModifiedDate = @Now,
			ModifiedBy = @ModifiedBy,			
			PublishDate= @PublishDate,
			XMLString = @XMLString,
			BinaryObject = @BinaryObject,
			Keywords = @Keywords,
			TemplateId =@TemplateId,
			SourceContentId=@SourceContentId,
			ParentId=@ParentId,
			Status = ISNULL(@Status,Status)
		where Id = @Id

		SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @RowCount = 0
		BEGIN
			--concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()
		END	
	END		
	
	EXEC [dbo].[Content_AdjustDisplayOrder] @ParentId, @ApplicationId
	IF (@OldParentId Is not null AND @OldParentId <> @ParentId) --- while moving a content/file/image to another fodler.
		EXEC [dbo].[Content_AdjustDisplayOrder] @OldParentId, @ApplicationId
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Post_GetPost]'
GO
/*****************************************************
* Name : Post_GetPost
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Post_GetPost]
(
	@ApplicationId		uniqueIdentifier,		
	@Id					uniqueIdentifier = null,		
	@PostMonth			varchar(20) = null,
	@PostYear			varchar(20) = null,
	@PageSize			int = null,
	@PageNumber			int = null,
	@FolderId			uniqueIdentifier = null,
	@BlogId				uniqueIdentifier = null,
	@Status				int = null,
	@CategoryId			uniqueIdentifier = null,
	@Label				nvarchar(1024) = null,
	@Author				nvarchar(1024) = null,
	@IncludeVersion		bit = null,
	@VersionId			uniqueidentifier=null,
	@IgnoreSite			bit = null
)
AS
BEGIN
	DECLARE @IsSticky bit
	IF @Status = 5
		SET @IsSticky = 1

	DECLARE @tbPostIds TABLE(RowNo int, TotalRows int, Id uniqueidentifier, BlogId uniqueidentifier, PostContentId uniqueidentifier)
	;WITH PostIds AS(
		SELECT ROW_NUMBER() over (order by IsSticky desc, PostDate desc, Title) AS RowNo, 
			COUNT(Id) over (PARTITION BY NULL) AS TotalRows,Id, BlogId, PostContentId
		FROM BLPost 
		WHERE (@Id IS NULL OR Id = @Id) AND
			(Status != dbo.GetDeleteStatus()) AND
			(@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND
			(@Status IS NULL OR Status = @Status) AND
			(@PostYear IS NULL OR YEAR(PostDate) = @PostYear) AND 
			(@PostMonth IS NULL OR MONTH(PostDate) = @PostMonth) AND 
			(@FolderId IS NULL OR HSId = @FolderId) AND 
			(@BlogId IS NULL OR BlogId = @BlogId) AND
			(@IsSticky IS NULL OR IsSticky = @IsSticky) AND
			(@Label IS NULL OR Labels LIKE '%' + @Label + '%') AND
			(@Author IS NULL OR AuthorEmail like @Author OR AuthorName like @Author) AND
			(@CategoryId IS NULL OR Id IN (SELECT ObjectId FROM COTaxonomyObject WHERE TaxonomyId = @CategoryId AND ObjectTypeId = 40))
	)
	
	INSERT INTO @tbPostIds
	SELECT RowNo, TotalRows, Id, BlogId, PostContentId FROM PostIds
	WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR 
		RowNo BETWEEN ((@PageNumber - 1) * @PageSize + 1) AND (@PageNumber * @PageSize))
	
	SELECT A.Id,			
		A.ApplicationId,
		A.Title,
		A.Description,
		A.ShortDescription,
		A.AuthorName,
		A.AuthorEmail,
		A.Labels,
		A.AllowComments,
		A.PostDate,
		A.Location,
		A.Status,
		A.IsSticky,
		A.CreatedBy,
		A.CreatedDate,
		A.ModifiedBy,
		A.ModifiedDate,
		A.FriendlyName,
		A.PostContentId,
		A.ContentId,
		A.ImageUrl,
		A.TitleTag,
		A.H1Tag,
		A.KeywordMetaData,
		A.DescriptiveMetaData,
		A.OtherMetaData,
		B.BlogListPageId,
		B.Id AS BlogId,
		A.WorkflowState,
		A.PublishDate,
		A.PublishCount,
		A.INWFFriendlyName
	FROM BLPost A 
		JOIN @tbPostIds T ON A.Id = T.Id
		JOIN BLBlog B ON A.BlogId = B.Id
	ORDER BY T.RowNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.Text, 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		sDir.VirtualPath As FolderPath
	FROM COContent C
		INNER JOIN @tbPostIds T ON C.Id = T.PostContentId
		INNER JOIN COContentStructure sDir on SDir.Id = C.ParentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		
	SELECT Top 1 TotalRows FROM @tbPostIds

	SELECT A.Id,
		ApplicationId,
		Title,
		Description,
		BlogNodeType,
		MenuId,
		ContentDefinitionId,
		ShowPostDate,
		DisplayAuthorName,
		EmailBlogOwner,
		ApproveComments,
		RequiresUserInfo,
		Status,
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		ModifiedDate,
		BlogListPageId,
		BlogDetailPageId,
		ContentDefinitionId
	FROM BLBlog A 
		INNER JOIN @tbPostIds T ON T.BlogId = A.Id
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
	
	SELECT I.Id,
		I.Title,
		C.TaxonomyId
	FROM COTaxonomyObject C
		JOIN @tbPostIds T ON T.Id = C.ObjectId
		JOIN COTaxonomy I ON I.Id = C.TaxonomyId

	SELECT Count(*) AS NoOfComments,
		T.Id
	FROM BLComments C
		JOIN @tbPostIds T ON C.ObjectId = T.Id
	GROUP BY T.Id

	IF @IncludeVersion = 1
	BEGIN
		DECLARE @tbVersionIds TABLE(Id uniqueidentifier)
		IF @VersionId IS NULL
		BEGIN
			;WITH CTEVersion AS (
				SELECT ROW_NUMBER() OVER (PARTITION BY T.Id ORDER BY RevisionNumber DESC, VersionNumber DESC) AS VerRank,
					V.Id FROM VEVersion V
					JOIN BLPost T On T.Id = V.ObjectId
			)
			INSERT INTO @tbVersionIds
			SELECT Id FROM CTEVersion WHERE VerRank = 1
		END
		ELSE
		BEGIN
			INSERT INTO @tbVersionIds VALUES (@VersionId)
		END

		SELECT V.Id AS VersionId,
			V.ObjectId AS PostId,
			V.XMLString
		FROM VEVersion V
			JOIN @tbVersionIds T On T.Id = V.Id

		SELECT 
			V.PostVersionId AS VersionId,
			C.ContentId,
			C.XMLString
		FROM VEPostContentVersion V
			JOIN @tbVersionIds T On T.Id = V.PostVersionId
			JOIN VEContentVersion C ON C.Id = V.ContentVersionId
	END
END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ProfileManager_GetProfiles]'
GO
/*****************************************************
* Name : ProfileManager_GetProfiles
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[ProfileManager_GetProfiles]
-- *************************************************************************
-- PARAMETERS
	@ApplicationId				uniqueidentifier,
    @AuthenticationOption		int,
    @PageIndex					int,
    @PageSize					int,
    @UserNameToMatch			nvarchar(256) = NULL,
    @InactiveSinceDate			datetime      = NULL

--**************************************************************************
AS
BEGIN
    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
	IF (@PageIndex > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	ELSE
		SET @PageUpperBound = 0;

	WITH ResultEntries AS
	(
		SELECT  ROW_NUMBER() OVER (ORDER BY u.UserName) AS RowNumber,
				u.UserName,
				u.IsAnonymous,
				u.LastActivityDate,
				p.LastUpdatedDate,
				DATALENGTH(p.PropertyName) + DATALENGTH(p.PropertyValueString) + DATALENGTH(p.PropertyValuesBinary) [Size]
        FROM    dbo.USUser u, dbo.USMemberProfile p, dbo.USSiteUser s
        WHERE   --s.SiteId = @ApplicationId AND
				s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
				u.Id = p.UserId AND
				u.Id = s.UserId		AND
				(@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@AuthenticationOption = 2)
                   OR (@AuthenticationOption = 0 AND IsAnonymous = 1)
                   OR (@AuthenticationOption = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
			AND u.Status <> dbo.GetDeleteStatus()
	)

	SELECT  DISTINCT UserName,
			IsAnonymous,
			LastActivityDate,
			LastUpdatedDate,
			[Size]
    FROM    ResultEntries
    WHERE   (@PageIndex = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [dbo].[PageDefinition_GetPageSegments]'
GO
/*****************************************************
* Name : PageDefinition_GetPageSegments
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[PageDefinition_GetPageSegments]
--********************************************************************************
-- Input Output Parameters
--********************************************************************************
	@ApplicationId uniqueidentifier = null,
	@PageId uniqueidentifier
AS
 
BEGIN
	DECLARE @pageDefSegmentIds TABLE(Id uniqueidentifier)

	DECLARE @DeskTopDeviceId uniqueidentifier
	DECLARE @DeskTopAudienceId uniqueidentifier
	SELECT @DeskTopDeviceId = dbo.GetDesktopDeviceId(@ApplicationId)
	SELECT @DeskTopAudienceId = dbo.GetDesktopAudienceSegmentId(@ApplicationId)

	--Setting the latest page definition segment version for all the device
	;With PageDefSegmentCTE AS
	(	
				SELECT [Id],DeviceID, AudienceSegmentId
					  ,ROW_NUMBER() over (Partition BY DeviceId,AudienceSegmentId Order by PageDefinitionId Desc) as pdfRank
				  FROM [PageDefinitionSegment]
				  WHERE PageDefinitionId = @PageId
	)

	INSERT INTO @pageDefSegmentIds 
		SELECT Id FROM PageDefSegmentCTE P1 WHERE pdfRank = 1
		AND Id NOT IN (SELECT Id FROM PageDefSegmentCTE P2 WHERE 
			P2.DeviceId = @DeskTopDeviceId
			AND P2.AudienceSegmentId = @DeskTopAudienceId)
			

	SELECT	   [Id]
			  ,NULL AS VersionId
			  ,[PageDefinitionId]
			  ,[DeviceId]
			  ,[AudienceSegmentId]
			  ,[UnmanagedXml]
			  ,NULL AS ContainerContentXML
	FROM [PageDefinitionSegment] 
	WHERE Id IN (SELECT Id from @pageDefSegmentIds)
 		  
	  -- Page Contents 
	SELECT 
		  pdfc.[PageDefinitionId]
		  ,pdfc.[ContainerId]
		  ,pdfc.[ContentId]
		  ,pdfc.[InWFContentId]
		  ,pdfc.[ContentTypeId]
		  ,pdfc.[IsModified]
		  ,pdfc.[IsTracked]
		  ,pdfc.[Visible]
		  ,pdfc.[InWFVisible]
		  ,pdfc.[DisplayOrder]
		  ,pdfc.[InWFDisplayOrder]
		  ,pdfc.[CustomAttributes]
		  ,pdfc.[PageDefinitionSegmentId]
		  ,pdfc.ContainerName
		FROM	
			PageDefinitionContainer pdfc
		WHERE	
			pdfc.PageDefinitionSegmentId IN (SELECT Id from @pageDefSegmentIds)

	


END



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[CampaignMailing_GetWorkDataBatch]'
GO
/*****************************************************
* Name : CampaignMailing_GetWorkDataBatch
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[CampaignMailing_GetWorkDataBatch]
	@CampaignRunId uniqueidentifier,
	@BatchNo int,
	@BatchSize int
As
Begin
	Declare @tblContacts Table
	(
		Row int,
		CampaignRunId uniqueidentifier,
		UserId uniqueidentifier,
		FirstName varchar(1024),
		MiddleName varchar(1024),
		LastName varchar(1024),
		CompanyName varchar(1024),
		BirthDate datetime,
		Gender varchar(1024),
		AddressId uniqueidentifier,
		Status int,
		HomePhone varchar(1024),
		MobilePhone varchar(1024),
		OtherPhone varchar(1024),
		Email varchar(256),
		Notes varchar(max),
		ContactType int
	)

	insert @tblContacts (Row, CampaignRunId, UserId, FirstName, MiddleName, LastName,
		CompanyName, BirthDate, Gender, AddressId, Status, HomePhone, MobilePhone,
		OtherPhone, Email, Notes, ContactType)
	Select c.Row, c.CampaignRunId, c.UserId, c.FirstName, c.MiddleName, c.LastName,
		c.CompanyName, c.BirthDate, c.Gender, c.AddressId, c.Status, c.HomePhone, c.MobilePhone,
		c.OtherPhone, c.Email, c.Notes, c.ContactType
	From (
		select ROW_NUMBER() OVER (ORDER BY CampaignRunId DESC) as Row, * 
		from MKCampaignRunWorkTable
		where CampaignRunId = @CampaignRunId
		) as c
	Where CampaignRunId = @CampaignRunId
		And Row Between (@BatchNo - 1) * @BatchSize + 1 and @BatchNo*@BatchSize

	-- Contacts
	Select 
		Row,
		CampaignRunId,
		UserId ,
		FirstName ,
		MiddleName ,
		LastName,
		CompanyName ,
		BirthDate ,
		Gender ,
		AddressId ,
		Status ,
		HomePhone ,
		MobilePhone ,
		OtherPhone ,
		Email ,
		Notes ,
		ContactType 
	From @tblContacts

	-- Address
	select	a.Id
			,a.FirstName
			,a.LastName
			,a.AddressType
			,a.AddressLine1
			,a.AddressLine2
			,a.AddressLine3
			,a.City
			,a.StateId
			,a.Zip			
			,a.CountryId
			,a.Phone
			,a.CreatedDate
			,a.CreatedBy
			,a.ModifiedDate
			,a.ModifiedBy
			,a.Status
			,a.County
	from GLAddress a
		Join @tblContacts c on c.AddressId = a.Id

	-- Profile
	select * 
	from UsMemberProfile p
		join @tblContacts c on c.UserId = p.UserId
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_Save]'
GO
/*****************************************************
* Name : Workflow_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: This procedure is used to create workflow and its sequence. In case of modification, 
-- if the workflow is executed already for any other object then modification is not allowed. 
-- If the workflow already existing then delete its sequence and next actor details then call the procedure  
-- Workflow_SaveWorkflow to create/modify the workflow, and then using this workflow id and the XML parameter, 
-- it inserts the sequence of the workflow into WFSequence table. Using the roleid, objectypeid and actorid 
-- from XML parameter, it find out the actions belongs to this role id using the function User_GetActions().
-- Author: Martin M V
-- Created Date: 23-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date: 01-June-2007
-- Modified By: Martin M V
-- Reason: NetEditor define permission for menu. There is no way to define permission for page. 
--         In NetEditor level we define workflow for object type as page instead of menu. 
---        So we won't get any permissions for page object type. 
---        Without permission in particular object type we can not create workflow for that object type.
---        So in stored procedure level we put a condition to take the permissions for menus instead of pages. 
---		   There is no change for other object types. 
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].[Workflow_Save] 
--          @XML = 'xml',
--          @Id = 'uniqueidentifier' or null,
--          @Title = 'value',
--          @Description = 'value',
--          @CreatedBy = 'uniqueidentifier',
--          @ModifiedBy = 'uniqueidentifier',
--          @Status = 1 (Active) or 0 (InActive),
--			@SkipAfterDays = 0,
--          @ObjectTypeId = 1
--          @RootId = null,
--			@ApplicationId	uniqueidentifier
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_Save](
--********************************************************************************
-- Parameters
--********************************************************************************
            @XML 			xml,
            @Id 			uniqueidentifier = null OUTPUT ,
            @Title			nvarchar(256),
            @Description	nvarchar(1024), 
            @CreatedBy	 	uniqueidentifier,
            @ModifiedBy		uniqueidentifier = null,
            @Status			int,
            @SkipAfterDays	int,            
            @ObjectTypeId	int,
            @RootId			uniqueidentifier = null OUTPUT,
            @ApplicationId	uniqueidentifier,
            @ProductId	uniqueidentifier,
            @IsGlobal	bit = null,
			@IsShared   bit = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
            @Count 				bigint,
            @NewSequenceGuid	uniqueidentifier,
            @RowCount       	int,

            @Action 			Cursor,

            @XmlRoleId 			int , 
            @XmlActorId 		uniqueidentifier,
            @XmlSequenceOrderNo smallint,
            @XmlActorType 		smallint
            
DECLARE		@TempObjectTypeId	int            
            
BEGIN
            /* Set Default values to Variable*/	
            SET @Count		= 0
            SET @RowCount	= 0	

            /* if Id is specified, ensure exists */
            IF (@Id IS NOT NULL)
            BEGIN	
                        IF ((SELECT COUNT([Title]) 
                             FROM   WFWorkflow 
                             WHERE  [Id] = @Id) = 0)
                        BEGIN
                                    DEALLOCATE @Action
                                    RAISERROR('NOTEXIST||%s', 16, 1, 'Id')
                                    RETURN dbo.GetBusinessRuleErrorCode()
                        END         

                        /*IF ((SELECT COUNT([ObjectTypeId]) 
                             FROM   WFWorkflowExecution 
                             WHERE  [WorkflowId] = @Id) > 0)
                        BEGIN
                                    DEALLOCATE @Action
                                    RAISERROR('RELATIONEXIST||%s', 16, 1, 'workflow already executed')
                                    RETURN dbo.GetBusinessRuleErrorCode()
                        END*/ 
	
						/* DELETE From Sequence Action */
						DELETE FROM dbo.WFSequenceActions WHERE [SequenceId] 
							IN (SELECT [Id] FROM dbo.WFSequence WHERE [WorkflowId] = @Id)
									IF @@ERROR <> 0
									BEGIN
											DEALLOCATE @Action
											RETURN dbo.GetDataBaseErrorCode()           
									END	

						/* Delete existing Sequence */
						DELETE FROM dbo.WFSequence WHERE [WorkflowId] = @Id
									IF @@ERROR <> 0
									BEGIN
											DEALLOCATE @Action
											RETURN dbo.GetDataBaseErrorCode()           
									END	
			END

            /* Call Workflow_SaveWorkflow to Save Workflow*/
            EXECUTE [dbo].Workflow_SaveWorkflow  
						@Id OUTPUT, 
						@Title,  
						@Description, 
						@CreatedBy, 
						@ModifiedBy,
						@Status,
						@SkipAfterDays,
						@ObjectTypeId, 
						@RootId OUTPUT,
						@ApplicationId,
						@ProductId,
						@IsGlobal,
						@IsShared

            IF @@ERROR = 0 
            BEGIN
				/* open the XML to get the list of actors and their roles */
				SET @Action = 
				CURSOR FAST_FORWARD 
				FOR 
				SELECT 
					tab.col.value('RoleId[1]/text()[1]','int') As RoleId,
					tab.col.value('ActorId[1]/text()[1]','uniqueidentifier') As ActorId,
					tab.col.value('ActorType[1]/text()[1]','int') As ActorType,
					tab.col.value('Order[1]/text()[1]','int') As SequenceOrder
				FROM @XML.nodes('//WFSequence') tab(col)					

				OPEN @Action
				FETCH NEXT FROM @Action INTO @XmlRoleId, @XmlActorId, @XmlActorType, @XmlSequenceOrderNo

				WHILE @@FETCH_STATUS = 0 
				BEGIN
					SET @NewSequenceGuid = newId()
					/* Insert into WFSequence*/
					INSERT INTO dbo.WFSequence 
							(
								[Id],
								WorkflowId,
								RoleId,
								ActorId,
								ActorType,
								[Order]
							)
					VALUES
							(
								@NewSequenceGuid,
								@Id, 
								@XmlRoleId,
								@XmlActorId, 
								@XmlActorType, 
								@XmlSequenceOrderNo								
							)
					SELECT @RowCount = @@ROWCOUNT
					IF @@ERROR <> 0
					BEGIN
								CLOSE @Action
								DEALLOCATE @Action
								RETURN dbo.GetDataBaseErrorCode()           
					END	
					IF @RowCount = 0
					BEGIN
								/* concurrency error */
								CLOSE @Action
								DEALLOCATE @Action
								RAISERROR('DBCONCURRENCY',16,1)
								RETURN dbo.GetDataConcurrencyErrorCode()                                    
					END   
					/* Check actions are there under this role and object type */
					IF @ObjectTypeId = 8 --- if workflow is define for page then taking actions for menu
						SET @TempObjectTypeId = 2
					ELSE
						SET @TempObjectTypeId = @ObjectTypeId
					
					SELECT	@Count = Count(ActionId) 
					FROM	dbo.User_GetActions(@XmlRoleId, @XmlActorId, @XmlActorType, @TempObjectTypeId, @ApplicationId) 
					WHERE	ActionId is not null
					IF @Count  > 0 
					BEGIN
						/* Insert actions in this role for an object type into sequence action table */
						INSERT INTO dbo.WFSequenceActions
								(
									SequenceId,
									ActionId
								)
								(
									SELECT
											@NewSequenceGuid,
											ActionId
									FROM	dbo.User_GetActions(@XmlRoleId, @XmlActorId, @XmlActorType, @TempObjectTypeId, @ApplicationId) A
									WHERE	ActionId is not null
											
								)	
						SELECT @RowCount = @@ROWCOUNT								
						IF @@ERROR <> 0
						BEGIN
									CLOSE @Action
									DEALLOCATE @Action
									RETURN dbo.GetDataBaseErrorCode()           
						END														
					END
					--ELSE 
					--BEGIN
							--CLOSE @Action
							--DEALLOCATE @Action
							--RAISERROR('NOTEXISTS||%s', 16, 1, 'Permissions')
							--RETURN dbo.GetDataBaseErrorCode()  						
					--END
												
					/* Fetching next row */
					FETCH NEXT FROM @Action INTO @XmlRoleId, @XmlActorId, @XmlActorType, @XmlSequenceOrderNo
				END

				CLOSE @Action

				/* Set first sequence order as the root of this workflow */
				SELECT	 Top 1 
						@RootId = [Id] 
				FROM	dbo.WFSequence
				WHERE	WorkflowId	=	@Id	AND
						[Order]		=	1

				/* Update Root Id of Workflow */
				UPDATE dbo.WFWorkflow 
				SET 	RootId 	= 	@RootId
				WHERE 	[Id]	=	@Id
				
				/* Update Executed Records in Workflow Execution with First Sequence only for the object whic are middle of the execution */
				UPDATE dbo.WFWorkflowExecution
				SET		SequenceId	=	@RootId
				WHERE	WorkflowId	=	@Id And Completed = 0
				
            END

            DEALLOCATE @Action
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Campaign_GetUserActionResultsForCampaign]'
GO
/*****************************************************
* Name : Campaign_GetUserActionResultsForCampaign
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Campaign_GetUserActionResultsForCampaign] (
	@CampaignId uniqueidentifier,
	@IncludeAll bit
)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @tblAddress TABLE(Id uniqueidentifier primary key, AddressLine1 nvarchar(100), AddressLine2 nvarchar(100), AddressLine3 nvarchar(100), City nvarchar(100), 
				State nvarchar(100), StateCode nvarchar(100), CountryName nvarchar(100), CountryCode nvarchar(100), Zip nvarchar(100), County nvarchar(255))
	DECLARE @tblSendLog TABLE (UserId uniqueidentifier primary key, LoweredEmail nvarchar(100), Opens int, Clicks int, Unsubscribes int, Bounces int, TriggeredWatches int)	
			
	INSERT INTO @tblAddress
	SELECT a.Id,
		a.[AddressLine1],
		a.[AddressLine2],
		a.[AddressLine3],
		a.[City],
		s.[State],
		s.[StateCode],
		c.[CountryName],
		c.[CountryCode],
		a.[Zip],
		a.County
	FROM GLAddress AS a
	INNER JOIN GLState AS s ON a.StateId = s.Id
	INNER JOIN GLCountry AS c ON a.CountryId = c.Id
	
	--If IncludeAll is true, perform a right join on MKEmailSendActions to include users who did not perform any action
	IF @IncludeAll = 0
		BEGIN
			INSERT INTO @tblSendLog
			SELECT		
				sl.UserId AS Id,
				us.LoweredEmail AS Email,
				SUM(CASE WHEN sa.ActionType = 1 THEN 1 ELSE 0 END) AS Opens,
				SUM(CASE WHEN sa.ActionType = 2 THEN 1 ELSE 0 END) AS Clicks,
				SUM(CASE WHEN sa.ActionType = 3 THEN 1 ELSE 0 END) AS Unsubscribes,
				(SELECT COUNT(Id) AS Bounces
									FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND Bounced = 1 AND UserId = sl.UserId) AS Bounces,
				SUM(CASE WHEN sa.ActionType = 5 THEN 1 ELSE 0 END) AS TriggeredWatches						
			 FROM		MKEmailSendLog AS sl
						INNER JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
						INNER JOIN	USMembership AS us ON us.UserId = sl.UserId
			 WHERE		sl.CampaignId = @CampaignId
			 GROUP BY	us.LoweredEmail, sl.UserId
						 
			SELECT DISTINCT(c.UserId), 
				m.[Opens],
				m.[Clicks],
				m.[Unsubscribes],
				m.[Bounces],
				m.[TriggeredWatches],
				c.[FirstName], 
				c.[MiddleName],
				c.[LastName],
				c.[CompanyName],
				c.[BirthDate],
				c.[Gender],
				c.[Status],
				c.[HomePhone],
				c.[MobilePhone],
				c.[OtherPhone],
				c.[Notes],
				c.[Email],
				a.[AddressLine1],
				a.[AddressLine2],
				a.[AddressLine3],
				a.[City],
				a.[State],
				a.[StateCode],
				a.[CountryName],
				a.[CountryCode],
				a.[Zip],
				a.County
			FROM vw_contacts AS c
			INNER JOIN @tblSendLog m ON m.UserId = c.UserId
			LEFT JOIN @tblAddress a ON a.Id = c.AddressId
			
			
			
			--Second table of Profile values to prevent recurring database
			SELECT distinct(sl.UserId),um.*
			FROM USMemberProfile As um
			INNER JOIN MKEmailSendLog AS sl ON um.UserId = sl.UserId
			INNER JOIN MKEmailSendActions AS sa ON sa.SendId = sl.Id
			WHERE sl.CampaignId = @CampaignId
		END
	ELSE
		BEGIN	
			INSERT INTO @tblSendLog
			SELECT		
				sl.UserId AS Id,
				us.LoweredEmail AS Email,
				SUM(CASE WHEN sa.ActionType = 1 THEN 1 ELSE 0 END) AS Opens,
				SUM(CASE WHEN sa.ActionType = 2 THEN 1 ELSE 0 END) AS Clicks,
				SUM(CASE WHEN sa.ActionType = 3 THEN 1 ELSE 0 END) AS Unsubscribes,
				(SELECT COUNT(Id) AS Bounces
									FROM MKEmailSendLog WHERE CampaignId = @CampaignId AND Bounced = 1 AND UserId = sl.UserId) AS Bounces,
				SUM(CASE WHEN sa.ActionType = 5 THEN 1 ELSE 0 END) AS TriggeredWatches						
			 FROM		MKEmailSendLog AS sl
						LEFT JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
						INNER JOIN	USMembership AS us ON us.UserId = sl.UserId
			 WHERE		sl.CampaignId = @CampaignId
			 GROUP BY	us.LoweredEmail, sl.UserId
	 				
			SELECT DISTINCT(c.UserId), 
				m.[Opens],
				m.[Clicks],
				m.[Unsubscribes],
				m.[Bounces],
				m.[TriggeredWatches],
				c.[FirstName], 
				c.[MiddleName],
				c.[LastName],
				c.[CompanyName],
				c.[BirthDate],
				c.[Gender],
				c.[Status],
				c.[HomePhone],
				c.[MobilePhone],
				c.[OtherPhone],
				c.[Notes],
				c.[Email],
				a.[AddressLine1],
				a.[AddressLine2],
				a.[AddressLine3],
				a.[City],
				a.[State],
				a.[StateCode],
				a.[CountryName],
				a.[CountryCode],
				a.[Zip],
				a.County
			FROM vw_contacts AS c
				INNER JOIN	@tblSendLog m ON m.UserId = c.UserId
				LEFT JOIN @tblAddress a ON a.Id = c.AddressId
				
			
			
			--Second table of Profile values to prevent recurring database
			SELECT distinct(sl.UserId),um.*
			FROM USMemberProfile As um
			INNER JOIN MKEmailSendLog AS sl ON um.UserId = sl.UserId
			LEFT JOIN	MKEmailSendActions AS sa ON sa.SendId = sl.Id
			WHERE sl.CampaignId = @CampaignId
		END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderPayment_Save]'
GO
/*****************************************************
* Name : OrderPayment_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderPayment_Save]
(@Id uniqueidentifier out,
@Amount money)
AS
BEGIN

	DECLARE @OrderId uniqueidentifier
	DECLARE @OrderPaymentAmount money
	DECLARE @OrderRefundAmount money
	SELECT @OrderId = OrderId FROM PTOrderPayment WHERE Id = @Id

	Update PTOrderPayment Set Amount=@Amount
	Where Id=@Id
	
	Select @OrderPaymentAmount = [dbo].[Order_GetPaymentTotal](@OrderId)
	SELECT @OrderRefundAmount = [dbo].[Order_GetRefundTotal](@OrderId)	

	Update OROrder Set PaymentTotal = @OrderPaymentAmount, RefundTotal = @OrderRefundAmount where Id = @OrderId

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Blog_GetBlog]'
GO
/*****************************************************
* Name : Blog_GetBlog
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Blog_GetBlog]
(
	@Id					uniqueIdentifier = null,
	@ApplicationId		uniqueIdentifier = null,
	@PageDefinitionId	uniqueIdentifier = null,
	@IgnoreSite			bit = null
)
AS
BEGIN
		SELECT 
			Id,
			ApplicationId,
			Title,
			Description,
			BlogNodeType,
			MenuId,
			ContentDefinitionId,
			ShowPostDate,
			DisplayAuthorName,
			EmailBlogOwner,
			ApproveComments,
			RequiresUserInfo,
			Status,
			CreatedDate,
			CreatedBy,
			ModifiedBy,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			ModifiedDate,
			BlogListPageId,
			BlogDetailPageId,
			ContentDefinitionId
		FROM BLBlog A
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		WHERE 
			(@Id IS NULL OR Id = @Id) AND
			(@ApplicationId IS NULL OR @IgnoreSite = 1 OR @ApplicationId = ApplicationId) AND
			(@PageDefinitionId IS NULL OR BlogListPageId = @PageDefinitionId OR BlogDetailPageId = @PageDefinitionId) AND
			Status <> dbo.GetDeleteStatus()
		ORDER BY
			A.Title	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetGlobalWorkflow]'
GO
/*****************************************************
* Name : Workflow_GetGlobalWorkflow
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetDefaultWorkflow
--			@ApplicationId	= uniqueidentifier null ,
--			@ProductId	= uniqueidentifier null 
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetGlobalWorkflow](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@ProductId		uniqueidentifier = null
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
    SELECT 
			ROW_NUMBER() OVER (ORDER BY A.Title) AS RowNumber,
			A.Id, 
			A.Title,  
			A.Description,
			A.ObjectTypeId,
			A.CreatedDate,
			A.CreatedBy,
			A.ModifiedDate,
			A.ModifiedBy,
			A.ArchivedDate,
			A.RootId,
			A.Status,
			A.SkipAfterDays,
			A.IsGlobal,
			A.IsShared,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
    FROM	dbo.WFWorkflow A
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
    WHERE	
			(@ApplicationId IS NULL OR (A.ApplicationId = @ApplicationId OR A.IsShared = 1 AND A.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))) AND
			A.ProductId		=	Isnull(@ProductId, ProductId) AND
			A.IsGlobal = 1
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_IsSystemUser]'
GO
/*****************************************************
* Name : Membership_IsSystemUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Membership_IsSystemUser @ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@ApplicationId='CDD38906-6071-4DD8-9FB6-508C38848C61',@Username=NULL,@UserId='798613EE-7B85-4628-A6CC-E17EE80C09C5'
--exec Membership_IsSystemUser @ProductId='CBB702AC-C8F1-4C35-B2DC-839C26E39848',@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@Username=NULL,@UserId='798613EE-7B85-4628-A6CC-E17EE80C09C5'
ALTER PROCEDURE [dbo].[Membership_IsSystemUser]
--********************************************************************************
-- PARAMETERS
	@ProductId					uniqueidentifier = NULL,
	@ApplicationId				uniqueidentifier,
	@Username					nvarchar(256),
	@UserId						uniqueidentifier
--********************************************************************************

AS
BEGIN
	DECLARE @IsSystemUser	bit
	SET @IsSystemUser = 0

	IF (@UserId IS NULL)
	BEGIN
		SELECT	@UserId = u.Id, @IsSystemUser = IsSystemUser
		FROM	dbo.USUser u JOIN	dbo.USSiteUser s	ON 
					--s.SiteId = @ApplicationId	AND	
					s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
					u.Id = s.UserId	AND	
					u.UserName = @Username AND
					s.ProductId = ISNULL(@ProductId , s.ProductId)
	END
	ELSE	IF(@UserId IS NOT NULL)
	BEGIN
		SELECT	@IsSystemUser = IsSystemUser
		FROM	dbo.USSiteUser	s
		WHERE    --s.SiteId = @ApplicationId	AND
					s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
					s.UserId = @UserId AND
					s.ProductId = ISNULL(@ProductId , s.ProductId)
	END

	SELECT ISNULL(@IsSystemUser,0)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[iAppsForm_GetiAppsForm]'
GO
/*****************************************************
* Name : iAppsForm_GetiAppsForm
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[iAppsForm_GetiAppsForm] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier
)
as
Begin
	
		SELECT	Id,
				ApplicationId,
				Title,
				Description, 
				FormType,								
				CreatedDate,
				CreatedBy,
				ModifiedBy,
				ModifiedDate, 
				Status,
				XMLString,				
				XSLTString,
				XSLTFileName,
				SendEmailYesNo,
				Email,
				ThankYouURL,
				SubmitButtonText,
				PollResultType,	
				PollVotingType,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				AddAsContact,
				ContactEmailField,
				WatchId,
				F.EmailSubjectText,
				F.EmailSubjectField,
				F.SourceFormId,
				ParentId
		FROM Forms F
		LEFT JOIN VW_UserFullName FN on FN.UserId = F.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = F.ModifiedBy
		where Id = @Id 			  
			  and
			  Status <> dbo.GetDeleteStatus()
			  Order by Title			  

	
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_GetSite]'
GO
/*****************************************************
* Name : Site_GetSite
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Site_GetSite]  
(  
	@Id				uniqueIdentifier = null ,  
	@Title			nvarchar(256) = null,  
	@ParentSiteId	uniqueIdentifier = null,  
	@Status			int = null,  
	@PageIndex		int,  
	@PageSize		int,
	@ForceAllSites	bit = null,
	@MasterSiteId   uniqueIdentifier = null,
	@SiteGroupId	uniqueIdentifier = null
 )  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	IF @PageSize is Null  
	BEGIN  
		SELECT S.Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId, 
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName, 
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			NotifyOnTranslationError, 
			TranslationEnabled, 
			TranslatePagePropertiesByDefault, 
			AutoImportTranslatedSubmissions, 
			DefaultTranslateToLanguage,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
			MasterSiteId
		FROM SISite S  
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id
		WHERE 
			(@Id IS NULL OR S.Id = @Id) AND
			(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
			(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
			(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
			(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
			(@Status IS NULL OR S.Status = @Status) AND
			(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus)
			
	END  
	ELSE  
	BEGIN  
		WITH ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate) AS Row, 
				S.Id, 
				Title, 
				Description, 
				Status, 
				CreatedBy, 
				CreatedDate, 
				ModifiedBy,   
				ModifiedDate, 
				URL,
				Type,
				ParentSiteId,
				Keywords,
				VirtualPath,
				HostingPackageInstanceId, 
				LicenseLevel, 
				SendNotification, 
				SubmitToTranslation,
				ImportAllAdminPermission,
				AllowMasterWebSiteUser,
				Prefix, 
				UICulture, 
				LoginURL, 
				DefaultURL,
				NotifyOnTranslationError, 
				TranslationEnabled, 
				TranslatePagePropertiesByDefault, 
				AutoImportTranslatedSubmissions, 
				DefaultTranslateToLanguage,
				PageImportOptions,
				ImportContentStructure,
				ImportFileStructure,
				ImportImageStructure,
				ImportFormsStructure,
				PrimarySiteURL,
				dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath,
				MasterSiteId
			FROM SISite S
			LEFT JOIN SISiteGroup SG ON SG.SiteId =  S.Id 
			WHERE 
				(@Id IS NULL OR S.Id = @Id) AND
				(@Title IS NULL OR @Title = '' OR UPPER(S.Title) = UPPER(@Title)) AND  
				(@ParentSiteId IS NULL OR S.ParentSiteId = @ParentSiteId) AND
				(@MasterSiteId IS NULL OR S.MasterSiteId = @MasterSiteId) AND
				(@SiteGroupId IS NULL OR SG.GroupId = @SiteGroupId) AND
				(@Status IS NULL OR S.Status = @Status) AND
				(@Status = @DeleteStatus OR @ForceAllSites = 1 OR S.Status != @DeleteStatus) 
		)
		
		SELECT 
			Id, 
			Title, 
			Description, 
			Status, 
			CreatedBy, 
			CreatedDate, 
			ModifiedBy, 
			ModifiedDate,   
			URL,
			Type,
			ParentSiteId,
			Keywords,
			VirtualPath,
			HostingPackageInstanceId,  
			FN.UserFullName CreatedByFullName,  
			MN.UserFullName ModifiedByFullName,
			LicenseLevel, 
			SendNotification, 
			SubmitToTranslation,
			ImportAllAdminPermission,
			AllowMasterWebSiteUser,
			Prefix, 
			UICulture, 
			LoginURL, 
			DefaultURL,
			PageImportOptions,
			ImportContentStructure,
			ImportFileStructure,
			ImportImageStructure,
			ImportFormsStructure,
			PrimarySiteURL,
			SitePath,
			MasterSiteId
		FROM ResultEntries  A  
			LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy  
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize  
	END  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[UserShippingAddress_Get]'
GO
/*****************************************************
* Name : UserShippingAddress_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[UserShippingAddress_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
	SELECT 
	A.Id AddressId
	,A.FirstName
	,A.LastName
	,A.AddressType
	,A.AddressLine1
	,A.AddressLine2
	,A.AddressLine3
	,A.City
	,A.StateId
	,A.StateName
	,A.Zip
	,A.CountryId
	,A.Phone
	,A.County
	,UA.Id
	,UA.NickName
	,UA.IsPrimary
	,UA.Sequence
	,UA.CreatedBy
	,UA.CreatedDate AS CreatedDate
	,UA.ModifiedDate AS ModifiedDate
	,UA.ModifiedBy 
	,UA.UserId
	FROM GLAddress A
	Inner Join USUserShippingAddress UA ON A.Id=UA.AddressId
	WHERE UA.Id=@Id
	AND UA. [Status] != dbo.GetDeleteStatus()
	Order By Sequence

	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_GetShippingAddress]'
GO
/*****************************************************
* Name : Customer_GetShippingAddress
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Customer_GetShippingAddress](@CustomerId uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
	SELECT 
	A.Id AddressId
	,A.FirstName
	,A.LastName
	,A.AddressType
	,A.AddressLine1
	,A.AddressLine2
	,A.AddressLine3
	,A.City
	,A.StateId
	,A.StateName
	,A.Zip
	,A.CountryId
	,A.Phone
	,UA.Id
	,UA.NickName
	,UA.IsPrimary
	,UA.Sequence
	,UA.CreatedBy
	,dbo.ConvertTimeFromUtc(UA.CreatedDate,@ApplicationId)CreatedDate
	,dbo.ConvertTimeFromUtc(UA.ModifiedDate,@ApplicationId)ModifiedDate
	,UA.ModifiedBy 
	,UA.UserId
	,A.County
	FROM GLAddress A
	Inner Join USUserShippingAddress UA ON A.Id=UA.AddressId
	WHERE UA.UserId=@CustomerId
	AND UA. [Status] != dbo.GetDeleteStatus()
	Order By Sequence

	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetAllUsers]'
GO
/*****************************************************
* Name : Membership_GetAllUsers
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetAllUsers]  
(  
 @ProductId    uniqueidentifier = NULL,  
 @ProductIds    xml = NULL,  
 @ExcludedProductIds  xml = NULL,  
 @ApplicationId   uniqueidentifier = NULL,  
 @Status     int = NULL,  
 @IsSystemUser   bit = NULL,  
 @IsApproved    bit = NULL,  
 @IsLockedOut   bit = NULL,  
 @PageNumber    int = NULL,  
 @PageSize    int = NULL,  
 @IncludeExpiredUsers bit = NULL,  
 @SortBy     nvarchar(100) = NULL,  
 @SortOrder    nvarchar(10) = NULL,  
 @FilterBy    nvarchar(500) = NULL,  
 @IncludeVariantUsers bit = NULL,  
 @IgnoreStatus   bit = NULL,  
 @SinceExpired   datetime = NULL,  
 @UserIds    xml = NULL,  
 @UserId     uniqueidentifier = NULL,  
 @UserName    nvarchar(256) = NULL   
)  
AS  
BEGIN  
 DECLARE @tbVariantSiteIds TABLE (Id uniqueidentifier)  
 IF @ApplicationId IS NOT NULL  
  INSERT INTO @tbVariantSiteIds SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId)  
  
 DECLARE @tbProductIds TABLE (Id uniqueidentifier)  
 IF @ProductIds IS NOT NULL  
  INSERT INTO @tbProductIds   
  SELECT tab.col.value('@Id','uniqueidentifier') FROM @ProductIds.nodes('/Products/Product') tab(col)  
 ELSE IF @ProductId IS NOT NULL  
  INSERT INTO @tbProductIds VALUES (@ProductId)  
  
 DECLARE @tbUserIds TABLE (Id uniqueidentifier)  
 IF @UserIds IS NOT NULL  
  INSERT INTO @tbUserIds   
  SELECT tab.col.value('@Id','uniqueidentifier') FROM @UserIds.nodes('/Users/User') tab(col)  
  
 DECLARE @tbExcludedUserIds TABLE (Id uniqueidentifier)  
 IF @ExcludedProductIds IS NOT NULL  
  INSERT INTO @tbExcludedUserIds  
  SELECT UserId FROM @ExcludedProductIds.nodes('/Products/Product') tab1(col)  
   JOIN USSiteUser S ON tab1.col.value('@Id','uniqueidentifier') = S.ProductId  
  
 IF @IncludeVariantUsers IS NULL SET @IncludeVariantUsers = 0  
 IF @Status < 0 SET @Status = NULL  
   
 IF(@UserId IS NULL AND @UserName IS NOT NULL)    
    SELECT @UserId = Id FROM dbo.USUser U   
   INNER JOIN dbo.USSiteUser S ON U.Id = S.UserId   
    WHERE LOWER(u.UserName) = LOWER(@UserName) AND    
   (@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))) AND  
   (@ProductId IS NULL OR @ProductId = S.ProductId)     
   
 DECLARE @tbUser TYUser  
 INSERT INTO @tbUser  
 SELECT DISTINCT   
  U.Id,   
  U.UserName,    
  M.Email,  
  M.PasswordQuestion,   
  M.IsApproved,  
  U.CreatedDate,  
  M.LastLoginDate,  
  U.LastActivityDate,  
  M.LastPasswordChangedDate,  
  M.IsLockedOut,  
  M.LastLockoutDate,  
  U.FirstName,  
  U.LastName,  
  U.MiddleName,  
  U.ExpiryDate,  
  U.EmailNotification,  
  U.TimeZone,  
  U.ReportRangeSelection,  
  U.ReportStartDate,  
  U.ReportEndDate,  
  U.BirthDate,  
  U.CompanyName,  
  U.Gender,  
  U.HomePhone,  
  U.MobilePhone,  
  U.OtherPhone,  
  U.ImageId,  
  U.Status,
  I.Title  
 FROM dbo.USUser U  
  INNER JOIN dbo.USMembership M ON M.UserId = U.Id  
  INNER JOIN dbo.USSiteUser S ON S.UserId = U.Id AND   
   (@IsSystemUser IS NULL OR S.IsSystemUser = @IsSystemUser) AND  
   (@ApplicationId IS NULL OR  
    (@IncludeVariantUsers = 0 AND S.SiteId = @ApplicationId) OR  
    (@IncludeVariantUsers = 1 AND S.SiteId IN (Select Id FROM @tbVariantSiteIds))   
   )   
  INNER JOIN @tbProductIds P ON P.Id = S.ProductId  
  INNER JOIN iAppsProductSuite I ON I.Id = S.ProductId
 WHERE (  
    (@Status IS NULL AND U.Status != 3) OR  
    (@Status IS NOT NULL AND @Status = U.Status )  
   ) AND  
   (  
    @IgnoreStatus = 1 OR  
    (         
     (@IsApproved IS NULL OR M.IsApproved = @IsApproved) AND  --Checks whether the user is approved.  
     (@IsLockedOut IS NULL OR M.IsLockedOut = @IsLockedOut) AND -- Checks whether the user is locked out.  
     (@IncludeExpiredUsers = 1 OR U.ExpiryDate IS NULL OR U.ExpiryDate >= getutcdate()) AND  
     (@SinceExpired IS NULL OR U.ExpiryDate < @SinceExpired)  
    )  
   ) AND  
   M.UserId NOT IN (SELECT Id FROM @tbExcludedUserIds) AND  
   (  
    @FilterBy IS NULL OR   
    U.UserName like @FilterBy OR U.FirstName like @FilterBy OR U.LastName like @FilterBy  
   ) AND  
   (@UserIds IS NULL OR U.Id IN (SELECT Id FROM @tbUserIds)) AND  
   (@UserId IS NULL OR U.Id = @UserId)  
   
 EXEC [dbo].[Membership_BuildUser]   
  @tbUser = @tbUser,   
  @SortBy = @SortBy,   
  @SortOrder = @SortOrder,  
  @PageNumber = @PageNumber,  
  @PageSize = @PageSize  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Content_GetContentForContentList]'
GO
/*****************************************************
* Name : Content_GetContentForContentList
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Content_GetContentForContentList]
(
	@NodeId					uniqueidentifier = null,
	@XmlFormId				uniqueidentifier = null,
	@IncludeChild			bit = 0,
	@PageNumber				int = 1,
	@PageSize				int = NULL,
	@MaxRecords				int = null,
	@SortClause				nvarchar(max) = null,
	@TaxonomyIds			nvarchar(max) = null,
	@IncludeChildTaxonomy	bit = null,
	@MatchAllTaxonomyIds	bit = null,
	@ContentStatus			int = NULL,
	@FilterClause			nvarchar(max) = null,
	@NodeIds				xml = null,
	@ContentIds				xml = null,
	@SiteId					uniqueidentifier = null
)
AS
BEGIN
	DECLARE @tbContentIds TABLE(Id uniqueidentifier)

	IF @ContentIds IS NOT NULL
		INSERT INTO @tbContentIds
			SELECT CO.C.value('text()[1]','uniqueidentifier') FROM @ContentIds.nodes('/GenericCollectionOfGuid/guid') CO(C)
	ELSE IF @NodeIds IS NOT NULL
		INSERT INTO @tbContentIds
			SELECT Id FROM COContent Where ParentId IN
				(SELECT CO.N.value('text()[1]','uniqueidentifier') FROM @NodeIds.nodes('/GenericCollectionOfGuid/guid') CO(N))
	ELSE IF @NodeId IS NOT NULL
	BEGIN
		IF @IncludeChild = 1
		BEGIN
			DECLARE @LftValue int, @RgtValue int
			SELECT @LftValue = LftValue, @RgtValue = RgtValue from COContentStructure WHERE Id = @NodeId
			INSERT INTO @tbContentIds
				SELECT Distinct C.Id FROM COContentStructure S INNER JOIN COContent C on C.ParentId = S.Id Where LftValue >= @LftValue AND RgtValue <= @RgtValue
		END
		ELSE
			INSERT INTO @tbContentIds SELECT Id FROM COContent Where ParentId = @NodeId
	END
	ELSE IF @XmlFormId IS NOT NULL
		INSERT INTO @tbContentIds
			SELECT Id FROM COContent WHERE TemplateId = @XmlFormId	
	ELSE
		RETURN	
	
	DECLARE @ContentTypeId int
	IF @XmlFormId IS NULL
		SET @ContentTypeId = 7
	ELSE
		SET @ContentTypeId = 13
		
	CREATE TABLE #ContentIds (Id uniqueidentifier)
	IF @TaxonomyIds IS NULL
		INSERT INTO #ContentIds Select Id FROM @tbContentIds
	ELSE
	BEGIN
		DECLARE @tbTaxonomy TABLE(Id uniqueidentifier, LftValue bigint, RgtValue bigint)		
		IF @IncludeChildTaxonomy IS NOT NULL AND @IncludeChildTaxonomy = 1
		BEGIN
			INSERT INTO @tbTaxonomy 
			SELECT Id, LftValue, RgtValue FROM COTaxonomy C Inner JOIN dbo.SplitGuid (@TaxonomyIds, ',') TI ON C.Id = TI.Items
			INSERT INTO @tbTaxonomy 
				SELECT C.Id, C.LftValue, C.RgtValue FROM @tbTaxonomy P INNER JOIN COTaxonomy C ON C.LftValue Between P.LftValue AND P.RgtValue 
		END
		ELSE
			INSERT INTO @tbTaxonomy SELECT TI.Items,0,0 FROM dbo.SplitGuid (@TaxonomyIds, ',') TI
		
		IF @MatchAllTaxonomyIds = 1
			INSERT INTO #ContentIds
			SELECT DISTINCT T.Id FROM @tbContentIds T
				JOIN COTaxonomyObject TAO ON TAO.ObjectId = T.Id AND TAO.Status = 1 --AND TAO.ObjectTypeId = @ContentTypeId
				JOIN @tbTaxonomy Tx ON Tx.Id =  TAO.TaxonomyId
			GROUP BY T.Id 
			HAVING COUNT(*) >= (SELECT COUNT(*) FROM @tbTaxonomy)
		ELSE
			INSERT INTO #ContentIds
			SELECT DISTINCT T.Id FROM @tbContentIds T
				JOIN COTaxonomyObject TAO ON TAO.ObjectId = T.Id AND TAO.Status = 1 --AND TAO.ObjectTypeId = @ContentTypeId
				JOIN @tbTaxonomy Tx ON Tx.Id =  TAO.TaxonomyId
	END
	
	--PRINT @strContentIds	
	IF @@ROWCOUNT <= 0
		RETURN

	IF @PageNumber <= 0
		SET @PageNumber = 1
	
	IF @FilterClause IS NULL
		SET @FilterClause = ''
	IF @SortClause IS NULL
		SET @SortClause = ''
		
	DECLARE @SQLStmt nvarchar(max)
	SET @SQLStmt = N'
		;WITH ResultEntries AS ( 
				SELECT ROW_NUMBER() OVER (ORDER BY ' + @SortClause + ' C.OrderNo ASC) AS Row,
					C.Id AS ContentId,
					C.Title AS ContentTitle,
					C.Description AS ContentDescription, 
					C.Text, 
					C.XMLString,
					C.Status,
					C.ParentId AS NodeId,
					CS.VirtualPath AS NodePath,
					CS.Title AS NodeTitle,
					CS.Description AS NodeDescription
					FROM #ContentIds T 
						JOIN COContent C ON C.Id = T.Id AND C.ObjectTypeId = @ContentTypeId AND (@XmlFormId IS NULL OR C.TemplateId = @XmlFormId)
						JOIN COContentStructure CS ON CS.Id = C.ParentId
					WHERE C.Status = 1 AND (@SiteId IS NULL OR C.ApplicationId = @SiteId)
						' + @FilterClause + '
			), TotalEntries AS (SELECT count(*) Cnt FROM ResultEntries)	
			
			SELECT R.*,
					CASE When T.Cnt > @MaxRecords THEN @MaxRecords
					ELSE T.Cnt
					END AS TotalRecords
			FROM ResultEntries R CROSS JOIN TotalEntries T
			WHERE (@MaxRecords IS NULL OR Row <= @MaxRecords) AND (@PageSize IS NULL OR Row between 
					(@PageNumber - 1) * @PageSize + 1 and @PageNumber*@PageSize)
	'
	
	EXEC sp_executesql @SQLStmt,
			N'@XmlFormId uniqueidentifier,
				@ContentTypeId int,
				@PageSize int,
				@PageNumber int,
				@MaxRecords int,
				@SiteId uniqueidentifier',
			@XmlFormId,
			@ContentTypeId,
			@PageSize,
			@PageNumber,
			@MaxRecords,
			@SiteId
				
	--PRINT @SQLStmt
	
	DROP TABLE #ContentIds
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ImageStructure_Copy]'
GO
/*****************************************************
* Name : ImageStructure_Copy
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[ImageStructure_Copy] 
(
	@SourceNodeId 	[uniqueidentifier],  
	@DestinationNodeId 	[uniqueidentifier],
	@CreatedBy			[uniqueidentifier],
	@SourcePath			[NVarchar](max) OUTPUT,
	@DestinationPath	[NVarchar](max) OUTPUT
	
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Source_Lft bigint,
	@Source_Rgt bigint,
	@Destination_Lft bigint,
	@Destination_Rgt bigint,
	@Inc bigint,
	@ApplicationId uniqueidentifier,
	@DestApplicationId uniqueidentifier,
	@SiteName nvarchar(1024)	
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@SourceNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COImageStructure WHERE Id=@SourceNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||SourceNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	IF (@DestinationNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COImageStructure WHERE Id=@DestinationNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||DestinationNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	select @Source_Lft=LftValue,@Source_Rgt=RgtValue,@ApplicationId=SiteId
	from COImageStructure
	Where Id=@SourceNodeId

	Select @Destination_Lft=LftValue, @Destination_Rgt=RgtValue,@DestApplicationId=SiteId 
	from COImageStructure Where Id=@DestinationNodeId

	IF (@DestApplicationId != @ApplicationId)
	BEGIN
			RAISERROR('NOTSUPPORTED',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END
	IF @Destination_Rgt between @Source_Lft and @Source_Rgt
		BEGIN
			RAISERROR('NOTSUPPORTED',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

		SET @Inc = (@Source_Rgt - @Source_Lft ) + 1
		--Creating space for new nodes
		Update COImageStructure SET LftValue = CASE WHEN LftValue >= @Destination_Rgt THEN LftValue + @Inc ELSE LftValue END,
							   RgtValue = CASE WHEN RgtValue >= @Destination_Rgt THEN RgtValue + @Inc ELSE RgtValue END
		Where SiteId=@ApplicationId
		SET @Inc =  @Destination_Rgt - @Source_Lft

		Insert into COImageStructure(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Description,PhysicalPath,VirtualPath,CreatedBy,CreatedDate,Status,Attributes,ThumbnailImagePath,FolderIconPath,[Exists],[Size])
		Select newid(),D.Title,D.Keywords,D.ParentId,D.LftValue+@Inc,D.RgtValue+@Inc,D.SiteId,D.Description,D.PhysicalPath,dbo.GetVirtualPath(D.Id),@CreatedBy,getutcdate(),D.Status,D.Attributes,D.ThumbnailImagePath,D.FolderIconPath,D.[Exists],D.[Size]
		FROM COImageStructure  D inner join COImageStructure S 
		ON D.Id = S.Id
		inner join COImageStructure NS 
		ON NS.LftValue = S.LftValue + @Inc
		Where S.LftValue Between @Source_Lft and @Source_Rgt
		And NS.LftValue Between @Source_Lft + @Inc and @Source_Rgt + @Inc
		And NS.SiteId=@ApplicationId
		
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||MoveNode',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
			--Update the ParentId of the Moved Node
		Update COImageStructure Set ParentId =@DestinationNodeId 
		Where Id = @SourceNodeId
		AND SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||UpdateParentId',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		SELECT @SourcePath = VirtualPath, @ApplicationId=ApplicationId From SISiteDirectory Where Id= @SourceNodeId
	SELECT @DestinationPath = VirtualPath From SISiteDirectory Where Id= @DestinationNodeId
	Select @SiteName = Title From SISite Where Id=@ApplicationId
END

GO
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].PageMapNode_SaveWithParams'
GO
ALTER PROCEDURE [dbo].[PageMapNode_SaveWithParams] 
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0
)	
AS
BEGIN
	Declare @LftValue int,@RgtValue int
	Declare @P_PropogateWorkflow bit,
			@P_PropogateSecurityLevels bit,
			@P_PropogateRoles bit

	Set @ModifiedDate = cast(isnull(@ModifiedDate,getdate()) as varchar(50))
	Set @LftValue=0
	Set @RgtValue=0
	
	IF (@ParentId is not null) 
	BEGIN
		Select 
			@LftValue =LftValue,
			@RgtValue=RgtValue, 
			@P_PropogateWorkflow =PropogateWorkflow,
			@P_PropogateSecurityLevels =PropogateSecurityLevels,
			@P_PropogateRoles=PropogateRoles
		from PageMapNode 
		Where PageMapNodeId=@ParentId and SiteId=@SiteId
	END
	ELSE 
	BEGIN
		Set @LftValue=1
		Set @RgtValue=1		
	END
	
	IF @LftValue <=0
	begin
		RAISERROR('NOTEXISTS||ParentId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	end

	if((@ParentId is not null) AND @P_PropogateSecurityLevels=1 OR @InheritSecurityLevels=1)
	BEGIN
		INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
		Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
		Where ObjectId=@ParentId
	END
			
	INSERT INTO USSecurityLevelObject(ObjectId,ObjectTypeId,SecurityLevelId)
	Select @PageMapNodeId,2,Value from SplitComma(@SecurityLevels,',')
	Where len(Value)>0
	except 
	Select @PageMapNodeId,ObjectTypeId,SecurityLevelId from USSecurityLevelObject
	Where ObjectId=@PageMapNodeId

	Update PageMapNode Set  LftValue = case when LftValue >@RgtValue Then LftValue +2 ELSE LftValue End,
							RgtValue = case when RgtValue >=@RgtValue then RgtValue +2 ELse RgtValue End 
	Where SiteId=@SiteId

	INSERT INTO [dbo].[PageMapNode]
           ([PageMapNodeId]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[SiteId]
           ,[ExcludeFromSiteMap]
           ,[Description]
           ,[DisplayTitle]
           ,[FriendlyUrl]
           ,[MenuStatus]
           ,[TargetId]
           ,[Target]
           ,[TargetUrl]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[PropogateWorkflow]
           ,[InheritWorkflow]
           ,[PropogateSecurityLevels]
           ,[InheritSecurityLevels]
           ,[PropogateRoles]
           ,[InheritRoles]
           ,[Roles]
           ,[LocationIdentifier]
           ,[HasRolloverImages]
           ,[RolloverOnImage]
           ,[RolloverOffImage]
           ,[IsCommerceNav]
           ,[AssociatedQueryId]
           ,[DefaultContentId]
           ,[AssociatedContentFolderId]
           ,[CustomAttributes]
           ,[HiddenFromNavigation])
     VALUES
           (@PageMapNodeId
           ,@ParentId
           ,@RgtValue
           ,@RgtValue+1
           ,@SiteId
           ,@ExcludeFromSiteMap
           ,@Description
           ,@DisplayTitle
           ,@FriendlyUrl
           ,@MenuStatus
           ,@TargetId
           ,@Target
           ,@TargetUrl
           ,@ModifiedBy
           ,@ModifiedDate
           ,@ModifiedBy
           ,@ModifiedDate
           ,@PropogateWorkflow
           ,@InheritWorkflow
           ,@PropogateSecurityLevels
           ,@InheritSecurityLevels
           ,@PropogateRoles
           ,@InheritRoles
           ,@Roles
           ,@LocationIdentifier
           ,@HasRolloverImages
           ,@RolloverOnImage
           ,@RolloverOffImage
           ,@IsCommerceNav
           ,@AssociatedQueryId
           ,@DefaultContentId
           ,@AssociatedContentFolderId
           ,@CustomAttributes
           ,@HiddenFromNavigation)

	-- Moved down after the Insert page map node as there is a relationship with workflowId
	if((@ParentId is not null) AND @P_PropogateWorkflow=1 OR @InheritWorkflow=1)
	BEGIN
		--PRINT 'In propogate workflow'
		Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
		Select NEWID(),@PageMapNodeId,WorkflowId,CustomAttributes from PageMapNodeWorkflow
		Where PageMapNodeId=@ParentId
	END
	
	Insert into PageMapNodeWorkflow(Id,PageMapNodeId,WorkflowId,CustomAttributes)
	Select NEWID(),@PageMapNodeId,WorkflowId,NULL
	from (
	select Value WorkflowId
	from SplitComma(@PageMapNodeWorkFlow,',')
	Where LEN(Value)>0
	except
	Select WorkflowId 
	from PageMapNodeWorkflow
	Where PageMapNodeId=@PageMapNodeId
	)F

	EXEC  [dbo].[PageMapNode_UpdateMemberRoles] 
		@SiteId = @SiteId,
		@PageMapNodeId = @PageMapNodeId,
		@Propogate = @PropogateRoles,
		@Inherit = @InheritRoles
				
	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 1

END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetPassword]'
GO
/*****************************************************
* Name : Membership_GetPassword
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Membership_GetPassword]
--********************************************************************************
-- PARAMETERS   
	@ApplicationId					uniqueidentifier,
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @PasswordAnswer                 nvarchar(128) = NULL,
    @ErrorCode					 int OUTPUT
--********************************************************************************
AS
BEGIN
  
--********************************************************************************
-- Variable declaration
--********************************************************************************
	
	DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

	DECLARE @CurrentTimeUtc							datetime
    
	--Get current date time
	SET		@CurrentTimeUtc =GetUTCDate()

    DECLARE @TranStarted   bit
    SET @TranStarted = 0
    SET @ErrorCode = 0

	DECLARE @DeletedSatus INT
	SET @DeletedSatus = dbo.GetDeleteStatus()

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.Id,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m WITH ( UPDLOCK )
    WHERE   --s.SiteId = @ApplicationId AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
            u.Id = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName AND
            u.Id = s.UserId AND 
            u.Status <> @DeletedSatus	

    --User not found
	IF ( @@rowcount = 0 )
    BEGIN
        RAISERROR ('NOTEXISTS||%s', 16, 1, @UserName)
        GOTO Cleanup
    END

	--User is locked out
    IF( @IsLockedOut = 1 )
    BEGIN
        RAISERROR ('USERLOCKED||%s', 16, 1, @UserName)
        GOTO Cleanup
    END

	
    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

			SET @ErrorCode = -3
            --RAISERROR ('USERLOCKED||%s', 16, 1, @UserName)
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 )
            END
        END

        UPDATE dbo.USMembership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
			SET @ErrorCode = 0
            RAISERROR ('OPERATIONFAILED||%s', 16, 1, 'MembershipUpdate')
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
		SET @TranStarted = 0
		COMMIT TRANSACTION
    END

    --IF( @ErrorCode = 0 )
        SELECT @Password Password, @PasswordFormat PasswordFormat

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN dbo.GetBusinessRuleErrorCode()

END

--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_GetPaymentInfo]'
GO
/*****************************************************
* Name : Customer_GetPaymentInfo
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
  
-- Purpose:   
-- Author:   
-- Created Date:06-01-2009  
-- Created By:Prakash  
  
-- Modified Date:   
-- Modified By:   
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
--  
--********************************************************************************  
  
  
ALTER PROCEDURE [dbo].[Customer_GetPaymentInfo](@CustomerId uniqueidentifier,@ApplicationId uniqueidentifier=null)  
AS  
BEGIN  
 -- open the symmetric key   
 OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd'   
 OPEN SYMMETRIC KEY [key_DataShare]   
 DECRYPTION BY CERTIFICATE cert_keyProtection;  
  
 Declare @BillingAddressId uniqueidentifier  
  
 SELECT @BillingAddressId = BillingAddressId   
 FROM USUserPaymentInfo   
 WHERE UserId=@CustomerId  
  
 SELECT UP.Id,  
  P.Id PaymentInfoId,  
  P.CardType,  
  CONVERT(varchar(max), DecryptByKey(P.Number))  [Number],  
  CAST(CONVERT(varchar(2), DecryptByKey(P.ExpirationMonth)) AS INT) ExpirationMonth  ,  
  CAST(CONVERT(varchar(4), DecryptByKey(P.ExpirationYear)) AS INT) ExpirationYear,  
  P.NameOnCard,  
  UP.UserId,  
  UP.BillingAddressId,  
  UP.Sequence,  
  UP.IsPrimary,  
  UP.CreatedBy,  
  dbo.ConvertTimeFromUtc(UP.CreatedDate,@ApplicationId)CreatedDate,  
  UP.ModifiedBy,  
  dbo.ConvertTimeFromUtc(UP.ModifiedDate,@ApplicationId)ModifiedDate,  
  UP.[Status],
  UP.ExternalProfileId,
  UP.Nickname  
 FROM PTPaymentInfo P  
 Inner Join USUserPaymentInfo UP ON P.Id=UP.PaymentInfoId  
 WHERE UP.UserId=@CustomerId  
 AND UP.[Status] != dbo.GetDeleteStatus()  
  
 SELECT     
 A.Id AddressId  
 ,A.FirstName  
 ,A.LastName  
 ,A.AddressType  
 ,A.AddressLine1  
 ,A.AddressLine2  
 ,A.AddressLine3  
 ,A.City  
 ,A.StateId  
 ,A.Zip  
 ,A.CountryId  
 ,A.Phone  
 ,UA.Id  
 ,UA.NickName  
 ,UA.IsPrimary  
 ,UA.Sequence  
 ,UA.CreatedBy  
 ,dbo.ConvertTimeFromUtc(UA.CreatedDate,@ApplicationId)CreatedDate  
 ,dbo.ConvertTimeFromUtc(UA.ModifiedDate,@ApplicationId)ModifiedDate  
 ,UA.ModifiedBy   
 ,A.County
 FROM GLAddress A  
 Inner Join USUserShippingAddress UA ON A.Id=UA.AddressId  
 Where UA.Id=@BillingAddressId  
 AND UA. [Status] != dbo.GetDeleteStatus()  
 -- close the symmetric key  
 CLOSE SYMMETRIC KEY [key_DataShare];  
 CLOSE MASTER KEY  
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[FileStructure_Copy]'
GO
/*****************************************************
* Name : FileStructure_Copy
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[FileStructure_Copy] 
(
	@SourceNodeId 	[uniqueidentifier],  
	@DestinationNodeId 	[uniqueidentifier],
	@CreatedBy			[uniqueidentifier],
	@SourcePath			[NVarchar](max) OUTPUT,
	@DestinationPath	[NVarchar](max) OUTPUT
	
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Source_Lft bigint,
	@Source_Rgt bigint,
	@Destination_Lft bigint,
	@Destination_Rgt bigint,
	@Inc bigint,
	@ApplicationId uniqueidentifier,
	@DestApplicationId uniqueidentifier,
	@SiteName nvarchar(1024)	
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@SourceNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COFileStructure WHERE Id=@SourceNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||SourceNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	IF (@DestinationNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COFileStructure WHERE Id=@DestinationNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||DestinationNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	select @Source_Lft=LftValue,@Source_Rgt=RgtValue,@ApplicationId=SiteId
	from COFileStructure
	Where Id=@SourceNodeId

	Select @Destination_Lft=LftValue, @Destination_Rgt=RgtValue,@DestApplicationId=SiteId 
	from COFileStructure Where Id=@DestinationNodeId

	IF (@DestApplicationId != @ApplicationId)
	BEGIN
			RAISERROR('NOTSUPPORTED',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END
	IF @Destination_Rgt between @Source_Lft and @Source_Rgt
		BEGIN
			RAISERROR('NOTSUPPORTED',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

		SET @Inc = (@Source_Rgt - @Source_Lft ) + 1
		--Creating space for new nodes
		Update COFileStructure SET LftValue = CASE WHEN LftValue >= @Destination_Rgt THEN LftValue + @Inc ELSE LftValue END,
							   RgtValue = CASE WHEN RgtValue >= @Destination_Rgt THEN RgtValue + @Inc ELSE RgtValue END
		Where SiteId=@ApplicationId
		SET @Inc =  @Destination_Rgt - @Source_Lft

		Insert into COFileStructure(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Description,PhysicalPath,VirtualPath,CreatedBy,CreatedDate,Status,Attributes,ThumbnailImagePath,FolderIconPath,[Exists],[Size])
		Select newid(),D.Title,D.Keywords,D.ParentId,D.LftValue+@Inc,D.RgtValue+@Inc,D.SiteId,D.Description,D.PhysicalPath,dbo.GetVirtualPath(D.Id),@CreatedBy,getutcdate(),D.Status,D.Attributes,D.ThumbnailImagePath,D.FolderIconPath,D.[Exists],D.[Size]
		FROM COFileStructure  D inner join COFileStructure S 
		ON D.Id = S.Id
		inner join COFileStructure NS 
		ON NS.LftValue = S.LftValue + @Inc
		Where S.LftValue Between @Source_Lft and @Source_Rgt
		And NS.LftValue Between @Source_Lft + @Inc and @Source_Rgt + @Inc
		And NS.SiteId=@ApplicationId
		
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||MoveNode',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
			--Update the ParentId of the Moved Node
		Update COFileStructure Set ParentId =@DestinationNodeId 
		Where Id = @SourceNodeId
		AND SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||UpdateParentId',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		SELECT @SourcePath = VirtualPath, @ApplicationId=ApplicationId From SISiteDirectory Where Id= @SourceNodeId
	SELECT @DestinationPath = VirtualPath From SISiteDirectory Where Id= @DestinationNodeId
	Select @SiteName = Title From SISite Where Id=@ApplicationId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[iAppsForm_GetFormsById]'
GO
/*****************************************************
* Name : iAppsForm_GetFormsById
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[iAppsForm_GetFormsById] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@FormsId uniqueidentifier
)
as
BEGIN
	
		SELECT	F.Id,
				F.ApplicationId,
				F.Title,
				COALESCE(F.Description,F.Title) [Description],
				F.CreatedDate,
				F.CreatedBy,
				F.ModifiedBy,
				F.ModifiedDate, 
				F.Status,
				F.XMLString,				
				F.XSLTString,
				F.XSLTFileName,
				F.SendEmailYesNo,
				F.Email,
				F.ThankYouURL,
				F.FormType,	
				F.PollResultType,	
				F.PollVotingType,
				F.AddAsContact,
				F.ContactEmailField,
				F.WatchId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				F.EmailSubjectText,
				F.EmailSubjectField,
				F.ParentId
		FROM Forms F
		LEFT JOIN VW_UserFullName FN on FN.UserId = F.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = F.ModifiedBy
		WHERE	F.Id =	@FormsId	AND F.Status <> dbo.GetDeleteStatus()
		ORDER BY F.Title
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ManualPriceSetSku_Get]'
GO
/*****************************************************
* Name : ManualPriceSetSku_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the Discount Range for pricing set
-- Author: Prakash
-- Created Date: 05-05-2009
-- Created By: Prakash V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].AutoPriceSetDiscount_Get 
--          @Id = 'uniqueidentifier'  null,
--			@ApplicationId	= uniqueidentifier null ,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[ManualPriceSetSku_Get](
--********************************************************************************
-- Parameters
--********************************************************************************
			@Id				uniqueidentifier = null,
			@PriceSetId		uniqueidentifier =null,
			@PageNumber		int = null,
			@PageSize		int = null,
			@ApplicationId uniqueidentifier=null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

	IF (@PageNumber is not null AND @PageSize is not null)
	BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageNumber,0)
		SET @PageUpperBound = @PageLowerBound - Isnull(@PageSize,0) + 1;
	
		WITH ResultEntries AS
		(
            SELECT ROW_NUMBER() OVER (ORDER BY M.CreatedDate Desc,P.Title,S.SKU) AS RowNumber,
			M.Id, 
		M.SKUId,
		M.PriceSetId , 
		M.MinQuantity, 
		M.MaxQuantity, 
		M.Price,
		M.CreatedDate,
		M.CreatedBy,
		M.ModifiedDate,
		M.ModifiedBy,
		S.ProductId,
		S.SiteId,
		S.Title,
		S.SKU,
		S.Description,
		S.Sequence,
		S.IsActive,
		S.IsOnline,
		S.ListPrice,
		S.UnitCost,
		S.WholesalePrice,
		--S.Quantity,
		S.PreviousSoldCount,
		S.Status,
		S.Measure,
		S.OrderMinimum,
		S.OrderIncrement,
		P.Title AS ParentProductTitle
	FROM PSPriceSetManualSKU M
		INNER JOIN PRProductSKU S ON M.SKUId=S.Id
		INNER JOIN PRPRoduct P ON P.Id = S.ProductId
			WHERE	(@Id is null OR M.Id = @Id) AND 
						(@PriceSetId is null OR [PriceSetId] = @PriceSetId)
		), TotalEntries AS (SELECT count(*) TotalRecords FROM ResultEntries)	

		SELECT * 
		FROM ResultEntries CROSS JOIN TotalEntries T
		WHERE ((@PageNumber is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound))
		Order By RowNumber
	END
	ELSE
	BEGIN

	SELECT 	M.Id, 
		M.SKUId,
		M.PriceSetId , 
		M.MinQuantity, 
		M.MaxQuantity, 
		M.Price,
		M.CreatedDate,
		M.CreatedBy,
		M.ModifiedDate,
		M.ModifiedBy,
		S.ProductId,
		S.SiteId,
		S.Title,
		S.SKU,
		S.Description,
		S.Sequence,
		S.IsActive,
		S.IsOnline,
		S.ListPrice,
		S.UnitCost,
		S.WholesalePrice,
		--S.Quantity,
		S.PreviousSoldCount,
		S.Status,
		S.Measure,
		S.OrderMinimum,
		S.OrderIncrement,
		P.Title AS ParentProductTitle
	FROM PSPriceSetManualSKU M
		INNER JOIN PRProductSKU S ON M.SKUId=S.Id
		INNER JOIN PRPRoduct P ON P.Id = S.ProductId
						WHERE	(@Id is null OR M.Id = @Id) AND 
								(@PriceSetId is null OR [PriceSetId] = @PriceSetId)
			Order By P.CreatedDate Desc,ParentProductTitle,SKU
	END

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_Save]'
GO
/*****************************************************
* Name : Order_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Order_Save]  
(   
 @Id uniqueidentifier output,  
 @CartId uniqueidentifier,  
 @OrderTypeId int,  
 @OrderStatusId int,  
 @PurchaseOrderNumber nvarchar(100),  
 @ExternalOrderNumber nvarchar(100),
 @SiteId uniqueidentifier,  
 @CustomerId uniqueidentifier,  
 @TotalTax money,  
 --@ShippingTotal money,
 @OrderTotal money,  
 @CODCharges money,
 @TaxableOrderTotal money,  
 @GrandTotal money,
 @TotalShippingCharge money,
 @TotalDiscount money,
 @PaymentTotal money,
 @PaymentRemaining money out,  
 @OrderDate datetime,  
 --@CreatedDate datetime,  
 --@CreatedBy uniqueidentifier,  
 --@ModifiedDate datetime,  
 @ModifiedBy uniqueidentifier,
 @TotalShippingDiscount money,
 @ApplicationId uniqueidentifier	,
 @AddedAttributes	xml=null,
 @CouponIds xml=null

)  
  
AS  
BEGIN       
  
DECLARE @CreatedDate datetime, @ModifiedDate DateTime

SET @SiteId = @ApplicationId

if(@CartId IS NULL OR @CartId = dbo.GetEmptyGUID())
	SET @CartId = NULL;
ELSE IF @Id is null  OR @Id = dbo.GetEmptyGUID()  
		SELECT @Id =Id from OROrder Where CartId=@CartId

if @CODCharges is null 
	set @CODCharges = 0

 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
  set @Id = newid()  
  set @CreatedDate = GetUTCDate()



  INSERT INTO OROrder  
           ([Id]  
   ,[CartId]  
   ,[OrderTypeId]  
   ,[OrderStatusId]  
   ,[PurchaseOrderNumber]  
	,[ExternalOrderNumber]
   ,[SiteId]  
   ,[CustomerId]  
   ,[TotalTax]  
   --,[ShippingTotal]  
   ,[OrderTotal]  
   ,[CODCharges]
   ,[TaxableOrderTotal]  
   ,[GrandTotal] 
   ,[TotalShippingCharge] 
   ,[TotalDiscount] 
   ,[PaymentTotal] 
   ,[RefundTotal]
   -- ,[PaymentRemaining]   -- This is changed to calculated columns
   ,[OrderDate]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[TotalShippingDiscount])  
     VALUES  
           (@Id,  
   @CartId,  
   @OrderTypeId,  
   @OrderStatusId,  
   @PurchaseOrderNumber, 
	@ExternalOrderNumber, 
   @SiteId,  
   @CustomerId,  
   @TotalTax,  
   --@ShippingTotal,
   @OrderTotal,  
	@CODCharges,
   @TaxableOrderTotal,  
   @GrandTotal, 
   @TotalShippingCharge, 
   @TotalDiscount, 
   dbo.Order_GetPaymentTotal(@Id), 
   dbo.Order_GetRefundTotal(@Id),
  -- @PaymentRemaining,  -- This is changed to calculated columns
   dbo.ConvertTimeToUtc(@OrderDate,@ApplicationId),  
   @CreatedDate,  
   @ModifiedBy,  
   @CreatedDate,  
   @ModifiedBy,
   @TotalShippingDiscount)  
   		
		if @AddedAttributes is not null
			begin
				Insert into OROrderAttributeValue(Id, OrderId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
				Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), @CreatedDate,@ModifiedBy
				From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple') tab(col)

			end

 end  
 else  
 begin  
  update OROrder  
  set CartId=@CartId,  
   OrderTypeId=@OrderTypeId,  
   OrderStatusId=@OrderStatusId,  
   PurchaseOrderNumber=@PurchaseOrderNumber,  
   ExternalOrderNumber =@ExternalOrderNumber,
   SiteId=@SiteId,  
   CustomerId=@CustomerId,  
   TotalTax=@TotalTax,  
   --ShippingTotal=@ShippingTotal,  
   OrderTotal=@OrderTotal,  
	CODCharges=@CODCharges,
   TaxableOrderTotal=@TaxableOrderTotal,  
   GrandTotal=@GrandTotal,
   TotalShippingCharge=@TotalShippingCharge,
   TotalDiscount=@TotalDiscount,
   PaymentTotal=dbo.Order_GetPaymentTotal(@Id),
   RefundTotal=dbo.Order_GetRefundTotal(@Id),
 --  PaymentRemaining=@PaymentRemaining,   -- This is changed to calculated columns
   OrderDate=dbo.ConvertTimeToUtc(@OrderDate,@ApplicationId),  
   --CreatedDate=@CreatedDate,  
   --CreatedBy=@CreatedBy,  
   ModifiedDate=GetUTCDate(),  
   ModifiedBy=@ModifiedBy,
   TotalShippingDiscount=@TotalShippingDiscount  
   where Id=@Id  
  
		if @AddedAttributes is not null
			begin
					Delete From dbo.OROrderAttributeValue where OrderId= @Id 
					
				Insert into OROrderAttributeValue(Id, OrderId, 
													AttributeId, AttributeEnumId, 
													Value, CreatedDate,CreatedBy)
				Select newId(), @Id, tab.col.value('(AttributeId)[1]','uniqueidentifier'),
													tab.col.value('(AttributeEnumId)[1]','uniqueidentifier'),
													tab.col.value('(Value)[1]','nvarchar(max)'), GetUTCDate(),@ModifiedBy
				From @AddedAttributes.nodes('GenericCollectionOfAssignableAttributeValueTuple/AssignableAttributeValueTuple') tab(col)
			end
     
 end  
 --if @CouponIds is not null
 if @CouponIds is not NULL ---AND CAST(@CouponIds AS NVARCHAR(MAX))!= '<GenericCollectionOfGuid/>'
	Exec Coupon_ApplyToOrder @CouponCodeIds=@CouponIds,
	 @OrderId=@Id,
	 @CustomerId=@CustomerId,
	 @ModifiedBy =@ModifiedBy

Select @PaymentRemaining = PaymentRemaining from OROrder Where Id =@Id
  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetUserNameByEmail]'
GO
/*****************************************************
* Name : Membership_GetUserNameByEmail
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Membership_GetUserNameByEmail 'CDD38906-6071-4DD8-9FB6-508C38848C61','rmaheswari@bridgelinesw.com'
--exec Membership_GetUserNameByEmail '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4','rmaheswari@bridgelinesw.com'

ALTER PROCEDURE [dbo].[Membership_GetUserNameByEmail]
--********************************************************************************
-- PARAMETERS

    @ApplicationId			  uniqueidentifier,
    @Email            nvarchar(256)

--********************************************************************************

AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m
        WHERE   --s.SiteId = @ApplicationId	AND
				s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
                u.Id = m.UserId AND
				u.Id = s.UserId AND
				u.Status <> dbo.GetDeleteStatus() AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  UserName
        FROM USUser
        Where Id in (
        SELECT Distinct U.Id
        FROM    dbo.USSiteUser s, dbo.USUser u, dbo.USMembership m
        WHERE   --s.SiteId = @ApplicationId  AND
				s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) AND
                u.Id = m.UserId AND
				u.Id = s.UserId AND
				u.Status <> dbo.GetDeleteStatus() AND
                LOWER(@Email) = m.LoweredEmail)
    RETURN(0)
END

--***********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[OrderPayment_GetByOrderId]'
GO
/*****************************************************
* Name : OrderPayment_GetByOrderId
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[OrderPayment_GetByOrderId]
	(@OrderId	uniqueidentifier 
	,@ApplicationId uniqueidentifier=null)
AS
BEGIN

IF EXISTS (SELECT OrderId FROM PTOrderPayment WHERE OrderId = @OrderId)
BEGIN 

OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
	OPEN SYMMETRIC KEY [key_DataShare] 
	DECRYPTION BY CERTIFICATE cert_keyProtection;

DECLARE @PaymentId uniqueidentifier ;


SELECT 
	OP.Id,
	OP.OrderId,
	OP.PaymentId,
	OP.Amount,
	OP.Sequence
FROM
	PTOrderPayment OP
INNER JOIN PTPayment P ON OP.PaymentId = P.Id
WHERE
	OrderId = @OrderId Order By P.CreatedDate

SELECT 
	P.Id,
	P.PaymentTypeId,
	P.PaymentStatusId,
	P.CustomerId,
	P.Amount,
	P.CapturedAmount,
	P.Description,
	dbo.ConvertTimeFromUtc(P.FailureDate,@ApplicationId)FailureDate,
	P.FailureDescription,
	dbo.ConvertTimeFromUtc(P.CreatedDate,@ApplicationId)CreatedDate,
	P.CreatedBy,
	dbo.ConvertTimeFromUtc(P.ClearDate,@ApplicationId)ClearDate,
	P.ProcessorTransactionId,
	P.AuthCode,
	P.Status,
	dbo.ConvertTimeFromUtc(P.ModifiedDate,@ApplicationId)ModifiedDate,
	P.ModifiedBy,
	P.IsRefund
FROM
	PTPayment P
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId 


SELECT 
	PCC.Id,
	PCC.PaymentInfoId,
	PCC.ParentPaymentInfoId,
	PCC.PaymentId,
	PCC.BillingAddressId,
	PCC.Phone,
	PCC.ExternalProfileId
FROM 
	PTPaymentCreditCard PCC
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId



SELECT 
	PINFO.Id,
	PINFO.CardType,
	CONVERT(varchar(max), DecryptByKey(PINFO.Number))  [Number],
	CAST(CONVERT(varchar(2), DecryptByKey(PINFO.ExpirationMonth)) AS INT) ExpirationMonth  ,
	CAST(CONVERT(varchar(4), DecryptByKey(PINFO.ExpirationYear)) AS INT) ExpirationYear,
	PINFO.NameOnCard,
	PINFO.CreatedBy,
	dbo.ConvertTimeFromUtc(PINFO.CreatedDate,@ApplicationId)CreatedDate,
	PINFO.Status,
	PINFO.ModifiedBy,
	dbo.ConvertTimeFromUtc(PINFO.ModifiedDate,@ApplicationId)ModifiedDate
FROM
	PTPaymentInfo PINFO
	INNER JOIN PTPaymentCreditCard PCC ON PCC.PaymentInfoId = PINFO.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId
	



SELECT 
	A.Id,
	A.FirstName,
	A.LastName,
	A.AddressType,
	A.AddressLine1,
	A.AddressLine2,
	A.AddressLine3,
	A.City,
	A.StateId,
	A.Zip,
	A.CountryId,
	A.Phone,
	dbo.ConvertTimeFromUtc(A.CreatedDate,@ApplicationId)CreatedDate,
	A.CreatedBy,
	dbo.ConvertTimeFromUtc(A.ModifiedDate,@ApplicationId)ModifiedDate,
	A.ModifiedBy,
	A.Status,
	A.County
FROM 
	GLAddress A	
	INNER JOIN PTPaymentCreditCard PCC ON PCC.BillingAddressId  = A.Id
	INNER JOIN PTPayment P ON P.Id = PCC.PaymentId
	INNER JOIN PTOrderPayment OP ON OP.PaymentId = P.Id
WHERE
	OP.OrderId = @OrderId


SELECT  PC.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        dbo.ConvertTimeFromUtc(PC.CreatedDate, @ApplicationId) CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId ,
        SUM(ISNULL(PC2.CapturedAmount,0)) AS TotalRefundAmount
FROM    PTPaymentCapture PC
        INNER JOIN PTOrderPayment OP ON OP.PaymentId = PC.PaymentId
        LEFT JOIN dbo.PTPaymentCapture PC2 ON PC.Id = PC2.ParentPaymentCaptureId
WHERE   OP.OrderId = @OrderId
GROUP BY Pc.Id ,
        PC.PaymentId ,
        PC.CapturedAmount ,
        PC.CreatedDate ,
        PC.TransactionId ,
        PC.ParentPaymentCaptureId
ORDER BY PC.CreatedDate ASC

END

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ProfileManager_GetProfiles_Count]'
GO
/*****************************************************
* Name : ProfileManager_GetProfiles_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[ProfileManager_GetProfiles_Count]
-- *************************************************************************
-- PARAMETERS
	@ApplicationId				uniqueidentifier,
    @AuthenticationOption		int,
    @UserNameToMatch			nvarchar(256) = NULL,
    @InactiveSinceDate			datetime      = NULL

--**************************************************************************
AS
BEGIN
	SELECT 	COUNT(*)
    FROM    dbo.USUser u, dbo.USMemberProfile p, dbo.USSiteUser s
    WHERE   --s.SiteId = @ApplicationId AND
			s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
			u.Id = p.UserId AND
			u.Id = s.UserId		AND
			(@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
        AND (     (@AuthenticationOption = 2)
               OR (@AuthenticationOption = 0 AND IsAnonymous = 1)
               OR (@AuthenticationOption = 1 AND IsAnonymous = 0)
             )
        AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
		AND u.Status <> dbo.GetDeleteStatus()
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_GetUserWithProfile]'
GO
/*****************************************************
* Name : Membership_GetUserWithProfile
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Membership_GetUserWithProfile @ProductId='00000000-0000-0000-0000-000000000000',@ApplicationId='CDD38906-6071-4DD8-9FB6-508C38848C61',@UserId=NULL,@UserName=N'iappsuser'
--exec Membership_GetUserWithProfile @ProductId='00000000-0000-0000-0000-000000000000',@ApplicationId='8039CE09-E7DA-47E1-BCEC-DF96B5E411F4',@UserId=NULL,@UserName=N'iappsuser'

ALTER PROCEDURE [dbo].[Membership_GetUserWithProfile]  
--********************************************************************************  
-- PARAMETERS  
 @ProductId     uniqueidentifier = NULL,  
 @ApplicationId    uniqueidentifier,  
    @UserId      uniqueidentifier,  
 @UserName     nvarchar(256) = NULL  
--********************************************************************************  
AS   
BEGIN  
  Declare @EmptyGuid uniqueidentifier  
  Select @EmptyGuid = dbo.GetEmptyGUID()  
  
  IF(@UserId IS NULL)  
  BEGIN  
   SELECT @UserId = Id  
   FROM dbo.USUser u,dbo.USSiteUser s  
   WHERE LOWER(u.UserName) = LOWER(@UserName) AND  
     --s.SiteId = ISNULL(@ApplicationId,s.SiteId) AND  
     (@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
     AND u.Id = s.UserId AND  
     (@ProductId Is null or @ProductId = @EmptyGuid or @ProductId = s.ProductId)  
  END    
  
  SELECT  u.UserName,  
    u.CreatedDate,  
    m.Email,   
    m.PasswordQuestion,    
    m.IsApproved,  
    m.LastLoginDate,   
    u.LastActivityDate,  
    m.LastPasswordChangedDate,   
    u.Id,   
    m.IsLockedOut,  
    m.LastLockoutDate,
    u.FirstName,
	u.LastName,
	u.MiddleName,
	u.ExpiryDate,
	u.EmailNotification,
	u.TimeZone,
	u.ReportRangeSelection,
	u.ReportStartDate,
	u.ReportEndDate,
	u.BirthDate,
	u.CompanyName,
	u.Gender,
	u.HomePhone,
	u.MobilePhone,
	u.OtherPhone,
	u.ImageId,
	u.DashboardData
  FROM    dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s--, dbo.USMemberProfile p  
  WHERE   u.Id = @UserId AND  
    u.Id = m.UserId     AND  
    --s.SiteId = ISNULL(@ApplicationId,s.SiteId)    AND  
     (@ApplicationId IS NULL OR s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)))
    AND m.UserId = s.UserId    AND  
    u.Status <> dbo.GetDeleteStatus() AND  
    --p.UserId = u.Id  AND  
    (@ProductId Is null or @ProductId = @EmptyGuid or @ProductId = s.ProductId)  
  
  --selects the user's profile from the UsMemberProfile table.  
  SELECT UserId,  
    PropertyName,  
    PropertyValueString,  
    PropertyValuesBinary,  
    LastUpdatedDate  
  FROM dbo.USMemberProfile  
  WHERE UserId = @UserId  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ProfileManager_DeleteInactiveProfiles]'
GO
/*****************************************************
* Name : ProfileManager_DeleteInactiveProfiles
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[ProfileManager_DeleteInactiveProfiles]
( 
--********************************************************************************
-- PARAMETERS
	@ApplicationId				uniqueidentifier,
    @AuthenticationOption		int = 1,
    @InactiveSinceDate			datetime
--********************************************************************************
)
AS
BEGIN
	DELETE
	FROM    dbo.USMemberProfile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.USUser u, dbo.USSiteUser s
                WHERE   --s.SiteId = @ApplicationId AND
						s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
						u.Id = s.UserId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@AuthenticationOption = 2)
                             OR (@AuthenticationOption = 0 AND IsAnonymous = 1)
                             OR (@AuthenticationOption = 1 AND IsAnonymous = 0)
                            )
            )
END
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_Get]'
GO
/*****************************************************
* Name : Customer_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Customer_Get]
(
      @Id uniqueidentifier=null,
      @CustomerIds xml=null,
      @UserId uniqueidentifier = null,
      @UserName nvarchar(256) =null,
      @Email      nvarchar(256)=null,
	  @AccountNumber nvarchar(256)=null,
      @isExpress  bit =null,
      @ApplicationId uniqueidentifier =null
)
as
begin
      IF @UserName is null AND @isExpress is null AND @Email is null AND @UserId is null and @AccountNumber is null
            BEGIN
            IF @CustomerIds is null
                  SELECT CP.[Id]
                          ,CP.[Id] as UserId
                          ,U.[FirstName]
                          ,U.[LastName]
                          ,U.[MiddleName]
                          ,U.[CompanyName]
                          ,U.[BirthDate]
                          ,U.[Gender]
                          ,CP.[IsBadCustomer]
                          ,CP.[IsActive]
                          ,CP.[IsOnMailingList]
                          ,CP.[Status]
                          ,CP.[CSRSecurityQuestion]
                          ,CP.[CSRSecurityPassword]
                          ,U.[HomePhone]
                          ,U.[MobilePhone]
                          ,U.[OtherPhone]
                          ,U.ImageId
                          ,CP.[AccountNumber]
                          ,CP.[IsExpressCustomer]
                          ,CP.ExternalId
                          ,CP.ExternalProfileId
                          ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                          , (Select count(*) from CSCustomerLogin where CustomerId= Isnull(@Id,CP.Id)) +1 as NumberOfLoginAccounts
                          , (Select count(*) from PTLineOfCreditInfo where CustomerId = IsnuLL(@Id, CP.Id) 
                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                   FROM [USCommerceUserProfile] CP
                   INNER JOIN USUser U on CP.Id = U.Id
            WHERE --CP.Id = Isnull(@Id,CP.Id)
				(@Id is null or CP.Id = @Id)
				 AND U.Status !=3
			ELSE
						  SELECT CP.[Id]
                          ,CP.[Id] as UserId
                          ,U.[FirstName]
                          ,U.[LastName]
                          ,U.[MiddleName]
                          ,U.[CompanyName]
                          ,U.[BirthDate]
                          ,U.[Gender]
                          ,CP.[IsBadCustomer]
                          ,CP.[IsActive]
                          ,CP.[IsOnMailingList]
                          ,CP.[Status]
                          ,CP.[CSRSecurityQuestion]
                          ,CP.[CSRSecurityPassword]
                          ,U.[HomePhone]
                          ,U.[MobilePhone]
                          ,U.[OtherPhone]
                          ,U.ImageId
                          ,CP.[AccountNumber]
                          ,CP.[IsExpressCustomer]
                          ,CP.ExternalId
                          ,CP.ExternalProfileId
                          ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                          , (Select count(*) from CSCustomerLogin where CustomerId= CP.Id) +1 as NumberOfLoginAccounts
                          , (Select count(*) from PTLineOfCreditInfo where CustomerId =  CP.Id 
                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                   FROM [USCommerceUserProfile] CP
                   INNER JOIN USUser U ON CP.Id = U.Id
                   INNER JOIN @CustomerIds.nodes('/GenericCollectionOfGuid/guid')tab(col) ON CP.Id =tab.col.value('text()[1]','uniqueidentifier')
                   Where U.Status !=3
            
            END
      ELSE
            BEGIN
                  if @UserId is null
                        begin
                                    select  @UserId = U.Id from USUser U 
                                    INNER JOIN USMembership M on M.UserId =U.Id
                                    INNER JOIN USSiteUser US ON US.UserId = U.Id
									LEFT JOIN USCommerceUserProfile UCP ON UCP.Id = U.Id
                                     WHERE --(U.UserName) = (isnull(@UserName,U.UserName))
												(@UserName is null or U.UserName = @UserName)
                                                AND 
                                                (@Email is null OR M.Email = @Email)
												AND
												(@AccountNumber is null OR UCP.AccountNumber = @AccountNumber)
												and U.Status <> dbo.GetDeleteStatus() 
                                                AND (@ApplicationId is null OR US.SiteID = @ApplicationId)
                                                
                                     
                        end
						
                  
                  if(@UserId is not null)
                        begin
                              SELECT CP.[Id]
                                      ,@UserId as UserId
                                      ,U.[FirstName]
                                      ,U.[LastName]
                                      ,U.[MiddleName]
                                      ,U.[CompanyName]
                                      ,U.[BirthDate]
                                      ,U.[Gender]
                                      ,CP.[IsBadCustomer]
                                      ,CP.[IsActive]
                                      ,CP.[IsOnMailingList]
                                      ,CP.[Status]
                                      ,CP.[CSRSecurityQuestion]
                                      ,CP.[CSRSecurityPassword]
                                      ,U.[HomePhone]
                                      ,U.[MobilePhone]
                                      ,U.[OtherPhone]
                                      ,U.ImageId
                                      ,CP.[AccountNumber]
                                      ,CP.[IsExpressCustomer]
                                      ,CP.[ModifiedDate]
                                      ,CP.ExternalId
									  ,CP.ExternalProfileId
                                      ,      dbo.ConvertTimeFromUtc(CP.ModifiedDate,@ApplicationId)ModifiedDate
                                        , (Select count(*) from CSCustomerLogin where CustomerId= Isnull(@Id,CP.Id)) +1 as NumberOfLoginAccounts
                                       , (Select count(*) from PTLineOfCreditInfo where CustomerId = IsnuLL(@Id, CP.Id)
                                                                                    and Status <> dbo.GetDeleteStatus()) as HasLOC
                               FROM [USCommerceUserProfile] CP
                               INNER JOIN USUser U ON CP.Id = U.Id
                               WHERE (CP.Id = (Select CustomerId from CSCustomerLogin where UserId = @UserId) OR CP.Id = @UserId )
                                    --AND IsExpressCustomer =isnull(@isExpress,IsExpressCustomer) 
									AND (@isExpress is null or IsExpressCustomer = @isExpress)
									AND U.Status != 3
                        end
            END  
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ContentStructure_Copy]'
GO
/*****************************************************
* Name : ContentStructure_Copy
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE  [dbo].[ContentStructure_Copy] 
(
	@SourceNodeId 	[uniqueidentifier],  
	@DestinationNodeId 	[uniqueidentifier],
	@CreatedBy			[uniqueidentifier],
	@SourcePath			[NVarchar](max) OUTPUT,
	@DestinationPath	[NVarchar](max) OUTPUT
	
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************

Declare @Source_Lft bigint,
	@Source_Rgt bigint,
	@Destination_Lft bigint,
	@Destination_Rgt bigint,
	@Inc bigint,
	@ApplicationId uniqueidentifier,
	@DestApplicationId uniqueidentifier,
	@SiteName nvarchar(1024)	
--********************************************************************************
-- code
--********************************************************************************
BEGIN
	/* if ID specified, ensure exists */
	IF (@SourceNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@SourceNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||SourceNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	IF (@DestinationNodeId IS NOT NULL)
		IF(SELECT count(*) FROM COContentStructure WHERE Id=@DestinationNodeId)=0
		BEGIN
			RAISERROR('NOTEXISTS||DestinationNodeId', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

	select @Source_Lft=LftValue,@Source_Rgt=RgtValue,@ApplicationId=SiteId
	from COContentStructure
	Where Id=@SourceNodeId

	Select @Destination_Lft=LftValue, @Destination_Rgt=RgtValue,@DestApplicationId=SiteId 
	from COContentStructure Where Id=@DestinationNodeId

	IF (@DestApplicationId != @ApplicationId)
	BEGIN
		RAISERROR('NOTSUPPORTED',16,1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	IF @Destination_Rgt between @Source_Lft and @Source_Rgt
		BEGIN
			RAISERROR('NOTSUPPORTED',16,1)
			RETURN dbo.GetBusinessRuleErrorCode()
		END

		SET @Inc = (@Source_Rgt - @Source_Lft ) + 1
		--Creating space for new nodes
		Update COContentStructure SET LftValue = CASE WHEN LftValue >= @Destination_Rgt THEN LftValue + @Inc ELSE LftValue END,
							   RgtValue = CASE WHEN RgtValue >= @Destination_Rgt THEN RgtValue + @Inc ELSE RgtValue END
		Where SiteId=@ApplicationId
		SET @Inc =  @Destination_Rgt - @Source_Lft

		Insert into COContentStructure(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Description,PhysicalPath,VirtualPath,CreatedBy,CreatedDate,Status,Attributes,ThumbnailImagePath,FolderIconPath,[Exists],[Size])
		Select newid(),D.Title,D.Keywords,D.ParentId,D.LftValue+@Inc,D.RgtValue+@Inc,D.SiteId,D.Description,D.PhysicalPath,dbo.GetVirtualPath(D.Id),@CreatedBy,getutcdate(),D.Status,D.Attributes,D.ThumbnailImagePath,D.FolderIconPath,D.[Exists],D.[Size]
		FROM COContentStructure  D inner join COContentStructure S 
		ON D.Id = S.Id
		inner join COContentStructure NS 
		ON NS.LftValue = S.LftValue + @Inc
		Where S.LftValue Between @Source_Lft and @Source_Rgt
		And NS.LftValue Between @Source_Lft + @Inc and @Source_Rgt + @Inc
		And NS.SiteId=@ApplicationId
		
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||MoveNode',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
			--Update the ParentId of the Moved Node
		Update COContentStructure Set ParentId =@DestinationNodeId 
		Where Id = @SourceNodeId
		AND SiteId=@ApplicationId
		IF @@Error <> 0
		BEGIN
			RAISERROR('DBERROR||UpdateParentId',16,1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		SELECT @SourcePath = VirtualPath, @ApplicationId=ApplicationId From SISiteDirectory Where Id= @SourceNodeId
	SELECT @DestinationPath = VirtualPath From SISiteDirectory Where Id= @DestinationNodeId
	Select @SiteName = Title From SISite Where Id=@ApplicationId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[SqlDirectoryProvider_GetFolderSecurityLevelMap]'
GO
/*****************************************************
* Name : SqlDirectoryProvider_GetFolderSecurityLevelMap
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[SqlDirectoryProvider_GetFolderSecurityLevelMap]
	@ApplicationId uniqueidentifier
AS
BEGIN
select F.Id, F.VirtualPath,S.SecurityLevelId from COImageStructure F 
	Inner Join USSecurityLevelObject S on F.Id=S.ObjectId  AND ObjectTypeId = 6
Where F.SiteId = @ApplicationId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetGlobalWorkflowWithSequence]'
GO
/*****************************************************
* Name : Workflow_GetGlobalWorkflowWithSequence
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflows with its sequences based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetDefaultWorkflowWithSequence
--			@ApplicationId	= uniqueidentifier null ,
--			@ProductId	= uniqueidentifier null 
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetGlobalWorkflowWithSequence](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,					
			@ProductId		uniqueidentifier = null
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		WITH ResultEntries AS
		(
            SELECT 
					ROW_NUMBER() OVER (ORDER BY A.Id) AS RowNumber,
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.ApplicationId,
					A.IsGlobal,
					A.IsShared
					
            FROM	dbo.WFWorkflow A
					
            WHERE	
					(@ApplicationId IS NULL OR (A.ApplicationId = @ApplicationId OR (A.IsShared = 1 AND A.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))))) AND
					A.ProductId		=	Isnull(@ProductId, ProductId)	AND
					A.IsGlobal = 1
		)

		SELECT 		A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.IsGlobal,
					A.ApplicationId,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName,
					B.Id As SequenceId, 
					B.RoleId,
					B.ActorId,
					B.ActorType,
					B.[Order],
					dbo.User_GetMemberName(B.ActorId,B.ActorType) As ActorName ,
					dbo.User_GetRoleName(B.RoleId) As RoleName
		FROM	ResultEntries	A
		INNER JOIN dbo.WFSequence	B ON A.[Id] = B.WorkflowId
		LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
		ORDER BY A.[Id], B.[Order] 

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Customer_GetCustomers]'
GO
/*****************************************************
* Name : Customer_GetCustomers
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Customer_GetCustomers]  
(  
 @IsActive   bit = null,  
 @Searchkeyword nvarchar(4000)=null,  
    @SortColumn VARCHAR(25)=null,  
    @PageSize INT=null,   
 @PageNumber INT=1  ,
@ApplicationId uniqueidentifier=null
)  
AS  
BEGIN  
  
Declare @searchText nvarchar(max), @IsactiveCondition nvarchar(100)  
  
IF @IsActive is null  
 SET @IsactiveCondition =''  
ELSE  
 SET @IsactiveCondition = ' IsActive = ' + cast(@IsActive as varchar(1)) + ' AND '  
   
IF @SearchKeyword is null  
 SET @SearchKeyword ='%'  
ELSE  
 set @SearchKeyword ='%' + (@Searchkeyword) + '%'  
Set @searchText = '''' + @Searchkeyword + ''''  
  
  
IF @PageSize IS NULL  
 SET @PageSize = 2147483647  
  
IF @SortColumn IS NULL  
 SET @SortColumn = 'LastName ASC'  
  
DECLARE @SQL nvarchar(max),@params nvarchar(100)  
  
SET @params = N'@SIZE INT, @nbr INT,  @Sort VARCHAR(25), @ApplicationId uniqueidentifier'  
  
SET @SQL = N'  
  
;WITH PagingCTE (Row_ID  
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone
   ,ImageId  
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy
   ,County
   ,ExternalId
   ,ExternalProfileId
    )  
  
AS  
  
(  
  
SELECT   
     ROW_NUMBER()   
  
            OVER(ORDER BY ' + @SortColumn + '  
  
--             CASE WHEN @Sort=''LastName DESC'' THEN LastName END DESC,  
--  
--             CASE WHEN @Sort=''LastName ASC''  THEN LastName END ASC,  
--  
--       CASE WHEN @Sort=''FirstName DESC'' THEN FirstName END DESC,  
--  
--             CASE WHEN @Sort=''FirstName ASC''  THEN FirstName END ASC,  
--           
--    CASE WHEN @Sort=''Email DESC'' THEN Email END DESC,  
--  
--             CASE WHEN @Sort=''Email ASC''  THEN Email END ASC,  
--  
--    CASE WHEN @Sort=''AddressLine1 DESC'' THEN AddressLine1 END DESC,  
--  
--             CASE WHEN @Sort=''AddressLine1 ASC''  THEN AddressLine1 END ASC,  
--  
--       CASE WHEN @Sort=''AddressLine2 DESC'' THEN AddressLine2 END DESC,  
--  
--             CASE WHEN @Sort=''AddressLine2 ASC''  THEN AddressLine2 END ASC,  
--           
--    CASE WHEN @Sort=''City DESC'' THEN City END DESC,  
--  
--             CASE WHEN @Sort=''City ASC''  THEN City END ASC,  
--  
--    CASE WHEN @Sort=''Phone DESC'' THEN Phone END DESC,  
--  
--             CASE WHEN @Sort=''PHONE ASC''  THEN Phone END ASC,  
--  
--    CASE WHEN @Sort=''Status DESC'' THEN Status END DESC,  
--  
--             CASE WHEN @Sort=''Status ASC''  THEN Status END ASC  
           
           
            ) AS [Row_ID]  
  
     
   ,UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State 
	,StateCode 
   ,CreatedBy  
   ,CreatedDate  
   ,ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM vwSearchCustomer  
Where SiteId = @ApplicationId AND ' + @IsactiveCondition + '   
 (  	
  (LastName) Like ' + @searchText + '  
  OR (FirstName) Like ' + @searchText + '  
  OR (Email) Like ' + @searchText + '  
  OR (AddressLine1) Like ' + @searchText + '  
  OR (City) Like ' + @searchText + '  
  OR (State) Like ' + @searchText + '  
  OR (Zip) Like ' + @searchText + '  
  OR (HomePhone) Like ' + @searchText + '  
  OR (AccountNumber) Like ' + @searchText + ' 
  OR (CompanyName) Like ' +@searchText + ' 
  OR case when Status =1 then ''active'' else ''deactive'' end Like ' + @searchText + '  
 )  
)   
  
SELECT   
 UserName  
   ,Email   
   ,Id  
   ,FirstName  
   ,LastName  
   ,MiddleName  
   ,CompanyName  
   ,BirthDate  
   ,Gender  
   ,IsBadCustomer  
   ,IsActive  
   ,IsOnMailingList  
   ,IsExpressCustomer  
   ,Status  
   ,CSRSecurityQuestion  
   ,CSRSecurityPassword  
   ,HomePhone  
   ,MobilePhone  
   ,OtherPhone  
   ,ImageId
   ,AccountNumber  
   ,ShippingAddressId  
   ,Name  
   ,AddressType  
   ,AddressLine1  
   ,AddressLine2  
   ,AddressLine3  
   ,City  
   ,StateId  
   ,Zip  
   ,CountryId  
   ,Phone  
   ,AddressId  
   ,NickName  
   ,IsPrimary  
   ,Sequence  
   ,State  
	,StateCode
   ,CreatedBy  
   ,dbo.ConvertTimeFromUtc(CreatedDate,@ApplicationId) CreatedDate  
   ,dbo.ConvertTimeFromUtc(ModifiedDate,@ApplicationId) ModifiedDate  
   ,ModifiedBy   
   ,County
   ,ExternalId
   ,ExternalProfileId
FROM PagingCTE pcte  
WHERE Row_ID >= (@SIZE * @nbr) - (@SIZE -1) AND Row_ID <= @SIZE * @nbr'  
  
 print @SQL  
EXEC sp_executesql   
  
      @SQL,  
  
      @params,  
  
      @SIZE = @PageSize,  
  
      @nbr = @PageNumber,  
  
      @Sort = @SortColumn ,
      @ApplicationId=@ApplicationId 
  
   
Select count(1) [Count]  
 FROM vwSearchCustomer  
Where SiteId = @ApplicationId AND
 (@IsActive is null OR IsActive = @IsActive) 
 --IsActive = isnull(@IsActive, IsActive)    
 AND (  
  (LastName) Like  @SearchKeyword   
  OR (FirstName) Like  @SearchKeyword   
  OR (Email) Like  @SearchKeyword   
  OR (AddressLine1) Like @SearchKeyword   
  OR (City) Like @SearchKeyword   
  OR (State) Like @SearchKeyword  
  OR (Zip) Like  @SearchKeyword   
  OR (HomePhone) Like  @SearchKeyword   
  OR (AccountNumber) Like @SearchKeyword   
   OR (CompanyName) Like @SearchKeyword   
  OR case when Status =1 then 'active' else 'deactive' end Like @SearchKeyword  
 )  
  
 END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_Get]'
GO
/*****************************************************
* Name : Order_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Order_Get]
    (
      @Searchkeyword NVARCHAR(4000) = NULL ,
      @SortColumn VARCHAR(25) = NULL ,
      @PageSize INT = NULL ,
      @PageIndex INT = 1 ,
      @CustomerId UNIQUEIDENTIFIER = NULL ,
      @OrderDate DATETIME = NULL ,
      @StartDate DATETIME = NULL ,
      @EndDate DATETIME = NULL ,
      @OrderStatus XML = NULL ,
      @ExternalShippingStatus INT = NULL ,
      @ApplicationId UNIQUEIDENTIFIER = NULL    
    )
AS 
    BEGIN    
        DECLARE @tblOrderStatus TABLE ( OrderStatus INT )    
        IF @OrderStatus IS NOT NULL 
            BEGIN
                INSERT  @tblOrderStatus
                        ( OrderStatus
                        )
                        SELECT  tab.col.value('text()[1]', 'int')
                        FROM    @OrderStatus.nodes('GenericCollectionOfInt32/int') tab ( col )
            END
    
        IF @SearchKeyword IS NULL 
            SET @SearchKeyword = '%'    
        ELSE 
            SET @SearchKeyword = '%' + ( @Searchkeyword ) + '%'    
    
    
        IF @PageSize IS NULL 
            SET @PageSize = 2147483647    
    
        IF @SortColumn IS NULL 
            SET @SortColumn = 'OrderDate Desc'    
    
        SET @SortColumn = LOWER(@SortColumn)    
    
    	 
        DECLARE @lOrderDate DATETIME
        IF @OrderDate IS NULL 
            BEGIN
                DECLARE @lStartDate DATETIME
                IF @StartDate IS NULL 
                    SET @lStartDate = '01-01-1970'
                ELSE 
                    SET @lStartDate = @StartDate
			
                DECLARE @lEndDate DATETIME
                IF @EndDate IS NULL 
                    SET @lEndDate = '01-01-3000'
                ELSE 
                    SET @lEndDate = @EndDate
            END
        ELSE 
            BEGIN
                SET @lStartDate = @OrderDate
                SET @lEndDate = DATEADD(d, 1, @OrderDate)
            END


        SET @lStartDate = dbo.ConvertTimeToUtc(@lStartDate, @ApplicationId)
        SET @lEndDate = dbo.ConvertTimeToUtc(@lEndDate, @ApplicationId)

		 
        DECLARE @tblOrders TABLE
            (
              Row_ID INT PRIMARY KEY ,
              Id UNIQUEIDENTIFIER ,
              PurchaseOrderNumber NVARCHAR(100) ,
              ExternalOrderNumber NVARCHAR(100) ,
              OrderTypeId INT ,
              OrderDate DATETIME ,
              FirstName NVARCHAR(255) ,
              MiddleName NVARCHAR(255) ,
              LastName NVARCHAR(255) ,
              OrderStatusId INT ,
              Quantity MONEY ,
              NumSkus DECIMAL(38, 2) ,
              OrderTotal MONEY ,
              CODCharges MONEY ,
              TotalDiscount MONEY ,
              RefundTotal MONEY ,
              TaxableOrderTotal MONEY ,
              GrandTotal MONEY ,
              TotalTax MONEY ,
              ShippingTotal MONEY ,
              TotalShippingCharge MONEY ,
              SiteId UNIQUEIDENTIFIER ,
              CustomerId UNIQUEIDENTIFIER ,
              TotalShippingDiscount MONEY ,
              CreatedByFullName NVARCHAR(3074)
            ) ;
            WITH    PagingCTE ( Row_ID, Id, PurchaseOrderNumber, ExternalOrderNumber, OrderTypeId, OrderDate, FirstName, MiddleName, LastName, OrderStatusId, Quantity, NumSkus, OrderTotal, CODCharges, RefundTotal, TaxableOrderTotal, GrandTotal, TotalTax, ShippingTotal, TotalShippingCharge, SiteId, CustomerId, TotalDiscount, TotalShippingDiscount, CreatedByFullName )
                      AS ( SELECT   ROW_NUMBER() OVER ( ORDER BY CASE
                                                              WHEN @SortColumn = 'purchaseordernumber desc'
                                                              THEN PurchaseOrderNumber
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'purchaseordernumber asc'
                                                              OR @SortColumn = 'purchaseordernumber'
                                                              THEN PurchaseOrderNumber
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'ordertypeid desc'
                                                              THEN CAST(OrderTypeId AS VARCHAR(25))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'ordertypeid asc'
                                                              OR @SortColumn = 'ordertypeid'
                                                              THEN CAST(OrderTypeId AS VARCHAR(25))
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'orderdate desc'
                                                              THEN CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'orderdate asc'
                                                              OR @SortColumn = 'orderdate'
                                                              THEN CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'firstname desc'
                                                              OR @SortColumn = 'customer.firstname desc'
                                                              THEN FirstName
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'firstname asc'
                                                              OR @SortColumn = 'firstname'
                                                              OR @SortColumn = 'customer.firstname asc'
                                                              THEN FirstName
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'createdByfullname desc'
                                                              OR @SortColumn = 'customer.createdByfullname desc'
                                                              THEN FirstName
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'createdByfullname asc'
                                                              OR @SortColumn = 'createdByfullname'
                                                              OR @SortColumn = 'customer.createdByfullname asc'
                                                              THEN FirstName
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'orderstatusid desc'
                                                              OR @SortColumn = 'orderstatus desc'
                                                              THEN CAST(OrderStatusId AS INT)
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'orderstatusid asc'
                                                              OR @SortColumn = 'orderstatusid'
                                                              OR @SortColumn = 'orderstatus asc'
                                                              THEN CAST(OrderStatusId AS INT)
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'quantity desc'
                                                              THEN Quantity
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'quantity asc'
                                                              OR @SortColumn = 'quantity'
                                                              THEN Quantity
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'ordertotal desc'
                                                              THEN OrderTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'ordertotal asc'
                                                              OR @SortColumn = 'ordertotal'
                                                              THEN OrderTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totaldiscount desc'
                                                              THEN TotalDiscount
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totaldiscount asc'
                                                              OR @SortColumn = 'totaldiscount'
                                                              THEN TotalDiscount
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totaltax desc'
                                                              THEN TotalTax
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totaltax asc'
                                                              OR @SortColumn = 'totaltax'
                                                              THEN TotalTax
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'taxableordertotal desc'
                                                              THEN TaxableOrderTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'taxableordertotal asc'
                                                              OR @SortColumn = 'taxableordertotal'
                                                              THEN TaxableOrderTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'grandtotal desc'
                                                              THEN GrandTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'grandtotal asc'
                                                              OR @SortColumn = 'grandtotal'
                                                              THEN GrandTotal
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'numskus desc'
                                                              THEN GrandTotal
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'numskus asc'
                                                              OR @SortColumn = 'numskus'
                                                              THEN NumSkus
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totalshippingcharge desc'
                                                              THEN TotalShippingCharge
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totalshippingcharge asc'
                                                              OR @SortColumn = 'totalshippingcharge'
                                                              THEN TotalShippingCharge
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'totalshippingdiscount desc'
                                                              THEN TotalShippingDiscount
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'totalshippingdiscount asc'
                                                              OR @SortColumn = 'totalshippingdiscount'
                                                              THEN TotalShippingDiscount
                                                              END ASC, CASE
                                                              WHEN @SortColumn = 'customerid desc'
                                                              THEN CAST(CustomerId AS CHAR(36))
                                                              END DESC, CASE
                                                              WHEN @SortColumn = 'customerid asc'
                                                              OR @SortColumn = 'customerid'
                                                              THEN CAST(CustomerId AS CHAR(36))
                                                              ELSE CAST(CAST(OrderDate AS DECIMAL(18,
                                                              4)) AS VARCHAR(18))
                                                              END ASC ) AS [Row_ID] ,
                                    Id ,
                                    PurchaseOrderNumber ,
                                    ExternalOrderNumber ,
                                    OrderTypeId ,
                                    OrderDate ,
                                    FirstName ,
                                    MiddleName ,
                                    LastName ,
                                    OrderStatusId ,
                                    Quantity ,
                                    NumSkus ,
                                    OrderTotal ,
                                    CODCharges ,
                                    RefundTotal ,
                                    TaxableOrderTotal ,
                                    GrandTotal ,
                                    TotalTax ,
                                    ShippingTotal ,
                                    TotalShippingCharge ,
                                    SiteId ,
                                    CustomerId ,
                                    TotalDiscount ,
                                    TotalShippingDiscount ,
                                    CreatedByFullName
                           FROM     vwSearchOrder
                           WHERE    ( @CustomerId IS NULL
                                      OR CustomerId = @CustomerId
                                    )
                                    AND ( SiteId = @ApplicationId )
                                    AND OrderDate BETWEEN @lStartDate AND @lEndDate
                                    AND ( @OrderStatus IS NULL
                                          OR OrderStatusId IN ( SELECT
                                                              OrderStatus
                                                              FROM
                                                              @tblOrderStatus )
                                        )
                                    AND ( @Searchkeyword = '%'
                                          OR ( PurchaseOrderNumber ) LIKE @SearchKeyword
                                          OR ( ExternalOrderNumber ) LIKE @SearchKeyword
                                          OR ( FirstName ) LIKE @SearchKeyword
                                          OR ( LastName ) LIKE @SearchKeyword
                                          OR ( MiddleName ) LIKE @SearchKeyword
                                        )
                                    AND ( @ExternalShippingStatus IS NULL
                                          OR ExternalShippingStatus = @ExternalShippingStatus
                                        )
                         )
            INSERT  INTO @tblOrders
                    SELECT  Row_ID ,
                            Id ,
                            PurchaseOrderNumber ,
                            ExternalOrderNumber ,
                            OrderTypeId ,
                            dbo.ConvertTimeFromUtc(OrderDate, @ApplicationId) OrderDate ,
                            FirstName ,
                            MiddleName ,
                            LastName ,
                            OrderStatusId ,
                            Quantity ,
                            NumSkus ,
                            OrderTotal ,
                            CODCharges ,
							TotalDiscount ,
                            RefundTotal ,                            
                            TaxableOrderTotal ,
                            GrandTotal ,
                            TotalTax ,
                            ShippingTotal ,
                            TotalShippingCharge ,
                            SiteId ,
                            CustomerId ,
                            TotalShippingDiscount ,
                            CreatedByFullName
                    FROM    PagingCTE pcte    



        SELECT  *
        FROM    @tblOrders
        WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                AND Row_ID <= @PageSize * @PageIndex    
    
        SELECT  COUNT(*) AS [Count] ,
                ISNULL(SUM(GrandTotal), 0) Total
        FROM    @tblOrders 

        SELECT  OS.[Id] ,
                OS.[OrderId] ,
                OS.[ShippingOptionId] ,
                OS.[OrderShippingAddressId] ,
                dbo.ConvertTimeFromUtc(OS.[ShipOnDate], @ApplicationId) ShipOnDate ,
                dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate], @ApplicationId) ShipDeliveryDate ,
                OS.[TaxPercentage] ,
                OS.[TaxPercentage] ,
                OS.[ShippingTotal] ,
                OS.[Sequence] ,
                OS.[IsManualTotal] ,
                OS.[Greeting] ,
                OS.[Tax] ,
                OS.[SubTotal] ,
                OS.[TotalDiscount] ,
                OS.[ShippingCharge] ,
                OS.[Status] ,
                OS.[CreatedBy] ,
                dbo.ConvertTimeFromUtc(OS.[CreatedDate], @ApplicationId) CreatedDate ,
                OS.[ModifiedBy] ,
                dbo.ConvertTimeFromUtc(OS.[ModifiedDate], @ApplicationId) ModifiedDate ,
                OS.[ShippingDiscount] ,
                OS.ShipmentHash ,
                OS.IsBackOrder ,
                OS.IsHandlingIncluded
        FROM    OROrderShipping OS
                JOIN @tblOrders T ON T.Id = OS.OrderId
        WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                AND Row_ID <= @PageSize * @PageIndex ;
 
 
 
 
        WITH    cteShipmentItemQuantity
                  AS ( SELECT   SUM(SI.Quantity) Quantity ,
                                OI.Id
                       FROM     FFOrderShipmentItem SI
                                INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId = S.Id
                                INNER JOIN OROrderItem OI ON SI.OrderItemId = OI.Id
                                INNER JOIN @tblOrders T ON T.Id = OI.OrderId
                       WHERE    ShipmentStatus = 2
                       GROUP BY OI.Id
                     )
            SELECT  OI.[Id] ,
                    OI.[OrderId] ,
                    OI.[OrderShippingId] ,
                    OI.[OrderItemStatusId] ,
                    OI.[ProductSKUId] ,
                    OI.[Quantity] ,
                    ISNULL(SQ.Quantity, 0) AS ShippedQuantity ,
                    OI.[Price] ,
                    OI.[UnitCost] ,
                    OI.[IsFreeShipping] ,
                    OI.[Sequence] ,
                    dbo.ConvertTimeFromUtc(OI.CreatedDate, @ApplicationId) CreatedDate ,
                    OI.[CreatedBy] ,
                    FN.UserFullName CreatedByFullName ,
                    dbo.ConvertTimeFromUtc(OI.ModifiedDate, @ApplicationId) ModifiedDate ,
                    OI.[ModifiedBy] ,
                    OI.[ParentOrderItemId] ,
                    OI.[HandlingCharge] ,
                    OI.[BundleItemId] ,
                    OI.[CartItemId] ,
                    OI.[Tax] ,
                    OI.[Discount]
            FROM    OROrderItem OI
                    INNER JOIN @tblOrders T ON T.Id = OI.OrderId
                    LEFT JOIN cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
                    LEFT JOIN VW_UserFullName FN ON FN.UserId = OI.CreatedBy
            WHERE   Row_ID >= ( @PageSize * @PageIndex ) - ( @PageSize - 1 )
                    AND Row_ID <= @PageSize * @PageIndex    

 
    
    END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ProductType_GetAttributeValues]'
GO
/*****************************************************
* Name : ProductType_GetAttributeValues
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[ProductType_GetAttributeValues]
(
	@ObjectId uniqueidentifier,
	@AttributeId uniqueidentifier=null,
	@ApplicationId uniqueidentifier=null
)
as
Begin
	select 
		AV.Id as Id,
		PTAV.AttributeId, 
		AV.Code,
		IsNull(AV.[IsDefault], 0) as [IsDefault],
		AV.Sequence,
		AV.NumericValue,
		AV.[Value], 		
		dbo.ConvertTimeFromUtc(AV.CreatedDate,@ApplicationId)reatedDate, 
		AV.CreatedBy, 
		dbo.ConvertTimeFromUtc(AV.ModifiedDate,@ApplicationId)ModifiedDate, 
		AV.ModifiedBy,
		AV.Status,
		PTAV.AttributeValueId as AttributeEnumId
	from PRProductTypeAttributeValue PTAV 
				Left join ATAttributeEnum AV on AV.Id = PTAV.AttributeValueId	
				join PRProductTypeAttribute PTA on PTAV.ProductTypeAttributeId=PTA.Id
	where PTA.ProductTypeId =  @ObjectId  and 
			--PTAV.AttributeId = isnull(@AttributeId,PTAV.AttributeId)
			(@AttributeId is null or PTAV.AttributeId = @AttributeId)
				and AV.Status = dbo.GetActiveStatus()
	Order By AV.Sequence
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Post_Save]'
GO
/*****************************************************
* Name : Post_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Post_Save]   
(  
	@Id					uniqueidentifier=null OUT ,  
	@BlogId				uniqueidentifier,   
	@ApplicationId		uniqueidentifier,   
	@Title				nvarchar(256)=null,  
	@Description		ntext =null,  
	@ShortDescription	nvarchar(2000)=null,  
	@AuthorName			nvarchar(256)=null,  
	@AuthorEmail		nvarchar(256)=null,  
	@AllowComments		bit =true,  
	@PostDate			datetime=null,  
	@Location			nvarchar(256)=null,  
	@Labels				nvarchar(Max)=null,  
	@ModifiedBy			uniqueidentifier,  
	@IsSticky			bit=null, 
	@HSId				uniqueidentifier,  
	@FriendlyName		nvarchar(256)=null,
	@PostContentId		uniqueidentifier = null,
	@ContentId			uniqueidentifier = null,
	@Imageurl			nvarchar(500) = null,
	@TitleTag			nvarchar(2000)=null,  
	@H1Tag				nvarchar(2000)=null,
	@KeywordMetaData	nvarchar(2000)=null,
	@DescriptiveMetaData  nvarchar(2000)=null,
	@OtherMetaData		nvarchar(2000)=null,
	@PublishDate		datetime = null,
	@PublishCount		int = null,
	@WorkflowState		int = null
)  
AS  
BEGIN  
	DECLARE  
	 @NewId  uniqueidentifier,  
	 @RowCount  INT,  
	 @Stmt   VARCHAR(25),  
	 @Now    datetime,  
	 @Error   int ,  
	 @MonthId uniqueidentifier,  
	 @return_value int ,
	 @Status int 
  
	 /* if ID specified, ensure exists */  
	 IF (@Id IS NOT NULL)  
	  IF (NOT EXISTS(SELECT 1 FROM   BLPost WHERE  Id = @Id AND Status <> dbo.GetDeleteStatus()))  
	  BEGIN  
	   set @Stmt = convert(varchar(36),@Id)  
	   RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1,@Stmt )  
	   RETURN dbo.GetBusinessRuleErrorCode()  
	  END    

	  IF @Status = 5
		SET @IsSticky = 1
	   
	 SET @Now = getutcdate()  
	  
	 IF (@Id IS NULL)   -- new insert  
	 BEGIN  
	  SET @Stmt = 'Create Post'  
	  SELECT @NewId =newid()  
	  
	  INSERT INTO BLPost  WITH (ROWLOCK)(  
		 Id,  
		ApplicationId,  
		Title,  
		BlogId,  
		Description,  
		ShortDescription,  
		AuthorName,  
		AuthorEmail,  
		Labels,  
		AllowComments,  
		PostDate,  
		Location,  
		Status,  
		IsSticky,
		CreatedBy,  
		CreatedDate,  
		HSId,  
		FriendlyName,
		PostContentId,
		ContentId,
		ImageUrl,
		TitleTag,
		H1Tag,
		KeywordMetaData,
		DescriptiveMetaData,
		OtherMetaData
	  )  
	  VALUES  
	   (  
			@NewId,  
			@ApplicationId,   
			@Title,  
			@BlogId,  
			@Description,  
			@ShortDescription,  
			@AuthorName,  
			@AuthorEmail,  
			@Labels,  
			@AllowComments,  
			@PostDate,  
			@Location,  
			1, 
			@IsSticky, 
			@ModifiedBy,  
			@Now,
			@HSId ,
			@FriendlyName,
			@PostContentId,
			@ContentId,
			@ImageUrl,
			@TitleTag,
			@H1Tag,
			@KeywordMetaData,
			@DescriptiveMetaData,
			@OtherMetaData
	   )  
	  SELECT @Error = @@ERROR  
	   IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()   
	  END  
	  
	  SELECT @Id = @NewId  
	 END  
	 ELSE   -- Update  
	 BEGIN  
	  SET @Stmt = 'Modify Post'  
	  UPDATE BLPost WITH (ROWLOCK)  
	   SET   
		 Id     = @Id,  
		ApplicationId  = @ApplicationId,  
		Title    = @Title,  
		BlogId    = @BlogId,  
		Description   = @Description,  
		ShortDescription = @ShortDescription,  
		AuthorName   = @AuthorName,  
		AuthorEmail   = @AuthorEmail,  
		Labels    = @Labels,  
		AllowComments  = @AllowComments,  
		PostDate   = @PostDate,  
		Location   = @Location ,  
		ModifiedBy   = @ModifiedBy,  
		ModifiedDate  = GETUTCDATE(),  
		HSId = @HSId,  
		FriendlyName=@FriendlyName,
		PostContentId = @PostContentId,
		ContentId = @ContentId,
		ImageUrl = @Imageurl,
		TitleTag = @TitleTag,
		H1Tag = @H1Tag,
		IsSticky = @IsSticky,
		KeywordMetaData = @KeywordMetaData,
		DescriptiveMetaData = @DescriptiveMetaData,
		OtherMetaData = @OtherMetaData,
		PublishDate = @PublishDate,
		PublishCount = @PublishCount,
		WorkflowState = @WorkflowState
	  WHERE Id = @Id  
	    
	  SELECT @Error = @@ERROR, @RowCount = @@ROWCOUNT  
	  
	  IF @Error <> 0  
	  BEGIN  
	   RAISERROR('DBERROR||%s',16,1,@Stmt)  
	   RETURN dbo.GetDataBaseErrorCode()  
	  END  
	  
	  IF @RowCount = 0  
	  BEGIN  
	   --concurrency error  
	   RAISERROR('DBCONCURRENCY',16,1)  
	   RETURN dbo.GetDataConcurrencyErrorCode()  
	  END   
	 END    
   
	EXEC PageMapBase_UpdateLastModification @ApplicationId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[PageDefinition_GetContainerXml]'
GO
/*****************************************************
* Name : PageDefinition_GetContainerXml
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


ALTER PROCEDURE [dbo].[PageDefinition_GetContainerXml](
	@PageDefinitionSegmentId uniqueidentifier
)
AS
BEGIN
	 SELECT 
	  pdfc.[PageDefinitionId]
	  ,pdfc.[ContainerId]
	  ,pdfc.[ContentId]
	  ,pdfc.[InWFContentId]
	  ,pdfc.[ContentTypeId]
	  ,pdfc.[IsModified]
	  ,pdfc.[IsTracked]
	  ,pdfc.[Visible]
	  ,pdfc.[InWFVisible]
	  ,pdfc.[DisplayOrder]
	  ,pdfc.[InWFDisplayOrder]
	  ,pdfc.[CustomAttributes]
	  ,pdfc.[PageDefinitionSegmentId]
	  ,pdfc.ContainerName
	FROM	
		PageDefinitionContainer pdfc WITH(NOLOCK)
	WHERE	
		pdfc.PageDefinitionSegmentId= @PageDefinitionSegmentId
		
		
	SELECT UnmanagedXml from PageDefinitionSegment WITH(NOLOCK)
		WHERE Id = @PageDefinitionSegmentId
		
End


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Taxonomy_GetTaxonomyByObjects]'
GO
/*****************************************************
* Name : Taxonomy_GetTaxonomyByObjects
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Taxonomy_GetTaxonomyByObjects](  
@ObjectIds varchar(max)  
)  
as  
Begin  
  
  select T.ObjectId as ObjectId,
  C.Id,  
  C.ParentId,
  C.ApplicationId,  
  C.Title,  
  C.Description,  
  C.CreatedDate,  
  C.CreatedBy,  
  C.ModifiedBy,  
  C.ModifiedDate,  
  C.Status,  
  FN.UserFullName CreatedByFullName,
  MN.UserFullName ModifiedByFullName
  from COTaxonomy C 
  Inner Join COTaxonomyObject T ON C.Id = T.TaxonomyId
  LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	Where  C.Status =1 AND T.ObjectId in (SELECT * FROM dbo.SplitGUID(@ObjectIds,','))
  
End

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Payment_ByGiftCard_Save]'
GO
/*****************************************************
* Name : Payment_ByGiftCard_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Payment_ByGiftCard_Save]  
(   
 @Id uniqueidentifier output,
 @GiftCardId uniqueidentifier output,	
 @GiftCardNumber nvarchar(50),
 @Last4Digits nvarchar(50),
 @BalanceAfterLastTransaction money,
 @OrderId uniqueidentifier,
 @PaymentTypeId int,
 @PaymentStatusId int =1,
 @Amount money,
 @CustomerId uniqueidentifier,
 @Description nvarchar(255),
 @ModifiedBy uniqueidentifier  ,
 @BillingAddressId uniqueidentifier,
 @OrderPaymentId uniqueidentifier output,
 @IsRefund tinyint = 0
)  
  
AS  
BEGIN       
  
DECLARE @CreatedDate datetime
  set @CreatedDate = GetUTCDate()

Declare @error	  int
Declare @stmt	  varchar(256)	
Declare @sequence int
 if(@Id is null OR @Id = dbo.GetEmptyGUID())  
 begin  
			  SET @Id = newid()  

				Insert into PTPayment 
				(Id
				,PaymentTypeId
				,PaymentStatusId
				,CustomerId
				,Amount
				,Description
				,CreatedDate
				,CreatedBy
				,Status
				,IsRefund
				)
				Values
				(
				@Id
				,@PaymentTypeId
				,@PaymentStatusId
				,@CustomerId
				,@Amount
				,@Description
				,@CreatedDate
				,@ModifiedBy
				,dbo.GetActiveStatus()
				,@IsRefund
			)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

		IF (@GiftCardId is null or @GiftCardId =dbo.GetEmptyGUID())
		BEGIN
	IF Exists (Select AddressId from OROrderShippingAddress Where Id=@BillingAddressId)
	Select @BillingAddressId =AddressId from OROrderShippingAddress Where Id=@BillingAddressId
			Select @GiftCardId =newid()
			Insert into PTGiftCardInfo
						(Id
						 ,CustomerId
						 ,GiftCardNumber
						 ,Last4Digits
						 ,BalanceAfterLastTransaction
						 ,BillingAddressId
						 ,Status
						 ,CreatedDate
						 ,CreatedBy
						 ,ModifiedDate
						 ,ModifiedBy
						)
				Values
				(@GiftCardId
				,@CustomerId
				,@GiftCardNumber
				,@Last4Digits
				,@BalanceAfterLastTransaction
				,@BillingAddressId
				,1
				,@CreatedDate
				,@ModifiedBy
				,@CreatedDate
				,@ModifiedBy
				)

	
		END

				Insert into PTPaymentGiftCard
				(Id,
				 PaymentId,
				 GiftCardId
				)
				Values
				(newId(),
				  @Id,
				  @GiftCardId
				)

					select @error = @@error
							if @error <> 0
							begin
								raiserror('DBERROR||%s',16,1,@stmt)
								return dbo.GetDataBaseErrorCode()	
							end

				

				Select @sequence = max(isnull(Sequence,0)) + 1
				From PTOrderPayment
				Where OrderId=@OrderId


				SET @OrderPaymentId  = newId()
				Insert Into PTOrderPayment(Id,OrderId,PaymentId,Amount,Sequence)
				Values(@OrderPaymentId,@OrderId,@Id,@Amount,@sequence)

				select @error = @@error
				if @error <> 0
				begin
					raiserror('DBERROR||%s',16,1,@stmt)
					return dbo.GetDataBaseErrorCode()	
				end

	end  
ELSE
	BEGIN
		set @CreatedDate = GetUTCDate()

		UPDATE PTPayment SET 
		PaymentTypeId = @PaymentTypeId, 
		PaymentStatusId = @PaymentStatusId,
		CustomerId = @CustomerId,
		Amount = @Amount,
		Description = @Description,
		ModifiedBy = @ModifiedBy,
		ModifiedDate = @CreatedDate,	
		Status = dbo.GetActiveStatus(),
		IsRefund = @IsRefund
		WHERE Id = @Id


		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		end


		UPDATE PTOrderPayment SET Amount = @Amount
		WHERE Id= @OrderPaymentId
		
		select @error = @@error
		if @error <> 0
		begin
			raiserror('DBERROR||%s',16,1,@stmt)
			return dbo.GetDataBaseErrorCode()	
		END
		
		Update PTGiftCardInfo
		Set @BalanceAfterLastTransaction=BalanceAfterLastTransaction
		Where Id=@GiftCardId
END  

Update OROrder Set PaymentTotal =dbo.Order_GetPaymentTotal(@OrderId), RefundTotal = dbo.Order_GetRefundTotal(@OrderId)
Where Id=@OrderId

  
  
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflowsWithSequence]'
GO
/*****************************************************
* Name : Workflow_GetWorkflowsWithSequence
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflows with its sequences based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflowsWithSequence 
--          @WorkflowId = 'uniqueidentifier'  null,
--          @ObjectTypeId = 'int'  null,
--			@ApplicationId	= uniqueidentifier null ,
--          @PageIndex = 'int' or 0,
--          @PageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflowsWithSequence](
--********************************************************************************
-- Parameters
--********************************************************************************
			@WorkflowId		uniqueidentifier = null,
			@ObjectTypeId	int = null,
			@ApplicationId	uniqueidentifier = null,					
			@ProductId		uniqueidentifier = null,
			@PageIndex		int = null,
			@PageSize		int = null,
			@IsGlobal		bit = null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint --,
			--@TotalRecords			bigint
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
		SET @PageLowerBound = Isnull(@PageSize,0) * Isnull(@PageIndex,0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1;
	
		WITH ResultEntries AS
		(
            SELECT 
					ROW_NUMBER() OVER (ORDER BY A.Id) AS RowNumber,
					A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.ApplicationId,
					A.IsGlobal,
					A.IsShared,
					FN.UserFullName CreatedByFullName,
					MN.UserFullName ModifiedByFullName
            FROM	dbo.WFWorkflow A
            LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
            WHERE	A.[Id]			=	Isnull(@WorkflowId,A.[Id])			AND 
					A.ObjectTypeId	=	Isnull(@ObjectTypeId,ObjectTypeId)	AND
					(@ApplicationId IS NULL OR (A.ApplicationId = @ApplicationId OR (A.IsShared = 1 AND A.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, 1, 1))))) AND
					A.ProductId		=	Isnull(@ProductId, ProductId)	AND
					A.Status = dbo.GetActiveStatus()	AND
					A.IsGlobal = Isnull(@IsGlobal, A.IsGlobal)
		)

		SELECT 		A.Id, 
					A.Title,  
					A.Description,
					A.ObjectTypeId,
					A.CreatedDate,
					A.CreatedBy,
					A.ModifiedDate,
					A.ModifiedBy,
					A.ArchivedDate,
					A.RootId,
					A.Status,
					A.SkipAfterDays,
					A.IsGlobal,
					A.ApplicationId,
					A.IsShared,
					CreatedByFullName,
					ModifiedByFullName,
					B.Id As SequenceId, 
					B.RoleId,
					B.ActorId,
					B.ActorType,
					B.[Order],
					dbo.User_GetMemberName(B.ActorId,B.ActorType) As ActorName ,
					dbo.User_GetRoleName(B.RoleId) As RoleName
		FROM	ResultEntries	A,
				dbo.WFSequence	B
		WHERE	( ((@PageIndex is null) OR (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)) 
				) AND 
				( A.[Id] = B.WorkflowId
				)
		ORDER BY A.[Id], B.[Order] 

--		SELECT  @TotalRecords = COUNT(*)
--		FROM    ResultEntries

		--RETURN @TotalRecords
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[CustomerPaymentInfo_Save]'
GO
/*****************************************************
* Name : CustomerPaymentInfo_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[CustomerPaymentInfo_Save]
(
	@Id					uniqueidentifier=null OUT ,
	@Title				nvarchar(256)=null,
	@Description       	nvarchar(1024)=null,
	@ModifiedBy       	uniqueidentifier,
	@ModifiedDate    	datetime=null,
	@Status				int=null,
	@CardType			uniqueidentifier,
    @Number				varchar(max),
	@ExpirationMonth	int,
    @ExpirationYear		int,
    @NameOnCard			varchar(255),
    @BillingAddressId	uniqueidentifier,
    @Sequence			int,
    @UserId				uniqueidentifier,
    @IsPrimary			bit,
	@ApplicationId		uniqueidentifier,
	@ExternalProfileId	nvarchar(255)=null,
	@Nickname			nvarchar(255)=null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @reccount int, 
		@error	  int,
		@Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int,
		@PaymentInfoId uniqueidentifier

Begin
--********************************************************************************
-- code
--********************************************************************************


	IF (@Status=dbo.GetDeleteStatus())
	BEGIN
		RAISERROR('INVALID||Status', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
    END

	IF (@Status is null OR @Status =0)
	BEGIN
		SET @Status = dbo.GetActiveStatus()
	END

/* if @Id specified, ensure exists */
   if (@Id is not null and @Id != dbo.GetEmptyGUID())
       if((select count(*) from  USUserPaymentInfo where  Id = @Id AND Status != dbo.GetDeleteStatus()) = 0)
       begin
			raiserror('NOTEXISTS||Id', 16, 1)
			return dbo.GetBusinessRuleErrorCode()
       end
       
    IF @IsPrimary = 1
	BEGIN
		UPDATE [dbo].[USUserPaymentInfo]
		SET IsPrimary = 0 WHERE UserId = @UserId
	END
	
	SET @Now = GetUTCDate()
	
	IF @ModifiedDate is null
		SET @ModifiedDate =@Now
	Else
		Set @ModifiedDate = dbo.ConvertTimeToUtc(@ModifiedDate,@ApplicationId)
		-- open the symmetric key 
		OPEN MASTER KEY DECRYPTION BY PASSWORD = 'P@ssw0rd' 
		OPEN SYMMETRIC KEY [key_DataShare] 
			DECRYPTION BY CERTIFICATE cert_keyProtection;

		IF (@Id is null)			-- new insert
		BEGIN
			SET @stmt = 'Payment Insert'
			Declare @pId uniqueidentifier
			SET @pId =NewId();

			INSERT INTO [dbo].[PTPaymentInfo]
			   ([Id]
			   ,[CardType]
			   ,[Number]
			   ,[ExpirationMonth]
			   ,[ExpirationYear]
			   ,[NameOnCard]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[Status]
			   ,[ModifiedBy]
			   ,[ModifiedDate])
		 VALUES
			   (@pId
			   ,@CardType
			   ,EncryptByKey(Key_Guid('key_DataShare'), @Number)
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS Varchar(2)))
			   ,EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS varchar(4)))
			   ,@NameOnCard
			   ,@ModifiedBy
			   ,@ModifiedDate
			   ,@Status
			   ,@ModifiedBy
			   ,@ModifiedDate)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end

			SET @Id =NewId();
			
			INSERT INTO [dbo].[USUserPaymentInfo]
			([Id]
			,[UserId]
			,[PaymentInfoId]
			,[BillingAddressId]
			,[Sequence]
			,[IsPrimary]
			,[CreatedBy]
			,[CreatedDate]
			,[Status]
			,[ModifiedDate]
			,[ModifiedBy]
			,[ExternalProfileId]
			,[Nickname])
			VALUES
			(@Id
			,@UserId
			,@pId
			,@BillingAddressId
			,@Sequence
			,@IsPrimary
			,@ModifiedBy
			,@ModifiedDate
			,@Status
			,@ModifiedDate
			,@ModifiedBy
			,@ExternalProfileId
			,@Nickname)

			select @error = @@error
			if @error <> 0
			begin
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				raiserror('DBERROR||%s',16,1,@stmt)
				return dbo.GetDataBaseErrorCode()	
			end
			
        END
        ELSE			-- update
        BEGIN
			SET @stmt = 'Payment Update'
			
			Select @PaymentInfoId=PaymentInfoId  FROM [dbo].[USUserPaymentInfo]
			Where Id=@Id

			UPDATE [dbo].[USUserPaymentInfo] WITH (ROWLOCK)
			SET [PaymentInfoId] = @PaymentInfoId
			  ,[BillingAddressId] = @BillingAddressId
			  ,[Sequence] = @Sequence
			  ,[IsPrimary] = @IsPrimary
			  ,[Status] = @Status
			  ,[ModifiedDate] = @ModifiedDate
			  ,[ModifiedBy] =@ModifiedBy
			  ,[ExternalProfileId]=@ExternalProfileId
			  ,[Nickname]=@Nickname
			WHERE Id=@Id
			AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)

			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
			  RAISERROR('DBERROR||%s',16,1,@stmt)
			  RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
			SET @rowcount= 0	

			UPDATE [dbo].[PTPaymentInfo]
			SET [CardType] = @CardType
				,[Number]=CASE WHEN (@Number is null or len(@Number)<=0) then Number else (EncryptByKey(Key_Guid('key_DataShare'), @Number )) end
				,[ExpirationMonth] = CASE WHEN (@ExpirationMonth is null or len(@ExpirationMonth)<=0) then ExpirationMonth else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationMonth AS VARCHAR(2)) )) end
				,[ExpirationYear]= CASE WHEN (@ExpirationYear is null or len(@ExpirationMonth)<=0) then ExpirationYear else (EncryptByKey(Key_Guid('key_DataShare'), CAST(@ExpirationYear AS VARCHAR(4)) )) end
				,[NameOnCard] = @NameOnCard
				,[Status] = @Status
				,[ModifiedBy] = @ModifiedBy
				,[ModifiedDate] = @ModifiedDate
			WHERE Id=@PaymentInfoId 
				--AND isnull(ModifiedDate,@Now)= isnull(@ModifiedDate,@Now)
					
			SELECT @error = @@error, @rowcount = @@rowcount
			IF @error <> 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
				RAISERROR('DBERROR||%s',16,1,@stmt)
				RETURN dbo.GetDataBaseErrorCode()
			END

			IF @rowcount = 0
			BEGIN
				-- close the symmetric key
				CLOSE SYMMETRIC KEY [key_DataShare];
				CLOSE MASTER KEY
		
				RAISERROR('CONCURRENCYERROR',16,1)
				RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
			END	
		END


	-- close the symmetric key
	CLOSE SYMMETRIC KEY [key_DataShare];
	CLOSE MASTER KEY
	

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Site_CreateMicroSiteData]'
GO
/*****************************************************
* Name : Site_CreateMicroSiteData
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER procedure [dbo].[Site_CreateMicroSiteData]
(
	@ProductId uniqueidentifier,
	@MasterSiteId uniqueidentifier,
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier = null,
	@ImportAllAdminUserAndPermission bit ,
	@ImportAllWebSiteUser bit,
	@PageImportOptions int =2,
	@ImportContentStructure bit =0,
	@ImportFileStructure bit =0,
	@ImportImageStructure bit =0,
	@ImportFormsStructure bit =0
)
AS
BEGIN
	
	--CMSFrontUSer
	IF(@ImportAllWebSiteUser = 1)
	BEGIN			
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId,
			SU.UserId,
			SU.ISSystemUser,
			SU.ProductId
		FROM USUser U 
			INNER JOIN USSiteUser SU on U.Id = SU.UserId  
			INNER JOIN USMembership M on SU.UserId = M.UserId  
		WHERE SU.SiteId = @MasterSiteId AND SU.ProductId = @ProductId  
			AND SU.IsSystemUser = 0 AND  U.Status = 1 
			AND M.IsApproved = 1 AND M.IsLockedOut= 0 AND u.ExpiryDate >= GETUTCDATE()
	END
	
-- This will not insert any record as we have not inserted any record in USSiteuser table
	INSERT INTO USMemberGroup(MemberId, MemberType, GroupId, ApplicationId) 
	SELECT SU.UserId, 1, MG.GroupId, @MicroSiteId  
		FROM USSiteUser SU 
			INNER JOIN USMemberGroup MG on SU.UserId = MG.MemberId 
	WHERE SU.SiteId = @MicroSiteId 
		AND MG.ApplicationId = @MasterSiteId --and MG.MemberType=1
	
	-- This is not required now, as it will inherit permision from parent site
	--****Admin USer****
	IF (@ImportAllAdminUserAndPermission=0)
	BEGIN
		-- Giving permission to Default users. Allow only CMS , Analyzer and Social product default users
		INSERT INTO [dbo].[USSiteUser]
		   ([SiteId]
		   ,[UserId]
		   ,[IsSystemUser]
		   ,[ProductId])
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId] 
		FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MasterSiteId AND ProductId in ('CBB702AC-C8F1-4C35-B2DC-839C26E39848','CAB9FF78-9440-46C3-A960-27F53D743D89','CED1F2B6-A3FF-4B82-A856-1583FA811906')
		Except
		SELECT @MicroSiteId As SiteId,[UserId],[IsSystemUser],[ProductId]  FROM USSiteUser WHERE UserId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				SiteId = @MicroSiteId 	
			
		INSERT INTO [dbo].[USMemberGroup]
		   ([ApplicationId]
		   ,[MemberId]
		   ,[MemberType]
		   ,[GroupId])   
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MasterSiteId
				AND GroupId IN (SELECT Id FROM USGroup WHERE ApplicationId = ProductId)
		Except
		SELECT @MicroSiteId As ApplicationId, MemberId, MemberType, GroupId
		FROM USMemberGroup  WHERE MemberId in (
			SELECT Id FROM USUser Where UserName IN ('iappsuser', 'iappssystemuser', 'insadmin', 'siadmin')
				) AND 
				ApplicationId = @MicroSiteId				
	END

	EXEC [dbo].[Site_CopyMenusAndPages] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,@PageImportOptions
	--content structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,7,@ImportContentStructure
	--File structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,9,@ImportFileStructure
	--Image structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,33,@ImportImageStructure
	--Forms structure
	EXEC [dbo].[Site_CopyDirectories] @MasterSiteId ,@MicroSiteId ,@ModifiedBy ,@ImportAllAdminUserAndPermission,38,@ImportFormsStructure
	
	-- Changing the status of the site		
	UPDATE SISite SET Status = 1 WHERE Id = @MicroSiteId		
	
	EXEC [Site_CreateTemplateBuilderData] @MicroSiteId

	-- This might be not required now
	--EXEC [CMSSite_InsertDefaultPermission] @MicroSiteId	
		
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[RssChannel_Save]'
GO
/*****************************************************
* Name : RssChannel_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[RssChannel_Save](

--********************************************************************************
-- Parameters
--********************************************************************************
	@Id				uniqueidentifier OUTPUT,
	@ApplicationId		uniqueidentifier,
	@Title			nvarchar(256),
	@Description	nvarchar(1024),
	@Link			nvarchar(256),
	@Copyright		nvarchar(256)= null,
	@Docs			nvarchar(256)= null,
	@Generator		nvarchar(256)= null,
	@Language       nvarchar(256)= null,
	@ManagingEditor nvarchar(256)= null,
	@PubDate    	datetime = null,
	@Rating    		nvarchar(256)= null,
	@SkipDays    	nvarchar(256)= null,
	@SkipHours    	nvarchar(256)= null,
	@TimeToLive    	int,
	@WebMaster    	nvarchar(256)= null,
	@CreatedBy    	uniqueidentifier,
	@ModifiedDate   datetime = null,
	@ModifiedBy    	uniqueidentifier = null,
	@RssImageId    	uniqueidentifier = null,
	@RssCloudId    	uniqueidentifier = null,
	@ChannelType	int=null
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************

DECLARE

	@NewGuid		uniqueidentifier,
	@CurrentUTCDate	datetime,
	@Rowcount 		int,
	@Error 			int,
	@Stmt			varchar(256)	

BEGIN
	--if Id is specified, ensure exists
	IF (@Id IS NOT NULL)
		IF (NOT EXISTS(SELECT 1 FROM SYRssChannel WHERE  Id = @Id AND Status != dbo.GetDeleteStatus()))
		BEGIN
			SET @Stmt = CONVERT(VARCHAR(36),@Id)
			RAISERROR ( 'NOTEXISTS||Id|%s' , 16, 1, @Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END
			
	SET @Rowcount = 0
	SET @CurrentUTCDate = GETUTCDATE()
	SET @Error = 0

	--if Id is null insert a new RssChannel in SYRssChannel table
	IF (@Id IS NULL)	
	BEGIN
		SET @Stmt = 'inserting RssChannel'
		SET @NewGuid = newid();
		INSERT INTO SYRssChannel(
			[Id],
			ApplicationId,
			Title,
			[Description],
			Link,
			Copyright,
			Docs,
			Generator,
			[Language],
			ManagingEditor,
			PubDate,
			Rating,
			SkipDays,
			SkipHours,
			TimeToLive,
			WebMaster,
			CreatedDate,
			CreatedBy,
			Status,
			RssImageId,
			RssCloudId,
			ChannelType)
		VALUES (
			@NewGuid,
			@ApplicationId,
			@Title,
			@Description,
			@Link,
			@Copyright,
			@Docs,
			@Generator,
			@Language,
			@ManagingEditor,
			@PubDate,
			@Rating,
			@SkipDays,
			@SkipHours,
			@TimeToLive,
			@WebMaster,
			@CurrentUTCDate,
			@CreatedBy,
			dbo.GetActiveStatus(),
			@RssImageId,
			@RssCloudId,
			@ChannelType)

		SET @Error = @@ERROR
	 	
		IF @Error <> 0
		BEGIN
			--Database error
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()			
		END	

		SET @Id = @NewGuid;

	END 

	--if Id is not null, update the RssChannel in SYRssChannel table
	ELSE	
	BEGIN
		
		SET @Stmt = 'updating RssChannel'
		UPDATE	SYRssChannel WITH (ROWLOCK)
		SET ApplicationId = @ApplicationId,
			Title = @Title ,
			[Description] = @Description,
			Link = @Link,
			Copyright = @Copyright,
			Docs = @Docs,
			Generator = @Generator,
			[Language] = @Language,
			ManagingEditor = @ManagingEditor,
			PubDate = @PubDate,
			Rating = @Rating,
			SkipDays = @SkipDays,
			SkipHours = @SkipHours,
			TimeToLive = @TimeToLive,
			WebMaster = @WebMaster,
			ModifiedDate = @CurrentUTCDate,
			ModifiedBy = @ModifiedBy,
			Status = dbo.GetActiveStatus(),
			RssImageId = @RssImageId,
			RssCloudId = @RssCloudId
		WHERE 	[Id] = @Id 
		AND		ISNULL(ModifiedDate,@CurrentUTCDate)= ISNULL(@ModifiedDate,@CurrentUTCDate)

		SELECT @Error = @@ERROR, @Rowcount = @@ROWCOUNT

		IF @Error <> 0
		BEGIN
			--Database error
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()			
		END	

		IF @Rowcount = 0
		BEGIN
			--Concurrency error
			RAISERROR('DBCONCURRENCY',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()		
		END	
	END
		
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Membership_FindUsersByEmail]'
GO
/*****************************************************
* Name : Membership_FindUsersByEmail
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


--exec Membership_FindUsersByEmail 'CDD38906-6071-4DD8-9FB6-508C38848C61','rmaheswari@bridgelinesw.com',1,1,10
--exec Membership_FindUsersByEmail '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4','rmaheswari@bridgelinesw.com',1,1,10

ALTER PROCEDURE [dbo].[Membership_FindUsersByEmail]
--********************************************************************************
-- PARAMETERS

	@ApplicationId			uniqueidentifier,
    @EmailToMatch			nvarchar(256),
	@IsSystemUser			bit = 0,	
    @PageIndex				int,
    @PageSize				int
--********************************************************************************
AS
BEGIN
    
--********************************************************************************
-- Variable declaration
--********************************************************************************

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
	IF (@PageIndex > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	ELSE
		SET @PageUpperBound = 0
    
	DECLARE @CurrentTimeUtc datetime
	SET @CurrentTimeUtc	= GetUtcDate();

	WITH ResultEntries AS
	(
			SELECT 
				ROW_NUMBER() OVER (ORDER BY m.LoweredEmail) AS RowNumber,
				u.Id, 
				u.UserName,  
				m.Email,
				m.PasswordQuestion, 
				m.IsApproved,
				u.CreatedDate,
				m.LastLoginDate,
				u.LastActivityDate,
				m.LastPasswordChangedDate,
				m.IsLockedOut,
				m.LastLockoutDate
			FROM   dbo.USUser u, dbo.USMembership m, dbo.USSiteUser s
			WHERE  s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND 
				m.UserId = u.Id AND 
				s.UserId = m.UserId AND
				s.IsSystemUser = @IsSystemUser	AND 
				u.Status <> dbo.GetDeleteStatus() AND	
				(
					(@EmailToMatch IS NULL AND m.Email IS NULL)
						OR
					(@EmailToMatch IS NOT NULL	AND	m.LoweredEmail LIKE LOWER(@EmailToMatch))
				)	AND
				(u.ExpiryDate is null OR u.ExpiryDate >= getutcdate())
				
	)
	
	SELECT  Id, 
			UserName,  
			Email,
			PasswordQuestion, 
			IsApproved,
			CreatedDate,
			LastLoginDate,
			LastActivityDate,
			LastPasswordChangedDate,
			IsLockedOut,
			LastLockoutDate 
	FROM ResultEntries 
	WHERE (@PageIndex = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound)
    
	RETURN 0
END
--***********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Workflow_GetWorkflows_Count]'
GO
/*****************************************************
* Name : Workflow_GetWorkflows_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Digital, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Digital, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of workflow based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 24-11-2006
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Workflow_GetWorkflows_Count 
--          @WorkflowId = 'uniqueidentifier'  null,
--          @ObjectTypeId = 'int'  null,
--			@ApplicationId	= uniqueidentifier null 
--********************************************************************************
ALTER PROCEDURE [dbo].[Workflow_GetWorkflows_Count](
--********************************************************************************
-- Parameters
--********************************************************************************
			@WorkflowId		uniqueidentifier = null,
			@ObjectTypeId	int = null,
			@ApplicationId	uniqueidentifier = null,
			@ProductId		uniqueidentifier = null,
			@Title			nvarchar(512) = null	
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
        SELECT 
			Count(*)
        FROM	dbo.WFWorkflow A
        WHERE	A.[Id]			=	Isnull(@WorkflowId,[Id])			AND 
				A.ObjectTypeId	=	Isnull(@ObjectTypeId,ObjectTypeId)	AND
				(@ApplicationId IS NULL OR (A.ApplicationId = @ApplicationId OR A.IsShared = 1 AND A.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSitesForSystemUser(@ApplicationId, 1, 0)))) AND
				A.ProductId		=	Isnull(@ProductId, ProductId)		AND
				A.Title			=	Isnull(@Title, Title)			AND
				A.Status = dbo.GetActiveStatus()
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_GetOrdersByXmlGuids]'
GO
/*****************************************************
* Name : Order_GetOrdersByXmlGuids
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Order_GetOrdersByXmlGuids](@Ids XML,@ApplicationId uniqueidentifier=null)    
AS    
BEGIN
Declare @OrderIds table(Id uniqueidentifier)
INSERT INTO @OrderIds
Select tab.col.value('text()[1]','uniqueidentifier') 
from  @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col)

IF (select count(*) from @OrderIds) =0
	begin
		raiserror('DBERROR||%s',16,1,'OrderId is required')
		return dbo.GetDataBaseErrorCode()	
	end

SELECT     
 O.[Id],  
 O.[Id]  OrderId,  
 O.[CartId],    
 O.[OrderTypeId],    
 O.[OrderStatusId],    
 O.[PurchaseOrderNumber],  
 O.[ExternalOrderNumber],  
 O.[SiteId],    
 O.[CustomerId],    
 O.[TotalTax],    
 --O.[ShippingTotal],    
 O.[OrderTotal],  
O.CODCharges,  
 O.[TaxableOrderTotal],    
 O.[GrandTotal],
 O.[TotalShippingCharge],
 O.[TotalDiscount],
 O.[PaymentTotal],
 O.RefundTotal,
 O.[PaymentRemaining],   
 dbo.ConvertTimeFromUtc(O.OrderDate,@ApplicationId) OrderDate,    
 dbo.ConvertTimeFromUtc(O.CreatedDate,@ApplicationId)CreatedDate,    
 O.[CreatedBy],    
 FN.UserFullName CreatedByFullName,
 dbo.ConvertTimeFromUtc(O.ModifiedDate,@ApplicationId)ModifiedDate,    
 O.[ModifiedBy],
 O.[TotalShippingDiscount]    
FROM OROrder O    
LEFT JOIN VW_UserFullName FN on FN.UserId = O.CreatedBy
WHERE
O.SiteId = @ApplicationId 
AND O.Id in (Select Id from @OrderIds)

;with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderId in (Select Id from @OrderIds) AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	FN.UserFullName CreatedByFullName,
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.Tax,
	OI.Discount
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
LEFT JOIN VW_UserFullName FN on FN.UserId = OI.CreatedBy
WHERE OI.OrderId in (Select Id from @OrderIds) ;

SELECT 
	OS.[Id],
OS.[OrderId],
OS.[ShippingOptionId],
OS.[OrderShippingAddressId],
dbo.ConvertTimeFromUtc(OS.[ShipOnDate],@ApplicationId)ShipOnDate,
dbo.ConvertTimeFromUtc(OS.[ShipDeliveryDate],@ApplicationId)ShipDeliveryDate,
OS.[TaxPercentage],
OS.[TaxPercentage],
OS.[ShippingTotal],
OS.[Sequence],
OS.[IsManualTotal],
OS.[Greeting],
OS.[Tax],
OS.[SubTotal],
OS.[TotalDiscount],
OS.[ShippingCharge],
OS.[Status],
OS.[CreatedBy],
dbo.ConvertTimeFromUtc(OS.[CreatedDate],@ApplicationId) CreatedDate,
OS.[ModifiedBy],
dbo.ConvertTimeFromUtc(OS.[ModifiedDate],@ApplicationId) ModifiedDate,
OS.[ShippingDiscount],
OS.ShipmentHash,
OS.IsBackOrder
FROM OROrderShipping OS
WHERE OS.OrderId in (Select Id from @OrderIds) ;

select  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , 
  A.IsPersonalized,
  P.OrderId
from PRProductAttribute PA  
Inner join 
	(Select ProductId,OI.Orderid FROM OROrderItem OI
		Inner Join PRProductSKU S on OI.ProductSKUId =S.Id  
		Where OI.OrderId in (Select Id from @OrderIds) 
		) P On PA.ProductId=P.ProductId   
Inner Join ATAttribute A on PA.AttributeId = A.Id   
where  A.Status=1  AND PA.IsSKULevel=1 AND  A.IsPersonalized =1


SELECT [OrderId]
      ,[OrderItemId]
      ,[SKUId]
      ,[ProductId]
      ,[CustomerId]
      ,[ProductName]
      ,[AttributeId]
      ,[AttributeName]
      ,[AttributeEnumId]
      ,[Value]
      ,[Code]
      ,[NumericValue]
      ,[Sequence]
      ,[IsDefault]
      ,[CartId]
      ,[CartItemID]
      ,[AttributeGroupId]
      ,[IsFaceted]
      ,[IsPersonalized]
      ,[IsSearched]
      ,[IsSystem]
      ,[IsDisplayed]
      ,[IsEnum]
      ,[AttributeDataTypeId]
  FROM [dbo].[VWOrderItemPersonalizedAttribute]
	Where OrderId in (Select Id from @OrderIds) 

--- order shipments
SELECT F.[Id]
	 ,OS.OrderId
      ,F.[OrderShippingId]
      ,dbo.ConvertTimeFromUtc(F.ShippedDate,@ApplicationId)ShippedDate
      ,F.[ShippingOptionId]
      ,dbo.ConvertTimeFromUtc(F.CreateDate,@ApplicationId)CreateDate
      ,F.[CreatedBy]
      ,F.[ModifiedBy]
      ,dbo.ConvertTimeFromUtc(F.ModifiedDate,@ApplicationId)ModifiedDate
	  ,F.[EstimatedShipmentTotal]
	  ,F.ShipmentStatus
	  ,F.WarehouseId
	,F.ExternalReferenceNumber
FROM [dbo].[FFOrderShipment] F
INNER JOIN  OROrderShipping OS ON F.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 
Order By CreateDate DESC

SELECT SI.[Id]
      ,OS.OrderId
      ,SI.[OrderShipmentId]
      ,SI.[OrderItemId]
      ,SI.[Quantity]
      ,SI.[OrderShipmentContainerId]
	  ,SI.externalreferencenumber
      ,dbo.ConvertTimeFromUtc(SI.[CreateDate],@ApplicationId)CreateDate
      ,SI.[CreatedBy]
      ,dbo.ConvertTimeFromUtc(SI.[ModifiedDate],@ApplicationId)ModifiedDate
      ,SI.[ModifiedBy]
FROM [dbo].[FFOrderShipmentItem] SI 
INNER JOIN FFOrderShipment S ON S.Id = SI.OrderShipmentId
INNER JOIN  OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 



SELECT SP.[Id]
	  ,OS.OrderId
      ,SP.[OrderShipmentId]
      ,SP.[ShippingContainerId]
      ,SP.[TrackingNumber]
      ,SP.[ShippingCharge]
      ,dbo.ConvertTimeFromUtc(SP.CreatedDate,@ApplicationId)CreatedDate
      ,SP.[ChangedBy]
      ,dbo.ConvertTimeFromUtc(SP.ModifiedDate,@ApplicationId)ModifiedDate
      ,SP.[ModifiedBy]
      ,SP.ShipmentStatus
      ,SP.ShippingLabel
      ,SP.Weight
FROM [dbo].[FFOrderShipmentPackage] SP 
INNER JOIN FFOrderShipment S ON S.Id = SP.OrderShipmentId
INNER JOIN OROrderShipping OS ON S.OrderShippingId = OS.Id
Where OS.OrderId in (Select Id from @OrderIds) 

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[iAppsForm_GetFormVariants]'
GO
/*****************************************************
* Name : iAppsForm_GetFormVariants
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[iAppsForm_GetFormVariants] 
(
	@delimitedMasterSiteFormGuids nvarchar(max),
	@microSiteId UNIQUEIDENTIFIER
)
AS
BEGIN
	
	SELECT Id,
		ApplicationId,
		Title,
		Description, 
		FormType,								
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		ModifiedDate, 
		Status,
		XMLString,				
		XSLTString,
		XSLTFileName,
		SendEmailYesNo,
		Email,
		ThankYouURL,
		SubmitButtonText,
		PollResultType,	
		PollVotingType,
		AddAsContact,
		ContactEmailField,
		WatchId,
		EmailSubjectText,
		EmailSubjectField,
		SourceFormId,
		ParentId
	FROM Forms
		WHERE SourceFormId IN (SELECT Items FROM dbo.SplitGUID(@delimitedMasterSiteFormGuids, ','))
			AND ApplicationId = @microSiteId
			AND Status <> dbo.GetDeleteStatus()

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[TranslationAsset_GetNextActors]'
GO
/*****************************************************
* Name : TranslationAsset_GetNextActors
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Procedure [dbo].[TranslationAsset_GetNextActors]
	(@ApplicationId uniqueidentifier, @PageDefinitionId uniqueidentifier, @WorkflowId uniqueidentifier)
AS
BEGIN

	DECLARE @t1 table
	(RowNumber int, ObjectId uniqueidentifier, ObjectTypeId int, WorkflowId uniqueidentifier, 
		SequenceId uniqueidentifier, NextActorId uniqueidentifier, NextActorType int)

	DECLARE @NextActorId uniqueidentifier, @NextActorType int


	insert into @t1
		exec Workflow_GetNextActors 
	        @ApplicationId,
            @PageDefinitionId,
            8,
            @WorkflowId,
            null,
            null,
            null,
			null,
			null,
			null	
			
	SELECT @NextActorId = NextActorId, @NextActorType = NextActorType
	FROM @t1 
	WHERE RowNumber = 1

	IF (@NextActorType = 1) -- User
	BEGIN
		SELECT Email, EmailNotification
		FROM USUser
		INNER JOIN 	USMembership ON USUser.Id = USMembership.UserId
		WHERE UserId = @NextActorId
	END
	ELSE
	BEGIN
		SELECT Email, EmailNotification
		FROM USUser
		INNER JOIN 	USMembership ON USUser.Id = USMembership.UserId
		WHERE UserId IN 
		(SELECT MemberId FROM USMemberGroup WHERE USMemberGroup.GroupId = @NextActorId 
		--And ApplicationId = @ApplicationId 
		And ApplicationId IN (Select SiteId from dbo.GetAncestorSites(@ApplicationId)))
	END
END

 

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Address_Get]'
GO
/*****************************************************
* Name : Address_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: 
-- Author: 
-- Created Date:06-01-2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Address_Get](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
	
	SELECT 
		Id,
		FirstName,
		LastName,
		AddressType,
		AddressLine1,
		AddressLine2,
		AddressLine3,
		City,
		StateId,
		StateName,
		Zip,
		isnull(CountryId, (select  CountryId from GLState where Id=StateId)) CountryId,
		Phone,
		CreatedDate,
		CreatedBy,
		ModifiedDate,
		ModifiedBy,
		[Status],
		County
	FROM GLAddress 
	WHERE Id = @Id
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Blog_GetBlogDataForSiteMap]'
GO
/*****************************************************
* Name : Blog_GetBlogDataForSiteMap
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Blog_GetBlogDataForSiteMap]
(     
      @SiteId uniqueidentifier
)
AS
BEGIN
      SELECT 
            p.Id as PostId,p.BlogId,STR(YEAR(p.PostDate), 4) + '/' + REPLACE(STR(month(p.PostDate), 2), ' ', '0') as MonthYear,p.Title as PostTitle,p.FriendlyName, REPLACE(STR(month(p.PostDate), 2), ' ', '0') as PostMonth, STR(YEAR(p.PostDate), 4) as PostYear,
            b.MenuId,b.BlogListPageId,b.BlogDetailPageId,b.ContentDefinitionId,(case when p.ModifiedDate is null then p.CreatedDate else p.ModifiedDate end ) as LastModifiedDate, 'yearly ' as ChangeFrequency ,
            p.status,P.CreatedBy,P.ModifiedBy, p.CreatedDate, p.ModifiedDate
      into #tblPostURLs
            FROM  BLPost p,BLBlog b
            WHERE  (p.Status = 1 or p.Status = 5) and b.Id=p.BlogId and MenuId is not null and p.ApplicationId = @SiteId  

      update      #tblPostURLs set ChangeFrequency='daily' where  datediff(dd,LastModifiedDate,getdate()) <=3
      update      #tblPostURLs set ChangeFrequency='weekly' where  datediff(dd,LastModifiedDate,getdate()) >3 and datediff(dd,LastModifiedDate,getdate())<=10
      update      #tblPostURLs set ChangeFrequency='monthly' where  datediff(dd,LastModifiedDate,getdate()) >10 and datediff(dd,LastModifiedDate,getdate()) <=60


      select * from #tblPostURLs
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [dbo].[FWFeed_GetChannels]'
GO
/*****************************************************
* Name : FWFeed_GetChannels
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[FWFeed_GetChannels](

--********************************************************************************
-- Parameters
--********************************************************************************
	
	@FWFeedId 		uniqueidentifier = null,
	@ApplicationId  uniqueidentifier = null,
	@ChannelType	int = null
)
AS

BEGIN
	
	--If the @FWFeedId is not null,Get the FWFeed Channels information from SYRssChannel and
	--SYFWFeedRssChannel tables based on FWFeedId
	IF @FWFeedId IS NOT NULL
	BEGIN
	SELECT  Channel.[Id],
			Channel.ApplicationId,
			Channel.Title,
			Channel.[Description],
			Channel.Link,
			Channel.Copyright,
			Channel.Docs,
			Channel.Generator,
			Channel.[Language],
			Channel.ManagingEditor,
			Channel.PubDate,
			Channel.Rating,
			Channel.SkipDays,
			Channel.SkipHours,
			Channel.TimeToLive,
			Channel.WebMaster,
			Channel.CreatedDate,
			Channel.CreatedBy,
			Channel.ModifiedDate,
			Channel.ModifiedBy,
			Channel.Status,
			Channel.RssImageId,
			Channel.RssCloudId,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
	FROM SYRssChannel Channel 
		 INNER JOIN SYFWFeedRssChannel FeedChannel ON  FeedChannel.RssChannelId = Channel.[Id]
		 LEFT JOIN VW_UserFullName FN on FN.UserId = Channel.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = Channel.ModifiedBy
	WHERE FeedChannel.FWFeedId = ISNULL(@FWFeedId,FWFeedId)
	--Active Status Channels
	AND   Channel.Status = dbo.GetActiveStatus()
	END

	ElSE
	--If the @ApplicationId is not null,Get the Channels information from SYRssChannel 
	--table based on ApplicationId and also if the @ApplicationId is null gets all the channels 
	--information from SYRssChannel table
	BEGIN
	SELECT  Channel.[Id],
			Channel.ApplicationId,
			Channel.Title,
			Channel.[Description],
			Channel.Link,
			Channel.Copyright,
			Channel.Docs,
			Channel.Generator,
			Channel.[Language],
			Channel.ManagingEditor,
			Channel.PubDate,
			Channel.Rating,
			Channel.SkipDays,
			Channel.SkipHours,
			Channel.TimeToLive,
			Channel.WebMaster,
			Channel.CreatedDate,
			Channel.CreatedBy,
			Channel.ModifiedDate,
			Channel.ModifiedBy,
			Channel.Status,
			Channel.RssImageId,
			Channel.RssCloudId,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
	FROM SYRssChannel Channel
	 LEFT JOIN VW_UserFullName FN on FN.UserId = Channel.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = Channel.ModifiedBy
	WHERE
	Channel.ApplicationId = ISNULL(@ApplicationId,ApplicationId) 	
	--Active Status Channels
	AND Channel.Status = dbo.GetActiveStatus() 
	AND (@ChannelType IS NULL OR ChannelType = @ChannelType)
	END
				
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Product_GetBundleListP]'
GO
/*****************************************************
* Name : Product_GetBundleListP
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline's express written   
-- permission is prohibited.  
   
-- Purpose: To get the details of Product based on the parameters from the database.   
-- Author: Martin M V  
-- Created Date: 08-04-2009  
-- Created By: Martin M V  
--********************************************************************************  
-- List of all the changes with date and reason  
--********************************************************************************  
-- Modified Date:  
-- Modified By:  
-- Reason:  
-- Changes:  
  
--********************************************************************************  
-- Warm-up Script:  
--   exec [dbo].Product_GetBundleList   
--   @ApplicationId = uniqueidentifier null ,  
--          @pageNumber = 'int' or 0,  
--          @pageSize = 'int' or 0,  
--********************************************************************************  
ALTER PROCEDURE [dbo].[Product_GetBundleListP](  
--********************************************************************************  
-- Parameters  
--********************************************************************************  
   @ApplicationId uniqueidentifier = null,      
   @Searchkeyword nvarchar(4000)=null,    
   @SortColumn VARCHAR(100)=null, 
   @pageNumber  int = null,  
   @pageSize  int = null  ,
   @WhereClause nvarchar(max)= null
)  
AS  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE  
   @PageLowerBound   bigint,  
   @PageUpperBound   bigint   
  Declare @count int
	SET @count = 0
--********************************************************************************  
-- Procedure Body  
--********************************************************************************  
BEGIN  

set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
SET @SortColumn = rtrim(ltrim((@SortColumn)))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256)) +1
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256))	+1




DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

     IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	  

	ELSE IF @SortColumn = 'productiduser desc'
	SET @SortColumn = 'Product.ProductIDUser DESC'  
ELSE IF @SortColumn = 'productiduser asc'
	SET @SortColumn = 'Product.ProductIDUser ASC'  

ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'listprice desc'
	SET @SortColumn = 'Sku.ListPrice DESC'  
ELSE IF @SortColumn = 'listprice asc'
	SET @SortColumn = 'Sku.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 	
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''			
					) '
					
IF @WhereClause != '' AND @WhereClause IS NOT NULL
	SET @DynamicWhereClause = @DynamicWhereClause +
			N' AND ('+@WhereClause+')';


SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
			Product.Id As ProductId, 
			Product.SiteId,
			Product.ShortDescription As Description,   
			Product.ProductIDUser As Sequence,
			Product.IsActive, 
			case Product.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,  
			Sku.ListPrice,
			0 As UnitCost, 0 As SalePrice,
			0 As WholesalePrice,
			CASE WHEN dbo.GetBundleQuantity(Sku.Id) >= 9999999 THEN 
			''Unlimited'' ELSE CAST (dbo.GetBundleQuantityPerSite(Sku.Id,'''+ cast(@ApplicationId as char(36)) +''') AS Varchar)
			END AS Quantity,   
			Product.Title As ProductTtitle,
			Product.Title As ProductTitle,
			Product.ProductIDUser ProductIDUser,
			Type.Title As TypeTitle,  
			Type.Id as TypeId,
			dbo.GetProductCategoryTitleCollection(Product.Id)  As CategoryTitle,
			dbo.GetProductCategoryCollection(Product.Id)  As CategoryIdsAndTitles,  
			Sku.Id as Id, 
			Sku.Title as Title,
			 dbo.ConvertTimeFromUtc(Product.BundleCompositionLastModified,@ApplicationId) BundleCompositionLastModified  
   FROM PRProduct As Product  
   INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id    
   Inner Join PRProductSKU as Sku on Sku.ProductId = Product.Id  
            WHERE 
     Product.IsBundle = 1       
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'


EXEC sp_executesql   @DynamicSQL ,N'@ApplicationId uniqueidentifier' , @ApplicationId=@ApplicationId

SET @DynamicSQL = N'
SELECT  
	Count(*)
   FROM PRProduct As Product  
   INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id    
   Inner Join PRProductSKU as Sku on Sku.ProductId = Product.Id  
            WHERE
     Product.IsBundle = 1      
				
		'+@DynamicWhereClause
EXEC sp_executesql   @DynamicSQL 

Select @PageLowerBoundVar,@PageUpperBoundVar

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Product_GetProductListP]'
GO
/*****************************************************
* Name : Product_GetProductListP
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: To get the details of Product based on the parameters from the database. 
-- Author: Martin M V
-- Created Date: 08-04-2009
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].Product_GetProductList 
--			@ApplicationId	= uniqueidentifier null ,
--          @pageSize = 'int' or 0,
--          @pageSize = 'int' or 0,
--********************************************************************************
ALTER PROCEDURE [dbo].[Product_GetProductListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(100)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN	

set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0) +1
SET @SortColumn = rtrim(ltrim((@SortColumn)))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256))
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256))
	


DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

     IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	  


ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'reallistprice desc'
	SET @SortColumn = 'PPR.ListPrice DESC'  
ELSE IF @SortColumn = 'reallistprice asc'
	SET @SortColumn = 'PPR.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'PPR.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'PPR.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 	
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''			
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''			
					) '

IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';

SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					Product.Id As ProductId,
					Product.SiteId,
					Product.ShortDescription As Description, 
					Product.ProductIDUser ProductIDUser,
					Product.ProductIDUser As Sequence, 
					Product.IsActive, 
					case Product.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,
					PPR.PriceRange as ListPrice,
					0 As UnitCost,
					0 As SalePrice,
					0 As WholesalePrice, 
					isnull(PPR.Quantity,0) As Quantity,
					isnull(PPR.AvailableToSell,0) As AvailableToSell,
					isnull(PPR.ListPrice,0) as RealListPrice,
					Product.IsBundle as IsBundle,
					dbo.ConvertTimeFromUtc(Product.BundleCompositionLastModified, '''+@ApplicationIdVar+''') as BundleCompositionLastModified,
					Product.Title As ProductTtitle,
					Product.Title As ProductTitle,
					Type.Title As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  As CategoryIdsAndTitles,
					dbo.IsProductUnlimited(Product.Id) as IsUnlimited,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia,
					Product.TaxCategoryID
			FROM PRProduct As Product
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN vwPRProductPriceRange PPR  ON PPR.Id =  Product.Id AND PPR.SiteId=''' + cast(@ApplicationId as Char(36)) + '''
            WHERE		Product.IsBundle = 0 			
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'

print @DynamicSQL
EXEC sp_executesql   @DynamicSQL 


SET @DynamicSQL = N'
SELECT 
	Count(*) 
FROM PRProduct As Product
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  

            WHERE	
					Product.IsBundle = 0 
		'+@DynamicWhereClause
EXEC sp_executesql   @DynamicSQL 

END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Product_GetProductSKUListP]'
GO
/*****************************************************
* Name : Product_GetProductSKUListP
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER PROCEDURE [dbo].[Product_GetProductSKUListP](
--********************************************************************************
-- Parameters
--********************************************************************************
			@ApplicationId	uniqueidentifier = null,				
			@pageNumber		int = null,
			@pageSize		int = null,
			@SortColumn VARCHAR(50)=null, 
			@Searchkeyword nvarchar(4000)=null,
			@WhereClause nvarchar(max)= null	
)
AS

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE
			@PageLowerBound			bigint,
			@PageUpperBound			bigint 


--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN

		
set @pageNumber = @pageNumber  -1
SET @PageLowerBound = Isnull(@pageSize,0) * Isnull(@pageNumber,0)
SET @SortColumn = lower(rtrim(ltrim((@SortColumn))))
SET @PageUpperBound = @PageLowerBound + Isnull(@pageSize,0) -1;

DECLARE @PageUpperBoundVar varchar(250)
DECLARE @PageLowerBoundVar varchar(250)
DECLARE @ApplicationIdVar nvarchar(64)
SET @ApplicationIdVar = cast(@ApplicationId as nvarchar(64))
SET @ApplicationIdVar = ltrim(rtrim(@ApplicationIdVar))
SET @PageLowerBoundVar = cast(@PageLowerBound as varchar(256)) +1
SET @PageUpperBoundVar = cast(@PageUpperBound as varchar(256)) + 1



--SET @SearchKeyword = ''

DECLARE  @DynamicSQL  AS NVARCHAR(MAX) 


SET @SearchKeyword = (@SearchKeyword)

IF @SortColumn = 'producttitle desc'	 
	SET @SortColumn = 'Product.Title DESC' 	
ELSE IF @SortColumn = 'producttitle asc'	
	SET @SortColumn = 'Product.Title asc' 	
  
ELSE IF @SortColumn = 'sku desc'
	SET @SortColumn = 'SKU.SKU DESC'  
ELSE IF @SortColumn = 'sku asc'
	SET @SortColumn = 'SKU.SKU ASC'  

ELSE IF @SortColumn = 'typetitle desc'
	SET @SortColumn = 'Type.Title DESC'  
ELSE IF @SortColumn = 'typetitle asc'
	SET @SortColumn = 'Type.Title ASC'  

ELSE IF @SortColumn = 'listprice desc'
	SET @SortColumn = 'SKU.ListPrice DESC'  
ELSE IF @SortColumn = 'listprice asc'
	SET @SortColumn = 'SKU.ListPrice ASC'  

ELSE IF @SortColumn = 'quantity desc'
	SET @SortColumn = 'inv.Quantity DESC'  
ELSE IF @SortColumn = 'quantity asc'
	SET @SortColumn = 'inv.Quantity ASC'

ELSE IF @SortColumn = 'availabletosell desc'
	SET @SortColumn = 'avail.Quantity DESC'  
ELSE IF @SortColumn = 'availabletosell asc'
	SET @SortColumn = 'avail.Quantity ASC'
  
ELSE IF @SortColumn = 'isactivedescription desc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) DESC'  
ELSE IF @SortColumn = 'isactivedescription asc'
	SET @SortColumn = 'cast(Product.IsActive as varchar(25)) ASC'  

DECLARE @DynamicWhereClause nvarchar(max)


IF @SearchKeyword = ''
	SET @DynamicWhereClause = N''
ELSE
	SET @DynamicWhereClause = 
					N'AND ((Product.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Product.ProductIDUser) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (Type.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.Title) Like ''%''+'''+@SearchKeyword+'''+''%''
					OR (SKU.SKU) Like ''%''+'''+@SearchKeyword+'''+''%''
					) '
					
IF @WhereClause != '' 
	SET @DynamicWhereClause = @DynamicWhereClause +
			N'AND ('+@WhereClause+')';


SET @DynamicSQL = N'
SELECT * FROM (
   SELECT   
     ROW_NUMBER() OVER (ORDER BY 	
	 '+@SortColumn+'
) AS RowNumber, 				
					SKU.Id, 
					SKU.ProductId,
					SKU.SiteId, 
					SKU.Title, 
					SKU.SKU, 
					SKU.Description, 
					SKU.Sequence, 
					SKU.IsActive, 
					case SKU.IsActive WHEN 1 THEN ''Active'' ELSE ''Inactive'' END  As IsActiveDescription ,  
					SKU.IsOnline, 
					SKU.ListPrice, 
					SKU.UnitCost, 
					SKU.ListPrice As SalePrice, 
					SKU.WholesalePrice As WholesalePrice, 
					IsNull(inv.Quantity,0) as Quantity,
					avail.Quantity as AvailableToSell,
					SKU.HandlingCharges,
					Product.Title As ProductTitle, 
					Product.ProductIDUser As ProductIDUser,
					Type.Title	As TypeTitle,  
					Type.Id as TypeId,
					dbo.GetProductCategoryTitleCollection(Product.Id)  
					As CategoryTitle, 
					dbo.GetProductCategoryCollection(Product.Id)  
					 As CategoryIdsAndTitles,
					IsUnlimitedQuantity AS IsUnlimited,
					SKU.Length,SKU.Height,SKU.Width,SKU.Weight,SKU.SelfShippable,SKU.AvailableForBackOrder,SKU.BackOrderLimit,
					ISNULL(Type.IsDownloadableMedia,0) as IsDownloadableMedia
			FROM PRProductSKU As SKU
			INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
			INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
			LEFT JOIN [vwInventoryPerSite] inv on SKU.Id = inv.SKUId AND inv.SiteId=''' + @ApplicationIdVar + '''
			LEFT JOIN vwAvailableToSellPerWarehouse avail ON SKU.Id = avail.SKUId AND avail.SiteId=''' + @ApplicationIdVar + '''
            WHERE	
				Product.IsBundle = 0		
				'+@DynamicWhereClause+'
		) TV
		WHERE 
		(RowNumber BETWEEN '+@PageLowerBoundVar+' AND '+@PageUpperBoundVar+')'


EXEC sp_executesql   @DynamicSQL 

SET @DynamicSQL = N'
SELECT  
	Count(*)
FROM 
	PRProductSKU As SKU
	INNER Join PRProduct AS Product ON Product.Id = SKU.ProductId 
	INNER Join PRProductType As Type ON Product.ProductTypeID = Type.Id  
WHERE	
		Product.IsBundle = 0
		'+@DynamicWhereClause

EXEC sp_executesql   @DynamicSQL 

END

GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order id
-- Author: 
-- Created Date:03/25/2009
-- Created By:Devin

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItems](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.Id=@Id  AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	OI.CreatedDate AS CreatedDate,
	OI.[CreatedBy],
	OI.ModifiedDate AS ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount]
FROM OROrderItem OI
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id
WHERE OI.OrderId = @Id;


END
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order id
-- Author: 
-- Created Date:03/25/2009
-- Created By:Devin

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItemsForShipping](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN

;with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
Where OI.OrderShippingId =@Id AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	Isnull(SI.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId),
	OI.[CreatedBy],
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId),
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount]
FROM OROrderItem OI 
Left Join cteShipmentItemQuantity SI ON  OI.Id = SI.Id
WHERE OI.OrderShippingId = @Id
ORDER BY OI.[Sequence];


END
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order id
-- Author: 
-- Created Date:06/24/2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItemsForShippment](@Id uniqueidentifier,@ApplicationId uniqueidentifier=null)
AS
BEGIN
with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Where OI.OrderShippingId =@Id And ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	Isnull(SI.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount]
FROM OROrderItem OI
INNER JOIN PRPRoductSKU PS ON PS.Id = OI.ProductSKUID
INNER JOIN PRProduct P ON P.Id = PS.ProductID
INNER JOIN PRProductType PT ON PT.Id = P.ProductTypeId
Left Join cteShipmentItemQuantity SI ON  OI.Id = SI.Id
WHERE OI.OrderShippingId = @Id
AND Isnull(SI.Quantity,0) < OI.Quantity
AND (NOT( ISNULL(PT.IsDownloadableMedia,0) = 1 
AND EXISTS (Select Id from PRProductMedia where SKUId = PS.Id)))

END
GO

ALTER PROCEDURE [dbo].[CommerceReport_SalesByPromoCode]
(	
	@couponCode nvarchar(255) = null,
	@startDate datetime,@endDate datetime, @maxReturnCount int,
	@pageSize int = 10,@pageNumber int, 
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS
BEGIN


--			declare @startDate datetime
--			declare @endDate datetime
--			declare @maxReturnCount int
--			declare @virtualCount int
--			declare @pageSize int
--			declare @pageNumber int
--			declare @sortBy nvarchar(100)


--		set @sortBy = 'SaleTotal asc'
--			set @returnTopCount = 4
			if @maxReturnCount = 0
				set @maxReturnCount = null
--
--			set @pageSize = 50
--			set @pageNumber =1
----
--			set @startDate = '06-01-2009'
--			set @endDate = '08-16-2009'


		IF @pageSize IS NULL
				SET @PageSize = 2147483647
		IF @endDate IS NULL
				SET @endDate = GetDate()

		declare @tmpCouponSale table(
							CouponId uniqueidentifier,
							Title nvarchar(255),
							Code nvarchar(255),
							OrderCount int,
							TotalDiscount money
										)

		INSERT INTO @tmpCouponSale

			SELECT CP.ID, CP.Title, CPC.Code, 
					Count(O.Id) As OrderCount , 
					Sum(O.TotalDiscount + O.TotalShippingDiscount) As TotalDiscount
			FROM OROrder O 
					INNER JOIN CPOrderCouponCode CPO on O.Id = CPO.OrderId
					INNER JOIN CPCouponCode CPC on CPC.Id = CPO.CouponCodeId 
					INNER JOIN CPCoupon CP on CPC.CouponId = CP.ID
			WHERE 
					--cast(convert(varchar(12),dbo.ConvertTimeFromUtc(OrderDate,@ApplicationId),101)  as datetime)>= cast(convert(varchar(12),@startDate,101) as datetime)
					--AND cast(convert(varchar(12),dbo.ConvertTimeFromUtc(OrderDate,@ApplicationId),101)  as datetime)<= cast(convert(varchar(12),@endDate,101) as datetime)
					O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
					AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
					AND O.OrderStatusId in (1,2,11,4)
					AND O.SiteId = @ApplicationId
					AND
					(@couponCode is null or CPC.Code = @couponCode) 
					--CPC.Code = IsNull(@couponCode, CPC.Code)
			GROUP BY
				  CPC.Code, CP.ID,  CP.Title

			Set @virtualCount = @@ROWCOUNT

			if @sortBy is null or (@sortBy) = 'code asc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by Code asc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.Code ASC
				end
			else if @sortBy is null or (@sortBy) = 'code desc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by Code desc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.Code desc
				end
			else if @sortBy is null or (@sortBy) = 'ordercount asc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by OrderCount asc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.OrderCount asc
				end
			else if @sortBy is null or (@sortBy) = 'ordercount desc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by OrderCount desc) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.OrderCount desc
				end
			else if @sortBy is null or (@sortBy) = 'totaldiscount asc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by TotalDiscount ASC) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.TotalDiscount ASC
				end	
			else if @sortBy is null or (@sortBy) = 'totaldiscount desc'
				Begin
					Select A.*
					FROM
						(
						Select *,  ROW_NUMBER() OVER(Order by TotalDiscount DESC) as RowNumber
						 from @tmpCouponSale
						) A
						where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and
						 A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber
					 Order by A.TotalDiscount DESC
				end	
END
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: Delete PSPriceSetDiscount
-- Author: 
-- Created Date:04/06/2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[AutoPriceSetDiscount_Delete](
--********************************************************************************
-- Parameters Passing in Attribute ID
--********************************************************************************
		@Id					uniqueidentifier,
		@ModifiedBy			uniqueidentifier,
		@Status				int OUTPUT
)
AS
--********************************************************************************
-- Variable declaration
--********************************************************************************
BEGIN

	IF((SELECT Count(Id) FROM  PSPriceSetDiscount WHERE Id = @Id) = 0)
	BEGIN
		RAISERROR('NOTEXISTS||Id', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	SET @Status	=	dbo.GetDeleteStatus()

	DELETE FROM PSPriceSetDiscount WHERE	[Id]	=	@Id
    IF @@Error <> 0
    BEGIN
		RETURN dbo.GetDataBaseErrorCode()           
    END

END
GO
ALTER PROCEDURE [dbo].[AutoPriceSetDiscount_Save]  
(  
	 @PriceSetId uniqueidentifier,  
	 @AutoPriceSetXml xml,  
	 @CreatedBy uniqueidentifier,  
	 @ModifiedBy uniqueidentifier,   
	 @Id uniqueidentifier output  
)  
as  
begin  
   insert into PSPriceSetDiscount  
   (Id, PriceSetId,MinQuantity,MaxQuantity,DiscountAmount,CreatedDate,CreatedBy)  
   SELECT newid(),  
	   @PriceSetId,  
       T.c.value('(MinQuantity)[1]','decimal(18,4)'),    
       T.c.value('(MaxQuantity)[1]','decimal(18,4)'),    
	   T.c.value('(DiscountAmount)[1]','decimal(18,4)'),    
       getutcdate(),  
       @ModifiedBy  
   FROM @AutoPriceSetXml.nodes('/GenericCollectionOfAutoPriceSetDiscount/AutoPriceSetDiscount') T(c)  
	Where T.c.value('(Id)[1]','uniqueidentifier') = dbo.GetEmptyGUID()
  

  update PSPriceSetDiscount   
  set PriceSetId = @PriceSetId,  
      MinQuantity = T.c.value('(MinQuantity)[1]','decimal(18,4)'),    
      MaxQuantity = T.c.value('(MaxQuantity)[1]','decimal(18,4)'),    
	  DiscountAmount = T.c.value('(DiscountAmount)[1]','decimal(18,4)'),    
      ModifiedDate = getutcdate(),  
       ModifiedBy = @ModifiedBy
FROM @AutoPriceSetXml.nodes('/GenericCollectionOfAutoPriceSetDiscount/AutoPriceSetDiscount') T(c)  
	Where T.c.value('(Id)[1]','uniqueidentifier') = Id
  

   
  
end

GO



GO
PRINT 'Altering procedure PageMapNode_Update'
GO
ALTER PROCEDURE [dbo].[PageMapNode_Update]
(
	@PageMapNodeId [uniqueidentifier],
	@ParentId [uniqueidentifier],
	@SiteId [uniqueidentifier] ,
	@ExcludeFromSiteMap bit = 0,
	@Description [nvarchar](500) =NULL,
	@DisplayTitle [nvarchar](256) =NULL,
	@FriendlyUrl [nvarchar](500) =NULL,
	@MenuStatus [int] =NULL,
	@TargetId [uniqueidentifier] =NULL,
	@Target [int]= NULL,
	@TargetUrl [nvarchar](500) =NULL,
	@ModifiedBy [uniqueidentifier] =NULL,
	@ModifiedDate [datetime]= NULL,
	@PropogateWorkflow [bit]= 0,
	@InheritWorkflow [bit]= 0,
	@PropogateSecurityLevels [bit]= NULL,
	@InheritSecurityLevels [bit]= 0,
	@PropogateRoles [bit]= NULL,
	@InheritRoles [bit]= 0,
	@Roles [nvarchar](max)= NULL,
	@LocationIdentifier [nvarchar](50)= NULL,
	@HasRolloverImages [bit]= 0,
	@RolloverOnImage [nvarchar](256)= NULL,
	@RolloverOffImage [nvarchar](256)= NULL,
	@IsCommerceNav [bit] =0,
	@AssociatedQueryId [uniqueidentifier]= NULL,
	@DefaultContentId [uniqueidentifier]= NULL,
	@AssociatedContentFolderId [uniqueidentifier]= NULL,
	@SecurityLevels				[varchar](max)=NULL,
	@PageMapNodeWorkFlow [varchar](max)=NULL,
	@CustomAttributes [xml] =NULL,
	@HiddenFromNavigation [bit] = 0
	)
AS
BEGIN
	Declare @Old_PropogateWorkflow bit,
		@Old_InheritWorkflow bit,
		@Old_PropogateSecurityLevels bit,
		@Old_InheritSecurityLevels bit,
		@Old_PropogateRoles bit,
		@Old_InheritRoles bit,
		@LftValue bigint,
		@RgtValue bigint

	if(@PageMapNodeId='00000000-0000-0000-0000-000000000000')
	BEGIN
		RAISERROR('INVALID||PageMapNodeId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	if not exists(Select 1 from PageMapNode where PageMapNodeId =@pageMapNodeId and SiteId = @SiteId)
	BEGIN
		RAISERROR('NOEXISTS||PageMapNodeId', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
		
	Select @Old_PropogateWorkflow =PropogateWorkflow,
			@Old_InheritWorkflow =InheritWorkflow,
			@Old_PropogateSecurityLevels =PropogateSecurityLevels,
			@Old_InheritSecurityLevels =InheritSecurityLevels,
			@Old_PropogateRoles=PropogateRoles,
			@Old_InheritRoles =InheritRoles,
			@LftValue =LftValue,
			@RgtValue=RgtValue
	FROM PageMapNode Where PageMapNodeId=@PageMapNodeId
			
	UPDATE [dbo].[PageMapNode]
	SET [SiteId] = @SiteId
      ,[ExcludeFromSiteMap] = @ExcludeFromSiteMap
      ,[Description] = @Description
      ,[DisplayTitle] = @DisplayTitle
      ,[FriendlyUrl] = @FriendlyUrl
      ,[MenuStatus] = @MenuStatus
      ,[TargetId] = @TargetId
      ,[Target] = @Target
      ,[TargetUrl] = @TargetUrl
      ,[ModifiedBy] = @ModifiedBy
      ,[ModifiedDate] = @ModifiedDate
      ,[PropogateWorkflow] = @PropogateWorkflow
      ,[InheritWorkflow] = @InheritWorkflow
      ,[PropogateSecurityLevels] = @PropogateSecurityLevels
      ,[InheritSecurityLevels] = @InheritSecurityLevels
      ,[PropogateRoles] = @PropogateRoles
      ,[InheritRoles] = @InheritRoles
      ,[Roles] = @Roles
      ,[LocationIdentifier] =@LocationIdentifier
      ,[HasRolloverImages] = @HasRolloverImages
      ,[RolloverOnImage] = @RolloverOnImage
      ,[RolloverOffImage] = @RolloverOffImage
      ,[IsCommerceNav] = @IsCommerceNav
      ,[AssociatedQueryId] = @AssociatedQueryId
      ,[DefaultContentId] = @DefaultContentId
      ,[AssociatedContentFolderId] = @AssociatedContentFolderId
      ,[CustomAttributes] = @CustomAttributes
      ,[HiddenFromNavigation] = @HiddenFromNavigation
	WHERE  [PageMapNodeId] = @PageMapNodeId

	if (@ExcludeFromSiteMap=1)
	BEGIN
		Update C set C.ExcludeFromSiteMap = @ExcludeFromSiteMap
		FROM PageMapNode C 
		Inner Join PageMapNode P on C.LftValue>P.LftValue AND C.RgtValue < P.RgtValue and P.SiteId=C.SiteId
		Where  P.SiteId = @SiteId AND P.PageMapNodeId=@PageMapNodeId
	END			
	
	IF (@Old_PropogateSecurityLevels != @PropogateSecurityLevels 
		OR @Old_InheritSecurityLevels!=@InheritSecurityLevels
		OR @SecurityLevels!=''	)
	BEGIN
		DELETE FROM USSecurityLevelObject Where ObjectId=@PageMapNodeId
		EXEC dbo.PageMapNode_AttachSecurityLevels @SiteId=@SiteId
			,@PageMapNodeId=@PageMapNodeId
			,@SecurityLevels=@SecurityLevels
			,@Propogate= @PropogateSecurityLevels
			,@Inherit= @InheritSecurityLevels
			,@ModifiedBy=@ModifiedBy
			,@ModifiedDate=@ModifiedDate
	END
	ELSE IF (@SecurityLevels = '')
		DELETE FROM USSecurityLevelObject Where ObjectId=@PageMapNodeId

		
	IF (@Old_PropogateWorkflow != @PropogateWorkflow 
	OR @Old_InheritWorkflow!=@InheritWorkflow
	OR @PageMapNodeWorkFlow!='')
	BEGIN
		EXECUTE [dbo].[PageMapNode_AttachWorkFlow] 
		   @SiteId =@SiteId
		  ,@PageMapNodeId=@PageMapNodeId
		  ,@PageMapNodeWorkFlow=@PageMapNodeWorkFlow
		  ,@Propogate=@PropogateWorkflow
		  ,@Inherit=@InheritWorkflow
		  ,@AppendToExistingWorkflows =0
		  ,@ModifiedBy=@ModifiedBy
		  ,@ModifiedDate=@ModifiedDate
			  
	END

	EXEC  [dbo].[PageMapNode_UpdateMemberRoles] 
		@SiteId = @SiteId,
		@PageMapNodeId = @PageMapNodeId,
		@Propogate = @PropogateRoles,
		@Inherit = @InheritRoles
	
	exec PageMapBase_UpdateLastModification @SiteId
	exec PageMap_UpdateHistory @SiteId, @PageMapNodeId, 2, 2
END

GO

PRINT 'Altering procedure OrderShipping_Save'
GO
ALTER PROCEDURE [dbo].[OrderShipping_Save]  
(   
 @Id uniqueidentifier output,  
 @OrderId uniqueidentifier,  
 @ShippingOptionId uniqueidentifier,  
 @OrderShippingAddressId uniqueidentifier,  
 @ShipOnDate datetime,  
 @ShipDeliveryDate datetime,  
 @TaxPercentage decimal(18,2),  
 @ShippingTotal money,
 @ShippingDiscount money,
 @Sequence int,  
 @IsManualTotal bit,  
 @Greeting nvarchar(max),
 @Tax money,
 @SubTotal money,
 @TotalDiscount money,
 @TaxableTotal money,
 @ShippingCharge money,  
 @Status int,  
 @CreatedBy uniqueidentifier,  
 --@CreatedDate datetime,  
 @ModifiedBy uniqueidentifier,  
 @ApplicationId uniqueidentifier,
 @ShipmentHash nvarchar(max),
 @IsBackOrder bit = null,
 @IsHandlingIncluded bit=null
)  
  
AS  
BEGIN       
  DECLARE @CreatedDate datetime, @ModifiedDate DateTime

If(@ShippingOptionId is null OR @ShippingOptionId = dbo.GetEmptyGUID())
BEGIN
	SET @ShippingOptionId = null
END

If(@OrderShippingAddressId is null OR @OrderShippingAddressId = dbo.GetEmptyGUID())
BEGIN
	SET @OrderShippingAddressId = null
END

If(@IsBackOrder is null)
BEGIN
	SET @IsBackOrder = 0
END

 if(@Id is null OR @Id = dbo.GetEmptyGUID() OR (Select count(*) FROM OROrderShipping Where Id=@Id) =0)  
 begin  
  set @Id = newid()  
  set @CreatedDate = getUTCDate()
  INSERT INTO OROrderShipping  
           ([Id]  
   ,[OrderId]  
   ,[ShippingOptionId]  
   ,[OrderShippingAddressId]  
   ,[ShipOnDate]  
   ,[ShipDeliveryDate]  
   ,[TaxPercentage]  
   ,[ShippingTotal] 
   ,[ShippingDiscount] 
   ,[Sequence]  
   ,[IsManualTotal]  
   ,[Greeting] 
   ,[Tax] 
   ,[TaxableTotal]
   ,[SubTotal] 
   ,[TotalDiscount] 
   ,[ShippingCharge]  
   ,[Status]  
   ,[CreatedDate]  
   ,[CreatedBy]  
   ,[ModifiedDate]  
   ,[ModifiedBy]
   ,[ShipmentHash]
   ,[IsBackOrder]
   ,[IsHandlingIncluded])  
     VALUES  
           (@Id,  
   @OrderId,  
   @ShippingOptionId,  
   @OrderShippingAddressId,  
   dbo.ConvertTimeToUtc(@ShipOnDate,@ApplicationId),  
   dbo.ConvertTimeToUtc(@ShipDeliveryDate,@ApplicationId),  
   @TaxPercentage,  
   @ShippingTotal, 
   @ShippingDiscount, 
   @Sequence,  
   @IsManualTotal,  
   @Greeting, 
   @Tax, 
   @TaxableTotal,
   @SubTotal, 
   @TotalDiscount, 
   @ShippingCharge,  
   @Status,  
   @CreatedDate,  
   @CreatedBy,  
   @CreatedDate,  
   @ModifiedBy,
   @ShipmentHash,
   @IsBackOrder,
   @IsHandlingIncluded)  
 end  
 else  
 begin  
  update OROrderShipping  
  set   
   OrderId=@OrderId,  
   ShippingOptionId=@ShippingOptionId,  
   OrderShippingAddressId=@OrderShippingAddressId,  
   ShipOnDate=@ShipOnDate,  
   ShipDeliveryDate=@ShipDeliveryDate,  
   TaxPercentage=@TaxPercentage,  
   ShippingTotal=@ShippingTotal,  
   ShippingDiscount=@ShippingDiscount,
   Sequence=@Sequence,  
   IsManualTotal=@IsManualTotal,  
   Greeting=@Greeting,
   Tax=@Tax,
   TaxableTotal =@TaxableTotal,
   SubTotal=@SubTotal,
   TotalDiscount=@TotalDiscount,
   ShippingCharge=@ShippingCharge,
   Status=@Status,  
   --CreatedDate=@CreatedDate,  
   CreatedBy=@CreatedBy,  
   ModifiedDate=GetUTCDate(),  
    ModifiedBy=@ModifiedBy,
    ShipmentHash=@ShipmentHash,  
    IsBackOrder=@IsBackOrder,
    IsHandlingIncluded=@IsHandlingIncluded
   where Id=@Id  
     
 end  

END  
  

GO

-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.

-- Purpose: gets the order items with the given order item ids
-- Author: 
-- Created Date:06/24/2009
-- Created By:Prakash

-- Modified Date: 
-- Modified By: 
-- Reason:  
-- Changes:
--
--********************************************************************************
-- List of all the changes with date and reason
--
--********************************************************************************
--
--********************************************************************************


ALTER PROCEDURE [dbo].[Order_GetOrderItemsByXmlGuids](@Ids xml,@ApplicationId uniqueidentifier=null)
AS
BEGIN

with cteShipmentItemQuantity 
AS
(
Select SUM(SI.Quantity) Quantity,OI.Id 
FROM FFOrderShipmentItem SI
INNER JOIN FFOrderShipmentPackage S ON SI.OrderShipmentContainerId=S.Id
Inner Join OROrderItem OI ON SI.OrderItemId =OI.Id
WHERE SI.OrderItemId in(Select tab.col.value('text()[1]','uniqueidentifier') from  @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col))
AND ShipmentStatus=2
Group By OI.Id
)

SELECT 
	OI.[Id],
	OI.[OrderId],
	OI.[OrderShippingId],
	OI.[OrderItemStatusId],
	OI.[ProductSKUId],
	OI.[Quantity],
	isnull(SQ.Quantity,0) As ShippedQuantity,
	OI.[Price],
	OI.[UnitCost],
	OI.[IsFreeShipping],
	OI.[Sequence],
	dbo.ConvertTimeFromUtc(OI.CreatedDate,@ApplicationId)CreatedDate,
	OI.[CreatedBy],
	dbo.ConvertTimeFromUtc(OI.ModifiedDate,@ApplicationId)ModifiedDate,
	OI.[ModifiedBy],
	OI.[ParentOrderItemId],
	OI.[HandlingCharge],
	OI.[BundleItemId],
	OI.[CartItemId],
	OI.[Tax],
	OI.[Discount]
FROM OROrderItem OI
INNER JOIN @Ids.nodes('/GenericCollectionOfGuid/guid')tab(col) ON OI.Id = tab.col.value('text()[1]','uniqueidentifier')
Left Join cteShipmentItemQuantity SQ ON SQ.Id = OI.Id


END

GO

ALTER PROCEDURE  [dbo].[SqlDirectoryProvider_MoveFiles] 
(
	@SourceNodeId 			[uniqueidentifier],  
	@DestinationNodeId 		[uniqueidentifier],
	@Recursive				[bit],
	@OverWrite				[bit],
	@ObjectType				int=null,
	@ModifiedBy				uniqueidentifier
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE 
	@ApplicationId uniqueidentifier,
	@SiteName nvarchar(1024),
	@PLft bigint,
	@PRgt bigint,
	@Count bigint,
	@RecordCount bigint,
	@Id uniqueidentifier,
	@Title nvarchar(256),
	@ObjectTypeId int,
	@ParentId uniqueidentifier,
	@LftValue bigint,
	@RgtValue bigint,
	@DLftValue bigint,
	@DRgtValue bigint,
	@FileExists bit,
	@tempFileName nvarchar(256),
	@DestinationFileId uniqueidentifier

Declare @temp table(Id uniqueidentifier,Title nvarchar(256),ObjectTypeId int,ParentId uniqueidentifier,LftValue bigint,RgtValue bigint)
Declare @FileName table(FileId uniqueidentifier,OldFileName nvarchar(max),NewFileName nvarchar(max),ParentId uniqueIdentifier,ObjectTypeId int)
--********************************************************************************

-- code
--********************************************************************************
BEGIN --1
	
	IF(@ObjectType IS NULL)
	BEGIN -- 100
		Select @PLft = LftValue , @PRgt = RgtValue,@ApplicationId=SiteId from HSStructure Where Id=@SourceNodeId

		Insert into @temp(Id,Title,ObjectTypeId,ParentId,LftValue,RgtValue)
		Select Id,Title,ObjectTypeId,ParentId,LftValue,RgtValue
		From HSStructure
		Where LftValue Between @PLft and @PRgt
		And ObjectTypeId!=dbo.GetSiteDirectoryObjectType()
		And (@Recursive=1 Or ParentId=@SourceNodeId)
		AND	SiteId=	@ApplicationId
		Order by LftValue
	END -- 100
	ELSE
	BEGIN -- 101
		IF(@ObjectType = 7)
			BEGIN-- 102
				Select @PLft = LftValue , @PRgt = RgtValue,@ApplicationId=SiteId from COContentStructure Where Id=@SourceNodeId
				
				Insert into @temp(Id,Title,ObjectTypeId,ParentId,LftValue,RgtValue)
				Select C.Id,C.Title,@ObjectType,C.ParentId,LftValue,RgtValue
				From COContentStructure CS
				INNER JOIN COContent C ON CS.Id = C.ParentId
				Where LftValue Between @PLft and @PRgt
				And (@Recursive=1 Or C.ParentId=@SourceNodeId)
				AND	SiteId=	@ApplicationId
				Order by LftValue
			END -- 102
		ELSE IF(@ObjectType = 9)
			BEGIN -- 103
				Select @PLft = LftValue , @PRgt = RgtValue,@ApplicationId=SiteId from COFileStructure Where Id=@SourceNodeId
				
				Insert into @temp(Id,Title,ObjectTypeId,ParentId,LftValue,RgtValue)
				Select C.Id,C.Title,@ObjectType,C.ParentId,LftValue,RgtValue
				From COFileStructure CS
				INNER JOIN COContent C  ON CS.Id = C.ParentId
				Where LftValue Between @PLft and @PRgt
				And (@Recursive=1 Or C.ParentId=@SourceNodeId)
				AND	SiteId=	@ApplicationId
				AND C.Status!= dbo.GetDeleteStatus()
				Order by LftValue
			
			END -- 103
		ELSE IF(@ObjectType = 33)
			BEGIN -- 104
			Select @PLft = LftValue , @PRgt = RgtValue,@ApplicationId=SiteId from COImageStructure Where Id=@SourceNodeId
			
			Insert into @temp(Id,Title,ObjectTypeId,ParentId,LftValue,RgtValue)
			Select C.Id,C.Title,@ObjectType,C.ParentId,LftValue,RgtValue
			From COImageStructure CS
			INNER JOIN COContent C  ON CS.Id = C.ParentId
			Where LftValue Between @PLft and @PRgt
			And (@Recursive=1 Or C.ParentId=@SourceNodeId)
			AND	SiteId=	@ApplicationId
			AND C.Status!= dbo.GetDeleteStatus()
			Order by LftValue
			
			END	-- 104
	END -- 101
	
--select * from @temp

	SET @RecordCount =@@RowCount
	SET @Count=0
	SET @LftValue=0
	While(@Count < @RecordCount)
	BEGIN --2
		Select top 1 @Id=Id,@Title=Title,@ObjectTypeId=ObjectTypeId,@ParentId=ParentId,@LftValue=LftValue,@RgtValue=RgtValue 
		From @temp
		--Select @Id
		Delete from @temp where Id = @Id
		--Where LftValue >@LftValue
		IF(@ObjectTypeId = 9)
			Select @tempFileName = VirtualPath from COFileStructure Where Id =@ParentId
		ELSE IF(@ObjectTypeId = 33)
			Select @tempFileName = VirtualPath from COImageStructure Where Id =@ParentId
		ELSE	
			Select @tempFileName = VirtualPath from SISiteDirectory Where Id =@ParentId
			
		--Check whether the file exists in destination
		SET @FileExists =0
		IF @ObjectTypeId =3
		BEGIN --2.1
				Insert into @FileName(FileId,OldFileName,ParentId,ObjectTypeId)
				Select @Id, @tempFileName + '/' + FileName,@ParentId,@ObjectTypeId from SITemplate Where Id=@Id
			IF EXISTS(Select * from SITemplate T inner join HSStructure S on T.Id=S.Id
			Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() 
					AND LOWER(FileName)=(Select LOWER(FileName) From SITemplate Where Id=@Id AND Status!= dbo.GetDeleteStatus()))
			SET @FileExists =1
		END --2.1
		ELSE If @ObjectTypeId =4
		BEGIN --2.2
			Insert into @FileName(FileId,OldFileName,ParentId,ObjectTypeId)
			Select @Id, @tempFileName + '/' + FileName,@ParentId,@ObjectTypeId from SIStyle Where Id=@Id
			IF EXISTS(Select * from SIStyle T inner join HSStructure S on T.Id=S.Id
			Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() 
					AND LOWER(FileName)=(Select LOWER(FileName) From SIStyle Where Id=@Id AND Status!= dbo.GetDeleteStatus()))
			BEGIN		
				SET @FileExists =1
				 Select @DestinationFileId = T.Id from SIStyle T inner join HSStructure S on T.Id=S.Id
			Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() 
					AND LOWER(FileName)=(Select LOWER(FileName) From SIStyle Where Id=@Id AND Status!= dbo.GetDeleteStatus())
			END
		END --2.2
		ELSE If @ObjectTypeId =9 
		BEGIN --2.2.1
			Insert into @FileName(FileId,OldFileName,ParentId,ObjectTypeId)
			Select @Id, @tempFileName + '/' + FileName,@ParentId,@ObjectTypeId from COFile Where ContentId=@Id
			
		END --2.2.1
		ELSE If @ObjectTypeId =33
		BEGIN --2.3
			Insert into @FileName(FileId,OldFileName,ParentId,ObjectTypeId)
			Select @Id, @tempFileName + '/' + FileName,@ParentId,@ObjectTypeId from COFile Where ContentId=@Id
			IF EXISTS(SELECT * FROM COFile F INNER JOIN COContent C ON C.Id = F.ContentId 
						WHERE C.ParentId = @DestinationNodeId AND LOWER(FileName)=(Select LOWER(FileName) From COFile Where ContentId=@Id))
			SET @FileExists =1
		END --2.3
		ELSE If @ObjectTypeId =35
		BEGIN --2.4
			Insert into @FileName(FileId,OldFileName,ParentId,ObjectTypeId)
			Select @Id, @tempFileName + '/' + FileName,@ParentId,@ObjectTypeId from SIScript Where Id=@Id
		IF EXISTS(Select * from SIScript T inner join HSStructure S on T.Id=S.Id
		Where S.ParentId=@DestinationNodeId AND Status!= dbo.GetDeleteStatus() 
				AND LOWER(FileName)=(Select LOWER(FileName) From SIScript Where Id=@Id AND Status!= dbo.GetDeleteStatus()))
			SET @FileExists =1
		END --2.4



		IF (@FileExists =1)
		BEGIN --3
			IF(@OverWrite=1)
			BEGIN --4
				Select @DLftValue = LftValue,@DRgtValue =RgtValue from HSStructure Where Id = @DestinationNodeId
				--Delete the file which already exists in the corresponding table
				IF @ObjectTypeId =3
				BEGIN --5
					Update SITemplate  SET Status= dbo.GetDeleteStatus() 
					FROM HSStructure S
					Where SITemplate.Id=@Id AND Status!= dbo.GetDeleteStatus()
						  AND S.Id = SITemplate.Id AND S.LftValue Between @DLftValue And @DRgtValue
				END --5
				ELSE If @ObjectTypeId =4
				BEGIN--6
				--Select @DestinationFileId
					Update SIStyle  SET Status= 7 
					Where SIStyle.Id=@DestinationFileId
					
				END--6
				ELSE If @ObjectTypeId =9 
				BEGIN--7
					Update COFile  SET RelativePath = replace(dbo.GetVirtualPath(S.Id),'/'+ FileName,'') 
					FROM HSStructure S
					Where LftValue > @PLft and RgtValue < @PRgt
					
					Update COContent   SET COContent.Status= dbo.GetDeleteStatus()
					FROM COContentStructure S
					Where COContent.Id=@Id AND COContent.Status!= dbo.GetDeleteStatus()
						  AND S.Id = COContent.Id AND S.LftValue Between @DLftValue And @DRgtValue
				END--7
				ELSE If @ObjectTypeId =33
				BEGIN--7
					Update COFile  SET RelativePath = replace(dbo.GetVirtualPath(S.Id),'/'+ FileName,'') 
					FROM HSStructure S
					Where LftValue > @PLft and RgtValue < @PRgt
					
					Update COContent   SET COContent.Status= dbo.GetDeleteStatus()
					FROM COImageStructure S
					Where COContent.Id=@Id AND COContent.Status!= dbo.GetDeleteStatus()
						  AND S.Id = COContent.Id AND S.LftValue Between @DLftValue And @DRgtValue
				END--7
				ELSE If @ObjectTypeId =35
				BEGIN--8
					Update SIScript  SET Status= dbo.GetDeleteStatus() 
					FROM HSStructure S
					Where SIScript.Id=@Id AND Status!= dbo.GetDeleteStatus()
						  AND S.Id = SIScript.Id AND S.LftValue Between @DLftValue And @DRgtValue
				END--8
				
				--Move the new File
				EXEC HierarchyNode_MoveTree @SourceNodeId = @Id,@DestinationNodeId =@DestinationNodeId
			END--4
			ELSE
			BEGIN --9 -- If the file/image already exist in the Destination directory
				--Auto Renaming of files
				IF(@ObjectType IS NOT NULL)
					EXEC SqlDirectoryProvider_UpdateFileName @DestinationNodeId=@DestinationNodeId, @FileId=@Id, @ObjectType = @ObjectType
				ELSE
					EXEC SqlDirectoryProvider_UpdateFileName @DestinationNodeId=@DestinationNodeId, @FileId=@Id
				 
			
			--- Change the Relative path... above sp change only file name.
				IF(@ObjectTypeId = 9 OR @ObjectTypeId = 33)
						Update COFile  SET RelativePath = dbo.GetVirtualPath(@DestinationNodeId)
						FROM COFile S
						Where S.ContentId=@Id
						
				UPDATE COContent 
					Set ParentId = @DestinationNodeId,
						ModifiedBy = @ModifiedBy,
						ModifiedDate = GETUTCDATE()
					where Id = @Id
			END --9
		END --3	
		ELSE
		BEGIN --10
			IF(@ObjectTypeId = 7 OR @ObjectTypeId = 9 OR @ObjectTypeId = 33)
			BEGIN
				IF(@ObjectTypeId = 9 OR @ObjectTypeId = 33)
					Update COFile  SET RelativePath = dbo.GetVirtualPath(@DestinationNodeId)
					--SELECT dbo.GetVirtualPath(@DestinationNodeId)
					FROM COFile S
					Where S.ContentId=@Id

				--SELECT @Id
				UPDATE COContent 
				Set ParentId = @DestinationNodeId,
					ModifiedBy = @ModifiedBy,
					ModifiedDate = GETUTCDATE()
				where Id = @Id
				
			END
			ELSE
			BEGIN
				EXEC HierarchyNode_MoveTree @SourceNodeId = @Id,@DestinationNodeId =@DestinationNodeId
			END	
			
		END  --10	
		SET @Count = @Count + 1
	END	--2
	
	IF(@ObjectType = 33)
		Select @tempFileName = VirtualPath from COImageStructure Where Id =@DestinationNodeId
	ELSE IF	(@ObjectType = 9)
		Select @tempFileName = VirtualPath from COFileStructure Where Id =@DestinationNodeId
	ELSE
		Select @tempFileName = VirtualPath from SISiteDirectory Where Id =@DestinationNodeId
	
	Update @FileName Set NewFileName = @tempFileName +'/' + T.FileName
	FROM SITemplate T 
	Where T.Id=FileId
	Update @FileName Set NewFileName = @tempFileName +'/' + s.FileName
	FROM SIStyle s 
	Where s.Id=FileId
	Update @FileName Set NewFileName = @tempFileName +'/' + s.FileName
	FROM SIScript s 
	Where s.Id=FileId
	Update @FileName Set NewFileName = @tempFileName +'/' + F.FileName
	FROM COFile F 
	Where F.ContentId=FileId
	
	SELECT FileId Id,OldFileName,NewFileName,ParentId,ObjectTypeId FROM @FileName	

END --1


GO

ALTER PROCEDURE [dbo].[cache_RefreshFlatPriceSets]
AS
BEGIN

	Declare @flatPriceSets table
		(SkuId uniqueidentifier, ProductId uniqueidentifier, 
			MinQuantity float, MaxQuantity float, EffectivePrice money, ListPrice money, 
				PriceSetId uniqueidentifier, CustomerGroupId uniqueidentifier, IsSale bit, SiteId uniqueidentifier)

	Insert into @flatPriceSets			

			SELECT PS.Id AS SkuId, P.Id AS ProductId, PSD.MinQuantity, CASE PSD.MaxQuantity WHEN 0 THEN 1.79E308 ELSE PSD.MaxQuantity END AS MaxQuantity, 
								  (100 - PSD.DiscountAmount) / 100 * PS.ListPrice AS EffectivePrice, PS.ListPrice, PSPT.PriceSetId,
									CGPS.CustomerGroupId, PSS.IsSale, PSS.SiteId
			FROM         dbo.PSPriceSetProductType AS PSPT INNER JOIN
								  dbo.PRProduct AS P ON PSPT.ProductTypeId = P.ProductTypeID INNER JOIN
								  dbo.PRProductSKU AS PS ON PS.ProductId = P.Id INNER JOIN
								  dbo.PSPriceSetDiscount AS PSD ON PSD.PriceSetId = PSPT.PriceSetId inner join
								  dbo.PSPriceSet PSS on PSPT.PriceSetId = PSS.Id inner join
								  dbo.CSCustomerGroupPriceSet CGPS on PSS.Id = CGPS.PriceSetId 
			Where  cast(convert(varchar(12),GetUTCDate(),101) as datetime)
					 BETWEEN COALESCE(cast(convert(varchar(12),PSS.StartDate,101) as datetime),
									cast(convert(varchar(12),GetUTCDate(),101) as datetime)) 
							AND COALESCE(cast(convert(varchar(12),PSS.EndDate,101) as datetime),
											cast(convert(varchar(12),GetUTCDate(),101) as datetime))	

			UNION
			
			SELECT PS.Id AS SkuId, P.Id AS ProductId, PSD.MinQuantity, CASE PSD.MaxQuantity WHEN 0 THEN 1.79E308 ELSE PSD.MaxQuantity END AS MaxQuantity, 
								  (100 - PSD.DiscountAmount) / 100 * PS.ListPrice AS EffectivePrice, PS.ListPrice, PSPT.PriceSetId,
									CGPS.CustomerGroupId, PSS.IsSale, PSS.SiteId
			FROM         dbo.PSPriceSetSKU AS PSPT INNER JOIN
								  dbo.PSPriceSet PSS on PSPT.PriceSetId = PSS.Id inner join
								  dbo.PRProductSKU AS PS ON PS.Id = PSPT.SKUId INNER JOIN
								  dbo.PRProduct AS P ON PS.ProductId = P.Id INNER JOIN
								  dbo.PSPriceSetDiscount AS PSD ON PSD.PriceSetId = PSPT.PriceSetId inner join
								  dbo.CSCustomerGroupPriceSet CGPS on PSS.Id = CGPS.PriceSetId 
			Where  cast(convert(varchar(12),GetUTCDate(),101) as datetime)
					 BETWEEN COALESCE(cast(convert(varchar(12),PSS.StartDate,101) as datetime),
									cast(convert(varchar(12),GetUTCDate(),101) as datetime)) 
							AND COALESCE(cast(convert(varchar(12),PSS.EndDate,101) as datetime),
											cast(convert(varchar(12),GetUTCDate(),101) as datetime))	
									
			UNION

			SELECT     PS.Id AS SkuId, P.Id AS ProductId, PSMS.MinQuantity, 
								  CASE PSMS.MaxQuantity WHEN 0 THEN 1.79E308 ELSE PSMS.MaxQuantity END AS MaxQuantity, PSMS.Price AS EffectivePrice, 
								  PS.ListPrice,PSMS.PriceSetId,CGPS.CustomerGroupId,PSS.IsSale, PSS.SiteId
			FROM         dbo.PSPriceSetManualSKU AS PSMS INNER JOIN
							  PSPriceSet PSS on PSMS.PriceSetId = PSS.Id inner join
								  dbo.PRProductSKU AS PS ON PSMS.SKUId = PS.Id INNER JOIN
								  dbo.PRProduct AS P ON PS.ProductId = P.Id
								inner join dbo.CSCustomerGroupPriceSet CGPS on PSS.Id = CGPS.PriceSetId 
			Where  cast(convert(varchar(12),GetUTCDate(),101) as datetime)
					 BETWEEN COALESCE(cast(convert(varchar(12),PSS.StartDate,101) as datetime),
									cast(convert(varchar(12),GetUTCDate(),101) as datetime)) 
							AND COALESCE(cast(convert(varchar(12),PSS.EndDate,101) as datetime),
											cast(convert(varchar(12),GetUTCDate(),101) as datetime))	

			UNION

			SELECT     PS.Id AS SkuId, P.Id AS ProductId, PSMS.MinQuantity, 
								  CASE PSMS.MaxQuantity WHEN 0 THEN 1.79E308 ELSE PSMS.MaxQuantity END AS MaxQuantity, PSMS.Price AS EffectivePrice, 
								  PS.ListPrice,PSMS.PriceSetId,CGPS.CustomerGroupId, PSS.IsSale, PSS.SiteId
			FROM         dbo.PSPriceSetManualSKU AS PSMS INNER JOIN
							 dbo.PSPriceSet PSS on PSMS.PriceSetId = PSS.Id inner join
								  dbo.PRProductSKU AS PS ON PSMS.SKUId = PS.Id INNER JOIN
								  dbo.PRProduct AS P ON PS.ProductId = P.Id inner join 
								  dbo.CSCustomerGroupPriceSet CGPS on PSS.Id = CGPS.PriceSetId 
			Where  cast(convert(varchar(12),GetUTCDate(),101) as datetime)
					 BETWEEN COALESCE(cast(convert(varchar(12),PSS.StartDate,101) as datetime),
									cast(convert(varchar(12),GetUTCDate(),101) as datetime)) 
							AND COALESCE(cast(convert(varchar(12),PSS.EndDate,101) as datetime),
											cast(convert(varchar(12),GetUTCDate(),101) as datetime))	

			truncate table cache_FlatPriceSets
			insert into cache_FlatPriceSets
				select * from @flatPriceSets

END
GO
ALTER PROCEDURE OrderItem_GetCouponCodeDiscounts
    (
      @OrderItemId UNIQUEIDENTIFIER 
    )
AS 
    SELECT  Id ,
            CouponCodeId ,
            OrderItemId ,
            Amount
    FROM    dbo.OROrderItemCouponCode
    WHERE   OrderItemId = @OrderItemId
            
    


GO
PRINT 'Altering procedure [CommerceReport_OrderTax]'
GO
ALTER PROCEDURE [dbo].[CommerceReport_OrderTax]
(	
	@startDate datetime,@endDate datetime,
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS
	BEGIN



declare @tmpStateTax table(
	StateId uniqueidentifier,
	CountryName nvarchar(255),
	CountryCode char(2),
	StateName nvarchar(255),
	StateCode char(3),
	City nvarchar(255),
	Zip varchar(25),
	TotalSale decimal(18,2),
	TotalTax decimal(18,2),
	TaxableTotal decimal(18,2),
	NumberOfOrders int
	)

SET @startDate =dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
IF @endDate is not null
SET @endDate = dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
ELSE
	SET @endDate = GetUTCDate()
	
INSERT INTO @tmpStateTax
SELECT
	GS.Id as StateId
	,GC.CountryName
	,GC.CountryCode
	,GS.State AS StateName
	,GS.StateCode
	,GA.City
	,GA.Zip
	,sum(OS.SubTotal) TotalSale
	,CONVERT(decimal(18,2),SUM(OS.Tax) -SUM(isnull(R.TAX,0))) AS Tax
	,SUM(OS.TaxableTotal) - SUM(isnull(R.TaxableTotal,0)) AS TaxableSale
	,Count(Distinct O.Id) as NumberOfOrders
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	LEFT JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	LEFT JOIN GLAddress GA ON OSA.AddressId = GA.Id
	LEFT JOIN GLState GS ON GS.Id = GA.StateId
	LEFT JOIN GLCountry GC ON GC.Id = GS.CountryId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND 
	O.OrderDate >= @startDate
	AND O.OrderDate <= @endDate
	AND O.OrderStatusId in (1,2,11)
	AND O.SiteId = @ApplicationId
GROUP BY
	GS.StateCode,GS.State,GS.Id, GC.CountryName, GC.CountryCode,GA.City,GA.Zip

Set @virtualCount = @@ROWCOUNT

IF @sortBy is null
	Select * from @tmpStateTax Order by CountryCode ASC, StateName ASC
ELSE IF (@sortBy)='statename desc'
	Select * from @tmpStateTax Order by StateName DESC
ELSE IF (@sortBy)='statecode asc'
	Select * from @tmpStateTax Order by StateCode ASC
ELSE IF (@sortBy)='statecode desc'
	Select * from @tmpStateTax Order by StateCode DESC
ELSE IF (@sortBy)='totaltax asc'
	Select * from @tmpStateTax Order by TotalTax ASC
ELSE IF (@sortBy)='totaltax desc'
	Select * from @tmpStateTax Order by TotalTax DESC
ELSE IF (@sortBy)='numberoforders asc'
	Select * from @tmpStateTax Order by NumberOfOrders ASC
ELSE IF (@sortBy)='numberoforders desc'
	Select * from @tmpStateTax Order by NumberOfOrders DESC
END
GO
PRINT 'Altering procedure [CommerceReport_SalesTaxByCategory]'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesTaxByCategory]
(	
	@startDate datetime,@endDate datetime,
	@pageSize int = 10,@pageNumber int, 
	@maxReturnCount int = null, 
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS
	BEGIN
IF @endDate IS NULL
	SET @endDate = GetUTCDate()


declare @tmpCityTax table(
	Id int,
	City nvarchar(255),
	--County nvarchar(255),
	ZipCode nvarchar(50),
	TaxCategory nvarchar(255),
	TotalTax decimal(18,2),
	NumberOfOrders int,
	StateId uniqueidentifier,
	CountryId uniqueidentifier
	)

INSERT INTO @tmpCityTax
SELECT
	ROW_NUMBER() over (order by GA.City),
	GA.City City,
	--GA.County,
	GA.Zip,
	ISNULL(PT.Title,'--'),
	CONVERT(decimal(18,2),SUM(OS.Tax) -SUM(isnull(R.TAX,0))) AS Tax,
	Count(Distinct O.Id) as NumberOfOrders,
	GA.StateId,
	GS.CountryId
FROM
OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLState GS ON GS.Id = GA.StateId
	INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
	INNER JOIN PRProductSKU PS ON PS.Id = OI.ProductSKUId
	INNER JOIN PRProduct P ON P.Id = PS.ProductID
	LEFT JOIN PRTaxCategory PT ON PT.Id = P.TaxCategoryId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId
GROUP BY
	GS.CountryId, GA.City,GA.County,GA.Zip, GA.StateId, PT.Title


declare @tmpStateTax table(
	StateId uniqueidentifier,
	StateName nvarchar(255),
	StateCode char(3),
	TaxCategory nvarchar(255),
	TotalTax decimal(18,2),
	CountryId uniqueidentifier
	)

INSERT INTO @tmpStateTax
SELECT
	GS.Id as StateId,
	GS.State AS StateName,
	GS.StateCode,
	ISNULL(PT.Title,'--'),
	CONVERT(decimal(18,2),SUM(OI.Tax)-SUM(isnull(R.TAX,0))) AS Tax,
	GS.countryId
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLState GS ON GS.Id = GA.StateId
	INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
	INNER JOIN PRProductSKU PS ON PS.Id = OI.ProductSKUId
	INNER JOIN PRProduct P ON P.Id = PS.ProductID
	LEFT JOIN PRTaxCategory PT ON PT.Id = P.TaxCategoryId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId
GROUP BY
	GS.StateCode,GS.State,GS.Id, GS.countryId,P.TaxCategoryId, PT.Title

declare @tmpCountryTax table(
	CountryId uniqueidentifier,
	CountryName nvarchar(255),
	CountryCode char(3),
	TotalCountryTax decimal(18,2)
	)

INSERT INTO @tmpCountryTax
SELECT
	GC.Id as CountryId,
	GC.CountryName AS CountryName,
	GC.CountryCode,
	CONVERT(decimal(18,2),SUM(OI.Tax)-SUM(isnull(R.TAX,0))) AS TotalCountryTax
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLCountry GC ON GC.Id = GA.CountryId
	INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId
GROUP BY
	GC.CountryCode,GC.CountryName,GC.Id

Set @virtualCount = @@ROWCOUNT

declare @tmpPgdCountryTax table(
	CountryId uniqueidentifier,
	CountryName nvarchar(255),
	CountryCode char(2),
	TotalCountryTax decimal(18,2),
	RowNumber int
	)

IF @sortBy is null or (@sortBy)='countryname asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryName asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryName ASC 

	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateName ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='countryname desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryName desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryName DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateName DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='countrycode asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryCode asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryCode ASC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateCode ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='countrycode desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryCode desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryCode DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateCode DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='totalcountrytax asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by TotalCountryTax asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.TotalCountryTax ASC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by TotalTax ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='totalcountrytax desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by TotalCountryTax desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.TotalCountryTax DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by TotalTax DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End


Select count(*) from @tmpCountryTax

END

GO
PRINT 'Altering procedure [CommerceReport_OrderTaxByCountry]'
GO

ALTER PROCEDURE [dbo].[CommerceReport_OrderTaxByCountry]
(	
	@startDate datetime,@endDate datetime,
	@pageSize int = 10,@pageNumber int, 
	@maxReturnCount int = null, 
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS
	BEGIN
--			declare @startDate datetime
--			declare @endDate datetime
--			declare @virtualCount int
--			declare @sortBy nvarchar(100)


--			set @sortBy = 'SaleTotal desc'
--			set @returnTopCount = 4

--		set	@startDate='06-25-2009'
--		set @endDate='08-25-2009'
IF @endDate IS NULL
	SET @endDate = GetUTCDate()
	
declare @tmpCityTax table(
	Id int,
	City nvarchar(255),
	--County nvarchar(255),
	ZipCode nvarchar(50),
	TotalTax decimal(18,2),
	NumberOfOrders int,
	StateId uniqueidentifier,
	CountryId uniqueidentifier
	)

INSERT INTO @tmpCityTax
SELECT
	ROW_NUMBER() over (order by GA.City),
	GA.City City,
	--GA.County,
	GA.Zip,
	CONVERT(decimal(18,2),SUM(OS.Tax) -SUM(isnull(R.TAX,0))) AS Tax,
	Count(Distinct O.Id) as NumberOfOrders,
	GA.StateId,
	GS.CountryId
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLState GS ON GS.Id = GA.StateId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId
GROUP BY
	GS.CountryId, GA.City,GA.County,GA.Zip, GA.StateId

declare @tmpStateTax table(
	StateId uniqueidentifier,
	StateName nvarchar(255),
	StateCode char(3),
	TotalTax decimal(18,2),
	NumberOfOrders int,
	CountryId uniqueidentifier
	)

INSERT INTO @tmpStateTax
SELECT
	GS.Id as StateId,
	GS.State AS StateName,
	GS.StateCode,
	CONVERT(decimal(18,2),SUM(OS.Tax) -SUM(isnull(R.TAX,0))) AS Tax,
	Count(Distinct O.Id) as NumberOfOrders,
	GS.countryId
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLState GS ON GS.Id = GA.StateId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId
GROUP BY
	GS.StateCode,GS.State,GS.Id, GS.countryId

declare @tmpCountryTax table(
	CountryId uniqueidentifier,
	CountryName nvarchar(255),
	CountryCode char(3),
	TotalCountryTax decimal(18,2),
	NumberOfCountryOrders int
	)

INSERT INTO @tmpCountryTax
SELECT
	GC.Id as CountryId,
	GC.CountryName AS CountryName,
	GC.CountryCode,
	CONVERT(decimal(18,2),SUM(OS.Tax) -SUM(isnull(R.TAX,0))) AS TotalCountryTax,
	Count(Distinct O.Id) as NumberOfCountryOrders
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLCountry GC ON GC.Id = GA.CountryId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11,4)
	AND O.SiteId = @ApplicationId
GROUP BY
	GC.CountryCode,GC.CountryName,GC.Id

Set @virtualCount = @@ROWCOUNT

declare @tmpPgdCountryTax table(
	CountryId uniqueidentifier,
	CountryName nvarchar(255),
	CountryCode char(2),
	TotalCountryTax decimal(18,2),
	NumberOfCountryOrders int,
	RowNumber int
	)

IF @sortBy is null or (@sortBy)='countryname asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryName asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryName ASC 

	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateName ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='countryname desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryName desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryName DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateName DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City Desc
End
ELSE IF (@sortBy)='countrycode asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryCode asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryCode ASC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateCode ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City ASC
End
ELSE IF (@sortBy)='countrycode desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by CountryCode desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.CountryCode DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by StateCode DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by City DESC
End
ELSE IF (@sortBy)='totalcountrytax asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by TotalCountryTax asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.TotalCountryTax ASC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by TotalTax ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by TotalTax ASC
End
ELSE IF (@sortBy)='totalcountrytax desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by TotalCountryTax desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.TotalCountryTax DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by TotalTax DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by TotalTax DESC
End
ELSE IF (@sortBy)='numberofcountryorders asc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by NumberOfCountryOrders asc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.NumberOfCountryOrders ASC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by NumberOfOrders ASC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by NumberOfOrders ASC
End
ELSE IF (@sortBy)='numberofcountryorders desc'
Begin
	Insert into @tmpPgdCountryTax
	Select A.*   
    FROM  
     (  
     Select *,  ROW_NUMBER() OVER(Order by NumberOfCountryOrders desc) as RowNumber  
      from @tmpCountryTax  
     ) A  
    where A.RowNumber <= Isnull(@maxReturnCount,@virtualCount) and  
      A.RowNumber >= (@pageSize * @pageNumber) - (@pageSize -1) AND A.RowNumber <= @pageSize * @pageNumber  
    Order by A.NumberOfCountryOrders DESC
	
	select * from @tmpPgdCountryTax
	Select * from @tmpStateTax where CountryId in (select CountryId from @tmpPgdCountryTax) Order by NumberOfOrders DESC
	select * from @tmpCityTax Where CountryId in (select CountryId from @tmpPgdCountryTax) Order by NumberOfOrders DESC
End

Select count(*) from @tmpCountryTax

END


GO
PRINT 'Altering procedure OrderItem_SaveCouponCodeDiscount'
GO
ALTER PROCEDURE OrderItem_SaveCouponCodeDiscount
    (
      @OrderItemId UNIQUEIDENTIFIER ,
      @CouponCodeId UNIQUEIDENTIFIER ,
      @Amount MONEY
    )
AS 
    DELETE  FROM OROrderItemCouponCode
    WHERE   OrderItemId = @OrderItemId
            AND CouponCodeId = @CouponCodeId

    INSERT  INTO dbo.OROrderItemCouponCode
            ( Id ,
              OrderItemId ,
              CouponCodeId ,
              Amount
            )
    VALUES  ( NEWID() , -- Id - uniqueidentifier
              @OrderItemID , -- OrderItemId - uniqueidentifier
              @CouponCodeId , -- OrderCouponCodeId - uniqueidentifier
              @Amount-- Amount - money
            )
GO
-- Copyright © 2000-2006 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline's express written 
-- permission is prohibited.
 
-- Purpose: Update the status of the PriceSet as deleted
-- Author: Martin M V
-- Created Date: 02-04-2009
-- Created By: Martin M V
--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************
-- Modified Date:
-- Modified By: Prakash
-- Reason:
-- Changes:

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].PriceSet_Delete 
--          @Id = 'uniqueidentifier' not null,
--			@ModifiedBy	= 'uniqueidentifier' not null
--********************************************************************************
ALTER PROCEDURE [dbo].[PriceSet_Delete](
--********************************************************************************
-- Parameters
--********************************************************************************
		@Id					uniqueidentifier
)
AS
--********************************************************************************
-- Procedure Body
--********************************************************************************
DECLARE
	@DeleteStatus			int, @lft int, @rgt int,
	@Stmt					nvarchar(max)
--********************************************************************************
-- Procedure Body
--********************************************************************************
BEGIN
IF EXISTS (SELECT Id FROM PSPriceSet WHERE Id=@Id)
	BEGIN
	
		Set @Stmt ='Delete'

		IF ( (SELECT Count(*) FROM PSPriceSetDiscount Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('Price Set is being used in Price Set Discount||%s',16,1,@Stmt)  
			RETURN dbo.GetDataBaseErrorCode()  
		END 

		IF ( (SELECT Count(*) FROM PSPriceSetManualSKU Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('Price Set is being used in Manual Price Set SKU||%s',16,1,@Stmt)  
			RETURN dbo.GetDataBaseErrorCode()  
		END 

		IF ( (SELECT Count(*) FROM PSPriceSetSKU Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('Price Set is being used in Price Set SKU||%s',16,1,@Stmt)  
			RETURN dbo.GetDataBaseErrorCode()  
		END 

		IF ( (SELECT Count(*) FROM PSPriceSetProductType Where PriceSetId = @Id ) > 0)  
		BEGIN  
			RAISERROR('Price Set is being used in Price Set Product Type||%s',16,1,@Stmt)  
			RETURN dbo.GetDataBaseErrorCode()  
		END  

		DELETE  CSCustomerGroupPriceSet  WHERE PriceSetId = @Id
		DELETE  PSPriceSet WHERE Id = @Id
		IF @@ERROR <> 0
		  BEGIN
			 RAISERROR('DBERROR||%s',16,1,@Stmt)
			 RETURN dbo.GetDataBaseErrorCode()
		  END
      END     
END
GO
PRINT 'Altering procedure Cache_GetSiteFlushUrls'

GO
ALTER PROCEDURE Cache_GetSiteFlushUrls
AS
BEGIN
	SELECT SiteId, Url, PublicSiteUrl FROM GLInvalidateCache
END

GO
PRINT 'Altering procedure Cache_SaveSiteFlushUrls'

GO
ALTER PROCEDURE Cache_SaveSiteFlushUrls
	@SiteUrls XML
AS
BEGIN

	DELETE FROM GLInvalidateCache

	INSERT INTO GLInvalidateCache (SiteId, Url, PublicSiteUrl)
			SELECT 
				T.C.value('SiteId[1]', 'UNIQUEIDENTIFIER'),
				T.C.value('Url[1]', 'NVARCHAR(MAX)'),
				T.C.value('PublicSiteUrl[1]', 'NVARCHAR(MAX)')
			 FROM @SiteUrls.nodes('(/DSUrlMappings/Mapping)') AS T(C)
END
GO
PRINT 'Altering procedure Comments_Save'

GO
ALTER PROCEDURE [dbo].[Comments_Save] 
(	
	@Id				uniqueidentifier=null OUT,
	@ObjectId		uniqueidentifier,
	@PostedBy		nvarchar(1024) = null,
	@Email			nvarchar(1024) = null,
	@Title			nvarchar(255) = null,
	@Comments		nvarchar(max) = null,	
	@ParentCommentId		uniqueidentifier = null,
	@Status			int,
	@ModifiedBy		uniqueidentifier = null,
	@ObjectTypeId	int,
	@PostedRating int = null,
	@IPAddress nvarchar(100)=null,
	@ApplicationId	uniqueidentifier = null
)
as

--********************************************************************************
-- Variable declaration
--********************************************************************************
DECLARE         

          @Stmt     varchar(25),
          @Error    int,
		  @NewId	uniqueidentifier,
		  @Now 		datetime 	
		 

--********************************************************************************
-- code
--********************************************************************************
BEGIN
IF (@ObjectId IS NOT NULL)
BEGIN		
	SET @Now = getutcdate()
	Select @NewId = newid()	
	
	IF @Email IS NOT NULL AND @Email != '' AND CHARINDEX('@', @Email) = 0 AND CHARINDEX('.', @Email) = 0
	BEGIN
		RAISERROR('INVALID||EMAIL', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END
	
	IF NOT EXISTS (SELECT * FROM BLComments WHERE Id = @Id)
	BEGIN
		Declare @LftValue int, @RgtValue int, @Level int
		
		IF @ParentCommentId IS NULL OR @ParentCommentId = '00000000-0000-0000-0000-000000000000'
			Select @LftValue = LftValue, @RgtValue = RgtValue, @Level = 0, @ParentCommentId = Id from BLComments Where LftValue =1 
		ELSE 
			Select @LftValue = LftValue, @RgtValue = RgtValue, @Level = Level + 1 from BLComments Where Id = @ParentCommentId
		
		IF @LftValue <=0
		begin
			RAISERROR('NOTEXISTS||ParentCommentId', 16, 1)
				RETURN dbo.GetBusinessRuleErrorCode()
		end
		
		print @LftValue
		
		Update BLComments Set LftValue = case when LftValue > @RgtValue Then LftValue + 2 ELSE LftValue End,
					RgtValue = case when RgtValue >= @RgtValue then RgtValue + 2 ElSE RgtValue End 
					
		SET @Stmt ='Adding Comments for a post'
        	Insert into BLComments (
				Id,
				PostedBy,
				Email,
				Comments,
				UserId,
				ParentId,
				ObjectId,
				Status,
				CreatedBy,
 				CreatedDate,
 				ObjectTypeId,
 				LftValue,
 				RgtValue,
 				Level,
 				ApplicationId
			)
      		Values
      		(	
				@NewId,
				@PostedBy,
				@Email,
				@Comments,
				@ModifiedBy,
				@ParentCommentId,
				@ObjectId,
				@Status,
				@ModifiedBy,
 				@Now,
 				@ObjectTypeId,
 				@RgtValue,
 				@RgtValue + 1,
 				@Level,
 				@ApplicationId
			)

		SELECT @Error = @@ERROR
 		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END
		if @PostedRating is not null and @PostedRating > 0
			begin
				Declare @ratingId uniqueidentifier
				set @ratingId =newid()
				Insert into BLRatings(Id, RatingValue,ObjectId,Status,IPAddress, CreatedDate, CreatedBy)
				values(@ratingId, @PostedRating,@ObjectId,@Status,@IPAddress,@Now,@ModifiedBy)
				
				insert into BLCommentRating(CommentId, RatingId, ObjectId)
				values(@NewId,@ratingId,@ObjectId)
				
			end
	END
	ELSE			-- Update
	BEGIN
		IF @Email = ''
			SET @Email = NULL

		SET @Stmt = 'Modify Comments for a post'
		UPDATE BLComments WITH (ROWLOCK)
			SET 
 				PostedBy		= @PostedBy,
				Email			= ISNULL(@Email, Email),
				Title			= @Title,
				Comments		= @Comments,
				Status			= ISNULL(@Status, Status),
				UserId			= ISNULL(@ModifiedBy, UserId),
				ParentId		= @ParentCommentId,
				ModifiedBy		= @ModifiedBy,
 				ModifiedDate	= @Now
		WHERE Id = @Id

		SELECT @Error = @@ERROR
 		IF @Error <> 0
		BEGIN
			RAISERROR('DBERROR||%s',16,1,@Stmt)
			RETURN dbo.GetDataBaseErrorCode()	
		END
	END	

	SELECT @Id = @NewId
END	
END
GO
PRINT 'Altering procedure Content_GetContent'

GO
ALTER PROCEDURE [dbo].[Content_GetContent] 
--********************************************************************************
-- Input Parameters
--********************************************************************************
(
	@Id uniqueidentifier
)
as
Begin
	
		select  C.Id,
				C.ApplicationId,
				C.Title,
				C.Description, 
				C.Text, 
				C.URL,
				C.URLType, 
				C.ObjectTypeId, 
				C.CreatedDate,
				C.CreatedBy,
				C.ModifiedBy,
				C.ModifiedDate, 
				C.Status,
				C.RestoreDate, 
				C.BackupDate, 
				C.StatusChangedDate,
				C.PublishDate, 
				C.ArchiveDate, 
				C.XMLString,
				C.BinaryObject, 
				C.Keywords,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				C.OrderNo, C.SourceContentId,
				C.ParentId,
				sDir.VirtualPath As FolderPath
				
		from COContent C
		inner join COContentStructure sDir on SDir.Id = C.ParentId
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
		Where C.Id = @Id-- and  C.Status <> dbo.GetDeleteStatus()
			  Order by OrderNo			  

	
END
GO
PRINT 'Altering procedure Content_GetContentsByDictionary'
GO
ALTER PROCEDURE [dbo].[Content_GetContentsByDictionary] 
--********************************************************************************
--Parameters
--********************************************************************************
(
@xmlGuids xml
)
AS
BEGIN
	DECLARE @tbContentIds TABLE (Id uniqueidentifier, ContentTypeId int)
	INSERT INTO @tbContentIds
	SELECT DISTINCT T.c.value('@Id','uniqueidentifier'), T.c.value('@ContentTypeID','int')
		FROM @xmlGuids.nodes('/Contents/Content') T(c)
		
	DECLARE @tbParentFolder TABLE(ContentId uniqueidentifier primary key, ParentId uniqueidentifier, FolderPath nvarchar(1000), ContentTypeID int)
	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COContentStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3

	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COImageStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3


	INSERT INTO @tbParentFolder
		SELECT DISTINCT C.Id, C.ParentId, CS.VirtualPath, C.ObjectTypeId FROM COFileStructure CS
			INNER JOIN COContent C ON CS.Id = C.ParentId 
			INNER JOIN @tbContentIds T ON C.Id = T.Id
		WHERE CS.Status != 3
		
	DECLARE @tbContentDefinitionIds TABLE(TemplateId uniqueidentifier, SiteId uniqueidentifier)
	INSERT INTO @tbContentDefinitionIds
		SELECT DISTINCT XMLString.value('(//contentDefinition/@TemplateId)[1]','uniqueidentifier'), C.ApplicationId  
			FROM COContent C INNER JOIN @tbParentFolder t on C.Id = t.ContentId 
		WHERE ObjectTypeId = 13
			
	
	SELECT Id,
		ApplicationId,
		Title,
		Description, 
		[Text], 
		URL,
		URLType, 
		ObjectTypeId, 
		CreatedDate,
		CreatedBy,
		ModifiedBy,
		ModifiedDate, 
		Status,
		RestoreDate, 
		BackupDate, 
		StatusChangedDate,
		PublishDate, 
		ArchiveDate, 
		XMLString,
		BinaryObject, 
		Keywords,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		OrderNo, SourceContentId,
		PA.ParentId ,
		PA.FolderPath  
	FROM COContent CO
		INNER JOIN @tbParentFolder PA ON CO.Id = PA.ContentId AND 
			(pa.ContentTypeID = 7 or pa.ContentTypeID = 13 ) and Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = CO.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = CO.ModifiedBy
	Order by OrderNo

	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata,             
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F ON C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COFileStructure PA ON C.ParentId = PA.Id	
				AND C.ObjectTypeId = 9 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN ON FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN ON MN.UserId = C.ModifiedBy
	Order by C.OrderNo
		
	SELECT  xF.Id,
			xF.ApplicationId,
			xF.Name,
			xF.XMLString,
			ISNULL(VX.XsltString, xF.XsltString) As XsltString,
			xF.Status,
			xF.Description,
			xF.XMLFileName,
			xF.XSLTFileName, xF.CreatedDate, xF.CreatedBy, 
			xF.ModifiedDate, xF.ModifiedBy, 
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName
	FROM COXMLForm xF
		INNER JOIN @tbContentDefinitionIds cD  On xF.Id  = cD.templateId
		LEFT JOIN VW_UserFullName FN on FN.UserId = xF.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = xF.ModifiedBy
		LEFT JOIN COXMLFormXslt VX on VX.SiteId = cD.SiteId And VX.XmlFormId = cD.templateId
		
	SELECT  C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo,	C.SourceContentId,
		C.ParentId,
		PA.VirtualPath  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN @tbContentIds T ON T.Id = C.Id
		INNER JOIN COImageStructure PA ON C.ParentId = PA.Id
				AND C.ObjectTypeId = 33 AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
		
	SELECT C.Id,
		C.ApplicationId,
		C.Title,
		C.Description, 
		C.[Text], 
		C.URL,
		C.URLType, 
		C.ObjectTypeId, 
		C.CreatedDate,
		C.CreatedBy,
		C.ModifiedBy,
		C.ModifiedDate, 
		C.Status,
		C.RestoreDate, 
		C.BackupDate, 
		C.StatusChangedDate,
		C.PublishDate, 
		C.ArchiveDate, 
		C.XMLString,
		C.BinaryObject, 
		C.Keywords  ,
		F.FileName,
 		F.FileSize,
		F.Extension,
		F.FolderName,
		F.Attributes, 
		F.RelativePath,
		F.PhysicalPath,
		F.MimeType,
		F.AltText,
		F.FileIcon, 
		F.DescriptiveMetadata, 
		F.ImageHeight, F.ImageWidth,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName,
		C.OrderNo, C.SourceContentId,
		dbo.GetEmptyGUID() ParentId ,
		'' FolderPath,
		V.BackgroundColor,
		V.DynamicStreaming,
		V.Height,
		V.IsUI,
		V.IsVideo,
		V.PlayerId,
		V.PlayerKey,
		V.VideoId,
		V.PlaylistId,
		V.VideoType,
		V.Width  
	FROM COContent C 
		INNER JOIN COFile F On C.Id = F.ContentId
		INNER JOIN COVideo V on V.ContentId=C.Id
		INNER JOIN @tbContentIds T ON T.Id = C.Id
				AND C.Status = 1
		LEFT JOIN VW_UserFullName FN on FN.UserId = C.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = C.ModifiedBy
	ORDER BY C.OrderNo
END
GO
PRINT	'Altering procedure Site_GetSiteGroups'
GO
ALTER PROCEDURE [dbo].[Site_GetSiteGroups]  
(  
	@SiteId			uniqueIdentifier = NULL,
	@Id				uniqueIdentifier = NULL
 )  
AS  
BEGIN  
	SELECT DISTINCT G.Id 
	       ,Title 
	       ,Description 
	       ,Status 
	       ,CreatedBy 
	       ,CreatedDate 
	       ,ModifiedBy 
	       ,ModifiedDate  
	       ,FN.UserFullName CreatedByFullName   
	       ,MN.UserFullName ModifiedByFullName
	FROM SISiteGroup SG
	INNER JOIN SIGroup G ON SG.GroupId = G.Id
	LEFT JOIN VW_UserFullName FN on FN.UserId = G.CreatedBy  
	LEFT JOIN VW_UserFullName MN on MN.UserId = G.ModifiedBy
	WHERE 
		(@SiteId IS NULL OR SG.SiteId = @SiteId) AND
		(@Id IS NULL OR G.Id = @Id)
END

GO
PRINT 'Altering procedure User_GetSocialToken'
GO
ALTER PROCEDURE dbo.User_GetSocialToken(
	@UserId uniqueidentifier,
	@SiteId uniqueidentifier)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT APIToken FROM USSocialUserProfile Where UserId=@UserId --AND SiteId=@SiteId
END

GO
PRINT 'Altering procedure Membership_CreateDefaultMembership'
GO
ALTER PROCEDURE [dbo].[Membership_CreateDefaultMembership]
--********************************************************************************
-- PARAMETERS
	@ApplicationId				uniqueidentifier,
	@ProductId					uniqueidentifier 
--********************************************************************************

AS
BEGIN

	-- Get all the users in the admin role of analytics
	-- Add membership for the users in Analytics ( insert into ussiteuser table)
	-- Add all the users into admin group ( insert into usmembergroup table)
	-- 
	--This requires because the group id will change on each installation. Here the group name should not be change.
	DECLARE @Administrator uniqueidentifier
	DECLARE @Id int

	SELECT @Administrator = Id FROM USGroup WHERE Title = 'Administrator'
	SET @Id = 16 --Role id of the Analytics administrator
	
	IF (@ProductId = 'CAB9FF78-9440-46C3-A960-27F53D743D89') -- Analytics
	BEGIN
		SET @Id = 16 --Role id of the Analytics administrator
		SELECT @Administrator = Id FROM USGroup WHERE Title = 'Administrator'
	END
	IF (@ProductId = 'CCF96E1D-84DB-46E3-B79E-C7392061219B')
	BEGIN
		SET @Id = 5 --Role id of the Commerce Site administrator
		SELECT @Administrator = Id FROM USGroup WHERE Title = 'Site Administrator'
	END
	IF (@ProductId = 'CD74F0C9-2BE3-48B1-B5B1-B8FD03EDDE7E')
	BEGIN
		SET @Id = 5 --Role id of the Marketier Site administrator
		SELECT @Administrator = Id FROM USGroup WHERE Title = 'Site Administrator'
	END

	
	DECLARE  @MemberType int -- 1 for user
	SET @MemberType = 1

	DECLARE @IsSystemUser int
	SET @IsSystemUser = 1

	DECLARE @ObjectTypeId int --
	SET @ObjectTypeId = NULL

	DECLARE @WithProfile bit
	SET @WithProfile = 0

	DECLARE @PageIndex int
	SET @PageIndex = 0

	DECLARE @PageSize int
	SET @PageSize = 1


	--Gets all the users present in the Administrator role in Analytics
	CREATE TABLE #temp
	(
		AppId uniqueidentifier,
		Member uniqueidentifier,
		MemType smallint,
		GrpId	uniqueidentifier
	)	

	INSERT INTO #temp
	SELECT DISTINCT @ApplicationId,Id,1,@Administrator
	FROM [User_GetUsersInRole](@Id, @MemberType, NULL, @IsSystemUser, @ObjectTypeId, @ProductId)
	
	INSERT INTO USMemberGroup
	SELECT * FROM #temp

	/*
		This is to make the memer
	*/
		
	 --This will delete the duplicate entries in the db.  
	   DELETE FROM dbo.USSiteUser  
	   FROM #temp T  
	   WHERE UserId = T.AppId AND   
			SiteId = @ApplicationId AND  
			ProductId = @ProductId 

	
	INSERT INTO USSiteUser
	SELECT   AppId,Member,1,@ProductId FROM #temp
	
	DROP TABLE #temp

	IF (@@ERROR <> 0)
	BEGIN
		RAISERROR ('DBERROR||%s', 16, 1, 'Failed')
		RETURN dbo.GetDataBaseErrorCode()
	END
	
	RETURN @@ROWCOUNT
END
--********************************************************************************
GO
PRINT 'Altering procedure RenderingDevice_GetCurrentRenderingDevice'
GO
ALTER PROCEDURE [dbo].[RenderingDevice_GetCurrentRenderingDevice](
	@ApplicationId uniqueidentifier = null,
	@Width varchar(10))
AS
BEGIN
Declare @Width_Local int 
If(@Width IS NOT NULL AND @Width <> '')
	SET @Width_Local = @Width
ELSE
	SET @Width_Local = 800
	
		
	SELECT G.Id,
		G.Title,
		G.Description,
		G.Status,
		G.CreatedBy,
		G.CreatedDate,
		G.ModifiedBy,
		G.ModifiedDate,
		G.MinWidth,
		G.MaxWidth,
		G.PreviewWidth,
		G.PreviewHeight,
		G.IsDefault,
		G.IsSystem,
		FN.UserFullName CreatedByFullName,
		MN.UserFullName ModifiedByFullName 
	FROM GLDevice G
		LEFT JOIN VW_UserFullName FN on FN.UserId = G.CreatedBy
		LEFT JOIN VW_UserFullName MN on MN.UserId = G.ModifiedBy
	WHERE (@ApplicationId is null or ApplicationId in (Select SiteId from GetVariantSites(@ApplicationId)))
		AND (@Width_Local BETWEEN G.MinWidth AND G.MaxWidth)
END
GO
PRINT 'Altering procedure Campaign_GetCampaignsBasedonNextActor'
GO
ALTER PROCEDURE [dbo].[Campaign_GetCampaignsBasedonNextActor]    
(    
 @Id uniqueidentifier = null,    
 @ApplicationId uniqueidentifier = null,    
 @ProductId uniqueidentifier = null,   
 @ObjectTypeId int = null,  
 @UserId uniqueidentifier = null ,
 @Type int = 1 
)    
AS    
Begin    
	Set @Type = isnull(@Type,1)
--  Declare @Id uniqueidentifier   
--  Declare @ApplicationId uniqueidentifier   
--  Declare @ProductId uniqueidentifier   
--  Declare @ObjectTypeId int  
--  Declare @UserId uniqueidentifier  
  
  Declare @lastLoginDate DateTime, @lastActionedDate DateTime  
  SELECT Top 1 @lastLoginDate = LastLoginDate FROM USMembership Order by LastLoginDate desc  
  set @lastLoginDate = isnull(@lastLoginDate, getUTCDate())  
  
  SELECT Top 1 @lastActionedDate = ActionedDate FROM  WFWorkflowExecution Order by ActionedDate desc  
  set @lastActionedDate = isnull(@lastActionedDate, getUTCDate())  
  
  IF ( (datePart(day,@lastActionedDate) <> datePart(day,@lastLoginDate)) OR   
  (datePart(month,@lastActionedDate) <> datePart(month,@lastLoginDate)) OR   
  (datePart(year,@lastActionedDate) <> datePart(year,@lastLoginDate) ))     
  BEGIN  
  exec [dbo].Workflow_GetWorkflowExecutionToSkipScheduler  
  END  
  
  Select  cm.[Id]  
      ,cm.[Title]  
      ,cm.[Description]  
      ,cm.[CampaignGroupId]  
      ,cm.[Status]  
      ,cm.[SenderName]  
      ,cm.[SenderEmail]  
      ,A.[CostPerEmail]  
      ,cm.[ConfirmationEmail]  
      ,cm.[CreatedBy]  
      ,cm.[CreatedDate]  
      ,cm.[ModifiedBy]  
      ,cm.[ModifiedDate]  
      ,cm.[ApplicationId]  
      ,A.[ScheduleId]  
      ,A.[WorkflowStatus]  
      ,A.[UniqueRecipientsOnly]  
      ,A.[AutoUpdateLists]  
      ,A.[LastPublishDate]  
      ,A.[WorkflowId]  
      ,A.[AuthorId]   
     ,na.ObjectId     
     ,na.ObjectTypeId    
     ,na.SequenceId    
     ,na.NextActorId    
     ,na.NextActorType   
	 ,A.[SendToNotTriggered]
	 ,cm.[SenderNameProperty]
	 ,cm.[SenderEmailProperty]
  From MKCampaign  cm   
 INNER JOIN MKCampaignAdditionalInfo A on cm.Id = A.CampaignId,  
  WFNextActor na, WFWorkflowExecution ex, WFWorkflow wf  
  Where cm.Type = @Type AND cm.Id = IsNull(@Id, cm.Id) AND cm.ApplicationId =  Isnull(@ApplicationId, cm.ApplicationId) AND A.WorkflowId  = na.WorkflowId AND  
  na.ObjectId = cm.Id AND na.ObjectTypeId =  Isnull(@ObjectTypeId, na.ObjectTypeId) AND  
  ex.WorkflowId = na.WorkflowId  AND ex.ObjectId  = na.ObjectId AND ex.ObjectTypeId = na.ObjectTypeId AND ex.Completed = 0 AND  
     wf.ApplicationId = Isnull(@ApplicationId, wf.ApplicationId) AND    
     wf.ProductId  = Isnull(@ProductId, wf.ProductId)   AND    
     wf.Id   = A.WorkflowId           
  AND  
  (  
  (@UserId Is null ) --- In case of admin  
  OR   
  (  
   (@UserId Is not null ) AND   
   (  
    (na.NextActorType = 1 AND na.NextActorId = Isnull(@UserId,na.NextActorId) )   
     OR  
    (na.NextActorType = 2 AND @UserId IN (  
      SELECT Id FROM [dbo].[User_GetUsersInGroup]   
      (@ProductId,@ApplicationId,na.NextActorId,1,@UserId,1)   
     )       
    )  
   )  
  )  
  )  
End
GO
PRINT 'Altering procedure Membership_CopyPermission'
GO
ALTER PROCEDURE [dbo].[Membership_CopyPermission]
(
	@ApplicationId	uniqueidentifier,
	@ObjectTypeId int,
	@SourceMemberId	uniqueidentifier,
	@TargetMemberId uniqueidentifier,
	@SourceMemberType smallint,
	@TargetMemberType smallint,
	@ObjectId	uniqueidentifier = NULL,
	@SourceMemberRoles xml	= NULL,
	@ProductId 	uniqueidentifier = NULL
)
AS
BEGIN
	
	IF(@SourceMemberRoles IS NOT NULL AND @ObjectId IS NOT NULL AND @SourceMemberId IS NULL)
	BEGIN
		/*This will copy the entire role information of the given object id to the target memberRole collection.*/

--		--Checks whether the given memberRole collection is having any member role to assign
--		DECLARE @Cnt int
--		SET @Cnt = 0
--		SELECT @Cnt = COUNT(*)
--		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)

		
		DELETE FROM [dbo].[USMemberRoles] 
		--FROM @SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
		WHERE	ApplicationId = @ApplicationId	AND
				ObjectId = @ObjectId	AND
				ObjectTypeId = @ObjectTypeId AND
				MemberType = ISNULL(@SourceMemberType,MemberType) AND
				ProductId = ISNULL(@ProductId, ProductId)	

		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	tab.col.value('(ApplicationId/text())[1]','uniqueidentifier') ApplicationId,
		   		tab.col.value('(MemberId/text())[1]','uniqueidentifier') MemberId,
		   		tab.col.value('(MemberType/text())[1]','smallint') MemberType,
		   		tab.col.value('(RoleId/text())[1]','int') RoleId,
				tab.col.value('(ObjectId/text())[1]','uniqueidentifier') ObjectId,	
				tab.col.value('(ObjectTypeId/text())[1]','int') ObjectTypeId,	
				tab.col.value('(Propagate/text())[1]','bit') Propagate,
				@ProductId ProductId
		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
		
	END
	ELSE IF(@SourceMemberRoles IS NOT NULL AND @ObjectId IS NULL AND @SourceMemberId IS NULL)
	BEGIN
		/*This will copy the given memberroles into the system.*/
		--Deletes all the user's roles those are to be updated/copied.
		--If any of the user is having any roles for the object id ,then remove that role for that user.
		DELETE FROM [dbo].[USMemberRoles] 
		FROM @SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
		WHERE	ApplicationId = tab.col.value('(ApplicationId/text())[1]','uniqueidentifier')	AND
				MemberId = tab.col.value('(MemberId/text())[1]','uniqueidentifier')	AND
				MemberType = tab.col.value('(MemberType/text())[1]','smallint')	AND
				--ObjectId = tab.col.value('(ObjectId/text())[1]','uniqueidentifier')	AND
				ObjectTypeId = ISNULL(tab.col.value('(ObjectTypeId/text())[1]','int'),ObjectTypeId)	AND
				ProductId = ISNULL(@ProductId, ProductId)
		
		--Inserts all the information for the user and objects.
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	tab.col.value('(ApplicationId/text())[1]','uniqueidentifier') ApplicationId,
		   		tab.col.value('(MemberId/text())[1]','uniqueidentifier') MemberId,
		   		tab.col.value('(MemberType/text())[1]','smallint') MemberType,
		   		tab.col.value('(RoleId/text())[1]','int') RoleId,
				tab.col.value('(ObjectId/text())[1]','uniqueidentifier') ObjectId,	
				tab.col.value('(ObjectTypeId/text())[1]','int') ObjectTypeId,	
				tab.col.value('(Propagate/text())[1]','bit') Propagate,
				@ProductId ProductId
		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
	END
	ELSE IF (@SourceMemberId IS NULL AND @TargetMemberId IS NULL AND @ObjectId IS NOT NULL AND @SourceMemberRoles IS NULL) --CopyObjectPermissionsToAllUsers
	BEGIN		
		
		/*This will copy all the users and roles to the given object id */
		
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	ApplicationId,
		   		MemberId,
		   		MemberType,
		   		RoleId,
				ObjectId,	
				ObjectTypeId,	
				Propagate,
				ProductId
		FROM	[dbo].[User_GetMembersPermissionByObject(@ApplicationId,@MemberType,@ObjectId,@ObjectTypeId,@Propagate,@ProductId)]
	END			 
	ELSE IF (@SourceMemberId IS NOT NULL AND @TargetMemberId IS NOT NULL AND @SourceMemberRoles IS NULL AND @ObjectId IS NULL) --
	BEGIN

		/* This will trasfer all the roles belonging to one member to another member within a given objectType and application scope*/
		--Deletes all the role information for that user on the given object type.
		DELETE FROM dbo.USMemberRoles 
		WHERE	ApplicationId = @ApplicationId AND
				MemberId = @TargetMemberId AND
				MemberType = @TargetMemberType	AND
				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId) AND
				ProductId = ISNULL(@ProductId, ProductId)
		
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		
		)
		SELECT 	ApplicationId,
		   		MemberId,
		   		MemberType,
		   		RoleId,
				ObjectId,	
				ObjectTypeId,	
				Propagate,
				@ProductId ProductId
		FROM	dbo.USMemberRoles
		WHERE	ApplicationId = @ApplicationId	AND
				MemberId = @SourceMemberId	AND
				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId)	AND
				MemberType = @SourceMemberType AND
				ProductId = ISNULL(@ProductId, ProductId)	
	END
	ELSE IF (@SourceMemberRoles IS NOT NULL AND @SourceMemberId IS NOT NULL AND @ObjectTypeId IS NOT NULL AND @ObjectId IS NULL)
	BEGIN
		--Checks whether the given memberRole collection is having any member role to assign
--		DECLARE @Cnt int
--		SET @Cnt = 0
--		SELECT @Cnt = COUNT(*)
--		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
--		
		
		--Deletes all the roles for the given member
		DELETE FROM dbo.USMemberRoles 
		WHERE	ApplicationId = ISNULL(@ApplicationId,ApplicationId) AND
				MemberId = @SourceMemberId AND
				MemberType = ISNULL(@SourceMemberType,MemberType)	AND
				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId) AND
				ProductId = ISNULL(@ProductId, ProductId)
	
		--Inserts the given memberrole information into the memberrole table.
		INSERT INTO dbo.USMemberRoles
		(
			ApplicationId,
			MemberId,
			MemberType,
			RoleId,
			ObjectId,
			ObjectTypeId,
			Propagate,
			ProductId
		)
		SELECT	tab.col.value('(ApplicationId/text())[1]','uniqueidentifier') ApplicationId,
	   			tab.col.value('(MemberId/text())[1]','uniqueidentifier') MemberId,
	   			tab.col.value('(MemberType/text())[1]','smallint') MemberType,
	   			tab.col.value('(RoleId/text())[1]','int') RoleId,
				tab.col.value('(ObjectId/text())[1]','uniqueidentifier') ObjectId,	
				tab.col.value('(ObjectTypeId/text())[1]','int') ObjectTypeId,	
				tab.col.value('(Propagate/text())[1]','bit') Propagate,
				@ProductId ProductId
		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
		
	END
--	ELSE IF(@SourceMemberRoles IS NOT NULL AND @ObjectId IS NOT NULL AND @ObjectTypeId IS NOT NULL)
--	BEGIN
--		DELETE FROM dbo.USMemberRoles 
--		WHERE	ApplicationId = ISNULL(@ApplicationId,ApplicationId) AND
--				ObjectId = @ObjectId AND
--				MemberType = ISNULL(@SourceMemberType,MemberType)	AND
--				ObjectTypeId = ISNULL(@ObjectTypeId,ObjectTypeId)
--
--		--Inserts all the given memberroles 
--		INSERT INTO dbo.USMemberRoles
--		(
--			ApplicationId,
--			MemberId,
--			MemberType,
--			RoleId,
--			ObjectId,
--			ObjectTypeId,
--			Propagate
--		)
--		SELECT	tab.col.value('(ApplicationId/text())[1]','uniqueidentifier') ApplicationId,
--	   			tab.col.value('(MemberId/text())[1]','uniqueidentifier') MemberId,
--	   			tab.col.value('(MemberType/text())[1]','smallint') MemberType,
--	   			tab.col.value('(RoleId/text())[1]','int') RoleId,
--				tab.col.value('(ObjectId/text())[1]','uniqueidentifier') ObjectId,	
--				tab.col.value('(ObjectTypeId/text())[1]','int') ObjectTypeId,	
--				tab.col.value('(Propagate/text())[1]','bit') Propagate
--		FROM	@SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
--	END

	IF (@SourceMemberRoles IS NOT NULL AND @ObjectTypeId = 2)
	BEGIN
		UPDATE P SET P.PropogateRoles = tab.col.value('(Propagate/text())[1]','bit')
			FROM PageMapNode P JOIN @SourceMemberRoles.nodes('//GenericCollectionOfMemberRole/MemberRole') tab(col)
				ON P.PageMapNodeId = tab.col.value('(ObjectId/text())[1]','uniqueidentifier')
	END
END

/*
1. Source Member to Target Member -> All the Role Permissions related to the given ObjectType
2. Source ObjectId to Target ObjectId -> All the Members and roles to the given ObjectType.
*/



GO
PRINT 'Altering procedure User_GetSocialToken'
GO
ALTER PROCEDURE [dbo].[CommerceReport_SalesTaxByCategoryForReport]
(	
	@startDate datetime,@endDate datetime,
	@sortBy nvarchar(50) = null,
	@virtualCount int output,
	@ApplicationId uniqueidentifier=null
) 
AS
	BEGIN
	IF @endDate IS NULL
	SET @endDate = GetUTCDate()

declare @tmpStateTax table(
	StateId uniqueidentifier,
	CountryName nvarchar(255),
	CountryCode char(2),
	StateName nvarchar(255),
	StateCode char(3),
	TaxCategory nvarchar(255),
	TotalTax decimal(18,2)
	)

INSERT INTO @tmpStateTax
SELECT
	GS.Id as StateId,
	GC.CountryName,
	GC.CountryCode,
	GS.State AS StateName,
	GS.StateCode,
	ISNULL(PT.Title,'--'),
	CONVERT(decimal(18,2),SUM(OI.Tax) -SUM(isnull(R.TAX,0))) AS Tax
FROM
	OROrder O
	INNER JOIN OROrderShipping OS ON O.Id = OS.OrderId
	INNER JOIN OROrderShippingAddress OSA ON OS.OrderShippingAddressId = OSA.Id
	INNER JOIN GLAddress GA ON OSA.AddressId = GA.Id
	INNER JOIN GLState GS ON GS.Id = GA.StateId
	INNER JOIN GLCountry GC ON GC.Id = GS.CountryId
	INNER JOIN OROrderItem OI ON OI.OrderId = O.Id
	INNER JOIN PRProductSKU PS ON PS.Id = OI.ProductSKUId
	INNER JOIN PRProduct P ON P.Id = PS.ProductID
	LEFT JOIN PRTaxCategory PT ON PT.Id = P.TaxCategoryId
	LEFT JOIN vwTaxRefund R on R.OrderShippingId= OS.Id
WHERE
	(O.OrderTypeId=1 OR O.OrderTypeId=2) 
	AND O.OrderDate >= dbo.ConvertTimeToUtc(@startDate,@ApplicationId)
	AND O.OrderDate < dbo.ConvertTimeToUtc(@endDate,@ApplicationId)
	AND O.OrderStatusId in (1,2,11)
	AND O.SiteId = @ApplicationId
GROUP BY
	GS.StateCode,GS.State,GS.Id, P.TaxCategoryId, PT.Title,GC.CountryName, GC.CountryCode



Set @virtualCount = @@ROWCOUNT
IF @sortBy is null
	Select * from @tmpStateTax Order by CountryCode ASC, StateName ASC
ELSE IF (@sortBy)='countrycode desc'
	Select * from @tmpStateTax Order by CountryCode DESC
ELSE IF (@sortBy)='countrycode asc'
	Select * from @tmpStateTax Order by CountryCode ASC
ELSE IF (@sortBy)='statename desc'
	Select * from @tmpStateTax Order by StateName DESC
ELSE IF (@sortBy)='statecode asc'
	Select * from @tmpStateTax Order by StateCode ASC
ELSE IF (@sortBy)='statecode desc'
	Select * from @tmpStateTax Order by StateCode DESC
ELSE IF (@sortBy)='totaltax asc'
	Select * from @tmpStateTax Order by TotalTax ASC
ELSE IF (@sortBy)='totaltax desc'
	Select * from @tmpStateTax Order by TotalTax DESC


END

GO
PRINT 'Altering procedure SiteDirectory_GetSiteDirectory'
GO
ALTER PROCEDURE [dbo].[SiteDirectory_GetSiteDirectory]  
(  
 @Id    uniqueIdentifier=null ,  
 @ApplicationId uniqueidentifier=null,  
 @Title   nvarchar(256)=null,  
 @ObjectTypeId int=null,  
 @PhysicalPath nvarchar(4000)=null,  
 @VirtualPath nvarchar(4000)=null,  
 @Status   int=null,  
 @PageIndex  int=1,  
 @PageSize  int=null   
 )  
AS  
  
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @EmptyGUID uniqueidentifier  
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  

IF @ObjectTypeId IS NULL
SET @ObjectTypeId =(Select top 1 ObjectTypeId from VWDirectoryIds Where Id = @Id)  

SET @EmptyGUID = dbo.GetEmptyGUID()  
 IF @PageSize is Null  
 BEGIN  
	if @ObjectTypeId not in (7,9,33,38)
		begin
			
			  SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size], IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			  FROM SISiteDirectory S  
			LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			  WHERE (@Id is null or @Id = S.Id) and
			   (@Title is null or S.Title=@Title) and  
			   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath) and  
			   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
			   (@ApplicationId is null or S.ApplicationId=@ApplicationId) and  
			   (@ObjectTypeId is null Or S.ObjectTypeId = @ObjectTypeId) and  
			   (@Status is null Or S.Status = @Status) and   
			   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  
		end
	else if @ObjectTypeId = 7
		begin
				SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate, 7 ObjectTypeId,PhysicalPath,VirtualPath,SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size], IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			  FROM COContentStructure S  
			  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			  WHERE (@Id is null or @Id = S.Id) and
			   (@Title is null or S.Title=@Title) and  
			   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath) and  
			   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
			   (@ApplicationId is null or S.SiteId=@ApplicationId) and  			  
			   (@Status is null Or S.Status = @Status) and   
			   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()) 
		end
	else if @ObjectTypeId = 9
		begin
				SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,9 ObjectTypeId,PhysicalPath,VirtualPath,SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size], IsSystem,0 IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			  FROM COFileStructure S  
			  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			  WHERE (@Id is null or @Id = S.Id )and
			   (@Title is null or S.Title=@Title )and  
			   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath )and  
			   (@VirtualPath is null or S.VirtualPath=@VirtualPath )and  
			   (@ApplicationId is null or S.SiteId=@ApplicationId )and  
			   (@Status is null Or S.Status = @Status )and   
			   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()) 
		end
	else if @ObjectTypeId = 33
		begin
				SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,33 ObjectTypeId,PhysicalPath,VirtualPath, SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size], IsSystem,0 IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			  FROM COImageStructure S  
			  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			  WHERE (@Id is null or @Id = S.Id) and
			   (@Title is null or S.Title=@Title )and  
			   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath ) and  
			   (@VirtualPath is null or S.VirtualPath=@VirtualPath ) and  
			   (@ApplicationId is null or S.SiteId=@ApplicationId ) and  
			   (@Status is null Or S.Status = @Status ) and   
			   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()) 
		end
	else if @ObjectTypeId = 38
		begin
				SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,38 ObjectTypeId,PhysicalPath,VirtualPath, SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size], IsSystem,0 IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			  FROM COFormStructure S  
			  LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
			  WHERE (@Id is null or @Id = S.Id) and
			   (@Title is null or S.Title=@Title )and  
			   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath ) and  
			   (@VirtualPath is null or S.VirtualPath=@VirtualPath ) and  
			   (@ApplicationId is null or S.SiteId=@ApplicationId ) and  
			   (@Status is null Or S.Status = @Status ) and   
			   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()) 
		end
 END  
 ELSE  
 BEGIN  
	if @ObjectTypeId not in (7,9,33,38)
		begin
			  WITH ResultEntries AS (   
			   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
				AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,   
				ModifiedDate,ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			   FROM SISiteDirectory S  
			   WHERE (@Id is null or @Id = S.Id ) and
						   (@Title is null or S.Title=@Title ) and  
						   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath) and  
						   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
						   (@ApplicationId is null or S.ApplicationId=@ApplicationId ) and  
						   (@ObjectTypeId is null Or S.ObjectTypeId = @ObjectTypeId) and  
						   (@Status is null Or S.Status = @Status ) and   
						   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  )  
	  
			  SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
				ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				TotalFiles  
			  FROM ResultEntries   A
			  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
			  WHERE Row between   
			   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize  
		end
	else if @ObjectTypeId =7
		begin
			  WITH ResultEntries AS (   
			   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
				AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,   
				ModifiedDate,7 ObjectTypeId,PhysicalPath,VirtualPath,SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			   FROM COContentStructure S  
			   WHERE (@Id is null or @Id = S.Id ) and
						   (@Title is null or S.Title=@Title) and  
						   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath) and  
						   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
						   (@ApplicationId is null or S.SiteId=@ApplicationId) and  
						   (@Status is null Or S.Status = @Status) and   
						   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  )  
	  
			  SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
				ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				TotalFiles  
			  FROM ResultEntries   A
			  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
			  WHERE Row between   
			   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize  
		end
	else if @ObjectTypeId =9
		begin
			  WITH ResultEntries AS (   
			   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
				AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,   
				ModifiedDate,7 ObjectTypeId,PhysicalPath,VirtualPath,SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,0 IsMarketierDir,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			   FROM COFileStructure S  
			   WHERE (@Id is null or @Id = S.Id ) and
						   (@Title is null or S.Title=@Title) and  
						   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath )and  
						   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
						   (@ApplicationId is null or S.SiteId=@ApplicationId) and  
						   (@Status is null Or S.Status = @Status) and   
						   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  )  
	  
			  SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
				ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				TotalFiles  
			  FROM ResultEntries   A
			  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
			  WHERE Row between   
			   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize  
		end
	else if @ObjectTypeId =33
		begin
			  WITH ResultEntries AS (   
			   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
				AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,   
				ModifiedDate,7 ObjectTypeId,PhysicalPath,VirtualPath,SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,0 IsMarketierDir,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			   FROM COImageStructure S  
			   WHERE (@Id is null or @Id = S.Id) and
						   (@Title is null or S.Title=@Title) and  
						   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath )and  
						   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
						   (@ApplicationId is null or S.SiteId=@ApplicationId) and  
						   (@Status is null Or S.Status = @Status )and   
						   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  )  
	  
			  SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
				ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				TotalFiles  
			  FROM ResultEntries   A
			  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
			  WHERE Row between   
			   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize  
		end
	else if @ObjectTypeId =38
		begin
			  WITH ResultEntries AS (   
			   SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
				AS Row, Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy,   
				ModifiedDate,38 ObjectTypeId,PhysicalPath,VirtualPath,SiteId ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,0 IsMarketierDir,
				dbo.GetTotalFiles(Id,@ObjectTypeId) TotalFiles  
			   FROM COFormStructure S  
			   WHERE (@Id is null or @Id = S.Id) and
						   (@Title is null or S.Title=@Title) and  
						   (@PhysicalPath is null or S.PhysicalPath =@PhysicalPath )and  
						   (@VirtualPath is null or S.VirtualPath=@VirtualPath) and  
						   (@ApplicationId is null or S.SiteId=@ApplicationId) and  
						   (@Status is null Or S.Status = @Status )and   
						   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  )  
	  
			  SELECT Id, Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,  
				ObjectTypeId,PhysicalPath,VirtualPath,ApplicationId,Attributes,ThumbnailImagePath,  
				FolderIconPath,[Exists],[Size],IsSystem,IsMarketierDir,
				FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
				TotalFiles  
			  FROM ResultEntries   A
			  LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
			LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
			  WHERE Row between   
			   (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize  
		end
 END  
  
END
GO
PRINT 'Altering procedure Content_GetUrls'
GO
ALTER PROCEDURE [dbo].[Content_GetUrls]   
(@ContentIds nvarchar(max)=null, @PageIds nvarchar(max)=null)
as  
BEGIN  
	DECLARE   
		@id uniqueidentifier,  
		@text varchar(max),  
		@SiteId uniqueidentifier,
		@ModifiedDate DateTime, @MaxModifiedDate DateTime, @ObjectTypeId int

	DECLARE 
		@FileType int, @ImageType int, @ContentType int, @XmlContentType int 	
  
	DECLARE @curSQL nvarchar(max)
  
	IF (@PageIds is not null)
	Begin
		DELETE FROM Content_AssetLinks Where PageId IN (@PageIds)
	End

	SELECT @MaxModifiedDate = Isnull(Min(ContentModifiedDate), '1900-01-01') FROM Content_AssetLinks
	IF (@ContentIds is not null AND @ContentIds<>'')
	Begin
		--DELETE FROM Content_AssetLinks Where ContentId IN (@ContentIds)
		SET @curSQL ='SELECT Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
			From COContent 
			where Id IN (SELECT VALUE FROM dbo.SplitComma(''' + @ContentIds + ''', '',''))'
	End
	ELSE
	Begin
		SET @curSQL ='SELECT Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
			From COContent 
			where Status!= 3 and Text is not null and Text not like  ''''  
			And ( ( Isnull(ModifiedDate, CreatedDate) >=  @MaxModifiedDate  And ObjectTypeId IN (7,13)) 
				) ORDER BY ModifiedDate'
	END
	
  
	SET @FileType = 9
	SET @ImageType = 33
	SET @ContentType = 7
	SET @XmlContentType = 13
	
	

	Set @curSQL='Declare textCursor cursor for '+ @curSQL
	--print @curSQL
	EXEC sp_executesql  @curSQL, N'@MaxModifiedDate DateTime', @MaxModifiedDate
	
	--select Id,Text, ApplicationId, Isnull(ModifiedDate, CreatedDate) As ModifiedDate, ObjectTypeId   
	--From COContent 
	--where Status!= 3 and Text is not null and Text not like  ''  
	--		And ( ( Isnull(ModifiedDate, CreatedDate) >=  @MaxModifiedDate And ObjectTypeId IN (7,13)) 
	--			) ORDER BY ModifiedDate
				
				
				
				--	OR  --Taking care in API Level [File and Images]
				--	( ObjectTypeId IN (9,33) 
			--AND Exists (SELECT PageDefinitionId FROM PageDefinition Where PageDefinitionXml like  '%' + Lower(Convert(nvarchar(max), Id )) + '%' )
			--		) 
--  and Text is not null and Text not like  '' uncommented this because Direct Content in Content Container taken care in API
				--)

	open textCursor  
	fetch next from textCursor into @id, @text, @SiteId, @ModifiedDate, @ObjectTypeId  
	while @@FETCH_STATUS = 0      
	begin      
		DELETE FROM Content_AssetLinks Where ContentId = @id
		
		-- For Direct Objects -- Taken care in API Level
		--insert into Content_AssetLinks
		--	(SiteId, ContentId, AssetLink, ContentModifiedDate, PageId, PageMapNodeId, PageSiteId, ObjectTypeId) 
		--	SELECT @SiteId, @id, '', @ModifiedDate, PageDefinitionId, PageMapNodeId, SiteId As PageSiteId, @ObjectTypeId
		--	FROM PageDefinition 
		--	WHERE PageDefinitionXml like  '%' + Lower(Convert(nvarchar(max), @id )) + '%' 

		--- For Embedded Objects
		IF (@text is not null AND @text<>'')
		begin 
			declare @link nvarchar(max), @links nvarchar(max), @hreflinks  nvarchar(max), @srclinks  nvarchar(max), @postion int
			--select  @links  = dbo.RegexSelectAll(@text,'(http|https)://(www\.)?([^\.]+)\.[a-z]*','|')  

			select @hreflinks  = dbo.RegexSelectAll(@text,'href\s*=\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))' ,'|')
			select @srclinks  = dbo.RegexSelectAll(@text,'src\s*=\s*(?:\"(?<1>[^"]*)"|(?<1>\S+))' ,'|')
			select @links = @hreflinks  			
			IF (@srclinks is not null AND @srclinks<>'') 
			Begin
				select @links = @links + '|' + @srclinks
			END

			if ((select charindex('|',@links)) = 1)
				set @links = SUBSTRING(@links, 2, len(@links) -1)

			while (@links != '')  
			begin  
				if((select charindex('|',@links)) != 0)  
				begin  
					select @postion = charindex('href',@links)
					IF (@postion=0) 
					Begin 
						select @postion = charindex('src',@links)
					End
					select @link  = substring(@links,@postion,(charindex('|',@links)-1))  
					select @links = replace(@links,@link+'|','')  
					select @links = replace(@links,@link,'')  
					
					insert into Content_AssetLinks
						(SiteId, ContentId, AssetLink, ContentModifiedDate, PageId, PageMapNodeId, PageSiteId, ObjectTypeId) 
						SELECT @SiteId, @id, dbo.UrlDecode(@link), @ModifiedDate, pg.PageDefinitionId, m.PageMapNodeId, pg.SiteId As PageSiteId, @ObjectTypeId
						FROM PageDefinition pg
						INNER JOIN PageMapNodePageDef m on pg.PageDefinitionId = m.PageDefinitionId
						INNER JOIN vw_PageContainerXmlDetails vg on vg.PageDefinitionId = pg.PageDefinitionId
						WHERE vg.ContentId =  @id  
				end  
				else  
				begin  

					insert into Content_AssetLinks
						(SiteId, ContentId, AssetLink, ContentModifiedDate, PageId, PageMapNodeId, PageSiteId, ObjectTypeId) 				 
						SELECT @SiteId, @id, dbo.UrlDecode(@links), @ModifiedDate, pg.PageDefinitionId, m.PageMapNodeId, pg.SiteId As PageSiteId, @ObjectTypeId
						FROM PageDefinition pg
						INNER JOIN PageMapNodePageDef m on pg.PageDefinitionId = m.PageDefinitionId
						INNER JOIN vw_PageContainerXmlDetails vg on vg.PageDefinitionId = pg.PageDefinitionId
						WHERE vg.ContentId =  @id  
												
						--FROM PageDefinition 
						--WHERE ContainerXml like  '%' + Lower(Convert(nvarchar(max), @id )) + '%' 

					set @links =''  
				end  
			end  
		end
		fetch next from textCursor into  @id, @text, @SiteId, @ModifiedDate, @ObjectTypeId  
	end  
	close textCursor  
	deallocate textCursor  

	--- Site Name Updations -- Taken care in API level
	--UPDATE Content_AssetLinks SET PageSiteName = Title 
	--FROM SISite Where SISite.Id = PageSiteId And PageSiteName is null

	--- Page Name Updations -- Taken care in API level
	--UPDATE Content_AssetLinks SET  friendlyName = Replace(Replace(Replace(Convert(xml, PageDefinition.PageDefinitionXml).value('(pageDefinition/@friendlyName)[1]','varchar(max)'), '_',' '),'.',' '),'''','')
	--FROM PageDefinition Where PageDefinition.PageDefinitionId = Content_AssetLinks.PageId And friendlyName is null

	--- PageMapUrl Updation --- Taken care in API level 
	--UPDATE Content_AssetLinks 
	--SET PageMapUrl = Replace(Replace(Replace(Convert(xml, (PageMapNode.PageMapNodeXml + '</pageMapNode>')).value('(pageMapNode/@friendlyUrl)[1]','varchar(max)'), '_',' '),'.',' '),'''','') 
	--FROM PageMapNode 
	--WHERE PageMapNode.PageMapNodeId = Content_AssetLinks.PageMapNodeId --And PageMapUrl is null
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
