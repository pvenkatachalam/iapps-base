﻿IF NOT EXISTS (SELECT TOP 1 1 FROM dbo.COFormStructure)
BEGIN
INSERT INTO [dbo].[COFormStructure]
           ([Id]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[Title]
           ,[Description]
           ,[SiteId]
           ,[Exists]
           ,[Attributes]
           ,[PhysicalPath]
           ,[VirtualPath]
           ,[ThumbnailImagePath]
           ,[FolderIconPath]
           ,[Keywords]
           ,[Size]
		  ,[CreatedBy]
		  ,[CreatedDate]
		  ,[ModifiedBy]
		  ,[ModifiedDate]
		  ,[Status]
		  ,[IsSystem]
		  ,[IsMarketierDir]
		  )
SELECT S.[Id]
	  ,H.ParentId
	  ,H.LftValue
	  ,H.RgtValue
        ,S.[Title]
      ,[Description]
      ,H.SiteId
      ,[Exists]
      ,[Attributes]
      ,[PhysicalPath]
      ,[VirtualPath]
       ,[ThumbnailImagePath]
      ,[FolderIconPath]
      ,H.Keywords
      ,[Size]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,[Status]
      ,Isnull(IsSystem,0)
      ,Isnull(IsMarketierDir,0)
  FROM [dbo].[SISiteDirectory] S
  Inner join HSStructure H on S.Id =H.Id
  Where S.ObjectTypeId =38
Order by SiteId,LftValue




Update F SET F.ParentId = H.ParentId
FROM Forms F 
INNER JOIN HSStructure H on F.Id = H.Id



END