﻿DECLARE @iAPPSUser uniqueidentifier
SET @iAPPSUser = (SELECT TOP 1 Id FROM USUser where username like 'iappssystemuser')

DECLARE @settingTypeId int
DECLARE @sequence int
DECLARE @MasterSiteId uniqueidentifier
DECLARE @SiteId uniqueidentifier

DECLARE MASTER_SITES Cursor 
FOR
SELECT Id FROM SISite Where Status = 1 AND Id = MasterSiteId

Open MASTER_SITES 
Fetch NEXT FROM MASTER_SITES INTO @MasterSiteId

While (@@FETCH_STATUS <> -1)
BEGIN
	DECLARE @contentDefinitionId uniqueidentifier
	SET @contentDefinitionId = 'f580f504-483b-4dbb-89e1-225ce3cb9596'

	IF NOT EXISTS (SELECT * FROM COXMLForm WHERE Id = @contentDefinitionId)
	BEGIN
		INSERT INTO COXMLForm
		(
			Id,
			ApplicationId,
			Name,
			Description,
			XMLString,
			Status,
			XMLFileName,
			XSLTFileName,
			CreatedBy,
			CreatedDate,
			XsltString
		)
		VALUES
		(
			@contentDefinitionId,
			@MasterSiteId,
			'Blog Post',
			'Default iAPPS Blog Post',
			'<contentDefinition type="BlogPost">
			  <contentDefinitionNode type="hidden" label="Template Id:" objectId="TemplateId" customAttribute="No" />
			  <contentDefinitionNode type="editor" label="Description" objectId="Description" required="Yes" customAttribute="No" />
			</contentDefinition>',
			1,
			'BlogPost.xml',
			'BlogPost.xsl',
			@iAPPSUser,
			getdate(),
			'<?xml version="1.0" encoding="utf-8"?>
			<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove" xmlns:radE="remove" >
				<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
				<xsl:template match="/">
				<xsl:for-each select="//contentDefinitionNode">
					<xsl:choose>
					<xsl:when test="@objectId=''Description''">
						<xsl:value-of select="@select" />
					</xsl:when>
					</xsl:choose>
				</xsl:for-each>
				</xsl:template>
			</xsl:stylesheet>'
		)
	END

	UPDATE BLBlog SET ContentDefinitionId = @contentDefinitionId 
		WHERE ContentDefinitionId IS NULL AND
			ApplicationId IN (select SiteId from dbo.GetVariantSites(@MasterSiteId)) 

	SET @settingTypeId = (SELECT TOP 1 Id FROM STSettingType Where name like 'Blogs.DefaultContentDefinitionId')

	IF @settingTypeId IS NULL
	BEGIN
		SELECT @sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
		Values('Blogs.DefaultContentDefinitionId','Blogs.DefaultContentDefinitionId',1,1,@sequence)
		SET @settingTypeId = @@Identity
	END

	IF NOT EXISTS (SELECT * FROM STSiteSetting WHERE SettingTypeId = @settingTypeId AND SiteId = @MasterSiteId)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@MasterSiteId,@settingTypeId, @contentDefinitionId)
			
	Fetch NEXT FROM MASTER_SITES INTO @MasterSiteId 
END
CLOSE MASTER_SITES
DEALLOCATE MASTER_SITES

DECLARE ALL_SITES Cursor 
FOR
SELECT Id FROM SISite Where Status = 1

Open ALL_SITES 
Fetch NEXT FROM ALL_SITES INTO @SiteId

While (@@FETCH_STATUS <> -1)
BEGIN
	DECLARE @directoryId uniqueidentifier
	SET @directoryId = (SELECT TOP 1 Id FROM COContentStructure Where Description = 'Default iAPPS Blog Post' AND SiteId = @SiteId)
	IF @directoryId IS NULL
		SET @directoryId = NEWID()

	IF NOT EXISTS (SELECT * FROM COContentStructure WHERE ID = @directoryId)
	BEGIN
		DECLARE @rootContentDirectory uniqueidentifier
		SET @rootContentDirectory = (SELECT Id FROM COContentStructure Where ParentId = SiteId  AND SiteId = @SiteId)

		IF @rootContentDirectory IS NOT NULL
		BEGIN
			DECLARE @tbDirectory TYDirectory	
			DELETE FROM @tbDirectory	
			INSERT INTO @tbDirectory(Id,
				Title,
				Description,
				Status,
				ParentId,
				IsSystem,
				SiteId,
				Lft,
				Rgt,
				IsMarketierDir,
				AllowAccessInChildrenSites)						
			VALUES(@directoryId,
				'Blog Posts',
				'Default iAPPS Blog Post',
				1,
				@rootContentDirectory,
				1,
				@SiteId,
				1,
				1,
				0,
				1)

			EXEC [dbo].[SqlDirectoryProvider_SaveContentDirectory] @tbDirectory, @rootContentDirectory, @SiteId, @iAPPSUser
		END
	END

	SET @settingTypeId = (SELECT TOP 1 Id FROM STSettingType Where name like 'ContentLibrary.BlogDirectory')

	IF @settingTypeId IS NULL
	BEGIN
		SELECT @sequence = MAX(sequence) FROM dbo.STSettingType
		INSERT INTO STSettingType(Name,FriendlyName,SettingGroupId,IsSiteOnly,Sequence)
		Values('ContentLibrary.BlogDirectory','ContentLibrary.BlogDirectory',1,1,@sequence)
		SET @settingTypeId = @@Identity
	END

	IF NOT EXISTS (SELECT * FROM STSiteSetting WHERE SettingTypeId = @settingTypeId AND SiteId = @SiteId)
		INSERT INTO STSiteSetting(SiteId,SettingTypeId,Value)
		Values(@SiteId,@settingTypeId, @directoryId)

	Fetch NEXT FROM ALL_SITES INTO @SiteId 
END
CLOSE ALL_SITES
DEALLOCATE ALL_SITES

GO

Update BLPost SET IsSticky = 0 WHERE IsSticky IS NULL
Update BLPost Set Status = 8 WHERE Status = 1 AND (PublishCount IS NULL OR PublishCount = 0) -- Make all active post published
Update BLPost Set Status = 7 WHERE Status = 6 -- Change archive to 7 to be consistent
Update BLPost Set IsSticky = 1 WHERE Status = 5 -- Sticky status moved as new column

GO

Declare @PublishDate DateTime
Select top 1 @PublishDate = PublishDate From BLPost
Select @PublishDate 
If (@PublishDate IS NULL)
	Update BLPost Set PublishDate = CreatedDate

GO

Declare @PublishCount int	
Select top 1 @PublishCount = PublishCount From BLPost where Status = 8
If(@PublishCount IS NULL OR @PublishCount = 0)
	Update BLPost Set PublishCount = 1 where Status = 8

GO

-- Delete all unused SPs
IF OBJECT_ID(N'[dbo].[Post_GetPostByFolderId]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Post_GetPostByFolderId]    
GO
IF OBJECT_ID(N'[dbo].[Post_GetPostUrl]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Post_GetPostUrl]    
GO
IF OBJECT_ID(N'[dbo].[Post_UpdatePostStatus]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Post_UpdatePostStatus]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetAllLabels]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetAllLabels]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetAuthors]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetAuthors]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetBlogRewriteURLData]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetBlogRewriteURLData]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetCategoriesByBlog]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetCategoriesByBlog]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetLivePostsByCategory]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetLivePostsByCategory]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetLivePostsByCategory_Count]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetLivePostsByCategory_Count]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetLivePostsByMonthYear]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetLivePostsByMonthYear]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetLivePostsByMonthYear_Count]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetLivePostsByMonthYear_Count]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostsByBlog]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostsByBlog]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostsByStatus]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostsByStatus]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostRewriteURLData]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostRewriteURLData]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostsByAuthor]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostsByAuthor]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostsByLabel]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostsByLabel]    
GO
IF OBJECT_ID(N'[dbo].[Blog_UpdateBlogStatus]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_UpdateBlogStatus]    
GO
IF OBJECT_ID(N'[dbo].[Blog_UpdatePostStatus]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_UpdatePostStatus]    
GO
IF OBJECT_ID(N'[dbo].[Blog_UpdateSettings]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_UpdateSettings]  
GO
IF OBJECT_ID(N'[dbo].[Blog_GetBlogByPageDefinition]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetBlogByPageDefinition]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetBlogByPageDefinition]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetBlogByPageDefinition]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostByBlog]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostByBlog]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostByStatus]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostByStatus]   
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostByBlog]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostByBlog]    
GO
IF OBJECT_ID(N'[dbo].[Blog_GetPostByStatus]', 'P') IS NOT NULL
Drop PROCEDURE [dbo].[Blog_GetPostByStatus]  
GO
IF OBJECT_ID(N'[dbo].[Post_UfGetTaxnomonyTb]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[Post_UfGetTaxnomonyTb]    
GO
IF OBJECT_ID(N'[dbo].[GetBlogRewriteData]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[GetBlogRewriteData]    
GO
IF OBJECT_ID(N'[dbo].[GetTaxonomyByBlog]', 'TF') IS NOT NULL
Drop FUNCTION [dbo].[GetTaxonomyByBlog]    
GO