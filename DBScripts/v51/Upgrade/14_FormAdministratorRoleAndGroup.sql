﻿Go

DECLARE @CreatedById	uniqueidentifier
SET @CreatedById = 'B10CB05D-7000-49AD-B115-872C19F5700B' --This has to be modified to InstallAdmin

DECLARE @CMSProductId uniqueidentifier
SET @CMSProductId ='CBB702AC-C8F1-4C35-B2DC-839C26E39848'

DECLARE @SiteId uniqueidentifier
SET @SiteId = '8039ce09-e7da-47e1-bcec-df96b5e411f4'

DECLARE @FormAdminRoleId int
SET @FormAdminRoleId = (SELECT TOP 1 Id FROM USRoles WHERE Name = 'FormAdministrator')
IF @FormAdminRoleId IS NULL
BEGIN
	SELECT @FormAdminRoleId = MAX(Id) + 1 FROM USRoles
	INSERT INTO USRoles (ApplicationId,Id,[Name],Description,IsGlobal) 
	VALUES (@CMSProductId, @FormAdminRoleId, 'FormAdministrator','FormAdministrator', 1)
END

DECLARE @FormAdminGroupId uniqueidentifier
SET @FormAdminGroupId = (SELECT TOP 1 Id FROM USGroup WHERE Title = 'Form Administrator')
IF @FormAdminGroupId IS NULL
BEGIN
	EXEC Group_Save  @CMSProductId,
		@ApplicationId = @CMSProductId, 
		@Id = @FormAdminGroupId output,
		@Title = 'Form Administrator',
		@Description = 'Form Administrator',
		@CreatedBy = @CreatedById,
		@ModifiedBy=NULL,
		@ExpiryDate=NULL,
		@Status=NULL,
		@GroupType=1	
END


IF NOT EXISTS (SELECT * FROM USMemberRoles WHERE MemberId = @FormAdminGroupId AND RoleId = @FormAdminRoleId)
	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate) 
	Values(@CMSProductId, @SiteId, @FormAdminGroupId, 2, @FormAdminRoleId, @CMSProductId, 1, 1)	


GO

