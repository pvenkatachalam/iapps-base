﻿/*
Run this script on:

        rnd-d-dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:04:39 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

PRINT 'Dropping All new procedures'
GO

IF OBJECT_ID('[dbo].[Log_GetCategories]') IS NOT NULL DROP PROCEDURE[dbo].[Log_GetCategories]	
IF OBJECT_ID('[dbo].[Membership_BuildUser]') IS NOT NULL DROP PROCEDURE[dbo].[Membership_BuildUser]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_SaveContentDirectory]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_SaveContentDirectory]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_SaveDirectory]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_SaveDirectory]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_SaveFormDirectory]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_SaveFormDirectory]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_SaveImageDirectory]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_SaveImageDirectory]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_SaveFileDirectory]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_SaveFileDirectory]	
IF OBJECT_ID('[dbo].[Site_CopyDirectories]') IS NOT NULL DROP PROCEDURE[dbo].[Site_CopyDirectories]	
IF OBJECT_ID('[dbo].[Site_GetSiteByHierarchy]') IS NOT NULL DROP PROCEDURE[dbo].[Site_GetSiteByHierarchy]	
IF OBJECT_ID('[dbo].[Post_GetAllLabels]') IS NOT NULL DROP PROCEDURE[dbo].[Post_GetAllLabels]	
IF OBJECT_ID('[dbo].[Log_WriteLog]') IS NOT NULL DROP PROCEDURE[dbo].[Log_WriteLog]	
IF OBJECT_ID('[dbo].[User_SaveSocialUser]') IS NOT NULL DROP PROCEDURE[dbo].[User_SaveSocialUser]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]	
IF OBJECT_ID('[dbo].[Site_GetSiteByHierarchy_Count]') IS NOT NULL DROP PROCEDURE[dbo].[Site_GetSiteByHierarchy_Count]	
IF OBJECT_ID('[dbo].[Search_GetWorkflows]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetWorkflows]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_GetContentDirectoriesByLevel]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_GetContentDirectoriesByLevel]	
IF OBJECT_ID('[dbo].[Version_SaveBlogPost]') IS NOT NULL DROP PROCEDURE[dbo].[Version_SaveBlogPost]	
IF OBJECT_ID('[dbo].[Post_UpdateStatus]') IS NOT NULL DROP PROCEDURE[dbo].[Post_UpdateStatus]	
IF OBJECT_ID('[dbo].[SiteGroup_Get]') IS NOT NULL DROP PROCEDURE[dbo].[SiteGroup_Get]	
IF OBJECT_ID('[dbo].[Search_GetContentDefinitions]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetContentDefinitions]	
IF OBJECT_ID('[dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]') IS NOT NULL DROP PROCEDURE[dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]	
IF OBJECT_ID('[dbo].[GetAutoBlogList]') IS NOT NULL DROP PROCEDURE[dbo].[GetAutoBlogList]	
IF OBJECT_ID('[dbo].[UploadContactDataLog_Get]') IS NOT NULL DROP PROCEDURE[dbo].[UploadContactDataLog_Get]	
IF OBJECT_ID('[dbo].[User_DeleteSocialUser]') IS NOT NULL DROP PROCEDURE[dbo].[User_DeleteSocialUser]	
IF OBJECT_ID('[dbo].[SiteGroup_Save]') IS NOT NULL DROP PROCEDURE[dbo].[SiteGroup_Save]	
IF OBJECT_ID('[dbo].[Search_GetMenus]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetMenus]	
IF OBJECT_ID('[dbo].[SocialCredentials_Save]') IS NOT NULL DROP PROCEDURE[dbo].[SocialCredentials_Save]	
IF OBJECT_ID('[dbo].[Search_GetImages]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetImages]	
IF OBJECT_ID('[dbo].[Blog_GetLivePostCategories]') IS NOT NULL DROP PROCEDURE[dbo].[Blog_GetLivePostCategories]	
IF OBJECT_ID('[dbo].[Log_GetLogByActivityId]') IS NOT NULL DROP PROCEDURE[dbo].[Log_GetLogByActivityId]	
IF OBJECT_ID('[dbo].[SiteGroup_Delete]') IS NOT NULL DROP PROCEDURE[dbo].[SiteGroup_Delete]	
IF OBJECT_ID('[dbo].[Site_GetAncestorSites]') IS NOT NULL DROP PROCEDURE[dbo].[Site_GetAncestorSites]	
IF OBJECT_ID('[dbo].[Search_GetFiles]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetFiles]	
IF OBJECT_ID('[dbo].[Search_GetStyles]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetStyles]	
IF OBJECT_ID('[dbo].[Log_GetLogs]') IS NOT NULL DROP PROCEDURE[dbo].[Log_GetLogs]	
IF OBJECT_ID('[dbo].[OrderItem_ClearCouponValue]') IS NOT NULL DROP PROCEDURE[dbo].[OrderItem_ClearCouponValue]	
IF OBJECT_ID('[Site_Move]') IS NOT NULL DROP PROCEDURE[Site_Move]	
IF OBJECT_ID('[Site_MoveTest]') IS NOT NULL DROP PROCEDURE[Site_MoveTest]	
IF OBJECT_ID('[dbo].[Log_AddCategory]') IS NOT NULL DROP PROCEDURE[dbo].[Log_AddCategory]	
IF OBJECT_ID('[dbo].[Search_GetTemplates]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetTemplates]	
IF OBJECT_ID('[dbo].[Site_GetSiteGroups]') IS NOT NULL DROP PROCEDURE[dbo].[Site_GetSiteGroups]	
IF OBJECT_ID('[dbo].[Search_GetProducts]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetProducts]	
IF OBJECT_ID('[dbo].[PageDefinition_GetByWorkflow]') IS NOT NULL DROP PROCEDURE[dbo].[PageDefinition_GetByWorkflow]	
IF OBJECT_ID('[dbo].[Post_GetAuthors]') IS NOT NULL DROP PROCEDURE[dbo].[Post_GetAuthors]	
IF OBJECT_ID('[dbo].[Site_GetSiteHierarchyPath]') IS NOT NULL DROP PROCEDURE[dbo].[Site_GetSiteHierarchyPath]	
IF OBJECT_ID('[dbo].[PageDefinition_GetPageIdsOfDifferentSiteContent]') IS NOT NULL DROP PROCEDURE[dbo].[PageDefinition_GetPageIdsOfDifferentSiteContent]	
IF OBJECT_ID('[dbo].[Site_GetSitesBySiteGroup]') IS NOT NULL DROP PROCEDURE[dbo].[Site_GetSitesBySiteGroup]	
IF OBJECT_ID('[dbo].[Membership_GetDefaultSite]') IS NOT NULL DROP PROCEDURE[dbo].[Membership_GetDefaultSite]	
IF OBJECT_ID('[dbo].[SocialMedia_Get]') IS NOT NULL DROP PROCEDURE[dbo].[SocialMedia_Get]	
IF OBJECT_ID('[dbo].[Search_GetUsers]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetUsers]	
IF OBJECT_ID('[dbo].[User_GetExternalSocialId]') IS NOT NULL DROP PROCEDURE[dbo].[User_GetExternalSocialId]	
IF OBJECT_ID('[dbo].[Search_GetContentItems]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetContentItems]	
IF OBJECT_ID('[dbo].[UploadContactDataLog_Save]') IS NOT NULL DROP PROCEDURE[dbo].[UploadContactDataLog_Save]	
IF OBJECT_ID('[dbo].[Log_ClearLogs]') IS NOT NULL DROP PROCEDURE[dbo].[Log_ClearLogs]	
IF OBJECT_ID('[dbo].[Search_GetPages]') IS NOT NULL DROP PROCEDURE[dbo].[Search_GetPages]	
IF OBJECT_ID('[dbo].[Payment_GetExternalProfile]') IS NOT NULL DROP PROCEDURE[dbo].[Payment_GetExternalProfile]
IF OBJECT_ID('[dbo].[OrderItem_SaveCouponCodeDiscount]') IS NOT NULL DROP PROCEDURE[dbo].[OrderItem_SaveCouponCodeDiscount]
IF OBJECT_ID('[dbo].[Cache_GetSiteFlushUrls]') IS NOT NULL DROP PROCEDURE[dbo].[Cache_GetSiteFlushUrls]
IF OBJECT_ID('[dbo].[Cache_SaveSiteFlushUrls]') IS NOT NULL DROP PROCEDURE[dbo].[Cache_SaveSiteFlushUrls]
IF OBJECT_ID('[dbo].[Payment_GetPaymentAndRefundPaymentInformation]') IS NOT NULL DROP PROCEDURE[dbo].[Payment_GetPaymentAndRefundPaymentInformation]
IF OBJECT_ID('[dbo].[OrderItem_GetCouponCodeDiscounts]') IS NOT NULL DROP PROCEDURE[dbo].[OrderItem_GetCouponCodeDiscounts]
IF OBJECT_ID('[dbo].[User_GetSocialToken]') IS NOT NULL DROP PROCEDURE[dbo].[User_GetSocialToken]
IF OBJECT_ID('[dbo].[PageMapNode_UpdateMemberRoles]') IS NOT NULL DROP PROCEDURE[dbo].[PageMapNode_UpdateMemberRoles]


GO
PRINT N'Creating [dbo].[Log_GetCategories]'
GO
IF OBJECT_ID(N'[dbo].[Log_GetCategories]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Log_GetCategories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Log_GetCategories]
(
	@SiteId			uniqueidentifier = NULL
)
AS 
BEGIN

	SELECT
		Id,
		Title
	FROM LGCategory
END

'
GO
PRINT N'Creating [dbo].[User_GetSocialToken]'
GO
IF OBJECT_ID(N'[dbo].[User_GetSocialToken]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : User_GetSocialToken
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE PROCEDURE dbo.User_GetSocialToken(
	@UserId uniqueidentifier,
	@SiteId uniqueidentifier)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT APIToken FROM USSocialUserProfile Where UserId=@UserId AND SiteId=@SiteId
END
'
GO
PRINT N'Creating [dbo].[Membership_BuildUser]'
GO
IF OBJECT_ID(N'[dbo].[Membership_BuildUser]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[Membership_BuildUser]
(
	@tbUser			TYUser READONLY,
	@SortBy			nvarchar(100) = NULL,
	@SortOrder		nvarchar(10) = NULL,	
	@PageNumber		int = NULL,
	@PageSize		int = NULL
)
AS
BEGIN
	IF @SortBy IS NULL SET @SortBy = ''USERNAME''
	IF @SortOrder IS NULL SET @SortOrder = ''ASC''
	SET @SortBy = UPPER(@SortBy)
	SET @SortOrder = UPPER(@SortOrder)

	DECLARE @PageLowerBound int
	DECLARE @PageUpperBound int
	SET @PageLowerBound = @PageSize * @PageNumber
	IF (@PageNumber > 0)
		SET @PageUpperBound = @PageLowerBound - @PageSize + 1
	ELSE
		SET @PageUpperBound = 0

	DECLARE @tbPagedUser TYUser
	;WITH CTEUsers AS(
		SELECT ROW_NUMBER() OVER (
			ORDER BY
				CASE WHEN (@SortBy = ''USERNAME'' AND @SortOrder = ''ASC'') THEN U.UserName END ASC,
				CASE WHEN (@SortBy = ''USERNAME'' AND @SortOrder = ''DESC'') THEN U.UserName END DESC,
				CASE WHEN (@SortBy = ''FIRSTNAME'' AND @SortOrder = ''ASC'') THEN U.FirstName END ASC,
				CASE WHEN (@SortBy = ''FIRSTNAME'' AND @SortOrder = ''DESC'') THEN U.FirstName END DESC,
				CASE WHEN (@SortBy = ''LASTNAME'' AND @SortOrder = ''ASC'') THEN U.LastName END ASC,
				CASE WHEN (@SortBy = ''LASTNAME'' AND @SortOrder = ''DESC'') THEN U.LastName END DESC,
				CASE WHEN (@SortBy = ''STATUS'' AND @SortOrder = ''ASC'') THEN U.Status END ASC,
				CASE WHEN (@SortBy = ''STATUS'' AND @SortOrder = ''DESC'') THEN U.Status END DESC	
			) AS RowNumber,
			U.*
		FROM @tbUser U
	)
		
	INSERT INTO @tbPagedUser
	SELECT
		Id,    
		UserName,      
		Email,    
		PasswordQuestion,      
		IsApproved,    
		CreatedDate,    
		LastLoginDate,    
		LastActivityDate,    
		LastPasswordChangedDate,    
		IsLockedOut,    
		LastLockoutDate,  
		FirstName,  
		LastName,  
		MiddleName,  
		ExpiryDate,  
		EmailNotification,  
		TimeZone,  
		ReportRangeSelection,  
		ReportStartDate,  
		ReportEndDate,   
		BirthDate,  
		CompanyName,  
		Gender,  
		HomePhone,  
		MobilePhone,  
		OtherPhone,  
		ImageId,
		Status,
		ProductSuite					
	FROM CTEUsers
	WHERE (@PageNumber is null) OR (@PageNumber = 0) or (RowNumber BETWEEN @PageUpperBound AND @PageLowerBound) 
		
	SELECT * FROM @tbPagedUser

	SELECT COUNT(*) FROM @tbUser
		
	SELECT	UserId ,             
			PropertyName,
			PropertyValueString, 
			PropertyValuesBinary,
			LastUpdatedDate
	FROM 	dbo.USMemberProfile P, @tbPagedUser T
	WHERE	P.UserId = T.Id 
				
	SELECT U.UserId, U.SecurityLevelId, S.Title  
	FROM USUserSecurityLevel U
		JOIN USSecurityLevel S ON U.SecurityLevelId = S.Id 
		JOIN @tbPagedUser T ON T.Id = U.UserId
END

'

GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_SaveContentDirectory]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_SaveContentDirectory]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_SaveContentDirectory
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_SaveContentDirectory]
(
	@tbDirectory	TYDirectory READONLY,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE	@Lft bigint,
		@Rgt bigint,
		@Count bigint,
		@NodeCount bigint,
		@PLft bigint,
		@PRgt bigint,
		@IsParentSet bit,
		@Id uniqueidentifier,
		@Inc int,
		@ModifiedDate DateTime
		
	SET @ModifiedDate =GetUtcDate()
	
	IF @ApplicationId is null AND @ParentId is not null
		SELECT @ApplicationId = SiteId FROM COContentStructure WHERE Id = @ParentId 

	-- checking for duplicates
	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
	OR EXISTS (Select 1 From COContentStructure P, @tbDirectory C  Where  P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId )
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END   

	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From COContentStructure P, @tbDirectory C Where P.Status !=dbo.GetDeleteStatus()and P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId)
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE COContentStructure  Set Title =T.Title,
		Description =T.Description,
		Attributes =T.Attributes,
		[Exists] =T.[Exists],
		FolderIconPath  =T.FolderIconPath,
		PhysicalPath =T.PhysicalPath,
		[Size] =T.[Size],
		Status =T.Status,
		ThumbnailImagePath =T.ThumbnailImagePath,
		ModifiedBy =@ModifiedBy, 
		ModifiedDate =@ModifiedDate,
		IsMarketierDir = T.IsMarketierDir,
		AllowAccessInChildrenSites =  T.AllowAccessInChildrenSites
	FROM @tbDirectory T 
	WHERE T.Id = COContentStructure.Id AND COContentStructure.SiteId=@ApplicationId

	IF(@ParentId is null)
	BEGIN
		SELECT TOP 1 @ParentId = ParentId FROM @tbDirectory Order By Lft
		IF @ParentId is null
		BEGIN
			SELECT TOP 1 @ParentId = ParentId FROM COContentStructure 
				WHERE SiteId=@ApplicationId Order by LftValue
		END
	END

	Select @PLft = LftValue , @PRgt = RgtValue From COContentStructure Where Id=@ParentId

	If @PLft is null
	BEGIN
		RAISERROR(''INVALID||ParentId'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE H SET H.Title = T.Title
	FROM @tbDirectory T INNER JOIN COContentStructure H
		ON T.Id = H.Id AND H.SiteId = @ApplicationId

	SELECT @NodeCount = count(*) from @tbDirectory
	SET @Lft=0
	SET @Count=0	
	SET @IsParentSet =1
		
	WHILE (@Count<@NodeCount)
		BEGIN
			--Selecting Record one by one
			Select Top 1 @Id =Id,
				@Lft =Lft,
				@Rgt =Rgt, 
				@ParentId =ParentId
			FROM @tbDirectory WHERE Lft>@Lft AND SiteId =@ApplicationId
			Order By Lft
			
			IF @IsParentSet =0 -- Select the parent Details of the current record
			BEGIN
				SELECT  @ParentId=Id	,
						@PLft = LftValue,
						@PRgt = RgtValue
						FROM COContentStructure 
						WHERE Id = @ParentId AND SiteId=@ApplicationId
			END

			--Check if that node exists or not
			IF Exists (Select 1 FROM COContentStructure 
						WHERE Id=@Id AND SiteId = @ApplicationId)	
			BEGIN
				Print  ''Node Already Exists''
				-- Node Already Exists so Just Skip
				-- Check if it has Childs if so set the Parent properties
				IF @Rgt -@Lft >1
				BEGIN
					SELECT @ParentId=Id	,
						   @PLft = LftValue,
						   @PRgt = RgtValue
					FROM COContentStructure 
					WHERE LftValue Between @PLft and @PRgt And Id=@Id AND SiteId=@ApplicationId
				END
				SET @IsParentSet = 1
			END
			ELSE
			BEGIN
				--Not Exists Insert the Node and all its child the Relation Table 
				--Create Space to Insert
				SET @Inc = (@Rgt - @Lft) + 1 -- Value to be added to create space
				UPDATE COContentStructure 
				SET LftValue=CASE WHEN LftValue>=@PRgt THEN LftValue + @Inc ELSE LftValue END,
					RgtValue=CASE WHEN RgtValue>=@PRgt THEN RgtValue + @Inc ELSE RgtValue END
				Where (LftValue >=@PRgt OR RgtValue >=@PRgt)			
				AND SiteId=@ApplicationId 
				
				INSERT INTO COContentStructure
				(Id
				,Title
				,LftValue
				,RgtValue
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,Status
				,ThumbnailImagePath
				,VirtualPath
				,CreatedBy
				,CreatedDate
				,IsMarketierDir
				,AllowAccessInChildrenSites )
				SELECT Id
				,Title
				,(Lft-@Lft)+ @PRgt 
				,(Rgt-@Lft) + @PRgt
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,dbo.GetActiveStatus()
				,ThumbnailImagePath
				,dbo.GetVirtualPathByObjectType(ParentId,7)+ ''/'' + Title
				,@ModifiedBy
				,@ModifiedDate 
				,IsMarketierDir
				,AllowAccessInChildrenSites
				FROM @tbDirectory 
				WHERE Lft Between @Lft and @Rgt
				
				-- Increment the Count to skip the no of time loop is executed
				SET @Count =@Count + ((@Rgt - @Lft) - 1)/2
				-- Increment the Lft value of the loop skips there records

				SET @Lft = @Rgt

				--- Propagating Pemission from the Parent
				If(@ParentId is not null)
				Begin
					print ''copy permission to its  child''
					INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, Propagate, ObjectId, ObjectTypeId ) 
					(
					SELECT ProductId, ApplicationId, MemberId, MemberType, RoleId,  Propagate, @Id, ObjectTypeId  
					FROM USMemberRoles Where ObjectId = @ParentId AND ApplicationId=@ApplicationId
					)
				END
			END
		
			SET @Count= @Count + 1
		END	

	END
	

'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_SaveDirectory]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_SaveDirectory]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_SaveDirectory
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_SaveDirectory]
(
	@tbDirectory	TYDirectory READONLY,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE	@Lft bigint,
		@Rgt bigint,
		@Count bigint,
		@NodeCount bigint,
		@PLft bigint,
		@PRgt bigint,
		@IsParentSet bit,
		@Id uniqueidentifier,
		@Inc int,
		@ModifiedDate DateTime
		
	SET @ModifiedDate =GetUtcDate()
	
	IF @ApplicationId is null AND @ParentId is not null
		SELECT @ApplicationId = SiteId FROM HSStructure WHERE Id = @ParentId 

	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From HSStructure P, @tbDirectory C , SISiteDirectory D Where D.Id=P.Id and D.Status !=dbo.GetDeleteStatus()and P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId AND D.ApplicationId=@ApplicationId)
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	Update SISiteDirectory  Set Title =T.Title,
		Description =T.Description,
		Attributes =T.Attributes,
		[Exists] =T.[Exists],
		FolderIconPath  =T.FolderIconPath,
		PhysicalPath =T.PhysicalPath,
		[Size] =T.[Size],
		Status =T.Status,
		ThumbnailImagePath =T.ThumbnailImagePath,
		ModifiedBy =@ModifiedBy, 
		ModifiedDate =@ModifiedDate,
		ObjectTypeId =T.ObjectTypeId,
		IsSystem= T.IsSystem,
		IsMarketierDir = T.IsMarketierDir 
	FROM @tbDirectory T Where T.Id = SISiteDirectory.Id AND SiteId=@ApplicationId

	If(@ParentId is null)
	Begin
		Select TOP 1 @ParentId = ParentId from @tbDirectory Order By Lft
		IF @ParentId is null or len(@ParentId )<36
		BEGIN
			IF Exists (Select * from HSDefaultParent Where ParentId = (Select TOP 1 Id from @tbDirectory Order By Lft ))
				Select @ParentId=SiteId from HSDefaultParent Where ParentId = (Select TOP 1 Id from @tbDirectory Order By Lft)
			ELSE	
			 Select TOP 1 @ParentId = ParentId From HSDefaultParent 
				Where SiteId=@ApplicationId And (ObjectTypeId=0 or ObjectTypeId=dbo.GetSiteDirectoryObjectType())
			 Order by ObjectTypeId Desc
		END
	End

	Select @PLft = LftValue , @PRgt = RgtValue From HSStructure Where Id=@ParentId AND SiteId=@ApplicationId

	If @PLft is null
	BEGIN
		RAISERROR(''INVALID||ParentId'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE H SET H.Title = T.Title
	FROM @tbDirectory T INNER JOIN HSStructure H
		ON T.Id = H.Id AND H.SiteId = @ApplicationId

	Select @NodeCount = count(*) from @tbDirectory
	SET @Lft=0
	SET @Count=0	
	SET @IsParentSet =1
	WHILE (@Count<@NodeCount)
	BEGIN

		--Selecting Record one by one
		Select Top 1 @Id =Id,
					 @Lft =Lft,
					 @Rgt =Rgt, 
					@ParentId =ParentId
		FROM @tbDirectory WHERE Lft>@Lft AND SiteId =@ApplicationId
		Order By Lft
		
		IF @IsParentSet =0 -- Select the parent Details of the current record
		BEGIN
			SELECT  @ParentId=Id	,
					@PLft = LftValue,
					@PRgt = RgtValue
					FROM HSStructure 
					WHERE Id = @ParentId AND SiteId=@ApplicationId
		END

		--Check if that node exists or not

		IF Exists (Select 1 FROM HSStructure 
					WHERE --LftValue Between @PLft and @PRgt And 
							Id=@Id  AND SiteId=@ApplicationId)	
		BEGIN
			Print  ''Node Already Exists''
			-- Node Already Exists so Just Skip
			-- Check if it has Childs if so set the Parent properties
			IF @Rgt -@Lft >1
			BEGIN
				SELECT @ParentId=Id	,
					   @PLft = LftValue,
					   @PRgt = RgtValue
				FROM HSStructure 
				WHERE LftValue Between @PLft and @PRgt And Id=@Id 
				AND SiteId=@ApplicationId
				
			END
			SET @IsParentSet = 1
		END
		ELSE
		BEGIN
			--Not Exists Insert the Node and all its child the Relation Table 
			--Create Space to Insert
			SET @Inc = (@Rgt - @Lft) + 1 -- Value to be added to create space
			UPDATE HSStructure 
			SET LftValue=CASE WHEN LftValue>=@PRgt THEN LftValue + @Inc ELSE LftValue END,
				RgtValue=CASE WHEN RgtValue>=@PRgt THEN RgtValue + @Inc ELSE RgtValue END
			Where (LftValue >=@PRgt OR RgtValue >=@PRgt)			
			AND SiteId=@ApplicationId 
			
			INSERT INTO HSStructure
			(Id,
			Title,
			ObjectTypeId,
			LftValue,
			RgtValue,
			ParentId,
			SiteId,
			MicroSiteId)
			SELECT Id,Title,dbo.GetSiteDirectoryObjectType(),(Lft-@Lft)+ @PRgt ,(Rgt-@Lft) + @PRgt, ParentId,SiteId,MicroSiteId 
			FROM @tbDirectory 
			WHERE Lft Between @Lft and @Rgt
			
			-- Increment the Count to skip the no of time loop is executed
			SET @Count =@Count + ((@Rgt - @Lft) - 1)/2
			-- Increment the Lft value of the loop skips there records

			SET @Lft = @Rgt

			--- Propagating Pemission from the Parent
			If(@ParentId is not null)
			Begin
				print ''copy permission to its  child''
				INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, Propagate, ObjectId, ObjectTypeId ) 
				(
				SELECT ProductId, ApplicationId, MemberId, MemberType, RoleId,  Propagate, @Id, ObjectTypeId  
				FROM USMemberRoles Where ObjectId = @ParentId AND ApplicationId=@ApplicationId
				)
			END
		END
	
		SET @Count= @Count + 1
	END	

	Insert into SISiteDirectory(Id,ApplicationId,Title,Description,Attributes,FolderIconPath,PhysicalPath,[Size],Status,ThumbnailImagePath,
								VirtualPath,CreatedBy,CreatedDate,ObjectTypeId,IsSystem,IsMarketierDir)
	Select Id,@ApplicationId,Title,Description,Attributes,FolderIconPath,PhysicalPath,[Size],dbo.GetActiveStatus(),ThumbnailImagePath,
								dbo.GetVirtualPath(Id),@ModifiedBy,@ModifiedDate,ObjectTypeId ,IsSystem,IsMarketierDir
	From @tbDirectory
	Where Id in (
	Select Id from @tbDirectory
	except
	Select S.Id
	FROM @tbDirectory T INNER JOIN SISiteDirectory S ON  T.Id = S.Id AND S.ApplicationId=@ApplicationId
	)

	Update S SET VirtualPath = dbo.GetVirtualPath(T.Id)
	FROM @tbDirectory T
	INNER JOIN SISiteDirectory S ON T.Id = S.Id AND S.ApplicationId=@ApplicationId
END
'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_SaveFormDirectory]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_SaveFormDirectory]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_SaveFormDirectory
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_SaveFormDirectory]
(
	@tbDirectory	TYDirectory READONLY,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE	@Lft bigint,
		@Rgt bigint,
		@Count bigint,
		@NodeCount bigint,
		@PLft bigint,
		@PRgt bigint,
		@IsParentSet bit,
		@Id uniqueidentifier,
		@Inc int,
		@ModifiedDate DateTime
		
	SET @ModifiedDate =GetUtcDate()
	
	IF @ApplicationId is null AND @ParentId is not null
		SELECT @ApplicationId = SiteId FROM COFormStructure WHERE Id = @ParentId 

	-- checking for duplicates
	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
	OR EXISTS (Select 1 From COFormStructure P, @tbDirectory C  Where  P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId )
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END   

	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From COFormStructure P, @tbDirectory C Where P.Status !=dbo.GetDeleteStatus()and P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId)
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE COFormStructure  Set Title =T.Title,
		Description =T.Description,
		Attributes =T.Attributes,
		[Exists] =T.[Exists],
		FolderIconPath  =T.FolderIconPath,
		PhysicalPath =T.PhysicalPath,
		[Size] =T.[Size],
		Status =T.Status,
		ThumbnailImagePath =T.ThumbnailImagePath,
		ModifiedBy =@ModifiedBy, 
		ModifiedDate =@ModifiedDate,
		IsMarketierDir = T.IsMarketierDir,
		AllowAccessInChildrenSites =  T.AllowAccessInChildrenSites
	FROM @tbDirectory T 
	WHERE T.Id = COFormStructure.Id AND COFormStructure.SiteId=@ApplicationId

	IF(@ParentId is null)
	BEGIN
		SELECT TOP 1 @ParentId = ParentId FROM @tbDirectory Order By Lft
		IF @ParentId is null
		BEGIN
			SELECT TOP 1 @ParentId = ParentId FROM COFormStructure 
				WHERE SiteId=@ApplicationId Order by LftValue
		END
	END

	Select @PLft = LftValue , @PRgt = RgtValue From COFormStructure Where Id=@ParentId

	If @PLft is null
	BEGIN
		RAISERROR(''INVALID||ParentId'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE H SET H.Title = T.Title
	FROM @tbDirectory T INNER JOIN COFormStructure H
		ON T.Id = H.Id AND H.SiteId = @ApplicationId

	SELECT @NodeCount = count(*) from @tbDirectory
	SET @Lft=0
	SET @Count=0	
	SET @IsParentSet =1
		
	WHILE (@Count<@NodeCount)
		BEGIN
			--Selecting Record one by one
			Select Top 1 @Id =Id,
				@Lft =Lft,
				@Rgt =Rgt, 
				@ParentId =ParentId
			FROM @tbDirectory WHERE Lft>@Lft AND SiteId =@ApplicationId
			Order By Lft
			
			IF @IsParentSet =0 -- Select the parent Details of the current record
			BEGIN
				SELECT  @ParentId=Id	,
						@PLft = LftValue,
						@PRgt = RgtValue
						FROM COFormStructure 
						WHERE Id = @ParentId AND SiteId=@ApplicationId
			END

			--Check if that node exists or not
			IF Exists (Select 1 FROM COFormStructure 
						WHERE Id=@Id AND SiteId = @ApplicationId)	
			BEGIN
				Print  ''Node Already Exists''
				-- Node Already Exists so Just Skip
				-- Check if it has Childs if so set the Parent properties
				IF @Rgt -@Lft >1
				BEGIN
					SELECT @ParentId=Id	,
						   @PLft = LftValue,
						   @PRgt = RgtValue
					FROM COFormStructure 
					WHERE LftValue Between @PLft and @PRgt And Id=@Id AND SiteId=@ApplicationId
				END
				SET @IsParentSet = 1
			END
			ELSE
			BEGIN
				--Not Exists Insert the Node and all its child the Relation Table 
				--Create Space to Insert
				SET @Inc = (@Rgt - @Lft) + 1 -- Value to be added to create space
				UPDATE COFormStructure 
				SET LftValue=CASE WHEN LftValue>=@PRgt THEN LftValue + @Inc ELSE LftValue END,
					RgtValue=CASE WHEN RgtValue>=@PRgt THEN RgtValue + @Inc ELSE RgtValue END
				Where (LftValue >=@PRgt OR RgtValue >=@PRgt)			
				AND SiteId=@ApplicationId 
				
				INSERT INTO COFormStructure
				(Id
				,Title
				,LftValue
				,RgtValue
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,Status
				,ThumbnailImagePath
				,VirtualPath
				,CreatedBy
				,CreatedDate
				,IsMarketierDir
				,AllowAccessInChildrenSites )
				SELECT Id
				,Title
				,(Lft-@Lft)+ @PRgt 
				,(Rgt-@Lft) + @PRgt
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,dbo.GetActiveStatus()
				,ThumbnailImagePath
				,dbo.GetVirtualPathByObjectType(ParentId,38)+ ''/'' + Title
				,@ModifiedBy
				,@ModifiedDate 
				,IsMarketierDir
				,AllowAccessInChildrenSites
				FROM @tbDirectory 
				WHERE Lft Between @Lft and @Rgt
				
				-- Increment the Count to skip the no of time loop is executed
				SET @Count =@Count + ((@Rgt - @Lft) - 1)/2
				-- Increment the Lft value of the loop skips there records

				SET @Lft = @Rgt

				--- Propagating Pemission from the Parent
				If(@ParentId is not null)
				Begin
					print ''copy permission to its  child''
					INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, Propagate, ObjectId, ObjectTypeId ) 
					(
					SELECT ProductId, ApplicationId, MemberId, MemberType, RoleId,  Propagate, @Id, ObjectTypeId  
					FROM USMemberRoles Where ObjectId = @ParentId AND ApplicationId=@ApplicationId
					)
				END
			END
		
			SET @Count= @Count + 1
		END	

	END
'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_SaveImageDirectory]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_SaveImageDirectory]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_SaveImageDirectory
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_SaveImageDirectory]
(
	@tbDirectory	TYDirectory READONLY,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE	@Lft bigint,
		@Rgt bigint,
		@Count bigint,
		@NodeCount bigint,
		@PLft bigint,
		@PRgt bigint,
		@IsParentSet bit,
		@Id uniqueidentifier,
		@Inc int,
		@ModifiedDate DateTime
		
	SET @ModifiedDate =GetUtcDate()
	
	IF @ApplicationId is null AND @ParentId is not null
		SELECT @ApplicationId = SiteId FROM COImageStructure WHERE Id = @ParentId 
	
	-- checking for duplicates
	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
	OR EXISTS (Select 1 From COImageStructure P, @tbDirectory C  Where  P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId )
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END   

	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From COImageStructure P, @tbDirectory C Where P.Status !=dbo.GetDeleteStatus()and P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId AND P.SiteId=@ApplicationId)
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	Update COImageStructure  Set Title =T.Title,
		Description =T.Description,
		Attributes =T.Attributes,
		[Exists] =T.[Exists],
		FolderIconPath  =T.FolderIconPath,
		PhysicalPath =T.PhysicalPath,
		[Size] =T.[Size],
		Status =T.Status,
		ThumbnailImagePath =T.ThumbnailImagePath,
		ModifiedBy =@ModifiedBy, 
		ModifiedDate =@ModifiedDate,
		AllowAccessInChildrenSites = T.AllowAccessInChildrenSites
	FROM @tbDirectory T 
	Where T.Id = COImageStructure.Id AND COImageStructure.SiteId=@ApplicationId

	IF(@ParentId is null)
	BEGIN
		SELECT TOP 1 @ParentId = ParentId FROM @tbDirectory Order By Lft
		IF @ParentId is null
		BEGIN
			SELECT TOP 1 @ParentId = ParentId FROM COImageStructure 
				WHERE SiteId=@ApplicationId Order by LftValue
		END
	END

	Select @PLft = LftValue , @PRgt = RgtValue From COImageStructure Where Id=@ParentId

	If @PLft is null
	BEGIN
		RAISERROR(''INVALID||ParentId'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE H SET H.Title = T.Title
	FROM @tbDirectory T INNER JOIN COImageStructure H
		ON T.Id = H.Id AND H.SiteId = @ApplicationId

	Select @NodeCount = count(*) from @tbDirectory
	SET @Lft=0
	SET @Count=0	
	SET @IsParentSet =1
	WHILE (@Count<@NodeCount)
	BEGIN

		--Selecting Record one by one
		Select Top 1 @Id =Id,
					 @Lft =Lft,
					 @Rgt =Rgt, 
					@ParentId =ParentId
		FROM @tbDirectory WHERE Lft>@Lft AND SiteId =@ApplicationId
		Order By Lft
		
		IF @IsParentSet =0 -- Select the parent Details of the current record
		BEGIN
			SELECT  @ParentId=Id	,
					@PLft = LftValue,
					@PRgt = RgtValue
					FROM COImageStructure 
					WHERE Id = @ParentId AND SiteId=@ApplicationId
		END

		--Check if that node exists or not

		IF Exists (Select 1 FROM COImageStructure 
					WHERE --LftValue Between @PLft and @PRgt And 
							Id=@Id  AND (SiteId=@ApplicationId OR SiteId in (SELECT ParentSiteId FROM SISite WHERE Id = @ApplicationId)))	
		BEGIN
			Print  ''Node Already Exists''
			-- Node Already Exists so Just Skip
			-- Check if it has Childs if so set the Parent properties
			IF @Rgt -@Lft >1
			BEGIN
				SELECT @ParentId=Id	,
					   @PLft = LftValue,
					   @PRgt = RgtValue
				FROM COImageStructure 
				WHERE LftValue Between @PLft and @PRgt And Id=@Id 
				AND (SiteId=@ApplicationId OR SiteId in (SELECT ParentSiteId FROM SISite WHERE Id = @ApplicationId))
				
			END
			SET @IsParentSet = 1
		END
		ELSE
		BEGIN
			--Not Exists Insert the Node and all its child the Relation Table 
			--Create Space to Insert
			SET @Inc = (@Rgt - @Lft) + 1 -- Value to be added to create space
			UPDATE COImageStructure 
			SET LftValue=CASE WHEN LftValue>=@PRgt THEN LftValue + @Inc ELSE LftValue END,
				RgtValue=CASE WHEN RgtValue>=@PRgt THEN RgtValue + @Inc ELSE RgtValue END
			Where (LftValue >=@PRgt OR RgtValue >=@PRgt)			
			AND SiteId=@ApplicationId 
			
			INSERT INTO COImageStructure
			(Id
			,Title
			,LftValue
			,RgtValue
			,ParentId
			,SiteId
			,Description
			,Attributes
			,FolderIconPath
			,PhysicalPath
			,[Size]
			,Status
			,ThumbnailImagePath
			,VirtualPath
			,CreatedBy
			,CreatedDate
			,AllowAccessInChildrenSites)
			SELECT Id
			,Title
			,(Lft-@Lft)+ @PRgt 
			,(Rgt-@Lft) + @PRgt
			,ParentId
			,SiteId
			,Description
			,Attributes
			,FolderIconPath
			,PhysicalPath
			,[Size]
			,dbo.GetActiveStatus()
			,ThumbnailImagePath
			,dbo.GetVirtualPathByObjectType(ParentId,33)+ ''/'' + Title
			,@ModifiedBy
			,@ModifiedDate 
			,AllowAccessInChildrenSites
			FROM @tbDirectory 
			WHERE Lft Between @Lft and @Rgt
			
			-- Increment the Count to skip the no of time loop is executed
			SET @Count =@Count + ((@Rgt - @Lft) - 1)/2
			-- Increment the Lft value of the loop skips there records

			SET @Lft = @Rgt

			--- Propagating Pemission from the Parent
			If(@ParentId is not null)
			Begin
				print ''copy permission to its  child''
				INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, Propagate, ObjectId, ObjectTypeId ) 
				(
				SELECT ProductId, ApplicationId, MemberId, MemberType, RoleId,  Propagate, @Id, ObjectTypeId  
				FROM USMemberRoles Where ObjectId = @ParentId AND ApplicationId=@ApplicationId
				)
			END
		END
	
		SET @Count= @Count + 1
	END	
END
'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_SaveFileDirectory]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_SaveFileDirectory]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_SaveFileDirectory
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_SaveFileDirectory]
(
	@tbDirectory	TYDirectory READONLY,
	@ParentId		uniqueidentifier,
	@ApplicationId	uniqueidentifier,
	@ModifiedBy		uniqueidentifier
)
AS
BEGIN
	DECLARE	@Lft bigint,
		@Rgt bigint,
		@Count bigint,
		@NodeCount bigint,
		@PLft bigint,
		@PRgt bigint,
		@IsParentSet bit,
		@Id uniqueidentifier,
		@Inc int,
		@ModifiedDate DateTime
	
	SET @ModifiedDate =GetUtcDate()

	IF @ApplicationId is null AND @ParentId is not null
		SELECT @ApplicationId = SiteId FROM COFileStructure WHERE Id = @ParentId 
	
	-- checking for duplicates
	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From COFileStructure P, @tbDirectory C  Where  P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId )
	BEGIN
		RAISERROR(''ISDUPLICATE||Title'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END   

	IF EXISTS (Select 1 From @tbDirectory P, @tbDirectory C Where P.ParentId=C.ParentId and P.Title = C.Title AND P.Id !=C.Id) 
		OR EXISTS (Select 1 From COFileStructure P, @tbDirectory C Where P.Status !=dbo.GetDeleteStatus()and P.Id!=C.Id AND P.ParentId=C.ParentId and LOWER(P.Title) = LOWER(C.Title) AND P.SiteId=@ApplicationId AND P.SiteId=@ApplicationId)
	BEGIN
			RAISERROR(''ISDUPLICATE||Title'', 16, 1)
			RETURN dbo.GetBusinessRuleErrorCode()
	END

	Update COFileStructure  Set Title =T.Title,
		Description =T.Description,
		Attributes =T.Attributes,
		[Exists] =T.[Exists],
		FolderIconPath  =T.FolderIconPath,
		PhysicalPath =T.PhysicalPath,
		[Size] =T.[Size],
		Status =T.Status,
		ThumbnailImagePath =T.ThumbnailImagePath,
		ModifiedBy =@ModifiedBy, 
		ModifiedDate =@ModifiedDate,
		AllowAccessInChildrenSites = T.AllowAccessInChildrenSites
	FROM @tbDirectory T 
	Where T.Id = COFileStructure.Id AND COFileStructure.SiteId=@ApplicationId

	IF(@ParentId is null)
	BEGIN
		SELECT TOP 1 @ParentId = ParentId FROM @tbDirectory Order By Lft
		IF @ParentId is null
		BEGIN
			SELECT TOP 1 @ParentId = ParentId FROM COFileStructure 
				WHERE SiteId=@ApplicationId Order by LftValue
		END
	END

	Select @PLft = LftValue , @PRgt = RgtValue From COFileStructure Where Id=@ParentId

	If @PLft is null
	BEGIN
		RAISERROR(''INVALID||ParentId'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	UPDATE H SET H.Title = T.Title
	FROM @tbDirectory T INNER JOIN COFileStructure H
		ON T.Id = H.Id AND H.SiteId = @ApplicationId

	Select @NodeCount = count(*) from @tbDirectory
	SET @Lft=0
	SET @Count=0	
	SET @IsParentSet =1
	WHILE (@Count<@NodeCount)
	BEGIN

			--Selecting Record one by one
			Select Top 1 @Id =Id,
				@Lft =Lft,
				@Rgt =Rgt, 
				@ParentId =ParentId
			FROM @tbDirectory WHERE Lft>@Lft AND SiteId =@ApplicationId
			Order By Lft
			
			IF @IsParentSet =0 -- Select the parent Details of the current record
			BEGIN
				SELECT  @ParentId=Id	,
						@PLft = LftValue,
						@PRgt = RgtValue
						FROM COFileStructure 
						WHERE Id = @ParentId AND SiteId=@ApplicationId
			END

			--Check if that node exists or not

			IF Exists (Select 1 FROM COFileStructure 
						WHERE --LftValue Between @PLft and @PRgt And 
								Id=@Id  AND (SiteId=@ApplicationId OR SiteId in (SELECT ParentSiteId FROM SISite WHERE Id = @ApplicationId)))	
			BEGIN
				Print  ''Node Already Exists''
				-- Node Already Exists so Just Skip
				-- Check if it has Childs if so set the Parent properties
				IF @Rgt -@Lft >1
				BEGIN
					SELECT @ParentId=Id	,
						   @PLft = LftValue,
						   @PRgt = RgtValue
					FROM COFileStructure 
					WHERE LftValue Between @PLft and @PRgt And Id=@Id 
					AND (SiteId=@ApplicationId OR SiteId in (SELECT ParentSiteId FROM SISite WHERE Id = @ApplicationId))
					
				END
				SET @IsParentSet = 1
			END
			ELSE
			BEGIN
				--Not Exists Insert the Node and all its child the Relation Table 
				--Create Space to Insert
				SET @Inc = (@Rgt - @Lft) + 1 -- Value to be added to create space
				UPDATE COFileStructure 
				SET LftValue=CASE WHEN LftValue>=@PRgt THEN LftValue + @Inc ELSE LftValue END,
					RgtValue=CASE WHEN RgtValue>=@PRgt THEN RgtValue + @Inc ELSE RgtValue END
				Where (LftValue >=@PRgt OR RgtValue >=@PRgt)			
				AND SiteId=@ApplicationId 
				
			
				
				INSERT INTO COFileStructure
				(Id
				,Title
				,LftValue
				,RgtValue
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,Status
				,ThumbnailImagePath
				,VirtualPath
				,CreatedBy
				,CreatedDate
				,AllowAccessInChildrenSites)
				SELECT Id
				,Title
				,(Lft-@Lft)+ @PRgt 
				,(Rgt-@Lft) + @PRgt
				,ParentId
				,SiteId
				,Description
				,Attributes
				,FolderIconPath
				,PhysicalPath
				,[Size]
				,dbo.GetActiveStatus()
				,ThumbnailImagePath
				,dbo.GetVirtualPathByObjectType(ParentId,9)+ ''/'' + Title
				,@ModifiedBy
				,@ModifiedDate 
				,AllowAccessInChildrenSites
				FROM @tbDirectory 
				WHERE Lft Between @Lft and @Rgt
				
				-- Increment the Count to skip the no of time loop is executed
				SET @Count =@Count + ((@Rgt - @Lft) - 1)/2
				-- Increment the Lft value of the loop skips there records

				SET @Lft = @Rgt

				--- Propagating Pemission from the Parent
				If(@ParentId is not null)
				Begin
					print ''copy permission to its  child''
					INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, Propagate, ObjectId, ObjectTypeId ) 
					(
					SELECT ProductId, ApplicationId, MemberId, MemberType, RoleId,  Propagate, @Id, ObjectTypeId  
					FROM USMemberRoles Where ObjectId = @ParentId AND ApplicationId=@ApplicationId
					)
				END
			END
		
			SET @Count= @Count + 1
		END	
	
END
'
GO
PRINT N'Creating [dbo].[Site_CopyDirectories]'
GO
IF OBJECT_ID(N'[dbo].[Site_CopyDirectories]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_CopyDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE procedure [dbo].[Site_CopyDirectories]
(
	@SiteId uniqueidentifier,
	@MicroSiteId uniqueidentifier,
	@ModifiedBy  uniqueidentifier,
	@ImportAllAdminUsersFromMaster bit,		
	@ObjectTypeId int,
	@ImportDirectories bit =0
)
as
begin
	declare @RootDirId uniqueidentifier
	declare @UnDirId uniqueidentifier		
    declare @SiteDirectoryObjectTypeId int
    
	SET @SiteDirectoryObjectTypeId = dbo.GetSiteDirectoryObjectType()

	if(@ImportDirectories=0)
	begin
		if(@ObjectTypeId=7) --content
		begin
			SELECT @RootDirId =newid()
			
			INSERT INTO COContentStructure 
				(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
			Values(@RootDirId,''Content Library'',null,@MicroSiteId,1,4,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/Content Library'')
			SELECT @UnDirId =newid()
							
			INSERT INTO COContentStructure
				(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
			Values(@UnDirId,''Unassigned'',null,@RootDirId,2,3,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/Content Library/Unassigned'')	
			
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,7,@RootDirId)
		end
		else if(@ObjectTypeId=9) --AssetFile
		begin
			SELECT @RootDirId =newid()
		
			INSERT INTO COFileStructure
				(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
			Values(@RootDirId,''File Library'',null,@MicroSiteId,1,4,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/File Library'')
			
			SELECT @UnDirId =newid()
		
			INSERT INTO COFileStructure
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath )
			Values(@UnDirId,''Unassigned'',null,@RootDirId,2,3,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/File Library/Unassigned'')
		    
		    INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,9,@RootDirId)
		
		end
		else if(@ObjectTypeId=33) --AssetImage
		begin
		SELECT @RootDirId =newid()
			
			INSERT INTO COImageStructure 
			(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath)
			Values(@RootDirId,''Image Library'',null,@MicroSiteId,1,4,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/Image Library'')
		
			SELECT @UnDirId =newid()
		
			INSERT INTO COImageStructure
				(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath)
			Values(@UnDirId,''Unassigned'',null,@RootDirId,2,3,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/Image Library/Unassigned'')
			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,33,@RootDirId)
		end
		else if(@ObjectTypeId=38) --Forms
		begin		
			SELECT @RootDirId =newid()
			SELECT @UnDirId =newid()

			INSERT INTO COFormStructure 
				(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath)
    				Values(@RootDirId,''Form Library'',null,@MicroSiteId,1,4,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/Form Library'')
		
			INSERT INTO COFormStructure
				(Id,Title,Keywords,ParentId,LftValue,RgtValue,SiteId,Status, CreatedBy, CreatedDate, VirtualPath)
    				Values(@UnDirId,''Unassigned'',null,@RootDirId,2,3,@MicroSiteId,1, @ModifiedBy, GETUTCDATE(),''~/Form Library/Unassigned'')

			INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,38,@RootDirId)
			
		end
		
	end
	else
	begin
		if(@ObjectTypeId=7) --content
		begin
			Print ''Copy Content Directory''
			INSERT INTO [dbo].[COContentStructure]
			   ([Id]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[Title]
			   ,[Description]
			   ,[SiteId]
			   ,[Exists]
			   ,[Attributes]
			   ,[Size]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PhysicalPath]
			   ,[VirtualPath]
			   ,[ThumbnailImagePath]
			   ,[FolderIconPath]
			   ,[Keywords]
			   ,[IsSystem]
			   ,[IsMarketierDir] 
			   ,[SourceDirId])
			select NEWID()
			   ,ParentId
			   ,LftValue
			   ,RgtValue
			   ,Title
			   ,Description
			   ,@MicroSiteId 
			   ,[Exists]
			   ,Attributes
			   ,Size
			   ,Status
			   ,@ModifiedBy 
			   ,GETUTCDATE()
			   ,null
			   ,null
			   ,PhysicalPath
			   ,VirtualPath
			   ,ThumbnailImagePath
			   ,FolderIconPath
			   ,Keywords
			   ,IsSystem
			   ,IsMarketierDir 
			   ,Id from [dbo].[COContentStructure] where SiteId=@SiteId  and IsMarketierDir !=1
	           
	           
	       update [dbo].[COContentStructure] set ParentId= @MicroSiteId where SiteId=@MicroSiteId and ParentId=@SiteId 
	           
		   Update  M Set M.ParentId = P.Id
		   FROM [dbo].[COContentStructure] M
		   INNER JOIN [dbo].[COContentStructure] P ON P.SourceDirId = M.ParentId
		   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
           
           set @RootDirId = ( select CS.Id FROM [dbo].[COContentStructure] CS
		   INNER JOIN HSDefaultParent HS on CS.SourceDirId=HS.ParentId where HS.SiteId=@SiteId and ObjectTypeId =7 and CS.SiteId=@MicroSiteId)	
		    
           INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,7,@RootDirId)		 
          		 
			if(@ImportAllAdminUsersFromMaster = 1)
			   begin
				  INSERT INTO [dbo].[USMemberRoles]
						   ([ProductId]
						   ,[ApplicationId]
						   ,[MemberId]
						   ,[MemberType]
						   ,[RoleId]
						   ,[ObjectId]
						   ,[ObjectTypeId]
						   ,[Propagate]
						   )
				SELECT Distinct [ProductId]
					  ,@MicroSiteId
					  ,[MemberId]
					  ,[MemberType]
					  ,[RoleId]
					  ,P.Id
					  ,[ObjectTypeId]
					  ,[Propagate]
				  FROM [dbo].[USMemberRoles]M
				INNER JOIN COContentStructure P ON M.ObjectId = P.SourceDirId
				 Where P.SiteId = @MicroSiteId 
				 and MemberId not in(select Id from USGroup where GroupType=2) -- Excluding custom groups, custom groups are not shared with variant sites
			 end 
		end
		else if(@ObjectTypeId =9) -- AssetFile
		begin
			INSERT INTO [dbo].[COFileStructure]
			   ([Id]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[Title]
			   ,[Description]
			   ,[SiteId]
			   ,[Exists]
			   ,[Attributes]
			   ,[Size]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PhysicalPath]
			   ,[VirtualPath]
			   ,[ThumbnailImagePath]
			   ,[FolderIconPath]
			   ,[Keywords]
			   ,[IsSystem]
			   ,[InheritSecurityLevels]
			   ,[PropagateSecurityLevels]
			   ,[SourceDirId])
			select NEWID()
			   ,ParentId
			   ,LftValue
			   ,RgtValue
			   ,Title
			   ,Description
			   ,@MicroSiteId
			   ,[Exists]
			   ,Attributes
			   ,Size
			   ,Status
			   ,@ModifiedBy 
			   ,GETUTCDATE()
			   ,null
			   ,null
			   ,PhysicalPath
			   ,VirtualPath
			   ,ThumbnailImagePath
			   ,FolderIconPath
			   ,Keywords
			   ,IsSystem
			   ,InheritSecurityLevels
			   ,PropagateSecurityLevels
			   ,Id from [dbo].[COFileStructure] where SiteId=@SiteId
	           
	       update [dbo].[COFileStructure] set ParentId= @MicroSiteId where SiteId=@MicroSiteId and ParentId=@SiteId     
	           
		   Update  M Set M.ParentId = P.Id
		   FROM [dbo].[COFileStructure] M
		   INNER JOIN [dbo].[COFileStructure] P ON P.SourceDirId = M.ParentId
		   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
           
           set @RootDirId = ( select FS.Id FROM [dbo].[COFileStructure] FS
		   INNER JOIN HSDefaultParent HS on FS.SourceDirId=HS.ParentId where HS.SiteId=@SiteId and ObjectTypeId =9 and FS.SiteId=@MicroSiteId)	
		    
           INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,9,@RootDirId)
           
           INSERT INTO [dbo].[USSecurityLevelObject]
			   ([ObjectId]
			   ,[ObjectTypeId]
			   ,[SecurityLevelId])
		  SELECT P.Id
				   ,ObjectTypeId
				   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN COFileStructure P ON SO.ObjectId = P.SourceDirId
			 Where P.SiteId = @MicroSiteId
		 
			if(@ImportAllAdminUsersFromMaster = 1)
			   begin
				  INSERT INTO [dbo].[USMemberRoles]
						   ([ProductId]
						   ,[ApplicationId]
						   ,[MemberId]
						   ,[MemberType]
						   ,[RoleId]
						   ,[ObjectId]
						   ,[ObjectTypeId]
						   ,[Propagate]
						   )
				SELECT Distinct [ProductId]
					  ,@MicroSiteId
					  ,[MemberId]
					  ,[MemberType]
					  ,[RoleId]
					  ,P.Id
					  ,[ObjectTypeId]
					  ,[Propagate]
				  FROM [dbo].[USMemberRoles]M
				INNER JOIN COFileStructure P ON M.ObjectId = P.SourceDirId
				 Where P.SiteId = @MicroSiteId 
				 and MemberId not in(select Id from USGroup where GroupType=2) -- Excluding custom groups, custom groups are not shared with variant sites
			 end 
           
		end
		else if(@ObjectTypeId=33) --AssetImage
		begin
			Print ''Copy Image Directory''
			INSERT INTO [dbo].[COImageStructure]
			   ([Id]
			   ,[ParentId]
			   ,[LftValue]
			   ,[RgtValue]
			   ,[Title]
			   ,[Description]
			   ,[SiteId]
			   ,[Exists]
			   ,[Attributes]
			   ,[Size]
			   ,[Status]
			   ,[CreatedBy]
			   ,[CreatedDate]
			   ,[ModifiedBy]
			   ,[ModifiedDate]
			   ,[PhysicalPath]
			   ,[VirtualPath]
			   ,[ThumbnailImagePath]
			   ,[FolderIconPath]
			   ,[Keywords]
			   ,[IsSystem]
			   ,[InheritSecurityLevels]
			   ,[PropagateSecurityLevels]
			   ,[SourceDirId])
			select NEWID()
			   ,ParentId
			   ,LftValue
			   ,RgtValue
			   ,Title
			   ,Description
			   ,@MicroSiteId
			   ,[Exists]
			   ,Attributes
			   ,Size
			   ,Status
			   ,@ModifiedBy 
			   ,GETUTCDATE()
			   ,null
			   ,null
			   ,PhysicalPath
			   ,VirtualPath
			   ,ThumbnailImagePath
			   ,FolderIconPath
			   ,Keywords
			   ,IsSystem
			   ,InheritSecurityLevels
			   ,PropagateSecurityLevels
			   ,Id from [dbo].[COImageStructure] where SiteId=@SiteId
	           
	           update [dbo].[COImageStructure] set ParentId= @MicroSiteId where SiteId=@MicroSiteId and ParentId=@SiteId    
		   Update  M Set M.ParentId = P.Id
		   FROM [dbo].[COImageStructure] M
		   INNER JOIN [dbo].[COImageStructure] P ON P.SourceDirId = M.ParentId
		   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
           
           set @RootDirId = ( select FS.Id FROM [dbo].[COImageStructure] FS
		   INNER JOIN HSDefaultParent HS on FS.SourceDirId=HS.ParentId where HS.SiteId=@SiteId and ObjectTypeId =33 and FS.SiteId=@MicroSiteId)	
		    
           INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,33,@RootDirId)
           
           INSERT INTO [dbo].[USSecurityLevelObject]
			   ([ObjectId]
			   ,[ObjectTypeId]
			   ,[SecurityLevelId])
		  SELECT P.Id
				   ,ObjectTypeId
				   ,SecurityLevelId
			FROM [USSecurityLevelObject] SO
			INNER JOIN COImageStructure P ON SO.ObjectId = P.SourceDirId
			 Where P.SiteId = @MicroSiteId
		 
			if(@ImportAllAdminUsersFromMaster = 1)
			   begin
				  INSERT INTO [dbo].[USMemberRoles]
						   ([ProductId]
						   ,[ApplicationId]
						   ,[MemberId]
						   ,[MemberType]
						   ,[RoleId]
						   ,[ObjectId]
						   ,[ObjectTypeId]
						   ,[Propagate]
						   )
				SELECT Distinct [ProductId]
					  ,@MicroSiteId
					  ,[MemberId]
					  ,[MemberType]
					  ,[RoleId]
					  ,P.Id
					  ,[ObjectTypeId]
					  ,[Propagate]
				  FROM [dbo].[USMemberRoles]M
				INNER JOIN COImageStructure P ON M.ObjectId = P.SourceDirId
				 Where P.SiteId = @MicroSiteId 
				 and MemberId not in(select Id from USGroup where GroupType=2) -- Excluding custom groups, custom groups are not shared with variant sites
			 end 
		end
		else if(@ObjectTypeId=38) --Forms
		begin
			Print ''Copy Form Directory''

INSERT INTO [dbo].[COFormStructure]
           ([Id]
           ,[ParentId]
           ,[LftValue]
           ,[RgtValue]
           ,[Title]
           ,[Description]
           ,[SiteId]
           ,[Exists]
           ,[Attributes]
           ,[Size]
           ,[Status]
           ,[CreatedBy]
           ,[CreatedDate]
           ,[ModifiedBy]
           ,[ModifiedDate]
           ,[PhysicalPath]
           ,[VirtualPath]
           ,[ThumbnailImagePath]
           ,[FolderIconPath]
           ,[Keywords]
           ,[IsSystem]
           ,[IsMarketierDir]
           ,[SourceDirId])
			select NEWID()
			   ,ParentId
			   ,LftValue
			   ,RgtValue
			   ,Title
			   ,Description
			   ,@MicroSiteId
			   ,[Exists]
			   ,Attributes
			   ,Size
			   ,Status
			   ,@ModifiedBy 
			   ,GETUTCDATE()
			   ,null
			   ,null
			   ,PhysicalPath
			   ,VirtualPath
			   ,ThumbnailImagePath
			   ,FolderIconPath
			   ,Keywords
			   ,IsSystem
			   ,0			  
			   ,Id from [dbo].[COFormStructure] where SiteId=@SiteId
	           
	           update [dbo].[COFormStructure] set ParentId= @MicroSiteId where SiteId=@MicroSiteId and ParentId=@SiteId 
	              
		   Update  M Set M.ParentId = P.Id
		   FROM [dbo].[COFormStructure] M
		   INNER JOIN [dbo].[COFormStructure] P ON P.SourceDirId = M.ParentId
		   Where P.SiteId =@MicroSiteId and M.SiteId=@MicroSiteId
           
           set @RootDirId = ( select FS.Id FROM [dbo].[COFormStructure] FS INNER JOIN HSDefaultParent HS on FS.SourceDirId=HS.ParentId where HS.SiteId=@SiteId and ObjectTypeId =38 and FS.SiteId=@MicroSiteId)	
		    
           INSERT INTO HSDefaultParent(SiteId,ObjectTypeId,ParentId)VALUES(@MicroSiteId,38,@RootDirId)
           
			   
			   
	    end       
    



		
		
		
	end

end

'
GO
PRINT N'Creating [dbo].[Site_GetSiteByHierarchy]'
GO
IF OBJECT_ID(N'[dbo].[Site_GetSiteByHierarchy]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_GetSiteByHierarchy
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Site_GetSiteByHierarchy ''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'',-1,null,null,null,''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4''
--exec Site_GetSiteByHierarchy ''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'',2
--exec Site_GetSiteByHierarchy @ParentSiteId=''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'',@Level=-1,@GroupId=NULL,@PageIndex=NULL,@PageSize=NULL,@MasterSiteId=''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4''
--exec Site_GetSiteByHierarchy @ParentSiteId=''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4'',@Level=-1,@GroupId=''89A5C527-2FE0-45A5-8ADA-575551BDC52E'',@PageIndex=null,@PageSize=null,@MasterSiteId=''8039CE09-E7DA-47E1-BCEC-DF96B5E411F4''
CREATE PROCEDURE [dbo].[Site_GetSiteByHierarchy]  
(  
	@ParentSiteId	uniqueIdentifier,
	@Level int,
	@GroupId uniqueIdentifier = null,
	@PageIndex		int = null,  
	@PageSize		int = null,
	@MasterSiteId	uniqueIdentifier,
	@Status int = null,
	@IncludeParent bit = null
)  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	DECLARE @Lft bigint, @Rgt bigint
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from SISite Where Id = @ParentSiteId
	
	IF(@IncludeParent IS NULL)
		SET @IncludeParent = 1
		
	IF @Level = 1
	BEGIN
			IF @PageSize is Null  
			BEGIN  
				SELECT DISTINCT S.[Id]
				  ,[Title]
				  ,[Description]
				  ,CAST([URL] AS NVARCHAR(MAX)) AS URL
				  ,[VirtualPath]
				  ,[ParentSiteId]
				  ,[Type]
				  ,[Keywords]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate]
				  ,[Status]
				  ,[LftValue]
				  ,[RgtValue]
				  ,CU.UserFullName CreatedByFullName
				  ,MU.UserFullName ModifiedByFullName
				  ,dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath
				  ,MasterSiteId
				  ,PrimarySiteUrl
			  FROM [SISite] S
			  LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy  
			  LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy
			  LEFT OUTER JOIN SISiteGroup SG ON SG.SiteId =  S.Id  
			  WHERE S.Id = @ParentSiteId And S.Status != @DeleteStatus
			  AND (@GroupId IS NULL OR SG.GroupId = @GroupId)
			   AND MasterSiteId = @MasterSiteId
			   AND (@Status IS NULL OR S.Status = @Status)
			  ORDER BY LftValue
		  END
		  ELSE
		  BEGIN
		  
		  ;WITH tmpResultEntries AS (   
			SELECT DISTINCT
				  S.[Id]
				  ,[Title]
				  ,[Description]
				  ,CAST([URL] AS NVARCHAR(MAX)) AS URL
				  ,[VirtualPath]
				  ,[ParentSiteId]
				  ,[Type]
				  ,[Keywords]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate]
				  ,[Status]
				  ,[LftValue]
				  ,[RgtValue]
				  ,CU.UserFullName CreatedByFullName
				  ,MU.UserFullName ModifiedByFullName
				  ,dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath
				  ,MasterSiteId
				  ,PrimarySiteUrl
			  FROM [SISite] S
			  LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy  
			  LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy
			  LEFT OUTER JOIN SISiteGroup SG ON SG.SiteId =  S.Id  
			  WHERE S.Id = @ParentSiteId And S.Status != @DeleteStatus
			  AND (@GroupId IS NULL OR SG.GroupId = @GroupId) 
			   AND MasterSiteId = @MasterSiteId
			   AND (@Status IS NULL OR S.Status = @Status)
			  ),
			  
			ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY LftValue) AS Row
				  ,[Id]
				  ,[Title]
				  ,[Description]
				  ,URL
				  ,[VirtualPath]
				  ,[ParentSiteId]
				  ,[Type]
				  ,[Keywords]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate]
				  ,[Status]
				  ,[LftValue]
				  ,[RgtValue]
				  ,CreatedByFullName
				  ,ModifiedByFullName
				  ,SitePath
				  ,MasterSiteId
				  ,PrimarySiteUrl
			  FROM tmpResultEntries 
			  )
			  --ORDER BY LftValue 
			  
			  SELECT [Id]
			  ,[Title]
			  ,[Description]
			  ,[URL]
			  ,[VirtualPath]
			  ,[ParentSiteId]
			  ,[Type]
			  ,[Keywords]
			  ,[CreatedBy]
			  ,[CreatedDate]
			  ,[ModifiedBy]
			  ,[ModifiedDate]
			  ,[Status]
			  ,[LftValue]
			  ,[RgtValue]
			  ,CreatedByFullName
			  ,ModifiedByFullName
			  ,SitePath
			  ,MasterSiteId
			  ,PrimarySiteUrl
			  FROM ResultEntries  A  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize 
			  
		  END	  

	END
	ELSE IF @Level <=-1
	BEGIN
			
			IF @PageSize is Null  
			BEGIN
				SELECT DISTINCT S.[Id]
				  ,[Title]
				  ,[Description]
				  ,CAST([URL] AS NVARCHAR(MAX)) AS URL
				  ,[VirtualPath]
				  ,[ParentSiteId]
				  ,[Type]
				  ,[Keywords]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate]
				  ,[Status]
				  ,[LftValue]
				  ,[RgtValue]
				  ,CU.UserFullName CreatedByFullName
				  ,MU.UserFullName ModifiedByFullName
				  ,dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath
				  ,MasterSiteId
				  ,PrimarySiteUrl
			 FROM [SISite] S
			 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy  
			 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy 
			 LEFT OUTER JOIN SISiteGroup SG ON SG.SiteId =  S.Id  
			 WHERE ((@IncludeParent = 1 AND S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() )
			 OR 
			 (@IncludeParent = 0 AND S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() )
			 )
			 --WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() 
			 --WHERE S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() 
			 AND (@GroupId IS NULL OR SG.GroupId = @GroupId)
			 AND MasterSiteId = @MasterSiteId
			 AND (@Status IS NULL OR S.Status = @Status)
			 ORDER BY LftValue  
		END
		ELSE
		BEGIN
		
		;WITH tmpResultEntries AS (   
			SELECT DISTINCT
				S.[Id]
				  ,[Title]
				  ,[Description]
				  ,CAST([URL] AS NVARCHAR(MAX)) AS URL
				  ,[VirtualPath]
				  ,[ParentSiteId]
				  ,[Type]
				  ,[Keywords]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate]
				  ,[Status]
				  ,[LftValue]
				  ,[RgtValue]
				  ,CU.UserFullName CreatedByFullName
				  ,MU.UserFullName ModifiedByFullName
				  ,dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath
				  ,MasterSiteId
				  ,PrimarySiteUrl
			 FROM [SISite] S
			 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy  
			 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy 
			 LEFT OUTER JOIN SISiteGroup SG ON SG.SiteId =  S.Id  
			  WHERE ((@IncludeParent = 1 AND S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() )
			 OR 
			 (@IncludeParent = 0 AND S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() )
			 )
			 --WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() 
			 --WHERE S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() 
			 AND (@GroupId IS NULL OR SG.GroupId = @GroupId)
			  AND MasterSiteId = @MasterSiteId
			  AND (@Status IS NULL OR S.Status = @Status)
			 ),
			ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY LftValue) AS Row,
				[Id]
				  ,[Title]
				  ,[Description]
				  ,[URL] 
				  ,[VirtualPath]
				  ,[ParentSiteId]
				  ,[Type]
				  ,[Keywords]
				  ,[CreatedBy]
				  ,[CreatedDate]
				  ,[ModifiedBy]
				  ,[ModifiedDate]
				  ,[Status]
				  ,[LftValue]
				  ,[RgtValue]
				  ,CreatedByFullName
				  ,ModifiedByFullName
				  ,SitePath
				  ,MasterSiteId
				  ,PrimarySiteUrl
			 FROM tmpResultEntries
			 )
			 
			 SELECT [Id]
			  ,[Title]
			  ,[Description]
			  ,[URL]
			  ,[VirtualPath]
			  ,[ParentSiteId]
			  ,[Type]
			  ,[Keywords]
			  ,[CreatedBy]
			  ,[CreatedDate]
			  ,[ModifiedBy]
			  ,[ModifiedDate]
			  ,[Status]
			  ,[LftValue]
			  ,[RgtValue]
			  ,CreatedByFullName
			  ,ModifiedByFullName
			  ,SitePath
			  ,MasterSiteId
			  ,PrimarySiteUrl
			  FROM ResultEntries  A  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize  
		END

	END
	ELSE 	
	BEGIN
			IF @PageSize is Null  
			BEGIN
				SELECT DISTINCT S.[Id]
					  ,[Title]
					  ,[Description]
					  ,CAST([URL] AS NVARCHAR(MAX)) AS URL
					  ,[VirtualPath]
					  ,[ParentSiteId]
					  ,[Type]
					  ,[Keywords]
					  ,[CreatedBy]
					  ,[CreatedDate]
					  ,[ModifiedBy]
					  ,[ModifiedDate]
					  ,[Status]
					  ,[LftValue]
					  ,[RgtValue]
					  ,CU.UserFullName CreatedByFullName
					  ,MU.UserFullName ModifiedByFullName
					  ,dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath
					  ,MasterSiteId
					  ,PrimarySiteUrl
				 FROM [SISite] S
				 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy  
				 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy
				 LEFT OUTER JOIN SISiteGroup SG ON SG.SiteId =  S.Id    
				WHERE S.LftValue between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			   And @Level>=(Select count(*) + 1 From SISite M   
					WHERE M.LftValue Between @Lft and @Rgt  
						And S.LftValue Between M.LftValue and M.RgtValue  
						And M.LftValue Not in (S.LftValue ,@Lft))
						AND (@GroupId IS NULL OR SG.GroupId = @GroupId)  
				     AND MasterSiteId = @MasterSiteId
				     AND (@Status IS NULL OR S.Status = @Status)
			ORDER BY LftValue
		END
		ELSE
		BEGIN
		
		;WITH tmpResultEntries AS (   
			SELECT DISTINCT S.[Id]
					  ,[Title]
					  ,[Description]
					  ,CAST([URL] AS NVARCHAR(MAX)) AS URL
					  ,[VirtualPath]
					  ,[ParentSiteId]
					  ,[Type]
					  ,[Keywords]
					  ,[CreatedBy]
					  ,[CreatedDate]
					  ,[ModifiedBy]
					  ,[ModifiedDate]
					  ,[Status]
					  ,[LftValue]
					  ,[RgtValue]
					  ,CU.UserFullName CreatedByFullName
					  ,MU.UserFullName ModifiedByFullName
					  ,dbo.GetSiteHierarchyPath(s.Id, S.MasterSiteId) AS SitePath
					  ,MasterSiteId
					  ,PrimarySiteUrl
				 FROM [SISite] S
				 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S.CreatedBy  
				 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S.ModifiedBy
				 LEFT OUTER JOIN SISiteGroup SG ON SG.SiteId =  S.Id    
				WHERE S.LftValue between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			   And @Level>=(Select count(*) + 1 From SISite M   
					WHERE M.LftValue Between @Lft and @Rgt  
						And S.LftValue Between M.LftValue and M.RgtValue  
						And M.LftValue Not in (S.LftValue ,@Lft))
						AND (@GroupId IS NULL OR SG.GroupId = @GroupId)
						 AND MasterSiteId = @MasterSiteId  
						 AND (@Status IS NULL OR S.Status = @Status)
			
			),
			
			ResultEntries AS (   
			SELECT ROW_NUMBER() OVER (ORDER BY LftValue) AS Row,
						[Id]
					  ,[Title]
					  ,[Description]
					  ,URL
					  ,[VirtualPath]
					  ,[ParentSiteId]
					  ,[Type]
					  ,[Keywords]
					  ,[CreatedBy]
					  ,[CreatedDate]
					  ,[ModifiedBy]
					  ,[ModifiedDate]
					  ,[Status]
					  ,[LftValue]
					  ,[RgtValue]
					  ,CreatedByFullName
					  ,ModifiedByFullName
					  ,SitePath
					  ,MasterSiteId
					  ,PrimarySiteUrl
				 FROM tmpResultEntries
			)
			
			SELECT [Id]
			  ,[Title]
			  ,[Description]
			  ,[URL]
			  ,[VirtualPath]
			  ,[ParentSiteId]
			  ,[Type]
			  ,[Keywords]
			  ,[CreatedBy]
			  ,[CreatedDate]
			  ,[ModifiedBy]
			  ,[ModifiedDate]
			  ,[Status]
			  ,[LftValue]
			  ,[RgtValue]
			  ,CreatedByFullName
			  ,ModifiedByFullName
			  ,SitePath
			  ,MasterSiteId
			  ,PrimarySiteUrl
			  FROM ResultEntries  A  
		WHERE 
			Row BETWEEN(@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize 
		END  
	
	END
END


'
GO
PRINT N'Creating [dbo].[Post_GetAllLabels]'
GO
IF OBJECT_ID(N'[dbo].[Post_GetAllLabels]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Post_GetAllLabels
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Post_GetAllLabels]
(
	@BlogId			uniqueIdentifier = null,
	@PostId			uniqueIdentifier = null,
	@ApplicationId	uniqueIdentifier
)
AS

BEGIN
	SELECT	DISTINCT Labels
	FROM	BLPost
	WHERE	ApplicationId = @ApplicationId AND
		(@BlogId IS NULL OR BlogId = @BlogId) AND
		(@PostId IS NULL OR Id = @PostId)
END

'
GO
PRINT N'Creating [dbo].[Log_WriteLog]'
GO
IF OBJECT_ID(N'[dbo].[Log_WriteLog]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Log_WriteLog
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Log_WriteLog]
(
	@EventID int, 
	@Priority int, 
	@Severity nvarchar(32), 
	@Title nvarchar(256), 
	@Timestamp datetime,
	@MachineName nvarchar(32), 
	@AppDomainName nvarchar(512),
	@ProcessID nvarchar(256),
	@ProcessName nvarchar(512),
	@ThreadName nvarchar(512),
	@Win32ThreadId nvarchar(128),
	@Message nvarchar(1500),
	@FormattedMessage xml,
	@LogId int OUTPUT
)
AS 
BEGIN
	INSERT INTO [LGLog] (
		SiteId,
		EventId,
		Priority,
		Severity,
		CreatedDate,
		MachineName,
		ProcessId,
		ActivityId,
		Message,
		AdditionalInfo
	)
	VALUES (
		@FormattedMessage.value(''(//SiteId)[1]'',''uniqueidentifier''),
		@EventID,
		@Priority,
		@Severity, 
		@Timestamp,
		@MachineName, 
		@ProcessID,
		@FormattedMessage.value(''(//ActivityId)[1]'',''uniqueidentifier''),
		@FormattedMessage.value(''(//Message)[1]'',''nvarchar(max)''),
		null)

	SET @LogID = @@IDENTITY
	
	IF @EventID = 100
	BEGIN
		INSERT INTO LGExceptionLog (
			LogId,
			StackTrace,
			InnerException
		)
		VALUES (
			@LogId,
			@FormattedMessage.value(''(//StackTrace)[1]'',''nvarchar(max)''),
			@FormattedMessage.value(''(//InnerException)[1]'',''nvarchar(max)'')
		)
	END
	
	RETURN @LogID
END



'
GO
PRINT N'Creating [dbo].[User_SaveSocialUser]'
GO
IF OBJECT_ID(N'[dbo].[User_SaveSocialUser]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : User_SaveSocialUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE PROCEDURE [dbo].[User_SaveSocialUser](
	@UserId uniqueidentifier,
	@ExternalId nvarchar(500),
	@APIToken nvarchar(500),
	@ApplicationId uniqueidentifier=null)
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM dbo.USSocialUserProfile Where UserId=@UserId and (SiteId  = @ApplicationId OR @ApplicationId is null))
	UPDATE  dbo.USSocialUserProfile 
		SET ExternalId = @ExternalId
			,APIToken=@APIToken
		Where UserId=@UserId and (SiteId  = @ApplicationId OR @ApplicationId is null)
	ELSE
	INSERT INTO dbo.USSocialUserProfile 
	(Id
	 ,UserId
	 ,SiteId
	 ,ExternalId
	 ,APIToken
	 )
	 VALUES
	 (newid()
	 ,@UserId
	 ,@ApplicationId
	 ,@ExternalId
	 ,@ApiToken
	 )	
	
END

'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_GetSharedDirectoriesByLevel
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_GetSharedDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier,
	@ObjectType		int
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from HSStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from HSStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (S.ParentId =@ParentId OR S.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId )
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR S.Id=@ParentId)
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id 
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = S.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.Id=@ParentId And D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) AND D.Status != dbo.GetDeleteStatus()
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure S Inner Join SISiteDirectory D ON S.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE S.LftValue < @Lft and S.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from HSStructure P Where S.LftValue > P.LftValue and S.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND S.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,D.ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM HSStructure  C  Inner Join SISiteDirectory D ON C.Id = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=C.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE C.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() And (D.ObjectTypeId =ISNULL(@ObjectType,D.ObjectTypeId) OR C.Id=@ParentId)
			And @ChildrenLevel>=(Select count(*) + 1 
				From HSStructure M 
				WHERE M.LftValue Between @Lft and @Rgt
				And C.LftValue Between M.LftValue and M.RgtValue
				And M.LftValue Not in (C.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND C.SiteId in (Select SiteId from dbo.GetVariantSites(@SiteId) )
		ORDER BY LftValue
	END 
END
'
GO
PRINT N'Creating [dbo].[Site_GetSiteByHierarchy_Count]'
GO
IF OBJECT_ID(N'[dbo].[Site_GetSiteByHierarchy_Count]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_GetSiteByHierarchy_Count
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Site_GetSiteByHierarchy_Count]  
(  
	@ParentSiteId	uniqueIdentifier,
	@Level int,
	@GroupId uniqueIdentifier = null,
	@PageIndex		int,  
	@PageSize		int,
	@Status int = null,
	@MasterSiteId uniqueIdentifier,
	@IncludeParent bit = null
	 
)  
AS  
BEGIN  
	DECLARE @EmptyGUID uniqueIdentifier  
	SET @EmptyGUID = dbo.GetEmptyGUID()  
	
	DECLARE @DeleteStatus int	
	SET @DeleteStatus = dbo.GetDeleteStatus()
	
	IF(@IncludeParent IS NULL)
		SET @IncludeParent = 1
		
	DECLARE @Lft bigint, @Rgt bigint
		
	SELECT @Lft = LftValue , @Rgt = RgtValue from SISite Where Id = @ParentSiteId
	
	IF @Level = 1
	BEGIN
			IF(@GroupId is null)
				  SELECT DISTINCT count(*)   
				  FROM [SISite] S
				  WHERE S.Id = @ParentSiteId And S.Status != @DeleteStatus
				  AND (@Status IS NULL OR S.Status = @Status)
				   AND MasterSiteId = @MasterSiteId
				   
		   ELSE
				  SELECT DISTINCT count(*)   
				  FROM [SISite] S
				  LEFT JOIN (Select SG.SiteId, SG.Id, SG.GroupId From SISiteGroup SG where SG.GroupId = @GroupId) T
				  ON S.Id = T.SiteId
				  WHERE S.Id = @ParentSiteId And S.Status != @DeleteStatus
				 AND (@GroupId IS NULL OR T.GroupId = @GroupId)
				  AND (@Status IS NULL OR S.Status = @Status)
				   AND MasterSiteId = @MasterSiteId
		   

	END
	ELSE IF @Level <=-1
	BEGIN
			IF(@GroupId is null)
			SELECT count(*)
			  FROM [SISite] S
			 WHERE ((@IncludeParent = 1 AND S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() )
			 OR 
			 (@IncludeParent = 0 AND S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() )
			 )
			 --WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() 
			 --WHERE S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() 
			 AND (@Status IS NULL OR S.Status = @Status)
			 AND MasterSiteId = @MasterSiteId
			ELSE
			SELECT count(*)
			  FROM [SISite] S
			  LEFT JOIN (Select SG.SiteId, SG.Id, SG.GroupId From SISiteGroup SG where SG.GroupId = @GroupId) T
			  ON S.Id = T.SiteId
			  WHERE ((@IncludeParent = 1 AND S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() )
			 OR 
			 (@IncludeParent = 0 AND S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() )
			 )
			  --WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus() 
			 --WHERE S.LftValue > @Lft and S.LftValue <= @Rgt And S.Status != dbo.GetDeleteStatus() 
			 AND (@GroupId IS NULL OR T.GroupId = @GroupId)
			 AND (@Status IS NULL OR S.Status = @Status)
			 AND MasterSiteId = @MasterSiteId
			 
		END
	ELSE 	
	BEGIN
	
			IF(@GroupId is null)
						SELECT count(*)
					  FROM [SISite] S
						WHERE S.LftValue between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
					   And @Level>=(Select count(*) + 1 From SISite M   
							WHERE M.LftValue Between @Lft and @Rgt  
								And S.LftValue Between M.LftValue and M.RgtValue  
								And M.LftValue Not in (S.LftValue ,@Lft))
								AND (@Status IS NULL OR S.Status = @Status)
								 AND MasterSiteId = @MasterSiteId
			ELSE
					SELECT count(*)
					  FROM [SISite] S
					  LEFT JOIN (Select SG.SiteId, SG.Id, SG.GroupId From SISiteGroup SG where SG.GroupId = @GroupId) T
					  ON S.Id = T.SiteId
						WHERE S.LftValue between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
					   And @Level>=(Select count(*) + 1 From SISite M   
							WHERE M.LftValue Between @Lft and @Rgt  
								And S.LftValue Between M.LftValue and M.RgtValue  
								And M.LftValue Not in (S.LftValue ,@Lft))
								AND (@GroupId IS NULL OR T.GroupId = @GroupId)
								AND (@Status IS NULL OR S.Status = @Status)
								 AND MasterSiteId = @MasterSiteId
			
		END
		
END


'
GO
PRINT N'Creating [dbo].[Search_GetWorkflows]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetWorkflows]', 'P') IS NULL
EXEC sp_executesql N'



-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the workflow data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetWorkflows] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetWorkflows]
--********************************************************************************
-- Input Parameter
    @SiteId UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
		SELECT WF.Id,
		       WF.ApplicationId,
		       WF.ArchivedDate,
		       WF.CreatedBy,
		       WF.CreatedDate,
		       WF.Description,
		       WF.IsGlobal,
		       WF.ModifiedBy,
		       WF.ModifiedDate,
		       WF.ObjectTypeId,
		       WF.ProductId,
		       WF.RootId,
		       WF.SkipAfterDays,
		       WF.Status,
		       WF.Title,
		       WFE.ActionId,
		       WFE.ActionedDate,
		       WFE.ActorId,
		       WFE.ActorType,
		       WFE.ActorType,
		       WFE.Completed,
		       WFE.ObjectId,
		       WFE.Remarks,
		       WFE.SequenceId,
		       WFE.SubmittedBy,
		       WFE.WorkflowExecId,
		       WFE.WorkflowId 
		FROM WFWorkflow WF 
		INNER JOIN WFWorkflowExecution WFE ON WF.Id= WFE.WorkflowId 
END
--********************************************************************************


'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_GetFormDirectoriesByLevel
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_GetFormDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 38
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFormStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COFormStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFormStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFormStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COFormStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_GetImageDirectoriesByLevel
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_GetImageDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 33
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COImageStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COImageStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COImageStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COImageStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COImageStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_GetFileDirectoriesByLevel
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_GetFileDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 9
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFileStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COFileStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COFileStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, 0 IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COFileStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COFileStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_GetContentDirectoriesByLevel]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_GetContentDirectoriesByLevel]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_GetContentDirectoriesByLevel
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_GetContentDirectoriesByLevel]
(
	@ParentId		uniqueIdentifier,
	@SiteId			uniqueIdentifier,  
	@ParentLevel	int,  
	@ChildrenLevel	int,
	@UserId			uniqueIdentifier
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 7
	
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	SELECT @Lft = LftValue , @Rgt = RgtValue from COContentStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
	
	IF @ParentLevel = -1
		Select @ParentLevel = 1
	ELSE
		Select @ParentLevel = count(*)+1 - @ParentLevel from COContentStructure D
			Where D.LftValue < @Lft and D.RgtValue > @Rgt
			AND SiteId = @SiteId
  
	IF @ChildrenLevel = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COContentStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		--Selecting Lft for Children
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			--ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE (D.ParentId =@ParentId OR D.Id=@ParentId) AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE IF @ChildrenLevel <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COContentStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue AND  P.SiteId=@SiteId )
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue Between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue 
	END  
	ELSE IF @ChildrenLevel =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COContentStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join  dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R on R.ObjectId = D.Id
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.Status != dbo.GetDeleteStatus()And D.Id =@ParentId
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue < @Lft and D.RgtValue > @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ParentLevel <= (Select count(*)+1 from COContentStructure P Where D.LftValue > P.LftValue and D.RgtValue < P.RgtValue  AND P.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		UNION ALL 
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,@ObjectType ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem, D.IsMarketierDir,
			CU.UserFullName CreatedByFullName,
			MU.UserFullName ModifiedByFullName,
			ISNULL(dbo.DirectoryProvider_GetUserRolesOnObject(@UserId,1,D.Id,@SiteId),'''') [Role],
			isnull(FC.[Count],0) TotalFiles  
		FROM COContentStructure D
			left outer join dbo.GetTotalFileCount(@SiteId,null,@ObjectType) FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy	
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy 
		WHERE D.LftValue between @Lft and @Rgt AND D.Status != dbo.GetDeleteStatus()
			And @ChildrenLevel>=(Select count(*) + 1 
		From COContentStructure M 
		WHERE M.LftValue Between @Lft and @Rgt
			And D.LftValue Between M.LftValue and M.RgtValue
			And M.LftValue Not in (D.LftValue ,@Lft) AND M.SiteId=@SiteId)
			AND D.SiteId = @SiteId
		ORDER BY LftValue
	END 
END
'
GO
PRINT N'Creating [dbo].[Version_SaveBlogPost]'
GO
IF OBJECT_ID(N'[dbo].[Version_SaveBlogPost]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Version_SaveBlogPost
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Version_SaveBlogPost](
	@VersionId	uniqueidentifier,
	@ContentId	uniqueidentifier,
	@FriendlyName	nvarchar(max),
	@ContentXml xml,
	@ModifiedBy uniqueidentifier
)
AS
BEGIN
	DECLARE @ContentVersionId uniqueidentifier
	SET @ContentVersionId = NEWID()

	INSERT INTO [dbo].[VEContentVersion]
		([Id],
		[ContentId],
		[ContentTypeId],
		[XMLString],
		[Status],
		[CreatedBy],
		[CreatedDate]
		)
	VALUES
		(@ContentVersionId,
		 @ContentId,
		 13,
		 @ContentXml,
		 1,
		 @ModifiedBy,
		 GETUTCDATE()
		)

	INSERT INTO VEPostContentVersion
		(Id,
		PostVersionId,
		ContentVersionId
		)
	VALUES
		(NEWID(),
		@VersionId,
		@ContentVersionId
		)

	UPDATE P SET 
		P.INWFFriendlyName = @FriendlyName,
		P.StatusChangeDate = GETUTCDATE(),
		P.WorkflowState = 2 --InDraft
	FROM BLPost P JOIN VEVersion V ON P.Id = V.ObjectId 
	WHERE V.Id = @VersionId
END
'
GO
PRINT N'Creating [dbo].[Post_UpdateStatus]'
GO
IF OBJECT_ID(N'[dbo].[Post_UpdateStatus]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Post_UpdateStatus
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Post_UpdateStatus]
(
	@Id		uniqueidentifier,	-- This should be the id of the comment
	@Status int 		
)
AS
DECLARE @Now	  datetime,
		@stmt	  varchar(256),
		@rowcount	int,
		@error	  int
		
BEGIN
	IF (@Id IS NOT NULL)
	BEGIN
		--check if the record exists or not..		
		IF (NOT EXISTS(SELECT 1 FROM   BLPost WHERE  Id = @Id))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( ''NOTEXISTS||Id|%s'' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()
		END
		
		--check if the status of content row is in deleted status and if yes
		--raiserror that this action is not supported..
		IF (EXISTS(SELECT 1 FROM BLPost WHERE Id=@Id AND Status = dbo.GetDeleteStatus()))
		BEGIN
			set @Stmt = convert(varchar(36),@Id)
			RAISERROR ( ''NOTSUPPORTED||Id|%s'' , 16, 1,@Stmt )
			RETURN dbo.GetBusinessRuleErrorCode()				
		END

		
		SET @stmt = ''Post UPDATE''
		
		UPDATE	BLPost  WITH (rowlock)	SET 
			Status=@Status,
			IsSticky = 0,
			ModifiedDate=@Now			
        WHERE
			Id = @Id

		SELECT @error = @@error, @rowcount = @@rowcount

		IF @error <> 0
		BEGIN
			RAISERROR(''DBERROR||%s'',16,1,@stmt)
			RETURN dbo.GetDataBaseErrorCode()
		END

		IF @rowcount = 0
		BEGIN
			RAISERROR(''CONCURRENCYERROR'',16,1)
			RETURN dbo.GetDataConcurrencyErrorCode()			-- concurrency error
		END	
	END -- (@Id IS NOT NULL)			
END

'
GO
PRINT N'Creating [dbo].[SiteGroup_Get]'
GO
IF OBJECT_ID(N'[dbo].[SiteGroup_Get]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SiteGroup_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/




-- Copyright © 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the Site Grouping details.
-- Author:Bridgeline Software, Inc.
-- Created Date:26.June.2013
-- Created By:Vidhya
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].[SiteGroup_Get] 
--********************************************************************************

CREATE PROCEDURE [dbo].[SiteGroup_Get]

--********************************************************************************
-- Input Parameters
--********************************************************************************
AS
BEGIN
--********************************************************************************
-- Variable Declaration
--********************************************************************************
SELECT [Id]
      ,[Title]
      ,[Description]
      ,[Status]
      ,[CreatedBy]
      ,[CreatedDate]
      ,[ModifiedBy]
      ,[ModifiedDate]
      ,FN.UserFullName CreatedByFullName  
	  ,MN.UserFullName ModifiedByFullName
  FROM [SIGroup] S
	LEFT JOIN VW_UserFullName FN on FN.UserId = S.CreatedBy  
	LEFT JOIN VW_UserFullName MN on MN.UserId = S.ModifiedBy
--********************************************************************************
END



'
GO
PRINT N'Creating [dbo].[Search_GetContentDefinitions]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetContentDefinitions]', 'P') IS NULL
EXEC sp_executesql N'


-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the ContentDefinitions data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetContentDefinitions] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetContentDefinitions]
--********************************************************************************
-- Input Parameter
    @ApplicationId UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
	SELECT  CO.Id,
			CO.ApplicationId AS SiteId,
			CO.Title,
			CO.Description, 
			CO.Text, 
			CO.URL,
			CO.URLType, 
			CO.ObjectTypeId, 
			CO.CreatedDate,
			CO.CreatedBy,
			CO.ModifiedBy,
			CO.ModifiedDate, 
			CO.Status,
			CO.RestoreDate, 
			CO.BackupDate, 
			CO.StatusChangedDate,
			CO.PublishDate, 
			CO.ArchiveDate, 
			CO.XMLString,
			CO.BinaryObject, 
			CO.Keywords,			
			CO.ParentId, 			
			CO.OrderNo,
			CO.SourceContentId,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			CS.Title AS FolderTitle,
			CS.Attributes AS ContentAttributes,
			CS.PhysicalPath AS ContentPhysicalPath,
			CS.VirtualPath AS ContentVirtualPath
	FROM COContent CO 
	INNER JOIN COContentStructure CS on CS.Id = CO.ParentId
	LEFT JOIN VW_UserFullName FN on FN.UserId = CO.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = CO.ModifiedBy
	WHERE  CO.ApplicationId = @ApplicationId			
			AND CO.ObjectTypeId = 13
			AND CO.ParentId IS NOT NULL  
	ORDER BY CO.OrderNo

END
--********************************************************************************





'
GO
PRINT N'Creating [dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]'
GO
IF OBJECT_ID(N'[dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SqlDirectoryProvider_GetDefaultFormDirectories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SqlDirectoryProvider_GetDefaultFormDirectories]
(
 @SiteId    uniqueIdentifier,      
 @Level     nvarchar(4000),      
 @UserId    uniqueIdentifier,    
 @AccessibleNodesOnly bit = 0,  
 @onlySharedNode bit = 0,
 @ParentId    uniqueIdentifier = null 
 )  
AS    
--********************************************************************************  
-- Variable declaration  
--********************************************************************************  
DECLARE @Lft bigint,  
	@Rgt bigint, 
	@ObjectType int
BEGIN  
--********************************************************************************  
-- code  
--********************************************************************************  
	SET @ObjectType = 38
	
	IF @AccessibleNodesOnly IS NULL
		SET @AccessibleNodesOnly= 0
  
	DECLARE @objectCount Table (Id uniqueidentifier, [Count] bigint) 
	INSERT INTO @objectCount SELECT Id, [Count] FROM dbo.GetTotalFileCount(@SiteId,null,@ObjectType)
	
	IF @ParentId IS NULL
		SET @ParentId =(Select  top 1 ParentId from HSDefaultParent  
			Where SiteId = @SiteId And ObjectTypeId = @ObjectType 
			Order by ObjectTypeId Desc)  
 
	SELECT @Lft = LftValue , @Rgt = RgtValue from COFormStructure   
		Where Id = @ParentId AND SiteId = @SiteId
  
	DECLARE @objectRole Table (ObjectId uniqueidentifier,Role nvarchar(max),Propogate int)
	INSERT INTO @objectRole
	SELECT ObjectId, [Role], Propogate from dbo.GetRolesForRangeUser(@ParentId,@Lft,@Rgt,@ObjectType,@UserId,@SiteId,0) R 
  
	IF @Level = 1   
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,
			D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,  
			ISNULL(R.[Role],'''')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites
		FROM COFormStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE (D.ParentId = @ParentId OR D.Id = @ParentId) And D.Status != dbo.GetDeleteStatus() 
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE IF @Level <=-1  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,S.Id,S.Title,S.Description,
			38 ObjectTypeId,S.PhysicalPath,S.VirtualPath,S.CreatedBy,  
			S.CreatedDate,S.ModifiedBy,S.ModifiedDate,S.Status,
			S.Attributes,S.ThumbnailImagePath,  
			S.FolderIconPath,S.[Exists],S.[Size],S.IsSystem,S.IsMarketierDir,
			ISNULL(R.[Role],'''') [Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName
			,ISNULL(FC.[Count],0)TotalFiles,
			ISNULL(S.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites   
		FROM COFormStructure S 
			left outer join  @objectRole R on R.ObjectId = S.Id  
			left outer join @objectCount FC ON FC.Id=S.Id
			left outer join VW_UserFullName CU on CU.UserId=S.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=S.ModifiedBy  
		WHERE S.LftValue Between @Lft and @Rgt And S.Status != dbo.GetDeleteStatus()
			AND S.SiteId = @SiteId AND (@onlySharedNode = 0 OR S.AllowAccessInChildrenSites = 1)
			AND (@AccessibleNodesOnly = 0 OR (S.ParentId = @SiteId OR S.Id IN 
				(Select ObjectId from @objectRole) OR S.ParentId IN 
				(Select ObjectId from @objectRole Where Propogate = 1)))
		ORDER BY LftValue  
	END  
	ELSE IF @Level =0		
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir,
			ISNULL(R.[Role],'''')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites  
			--,  dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFormStructure D
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.Id=@ParentId And D.Status != dbo.GetDeleteStatus()  
			AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END  
	ELSE  
	BEGIN  
		SELECT LftValue, RgtValue,ParentId,D.Id,D.Title,D.Description,38 ObjectTypeId,D.PhysicalPath,D.VirtualPath,D.CreatedBy,  
			D.CreatedDate,D.ModifiedBy,D.ModifiedDate,D.Status,D.Attributes,D.ThumbnailImagePath,  
			D.FolderIconPath,D.[Exists],D.[Size],D.IsSystem,D.IsMarketierDir, 
			ISNULL(R.[Role],'''')[Role],  
			CU.UserFullName CreatedByFullName,  
			MU.UserFullName ModifiedByFullName,
			ISNULL(FC.[Count],0) TotalFiles ,
			ISNULL(D.AllowAccessInChildrenSites,0)  AllowAccessInChildrenSites 
			--, dbo.GetTotalFiles(D.Id,@ObjectType) TotalFiles  
		FROM COFormStructure  D 
			left outer join  @objectRole R on R.ObjectId = D.Id  
			left outer join @objectCount FC ON FC.Id=D.Id
			left outer join VW_UserFullName CU on CU.UserId=D.CreatedBy  
			left outer join VW_UserFullName MU on MU.UserId=D.ModifiedBy  
		WHERE D.LftValue between @Lft and @Rgt And D.Status != dbo.GetDeleteStatus()
		   And @Level>=(Select count(*) + 1 From COFormStructure M   
				WHERE M.LftValue Between @Lft and @Rgt  
					And D.LftValue Between M.LftValue and M.RgtValue  
					And M.LftValue Not in (D.LftValue ,@Lft))  
					AND D.SiteId = @SiteId
		ORDER BY LftValue  
	END 
END
'
GO
PRINT N'Creating [dbo].[GetAutoBlogList]'
GO
IF OBJECT_ID(N'[dbo].[GetAutoBlogList]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : GetAutoBlogList
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[GetAutoBlogList]
(
@SiteId uniqueidentifier,
@ListId uniqueidentifier,
@BlogId uniqueidentifier,
@PublishDateType int,
@FromDate datetime,
@ToDate datetime,
@DaysOlder int,
@OrderBy varchar(20),
@OrderType int

)
As
begin
declare 
@str nvarchar(max),
@i_Ordertype varchar(15),
@iCount int,
@i int,
@Taxonomyxml xml,
@Lft bigint,
@Rgt bigint

if (@OrderBy is null or @OrderBy = '''')
	set @OrderBy = ''P.Title''
if (lower(@OrderBy) = ''createddate'')
	set @OrderBy = ''PublishDate''

if (@OrderType=2)
      set @i_Ordertype = ''desc''
else
	set @i_Ordertype = ''asc''

set @str = ''declare @Pagedef varchar(max),
@Taxonomyxml xml,
@Lft bigint,
@Rgt bigint,
@PageStatus int,
@WokkFlowState int
declare
@Taxonomy_table table(
TaxonomyId uniqueidentifier,
ObjectId uniqueidentifier
)

set @Taxonomyxml  = (select ObjectId from GetObjectByRelation(''''L-T'''','''''' + convert(varchar(36),@ListId) + '''''') for XMl Auto, root)
--Select @Taxonomyxml
set @PageStatus = dbo.GetStatus(8,''''Publish'''')
set @WokkFlowState = dbo.GetWorkFlowStatus(''''None'''')
set @Pagedef=''''''''
	if(convert(nvarchar(max),@Taxonomyxml) != '''''''')
		Begin
		 insert into @Taxonomy_table Select  tab.col.value(''''@ObjectId'''',''''uniqueidentifier'''') TaxonomyId,T.ObjectId  
         from @Taxonomyxml.nodes(''''/root/GetObjectByRelation'''') tab(col)   
                inner join COTaxonomy H on tab.col.value(''''@ObjectId'''',''''uniqueidentifier'''')  = H.Id  
                INNER JOIN COTaxonomyObject T on H.Id = T.TaxonomyId
                Where T.ObjectTypeId=40
                
		 --select * from @Taxonomy_table
		 ''
if(@PublishDateType=1)
Begin
set @str = @str + ''
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId =B.Id inner join  @Taxonomy_table T on P.Id = T.ObjectId where B.Id = '''''' + convert(nvarchar(36),@BlogId) + '''''' and P.PublishCount >= 1 and P.Status = 8 and  convert(varchar(10),PublishDate,21) between ''''''+ convert(varchar(10),@FromDate,21) + '''''' and '''''' + convert(varchar(10),@ToDate,21) + ''''''  order by '' + @OrderBy + '' '' + @i_Ordertype + '' End
				Else
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id where P.PublishCount >= 1 and P.Status = 8 and  B.Id = '''''' + convert(nvarchar(36),@BlogId) + '''''' and convert(varchar(10),PublishDate,21) between ''''''+ convert(varchar(10),@FromDate,21) + '''''' and '''''' + convert(varchar(10),@ToDate,21) + ''''''  order by '' + @OrderBy + '' '' + @i_Ordertype + ''
					''

End

Else if(@PublishDateType=2)
Begin
declare 
@today datetime
set @today = GETUTCDATE()
print @today

set @str =  @str +  ''
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id  inner join  @Taxonomy_table T on P.Id = T.ObjectId where B.Id = '''''' + convert(nvarchar(36),@BlogId) + '''''' and P.PublishCount >= 1 and P.Status = 8 and  convert(varchar(10),PublishDate,21) between ''''''+ convert(varchar(10),(@today - @DaysOlder), 21) + '''''' and '''''' + convert(varchar(10),@today,21) + ''''''  order by '' + @OrderBy + '' '' + @i_Ordertype + '' End
				Else
					select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id where P.PublishCount >= 1 and P.Status = 8 and B.Id = '''''' + convert(nvarchar(36),@BlogId) + '''''' and convert(varchar(10),PublishDate,21) between ''''''+ convert(varchar(10),(@today - @DaysOlder), 21) + '''''' and '''''' + convert(varchar(10),@today,21) + ''''''  order by '' + @OrderBy + '' '' + @i_Ordertype + ''			
				''
End

Else
Begin
set @str  = @str +  ''
			select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id inner join  @Taxonomy_table T on P.Id = T.ObjectId where B.Id = '''''' + convert(nvarchar(36),@BlogId) + '''''' and P.PublishCount >= 1  and P.Status = 8 order by '' + @OrderBy + '' '' + @i_Ordertype + '' End
		Else
			select P.Id from BLPost P INNER JOIN BLBlog B on P.BlogId=B.Id where P.PublishCount >= 1 and B.Id = ''''''+ convert(nvarchar(36),@BlogId) + '''''' and P.Status = 8   order by '' + @OrderBy + '' '' + @i_Ordertype + ''  

			''
End
--PRINT @str

EXECUTE sp_executesql @str 
return end


'
GO
PRINT N'Creating [dbo].[UploadContactDataLog_Get]'
GO
IF OBJECT_ID(N'[dbo].[UploadContactDataLog_Get]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : UploadContactDataLog_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[UploadContactDataLog_Get]
	@Id uniqueidentifier
AS
BEGIN
	select Id, UploadHistoryId, ErrorType, ErrorMessage from UploadContactDataLog where @Id is null or Id=@Id
END

'
GO
PRINT N'Creating [dbo].[User_DeleteSocialUser]'
GO
IF OBJECT_ID(N'[dbo].[User_DeleteSocialUser]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : User_DeleteSocialUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[User_DeleteSocialUser]
	@UserId uniqueidentifier
AS
BEGIN
	DELETE FROM USSocialUserProfile WHERE UserId = @UserId
END

'
GO
PRINT N'Creating [dbo].[SiteGroup_Save]'
GO
IF OBJECT_ID(N'[dbo].[SiteGroup_Save]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SiteGroup_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/



-- Copyright © 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to Add a new Site Group details and
--          Update an existing Site Group details.
-- Author:Bridgeline Software, Inc.
-- Created Date:26.June.2013
-- Created By:Vidhya
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Add
--   exec [dbo].[SiteGroup_Save] ''00000000-0000-0000-0000-000000000000'', ''Sample 1'',
--                                  ''Sample Description'' ,
--                                  ''798613EE-7B85-4628-A6CC-E17EE80C09C5'' ,
--                                  ''798613EE-7B85-4628-A6CC-E17EE80C09C5'' 
--                                  

-- To Update
--   exec [dbo].[SiteGroup_Save] ''3F425AD6-D44D-4C37-BF65-8BCCC7367A6B'', ''Sample 2'',
--                                  ''Sample Description 2'' ,
--                                  ''798613EE-7B85-4628-A6CC-E17EE80C09C5'' ,
--                                  ''798613EE-7B85-4628-A6CC-E17EE80C09C5'' 
--                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[SiteGroup_Save]
--********************************************************************************
-- Input Parameter
    @Id             uniqueidentifier OUTPUT,
	@Title			nvarchar(256),
	@Description	nvarchar(1024),
	@CreatedBy		uniqueidentifier,
	@ModifiedBy		uniqueidentifier
	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration

    DECLARE @CurrentTimeUtc	Datetime    
	SET	@CurrentTimeUtc = GetUtcDate()
	
    DECLARE @EmptyGuid uniqueidentifier
    SET @EmptyGuid = ''00000000-0000-0000-0000-000000000000''
--********************************************************************************
    -- To Add a New Site Group
	IF (@Id IS NULL OR @Id = @EmptyGuid)
	BEGIN
		
		--Assign New Guid value to the Site Group
		SELECT @Id = NEWID()		

		INSERT INTO dbo.SIGroup
		(
			Id,
			Title,
			Description,
			Status,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate			
		)
		VALUES
		(
			@Id,
			@Title,
			@Description,
			dbo.GetActiveStatus(),
			@CreatedBy,
			@CurrentTimeUtc,
			@ModifiedBy,
			@CurrentTimeUtc			
		)

	END	

    -- To Update an existing Site Group
	ELSE	
	BEGIN

--********************************************************************************
-- Variable Declaration
	    
		DECLARE @SiteGroupId uniqueidentifier
--********************************************************************************
		
		--Checks whether the Site Group details is already existing.
		SELECT @SiteGroupId = Id FROM dbo.SIGroup WHERE Id = @Id AND Status <> dbo.GetDeleteStatus()
		
		--Site Group details is not found
		IF (@SiteGroupId IS NULL)
		BEGIN
			RAISERROR (''NOTEXISTS||%s'', 16, 1, ''SecurityLevel'')
			RETURN dbo.GetBusinessRuleErrorCode()
		END
		
		--Updates the SIGroup table.
		UPDATE dbo.SIGroup 
		SET	Title = @Title,
			Description = @Description,
			ModifiedBy = @ModifiedBy,
			ModifiedDate = @CurrentTimeUtc
		WHERE	Id = @Id
	END
	
	--On any sql error
	IF (@@ERROR <> 0)
	BEGIN
		RAISERROR (''DBERROR||Save'', 16, 1)
		RETURN dbo.GetDataBaseErrorCode()
	END	

END
--********************************************************************************



'
GO
PRINT N'Creating [dbo].[Search_GetMenus]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetMenus]', 'P') IS NULL
EXEC sp_executesql N'



-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the menu data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetMenus] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetMenus]
--********************************************************************************
-- Input Parameter
    @SiteId UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
		SELECT PN.PageMapNodeId,
			   PN.ParentId,
			   PN.LftValue,
			   PN.RgtValue, 
			   PN.SiteId, 
			   PN.ExcludeFromSiteMap,	        
			   PN.Description, 
			   PN.DisplayTitle, 
			   PN.FriendlyUrl, 
			   PN.MenuStatus, 
			   PN.TargetId, 
			   PN.Target, 
			   PN.TargetUrl, 
			   PN.CreatedBy,
			   PN.CreatedDate, 
			   PN.ModifiedBy, 
			   PN.ModifiedDate, 
			   PN.PropogateWorkflow, 
			   PN.InheritWorkflow, 
			   PN.PropogateSecurityLevels,
			   PN.InheritSecurityLevels, 
			   PN.PropogateRoles, 
			   PN.InheritRoles, 
			   PN.Roles, 
			   PN.LocationIdentifier, 
			   PN.HasRolloverImages,
			   PN.RolloverOnImage, 
			   PN.RolloverOffImage, 
			   PN.IsCommerceNav, 
			   PN.AssociatedQueryId, 
			   PN.DefaultContentId, 
			   PN.AssociatedContentFolderId, 
			   PN.CustomAttributes, 
			   PN.HiddenFromNavigation, 
			   PN.SourcePageMapNodeId,		 
			   PDF.PageDefinitionId,
			   PDF.DisplayOrder,		  
			   USS.ObjectTypeId,
			   USS.SecurityLevelId		
	FROM PageMapNode PN 	
	INNER JOIN PageMapNodePageDef PDF ON PN.PageMapNodeId = PDF.PageMapNodeId	
	LEFT JOIN USSecurityLevelObject USS ON USS.ObjectId = PN.PageMapNodeId
	WHERE SiteId = @SiteId				
	ORDER BY LftValue		
END
--********************************************************************************




'
GO
PRINT N'Creating [dbo].[SocialCredentials_Save]'
GO
IF OBJECT_ID(N'[dbo].[SocialCredentials_Save]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SocialCredentials_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SocialCredentials_Save](
	@SocialMedia	nvarchar(1024),
	@Token			nvarchar(1024),
	@Secret			nvarchar(1024) = NULL,
	@ExternalId		nvarchar(50) = NULL,
	@UserId			uniqueidentifier,
	@CreatedBy		uniqueidentifier
	)
AS
BEGIN
	DECLARE @SocialMediaId int
	SET @SocialMediaId = (SELECT Id FROM SMSocialMedia WHERE name like @SocialMedia AND IsActive = 1)

	IF @SocialMediaId IS NULL
	BEGIN
		SET @SocialMediaId = (SELECT MAX(Id) + 1 FROM SMSocialMedia)

		INSERT INTO SMSocialMedia(Id, Name, Provider, IsActive)
		VALUES (@SocialMediaId, @SocialMedia, @SocialMedia, 1)
	END
	
	DELETE FROM SMSocialCredentials WHERE UserId = @UserId AND SocialMediaId = @SocialMediaId

	INSERT INTO SMSocialCredentials (Id, SocialMediaId, UserId, ExternalId, Token, [Secret], CreatedDate, CreatedBy)
	VALUES (NEWID(), @SocialMediaId, @UserId, @ExternalId, @Token, @Secret, GETDATE(), @CreatedBy)
END
'
GO
PRINT N'Creating [dbo].[Search_GetImages]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetImages]', 'P') IS NULL
EXEC sp_executesql N'


-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the image data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetImages] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetImages]
--********************************************************************************
-- Input Parameter
    @ApplicationId UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
	SELECT      CO.Id,
				CO.ApplicationId AS SiteId,
				CO.Title,
				CO.Description, 
				CO.Text, 
				CO.URL,
				CO.URLType, 
				CO.ObjectTypeId, 
				CO.CreatedDate,
				CO.CreatedBy,
				CO.ModifiedBy,
				CO.ModifiedDate, 
				CO.Status,
				CO.RestoreDate, 
				CO.BackupDate, 
				CO.StatusChangedDate,
				CO.PublishDate, 
				CO.ArchiveDate, 
				CO.XMLString,
				CO.BinaryObject, 
				CO.Keywords,
				CO.SourceContentId,
				CF.FileName, 
				CF.FileSize, 
				CF.Extension, 
				CF.FolderName, 
				CF.Attributes, 
				CF.RelativePath, 
				CF.PhysicalPath,
				CF.MimeType,
				CF.AltText,
				CF.FileIcon, 
				CF.DescriptiveMetadata, 
				CF.ImageHeight, 
				CF.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CO.OrderNo,
				CO.ParentId,
				CIS.Title AS FolderTitle,
			    CIS.Attributes AS ImageAttributes,
			    CIS.PhysicalPath AS ImagePhysicalPath,
			    CIS.VirtualPath AS ImageVirtualPath											
		FROM COContent CO 
		INNER JOIN COFile CF ON CO.Id = CF.ContentId
		INNER JOIN COImageStructure CIS ON CO.ParentId = CIS.Id
		LEFT JOIN VW_UserFullName FN ON FN.UserId = CO.CreatedBy
		LEFT JOIN VW_UserFullName MN ON MN.UserId = CO.ModifiedBy
		WHERE @ApplicationId IS NULL OR CO.ApplicationId = @ApplicationId	
		                             AND CO.ObjectTypeId = 33
		                             AND CO.ParentId IS NOT NULL
		 ORDER BY CO.OrderNo  

END
--********************************************************************************




'
GO
PRINT N'Creating [dbo].[Blog_GetLivePostCategories]'
GO
IF OBJECT_ID(N'[dbo].[Blog_GetLivePostCategories]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Blog_GetLivePostCategories
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Blog_GetLivePostCategories]
(
	@Id			uniqueIdentifier		
)
AS
BEGIN
	Select T.Id AS TaxonomyId,
		T.Title,
		COUNT(OT.ObjectId) AS NoOfPosts   
	FROM COTaxonomy T   
		JOIN COTaxonomyObject OT ON T.Id = OT.TaxonomyId  
		JOIN BLPost P on OT.ObjectId = P.Id  
	WHERE 
		P.BlogId = @Id AND P.Status = 8
	GROUP BY T.Id, T.Title
END

'
GO
PRINT N'Creating [dbo].[Log_GetLogByActivityId]'
GO
IF OBJECT_ID(N'[dbo].[Log_GetLogByActivityId]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Log_GetLogByActivityId
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Log_GetLogByActivityId]
(
	@ActivityId		uniqueidentifier
)
AS 
BEGIN
	
	SELECT 
		L.Id,
		L.Priority,
		L.Severity,
		L.CreatedDate,
		L.MachineName,
		L.ActivityId,
		L.Message,
		E.Stacktrace,
		E.InnerException
	FROM LGLog L
		LEFT JOIN LGExceptionLog E ON L.Id = E.Id
	WHERE L.ActivityId = @ActivityId
	
END



'
GO
PRINT N'Creating [dbo].[SiteGroup_Delete]'
GO
IF OBJECT_ID(N'[dbo].[SiteGroup_Delete]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SiteGroup_Delete
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/



-- Copyright © 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to delete the Site Grouping details 
--          if no sites are associated with the selected SiteGroup.
-- Author:Bridgeline Software, Inc.
-- Created Date:26.June.2013
-- Created By:Vidhya
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
--   exec [dbo].[SiteGroup_Delete] ''F1E564BC-6983-4304-B64B-AD930163F8AF'',
--                                    ''798613EE-7B85-4628-A6CC-E17EE80C09C5''
--********************************************************************************



CREATE PROCEDURE [dbo].[SiteGroup_Delete]
--********************************************************************************
-- Input Parameters

	@Id			uniqueidentifier,
	@ModifiedBy	uniqueidentifier    
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration

   DECLARE @DelStatus int
   SET @DelStatus = [dbo].[GetDeleteStatus]()
--********************************************************************************
	
	
	--Checks whether the SiteGroup details are already in the deleted status and does not have any sites associated to this group in SISiteGroup Table.
	IF ((SELECT COUNT(*) FROM dbo.SIGroup WHERE @Id = Id AND Status = @DelStatus) > 0 AND @Id IN (SELECT GroupId FROM SISiteGroup))
	BEGIN
		RAISERROR (''NOTEXISTS||SecurityLevel'', 16, 1)
		RETURN	dbo.GetBusinessRuleErrorCode()
	END	

	--Updates the status of the Site Group(Logical delete)
	UPDATE	dbo.SIGroup
	SET	ModifiedBy = @ModifiedBy,
		ModifiedDate = GetUtcDate(),
		Status = @DelStatus
	WHERE Id = @Id
	
	IF (@@ROWCOUNT = 0)
	BEGIN
		--Error because of sql server
		IF (@@ERROR <> 0)
		BEGIN	
			RAISERROR (''DBERROR'', 16, 1)
			RETURN dbo.GetDataBaseErrorCode()
		END
		
		--Error because of non existence of given security level
		RAISERROR (''NOTEXISTS||SecurityLevel'', 16, 1)
		RETURN dbo.GetBusinessRuleErrorCode()
	END

	-- Deletes from SIGroup table
	DELETE FROM dbo.SIGroup
	WHERE	Id = @Id
	
	RETURN @@ROWCOUNT
END

--********************************************************************************





'
GO
PRINT N'Creating [dbo].[Site_GetAncestorSites]'
GO
IF OBJECT_ID(N'[dbo].[Site_GetAncestorSites]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_GetAncestorSites
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

  
CREATE PROCEDURE [dbo].[Site_GetAncestorSites]          
(          
@SiteId uniqueIdentifier        
)          
AS          
BEGIN          
  
  
 DECLARE @DeleteStatus int        
 SET @DeleteStatus = dbo.GetDeleteStatus()      
  
 DECLARE @AllowSiblingAccess BIT    
  
 SELECT @AllowSiblingAccess = [VALUE] FROM STSiteSetting SS JOIN STSettingType ST ON ST.Id = SS.SettingTypeId AND ST.Name=''AllowSiblingSiteAccess''   
   
  
IF (@AllowSiblingAccess = 1)  
  
BEGIN  
  
 ;WITH siteAncestorHierarchy AS (        
 SELECT [Id]        
 ,[Title]        
 ,[Description]        
 ,[URL]        
 ,[VirtualPath]        
 ,[ParentSiteId]        
 ,[Type]        
 ,[Keywords]        
 ,[CreatedBy]        
 ,[CreatedDate]        
 ,[ModifiedBy]        
 ,[ModifiedDate]        
 ,[Status]        
 ,[LftValue]        
 ,[RgtValue]        
 ,MasterSiteId        
 FROM SISite S        
 where Id = @SiteId        
   
 UNION ALL         
   
 SELECT S1.[Id]        
 ,S1.[Title]        
 ,S1.[Description]        
 ,S1.[URL]        
 ,S1.[VirtualPath]        
 ,S1.[ParentSiteId]        
 ,S1.[Type]        
 ,S1.[Keywords]        
 ,S1.[CreatedBy]        
 ,S1.[CreatedDate]        
 ,S1.[ModifiedBy]        
 ,S1.[ModifiedDate]        
 ,S1.[Status]        
 ,S1.[LftValue]        
 ,S1.[RgtValue]        
 ,S1.MasterSiteId        
 FROM SISite S1         
 INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId         
 WHERE S1.Status != @DeleteStatus        
 )        
  
  
  
 SELECT [Id]        
 ,[Title]        
 ,[Description]        
 ,[URL]        
 ,[VirtualPath]        
 ,[ParentSiteId]        
 ,[Type]        
 ,[Keywords]        
 ,[CreatedBy]        
 ,[CreatedDate]        
 ,[ModifiedBy]        
 ,[ModifiedDate]        
 ,[Status]        
 ,[LftValue]        
 ,[RgtValue]        
 ,MasterSiteId    
 ,CU.UserFullName CreatedByFullName        
 ,MU.UserFullName ModifiedByFullName        
        
 FROM siteAncestorHierarchy S1        
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S1.CreatedBy          
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S1.ModifiedBy          
 WHERE Id != @SiteId     
   
 UNION ALL  
   
 SELECT  [Id]        
 ,[TITLE]        
 ,[DESCRIPTION]        
 ,[URL]        
 ,[VIRTUALPATH]        
 ,[PARENTSITEID]        
 ,[TYPE]        
 ,[KEYWORDS]        
 ,[CREATEDBY]        
 ,[CREATEDDATE]        
 ,[MODIFIEDBY]        
 ,[MODIFIEDDATE]        
 ,[STATUS]        
 ,[LFTVALUE]        
 ,[RGTVALUE]  
 ,MASTERSITEID      
 ,CreatedByFullName        
 ,ModifiedByFullName      
 FROM [GetSiblingSites](@SITEID)  

 UNION ALL
 
 SELECT  [Id]        
 ,[TITLE]        
 ,[DESCRIPTION]        
 ,[URL]        
 ,[VIRTUALPATH]        
 ,[PARENTSITEID]        
 ,[TYPE]        
 ,[KEYWORDS]        
 ,[CREATEDBY]        
 ,[CREATEDDATE]        
 ,[MODIFIEDBY]        
 ,[MODIFIEDDATE]        
 ,[STATUS]        
 ,[LFTVALUE]        
 ,[RGTVALUE]  
 ,MASTERSITEID      
 ,CreatedByFullName        
 ,ModifiedByFullName      
 FROM [GetChildSites](@SITEID)  	         
   
END  
   
ELSE  
   
BEGIN  
   
 ;WITH siteAncestorHierarchy AS (        
 SELECT [Id]        
 ,[Title]        
 ,[Description]        
 ,[URL]        
 ,[VirtualPath]        
 ,[ParentSiteId]        
 ,[Type]        
 ,[Keywords]        
 ,[CreatedBy]        
 ,[CreatedDate]        
 ,[ModifiedBy]        
 ,[ModifiedDate]        
 ,[Status]        
 ,[LftValue]        
 ,[RgtValue]        
 ,MasterSiteId        
 FROM SISite S        
 where Id = @SiteId        
   
 UNION ALL         
   
 SELECT S1.[Id]        
 ,S1.[Title]        
 ,S1.[Description]        
 ,S1.[URL]        
 ,S1.[VirtualPath]        
 ,S1.[ParentSiteId]        
 ,S1.[Type]        
 ,S1.[Keywords]        
 ,S1.[CreatedBy]        
 ,S1.[CreatedDate]        
 ,S1.[ModifiedBy]        
 ,S1.[ModifiedDate]        
 ,S1.[Status]        
 ,S1.[LftValue]        
 ,S1.[RgtValue]        
 ,S1.MasterSiteId        
 FROM SISite S1         
 INNER JOIN siteAncestorHierarchy SA ON S1.Id = SA.ParentSiteId         
 WHERE S1.Status != @DeleteStatus        
 )        
  
  
  
 SELECT [Id]        
 ,[Title]        
 ,[Description]        
 ,[URL]        
 ,[VirtualPath]        
 ,[ParentSiteId]        
 ,[Type]        
 ,[Keywords]        
 ,[CreatedBy]        
 ,[CreatedDate]        
 ,[ModifiedBy]        
 ,[ModifiedDate]        
 ,[Status]        
 ,[LftValue]        
 ,[RgtValue]         
 ,CU.UserFullName CreatedByFullName        
 ,MU.UserFullName ModifiedByFullName        
 ,MasterSiteId         
 FROM siteAncestorHierarchy S1        
 LEFT OUTER JOIN VW_UserFullName CU on CU.UserId=S1.CreatedBy          
 LEFT OUTER JOIN VW_UserFullName MU on MU.UserId=S1.ModifiedBy          
 where Id != @SiteId   
 
 UNION ALL
 
 SELECT  [Id]        
 ,[TITLE]        
 ,[DESCRIPTION]        
 ,[URL]        
 ,[VIRTUALPATH]        
 ,[PARENTSITEID]        
 ,[TYPE]        
 ,[KEYWORDS]        
 ,[CREATEDBY]        
 ,[CREATEDDATE]        
 ,[MODIFIEDBY]        
 ,[MODIFIEDDATE]        
 ,[STATUS]        
 ,[LFTVALUE]        
 ,[RGTVALUE]  
 ,MASTERSITEID      
 ,CreatedByFullName        
 ,ModifiedByFullName      
 FROM [GetChildSites](@SITEID)   
   
END  
   
  
  
END    
'
GO
PRINT N'Creating [dbo].[Search_GetFiles]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetFiles]', 'P') IS NULL
EXEC sp_executesql N'


-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the file data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetFiles] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetFiles]
--********************************************************************************
-- Input Parameter
    @ApplicationId UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
	SELECT      CO.Id,
				CO.ApplicationId AS SiteId,
				CO.Title,
				CO.Description, 
				CO.Text, 
				CO.URL,
				CO.URLType, 
				CO.ObjectTypeId, 
				CO.CreatedDate,
				CO.CreatedBy,
				CO.ModifiedBy,
				CO.ModifiedDate, 
				CO.Status,
				CO.RestoreDate, 
				CO.BackupDate, 
				CO.StatusChangedDate,
				CO.PublishDate, 
				CO.ArchiveDate, 
				CO.XMLString,
				CO.BinaryObject, 
				CO.Keywords,
				CO.SourceContentId,
				CF.FileName, 
				CF.FileSize, 
				CF.Extension, 
				CF.FolderName, 
				CF.Attributes, 
				CF.RelativePath, 
				CF.PhysicalPath,
				CF.MimeType,
				CF.AltText,
				CF.FileIcon, 
				CF.DescriptiveMetadata, 
				CF.ImageHeight, 
				CF.ImageWidth,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				CO.OrderNo,
				CO.ParentId,
				CFS.Title AS FolderTitle,
			    CFS.Attributes AS FileAttributes,
			    CFS.PhysicalPath AS FilePhysicalPath,
			    CFS.VirtualPath AS FileVirtualPath											
		FROM COContent CO 
		INNER JOIN COFile CF ON CO.Id = CF.ContentId
		INNER JOIN COFileStructure CFS ON CO.ParentId = CFS.Id
		LEFT JOIN VW_UserFullName FN ON FN.UserId = CO.CreatedBy
		LEFT JOIN VW_UserFullName MN ON MN.UserId = CO.ModifiedBy
		WHERE @ApplicationId IS NULL OR CO.ApplicationId = @ApplicationId	
		                             AND CO.ObjectTypeId = 9
		                             AND CO.ParentId IS NOT NULL
		ORDER BY CO.OrderNo  

END
--********************************************************************************


'
GO
PRINT N'Creating [dbo].[Search_GetStyles]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetStyles]', 'P') IS NULL
EXEC sp_executesql N'



-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the styles data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetStyles] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetStyles]
--********************************************************************************
-- Input Parameter
   (
    @ApplicationId UNIQUEIDENTIFIER,
    @Id    UNIQUEIDENTIFIER  = NULL,   
    @Title   NVARCHAR(256)=NULL,  
    @FileName  NVARCHAR(4000)=NULL,  
    @Status   INT=NULL,  
    @TemplateId  UNIQUEIDENTIFIER=NULL,  
    @PageIndex  INT=1,  
    @PageSize  INT=NULL   
 )  
AS	
--********************************************************************************
BEGIN
--********************************************************************************
-- Variable declaration
   DECLARE @EmptyGUID UNIQUEIDENTIFIER  
--********************************************************************************
   SET @EmptyGUID = dbo.GetEmptyGUID()  
  
 IF @TemplateId IS NULL  
 BEGIN  
   IF @PageSize IS NULL  
    BEGIN   
	 SELECT  S.Id, 
	         S.Title, 
	         S.Description, 
	         S.Status, 
	         S.CreatedBy, 
	         S.CreatedDate, 
	         S.ModifiedBy, 
	         S.ModifiedDate,  
	         S.FileName AS StyleFileName,
	         S.ApplicationId AS SiteId, 
	         S.Media, 
	         S.GlobalProperty,
	         FN.UserFullName CreatedByFullName,	
	         MN.UserFullName ModifiedByFullName,
	         S.OrderNo
	 FROM SIStyle S  
	 LEFT JOIN VW_UserFullName FN ON FN.UserId = S.CreatedBy
	 LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy
	 WHERE S.Id = ISNULL(@Id, S.Id) AND  
	 ISNULL(UPPER(S.Title),'''') = ISNULL(UPPER(@Title), ISNULL(UPPER(S.Title),'''')) AND  
	 ISNULL(UPPER(S. FileName),'''') = ISNULL(UPPER(@FileName), ISNULL(UPPER(S.FileName),'''')) AND    
	(@ApplicationId IS NULL OR S.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) AND
	 S.Status = isnull(@Status, S.Status)AND   
	(S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())	  
    END     
   ELSE     
    BEGIN      
	WITH ResultEntries AS (   
	SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
		AS Row,
		  S.Id, 
		  S.Title, 
		  S.Description, 
		  S.Status, 
		  S.CreatedBy, 
		  S.CreatedDate, 
		  S.ModifiedBy,   
		  S.ModifiedDate, 
		  S.FileName as StyleFileName, 
		  S.ApplicationId,
		  S.Media, 
		  S.GlobalProperty,
		  S.OrderNo 
	FROM SIStyle S  
	WHERE S.Id = ISNULL(@Id, S.Id) AND  
	ISNULL(UPPER(S.Title),'''') = ISNULL(UPPER(@Title), ISNULL(UPPER(S.Title),'''')) AND  
	ISNULL(UPPER(S. FileName),'''') = ISNULL(UPPER(@FileName), ISNULL(UPPER(S.FileName),'''')) AND 	     
	(@ApplicationId IS NULL OR S.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) AND
	S.Status = ISNULL(@Status, S.Status)AND   
   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))  
  
    SELECT A.Id, 
           A.Title, 
           A.Description, 
           A.Status, 
           A.CreatedBy, 
           A.CreatedDate, 
           A.ModifiedBy, 
           A.ModifiedDate,  
           A.StyleFileName,
           A.ApplicationId, 
           A.Media, 
           A.GlobalProperty,  
           FN.UserFullName CreatedByFullName,
	       MN.UserFullName ModifiedByFullName,
           A.OrderNo
    FROM ResultEntries   A
    LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
    WHERE Row BETWEEN   
     (@PageIndex - 1) * @PageSize + 1 AND @PageIndex*@PageSize   
    END      
   END  
 ELSE  
   BEGIN  
 IF @PageSize IS NULL  
  BEGIN  
   SELECT S.Id, 
          S.Title, 
          S.Description, 
          S.Status, 
          S.CreatedBy, 
          S.CreatedDate, 
          S.ModifiedBy, 
          S.ModifiedDate,  
          S.FileName AS StyleFileName,
          S.ApplicationId, 
          S.Media, 
          S.GlobalProperty,
          FN.UserFullName CreatedByFullName,
	      MN.UserFullName ModifiedByFullName,
	      S.OrderNo
   FROM SIStyle S 
   INNER JOIN SITemplateStyle T ON (T.StyleId = S.Id AND T.TemplateId = @TemplateId)  
   LEFT JOIN VW_UserFullName FN ON FN.UserId =S.CreatedBy
   LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy
   WHERE S.Id = ISNULL(@Id, S.Id) AND  
   ISNULL(UPPER(S.Title),'''') = ISNULL(UPPER(@Title), ISNULL(UPPER(S.Title),'''')) AND  
   ISNULL(UPPER(S. FileName),'''') = ISNULL(UPPER(@FileName), ISNULL(UPPER(S.FileName),'''')) AND   
   (@ApplicationId IS NULL OR S.ApplicationId IN (SELECT SiteId FROM dbo.GetVariantSites(@ApplicationId))) AND
    S.Status = ISNULL(@Status, S.Status)AND   
   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus())  
  END  
  ELSE  
  BEGIN  
   WITH ResultEntries AS (   
    SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)  
    AS Row, 
       S.Id, 
       S.Title, 
       S.Description, 
       S.Status, 
       S.CreatedBy,
       S.CreatedDate, 
       S.ModifiedBy,   
       S.ModifiedDate, 
       S.FileName as StyleFileName, 
       S.ApplicationId, 
       S.Media,
       S.GlobalProperty,  
       S.OrderNo
    FROM SIStyle S 
    INNER JOIN SITemplateStyle T ON (T.StyleId = S.Id AND T.TemplateId = @TemplateId)  
    WHERE S.Id = ISNULL(@Id, S.Id) AND  
    ISNULL(UPPER(S.Title),'''') = ISNULL(UPPER(@Title), ISNULL(UPPER(S.Title),'''')) AND  
    ISNULL(UPPER(S. FileName),'''') = ISNULL(UPPER(@FileName), ISNULL(UPPER(S.FileName),'''')) AND  
    (@ApplicationId IS NULL OR S.ApplicationId IN (select SiteId from dbo.GetVariantSites(@ApplicationId))) AND 
    S.Status = isnull(@Status, S.Status)AND   
   (S.Status != dbo.GetDeleteStatus()OR @Status=dbo.GetDeleteStatus()))  
  
    SELECT A.Id, 
           A.Title, 
           A.Description, 
           A.Status, 
           A.CreatedBy, 
           A.CreatedDate, 
           A.ModifiedBy,
           A.ModifiedDate,  
           A.StyleFileName,
           A.ApplicationId, 
           A.Media,
           A.GlobalProperty,  
           FN.UserFullName CreatedByFullName,
	       MN.UserFullName ModifiedByFullName,
           A.OrderNo
   FROM ResultEntries   A
   LEFT JOIN VW_UserFullName FN on FN.UserId = A.CreatedBy
   LEFT JOIN VW_UserFullName MN on MN.UserId = A.ModifiedBy
   WHERE Row between   
    (@PageIndex - 1) * @PageSize + 1 and @PageIndex*@PageSize   
  END  
 END   
END
--********************************************************************************


'
GO
PRINT N'Creating procedure [dbo].[Log_GetLogs]'
GO
IF OBJECT_ID(N'[dbo].[Log_GetLogs]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[Log_GetLogs]
(
	@ApplicationId	uniqueidentifier,  
	@EventId		int, 
	@CategoryIds	nvarchar(100) = NULL,
	@SiteId			uniqueidentifier = NULL,
	@Priority		int = NULL, 
	@Severity		nvarchar(32)= NULL, 
	@StartDate		datetime = NULL, 
	@EndDate		datetime = NULL,
	@MachineName	nvarchar(32) = NULL, 
	@Keyword		nvarchar(1500) = NULL,
	@PageNumber		int = NULL,
	@PageSize		int = NULL
)
AS 
BEGIN

	IF @Keyword IS NOT NULL
		SET @Keyword = ''%'' + @Keyword + ''%''

	IF @StartDate IS NOT NULL
		SET @StartDate = dbo.ConvertTimeToUtc(@StartDate, @ApplicationId)
	IF @EndDate IS NOT NULL
		SET @EndDate = dbo.ConvertTimeToUtc(@EndDate, @ApplicationId)
		
	DECLARE @tblLogIds TABLE (LogId int)
	
	INSERT INTO @tblLogIds
	SELECT DISTINCT LogId FROM LGCategoryLog	
		WHERE (@CategoryIds IS NULL OR
			CategoryId IN (SELECT Value FROM dbo.SplitComma(@CategoryIds, '','')))
	
	;With LogResults AS (
		SELECT ROW_NUMBER() OVER (ORDER BY L.CreatedDate DESC) AS RowNo,
			L.Id,
			L.Priority,
			L.Severity,
			L.CreatedDate,
			L.MachineName,
			L.ActivityId,
			L.Message,
			E.Stacktrace,
			E.InnerException
		FROM LGLog L
			INNER JOIN @tblLogIds C ON L.Id = C.LogId
			LEFT JOIN LGExceptionLog E ON L.Id = E.LogId
		WHERE
			L.EventId = @EventId AND
			(@SiteId IS NULL OR L.SiteId = @SiteId) AND
			(@Priority IS NULL OR L.Priority = @Priority) AND
			(@Severity IS NULL OR L.Severity like @Severity) AND
			(@StartDate IS NULL OR @EndDate IS NULL OR L.CreatedDate BETWEEN @StartDate AND @EndDate) AND
			(@MachineName IS NULL OR @MachineName like L.MachineName) AND
			(@Keyword IS NULL OR L.Message like @Keyword OR E.StackTrace like @Keyword)
	), TotalRecords AS (SELECT count(*) Cnt FROM LogResults)
	
	SELECT L.*, T.Cnt AS TotalRecords FROM LogResults L
		CROSS JOIN TotalRecords T
	WHERE (@PageSize IS NULL OR @PageNumber IS NULL OR
		RowNo between (@PageNumber - 1) * @PageSize + 1 and @PageNumber*@PageSize)
	
END


'
GO
PRINT N'Creating [dbo].[OrderItem_ClearCouponValue]'
GO
IF OBJECT_ID(N'[dbo].[OrderItem_ClearCouponValue]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : OrderItem_ClearCouponValue
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[OrderItem_ClearCouponValue]
	@OrderItemId uniqueidentifier
AS
DELETE FROM OROrderItemCouponCode where OrderItemId = @OrderItemId	


'
GO
PRINT N'Creating [dbo].[Site_Move]'
GO
IF OBJECT_ID(N'[dbo].[Site_Move]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_Move
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Site_Move ''BB4F33FC-7C20-490E-B00D-D7B40AEEA9B6'',''5371A5DD-4C24-47B6-B1E6-379EF334FCAF'',''BB4F33FC-7C20-490E-B00D-D7B40AEEA9B6'',''4B49DD31-F9AA-4D34-97B7-3984FABC0F95''
CREATE PROCEDURE [Site_Move]   
(  
	@MasterSiteId uniqueidentifier,   
	@SiteNodeId uniqueidentifier,   
	@SourceNodeId uniqueidentifier,  
	@DestNodeId uniqueidentifier  
)   
AS
BEGIN


	Declare @Source_Lft bigint,  
		@Source_Rgt bigint,  
		@Destination_rgt bigint  
	IF (@SiteNodeId IS NOT NULL)  
		IF(SELECT count(*) FROM SISite WHERE Id=@SiteNodeId)=0  
		BEGIN  
		
			RAISERROR(''NOTEXISTS||SourceNodeId'', 16, 1)  
			RETURN dbo.GetBusinessRuleErrorCode()  
		END  
  
	IF (@DestNodeId IS NOT NULL)  
		IF(SELECT count(*) FROM SISite WHERE Id=@DestNodeId)=0  
		BEGIN  
			RAISERROR(''NOTEXISTS||DestinationNodeId'', 16, 1)  
			RETURN dbo.GetBusinessRuleErrorCode()  
		END  
  
	 select @Source_Lft = LftValue,@Source_Rgt=RgtValue from SISite  
		where Id=@SiteNodeId  
  
	SET @Destination_rgt = (Select RgtValue from SISite Where Id=@DestNodeId)  
  
	IF @Destination_rgt between @Source_Lft and @Source_Rgt  
	BEGIN  
		RAISERROR(''NOTSUPPORTED'',16,1)  
		RETURN dbo.GetBusinessRuleErrorCode()  
	END  
	 Update SISite  
	  Set LftValue  
		 = LftValue   
	   + case   
	   when @Destination_rgt <@Source_Lft  
	   Then CASE  
	  
		When LftValue Between @Source_Lft and @Source_Rgt  
		Then @Destination_rgt-@Source_Lft  
		When LftValue Between @Destination_rgt  
		 And @Source_Lft - 1  
		Then @Source_Rgt - @Source_Lft + 1  
		Else 0 End  
	   When @Destination_rgt > @Source_Rgt  
	   Then CASE  
		When LftValue Between @Source_Lft  
		 And @Source_Rgt  
	   Then @Destination_rgt - @Source_Rgt - 1  
	     
	   When LftValue Between @Source_Rgt + 1  
		 And @Destination_rgt - 1  
	   Then @Source_Lft - @Source_Rgt - 1  
	   Else 0 End  
	   Else 0 End,  
	 RgtValue  
	   =RgtValue  
	  + Case  
		When @Destination_rgt < @Source_Lft  
		Then CASE  
	   
	   When RgtValue Between @Source_Lft  
		AND @Source_Rgt  
	   Then @Destination_rgt - @Source_Lft  
	   When RgtValue Between @Destination_rgt And @Source_Lft -1  
	   Then @Source_Rgt - @Source_Lft + 1  
	   Else 0 End  
	   When @Destination_rgt>@Source_Rgt  
	   THEN CASE  
		When RgtValue Between @Source_Lft  
		 And @Source_Rgt  
		Then @Destination_rgt - @Source_Rgt - 1  
		When RgtValue Between @Source_Rgt + 1  
		 And @Destination_rgt - 1  
		Then @Source_Lft - @Source_Rgt - 1  
		Else 0 End  
	   Else 0 End,  
	 ParentSiteId  
	   =Case When Id=@SiteNodeId   
	  Then @DestNodeId  
	  Else ParentSiteId  
	  End  
	  Where MasterSiteId=@MasterSiteId	
 
  
	IF @@Error <> 0  
	BEGIN  
		RAISERROR(''DBERROR||MoveNode'',16,1)  
		RETURN dbo.GetDataBaseErrorCode()  
	END  
    
END	
  



'
GO
PRINT N'Creating [dbo].[OrderItem_GetCouponCodeDiscounts]'
GO
IF OBJECT_ID(N'[dbo].[OrderItem_GetCouponCodeDiscounts]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : OrderItem_GetCouponCodeDiscounts
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE OrderItem_GetCouponCodeDiscounts
    (
      @OrderItemId UNIQUEIDENTIFIER 
    )
AS 
    SELECT  Id ,
            CouponCodeId ,
            OrderItemId ,
            Amount
    FROM    dbo.OROrderItemCouponCode
    WHERE   OrderItemId = @OrderItemId
            
    
'
GO
PRINT N'Creating [dbo].[Site_MoveTest]'
GO
IF OBJECT_ID(N'[dbo].[Site_MoveTest]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_MoveTest
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--exec Site_Move ''BB4F33FC-7C20-490E-B00D-D7B40AEEA9B6'',''5371A5DD-4C24-47B6-B1E6-379EF334FCAF'',''BB4F33FC-7C20-490E-B00D-D7B40AEEA9B6'',''4B49DD31-F9AA-4D34-97B7-3984FABC0F95''
CREATE PROCEDURE [Site_MoveTest]   
(  
	@MasterSiteId uniqueidentifier,   
	@SiteNodeId uniqueidentifier,   
	@SourceNodeId uniqueidentifier,  
	@DestNodeId uniqueidentifier  
)   
AS
BEGIN


	Declare @Source_Lft bigint,  
		@Source_Rgt bigint,  
		@Destination_rgt bigint  
	IF (@SiteNodeId IS NOT NULL)  
		IF(SELECT count(*) FROM TestHierarchy WHERE Id=@SiteNodeId)=0  
		BEGIN  
		
			RAISERROR(''NOTEXISTS||SourceNodeId'', 16, 1)  
			RETURN dbo.GetBusinessRuleErrorCode()  
		END  
  
	IF (@DestNodeId IS NOT NULL)  
		IF(SELECT count(*) FROM TestHierarchy WHERE Id=@DestNodeId)=0  
		BEGIN  
			RAISERROR(''NOTEXISTS||DestinationNodeId'', 16, 1)  
			RETURN dbo.GetBusinessRuleErrorCode()  
		END  
  
	 select @Source_Lft = LftValue,@Source_Rgt=RgtValue from TestHierarchy  
		where Id=@SiteNodeId  
  
	SET @Destination_rgt = (Select RgtValue from TestHierarchy Where Id=@DestNodeId)  
  
	IF @Destination_rgt between @Source_Lft and @Source_Rgt  
	BEGIN  
		RAISERROR(''NOTSUPPORTED'',16,1)  
		RETURN dbo.GetBusinessRuleErrorCode()  
	END  
	 Update TestHierarchy  
	  Set LftValue  
		 = LftValue   
	   + case   
	   when @Destination_rgt <@Source_Lft  
	   Then CASE  
	  
		When LftValue Between @Source_Lft and @Source_Rgt  
		Then @Destination_rgt-@Source_Lft  
		When LftValue Between @Destination_rgt  
		 And @Source_Lft - 1  
		Then @Source_Rgt - @Source_Lft + 1  
		Else 0 End  
	   When @Destination_rgt > @Source_Rgt  
	   Then CASE  
		When LftValue Between @Source_Lft  
		 And @Source_Rgt  
	   Then @Destination_rgt - @Source_Rgt - 1  
	     
	   When LftValue Between @Source_Rgt + 1  
		 And @Destination_rgt - 1  
	   Then @Source_Lft - @Source_Rgt - 1  
	   Else 0 End  
	   Else 0 End,  
	 RgtValue  
	   =RgtValue  
	  + Case  
		When @Destination_rgt < @Source_Lft  
		Then CASE  
	   
	   When RgtValue Between @Source_Lft  
		AND @Source_Rgt  
	   Then @Destination_rgt - @Source_Lft  
	   When RgtValue Between @Destination_rgt And @Source_Lft -1  
	   Then @Source_Rgt - @Source_Lft + 1  
	   Else 0 End  
	   When @Destination_rgt>@Source_Rgt  
	   THEN CASE  
		When RgtValue Between @Source_Lft  
		 And @Source_Rgt  
		Then @Destination_rgt - @Source_Rgt - 1  
		When RgtValue Between @Source_Rgt + 1  
		 And @Destination_rgt - 1  
		Then @Source_Lft - @Source_Rgt - 1  
		Else 0 End  
	   Else 0 End,  
	 ParentId  
	   =Case When Id=@SiteNodeId   
	  Then @DestNodeId  
	  Else ParentId  
	  End  
	  Where MasterSiteId=@MasterSiteId	
 
  
	IF @@Error <> 0  
	BEGIN  
		RAISERROR(''DBERROR||MoveNode'',16,1)  
		RETURN dbo.GetDataBaseErrorCode()  
	END  
    
END	
  



'
GO
PRINT N'Creating [dbo].[Log_AddCategory]'
GO
IF OBJECT_ID(N'[dbo].[Log_AddCategory]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Log_AddCategory
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Log_AddCategory]
	@CategoryName nvarchar(64),
	@LogID int
AS
BEGIN
	SET NOCOUNT ON;
    DECLARE @CategoryId INT
	SELECT @CategoryId = Id FROM LGCategory WHERE Title like @CategoryName
	IF @CategoryId IS NULL
	BEGIN
		INSERT INTO LGCategory (Title) VALUES(@CategoryName)
		SELECT @CategoryId = @@IDENTITY
	END
	
	DECLARE @CatLogID INT
	SELECT @CatLogID FROM LGCategoryLog WHERE CategoryId = @CategoryID and LogId = @LogID
	IF @CatLogID IS NULL
	BEGIN
		INSERT INTO LGCategoryLog (CategoryID, LogID) VALUES(@CategoryID, @LogID)
		RETURN @@IDENTITY
	END
	ELSE RETURN @CatLogID
	
END


'
GO
PRINT N'Creating [dbo].[Payment_GetPaymentAndRefundPaymentInformation]'
GO
IF OBJECT_ID(N'[dbo].[Payment_GetPaymentAndRefundPaymentInformation]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Payment_GetPaymentAndRefundPaymentInformation
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE Payment_GetPaymentAndRefundPaymentInformation
    (
      @RefundPaymentID AS UNIQUEIDENTIFIER
	
    )
AS 
    SELECT  P.Id AS PaymentId ,
            P2.Id AS RefundPaymentId ,
            P.Amount ,
            P2.Amount
    FROM    dbo.PTPayment P
            INNER JOIN dbo.PTOrderPayment OP ON OP.PaymentId = P.Id
            INNER JOIN dbo.PTRefundPayment RP ON RP.OrderPaymentId = OP.Id
            INNER JOIN dbo.PTOrderPayment ROP ON ROP.Id = RP.RefundOrderPaymentId
            INNER JOIN dbo.PTPayment P2 ON P2.Id = ROP.PaymentId
    WHERE   P2.Id = @RefundPaymentID
'
GO
PRINT N'Creating [dbo].[Search_GetTemplates]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetTemplates]', 'P') IS NULL
EXEC sp_executesql N'



-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the page templates data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetTemplates] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetTemplates]
--********************************************************************************
-- Input Parameter
    @ApplicationId	UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************
    DECLARE @EmptyGUID UNIQUEIDENTIFIER
    SET @EmptyGUID = dbo.GetEmptyGUID();
   
    WITH ResultEntries AS ( 
	SELECT ROW_NUMBER() OVER (ORDER BY CreatedDate)
				AS Row, S.Id, S.Title, S.Description, S.Status, S.CreatedBy, S.CreatedDate, S.ModifiedBy, 
				S.ModifiedDate, S.FileName,S.ImageURL,S.Type,S.ApplicationId,
			    (SELECT StyleId FROM SITemplateStyle CSS WHERE CSS.TemplateId = S.Id FOR XML AUTO ) StyleId,
				(SELECT ScriptId FROM SITemplateScript SC WHERE SC.TemplateId = S.Id FOR XML AUTO ) ScriptId,
				[dbo].[GetParentDirectoryVirtualPath](Id) AS ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml
	FROM SITemplate S
	WHERE ISNULL(S.ApplicationId,@EmptyGUID) = ISNULL(@ApplicationId, ISNULL(S.ApplicationId,@EmptyGUID)))
				

	SELECT S.Id, S.Title, Description, Status, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate,
				FileName,ImageURL,Type, ApplicationId AS SiteId, StyleId,ScriptId,
				FN.UserFullName CreatedByFullName,
				MN.UserFullName ModifiedByFullName,
				ParentVirtualPath, CodeFile,PagePart,LayoutTemplateId,ZonesXml, H.ParentId
	FROM ResultEntries S
	INNER JOIN HSStructure H ON S.Id = H.Id
	LEFT JOIN VW_UserFullName FN ON FN.UserId = S.CreatedBy
	LEFT JOIN VW_UserFullName MN ON MN.UserId = S.ModifiedBy
END
--********************************************************************************



'
GO
PRINT N'Creating [dbo].[Site_GetSiteGroups]'
GO
IF OBJECT_ID(N'[dbo].[Site_GetSiteGroups]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_GetSiteGroups
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE PROCEDURE [dbo].[Site_GetSiteGroups]  
(  
	@SiteId			uniqueIdentifier 
 )  
AS  
BEGIN  
	SELECT  G.Id 
	       ,Title 
	       ,Description 
	       ,Status 
	       ,CreatedBy 
	       ,CreatedDate 
	       ,ModifiedBy 
	       ,ModifiedDate  
	       ,FN.UserFullName CreatedByFullName   
	       ,MN.UserFullName ModifiedByFullName
	FROM SISiteGroup SG
	INNER JOIN SIGroup G ON SG.GroupId = G.Id
	LEFT JOIN VW_UserFullName FN on FN.UserId = G.CreatedBy  
	LEFT JOIN VW_UserFullName MN on MN.UserId = G.ModifiedBy
	WHERE SG.SiteId = @SiteId
END


'
GO
PRINT N'Creating [dbo].[Cache_SaveSiteFlushUrls]'
GO
IF OBJECT_ID(N'[dbo].[Cache_SaveSiteFlushUrls]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Cache_SaveSiteFlushUrls
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE Cache_SaveSiteFlushUrls
	@SiteUrls XML	
AS
BEGIN

	DELETE FROM GLInvalidateCache

	INSERT INTO GLInvalidateCache (SiteId, Url)
			SELECT 
				T.C.value(''SiteId[1]'', ''UNIQUEIDENTIFIER''),
				T.C.value(''Url[1]'', ''NVARCHAR(MAX)'')	
			 FROM @SiteUrls.nodes(''(/DSUrlMappings/Mapping)'') AS T(C)
END

'
GO
PRINT N'Creating [dbo].[Search_GetProducts]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetProducts]', 'P') IS NULL
EXEC sp_executesql N'


-- Copyright � 2000-2006 Bridgeline Software, Inc.  
-- All rights reserved. This software contains the confidential and trade   
-- secret information of Bridgeline Software, Inc. ("Bridgeline").    
-- Copying, distribution or disclosure without Bridgeline''s express written   
-- permission is prohibited.  
  
-- Purpose: Get a collection of products by a given list of IDs  
-- Author: Bridgeline Software, Inc.  
-- Created Date:03-23-2009  
-- Created By:Prakash  
  
-- Modified Date: 03-27-2009  
-- Modified By: Devin  
-- Reason:    
-- Changes:  
--  
--********************************************************************************  
-- List of all the changes with date and reason  
--  
--********************************************************************************  
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetProducts] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''  
--********************************************************************************  
  
  
CREATE PROCEDURE [dbo].[Search_GetProducts]      
(      
		@ApplicationId UNIQUEIDENTIFIER = NULL
)      
AS      
      
--********************************************************************************      
-- Variable declaration      
--********************************************************************************      
BEGIN      
--********************************************************************************      
-- code      
--********************************************************************************      
DECLARE @tempXml TABLE(Sno int IDENTITY(1,1),ProductId UNIQUEIDENTIFIER)      
      
INSERT INTO @tempXml(ProductId)      
SELECT DISTINCT ProductId AS Id FROM PRProductSKU WHERE IsActive=1 AND IsOnline=1  
      
 SELECT        
 S.[Id],      
 S.[ProductIDUser],      
 S.[Title],      
 S.[ShortDescription],      
 S.[LongDescription],      
 S.[Description],      
 S.[Keyword],      
 S.[IsActive],      
 S.[ProductTypeID],      
 vPT.[ProductTypeName] AS ProductTypeName,      
 vPT.[ProductTypePath] + N''id-'' + S.ProductIDUser + ''/'' + S.UrlFriendlyTitle AS DefaultUrl, S.UrlFriendlyTitle,       
 vPT.[IsDownloadableMedia],S.[ProductStyle],      
 (SELECT Count(*) FROM PRProductSKU WHERE ProductId= S.[Id])AS NumberOfSKUs,      
 S.CreatedDate,      
 S.CreatedBy,      
 S.ModifiedDate,      
 S.ModifiedBy,      
 S.Status,      
 S.IsBundle,      
	dbo.ConvertTimeFromUtc( S.BundleCompositionLastModified,@ApplicationId) AS BundleCompositionLastModified,      
 [PrdSeo].FriendlyUrl AS SEOFriendlyUrl,
 [S].TaxCategoryID        
  FROM @tempXml T      
  INNER JOIN PRProduct S ON S.Id=T.ProductId      
  INNER JOIN vwProductTypePath vPT ON [S].ProductTypeID = vPT.ProductTypeId      
  LEFT JOIN PRProductSEO PrdSeo ON S.Id = PrdSeo.ProductId   
--Order By T.Sno      
  
  
SELECT   
 SA.Id,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 IsNull(AE.[IsDefault], 0) as [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status,
 SA.ProductId
FROM @tempXml t   
INNER JOIN PRProductAttributeValue SA  ON SA.ProductId=t.ProductId  
LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
WHERE SA.Status = dbo.GetActiveStatus()  
  
  
SELECT [Id]    
      ,S.[ProductId]    
      ,S.[SiteId]    
      ,S.[SKU]    
      ,S.[Title]    
      ,S.[Description]    
      ,S.[Sequence]    
      ,S.[IsActive]    
      ,S.[IsOnline]    
      ,S.[ListPrice]    
      ,S.[UnitCost]    
      ,S.[PreviousSoldCount]    
      ,S.[CreatedBy]    
      ,S.[CreatedDate]    
      ,S.[ModifiedBy]    
      ,S.[ModifiedDate]    
      ,S.[Status]    
      ,S.[Measure]    
      ,S.[OrderMinimum]    
      ,S.[OrderIncrement]  
      ,S.[HandlingCharges]  
      ,S.[Length]
	  ,S.[Height]
	  ,S.[Width]
	  ,S.[Weight]
	  ,S.[SelfShippable]    
      ,S.[AvailableForBackOrder]
	  ,S.[BackOrderLimit]
	  ,S.[IsUnlimitedQuantity]
  FROM [PRProductSKU] S   
  INNER JOIN @tempXml t ON S.ProductId=t.ProductId  
  
  
SELECT   
 SA.Id,  
 SA.SKUId,  
 SA.AttributeId,  
 SA.AttributeEnumId,  
 AE.NumericValue,  
 ISNULL(AE.[IsDefault], 0) AS [IsDefault],  
 AE.Sequence,  
 SA.Value,  
 AE.Code,  
 SA.CreatedDate,  
 SA.CreatedBy,  
 SA.ModifiedDate,  
 SA.ModifiedBy,  
 SA.Status  
FROM @tempXml t   
INNER JOIN PRProductAttribute PA  ON PA.ProductId=t.ProductId  
INNER JOIN PRProductSKU PS ON PA.ProductId = PS.ProductId  
INNER JOIN PRProductSKUAttributeValue SA  ON (SA.AttributeId=PA.AttributeId AND PS.Id = SA.SKUId )  
LEFT JOIN ATAttributeEnum AE ON SA.AttributeEnumId = AE.Id  
WHERE IsSKULevel=1   
AND SA.Status = dbo.GetActiveStatus()  
--Order By SA.AttributeId  
 
SELECT  
  A.Id,  
  A.AttributeGroupId,  
  A.AttributeDataType,  
  A.Title,  
  A.Description,  
  A.Sequence,  
  A.Code,  
  A.IsFaceted,  
  A.IsSearched,  
  A.IsSystem,  
  A.IsDisplayed,  
  A.IsEnum,    
  A.IsPersonalized,
  A.CreatedDate,  
  A.CreatedBy,  
  A.ModifiedDate,  
  A.ModifiedBy,  
  A.Status,  
  PA.IsSKULevel,  
  PA.ProductId,  
  A.IsSystem   , A.IsPersonalized
FROM @tempXml t   
INNER JOIN PRProductAttribute PA  ON PA.ProductId=t.ProductId   
INNER JOIN ATAttribute A ON PA.AttributeId = A.Id   
WHERE  A.Status=dbo.GetActiveStatus() AND  A.IsSystem=0 AND A.IsFaceted=1
  

SELECT P.Id AS ProductId,	
CASE WHEN Min(MinimumEffectivePrice) IS NULL          THEN MIN(ListPrice) 
     WHEN Min(MinimumEffectivePrice) < MIN(ListPrice) THEN Min(MinimumEffectivePrice)
ELSE Min(ListPrice) END  as Price , cast( SUM(PS.PreviousSoldCount) AS INT) AS soldcount, P.CreatedDate		
			FROM			
				dbo.PRProduct P
				INNER JOIN dbo.PRProductSKU PS ON P.Id = PS.ProductId 		
				LEFT JOIN (SELECT * FROM cache_ProductMinEffectivePrice MP ) PP ON P.Id= PP.ProductId 	
				INNER JOIN @tempXml TP  ON P.Id = TP.ProductId 	
			WHERE     
				(PS.IsActive = 1) AND (PS.IsOnline = 1) AND (P.IsActive = 1)     
			GROUP BY     
				P.Id, 
				PP.MinimumEffectivePrice,
				P.CreatedDate,   
				P.ProductIDUser   
END



'
GO
PRINT N'Creating [dbo].[PageDefinition_GetByWorkflow]'
GO
IF OBJECT_ID(N'[dbo].[PageDefinition_GetByWorkflow]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : PageDefinition_GetByWorkflow
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[PageDefinition_GetByWorkflow]
	@WorkflowId UNIQUEIDENTIFIER
AS
BEGIN		
		;WITH Pages AS(
			SELECT ObjectId, ROW_NUMBER() OVER(PARTITION BY ObjectId ORDER BY ActionedDate desc)AS Ord FROM WFWorkflowExecution
			WHERE WorkflowId = @WorkflowId
		)
		SELECT 			M.PageDefinitionId, 
			M.PageMapNodeId, 
			SiteId, 
			M.DisplayOrder,
			TemplateId,
			Title,
			Description,
			PageStatus,
			WorkflowState,
			PublishCount,
			PublishDate,
			FriendlyName,
			WorkflowId,
			CreatedBy,
			CreatedDate,
			ModifiedBy,
			ModifiedDate,
			ArchivedDate,
			StatusChangedDate,
			AuthorId,
			EnableOutputCache,
			OutputCacheProfileName,
			ExcludeFromSearch,
			IsDefault,
			IsTracked,
			HasForms,
			TextContentCounter,
			CustomAttributes,
			SourcePageDefinitionId,
			IsInGroupPublish,
			ScheduledPublishDate,
			ScheduledArchiveDate,
			NextVersionToPublish 
			FROM PageDefinition D
			INNER JOIN PageMapNodePageDef M ON D.PageDefinitionId =M.PageDefinitionId
			INNER JOIN Pages O ON O.ObjectId = D.PageDefinitionId
			WHERE O.Ord = 1
	
END

'
GO
PRINT N'Creating [dbo].[Cache_GetSiteFlushUrls]'
GO
IF OBJECT_ID(N'[dbo].[Cache_GetSiteFlushUrls]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Cache_GetSiteFlushUrls
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE Cache_GetSiteFlushUrls
AS
BEGIN
	SELECT SiteId, Url FROM GLInvalidateCache
END
'
GO
PRINT N'Creating [dbo].[Post_GetAuthors]'
GO
IF OBJECT_ID(N'[dbo].[Post_GetAuthors]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Post_GetAuthors
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Post_GetAuthors]
(     
      @BlogId			uniqueidentifier	= NULL,
      @PostId			uniqueidentifier	= NULL,
      @ApplicationId	uniqueidentifier
)
AS
BEGIN
	SELECT	DISTINCT AuthorName, AuthorEmail, Location
	FROM	BLPost
	WHERE	ApplicationId = @ApplicationId AND
		(@BlogId IS NULL OR BlogId = @BlogId) AND
		(@PostId IS NULL OR Id = @PostId)
END

'
GO
PRINT N'Creating [dbo].[Site_GetSiteHierarchyPath]'
GO
IF OBJECT_ID(N'[dbo].[Site_GetSiteHierarchyPath]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_GetSiteHierarchyPath
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Site_GetSiteHierarchyPath]  
(  
	@Id	uniqueIdentifier
 )  
AS  
BEGIN  
	DECLARE @MasterSiteId uniqueIdentifier
	SELECT @MasterSiteId = MasterSiteId FROM SISite WHERE Id = @Id
	
	IF @MasterSiteId IS NOT NULL AND @MasterSiteId != dbo.GetEmptyGUID()
		SELECT dbo.GetSiteHierarchyPath(@Id, @MasterSiteId) AS SitePath
	
END

'
GO


PRINT N'Creating [dbo].[PageDefinition_GetPageIdsOfDifferentSiteContent]'
GO
IF OBJECT_ID(N'[dbo].[PageDefinition_GetPageIdsOfDifferentSiteContent]', 'P') IS NULL
EXEC sp_executesql N'
CREATE procedure [dbo].[PageDefinition_GetPageIdsOfDifferentSiteContent]
(
	@SiteId uniqueidentifier,
	@ExcludeMasterSite bit=0	
)
as
begin
	declare @MasterSiteId uniqueidentifier 
	 set @MasterSiteId = (select MasterSiteId  from SISite where Id=@SiteId)
	 
	 select distinct P.PageDefinitionId from PageDefinition P inner join PageDefinitionContainer PC on P.PageDefinitionId = PC.PageDefinitionId 
	 inner join COContent C on PC.ContentId = C.Id  where P.SiteId=@SiteId  and C.ApplicationId !=@SiteId and (@ExcludeMasterSite=0 or (@ExcludeMasterSite=1 and C.ApplicationId !=@MasterSiteId ))
	 union
	 select distinct P.PageDefinitionId  from PageDefinition P inner join PageDefinitionContainer PC on P.PageDefinitionId = PC.PageDefinitionId 
	 inner join COContent C on PC.InWFContentId  = C.Id  where P.SiteId=@SiteId  and C.ApplicationId !=@SiteId and (@ExcludeMasterSite=0 or (@ExcludeMasterSite=1 and C.ApplicationId !=@MasterSiteId ))

END
'

GO
PRINT N'Creating [dbo].[Site_GetSitesBySiteGroup]'
GO
IF OBJECT_ID(N'[dbo].[Site_GetSitesBySiteGroup]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Site_GetSitesBySiteGroup
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Site_GetSitesBySiteGroup]  
(  
	@UserId		uniqueIdentifier = null
)  
AS  
BEGIN  
	;WITH CTE AS(
		SELECT DISTINCT SiteId FROM dbo.GetVariantSitesForUser(@UserId)
	)

	SELECT  
		ROW_NUMBER() over (Partition By G.Id Order by S.Title) AS RowNo,
		S.Id AS SiteId,
		S.Title AS SiteTitle,
		S.PrimarySiteUrl AS SiteUrl,
		S.MasterSiteId AS MasterSiteId,
		G.Id AS SiteGroupId,
		G.Title AS SiteGroupTitle
	FROM SISiteGroup SG
	INNER JOIN SIGroup G ON SG.GroupId = G.Id
	INNER JOIN SISite S ON SG.SiteId = S.Id
	INNER JOIN CTE C ON C.SiteId = S.Id
	WHERE S.Status = 1
	
	UNION
	
	SELECT
		ROW_NUMBER() over (Order by S.Title) AS RowNo,
		S.Id AS SiteId,
		S.Title AS SiteTitle,
		S.PrimarySiteUrl AS SiteUrl,
		S.MasterSiteId AS MasterSiteId,
		NULL,
		NULL
	FROM SISite S 
	INNER JOIN CTE C ON C.SiteId = S.Id
	WHERE S.Id NOT IN (SELECT SiteId FROM SISiteGroup)
	AND S.Status = 1
	
	Order by SiteGroupTitle, RowNo
END
'
GO
PRINT N'Creating [dbo].[Membership_GetDefaultSite]'
GO
IF OBJECT_ID(N'[dbo].[Membership_GetDefaultSite]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Membership_GetDefaultSite
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Membership_GetDefaultSite]
(
	@UserName	nvarchar(1024),
	@SiteId		uniqueidentifier,
	@SiteUrl	nvarchar(1024) = null
)
AS
BEGIN
	DECLARE @defaultSiteId uniqueidentifier
	DECLARE @LftValue int, @RgtValue int
	SELECT @LftValue = LftValue, @RgtValue = RgtValue FROM SISite WHERE Id = @SiteId

	IF @LftValue IS NOT NULL AND @RgtValue IS NOT NULL AND @RgtValue > @LftValue + 1
	BEGIN
		DECLARE @DeletedSatus INT
		SET @DeletedSatus = dbo.GetDeleteStatus()
		
		DECLARE @UserId uniqueidentifier
		SET @UserId = (
			SELECT U.Id FROM USUser U
				WHERE U.LoweredUserName = LOWER(@UserName)
					AND U.Status <> @DeletedSatus
		)

		SET @defaultSiteId = (
			SELECT TOP 1 SiteId FROM USSiteUser WHERE UserId = @UserId AND SiteId = @SiteId
		)
	
		IF @defaultSiteId IS NULL AND @SiteUrl IS NOT NULL
		BEGIN
			--SET @SiteUrl = [dbo].[GetDomainLoweredUrl](@SiteUrl)
			SET @defaultSiteId = (
				SELECT TOP 1 S.Id FROM SISite S 
					JOIN USSiteUser SU ON S.Id = SU.SiteId
				WHERE SU.UserId = @UserId 
					AND S.Status = 1
					AND S.LftValue BETWEEN @LftValue AND @RgtValue
					--AND EXISTS(SELECT * FROM vwFormattedSiteUrls V WHERE Url like @SiteUrl)
				Order by S.LftValue
			)
		END
	END
	
	SELECT @defaultSiteId
END

'
GO
PRINT N'Creating [dbo].[SocialMedia_Get]'
GO
IF OBJECT_ID(N'[dbo].[SocialMedia_Get]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : SocialMedia_Get
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[SocialMedia_Get](
	@ApplicationId uniqueidentifier = NULL
)
AS
BEGIN
	SELECT Id, Name, Provider, IsActive FROM SMSocialMedia
END
'
GO
PRINT N'Creating [dbo].[Search_GetUsers]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetUsers]', 'P') IS NULL
EXEC sp_executesql N'



-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the users data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetUsers] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetUsers]
--********************************************************************************
-- Input Parameter
    @SiteId UNIQUEIDENTIFIER	
--********************************************************************************
AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
		SELECT US.FirstName,
		       US.MiddleName,		       
		       US.LastName,
		       US.BirthDate,
		       US.Gender,
		       US.UserName,
		       US.LoweredUserName,
		       US.CompanyName,
		       US.Description,
		       US.Title,
		       US.ExpiryDate,
		       US.EmailNotification,
		       US.CreatedBy,
		       US.CreatedDate,
		       US.DashboardData,
		       US.HomePhone,
		       US.MobilePhone,
		       US.OtherPhone,
		       US.ModifiedBy,
		       US.ModifiedDate,
		       US.ImageId,
		       US.IsAnonymous,
		       US.LastActivityDate,
		       US.MobileAlias,
		       US.ReportEndDate,
		       US.ReportRangeSelection,
		       US.ReportStartDate,
		       US.Status,
		       US.TimeZone,
		       USSU.IsSystemUser,
		       USSU.ProductId,
		       USSU.SiteId,
		       USSU.UniqueNo		      
		FROM USUser US 
		INNER JOIN USSiteUser USSU ON US.Id = USSU.UserId 
		WHERE SiteId = @SiteId
		ORDER BY USSU.UniqueNo
END
--********************************************************************************


'
GO
PRINT N'Creating [dbo].[User_GetExternalSocialId]'
GO
IF OBJECT_ID(N'[dbo].[User_GetExternalSocialId]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : User_GetExternalSocialId
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/


CREATE PROCEDURE [dbo].[User_GetExternalSocialId](
	@UserId uniqueidentifier,
	@ApplicationId uniqueidentifier=null)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT top 1 ExternalId FROM USSocialUserProfile Where UserId=@UserId AND (SiteId=@ApplicationId OR @ApplicationId is null)
END

'
GO
PRINT N'Creating [dbo].[Search_GetContentItems]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetContentItems]', 'P') IS NULL
EXEC sp_executesql N'


-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the ContentItems data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetContentItems] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetContentItems]
--********************************************************************************
-- Input Parameter
    @ApplicationId UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable Declaration   
--********************************************************************************
   
	SELECT  CO.Id,
			CO.ApplicationId AS SiteId,
			CO.Title,
			CO.Description, 
			CO.Text, 
			CO.URL,
			CO.URLType, 
			CO.ObjectTypeId, 
			CO.CreatedDate,
			CO.CreatedBy,
			CO.ModifiedBy,
			CO.ModifiedDate, 
			CO.Status,
			CO.RestoreDate, 
			CO.BackupDate, 
			CO.StatusChangedDate,
			CO.PublishDate, 
			CO.ArchiveDate, 
			CO.XMLString,
			CO.BinaryObject, 
			CO.Keywords,			
			CO.ParentId, 			
			CO.OrderNo,
			CO.SourceContentId,
			FN.UserFullName CreatedByFullName,
			MN.UserFullName ModifiedByFullName,
			CS.Title AS FolderTitle,
			CS.Attributes AS ContentAttributes,
			CS.PhysicalPath AS ContentPhysicalPath,
			CS.VirtualPath AS ContentVirtualPath
	FROM COContent CO 
	INNER JOIN COContentStructure CS on CS.Id = CO.ParentId
	LEFT JOIN VW_UserFullName FN on FN.UserId = CO.CreatedBy
	LEFT JOIN VW_UserFullName MN on MN.UserId = CO.ModifiedBy
	WHERE  CO.ApplicationId = @ApplicationId			
			AND CO.ObjectTypeId = 7
			AND CO.ParentId IS NOT NULL 
	ORDER BY CO.OrderNo

END
--********************************************************************************





'
GO
PRINT N'Creating [dbo].[UploadContactDataLog_Save]'
GO
IF OBJECT_ID(N'[dbo].[UploadContactDataLog_Save]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : UploadContactDataLog_Save
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[UploadContactDataLog_Save]
(
	@Id uniqueidentifier output,
	@UploadHistoryId uniqueidentifier,
	@ErrorType int,
	@ErrorMessage nvarchar(max)	
)
AS
BEGIN
	if(@Id is null or @Id= dbo.GetEmptyGUID())
	begin
		set @Id = newid()
		INSERT INTO [dbo].[UploadContactDataLog]
           ([Id]
           ,[UploadHistoryId]
           ,[ErrorType]
           ,[ErrorMessage])
		 VALUES
			   (@Id
			   ,@UploadHistoryId 
			   ,@ErrorType 
			   ,@ErrorMessage)

	end
	else
	begin
		Update [dbo].[UploadContactDataLog]
		set UploadHistoryId=@UploadHistoryId, ErrorType=@ErrorType,ErrorMessage=@ErrorMessage where ID =@Id 
	end
END
'
GO
PRINT N'Creating [dbo].[OrderItem_SaveCouponCodeDiscount]'
GO
IF OBJECT_ID(N'[dbo].[OrderItem_SaveCouponCodeDiscount]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : OrderItem_SaveCouponCodeDiscount
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE OrderItem_SaveCouponCodeDiscount
    (
      @OrderItemId UNIQUEIDENTIFIER ,
      @CouponCodeId UNIQUEIDENTIFIER ,
      @Amount MONEY
    )
AS 
    DELETE  FROM OROrderItemCouponCode
    WHERE   OrderItemId = @OrderItemId
            AND CouponCodeId = @CouponCodeId

    INSERT  INTO dbo.OROrderItemCouponCode
            ( Id ,
              OrderItemId ,
              CouponCodeId ,
              Amount
            )
    VALUES  ( NEWID() , -- Id - uniqueidentifier
              @OrderItemID , -- OrderItemId - uniqueidentifier
              @CouponCodeId , -- OrderCouponCodeId - uniqueidentifier
              @Amount-- Amount - money
            )
'
GO
PRINT N'Creating [dbo].[Log_ClearLogs]'
GO
IF OBJECT_ID(N'[dbo].[Log_ClearLogs]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Log_ClearLogs
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

CREATE PROCEDURE [dbo].[Log_ClearLogs]
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM LGCategoryLog
	DELETE FROM LGLog
	DELETE FROM LGExceptionLog
	DELETE FROM LGCategory
	
	DBCC CHECKIDENT(''LGLog'', RESEED, 0)
	DBCC CHECKIDENT(''LGCategoryLog'', RESEED, 0)
	DBCC CHECKIDENT(''LGExceptionLog'', RESEED, 0)
	DBCC CHECKIDENT(''LGCategory'', RESEED, 0)
END

'
GO
PRINT N'Creating [dbo].[Payment_GetExternalProfile]'
GO
IF OBJECT_ID(N'[dbo].[Payment_GetExternalProfile]', 'P') IS NULL
EXEC sp_executesql N'/*****************************************************
* Name : Payment_GetExternalProfile
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:	Prakash V
-- Create date: 2013-10-02
-- Description:	Get the Profile Id of the saved card
-- =============================================
CREATE PROCEDURE Payment_GetExternalProfile
(
	@SavedCardId uniqueidentifier	
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT ExternalProfileId FROM USUserPaymentInfo Where Id = @SavedCardId
END
'
GO
PRINT N'Creating [dbo].[PageMapNode_UpdateMemberRoles]'
GO
IF OBJECT_ID(N'[dbo].[PageMapNode_UpdateMemberRoles]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[PageMapNode_UpdateMemberRoles] 
(
	@SiteId			uniqueidentifier,	
	@PageMapNodeId	uniqueidentifier,	
	@Propogate		bit = 0,
	@Inherit		bit = 0
)	
AS 
BEGIN
	DECLARE @ParentId uniqueidentifier
	SET @ParentId = (SELECT TOP 1 ParentId FROM PageMapNode WHERE PageMapNodeId = @PageMapNodeId)

	DECLARE @tbMemberRoles TABLE(ProductId uniqueidentifier, MemberId uniqueidentifier, MemberType int, RoleId int)

	INSERT INTO @tbMemberRoles
	SELECT ProductId, MemberId, MemberType, RoleId FROM USMemberRoles WHERE ObjectId = @PageMapNodeId

	IF (@Inherit = 1)
		INSERT INTO @tbMemberRoles
		SELECT ProductId, MemberId, MemberType, RoleId FROM USMemberRoles WHERE ObjectId = @ParentId

	DELETE FROM USMemberRoles WHERE ObjectId = @PageMapNodeId
	INSERT INTO USMemberRoles (ProductId, ApplicationId, MemberId, MemberType, RoleId, ObjectId, ObjectTypeId, Propagate)
	SELECT DISTINCT ProductId, 
		@SiteId,
		MemberId,
		MemberType,
		RoleId,
		@PageMapNodeId,
		2,
		@Propogate
	FROM @tbMemberRoles
END
'
GO




PRINT N'Creating [dbo].[Search_GetPages]'
GO
IF OBJECT_ID(N'[dbo].[Search_GetPages]', 'P') IS NULL
EXEC sp_executesql N'



-- Copyright � 2013-2020 Bridgeline Software, Inc.
-- All rights reserved. This software contains the confidential and trade 
-- secret information of Bridgeline Software, Inc. ("Bridgeline").  
-- Copying, distribution or disclosure without Bridgeline''s express written 
-- permission is prohibited.

-- Purpose: This stored procedure is used to get the pages data for generating
--          XML File with data sent for admin indexing.
-- Author:Bridgeline Software, Inc.
-- Created Date : 8.August.2013
-- Created By   : Vidhya Ananthanarayanan
 
-- Modified Date:
-- Modified By:
-- Reason:
-- Changes:

--********************************************************************************
-- List of all the changes with date and reason
--********************************************************************************

--********************************************************************************
-- Warm-up Script:
-- To Get
--   exec [dbo].[Search_GetPages] ''8039ce09-e7da-47e1-bcec-df96b5e411f4''                                  
--********************************************************************************

CREATE PROCEDURE [dbo].[Search_GetPages]
--********************************************************************************
-- Input Parameter
    @ApplicationId	UNIQUEIDENTIFIER	
--********************************************************************************

AS
BEGIN
--********************************************************************************
-- Variable declaration
--********************************************************************************
   SELECT PD.ArchivedDate,
          PD.AuthorId,
          PD.ContainerXmlNotUsed,
          PD.CreatedBy,
          PD.CreatedDate,
          PD.CustomAttributes,
          PD.EnableOutputCache,
          PD.ExcludeFromSearch,
          PD.FriendlyName,
          PD.HasForms,
          PD.IsDefault,
          PD.IsInGroupPublish,
          PD.IsTracked,
          PD.ModifiedBy,
          PD.ModifiedDate,
          PD.NextVersionToPublish,
          PD.OutputCacheProfileName,
          PD.PageDefinitionId,
          PD.PageDefinitionXmlNotUsed,
          PD.PageStatus,
          PD.PublishCount,
          PD.ScheduledArchiveDate,
          PD.ScheduledPublishDate,
          PD.SiteId,
          PD.SourcePageDefinitionId,
          PD.StatusChangedDate,
          PD.TemplateId,
          PD.TextContentCounter,
          PD.UnmanagedContentXmlNotUsed,
          PD.WorkflowId,
          PD.WorkflowState,
          PD.description,
          PD.publishDate,
          PD.title,
          PDS.AudienceSegmentId,
          PDS.DeviceId,
          PDS.UnmanagedXml,
          ALA.SegmentName,
          ALA.Description AS AudienceSegmentDescription
           
           FROM PageDefinition PD INNER JOIN PageDefinitionSegment PDS ON PD.PageDefinitionId = PDS.PageDefinitionId INNER JOIN ALAudienceSegment ALA ON PDS.AudienceSegmentId = ALA.Id 
END
--********************************************************************************



'
GO