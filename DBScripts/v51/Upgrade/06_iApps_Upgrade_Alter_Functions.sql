/*
Run this script on:

        RnD-D-Dev.v50_iapps_dev    -  This database will be modified

to synchronize it with a database with the schema represented by:

        dbo

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.4.8 from Red Gate Software Ltd at 10/18/2013 1:36:50 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [dbo].[GenerateXml]'
GO
IF OBJECT_ID(N'[dbo].[GenerateXml]', 'AF') IS NOT NULL
DROP AGGREGATE [dbo].[GenerateXml]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[GetVariantSites]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVariantSites]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[GetVariantSites]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GetVariantSites]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetVariantSites]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'/*****************************************************
* Name : GetVariantSites
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Prakash Venkatachalam
-- Create date: 6/25/2013
-- Description:	Get the siblings, child and Parent site
-- =============================================
CREATE FUNCTION [dbo].[GetVariantSites](@ApplicationId uniqueidentifier)

RETURNS 
@Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	IF @ApplicationId is NULL
	INSERT INTO @Sites
	SELECT Id from SISite Where Status=1
	ELSE 
	BEGIN
		Declare @ParentId uniqueidentifier
		Declare @Lft int,@Rgt int
		Declare @cLft int,@cRgt int
		select top 1 @ParentId =ParentSiteId,@Lft =LftValue, @Rgt=RgtValue FROM SISite Where Id=@ApplicationId
		IF @ParentId <>''00000000-0000-0000-0000-000000000000''
		BEGIN
			SELECT @cLft=@Lft,@cRgt=@Rgt
			select top 1 @Lft =LftValue, @Rgt=RgtValue FROM SISite 
			Where LftValue <@Lft and RgtValue>@Rgt AND ParentSiteId=''00000000-0000-0000-0000-000000000000''
		END	
		INSERT INTO @Sites
		Select Id FROM SISite
		Where LftValue between @Lft and @Rgt And Status=1
		
	END
	
	RETURN 
END

'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[User_GetGroupChildren]'
GO
/*****************************************************
* Name : User_GetGroupChildren
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Function [dbo].[User_GetGroupChildren](@ObjectId uniqueidentifier,@ApplicationId uniqueidentifier,@ProductId uniqueidentifier)  
RETURNS @TABLE TABLE(
	ID INT IDENTITY (1,1) NOT NULL,
	GROUPID uniqueidentifier null,
	tier_cnt int NULL,
	lineage_id varchar(50))
AS
BEGIN
	DECLARE @tier as int
	SET @tier = 2
	
	IF((SELECT COUNT(MemberId) FROM USMemberGroup WHERE GroupId = @ObjectId AND ApplicationId = ISNULL(@ApplicationId,ApplicationId)) > 0)
	BEGIN
		INSERT INTO @TABLE (GROUPID,tier_cnt,lineage_id) 
		VALUES(@ObjectId,1,'1')
		
		INSERT INTO @TABLE
		SELECT MemberId ,2,'1' FROM USMemberGroup WHERE GroupId = @ObjectId 
		AND MemberType = 2 AND ApplicationId = ISNULL(@ApplicationId,ApplicationId)
		
		UPDATE @TABLE SET lineage_id = lineage_id+LTRIM(STR(ID)) WHERE 
		len(lineage_id)	 < tier_cnt
		
		WHILE @@rowcount > 0
		BEGIN
			SET @tier = @tier+1
			
			INSERT INTO @TABLE (GROUPID,tier_cnt)--,lineage_id)
			SELECT MemberId, @tier--,(SELECT lineage_id FROM @TABLE WHERE GROUPID = GroupId) 
			--Above line is commented because multiple rows are returned in the select query
			FROM USMemberGroup 
			WHERE GroupId IN (SELECT GROUPID FROM @TABLE) 
			AND MemberId NOT in (SELECT GROUPID FROM @TABLE)
			AND MemberType = 2 AND 
			--ApplicationId = ISNULL(@ApplicationId,ApplicationId)
			(@ApplicationId is null OR ApplicationId IN (Select SiteId From dbo.GetAncestorSites(@ApplicationId)))
			
			--UPDATE @TABLE SET lineage_id = lineage_id+LTRIM(STR(ID)) WHERE 
			--len(lineage_id) < tier_cnt
			--Since lineage_id is not inserted the update is not done
		END
	END
	RETURN 
END	
--********************************************************************************

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetVirtualPathByObjectType]'
GO
/*****************************************************
* Name : GetVirtualPathByObjectType
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Function [dbo].[GetVirtualPathByObjectType](@Id uniqueidentifier,@ObjectType int)  
RETURNS NVarchar(Max)
AS  
BEGIN    
	Declare @Path NVarchar(Max),@Lft bigint, @Rgt bigint,@SiteId uniqueidentifier
	Set @Path = '~'
	IF @ObjectType =7 
	BEGIN
		Select @Lft  = LftValue , @Rgt = RgtValue,@SiteId=SiteId From COContentStructure Where Id=@Id
		Select @Path = @Path + '/' +  Title  From COContentStructure
		Where LftValue <= @Lft and RgtValue >= @Rgt
		And LftValue >=1
		And SiteId=@SiteId
		Order by LftValue
	END
	ELSE IF @ObjectType =9
	BEGIN
		Select @Lft  = LftValue , @Rgt = RgtValue,@SiteId=SiteId From COFileStructure Where Id=@Id
		Select @Path = @Path + '/' +  Title  From COFileStructure
		Where LftValue <= @Lft and RgtValue >= @Rgt
		And LftValue >=1
		And SiteId=@SiteId
		Order by LftValue
	END
	ELSE IF @ObjectType =33
	BEGIN
		Select @Lft  = LftValue , @Rgt = RgtValue,@SiteId=SiteId From COImageStructure Where Id=@Id
		Select @Path = @Path + '/' +  Title  From COImageStructure
		Where LftValue <= @Lft and RgtValue >= @Rgt
		And LftValue >=1
		And SiteId=@SiteId
		Order by LftValue
	END
	ELSE IF @ObjectType =38
	BEGIN
		Select @Lft  = LftValue , @Rgt = RgtValue,@SiteId=SiteId From COFormStructure Where Id=@Id
		Select @Path = @Path + '/' +  Title  From COFormStructure
		Where LftValue <= @Lft and RgtValue >= @Rgt
		And LftValue >=1
		And SiteId=@SiteId
		Order by LftValue
	END

	ELSE
	BEGIN
		Select @Lft  = LftValue , @Rgt = RgtValue,@SiteId=SiteId From HSStructure Where Id=@Id
		Select @Path = @Path + '/' +  Title  From HSStructure 
		Where LftValue <= @Lft and RgtValue >= @Rgt
		And LftValue >1
		And SiteId=@SiteId
		Order by LftValue
	END
	RETURN  @Path
END
GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[RegexSelectAll]'
GO
ALTER FUNCTION [dbo].[RegexSelectAll]
(@input NVARCHAR (max), @pattern NVARCHAR (max), @matchDelimiter NVARCHAR (max))
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[RegexSearch].[RegexSelectAll]
 GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetVariantSites]'
GO
ALTER FUNCTION [dbo].[GetVariantSites](@ApplicationId uniqueidentifier)
RETURNS @Sites TABLE 
(
	SiteId uniqueidentifier
)
AS
BEGIN
	IF @ApplicationId IS NULL
	BEGIN
		INSERT INTO @Sites
		SELECT Id from SISite Where Status = 1
	END
	ELSE 
	BEGIN
		DECLARE @MasterSiteId uniqueidentifier
		SET @MasterSiteId = (SELECT TOP 1 MasterSiteId FROM SISite WHERE Id = @ApplicationId)
		
		INSERT INTO @Sites
		SELECT Id FROM SISite WHERE MasterSiteId = @MasterSiteId AND Status = 1
	END
	
	RETURN 
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Regex_ReplaceMatches]'
GO
ALTER FUNCTION [dbo].[Regex_ReplaceMatches]
(@source NVARCHAR (max), @regexPattern NVARCHAR (max), @regexOptions INT, @replacement NVARCHAR (max))
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_ReplaceMatches]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Regex_IsMatch]'
GO
ALTER FUNCTION [dbo].[Regex_IsMatch]
(@source NVARCHAR (max), @regexPattern NVARCHAR (max), @regexOptions INT)
RETURNS BIT
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_IsMatch]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Regex_GetMatches]'
GO
ALTER FUNCTION [dbo].[Regex_GetMatches]
(@source NVARCHAR (max), @regexPattern NVARCHAR (max), @regexOptions INT)
RETURNS 
     TABLE (
        [Match] NVARCHAR (MAX) NULL)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatches]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Regex_GetMatch]'
GO
ALTER FUNCTION [dbo].[Regex_GetMatch]
(@source NVARCHAR (max), @regexPattern NVARCHAR (max), @regexOptions INT)
RETURNS NVARCHAR (max)
AS
 EXTERNAL NAME [Bridgeline.CLRFunctions].[UserDefinedFunctions].[Regex_GetMatch]

 GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Order_GetPaymentTotal]'
GO
/*****************************************************
* Name : Order_GetPaymentTotal
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

-- =============================================
-- Author:		Prakash
-- Create date: 7/15/2009
-- Description:	Calculates the Payment amount for the given order
-- =============================================
ALTER Function [dbo].[Order_GetPaymentTotal]
(
	@OrderId uniqueidentifier
)
RETURNS money
AS
BEGIN

	DECLARE @paymentTotal money

	Select @paymentTotal = sum(isnull(OP.Amount,0)) 
    from PTOrderPayment OP
	INNER JOIN PTPayment P ON OP.PaymentId=P.Id
	Where P.PaymentStatusId in (1,2,3,8,12) and P.IsRefund = 0
	AND OP.OrderId=@OrderId

	RETURN isnull(@paymentTotal,0)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetTotalFileCount]'
GO
/*****************************************************
* Name : GetTotalFileCount
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Function [dbo].[GetTotalFileCount]
(	
	 @SiteId uniqueidentifier
	,@Id uniqueidentifier=null
	,@ObjectTypeId int
)
RETURNS @Results TABLE (Id uniqueidentifier,[Count] bigint)
AS
BEGIN
	IF @ObjectTypeId =7 OR @ObjectTypeId =9 OR @ObjectTypeId =33 
	BEGIN
		INSERT INTO @Results
			Select ParentId Id,count(*) [Count] FROM COContent
				Where (@Id IS NULL OR ParentId = @Id)
					AND ApplicationId = @SiteId
					And (Status=1 OR Status=7) -- displaying ALL content by default so the count should be all the contents
					AND (
						ObjectTypeId = @ObjectTypeId
							OR (
								(ObjectTypeId=7 OR ObjectTypeId=13) 
								AND (@ObjectTypeId = 7 OR @ObjectTypeId = 13)
								)	
						)
				Group By ParentId
	END
	ELSE IF  @ObjectTypeId = 38
	BEGIN
	
	INSERT INTO @Results
			Select ParentId Id,count(*) [Count] FROM Forms
				Where (@Id IS NULL OR ParentId = @Id)
					AND ApplicationId = @SiteId
					And (Status=1 OR Status=7) -- displaying ALL content by default so the count should be all the contents
					
				Group By ParentId
				
	END
	ELSE
	BEGIN
		INSERT INTO @Results
			Select ParentId Id,count(*) [Count]FROM HSStructure 
				Where (@Id IS NULL OR ParentId = @Id)
					AND SiteId IN (Select SiteId from dbo.GetVariantSites(@SiteId))
					AND ObjectTypeId = @ObjectTypeId
				Group By ParentId
	END
	RETURN
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[User_GetGroups]'
GO
/*****************************************************
* Name : User_GetGroups
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--select * from User_GetGroups('798613EE-7B85-4628-A6CC-E17EE80C09C5','8039CE09-E7DA-47E1-BCEC-DF96B5E411F4')
--select * from User_GetGroups('798613EE-7B85-4628-A6CC-E17EE80C09C5','CDD38906-6071-4DD8-9FB6-508C38848C61')

ALTER Function [dbo].[User_GetGroups]
(
	@MemberId uniqueidentifier,
	@ApplicationId	uniqueidentifier
)
RETURNS TABLE AS
RETURN 
(
		with cte1 as 
		(
			SELECT GroupId FROM USMemberGroup MG WHERE MemberId = @MemberId AND MG.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))
		)

	Select  GroupId from cte1
	union All
	SELECT GroupId FROM USMemberGroup WHERE MemberId IN (Select GroupId from cte1)  

)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetRolesForRangeUser]'
GO
/*****************************************************
* Name : GetRolesForRangeUser
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--Declare @ObjectId			uniqueidentifier
ALTER Function [dbo].[GetRolesForRangeUser](
	@ParentId			uniqueidentifier,
	@Lft				bigint,
	@Rgt				bigint,
	@ObjectType			int,
	@UserId				uniqueidentifier,
	@ApplicationId		uniqueidentifier,
	@isGlobalStructure	bit = 0)
RETURNS @Result TABLE(ObjectId uniqueidentifier, Role nvarchar(max), Propogate int)
AS
BEGIN		
	--******** this is required to get analytics global structure data
	DECLARE @SiteId uniqueidentifier
	IF (@isGlobalStructure != 1) -- true = get analytics folder structure where siteid is null
	  SET @SiteId = @ApplicationId
	--******** 

	IF @ObjectType = 7
	BEGIN
		WITH cte1 AS(
			SELECT  --CHECK DIRECT ROLE IN USMemberRoles
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name] Name,
				cast(UR.[Propagate] as int) Propagate
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN COContentStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE UR.MemberId = @UserId
				AND UR.MemberType = 1 AND UR.ApplicationId = @ApplicationId
		UNION 
			SELECT  --CHECK ROLE IN Groups
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name],
				UR.[Propagate]
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN dbo.User_GetGroups(@UserId, @ApplicationId) G on UR.MemberId = G.GroupId
				INNER JOIN  COContentStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE  UR.MemberType = 2 AND UR.ApplicationId = @ApplicationId
		)

		INSERT INTO @Result
		SELECT 
			O.ObjectId,
			'<roles>' + STUFF((
				SELECT '<role Id="' + cast(RoleId as nvarchar(max)) + '" Name="' + CAST([Name] AS VARCHAR(MAX)) + '" Propagate="' + cast(Propagate As nvarchar(max)) + '"/>'
				FROM cte1 WHERE (ObjectId= O.ObjectId) FOR XML PATH (''),TYPE).value('.','VARCHAR(MAX)') ,1,0,'') +
			'</roles>' AS Roles,
			Propagate
		FROM cte1 O
		GROUP BY ObjectId, Propagate
	END
	ELSE IF @ObjectType = 9
	BEGIN
		WITH cte1 AS(
			SELECT  --CHECK DIRECT ROLE IN USMemberRoles
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name] Name,
				cast(UR.[Propagate] as int) Propagate
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN COFileStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE UR.MemberId = @UserId
				AND UR.MemberType = 1 AND UR.ApplicationId = @ApplicationId
		UNION 
			SELECT  --CHECK ROLE IN Groups
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name],
				UR.[Propagate]
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN dbo.User_GetGroups(@UserId, @ApplicationId) G on UR.MemberId = G.GroupId
				INNER JOIN  COFileStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE  UR.MemberType = 2 AND UR.ApplicationId = @ApplicationId
		)

		INSERT INTO @Result
		SELECT 
			O.ObjectId,
			'<roles>' + STUFF((
				SELECT '<role Id="' + cast(RoleId as nvarchar(max)) + '" Name="' + CAST([Name] AS VARCHAR(MAX)) + '" Propagate="' + cast(Propagate As nvarchar(max)) + '"/>'
				FROM cte1 WHERE (ObjectId= O.ObjectId) FOR XML PATH (''),TYPE).value('.','VARCHAR(MAX)') ,1,0,'') +
			'</roles>' AS Roles,
			Propagate
		FROM cte1 O
		GROUP BY ObjectId, Propagate
	END
	ELSE IF @ObjectType = 33
	BEGIN
		WITH cte1 AS(
			SELECT  --CHECK DIRECT ROLE IN USMemberRoles
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name] Name,
				cast(UR.[Propagate] as int) Propagate
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN COImageStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE UR.MemberId = @UserId
				AND UR.MemberType = 1 AND UR.ApplicationId = @ApplicationId
		UNION 
			SELECT  --CHECK ROLE IN Groups
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name],
				UR.[Propagate]
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN dbo.User_GetGroups(@UserId, @ApplicationId) G on UR.MemberId = G.GroupId
				INNER JOIN  COImageStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE  UR.MemberType = 2 AND UR.ApplicationId = @ApplicationId
		)

		INSERT INTO @Result
		SELECT 
			O.ObjectId,
			'<roles>' + STUFF((
				SELECT '<role Id="' + cast(RoleId as nvarchar(max)) + '" Name="' + CAST([Name] AS VARCHAR(MAX)) + '" Propagate="' + cast(Propagate As nvarchar(max)) + '"/>'
				FROM cte1 WHERE (ObjectId= O.ObjectId) FOR XML PATH (''),TYPE).value('.','VARCHAR(MAX)') ,1,0,'') +
			'</roles>' AS Roles,
			Propagate
		FROM cte1 O
		GROUP BY ObjectId, Propagate
	END
	ELSE
	BEGIN
		WITH cte1 AS(
			SELECT  --CHECK DIRECT ROLE IN USMemberRoles
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name] Name,
				cast(UR.[Propagate] as int) Propagate
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN HSStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt AND S.ObjectTypeId = 6 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE UR.MemberId = @UserId AND UR.MemberType=1 
		UNION 
			SELECT  --CHECK ROLE IN Groups
				S.[Id] ObjectId,
				R.[Id] RoleId,
				R.[Name],
				UR.[Propagate]
			FROM USRoles R 
				INNER JOIN USMemberRoles UR on UR.RoleId = R.Id
				INNER JOIN dbo.User_GetGroups(@UserId,@ApplicationId) G on UR.MemberId = G.GroupId
				INNER JOIN  HSStructure S ON UR.ObjectId = S.Id 
					AND S.LftValue Between @Lft and @Rgt AND S.ObjectTypeId = 6 
					AND (SiteId IS NULL OR SiteId = @SiteId)
			WHERE  UR.MemberType = 2 
		)
		INSERT INTO @Result
		SELECT 
			O.ObjectId,
			'<roles>' + STUFF((
				SELECT '<role Id="' + cast(RoleId as nvarchar(max)) + '" Name="' + CAST([Name] AS VARCHAR(MAX)) + '" Propagate="' + cast(Propagate As nvarchar(max)) + '"/>'
				FROM cte1 WHERE (ObjectId= O.ObjectId) FOR XML PATH (''),TYPE).value('.','VARCHAR(MAX)') ,1,0,'') +
			'</roles>' AS Roles,
			Propagate
		FROM cte1 O
		GROUP BY ObjectId, Propagate
	END

RETURN
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[USGetUserIdForSite]'
GO
/*****************************************************
* Name : USGetUserIdForSite
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

--select dbo.USGetUserIdForSite('iappsuser','CDD38906-6071-4DD8-9FB6-508C38848C61')
--select dbo.USGetUserIdForSite('iappsuser', '8039CE09-E7DA-47E1-BCEC-DF96B5E411F4')

ALTER Function [dbo].[USGetUserIdForSite] 
(
	@UserName	nvarchar(256),
	@ApplicationId		uniqueidentifier
)
RETURNS uniqueidentifier
AS
BEGIN
	DECLARE @UserId		uniqueidentifier
	SET @UserId	=	null
	Declare @loweredUserName nvarchar(256)
	SET @loweredUserName =lower(@UserName)

	SELECT  @UserId = u.Id
    FROM    USUser u, USSiteUser s, USMembership m
    WHERE   LoweredUserName = @loweredUserName AND
            --s.SiteId = @ApplicationId AND
            --(s.SiteId in (select SiteId from dbo.GetVariantSites(@ApplicationId))) AND
            s.SiteId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))AND
			u.Id = m.UserId AND
			u.Id = s.UserId AND
			u.Status <> dbo.GetDeleteStatus()

	RETURN @UserId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetTotalBlogFiles]'
GO
/*****************************************************
* Name : GetTotalBlogFiles
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Function [dbo].[GetTotalBlogFiles]
(
	@Id				uniqueidentifier,
	@IsSystemUser	bit
)
RETURNS bigint
AS
BEGIN
	Declare @Count int
	SET @Count=0
	if NOT EXISTS(select ParentId from dbo.HSDefaultParent where ObjectTypeId=dbo.GetBlogObjectType() and ParentId=@Id)
	Begin
		Select @Count= count(*) from BLPost where Id in(
		select Id  from dbo.GetChildrenByHierarchy(@Id,-1) where ObjectTypeId=dbo.GetPostObjectType()) 
			and ((@IsSystemUser = 1 AND (Status = 1 OR Status = 8))
			or (@IsSystemUser = 0 AND Status = 8))
	end
	RETURN @Count

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetCustomAttributeXml]'
GO
/*****************************************************
* Name : GetCustomAttributeXml
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/



ALTER FUNCTION [dbo].[GetCustomAttributeXml] 
(
	@Xml xml,
	@TagName nvarchar(100)
)
RETURNS xml
AS
BEGIN

IF lower(@TagName) = 'style'
BEGIN
	SET @Xml.modify('delete /style/@id[1]')
	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<style', '<customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'script'
BEGIN
	SET @Xml.modify('delete /script/@id[1]')
	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<script', '<customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'pagedefinition'
BEGIN
	SET @Xml.modify('delete /pageDefinition/@id[1]')
	SET @Xml.modify('delete /pageDefinition/@friendlyName[1]')
	SET @Xml.modify('delete /pageDefinition/@templateId[1]')
	SET @Xml.modify('delete /pageDefinition/@isDefault[1]')
	SET @Xml.modify('delete /pageDefinition/@isTracked[1]')
	SET @Xml.modify('delete /pageDefinition/@workflowId[1]')
	SET @Xml.modify('delete /pageDefinition/@title[1]')
	SET @Xml.modify('delete /pageDefinition/@description[1]')
	SET @Xml.modify('delete /pageDefinition/@status[1]')
	SET @Xml.modify('delete /pageDefinition/@createdDate[1]')
	SET @Xml.modify('delete /pageDefinition/@createdBy[1]')
	SET @Xml.modify('delete /pageDefinition/@modifiedDate[1]')
	SET @Xml.modify('delete /pageDefinition/@modifiedBy[1]')
	SET @Xml.modify('delete /pageDefinition/@archiveDate[1]')
	SET @Xml.modify('delete /pageDefinition/@publishDate[1]')
	SET @Xml.modify('delete /pageDefinition/@statusChangedDate[1]')
	SET @Xml.modify('delete /pageDefinition/@securityLevels[1]')
	SET @Xml.modify('delete /pageDefinition/@authorId[1]')
	SET @Xml.modify('delete /pageDefinition/@publishCount[1]')
	SET @Xml.modify('delete /pageDefinition/@workflowState[1]')
	SET @Xml.modify('delete /pageDefinition/@TextContentCounter[1]')
	SET @Xml.modify('delete /pageDefinition/@DisplayOrder[1]')
	SET @Xml.modify('delete /pageDefinition/@HasForms[1]')
	SET @Xml.modify('delete /pageDefinition/@EnableOutputCache[1]')
	SET @Xml.modify('delete /pageDefinition/@OutputCacheProfileName[1]')
	SET @Xml.modify('delete /pageDefinition/@ExcludeFromSearch[1]')
	SET @Xml.modify('delete /pageDefinition/@sourcePageDefinitionId[1]')
	SET @Xml.modify('delete /pageDefinition/@isInGroupPublish[1]')
	SET @Xml.modify('delete /pageDefinition/@scheduledPublishDate[1]')
	SET @Xml.modify('delete /pageDefinition/@scheduledArchiveDate[1]')
	SET @Xml.modify('delete /pageDefinition/@nextVersionToPublish[1]')
	
	SET  @Xml.modify('delete /pageDefinition/*')
	
	SET @Xml = cast(replace(replace(cast(@Xml as nvarchar(max)), '<pageDefinition', '<customAttributes'), '</pageDefinition', '</customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'container'
BEGIN
	SET @Xml.modify('delete /container/@id[1]')
	SET @Xml.modify('delete /container/@name[1]')
	SET @Xml.modify('delete /container/@contentId[1]')
	SET @Xml.modify('delete /container/@inWFContentId[1]')
	SET @Xml.modify('delete /container/@contentTypeId[1]')
	SET @Xml.modify('delete /container/@isModified[1]')
	SET @Xml.modify('delete /container/@isTracked[1]')
	SET @Xml.modify('delete /container/@visible[1]')
	SET @Xml.modify('delete /container/@inWFVisible[1]')
	SET @Xml.modify('delete /container/@displayOrder[1]')
	SET @Xml.modify('delete /container/@inWFDisplayOrder[1]')

	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<container', '<customAttributes') as xml)
END
ELSE IF lower(@TagName) = 'pagemapnode'
BEGIN
	SET @Xml.modify('delete /pageMapNode/pageMapNodeWorkFlow')
	SET @Xml.modify('delete /pageMapNode/@id[1]')
	SET @Xml.modify('delete /pageMapNode/@parentId[1]')
	SET @Xml.modify('delete /pageMapNode/@description[1]')
	SET @Xml.modify('delete /pageMapNode/@displayTitle[1]')
	SET @Xml.modify('delete /pageMapNode/@friendlyUrl[1]')
	SET @Xml.modify('delete /pageMapNode/@menuStatus[1]')
	SET @Xml.modify('delete /pageMapNode/@targetId[1]')
	SET @Xml.modify('delete /pageMapNode/@target[1]')
	SET @Xml.modify('delete /pageMapNode/@targetUrl[1]')
	SET @Xml.modify('delete /pageMapNode/@createdBy[1]')
	SET @Xml.modify('delete /pageMapNode/@createdDate[1]')
	SET @Xml.modify('delete /pageMapNode/@modifiedBy[1]')
	SET @Xml.modify('delete /pageMapNode/@modifiedDate[1]')
	SET @Xml.modify('delete /pageMapNode/@propogateWorkFlow[1]')
	SET @Xml.modify('delete /pageMapNode/@inheritWorkFlow[1]')
	SET @Xml.modify('delete /pageMapNode/@roles[1]')
	SET @Xml.modify('delete /pageMapNode/@propogateSecurityLevels[1]')
	SET @Xml.modify('delete /pageMapNode/@inheritSecurityLevels[1]')
	SET @Xml.modify('delete /pageMapNode/@propogateRoles[1]')
	SET @Xml.modify('delete /pageMapNode/@inheritRoles[1]')
	SET @Xml.modify('delete /pageMapNode/@locationIdentifier[1]')
	SET @Xml.modify('delete /pageMapNode/@hasRolloverImages[1]')
	SET @Xml.modify('delete /pageMapNode/@rolloverOnImage[1]')
	SET @Xml.modify('delete /pageMapNode/@rolloverOffImage[1]')
	SET @Xml.modify('delete /pageMapNode/@securityLevels[1]')
	SET @Xml.modify('delete /pageMapNode/@IsCommerceNav[1]')
	SET @Xml.modify('delete /pageMapNode/@DefaultContentId[1]')
	SET @Xml.modify('delete /pageMapNode/@AssociatedQueryId[1]')
	SET @Xml.modify('delete /pageMapNode/@AssociatedContentFolderId[1]')
	
	SET  @Xml.modify('delete /pageMapNode/*')
	
	SET @Xml = cast(replace(cast(@Xml as nvarchar(max)), '<pageMapNode', '<customAttributes') as xml)
END
RETURN @Xml
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetAllParents]'
GO
ALTER FUNCTION [dbo].[GetAllParents](@ObjectId uniqueidentifier)  
RETURNS @Results TABLE 
(
	ObjectId uniqueidentifier,  
	ObjectTypeId int,  
	ParentId uniqueidentifier
)  
AS  
BEGIN    
	DECLARE  @RgtValue BIGINT
	DECLARE @LftValue BIGINT 
	DECLARE @SiteId uniqueidentifier 
	
	IF EXISTS (SELECT * FROM PageMapNode Where PageMapNodeId = @ObjectId)
	BEGIN
		SELECT @LftValue = Lftvalue, @RgtValue = RgtValue, @SiteId = SiteId FROM PageMapNode WHERE PageMapNodeId = @ObjectId	
		INSERT INTO @Results
		SELECT PageMapNodeId, 2, ParentId FROM PageMapNode 
			WHERE @LftValue > LftValue AND RgtValue > @RgtValue AND SiteId= @SiteId
		ORDER BY LftValue
	END
	ELSE IF EXISTS (SELECT * FROM COContentStructure Where Id = @ObjectId)
	BEGIN
		SELECT @LftValue = Lftvalue, @RgtValue = RgtValue, @SiteId = SiteId FROM COContentStructure WHERE Id = @ObjectId	
		INSERT INTO @Results
		SELECT Id, 2, ParentId FROM COContentStructure 
			WHERE @LftValue > LftValue AND RgtValue > @RgtValue AND SiteId= @SiteId
		ORDER BY LftValue
	END
	ELSE IF EXISTS (SELECT * FROM COFileStructure Where Id = @ObjectId)
	BEGIN
		SELECT @LftValue = Lftvalue, @RgtValue = RgtValue, @SiteId = SiteId FROM COFileStructure WHERE Id = @ObjectId	
		INSERT INTO @Results
		SELECT Id, 2, ParentId FROM COFileStructure 
			WHERE @LftValue > LftValue AND RgtValue > @RgtValue AND SiteId= @SiteId
		ORDER BY LftValue
	END
	ELSE IF EXISTS (SELECT * FROM COImageStructure Where Id = @ObjectId)
	BEGIN
		SELECT @LftValue = Lftvalue, @RgtValue = RgtValue, @SiteId = SiteId FROM COImageStructure WHERE Id = @ObjectId	
		INSERT INTO @Results
		SELECT Id, 2, ParentId FROM COImageStructure 
			WHERE @LftValue > LftValue AND RgtValue > @RgtValue AND SiteId= @SiteId
		ORDER BY LftValue
	END
	ELSE
	BEGIN	
		SELECT @LftValue = LftValue, @RgtValue = RgtValue, @SiteId = SiteId FROM dbo.HSStructure WHERE [Id] = @ObjectId
		INSERT INTO @Results  
		SELECT P.Id, P.ObjectTypeId, P.ParentId FROM HSStructure P  
			WHERE @LftValue > P.LftValue AND P.RgtValue > @RgtValue AND SiteId = @SiteId
		ORDER BY P.LftValue  
	END
			 
	  
	RETURN  
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[User_GetMemberRoles]'
GO
/*****************************************************
* Name : User_GetMemberRoles
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Function [dbo].[User_GetMemberRoles]  
(  
 @ApplicationId uniqueidentifier,  
 @Id    int,  
 @MemberId  uniqueidentifier,  
 @MemberType  smallint,  
 @ObjectId  uniqueidentifier,  
 @ObjectTypeId int,  
 @OnlyGlobal  bit = null,  
 @ProductId  uniqueidentifier = NULL  
  
)  
RETURNS @ResultTable TABLE   
(  
 Id   int,  
 [Name]  nvarchar(256),  
 Description nvarchar(256),  
 IsGlobal bit,  
 ApplicationId uniqueidentifier,  
 ProductId  uniqueidentifier   
)  
AS  
BEGIN  
 -- If memberId is null then get all roles from the usroles table  
 IF (@MemberId IS NULL)  
 BEGIN  
  INSERT INTO @ResultTable  
  SELECT   
   Id,  
   [Name],  
   Description,  
   IsGlobal, ApplicationId, ApplicationId  
  FROM   
   USRoles  
  WHERE  
   ApplicationId = Isnull(@ApplicationId,ApplicationId) AND  
   IsGlobal = ISNULL(@OnlyGlobal, IsGlobal)  
 END  
 ELSE IF(@ObjectId IS NULL AND @ObjectTypeId IS NOT NULL)  
 BEGIN  
  INSERT INTO @ResultTable  
  SELECT DISTINCT -- CHECK FOR GLOBAL ROLES  
   R.[Id],  
   R.[Name],  
   R.[Description],  
   R.IsGlobal, MR.ApplicationId, MR.ProductId   
  FROM USRoles R, USMemberRoles MR    
  WHERE MR.ObjectTypeId = @ObjectTypeId    
    AND  
    ((  
     (R.IsGlobal = 0 OR R.IsGlobal is null) AND Isnull(@OnlyGlobal,0) = 0  AND -- OnlyGlobal is null means both roles  
     MR.ApplicationId  = @ApplicationId   
    )   
    OR  
    (  
     R.IsGlobal = 1 AND Isnull(@OnlyGlobal,1) = 1  -- For Global Roles there is no application id checking  
    ))   
    AND  
    MR.RoleId = R.Id AND  
    (  
     (  
      MR.MemberId = @MemberId    
      --And UR.UserType=@MemberType  
     )   
     OR   
     (  
      -- To check in the group  
      MR.MemberId in   
      (  
       SELECT GroupId FROM User_GetGroups(@MemberId,@ApplicationId)   
      )  
      AND MR.MemberType = 2 -- group  
     )  
    ) AND MR.ProductId = ISNULL(@ProductId,ProductId)  
  
  
 END  
 ELSE IF( @ObjectId IS NOT NULL )  
 BEGIN  
  --MIXED MODE PERMISSION  
  INSERT INTO @ResultTable  
  SELECT  --CHECK DIRECT ROLE IN USUSERROLES  
   R.[Id],  
   R.[Name],  
   R.[Description],  
   R.IsGlobal , MR.ApplicationId, MR.ProductId  
  FROM USRoles R, USMemberRoles MR  
  WHERE R.[Id] = MR.RoleId  AND   
    MR.ObjectId = @ObjectId  AND --- if object id there then it is not global permission; it is custom permission to that object  
    MR.ApplicationId   = @ApplicationId  AND  
    (  
     (   
      MR.MemberId = @MemberId  AND   
      MR.MemberType = @MemberType    
     )   
     OR   
     (  
      MR.MemberId IN   
      (  
       SELECT GroupId   
       FROM  USMemberGroup   
       WHERE MemberId = @MemberId AND   
         MemberType = @MemberType  And ApplicationId   = @ApplicationId 
      )  
      AND MR.MemberType=2 --Group  
     )  
    ) AND MR.ProductId = ISNULL(@ProductId,ProductId)  
    
  UNION  
    
  SELECT  --CHECK FOR ITS PARENT HAVING PROPAGATE SET TO TRUE  
   R.[Id],  
   R.[Name],  
   R.[Description],  
   R.IsGlobal , MR.ApplicationId, MR.ProductId  
  FROM USRoles R, USMemberRoles MR, dbo.GetAllParents(@ObjectId) T  
  WHERE   
   R.[Id] = MR.RoleId   AND  
   MR.ApplicationId   = @ApplicationId  AND  
   MR.ObjectId = T.ObjectId AND  
   MR.MemberId = @MemberId  AND  
   MR.MemberType = @MemberType AND  
   MR.Propagate = 1 AND  
   MR.ProductId = ISNULL(@ProductId,ProductId)  
  
    
 END  
 IF( @ObjectId IS NULL AND @ObjectTypeId IS NULL )   
 BEGIN  
  /*  
   This is to get the roles for the given member.   
   Here both the ObjectId and ObjectTypeId will not be considered.  
   If the global bit is true then only global roles will be returned.  
  */  
  DECLARE @GroupTable TABLE (GroupId uniqueidentifier)  
  INSERT INTO @GroupTable   
  SELECT GroupId FROM User_GetGroups(@MemberId,@ApplicationId)  
  --with cte1 as   
  --(  
  -- SELECT GroupId FROM User_GetGroups(@MemberId,@ApplicationId)  
  --) -- do not use cte in inner query -- i read in sqlservercenter.com  
    
  INSERT INTO @ResultTable  
  SELECT DISTINCT  
   R.[Id],  
   R.[Name],  
   R.[Description],  
   R.IsGlobal , 
   @ApplicationId
   --MR.ApplicationId, 
   ,MR.ProductId  
  FROM USRoles R  
  INNER JOIN USMemberRoles MR ON R.Id = MR.RoleId  
  WHERE    
  -- I do not know why we need this. whatever the case role & member has to be there in MR -- probably for group   
    ((   
     (R.IsGlobal = 0 OR R.IsGlobal is null) AND Isnull(@OnlyGlobal,0) = 0  AND  
     MR.ApplicationId   = @ApplicationId  -- This will check the direct roles.  
    )   
    OR  
    (  
     R.IsGlobal = 1 AND Isnull(@OnlyGlobal,1) = 1 
    ))  
    AND  
   --R.Id = MR.RoleId AND -- changed to inner join  
   (  
    (  
     MR.MemberId = @MemberId  ---- direct roles  
     And MR.MemberType = @MemberType  --AND  MR.ApplicationId IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId))  --= @ApplicationId
    )  
    OR (MR.MemberType = 2 --AND  MR.ApplicationId  IN (SELECT SiteId FROM dbo.GetAncestorSites(@ApplicationId)) -- = @ApplicationId  
    AND MR.MemberId IN (SELECT GroupId FROM @GroupTable) -- (SELECT GroupId FROM User_GetGroups(@MemberId,@ApplicationId)) -- through group  
     )  
   ) AND MR.ProductId = ISNULL(@ProductId,ProductId)  
 END  
 RETURN   
END  

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[GetChildrenByHierarchyType]'
GO
/*****************************************************
* Name : GetChildrenByHierarchyType
* File Version : 5.1.0.10182013
* Created By : 
* Change Log (Reason, Changed By, Changed Date) : 
******************************************************/

ALTER Function [dbo].[GetChildrenByHierarchyType](@Id uniqueidentifier,@Level int,@ObjectTypeId int)
RETURNS @Results TABLE (Id uniqueidentifier,
		ObjectTypeId int,
		Title nvarchar(256),
		Keywords nvarchar(1024),
		ParentId uniqueidentifier,
		LftValue bigint,
		RgtValue bigint,
		VirtualPath nvarchar(max),
		SiteId uniqueidentifier)
AS
Begin
--********************************************************************************
-- Variable declaration
--********************************************************************************
Declare @Lft bigint,@Rgt bigint,@SiteId uniqueidentifier
Declare @isIdSite bit
IF EXISTS(Select 1 from SISite where Id=@Id) 
	SET @isIdSite = 1
ELSE 
	SET @isIdSite = 0
--********************************************************************************
-- code
--********************************************************************************
IF @ObjectTypeId =7
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				@ObjectTypeId ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COContentStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE Id = @Id OR @isIdSite = 1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COContentStructure WHERE Id=@Id
	ELSE
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COContentStructure Order By LftValue
		
		INSERT INTO @Results 
		SELECT  S.Id,
				7 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,	
				VirtualPath,
				SiteId
			FROM COContentStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END
ELSE IF @ObjectTypeId =9 
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				9 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COFileStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE S.Id = @Id OR @isIdSite =1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFileStructure WHERE Id=@Id
	else
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFileStructure  Order By LftValue
			
		INSERT INTO @Results 
		SELECT  S.Id,
				9 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,	
				SiteId
			FROM COFileStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END
ELSE IF @ObjectTypeId =33 
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				33 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COImageStructure S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE S.Id = @Id or @isIdSite = 1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COImageStructure WHERE Id=@Id
	ELSE
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COImageStructure  Order By LftValue
			
		INSERT INTO @Results 
		SELECT  S.Id,
				33 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,	
				VirtualPath,
				SiteId
			FROM COImageStructure  S
			--Inner Join COContent C on S.Id =C.ParentId
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END
ELSE IF @ObjectTypeId =38 
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  S.Id,
				38 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,
				VirtualPath,
				SiteId
			FROM COFormStructure S
			WHERE S.Id = @Id or @isIdSite = 1
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	IF @isIdSite = 0
		SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFormStructure WHERE Id=@Id
	ELSE
		SELECT top 1 @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM COFormStructure  Order By LftValue
			
		INSERT INTO @Results 
		SELECT  S.Id,
				38 ObjectTypeId,
				S.Title,
				S.Keywords,
				S.ParentId,
				LftValue,
				RgtValue,	
				VirtualPath,
				SiteId
			FROM COFormStructure  S
			WHERE LftValue >= @Lft and RgtValue <= @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
END

ELSE
BEGIN
	IF (@Level = 0)
	BEGIN
		INSERT INTO @Results 
		SELECT  H.Id,
				H.ObjectTypeId,
				H.Title,
				Keywords,
				ParentId,
				LftValue,
				RgtValue,
				S.VirtualPath AS VirtualPath,
				SiteId
			FROM HSStructure H
			INNER JOIN SISiteDirectory S ON H.ParentId = S.Id
			WHERE ParentId = @Id
			ORDER BY LftValue
	END
	ELSE
	BEGIN
	SELECT @Lft=LftValue,@Rgt=RgtValue,@SiteId=SiteId FROM HSStructure WHERE Id=@Id
		INSERT INTO @Results 
		SELECT  H.Id,
				H.ObjectTypeId,
				H.Title,
				Keywords,
				ParentId,
				LftValue,
				RgtValue,	
				S.VirtualPath AS VirtualPath,
				SiteId
			FROM HSStructure H
			INNER JOIN SISiteDirectory S ON H.ParentId = S.Id
			WHERE LftValue > @Lft and RgtValue < @Rgt
			AND SiteId=@SiteId
			ORDER BY LftValue
	END		
	
END
	
Return 
End


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GenerateXml]'
GO
IF OBJECT_ID(N'[dbo].[GenerateXml]', 'AF') IS NULL
EXEC sp_executesql N'CREATE AGGREGATE [dbo].[GenerateXml] (@Value [NVARCHAR] (max))
RETURNS [NVARCHAR] (max)
EXTERNAL NAME [Bridgeline.CLRFunctions].[GenerateXml]
'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
