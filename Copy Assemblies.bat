mkdir temp

robocopy "Working\GACDLLs" "temp" /e /a-:R

robocopy "temp" "Working\Admin\AnalyzerAdmin\bin" /e /a-:R
robocopy "temp" "Working\Admin\AnalyzerWebService\bin" /e /a-:R
robocopy "temp" "Working\Admin\CMSAdmin\bin" /e /a-:R
robocopy "temp" "Working\Admin\CMSWebService\bin" /e /a-:R
robocopy "temp" "Working\Admin\CommerceAdmin\bin" /e /a-:R
robocopy "temp" "Working\Admin\CommerceWebService\bin" /e /a-:R
robocopy "temp" "Working\Admin\iAPPSCommonLogin\bin" /e /a-:R
robocopy "temp" "Working\Admin\MarketierAdmin\bin" /e /a-:R
robocopy "temp" "Working\Admin\MarketierWebService\bin" /e /a-:R
robocopy "temp" "Working\Admin\SocialAdmin\bin" /e /a-:R
robocopy "temp" "Working\FrontEnd\CMSFrontEndSite\bin" /e /a-:R

rmdir /s /q temp