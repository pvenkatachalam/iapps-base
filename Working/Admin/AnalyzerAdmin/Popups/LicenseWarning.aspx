﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LicenseWarning.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Analyzer.Web.LicenseWarning" 
    MasterPageFile="~/Popups/iAPPSPopup.Master" StylesheetTheme="General" Title="<%$ Resources:GUIStrings, iAPPSAnalyzerLicenseWarning %>" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.licenseWarningWindow.hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="pldContentManager" runat="server" Visible="false">
        <div class="warningLeftColumn">
            <asp:Image ID="imgCMS" runat="server" ImageUrl="~/images/cm-logo.png"
                AlternateText="<%$ Resources:GUIStrings, iAPPSContentManager %>" />
        </div>
        <div class="warningRightColumn">
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, ContentManagerempowersnontechnicaluserstoquicklyandeasilycreateeditandpublishpersuasiveconversionorientedcontent %>"/></p>
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, AccesstoContentManagerrequiresalicensePleasecontactBridgelineformoreinformationonupgradingtothepowerofContentManager %>"/></p>
            <p style="height: 40px;">&nbsp;</p>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="pldAnalytics" runat="server" Visible="false">
        <div class="warningLeftColumn">
            <asp:Image ID="imgAnalytics" runat="server" ImageUrl="~/images/analytics-logo.png"
                AlternateText="<%$ Resources:GUIStrings, iAPPSAnalytics %>" />
        </div>
        <div class="warningRightColumn">
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, Analyticsisanextgenerationwebanalyticsproductthatvastlyimprovestheeffectivenessofyourwebpropertiesbyprovidingactionableintelligenceonuserbehavior %>"/></p>
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, AccesstoAnalyticsrequiresalicensePleasecontactBridgelineformoreinformationonhowthedeepdatalevelintegrationofAnalyticswithContentManagerMarketierandCommercedeliversimprovedanalyticsdrivencontent %>"/></p>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="pldMarketier" runat="server" Visible="false">
        <div class="warningLeftColumn">
            <asp:Image ID="imgMarketier" runat="server" ImageUrl="~/images/marketier-logo.png"
                AlternateText="<%$ Resources:GUIStrings, iAPPSMarketier %>" />
        </div>
        <div class="warningRightColumn">
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, Marketierempowersyourmarketerstoeasilycreatepersonalizedultratargetedandmeasurableonlinecampaignsthatdriveconversion %>"/></p>
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, AccesstoMarketierrequiresalicensePleasecontactBridgelinetolearnmoreabouttheadvancedmarketingmanagementcapabilitiesofMarketier %>"/></p>
            <p style="height: 50px;">&nbsp;</p>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="pldCommerce" runat="server" Visible="false">
        <div class="warningLeftColumn">
            <asp:Image ID="imgCommerce" runat="server" ImageUrl="~/images/commerce-logo.png"
                AlternateText="<%$ Resources:GUIStrings, iAPPSCommerce %>" />
        </div>
        <div class="warningRightColumn">
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, CommercedeliversarobustindustryleadingeCommerceplatformthatwillsuperchargeyouronlinesales %>"/></p>
            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, AccesstoCommercerequiresalicensePleasecontactBridgelinetolearnhowCommercecantakeyouronlinebusinessinitiativestothenextlevel %>"/></p>
            <p style="height: 70px;">&nbsp;</p>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="pldSocial" runat="server" Visible="false">
        <div class="warningLeftColumn">
            <asp:Image ID="imgSocial" runat="server" ImageUrl="~/images/marketier-logo.png"
                AlternateText="<%$ Resources:GUIStrings, iAPPSSocial %>" />
        </div>
        <div class="warningRightColumn">
            <p><asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, Marketierempowersyourmarketerstoeasilycreatepersonalizedultratargetedandmeasurableonlinecampaignsthatdriveconversion %>"/></p>
            <p><asp:localize ID="Localize2" runat="server" text="<%$ Resources:GUIStrings, AccesstoMarketierrequiresalicensePleasecontactBridgelinetolearnmoreabouttheadvancedmarketingmanagementcapabilitiesofMarketier %>"/></p>
            <p style="height: 50px;">&nbsp;</p>
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>"
        OnClientClick="return ClosePopup();" />
</asp:Content>