﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master" ValidateRequest="false"
    CodeBehind="AssignTagsPopup.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.AssignTagsPopup" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <UC:AssignTags ID="indexTermsTree" runat="server" TreeHeight="400" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"
        OnClick="btnSave_Click" />
</asp:Content>
