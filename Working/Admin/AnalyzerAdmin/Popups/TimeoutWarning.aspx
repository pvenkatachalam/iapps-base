﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="TimeoutWarning" StylesheetTheme="General"
    CodeBehind="TimeoutWarning.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:localize runat="server" text="<%$ Resources:GUIStrings, iAPPSSessionTimeoutWarning %>"/></title>
    <script type="text/javascript">
        var isFrontEndG;
        $(document).ready(function () {
            StartTimeoutCounter();
        });
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="licenseWarning">
        <div class="popupBoxShadow ">
            <div class="popupboxContainer">
                <div class="dialogBox">
                    <h3>
                        <asp:localize runat="server" text="<%$ Resources:GUIStrings, Warning %>"/></h3>
                    <div>
                        <p id="spnTimeoutMsg">
                            <asp:localize runat="server" text="<%$ Resources:GUIStrings, Yoursessionwilltimeoutin60Doyouwishtoresetyoursessionorlogout %>"/></p>
                    </div>
                    <div class="footerContent">
                        <asp:Button ID="btnReset" runat="server" Text="<%$ Resources:GUIStrings, Reset %>" CssClass="button" ToolTip="<%$ Resources:GUIStrings, Reset %>"
                            OnClientClick="resetUserSession(this);return false;" />
                        <asp:Button ID="btnLogout" runat="server" Text="<%$ Resources:GUIStrings, LogOut %>" CssClass="button" ToolTip="<%$ Resources:GUIStrings, LogOut %>"
                            OnClientClick="LogoutUser();return false;" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
