<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.HierarchicalGridMenusPermission"
    CodeBehind="HierarchicalGridMenusPermissions.ascx.cs" %>
<script type="text/javascript">
    function grdMenuPermissions_onItemExpand(sender, eventArgs) {
        //grdMenuPermissions.render();
    }

    function grdMenuPermissions_onRenderComplete(sender, eventArgs) {
        if (globalAuthor)
            $("[id^=checkbox_" + grdMenuPermissions.get_id() + "_Author]").prop("disabled", true).prop("checked", true);
        if (globalApprover)
            $("[id^=checkbox_" + grdMenuPermissions.get_id() + "_Approver]").prop("disabled", true).prop("checked", true);
        if (globalPublisher)
            $("[id^=checkbox_" + grdMenuPermissions.get_id() + "_Publisher]").prop("disabled", true).prop("checked", true);
        if (globalNavEditor)
            $("[id^=checkbox_" + grdMenuPermissions.get_id() + "_CustomNavEditor]").prop("disabled", true).prop("checked", true);

    }

    var allAuthors = false;
    var allApprovers = false;
    var allPublishers = false;
    var allNone = false;
    var allArchiver = false;
    var allCustomNavEditor = false;
    var allMasterImport = false;

    var AUTHOR = 1;
    var APPROVER = 2;
    var PUBLISHER = 3;
    var ARCHIVE = 4;
    var CUSTOMNAVEDITOR = 5;
    var MASTERIMPORT = 6;
    var MENU_NONE = 7;

    var OBJECTID = 8;
    var TRACK = 9;
    var GRPTRACK = 10;
    function checkAllNone(obj) {
        if (allNone) {
            allNone = false;
            //CheckAllChildrenOfNode( grdMenuPermissions.Table,columnIndex,false);
            UncheckAllItems(MENU_NONE);
        }
        else {
            allNone = true;
            CheckAllItems(MENU_NONE);
            UncheckAllItems(AUTHOR);
            UncheckAllItems(APPROVER);
            UncheckAllItems(PUBLISHER);
            UncheckAllItems(CUSTOMNAVEDITOR);
            UncheckAllItems(MASTERIMPORT);
            UncheckAllItems(ARCHIVE);

            document.getElementById("chkAuthor").checked = false;
            document.getElementById("chkApprover").checked = false;
            document.getElementById("chkPublisher").checked = false;
            document.getElementById("chkArchiver").checked = false;
            document.getElementById("chkNavEditor").checked = false;
            document.getElementById("chkMasterImport").checked = false;
        }
    }
    function checkAllApprovers(obj) {
        if (allApprovers) {
            allApprovers = false;
            UncheckAllItems(APPROVER);
        }
        else {
            allApprovers = true;
            CheckAllItems(APPROVER);
            //UncheckAllItems(MENU_NONE);
            document.getElementById("chkNone").checked = false;
        }
    }
    //This function will be called when the user clicks All checkbox. 
    function checkAllAuthor(obj) {

        if (allAuthors) {
            allAuthors = false;
            UncheckAllItems(AUTHOR);
        }
        else {
            allAuthors = true;

            CheckAllItems(AUTHOR);
            //UncheckAllItems(MENU_NONE);
            document.getElementById("chkNone").checked = false;
        }
    }
    function checkAllArchiver(obj) {
        if (allArchiver) {
            allArchiver = false;
            UncheckAllItems(ARCHIVE);
        }
        else {
            allArchiver = true;

            CheckAllItems(ARCHIVE);

            document.getElementById("chkNone").checked = false;
        }
    }

    function checkallCustomNavEditor(obj) {
        if (allCustomNavEditor) {
            allCustomNavEditor = false;
            UncheckAllItems(CUSTOMNAVEDITOR);
        }
        else {
            allCustomNavEditor = true;
            CheckAllItems(CUSTOMNAVEDITOR);
            document.getElementById("chkNone").checked = false;
        }
    }


    function checkallMasterImport(obj) {
        if (allMasterImport) {
            allMasterImport = false;
            UncheckAllItems(MASTERIMPORT);
        }
        else {
            allMasterImport = true;
            CheckAllItems(MASTERIMPORT);
            document.getElementById("chkNone").checked = false;
        }
    }

    function CheckAllItems(columnno) {
        var gridItem;
        var gridItemChild;
        var itemIndex = 0;
        var itemIndexChild = 0;
        var childTable;

        var grid = grdMenuPermissions;
        if (grid != null) {
            //For all the levels all the nodes should be checked.
            for (var i = 0; i < grid.Levels.length; i++) {
                CheckAllChildrenOfNode(grid.get_table(i), columnno, true);
            }
        }
        grid.render();
    }

    function checkAllPublisher(obj) {
        if (allPublishers) {
            allPublishers = false;
            UncheckAllItems(PUBLISHER);
        }
        else {
            allPublishers = true;
            CheckAllItems(PUBLISHER);
            UncheckAllItems(MENU_NONE);
            document.getElementById("chkNone").checked = false;
        }
    }

    //This will uncheck all the items .    
    function UncheckAllItems(columnno) {
        var gridItem;
        var gridItemChild;
        var itemIndex = 0;
        var itemIndexChild = 0;
        var childTable;

        var grid = grdMenuPermissions;
        if (grid != null) {
            //For all the levels all the nodes should be unchecked.
            for (var i = 0; i < grid.Levels.length; i++) {
                CheckAllChildrenOfNode(grid.get_table(i), columnno, false);
            }
        }

        grid.render();
    }

    //This will return true if the checkbox is checked else return false;
    function IsChecked(checkFlag) {
        //alert(event.srcElement.checked);

        //if(event != null && event.srcElement != null)
        {
            return checkFlag;
        }
    }

    var SelectedColumnIndex = null;
    function grdMenuPermissions_onItemBeforeCheckChange(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();
        SelectedColumnIndex = eventArgs.get_columnIndex();
    }

    //Event handler for all the check boxes except the header check box. 
    function grdMenuPermissions_onItemCheckChange(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();

        var columnIndex = SelectedColumnIndex;
        var isPropagate = false;
        var table = selectedItem.ChildTable;
        var isChecked = selectedItem.Data[columnIndex];
        var objectId = selectedItem.Cells[OBJECTID].Value;
        var trackColumnvalue = selectedItem.Cells[TRACK].Value; //Column track

        //        if (table != null) {
        //            var childTableRowCount = eventArgs.get_item().get_childTable().getRowCount();
        //            isPropagate = window.confirm(GetMessage("PropagateWarning"));
        //        }

        if (self["GetchkPropagateValue"])
            isPropagate = GetchkPropagateValue();


        var hdnAuthorPropagateTracker = document.getElementById("<%=HdnAuthorPropagateTrack.ClientID %>");
        var hdnApproverPropagateTracker = document.getElementById("<%=HdnApproverPropagateTrack.ClientID %>");
        var hdnPublisherPropagateTracker = document.getElementById("<%=HdnPublisherPropagateTrack.ClientID %>");
        var HdnCustomNavEditorPropagateTracker = document.getElementById("<%=HdnCustomNavEditorPropagateTrack.ClientID %>");
        var hdnArchiverPropagateTracker = document.getElementById("<%=HdnArchiverPropagateTrack.ClientID %>");
        var hdnMasterImportPropagateTracker = document.getElementById("<%=HdnMasterImportPropagateTrack.ClientID %>");

        if (columnIndex == MENU_NONE) {
            var isNoneChecked = selectedItem.Data[MENU_NONE];

            if (isNoneChecked) {
                if (isPropagate) {
                    CheckAllChildrenOfNode(table, MENU_NONE, isNoneChecked);
                    if (isNoneChecked) {
                        CheckAllChildrenOfNode(table, AUTHOR, false);
                        CheckAllChildrenOfNode(table, APPROVER, false);
                        CheckAllChildrenOfNode(table, PUBLISHER, false);
                        CheckAllChildrenOfNode(table, CUSTOMNAVEDITOR, false);
                        CheckAllChildrenOfNode(table, ARCHIVE, false);
                        CheckAllChildrenOfNode(table, MASTERIMPORT, false);
                    }
                    RemoveFromPropagateValue(objectId, MENU_NONE);
                }

                //This will uncheck all the chechboxes and remove the object from the changed memberrole collection.
                for (var j = 1; j < 7; j++) {
                    if (j != 4 && selectedItem.Data[j]) {
                        RemoveFromPropagateValue(objectId, j);
                        var newvalue = UpdateTrackColumn(selectedItem.Cells[TRACK].Value, j);
                        selectedItem.SetValue(TRACK, newvalue, false);
                        selectedItem.SetValue(j, false, false); //This will uncheck all the other check boxes.
                    }
                }
            }
            else {
                CheckAllChildrenOfNode(table, NONE_OBJ, false);
            }
        }
        else {
            var hdnTracker = null;
            //The same switch case is repeating below in another place.
            switch (columnIndex) {
                case 1:
                    hdnTracker = hdnAuthorPropagateTracker;
                    break;
                case 2:
                    hdnTracker = hdnApproverPropagateTracker;
                    break;
                case 3:
                    hdnTracker = hdnPublisherPropagateTracker;
                    break;
                case 4:
                    hdnTracker = hdnArchiverPropagateTracker;
                    break;
                case 5:
                    hdnTracker = HdnCustomNavEditorPropagateTracker;
                    break;
                case 6:
                    hdnTracker = hdnMasterImportPropagateTracker;
                    break;
            }

            if (isChecked) {
                //Remove the selection of None;
                selectedItem.SetValue(MENU_NONE, false, false);

                if (isPropagate) {
                    if (hdnTracker.value != "") {
                        if (!Contains(hdnTracker.value, objectId)) {
                            hdnTracker.value = hdnTracker.value + ":" + objectId;
                        }
                    }
                    else
                        hdnTracker.value = objectId;
                }
            }
            else {
                //Update the track column with the role id 
                //var oldTrackColumnValues = trackColumnvalue.split(':');
                var returnvalue = UpdateTrackColumn(trackColumnvalue, columnIndex);

                if (returnvalue != null) {
                    //Sets the new track value to the column.
                    selectedItem.SetValue(TRACK, returnvalue, false);  //recently changed.
                }

                //alert('New Trackvalue '+newTrackColumnvalue);

                if (hdnTracker.value != "") {
                    RemoveFromPropagateValue(objectId, columnIndex);
                    //If any one of the parents present in the list then remove that.
                    CheckAnyParentIsPropagated(selectedItem, columnIndex);
                }
            }

            //Check all the column under this.
            if (isPropagate)
                CheckAllChildrenOfNode(table, columnIndex, isChecked); //TODO: change the column index dynamically
        }
        //if(isPropagate)
        grdMenuPermissions.render();
        //alert(hdnTracker.value +" 1 ");
    }

    function UpdateTrackColumn(trackColvalue, newColumnIndex) {
        var newTrackColumnvalue = null;
        if (!Contains(trackColvalue, newColumnIndex)) {
            var oldTrackColumnValues = "";
            if (trackColvalue != null) {
                oldTrackColumnValues = trackColvalue.toString().split(':');
            }

            if (oldTrackColumnValues == "")
                return newColumnIndex;
            oldTrackColumnValues[oldTrackColumnValues.length] = newColumnIndex;
            newTrackColumnvalue = oldTrackColumnValues.join(':');

        }
        return newTrackColumnvalue;
    }

    function Contains(sourceValue, targetValue) {
        var returnStatus = false;
        if (sourceValue != null) {
            var splittedValues = sourceValue.toString().split(':');

            for (i = 0; i < splittedValues.length; i++) {
                if (splittedValues[i] == targetValue)
                    return true;
            }
        }
        return returnStatus;
    }

    function GetColumnIndex(element) {

        var columnIndex = null;

        var splittedIds = new Array();
        splittedIds = element.id.split('_');
        var columnName = splittedIds[splittedIds.length - 2];
        switch (columnName) {
            case 'Author':
                columnIndex = 1;
                break;
            case 'Approver':
                columnIndex = 2;
                break;
            case 'Publisher':
                columnIndex = 3;
                break;
            case 'Archiver':
                columnIndex = 4;
                break;
            case 'CUSTOMNAVEDITOR':
                columnIndex = 5;
                break;
            case 'MasterImport':
                columnIndex = 6;
                break;
            case 'None':
                columnIndex = 7;
                break;
            default:
                if (element.id == 'chkAuthor')
                    columnIndex = 1;
                else if (element.id == 'chkApprover')
                    columnIndex = 2;
                else if (element.id == 'chkPublisher')
                    columnIndex = 3;
                else if (element.id == 'chkArchiver')
                    columnIndex = 4;
                else if (element.id == 'chkNavEditor')
                    columnIndex = 5;
                else if (element.id == 'chkMasterImport')
                    columnIndex = 6;
                else if (element.id == 'chkNone')
                    columnIndex = 7;
                break;
        }

        return columnIndex;
    }

    function CheckAnyParentIsPropagated(currentRow, columnIndex) {
        var gridItem;
        var gridItemChild;
        var itemIndex = 0;
        var itemIndexChild = 0;
        var childTable;

        if (currentRow != null) {
            gridItem = currentRow.ParentItem;

            //alert(gridItem.Cells[1].Value);
            //while(gridItem = currentRow.ParentItem)
            {
                //childTable = gridItem.ChildTable;
                itemIndexChild = 0;
                if (gridItem != null) {
                    //alert(gridItem.Cells[1].Value);
                    CheckAnyParentIsPropagated(gridItem.ParentItem, columnIndex);
                    if (gridItem.getMember("Id").get_text().toLowerCase() == jAppId) gridItem.SetValue(columnIndex, false, false);
                    RemoveFromPropagateValue(gridItem.Cells[OBJECTID].Value, columnIndex);
                }
                itemIndex++;
            }
        }
    }

    //It removes the values from the propagate hidden value.
    function RemoveFromPropagateValue(objectId, columnIndex) {
        //alert('OldValue '+objectId);
        var hdnCtrl = null;

        var hdnAuthorPropagateTracker = document.getElementById("<%=HdnAuthorPropagateTrack.ClientID %>");
        var hdnApproverPropagateTracker = document.getElementById("<%=HdnApproverPropagateTrack.ClientID %>");
        var hdnPublisherPropagateTracker = document.getElementById("<%=HdnPublisherPropagateTrack.ClientID %>");
        var HdnCustomNavEditorPropagateTracker = document.getElementById("<%=HdnCustomNavEditorPropagateTrack.ClientID %>");
        var hdnArchiverPropagateTracker = document.getElementById("<%=HdnArchiverPropagateTrack.ClientID %>");
        var hdnMasterImportPropagateTracker = document.getElementById("<%=HdnMasterImportPropagateTrack.ClientID %>");

        var hdnTracker = null;

        switch (columnIndex) {
            case 1:
                hdnTracker = hdnAuthorPropagateTracker;
                break;
            case 2:
                hdnTracker = hdnApproverPropagateTracker;
                break;
            case 3:
                hdnTracker = hdnPublisherPropagateTracker;
                break;
            case 4:
                hdnTracker = hdnArchiverPropagateTracker;
                break;
            case 5:
                hdnTracker = HdnCustomNavEditorPropagateTracker;
                break;
            case 6:
                hdnTracker = hdnMasterImportPropagateTracker;
                break;
        }
        if (hdnTracker != null) {
            var oldValue = hdnTracker.value;
            var splittedValues = oldValue.toString().split(':');

            for (i = splittedValues.length - 1; i >= 0; i--) {
                if (splittedValues[i] == objectId) {
                    splittedValues.splice(i, 1);
                }
            }

            var newValue = splittedValues.join(':');
            hdnTracker.value = newValue;
        }
    }

    function CheckAllChildrenOfNode(table, columnIndex, isCheck) {
        var gridItem;
        var gridItemChild;
        var itemIndex = 0;
        var itemIndexChild = 0;
        var childTable;

        //If any of the node is unchecked through the parent node, then add that node into the changed role collection.
        //It is not yet implemented.
        if (table != null) {
            while (gridItem = table.GetRow(itemIndex)) {
                childTable = gridItem.ChildTable;
                itemIndexChild = 0;
                if (childTable != null) {
                    CheckAllChildrenOfNode(childTable, columnIndex, isCheck);
                }
                gridItem.SetValue(columnIndex, isCheck, isCheck);
                if (isCheck && columnIndex != MENU_NONE)
                    gridItem.SetValue(MENU_NONE, false, false);  //Sets the value to false for the None check box on that row.
                if (!isCheck) {
                    var newvalue = UpdateTrackColumn(gridItem.Cells[TRACK].Value, columnIndex);
                    gridItem.SetValue(TRACK, newvalue, false);
                }
                itemIndex++;
            }
        }
    }
</script>
<ComponentArt:Grid ID="grdMenuPermissions" EmptyGridText="<%$ Resources:GUIStrings, NoUsersGroupsSelected %>"
    SkinID="Hierarchical" RunningMode="Client" runat="server">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" HeadingCellCssClass="iapps-heading-cell"
            HeadingRowCssClass="iapps-heading-row" HeadingTextCssClass="iapps-heading-text"
            DataCellCssClass="iapps-data-cell" RowCssClass="iapps-row" SelectorCellWidth="1"
            SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false" HeadingCellHoverCssClass="iapps-heading-cell-hover"
            SelectedRowCssClass="iapps-selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
            SortImageWidth="8" SortImageHeight="7" AlternatingRowCssClass="iapps-alternate-row"
            ShowSelectorCells="false" AllowGrouping="false" HoverRowCssClass="iapps-hover-row">
            <Columns>
                <ComponentArt:GridColumn FixedWidth="true" DataField="Name" AllowEditing="False"
                    Width="553" AllowReordering="false" HeadingText="<%$ Resources:GUIStrings, Category %>" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Author" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, Author %>" AllowEditing="True" Width="82"
                    Align="Center" AllowReordering="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Approver" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, Approver %>" AllowEditing="True" Width="80"
                    Align="Center" AllowReordering="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Publisher" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, Publisher %>" AllowEditing="True" Width="80"
                    Align="Center" AllowReordering="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Archiver" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, Archive %>" AllowEditing="True" Width="82"
                    Align="Center" AllowReordering="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="CustomNavEditor" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, NavEditor %>" AllowEditing="True" Width="80"
                    Align="Center" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="MasterImport" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, MasterImport %>" AllowEditing="True" Width="82"
                    Align="Center" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="None" ColumnType="CheckBox"
                    HeadingText="<%$ Resources:GUIStrings, ClearAll %>" AllowEditing="True" Width="80"
                    Align="Center" AllowReordering="false" HeadingCellCssClass="iapps-last-heading-cell" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Id" Visible="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Track" Visible="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="GrpTrack" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ItemExpand EventHandler="grdMenuPermissions_onItemExpand" />
        <ItemCollapse EventHandler="grdMenuPermissions_onItemExpand" />
        <ItemCheckChange EventHandler="grdMenuPermissions_onItemCheckChange" />
        <ItemBeforeCheckChange EventHandler="grdMenuPermissions_onItemBeforeCheckChange" />
        <RenderComplete EventHandler="grdMenuPermissions_onRenderComplete" />
    </ClientEvents>
</ComponentArt:Grid>
<%--START: Hidden control to store the propagate nodes--%>
<asp:HiddenField ID="HdnAuthorPropagateTrack" Value="" runat="server" />
<asp:HiddenField ID="HdnApproverPropagateTrack" Value="" runat="server" />
<asp:HiddenField ID="HdnPublisherPropagateTrack" Value="" runat="server" />
<asp:HiddenField ID="HdnArchiverPropagateTrack" Value="" runat="server" />
<asp:HiddenField ID="HdnCustomNavEditorPropagateTrack" Value="" runat="server" />
<asp:HiddenField ID="HdnMasterImportPropagateTrack" Value="" runat="server" />
<%--END: Hidden control to store the propagate nodes--%>
