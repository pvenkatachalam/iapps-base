﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ManageAdminUser.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageAdminUser" %>
<script type="text/javascript">
    function grdAdminUser_OnLoad(sender, eventArgs) {
        $(".manage-users").gridActions({ objList: grdAdminUser });
    }

    function grdAdminUser_OnRenderComplete(sender, eventArgs) {
        $(".manage-users").gridActions("bindControls");

        var isiAppsUser = gridActionsJson.CustomFilter == "-1";
        $(".view-permissions").iAppsClickMenu({
            width: "250px",
            heading: isiAppsUser ?"<%= GUIStrings.Products %>" : "<%= GUIStrings.Permissions %>",
            create: function (e, menu) {
                var displayList = null;
                if (isiAppsUser)
                    displayList = AdminCallback.GetUserProducts(e.attr("objectId"));
                else
                    displayList = AdminCallback.GetUserPermission(e.attr("objectId"));
                if (displayList != null && displayList != '') {
                    var lstValues = displayList.split('#');
                    if (lstValues.length > 0) {
                        $.each(lstValues, function () {
                            if (this.toString() != "")
                                menu.addListItem(this.toString());
                        });
                    }
                }
            }
        });
    }

    function grdAdminUser_OnCallbackComplete(sender, eventArgs) {
        $(".manage-users").gridActions("displayMessage");
    }

    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;
        switch (gridActionsJson.Action) {
            case "Edit":
                doCallback = false;
                EditAdminUserDetails(selectedItemId);
                break;
            case "Add":
                doCallback = false;
                EditAdminUserDetails(null);
                break;
            case "Delete":
                doCallback = DeleteAdminUser(selectedItem, true);
                break;
            case "Deactivate":
                doCallback = DeleteAdminUser(selectedItem, false, false);
                break;
            case "Activate":
                doCallback = DeleteAdminUser(selectedItem, false, true);
                break;
            case "Permissions":
                doCallback = false;
                RedirectToAdminPage("ManageAdminUserPermissions",
                    stringformat("MemberType=1&MemberId={0}&MemberName={1}", selectedItemId, selectedItem.getMember("UserName").get_text()));
                break;
            case "AddToList":
                doCallback = false;
                AddManualUserItem(grdAdminUser);
                break;
        }

        if (typeof Custom_Grid_Callback == "function")
            doCallback = Custom_Grid_Callback(sender, eventArgs);

        if (doCallback) {
            grdAdminUser.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdAdminUser.callback();
        }
    }

    function EditAdminUserDetails(userId) {
        RedirectToAdminPage("ManageAdminUserDetails", userId != null ? "UserId=" + userId : "");
    }

    function DeleteAdminUser(selectedItem, deleteUser, activateUser) {
        var selectedUserId = selectedItem.getMember("UserId").get_text();
        var selectedUserName = selectedItem.getMember("UserName").get_text();
        var currentPermission = AdminCallback.GetUserPermission(selectedUserId);
        var memberType = 1;

        if (deleteUser)
            isConfirm = window.confirm(GetMessage("DeleteConfirmation"));
        else if (activateUser)
            isConfirm = window.confirm(GetMessage("ActivateConfirmation"));
        else
            isConfirm = window.confirm(GetMessage("DeactivateConfirmation"));

        if (isConfirm) {
            var count = 0;
            if (deleteUser || !activateUser)
                count = CheckUserWorklows(selectedUserId, memberType, selectedUserName, deleteUser ? currentPermission : null);

            if (count == -1) {
                var sitesString = GetSiteLists(currentPermission);
                alert(deleteUser ? GetMessage('DeleteUserDeniedCrossSite') : GetMessage('DeactivateUserDeniedCrossSite') + " " + sitesString);
            }
            else if (count == 1) {
                alert(deleteUser ? GetMessage('DeleteUserDeniedWorkflow') : GetMessage('DeactivateUserDeniedWorkflow'));
            }
            else if (count == 2) {
                //If any siteadmins are trying to delete the install admins, then it is not allowed.
                alert(deleteUser ? GetMessage('DeleteUserDeniedPermission') : GetMessage('DeleteUserDeniedPermission'));
            }
            else {
                var statusOfOperation = DeleteDeactivate(selectedUserId, selectedUserName, deleteUser ? 1 : (activateUser ? 4 : 2), null);
                if (statusOfOperation == "True")
                    return true;
                else
                    alert(statusOfOperation);
            }
        }

        return false;
    }

    function Grid_ItemSelect(sender, eventArgs) {
        var isEditable = sender.getItemByCommand("Edit").length > 0;
        if (isEditable) {
            ShowiAppsLoadingPanel();

            sender.getItemByCommand("Edit").hideButton();
            sender.getItemByCommand("Delete").hideButton();
            sender.getItemByCommand("Deactivate").hideButton();
            sender.getItemByCommand("Permissions").hideButton();
            if (typeof AdminCallback.CanEditUser == "function" && AdminCallback.CanEditUser(gridActionsJson.SelectedItems.first())) {
                sender.getItemByCommand("Edit").showButton();
                sender.getItemByCommand("Delete").showButton();
                sender.getItemByCommand("Deactivate").showButton();
                if (typeof AdminCallback.CheckCustomPermission == "function" && AdminCallback.CheckCustomPermission(gridActionsJson.SelectedItems.first()))
                    sender.getItemByCommand("Permissions").showButton();
            }

            //if IsLockedOut=1 then IsActivated is populating as 'true' else 'false'. And if IsLockedOut =0 then it is considering as active.
            //From begining onwards it was like this. it is tough to change now.
            if (eventArgs.selectedItem.getMember("IsActivated").get_text().toLowerCase() == "true") {
                sender.getItemByCommand("Deactivate").setText("<%= JSMessages.ActivateUser %>");
                sender.getItemByCommand("Deactivate").setComandArgument("Activate");
                sender.getItemByCommand("Deactivate").removeClass("deactivate");
                sender.getItemByCommand("Deactivate").addClass("activate");
            }
            else {
                sender.getItemByCommand("Deactivate").setText("<%= JSMessages.DeactivateUser %>");
                sender.getItemByCommand("Deactivate").setComandArgument("Deactivate");
                sender.getItemByCommand("Deactivate").removeClass("activate");
                sender.getItemByCommand("Deactivate").addClass("deactivate");
            }
            CloseiAppsLoadingPanel();
        }
    }
</script>
<div class="manage-users">
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="grdAdminUser" SkinID="Default" runat="server" Width="100%"
            RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdAdminUserLoadingPanelTemplate">
            <clientevents>
                <Load EventHandler="grdAdminUser_OnLoad" />
                <CallbackComplete EventHandler="grdAdminUser_OnCallbackComplete" />
                <RenderComplete EventHandler="grdAdminUser_OnRenderComplete" />
            </clientevents>
            <levels>
                <ComponentArt:GridLevel DataKeyField="UserId" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="UserName" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="LastName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="FirstName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="UserId" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsActivated" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </levels>
            <servertemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["UserId"]%>'>
                            <%--<div class="first-cell">
                                <%# GetGridItemRowNumber(Container.DataItem)%>
                            </div>--%>
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["UserName"]%>
                                </h4>
                                <p>
                                    <%# String.Format("{0}, {1}",  Container.DataItem["LastName"], Container.DataItem["FirstName"])%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["IsActivated"].ToString().ToLower() == "true" ? GUIStrings.InActive : GUIStrings.Active)%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<a objectId='{0}' class='view-permissions' href='#'>{1}</a>", 
                                    Container.DataItem["UserId"], gridActionsDTO.CustomFilter == "-1" ? GUIStrings.Products : GUIStrings.CurrentPermissions)%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="PopupTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item clear-fix">
                            <div class="first-cell">
                                <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                    value='<%# Container.DataItem["UserId"]%>' />
                                <h4>
                                    <%# Container.DataItem["UserName"]%></h4>
                            </div>
                            <div class="last-cell">
                                <%# String.Format("{0}, {1}", Container.DataItem["LastName"], Container.DataItem["FirstName"])%>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </servertemplates>
            <clienttemplates>
                <ComponentArt:ClientTemplate ID="grdAdminUserLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdAdminUser) ##
                </ComponentArt:ClientTemplate>
            </clienttemplates>
        </ComponentArt:Grid>
    </div>
</div>
