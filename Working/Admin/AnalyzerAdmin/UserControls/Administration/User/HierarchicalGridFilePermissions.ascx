<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.HierarchicalGridFilePermissions"
    CodeBehind="HierarchicalGridFilePermissions.ascx.cs" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<script type="text/javascript">
    function grdFilePermissions_onItemExpand(sender, eventArgs) {
        //var isExpanded = eventArgs.get_item().Expanded;
        //debugger;
        //alert(eventArgs.get_item().Data[0]);
        //eventArgs.get_item().Expanded = !isExpanded;  

        //    var gr = window[GetCurrentGridId()];
        //    gr.render();
    }
    var allAuthors = false;
    var allApprovers = false;
    var allPublishers = false;
    var allNone = false;
    var allArchive = false;
    var allEditNav = false;


    var allViewers = false;
    var allManagers = false;
    var allMasterImport = false;
    var allNone = false;

    var MEMBERNAME_OBJ = 0;
    var VIEWER = 1;
    var MANAGER = 2;
    var MASTERIMPORT = 3;
    var NONE_OBJ = 4;
    var MEMBERID_OBJ = 5;
    //var TRACK_OBJ = 8;


    function checkAllViewer(obj) {
        if (allViewers) {
            allViewers = false;
            UncheckAllItems_1(VIEWER);
        }
        else {

            var gr = window[GetCurrentGridId()];
            allViewers = true;
            CheckAllItems_1(VIEWER);
            UncheckAllItems(NONE_OBJ);
            document.getElementById("chkNone").checked = false;
        }

        //grid.render();
    }

    function checkAllMasterImport(obj) {
        if (allMasterImport) {
            allMasterImport = false;
            UncheckAllItems_1(MASTERIMPORT);
        }
        else {

            var gr = window[GetCurrentGridId()];
            allMasterImport = true;
            CheckAllItems_1(MASTERIMPORT);
            UncheckAllItems(NONE_OBJ);
            document.getElementById("chkNone").checked = false;
        }
    }


    function IsChecked_1() {
        if (event != null && event.srcElement != null) {
            return event.srcElement.checked;
        }
        else {
            return false;
        }
    }

    function checkAllManager(obj) {
        if (allManagers) {
            allManagers = false;
            UncheckAllItems_1(MANAGER);
        }
        else {
            allManagers = true;
            CheckAllItems_1(MANAGER);
            UncheckAllItems_1(NONE_OBJ);
            document.getElementById("chkNone").checked = false;
        }

        //grid.render();
    }

    function checkAllNone(obj) {
        if (allNone) {
            allNone = false;
            UncheckAllItems_1(NONE_OBJ);
        }
        else {
            allNone = true;
            CheckAllItems_1(NONE_OBJ);
            UncheckAllItems_1(VIEWER);
            UncheckAllItems_1(MANAGER);
            UncheckAllItems_1(MASTERIMPORT);
            document.getElementById("chkViewer").checked = false;
            document.getElementById("chkManager").checked = false;
            document.getElementById("chkMasterImport").checked = false;


        }

        //grid.render();
    }

    function CheckAllItems_1(columnno) {

        var gridId = GetCurrentGridId();
        var grid = window[gridId];

        if (grid != null) {
            //For all the levels all the nodes should be checked.
            var i = 0;
            //for(var i=0;i<grid.Levels.length;i++)
            {
                CheckAllChildrenOfNode_1(grid.get_table(i), columnno, true);
            }
        }
        //grdFilePermissions.Render(); 
    }

    function UncheckAllItems_1(columnno) {
        var gridId = GetCurrentGridId();
        var grid = window[gridId];

        if (grid != null) {
            //For all the levels all the nodes should be checked.

            var i = 0;
            //for(var i=0;i<grid.Levels.length;i++)
            {
                CheckAllChildrenOfNode_1(grid.get_table(i), columnno, false);
            }
        }

        //grdFilePermissions.Render(); 
    }
    //END

    var SelectedColumnIndex_1 = null;
    function BeforeCheckChanged_1(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();
        SelectedColumnIndex_1 = eventArgs.get_columnIndex();
    }



    function CheckedChanged_1(sender, eventArgs) 
    {
        debugger;
        var isPropagate = false;
        var columnIndex = SelectedColumnIndex_1;
        var selectedItem = eventArgs.get_item();
        var table = selectedItem.ChildTable;
        var isChecked = selectedItem.Data[columnIndex];
        var objectId = selectedItem.Cells[4].Value;

        if (!isChecked) 
        {
            //Uncheck the select All tab
            if (SelectedColumnIndex_1 != null) 
            {
                switch (SelectedColumnIndex_1) 
                {
                    case VIEWER:
                        //document.getElementById('chkViewer').checked = false;
                        allViewers = false;
                        break;
                    case MANAGER:
                        //document.getElementById('chkManager').checked = false;
                        allManagers = false;
                        break;
                    case MASTERIMPORT:
                        //document.getElementById('chkManager').checked = false;
                        allMasterImport = false;
                        break;
                    case NONE_OBJ:
                        //document.getElementById('chkNone').checked = false;
                        allNone = false;
                        break;
                    default:
                        break;
                }
            }
        }
        
        //    if( table != null)
        //    {
        //        var childTableRowCount = eventArgs.get_item().get_childTable().getRowCount();
        //        isPropagate = window.confirm(GetMessage('PropagateWarning'));
        //    }
        if (self["GetchkPropagateValue"])
            isPropagate = GetchkPropagateValue();

        //Propagate tracking hidden field for viewer column.
        var hdnPropagateTracker = document.getElementById("<%=HdnPropagateTrack.ClientID %>");

        //Propagate tracking hidden field for manager column.
        var hdnManagerPropagateTracker = document.getElementById("<%=HdnManagerPropagateTrack.ClientID %>");

        //Propagate tracking hidden field for MasterImport column.
        var hdnMasterImportPropagateTracker = document.getElementById("<%=HdnMasterImportPropagateTrack.ClientID %>");

        if (columnIndex == 4) 
        {

            var isNoneChecked = selectedItem.Data[4];

            //If the none check box is checked and propagate is true then remove the selection of all the check boxes in each level
            //In the case of file,images and content no need to consider the changed roles of the user.
            // Consider updating the propagate bit in the corresponding hidden fields.
            if (isNoneChecked) 
            {
                if (isPropagate) 
                {
                    CheckAllChildrenOfNode_1(table, 4, isNoneChecked);
                    if (isNoneChecked) 
                    {
                        CheckAllChildrenOfNode_1(table, VIEWER, false);
                        CheckAllChildrenOfNode_1(table, MANAGER, false);
                        CheckAllChildrenOfNode_1(table, MASTERIMPORT, false);
                    }
                    RemoveFromPropagateValue(objectId, NONE_OBJ);
             }
                //            //Get the cells that are checked.
                for (var j = 1; j < 4; j++) 
                {

                    //alert(selectedItem.Data[i].Value == true);
                    if (j != 4 && selectedItem.Data[j]) {
                        RemoveFromPropagateValue(objectId, j);
                        selectedItem.SetValue(j, false, false);
                    }
                }
            }
            else 
            {
                CheckAllChildrenOfNode_1(table, NONE_OBJ, false);
            }

            //        else //if the none is unchecked.
            //        {
            //        
            //        }
        }
        else 
        {
            var hdnTracker = null;

            if (columnIndex == 1) 
            {
                hdnTracker = hdnPropagateTracker;
            }
            else if (columnIndex == 2) {
                hdnTracker = hdnManagerPropagateTracker;
            }
            else if (columnIndex == 3) {
                hdnTracker = hdnMasterImportPropagateTracker;
            }

            if (isChecked) 
            {
                //Remove the selection of None;
                selectedItem.SetValue(4, false, false);
                if (isPropagate) 
                {
                    if (hdnTracker.value != "") 
                    {
                        if (!Contains_1(hdnTracker.value, selectedItem.Cells[5].Value)) 
                        {
                            hdnTracker.value = hdnTracker.value + ":" + selectedItem.Cells[5].Value;
                        }
                    }
                    else 
                    {
                        hdnTracker.value = selectedItem.Cells[5].Value;

                    }
                }
            }
            else 
            {
                if (hdnTracker.value != "") 
                {
                    RemoveFromPropagateValue_1(selectedItem.Cells[5].Value, columnIndex);
                    //If any one of the parents present in the list then remove that.
                    CheckAnyParentIsPropagated_1(selectedItem, columnIndex);
                }
            }

            //Check all the column under this.
            if (isPropagate)
                CheckAllChildrenOfNode_1(table, columnIndex, isChecked); //TODO: change the column index dynamically
        }

        sender.render();
    }

    function Contains_1(sourceValue, targetValue) {
        var returnStatus = false;
        var splittedValues = sourceValue.toString().split(':');

        for (i = 0; i < splittedValues.length; i++) {
            if (splittedValues[i] == targetValue)
                return true;
        }

        return returnStatus;
    }

    function GetColumnIndex_1(element) {
        var columnIndex = null;

        var splittedIds = new Array();
        splittedIds = element.id.split('_');
        var columnName = splittedIds[splittedIds.length - 2];
        switch (columnName) {
            case 'Viewer':
                columnIndex = 1;
                break;
            case 'Manager':
                columnIndex = 2;
                break;
            case 'MasterImport':
                columnIndex = 3;
                break;
            case 'None':
                columnIndex = 4;
                break;
            default:
                break;
        }

        return columnIndex;
    }

    function CheckAnyParentIsPropagated_1(currentRow, columnIndex) {
        var gridItem;
        var gridItemChild;
        var itemIndex = 0;
        var itemIndexChild = 0;
        var childTable;

        if (currentRow != null) {
            gridItem = currentRow.ParentItem;

            //alert(gridItem.Cells[1].Value);
            //while(gridItem = currentRow.ParentItem)
            {
                //childTable = gridItem.ChildTable;
                itemIndexChild = 0;
                if (gridItem != null) {
                    //alert(gridItem.Cells[1].Value);
                    CheckAnyParentIsPropagated_1(gridItem.ParentItem, columnIndex);
                    //gridItem.SetValue(columnIndex, false, false);
                    RemoveFromPropagateValue_1(gridItem.Cells[5].Value, columnIndex);
                }
                itemIndex++;
            }
        }
    }

    function RemoveFromPropagateValue_1(objectId, columnIndex) {
        //alert('OldValue '+objectId);
        var hdnCtrl = null;

        if (columnIndex == 1) {
            hdnCtrl = document.getElementById("<%=HdnPropagateTrack.ClientID %>");
        }
        else if (columnIndex == 2) {
            hdnCtrl = document.getElementById("<%=HdnManagerPropagateTrack.ClientID %>");
        }
        else if (columnIndex == 3) {
            hdnCtrl = document.getElementById("<%=HdnMasterImportPropagateTrack.ClientID %>");
        }


        var oldValue = hdnCtrl.value;
        var splittedValues = oldValue.toString().split(':');

        for (i = splittedValues.length - 1; i >= 0; i--) {
            if (splittedValues[i] == objectId) {
                splittedValues.splice(i, 1);
            }
        }

        var newValue = splittedValues.join(':');
        hdnCtrl.value = newValue;
    }

    function CheckAllChildrenOfNode_1(table, columnIndex, isCheck) {
        var gridItem;
        var gridItemChild;
        var itemIndex = 0;
        var itemIndexChild = 0;
        var childTable;
        if (table != null) {
            while (gridItem = table.GetRow(itemIndex)) {
                childTable = gridItem.ChildTable;
                itemIndexChild = 0;
                if (childTable != null) {
                    CheckAllChildrenOfNode_1(childTable, columnIndex, isCheck);
                }
                gridItem.SetValue(columnIndex, isCheck, isCheck);
                if (isCheck && columnIndex != NONE_OBJ)
                    gridItem.SetValue(NONE_OBJ, false, false);  //Sets the value to false for the None check box on that row.
                itemIndex++;
            }
        }
    }

</script>
<div>
    <ComponentArt:Grid ID="grdFilePermissions" EmptyGridText="<%$ Resources:GUIStrings, NoUsersGroupsSelected %>"
        SkinID="Hierarchical" RunningMode="Client" runat="server" TreeLineImageWidth="18"
        IndentCellWidth="18" DataAreaCssClass="hgDataAreaTable">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" HeadingCellCssClass="iapps-heading-cell" HeadingRowCssClass="iapps-heading-row"
                HeadingTextCssClass="iapps-heading-text" DataCellCssClass="iapps-data-cell" RowCssClass="iapps-row"
                SelectorCellWidth="1" SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false"
                HeadingCellHoverCssClass="iapps-heading-cell-hover" SelectedRowCssClass="iapps-selected-row" 
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7"
                AlternatingRowCssClass="iapps-alternate-row" ShowSelectorCells="false" AllowGrouping="false"
                HoverRowCssClass="iapps-hover-row">
                <Columns>
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Name"
                        AllowEditing="False" AllowReordering="false" Width="550" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Viewer" ColumnType="CheckBox"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, Viewer %>" AllowEditing="True"
                        Align="Center" Width="80" AllowReordering="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Manager" ColumnType="CheckBox"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, Manager %>" AllowEditing="True"
                        Align="Center" Width="80" AllowReordering="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="MasterImport" ColumnType="CheckBox"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, MasterImport %>" AllowEditing="True"
                        Align="Center" Width="80" AllowReordering="false" Visible="true" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="None" ColumnType="CheckBox"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, ClearAll %>" AllowEditing="True"
                        Align="Center" Width="80" HeadingCellCssClass="iapps-last-heading-cell" AllowReordering="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="ViewerPropagate" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="ManagerPropagate" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="MasterImportPropagate" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="NonePropagate" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="GrpTrack" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemExpand EventHandler="grdFilePermissions_onItemExpand" />
            <ItemCollapse EventHandler="grdFilePermissions_onItemExpand" />
            <ItemCheckChange EventHandler="CheckedChanged_1"></ItemCheckChange>
            <ItemBeforeCheckChange EventHandler="BeforeCheckChanged_1" />
        </ClientEvents>
    </ComponentArt:Grid>
    <%--START: Hidden control to store the propagate nodes--%>
    <asp:HiddenField ID="HdnPropagateTrack" Value="" runat="server" />
    <asp:HiddenField ID="HdnManagerPropagateTrack" Value="" runat="server" />
    <asp:HiddenField ID="HdnMasterImportPropagateTrack" Value="" runat="server" />
    <%--END: Hidden control to store the propagate nodes--%>
</div>
