﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HTMLEditor.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.HTMLEditor" %>
<style type="text/css">
    body > object {
        display: none;
    }
</style>
<script type="text/javascript" src="/admin/script/swfobject.js"></script>
<script type="text/javascript">
    var jTemplateType = '0';
    var parentHtml = '';
    var editorClosed = '';
    var adminSiteUrl = "../";

    var requestChars = '<asp:Localize runat="server" Text="<%$ Resources:JSMessages, RequestIsTooLargePleaseSelectLessThan0Characters %>" />';  //Created by Adams

    function ContentSave() {
        if (EditorToUse.toLowerCase() == 'ckeditor')
            oureditor = ckEditor;
        OnClientSubmitAdminContext(oureditor);
    }

    function ContentCancel() {
        if (EditorToUse.toLowerCase() == 'ckeditor')
            oureditor = ckEditor;
        OnClientCancelAdminContext(oureditor);
    }

    function SetTextHtml(objectClientId) {
        var radObject = document.getElementById(objectClientId);
        var returnString = set_html(parentHtml);
    }

    var editorTools;

    function OnClientLoad(editor) {
        oureditor = editor;
        if (editorClosed != 'true') {
            var returnString = ''; // ConvertToHtml(parentHtml); //SetHtml(parentHtml);
            if (editor != null && returnString != '') {
                if (window.parent.isContentUpdated == "1")
                    editor.set_html(returnString);
            }
            var sharepointText = editor.getToolByName("SharePoint").get_element();
            if (sharepointText != null) {
                var parentCustomTool = sharepointText.parentNode;
                sharepointText.style.display = 'none';
                if (parentCustomTool != null) {
                    parentCustomTool.style.width = 'auto';
                    parentCustomTool.innerHTML = '<span class="sharepoint-text">' + sharepointText.getAttribute('title') + '</span>';
                }
            }

            editorTools = editor.get_toolAdapter().get_tools();
            //setTimeout("PopulateSnippets(oureditor);", 250);
            var languagedropdown = oureditor.getToolByName("TranslateTool").get_element();
            languagedropdown.setAttribute("title", "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, PoweredByBing %>' />");
        }

        if (EditorToUse == 'RadEditor') {
            setTimeout('oureditor.repaint();', 1000);
        }

        if (typeof Editor_GetDefaultData == "function") {
            var htmlData = Editor_GetDefaultData();
            editor.set_html(htmlData);
        }
    }

    function OnClientSubmitAdminContext(editor) {

        var toCancel = confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, AreYouSureYouWantToSaveTheUpdatedText %>' />");
        if (!toCancel) return false;
        if (toCancel) {
            var htmlString;
            if (editor == null) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ErrorTheEditorRenderActionFailed %>' />");
                return false;
            }
            else {
                if (EditorToUse.toLowerCase() == 'ckeditor')
                    htmlString = editor.getData();
                else
                    htmlString = editor.get_html(true);
            }
            var returnString = GetHtml(htmlString);
            window.parent.SetXML(returnString);

            //Auto save and close row if popup called from Content Library Admin

            if (window.parent.location.pathname.indexOf("/Libraries/Data/ManageContentLibrary.aspx") >= 0) {
                window.parent.gridContent_BeforeUpdate(true);
            }

            CloseDialog();
        }
    }
    function OnClientCancelAdminContext(editor) {
        var toCancel = confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, AreYouSureYouWantToCancel %>' />");
        if (!toCancel) return false;
        if (toCancel) {
            CloseDialog();
            return false;
        }
    }
    function CloseDialog() {
        window.parent.CloseEditor = 'true';
        parent.contentDefinitionPopup.hide();
    }

    if (EditorToUse.toLowerCase() == 'ckeditor') {
        //setting the editor global variable when the editor initializes
        $(document).ready(function () {
            CKEDITOR.on('instanceReady', function (ev) {
                ckEditor = ev.editor;
                fn_AddSharepointText();
                var returnString = ConvertToHtml(parentHtml);
                if (ckEditor != null) {
                    if (returnString != '' && window.parent.isContentUpdated == "1")
                        ckEditor.setData(returnString);

                    if (typeof Editor_GetDefaultData == "function") {
                        var htmlData = Editor_GetDefaultData();
                        ckEditor.setData(htmlData);
                    }
                }
            });
            //setTimeout("$('.cke_hc').removeClass('cke_hc');", 500);
        });
    }

    function GetEditorContent() {
        var editor = null;
        if (EditorToUse.toLowerCase() == 'ckeditor')
            editor = ckEditor;
        else
            editor = $find("<%=RadEditor1.ClientID%>");

        var htmlString;

        if (editor != null) {
            if (EditorToUse.toLowerCase() == 'ckeditor')
                htmlString = editor.getData();
            else
                htmlString = editor.get_html(true);
        }

        return htmlString;
    }
</script>
<radE:RadEditor ID="RadEditor1" runat="server" OnClientLoad="OnClientLoad" EditModes="Design,Html" 
    DictionaryPath="~/App_Data/RadSpell" OnClientCommandExecuted="OnClientCommandExecuted" OnClientModeChange="OnClientModeChange"
    NewLineMode="P" AllowScripts="true" Width="960" Height="520" DialogsCssFile="~/iAPPS_Editor/radeditor_dialog.css" 
    EnableResize="false" StripFormattingOptions="MSWordRemoveAll" 
    ContentFilters="ConvertCharactersToEntities,FixUlBoldItalic,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent">
    <Content>&nbsp;</Content>
</radE:RadEditor>
<CKEditor:CKEditorControl ID="ckEditor" runat="server" AutoGrowMinHeight="200" EnterMode="P"
    BasePath="~/iAPPS_Editor" Width="960" Height="400" Entities="true" HtmlEncodeOutput="true"
    IgnoreEmptyParagraph="true" BrowserContextMenuOnCtrl="true" EntitiesGreek="true"
    ScaytAutoStartup="false" ShiftEnterMode="BR" TemplatesReplaceContent="false"
    ForceSimpleAmpersand="true" ToolbarCanCollapse="false">
</CKEditor:CKEditorControl>
<asp:PlaceHolder runat="server" ID="phScriptTags" />
