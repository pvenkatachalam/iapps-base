﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="General" Title="CustomErrorPage" Inherits="Bridgeline.iAPPS.Admin.Common.Web.CustomErrorPage"  Codebehind="CustomErrorPage.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <div style="position: relative; min-height: 100%;">
        <form id="frmMain" runat="server">
        <!-- Header Starts -->
        <div class="header-wrapper" id="headerWrapper">
            <div class="product-nav-wrapper">
                <div class="product-nav clear-fix">
                </div>
            </div>
            <div class="site-utility clear-fix">
            </div>
        </div>
        <!-- Header Ends -->
        <div class="wrapper clear-fix" id="wrapper">
            <div class="breadcrumb clear-fix">
            </div>
            <!-- Content Section Starts -->
            <div class="contentSection clear-fix" runat="server" id="contentSection" style="padding-top: 100px;">
                <h1 style="background: url('/Admin/App_Themes/General/images/smiley_error.png') no-repeat left top;
                    padding: 50px 0 150px 250px; font-size: 30px;">
                    Hmmmm, we're having trouble loading the page.</h1>
                <div class="clear-fix">
                    <div class="left-col" style="float: left; width: 300px; margin: 0 50px 0 250px;">
                        <p style="margin-bottom: 20px;">
                            <strong>We hate to say but you've managed to find an error. We've sent an email to the
                                system administrator.</strong></p>
                        <ul style="list-style-type: square; margin-left: 15px;">
                            <li style="margin-bottom: 10px;">If you typed in the address, try double-checking the spelling.</li>
                            <li style="margin-bottom: 10px;"><asp:Localize ID="UnhandledException" runat="server"/></li>
                        </ul>
                    </div>
                    <div class="right-col" style="float: left; width: 300px;">
                        <p style="margin-bottom: 20px;">
                            <strong>If you're really stuck, here are some other options:</strong></p>
                        <ul style="list-style-type: square; margin-left: 15px;">
                            <li style="margin-bottom: 10px;">
                                <asp:HyperLink ID="gotoControlCenter" runat="server" Text="Click here" NavigateUrl="javascript:redirectToHome();" />
                                to go back to the Control Center.</li>
                            <li style="margin-bottom: 10px;">Learn more about <a href="http://www.bridgelinedigital.com/website-management"
                                target="_blank">iAPPS</a>.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Starts -->
        <div class="footerWrapper" id="footerWrapper">
            <div class="footer" id="footer">
                <div class="leftFooter">
                </div>
                <div class="rightFooter">
                    <asp:Image ImageUrl="~/App_Themes/General/images/bridgeline-logo.png" ID="companyLogo" runat="server"
                        ToolTip="Bridgeline Digital" AlternateText="Bridgeline Digital" />
                </div>
            </div>
        </div>
        </form>
    </div>
</body>
</html>
