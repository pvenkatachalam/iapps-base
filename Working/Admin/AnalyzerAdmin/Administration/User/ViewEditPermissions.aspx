<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ViewEditPermissions" CodeBehind="ViewEditPermissions.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="MenuPermissions" Src="~/UserControls/Administration/User/HierarchicalGridMenusPermissions.ascx" %>
<%@ Register TagPrefix="UC" TagName="FilePermissions" Src="~/UserControls/Administration/User/HierarchicalGridFilePermissions.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function GetchkPropagateValue() {
            return $('#<%=chkPropagate.ClientID %>').is(':checked');
        }
        function togglePermissionType() {
            var selTab = parseInt($("#ddlTarget").val());
            for (var i = 1; i <= 4; i++) {
                if (i != selTab)
                    $("#tab" + i).hide();
                else
                    $("#tab" + i).show();
            }
        }
        $(function () {
            togglePermissionType();
            $(".page-header").iAppsFixHeader();

            $("#<%=hplMemberDetails.ClientID%>").click(function () {
                if ($(this).text() == "<%=GUIStrings.ViewCMSUserProfile %>")
                    $("#memberDetails").iAppsDialog("open");
                else
                    $("#groupDetails").iAppsDialog("open");
                return false;
            });

            $('#close').on('click', function () {
                $("#memberDetails").iAppsDialog("close");
                return false;
            });

            $('#groupClose').on('click', function () {
                $("#groupDetails").iAppsDialog("close");
                return false;
            });

            
        });
        function HIdePopup() {
        }
    </script>
</asp:Content>
<asp:Content ID="content" runat="server" ContentPlaceHolderID="cphContent">
    <asp:Literal ID="ltrObjTitle" runat="server" />
    <asp:UpdatePanel runat="server" ID="upLibraryPermissions">
        <ContentTemplate>
            <div class="grid-utility clear-fix">
                <div class="form-column">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SelectLibrary %>" />
                    </label>
                    <asp:DropDownList runat="server" ID="ddlTarget" ClientIDMode="Static" Width="160"
                        AutoPostBack="true" EnableViewState="true">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Menus %>" Value="1" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Files %>" Value="2" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Images %>" Value="3" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Content %>" Value="4" />
                    </asp:DropDownList>
                    <asp:CheckBox ID="chkPropagate" runat="server" CssClass="check-box" Checked="True"
                        Text="<%$ Resources:GUIStrings, PropagatePermissions %>" />
                </div>
                <div class="form-right-column">
                    <asp:HyperLink ID="hplMemberDetails" runat="server" NavigateUrl="#" CssClass="hyperlink" />
                    <img src="/Admin/App_Themes/General/images/separator.png" alt=" | " />
                    <a href="#" onclick="OpeniAppsAdminPopup('ViewGroupDefinitions');">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ViewGroupDefinitions %>" /></a>                  
                </div>
            </div>
            <div id="tab1" runat="server">
                <UC:MenuPermissions ID="menuPermissions" runat="server" />
            </div>
            <div id="tab2" runat="server" style="display: none;">
                <UC:FilePermissions ID="filePermissions" runat="server" />
            </div>
            <div id="tab3" runat="server" style="display: none;">
                <UC:FilePermissions ID="imagePermissions" runat="server" />
            </div>
            <div id="tab4" runat="server" style="display: none;">
                <UC:FilePermissions ID="contentPermissions" runat="server" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="iapps-modal clear-fix user-profile" id="memberDetails" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.CMSUserProfile %></h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="profile-properties">
                <div class="picture-column">
                    <asp:Image runat="server" ID="imgImageURL" ImageUrl="~/App_Themes/General/images/no_photo.jpg"
                        ClientIDMode="Static" />
                </div>
                <div class="properties-column">
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.FirstName %>:</label>
                        <div class="form-value text-value">
                            <asp:Literal ID="litFirstName" runat="server" /></div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.LastName %>:</label>
                        <div class="form-value text-value">
                            <asp:Literal ID="litLastName" runat="server" /></div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.Email %>:</label>
                        <div class="form-value text-value">
                            <asp:Literal ID="litEmail" runat="server" /></div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.UserName %>:</label>
                        <div class="form-value text-value">
                            <asp:Literal ID="litUserName" runat="server" /></div>
                    </div>
                </div>
            </div>
            <div class="group-permission">
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.CurrentSitesAccess %></label>
                    <div class="form-value">
                        <asp:ListBox ID="lstPermissions" runat="server" Width="500" Height="150" />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <a href="javascript: //" id="close" class="button">
                <%= GUIStrings.Close %>
            </a>
        </div>
    </div>
    <div class="iapps-modal clear-fix" id="groupDetails" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.CMSGroupProfile %></h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.GroupNameColon %>:</label>
                <div class="form-value text-value">
                    <asp:Literal ID="litGroupName" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.DescriptionColon %>:</label>
                <div class="form-value text-value">
                    <asp:Literal ID="litGroupDescription" runat="server" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="javascript: //" id="groupClose" class="button">
                <%= GUIStrings.Close %></a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="buttons" runat="server" ContentPlaceHolderID="cphHeaderButtons">
    <div class="page-header clear-fix">
        <h1>
            <%= GUIStrings.ManagePermissions %></h1>
        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
        <asp:HyperLink ID="hplCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    </div>
</asp:Content>
