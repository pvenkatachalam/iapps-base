﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageAdminUser"
    MasterPageFile="~/MasterPage.master" StylesheetTheme="General" CodeBehind="ManageAdminUser.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="User" Src="~/UserControls/Administration/User/ManageAdminUser.ascx" %>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">  
    <UC:User ID="ctlManageAdminUser" runat="server" />
</asp:Content>
