﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageAdminGroupDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageAdminGroupDetails" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="EditAdminGroupDetails" Src="~/UserControls/Administration/User/ManageAdminGroupDetails.ascx" %>
<asp:Content ID="header" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" OnClick="btnSaveUser_Click"
            cssclass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"  OnClientClick="return ValidateEditGroup();" />
        <asp:Button ID="btnSaveAndPermission" runat="server" Text="<%$ Resources:GUIStrings, SaveAndEditPermission %>" OnClick="btnSaveAndEditPermission_Click"
            cssclass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAndEditPermission %>"  OnClientClick="return ValidateEditGroup();" />
        <a href="ManageAdminGroup.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">  
    <UC:EditAdminGroupDetails ID="editAdminGroupDetail" runat="server" />
</asp:Content>
