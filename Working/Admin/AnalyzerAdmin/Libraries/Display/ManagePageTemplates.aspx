<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManagePageTemplates"
    MasterPageFile="~/MasterPage.master" StylesheetTheme="General" CodeBehind="ManagePageTemplates.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="Library" Src="~/UserControls/Libraries/Display/ManagePageTemplates.ascx" %>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><asp:Literal ID="ltPageHeading" runat="server" Text="<%$ Resources:GUIStrings, PageTemplates %>" /></h1>
    <UC:Library ID="managePageTemplates" runat="server"></UC:Library>
</asp:Content>
