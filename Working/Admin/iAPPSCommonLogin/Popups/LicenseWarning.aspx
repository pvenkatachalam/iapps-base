﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.Popups.LicenseWarning" Theme="General"
    CodeBehind="LicenseWarning.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, iAPPSMarketierLicense %>"/></title>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="iapps-modal licenseWarning">
        <div class="modal-header clear-fix">
            <h2><asp:localize ID="Localize2" runat="server" text="<%$ Resources:GUIStrings, Warning %>"/></h2>
        </div>
        <div class="modal-content clear-fix">
            <asp:PlaceHolder ID="pldContentManager" runat="server" Visible="false">
                <div class="warningLeftColumn">
                    <asp:Image ID="imgCMS" runat="server" ImageUrl="~/App_Themes/General/images/cm-logo.png"
                        AlternateText="<%$ Resources:GUIStrings, iAPPSContentManager %>" />
                </div>
                <div class="warningRightColumn">
                    <p><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, ContentManagerUpsellMessage %>" /></p>
                    <p><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, AccessToContentManagerRequiresALicense %>" /></p>
                    <p style="height: 40px;">&nbsp;</p>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="pldAnalytics" runat="server" Visible="false">
                <div class="warningLeftColumn">
                    <asp:Image ID="imgAnalytics" runat="server" ImageUrl="~/App_Themes/General/images/analytics-logo.png"
                        AlternateText="<%$ Resources:GUIStrings, iAPPSAnalytics %>" />
                </div>
                <div class="warningRightColumn">
                    <p><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, AnalyticsUpsellMessage %>" /></p>
                    <p><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, AccessToAnalyticsRequiresALicense %>" /></p>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="pldMarketier" runat="server" Visible="false">
                <div class="warningLeftColumn">
                    <asp:Image ID="imgMarketier" runat="server" ImageUrl="~/App_Themes/General/images/marketier-logo.png"
                        AlternateText="<%$ Resources:GUIStrings, iAPPSMarketier %>" />
                </div>
                <div class="warningRightColumn">
                    <p><asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, MarketierUpsellMessage %>" /></p>
                    <p><asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, AccessToMarketierRequiresALicense %>" /></p>
                    <p style="height: 50px;">&nbsp;</p>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="pldCommerce" runat="server" Visible="false">
                <div class="warningLeftColumn">
                    <asp:Image ID="imgCommerce" runat="server" ImageUrl="~/App_Themes/General/images/commerce-logo.png"
                        AlternateText="<%$ Resources:GUIStrings, iAPPSCommerce %>" />
                </div>
                <div class="warningRightColumn">
                    <p><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, CommerceUpsellMessage %>" /></p>
                    <p><asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, AccessToCommerceRequiresALicense %>" /></p>
                    <p style="height: 70px;">&nbsp;</p>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="pldSocial" runat="server" Visible="false">
                <div class="warningLeftColumn">
                    <asp:Image ID="imgSocial" runat="server" ImageUrl="~/App_Themes/General/images/social-logo.png"
                        AlternateText="<%$ Resources:GUIStrings, iAPPSCommerce %>" />
                </div>
                <div class="warningRightColumn">
                    <p><asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, SocialUpsellMessage %>" /></p>
                    <p><asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, AccessToSocialRequiresALicense %>" /></p>
                    <p style="height: 70px;">&nbsp;</p>
                </div>
            </asp:PlaceHolder>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" cssclass="button" ToolTip="<%$ Resources:GUIStrings, Close %>"
              OnClientClick="return CanceliAppsAdminPopup();"  />
        </div>
    </div>
    </form>
</body>
</html>
