<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.ForgotPassword" 
    StylesheetTheme="General" Codebehind="ForgotPassword.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, iAPPSContentManagerForgotPassword %>" /></title>
    <meta name="robots" content="noindex, nofollow" />
    <link rel="shortcut icon" href="/commonlogin/favicon.ico" />
    <script type="text/javascript" src="../common/js/JSMessages.js.aspx"></script>    
    <script type="text/javascript" src="../script/General.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:PasswordRecovery ID="forgotPassword" runat="server" UserNameFailureText="<%$ Resources:JSMessages, InvalidUserName %>"
            GeneralFailureText="<%$ Resources:JSMessages, ErrorRetrevingPassword %>"
            RenderOuterTable="true">
            <UserNameTemplate>
                <div class="iapps-modal" style="width: 320px">
                    <div class="modal-header">
                        <h2><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ForgotYourPassword %>" /></h2>
                    </div>
                    <div class="modal-content">
                        <div class="form-row">
                            <p><asp:Localize runat="server" /></p>
                        </div>
                        <div class="form-row">
                            <asp:Label AssociatedControlID="UserName" ID="userNameLabel" runat="server" Text="<%$ Resources:GUIStrings, EnterYourUserName %>"></asp:Label>
                        </div>
                        <div class="form-row">
                            <asp:TextBox ID="UserName" runat="server" CssClass="textBoxes" Width="290"></asp:TextBox>
                        </div>
                        <div class="form-row" style="color: #ff0000;">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False">&nbsp;</asp:Literal>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button" 
                            CausesValidation="false" OnClientClick="parent.forgotPasswordWindow.hide(); return false;" />
                        <asp:Button ID="SubmitButton" runat="server" CommandName="Submit" CssClass="primarybutton"
                            Text="<%$ Resources:GUIStrings, RetrievePassword %>" ValidationGroup="forgotPassword" ToolTip="<%$ Resources:GUIStrings, RetrievePassword %>" />
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" Display="None"
                            ErrorMessage="<%$ Resources:GUIStrings, UserNameIsRequired %>" ToolTip="<%$ Resources:GUIStrings, UserNameIsRequired %>" ValidationGroup="forgotPassword" Text=" "></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="InvalidUserNameChars" runat="server" ControlToValidate="UserName" Display="None"
                            ErrorMessage="<%$ Resources:JSMessages, InvalidInput %>" SetFocusOnError="True"
                            ValidationExpression="^[^<>]+$" ValidationGroup="forgotPassword">&nbsp;</asp:RegularExpressionValidator>
                        <asp:ValidationSummary ID="summaryValidation" runat="Server" EnableClientScript="true" ShowMessageBox="true" ValidationGroup="forgotPassword"
                            ShowSummary="false" DisplayMode="List" />
                    </div>
                </div>
            </UserNameTemplate>
            <SuccessTemplate>
                <div class="iapps-modal" style="width: 320px" runat="server" id="iappsmodal">
                    <div class="modal-header" runat="server" id="modalheader">
                        <h2 runat="server" id="modalh2"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ForgotYourPassword %>" /></h2>
                    </div>
                    <div class="modal-content" runat="server" id="modalcontent">
                        <div class="form-row" runat="server" id="formrow">
                            <p runat="server" id="modalp"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:JSMessages, PasswordRetreived %>" /></p>
                        </div>
                    </div>
                    <div class="modal-footer" runat="server" id="modalfooter">
                        <input type="button" value="<%$ Resources:GUIStrings, Close %>" class="button" onclick="parent.forgotPasswordWindow.hide();return false;"
                            runat="server" id="modalbutton" />
                    </div>
                </div>
            </SuccessTemplate>
        </asp:PasswordRecovery>
    </form>
</body>
</html>