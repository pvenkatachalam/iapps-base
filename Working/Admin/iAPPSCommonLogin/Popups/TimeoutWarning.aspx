﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.Popups.TimeoutWarning" StylesheetTheme="General"
    CodeBehind="TimeoutWarning.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, iAPPSSessionTimeoutWarning %>" /></title>
    <script type="text/javascript" src="../common/js/JSMessages.js.aspx"></script>    
    <script type="text/javascript" src="../script/General.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            StartTimeoutCounter();
        });
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="iapps-modal" style="width:300px;z-index: 99999;">
        <div class="modal-header">
            <h2><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Warning %>" /></h2>
        </div>
        <div class="modal-content">
            <p id="spnTimeoutMsg"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YourSessionWillTimeoutIn60DoYouWishToResetYourSessionOrLogOut %>" /></p>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnLogout" runat="server" Text="<%$ Resources:GUIStrings, LogOut %>" CssClass="button"
                ToolTip="<%$ Resources:GUIStrings, LogOut %>" OnClientClick="LogoutUser();return false;" />
            <asp:Button ID="btnReset" runat="server" Text="<%$ Resources:GUIStrings, Reset %>" CssClass="primarybutton" 
                ToolTip="<%$ Resources:GUIStrings, Reset %>" OnClientClick="resetUserSession(this);return false;" />
        </div>
    </div>
    </form>
</body>
</html>
