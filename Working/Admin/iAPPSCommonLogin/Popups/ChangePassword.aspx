<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.ChangePassword"
    StylesheetTheme="General" CodeBehind="ChangePassword.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, iAPPSContentManagerForgotPassword %>" /></title>
    <meta name="robots" content="noindex, nofollow" />
    <link rel="shortcut icon" href="/commonlogin/favicon.ico" />
    <script type="text/javascript" src="../common/js/JSMessages.js.aspx"></script>
    <script type="text/javascript" src="../script/General.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
        function attemptReisize() {
            this.parent.changePasswordWindow.FitToContents();
        }

    </script>
        <asp:Panel runat="server" ID="changePassword" DefaultButton="ChangePasswordPushButton" >
            <div class="iapps-modal" style="width: 320px">
                <div class="modal-header">
                    <h2>
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ChangePassword %>" /></h2>
                </div>
                <div class="modal-content">
                    <div class="form-row">
                          <asp:ValidationSummary ID="summaryValidation" ValidationGroup="changePassword" runat="Server" EnableClientScript="true" 
                        ShowMessageBox="false" ShowSummary="true" DisplayMode="BulletList"  />
                        <asp:Label ID="lblPassword" runat="server" CssClass="form-label" AssociatedControlID="CurrentPassword"><span class="req">*</span><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, CurrentPassword %>" /></asp:Label>
                        <div class="form-value">
                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="textBoxes" TextMode="Password" Width="290"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <asp:Label ID="lblNewPassword" runat="server" CssClass="form-label" AssociatedControlID="NewPassword"><span class="req">*</span><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, NewPassword %>" /></asp:Label>
                        <div class="form-value">
                            <asp:TextBox ID="NewPassword" runat="server" CssClass="textBoxes" TextMode="Password" Width="290"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <asp:Label ID="lblConfirm" runat="server" CssClass="form-label" AssociatedControlID="ConfirmNewPassword"><span class="req">*</span><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, ConfirmNewPassword %>" /></asp:Label>
                        <div class="form-value">
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="textBoxes" TextMode="Password" Width="290"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </label>
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button"
                        CausesValidation="false" OnClientClick="parent.changePasswordWindow.hide(); return false;" />
                    <asp:Button ID="ChangePasswordPushButton" CausesValidation="true" runat="server" OnClientClick="attemptReisize();" OnClick="ChangePasswordPushButton_Click" CssClass="primarybutton"
                        Text="<%$ Resources:GUIStrings, ChangePassword %>" ValidationGroup="changePassword" ToolTip="<%$ Resources:GUIStrings, ChangePassword %>" />
                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" Display="None"
                        ErrorMessage="<%$ Resources: JSMessages, CurrentPasswordRequired %>" ToolTip="<%$ Resources: JSMessages, CurrentPasswordRequired %>"
                        ValidationGroup="changePassword" Text=" "></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" Display="None"
                        Text=" " ErrorMessage="<%$ Resources: JSMessages, NewPasswordRequired %>" ToolTip="<%$ Resources: JSMessages, NewPasswordRequired %>"
                        ValidationGroup="changePassword"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" Display="None"
                        ErrorMessage="<%$ Resources: JSMessages, ConfirmNewPasswordRequired %>" ToolTip="<%$ Resources: JSMessages, ConfirmNewPasswordRequired %>"
                        Text=" " ValidationGroup="changePassword"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="InvalidNewPasswordChars" runat="server" ControlToValidate="NewPassword" Display="None"
                        ErrorMessage="<%$ Resources: JSMessages, InvalidInput %>" SetFocusOnError="True"
                        ValidationExpression="^[^<>]+$" ValidationGroup="changePassword">&nbsp;</asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="InvalidConfirmNewPasswordChars" runat="server" Display="None"
                        ControlToValidate="ConfirmNewPassword" ErrorMessage="<%$ Resources: JSMessages,  InvalidInput %>"
                        SetFocusOnError="True" ValidationExpression="^[^<>]+$" ValidationGroup="changePassword">&nbsp;</asp:RegularExpressionValidator>
                    <asp:CustomValidator ID="invalidCurrentPassword" ClientValidationFunction="customvalidator" ControlToValidate="CurrentPassword" Text=" " runat="server" Display="None" ValidationGroup="changePassword" OnServerValidate="invalidCurrentPassword_ServerValidate" ErrorMessage="Current password is incorrect"></asp:CustomValidator>
                    <asp:CustomValidator ID="passwordMatchesLastPassword" ClientValidationFunction="customvalidator" ControlToValidate="NewPassword" Text=" " runat="server" Display="None" ValidationGroup="changePassword" OnServerValidate="passwordMatchesLastPassword_ServerValidate" ErrorMessage="New password must not match previous X passwords"></asp:CustomValidator>
                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" Display="None"
                        Text=" " ControlToValidate="ConfirmNewPassword" ErrorMessage="<%$ Resources: JSMessages, PasswordMismatch %>"
                        ValidationGroup="changePassword"></asp:CompareValidator>                  
                </div>
            </div>
        </asp:Panel>
        <asp:PlaceHolder runat="server" ID="success" Visible="false">
            <div class="iapps-modal" style="width: 320px" runat="server" id="iappsmodal">
                <div class="modal-header" runat="server" id="modalheader">
                    <h2 runat="server" id="modalh2">
                           <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ChangePassword %>" /></h2>
                </div>
                <div class="modal-content">
                    <div class="form-row">
                        <label class="form-label"><%= GUIStrings.YourPasswordHasBeenChangedSuccessfully %></label>
                    </div>

                </div>
                <div class="modal-footer">
                    <asp:Button ID="button" Text="<%$ Resources:GUIStrings, Continue %>" ToolTip="<%$ Resources:GUIStrings, Continue %>"
                        CssClass="primarybutton" OnClientClick="parent.changePasswordWindow.hide(); return false;" runat="server" />
                </div>
            </div>
        </asp:PlaceHolder>

    </form>
</body>
</html>
