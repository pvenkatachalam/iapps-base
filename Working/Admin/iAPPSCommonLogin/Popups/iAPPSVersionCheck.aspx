﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iAPPSVersionCheck.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.Popups.iAPPSVersionCheck"
    StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, iAPPSProductSuiteVersionCheck %>" /></title>
    <script type="text/javascript" src="../common/js/JSMessages.js.aspx"></script>    
    <script type="text/javascript" src="../script/General.js"></script>
    <script type="text/javascript">
        function ClosePopup() {
            parent.versionCheckWindow.hide();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="iapps-modal" style="width:300px;">
        <div class="modal-header">
            <h2><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, iAPPSUpdates %>" /></h2>
        </div>
        <div class="modal-content">
            <asp:PlaceHolder ID="phNoUpdates" runat="server" Visible="false">
            <p>
                <asp:Literal ID="ltNoUpdates" runat="server"></asp:Literal>
            </p>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phUpdatesAvailable" runat="server">
            <p>
                <asp:Literal ID="ltNewVersion" runat="server"></asp:Literal><asp:Literal ID="ltCurrentVersion" runat="server"></asp:Literal>  
            </p>
            </asp:PlaceHolder>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button"
                ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup();" />
        </div>
    </div>
    </form>
</body>
</html>
