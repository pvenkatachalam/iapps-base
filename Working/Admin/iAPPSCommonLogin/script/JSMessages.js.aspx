﻿<%@ Page Language="C#"  ContentType="text/javascript" %>

var __JSMessages = {
    "DHTMLWindow" : "<%= JSMessages.DHTMLWindow %>",
    "Minimize" : "<%= JSMessages.Minimize %>",
    "Close" : "<%= JSMessages.Close %>",
    "AnErrorHasOccuredSomwhereInsideYouroncloseEventHandler" : "<%= JSMessages.AnErrorHasOccuredSomwhereInsideYourOncloseEventHandler %>",
    "HideBar" : "<%= JSMessages.HideBar %>",
    "pagetree" : "<%= JSMessages.PageTree %>",
    "SiteStructure" : "<%= JSMessages.SiteStructure %>",
    "ExpandTree" : "<%= JSMessages.ExpandTree %>",
    "CollapseTree" : "<%= JSMessages.CollapseTree %>",
    "TypeHereToFilterResults" : "<%= JSMessages.TypeHereToFilterResults %>",
    "Search" : "<%= JSMessages.Search %>",
    "PleaseSelectASite" : "<%= JSMessages.PleaseSelectASite %>",
    "PleaseSelectAGroup" : "<%= JSMessages.PleaseSelectAGroup %>",
    "CacheReset" : "<%= JSMessages.CacheReset %>",
    "LicenseWarning" : "<%= JSMessages.LicenseWarning %>",
    "SessionTimeoutWarning" : "<%= JSMessages.SessionTimeoutWarning %>",
    "PleaseWaitWhileYourSessionIsBeingReset" : "<%= JSMessages.PleaseWaitWhileYourSessionIsBeingReset %>",
    "iAPPSVersionCheck" : "<%= JSMessages.iAPPSVersionCheck %>"
}