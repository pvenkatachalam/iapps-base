﻿//Global variables for toolbar and inline editing
var editable = true;
var autohide = false;
var varPopMenu = false;
var objDoc;
var oureditor;

/** Browser Detection Code **/
var detect = navigator.userAgent.toLowerCase();
var OS,browser,version,total,thestring, isIE6=false;
var browserPlatform = navigator.platform.toLowerCase();
var macBrowser = navigator.userAgent.toLowerCase();

if (checkIt('safari')) browser = "safari";
else if (checkIt('msie'))
{
    browser = "ie";
    if(checkIt('msie 6.0'))
    {
        isIE6 = true;
    }
}
else if (!checkIt('compatible'))
{
    browser = "mozilla";
}
function checkIt(string)
{
    place = detect.indexOf(string) + 1; 
    thestring = string;
    return place;
}
if(OScheckIt('mac')) OS = "mac";
else if(OScheckIt('win')) OS = "windows";

function OScheckIt(string)
{
place=browserPlatform.indexOf(string)+1;
thestring=string;
return place;
}
if (OS == "mac" && checkIt("mozilla")) browser="macMozilla";

/** Browser Detection Code **/

/** Fixing the Footer to the bottom of the page **/
window.onload = function() {
    setTimeout("setFooter();", 300);
    //preLoadImages();
}
window.onresize = function() {
    setFooter();
}

function ChangeSpecialCharacters(str)
{
    var replacedString = str;
    if (replacedString!=null)
    {
        replacedString = replacedString.replace(new RegExp("&","g"), "&amp;");
        replacedString = replacedString.replace(new RegExp("<","g"), "&lt;");
        replacedString = replacedString.replace(new RegExp(">","g"), "&gt;");
        replacedString = replacedString.replace(new RegExp("\"","g"), "&quot;");
        replacedString = replacedString.replace(new RegExp("'","g"), "\'");
        replacedString = replacedString.replace(new RegExp("\n","g"), " ");
    }
    return replacedString;          
}

function getWindowHeight()
{
    var windowHeight=0;
    if (typeof(window.innerHeight)=='number')
    {
        windowHeight=window.innerHeight;
    }
    else
    {
        if (document.documentElement&& document.documentElement.clientHeight) 
        {
            windowHeight= document.documentElement.clientHeight;
        }
        else
        {
            if (document.body&&document.body.clientHeight)
            {
                windowHeight=document.body.clientHeight;
            }
        }
    }
    return windowHeight;
}


function setFooter()
{
    var footerElement = document.getElementById('footerWrapper');
    var headerElement = document.getElementById('headerWrapper');
    var userInfoElement = document.getElementById('userInformationWrapper');
    var contentElement = document.getElementById('wrapper');
    var margins = 20;
    if(footerElement)
    {
        var windowHeight = getWindowHeight();
        if(document.getElementById('wrapper') != null || document.getElementById('footerWrapper') != null)
        {
            if (windowHeight > 0)
            {
                var contentHeight = contentElement.offsetHeight;                
                var footerHeight = footerElement.offsetHeight;
                var headerHeight = headerElement.offsetHeight;
                var userInfoHeight = userInfoElement.offsetHeight;
                if (windowHeight-(contentHeight+footerHeight+headerHeight+userInfoHeight+20)>=10)
                {
                    footerElement.style.position='relative';
                    if(browser == "ie")
                        footerElement.style.top=(windowHeight-(contentHeight+footerHeight+headerHeight+userInfoHeight+margins))+'px';
                    else
                        footerElement.style.top=(windowHeight-(contentHeight+footerHeight+headerHeight+userInfoHeight+margins+45))+'px';
                }
                else
                {
                    footerElement.style.position='static';
                }
            }
        }
        footerElement.style.visibility = "visible";
    }
}
/** Fixing the Footer to the bottom of the page **/

//function to hide and un-hide the toolbar
function alterToolBar()
{
    if(autohide == true)
    {
        autohide = false;
        document.getElementById('bar').style.top = (objDoc.scrollTop - 27) + 'px';
        document.getElementById("expandArrow").src = "../images/toolbar-expand.gif";
        
    }
    else
    {               
        autohide = true;
        document.getElementById('bar').style.top = objDoc.scrollTop + 'px'; 
        document.getElementById("expandArrow").src = "../images/toolbar-collapse.gif";
    }
}

//function to scroll the toolbar when the user scrolls through the page
function scrollAdjust()
{
    if(autohide)
    {
        document.getElementById('bar').style.top = (objDoc.scrollTop) + 'px';
    }
    else
    {
        document.getElementById('bar').style.top = (objDoc.scrollTop-27) + 'px';
    }
}

function workflowGrid_onCallbackComplete(sender, eventArgs)
{
    var startRecord = (sender.get_currentPageIndex() * sender.get_pageSize()) + 1;
    var endRecord = (sender.get_currentPageIndex() * sender.get_pageSize()) + sender.get_pageSize();
    //if record count is less than the calculated end record number then make end record equal to record count
    if(endRecord > sender.get_recordCount())
    {
        endRecord = sender.get_recordCount();
    }
    document.getElementById("ctl00_ContentPlaceHolder1_publishWorkflow_workflowSnap_startRecord").innerHTML = startRecord;
    document.getElementById("ctl00_ContentPlaceHolder1_publishWorkflow_workflowSnap_endRecord").innerHTML = endRecord;
}

//function to show the context menu for the toolbar
function ShowToolbarContextMenu(evt, sender)
{
    toolbarContextMenu.showContextMenu(evt);
}

function Toolbar_onContextMenu(sender, eventArgs)
{
    if (eventArgs.get_item().get_text() == __JSMessages["HideBar"])
    {
        alterToolBar();
    }
}
// Function to trim
function Trim(s) 
{
	// Remove leading spaces and carriage returns
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r'))
	{
		s = s.substring(1,s.length);
	}
	// Remove trailing spaces and carriage returns
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length) == '\n') || (s.substring(s.length-1,s.length) == '\r'))
	{
		s = s.substring(0,s.length-1);
	}
	return s;
}

function validKey(e)
{
    var key;
    var keychar;
    var reg;

    if(window.event) {
        // for IE, e.keyCode or window.event.keyCode can be used
        key = e.keyCode; 
    }
    else if(e.which) {
        // netscape
        key = e.which; 
    }
    else {
        // no event, so pass through
        return true;
    }
    if (key > 31 || key == 8 || key == 16)
    {
        return true;
    }
    else
    {
        return false;
    }              
}

//Functions to show the Grid Context Menu for History Page
//function Grid1_onContextMenu(sender, eventArgs) 
//    {
//      Grid1.select(eventArgs.get_item()); 
//      GridContextMenu.showContextMenu(eventArgs.get_event()); 
//      GridContextMenu.set_contextData(eventArgs.get_item()); 
//    }
//    
//    function Grid1_onItemBeforeInsert(sender, eventArgs) 
//    {
//      if (document.getElementById('chkConfirmInsert').checked)
//        if (!confirm("Insert record?"))
//          eventArgs.set_cancel(false);
//    }
//    
//    function Grid1_onItemBeforeUpdate(sender, eventArgs) 
//    {
//      if (document.getElementById('chkConfirmUpdate').checked)
//        if (!confirm("Update record?"))
//          eventArgs.set_cancel(false);
//    }

//    function Grid1_onItemBeforeDelete(sender, eventArgs) 
//    {
//      if (document.getElementById('chkConfirmDelete').checked)
//        if (!confirm("Delete record?"))
//          eventArgs.set_cancel(false);
//    }
//}

//function for the Calendar and Picker on Modify Users Page
function expirationCalendar_onSelectionChanged(sender, eventArgs)
{
     
     var cal=window[expirydateIdHolder];
      var selectedDate = cal.getSelectedDate();
     
    //var selectedDate = expirationCalendar.getSelectedDate();
    //alert();
    if(selectedDate <= new Date())
    {
        alert(GetMessage('UserInvalidExpiryDate'));
    }
    else
    {
        //var calTextBox=window[expirydateTxtIdHolder];
         document.getElementById(expirydateTxtIdHolder).value = cal.formatDate(selectedDate,"MM/dd/yyyy");
        // calTextBox.value=cal.formatDate(selectedDate,"MMMM dd, yyyy");
       // document.getElementById(expirydateIdHolder).value = expirationCalendar.formatDate(selectedDate,"MMMM dd, yyyy");
    }
}

function popUpCalendar()
{ 
    
    var thisDate = new Date();
    obj = document.getElementById(expirydateTxtIdHolder).value;
    
    if(obj != "" && obj != null)

    if(validate_dateSummary(document.getElementById(expirydateTxtIdHolder))==true)// available in validation.js 

    thisDate = new Date(obj);

    var cal=window[expirydateIdHolder];
    //cal.setSelectedDate(thisDate)
    if(!(cal.get_popUpShowing())); 
        cal.show();
       
        
//    expirationCalendar.setSelectedDate(thisDate);
//    if(!(expirationCalendar.get_popUpShowing())); 
//        expirationCalendar.show();
}

/** Methods for Context Menu of Add a Page **/
function cmPageTree_onContextMenu(sender, eventArgs){  
        var evt = eventArgs.get_event();
        cmPageTree.showContextMenuAtEvent(evt);
}
function cmPageTree_onItemSelect(sender, eventArgs) { alert(__JSMessages["pagetree"]); }
/** Methods for Context Menu of Add a Page **/

/** Methods to expand collapse the trees on the control center page **/
function expandFileTree(){ fileLibraryTree.expandAll(); }
function expandImageTree(){ imageLibraryTree.expandAll(); }
function expandPageTree(){ addPageTree.expandAll(); }
/** Methods to expand collapse the trees on the control center page **/

/** Methods for Context Menu on Modify Delete User Page **/
/*function cmUsers_onContextMenu(sender, eventArgs){  
    if(browser == "ie")
    {
        var evt = eventArgs.get_event();
        cmUsers.showContextMenu(evt.clientX, evt.clientY);
    }
    else { cmUsers.showContextMenu(eventArgs.get_event()); }
}
function cmUsers_onItemSelect(sender, eventArgs){ alert("User Action"); }*/
/** Methods for Context Menu on Modify Delete User Page **/

/** Methods for Context Menu on Site Structure Page **/
function siteStructure_onContextMenu(sender, eventArgs){  
    var evt = eventArgs.get_event();
    cmSiteStructure.showContextMenuAtEvent(evt);
}
function siteStructure_onItemSelect(sender, eventArgs) { alert(__JSMessages["SiteStructure"]); }
/** Methods for Context Menu on Site Structure Page **/

/** Methods to Expand/Collapse Tree **/
function expandTree(objTag)
{
    var imgId = "";
    var textId = "";
    var tagId = objTag.id;
    //get the id of the expand/collapse image
    imgId = "icon" + tagId;
    //get the id of the expand/collapse text
    textId = "text" + tagId;
    switch (document.getElementById(textId).innerHTML)
    {
        case __JSMessages["ExpandTree"]:
            //expand the tree
            if(tagId == "FileLibrary")
                ToggleExpand(fileLibraryTree, imgId, textId);
            else if(tagId == "ImageLibrary")
                ToggleExpand(imageLibraryTree, imgId, textId);
            else if(tagId == "PageLibrary")
                ToggleExpand(menuTree, imgId, textId);
            else if(tagId == "ManageSiteStructure")
                ToggleExpand(siteStructureTree, imgId, textId);
            else if(tagId == "ExpandSearchResult")
                ToggleExpand(searchResultTree, imgId, textId);
            break;
        case __JSMessages["CollapseTree"]:
            //Collapse the tree
            if(tagId == "FileLibrary")
                ToggleCollapse(fileLibraryTree, imgId, textId);
            else if(tagId == "ImageLibrary")
                ToggleCollapse(imageLibraryTree, imgId, textId);
            else if(tagId == "PageLibrary")
                ToggleCollapse(menuTree, imgId, textId);
            else if(tagId == "ManageSiteStructure")
                ToggleCollapse(siteStructureTree, imgId, textId);
            else if(tagId == "ExpandSearchResult")
                ToggleCollapse(searchResultTree, imgId, textId);
            break;
    }
}

function ToggleExpand(objTree, imgId, textId)
{
    objTree.expandAll();
    //change the image to collapse
    document.getElementById(imgId).src = "../App_Themes/General/images/minus.gif";
    //change the text to "Collapse Tree"
    document.getElementById(textId).innerHTML = __JSMessages["CollapseTree"];
}

function ToggleCollapse(objTree, imgId, textId)
{
    objTree.collapseAll();
    //change the image to expand
    document.getElementById(imgId).src = "../App_Themes/General/images/plus.gif";
    //change the text to "Expand Tree"
    document.getElementById(textId).innerHTML = __JSMessages["ExpandTree"];
}
/** Methods to Expand/Collapse Tree **/

function clearText(objTextbox)
{
    if (objTextbox.value == __JSMessages["TypeHereToFilterResults"])
        objTextbox.value = "";
}


/** Method to change the start and end record number for the workflow grid **/
function updateGrid(objPageSize)
{
//    cmsUsers.set_pageSize(objPageSize.options[objPageSize.selectedIndex].value);
//    cmsUsers.scrollTo(cmsUsers.get_currentPageIndex());
}

/** Methods for View Edit Permissions **/
//var allAuthors = false;
//var allApprovers = false;
//var allPublishers = false;
//var allNone = false;
//var allArchive = false;
//var allEditNav = false;

//function checkAllAuthor(obj)
//{
//    if(allAuthors)
//    {
//        allAuthors = false;
//        UncheckAllItems(2);
//    }
//    else
//    {
//        allAuthors = true;
//        CheckAllItems(2);
//        UncheckAllItems(5);
//        document.getElementById("chkNone").checked = false;
//    }
//}

//function checkAllApprover(obj)
//{
//    if(allApprovers)
//    {
//        allApprovers = false;
//        UncheckAllItems(3);
//    }
//    else
//    {
//        allApprovers = true;
//        CheckAllItems(3);
//        UncheckAllItems(5);
//        document.getElementById("chkNone").checked = false;
//    }
//}

//function checkAllPublisher(obj)
//{
//    if(allPublishers)
//    {
//        allPublishers = false;
//        UncheckAllItems(4);
//    }
//    else
//    {
//        allPublishers = true;
//        CheckAllItems(4);
//        UncheckAllItems(5);
//        document.getElementById("chkNone").checked = false;
//    }
//}

//function checkAllNone(obj)
//{
//    if(allNone)
//    {
//        allNone = false;
//        UncheckAllItems(5);
//    }
//    else
//    {
//        allNone = true;
//        CheckAllItems(5);
//        UncheckAllItems(2);
//        UncheckAllItems(3);
//        UncheckAllItems(4);
//        document.getElementById("chkAuthor").checked = false;
//        document.getElementById("chkApprover").checked = false;
//        document.getElementById("chkPublisher").checked = false;
//    }
//}

//function checkAllArchive(obj)
//{
//    if(allArchive)
//    {
//        allArchive = false;
//        UncheckAllItems(6);
//    }
//    else
//    {
//        allArchive = true;
//        CheckAllItems(6);
//    }
//}

//function checkAllEditNav(obj)
//{
//    if(allEditNav)
//    {
//        allEditNav = false;
//        UncheckAllItems(7);
//    }
//    else
//    {
//        allEditNav = true;
//        CheckAllItems(7);
//    }
//}

//function CheckAllItems(columnno)
//{
//    var gridItem; 
//    var gridItemChild; 
//    var itemIndex = 0; 
//    var itemIndexChild = 0; 
//    var childTable;
//    while(gridItem = permissions.Table.GetRow(itemIndex))
//    { 
//        childTable = gridItem.ChildTable;
//        itemIndexChild = 0; 
//        if(childTable != null)
//        {
//            while(gridItemChild = childTable.GetRow(itemIndexChild))
//            {
//                gridItemChild.SetValue(columnno,true,false); 
//                itemIndexChild++;
//            }
//        }
//        gridItem.SetValue(columnno,true,false); 
//        itemIndex++; 
//    } 
//    permissions.Render(); 
//}

//function UncheckAllItems(columnno) 
//{ 
//    var gridItem; 
//    var gridItemChild; 
//    var itemIndex = 0; 
//    var itemIndexChild = 0; 
//    var childTable; 
//    while(gridItem = permissions.Table.GetRow(itemIndex))
//    { 
//        childTable = gridItem.ChildTable;
//        itemIndexChild = 0; 
//        if(childTable != null)
//        {
//            while(gridItemChild = childTable.GetRow(itemIndexChild))
//            {
//                gridItemChild.SetValue(columnno,false,false); 
//                itemIndexChild++;
//            }
//        }
//        gridItem.SetValue(columnno,false,false); 
//        itemIndex++; 
//    } 
//    permissions.Render(); 
//}
///** Methods for View Edit Permissions **/

/** Methods for the Login Form **/
function resetLoginForm()
{
    document.forms[0].reset();
    return false;
}
/** Methods for the Login Form **/

function imageDirectory_onContextMenu(sender, eventArgs){
    var evt = eventArgs.get_event();
    cmImageDirectory.showContextMenuAtEvent(evt, eventArgs.get_node());   
}

function cmImageDirectory_onItemSelect(menuItem)
{
    if(menuItem.ContextData.get_parentNode() != null)
    {
        sURL = "UploadImage.aspx?folderPath=" + menuItem.ContextData.get_parentNode().get_text() + "~" + menuItem.ContextData.get_text();
    }
    else
    {
        sURL = "UploadImage.aspx?folderPath=" + "~" + menuItem.ContextData.get_text();
    }
    window.open(sURL, null, "height=200,width=400,status=yes,toolbar=no,menubar=no,location=no,resizable=no")
}

//Method to change the button bg on mouse over and mouse out
function changeButton(objButton)
{
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if(buttonClass.indexOf("Hover") > -1)
    {
        buttonClassNum = buttonClass.substring(6,7);
        newButtonClass = "button" + buttonClassNum;
        oldButtonClass = "button" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else
    {
        buttonClassNum = buttonClass.substring(6,7);
        oldButtonClass = "button" + buttonClassNum;
        newButtonClass = "button" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
//Method to change the arrow button bg on mouse over and mouse out
function changeArrowButton(objButton)
{
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if(buttonClass.indexOf("Hover") > -1)
    {
        buttonClassNum = buttonClass.substring(11,12);
        newButtonClass = "arrowButton" + buttonClassNum;
        oldButtonClass = "arrowButton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else
    {
        buttonClassNum = buttonClass.substring(11,12);
        oldButtonClass = "arrowButton" + buttonClassNum;
        newButtonClass = "arrowButton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
//Method to change the arrow button bg on mouse over and mouse out
function changeGoButton(objButton)
{
    var buttonClass = objButton.className;
    if(buttonClass.indexOf("Hover") > -1)
    {
        objButton.className = "goButton";
    }
    else
    {
        objButton.className = "goButtonHover";
    }
}
//Method to change the button bg on mouse over and mouse out
function changeFileButton(objButtonCntrl)
{
    var objButton = document.getElementById(objButtonCntrl)
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if(buttonClass.indexOf("Hover") > -1)
    {
        buttonClassNum = buttonClass.substring(6,7);
        newButtonClass = "button" + buttonClassNum;
        oldButtonClass = "button" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else
    {
        buttonClassNum = buttonClass.substring(6,7);
        oldButtonClass = "button" + buttonClassNum;
        newButtonClass = "button" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
//Method to change the primary button bg on mouse over and mouse out
function changePrimaryButton(objButton)
{
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if(buttonClass.indexOf("Hover") > -1)
    {
        buttonClassNum = buttonClass.substring(13,14);
        newButtonClass = "primarybutton" + buttonClassNum;
        oldButtonClass = "primarybutton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else
    {
        buttonClassNum = buttonClass.substring(13,14);
        oldButtonClass = "primarybutton" + buttonClassNum;
        newButtonClass = "primarybutton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
//setting the selected file for menu rollover on image
function setOnImage(fileUploadObject,objText)
{
    document.getElementById(fileUploadObject).click();
    document.getElementById(objText).value = document.getElementById(fileUploadObject).value;
}
//setting the selected file for menu rollover off image
function setOffImage(fileUploadObject,objText)
{
    document.getElementById(fileUploadObject).click();
    objText.value =  fileUploadObject.value;
}

/** Methods for Search Grid **/
function clearDefaultText(objTextbox)
{
    if (objTextbox.value == __JSMessages["TypeHereToFilterResults"])
        objTextbox.value = "";
}
function setDefaultText(objTextbox)
{
    if(Trim(objTextbox.value) == "")
        objTextbox.value = __JSMessages["TypeHereToFilterResults"];
}
/** Methods for Search Grid **/
/** Methods for Global search **/
function clearSearch(objText)
{
    if(objText.value == __JSMessages["Search"])
        objText.value = "";
}
function setSearch(objText)
{
    if(Trim(objText.value) == "")
    {
        objText.value = __JSMessages["Search"];
    }
}
/** Methods for Global Search **/
/** Method to add permission **/
function addCustomPermission(objSites, objGroups, objPermissions)	
{
	var selectedSite = document.getElementById(objSites).value;
	var selectedGroup = document.getElementById(objGroups).value;
	var ddlPermission = document.getElementById(objPermissions);
	if(selectedSite == "" || selectedSite == null)
	{
	    alert(__JSMessages["PleaseSelectASite"]);
	    return false;
	}
	if(selectedGroup == "" || selectedGroup == null)
	{
	    alert(__JSMessages["PleaseSelectAGroup"]);
	    return false;
	}
	ddlPermission.options[ddlPermission.length] = new Option(selectedSite + ": " + selectedGroup, selectedSite + ": " + selectedGroup);
	return false;
}
/** Method to add permission **/

/** Method to delete permission **/
function deletePermission(objPermissions)	
{
	var ddlPermission = document.getElementById(objPermissions);
	for(var i=0; i<ddlPermission.length; i++)
	{
	    if(ddlPermission.options[i].selected)
	    {
	        ddlPermission.remove(i);
	        i--;
	    }
	}
	return false;
}
/** Method to delete permission **/

/** Method for Preloading Images **/
function preLoadImages()
{
    pic1= new Image(29,23);
    pic1.src = "../App_Themes/General/images/arrow-btn1-hover.gif)";
    pic2= new Image(30,25);
    pic2.src = "../App_Themes/General/images/arrow-btn1-hover.png)";
    pic3= new Image(70,25);
    pic3.src = "../App_Themes/General/images/arrow-btn2-hover.gif)";
    pic4= new Image(70,25);
    pic4.src = "../App_Themes/General/images/arrow-btn2-hover.png)";
    pic5= new Image(89,23);
    pic5.src = "../App_Themes/General/images/arrow-btn3-hover.gif)";
    pic6= new Image(90,25);
    pic6.src = "../App_Themes/General/images/arrow-btn3-hover.png)";
    pic7= new Image(109,23);
    pic7.src = "../App_Themes/General/images/arrow-btn4-hover.gif)";
    pic8= new Image(110,25);
    pic8.src = "../App_Themes/General/images/arrow-btn4-hover.png)";
    pic9= new Image(129,23);
    pic9.src = "../App_Themes/General/images/arrow-btn5-hover.gif)";
    pic10= new Image(130,25);
    pic10.src = "../App_Themes/General/images/arrow-btn5-hover.png)";
    pic11= new Image(149,23);
    pic11.src = "../App_Themes/General/images/arrow-btn6-hover.gif)";
    pic12= new Image(150,25);
    pic12.src = "../App_Themes/General/images/arrow-btn6-hover.png)";
    pic13= new Image(169,23);
    pic13.src = "../App_Themes/General/images/arrow-btn7-hover.gif)";
    pic14= new Image(171,25);
    pic14.src = "../App_Themes/General/images/arrow-btn7-hover.png)";
    pic15= new Image(249,23);
    pic15.src = "../App_Themes/General/images/arrow-btn8-hover.gif)";
    pic16= new Image(250,25);
    pic16.src = "../App_Themes/General/images/arrow-btn8-hover.png)";
    pic17= new Image(299,23);
    pic17.src = "../App_Themes/General/images/arrow-btn9-hover.gif)";
    pic18= new Image(300,25);
    pic18.src = "../App_Themes/General/images/arrow-btn9-hover.png)";
    pic19= new Image(30,25);
    pic19.src = "../App_Themes/General/images/btn-go-hover.gif)";
    pic20= new Image(69,23);
    pic20.src = "../App_Themes/General/images/btn01-hover.gif)";
    pic21= new Image(70,25);
    pic21.src = "../App_Themes/General/images/btn01-hover.png)";
    pic22= new Image(89,23);
    pic22.src = "../App_Themes/General/images/btn02-hover.gif)";
    pic23= new Image(90,25);
    pic23.src = "../App_Themes/General/images/btn02-hover.png)";
    pic24= new Image(109,23);
    pic24.src = "../App_Themes/General/images/btn03-hover.gif)";
    pic25= new Image(110,25);
    pic25.src = "../App_Themes/General/images/btn03-hover.png)";
    pic26= new Image(129,23);
    pic26.src = "../App_Themes/General/images/btn04-hover.gif)";
    pic27= new Image(130,25);
    pic27.src = "../App_Themes/General/images/btn04-hover.png)";
    pic28= new Image(149,23);
    pic28.src = "../App_Themes/General/images/btn05-hover.gif)";
    pic29= new Image(150,25);
    pic29.src = "../App_Themes/General/images/btn05-hover.png)";
    pic30= new Image(171,25);
    pic30.src = "../App_Themes/General/images/btn06-hover.png)";
    pic31= new Image(249,23);
    pic31.src = "../App_Themes/General/images/btn07-hover.gif)";
    pic32= new Image(250,25);
    pic32.src = "../App_Themes/General/images/btn07-hover.png)";
    pic33= new Image(12,9);
    pic33.src = "../App_Themes/General/images/cm-submenu-hover.gif)";
    pic34= new Image(1,1);
    pic34.src = "../App_Themes/General/images/first_hover.gif)";
    pic35= new Image(39,22);
    pic35.src = "../App_Themes/General/images/go-btn-hover.gif)";
    pic36= new Image(1000,24);
    pic36.src = "../App_Themes/General/images/header_hoverBg.gif)";
    pic37= new Image(7,21);
    pic37.src = "../App_Themes/General/images/hover_tab_bg.gif)";
    pic38= new Image(4,21);
    pic38.src = "../App_Themes/General/images/hover_tab_left_icon.gif)";
    pic39= new Image(4,21);
    pic39.src = "../App_Themes/General/images/hover_tab_right_icon.gif)";
    pic40= new Image(1,1);
    pic40.src = "../App_Themes/General/images/last_hover.gif)";
    pic41= new Image(144,25);
    pic41.src = "../App_Themes/General/images/menu-hover-administration.gif)";
    pic42= new Image(144,25);
    pic42.src = "../App_Themes/General/images/menu-hover-libraries.gif)";
    pic43= new Image(63,25);
    pic43.src = "../App_Themes/General/images/next_hover.gif)";
    pic44= new Image(84,25);
    pic44.src = "../App_Themes/General/images/prev_hover.gif)";
    pic45= new Image(13,22);
    pic45.src = "../App_Themes/General/images/slider_grip_hover.gif)";
    pic46= new Image(8,4);
    pic46.src = "../App_Themes/General/images/toolbar-hover-arrow.gif)";
    pic47= new Image(21,21);
    pic47.src = "../App_Themes/General/images/toolbar-hover-edit.gif)";
    pic48= new Image(21,21);
    pic48.src = "../App_Themes/General/images/toolbar-hover-preview.gif)";
    pic49= new Image(21,21);
    pic49.src = "../App_Themes/General/images/toolbar-hover-www.gif)";
    pic50= new Image(142,22);
    pic50.src = "../App_Themes/General/images/top-item-hover.gif)";
    pic51= new Image(4,24);
    pic51.src = "../App_Themes/General/images/vhover_tab_bg.gif)";
    pic52= new Image(4,24);
    pic52.src = "../App_Themes/General/images/vhover_tab_bg_lb.gif)";
    pic53= new Image(4,24);
    pic53.src = "../App_Themes/General/images/vhover_tab_left_icon.gif)";
    pic54= new Image(4,24);
    pic54.src = "../App_Themes/General/images/vhover_tab_left_icon_lb.gif)";
    pic55= new Image(4,24);
    pic55.src = "../App_Themes/General/images/vhover_tab_left_icon_w.gif)";
    pic56= new Image(4,24);
    pic56.src = "../App_Themes/General/images/vhover_tab_right_icon.gif)";
    pic57= new Image(4,24);
    pic57.src = "../App_Themes/General/images/vhover_tab_right_icon_lb.gif)";
    pic58= new Image(4,24);
    pic58.src = "../App_Themes/General/images/vhover_tab_right_icon_w.gif)";
    pic59= new Image(1,1);
    pic59.src = "../App_Themes/General/images/first_hover.gif)";
    pic60= new Image(1,1);
    pic60.src = "../App_Themes/General/images/last_hover.gif)";
    pic61= new Image(63,25);
    pic61.src = "../App_Themes/General/images/next_hover.gif)";
    pic62= new Image(84,25);
    pic62.src = "../App_Themes/General/images/prev_hover.gif)";
    pic63= new Image(13,22);
    pic63.src = "../App_Themes/General/images/slider_grip_hover.gif)";
}
/** Method for Preloading Images **/
/** method for changing page Index **/
function currentPageIndex(grd)

{

    if (grd.RecordCount==0)

    {

        return 1;

    }

    else

    {

        return grd.CurrentPageIndex + 1;

    }

}

function pageCount(grd)
{
    if (grd.RecordCount==0)
    {
        return 1;
    }
    else
    {
        return grd.PageCount;
    }
}
/** method for changing page Index **/



function UpdateSessionStatus()
{
   // debugger;
  // get session timeout value in minutes (it comes from server and is set to this window value in MasterPage.master
  var sessionTimeout = window["SessionTimeoutValue"] ; // 1 minute less than the actual server session timeout
  if(window["IsSessionValid"]!=false)
  {
      if(sessionTimeoutCounter >= sessionTimeout) // session has already expired
      {
        window["IsSessionValid"] = false;
      }
      else
       {
        var shouldResetSessionCounter = GetCookie('ResetSessionCounter');
        if(shouldResetSessionCounter=="yes")
            {
                sessionTimeoutCounter = 2;
                SetCookie('ResetSessionCounter','no');
            }
        else
            sessionTimeoutCounter = sessionTimeoutCounter + 1;

        window["IsSessionValid"] = true;
      }
  }
}      
      
function IsThisSessionValid()
{
    if(window["IsSessionValid"])
    {
            // alert(document.cookie);
        DeleteCookie('ResetSessionCounter');
         //       alert(document.cookie);

        return true;
    }
   
   return false;

}
      
var timerObj;
var sessionTimeoutCounter = 0;
window["IsSessionValid"] = true;
function SetTimer(){
  
  var dblMinutes = 1;       // every minute check call this function
  //set timer to call function to confirm update 
  timerObj = setInterval("UpdateSessionStatus()",1000*60*dblMinutes);
}

function CheckSession()
{
    if(!IsThisSessionValid())
    {
        window.location.reload();
        return false;
    }
    return true;
}

//Event Handler 

function CheckSessionEventHandler(sender, eventArgs)
{
   if(!IsThisSessionValid())
   {    
    eventArgs.set_cancel(true);
    window.location.reload();
   }
}

var cacheResetPopup;
function openCacheResetPopup()
{
    cacheResetPopup = dhtmlmodal.open('Content', 'iframe', '/Admin/Popups/ResetCachePopup.aspx', __JSMessages["CacheReset"],
                                              'width=388px,height=234px,center=1,resize=0,scrolling=0');
    cacheResetPopup.onclose = function()
    {
        var a = document.getElementById('Content');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        return true;
    }
}

var toolObjId;
function ToolTipPosition(e, objId)
{
//debugger;
    var ie5=document.all&&document.getElementById;
    var ns6=document.getElementById&&!document.all;
    //var menuobj=document.getElementById("dhtmltooltip");
    var menuobj=document.getElementById(objId);
    toolObjId = objId;
    //Find out how close the mouse is to the corner of the window
    var rightedge = ie5? (document.documentElement.clientWidth-e.clientX) : (window.innerWidth-e.clientX);
    var bottomedge = ie5? (document.documentElement.clientHeight-e.clientY) : (window.innerHeight-e.clientY);

    //if the horizontal distance isn't enough to accomodate the width of the context menu
    if (rightedge<menuobj.offsetWidth)
    {
        //move the horizontal position of the menu to the left by it's width
        xLeft=ie5? document.documentElement.scrollLeft+e.clientX-menuobj.offsetWidth : window.pageXOffset+e.clientX-menuobj.offsetWidth;
    }
    else
    {
        //position the horizontal position of the menu where the mouse was clicked
        xLeft=ie5? document.documentElement.scrollLeft+e.clientX : window.pageXOffset+e.clientX;
    }
    //if the vertical distance isn't enough to accomodate the height of the context menu
    if (bottomedge<menuobj.offsetHeight)
    {
        //move the vertical position of the menu up by it's height
        yTop=ie5? document.documentElement.scrollTop+e.clientY-menuobj.offsetHeight : window.pageYOffset+e.clientY-menuobj.offsetHeight;
    }
    else
    {
        if(browser == "safari")
        {
            //position the vertical position of the menu where the mouse was clicked
            yTop=ie5? document.documentElement.scrollTop+e.clientY : e.clientY;
        }
        else
        {
            //position the vertical position of the menu where the mouse was clicked
            yTop=ie5? document.documentElement.scrollTop+e.clientY : window.pageYOffset+e.clientY;
        }
    }

    menuobj.style.visibility = "visible";
    menuobj.style.top = yTop + 'px';
    menuobj.style.left = xLeft + 'px';
    return false;
}
function HideToolTip()
{
//debugger;
 document.getElementById(toolObjId).style.visibility = "hidden";
}
