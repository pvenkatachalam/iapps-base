﻿var objSessionMonitor;
var currentTimeLeft;
var isFrontEndG, isAnalyticsG;
var sessionTimeout;

function ScheduleSessionMonitor(frontEnd, isAnalytics, timeOut) {
    var isInIFrame = (window.location != window.parent.location) ? true : false;

    if (!isInIFrame) {
        if (timeOut != null) {
            sessionTimeout = timeOut;
        }

        currentTimeLeft = sessionTimeout;
        isFrontEndG = frontEnd;
        
        //if (isAnalytics != null)
        //    isAnalyticsG = isAnalytics;
        
        objSessionMonitor = setInterval("StartSessionMonitor()", 1000 * 60);
    }
}

function StartSessionMonitor() {
    var shouldReset = GetCookie("ResetSessionCounter");

    if (shouldReset == "yes") {
        currentTimeLeft = sessionTimeout;
        SetCookie("ResetSessionCounter", "no", null, "/");
    }
   
    currentTimeLeft--;

    if (currentTimeLeft <= 2) {
        clearInterval(objSessionMonitor);
        OpenTimeoutWarningPopup();
    }
}

var timeoutWarningWindow;

function OpenTimeoutWarningPopup() {
    var currentSite = GetCurrentSection();
    var url = "/commonlogin/Popups/TimeoutWarning.aspx?Site=" + currentSite + "&IsFrontEnd=" + isFrontEndG;

    //if (isAnalyticsG)
    //    url = "/" + GetCurrentSection() + "/Popups/TimeoutWarning.aspx?Site=" + currentSite + "&IsFrontEnd=false";

    timeoutWarningWindow = dhtmlmodal.open('SessionTimeoutWarning', 'iframe', url, __JSMessages["SessionTimeoutWarning"], 'width=425px,height=200px,center=1,resize=0,scrolling=1');
    timeoutWarningWindow.onclose = function () {
        var a = document.getElementById('SessionTimeoutWarning');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        window.frames[ifr[0].name].documentElement.style.zIndex = "100000";
        return true;
    }
}


function LogoutUser() {
    var currentSite = getQueryString("Site");
    var isFrontEnd = getQueryString("IsFrontEnd");

    if (isFrontEnd == "true") {
        parent.iapps_container_contentChanged_ShowAlert = false;
        parent.LogOutFromSiteEditor();
    }
    else {
        var parentUrl = window.parent.location + "";
        var indexOfQuestionMark = parentUrl.indexOf("?");

        if (indexOfQuestionMark >= 0)
            parentUrl = parentUrl.substring(0, indexOfQuestionMark);
        
        window.parent.location = "/" + currentSite + '/SignOut.aspx?timedOut=true&ReturnPage=' + parentUrl;
    }
}
function UpdateTimeoutCounter() {
    var spnTimeOutSec = document.getElementById("spnTimeOutSec");
    spnTimeOutSec.innerHTML = parseInt(spnTimeOutSec.innerHTML) - 1;
    
    if (parseInt(spnTimeOutSec.innerHTML) == 0) {
        LogoutUser();
    }
    else if (parseInt(spnTimeOutSec.innerHTML) < 0) {
        parent.timeoutWarningWindow.hide();
    }
}

function StartTimeoutCounter() {
    sessionTimeoutInterval = setInterval("UpdateTimeoutCounter()", 1000);
}

function resetUserSession(objButton) {
    clearInterval(sessionTimeoutInterval);

    var currentSite = getQueryString("Site");
    var isFrontEnd = getQueryString("IsFrontEnd");
    
    isFrontEndG = isFrontEnd;
    
    objButton.disabled = true;
    objButton.className += " disabled";

    document.getElementById("spnTimeoutMsg").innerHTML = __JSMessages["PleaseWaitWhileYourSessionIsBeingReset"];
    
    if (isFrontEnd == "true") {
        $.ajax({
            type: 'POST',
            url: '/Default.aspx',
            data: "",
            success: onResetSession,
            dataType: null
        });
    }
    else {
        $.ajax({
            type: 'POST',
            url: "/" + currentSite + '/SignOut.aspx?Reset=true',
            data: "",
            success: onResetSession,
            dataType: null
        });
    }
}

function onResetSession() {
    parent.ScheduleSessionMonitor(parent.isFrontEndG, null, parent.sessionTimeout);
    parent.timeoutWarningWindow.hide();
}



function getQueryString(key) {
    var indexOfQuestionMark = location.search.indexOf("?");

    if (indexOfQuestionMark >= 0) {
        var queryString = location.search.substring(indexOfQuestionMark + 1);

        return getMultiValueCookieValue(queryString, key);
    }
    else {
        return "";
    }
}

function getMultiValueCookieValue(complexCookieValue, key) {
    var indexOfKey = complexCookieValue.indexOf(key + "=");

    if (indexOfKey == -1) {
        indexOfKey = complexCookieValue.indexOf("&" + key + "=");

        return "";
    }

    complexCookieValue = complexCookieValue.substring(indexOfKey + 1);

    var indexOfEqualSign = complexCookieValue.indexOf("=");

    if (indexOfEqualSign == -1)
        return "";

    var indexOfAmperSign = complexCookieValue.indexOf("&");

    if (indexOfAmperSign == -1)
        return complexCookieValue.substring(indexOfEqualSign + 1);
    else
        return complexCookieValue.substring(indexOfEqualSign + 1, indexOfAmperSign);
}

function GetURL() {
    var url = document.URL;
    var indexOfQues = url.indexOf("?");
    if (indexOfQues != -1) {
        url = url.substring(0, indexOfQues);
    }
    return url;
}

function GetPageName() {
    var url = GetURL();
    var pageName = "";
    if (url.substring(url.length - 1) == '/' || url.substring(url.length - 1) == '\\') {
        url = url.substring(0, url.length - 1);
    }
    var lastIndexOfSlash = url.lastIndexOf("/");
    if (lastIndexOfSlash == -1)
        lastIndexOfSlash = url.lastIndexOf("\\");
    if (lastIndexOfSlash != -1) {
        pageName = url.substring(lastIndexOfSlash + 1);
    }
    return pageName.toLowerCase();
}



function GetCurrentSection() {
    var url = GetURL().toLowerCase();
    var domain = document.domain.toLowerCase();
    var indexOfDomain = url.indexOf(domain);
    var currentSection = url.substring(indexOfDomain + domain.length);

    if (currentSection.length > 0) {
        if (currentSection.substring(0, 1) == '/')
            currentSection = currentSection.substring(1);
    }

    var indexOfQuestion = currentSection.indexOf("/");
    if (indexOfQuestion > -1)
        currentSection = currentSection.substring(0, indexOfQuestion);
    else {
        indexOfQuestion = currentSection.indexOf("?");
        if (indexOfQuestion > -1)
            currentSection = currentSection.substring(0, indexOfQuestion);
    }

    return unescape(currentSection);
}

/*** Methods added by AV ***/
// "Internal" function to return the decoded value of a cookie
//
function getCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

//
// Function to return the value of the cookie specified by "name".
// name - String object containing the cookie name.
// returns - String object containing the cookie value, or null if
// the cookie does not exist.
//
function GetCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

//
// Function to create or update a cookie.
// name - String object object containing the cookie name.
// value - String object containing the cookie value. May contain
// any valid string characters.
// [expires] - Date object containing the expiration data of the cookie. If
// omitted or null, expires the cookie at the end of the current session.
// [path] - String object indicating the path for which the cookie is valid.
// If omitted or null, uses the path of the calling document.
// [domain] - String object indicating the domain for which the cookie is
// valid. If omitted or null, uses the domain of the calling document.
// [secure] - Boolean (true/false) value indicating whether cookie transmission
// requires a secure channel (HTTPS). 
//
// The first two parameters are required. The others, if supplied, must
// be passed in the order listed above. To omit an unused optional field,
// use null as a place holder. For example, to call SetCookie using name,
// value and path, you would code:
//
// SetCookie ("myCookieName", "myCookieValue", null, "/");
//
// Note that trailing omitted parameters do not require a placeholder.
//
// To set a secure cookie for path "/myPath", that expires after the
// current session, you might code:
//
// SetCookie (myCookieVar, cookieValueVar, null, "/myPath", null, true);
//
function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape(value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}

// Function to delete a cookie. (Sets expiration date to current date/time)
// name - String object containing the cookie name
//
function DeleteCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1); // This cookie is history
    var cval = GetCookie(name);
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}




function AddWithComma(objTextBox, strData) {
    if (objTextBox.value == "")
        objTextBox.value = ",";
    if (!CheckWithComma(objTextBox, strData))
        objTextBox.value += strData + ",";
}

function RemoveWithComma(objTextBox, strData) {
    objTextBox.value = objTextBox.value.replace("," + strData + ",", ",");
}

function CheckWithComma(objTextBox, strData) {
    if (objTextBox.value.indexOf("," + strData + ",") != -1)
        return true;
    else
        return false;
}
