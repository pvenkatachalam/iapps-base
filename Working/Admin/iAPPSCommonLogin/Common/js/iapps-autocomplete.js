﻿if (jQuery.ui) {
    var catcompleteJson = { categoryId: "", groupLimit: 3, limitAfterTruncate: 20 };

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";
            var i = 0;
            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    var $li = $("<li class='category-section'/>").append(
                            $("<a class='category-label category-filter'>")
                                .text(item.category)
                                .attr("categoryId", item.categoryId)
                        ).attr("prevCount", i);
                    if (catcompleteJson.categoryId != "")
                        $li.append($("<a class='category-show-all category-filter' />").text("Show All Groups"));

                    ul.append($li);
                    currentCategory = item.category;

                    i = 0;
                }

                if (catcompleteJson.categoryId != "" || i < catcompleteJson.groupLimit ||
                    items.length < catcompleteJson.limitAfterTruncate) {
                    self._renderItemData(ul, item);
                }
                i++;
            });

            ul.find("li.category-section").each(function () {
                var cnt = $(this).nextAll("li.category-section").attr("prevCount");
                if (typeof cnt == "undefined")
                    cnt = i;
                var catLabel = $(this).children("a.category-label");
                catLabel.text(catLabel.text() + " (" + cnt + ")");
            });
        }
    });

    function SwithSiteByUrl(url, id) {
        url = url.replace("[APPLICATIONNAME]", GetCurrentVDName());

        RedirectWithToken(GetCurrentProductName(), url, id);
    }

    function CheckSiteSwitchProductAccess(isMasterSite) {
        if (jProductId == jCMSProductId || jProductId == jSocialProductId || jProductId == jAnalyticsProductId || jProductId == jMarketierProductId)
            return true;

        if (isMasterSite && (jProductId == jCommerceProductId ))
            return true;

        return false;
    }

    $(function () {
        if (typeof siteSwitchJson == "undefined")
            return;

        var noOfSites = siteSwitchJson.length;
        if (jProductId == jCommerceProductId ) {
            var data = $.grep(siteSwitchJson, function (value) {
                return value.isMasterSite;
            });

            noOfSites = data.length;
        }

        if (noOfSites <= 1) {
            $(".site-search.ui-front #txtGlobalSearch").addClass("disabled");
            $(".site-search.ui-front #txtGlobalSearch").prop("disabled", true);
        }
        else {
            $(".site-search.ui-front #txtGlobalSearch").catcomplete({
                appendTo: ".site-search.ui-front",
                position: { my: "center top", at: "center bottom", collision: "flip", offset: "0 5" },
                minLength: 0,
                autoFocus: true,
                source: function (request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    var data = $.grep(siteSwitchJson, function (value) {
                        return (matcher.test(value.label) || matcher.test(value.category)) &&
                            (catcompleteJson.categoryId == "" || catcompleteJson.categoryId == value.categoryId) &&
                            (CheckSiteSwitchProductAccess(value.isMasterSite));
                    });
                    response(data);
                },
                select: function (event, ui) {
                    if (typeof ui.item != "undefined")
                        SwithSiteByUrl(ui.item.url, ui.item.id);
                    return false;
                }
            }).focus(function () {
                $(this).catcomplete("search", $(this).val());
            });

            $(document).on("click", ".site-search.ui-front .category-filter", function () {
                catcompleteJson.categoryId = typeof $(this).attr("categoryId") == "undefined" ? "" : $(this).attr("categoryId");

                var $txtSearch = $(".site-search.ui-front #txtGlobalSearch");
                var searchVal = $txtSearch.val() == $txtSearch.attr("title") ? "" : $txtSearch.val();
                $txtSearch.catcomplete("search", searchVal);
            });

            $(".site-search.ui-front #txtGlobalSearch").keyup(function (event) {
                if (event.keyCode == 13) {
                    var $txtValue = $(this).val();
                    var data = $.grep(siteSwitchJson, function (value) {
                        return (value.label.toLowerCase() == $txtValue.toLowerCase());
                    });

                    if (data != null && data.length > 0)
                        SwithSiteByUrl(data[0].url, data[0].id);

                    event.stopPropagation();
                    return false;
                }
            });
        }
    });
}