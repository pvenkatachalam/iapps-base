﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DateRange.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.DateRange" %>

<asp:Panel runat="server" CssClass="date-range" ID="pnlDateRange">
    <script type="text/javascript">
        function GetSelectedDate_<%=this.ClientID %>()
        {
            var selStartDate = new Date();
            var selEndDate = new Date();

            var predefinedRange = $("#<%= ddlPredefindedRange.ClientID %>").val();

            if (predefinedRange == "Custom"){
                selStartDate = new Date($("#<%= txtFromDate.ClientID %>").val());
                selEndDate = new Date($("#<%= txtToDate.ClientID %>").val());
            }
            else {
                selStartDate = jQuery.toDate(dateRangeJson[predefinedRange].StartDate);
                selEndDate = jQuery.toDate(dateRangeJson[predefinedRange].EndDate);
            }
    
            if (predefinedRange != "All")
                selEndDate = new Date(selEndDate.getTime() + 86400000);

            return { StartDate: selStartDate, EndDate: selEndDate};
        }
    </script>
    <asp:DropDownList ID="ddlPredefindedRange" runat="server" CssClass="command-select">
        <asp:ListItem Text="<%$ Resources:GUIStrings, All %>" Value="All" runat="server" />
        <asp:ListItem Text="<%$ Resources:GUIStrings, CurrentDay %>" Value="CurrentDay" Selected="True"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, CurrentWeek %>" Value="CurrentWeek"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, MonthToDate %>" Value="MonthToDate"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, QuarterToDate %>" Value="QuarterToDate"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, YearToDate %>" Value="YearToDate"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last30Days %>" Value="Last30days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last60Days %>" Value="Last60days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last180Days %>" Value="Last180days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last365Days %>" Value="Last365days"></asp:ListItem>
    </asp:DropDownList>
    <asp:Panel ID="pnlCustomRange" runat="server" CssClass="date-range-popup" Visible="false">
        <script type="text/javascript">
            function CalendarFrom_OnChange_<%=this.ClientID %>(sender, eventArgs)
            {
                var selectedDate = <%=CalendarFrom.ClientID%>.getSelectedDate();
                var date = new Date();
                if(selectedDate > date)
                {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DateShouldNotExceedTodaysDate %>' />");
                    selectedDate = date;       
                }
                else
                {
                    document.getElementById("<%=txtFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
                    document.getElementById("<%=hdnFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(selectedDate, calendarDateFormat);  
                }
            }

            function CalendarTo_OnChange_<%=this.ClientID %>(sender, eventArgs)
            {
                var selectedToDate = <%=CalendarTo.ClientID%>.getSelectedDate();
                var selectedFromDate = <%=CalendarFrom.ClientID%>.getSelectedDate();

                document.getElementById("<%=txtToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(selectedToDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
                document.getElementById("<%=hdnToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(selectedToDate, calendarDateFormat);  
            }

            function popUpFromCalendar_<%=this.ClientID %>(textBoxId, hiddenFieldId)
            {
                var thisDate = new Date();
                var textBoxObject = document.getElementById(textBoxId);
                var hiddenFieldObject = document.getElementById(hiddenFieldId);
                if(Trim(textBoxObject.value) == "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, FromDate %>' />" || Trim(textBoxObject.value) == "")
                    textBoxObject.value = "";
                
                if(textBoxObject.value != "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, FromDate %>' />" && Trim(textBoxObject.value) != "" )
                {
                    ValidatorEnable(document.getElementById("<%= valFromDate.ClientID %>"), true);
                    if(document.getElementById("<%= valFromDate.ClientID %>").isvalid)
                    {
                        thisDate = new Date(hiddenFieldObject.value);  
                        <%=CalendarFrom.ClientID%>.setSelectedDate(thisDate);  
                    }
                }

                if(!(<%=CalendarFrom.ClientID%>.get_popUpShowing())); 
                    <%=CalendarFrom.ClientID%>.show();
            }

            function popUpToCalendar_<%=this.ClientID %>(textBoxId, hiddenFieldId)
            {
                var thisDate = new Date();
                var textBoxObject = document.getElementById(textBoxId);
                var hiddenFieldObject = document.getElementById(hiddenFieldId);
                if(Trim(textBoxObject.value) == "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ToDate %>' />" || Trim(textBoxObject.value) == "")
                    textBoxObject.value = "";
                
                if(textBoxObject.value != "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ToDate %>' />"  && Trim(textBoxObject.value) != "")
                {
                    ValidatorEnable(document.getElementById("<%= valToDate.ClientID %>"), true);
                    if(document.getElementById("<%= valToDate.ClientID %>").isvalid)
                    {
                        thisDate = new Date(hiddenFieldObject.value);    
                        <%=CalendarTo.ClientID%>.setSelectedDate(thisDate);
                    }
                }
               
                if(!(<%=CalendarTo.ClientID%>.get_popUpShowing())); 
                    <%=CalendarTo.ClientID%>.show();
            }

            function toggleCustomRange(dropdown) {
                var spanId = dropdown.id.replace("ddlPredefindedRange","pnlCustomRange");
                if(dropdown.value == "Custom") {
                    $("#" + spanId).show();
                }
                else {
                    $("#" + spanId).hide();
                }
            }

            function CloseCustomRange(pnlCustomRange) {
                $("#" + pnlCustomRange).hide();

                var dropDownId = pnlCustomRange.replace("pnlCustomRange","ddlPredefindedRange");
                $("#" + dropDownId).val("All");
                
                return false;
            }  
        </script>

        <div class="date-range-close" style="display:none;">
            <a runat="server" id="btnCloseCustomRange">
                <img alt="close" src="~/App_Themes/General/images/menu-close.png" runat="server" /></a>
        </div>
        <div class="form-row">
            <div class="form-value calendar-value">
                <span>
                    <asp:TextBox ID="txtFromDate" runat="server" Text="<%$ Resources:GUIStrings, FromDate %>"
                        CssClass="textBoxes" Width="170" />
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, FromDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectFromDate %>" ID="imgFromDate" runat="server"
                        CssClass="calenderButton" />
                </span>
                <asp:HiddenField ID="hdnFromDate" runat="server" />
                <asp:CompareValidator CultureInvariantValues="true" ID="valFromDate" runat="server"
                    Type="Date" ControlToValidate="txtFromDate" Operator="DataTypeCheck" ValidationGroup="DateRange"
                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDateForFromDate %>"
                    Display="None" />
                <ComponentArt:Calendar runat="server" ID="CalendarFrom" SkinID="Default" />
            </div>
        </div>
        <div class="form-row">
            <div class="form-value calendar-value">
                <span>
                    <asp:TextBox ID="txtToDate" runat="server" Text="<%$ Resources:GUIStrings, ToDate %>"
                        CssClass="textBoxes" Width="170" />
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, ToDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectToDate %>" ID="imgToDate" runat="server"
                        CssClass="calenderButton" />
                </span>
                <asp:HiddenField ID="hdnToDate" runat="server" />
                <asp:CompareValidator CultureInvariantValues="true" ID="valToDate" runat="server"
                    Type="Date" ControlToValidate="txtToDate" Operator="DataTypeCheck" ValidationGroup="DateRange"
                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDateForToDate %>" Display="None" />
                <ComponentArt:Calendar runat="server" ID="CalendarTo" SkinID="Default" />
            </div>
        </div>
    </asp:Panel>
    <asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" ID="valSummary"
        ValidationGroup="DateRange" />
</asp:Panel>
