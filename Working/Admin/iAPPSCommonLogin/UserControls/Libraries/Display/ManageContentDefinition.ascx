<%@ Control Language="C#" AutoEventWireup="True" Codebehind="ManageContentDefinition.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageContentDefinition" %>
<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<script type="text/javascript">
    function grdXmlForm_OnLoad(sender, eventArgs) {
        $(".display-library").gridActions({ objList: grdXmlForm });
    }

    function LoadLibraryGrid() {
        $(".display-library").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function grdXmlForm_OnRenderComplete(sender, eventArgs) {
        $(".display-library").gridActions("bindControls");
        $(".display-library").iAppsSplitter();
    }

    function grdXmlForm_OnCallbackComplete(sender, eventArgs) {
        $(".display-library").gridActions("displayMessage");
    }

    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;
        switch (gridActionsJson.Action) {
            case "EditSequence":
                doCallback = false;
                displayPopupWindow = dhtmlmodal.open('DisplayOrder', 'iframe', 'DisplayOrderPopup.aspx?ObjectType=XmlForm', '', '');
                break;
            case "Edit":
                doCallback = false;
                EditContentDefinitionDetails(selectedItemId);
                break;
            case "Add":
                doCallback = false;
                EditContentDefinitionDetails(null);
                break;
        }

        if (doCallback && typeof grdXmlForm != "undefined") {
            grdXmlForm.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdXmlForm.callback();
        }
    }

    function RefreshContentLibrary() {
        $(".display-library").gridActions("callback", { refreshTree: true });
    }

    function EditContentDefinitionDetails(xmlFormId) {
        var queryString = 'NodeId=' + gridActionsJson.FolderId;
        if (xmlFormId != null)
            queryString += '&ContenteDefinitionId=' + xmlFormId;

        OpeniAppsAdminPopup("ManageContentDefinitionDetails", queryString, "RefreshContentLibrary");
    }

    function Grid_ItemSelect(sender, eventArgs) {
        if (eventArgs.selectedItem.getMember("Status").get_text() == 1) {
            sender.getItemByCommand("Archive").setText("<%= JSMessages.Archive %>");
            sender.getItemByCommand("Archive").setComandArgument("Archive");
            sender.getItemByCommand("Archive").removeClass("activate");
            sender.getItemByCommand("Archive").addClass("deactivate");
        }
        else {
            sender.getItemByCommand("Archive").setText("<%= JSMessages.MakeActive %>");
            sender.getItemByCommand("Archive").setComandArgument("MakeActive");
            sender.getItemByCommand("Archive").removeClass("deactivate");
            sender.getItemByCommand("Archive").addClass("activate");
        }
    }
</script>
<div class="left-control">
    <UC:LibraryTree ID="libraryTree" runat="server" />
</div>
<div class="right-control">
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <ComponentArt:Grid ID="grdXmlForm" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
        CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdXmlFormLoadingPanelTemplate">
        <ClientEvents>
            <Load EventHandler="grdXmlForm_OnLoad" />
            <CallbackComplete EventHandler="grdXmlForm_OnCallbackComplete" />
            <RenderComplete EventHandler="grdXmlForm_OnRenderComplete" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                HoverRowCssClass="hover-row">
                <ConditionalFormats>
                    <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Status').Value==7"
                        RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                </ConditionalFormats>
                <Columns>
                    <ComponentArt:GridColumn DataField="Name" DataCellServerTemplateId="DetailsTemplate" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                    <ComponentArt:GridColumn DataField="XMLFileName" Visible="false" IsSearchable="true" />
                    <ComponentArt:GridColumn DataField="XSLTFileName" Visible="false" IsSearchable="true" />
                    <ComponentArt:GridColumn DataField="StatusName" Visible="false" />
                    <ComponentArt:GridColumn DataField="Status" Visible="false" />
                    <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                    <ComponentArt:GridColumn DataField="ModifiedByName" Visible="false" />
                    <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="true" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                <Template>
                    <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["Id"]%>'>
                        <%--<div class="first-cell">
                            <%# GetGridItemRowNumber(Container.DataItem) %>
                        </div>--%>
                        <div class="large-cell">
                            <h4>
                                <%# Container.DataItem["Name"]%>
                            </h4>
                            <p>
                                <%# Container.DataItem["Name"].ToString() != Container.DataItem["Description"].ToString() ? Container.DataItem["Description"] : ""%>
                            </p>
                        </div>
                        <div class="small-cell">
                            <div class="child-row">
                                <%# String.Format("<strong>{0}</strong>: {1} | {2}", GUIStrings.FileName, Container.DataItem["XMLFileName"], Container.DataItem["XSLTFileName"])%>
                            </div>
                            <div class="child-row">
                                <%# String.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}",Container.DataItem["ModifiedDate"]),
                                    GUIStrings.By, Container.DataItem["ModifiedByName"])%>
                            </div>
                            <div class="child-row">
                                <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["StatusName"])%>
                            </div>
                        </div>
                    </div>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="grdXmlFormLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdXmlForm) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</div>
