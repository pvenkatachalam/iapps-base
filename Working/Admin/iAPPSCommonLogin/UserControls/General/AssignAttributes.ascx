﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignAttributes.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.AssignAttributes" %>
<script type="text/javascript">
    $(function () {
        BindAttributeValues();
    });
</script>
<div class="attribute-values" id="attribute_values">
    <div data-bind="foreach: Attributes">
        <div class="form-row" data-bind="css: ValidationCss">
            <label class="form-label" data-bind="html: DisplayTitle"></label>
            <input type="hidden" data-bind="value: Id" />
            <div class="form-value" data-bind="css: ControlType, template: 'controlTemplate'"></div>
        </div>
    </div>
</div>
<script id="controlTemplate" type="text/html">
    <!-- ko if: ControlType() == 'textbox'-->
    <input type="text" data-bind="value: SelectedValue" />
    <!-- /ko -->
    <!-- ko if: ControlType() == 'dropdownlist'-->
    <select data-bind='options: Options, optionsText: "Title", optionsValue: "Id", value: SelectedValue'></select>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'checkbox'-->
    <input type="checkbox" data-bind="checked: SelectedValue" />
    <!-- /ko -->
    <!-- ko if: ControlType() == 'checkboxlist'-->
    <div data-bind='foreach: Options'>
        <div class="list-item">
            <input type="checkbox" data-bind="checked: $parent.SelectedValues, value: Id">
            <span data-bind='text: Title'></span>
        </div>
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'radiobuttonlist'-->
    <div data-bind='foreach: Options'>
        <div class="list-item">
            <input type="radio" data-bind="checked: $parent.SelectedValue, value: Id != EmptyGuid ? Id : Value" />
            <span data-bind='text: Title'></span>
        </div>
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'dateselector'  -->
    <div class="value-selector date-selector">
        <img class="open-button" src="~/App_Themes/General/images/calendar-icon.png" data-bind="click: OpenDateSelector" />
        <input type="text" class="picker" data-bind="value: SelectedValue, attr: { placeholder: PlaceHolder, 'data-minValue': MinValue, 'data-maxValue': MaxValue }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'editor'  -->
    <div class="value-selector html-editor">
        <img class="open-button" src="~/App_Themes/General/images/browse-icon.png" data-bind="click: OpenHtmlEditor" />
        <input type="text" data-bind="value: SelectedText, attr: { placeholder: PlaceHolder }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'fileupload'  -->
    <div class="value-selector file-uploader">
        <img class="open-button" src="~/App_Themes/General/images/browse-icon.png" data-bind='click: OpenFileUploader' />
        <input type="text" data-bind="value: SelectedText, attr: { placeholder: PlaceHolder }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'pagelibrary'  -->
    <div class="value-selector library-selector">
        <img class="open-button" src="~/App_Themes/General/images/browse-icon.png" data-bind='click: OpenPageLibrary' />
        <input type="text" data-bind="value: SelectedText, attr: { placeholder: PlaceHolder }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'contentlibrary'  -->
    <div class="value-selector library-selector">
        <img class="open-button" src="~/App_Themes/General/images/browse-icon.png" data-bind='click: OpenContentLibrary' />
        <input type="text" data-bind="value: SelectedText, attr: { placeholder: PlaceHolder }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'filelibrary'  -->
    <div class="value-selector library-selector">
        <img class="open-button" src="~/App_Themes/General/images/browse-icon.png" data-bind='click: OpenFileLibrary' />
        <input type="text" data-bind="value: SelectedText, attr: { placeholder: PlaceHolder }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
    <!-- ko if: ControlType() == 'imagelibrary'  -->
    <div class="value-selector library-selector">
        <img class="open-button" src="~/App_Themes/General/images/browse-icon.png" data-bind='click: OpenImageLibrary' />
        <input type="text" data-bind="value: SelectedText, attr: { placeholder: PlaceHolder }" readonly="readonly" />
        <img class="remove-button" src="~/App_Themes/General/images/cm-icon-delete.png" data-bind="click: RemoveSelectedValue" />
    </div>
    <!-- /ko -->
</script>
<asp:HiddenField ID="hdnSaveAttribute" runat="server" ClientIDMode="Static" />
