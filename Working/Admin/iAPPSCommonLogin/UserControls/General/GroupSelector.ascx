﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupSelector.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.GroupSelector" %>
<script type="text/javascript">
    function RefreshLibraryTree() {
        upGroupSelector_Callback();
    }

    function Group_Callback(sender, eventArgs) {
        treeActionsJson.Success = false;
        var $txtName = $("#<%= txtName.ClientID %>");
        switch (treeActionsJson.Action) {
            case "AddItem":
                $txtName.val("");
                $('#divManageGroups').iAppsDialog("open");
                $txtName.focus();
                break;
            case "EditItem":
                $txtName.val(treeActionsJson.Text).focus();
                $('#divManageGroups').iAppsDialog("open");
                $txtName.focus();
                break;
            case "DeleteItem":               
                if (window.confirm("Are you sure you want to delete this item?")) {
                    upGroupSelector_Callback();
                }
                break;            
        }
    }

    function Group_ItemSelect(sender, eventArgs) {
        if (typeof LoadItemsGrid == "function")
            LoadItemsGrid();
      
    }

    function upGroupSelector_OnLoad() {
        $(".group-container").groupActions();
    }

    function upGroupSelector_OnRenderComplete() {
        $(".group-container").groupActions("bindControls");
    }

    function upGroupSelector_OnCallbackComplete() {
        $(".group-container").groupActions("displayMessage");
    }

    function SaveManageGroup() {
        if (Page_ClientValidate("ManageGroup")) {
            treeActionsJson.NewText = $("#<%= txtName.ClientID %>").val();
            $("#divManageGroups").iAppsDialog("close");

            upGroupSelector_Callback();
        }
        return false;
    }

    function upGroupSelector_Callback() {
        upGroupSelector.set_callbackParameter(JSON.stringify(treeActionsJson));
        upGroupSelector.callback();
    }

    function CloseManageGroup() {
        $("#divManageGroups").iAppsDialog("close");

        return false;
    }

    $(function () {
        $("#<%= txtName.ClientID %>").onEnter(function (e) {
            SaveManageGroup();
            return false;
        });
    });

</script>
<iAppsControls:CallbackPanel ID="upGroupSelector" runat="server" OnClientLoad="upGroupSelector_OnLoad"
    OnClientRenderComplete="upGroupSelector_OnRenderComplete" CssClass="group-container"
    OnClientCallbackComplete="upGroupSelector_OnCallbackComplete">
    <ContentTemplate>
        <div class="group-header clear-fix">
            <iAppsControls:GroupActions ID="groupActions" runat="server" />
            <asp:Literal ID="ltGroupHeading" runat="server" />
        </div>
        <asp:ListView ID="lvGroups" runat="server" ItemPlaceholderID="phGroupsList">
            <LayoutTemplate>
                <div class="group-list">
                    <asp:PlaceHolder ID="phGroupsList" runat="server" />
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <asp:Panel ID="pnlItem" runat="server" CssClass="group-item clear-fix">
                    <asp:Label ID="lblTitle" runat="server" CssClass="group-item-title" />
                    <asp:Label ID="lblCount" runat="server" CssClass="group-item-count" />
                    <input type="hidden" id="hdnItemAction" runat="server" class="group-item-action" />
                </asp:Panel>
            </ItemTemplate>
        </asp:ListView>
    </ContentTemplate>
</iAppsControls:CallbackPanel>
<div class="iapps-modal group-item-popup" id="divManageGroups" style="display: none;">
    <div class="modal-header clear-fix">
        <h2>
            <asp:Literal ID="ltManageHeading" runat="server" Text="<%$ Resources:GUIStrings, ManageGroups %>" /></h2>
    </div>
    <div class="modal-content clear-fix">
        <asp:ValidationSummary ID="vsManageGroup" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="ManageGroup" />
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.Name %><span class="req">&nbsp;*</span></label>
            <div class="form-value">
                <asp:TextBox ID="txtName" runat="server" Width="200" />
                <asp:HiddenField ID="hdnGroupId" runat="server" />
                <asp:RequiredFieldValidator ID="reqtxtName" runat="server" ControlToValidate="txtName" ValidationGroup="ManageGroup"
                    Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseProvideValueForName %>" />
            </div>
        </div>
    </div>
    <div class="modal-footer clear-fix">
        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CloseManageGroup();" />
        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" ValidationGroup="ManageGroup" OnClientClick="return SaveManageGroup();" />
    </div>
</div>
