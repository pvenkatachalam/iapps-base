<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.RegisterLic"
    CodeBehind="RegisterLic.aspx.cs" StylesheetTheme="General" MasterPageFile="~/General/MainMaster.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <meta name="robots" content="noindex, nofollow" />
    <link rel="shortcut icon" href="/commerceadmin/favicon.ico" />
    <script type="text/javascript" src="../common/js/JSMessages.js.aspx"></script>
    <script type="text/javascript" src="../Script/General.js"></script>
    <script type="text/javascript">
        var iAppsProductsSuiteClientId;
        $(document).ready(function () {
            $('.shadow-box').css('top', $('.login-block').outerHeight());
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="top-section clear-fix product-registration">
        <!-- Message Information Starts -->
        <div id="div1" class="messageContainer" runat="server" visible="false">
        </div>
        <!-- Message Information Ends -->
        <div class="login-block register">
            <h2>
                <asp:Image ID="imgiAPPS" runat="server" ImageUrl="~/App_Themes/General/images/iapps-logo.png" AlternateText="<%$ Resources:GUIStrings, iAPPS %>" /></h2>
            <div class="form-row">
                <asp:Label ID="lblResult" runat="server"></asp:Label>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Label ID="lblProduct" Text="<%$ Resources:GUIStrings, SelectProductColon %>" runat="server" /></label>
            </div>
            <div class="form-row" style="display: none;">
                <div class="form-value">
                    <asp:DropDownList ID="iAppsProductsSuite" runat="server" Width="280" ClientIDMode="Static">
                        <asp:ListItem Value="CMS" Text="<%$ Resources:GUIStrings, ContentManager %>"></asp:ListItem>
                        <asp:ListItem Value="ANALYTICS" Text="<%$ Resources:GUIStrings, Analyzer %>"></asp:ListItem>
                        <asp:ListItem Value="COMMERCE" Text="<%$ Resources:GUIStrings, Commerce %>"></asp:ListItem>
                        <asp:ListItem Value="MARKETIER" Text="<%$ Resources:GUIStrings, Marketier %>"></asp:ListItem>
                        <asp:ListItem Value="SOCIAL" Text="<%$ Resources:GUIStrings, Social %>"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-row products clear-fix">
                <input type="button" class="cms-button" value="Content Manager" tabindex="4" onclick="return fn_SelectProduct('cms', this);" />
                <input type="button" class="analyzer-button" value="Analyzer" tabindex="5" onclick="return fn_SelectProduct('analyzer', this);" />
                <input type="button" class="marketier-button" value="Marketier" tabindex="6" onclick="return fn_SelectProduct('marketier', this);" />
                <input type="button" class="commerce-button" value="Commerce" tabindex="7" onclick="return fn_SelectProduct('commerce', this);" />
                <input type="button" class="social-button" value="Social" tabindex="8" onclick="return fn_SelectProduct('social', this);" />
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Label ID="lblLic" Text="<%$ Resources:GUIStrings, LicenseKeyColon %>" runat="server"></asp:Label></label>
            </div>
            <div class="form-row">
                <div class="form-value">
                    <asp:TextBox ID="txtLK1" runat="server" MaxLength="5" Width="60" CssClass="textBoxes"></asp:TextBox>-<asp:TextBox ID="txtLK2" runat="server" MaxLength="5" Width="60" CssClass="textBoxes"></asp:TextBox>-<asp:TextBox ID="txtLK3" runat="server" MaxLength="5" Width="60" CssClass="textBoxes"></asp:TextBox>-<asp:TextBox ID="txtLK4" runat="server" MaxLength="5" Width="60" CssClass="textBoxes"></asp:TextBox>-<asp:TextBox ID="txtLK5" runat="server" MaxLength="5" Width="60" CssClass="textBoxes"></asp:TextBox>-<asp:TextBox ID="txtLK6" runat="server" MaxLength="1" Width="20" CssClass="textBoxes"></asp:TextBox>
                </div>
            </div>
            <div class="form-row">
                <p>Don�t have a license? &nbsp;<asp:LinkButton ID="btnDemo" runat="server" Text="Install the Demo." OnClick="btnDemo_Click" /></p>
            </div>
            <div class="form-row clear-fix">
                <asp:Button ID="btnGo" runat="server" Text="<%$ Resources:GUIStrings, Login1 %>"
                    OnClick="btnGo_Click" CssClass="button" style="float: left;" />
                <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:GUIStrings, Activate %>"
                    OnClick="btnSubmit_Click" CssClass="primarybutton" style="float: right" />
            </div>
        </div>
    </div>
</asp:Content>
