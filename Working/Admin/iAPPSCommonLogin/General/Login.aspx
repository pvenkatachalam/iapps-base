<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CommonLogin.Web.Login"
    MasterPageFile="~/General/MainMaster.Master" StylesheetTheme="General" CodeBehind="Login.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var hasCMSLicense;
        var hasAnalyticsLicense;
        var hasCommerceLicense;
        var hasMarketierLicense;
        var hasSocialLicense;
        var jAdminSiteUrl;

        var hasCMSLicense_SiteLevel;
        var hasAnalyticsLicense_SiteLevel;
        var hasCommerceLicense_SiteLevel;
        var hasMarketierLicense_SiteLevel;
        var hasSocialLicense_SiteLevel;

        function openProductWindow(url) {
            window.open(url);
            return false;
        }
        function loadLogin() {
            if (window.frameElement != null) {
                window.parent.location = window.location;
            }
        }
        var iAppsProductsSuiteClientId;
        function CheckLicensePermission() {
            if (Page_ClientValidate("iAppsLoginCheck")) {
                var selectedProductObj = document.getElementById(iAppsProductsSuiteClientId);
                var selectedProduct = '';
                if (selectedProductObj != null) {
                    for (i = 0; i < selectedProductObj.length; i++) {
                        if (selectedProductObj[i].selected) {
                            selectedProduct = selectedProductObj[i].text;
                            break;
                        }
                    }
                }

                if ((hasCMSLicense != 'True' || hasCMSLicense_SiteLevel != 'True') && selectedProduct == 'Content Manager') {
                    OpenLicenseWarningPopup(selectedProduct, false, false);
                    return false;
                }
                else if ((hasAnalyticsLicense != 'True' || hasAnalyticsLicense_SiteLevel != 'True') && selectedProduct == 'Analyzer') {
                    OpenLicenseWarningPopup(selectedProduct, false, false);
                    return false;
                }
                else if ((hasCommerceLicense != 'True' || hasCommerceLicense_SiteLevel != 'True') && selectedProduct == 'Commerce') {
                    OpenLicenseWarningPopup(selectedProduct, false, false);
                    return false;
                }
                else if ((hasMarketierLicense != 'True' || hasMarketierLicense_SiteLevel != 'True') && selectedProduct == 'Marketier') {
                    OpenLicenseWarningPopup(selectedProduct, false, false);
                    return false;
                }
                else if ((hasSocialLicense != 'True' || hasSocialLicense_SiteLevel != 'True') && selectedProduct == 'Social') {
                    OpenLicenseWarningPopup(selectedProduct, false, false);
                    return false;
                }
            }
        }
        function ShowForgotPassword() {
            url = "../Popups/ForgotPassword.aspx";
            forgotPasswordWindow = dhtmlmodal.open('', '', url, '', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
        }

        function ShowChangePassword(username) {
            url = "../Popups/ChangePassword.aspx?username=" + username;
            changePasswordWindow = dhtmlmodal.open('', '', url, '', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="top-section clear-fix">
        <div class="left-section">
            <div class="login-block">
                <h2>
                    <asp:Image ID="logo" AlternateText="<%$ Resources:GUIStrings, iAPPS %>" ImageUrl="~/App_Themes/General/images/iapps-logo.png" runat="server" ToolTip="<%$ Resources:GUIStrings, iAPPS %>" /></h2>
                <asp:Login ID="iAppsLogin" runat="Server" TitleText="" PasswordRecoveryText="<%$ Resources:GUIStrings, PasswordRetrieval %>"
                    PasswordRecoveryUrl="~/Popups/ForgotPassword.aspx" DisplayRememberMe="false"
                    FailureText="<%$ Resources:GUIStrings, FailureText %>">
                    <LayoutTemplate>
                        <div class="form-row" style="color: #ff0000;">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False" Visible="false"></asp:Literal>
                        </div>
                        <div class="form-row">
                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName" CssClass="form-label" Text="<%$ Resources:GUIStrings, EnterUsername %>" />
                        </div>
                        <div class="form-row double-padding">
                            <asp:TextBox ID="UserName" runat="server" Width="440" TabIndex="1" autocomplete="off"></asp:TextBox>
                        </div>
                        <div class="form-row clear-fix">
                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="form-label" Style="float: left;" Text="<%$ Resources:GUIStrings, EnterPassword %>" />
                        </div>
                        <div class="form-row">
                            <asp:TextBox ID="Password" runat="server" Width="440" TextMode="Password" TabIndex="2" autocomplete="off"></asp:TextBox>
                        </div>
                        <div class="form-row clear-fix">
                            <asp:HyperLink ID="PasswordRecoveryLink" TabIndex="11" runat="server" NavigateUrl="javascript: ShowForgotPassword()" Style="float: right;" Text="<%$ Resources:GUIStrings, PasswordRetrieval %>" />
                        </div>
                        <div id="diviAppsProductSuite" runat="server">
                            <div class="form-row">
                                <asp:Label ID="SuiteLabel" runat="server" AssociatedControlID="iAppsProductsSuite" CssClass="form-label" Text="<%$ Resources:GUIStrings, SelectaProduct %>" />
                            </div>
                            <div class="form-row" style="display: none;">
                                <asp:DropDownList ID="iAppsProductsSuite" runat="server" Width="450" TabIndex="3" ClientIDMode="Static" />
                            </div>
                            <div class="form-row products clear-fix double-padding">
                                <asp:PlaceHolder ID="phCMS" runat="server" Visible="false">
                                    <input type="button" class="cms-button" value="Content Manager" tabindex="4" onclick="return fn_SelectProduct('cms', this);" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="phAnalyzer" runat="server" Visible="false">
                                    <input type="button" class="analyzer-button" value="Analyzer" tabindex="5" onclick="return fn_SelectProduct('analyzer', this);" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="phMarketier" runat="server" Visible="false">
                                    <input type="button" class="marketier-button" value="Marketier" tabindex="6" onclick="return fn_SelectProduct('marketier', this);" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="phCommerce" runat="server" Visible="false">
                                    <input type="button" class="commerce-button" value="Commerce" tabindex="7" onclick="return fn_SelectProduct('commerce', this);" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="phSocial" runat="server" Visible="false">
                                    <input type="button" class="social-button" value="Social" tabindex="8" onclick="return fn_SelectProduct('social', this);" />
                                </asp:PlaceHolder>
                            </div>
                            <div class="form-row">
                                <asp:CheckBox ID="RememberMe" runat="server" Text="<%$ Resources:GUIStrings, Remembermyselection %>" TabIndex="9" />
                            </div>
                        </div>
                        <asp:Button ID="LoginButton" runat="server" CssClass="primarybutton login-button" Text="<%$ Resources:GUIStrings, Login %>"
                            ToolTip="<%$ Resources:GUIStrings, Login1 %>" CausesValidation="true" ValidationGroup="iAppsLoginCheck"
                            OnClick="iAppsLogin_LoggingIn" OnClientClick="return CheckLicensePermission();" TabIndex="10" UseSubmitBehavior="true" />
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="<%$ Resources:JSMessages, UserNameRequired %>" ValidationGroup="iAppsLoginCheck" SetFocusOnError="true" Display="None">&nbsp;</asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="<%$ Resources:JSMessages, PasswordRequired %>" ValidationGroup="iAppsLoginCheck" SetFocusOnError="true" Display="None">&nbsp;</asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName" ErrorMessage="<%$ Resources:JSMessages, InvalidInput %>" SetFocusOnError="True" ValidationExpression="^[^<>]+$" ValidationGroup="iAppsLoginCheck" Display="None">&nbsp;</asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Password"
                            ErrorMessage="<%$ Resources:JSMessages, InvalidCharactersInPassword %>" SetFocusOnError="True"
                            ValidationExpression="^[^<>]+$" ValidationGroup="iAppsLoginCheck" Display="None">&nbsp;</asp:RegularExpressionValidator>
                        <asp:ValidationSummary ID="summaryValidation" runat="Server" ValidationGroup="iAppsLoginCheck" EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" DisplayMode="List" />
                    </LayoutTemplate>
                </asp:Login>
            </div>
        </div>
        <div class="right-section">
            <div class="clear-fix login-content isg-help" message-key="isg message"></div>
            <div class="clear-fix login-content notification" message-key="notification"></div>
        </div>
    </div>

    <script type="text/javascript">
        var isEnterKey = 0;
        $(function () {
            $(".login-content").first().html(stringformat("<img class='iapps-loading' src='{0}' />", jSpinnerUrl));
            $.get("/CommonLogin/api/fwcallback/GetLoginPageContent?serverName=<%= Environment.MachineName %>", function (val) {
                if (val != "") {
                    var contentJson = JSON.parse(val);
                    $(".login-content").each(function () {
                        var sectionName = $(this).attr("message-key");
                        if (typeof sectionName != "undefined" && sectionName != "" &&
                            typeof contentJson[sectionName] != "undefined" && contentJson[sectionName] != "") {
                            $(this).html(contentJson[sectionName]);
                            $(this).show();
                        }
                    });
                }
            }).always(function () {
                $(".login-content").first().find("img.iapps-loading").hide();
            });
        });
        $('input[type=text], input[type=password]').on('keypress', function (e) {
            if (e.which == '13') {
                $('.login-button').click();
            }
        });
        $('.products input[type=button]').on('keypress', function (e) {
            if (e.which == '13') {
                isEnterKey = isEnterKey + 1;
            }
        });
        function fn_SubmitForm(evt) {
            if (isEnterKey == 2 && $('#' + txtUserName).val() != '' && $('#' + txtPassword).val() != '') {
                isEnterKey = 0;
                $('.login-button').click();
            }
        }
    </script>
</asp:Content>
