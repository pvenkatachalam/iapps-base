<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageContentDefinition"
    MasterPageFile="~/MasterPage.master" StylesheetTheme="General" CodeBehind="ManageContentDefinition.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="Library" Src="~/UserControls/Libraries/Display/ManageContentDefinition.ascx" %>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.ContentDefinitions %></h1>
    <UC:Library ID="manageContentDefinition" runat="server"></UC:Library>
</asp:Content>
