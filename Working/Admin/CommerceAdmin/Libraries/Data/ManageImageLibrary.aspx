<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageImageLibrary" MasterPageFile="~/MasterPage.master"
    Theme="General" ValidateRequest="false" CodeBehind="ManageImageLibrary.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="ImageLibrary" Src="~/UserControls/Libraries/Data/ManageAssetImage.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.Images %></h1>
    <UC:ImageLibrary ID="ctlImageLibrary" runat="server" />
</asp:Content>
