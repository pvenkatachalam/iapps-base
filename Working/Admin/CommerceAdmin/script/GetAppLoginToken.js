﻿function sendRequest(url, callback, postData) {
    var req = createXMLHTTPObject();
    if (!req) return;
    var method = (postData) ? "POST" : "GET";
    req.open(method, url, true);
    req.setRequestHeader('User-Agent', 'XMLHTTP/1.0');
    if (postData)
        req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.onreadystatechange = function () {
        if (req.readyState != 4) return;
        if (req.status != 200 && req.status != 304) {
            //			alert('HTTP error ' + req.status);
            return;
        }
        callback(req);
    }
    req.send(postData);

    if (req.readyState == 4) return;
}

var XMLHttpFactories = [
	function () { return new XMLHttpRequest() },
	function () { return new ActiveXObject("Msxml2.XMLHTTP") },
	function () { return new ActiveXObject("Msxml3.XMLHTTP") },
	function () { return new ActiveXObject("Microsoft.XMLHTTP") }
];

function createXMLHTTPObject() {
    var xmlhttp = false;
    for (var i = 0; i < XMLHttpFactories.length; i++) {
        try {
            xmlhttp = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    return xmlhttp;
}


function querySt(qsKey) {
    hu = window.location.search.substring(1);
    gy = hu.split("&");
    for (i = 0; i < gy.length; i++) {
        ft = gy[i].split("=");
        if (ft[0] == qsKey) {
            return ft[1];
        }
    }
}

function cleanPreviousQS() {

    var curDocUrl = document.URL;
    // known qs parts which should not go back to front end
    var backToAdminKey = 'BackToAdmin';
    var backToAdminValue = querySt(backToAdminKey);

    var lastFrontEndKey = 'LastFrontEndPage'
    var lastFrontEndKeyValue = querySt(lastFrontEndKey);

    var lastTokenKey = 'Token';
    var lastTokenValue = querySt(lastTokenKey);

    var cleanedUpUrl = curDocUrl.replace(backToAdminKey + "=" + backToAdminValue + '&', '');
    cleanedUpUrl = cleanedUpUrl.replace(lastFrontEndKey + "=" + lastFrontEndKeyValue + '&', '');
    cleanedUpUrl = cleanedUpUrl.replace(lastTokenKey + "=" + lastTokenValue + '&', '');
    // if previos statement does not work then
    cleanedUpUrl = cleanedUpUrl.replace(lastTokenKey + "=" + lastTokenValue, '');

    if (cleanedUpUrl.lastIndexOf('?&', 0))
        cleanedUpUrl = cleanedUpUrl.replace('?&', '?');

    return cleanedUpUrl;
}

function handleTokenResponse(req) {
    var callResponse = req.responseText;
    var guid = '00000000-0000-0000-0000-000000000000';
    if (callResponse.length == guid.length) {
        if (callResponse != 'Invalid') {
            var destinationUrl = "";
            var currentUrl = cleanPreviousQS();
            if (currentUrl == null || currentUrl == '') {
                currentUrl = document.referrer; // if it is a popup, we have to take the referrer page instead of popup url; so we have to change line number 1 with line number 4 in popup
            }
            if (currentUrl.indexOf('?') > 0) {
                currentUrl = currentUrl.replace('?', '^');
                currentUrl = currentUrl.replace('&', '^');
            }
            if (itemContext == 'Editor' || itemContext == 'JumpToHome') {
                destinationUrl = jPublicSiteUrl + '/Default.aspx?LastVisitedeCommerceAdminPageUrl=' + currentUrl + '&Token=' + callResponse + '&PageState=Edit';
            }
            else if (itemContext == 'JumpToLastVisitedPage') {
                if (jLastVisitedFrontEndPage == null || jLastVisitedFrontEndPage == '') {
                    jLastVisitedFrontEndPage = "Default.aspx";
                }
                destinationUrl = jPublicSiteUrl + '/' + jLastVisitedFrontEndPage + '?LastVisitedeCommerceAdminPageUrl=' + currentUrl + '&Token=' + callResponse + '&PageState=Edit';
            }
            if (itemContext == 'Editor' || itemContext == 'JumpToHome' || itemContext == 'JumpToLastVisitedPage') {
                window.location = destinationUrl;
            }
        }
    }
    else {
        window.location = jCommerceAdminUrl;
    }
}

function handleResponseWithReturnFunction(req) {
    var callResponse = req.responseText;
    var emptyGuid = '00000000-0000-0000-0000-000000000000';

    if (callResponse.length == emptyGuid.length) {
        if (callResponse != 'Invalid') {
            //check '?' we have to add here
            if (navigateURL.indexOf('?') > -1) {
                destinationUrl = navigateURL + '&Token=' + callResponse;
            }
            else {
                destinationUrl = navigateURL + '?Token=' + callResponse;
            }
            setTimeout(functionToCallAfterTokenCallback, 1000);
        }
    }
}

var navigateURL;
var itemContext;
function SendTokenRequest(itemId) {
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCMSProductId + "&userName=" + juserName;
    var urlToCall = jCommerceAdminUrl + '/' + tokenPageUrl;
    var itemId = itemContext = itemId;

    switch (itemId) {
        case 'Editor':
        case 'JumpToHome':
        case 'JumpToLastVisitedPage':
            sendRequest(urlToCall, handleTokenResponse, "GET");
            break;
        case 'CheckForUpdates':
            OpenVersionCheckPopup();
            break;
        case 'Analytics':
        case 'Analyzer':
            if (!hasAnalyticsLicense || !hasAnalyticsPermission) {
                //call popup to display message
                OpenLicenseWarningPopup(id, hasAnalyticsLicense, hasAnalyticsPermission);
            }
            break;
        case "Marketier":
            if (!hasMarketierLicense || !hasMarketierPermission) {
                OpenLicenseWarningPopup(id, hasMarketierLicense, hasMarketierPermission);
            }
            break;
        case "Social":
            if (!hasSocialLicense || !hasSocialPermission) {
                OpenLicenseWarningPopup(id, hasSocialLicense, hasSocialPermission);
            }
            break;
        case "CMS":
            if (!hasCMSLicense || !hasCMSPermission) {
                OpenLicenseWarningPopup(id, hasCMSLicense, hasCMSPermission);
            }
            break;
    }
}


var functionToCallAfterTokenCallback;
function SendTokenRequestByURLForContentDefinitionPopup(redirectURL) {
    destinationUrl = "";
    var tokenPageUrl = "LoginTokenProvider.aspx?userId=" + jUserId + "&appId=" + jAppId + "&productId=" + jCMSProductId + "&userName=" + juserName;
    var urlToCall = jCommerceAdminUrl + '/' + tokenPageUrl;
    navigateURL = redirectURL;
    functionToCallAfterTokenCallback = "OpenContentDefinitionPopup(destinationUrl)";

    sendRequest(urlToCall, handleResponseWithReturnFunction, "GET");
}

function SendTokenRequestByURLWithMethodName(redirectURL, methodName) {
    destinationUrl = redirectURL;
    functionToCallAfterTokenCallback = methodName;
    setTimeout(functionToCallAfterTokenCallback, 10);
}
