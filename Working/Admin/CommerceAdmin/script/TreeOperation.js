﻿// Whatever method starting with PageLevel has to implement the page

var newNode;        // child node being added to the selected node
var treeNode;       // selected tree node
var nodeId;         // id of the node where context menu is called
var treeOperation;  // operation being performed on tree node e.g. Add New Node / Rename
var isTreeCallback;     // is in Call back or not
var isRenameInProgess = 'false';
    
var selectedTreeNode ;
var selectedNodeId  ;
var emptyGuid = '00000000-0000-0000-0000-000000000000'; 

var treeObject;                  //this is tree object using in your page.. set value to this at the end of the page
var treeMenuObject;              //This is the menu using in your page. set value to this at the end of the page
var treeObjectName;             //This is the name of the tree;

// Checking before node select
function tree_onNodeBeforeSelect(sender, eventArgs)
{
    if (isTreeCallback == 'true')
    {
        eventArgs.set_cancel(true);
    }
    if (eventArgs.get_node()!=null)
    {
        var indexOfSelectedNode = eventArgs.get_node().get_depth();
        if (indexOfSelectedNode == 0)   // Node Selection is not avialable for the first node
        {
            eventArgs.set_cancel(true);
        }
    } 
    PageLevelOnNodeBeforeSelect(sender, eventArgs);
}
    
// While selecting a node in the tree.. 
function tree_onNodeSelect(sender, eventArgs)
{   
    if (treeOperation !='CreateNewNode')  // new node creation not completed
    { 
        selectedTreeNode = eventArgs.get_node();
        selectedTreeNodeId = eventArgs.get_node().get_id();     
        selectedNodeId = selectedTreeNodeId;    // selectedNodeId is using in Tree operations and selectedTreeNodeId in GridOperations and declare in respective location. and both are same
        treeOperation = 'Load';
        PageLevelOnNodeSelect(sender, eventArgs);
    }    
}
 
//Context menu for Tree.. disable or enable
function tree_onContextMenu(sender, eventArgs)
{
    if (isRenameInProgess != 'true' && isTreeCallback !='true')
    {
        var e = eventArgs.get_event();
        selectedTreeNode = eventArgs.get_node();
        var indexOfSelectedNode = selectedTreeNode.get_depth();
        var menuItems = treeMenuObject.get_items(); 
        
        if (TreeContextMenuGeneralChecking(menuItems, indexOfSelectedNode)  && PageLevelTreeOnContextMenu(menuItems, indexOfSelectedNode))
        {	            
            treeMenuObject.showContextMenuAtEvent(e, selectedTreeNode);              
        }
    }        
} 

// if it is normal Add, Delete, Raname menus will handle here.. other than this, 
// if you want to overcome something in pagelevel, you can write code in PageLevelOnContextMenu() method. otherwise just return true.
function TreeContextMenuGeneralChecking(menuItems, indexOfSelectedNode)
{
    var isAnyMenuVisible =false;
    if (indexOfSelectedNode == 0)   // Only Add Context is avialable for the first node
    {
        for(i=0;i<menuItems.get_length();i++)
        {
            if(menuItems.getItem(i).get_id()=='cmTreeAdd')
            {
                isAnyMenuVisible = true;                
            }
            else
            {
                menuItems.getItem(i).set_visible(false);
            }
       }
    }
    else
    {
        for(i=0;i<menuItems.get_length();i++)
        {
              menuItems.getItem(i).set_visible(true);
              isAnyMenuVisible = true;
        }
    }
    return true ;
}
// this functino calls while click on tree menu
function treeMenu_onItemClick(sender, eventArgs)
{
    var menuItem = eventArgs.get_item();
    var contextDataNode = menuItem.get_parentMenu().get_contextData();
    var selectedMenuId = menuItem.get_id();
    if (selectedMenuId!=null)
    {
        switch (selectedMenuId) 
        {
            case 'cmTreeAdd':
                sender.hide();
                AddTreeNode(contextDataNode, treeObject);            
                break;
            case 'cmTreeDelete' :
                sender.hide();
                RemoveTreeNode(contextDataNode);            
                break;
            case 'cmTreeRename':
                sender.hide();
                RenameTreeNode(contextDataNode);            
                break;
            default :
                PageLevelTreeOnItemSelect(selectedMenuId, contextDataNode, sender, eventArgs); //implement this method to handle extra items
                break;                
        }
        
    }
}
// creating new tree node
function AddTreeNode(contextMenuData, treeObject)
{
    newNode = null;
    treeObject.beginUpdate();
    newNode = new ComponentArt.Web.UI.TreeViewNode(); 
    newNode.set_text(__JSMessages["NewNode"]); 
    newNode.set_id('New Node');       
    
    PageLevelAddEmptyTreeNode(contextMenuData, treeObject,newNode); //implement this method in page to add extra functionalities
    
    contextMenuData.Expand();
    contextMenuData.get_nodes().add(newNode); 
    treeObject.endUpdate();
    treeObject.render();
    treeOperation = 'CreateNewNode';
    newNode.select();
    newNode.edit();
    nodeText = newNode.get_text()
    nodeId = contextMenuData.get_id();        
    treeOperation = 'New';    
}

function RemoveTreeNode(contextMenuData)
{
    treeOperation = 'Delete';
    var result = window.confirm(__JSMessages["AreYouSureYouWantToDeleteThisNodeItem"]);        
    if (result == true)
    {
        var childNodes = contextMenuData.get_nodes(); // Nods under one child of root
        if (childNodes.get_length() > 0 )
        {
            alert(__JSMessages["FirstDeleteTheChildNodesBeforeDeletingThis"]); 
        }
        else
        {
            isTreeCallback = 'true';            
            var deleteResult = PageLevelRemoveTreeNode(contextMenuData);
           
            if(deleteResult != null && deleteResult.toLowerCase() == 'true')
            {
                 contextMenuData.remove();
                 alert(__JSMessages["SuccessfullyDeleted"]);
            }
            else if (deleteResult != null) 
            {   
                alert(__JSMessages["ErrorDeleteNodeActionFailed"] + deleteResult);
            }
            isTreeCallback = 'false';
        }
    }
}    

function RenameTreeNode(contextMenuData)
{
   treeOperation='ReName';
   contextMenuData.edit();
   PageLevelRenameTreeNode(contextMenuData);
}

//Load will work in post back and call back . in postback selectedTreeNode is null and callback it has some value
function tree_Load(sender, eventArgs)
{
    if (isTreeCallback != 'true')
    { 
        SetTreeLevelGeneralVariablesinPage();
        if (selectedTreeNode!=null )
        {
            setTimeout(function() 
            {
                treeObject.selectNodeById(selectedNodeId);  
            }, 500);
        }           
        else
        { 
            var treeNodes = sender.get_nodes();
            if (treeNodes.get_length()>=1)
            {        
                var childNodes = treeNodes.getNode(0).get_nodes();
                if (childNodes.get_length()>=1)
                {
                    treeNodes.getNode(0).Expand();
                    var firstNodeId = childNodes.getNode(0).get_id();
                    setTimeout(function(){treeObject.selectNodeById(firstNodeId)},1000);    
                }
            }
        }
        PageLevelTreeLoad(sender, eventArgs);
    }
}

function PutInEditMode(nodeId)
{
    var node = treeObject.findNodeById(nodeId)
    node.edit();
}

function tree_BeforeRename(sender, eventArgs)
{
    var result =null;
    var node=eventArgs.get_node();
    var parNode= eventArgs.get_node().get_parentNode();   
    var newText = Trim(eventArgs.get_newText());
    var oldText =eventArgs.get_node().get_text();
    var parentId = parNode.get_id();
    var canProceed = true;
    
    if (newText==null || newText =='')
    {
       alert(__JSMessages["NodeNameShouldNotBeEmpty"]); 
       canProceed =false;
       window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
       isRenameInProgess = 'true';
    }
    else 
    {
//    alert(oldText);
//    alert(newText);
        
        if(!self['PageLevelValidateNode'])
        {
            if (!newText.match(/^[a-zA-Z0-9. _'/-<>.&$!|#:,]+$/))
            {
                if (IsNameAscii(newText)!='false')
                {
                   alert(__JSMessages["NodeNameCanHaveOnlyAlphanumericCharacters"]);            
                   canProceed =false;
                   eventArgs.set_cancel(true);
                   node.set_text(oldText);
                   window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
                   isRenameInProgess = 'true';
                }
            }  
            
            if(Trim(newText) =='&#0;' || newText.indexOf('&#0;') >0 )
            {
               alert(__JSMessages["IsUnicodeCombinationOfNewLineCharacterItIsNotAllowed"]); 
               canProceed =false;
               eventArgs.set_cancel(true);
               node.set_text(oldText);
               window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
               isRenameInProgess = 'true';            
            }            
        }
        else
        {
            canProceed = PageLevelValidateNode(newText);
            if(canProceed ==false)
            {
                eventArgs.set_cancel(true);
                node.set_text(oldText);
                window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
                isRenameInProgess = 'true';
            }
        }
       
        
        if (canProceed)
        {
            // check for same node name in the same level
            var childNodes = parNode.get_nodes();
            var currentNodeId = eventArgs.get_node().get_id();
            for (var i = 0; i < childNodes.get_length(); i++)
            {
                var childNodeText = Trim(childNodes.getNode(i).get_text());
                var childNodeId = childNodes.getNode(i).get_id();
                if(currentNodeId != childNodeId)
                {
                    if(childNodeId.length==36)
                    {
                        if(childNodeText.toLowerCase()  == newText.toLowerCase())
                        {
                            alert(__JSMessages["ThisNodeItemNameAlreadyExistsInThisLevel"]);
                            if(treeOperation!="New")
                            {
                                canProceed = false;
                                eventArgs.get_node().setProperty("Text", oldText);
                                //eventArgs.set_cancel(true); 
                                window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
                                isRenameInProgess = 'true';                            
                            }
                            else //Remove only in case of new node
                            {
                                canProceed =false;
                                eventArgs.get_node().setProperty("Text", '');
                                window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
                                isRenameInProgess = 'true';
                                //newNode.remove();
                            }
                        }
                    }
                }
            }
        }
    }
    
    if (canProceed)
    {
        if(treeOperation=='New')
        {
            // server side callback method
            isTreeCallback = 'true';
            var id = PageLevelCreateNewNode(newText,parentId, node);
            if(id != null && id != emptyGuid &&  id.toString().length==36)
            {          
                treeObject.beginUpdate();
                eventArgs.get_node().set_text(newText);       
                eventArgs.get_node().set_id(id); 
                treeObject.endUpdate(); 
                result='true';
                selectedNodeId = id;  
                selectedTreeNodeId =id;
                selectedTreeNode = eventArgs.get_node();
                setTimeout(function() 
                {
                    treeObject.selectNodeById(selectedNodeId);  
                }, 1);
                PageLevelLoadNewNode(selectedNodeId,eventArgs);  
                //selectedTreeNode.select();     // should handle in pagelevel, because some grid may be in callback
            }
            else
            {
              result='false';
              newNode.remove();
              alert(__JSMessages["ErrorNodeCreationFailed"] + id);
            } 
            isTreeCallback = 'false';           
        }
        else if (treeOperation=='ReName')
        {
            isTreeCallback = 'true';
            result = PageLevelEditNode(eventArgs.get_node().get_id(),eventArgs.get_newText(), node);            
            if (result != null && result.toLowerCase()=='true')
            {
                if (eventArgs.get_node() != null)
                {
                    treeObject.beginUpdate();
                    eventArgs.get_node().set_text(newText);  
                    treeObject.endUpdate();    
                    selectedNodeId = eventArgs.get_node().get_id();
                    selectedTreeNodeId = selectedNodeId;
                    selectedTreeNode = eventArgs.get_node();
                    setTimeout(function() 
                    {
                        treeObject.selectNodeById(selectedNodeId);  
                    }, 10);                    
                    PageLevelLoadEditedNode(selectedNodeId,sender, eventArgs);                         
                }        
            }
            else
            {
                eventArgs.get_node().setProperty('Text', oldText);
                alert(__JSMessages["ErrorRenameNodeItemActionFailed"] + result);
            }
            isTreeCallback = 'false';
        }
       
        if(result == null || (result!=null && result.toLowerCase()!='true'))
        {
            eventArgs.set_cancel(true); 
        }  
        isRenameInProgess = 'false';            
    }
}

function RefreshTree()
{ 
    if(treeObject != null)
        treeObject.dispose();
    var t=setTimeout("PageLevelRefreshTreeImmediate()",100)
}

function CheckForDuplicate(node,newText,showAlert)
{
    var returnVal=true;        
    var parNode= node.get_parentNode(); 
    var childNodes = parNode.get_nodes();
    var currentNodeId = node.get_id();
    for (var i = 0; i < childNodes.get_length(); i++)
    {
        var childNodeText = Trim(childNodes.getNode(i).get_text());
        var childNodeId = childNodes.getNode(i).get_id();
        
        if(currentNodeId != childNodeId)
        {
            if(childNodeId.length==36)
            {
                if(childNodeText.toLowerCase()  == newText.toLowerCase())
                {
                    if(showAlert)
                    {
                        alert(__JSMessages["DuplicateNameInThisLevel"]);
                    }
                    returnVal = false;
                }
            }
        }
    }
    
    return returnVal;
}


function CheckForDuplicatesWithParentNode(parentNode,newText,showAlert)
{
    var returnVal=true;       
    
    var childNodes = parentNode.get_nodes();    
    for (var i = 0; i < childNodes.get_length(); i++)
    {
        var childNodeText = Trim(childNodes.getNode(i).get_text());
        var childNodeId = childNodes.getNode(i).get_id();        
        
        if(childNodeId.length==36)
        {
            if(childNodeText.toLowerCase()  == newText.toLowerCase())
            {
                if(showAlert)
                {
                    alert(__JSMessages["DuplicateNameInThisLevel"]);
                }
                returnVal = false;
            }
        }
        
    }
    
    return returnVal;
}

function TreeLevelSpecialCharacterChecking(thisObj)
{
    var msg = "";
    if (!thisObj.match(/^[a-zA-Z0-9. _'/-<>.&$!|#:,]+$/))
    {
        if (IsNameAscii(thisObj)!='false')
        {
           msg += __JSMessages["TreeNodeNameCanHaveOnlyAlphanumericCharacters"];            
        }
    }
    if(Trim(thisObj) =='&#0;' || thisObj.indexOf('&#0;') >0 )
    {
        msg += __JSMessages["IsUnicodeCombinationOfNewLineCharacterItIsNotAllowed"]; 
    }    
    return msg;
}
