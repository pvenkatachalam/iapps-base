﻿if (!window.jQuery) {
    var documentProtocol = parent.location.protocol == "https:" ? "https://" : "http://";
    documentProtocol = documentProtocol + "ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js";
    var jsJquery = document.createElement("script");
    jsJquery.setAttribute("type", "text/javascript");
    jsJquery.setAttribute("src", documentProtocol);

    if (document.getElementsByTagName("head")) {
        document.getElementsByTagName("head").item(0).appendChild(jsJquery);
    }
    else {
        document.appendChild(jsJquery);
    }
}

/** Browser Detection Code **/
var detect = navigator.userAgent.toLowerCase();
var OS, browser, version, total, thestring;
var browserPlatform = navigator.platform.toLowerCase();
var macBrowser = navigator.userAgent.toLowerCase();

if (checkIt('safari')) browser = "safari";
else if (checkIt('msie')) browser = "ie";
else if (!checkIt('compatible')) {
    browser = "mozilla";
}
function checkIt(string) {
    place = detect.indexOf(string) + 1;
    thestring = string;
    return place;
}
function OScheckIt(string) {
    place = browserPlatform.indexOf(string) + 1;
    thestring = string;
    return place;
}
if (OScheckIt('mac')) OS = "mac";
else if (OScheckIt('win')) OS = "windows";
/** Browser Detection Code **/
//Method to change the primary button bg on mouse over and mouse out
function changePrimaryButton(objButton) {
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if (buttonClass.indexOf("Hover") > -1) {
        buttonClassNum = buttonClass.substring(13, 14);
        newButtonClass = "primarybutton" + buttonClassNum;
        oldButtonClass = "primarybutton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else {
        buttonClassNum = buttonClass.substring(13, 14);
        oldButtonClass = "primarybutton" + buttonClassNum;
        newButtonClass = "primarybutton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
//Method to change the button bg on mouse over and mouse out
function changeButton(objButton) {
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if (buttonClass.indexOf("Hover") > -1) {
        buttonClassNum = buttonClass.substring(6, 7);
        newButtonClass = "button" + buttonClassNum;
        oldButtonClass = "button" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else {
        buttonClassNum = buttonClass.substring(6, 7);
        oldButtonClass = "button" + buttonClassNum;
        newButtonClass = "button" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
//Method to change the arrow button bg on mouse over and mouse out
function changeArrowButton(objButton) {
    var buttonClass = objButton.className;
    var oldButtonClass = "";
    var newButtonClass = "";
    var buttonClassNum = "";
    if (buttonClass.indexOf("Hover") > -1) {
        buttonClassNum = buttonClass.substring(11, 12);
        newButtonClass = "arrowButton" + buttonClassNum;
        oldButtonClass = "arrowButton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
    else {
        buttonClassNum = buttonClass.substring(11, 12);
        oldButtonClass = "arrowButton" + buttonClassNum;
        newButtonClass = "arrowButton" + buttonClassNum + "Hover";
        objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
    }
}
// Function to trim
function Trim(s) {
    if (s != null) {
        // Remove leading spaces and carriage returns
        while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n') || (s.substring(0, 1) == '\r')) {
            s = s.substring(1, s.length);
        }
        // Remove trailing spaces and carriage returns
        while ((s.substring(s.length - 1, s.length) == ' ') || (s.substring(s.length - 1, s.length) == '\n') || (s.substring(s.length - 1, s.length) == '\r')) {
            s = s.substring(0, s.length - 1);
        }
    }
    return s;
}
/** Methods for Global search **/
function clearSearch(objText) {
    if (objText.value == __JSMessages["Search"])
        objText.value = "";
}
function setSearch(objText) {
    if (Trim(objText.value) == "") {
        objText.value = __JSMessages["Search"];
    }
}
/** Methods for Global Search **/
/** Methods for Grid search **/
function setText(objText) {
    if (Trim(objText.value) == "") {
        objText.value = __JSMessages["TypeHereToFilterResults"];
    }
}
function clearText(objText) {
    if (objText.value == __JSMessages["TypeHereToFilterResults"]) {
        objText.value = "";
    }
}
/** Methods for Grid search **/
function validKey(e) {
    var key;
    var keychar;
    var reg;

    if (window.event) {
        // for IE, e.keyCode or window.event.keyCode can be used
        key = e.keyCode;
    }
    else if (e.which) {
        // netscape
        key = e.which;
    }
    else {
        // no event, so pass through
        return true;
    }
    if (key > 31 || key == 8 || key == 16) {
        return true;
    }
    else {
        return false;
    }
}
function checkEnterKey(evt) {
    if (window.event) {
        // for IE, e.keyCode or window.event.keyCode can be used
        key = evt.keyCode;
    }
    else if (e.which) {
        // netscape
        key = evt.which;
    }
    else {
        // no event, so pass through
        return true;
    }
    if (key == 13) {
        evt.returnValue = false;
        evt.cancel = true;
        return false;
    }
}
/*** Methods added by AV ***/
// "Internal" function to return the decoded value of a cookie
//
function getCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

//
// Function to return the value of the cookie specified by "name".
// name - String object containing the cookie name.
// returns - String object containing the cookie value, or null if
// the cookie does not exist.
//
function GetCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

//
// Function to create or update a cookie.
// name - String object object containing the cookie name.
// value - String object containing the cookie value. May contain
// any valid string characters.
// [expires] - Date object containing the expiration data of the cookie. If
// omitted or null, expires the cookie at the end of the current session.
// [path] - String object indicating the path for which the cookie is valid.
// If omitted or null, uses the path of the calling document.
// [domain] - String object indicating the domain for which the cookie is
// valid. If omitted or null, uses the domain of the calling document.
// [secure] - Boolean (true/false) value indicating whether cookie transmission
// requires a secure channel (HTTPS). 
//
// The first two parameters are required. The others, if supplied, must
// be passed in the order listed above. To omit an unused optional field,
// use null as a place holder. For example, to call SetCookie using name,
// value and path, you would code:
//
// SetCookie ("myCookieName", "myCookieValue", null, "/");
//
// Note that trailing omitted parameters do not require a placeholder.
//
// To set a secure cookie for path "/myPath", that expires after the
// current session, you might code:
//
// SetCookie (myCookieVar, cookieValueVar, null, "/myPath", null, true);
//
function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape(value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}

// Function to delete a cookie. (Sets expiration date to current date/time)
// name - String object containing the cookie name
//
function DeleteCookie(name) {
    var exp = iAppsCurrentLocalDate;
    exp.setTime(exp.getTime() - 1); // This cookie is history
    var cval = GetCookie(name);
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}

function UpdateSessionStatus() {
    // debugger;
    // get session timeout value in minutes (it comes from server and is set to this window value in MasterPage.master
    var sessionTimeout = window["SessionTimeoutValue"]; // 1 minute less than the actual server session timeout
    if (window["IsSessionValid"] != false) {
        if (sessionTimeoutCounter >= sessionTimeout) // session has already expired
        {
            window["IsSessionValid"] = false;
        }
        else {
            var shouldResetSessionCounter = GetCookie('ResetSessionCounter');
            if (shouldResetSessionCounter == "yes") {
                sessionTimeoutCounter = 2;
                SetCookie('ResetSessionCounter', 'no');
            }
            else
                sessionTimeoutCounter = sessionTimeoutCounter + 1;

            window["IsSessionValid"] = true;
        }
    }
}

function IsThisSessionValid() {
    if (window["IsSessionValid"]) {
        DeleteCookie('ResetSessionCounter');
        return true;
    }
    return false;
}

var timerObj;
var sessionTimeoutCounter = 0;
window["IsSessionValid"] = true;
function SetTimer() {

    var dblMinutes = 1;       // every minute check call this function
    //set timer to call function to confirm update 
    timerObj = setInterval("UpdateSessionStatus()", 1000 * 60 * dblMinutes);
}

function CheckSession() {
    if (!IsThisSessionValid()) {
        window.location.reload();
        return false;
    }
    return true;
}

//Event Handler 

function CheckSessionEventHandler(sender, eventArgs) {
    if (!IsThisSessionValid()) {
        eventArgs.set_cancel(true);
        window.location.reload();
    }
}
//Method for validating an email address
function ValidateEmail(eMail) {
    return /^(\w+\.)*([\w-]+)@([\w-]+\.)+([a-zA-Z]{2,4})$/.test(eMail);
}

//Method for converting Integer value to Decimal value
function GetContentDecimal(items) {
    if (items != null) {

        var aInt = items;
        var aintDecimal = (parseFloat(aInt) - 0.00).toFixed(2);
        return aintDecimal;
    }

}

function GetContentAfterSlash(items) {
    if (items != null && items != "") {
        var strMenu = items;
        var index = strMenu.lastIndexOf('/');
        if (index < 0) {
            return items;
        }
        else {
            return strMenu.substring(0, strMenu.lastIndexOf('/'));
        }

    }

}

/*
* Accepts a date, a mask, or a date and a mask.
* Returns a formatted version of the given date.
* The date defaults to the current date/time.
* The mask defaults to dateFormat.masks.default.
*/
var dateFormati18n = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
		    val = String(val);
		    len = len || 2;
		    while (val.length < len) val = "0" + val;
		    return val;
		};

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormati18n;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date();
        if (isNaN(date)) throw new SyntaxError(__JSMessages["InvalidDate"]);

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
			    d: d,
			    dd: pad(d),
			    ddd: dF.i18n.dayNames[D],
			    dddd: dF.i18n.dayNames[D + 7],
			    m: m + 1,
			    mm: pad(m + 1),
			    mmm: dF.i18n.monthNames[m],
			    mmmm: dF.i18n.monthNames[m + 12],
			    yy: String(y).slice(2),
			    yyyy: y,
			    h: H % 12 || 12,
			    hh: pad(H % 12 || 12),
			    H: H,
			    HH: pad(H),
			    M: M,
			    MM: pad(M),
			    s: s,
			    ss: pad(s),
			    l: pad(L, 3),
			    L: pad(L > 99 ? Math.round(L / 10) : L),
			    t: H < 12 ? "a" : "p",
			    tt: H < 12 ? "am" : "pm",
			    T: H < 12 ? "A" : "P",
			    TT: H < 12 ? "AM" : "PM",
			    Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
			    o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
			    S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
} ();

// Some common format strings
dateFormati18n.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormati18n.i18n = {
    dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
    monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormati18n(this, mask, utc);
};

function cover(coverObject) {
    var ieMat = null; //for IE6 iframe shim
    // Cover <select> elements with <iframe> elements only in IE < 7
    if (navigator.appVersion.substr(22, 2) == "6." && browser == "ie") {
        if (ieMat == null) {
            ieMat = document.createElement('iframe');
            if (document.location.protocol == "https:")
                ieMat.src = "//0";
            else if (window.opera != "undefined")
                ieMat.src = "";
            else
                ieMat.src = "javascript:false";
            ieMat.scrolling = "no";
            ieMat.frameBorder = "0";
            ieMat.style.position = "absolute";
            ieMat.style.top = coverObject.offsetTop + "px";
            ieMat.style.left = coverObject.offsetLeft + "px";
            ieMat.style.width = "232px";
            ieMat.style.height = "185px";
            ieMat.style.filter = "progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=0)";
            ieMat.style.zIndex = "1";
            document.getElementById('dateRangeContainer').insertBefore(ieMat, document.getElementById('dateRangeSelector'));
        }
    }
    return ieMat;
}

function clearDefaultText(textboxObj) {
    if (textboxObj.value == __JSMessages["TypeHereToFilterResults"]) {
        textboxObj.value = "";
    }
}
function setDefaultText(textboxObj) {
    if (textboxObj.value == "") {
        textboxObj.value = __JSMessages["TypeHereToFilterResults"];
    }
}

function ChangeSpecialCharacters(str) {
    var replacedString = str;
    if (replacedString != null) {
        replacedString = replacedString.replace(new RegExp("&", "g"), "&amp;");
        replacedString = replacedString.replace(new RegExp("<", "g"), "&lt;");
        replacedString = replacedString.replace(new RegExp(">", "g"), "&gt;");
        replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;");
        replacedString = replacedString.replace(new RegExp("'", "g"), "\'");
        replacedString = replacedString.replace(new RegExp("\n", "g"), " ");
    }
    return replacedString;
}

//methods used to strip html tags
function removeHTMLTags(textToStrip) {
    textToStrip = textToStrip.replace(/&(lt|gt);/g, function (strMatch, p1) {
        return (p1 == "lt") ? "<" : ">";
    });
    var strTagStrippedText = textToStrip.replace(/<\/?[^>]+(>|$)/g, "");
    return strTagStrippedText;
}

function expirationCalendar_onSelectionChanged(sender, eventArgs) {
    var cal = window[expirydateIdHolder];
    var selectedDate = cal.getSelectedDate();
    if (selectedDate <= iAppsCurrentLocalDate) {
        alert(__JSMessages["TheExpirationDateCannotBeBeforeTomorrowsDate"]);
    }
    else {
        document.getElementById(expirydateTxtIdHolder).value = cal.formatDate(selectedDate, shortCultureDateFormat);
        document.getElementById(hiddenDateId).value = cal.formatDate(selectedDate, calendarDateFormat);
    }
}

function popUpCalendar(hdnExpiryDate) {
    var thisDate = iAppsCurrentLocalDate;
    thisDate.setDate(thisDate.getDate() - 1);
    obj = hdnExpiryDate.value;

    var cal = window[expirydateIdHolder];

    if (obj != "" && obj != null) {
        if (validate_dateSummary(hdnExpiryDate) == true) {// available in validation.js 
            thisDate = new Date(obj);
            cal.setSelectedDate(thisDate);
        }
    }

    if (!(cal.get_popUpShowing()));
    cal.show();
}


/** Tooltip related script STARTS **/
var offsetfromcursorX = 12; //Customize x offset of tooltip
var offsetfromcursorY = 10; //Customize y offset of tooltip
var offsetdivfrompointerX = 10; //Customize x offset of tooltip DIV relative to pointer image
var offsetdivfrompointerY = 9; //Customize y offset of tooltip DIV relative to pointer image. Tip: Set it to (height_of_pointer_image-1).

document.write('<div id="dhtmltooltip"></div>'); //write out tooltip DIV
//document.write('<img id="dhtmlpointer" src="' + jCommerceAdminSiteUrl + '"/App_Themes/General/images/tooltip-arrow.gif">'); //write out pointer image
document.write('<img id="dhtmlpointer" src="/CommerceAdmin/App_Themes/General/images/tooltip-arrow.gif">'); //write out pointer image

var ie = document.all;
var ns6 = document.getElementById && !document.all;
var enabletip = false;
if (ie || ns6) {
    var tipobj = document.all ? document.all["dhtmltooltip"] : document.getElementById ? document.getElementById("dhtmltooltip") : "";
}
var pointerobj = document.all ? document.all["dhtmlpointer"] : document.getElementById ? document.getElementById("dhtmlpointer") : "";
function ietruebody() {
    return (document.compatMode && document.compatMode != "BackCompat") ? document.documentElement : document.body;
}

function ddrivetip(thetext, thewidth, thecolor) {
    if (ns6 || ie) {
        if (typeof thewidth != "undefined") {
            tipobj.style.width = thewidth + "px";
        }
        else {
            tipobj.style.width = "auto";
        }
        if (typeof thecolor != "undefined" && thecolor != "") {
            tipobj.style.backgroundColor = thecolor;
        }
        tipobj.innerHTML = thetext;
        enabletip = true;
        return false;
    }
}

function positiontip(e) {
    if (enabletip) {
        pointerobj.src = tooltipImage;
        var nondefaultpos = false;
        var curX = (ns6) ? e.pageX : event.clientX + ietruebody().scrollLeft;
        var curY = (ns6) ? e.pageY : event.clientY + ietruebody().scrollTop;
        //Find out how close the mouse is to the corner of the window
        var winwidth = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
        var winheight = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
        var rightedge = ie && !window.opera ? winwidth - event.clientX - offsetfromcursorX : winwidth - e.clientX - offsetfromcursorX;
        var bottomedge = ie && !window.opera ? winheight - event.clientY - offsetfromcursorY : winheight - e.clientY - offsetfromcursorY;
        var leftedge = (offsetfromcursorX < 0) ? offsetfromcursorX * (-1) : -1000;
        //if the horizontal distance isn't enough to accomodate the width of the context menu
        if (rightedge < tipobj.offsetWidth) {
            //move the horizontal position of the menu to the left by it's width
            pointerobj.src = reverseTooltipImage;
            tipobj.style.left = (curX - tipobj.offsetWidth) + "px";
            pointerobj.style.left = (curX - offsetfromcursorX - pointerobj.width) + "px";
            nondefaultpos = true;
        }
        else if (curX < leftedge) {
            tipobj.style.left = "5px";
        }
        else {
            //position the horizontal position of the menu where the mouse is positioned
            tipobj.style.left = (curX + offsetfromcursorX - offsetdivfrompointerX) + "px";
            pointerobj.style.left = (curX + offsetfromcursorX) + "px";
        }
        //same concept with the vertical position
        if (bottomedge < tipobj.offsetHeight) {
            pointerobj.src = downTooltipImage;
            tipobj.style.top = (curY - tipobj.offsetHeight - offsetfromcursorY) + "px";
            pointerobj.style.top = (curY - offsetfromcursorY - 1) + "px";
            nondefaultpos = true;
        }
        else {
            tipobj.style.top = (curY + offsetfromcursorY + offsetdivfrompointerY) + "px";
            pointerobj.style.top = (curY + offsetfromcursorY) + "px";
        }
        if (bottomedge < tipobj.offsetHeight && rightedge > tipobj.offsetWidth) {
            pointerobj.src = reversedownTooltipImage;
        }
        tipobj.style.visibility = "visible";
        if (!nondefaultpos)
            pointerobj.style.visibility = "visible";
        else
            pointerobj.style.visibility = "visible";
    }
}
function hideddrivetip() {
    if (ns6 || ie) {
        enabletip = false;
        tipobj.style.visibility = "hidden";
        pointerobj.style.visibility = "hidden";
        tipobj.style.left = "-1000px";
        tipobj.style.backgroundColor = '';
        tipobj.style.width = '';
    }
}
document.onmousemove = positiontip;
/** Tooltip related script ENDS **/

function OpenLicenseWarningPopup(productName, hasLicenese, hasPermission) {
    var url = "..";
    if (jCommerceAdminUrl != undefined && jCommerceAdminUrl != null && jCommerceAdminUrl != 'undefined' && jCommerceAdminUrl != '')
        url = jCommerceAdminUrl;
    url += "/Popups/LicenseWarning.aspx?ProductName=" + productName + "&HasLicense=" + hasLicenese + "&HasPermission=" + hasPermission;
    licenseWarningWindow = dhtmlmodal.open('LicenseWarning', 'iframe', url, '', 'width=415px,height=350px,center=1,resize=0,scrolling=1');
    licenseWarningWindow.onclose = function () {
        var a = document.getElementById('LicenseWarning');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        return true;
    }
}

function stringformat(str)	//created by Adams to format the string with placeholder
{
    if (arguments.length == 0)
        return null;
    var str = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        str = str.replace(re, arguments[i]);
    }
    return str;
}

function isPositiveNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n) && parseInt(n) > -1
}

function toggleTextBoxBasedOnCheckbox(chkBox, chkBoxId, TextBoxId) {
    var txtboxId = chkBox.id.replace(chkBoxId, TextBoxId);
    if (chkBox.checked) {
        $('#' + txtboxId).removeAttr('disabled');
    }
    else {
        $('#' + txtboxId).attr('disabled', 'disabled');
    }
}
function toggleControlsBasedOnCheckbox(chkBox, chkBoxId, toggle) {
    if (arguments.length > 3) {
        for (var i = 3; i < arguments.length; i++) {
            var txtboxId = chkBox.id.replace(chkBoxId, arguments[i]);
            var enable = (toggle == "true");
            if (enable)
                enable = chkBox.checked;
            else
                enable = !chkBox.checked;
            var cntrl = document.getElementById(txtboxId);
            if (enable) {
                if (cntrl.tagName != "INPUT")
                    cntrl.style.display = 'block';
                else
                    $('#' + txtboxId).removeAttr('disabled');
            }
            else {
                if (cntrl.tagName != "INPUT")
                    cntrl.style.display = 'none';
                else
                    $('#' + txtboxId).attr('disabled', 'disabled');
            }
        }
    }

}

function GetSelectedTextWithValueOfLstBox(lstBox) {
    if (lstBox != null) {
        var textWithValue = new Array();
        var lstOptions = lstBox.options;

        for (i = 0; i < lstOptions.length; i++) {
            if (lstOptions[i].selected) {
                var option = lstOptions[i];
                var text;
                if (option.innerText) {
                    text = lstOptions[i].innerText;
                }
                else {
                    text = lstOptions[i].text;
                }
                textWithValue[textWithValue.length] = text + "#" + lstOptions[i].value;
            }
        }

        return textWithValue;
    }
    else {
        return null;
    }

}

function RemoveSelectedItemsFromLstBox(lstBox) {

    for (var i = lstBox.options.length - 1; i >= 0; i--) {
        if (lstBox.options[i].selected)
            lstBox.remove(i);
    }
}

/*
function to move the selected site and groups to the permissions Listbox.
site    : siteName#SiteId
groups  : array of groupName#groupId
permissions : object of permissions list box.
*/
var siteGroupListItems = new Array();
function MoveSiteGroupsToPermission(site, groups, permissions) {

    if (groups != null && site != null) {
        var siteNameWithValue = new Array();
        if (site != null && site.length > 0) {
            siteNameWithValue = site[0].split('#');
            for (i = 0; i < groups.length; i++) {
                var groupNameWithValue = new Array();
                groupNameWithValue = groups[i].split("#");
                var newText = siteNameWithValue[0] + ":" + groupNameWithValue[0];
                var newValue = siteNameWithValue[1] + ":" + groupNameWithValue[1];

                if (!containsElement(permissions, newValue)) {
                    permissions.options[permissions.options.length] = new Option(newText, newValue);
                    siteGroupListItems[siteGroupListItems.length] = newValue;
                }
            }
        }
    }
}
function containsElement(lstToBeSearched, valueToBeSearched) {
    for (j = 0; j < lstToBeSearched.options.length; j++) {
        if (lstToBeSearched.options[j].value == valueToBeSearched) {
            return true;
        }
    }

    if (browser == "ie") {
        lstToBeSearched.focus();
    }
    else {
        lstToBeSearched.focus();
    }
}

function OpenJPopup(linkTitle, linkUrl) {
    jQuery('<iframe />').attr('src', "/blank.html").dialog({
        title: linkTitle,
        width: 900,
        height: 670,
        modal: true,
        dialogClass: 'view-page',
        open: function () {
            $(this).contents().find('html').html("Loading...")
            $(this).attr('src', linkUrl);
        },   
        close: function (ev, ui) {
            $(this).dialog('destroy');
        },
        buttons: {
            Close: function () {
                $(this).dialog("close");
            }
        }
    });
}


function SetTextboxDefaultValues() {
    $(".textBoxes").each(function () {
        if ($(this).attr("title") && $(this).attr("title") != "" && 
                ($(this).val() == "" || $(this).val() == $(this).attr("title"))) {
            $(this).focus(function () {
                if ($(this).val() == $(this).attr("title")) {
                    $(this).removeClass("defaultText");
                    $(this).val("");
                }
            });
            $(this).blur(function () {
                if ($(this).val() == "" || $(this).val() == $(this).attr("title")) {
                    $(this).addClass("defaultText");
                    $(this).val($(this).attr("title"));
                }
            });

            $(this).blur();
        }
    });
}

$(function () {    
    SetTextboxDefaultValues();
});


// Address Validation Popup code starts here


function openAddressSuggestion() {

    popupPath = "/CommerceAdmin/Popups/StoreManager/Customers/SuggestedAddresses.aspx";
    addressSuggestionPopup = dhtmlmodal.open('AddressSuggestion', 'iframe', popupPath, '', 'width=500px,height=500px,center=1,resize=0,scrolling=1');
    addressSuggestionPopup.onclose = function () {
        var a = document.getElementById('AddressSuggestion');
        var ifr = a.getElementsByTagName('iframe');
        window.frames[ifr[0].name].location.replace("about:blank");
        return true;
    }
    return false;
}

function CloseAddressSuggestionPopup() {
    addressSuggestionPopup.hide();
}

function DisplayAddressAndClose(selectedAddress, status, action) {
    action = typeof action !== 'undefined' ? action : '0';
    if (selectedAddress != null && selectedAddress != '') {

        //Populate the
        var addressElements = selectedAddress.split('$');
        var addressLine1 = addressElements[0];
        var addressLine2 = addressElements[1];
        var addressLine3 = addressElements[2];
        var city = addressElements[3];
        var stateId = addressElements[4];
        //5 is country
        var ZipCode = addressElements[6];

        //This is for registration

        $("input[id$=txtStreetAddress]").val(addressLine1);

        //Populate the controls
      //  if (addressLine1 != '')
            $("input[id$=txtAddress1]").val(addressLine1);
      //  if (addressLine2 != '')
            $("input[id$=txtAddress2]").val(addressLine2);
     //   if (addressLine3 != '')
            $("input[id$=txtAddress3]").val(addressLine3);
       // if (city != '')
            $("input[id$=txtCity]").val(city);
            $("input[id$=txtZip]").val(ZipCode);
            
            //Overwrite the zip with only primary for the registration page
            var splittedZip = ZipCode.split('-');
            if(splittedZip.length == 2)
                $('.RegistrationZip').val(splittedZip[0]);

        // select the option value  
        $("[id$=ddlState]").val(stateId);

    }
    $("[id$=hfIsAddressValidated]").val('1');

    //Close the popup
    CloseAddressSuggestionPopup();
 
    if (action == '1' &&  typeof(PageLevelNextAction) != "undefined") 
        PageLevelNextAction();
}

//Stop Form Submission of Enter Key Press
function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text" || node.type == "select-multiple" || node.type == "checkbox")) { return false; }
}

    

//Adddress validation code ends here
