﻿
    function clearDefaultText(objTextbox)
    {
        
        if(objTextbox.value == __JSMessages["Search"])
            objTextbox.value = "";
    }
    function setDefaultText(objTextbox)
    {
        
        if(Trim(objTextbox.value) == "")
            objTextbox.value = __JSMessages["Search"];
    }        
                                                        
//groupMembers.ascx
    var activeTableData=null;
    var deactiveTableData=null;
    var grouptableData=null;
    var usertableData=null;

function SelectedUsersGroups_onItemExternalDrop(sender, eventArgs)
        {       
            CheckSession();                  
            var draggedItem = eventArgs.get_item();            
            if(!draggedItem)
            {
                return;
            }
            //if(tabSite.getSelectedTab().get_id()=="zero")
            //{
                if(ModifyDeleteAvailableUsers.getSelectedItems().length<=0)                
                {
                    ModifyDeleteAvailableUsers.select(draggedItem);
                }
            //}
//            else if(tabSite.getSelectedTab().get_id()=="one")
//            {
//                if(ModifyDeleteAvailableGroups.getSelectedItems().length<=0)                
//                {
//                    ModifyDeleteAvailableGroups.select(draggedItem);
//                }
//            }
            RightArrowClick(ModifyDeleteAvailableUsers,SelectedUsersGroups,tabSite);
            
        }
 function SelectedUsersGroups_onItemRemove(sender, eventArgs)
 {        
            CheckSession(); 
            var draggedItem = eventArgs.get_item();            
            if(!draggedItem)
            {
                return;
            }
            
            if(SelectedUsersGroups.getSelectedItems().length<=0)                
            {
                SelectedUsersGroups.select(draggedItem);
            }
           
            LeftArrowClick(SelectedUsersGroups);
 
 
 }
 function usersGroupsSearchBtn()
 {
    var str = document.getElementById("searchModifyUsersGroups").value;
    if (str == __JSMessages["Search"])
        str = "";
    var tabId =gE(HiddenTabIndexId).value;                
    document.getElementById(clntHiddenSearchValue).value = str;

     //Loading user grid if data already exists
//    if(usertableData)
//    {   
//        ModifyDeleteAvailableUsers.get_table().Data=usertableData;                                 
//        ModifyDeleteAvailableUsers.get_table().Grid.Data=usertableData;                          
//        ModifyDeleteAvailableUsers.render();                    
//        usertableData=null;                
//    }     

//    ModifyDeleteAvailableUsers.scrollTo(0);
    ModifyDeleteAvailableUsers.Search(str, false);  
    //if record count is 0 placing the data in tableData
//    if(ModifyDeleteAvailableUsers.RecordCount==0 && usertableData==null)
//    {   
//        usertableData=ModifyDeleteAvailableUsers.get_table().get_data();
//        ModifyDeleteAvailableUsers.get_table().ClearData();
//        ModifyDeleteAvailableUsers.render();
//    }                  

 }
 function usersGroupsSearch(evt)
            {                        
            if(validKey(evt))
            {            
                var str = document.getElementById("searchModifyUsersGroups").value;
                var tabId =gE(HiddenTabIndexId).value;                
                //if(tabId == "0")
                //{
                     //Loading user grid if data already exists
                    if(usertableData)
                    {   
                        ModifyDeleteAvailableUsers.get_table().Data=usertableData;                                 
                        ModifyDeleteAvailableUsers.get_table().Grid.Data=usertableData;                          
                        ModifyDeleteAvailableUsers.render();                    
                        usertableData=null;                
                    }     
                
                    ModifyDeleteAvailableUsers.scrollTo(0);
                    ModifyDeleteAvailableUsers.Search(str, false);  
                    //if record count is 0 placing the data in tableData
                    if(ModifyDeleteAvailableUsers.RecordCount==0 && usertableData==null)
                    {   
                        usertableData=ModifyDeleteAvailableUsers.get_table().get_data();
                        ModifyDeleteAvailableUsers.get_table().ClearData();
                        ModifyDeleteAvailableUsers.render();
                    }                  
//                }
//                else if(tabId == "1")
//                {
//                     //Loading user grid if data already exists
//                    if(grouptableData)
//                    {   
//                        ModifyDeleteAvailableGroups.get_table().Data=grouptableData;                                 
//                        ModifyDeleteAvailableGroups.get_table().Grid.Data=grouptableData;                          
//                        ModifyDeleteAvailableGroups.render();                    
//                        grouptableData=null;                
//                    }    
//                    
//                    ModifyDeleteAvailableGroups.scrollTo(0);
//                    ModifyDeleteAvailableGroups.Search(str, false);  
//                    //if record count is 0 placing the data in tableData
//                    if(ModifyDeleteAvailableGroups.RecordCount==0 && grouptableData==null)
//                    {   
//                        grouptableData=ModifyDeleteAvailableGroups.get_table().get_data();
//                        ModifyDeleteAvailableGroups.get_table().ClearData()
//                        ModifyDeleteAvailableGroups.render();
//                    }                      
//                }
                
            }
        }
        
function Refresh_GroupsUserGrid(sender, eventArgs)
{    
    //var List= document.getElementById(SortingClientId);
    var tabText = eventArgs.get_tab().get_text();
    gE("searchModifyUsersGroups").value = __JSMessages["Search"];
    var searchText=gE("searchModifyUsersGroups").value; 
    if(searchText=="Search")
    {
        searchText="";       
    }
    if(tabText == "Groups")
    {
        if(window["ModifyDeleteAvailableGroups"]!=null)
        {
            //Loading Groups
            if(grouptableData)
            {      
                ModifyDeleteAvailableGroups.get_table().Data=grouptableData;                                 
                ModifyDeleteAvailableGroups.get_table().Grid.Data=grouptableData;  
            }   
            gE(HiddenTabIndexId).value="1";
//            if(List)
//            {
//                for(row =List.options.length-1; row >=0; row=row-1)
//                {
//                   if(browser == "ie")
//                    {
//                        List.options.remove(row);
//                    }
//                    else
//                    {                    
//                        List.remove(row);
//                    }
//                }
//                                      
//                var myOption = new Option("GroupName", "GroupName");
//                List.options.add(myOption);
//                
//                ModifyDeleteAvailableGroups.Search(searchText, false); 
//                //ModifyDeleteAvailableGroups.sort(1,false);
//                ModifyDeleteAvailableGroups.render();  
//                
//                          
//            }
            setTimeout("ModifyDeleteAvailableGroups.render();",100);
         }
         else
         {                    
            eventArgs.set_cancel(true); 
         }
    }
    else if(tabText == __JSMessages["Users"])
    {
        if(window["ModifyDeleteAvailableUsers"]!=null)
        {
            //Loading users
            if(usertableData)
            {      
                ModifyDeleteAvailableUsers.get_table().Data=usertableData;                                 
                ModifyDeleteAvailableUsers.get_table().Grid.Data=usertableData;  
            }   
            gE(HiddenTabIndexId).value="0";
//            if(List)
//            {
//                for(row =List.options.length-1; row >=0; row=row-1)
//                {
//                   if(browser == "ie")
//                    {
//                        List.options.remove(row);
//                    }
//                    else
//                    {                    
//                        List.remove(row);
//                    }
//                }
//                
//                var myOption = new Option("FirstName", "FirstName");
//                List.options.add(myOption);
//                
//                var myOption = new Option("LastName", "LastName");
//                List.options.add(myOption);
//            }
            ModifyDeleteAvailableUsers.Search(searchText, false); 
            setTimeout("ModifyDeleteAvailableUsers.render();",100);
         }
         else
         {                    
            eventArgs.set_cancel(true); 
         }
    }    
}
//This method is not used. It is changed into LeftArrowClick()
function LeftArrow_Click()
{
    for(var i=SelectedUsersGroups.get_table().getRowCount()-1;i>=0;i--)
    {     
             
        var item=SelectedUsersGroups.get_table().getRow(i);   
         
         if(item.Selected==true)
         {       
            SelectedUsersGroups.unSelect(item);                       
            SelectedUsersGroups.deleteItem(item);                
         } 
          
        
    }
    
    SelectedUsersGroups.render();
                    
}
//This method is not used. It is changed into RightArrowClick()
function RightArrow_Click()
{     
    
    var selectedGroup=null;       
    if(document.getElementById(GroupClientId))
    {
        selectedGroup= document.getElementById(GroupClientId).value;
    }    
    var grd=new ComponentArt.Web.UI.Grid();
    //if(tabSite.getSelectedTab().get_id()=="zero")
    //{                       
        for(var indx in ModifyDeleteAvailableUsers.getSelectedItems())                      
        {      
            var isExists=false;
            
            for(var i=SelectedUsersGroups.get_table().getRowCount()-1;i>=0;i--)
            {                
                var id=SelectedUsersGroups.get_table().getRow(i).get_cells(0)[0].Value;                
                if(id==ModifyDeleteAvailableUsers.getSelectedItems()[indx].GetMemberAt(0).get_text())
                {
                    isExists=true;
                }
            }
            if(isExists==false)
            {           
                SelectedUsersGroups.beginUpdate();
                var rowitem = SelectedUsersGroups.get_table().addEmptyRow(0);
                //SelectedUsersGroups.editComplete();
                
                var selItem=ModifyDeleteAvailableUsers.getSelectedItems()[indx]
                SelectedUsersGroups.edit(rowitem);
                id=selItem.GetMemberAt(0).get_text()               
                var firstName=selItem.GetMemberAt(2).get_text()
                var lastName=selItem.GetMemberAt(3).get_text()
                                                                
                //var rowitem = SelectedUsersGroups.get_table().getRow(0);
                
                rowitem.setValue(0,id);
                
                rowitem.setValue(1,firstName);
                rowitem.setValue(2,"1");      
                rowitem.setValue(3,lastName); 
                SelectedUsersGroups.editComplete();  
            }     
                                                 
        }
        //Unselecting the selected items        
        var selectedItems=ModifyDeleteAvailableUsers.getSelectedItems();       
        for(var itemIndex in selectedItems)       
        {               
            ModifyDeleteAvailableUsers.unSelect(selectedItems[itemIndex]);            
        }
        SelectedUsersGroups.render();       
    //}
//    else if(tabSite.getSelectedTab().get_id()=="one")
//    {
//        for(var indx in ModifyDeleteAvailableGroups.getSelectedItems())
//        {
//            var isExists=false;
//            if(selectedGroup!=ModifyDeleteAvailableGroups.getSelectedItems()[indx].GetMemberAt(1).get_text())
//            {
//                for(var i=SelectedUsersGroups.get_table().getRowCount()-1;i>=0;i--)
//                {                
//                    var id=SelectedUsersGroups.get_table().getRow(i).get_cells(0)[0].Value;                
//                    if(id==ModifyDeleteAvailableGroups.getSelectedItems()[indx].GetMemberAt(0).get_text())
//                    {
//                        isExists=true;
//                    }
//                }
//            }
//            else
//            {
//                isExists=true;   
//            }            
//            if(isExists==false || isExists=="False")
//            {
//                var currentId=ModifyDeleteAvailableGroups.getSelectedItems()[indx].GetMemberAt(0).get_text();                
//                var parentCount=0;
//                if(hiddenGroupId)
//                {
//                    if(document.getElementById(hiddenGroupId).value!="")
//                    {
//                        parentCount=GetParentLevelsCount(document.getElementById(hiddenGroupId).value);
//                    }
//                }                
//                var childrenCount=GetChildrenLevelsCount(currentId);
//                isExists=AllowAddGroup(parentCount,childrenCount);
//                if(isExists==true || isExists=="True")
//                {
//                    var currentName=ModifyDeleteAvailableGroups.getSelectedItems()[indx].GetMemberAt(1).get_text();
//                    alert(currentName+" has exceeded maximum level count");
//                }
//            }
//            if(isExists==false || isExists=="False")
//            {
//                SelectedUsersGroups.beginUpdate();
//                var rowitem =SelectedUsersGroups.get_table().addEmptyRow(0);
//                //SelectedUsersGroups.editComplete();
//                
//                var selItem=ModifyDeleteAvailableGroups.getSelectedItems()[indx]
//                SelectedUsersGroups.edit(rowitem);
//                id=selItem.GetMemberAt(0).get_text();
//                name=selItem.GetMemberAt(1).get_text();
//                                                                
//                //var rowitem = SelectedUsersGroups.get_table().getRow(0);
//                
//                rowitem.setValue(0,id);
//                rowitem.setValue(1,name);
//                rowitem.setValue(2,"2"); 
//                SelectedUsersGroups.editComplete();      
//            }                     
//               
//                                   
//        }
//        //Unselecting the selected items        
//        var selectedItems=ModifyDeleteAvailableGroups.getSelectedItems();       
//        for(var itemIndex in selectedItems)       
//        {               
//            ModifyDeleteAvailableGroups.unSelect(selectedItems[itemIndex]);            
//        }
//        SelectedUsersGroups.render();
//    }
}
function GroupSort_ItemChanged()
{       
 
    var sortItem= document.getElementById(SortingClientId).value
    var List= document.getElementById(SortingClientId);
    if(sortItem=="FirstName")
    {    
        ModifyDeleteAvailableUsers.sort(2,false);
        ModifyDeleteAvailableUsers.render();
        for(row =List.options.length-1; row >=0; row=row-1)
        {
            if(List.options[row].value=="")
            {
                if(browser == "ie")
                {
                    List.options.remove(row);
                }
                else
                {                    
                    List.remove(row);
                }
            }
            
        }
    }
    else if(sortItem=="LastName")
    {
        ModifyDeleteAvailableUsers.sort(3,false);
        ModifyDeleteAvailableUsers.render();
        for(row =List.options.length-1; row >=0; row=row-1)
        {
            if(List.options[row].value=="")
            {
                if(browser == "ie")
                {
                    List.options.remove(row);
                }
                else
                {                    
                    List.remove(row);
                }
            }
            
        }
    }
    else if(sortItem=="GroupName")
    {
        ModifyDeleteAvailableGroups.sort(1,false);
        ModifyDeleteAvailableUsers.render();
                            
        for(row =List.options.length-1; row >=0; row=row-1)
        {
            if(List.options[row].value=="")
            {
                if(browser == "ie")
                {
                    List.options.remove(row);
                }
                else
                {                    
                    List.remove(row);
                }
            }
            
        }
    }
    
    
}
//ModifyDeleteGroup.aspx


        var WEB_ACTIVATE_USER = __JSMessages["ActivateGroup"];
        var WEB_DEACTIVATE_USER = __JSMessages["DeactivateGroup"];       
        var groupItem;
        var selectedItem;
        var selectedOption = null;
        var selTab=null;
        function modifyDeleteGroup_onContextMenu(sender, eventArgs) 
        {
         
         CheckSession();            
         var tabId = gE(HdnSelectedTabIdHolder).value;
         //debugger
         if(sender!=null)
         {
             if(sender.Id==ActiveGroupClientId)
             {
                tabId="0";
             }
             else
             {
                tabId="1";
             }
         }
         //Added for Activating group
            if(eventArgs != null)
            {
                if(tabId != null && tabId != "")
                {
                    switch(tabId)
                    {
                        case "0":   
                        //set the context menu text deativate as activate
                        //delete is possible.
                        if ((selectedGridItem!=null && selectedItemGroupId =='') || 
                          (grdActivatedGroups.get_table().getRow(0).getMember(0).get_text() =='')) //only add
                        {
                            modifyDeleteMenu.get_items().getItem(0).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(1).set_visible(false);
                            modifyDeleteMenu.get_items().getItem(2).set_visible(false);
                            modifyDeleteMenu.get_items().getItem(3).set_visible(false);
                            modifyDeleteMenu.get_items().getItem(4).set_visible(false);
                            modifyDeleteMenu.get_items().getItem(5).set_visible(false);
                            modifyDeleteMenu.get_items().getItem(6).set_visible(false);
                        }
                        else
                        {
                            modifyDeleteMenu.get_items().getItemById("DeactivateGroupMenuItem").set_text(WEB_DEACTIVATE_USER);
                            modifyDeleteMenu.get_items().getItem(0).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(1).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(2).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(3).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(4).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(5).set_visible(true);
                            modifyDeleteMenu.get_items().getItem(6).set_visible(true);
                        }                        
                        selTab=0;
                        break;
                        
                        case "1":
                        //set the context menu text deactivate as activate
                        //delete is possible.
                        modifyDeleteMenu.get_items().getItemById("DeactivateGroupMenuItem").set_text(WEB_ACTIVATE_USER);
                        modifyDeleteMenu.get_items().getItem(0).set_visible(false);
                        modifyDeleteMenu.get_items().getItem(1).set_visible(false);
                        //modifyDeleteMenu.get_items().getItem(2).set_visible(false);
                        //modifyDeleteMenu.get_items().getItem(3).set_visible(false);
                        selTab=1;
                        break;                        
                    }
                }
            }
         
         //end
         
          groupItem = eventArgs.get_item();        
          if(browser == "ie")
            {
                var evt = eventArgs.get_event();
                 modifyDeleteMenu.set_contextData(groupItem);
                 modifyDeleteMenu.showContextMenuAtEvent(evt, grdActivatedGroups);                
                                
            }
            else { modifyDeleteMenu.showContextMenuAtEvent(eventArgs.get_event()); }
                   
            
                
                if(tabId != null && tabId != "")
                {
                    if(tabId=="1")
                    {       
                        for(var i=0;i<grdDeActivatedGroups.get_table().getRowCount();i=i+1)
                         {
                            if(selectedItem!=null)
                            {
                                if(selectedItem.Data[0]!=grdDeActivatedGroups.get_table().getRow(i).Cells[0].Value)
                                {
                                    grdDeActivatedGroups.unSelect(grdDeActivatedGroups.get_table().getRow(i));
                                 }
                            }                                                        
                         }    
                         if(groupItem!=null)
                         {
                            grdDeActivatedGroups.Select(groupItem);   
                         }                                                     
                    }
                    else if(tabId=="0")
                    {
                        if(groupItem!=null)
                        {                
                            grdActivatedGroups.Select(groupItem); 
                        }     
                    }
                }    
            
            
        }
           
        function searchGrdSiteManager(evt)
        {
            if(validKey(evt))
            {            
                var str = document.getElementById("searchGroups").value;
                var selGrid=mltpGroups.get_selectedPage();                                           
                if(selGrid.get_id() == "pgvActive")
                    {grdActivatedGroups.scrollTo(0);  
                    grdActivatedGroups.Search(str, false);}
                else if(selGrid.get_id() == "pgvDeactivated")
                     {grdDeActivatedGroups.scrollTo(0);
                     grdDeActivatedGroups.Search(str, false);}
            }            
        }
       
        function clearDefaultText(objTextbox)
        {
            if(objTextbox.value == __JSMessages["Search"])
                objTextbox.value = "";
        }
        function setDefaultText(objTextbox)
        {
            if(Trim(objTextbox.value) == "")
                objTextbox.value = __JSMessages["Search"];
        }
        function setDefaultTextWithBtn(objTextbox)
        {
            if(Trim(objTextbox.value) == "")
                objTextbox.value = __JSMessages["Search"];
                
            var str = objTextbox.value ;
            if (str == __JSMessages["Search"])
                str = "";            
            document.getElementById(clntHiddenSearchValue).value = str;
        }        
        
        function modifyDeleteGroupItem_beforeSelect(sender,eventArgs)
        {                                  
             
             var cancelEvent=false;
                   
             var tabId = gE(HdnSelectedTabIdHolder).value;
                
                if(tabId != null && tabId != "")
                {
                    if(tabId=="1")
                    {                        
                        cancelEvent=true;                        
                    }
                }      
            
        }
        var selectedGridItem;
        var selectedItemGroupId;
        function modifyDeleteGroupItem_onSelect(sender,eventArgs)
        {                       
            CheckSession();                  
            selectedGridItem = eventArgs.get_item();
            selectedItemGroupId = eventArgs.get_item().getMemberAt(0).get_text();
            selectedItem=eventArgs.get_item();       
            return;
            
//            document.getElementById(hiddenGroupId).value=eventArgs.get_item().getMemberAt(0).get_text();
//            selectedItem=eventArgs.get_item();       
//            //alert('modifyDeleteGroupItem_onSelect_1');
//            ModifyDeleteGroupCallBack.Callback(eventArgs.get_item().getMemberAt(0).get_text());  
//            //For Populating the selected user groups
//            
//            setTimeout(function() {        
//                if(SelectedUsersGroups!=null)
//                {
//                    var insertItem = SelectedUsersGroups.get_table().addEmptyRow(0);
//                        SelectedUsersGroups.edit(insertItem);
//                        insertItem.setValue(0,"_"+eventArgs.get_item().getMemberAt(0).get_text());
//                        SelectedUsersGroups.editComplete();                    
//                        //alert('modifyDeleteGroupItem_onSelect_2');
//                        SelectedUsersGroups.callback();                
//                }
//            }, 100);
            //end
        }
        function EditGroup(groupId)
        {
            CheckSession();                  
            document.getElementById(hiddenGroupId).value=groupId;
            //selectedItem=eventArgs.get_item();       
            //alert('modifyDeleteGroupItem_onSelect_1');
            ModifyDeleteGroupCallBack.Callback(groupId);  
            //For Populating the selected user groups
            
            setTimeout(function() {        
                if(SelectedUsersGroups!=null)
                {
                    var insertItem = SelectedUsersGroups.get_table().addEmptyRow(0);
                        SelectedUsersGroups.edit(insertItem);
                        insertItem.setValue(0,"_" + groupId);
                        SelectedUsersGroups.editComplete();                    
                        //alert('modifyDeleteGroupItem_onSelect_2');
                        SelectedUsersGroups.callback();                
                }
            }, 100);        
        }
        function GroupCallBack_Completed(sender,eventArgs)
        {            
            //ModifyDeleteAvailableUsers.render();              
            //alert('GroupCallBack_Completed');
            setTimeout("ModifyDeleteAvailableUsers.render();",100);
        }
      
      /** Methods for Context Menu on Modify/Delete Cms User Page **/

//function CheckUserWorklows(selectedGroupIdInGrid,memberType,username,sitegroupId)
//{
//    var returnValueOfMethod = IsGroupHavingAnyWorkflow(selectedGroupIdInGrid,memberType,username,sitegroupId); //1 : MemberType
//    return returnValueOfMethod;
//}
       
      
   function group_onItemSelect(sender,eventArgs)
   {                 
      CheckSession();             
    var itemClicked = eventArgs.get_item();        
    if(itemClicked != null)
    {
        itemId = itemClicked.get_id();
//        var selectedOption = null;
        var selectedGroupId = groupItem.Data[0];
        var selectedGroupName = groupItem.Data[1];
        
        if(sender != null)
        {
            sender.SelectedItem = 1;
        }
        var memberType = 1;
        //var url = '../../DeactivateUserPopup.aspx';
        
        switch(itemId)
        {            
            case "AddGroupMenuItem":
                Group_ClearForm();
                document.getElementById(SaveBtnClientId).disabled=false;                    
                document.getElementById(SaveBtnClientId).className = document.getElementById(SaveBtnClientId).className.replace(/ disabled/g, " ");
                break;
                
            case "EditGroupMenuItem":
                EditGroup(selectedItemGroupId);
                 var divObj = document.getElementById(jdivPageTopContainerId);
                 if (divObj!=null)
                 {
                    divObj.innerHTML =''; 
                    divObj.style.display = "none";
                 }                   
                if (selTab ==0) //Active Group
                { 
                    document.getElementById(SaveBtnClientId).disabled=false;                    
                    document.getElementById(SaveBtnClientId).className = document.getElementById(SaveBtnClientId).className.replace(/ disabled/g, " ");
                }
                //for deactive group is should be disabled..
                break;
            case "DeleteGroupMenuItem":
                memberType = 2;
                var statusOfOperation ="";
                var yesOrNo = window.confirm(__JSMessages["AreYouSureYouWantToDeleteTheSelectedRecord"]);  
                             
                if(yesOrNo)
                {
                
//                   var count = CheckUserWorklows(selectedGroupId,memberType ,selectedGroupName,null);                                                                                                                                                                                                          
//                    if(count > 0 && selTab==0)
//                    {
//                        selectedOption = 1;
//                        //isUpdated = UpdateSessionData(selectedGroupId,selectedGroupName,1,selectedOption,null);
//                        //if(isUpdated)                      
//                        PopUp('DeleteDeactivateMember',url + "?id="+selectedGroupId+"&type="+memberType+"&mode="+selectedOption ,'Delete User.');
//                    }
//                    else
//                    {
                       //Delete the user directly by using the call back method.     
                       statusOfOperation = DeleteDeactivateGroup(selectedGroupId,selectedGroupName,1,null); //Isdelete:1, IsDeactivate: 0
                       //debugger
                        if(statusOfOperation == "True" || statusOfOperation == true)
                        {       
                            if(selTab==0)
                            {                                            
                                grdActivatedGroups.deleteItem(groupItem);                                                                            
                                grdActivatedGroups.render();
                            }
                            else if(selTab==1)
                            {
                                grdDeActivatedGroups.deleteItem(groupItem);                                                                            
                                grdDeActivatedGroups.render();
                            }
                            var activeGroupName=document.getElementById(GroupClientId).value;
                            if(selectedGroupName==activeGroupName) 
                            {
                                document.getElementById(GroupClientId).value="" ;
                                document.getElementById(DescClientId).value=""  ;
                                
                                 if(typeof SecurityLevel != "undefined") {
                                   ClearListBox(document.getElementById( SecurityLevel )) ;
                                   }
                            }
                            //document.getElementById(SelGroupClientId).clear(); 
//                            if(selTab==0)
//                            {
//                                var grid=window[AvailGroupClientId];              
//                                var item;
//                                 for(var i=0;i<grid.get_table().getRowCount();i=i+1)
//                                 {
//                                    if(grid.get_table().getRow(i).Data[0]==selectedGroupId)
//                                    {
//                                        item=grid.get_table().getRow(i);
//                                        break;
//                                    }
//                                    
//                                 }
//                                 grid.deleteItem(item);                                                                              
//                                 grid.render(); 
//                             }
                             if(selectedGroupName==activeGroupName) 
                             {
                                 //removing Index terms                                                        
                                 DeleteIndexes(selectedGroupId); 
                                 ClearUserGroups();  
                             }                          
                             if(selTab == 0)
                             {                                                             
                                if(selectedItem!=null)
                                {                
                                    //modifyDeleteGroup.Select(selectedItem); 
                                }
                             }                                                                                                          
                        }        
                                      
                    //}
                    
                }
                //window.open("../../DeactivateUserPopup.aspx?id="+selectedGroupId+'&type='+1+'&mode='+selectedOption); //Mode 1 : delete ,Mode 2: deactivate
               
                break;
            case "BreakMenuItem":
                selectedOption = -1;
                break;
            case 'DeactivateGroupMenuItem':
                memberType = 2;
                var statusOfOperation="";                
                //For activation
                if(itemClicked.get_text() == WEB_ACTIVATE_USER)
                {
                    selectedOption = 3;
                }
                else if(itemClicked.get_text() == WEB_DEACTIVATE_USER)
                {
                    selectedOption = 2;
                }
                var yesOrNo = false;
                if(selectedOption == 2)
                {
                    yesOrNo = window.confirm(__JSMessages["AreYouSureYouWantToDeactivateTheSelectedGroup"]);
                }
                else if(selectedOption == 3) 
                {
                    yesOrNo = window.confirm(__JSMessages["AreYouSureYouWantToActivateTheSelectedGroup"]);
                }
                //end
                
                //var yesOrNo = window.confirm(GetMessage("DeactivateConfirmation"));
                if(yesOrNo)
                {
//                    var count = CheckUserWorklows(selectedGroupId,memberType,selectedGroupName,null);                                                                                                                                                                                                                                                      
//                    if(count > 0 && selectedOption == 2)
//                    {                        
//                        PopUp('DeleteDeactivateMember',url + "?id="+selectedGroupId+"&type="+memberType+"&mode="+selectedOption ,'Deactivate User.');
//                    }
//                    else
//                    {
                       //Delete the user directly by using the call back method.     
                       statusOfOperation = DeleteDeactivateGroup(selectedGroupId,selectedGroupName,selectedOption,null); //Isdelete:1, IsDeactivate: 0
                        if(statusOfOperation== "True" || statusOfOperation == true)
                        {
                            //modifyDeleteGroup.deleteItem(modifyDeleteGroup.getSelectedItems()[0]);    
                            
                            ActivateDeactivateGroup();

                            var activeGroupName=document.getElementById(GroupClientId).value;
                            if(selectedOption == 2 && selectedGroupName==activeGroupName) 
                            {
                                document.getElementById(GroupClientId).value="";
                                document.getElementById(DescClientId).value="";        
                                 if(typeof SecurityLevel != "undefined") {
                                   ClearListBox(document.getElementById( SecurityLevel )) ;
                                   }                     
                            }
//                            if(selectedOption == 2)
//                            {
//                                var grid=window[AvailGroupClientId];              
//                                var item;
//                                 for(var i=0;i<grid.get_table().getRowCount();i=i+1)
//                                 {
//                                    if(grid.get_table().getRow(i).Data[0]==selectedGroupId)
//                                    {
//                                        item=grid.get_table().getRow(i);
//                                        break;
//                                    }
//                                    
//                                 }
//                                 grid.deleteItem(item);                                                                              
//                                 grid.render(); 
//                             }
                                                                                     
                             if(selectedOption == 2 && selectedGroupName==activeGroupName) 
                             {
                                //Removing Index Terms
                                DeleteIndexes(selectedGroupId);  
                                ClearUserGroups();
                             }
                             
//                             if(selectedOption == 3)
//                             {                                
//                                if(selectedItem!=null)
//                                {                
//                                    modifyDeleteGroup.Select(selectedItem); 
//                                }
//                             }
                                                                               
                        }                                                                                                 
                    //}                    
                    
                }
                
                
                //PopUp('DeleteDeactivateMember',url + "?id="+selectedGroupId+"&type="+1+"&mode="+selectedOption ,'Deactivate User.');
                //window.open("../../DeactivateUserPopup.aspx?id="+selectedGroupId+'&type='+1+'&mode='+selectedOption); //Mode 1 : delete ,Mode 2: deactivate
                break;
            default:
                selectedOption = null;
                alert(__JSMessages["MenuItemNotAdded"]);
                break;
        }
        
        if(selectedOption != null)
        {
           // selectedGroupId = cmsUsers.getSelectedItems()[0].Data[5];
             
            //doPostBackForContextMenuSelection(selectedGroupId,selectedOption);
        }
    }
    
}

function ClearListBox(lb){
//  for (var i=lb.options.length-1; i>=0; i--){
//    lb.options[i] = null;
//  }
  lb.selectedIndex = -1;
}

function DeleteIndexes(GroupId)
{
    
    var List= document.getElementById(IndexTermsClientId);
     var indexColl="";
     if(List)
     {
        
         var len=null;
         if(browser == "ie")
         {
            len=List.options.length;
            for(row=List.options.length-1;row>=0; row=row-1)
             {
                if(row==len-1)
                {
                    indexColl=List.options(row).value;
                }
                else
                {
                    indexColl= indexColl + "," + List.options(row).value;
                }
                
                List.options.remove(row);                   
             }
         }
         else
         {
            len=List.length;
            for(row=List.length-1;row>=0; row=row-1)
             {
                if(row==len-1)
                {
                    indexColl=List.options[row].value;
                }
                else
                {
                    indexColl= indexColl + "," + List.options[row].value;
                }
                                 
                List.remove(row);
                  
             }
         }
     }
}

 function PopUp(id,url,title)
    {
        deleteDeactivateMemberPopup = dhtmlmodal.open(id, 'iframe', 
            url, title ,'width=390px,height=432px,center=1,resize=0,scrolling=0'); 
        deleteDeactivateMemberPopup.onclose=function()
        { 
            var a=document.getElementById(id);
            var ifr=a.getElementsByTagName("iframe");
            window.frames[ifr[0].name].location.replace("about:blank") ; 
            return true;
        }      
    }
    
    function ActivateDeactivateGroup()
    {
        var searchText=document.getElementById(SearchClientId).value; 
        if(searchText== __JSMessages["Search"])
        {
            searchText="";
        } 
        if(selectedOption == 2)
        {
            
            //grdActivatedGroups.Search(searchText, false); 
             grdActivatedGroups.render();
             grdActivatedGroups.deleteItem(groupItem);                            
             grdActivatedGroups.render();                            
                var updateItem = grdDeActivatedGroups.get_table().addEmptyRow(0);
                grdDeActivatedGroups.edit(updateItem);
                updateItem.setValue(0,groupItem.GetMemberAt(0).Value);                                
                updateItem.setValue(1,groupItem.GetMemberAt(1).Value);                
                grdDeActivatedGroups.editComplete(); 
                grdDeActivatedGroups.render();
             
        }
        else if(selectedOption == 3) 
        {
                grdDeActivatedGroups.deleteItem(groupItem);                            
                grdDeActivatedGroups.render();   
             
                //grdActivatedGroups.Search(searchText, false); 
                grdActivatedGroups.render();            
                var updateItem = grdActivatedGroups.get_table().addEmptyRow(0);
                grdActivatedGroups.edit(updateItem);
                updateItem.setValue(0,groupItem.GetMemberAt(0).Value);                                
                updateItem.setValue(1,groupItem.GetMemberAt(1).Value);                
                grdActivatedGroups.editComplete(); 
                grdActivatedGroups.render();
                
//                var grid=window[AvailGroupClientId];  
//                var insertItem = grid.get_table().addEmptyRow(0);
//                grid.edit(insertItem);
//                insertItem.setValue(0,groupItem.GetMemberAt(0).Value);                                
//                insertItem.setValue(1,groupItem.GetMemberAt(1).Value);                
//                grid.editComplete(); 
//                grid.render();
                
        }
    
        
        
    }
  
//function PopupReturnCall(returnValue)
//    {   
//         
//        if(returnValue=="True" || returnValue==true)
//        {           
//        
//            var selectedGroupName = groupItem.Data[1];
//            if(selectedOption == 1)
//            {
//                if(selTab==0)
//                { 
//                    grdActivatedGroups.deleteItem(groupItem);                                                                            
//                    grdActivatedGroups.render();
//                }
//                else if(selTab==1)
//                { 
//                    grdDeActivatedGroups.deleteItem(groupItem);                                                                            
//                    grdDeActivatedGroups.render();
//                }
//            }  
//            else
//            {
//                ActivateDeactivateGroup();
//            }                      
//            var activeGroupName=document.getElementById(GroupClientId).value;
//            if(selectedGroupName==activeGroupName) 
//            {
//                document.getElementById(GroupClientId).value="";
//                document.getElementById(DescClientId).value="";   
//            }
//            if(selTab==0)
//            {      
//                var grid=window[AvailGroupClientId];              
//                var item;
//                 for(var i=0;i<grid.get_table().getRowCount();i=i+1)
//                 {
//                    if(grid.get_table().getRow(i).Data[0]==groupItem.Data[0])
//                    {
//                        item=grid.get_table().getRow(i);
//                        break;
//                    }
//                    
//                 }
//                 grid.deleteItem(item);                                                                              
//                 grid.render(); 
//             }
//             if(selectedGroupName==activeGroupName) 
//             {
//                //Removing Index Terms              
//                DeleteIndexes(groupItem.Data[0]);
//                ClearUserGroups();                
//             }
//             if(selTab == 1)
//             {                                    
//                            
//                if(groupItem!=null)
//                {    
//                    document.getElementById(GroupClientId).value="";
//                    document.getElementById(DescClientId).value="";   
//                    DeleteIndexes(groupItem.Data[0]);
//                    ClearUserGroups();
//                }
//             }     
//         }
//    }
//    
    function ClearUserGroups()
    {
        
        var grid=window[MembersClientId];              
        var item;
         for(var i=grid.get_table().getRowCount()-1;i>=0;i=i-1)
         {
            item=grid.get_table().getRow(i);
            grid.deleteItem(item);             
         }
                                                                                      
         grid.render(); 
    }
    
//    function Group_ClearForm()
//    {
//        document.getElementById(GroupClientId).value="";
//        document.getElementById(DescClientId).value="";                                             
//        
//        var grid=window[MembersClientId];                      
//        var item;
//         for(var i=grid.get_table().getRowCount()-1;i>=0;i=i-1)
//         {
//            item=grid.get_table().getRow(i);
//            grid.deleteItem(item);                         
//         }                                                                                      
//         grid.render();       
//         
//         var List= document.getElementById(IndexTermsClientId);                  
//         if(List)
//         {
//         
//         
//             var len=null;
//             if(browser == "ie")
//                {
//                    len=List.options.length;
//                    for(row=List.options.length-1;row>=0; row=row-1)
//                     {                                        
//                        List.options.remove(row);                        
//                     }        
//                }
//                else
//                {                    
//                    len=List.length;
//                    for(row=List.length-1;row>=0; row=row-1)
//                     {                                                        
//                         List.remove(row);                        
//                     }        
//                }
//          }
//          
//          if( typeof SecurityLevel != "undefined") {
//                clearlistbox(document.getElementById( SecurityLevel )) ;
//             }
//         
//         
//         
//    }
//    

    function Group_ClearForm()
    {
        selectedGroupName ='';
        selectedGroupId=''
        selectedGridItem = null;
        selectedItemGroupId = ''; 
        document.getElementById(hiddenGroupId).value = '';   
        
        document.getElementById(GroupClientId).value="";
        document.getElementById(DescClientId).value="";                                             
        
        document.getElementById(SaveBtnClientId).disabled=true;
        document.getElementById(SaveBtnClientId).className += " disabled";
        
//        SelectedUsersGroups.selectAll();
//        SelectedUsersGroups.deleteSelected();   // grid is in callback
        var grid=window[MembersClientId];                      
        var item;
         for(var i=grid.get_table().getRowCount()-1;i>=0;i=i-1)
         {
            item=grid.get_table().getRow(i);
            grid.deleteItem(item);                         
         }                                                                                      
         grid.render();       
          
        var List= document.getElementById(IndexTermsClientId);                  
        if(List)
        {
            if(browser == "ie")
            {
                len=List.options.length;
                for(row=List.options.length-1;row>=0; row=row-1)
                 {                                        
                    List.options.remove(row);                        
                 }        
            }
            else
            {                    
                len=List.length;
                for(row=List.length-1;row>=0; row=row-1)
                 {                                                        
                     List.remove(row);                        
                 }        
            }
        }          
          
        if( typeof SecurityLevel != "undefined") {
            ClearListBox(document.getElementById( SecurityLevel )) ;
         }
         var divObj = document.getElementById(jdivPageTopContainerId);
         if (divObj!=null)
         {
            divObj.innerHTML =''; 
            divObj.style.display = "none";
         }   
    }
    
    function gE(elementName)
    {
        return document.getElementById(elementName);  
    } 

 
        function TabSelectionChanged(sender, eventArgs)
        {       
            Group_ClearForm();
                                                                     
            var tabText = eventArgs.get_tab().get_id();          
            document.getElementById(SearchClientId).value= __JSMessages["Search"];
            var searchText=document.getElementById(SearchClientId).value;
            if (searchText == __JSMessages["Search"])
            {
                searchText="";
            }
            if(tabText == "tabActiveGroup")
            {   
                if(window["grdActivatedGroups"]!=null)
                {                       
                    if(activeTableData)
                    {      
                        grdActivatedGroups.get_table().Data=activeTableData;                                 
                        grdActivatedGroups.get_table().Grid.Data=activeTableData;  
                    }    
                    //grdActivatedGroups.Search(searchText, false);                
                    gE(HdnSelectedTabIdHolder).value = 0;
                    document.getElementById(SaveBtnClientId).disabled=false;                    
                    document.getElementById(SaveBtnClientId).className = document.getElementById(SaveBtnClientId).className.replace(/ disabled/g, " ");
                    setTimeout("grdActivatedGroups.render();",100);
                    if(grdActivatedGroups.getSelectedItems().length>0 && grdActivatedGroups.getSelectedItems()[0].getMemberAt(0).get_text()!='')
                    {                 
                    //alert('tabselection changed_1');   
                        ModifyDeleteGroupCallBack.Callback(grdActivatedGroups.getSelectedItems()[0].getMemberAt(0).get_text());                     
                        for(var i=SelectedUsersGroups.get_table().getRowCount()-1;i>=0;i=i-1)
                         {
                            item=SelectedUsersGroups.get_table().getRow(i);
                            SelectedUsersGroups.deleteItem(item);             
                         }
                                                                                                      
                         SelectedUsersGroups.render();                        
                        var insertItem = SelectedUsersGroups.get_table().addEmptyRow(0);
                        SelectedUsersGroups.edit(insertItem);
                        insertItem.setValue(0,"_" + grdActivatedGroups.getSelectedItems()[0].getMemberAt(0).get_text());
                        SelectedUsersGroups.editComplete();                    
                   //alert('tabselection changed_2');   
                        SelectedUsersGroups.callback();                                    
                    }
                 }
                 else
                 {                    
                    eventArgs.set_cancel(true); 
                 }
            }
            else if(tabText == "tabDeactivatedGroup")
            {                
                if(window["grdDeActivatedGroups"]!=null)
                {                    
                    if(deactiveTableData)
                    {
                        grdDeActivatedGroups.get_table().Data=deactiveTableData;                                 
                        grdDeActivatedGroups.get_table().Grid.Data=deactiveTableData;
                    }
                    //grdDeActivatedGroups.Search(searchText, false);                
                    gE(HdnSelectedTabIdHolder).value = 1;                    
                    document.getElementById(SaveBtnClientId).disabled=true;
                    document.getElementById(SaveBtnClientId).className += " disabled";
                    
                    setTimeout("grdDeActivatedGroups.render();",100);
                    if(grdDeActivatedGroups.getSelectedItems().length>0)
                    {                    
                    //alert('tabselection changed_3');  
                        ModifyDeleteGroupCallBack.Callback(grdDeActivatedGroups.getSelectedItems()[0].getMemberAt(0).get_text()); 
                        for(var i=SelectedUsersGroups.get_table().getRowCount()-1;i>=0;i=i-1)
                         {
                            item=SelectedUsersGroups.get_table().getRow(i);
                            SelectedUsersGroups.deleteItem(item);             
                         }
                                                                                                      
                         SelectedUsersGroups.render();                        
                        var insertItem = SelectedUsersGroups.get_table().addEmptyRow(0);
                        SelectedUsersGroups.edit(insertItem);
                        insertItem.setValue(0,"_"+ grdDeActivatedGroups.getSelectedItems()[0].getMemberAt(0).get_text());
                        SelectedUsersGroups.editComplete();                    
                    //alert('tabselection changed_4');  
                        SelectedUsersGroups.callback();
                    }
                }
                else
                {                    
                    eventArgs.set_cancel(true); 
                }
            }
       
        }

        //Validator for description
        function validator(sender,args)
        {        
           var description = document.getElementById(DescClientId).value;            
           var compare1 ="<";
           var compare2 =">";

            if(description.search(compare1) == -1 || description.search(compare2) == -1)
            {

                args.IsValid=true;         
    
            }

            else
            {                       
                args.IsValid=false;
            } 
        
        }    
        
        
        
function RightArrowClick(grdUsers,grdUserGroups,selectedTab)
{     
    var rvalue=CheckSession();     
    if(rvalue==false || rvalue=="False" || rvalue=="false")
    {        
        return;
    } 
    var selectedGroup=null;       
    
    
    if(document.getElementById(GroupClientId))
    {
        selectedGroup= document.getElementById(GroupClientId).value;
    }    
    
    if(FormName=="ModifyDeleteGroup")
    {
        if(grdActivatedGroups!=null)
        {
            if(grdActivatedGroups.getSelectedItems().length>0)
            {
                selectedGroup=grdActivatedGroups.getSelectedItems()[0].GetMemberAt(1).get_text();
            }
        }
    }
    
    var grd=new ComponentArt.Web.UI.Grid();
//    if(selectedTab.getSelectedTab().get_id()=="zero")
//    {
    for (var indx = 0; indx <= grdUsers.getSelectedItems().length-1; indx++)
    
     {
        var isExists = false;

        for (var i = grdUserGroups.get_table().getRowCount() - 1; i >= 0; i--)
         {
            var id = grdUserGroups.get_table().getRow(i).get_cells(0)[0].Value;
            if (id == grdUsers.getSelectedItems()[indx].GetMemberAt(0).get_text())
            {
                isExists = true;
            }
        }
        if (isExists == false) {
            grdUserGroups.beginUpdate();
            var rowitem = grdUserGroups.get_table().addEmptyRow(0);
            //grdUserGroups.editComplete();

            var selItem = grdUsers.getSelectedItems()[indx]
            grdUserGroups.edit(rowitem);
            id = selItem.GetMemberAt(0).get_text()
            var firstName = selItem.GetMemberAt(2).get_text()
            var lastName = selItem.GetMemberAt(3).get_text()

            if (firstName == '' || firstName == null) {
                firstName = selItem.GetMemberAt(1).get_text()
            }
            //var rowitem = grdUserGroups.get_table().getRow(0);

            rowitem.setValue(0, id);

            rowitem.setValue(1, firstName);
            rowitem.setValue(2, "1");
            rowitem.setValue(3, lastName);
            grdUserGroups.editComplete();
            //alert(firstName + ';' + lastName);
        }

    }
        //Unselecting the selected items        
        var selectedItems=grdUsers.getSelectedItems();
        for (var indx = 0; indx <= grdUsers.getSelectedItems().length - 1; indx++)       
        {
            grdUsers.unSelect(selectedItems[indx]);            
        }
        grdUserGroups.render();       
//    }
//    else if(selectedTab.getSelectedTab().get_id()=="one")
//    {
//        for(var indx in grdGroups.getSelectedItems())
//        {
//            var isExists=false;
//            if(selectedGroup!=grdGroups.getSelectedItems()[indx].GetMemberAt(1).get_text())
//            {
//                for(var i=grdUserGroups.get_table().getRowCount()-1;i>=0;i--)
//                {                
//                    var id=grdUserGroups.get_table().getRow(i).get_cells(0)[0].Value;                
//                    if(id==grdGroups.getSelectedItems()[indx].GetMemberAt(0).get_text())
//                    {
//                        isExists=true;
//                    }
//                }
//            }
//            else
//            {
//                isExists=true;   
//            }            
//            if(isExists==false || isExists=="False")
//            {
//                var currentId=grdGroups.getSelectedItems()[indx].GetMemberAt(0).get_text();                
//                var parentCount=0;
//                if(hiddenGroupId)
//                {
//                    if(document.getElementById(hiddenGroupId).value!="")
//                    {
//                        parentCount=GetParentLevelsCount(document.getElementById(hiddenGroupId).value);
//                    }
//                }                
//                var childrenCount=GetChildrenLevelsCount(currentId);
//                isExists=AllowAddGroup(parentCount,childrenCount);
//                if(isExists==true || isExists=="True")
//                {
//                    var currentName=grdGroups.getSelectedItems()[indx].GetMemberAt(1).get_text();
//                    //alert(currentName+" has exceeded maximum level count");
//                    alert("The main group already contains the maximum number (" + GroupMaxLevel + ") of sub-group levels. This member (" + currentName +  ") cannot be added.");
//                }
//            }
//            if(isExists==false || isExists=="False")
//            {
//                grdUserGroups.beginUpdate();
//                var rowitem =grdUserGroups.get_table().addEmptyRow(0);
//                //grdUserGroups.editComplete();
//                
//                var selItem=grdGroups.getSelectedItems()[indx]
//                grdUserGroups.edit(rowitem);
//                id=selItem.GetMemberAt(0).get_text();
//                name=selItem.GetMemberAt(1).get_text();
//                                                                
//                //var rowitem = grdUserGroups.get_table().getRow(0);
//                
//                rowitem.setValue(0,id);
//                rowitem.setValue(1,name);
//                rowitem.setValue(2,"2"); 
//                grdUserGroups.editComplete();      
//            }                     
//               
//                                   
//        }
//        //Unselecting the selected items        
//        var selectedItems=grdGroups.getSelectedItems();       
//        for(var itemIndex in selectedItems)       
//        {               
//            grdGroups.unSelect(selectedItems[itemIndex]);            
//        }
//        grdUserGroups.render();
//    }
}

function LeftArrowClick(grdUserGroups)
{
    //CheckSession(); 
    var rvalue=CheckSession();     
    if(rvalue==false || rvalue=="False" || rvalue=="false")
    {        
        return;
    } 
    for(var i=grdUserGroups.get_table().getRowCount()-1;i>=0;i--)
    {     
             
        var item=grdUserGroups.get_table().getRow(i);   
         
         if(item.Selected==true)
         {       
            grdUserGroups.unSelect(item);                       
            grdUserGroups.deleteItem(item);                
         } 
          
        
    }
    
    grdUserGroups.render();
                    
}