﻿var navCategoryItem;

function grdNavigationCategories_onContextMenu(sender, eventArgs) {
    var e = eventArgs.get_event();
    navCategoryItem = eventArgs.get_item();
    var IsExcluded = navCategoryItem.getMember(4).get_text();
    var menuItems = mnuNavCategory.get_items();
    for (i = 0; i < menuItems.get_length(); i++) {
        menuItems.getItem(i).set_visible(false);
        if (menuItems.getItem(i).get_id() == 'cmAdd' && IsExcluded == "true") {
            menuItems.getItem(i).set_visible(true);
        }
        else if (menuItems.getItem(i).get_id() == 'cmRemove' && IsExcluded == "false") {
            menuItems.getItem(i).set_visible(true);
        }

    }
    mnuNavCategory.showContextMenuAtEvent(e);
}
function mnuNavCategory_onItemSelect(sender, eventArgs) {

    var navParameter = navCategoryItem.getMember(3).get_text();
    if (eventArgs.get_item().get_id() == "cmAdd") {
        navParameter += "~" + "0";
    }
    else if (eventArgs.get_item().get_id() == "cmRemove") {
        navParameter += "~" + "1";
    }
    grdNavigationCategories.set_callbackParameter(navParameter);
    grdNavigationCategories.callback();
}