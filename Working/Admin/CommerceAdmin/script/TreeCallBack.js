﻿var contextData;    // context data being passed from tree context menu
var treeNode;   // selected tree node
var nodeId;     // id of the node where context menu is called
var treeOperation;  // operation being performed on tree node e.g. Add New Node / Rename
var newNode;        // child node being added to the selected node
var loadGrid=true;  // whether to load the grid data on node select ( work around for CA node edit problem)
var currentMenu;
var copyParent='false';
var isCallback;     // is in Call back or not
var isRenameInProgess = 'false';

    function treeProductTypes_onContextMenu(sender, eventArgs) {
        if (isRenameInProgess != 'true' && isCallback !='true')
        {
            var e = eventArgs.get_event();
            var selectedTreeNode = eventArgs.get_node();
            if (PageLevelChecking(eventArgs, mnuProductTypes))
            {
                mnuProductTypes.showContextMenuAtEvent(e, selectedTreeNode);
            }
        }
    }
    function mnuProductTypes_onItemSelect(sender, eventArgs) {
        var selectedMenuId = eventArgs.get_item().get_id();
        currentMenu =mnuProductTypes; 
          contextData =  currentMenu.get_contextData();
            if (selectedMenuId != null) {
            //alert(selectedMenuId);
                switch (selectedMenuId) {
                    case "cmAddPT":
                        menuTree_AddNode(sender, eventArgs);
                        break;
                     case "cmAddPTC":
                        copyParent ='true';
                        menuTree_AddNode(sender, eventArgs);
                        break;
                    case "cmRenamePT":
                        menuTree_RenameNode(sender, eventArgs);
                        break;
                    case "cmDeletePT":
                        menuTree_DeleteNode(sender, eventArgs);
                        break;
                }
             }
    }
    
    function menuTree_AddNode(sender, eventArgs) {
        var selectedMenuId = eventArgs.get_item().get_id();
            if (selectedMenuId != null) {
                contextData =  currentMenu.get_contextData();
                newNode = null;
                trvProductTypes.beginUpdate();
                newNode = new ComponentArt.Web.UI.TreeViewNode(); 
                newNode.set_text(__JSMessages["NewNode"]); 
                newNode.set_id('New Node'); 
                newNode.set_value(0);
      
                contextData.Expand();
                contextData.get_nodes().add(newNode); 
                trvProductTypes.endUpdate();
                contextData.Expand();
                currentMenu.hide();
                trvProductTypes.render();
                loadGrid = false;
                newNode.select();
                newNode.edit();
                loadGrid = true;
                nodeId = contextData.get_id();
                treeOperation = "New";
            
             }
    }
    
    function menuTree_RenameNode(sender, eventArgs) {
        treeOperation="ReName";
        contextData.edit();            
    }
    
    function menuTree_DeleteNode(sender, eventArgs) {
        var selectedMenuId = eventArgs.get_item().get_id();
            if (selectedMenuId != null) {
                var isDel = confirm(__JSMessages["AreYouSureYouWantToDoThis"])
    
                if(isDel==true)
                {                
	                var childNodes = contextData.get_nodes(); // Nods under one child of root
                    if (childNodes.get_length() > 0 )
                    {
                        alert(__JSMessages["FirstDeleteTheChildNodesBeforeDeletingThis"]); 
                    }
                    else
                    {       
                        isCallback = 'true';         
                        var id = DeleteProductType(contextData.get_id());
                        if(id=="True")
                        {
                            var parentNode =contextData.get_parentNode();
                            contextData.remove();                  
                            parentNode.select();
                        }
                        else
                        {
                            if(id!=null && id.toString().length>0)
                                alert(id.substring(1));
                            else
                                alert(__JSMessages["DeletingFailedPleaseTryAgain"]);
                        }
                        isCallback = 'false';
                    }
                }
            
             }
    }




//function Menu_onItemSelect(sender, eventArgs)
//{
//  Tree_ContextMenuClickHandler(sender,eventArgs);
//}
//       
//function Tree_ContextMenuClickHandler(sender, eventArgs)
//{
//    //Check's if session expired and reload the page.
//    CheckSession();
//    var selectedMenuId = eventArgs.get_item().get_id();
//    nodeId=null;
//    contextData =  currentMenu.get_contextData();
//    
//    if(selectedMenuId == "AddSubMenu")
//    {
//   
//        newNode = null;
//        TrvLibrary.beginUpdate();
//        newNode = new ComponentArt.Web.UI.TreeViewNode(); 
//        newNode.set_text('New Node'); 
//        newNode.set_id('New Node'); 
//        
//        if(contextData.Value ==1)
//            newNode.set_value(1);
//        else if(contextData.Value ==2)
//            newNode.set_value(1);
//        else if(contextData.Value ==3)
//            newNode.set_value(0);
//        else if(contextData.Value ==4)
//            newNode.set_value(0);
//        else
//        newNode.set_value(contextData.Value);
////        if(contextData.Value!=2)
////            newNode.set_value(contextData.Value);
////        else
////            newNode.set_value(1);
//        
//        contextData.Expand();
//        contextData.get_nodes().add(newNode); 
//        TrvLibrary.endUpdate();
//          contextData.Expand();
//        currentMenu.hide();
//        TrvLibrary.render();
//        
//        loadGrid = false;
//        newNode.select();
//        newNode.edit();
//        loadGrid = true;
//        
//        nodeId = contextData.get_id();
//        treeOperation = "New";
//    }
//    else if( selectedMenuId == "RenameMenu")
//    {
//        treeOperation="ReName";
//       // contextData.set_text(trim(contextData.get_text()));
//        contextData.edit();
//        //contextData.Text = trim(contextData.get_text());
//    }
//    else if( selectedMenuId == "DeleteMenu")
//    {
//    var isDel =confirm("Any files / items in this directory will be moved to the Unassigned directory and this directory will be deleted. Are you sure you want to do this?")
//    
//        if(isDel==true)
//        {
//            var id = DeleteNode(contextData.get_id());
//            if(id=="True")
//            {
//                var parentNode =contextData.get_parentNode();
//                contextData.remove();                  
//                if (contextData.get_depth() == 1)
//                {    
//                    var rootParNode = TrvLibrary.get_nodes();
//                    var rootChildNodes = rootParNode.getNode(0).get_nodes();
//                    var UnAssignNode = rootChildNodes.getNode(0);
//                    UnAssignNode.select();
//                }
//                else   
//                {            
//                    parentNode.select();
//                }
//            }
//            else
//            {
//             if(id!=null && id.toString().length>0)
//                alert(id.substring(1));
//             else
//               alert("Deleting Failed. Please try again");
//            }
//         }
//    }
//    else if(selectedMenuId=='ViewEditPermission')
//    {
//        window.location = "../../Administration/User/ViewEditPermissionsByTarget.aspx?objId=" + contextData.get_id() + "&objType=" + ObjectType;
//    }
//    if(selectedMenuId == "AddImages")
//    {
//            TrvLibrary.SelectNodeById(contextData.get_id());
//          Imagewindow = dhtmlmodal.open('ImageId', 'iframe', '../../ImageQuickUpload.aspx', 'Upload Image', 'width=360px,height=440px,center=1,resize=0,scrolling=1');
//           Imagewindow.onclose=function()
//        { 
//            var a=document.getElementById('ImageId');
//            var ifr=a.getElementsByTagName("iframe");
//            window.frames[ifr[0].name].location.replace("about:blank") ; 
//            return true;
//        }  
//    }
//      
//}

//      
//function Tree_nodeSelect(sender, eventArgs)
//{   
//   //Check's if session expired and reload the page.
//    CheckSession();
//    //store the last selected node in the global variable
//    window["SelectedNodeId"] = eventArgs.get_node().get_id();
//  //if user is a view the below node is set to 1
//    currentTreeNodeRole =1;
//    if(eventArgs.get_node().Value==1 ||eventArgs.get_node().Value==2 ||eventArgs.get_node().Value==3 ||eventArgs.get_node().Value==4 || eventArgs.get_node().Value==5)
//    {
//    //if the user is the manager then the below variable is set to 2; 
//    currentTreeNodeRole =2;
//    
//    }
//        
//    if(loadGrid==true)
//    {
//        if(self['TrvLibrary_nodeSelect'])
//        {
//            TrvLibrary_nodeSelect(sender,eventArgs);//This function is to be in page, when any node is select it will be called
//        }
//     }
//    
//        if(self['Library_DirectoryView'])
//        {
//            Library_DirectoryView(sender,eventArgs);
//        }
//   
//       // alert("nodeSelect");
//        
////        var GridCallback = window['GridCallBack'];
////          var Grid = window["grdPageLibrary"];
////         
////          var rowitem = Grid.get_table().addEmptyRow(0);
////          
////          Grid.edit(rowitem);
////          var gridc = Grid.get_table().get_columns();
////          rowitem.setValue(gridc.length-1,eventArgs.get_node().get_value());
////          Grid.editComplete();
////          Grid.callback();
////          window["SelectedNodeId"] = eventArgs.get_node().get_value();          
//                             
//        //GridCallBack.Callback(eventArgs.get_node().Value); 
// }
 function TreeView_NodeRenamed(sender, eventArgs)
 {
 loadGrid=false;
 eventArgs.get_node().select();
 loadGrid=true;
 }
//

function PutInEditMode(nodeId, sender, eventArgs)
{
    var node = trvProductTypes.findNodeById(nodeId)
    node.edit();
} 

function TreeView_onNodeBeforeRename(sender, eventArgs)
{  
   //Check's if session expired and reload the page.
    //CheckSession();

    var result =null;
    var node=eventArgs.get_node();
    var parNode= eventArgs.get_node().get_parentNode();   
    var newText = Trim(eventArgs.get_newText());
    var oldText =eventArgs.get_node().get_text();
    var parentId = parNode.get_id();
    var canProceed = true;  
  
    if (newText==null || newText =='')
    {
        alert(__JSMessages["NodeNameShouldNotBeEmpty"]); 
       canProceed =false;
       window.setTimeout('PutInEditMode(\'' + node.get_id() + '\')',1);
       isRenameInProgess = 'true';
    }
    else 
    {
        // check for same node name in the same level
        var childNodes = parNode.get_nodes();
        var currentNodeId = eventArgs.get_node().get_id();
        for (var i = 0; i < childNodes.get_length(); i++)
        {
            var childNodeText = Trim(childNodes.getNode(i).get_text());
            var childNodeId = childNodes.getNode(i).get_id();
            if(currentNodeId != childNodeId)
            {
                if(childNodeId.length==36)
                {
                    if(childNodeText.toLowerCase()  == newText.toLowerCase())
                    {
                        alert(__JSMessages["ThisNodeItemNameAlreadyExistsInThisLevel"]);
                        if(treeOperation!="New")
                        {
                            canProceed = false;
                            eventArgs.get_node().setProperty("Text", oldText);
                            eventArgs.set_cancel(true); 
                        }
                        else //Remove only in case of new node
                        {
                            canProceed =false;
                            newNode.remove();
                        }
                    }
                }
            }
        }
    }  
  
        if (canProceed)
        {  
           // alert("In rename");
            if(treeOperation!="New")
            {
                    // server side callback method
               isCallback = 'true';     
               if(trim(eventArgs.get_newText())!=trim(eventArgs.get_node().Text))
               {
               var newtext =trim(eventArgs.get_newText());
                if(newtext==null || newtext.length==0)
                {
                    newtext ='';
                }    
                    result = EditProductType(eventArgs.get_node().get_id(),newtext);
                    eventArgs.get_node().select();
                    eventArgs.get_node().set_value(result);    
               }
                else
                 result='True'; 
              isCallback = 'false';      
            }
            else
            {      
                isCallback = 'true';
                var node=eventArgs.get_node();
                
                var parNode= eventArgs.get_node().get_parentNode();   
                var newText = eventArgs.get_newText();
                var oldText =eventArgs.get_node().get_text();
                var parentId = parNode.get_id();
                
                    // server side callback method
                    
                var id = CreateProductTypeWithCopy(newText,parentId,copyParent);
                copyParent ='false';
                if(id !=null && id.toString().length==36 && id.substring(0,1)!="~")
                {          
                  trvProductTypes.beginUpdate();      
                  eventArgs.get_node().set_id(id); 
                  trvProductTypes.endUpdate(); 
                  result="True";
                }
                else
                {
                  result="True";
                  eventArgs.get_node().remove();
//                  loadGrid=false;
//                  parNode.select();
//                  parNode.expand();
//                  loadGrid=true;
                  if(id!=null && id.toString().length>0)
                  {
                    if (id.charAt(0)=='~')
                    {
                        alert(id.substring(1));
                    }
                    else
                    {
                        alert(id); //alert(id.substring(1));
                    }
                  }
                  else
                      alert(__JSMessages["AddingNewNodeFailedPleaseTryAgain"]);
                  //eventArgs.get_node().setProperty("Text", oldText);
                  
                }
                isCallback = 'false';
            }
            if(result=="True")
            {
                if(treeOperation=="New")
                {
                if(newNode.get_id()!=null && newNode.get_id().length==36)
                    newNode.select();
                }
          
            }
            else
            {
            if(result!=null && result.toString().length>0)
                alert(result.substring(1));
            else
                alert(__JSMessages["RenameFailedPleaseTryAgain"]);
                var editNode = eventArgs.get_node();
               eventArgs.set_cancel(true);
               loadGrid=false;
               editNode.select();
             //  editNode.edit();
                loadGrid=true;
            }
            
            isRenameInProgess = 'false';                
      }      
            
  }

//function expandPageTree(objTag)
//{
//    var imgId = "";
//    var textId = "";
//    var tagId = objTag.id;
//    //get the id of the expand/collapse image
//    imgId = "icon" + tagId;
//    //get the id of the expand/collapse text
//    textId = "text" + tagId;
//    switch (document.getElementById(textId).innerHTML)
//    {
//    

//        case "Expand Tree":
//            //expand the tree
//            TrvLibrary.expandAll();
//            //change the image to collapse
//            document.getElementById(imgId).src = "../../images/minus.gif";
//            //change the text to "Collapse Tree"
//            document.getElementById(textId).innerHTML = "Collapse Tree";
//            break;
//        case "Collapse Tree":
//            //Collapse the tree
//            TrvLibrary.collapseAll();
//            //change the image to expand
//            document.getElementById(imgId).src = "../../images/plus.gif";
//            //change the text to "Expand Tree"
//            document.getElementById(textId).innerHTML = "Expand Tree";
//            break;
//    }
//}
///** Methods to Expand/Collapse Tree **/
//function cmFileTree_onContextMenu(sender, eventArgs)
//{  
////alert("in tree menu");
////alert(eventArgs.get_node().Value);
////alert(eventArgs.get_node().Value);
////alert(eventArgs.get_node().get_id());
//    if(eventArgs.get_node().Value =="2" || eventArgs.get_node().Value =="4")
//    {
//        currentMenu=MnuTopContextMenu;
//    }
//    else if(eventArgs.get_node().Value =="5" && MnuUnassignedContextMenu.GetItems().length>0)
//    {
//        currentMenu =MnuUnassignedContextMenu;
//    }
//    else
//    {
//        currentMenu=MnuContextMenu;
//    }
//    if(eventArgs.get_node().Value==1 ||eventArgs.get_node().Value==2 ||eventArgs.get_node().Value==3 ||eventArgs.get_node().Value==4 || (eventArgs.get_node().Value =="5" && MnuUnassignedContextMenu.GetItems().length>0))
//    {
//     
//    
//        var evt = eventArgs.get_event();
//        evt.cancelBubble = true; 
//        evt.returnValue = false; 
//        var treeContextMenuX = evt.pageX ? evt.pageX : evt.clientX;
//        var treeContextMenuY = evt.pageY ? evt.pageY : evt.clientY;
//        treeNode = null;
//        treeNode = eventArgs.get_node();
//        parentNode = treeNode.get_parentNode(); 
//        currentMenu.set_contextData(treeNode);
//        currentMenu.showContextMenu(treeContextMenuX,treeContextMenuY,treeNode);
//      }
//// var item = eventArgs.get_node();
////       //   cmFileTree.select(item);
////          if(browser == "ie")
////            {
////                var evt = eventArgs.get_event();
////                 MnuContextMenu.showContextMenu(evt.clientX, evt.clientY);
////                 MnuContextMenu.set_contextData(item);
////            }
////            else {  MnuContextMenu.showContextMenu(eventArgs.get_event()); }
//    
//}
function trim(str) 
{
    if(!str || typeof str != 'string')
        return null;     
    return str.replace(/^[\s]+/,'').replace(/[\s]+$/,'').replace(/[\s]{2,}/,' '); 
}
