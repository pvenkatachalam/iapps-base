﻿iAPPS_Tooltip = {
    Initialize: function (attributeForTooltip, attributeForTooltipText, tooltipPosition) {
        var tooltipTopHtml = '<div id="iappsTooltip">';
        var tooltipBottomHtml = '</div>';

        //Select all anchor tag with rel set to tooltip
        $('*[' + attributeForTooltip + '=tooltip]').mouseover(function (e) {

            //Grab the title attribute's value and assign it to a variable
            var tip = $(this).attr(attributeForTooltipText);

            //Remove the title attribute's to avoid the native tooltip from the browser
            $(this).attr(attributeForTooltipText, '');

            //Append the tooltip template and its value
            $(this).append(tooltipTopHtml + tip + tooltipBottomHtml);

            //position the tooltip
            iAPPS_Tooltip.Position($(this), tooltipPosition);

            //Show the tooltip with faceIn effect
            $('#iappsTooltip').fadeIn('500');
            $('#iappsTooltip').fadeTo('10', 1.0);

        }).mouseout(function () {

            //Put back the title attribute's value
            $(this).attr(attributeForTooltipText, $('#iappsTooltip').html());

            //Remove the appended tooltip template
            $(this).children('div#iappsTooltip').remove();

        });
    },

    Position: function (element, tooltipPosition) {
        //tooltip object
        var objTooltip = $('#iappsTooltip');
        //width of tooltip
        var tooltipWidth = $(objTooltip).outerWidth();
        //height of tooltip
        var tooltipHeight = $(objTooltip).outerHeight();
        var elementPosition = $(element).offset();
        //width of the element for which tooltip needs to be displayed
        var elementWidth = $(element).outerWidth();
        //height of the element for which tooltip needs to be displayed
        var elementHeight = $(element).outerHeight();
        var screenWidth = screen.width;
        var tooltipLeft = (elementPosition.left + (elementWidth / 2)) - ((tooltipWidth / 2) + 10);
        var tooltipTop = (elementPosition.top + (elementHeight / 2)) - (tooltipHeight / 2);
        switch (tooltipPosition.toLowerCase()) {
            case 'top':
                $(objTooltip).css('left', tooltipLeft);
                $(objTooltip).css('top', elementPosition.top - tooltipHeight);
                break;
            case 'right':
                //Set the X and Y axis of the tooltip
                $(objTooltip).css('left', elementPosition.left + elementWidth + 10);
                $(objTooltip).css('top', tooltipTop);
                break;
            case 'bottom':
                //Set the X and Y axis of the tooltip
                $(objTooltip).css('left', tooltipLeft);
                $(objTooltip).css('top', elementPosition.top + tooltipHeight);
                break;
            case 'left':
                //Set the X and Y axis of the tooltip
                $(objTooltip).css('left', elementPosition.left - tooltipWidth - 10);
                $(objTooltip).css('top', tooltipTop);
                break;
        }
    },

    InitializeGridTooltip: function (gridCssClass, tip) {
        $('.' + gridCssClass).mouseover(function (e) {
            //Append the tooltip template and its value
            $('body').append('<div id="iappsTooltip">' + tip + '</div>');

            //Set the X and Y axis of the tooltip
            $('#tooltip').css('top', e.pageY - 30);
            $('#tooltip').css('left', e.pageX);

            //Show the tooltip with faceIn effect
            $('#tooltip').fadeIn('500');
            $('#tooltip').fadeTo('10', 0.8);
        }).mousemove(function (e) {
            //Keep changing the X and Y axis for the tooltip, thus, the tooltip move along with the mouse
            $('#tooltip').css('top', e.pageY - 30);
            $('#tooltip').css('left', e.pageX);
        }).mouseout(function () {
            //Remove the appended tooltip template
            $('body').children('div#tooltip').remove();
        });
    },

    InitializeGridTooltipAtPosition: function (gridCssClass, tip, tooltipPosition) {
        var tooltipTopHtml = '<div id="iappsTooltip">';
        var tooltipBottomHtml = '</div>';

        $('.' + gridCssClass).mouseover(function (e) {
            //Append the tooltip template and its value
            $('body').append(tooltipTopHtml + tip + tooltipBottomHtml);

            //position the tooltip
            iAPPS_Tooltip.Position($(this), tooltipPosition);

            //Show the tooltip with faceIn effect
            $('#iappsTooltip').fadeIn('500');
            $('#iappsTooltip').fadeTo('10', 1.0);

        }).mouseout(function () {

            //Remove the appended tooltip template
            $('body').children('div#iappsTooltip').remove();

        });
    }
};