﻿// Whatever method starting with PageLevel has to implement the page

var selectedTreeNodeId;
var isGridCallback = false;
var selectedMenu;       // selected context menu of Grid
var gridItem; // selected/current item in the grid
var gridOperation;              // current operation is going on in grid
var emptyGuid = '00000000-0000-0000-0000-000000000000'; // empty guid
var draggingEnabled = 0;         //settings for dragging enable and disable
var uniqueIdColumn;            //This is the unique id to control the menu properties and grid editing
var operationColumn;           //Keeps operation value 
var gridObject;                //this is grid object using in your page.. set value to this at the end of the page
var menuObject;                 //This is the menu using in your page. set value to this at the end of the page
var gridObjectName;             //This is the name of the grid;
var menuItems;                 //This is the menu items of the context menu
var isDisplayContextMenu = true;
var isLoadPageAfterEachDelete = true;   //if it is true, it will load the page after each delete, otherwise it simply remove that particular record thats all.

// execute after the call back of the grid
function grid_onCallbackComplete(sender, eventArgs) {
    isGridCallback = false;
    isDisplayContextMenu = true;
    if (draggingEnabled == 1) {
        gridObject.ItemDraggingEnabled = 1;
        draggingEnabled = 0;
    }

    if (gridOperation == "Delete") {
        gridOperation = '';
        if (gridObject.get_recordCount() != 0) {
            if (gridObject.get_recordCount() % gridObject.get_pageSize() == 0) {
                gridObject.previousPage();
            }
        }
    }

    PageLevelCallbackComplete();
}
// calling while click on the context menu of the grid
function grid_onContextMenu(sender, eventArgs) {

    if (isDisplayContextMenu) {
        gridObject = sender;
        gridItem = eventArgs.get_item();
        gridObject.select(gridItem);
        var e = eventArgs.get_event();

        menuItems = menuObject.get_items();
        if (ContextMenuGeneralChecking(menuItems) && PageLevelOnContextMenu(menuItems)) {
            RemoveSetAsDefaultMenu(menuItems);
            menuObject.showContextMenuAtEvent(e);
        }
    }
}

function RemoveSetAsDefaultMenu(menuItems) {
    if (typeof IsPersonalized != 'undefined' && IsPersonalized != undefined && IsPersonalized == 'True') {
        for (i = 0; i < menuItems.get_length(); i++) {

            if (menuItems.getItem(i).get_id() == 'cmDefault')
                menuItems.getItem(i).set_visible(false);
        }
    }
}

// if it is normal Add, Delete, Raname menus will handle here.. other than this, 
// if you want to overcome something in pagelevel, you can write code in PageLevelOnContextMenu() method. otherwise just return true.
function ContextMenuGeneralChecking(menuItems) {
    var isAnyMenuVisible = false;
    var uniqueId = gridItem.getMember(uniqueIdColumn).get_text();
    if (uniqueId != '' && uniqueId != emptyGuid) //only add
    {
        for (i = 0; i < menuItems.get_length(); i++) {
            menuItems.getItem(i).set_visible(true);
            isAnyMenuVisible = true;
        }
    }
    else {
        for (i = 0; i < menuItems.get_length(); i++) {
            if (menuItems.getItem(i).get_id() == 'cmAdd') {
                isAnyMenuVisible = true;
            }
            else {
                menuItems.getItem(i).set_visible(false);
            }
        }
    }
    return true;
}

// Grid Context menu selection
function menu_onItemSelect(sender, eventArgs) {
    menuObject = sender;
    var menuItem = eventArgs.get_item();
    //var contextDataNode = menuItem.get_parentMenu().get_contextData();
    var selectedMenuId = menuItem.get_id();

    if (selectedMenuId != null) {
        switch (selectedMenuId) {
            case 'cmAdd':
                AddGridItem();
                break;
            case 'cmEdit':
                EditGridItem();
                break;
            case 'cmDelete':
                DeleteSelectedGridItem();
                break;
            default:
                PageLevelOnItemSelect(selectedMenuId, gridItem, sender, eventArgs); //implement this method to handle extra items
                break;
        }
    }
}

//Grid Level to add a blank row for adding
function AddGridItem() {
    if (gridObject.ItemDraggingEnabled == 1) {
        draggingEnabled = 1;
        gridObject.ItemDraggingEnabled = 0;
    }
    if (gridObject.Data.length == 1 && (gridItem.getMember(uniqueIdColumn).get_text() == null || gridItem.getMember(uniqueIdColumn).get_text() == '')) {
        gridObject.edit(gridItem);
    }
    else {
        gridObject.beginUpdate();
        gridObject.get_table().addEmptyRow(0);
        gridObject.editComplete();

        gridItem = null;
        gridItem = gridObject.get_table().getRow(0);
        gridObject.edit(gridItem);
        while (gridObject.CurrentPageIndex > 0) {
            gridObject.previousPage();
        }

    }
    gridOperation = "Insert";
    isDisplayContextMenu = false;
    PageLevelAddItem(gridItem); //implement this method in page to add extra functionalities
}

//Grid Level method to provide editing functionalities
function EditGridItem() {
    if (gridObject.ItemDraggingEnabled == 1) {
        draggingEnabled = 1;
        gridObject.ItemDraggingEnabled = 0;
    }
    gridOperation = "Edit";
    gridObject.edit(gridItem);
    isDisplayContextMenu = false;
    PageLevelEditItem(gridItem); //implement this method in page to add extra functionalities
}

//Grid Level method to provide delete functionality
function DeleteSelectedGridItem() {
    if (confirm(__JSMessages["AreYouSureYouWantToDeleteTheSelectedItem"])) {

        var isDelete = PageLevelDeleteItem(gridItem);

        if (isDelete.toLowerCase() == "true") {
            alert(__JSMessages["RecordDeletedSuccessfully"]);
            if (isLoadPageAfterEachDelete) {
                gridOperation = "Delete";
                gridObject.edit(gridItem);
                if (selectedTreeNodeId != undefined && selectedTreeNodeId != 'undefined')
                    gridItem.SetValue(operationColumn, gridOperation + ';' + selectedTreeNodeId);
                else
                    gridItem.SetValue(operationColumn, gridOperation);
                isGridCallback = true;
                gridObject.editComplete();
                gridObject.callback();
            }
            else        //simply remove the selected record from client side. because it may be delete in callback method.
            {
                gridObject.deleteItem(gridItem);
                isLoadPageAfterEachDelete = true;
            }
        }
        else if (isDelete.toLowerCase() == "false") {
            alert(__JSMessages["ThisItemCannotBeDeleted"]);
        }
        else if (isDelete.toLowerCase() == "sucess") {
            alert(__JSMessages["ThisItemDeletedSuccessfully"]);
        }
        else {
            alert(__JSMessages["DeleteActionFailed"] + isDelete);
        }
    }
}
// Validations before saving
function gridObject_BeforeUpdate() {
    if (PageLevelGridValidation(gridItem)) {
        SaveRecord();
    }
}
// Save the modified record
function SaveRecord() {
    gridObject.beginUpdate();
    isGridCallback = true;
    gridObject.editComplete();
    if (selectedTreeNodeId != undefined && selectedTreeNodeId != 'undefined')
        gridItem.SetValue(operationColumn, gridOperation + ';' + selectedTreeNodeId);
    else
        gridItem.SetValue(operationColumn, gridOperation);
    gridObject.callback();
    isDisplayContextMenu = true;
    PageLevelSaveRecord();
}
// While click on cancel button
function CancelClicked() {
    if (gridOperation == 'Insert') {
        if (gridObject.Data.length == 1 && (gridItem.getMember(uniqueIdColumn).get_text() == null || gridItem.getMember(uniqueIdColumn).get_text() == '')) {
            gridObject.get_table().ClearData();
            gridObject.editCancel();
            gridItem = null;
            if (gridObject.Data.length == 0) {
                gridObject.get_table().addEmptyRow(0);
            }
        }
        else {
            gridObject.deleteItem(gridItem);
            gridObject.editCancel();
            gridItem = null;
        }
    }
    else {
        gridObject.editCancel();
        gridItem = null;
    }
    isDisplayContextMenu = true;
    PageLevelCancelEdit();
}

function grid_onLoad(sender, eventArgs) {
    SetGridLevelGeneralVariablesinPage();
}

// This function calls for load operation
function LoadGrid(actionColumnValue) {
    var gridObjectForLoad = window[gridObjectName];
    if (gridObjectForLoad == null && gridObject == null)
        return;
    if (gridObjectForLoad == null && gridObject != null) {
        gridObjectForLoad = gridObject;
    }
    if (actionColumnValue == null || actionColumnValue == '') {
        gridObjectForLoad.ItemDraggingEnabled = 0;
        gridObjectForLoad.AllowPaging = true;
        var chkObj = window["EnableDisplayOrderSort"];
        if (chkObj != null && chkObj.checked) {
            actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ";EnableDisplayOrderSort";
            gridObjectForLoad.ItemDraggingEnabled = 1;
            gridObjectForLoad.AllowPaging = false;
            SetSortingValue(false);
        }
        else {
            SetSortingValue(true);
            if (chkObj == null)   // object is not there
            {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ";DisableDisplayOrderSort";
            }
            else // object is there but not checked
            {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ";DisableDisplayOrderSort";
            }
        }
    }
    setTimeout(function () {
        if (gridObjectForLoad != null) {
            var rowitem = gridObjectForLoad.get_table().addEmptyRow(0);
            gridObjectForLoad.edit(rowitem);
            rowitem.SetValue(operationColumn, actionColumnValue); // action column
            isGridCallback = true;
            gridObjectForLoad.editComplete();
            gridObjectForLoad.callback();
        }
    }, 500);
    gridOperation = 'Load';
}

//TextBox
// set value method for setExpression
function setValue(control, DataField) {
    var value = gridItem.GetMember(DataField).Value;
    value = value + '';
    var txtControl = document.getElementById(control);
    if (value != '' && txtControl != null && value != null) {
        txtControl.value = value.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
        if (txtControl.value == null || txtControl.value == 'null')
            txtControl.value = '';
    }
}
// get value method for setExpression
function getValue(control, DataField) {
    var PageName;
    var txtControl = document.getElementById(control);
    if (txtControl != null) {
        PageName = Trim(txtControl.value.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
    }
    return [PageName, PageName];
}
//Check Box
// set value method for setExpression
function setCheckBoxValue(control, DataField) {
    var value = gridItem.GetMember(DataField).Value;
    var txtControl = document.getElementById(control);
    if (value != '' && txtControl != null && value != null) {
        txtControl.checked = value;
    }
}
// get value method for setExpression
function getCheckBoxValue(control, DataField) {
    var PageName;
    var txtControl = document.getElementById(control);
    if (txtControl != null) {
        PageName = txtControl.checked;
    }
    return [PageName, PageName];
}

//dropdown control
function setDropDownValueGeneral(control, DataField) {
    var dropDownListControl = document.getElementById(control);
    var selectedItemId = gridItem.GetMember(DataField).Value;
    if (selectedItemId != null) {
        for (i = 0; i < dropDownListControl.length; i++) {
            if (dropDownListControl[i].value == selectedItemId) {
                dropDownListControl[i].selected = true;
            }
        }
    }
}

function getDropDownValueGeneral(control, DataField) {
    var dropDownListControl = document.getElementById(control);
    var selectedItemIds = "";
    for (i = 0; i < dropDownListControl.length; i++) {
        if (dropDownListControl[i].selected) {
            selectedItemIds = selectedItemIds + dropDownListControl[i].value;
        }
    }
    return [selectedItemIds, selectedItemIds];
}

//List Box
function setListBoxValue(control, DataField) {
    var itemListControl = document.getElementById(control);
    var selectedItemIds;
    var allItemIds = gridItem.GetMember(DataField).Value;
    if (allItemIds != null) {
        //allItemIds = allItemIds.substring(0, allItemIds.length - 1);
        selectedItemIds = allItemIds.split(",");
        for (i = 0; i < itemListControl.length; i++) {
            for (j = 0; j < selectedItemIds.length; j++) {
                if (Trim(itemListControl[i].value.toLowerCase()) == Trim(selectedItemIds[j].toLowerCase())) {
                    itemListControl[i].selected = true;
                }
            }
        }
    }

}
function getListBoxValue(control, DataField) {
    var itemListControl = document.getElementById(control);
    var selectedItemGuids = "";
    for (i = 0; i < itemListControl.length; i++) {
        if (itemListControl[i].selected) {
            selectedItemGuids = selectedItemGuids + itemListControl[i].value + ",";
        }
    }
    return [selectedItemGuids, selectedItemGuids];
}


//Sort order functionality
function grid_OnSortChange(sender, eventArgs) {
    if (!isGridCallback) {
        var gitem = gridObject.get_table().getRow(0);

        gridOperation = "";
        var chkObj = window["EnableDisplayOrderSort"];
        gridObject.ItemDraggingEnabled = 0;
        gridObject.AllowPaging = true;
        if (chkObj != null && chkObj.checked) {
            gridOperation = 'Sort' + ';' + selectedTreeNodeId + ";EnableDisplayOrderSort";
            gridObject.ItemDraggingEnabled = 1;
            gridObject.AllowPaging = false;
        }
        else {
            if (chkObj == null)   // object is not there
            {
                gridOperation = 'Sort' + ';' + selectedTreeNodeId + ";DisableDisplayOrderSort";
            }
            else // object is there but not checked
            {
                gridOperation = 'Sort' + ';' + selectedTreeNodeId + ";DisableDisplayOrderSort";
            }
        }

        gridOperation = gridOperation + ';' + eventArgs.get_column().DataField + ";" + eventArgs.get_descending();
        gridObject.edit(gitem);
        gitem.SetValue(operationColumn, gridOperation);
        gridObject.page(0);
        gridObject.editComplete();
        gridObject.callback();
        isGridCallback = true;
    }
    else {
        eventArgs.set_cancel(true);
    }
}

// This is for Enable Sort Order functionality. it is expecting the checkbox as parameter
function RefreshGridForDisplayOrder(chkDisplayOrder, scrollingGrid) {
    if (typeof scrollingGrid == "undefined")
        scrollingGrid = false;

    var rowitem = gridObject.get_table().addEmptyRow(0);
    var gridc = gridObject.get_table().get_columns();
    gridObject.AllowPaging = true;
    var actionColumnValue = "";
    window["EnableDisplayOrderSort"] = chkDisplayOrder;
    //Sorting Disable
    if (chkDisplayOrder.checked) {
        gridObject.get_levels()[0].set_hoverRowCssClass("selected-row");
        actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';EnableDisplayOrderSort';
        gridObject.set_externalDropTargets(gridObjectName);
        if (!scrollingGrid)
            gridObject.set_pageSize(5000);
        gridObject.ItemDraggingEnabled = 1;
        if (!scrollingGrid)
            gridObject.AllowPaging = false;
        SetSortingValue(false);
    }
    else {
        gridObject.get_levels()[0].set_hoverRowCssClass('');
        actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';DisableDisplayOrderSort';
        gridObject.set_externalDropTargets(gridObjectName);
        if (!scrollingGrid)
            gridObject.set_pageSize(10);
        gridObject.ItemDraggingEnabled = 0;
        SetSortingValue(true);
    }
    LoadGrid(actionColumnValue);
}

function SetSortingValue(sortValue) {
    var gridc = gridObject.get_table().get_columns();
    for (var i = 0; i < gridc.length; i++) {
        gridc[i].AllowSorting = sortValue;
    }
}

var isItemMoved;
function grid_OnExternalDrop(sender, eventArgs) {
    var targetObjectName = eventArgs.get_targetControl().getProperty('GlobalAlias');
    var targetObjectId = eventArgs.get_targetControl().Id;
    if (targetObjectName == gridObjectName || targetObjectId == gridObjectName) {
        var chkObj = window["EnableDisplayOrderSort"];
        if (chkObj != null && chkObj.checked) {
            var draggedItem = eventArgs.get_item();
            var fromItemId = null;
            var toItemId = null;
            var toNewPosition;
            if (draggedItem != null) {
                fromItemId = draggedItem.getMember(uniqueIdColumn).get_text();
            }
            var targetItem = eventArgs.get_target();
            if (targetItem != null) {
                toItemId = targetItem.getMember(uniqueIdColumn).get_text();
                toNewPosition = targetItem.getMember('Sequence').get_value();
            }
            if (toItemId != null && fromItemId != null) {
                isItemMoved = PageLevelDragAndDrop(draggedItem, targetItem, fromItemId, toNewPosition);
                if (isItemMoved == 'true') {
                    LoadGrid('Load' + ';' + selectedTreeNodeId + ';EnableDisplayOrderSort');
                }
                else if (isItemMoved != 'false' && isItemMoved != 'False') {
                    alert(isItemMoved);
                }
            }
        }
    }
}

function grid_onRenderComplete(sender, eventArgs) {
    document.getElementById(sender.get_id() + "_dom").style.height = "auto";
}

function gridCurrentPageIndex(objGrid) {
   return currentPageIndex(objGrid);
}

function gridPageCount(objGrid) {
    return pageCount(objGrid);
}

function currentPageIndex(objGrid) {
    return objGrid.RecordCount == 0 ? 1 : objGrid.CurrentPageIndex + 1;
}

function pageCount(objGrid) {
    var pCount = objGrid.RecordCount == 0 ? 1 : objGrid.PageCount;
    var fnToCall = function () { HideGridFooter(objGrid, pCount); };
    setTimeout(fnToCall, 1);

    return pCount;
}

function HideGridFooter(objGrid, pCount) {
    var gridFooter = document.getElementById(objGrid.get_id() + "_footer");
    if (gridFooter) {
        if (objGrid.RecordCount == 0) {
            gridFooter.style.display = "none";
            $("#" + objGrid.get_id()).addClass("empty-grid");
        }
        else {
            gridFooter.style.display = "";
            $("#" + objGrid.get_id()).removeClass("empty-grid");

            if (pCount <= 1)
                $("#" + objGrid.get_id() + "_footer table").find("td").first().hide();
        }
    }
}

function SetGridStyles(sender, eventArgs) {
    FormatDataGrid(sender, true) 
}

function FormatDataGrid(sender, formatLastColumn) {
    var pCount = sender.RecordCount == 0 ? 1 : sender.PageCount;
    HideGridFooter(sender, pCount);
}

function GetGridLoadingPanelContent(objGrid) {
    var height = 50;
    var width = 100;
    var gridDom = document.getElementById(objGrid.get_id() + "_dom");
    if (gridDom) {
        height = gridDom.offsetHeight;
        width = gridDom.offsetWidth;
    }
    var html = "<table><tr><td class='LoadingPopup' style='height:{0}px;width:{1}px;'>Loading...<br /><img src='/iapps_images/spinner.gif' border='0' alt='' /></td></tr></table>";

    return stringformat(html, height, width);
}

function SetVerticalScroll(sender, eventArgs) {
    $("#" + sender.get_id() + "_VerticalScrollDiv").addClass("vertical-scroll");
}

function GetPageLoadingPanelContent() {
    var html = "<div class='LoadingPopup'><div class='iAPPSPopupOverlay'></div><div class='iapps-modal loadingSpinner'><img src='/iapps_images/spinner.gif' border='0' alt='Please wait...' /></div></div>";
    return stringformat(html, $(document).width() + 100, $(document).height() + 100);
}

function onKeyPressSearchonGrid(evt, objText, objGrid) {
    if (validKey(evt)) {
        var str = objText.value;
        objGrid.Search(str, true);
    }
}

function FormatStatusColumn(txtStatus) {
    if (txtStatus.toLowerCase() == "true")
        return __JSMessages["InActiveStatus"];
    else
        return __JSMessages["ActiveStatus"];
}

function onClickSearchonGrid(txtBoxId, objGrid) {
    var txtValue = $("#" + txtBoxId).val();
    if (txtValue == $("#" + txtBoxId).attr("title"))
        txtValue = "";
    /*if (txtValue == "" || txtValue == $("#" + txtBoxId).attr("title")) {
    alert(EnterValue2Search);
    $("#" + txtBoxId).focus();
    return false;
    }
    else if (!txtValue.match(/^[a-zA-Z0-9. _']+$/)) {
    alert(InValidSearchTerm);
    $("#" + txtBoxId).focus();
    return false;
    }
    else*/
    {
        objGrid.Search(txtValue, false);
        document.getElementById(objGrid.get_id() + "_dom").style.height = "auto";
        return true;
    }
}

function GridLevelSpecialCharacterChecking(thisObj, thisObjName) {
    var msg = "";
    if (!thisObj.match(/^[a-zA-Z0-9. _'\/\-<>.&$!|#:,]+$/)) {
        if (IsNameAscii(thisObj) != 'false') {
            msg += stringformat(__JSMessages["CanHaveOnlyAlphanumericCharacters"], thisObjName);
        }
    }
    if (Trim(thisObj) == '&#0;' || thisObj.indexOf('&#0;') > 0) {
        msg += stringformat(__JSMessages["IsUnicodeCombinationOfNewLineCharacter"], thisObjName);
    }
    return msg;
}

