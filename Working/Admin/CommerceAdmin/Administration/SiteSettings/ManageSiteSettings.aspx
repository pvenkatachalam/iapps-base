﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceDeveloperConfigurations %>"
    Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="ManageSiteSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageSiteSettings" StylesheetTheme="General" %>

<%@ Register assembly="ComponentArt.Web.UI" namespace="ComponentArt.Web.UI" tagprefix="ComponentArt" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/Administration.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        var gridItem;
        function mnuCustomSettings_onItemSelect(sender, eventArgs) {
            var id = eventArgs.get_item().get_id();

            if (id == "cmAdd") {
                grdCustomSettings.get_table().addRow();
            }
            else if (id == "cmDelete") {
                grdCustomSettings.deleteItem(gridItem);
                if (grdCustomSettings.get_table().getRowCount() == 0)
                    grdCustomSettings.get_table().addRow();
            }
        }

        function grdCustomSettings_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            gridItem = eventArgs.get_item();
            mnuCustomSettings.showContextMenuAtEvent(evt);
        }

        function saveCell(itemId, columnField, newValue) {
            var row = grdCustomSettings.GetRowFromClientId(itemId);

            if (row != null) {
                // Check if value was changed
                var oldValue = row.GetMember(columnField).Value;
                if (oldValue != newValue) {
                    // Get column index for SetValue
                    var col = 0;
                    for (var i = 0; i < grdCustomSettings.Table.Columns.length; i++) {
                        if (grdCustomSettings.Table.Columns[i].DataField == columnField) {
                            col = i;
                            break;
                        }
                    }
                    row.SetValue(col, newValue, true);
                }
            }
            return true;
        }


        function CheckBoxListSelect(cbControl) {
            var checkAll = document.getElementById("cbCheckAll");

            var chkBoxList = document.getElementById(cbControl);
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            for (var i = 0; i < chkBoxCount.length; i++) {
                chkBoxCount[i].checked = checkAll.checked;
            }

            return false;
        }


        var editAddressPopup;
        function OpenEditAddressPopup(controlId) {
            var popupPath = stringformat("../../Popups/EditAddress.aspx?AddressId={0}&ControlId={1}", document.getElementById(controlId).value, controlId);
            editAddressPopup = dhtmlmodal.open('EditAddressPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
            editAddressPopup.onclose = function () {
                var a = document.getElementById('EditAddressPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function CloseEditAddressPopup(addressId, controlId) {
            
            if (addressId != "" && controlId != '') {
                $('#' + controlId).val(addressId);
            }

            editAddressPopup.hide();
        }

        function fn_ToggleSettings(objClick, sectionId) {
            switch (sectionId) {
                case 'global':
                    $('#divGlobal').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
                case 'general':
                    $('#divGeneral').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
                case 'shipper':
                    $('#divShipper').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
                case 'tax':
                    $('#divTax').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
                case 'payment':
                    $('#divPayment').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
                case 'b2b':
                    $('#divB2B').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
                case 'custom':
                    $('#divCustom').show().siblings().hide();
                    $(objClick).addClass('active').siblings().removeClass('active');
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <div class="site-settings">
        <div class="page-header clear-fix">
            <h1><%= SiteSettings.DeveloperConfigurations %></h1>
            <asp:Button runat="server" ID="btnSave" OnClick="btnSave_Onclick" Text="<%$ Resources:SiteSettings, Save %>"
                ToolTip="<%$ Resources:SiteSettings, Save %>" CssClass="primarybutton" ValidationGroup="callback" />
        </div>
        <div class="nav-links">
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'global')" class="active"><%= SiteSettings.GlobalSettings %></a>
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'general')"><%= SiteSettings.GeneralSiteSettings %></a>
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'shipper')"><%= SiteSettings.ShipperSettings %></a>
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'tax')"><%= SiteSettings.TaxSettings %></a>
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'payment')"><%= SiteSettings.PaymentSettings %></a>
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'b2b')"><%= SiteSettings.B2BSettings %></a>
            <a href="#" onclick="javascript: fn_ToggleSettings(this, 'custom')"><%= SiteSettings.CustomSettings %></a>
        </div>
        <div>
            <div id="divGlobal">
                <span class="formRow">
                    <label class="formLabel">Admin Email:</label><asp:TextBox ID="txtAdminEmail" runat="server" CssClass="textBoxes  textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Analytics Product Id:</label><asp:TextBox ID="txtAnalyticsProductId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">AnalyticsAdminVDName:</label><asp:TextBox ID="txtAnalyticsAdminVDName" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">AnalyticsProduct.Key:</label><asp:TextBox ID="txtAnalyticsProductKey" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>

                <span class="formRow">
                    <label class="formLabel">Cache.Email:</label><asp:TextBox ID="txtcacheemail" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">CMS Product Id:</label><asp:TextBox ID="txtCMSProductId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">CMS Product.Key:</label><asp:TextBox ID="txtCMSProductKey" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">CMSAdminVDName:</label><asp:TextBox ID="txtCMSAdminVDName" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">CommonLoginVD:</label><asp:TextBox ID="txtCommonLoginVD" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">CommerceAdminVDName:</label><asp:TextBox ID="txteCommerceAdminVDName" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">CommerceProduct.Key:</label><asp:TextBox ID="txteCommerceProductKey" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Commerce Product Id:</label><asp:TextBox ID="txtCommerceProductId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">ComponentArtScriptControls:</label><asp:TextBox ID="txtComponentArtScriptControls" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Context MenuXml:</label><asp:TextBox ID="txtContextMenuXml" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">EventLog Application Key:</label><asp:TextBox ID="txtFrontEndEventLogApplicationKey" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">FilesToOpenInBrowser:</label><asp:TextBox ID="txtFilesToOpenInBrowser" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">iAPPS_Version:</label><asp:TextBox ID="txtiAPPS_Version" runat="server" CssClass="textBoxes  textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">iApps.DateFormat:</label><asp:TextBox ID="txtiAppsDateFormat" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">IAppsSystemUser Id:</label><asp:TextBox ID="txtiAPPSUser" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="retxtiAPPSUser" runat="server" ControlToValidate="txtiAPPSUser" ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                        ValidationGroup="callback" ErrorMessage="Please enter a valid Guid in 'iAPPS System User' field" Text="*" />
                </span>
                <span class="formRow">
                    <label class="formLabel">iAppsTimeoutInMinutes:</label><asp:TextBox ID="txtiAppsTimeoutInMinutes" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>               
                <span class="formRow">
                    <label class="formLabel">Invisible TreeViewNode CssClass:</label><asp:TextBox ID="txtInvisibleTreeViewNodeCssClass" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Invisible Selected NodeCssClass:</label><asp:TextBox ID="txtInvisibleSelectedNodeCssClass" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">ISYSBinPath:</label><asp:TextBox ID="txtISYSBinPath" runat="server" CssClass="textBoxes  textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">ISYSLicenseKey:</label><asp:TextBox ID="txtISYSLicenseKey" runat="server" CssClass="textBoxes  textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">ISYSProductSearchIndex:</label><asp:TextBox ID="txtISYSProductSearchIndex" runat="server" CssClass="textBoxes" Width="637" ReadOnly="false"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">JQueryGoogleCDN for Admin:</label><asp:TextBox ID="txtjQueryGoogleCDN" runat="server" CssClass="textBoxes  textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">MarketierAdminVD:</label><asp:TextBox ID="txtMarketierAdminVD" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Marketier Product Id:</label><asp:TextBox ID="txtMarketierProductId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">MarketierProduct.Key:</label><asp:TextBox ID="txtMarketierProductKey" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">PageAccessXml:</label><asp:TextBox ID="txtPageAccessXml" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">PublicSite.URL:</label><asp:TextBox ID="txtPublicSiteURL" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Render Controls Attributes In Live:</label>
                    <asp:DropDownList ID="ddlRenderControlsAttributesInLive" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">Render iAPPS SEO Description:</label>
                    <asp:DropDownList ID="ddlRenderiAPPSSEODescription" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">Site Id:</label><asp:TextBox ID="txtSiteId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">Use Cache:</label>
                    <asp:DropDownList ID="ddlUseCache" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </span>
            </div>
            <div id="divGeneral" style="display: none;">
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AbandonedCartTime %>" /></label>
                    <asp:TextBox runat="server" ID="txtAbandonedCartTime" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtAbandonedCartTime" runat="server"
                        ControlToValidate="txtAbandonedCartTime" Type="Integer" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberInAbandonedCartTimeField %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AddNewWishListPageURL %>" /></label><asp:TextBox runat="server" ID="txtAddNewWishListPageURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AddToCartLoginOnly %>" /></label><asp:CheckBox runat="server" ID="cbAddToCartLoginOnly" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AdminSiteID %>" /></label><asp:TextBox ID="txtAdminSiteID" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AdvancedDetailsAttributeGroupId %>" /></label><asp:TextBox runat="server" ID="txtAdvancedDetailsAttributeGroupId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AllowCustomerCompanyName %>" /></label><asp:CheckBox runat="server" ID="cbAllowCustomerCompanyName" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AllowCustomerIsApproved %>" /></label><asp:CheckBox runat="server" ID="cbAllowCustomerIsApproved" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AllowPreApproval %>" /></label><asp:CheckBox runat="server" ID="cbAllowPreapproval" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AutoNewProductsDays %>" /></label>
                    <asp:TextBox ID="txtAutoNewProductsDays" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtAutoNewProductsDays"
                        runat="server" ControlToValidate="txtAutoNewProductsDays" Type="Integer" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberInAutoNewProductsDaysField %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:SiteSettings, BackOrderSupport %>" /></label>
                    <asp:DropDownList ID="ddlBackOrderSupport" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:SiteSettings, BackOrderShippingSplit %>" /></label>
                    <asp:DropDownList ID="ddlBackOrderShippingSplit" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CustomerOnHoldOrderActions %>" /></label>
                    <asp:DropDownList runat="server" ID="ddlBadCustomerOrderAction">
                        <asp:ListItem Text="<%$ Resources:SiteSettings, AllowOrder %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PutOrderOnHold %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PreventOrderFromBeingPlaced %>" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CanadaId %>" /></label><asp:TextBox runat="server" ID="txtCanadaId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CartCookieName %>" /></label><asp:TextBox runat="server" ID="txtCartCookieName" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CartCookieTimeOutInHours %>" /></label>
                    <asp:TextBox runat="server" ID="txtCartCookieTimeOutInHours" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtCartCookieTimeOutInHours"
                        runat="server" ControlToValidate="txtCartCookieTimeOutInHours" Type="Integer"
                        Operator="DataTypeCheck" ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberInCartCookieTimeOutInHoursField %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CartExpirationDurationInDays %>" /></label>
                    <asp:TextBox runat="server" ID="txtCartExpirationDurationInDays" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtCartExpirationDurationInDays"
                        runat="server" ControlToValidate="txtCartExpirationDurationInDays" Type="Integer"
                        Operator="DataTypeCheck" ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberInCartExpirationDurationInDaysField %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CartURL %>" /></label><asp:TextBox runat="server" ID="txtCartURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CheckoutURL %>" /></label><asp:TextBox runat="server" ID="txtCheckoutURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CODProcessingFee %>" /></label><asp:TextBox ID="txtCODProcessingFee" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CODShippingMethods %>" /></label><asp:ListBox runat="server" ID="lbx" SelectionMode="Multiple"></asp:ListBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CommerceEveroneGroupID %>" /></label><asp:TextBox ID="txtCommerceEveryOneGroupId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CommerceNavRootNodeID %>" /></label><asp:TextBox ID="txtCommerceNavRootNodeId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CustomersLoginPageURL %>" /></label><asp:TextBox runat="server" ID="txtCustomersLoginPageURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CustomerSecurityLevelID %>" /></label><asp:TextBox ID="txtCustomerSecurityLevelId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CustomerAttributeGroupID %>" /></label><asp:TextBox ID="txtCustomerAttributeGroupId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DefaultPagesXml %>" /></label><asp:TextBox runat="server" ID="txtDefaultPagesXml" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:SiteSettings, DefaultProductLength %>" /></label>
                    <asp:TextBox runat="server" ID="txtDefaultProductLength" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator3" runat="server"
                        ControlToValidate="txtDefaultProductLength" Type="Double" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberDefaultProductLength %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:SiteSettings, DefaultProductWidth %>" /></label>
                    <asp:TextBox runat="server" ID="txtDefaultProductWidth" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator4" runat="server"
                        ControlToValidate="txtDefaultProductWidth" Type="Double" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberDefaultProductWidth %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:SiteSettings, DefaultProductHeight %>" /></label>
                    <asp:TextBox runat="server" ID="txtDefaultProductHeight" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator5" runat="server"
                        ControlToValidate="txtDefaultProductHeight" Type="Double" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberDefaultProductHeight %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:SiteSettings, DefaultProductWeight %>" /></label>
                    <asp:TextBox runat="server" ID="txtDefaultProductWeight" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator6" runat="server"
                        ControlToValidate="txtDefaultProductWeight" Type="Double" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberDefaultProductWeight %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DigitalLibraryUrl %>" /></label><asp:TextBox runat="server" ID="txtDigitalLibraryUrl" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DisableExpressCheckout %>" /></label><asp:CheckBox runat="server" ID="cbkDisableExpressCheckout" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DisableQuantityForMediaProduct %>" /></label><asp:CheckBox runat="server" ID="cbkDisableQuantityForMedia" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DollarOffOrderCouponTypeId %>" /></label><asp:TextBox runat="server" ID="txtDollarOffOrderCouponTypeId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DollarOffShippingCouponTypeId %>" /></label><asp:TextBox runat="server" ID="txtDollarOffShippingCouponTypeId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, EditOnAccountAddress %>" /></label><asp:CheckBox runat="server" ID="cbEditOnAccountAddress" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, EditPersonalizationAttributeURL %>" /></label><asp:TextBox ID="txtEditPersonalizedAttributeURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize36" runat="server" Text="<%$ Resources:SiteSettings, EnforceMaxDiscountPercentageOnCoupons %>" /></label><asp:CheckBox runat="server" ID="cbEnforceMaxDiscountPercentageOnCoupons" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize37" runat="server" Text="<%$ Resources:SiteSettings, EnforceMaxDiscountPercentageOnPriceSets %>" /></label><asp:CheckBox runat="server" ID="cbEnforceMaxDiscountPercentageOnPriceSets" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize38" runat="server" Text="<%$ Resources:SiteSettings, EnforceMaxDiscountPercentageOnSalePriceSets %>" /></label><asp:CheckBox runat="server" ID="cbEnforceMaxDiscountPercentageOnSalePriceSets" /></span>

                <span class="formRow">

                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AttributeFileUploadURL %>" /></label><asp:TextBox ID="txtAttributeFileUploadURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ExcedingLineOfCreditOrderAction %>" /></label>
                    <asp:DropDownList runat="server" ID="ddlexecLineOfCreditOrderAction">
                        <asp:ListItem Text="<%$ Resources:SiteSettings, AllowOrder %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PutOrderOnHold %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PreventOrderFromBeingPlaced %>" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FeaturesAttributesGroupId %>" /></label><asp:TextBox runat="server" ID="txtFeaturesAttributesGroupId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FeaturesTopSellerId %>" /></label><asp:TextBox runat="server" ID="txtFeaturesTopSellerId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FeatureTypeForAutoNewProducts %>" /></label><asp:TextBox ID="txtFeatureTypeForAutoNewProducts" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FeatureTypeForTopSellers %>" /></label><asp:TextBox ID="txtFeatureTypeForTopSellers" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FlatRateShippingCouponTypeId %>" /></label><asp:TextBox runat="server" ID="txtFlatRateShippingCouponTypeId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FreeShippingOptionId %>" /></label><asp:DropDownList ID="ddlFreeShippingOptionId" runat="server" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, GenerateOrderNumberInSequence %>" /></label><asp:CheckBox ID="ckOrderNumberInSeq" runat="server" Text="" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:SiteSettings, GWSProcessOrderImmediate %>" /></label>
                    <asp:DropDownList ID="ddlGwsProcessImmediate" runat="server">
                        <asp:ListItem Text="true" Value="true" />
                        <asp:ListItem Text="false" Value="false" />
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, HomeUrl %>" /></label><asp:TextBox runat="server" ID="txtHomeUrl" CssClass="textBoxes textBoxesDisabled" ReadOnly="true" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, IsDevelopment %>" /></label><asp:CheckBox runat="server" ID="chkIsDevelopment" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, IsSiteUnderSSL %>" /></label><asp:CheckBox runat="server" ID="cbkRequireSSL" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, IsUserRegistrationrequiredEmailConfirmation %>" /></label><asp:CheckBox runat="server" ID="cbkRequireRegistrationEmailConfirmation" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, NavigationCategoryContentFolderId %>" /></label><asp:TextBox ID="txtNavigationCategoryContentFolderId" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, NavigationCategoryContentDefinitionFolderId %>" /></label><asp:TextBox ID="txtNavigationCategoryContentDefinitionFolderId" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MainImageTypeNumber %>" /></label><asp:TextBox runat="server" ID="txtMainImageTypeNumber" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ManageWishListItemsPageURL %>" /></label><asp:TextBox runat="server" ID="txtManageWishListItemsPageURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MediaLibraryPath %>" /></label><asp:TextBox runat="server" ID="txtMediaLibraryPath" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MinimumOrderAmount %>" /></label><asp:TextBox ID="txtMinimumOrderAmount" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MaximumOrderAmount %>" /></label><asp:TextBox ID="txtMaximumOrderAmount" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MinimumOrderAmountAction %>" /></label>
                    <asp:DropDownList runat="server" ID="ddlMinimumOrderAmountAction">
                        <asp:ListItem Text="<%$ Resources:SiteSettings, AllowOrder %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PutOrderOnHold %>" Value="2"> </asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PreventOrderFromBeingPlaced %>" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MaximumOrderAmountAction %>" /></label>
                    <asp:DropDownList runat="server" ID="ddlMaximumOrderAmountAction">
                        <asp:ListItem Text="<%$ Resources:SiteSettings, AllowOrder %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PutOrderOnHold %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PreventOrderFromBeingPlaced %>" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MyAccountURL %>" /></label><asp:TextBox runat="server" ID="txtMyAccountURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, MyOrderURL %>" /></label><asp:TextBox runat="server" ID="txtMyOrderURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, OrderNumberPrefix %>" /></label><asp:TextBox runat="server" ID="txtOrderNumberPrefix" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, OrderAttributeGroupID %>" /></label><asp:TextBox ID="txtOrderAttributeGroupId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, OverdueOrderAction %>" /></label>
                    <asp:DropDownList runat="server" ID="ddlOverdueOrderAction">
                        <asp:ListItem Text="<%$ Resources:SiteSettings, AllowOrder %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PutOrderOnHold %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, PreventOrderFromBeingPlaced %>" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, PercentOffOrderCouponTypeId %>" /></label><asp:TextBox runat="server" ID="txtPercentOffOrderCouponTypeId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, PercentOffShippingCouponTypeId %>" /></label><asp:TextBox runat="server" ID="txtPercentOffShippingCouponTypeId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, PrintOrderConfirmationPage %>" /></label><asp:TextBox runat="server" ID="txtOrderConfirmPage" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductDataCacheKey %>" /></label><asp:TextBox runat="server" ID="txtProductDataCacheKey" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductImagesFolder %>" /></label><asp:TextBox ID="txtProductImagesFolder" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductAttributeFileUploadFolder %>" /></label><asp:TextBox ID="txtProductExternalFileUpload" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductNavRewriteRulesCacheKey %>" /></label><asp:TextBox runat="server" ID="txtProductNavRewriteRulesCacheKey" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductSearchPageUrl %>" /></label><asp:TextBox runat="server" ID="txtProdSearchPageUrl" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductTypePageMapNodeId %>" /></label><asp:TextBox runat="server" ID="txtPTPageMapNodeId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductURLToProductIdMapCacheKey %>" /></label><asp:TextBox runat="server" ID="txtProductURLCacheKey" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RecentlyViewedProductsDisplayCount %>" /></label>
                    <asp:TextBox runat="server" ID="txtRVDisplayCount" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtRVDisplayCount" runat="server"
                        ControlToValidate="txtRVDisplayCount" Type="Integer" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberInRecentlyViewedProductsDisplayCountField %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RefreshCacheJobId %>" /></label><asp:TextBox runat="server" ID="txtRefreshCacheJobId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SiteSetting_ReturnWarehouse %>" /></label>
                    <asp:DropDownList ID="ddlReturnWarehouse" runat="server" />
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SecuredPagesHttps %>" /></label><asp:TextBox runat="server" ID="txtSecuredPages" TextMode="MultiLine" Rows="5" Width="641"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SelectShippingPopupURL %>" /></label><asp:TextBox runat="server" ID="txtSelectShipUrl" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SetUsernameAsEmail %>" /></label><asp:CheckBox runat="server" ID="chkIsUserNameAsEmail" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, ShippingRateMarkupPercentage %>" /></label>
                    <asp:TextBox runat="server" ID="txtShippingMarkupPercentage" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator1" runat="server"
                        ControlToValidate="txtShippingMarkupPercentage" Type="Double" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberShippingMarkupPercentage %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, ShippingRateMarkupAmount %>" /></label>
                    <asp:TextBox runat="server" ID="txtShippingMarkupAmount" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator2" runat="server"
                        ControlToValidate="txtShippingMarkupAmount" Type="Currency" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberShippingMarkupAmount %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ShippingRatesPopupURL %>" /></label><asp:TextBox runat="server" ID="txtShipRateUrl" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ShowAuthCode %>" /></label><asp:CheckBox runat="server" ID="cbShowAuthCode" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ShowOutOfStockProducts %>" /></label><asp:CheckBox runat="server" ID="cbShowOutOfStockProducts" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SkipShippingMediaOnly %>" /></label><asp:CheckBox ID="cbkSkipShippingMediaOnly" runat="server" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SKULevelSystemAttributesGroupId %>" /></label><asp:TextBox runat="server" ID="txtSkuLevelSystemAttributesGroupId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, StoreAdminEmail %>" /></label><asp:TextBox ID="txtStoreAdminEmail" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, StoreCustomerServiceEmail %>" /></label><asp:TextBox ID="txtStoreCustomerServiceEmail" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, TopSellersCount %>" /></label>
                    <asp:TextBox ID="txtTopSellersCount" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtTopSellersCount" runat="server"
                        ControlToValidate="txtTopSellersCount" Type="Integer" Operator="DataTypeCheck"
                        ValidationGroup="callback" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidNumberInTopSellersCountField %>"
                        Text="*"></asp:CompareValidator>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UnitedStatesId %>" /></label><asp:TextBox runat="server" ID="txtUnitedStatesId" CssClass="textBoxes textBoxesDisabled" Width="637" ReadOnly="true"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UseSimpleSearchFilters %>" /></label><asp:CheckBox runat="server" ID="cbUseSimpleSearchFilters"></asp:CheckBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ViewCartLoginOnly %>" /></label><asp:CheckBox runat="server" ID="cbViewCartLoginOnly" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ViewOrderLink %>" /></label><asp:TextBox ID="txtViewOrderLink" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ViewPriceAfterLoginOnly %>" /></label><asp:CheckBox runat="server" ID="cbViewPriceLoginOnly" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ViewProductAfterLoginOnly %>" /></label><asp:CheckBox runat="server" ID="cbViewProductLoginOnly" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, WishListEmailPageURL %>" /></label><asp:TextBox runat="server" ID="txtWishListEmailPageURL" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, WishListsEnabled %>" /></label><asp:CheckBox runat="server" ID="cbUseWishLists" /></span>
            </div>
            <div id="divShipper" style="display: none;">
                <span class="formRow">
                    <asp:CheckBox runat="server" ID="chkUseRealTime" /><asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UseRealTimeShippingRate %>" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, DefaultAddressToCalcuateShipping %>" /></label><asp:TextBox ID="txtDefaultAddressToCalcuateShipping" runat="server" CssClass="textBoxes" Width="637" MaxLength="36"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FedexKey %>" /></label><asp:TextBox ID="txtFedexKey" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FedexPassword %>" /></label>
                    <asp:TextBox ID="txtFedexPassword" TextMode="Password" runat="server" CssClass="textBoxes"
                        Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FedexAccountNumber %>" /></label>
                    <asp:TextBox ID="txtFedexAccountNumber" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FedexMeterNumber %>" /></label>
                    <asp:TextBox ID="txtFedexMeterNumber" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UPSLicenseNumber %>" /></label>
                    <asp:TextBox ID="txtUPSLicenseNumber" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UPSPassword %>" /></label>
                    <asp:TextBox ID="txtUPSPassword" TextMode="Password" runat="server" CssClass="textBoxes"
                        Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UPSUserName %>" /></label>
                    <asp:TextBox ID="txtUPSUserName" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize24" runat="server" Text="<%$ Resources:SiteSettings, UPSShipperNumber %>" /></label>
                    <asp:TextBox ID="txtUPSShipperNumber" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UPSShippingProdURL %>" /></label>
                    <asp:TextBox ID="txtUPSShippingProdURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UPSShippingDevURL %>" /></label>
                    <asp:TextBox ID="txtUPSShippingDevURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, USPSDevelopmentURL %>" /></label>
                    <asp:TextBox ID="txtUSPSDevelopmentURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, USPSProductionURL %>" /></label>
                    <asp:TextBox ID="txtUSPSProductionURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, USPSUserName %>" /></label>
                    <asp:TextBox ID="txtUSPSUserName" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, USPSPassword %>" /></label>
                    <asp:TextBox ID="txtUSPSPassword" TextMode="Password" runat="server" CssClass="textBoxes"
                        Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UnitOfMeasureLength %>" /></label>
                    <asp:TextBox ID="txtUnitOfMeasureLength" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, UnitOfMeasureWeight %>" /></label>
                    <asp:TextBox ID="txtUnitOfMeasureWeight" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
            </div>
            <div id="divTax" style="display: none;">
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, TaxShippingHandling %>" /></label>
                    <asp:DropDownList runat="server" ID="ddlTaxSandH">
                        <asp:ListItem Text="<%$ Resources:SiteSettings, None %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, ShippingOnly %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, HandlingOnly %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:SiteSettings, ShippingAndHandling %>" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SellerVATRegistration %>" /></label>
                    <asp:TextBox ID="txtSellerVATReg" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, StoreLocation %>" /></label>
                    <asp:TextBox ID="txtStoreLocationId" runat="server" CssClass="textBoxes textBoxesDisabled" Width="637"></asp:TextBox>
                    <img src="../../App_Themes/General/images/icon-browse.gif" onclick="return OpenEditAddressPopup('<%=txtStoreLocationId.ClientID %>');"
                        alt="Browse" />
                </span>
                <div class="form-row" id="spnUnitesStatesLst" runat="server">
                    <label class="form-label">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, TaxableStates1 %>" /></label>
                    <div class="form-value" style="width: 760px;">
                        <input id="cbCheckAll" type="checkbox" onclick="javascript: CheckBoxListSelect ('<%= chkLstStates.ClientID %>')" /><asp:Localize
                            runat="server" Text="<%$ Resources:SiteSettings, CheckAll %>" />
                        <asp:CheckBoxList ID="chkLstStates" runat="server" RepeatLayout="Table" RepeatColumns="6" />
                    </div>
                </div>
                <div class="form-row" id="spnCanadaLst" runat="server">
                    <label class="form-label">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CATaxableStates %>" /></label>
                    <div class="form-value" style="width: 760px;">
                        <input id="cbCACheckAll" type="checkbox" onclick="javascript: CheckBoxListSelect ('<%= chkLstCAStates.ClientID %>')" /><asp:Localize
                            runat="server" Text="<%$ Resources:SiteSettings, CheckAll %>" />
                        <asp:CheckBoxList ID="chkLstCAStates" runat="server" RepeatLayout="Table" RepeatColumns="6" />
                    </div>
                </div>
            </div>
            <div id="divPayment" style="display: none;">
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CurrencyISOCode %>" /></label>
                    <asp:TextBox ID="txtCurrencyISOCode" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CurrencySymbol %>" /></label>
                    <asp:TextBox ID="txtCurrencySymbol" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize39" runat="server" Text="<%$ Resources:SiteSettings, AuthorizeNetAPILogin %>" /></label>
                    <asp:TextBox ID="txtAuthorizeNetAPILogin" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize40" runat="server" Text="<%$ Resources:SiteSettings, AuthorizeNetTransactionKey %>" /></label>
                    <asp:TextBox ID="txtAuthorizeNetTransactionKey" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:SiteSettings, CyberSourceMerchantId %>" /></label>
                    <asp:TextBox ID="txtCyberSourceMerchantId" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:SiteSettings, CyberSourceTransactionKey %>" /></label>
                    <asp:TextBox ID="txtCyberSourceTransactionKey" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize33" runat="server" Text="<%$ Resources:SiteSettings, CyberSourceSecretKey %>" /></label>
                    <asp:TextBox ID="txtCybersourceSecretKey" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize34" runat="server" Text="<%$ Resources:SiteSettings, CyberSourceAccessKey %>" /></label>
                    <asp:TextBox ID="txtCybersourceAccessKey" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize35" runat="server" Text="<%$ Resources:SiteSettings, CyberSourceProfileId %>" /></label>
                    <asp:TextBox ID="txtCybersourceProfileId" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize26" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProIsSandbox %>" /></label><asp:CheckBox ID="chkPayFlowIsSandbox" runat="server" /></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize27" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProSandboxUrl %>" /></label><asp:TextBox ID="txtPayFlowSandboxUrl" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize28" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProProductionUrl %>" /></label><asp:TextBox ID="txtPayFlowProductionUrl" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize29" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProVendor %>" /></label><asp:TextBox ID="txtPayFlowVendor" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize30" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProPartner %>" /></label><asp:TextBox ID="txtPayFlowPartner" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize31" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProMerchantUser %>" /></label><asp:TextBox ID="txtPayFlowMerchantUser" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize32" runat="server" Text="<%$ Resources:SiteSettings, PayFlowProPassword %>" /></label><asp:TextBox ID="txtPayFlowPassword" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>

                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPISandboxHost %>" /></label><asp:TextBox ID="txtPaypalAPISandboxHost" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPISandboxEndPointURL %>" /></label><asp:TextBox ID="txtPaypalAPISandboxEndPointURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPILiveHost %>" /></label><asp:TextBox ID="txtPaypalAPILiveHost" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPILiveEndPointURL %>" /></label><asp:TextBox ID="txtPaypalAPILiveEndPointURL" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPIUserName %>" /></label><asp:TextBox ID="txtPaypalAPIUserName" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPIPassword %>" /></label><asp:TextBox ID="txtPaypalAPIPassword" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:SiteSettings, PaypalAPISignature %>" /></label><asp:TextBox ID="txtPaypalAPISignature" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:SiteSettings, PaypalExpressCheckoutReturnUrl %>" /></label><asp:TextBox ID="txtPaypalExpressCheckoutReturnUrl" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:SiteSettings, PaypalExpressCheckoutCancelUrl %>" /></label><asp:TextBox ID="txtPaypalExpressCheckoutCancelUrl" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox></span>
                <span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize21" runat="server" Text="<%$ Resources:SiteSettings, WebsitePaymentProApiEnvironment %>" /></label>
                    <asp:TextBox ID="txtWPPAPIEnvironment" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize22" runat="server" Text="<%$ Resources:SiteSettings, WebsitePaymentProApiUsername %>" /></label>
                    <asp:TextBox ID="txtWPPAPIUsername" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize23" runat="server" Text="<%$ Resources:SiteSettings, WebsitePaymentProApiPassword %>" /></label>
                    <asp:TextBox ID="txtWPPAPIPassword" TextMode="Password" runat="server" CssClass="textBoxes"
                        Width="637"></asp:TextBox>
                </span><span class="formRow">
                    <label class="formLabel">
                        <asp:Localize ID="Localize25" runat="server" Text="<%$ Resources:SiteSettings, WebsitePaymentProSignature %>" /></label>
                    <asp:TextBox ID="txtWPPSignature" runat="server" CssClass="textBoxes" Width="637"></asp:TextBox>
                </span>
            </div>
            <div id="divB2B" style="display: none;">
                <span class="formRow clear-fix">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AllowOnAccountPayment %>" /></label>
                    <asp:CheckBox runat="server" ID="cbAllowonAccountPayment" />
                </span>
                <span class="formRow clear-fix">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AllowMultipleLogins %>" /></label>
                    <asp:CheckBox ID="cbMultipleLogins" runat="server" />
                </span>
                <span class="formRow clear-fix">
                    <label class="formLabel">
                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RequireCustomerLineofCredit %>" /></label>
                    <asp:CheckBox runat="server" ID="cbRequireCustomerLineOfCredit" />
                </span>
            </div>
            <div id="divCustom" style="display: none;">
                <ComponentArt:Menu ID="mnuCustomSettings" SkinID="ContextMenu" runat="server">
                    <Items>
                        <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:SiteSettings, AddSetting %>"
                            Look-LeftIconUrl="cm-icon-add.png">
                        </ComponentArt:MenuItem>
                        <ComponentArt:MenuItem LookId="BreakItem">
                        </ComponentArt:MenuItem>
                        <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:SiteSettings, DeleteSetting %>"
                            Look-LeftIconUrl="cm-icon-delete.png">
                        </ComponentArt:MenuItem>
                    </Items>
                    <ClientEvents>
                        <ItemSelect EventHandler="mnuCustomSettings_onItemSelect" />
                    </ClientEvents>
                </ComponentArt:Menu>
                <ComponentArt:Grid ID="grdCustomSettings" AllowTextSelection="true" EnableViewState="true"
                    EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False"
                    KeyboardEnabled="false" ShowFooter="false" SkinID="Default" FooterCssClass="GridFooter"
                    RunningMode="Client" runat="server" PageSize="1000">
                    <Levels>
                        <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="Row"
                            ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                            HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                            HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="Row"
                            GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            SortImageWidth="8" AllowSorting="false" SortImageHeight="7" AllowGrouping="false"
                            AlternatingRowCssClass="AlternateDataRow" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
                            EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
                            <Columns>
                                <ComponentArt:GridColumn HeadingText="Key" DataField="Key" DataCellClientTemplateId="KeyTemplate"
                                    Width="320" AllowReordering="False" FixedWidth="true" />
                                <ComponentArt:GridColumn HeadingText="Value" DataField="Value" DataCellClientTemplateId="ValueTemplate"
                                    Width="560" AllowReordering="False" FixedWidth="true" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="KeyTemplate">
                            <input id="txtKey" class="textBoxes" style="width: 300px;" type="text" value="## DataItem.getCurrentMember().get_value() ##"
                                onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this.value);" />
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="ValueTemplate">
                            <input id="txtValue" class="textBoxes" style="width: 550px;" type="text" value="## DataItem.getCurrentMember().get_value() ##"
                                onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this.value);" />
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                    <ClientEvents>
                        <ContextMenu EventHandler="grdCustomSettings_onContextMenu" />
                    </ClientEvents>
                </ComponentArt:Grid>
            </div>
        </div>
        <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
            ValidationGroup="callback" />
    </div>
</asp:Content>
