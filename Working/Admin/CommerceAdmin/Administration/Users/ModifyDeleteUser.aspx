﻿<%@ Page Title="<%$ Resources:SiteSettings, iAPPSCommerceAdministrationUsersModifyDeleteCommerceUser %>"
    Language="C#" MasterPageFile="~/Administration/Users/UserMaster.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeBehind="ModifyDeleteUser.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ModifyDeleteUser"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="ModifyDeleteUser" Src="~/UserControls/Administration/Users/ModifyDeleteUser.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cphHead">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="userHeaderButtons">
    <asp:HyperLink ID="hplAddNewUser" CssClass="primarybutton" Text="<%$ Resources:SiteSettings, AddNewUser %>"
        NavigateUrl="AddNewUser.aspx" runat="server" />
</asp:Content>
<asp:Content ID="modifyDeleteUserContent" runat="server" ContentPlaceHolderID="userContentHolder">
    <uc:ModifyDeleteUser ID="modifyDeleteUser" runat="server" />
</asp:Content>
