﻿<%@ Page Title="<%$ Resources:SiteSettings, iAPPSCommerceAdministrationUsersAddNewCommerceUser %>"
    Language="C#" MasterPageFile="~/Administration/Users/UserMaster.master" AutoEventWireup="true" EnableEventValidation="false" 
    CodeBehind="AddNewUser.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddNewUser" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="AddNewUser" Src="~/UserControls/Administration/Users/AddNewUser.ascx" %>
<asp:Content ID="addNewUserContent" ContentPlaceHolderID="userContentHolder" runat="server">
    <UC:AddNewUser runat="server" ID="addNewUser" />
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="userHeaderButtons">
    <a href="ModifyDeleteUser.aspx" class="button">
        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Cancel %>" /></a>
    <asp:Button ID="btnSaveUser" runat="server" OnClick="btnSaveUser_Click" Text="<%$ Resources:SiteSettings, Save %>"
        ToolTip="<%$ Resources:SiteSettings, Save %>" CssClass="primarybutton" OnClientClick="return ValidateEditUser();" />
</asp:Content>
