﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewAllWarehouse.aspx.cs"
    Title="<%$ Resources:SiteSettings, ViewAllWarehouse%>" StylesheetTheme="General"
    MasterPageFile="~/MainMaster.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewAllWarehouse" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var warehouseId;
        function grdWarehouseListing_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            var WarehouseListItem = eventArgs.get_item();
            WarehouseId = WarehouseListItem.getMember(7).get_text();
            mnuWarehouse.showContextMenuAtEvent(evt);
        }

        function mnuWarehouse_onItemSelect(sender, eventArgs) {

            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            if (selectedMenuId == 'cmAdd') {
                window.location = '/CommerceAdmin/Administration/Warehouse/EditWarehouse.aspx';
            }
            else if (selectedMenuId == 'cmEdit') {
                window.location = '/CommerceAdmin/Administration/Warehouse/EditWarehouse.aspx?WarehouseId=' + WarehouseId;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <div class="warehouse">
        <div class="page-header">
            <h1>
                <%= SiteSettings.Warehouses %></h1>
            <asp:HyperLink ID="hplAddNewWarehouse" runat="server" CssClass="primarybutton"
                Text="<%$ Resources:SiteSettings, AddNewWarehouse %>" NavigateUrl="~/Administration/Warehouse/EditWarehouse.aspx" />
            <div class="clearFix">
            </div>
        </div>
        <asp:Repeater runat="server" ID="rptWarehouse" OnItemDataBound="rptWarehouse_ItemDataBound">
            <HeaderTemplate>
                <hr style="margin-bottom: 0;" />
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="warehouse-list">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td width="80px" align="center" valign="top">
                        <img src="../../App_Themes/General/images/warehouse-icon.png" alt="" />
                    </td>
                    <td valign="top" width="40%">
                        <h2>
                            <asp:Literal ID="ltTitle" runat="server" /></h2>
                        <div class="form-row">
                            <asp:Literal ID="ltAddress" runat="server" />
                        </div>
                        <div class="form-row">
                            <asp:HyperLink ID="hplEditWarehouse" runat="server" CssClass="small-button"
                                Text="<%$ Resources:SiteSettings, Edit %>" />
                            <asp:LinkButton ID="lbtnUpdateStatus" runat="server" CssClass="small-button"
                                Text="<%$ Resources:SiteSettings, Disable %>" OnClick="lbtnUpdateStatus_Click" />
                            <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="small-button" Text="<%$ Resources:SiteSettings, Warehouse_Delete %>"
                                OnClick="lbtnDelete_Click" />
                            <asp:HyperLink ID="hplRestock" runat="server" CssClass="small-button" Text="<%$ Resources:SiteSettings, ViewRestockings %>" />
                        </div>
                    </td>
                    <td valign="top" width="50%">
                        <div class="form-row">
                            <label class="form-label">
                                <%= SiteSettings.InventoryManagement %></label>
                            <div class="form-value">
                                <asp:Literal ID="ltWarehouseType" runat="server" /></div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <%= SiteSettings.ShippingCoverage %></label>
                            <div class="form-value">
                                <asp:Literal ID="ltShippingCoverage" runat="server" />
                            </div>
                        </div>
                    </td>
                    <tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
