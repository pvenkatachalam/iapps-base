﻿<%@ Page Title="<%$ Resources:SiteSettings, iAPPSCommerceAdministrationManageRestocking %>"
    Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="ViewRestockings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewRestockings" StylesheetTheme="General" %>

<%@ Register Src="~/UserControls/General/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<%@ Register TagPrefix="SM" TagName="ScriptManager" Src="~/UserControls/General/ScriptManagerControl.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //function to change the css of the parent tr
        function fn_ChangeParentCssClass(objCheckbox) {
            var parentRow = $(objCheckbox).parents('tr');
            if (objCheckbox.checked) {
                $(parentRow).addClass('selected-row');
            }
            else {
                $(parentRow).removeClass('selected-row');
            }
        }
        //function to select all the checkboxes which are enabled
        function fn_CheckAllCheckboxes(status) {
            $('#gridview input[type=checkbox]').each(function () {
                if ($(this).attr("disabled") != "disabled")
                    $(this).attr('checked', status);
            });
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <div class="restocking">
        <div class="page-header">
            <h1>
                <%= SiteSettings.Restocking_DeliveriesRestocking %></h1>
            <asp:HyperLink ID="hplAddDelivery" runat="server" CssClass="primarybutton"
                Text="<%$ Resources:SiteSettings, Restocking_AddADelivery %>" NavigateUrl="~/Administration/Warehouse/EditRestocking.aspx" />
            <div class="clear-fix">
                &nbsp;</div>
        </div>
        <SM:ScriptManager ID="sManager" runat="server" />
        <asp:UpdatePanel ID="upRestocking" runat="server">
            <ContentTemplate>
                <div class="grid-utility">
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="textBoxes" Width="300" ToolTip="<%$ Resources:SiteSettings, Restocking_TypePONumberHere %>"></asp:TextBox>
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="160" />
                    <asp:DropDownList ID="ddlWarehouse" runat="server" Width="200" />
                    <uc1:DateRange ID="ctlRestockingDate" runat="server" DefaultOption="PredefinedRange" />
                    <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:SiteSettings, Restocking_Filter %>"
                        OnClick="btnSearch_Click" CssClass="small-button" />
                </div>
                <asp:ObjectDataSource ID="odsRestocking" runat="server" EnablePaging="true" EnableViewState="true"
                    EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.Commerce.Web.ViewRestockings" OnSelecting="odsRestocking_Selecting"
                    OnDeleted="odsRestocking_Deleted" MaximumRowsParameterName="pageSize" StartRowIndexParameterName="rowIndex"
                    SortParameterName="sortExpression" SelectCountMethod="GetRecordCount" SelectMethod="GetRecords"
                    DeleteMethod="DeleteRecord"></asp:ObjectDataSource>
                <asp:ListView ID="lstRestocking" runat="server" ItemPlaceholderID="itemContainer"
                    DataSourceID="odsRestocking" DataKeyNames="Id" OnItemDataBound="lstRestocking_ItemDataBound">
                    <LayoutTemplate>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="grid-view"
                            id="gridview">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:LinkButton ID="lbtnExpectedDate" runat="server" Text="<%$ Resources:SiteSettings, Restocking_ExpectedDate1 %>"
                                            CommandName="Sort" CommandArgument="ExpectedDate" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lbtnTitle" runat="server" Text="<%$ Resources:SiteSettings, Restocking_PONo %>"
                                            CommandName="Sort" CommandArgument="Title" />
                                    </th>
                                    <th>
                                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_Warehouse %>" />
                                    </th>
                                    <th>
                                        <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_Status1 %>" />
                                    </th>
                                    <th>
                                        <asp:LinkButton ID="lbtnRestockingCode" runat="server" Text="<%$ Resources:SiteSettings, Restocking_RestockingCode1 %>"
                                            CommandName="Sort" CommandArgument="RestockingCode" />
                                    </th>
                                    <th>
                                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, Restocking_NoOfSKUs %>" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="itemContainer" runat="server" />
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <asp:DropDownList ID="ddlPageSize" runat="server" Visible="false" AutoPostBack="true"
                                            OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Text="10 items Per Page" Value="10" />
                                            <asp:ListItem Text="20 items Per Page" Value="20" />
                                            <asp:ListItem Text="25 items Per Page" Value="25" />
                                        </asp:DropDownList>
                                        <asp:DataPager ID="dpRestocking" runat="server" PageSize="10">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="false" ShowLastPageButton="false"
                                                    NextPageImageUrl="~/App_Themes/General/images/next-page.png" RenderNonBreakingSpacesBetweenControls="false"
                                                    PreviousPageImageUrl="~/App_Themes/General/images/previous-page.png" ButtonCssClass="pager-button" />
                                                <asp:TemplatePagerField>
                                                    <PagerTemplate>
                                                        <div class="pager">
                                                            <%# string.Format(JSMessages.Page0Of1Items, (Container.StartRowIndex / Container.PageSize) + 1, 
                                                    Math.Ceiling((double)Container.TotalRowCount / Container.PageSize), Container.TotalRowCount)%>
                                                        </div>
                                                    </PagerTemplate>
                                                </asp:TemplatePagerField>
                                            </Fields>
                                        </asp:DataPager>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
                            <td>
                                <asp:Literal ID="ltExpectedDate" runat="server" Text='<%# Eval("ExpectedDate") %>' />
                            </td>
                            <td>
                                <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("Title") %>' ToolTip='<%# Eval("Title") %>'
                                    NavigateUrl='<%# string.Format("~/Administration/Warehouse/EditRestocking.aspx?RestockingId={0}", Eval("Id")) %>' />
                            </td>
                            <td>
                                <asp:Literal ID="ltWarehouse" runat="server" Text='<%# Eval("WarehouseName") %>' />
                            </td>
                            <td class="dark-font">
                                <asp:Literal ID="ltStatus" runat="server" Text='<%# Eval("Status") %>' />
                            </td>
                            <td>
                                <asp:Literal ID="ltRestockingCode" runat="server" Text='<%# Eval("RestockingCode") %>' />
                            </td>
                            <td>
                                <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("NumberOfSkus") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <div class="grid-emptyText">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, Restocking_NoItems %>" /></div>
                    </EmptyDataTemplate>
                </asp:ListView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
