﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditWarehouse.aspx.cs"
    StylesheetTheme="General" MasterPageFile="~/MainMaster.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Administration.Warehouse.EditWarehouse" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ValidateZip(src, args) {
            args.IsValid = true;
            var zipRE = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ZipCodeRegex %>'/>";
            var countryDDL = document.getElementById('<%=ddlCountry.ClientID %>');

            if (countryDDL.options[countryDDL.selectedIndex].value == usId) {
                if (!args.Value.match(/^\d{5}([\-]\d{4})?$/)) args.IsValid = false;
            }
        }

    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <asp:ScriptManager runat="server" />
    <div class="warehouse warehouse-details">
        <div class="page-header">
            <h1><asp:Literal ID="litHeading" runat="server"></asp:Literal></h1>
            <asp:LinkButton ID="lbtnCancel" Text="Cancel" runat="server" CssClass="button" style="float: right; margin-left: 10px;" />
            <asp:Button ID="btnSave" Text="Save" runat="server" CausesValidation="true" ValidationGroup="vGroup" OnClick="btnSave_Click" CssClass="primarybutton" style="float: right;" />
            <div class="clearFix"></div>
        </div>
        <hr />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="form-row">
                    <label class="form-label"><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_Type %>" /><span class="req">*</span></label>
                    <div class="form-value">
                        <asp:RadioButtonList runat="server" ID="rdlType" RepeatDirection="Horizontal" OnSelectedIndexChanged="rdlType_SelectedIndexChanged"
                            AutoPostBack="true" style="float: left;">
                            <asp:ListItem Text="Internal" Value="1" Selected="True" />
                            <asp:ListItem Text="External" Value="3" />
                            <asp:ListItem Text="Digital" Value="2" />
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ControlToValidate="rdlType" runat="server" ID="RequiredFieldValidator1"
                            ValidationGroup="vGroup" Text=" " ErrorMessage="please select a warehouse type" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_Name %>" /><span class="req">*</span></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="textbox" Width="274" />
                        <asp:RequiredFieldValidator runat="server" ID="rvTxtTitle" ControlToValidate="txtTitle" ValidationGroup="vGroup" Text=" " ErrorMessage="please enter warehouse name" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_Description %>" /><span class="req">*</span></label>
                    <div class="form-value"><asp:TextBox ID="txtDescription" runat="server" CssClass="textbox" Width="274" /></div>
                </div>
                <asp:PlaceHolder runat="server" ID="phPhysicalWarehouse">
                        <asp:Panel runat="server" ID="pnlFirstName" CssClass="form-row">
                        <label class="form-label"><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Warehouse_FirstName %>" /><span class="req">*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtFirstName"
                                ValidationGroup="vGroup" runat="server" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterFirstName %>"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="rvFirstName" ControlToValidate="txtFirstName"
                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInFirstName %>"
                                Text=" "></asp:RegularExpressionValidator>
                        </div>
                    </asp:Panel>
                            <asp:Panel runat="server" ID="pnlLastName" CssClass="form-row">
                        <label class="form-label"><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Warehouse_LastName %>" /><span class="req">*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtLastName"
                                ValidationGroup="vGroup" runat="server" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterLastName %>"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ControlToValidate="txtLastName"
                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInLastName %>"
                                Text=" "></asp:RegularExpressionValidator>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAddressLine1" CssClass="form-row">
                        <label class="form-label"><asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_AddressLine1 %>" /><span class="req">*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtAddress1" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtAddress1"
                                ValidationGroup="vGroup" runat="server" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAddressLine1 %>"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="revtxtAddress1" ControlToValidate="txtAddress1"
                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInAddressLine1 %>"
                                Text=" "></asp:RegularExpressionValidator>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAddressLine2" CssClass="form-row">
                        <label class="form-label"><asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_AddressLine2 %>" /></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtAddress2" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                            <asp:RegularExpressionValidator runat="server" ID="revtxtAddress2" ControlToValidate="txtAddress2"
                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInAddressLine2 %>"
                                Text=" "></asp:RegularExpressionValidator>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlCity" CssClass="form-row">
                        <label class="form-label"><asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_City %>" /><span class="req">*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtCity" runat="server" CssClass="textbox" Width="274" MaxLength="255"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvtxtCity" ControlToValidate="txtCity" runat="server"
                                ValidationGroup="vGroup" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheCity %>"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="revtxtCity" ControlToValidate="txtCity"
                                ValidationGroup="vGroup" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInCity %>"
                                Text=" "></asp:RegularExpressionValidator>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlCountry" EnableViewState="true" CssClass="form-row">
                        <label class="form-label"><asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_Country %>" /><span class="req">*</span></label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="selectbox" Width="282" EnableViewState="true"
                                AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" />
                            <asp:RequiredFieldValidator ID="reqddlCountry" ControlToValidate="ddlCountry" runat="server"
                                ValidationGroup="vGroup" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectACountry %>"
                                InitialValue="00000000-0000-0000-0000-000000000000"></asp:RequiredFieldValidator>
                        </div>
                    </asp:Panel>
                    <asp:PlaceHolder ID="phCountryRelated" runat="server">
                        <asp:Panel runat="server" ID="pnlState" EnableViewState="true" CssClass="form-row">
                            <label class="form-label"><asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_State %>" /><span class="req">*</span></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlState" runat="server" CssClass="selectbox" Width="282" EnableViewState="true"
                                    AutoPostBack="false" onchange="ddlstate_onchange(this)" />
                                <asp:TextBox runat="server" ID="txtState" CssClass="textbox" Width="274" MaxLength="255" Visible="false"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvddlState" ControlToValidate="ddlState" runat="server"
                                    ValidationGroup="vGroup" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAState %>"
                                    InitialValue="00000000-0000-0000-0000-000000000000"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="rvStateText" ControlToValidate="txtState" runat="server"
                                    Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAState %>"></asp:RequiredFieldValidator>
                            </div>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlZipcode" CssClass="form-row">
                            <label class="form-label"><asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_ZipPostalCode %>" /><span class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtZip" runat="server" Width="274" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtZip" ControlToValidate="txtZip" runat="server"
                                    ValidationGroup="vGroup" Text=" " ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAZipCode %>"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvtxtZip" runat="server" ControlToValidate="txtZip" Text=" "
                                    ValidationGroup="vGroup" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidZipCode %>"
                                    ClientValidationFunction="ValidateZip"></asp:CustomValidator>
                            </div>
                        </asp:Panel>
                    </asp:PlaceHolder>
                </asp:PlaceHolder>
                <div class="form-row">
                    <label class="form-label"><asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_FullfillmentProvider %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlFullfillmentProvider" runat="server" CssClass="selectbox" Width="282" />
                    </div>
                </div>
                <asp:Panel ID="pnlIntegrationCode" runat="server" CssClass="form-row">
                    <label class="form-label"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_IntegrationCode %>" /></label>
                    <div class="form-value"><asp:TextBox ID="txtWarehouseCode" runat="server" CssClass="textbox" Width="274" /></div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlCoverageType" CssClass="form-row">
                    <label class="form-label"><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:SiteSettings, Warehouse_ShippingCoverageOption %>" /><span class="req">*</span></label>
                    <div class="form-value">
                        <asp:RadioButtonList ID="rdShippingCoverageOption" runat="server" RepeatDirection="Horizontal"
                            AutoPostBack="true" OnSelectedIndexChanged="rdShippingCoverageOption_SelectedIndexChanged">
                            <asp:ListItem Text="<%$ Resources:SiteSettings, Warehouse_Worldwide %>" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:SiteSettings, Warehouse_RegionCountry %>"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlShippingCoverage" Visible="false" CssClass="form-row">
                    <label class="form-label">&nbsp;</label>
                    <div class="form-value">
                        <div class="tree-view-container">
                            <p>Select and add regions and/or countries</p>
                            <asp:TreeView runat="server" ID="trvSource" CssClass="tree-view" SelectedNodeStyle-CssClass="selected-node"></asp:TreeView>
                        </div>
                        <div class="move-buttons">
                            <asp:Button ID="btnAddCountry" Text="<%$ Resources:SiteSettings, Warehouse_Add %>" runat="server" OnClick="btnAddCountry_Click" 
                                CssClass="gray-right-arrow-button" Width="80" />
                            <br />
                            <asp:Button ID="btnRemoveCountry" Text="<%$ Resources:SiteSettings, Warehouse_Remove %>" runat="server" OnClick="btnRemoveCountry_Click" 
                                CssClass="gray-left-arrow-button" Width="80" />
                        </div>
                        <div class="tree-view-container">
                            <p>Selected shipping coverage</p>
                            <asp:TreeView runat="server" ID="trvDestination" CssClass="tree-view" SelectedNodeStyle-CssClass="selected-node"></asp:TreeView>
                        </div>
                    </div>
                </asp:Panel>
                <asp:ValidationSummary runat="server" ID="vsSummary" EnableClientScript="true" ShowMessageBox="true"
                    ShowSummary="false" DisplayMode="List" ValidationGroup="vGroup" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>