﻿<%@ Page Title="<%$ Resources:SiteSettings, iAPPSCommerceAdministrationManageRestocking %>"
    Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="EditRestocking.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.EditRestocking" StylesheetTheme="General" %>

<%@ Register TagPrefix="SM" TagName="ScriptManager" Src="~/UserControls/General/ScriptManagerControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var popupPath;
        var productSearchPopup;
        var importRestockingPopup;
        function ShowImportRestockingPopup(restockingId) {
            popupPath = "../../Popups/Administration/ImportRestocking.aspx?RestockingId=<%= RestockingId %>";
            importRestockingPopup = dhtmlmodal.open('', 'iframe', popupPath, '', 'width=290px,height=216px,center=1,resize=0,scrolling=1');
            importRestockingPopup.onclose = function () {
                var a = document.getElementById('ImportRestockingPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }
        function CloseImportRestockingPopup(){
            <%=ClientScript.GetPostBackEventReference(btnRefresh, "")%>
        }

        function ShowAddProductPopup() {
            popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx";
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, 'ProductSearchPopup', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
            productSearchPopup.onclose = function () {
                var a = document.getElementById('ProductSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }

            return false;
        }
        function CloseProductPopup(arrSelected) {
            productSearchPopup.hide();
            if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                var skuIds = '';
                while (arrSelected.length != 0) {
                    skuIds += ',' + arrSelected.pop()[1];
                }
                if (skuIds != '') {

                    skuIds = skuIds.substring("1");
                    skuIds = skuIds.replace(/,,/g, ",");

                    AddSkus(skuIds);
                }
                else {
                    alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, ThereIsNoSkuToAdd %>"/>');
                }
            }
        }

        function AddSkus(skuIds) {
            if (skuIds != null && skuIds != '') {
                var restockingId = document.getElementById("<%= hdnRestockingId.ClientID %>");
                if (restockingId.value != "") {
                    AddSKuToRestocking(restockingId.value, skuIds);
                    <%=ClientScript.GetPostBackEventReference(btnRefresh, "")%>
                }
            }
            else {
                alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, ThereIsNoSkuToAdd %>"/>');
            }
        }

        function popUpExpectedDateCalendar() {
            var thisDate = iAppsCurrentLocalDate;
            if (document.getElementById("<%=hdnExpectedDate.ClientID%>").value != "") {
                thisDate = new Date(document.getElementById("<%=hdnExpectedDate.ClientID%>").value);
                expectedDateCalendar.setSelectedDate(thisDate);
            }

            if (!(expectedDateCalendar.get_popUpShowing()));
                expectedDateCalendar.show();
        }

        function expectedDateCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = expectedDateCalendar.getSelectedDate();
            document.getElementById("<%=txtExpectedDate.ClientID%>").value = expectedDateCalendar.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            document.getElementById("<%=hdnExpectedDate.ClientID%>").value = expectedDateCalendar.formatDate(selectedDate, calendarDateFormat);
        }
        //function to change the css of the parent tr
        function fn_ChangeParentCssClass(objCheckbox) {
            var parentRow = $(objCheckbox).parents('tr');
            if (objCheckbox.checked) {
                $(parentRow).addClass('selected-row');
            }
            else {
                $(parentRow).removeClass('selected-row');
            }
        }
        //function to select all the checkboxes which are enabled
        function fn_CheckAllCheckboxes(status) {
            $('#gridview input[type=checkbox]').each(function () {
                if ($(this).attr("disabled") != "disabled") {
                    $(this).attr('checked', status);
                    fn_ChangeParentCssClass(this);
                }
            });
        }

        function ShowAdditionalInfo()
        {
            $(".additionalInfo").show();
            $("#popupOverlay").show();
            return false;
        }

        function HideAdditionalInfo()
        {
            $(".additionalInfo").hide();
            $("#popupOverlay").hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <SM:ScriptManager ID="sManager" runat="server" />
    <asp:HiddenField ID="hdnRestockingId" runat="server" />
    <div class="restocking restocking-details">
        <div class="page-header clear-fix">
            <h1>
                <%= SiteSettings.RestockingDetails_DeliveryDetails %></h1>
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_Save %>"
                OnClick="btnSave_Click" ValidationGroup="valRestocking" CssClass="primarybutton" />
            <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="button"
                Visible="false" Text="<%$ Resources:SiteSettings, RestockingDetails_Delete %>"
                OnClick="lbtnDelete_Click" />
        </div>
        <hr />
        <asp:ValidationSummary ID="valRestocking" runat="server" ShowSummary="false" ShowMessageBox="true"
            ValidationGroup="valRestocking" />
        <asp:UpdatePanel ID="upRestocking" runat="server">
            <ContentTemplate>
                <asp:PlaceHolder runat="server" ID="phEditMode">
                    <div class="column">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_PurchaseOrderNumber %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtName" runat="server" Width="274" CssClass="textbox" />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                                    CultureInvariantValues="true" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterRestockingName %>" Text="*" ValidationGroup="valRestocking" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_DeliveryDate %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value calendar-value">
                                <asp:TextBox ID="txtExpectedDate" runat="server" CssClass="textbox" Width="262" />
                                <span>
                                    <asp:Image ID="calendar_button" ClientIDMode="Static" runat="server" AlternateText="<%$ Resources:SiteSettings, ShowCalendar %>"
                                        ToolTip="<%$ Resources:SiteSettings, ShowCalendar %>" CssClass="buttonCalendar"
                                        ImageUrl="~/App_Themes/General/images/calendar-button.png" />
                                </span>
                                <asp:HiddenField ID="hdnExpectedDate" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvtxtExpectedDate" runat="server" ControlToValidate="txtExpectedDate"
                                    CultureInvariantValues="true" ErrorMessage="<%$ Resources:SiteSettings, PleaseSelectRestockingDate %>" Text="*" ValidationGroup="valRestocking" />
                                <asp:CompareValidator CultureInvariantValues="true" ID="cvExpectedDate" runat="server"
                                    ControlToValidate="txtExpectedDate" Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:SiteSettings, EnterValidDate %>"
                                    Display="None" Text="*" ValidationGroup="valRestocking"></asp:CompareValidator>
                                <asp:CompareValidator ID="cvExpectedDateLater" runat="server" ControlToValidate="txtExpectedDate"
                                    Operator="GreaterThan" Type="Date" CultureInvariantValues="true" Display="None"
                                    ValidationGroup="valRestocking1" Text="*" ErrorMessage="<%$ Resources:SiteSettings, ExpectedDateShouldNotBeLessThanToday %>">
                                </asp:CompareValidator>
                                <ComponentArt:Calendar runat="server" SkinID="Default" ID="expectedDateCalendar"
                                    MaxDate="9998-12-31" PickerFormat="Custom" PopUpExpandControlId="calendar_button">
                                    <ClientEvents>
                                        <SelectionChanged EventHandler="expectedDateCalendar_onSelectionChanged" />
                                    </ClientEvents>
                                </ComponentArt:Calendar>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_ReceivingWarehouse %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="selectbox" Width="283" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_RestockingCode %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtRestockingCode" runat="server" CssClass="textbox" Width="274" />
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phStatus" runat="server">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_Status %>" /></label>
                                <div class="form-value">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectbox" Width="283" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <div class="form-row">
                            <label class="form-label" style="vertical-align: top; padding-top: 3px;">
                                <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_Comments %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtComments" runat="server" CssClass="textbox" Width="275" Rows="5"
                                    TextMode="MultiLine" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="phViewMode" Visible="false">
                    <div class="second-column">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_PurchaseOrderNumber %>" /></label>
                            <div class="form-value">
                                <asp:Literal runat="server" ID="ltName" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_DeliveryDate %>" /></label>
                            <div class="form-value">
                                <asp:Literal runat="server" ID="ltExpectedDate" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_ReceivingWarehouse %>" /></label>
                            <div class="form-value">
                                <asp:Literal runat="server" ID="ltReceivingWarehouse" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_RestockingCode %>" /></label>
                            <div class="form-value">
                                <asp:Literal runat="server" ID="ltRestockingCode" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_Status %>" /></label>
                            <div class="form-value">
                                <asp:Literal runat="server" ID="ltStatus" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_Comments %>" /></label>
                            <div class="form-value">
                                <asp:Literal runat="server" ID="ltComments" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div class="clearFix">
                </div>
                <div class="restocking-items">
                    <asp:PlaceHolder ID="phRestockingItemHeader" runat="server">
                        <div class="page-header">
                            <h2>
                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_ProductsInThisDelivery %>" /></h2>
                            <asp:LinkButton ID="lbtnExport" runat="server" CssClass="button"
                                Visible="false" Text="<%$ Resources:SiteSettings, RestockingDetails_Export %>"
                                OnClientClick="return ShowAdditionalInfo();" OnClick="lbtnExport_Click" />
                            <div class="clear-fix">
                            </div>
                        </div>
                        <hr />
                        <div class="additionalInfo column" style="display: none;">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_ShipmentNumber %>" /><span
                                        class="req">*</span></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtShipmentNumber" runat="server" Width="274" CssClass="textbox" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                        CultureInvariantValues="true" ErrorMessage="Enter a name" Text="*" ValidationGroup="valAdditional" />
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_ShippedDate %>" /><span
                                        class="req">*</span></label>
                                <div class="form-value calendar-value">
                                    <asp:TextBox ID="txtShippedDate" runat="server" CssClass="textbox" Width="262" />
                                    <span>
                                        <asp:Image ID="Image1" ClientIDMode="Static" runat="server" AlternateText="<%$ Resources:SiteSettings, ShowCalendar %>"
                                            ToolTip="<%$ Resources:SiteSettings, ShowCalendar %>" CssClass="buttonCalendar"
                                            ImageUrl="~/App_Themes/General/images/calendar-button.png" />
                                    </span>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator1" runat="server"
                                        ControlToValidate="txtExpectedDate" Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:SiteSettings, EnterValidDate %>"
                                        Display="None" Text="*" ValidationGroup="valAdditional"></asp:CompareValidator>
                                    <ComponentArt:Calendar runat="server" SkinID="Default" ID="Calendar1" MaxDate="9998-12-31"
                                        PickerFormat="Custom" PopUpExpandControlId="calendar_button">
                                        <ClientEvents>
                                            <SelectionChanged EventHandler="expectedDateCalendar_onSelectionChanged" />
                                        </ClientEvents>
                                    </ComponentArt:Calendar>
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:SiteSettings, RestockingDetails_SchArrivalDate %>" /><span
                                        class="req">*</span></label>
                                <div class="form-value calendar-value">
                                    <asp:TextBox ID="txtSchArrivalDate" runat="server" CssClass="textbox" Width="262" />
                                    <span>
                                        <asp:Image ID="Image2" ClientIDMode="Static" runat="server" AlternateText="<%$ Resources:SiteSettings, ShowCalendar %>"
                                            ToolTip="<%$ Resources:SiteSettings, ShowCalendar %>" CssClass="buttonCalendar"
                                            ImageUrl="~/App_Themes/General/images/calendar-button.png" />
                                    </span>
                                    <asp:HiddenField ID="HiddenField2" runat="server" />
                                    <asp:CompareValidator CultureInvariantValues="true" ID="CompareValidator3" runat="server"
                                        ControlToValidate="txtExpectedDate" Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:SiteSettings, EnterValidDate %>"
                                        Display="None" Text="*" ValidationGroup="valAdditional"></asp:CompareValidator>
                                    <ComponentArt:Calendar runat="server" SkinID="Default" ID="Calendar2" MaxDate="9998-12-31"
                                        PickerFormat="Custom" PopUpExpandControlId="calendar_button">
                                        <ClientEvents>
                                            <SelectionChanged EventHandler="expectedDateCalendar_onSelectionChanged" />
                                        </ClientEvents>
                                    </ComponentArt:Calendar>
                                </div>
                            </div>
                            <div class="form-footer">
                                <asp:ValidationSummary runat="server" ID="vsvalAdditional" ValidationGroup="valAdditional"
                                    ShowMessageBox="true" ShowSummary="false" />
                                <a href="#" onclick="return HideAdditionalInfo();" class="button">
                                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:SiteSettings, Cancel %>" /></a>
                                <asp:LinkButton ID="lbtnSubmit" runat="server" Text="<%$ Resources:SiteSettings, Submit %>"
                                    CssClass="primarybutton" OnClick="lbtnSubmit_Click" ValidationGroup="valAdditional" />
                            </div>
                        </div>
                        <div class="instruction">
                            <p>
                                <asp:Literal runat="server" ID="ltAddSkuMessage" /></p>
                            <asp:HyperLink ID="hplAddSku" runat="server" CssClass="small-button" Text="<%$ Resources:SiteSettings, Restocking_AddSku %>"
                                NavigateUrl="#" />
                            <asp:HyperLink ID="hplImport" runat="server" CssClass="small-button" Text="<%$ Resources:SiteSettings, Restocking_Import %>"
                                NavigateUrl="#" />
                            <div class="clear-fix">
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phRestockingItems" runat="server">
                        <div class="grid-utility">
                            <asp:PlaceHolder ID="phGridAction" runat="server">
                                <div class="action-section">
                                    <asp:CheckBox ID="chkSelectAll" runat="server" onclick="fn_CheckAllCheckboxes(this.checked);" />
                                    <asp:Button ID="btnDelete" runat="server" Text="<%$ Resources:SiteSettings, Restocking_Delete %>"
                                        OnClick="btnDelete_Click" CausesValidation="true" CssClass="small-button" />
                                </div>
                            </asp:PlaceHolder>
                            <div class="filter-section">
                                <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" Width="140" />
                                <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:SiteSettings, Restocking_Search %>"
                                    OnClick="btnSearch_Click" CssClass="small-button" />
                            </div>
                            <div class="clearFix">
                            </div>
                        </div>
                        <asp:ObjectDataSource ID="odsRestocking" runat="server" EnablePaging="true" EnableViewState="true"
                            EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.Commerce.Web.EditRestocking" OnSelecting="odsRestocking_Selecting"
                            OnDeleted="odsRestocking_Deleted" MaximumRowsParameterName="pageSize" StartRowIndexParameterName="rowIndex"
                            SortParameterName="sortExpression" SelectCountMethod="GetRecordCount" SelectMethod="GetRecords" />
                        <asp:ListView ID="lstRestocking" runat="server" ItemPlaceholderID="itemContainer"
                            OnLayoutCreated="lstRestocking_LayoutCreated" DataSourceID="odsRestocking" DataKeyNames="Id,RestockingSkuId,IsLocked"
                            OnItemDataBound="lstRestocking_ItemDataBound">
                            <LayoutTemplate>
                                <table width="100%" cellpadding="0" cellspacing="0" class="grid-view" id="gridview">
                                    <thead>
                                        <tr>
                                            <th>
                                                &nbsp;
                                            </th>
                                            <th>
                                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_ProductName %>" />
                                            </th>
                                            <th>
                                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_SKU %>" />
                                            </th>
                                            <th>
                                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_ExpectedQty %>" />
                                            </th>
                                            <th runat="server" id="thReceivedQty">
                                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_ReceivedQty %>" />
                                            </th>
                                            <th>
                                                <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Restocking_ItemComments %>" />
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:PlaceHolder ID="itemContainer" runat="server" />
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6" align="right">
                                                <asp:DataPager ID="dpRestocking" runat="server" PageSize="10">
                                                    <Fields>
                                                        <asp:TemplatePagerField>
                                                            <PagerTemplate>
                                                                <div class="pager">
                                                                    <%# string.Format(GUIStrings.ListViewPagerTemplateFormat, Container.StartRowIndex + 1, (Container.StartRowIndex + Container.PageSize) > Container.TotalRowCount ? Container.TotalRowCount : (Container.StartRowIndex + Container.PageSize), Container.TotalRowCount)%>
                                                                </div>
                                                            </PagerTemplate>
                                                        </asp:TemplatePagerField>
                                                        <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="false" ShowLastPageButton="false"
                                                            NextPageText="" PreviousPageText="" RenderDisabledButtonsAsLabels="true" NextPageImageUrl="~/App_Themes/General/images/next-page.png"
                                                            RenderNonBreakingSpacesBetweenControls="false" PreviousPageImageUrl="~/App_Themes/General/images/previous-page.png" />
                                                    </Fields>
                                                </asp:DataPager>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
                                    <td>
                                        <asp:CheckBox ID="chkSelect" runat="server" onclick="fn_ChangeParentCssClass(this);" />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltProductTitle" runat="server" Text='<%# Eval("ProductTitle") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltSKU" runat="server" Text='<%# Eval("SKU") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtExpectedQty" runat="server" Text='<%# Eval("ExpectedQuantity") %>'
                                            Width="90" />
                                        <asp:Literal ID="ltExpectedQty" runat="server" Visible="false" Text='<%# Eval("ExpectedQuantity") %>' />
                                        <asp:RequiredFieldValidator ID="rfvExpectedQty" runat="server" ControlToValidate="txtExpectedQty"
                                            ErrorMessage="Expected quantity is required." Text="*" ValidationGroup="valRestocking" />
                                        <asp:CompareValidator ID="cvExpectedQty" ControlToValidate="txtExpectedQty" runat="server"
                                            ValidationGroup="valRestocking" Operator="DataTypeCheck" Type="Double" CultureInvariantValues="true"
                                            ErrorMessage="Enter valid expected quantity" Text="*"></asp:CompareValidator>
                                        <asp:CompareValidator ID="cvExpectedQty1" ControlToValidate="txtExpectedQty" runat="server"
                                            ValidationGroup="valRestocking" CultureInvariantValues="true" Operator="GreaterThan"
                                            ValueToCompare="0" ErrorMessage="Enter valid expected quantity" Text="*"></asp:CompareValidator>
                                    </td>
                                    <td runat="server" id="tdReceivedQty">
                                        <asp:TextBox ID="txtReceivedQty" runat="server" Text='<%# Eval("ReceivedQuantity") %>'
                                            Width="90" />
                                        <asp:Literal ID="ltReceivedQty" runat="server" Visible="false" Text='<%# Eval("ReceivedQuantity") %>' />
                                        <asp:CompareValidator ID="cvReceivedQty" ControlToValidate="txtReceivedQty" runat="server"
                                            ValidationGroup="valRestocking" Operator="DataTypeCheck" Type="Double" CultureInvariantValues="true"
                                            ErrorMessage="Enter valid received quantity" Text="*"></asp:CompareValidator>
                                        <asp:CompareValidator ID="cvRecivedQty1" ControlToValidate="txtReceivedQty" runat="server"
                                            ValidationGroup="valRestocking" CultureInvariantValues="true" ControlToCompare="txtExpectedQty"
                                            Operator="LessThanEqual" Type="Double" ErrorMessage="Received quantity should be less than or equal to expected quantity"
                                            Text="*"></asp:CompareValidator>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtComments" runat="server" Text='<%# Eval("Comments") %>' Width="120" />
                                        <asp:Literal ID="ltComments" runat="server" Visible="false" Text='<%# Eval("Comments") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </asp:PlaceHolder>
                    <asp:Button ID="btnRefresh" runat="server" Text="<%$ Resources:SiteSettings, Restocking_Refresh %>"
                        OnClick="btnRefresh_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="popupOverlay" id="popupOverlay">
            <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
