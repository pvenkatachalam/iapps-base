﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageWarehouseMessages.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Administration.Warehouse.ManageWarehouseMessages"
    StylesheetTheme="General" MasterPageFile="~/MainMaster.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="mainPlaceHolder" runat="server">
<div class="page-header clear-fix">
    <h1><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, UploadMessages %>" /></h1>
    <asp:Button runat="server" ID="btnUpload" Text="<%$ Resources:SiteSettings, Upload %>" onclick="btnUpload_Click" CssClass="primarybutton" />
</div>
<hr />
<div class="form-row">
    <label class="form-label"><%= SiteSettings.MessageType %></label>
    <div class="form-value">
        <asp:DropDownList runat="server" ID="ddlFileType">
            <asp:ListItem Text="Invoice" Value ="6" />
            <asp:ListItem Text="PIX"  Value ="4" />
            <asp:ListItem Text="Inventory Snapshot"  Value ="5" />
        </asp:DropDownList>
    </div>
</div>
<div class="form-row">
    <label class="form-label"><%= SiteSettings.Source %></label>
    <div class="form-value">
        <asp:FileUpload runat="server" ID="fuFile" />
    </div>
</div>

<hr />
<div class="page-header clear-fix">
    <h1><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, OutboundMessages %>" /></h1>
    <asp:Button runat="server" ID="btnExport" Text="<%$ Resources:SiteSettings, ClickToExportItemMaster %>" onclick="btnExport_Click" CssClass="primarybutton" />
</div>
</asp:Content>