﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true"
    CodeBehind="InvalidateCachePage.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.InvalidateCachePage"
    Title="iAPPS Commerce: Control Center" StylesheetTheme="General" %>

<asp:Content ID="CacheContentHead" ContentPlaceHolderID="head" runat="server">
    <link href="../../css/Administration.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        var ddrivetip1 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, ClearsTheCachedProductDataResultedFromNavigationFilterQueries %>'/>";
        var ddrivetip2 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, RefreshesTheCachedDataOfAllTheNavFacetsProductData %>'/>";
        var ddrivetip3 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, RefreshesTheCachedDataOfAllTheProductsPriceSetData %>'/>";
        var ddrivetip4 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, RepopulatesTheCachedProductDataResultedFromAutoFeatures %>'/>";
        var ddrivetip5 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, ClearsProductNavRewriteRulesProductURLAndProductDataDictionaries %>'/>";
        var ddrivetip6 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, ClearsBaseDataOfCreditCardTypes %>'/>";
        var ddrivetip7 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, ClearsSiteSettingsDataFromDistCache %>'/>";
        var ddrivetip8 = "<asp:localize runat='server' text='<%$ Resources:SiteSettings, ClearsAllTheCache %>'/>";

        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";

        function allowonlynumbers(event) {
            var key = event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode);

            if ((key >= 48 && key <= 57) || key == 8 || key == 127) {
                return true;
            }
            else
            { return false; }
        }

        function ValidateIntervalData() {
            var txtControl = document.getElementById("<%= txtIntervalMinutes.ClientID %>");
            var returnVal;

            if (txtControl != null) {
                if (txtControl.value != null && txtControl.value != '') {
                    if (!txtControl.value.match(/^[0-9]+$/)) {
                        returnVal = false;
                    }
                    else {
                        returnVal = true;
                    }
                }
                else {
                    returnVal = false;
                }
            }

            if (returnVal == false) alert("<asp:localize runat='server' text='<%$ Resources:SiteSettings, PleaseEnterAValidIntervalNumberInMinutes %>'/>");
            return returnVal;
        }
    </script>
</asp:Content>
<asp:Content ID="DashboardContent" ContentPlaceHolderID="mainPlaceHolder" runat="server">
<div class="site-settings">
    <div class="page-header clear-fix">
        <h1 style="margin-top: 0;"><asp:Label ID="subPageHeader" runat="server" Text="<%$ Resources:SiteSettings, CacheManagement %>"></asp:Label></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:SiteSettings, Save %>"
            ToolTip="<%$ Resources:SiteSettings, Save %>" CssClass="primarybutton"
            OnClick="btnSave_Click" OnClientClick="return ValidateIntervalData();" />
    </div>
    <hr />
    <h3><asp:Localize runat="server" Text="<%$ Resources:SiteSettings, Cache %>" /></h3>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, NavigationFilterCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnInvalidateNavFilter" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnInvalidateNavFilter_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip1, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblNavFilterCache" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ProductFacetCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnProductFacetCache" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnProductFacetCache_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip2, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblFacetCache" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, PriceSetCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnPriceSetCache" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnPriceSetCache_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip3, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblPriceSetCache" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, FeatureOutputCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnFeatureOutput" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnFeatureOutput_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip4, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblFeatureOuput" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ApplicationCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnAppCache" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnAppCache_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip5, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblAppCache" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, ApplicationBaseDataCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnAppBaseDataCache" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnAppBaseDataCache_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip6, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblAppBaseDataCache" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, SiteSettingsCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="button" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btn_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip7, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblSiteSettingsCache" runat="server" />)
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:SiteSettings, AllCache %>" /></label>
        <div class="form-value">
            <asp:Button ID="btnRefressAll" runat="server" Text="<%$ Resources:SiteSettings, Refresh %>"
                OnClick="btnRefressAll_Click" CssClass="button" onmouseover="ddrivetip(ddrivetip8, 300,'');positiontip(event);"
                onmouseout="hideddrivetip();" />
            (<asp:Label ID="lblAllCache" runat="server" />)
        </div>
    </div>
    <h3><asp:Localize runat="server" Text="<%$ Resources:SiteSettings, CacheRefreshSchedule %>" /></h3>
    <div class="form-row">
    <label class="form-label"><asp:Label runat="server" Text="<%$ Resources:SiteSettings, ScheduleInMinutes %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtIntervalMinutes" runat="server" CssClass="textBoxes" Width="50"
                onkeypress="return allowonlynumbers(event);" MaxLength="4"></asp:TextBox>
        </div>
    </div>
</div>
</asp:Content>