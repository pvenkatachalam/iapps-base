﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageWorkflows.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageWorkflows"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdWorkflows_OnLoad(sender, eventArgs) {
            $(".manage-workflows").gridActions({ objList: grdWorkflows });
        }

        function grdWorkflows_OnRenderComplete(sender, eventArgs) {
            $(".manage-workflows").gridActions("bindControls");
        }


        function grdWorkflows_OnCallbackComplete(sender, eventArgs) {
            $(".manage-workflows").gridActions("displayMessage");
        }

        function Grid_ItemSelect(sender, eventArgs) {
            var siteId = eventArgs.selectedItem.getMember("ApplicationId").get_value();

            if (jAppId != siteId) {
                sender.getItemByCommand("Edit").hideButton();
                sender.getItemByCommand("Delete").hideButton();
            }
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;
            switch (gridActionsJson.Action) {
                case "ViewPages":
                    doCallback = false;
                    OpeniAppsAdminPopup("ViewWorkflowPages", "workflowId=" + selectedItemId);
                    break;
                case "View":
                    doCallback = false;
                    OpeniAppsAdminPopup("ViewWorkflow", "workflowId=" + selectedItemId);
                    break;
                case "Add":
                    doCallback = false;
                    window.location = "ManageWorkflowDetails.aspx";
                    break;
                case "Edit":
                    doCallback = false;
                    window.location = "ManageWorkflowDetails.aspx?Id=" + selectedItemId;
                    break;
                case "Delete":
                    doCallback = confirm("<%= JSMessages.DeleteWorkflow %>");
                    break;
                case "Assign":
                    doCallback = false;
                    window.location = "AssignWorkflows.aspx?Id=" + selectedItemId;
                    break;
            }

            if (doCallback) {
                grdWorkflows.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdWorkflows.callback();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manage-workflows">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdWorkflows" SkinID="Default" runat="server" Width="100%"
            RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdWorkflowsLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="WorkflowId" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="WorkflowName" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="SiteName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Sequence" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="WorkflowId" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="IsGlobal" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="IsShared" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ApplicationId" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <%--<div class="first-cell">
                                <%# GetGridItemRowNumber(Container.DataItem) %>
                            </div>--%>
                            <div class="large-cell">
                                <h4><%# Container.DataItem["SiteName"] %> :
                                    <%# Container.DataItem["WorkflowName"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["Sequence"]%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# CurrentProduct == Bridgeline.FW.Global.Security.ProductEnum.CMS ? 
                                    String.Format("<strong>{0}</strong> : {1} ", GUIStrings.Type, Container.DataItem["IsGlobal"].ToString().ToLower() == "true" ? GUIStrings.Global : GUIStrings.Menu) : String.Empty%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdWorkflowsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdWorkflows) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <RenderComplete EventHandler="grdWorkflows_OnRenderComplete" />
                <Load EventHandler="grdWorkflows_OnLoad" />
                <CallbackComplete EventHandler="grdWorkflows_OnCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
    </div>
</asp:Content>
