﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsManagePriceSets %>"
    Language="C#" MasterPageFile="~/StoreSetup/StoreSetupMaster.master" AutoEventWireup="true"
    CodeBehind="ManagePriceSets.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManagePriceSets"
    StylesheetTheme="General" %>

<asp:Content ID="header" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <script type="text/javascript">
        var typeColumn = 3;
        var startDateColumn = 1;
        var endDateColumn = 2;
        var gridItem;
        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdPriceSet;
                menuObject = mnuPriceSets;
                gridObjectName = "grdPriceSet";
                uniqueIdColumn = 8;
                operationColumn = 7;
                selectedTreeNodeId = emptyGuid;
            }, 500);
        }

        function PageLevelOnContextMenu(menuItems) {
            var priceSetType = gridItem.getMember(typeColumn).get_text();
            if (priceSetType == '') {
                menuItems.getItem(0).set_visible(true);
                for (i = 1; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(false);
                }
            }
            else if (priceSetType == 'Manual') //Manual
            {

                menuItems.getItem(5).set_visible(false);
                menuItems.getItem(6).set_visible(false);
                menuItems.getItem(7).set_visible(false);
                menuItems.getItem(8).set_visible(false);

                menuItems.getItem(9).set_visible(true);
                menuItems.getItem(10).set_visible(true);
            }
            else {
                menuItems.getItem(9).set_visible(false);
                menuItems.getItem(10).set_visible(false);

                menuItems.getItem(5).set_visible(true);
                menuItems.getItem(6).set_visible(true);
                menuItems.getItem(7).set_visible(true);
                menuItems.getItem(8).set_visible(true);
            }
            return true;
        }

        function PageLevelOnItemSelect(selectedMenuId, currentGridItem, sender, eventArgs) {
            if (selectedMenuId == 'cmManageDiscount') {
                ShowDisountPopup(currentGridItem);
            }
            else if (selectedMenuId == 'cmManageProductTypeAndSku') {
                ShowProductTypePopup(currentGridItem);
            }
            else if (selectedMenuId == 'cmManageSKU') {
                ShowManageSkuPopup(currentGridItem);
            }
        }

        function PageLevelAddItem(currentGridItem) {
            grdPriceSet.get_table().get_columns()[typeColumn].set_allowEditing(true);

            var toDay = iAppsCurrentLocalDate;
            pckStartDate.setSelectedDate(toDay);

            //        var modifiedDate = new Date(9999,12, 31);
            //        pckEndDate.setSelectedDate(modifiedDate);
            //alert(pckEndDate.get_maxDate());
            //pckEndDate.setSelectedDate(pckEndDate.get_maxDate());
            grdPriceSet.render();
            return true;
        }

        function PageLevelEditItem(currentGridItem) {
            grdPriceSet.get_table().get_columns()[typeColumn].set_allowEditing(false);
            grdPriceSet.render();
            return true;
        }

        function PageLevelDeleteItem(currentGridItem) {
            //Call Callback method to delete.. 
            var priceSetId = gridItem.getMember(uniqueIdColumn).get_text();
            var isResult = DeletePriceSet(priceSetId);

            if (isResult.toLowerCase() == 'true') {
                if (grdPriceSet.get_recordCount() > grdPriceSet.get_pageSize()) {
                    if (grdPriceSet.get_recordCount() % grdPriceSet.get_pageSize() == 1) {
                        grdPriceSet.previousPage();
                    }
                }
            }
            return isResult;
        }

        function PageLevelSaveRecord() {
            return true;
        }

        function PageLevelCancelEdit() {
            return true;
        }

        function PageLevelGridValidation(currentGridItem) {
            grid = currentGridItem;
            var isValid = true;
            var thisName = Trim(document.getElementById(txtName).value);
            var msg = "";

            if (Trim(thisName) == "") {
                msg += "<%= GUIStrings.NameShouldNotBeEmpty %>";
            }
            else {
                msg += GridLevelSpecialCharacterChecking(thisName, 'Name');
            }

            //else if (!thisName.match(/^[a-zA-Z0-9. _'/-<>.&$!|#:,]+$/))
            //{
            //    if (IsNameAscii(thisName)!='false')
            //    {
            //       msg += "Name can have only alphanumeric characters and '.','&','!','|','#',':',',','’', '_','<','>'. Please re-enter. . \n";            
            //    }
            // }
            // if(Trim(thisName) =='&#0;')
            // {
            //     msg += "'&#0;' is unicode combination of new line character, it is not allowed in name. Please re-enter. \n"; 
            // }

            var len = gridObject.Data.length;
            var gItem;
            var nameOth;
            for (var i = 0; i < len; i++) {
                if (i != gridItem.Index) {
                    gItem = gridObject.get_table().getRow(i);
                    nameOth = gItem.getMember(0).get_value();
                    if (thisName.toLowerCase() == nameOth.toLowerCase()) {
                        msg += "<%= GUIStrings.DuplicateNameExisting %>";
                    }
                }
            }
            if (pckStartDate.getSelectedDate() == '' || pckStartDate.getSelectedDate() == null) {
                msg += '<%= GUIStrings.StartDateShouldNotBeEmpty %>';
            }
            if (gridItem.getMember(uniqueIdColumn).get_value() == '' || gridItem.getMember(uniqueIdColumn).get_value() == null || gridItem.getMember(uniqueIdColumn).get_value() == emptyGuid) {
                var startDate = new Date(pckStartDate.getSelectedDate());
                var toDay = iAppsCurrentLocalDate;

                var startDateMil = startDate.getTime();
                var toDayMil = toDay.getTime();

                if (startDateMil < toDayMil) {
                    //sometime date may be same but time makes problem.
                    if (startDate.getFullYear() < toDay.getFullYear()) {
                        msg += '<%= GUIStrings.StartDateShouldNotBeLessThanToday %>';
                    }
                    else if (startDate.getMonth() < toDay.getMonth()) {
                        msg += '<%= GUIStrings.StartDateShouldNotBeLessThanToday %>';
                    }
                    else if (startDate.getDate() < toDay.getDate()) {
                        msg += '<%= GUIStrings.StartDateShouldNotBeLessThanToday %>';
                    }
                }
            }

            if (pckEndDate.getSelectedDate() != null && pckStartDate.getSelectedDate() != null) {
                //pckEndDate.getSelectedDate() < pckStartDate.getSelectedDate()) {

                var startDate = new Date(pckStartDate.getSelectedDate());
                var endDate = new Date(pckEndDate.getSelectedDate());

                var startDateMil = startDate.getTime();
                var endDateMil = endDate.getTime();

                if (startDateMil > endDateMil) {
                    //sometime date may be same but time makes problem.
                    if (startDate.getFullYear() > endDate.getFullYear()) {
                        msg += '<%= GUIStrings.EndShouldBeGreaterThanStartDate %>';
                    }
                    else if (startDate.getMonth() > endDate.getMonth()) {
                        msg += '<%= GUIStrings.EndShouldBeGreaterThanStartDate %>';
                    }
                    else if (startDate.getDate() > endDate.getDate()) {
                        msg += '<<%= GUIStrings.EndShouldBeGreaterThanStartDate %>';
                    }
                }
                //msg += "End should be greater than Start date. \n";
            }

            if (msg != "") {
                alert(msg);
                isValid = false;
            }
            else {
                isValid = true;
            }
            return isValid;
        }

        function PageLevelSaveRecord() { 
             if (gridItem.Data[5].length == undefined || gridItem.Data[5][0]=='')
                 alert("<%= GUIStrings.SelectAGroup %>");
        }
    </script>
    <script type="text/javascript">
        var popupPath;
        var disountPopup;
        var productTypePopup;
        var skuPopup;
        function ShowManageSkuPopup(currentItem) {
            skuPopup = OpeniAppsCommercePopup('ShowManageSkuPopup', 'Id=' + gridItem.getMember(8).get_text());
            skuPopup.onclose = function () {
                var a = document.getElementById('ShowManageSkuPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function ShowProductTypePopup(currentItem) {
            popupPath = '../../Popups/StoreManager/ProductTypesAndSkuPriceSets.aspx?Id=' + gridItem.getMember(8).get_text();
            productTypePopup = dhtmlmodal.open('ShowProductTypePopup', 'iframe', popupPath, '', 'width=975px,height=610px,center=1,resize=0,scrolling=1');
            productTypePopup.onclose = function () {
                var a = document.getElementById('ShowProductTypePopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function ShowDisountPopup(currentItem) {
            popupPath = '../../Popups/StoreManager/DiscountAndRangesPriceSets.aspx?Id=' + gridItem.getMember(8).get_text();
            disountPopup = dhtmlmodal.open('ShowDisountPopup', 'iframe', popupPath, '', 'width=550px,height=410px,center=1,resize=0,scrolling=1');
            disountPopup.onclose = function () {
                var a = document.getElementById('ShowDisountPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function RefreshThisPage() {
            LoadGrid();
        }

        function pckStartDate_OnDateChange() {
            var fromDate = pckStartDate.getSelectedDate();
            calStartDate.setSelectedDate(fromDate);
        }

        function calStartDate_OnChange() {
            var fromDate = calStartDate.getSelectedDate();
            pckStartDate.setSelectedDate(fromDate);
        }

        function pckEndDate_OnDateChange() {
            var fromDate = pckEndDate.getSelectedDate();
            calEndDate.setSelectedDate(fromDate);
        }

        function calEndDate_OnChange() {
            var fromDate = calEndDate.getSelectedDate();
            pckEndDate.setSelectedDate(fromDate);
        }

        function Button1_OnClick(button) {
            if (calStartDate.get_popUpShowing()) {
                calStartDate.hide();
            }
            else {
                var date = pckStartDate.getSelectedDate();
                calStartDate.setSelectedDate(date);
                calStartDate.show(button);
            }
        }

        function Button2_OnClick(button) {


            if (calEndDate.get_popUpShowing()) {
                calEndDate.hide();
            }
            else {

                var data = gridItem.GetMember(endDateColumn).get_object();
                var value = new Date(data);
                if (value == "" || value == null || data == undefined) {
                    var value = new Date(9999, 12, 31);
                }

                var enddate = pckEndDate.getSelectedDate();

                if (pckEndDate.getSelectedDate() == pckEndDate.get_maxDate() || value.getFullYear() == 9999) {
                    startdate = pckStartDate.getSelectedDate();
                    enddate = new Date(startdate);
                }

                if (data != null) {
                    calEndDate.setSelectedDate(enddate);
                }
                else {
                    calEndDate.clearSelectedDate();
                }

                calEndDate.show(button);
            }
        }

        //Calendar Control
        // set value method for setExpression
        function setCalendarValue(control, DataField) {
            //var txtControl = document.getElementById(control);
            var data = gridItem.GetMember(DataField).get_object();
            var psDate = new Date(data);
            if (control == 'StartDate') {
                if (psDate == "" || psDate == null || data == undefined) {
                    var curDate = iAppsCurrentLocalDate;
                    psDate = new Date(curDate);
                }
                pckStartDate.setSelectedDate(psDate);
            }
            else if (control == 'EndDate') {
                if (psDate == "" || psDate == null || data == undefined) {
                }
                else {
                    pckEndDate.setSelectedDate(psDate);
                }
            }
        }
        // get value method for setExpression
        function getCalendarValue(control, DataField) {
            var psDate;
            if (control == "StartDate") {
                psDate = pckStartDate.formatDate(pckStartDate.getSelectedDate(), shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            }
            else if (control == "EndDate") {
                if (pckEndDate.getSelectedDate() != pckEndDate.get_maxDate()) {
                    psDate = pckEndDate.formatDate(pckEndDate.getSelectedDate(), shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
                }
            }
            if ((control == "StartDate") && (psDate == "" || psDate == null || psDate == undefined)) {
                var curDate = iAppsCurrentLocalDate;
                psDate = curDate;
            }
            return [psDate, psDate];
        }

        //dropdown control
        function setDropDownValue(control, DataField) {
            if (grdPriceSet.get_table().get_columns()[typeColumn].get_allowEditing()) {
                var dropDownListControl = document.getElementById(control);
                var selectedItemId = gridItem.GetMember(DataField).Value;
                if (selectedItemId != null) {
                    for (i = 0; i < dropDownListControl.length; i++) {
                        if (dropDownListControl[i].value == selectedItemId) {
                            dropDownListControl[i].selected = true;
                        }
                    }
                }
            }
        }

        function getDropDownValue(control, DataField) {
            if (grdPriceSet.get_table().get_columns()[typeColumn].get_allowEditing()) {
                var dropDownListControl = document.getElementById(control);
                var selectedItemIds = "";
                for (i = 0; i < dropDownListControl.length; i++) {
                    if (dropDownListControl[i].selected) {
                        selectedItemIds = selectedItemIds + dropDownListControl[i].value;
                    }
                }
                return [selectedItemIds, selectedItemIds];
            }
            else {
                return gridItem.GetMember(DataField).Value;
            }
        }

        function DisplayGroupNames(DataItem) {
            return DataItem.GetMember('CustomerGroupTitles').get_text();
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="grid-utility">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Filter %>"
                onclick="return onClickSearchonGrid('txtSearch', grdPriceSet);" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Grid ID="grdPriceSet" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
        PagerInfoClientTemplateId="grdPriceSetPaginationTemplate" AllowEditing="true"
        AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="ProductListingSliderTemplateCached"
        SliderPopupClientTemplateId="ProductListingSliderTemplate" AutoCallBackOnUpdate="false"
        CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" CallbackCachingEnabled="true"
        AllowMultipleSelect="false" EditOnClickSelectedItem="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        LoadingPanelClientTemplateId="grdPriceSetLoadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="280" HeadingText="<%$ Resources:GUIStrings, Name %>"
                        DataField="Title" Align="Left" EditControlType="Custom"
                        IsSearchable="true" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate"
                        EditCellServerTemplateId="NameServerTemplate" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, StartDate %>"
                        DataField="StartDate" IsSearchable="false" Align="Left" EditControlType="Custom"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="StartDateHoverTemplate" EditCellServerTemplateId="StartDateServerTemplate"
                        DataType="System.DateTime" FormatString="d" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, EndDate1 %>"
                        DataField="EndDate" IsSearchable="false" Align="Left" EditControlType="Custom"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="EndHoverTemplate" EditCellServerTemplateId="EndDateServerTemplate"
                        DataType="System.DateTime" FormatString="d" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="Type" IsSearchable="true" Align="Left" EditControlType="Custom" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="TypeHoverTemplate"
                        EditCellServerTemplateId="TypeServerTemplate" />
                    <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, IsSale %>"
                        DataField="IsSale" IsSearchable="false" Align="Left" EditControlType="Custom"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="IsSaleHoverTemplate" EditCellServerTemplateId="IsSaleServerTemplate" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, NumGroups %>"
                        DataField="CustomerGroupCount" IsSearchable="false" Align="Right" EditControlType="Custom"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="GroupsHoverTemplate" EditCellServerTemplateId="GroupsServerTemplate" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, NumSKUs %>"
                        DataField="SkuCount" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="SKUsHoverTemplate"
                        AllowEditing="False" />
                    <ComponentArt:GridColumn Width="50" AllowSorting="false" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                        EditControlType="Custom" Align="Center" FixedWidth="true"
                        AllowReordering="false" EditCellServerTemplateId="svtActions" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="CustomerGroupIds" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="CustomerGroupTitles" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grid_onContextMenu" />
            <Load EventHandler="grid_onLoad" />
        </ClientEvents>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="NameServerTemplate">
                <Template>
                    <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="160"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="StartDateServerTemplate">
                <Template>
                    <div class="calendar-value clear-fix">
                        <ComponentArt:Calendar ID="pckStartDate" runat="server" PickerFormat="Custom" PickerCustomFormat="d" Width="75"
                            ControlType="Picker" PickerCssClass="textBoxes" ClientSideOnSelectionChanged="pckStartDate_OnDateChange" />
                        <span>
                            <img id="calendar_from_button" runat="server" alt="<%$ Resources:GUIStrings, CalendarButton %>"
                                class="buttonCalendar" src="../../App_Themes/General/images/calendar-button.png"
                                onclick="Button1_OnClick(this);" />
                        </span>
                        <ComponentArt:Calendar runat="server" ID="calStartDate" ControlType="Calendar" PopUp="Custom"
                            PopUpExpandControlId="calendar_from_button" ClientSideOnSelectionChanged="calStartDate_OnChange"
                            SkinID="Default">
                        </ComponentArt:Calendar>
                    </div>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="EndDateServerTemplate">
                <Template>
                    <div class="calendar-value clear-fix">
                        <ComponentArt:Calendar ID="pckEndDate" runat="server" PickerFormat="Custom" PickerCustomFormat="MM/dd/yyyy" Width="75"
                            ControlType="Picker" PickerCssClass="textBoxes" ClientSideOnSelectionChanged="pckEndDate_OnDateChange" />
                        <span>
                            <img id="calendar_to_button" runat="server" alt="<%$ Resources:GUIStrings, CalendarButton %>"
                                class="buttonCalendar" src="../../App_Themes/General/images/calendar-button.png"
                                onclick="Button2_OnClick(this);" />
                        </span>
                        <ComponentArt:Calendar runat="server" ID="calEndDate" ControlType="Calendar" PopUp="Custom"
                            PopUpExpandControlId="calendar_to_button" ClientSideOnSelectionChanged="calEndDate_OnChange"
                            SkinID="Default">
                        </ComponentArt:Calendar>
                    </div>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="IsSaleServerTemplate">
                <Template>
                    <asp:CheckBox ID="chkIsSale" runat="server" Text="Sale?" />
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="TypeServerTemplate">
                <Template>
                    <asp:DropDownList ID="ddlType" runat="server" Width="90" Rows="3">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, PDiscount %>" Value="<%$ Resources:GUIStrings, PDiscount %>"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Manual %>" Value="<%$ Resources:GUIStrings, Manual %>"></asp:ListItem>
                    </asp:DropDownList>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="GroupsServerTemplate">
                <Template>
                    <asp:ListBox ID="lstGroups" runat="server" Width="130" Rows="3" SelectionMode="Multiple">
                        <%--<asp:ListItem Text="--" Value="--"></asp:ListItem>--%>
                    </asp:ListBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtActions">
                <Template>
                    <table>
                        <tr>
                            <td>
                                <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                    runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11"
                                    onclick="gridObject_BeforeUpdate();return false;" />
                            </td>
                            <td>
                                <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                    src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StartDateHoverTemplate">
                <span title="## DataItem.GetMember('StartDate').get_text() ##">## DataItem.GetMember('StartDate').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('StartDate').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EndHoverTemplate">
                <span title="## DataItem.GetMember('EndDate').get_text() ##">## DataItem.GetMember('EndDate').get_text()
                    == "" || DataItem.GetMember('EndDate').get_text() == "12/31/9999" || DataItem.GetMember('EndDate').get_text()
                    == "31/12/9999" ? "&nbsp;" : DataItem.GetMember('EndDate').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <span title="## DataItem.GetMember('Type').get_text() ##">## DataItem.GetMember('Type').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Type').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="IsSaleHoverTemplate">
                <span title="## DataItem.GetMember('IsSale').get_text() ##">## DataItem.GetMember('IsSale').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('IsSale').get_text() == "true"? "Yes":"No"
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="GroupsHoverTemplate">
                <span title="## javascript:DisplayGroupNames(DataItem) ##">## DataItem.GetMember('CustomerGroupCount').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('CustomerGroupCount').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SKUsHoverTemplate">
                <span title="## DataItem.GetMember('SkuCount').get_text() ##">## DataItem.GetMember('SkuCount').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('SkuCount').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdPriceSet.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdPriceSet.RecordCount
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdPriceSet.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdPriceSet.RecordCount
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdPriceSetPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdPriceSet), pageCount(grdPriceSet),
                grdPriceSet.RecordCount) ##
            </ComponentArt:ClientTemplate>
             <ComponentArt:ClientTemplate ID="grdPriceSetLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdPriceSet) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuPriceSets" runat="server" SkinID="ContextMenu" ExpandOnClick="true"
        Width="215">
        <ClientEvents>
            <ItemSelect EventHandler="menu_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
