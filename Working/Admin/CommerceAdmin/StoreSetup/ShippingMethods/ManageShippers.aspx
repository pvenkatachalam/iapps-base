﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupManageShippers %>" validateRequest="false"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="ManageShippers.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageShippers" StylesheetTheme="General"
    EnableEventValidation="false" %>

<%@ Register TagPrefix="sm" TagName="ShipperDetail" Src="~/UserControls/StoreSetup/ShippingMethods/ShipperDetails.ascx" %>
<%@ Register TagPrefix="sm" TagName="ShippingMethodDetail" Src="~/UserControls/StoreSetup/ShippingMethods/ShippingMethodDetail.ascx" %>
<%@ Register TagPrefix="sm" TagName="ShippingOptionRates" Src="~/UserControls/StoreSetup/ShippingMethods/ShippingOptions.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <script type="text/javascript">
        var nodeType = '';

        function trvShippers_onNodeSelect(sender, eventArgs) {
            selectedTreeNode = eventArgs.get_node();
            selectedTreeNodeId = eventArgs.get_node().get_id();
            selectedNodeId = selectedTreeNodeId;    // selectedNodeId is using in Tree operations and selectedTreeNodeId in GridOperations and declare in respective location. and both are same

            if (selectedTreeNode.get_depth() == 1) {
                document.getElementById("divShipperDetails").style.display = "";
                document.getElementById("divShippingMethodDetails").style.display = "none";
                SetShipperDetails(selectedTreeNode);
            }
            else if (selectedTreeNode.get_depth() == 2) {
                document.getElementById("divShippingMethodDetails").style.display = "";
                document.getElementById("divShipperDetails").style.display = "none";

                SetShippingOptionDetails(selectedTreeNode);
            }
        }

        function NodeBeforeMoveEvent(sender, eventArgs) {
            var isMovePossible = true;
            var nodeMoved = eventArgs.get_node();
            var parentNode = eventArgs.get_node().get_parentNode();
            var newParentNode = eventArgs.get_newParentNode();


            var treeNodeId = eventArgs.get_node().get_id();
            var oldParentNodeId = eventArgs.get_node().get_parentNode().get_id();
            var parentId = eventArgs.get_newParentNode().get_id();

            var nodeText = nodeMoved.get_text();
            var newParentNodeText = newParentNode.get_text();

            var result;


            if (isMovePossible) {

                if (eventArgs.get_node().get_parentNode().get_id() == eventArgs.get_newParentNode().get_id()) {
                    if (eventArgs.get_node().get_depth() == 1)
                        result = ReorderShipperNode(treeNodeId, eventArgs.get_index() + 1);
                    else if (eventArgs.get_node().get_depth() == 2)
                        result = ReorderShippingMethod(treeNodeId, eventArgs.get_node().get_parentNode().get_id(), eventArgs.get_index() + 1);

                }
                else {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NodesCanOnlyBeSortedWithinTheSameLevel %>' />");
                    isMovePossible = false;
                }
            }

            if (!isMovePossible) {
                eventArgs.set_cancel(true);
            }

            if (result != null) {
                if (result == "False") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ErrorNodesNotReorderedSuccessfully %>' />");
                    eventArgs.set_cancel(true);
                }
            }
        }

        function NodeAfterMoveEvent(sender, eventArgs) {

        }

        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    trvShippers.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CollapseTree %>' />";
                }
                else if (treeHandle.className == 'collapse') {
                    trvShippers.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ExpandTree %>' />";
                }
            }
            return false
        }

        //This has to set first .. to pass the objects to the global script
        function SetTreeLevelGeneralVariablesinPage() {
            setTimeout(function () {
                treeObject = trvShippers;
                if (typeof (mnuShippingOptions) != 'undefined')
                    treeMenuObject = mnuShippingOptions;
                treeObjectName = "trvShippers";
            }, 500);
        }
        //Calling after node select
        function PageLevelOnNodeSelect(sender, eventArgs) {
        }

        //Calling before selecting a node
        function PageLevelOnNodeBeforeSelect(sender, eventArgs) {
            return true;
        }
        //Calling begore displaying context menu
        function PageLevelTreeOnContextMenu(menuItems, indexOfSelectedNode) {
            if (indexOfSelectedNode == 1) {
                menuItems.getItem(0).set_visible(true);     //Add Shipping Option
                menuItems.getItem(1).set_visible(false);    //Break item
                menuItems.getItem(2).set_visible(false);    //Delete Shipping Option 

                return true;
            }
            else if (indexOfSelectedNode == 2) {
                menuItems.getItem(0).set_visible(false);    //Add Shipping Option
                menuItems.getItem(1).set_visible(false);    //Break item
                menuItems.getItem(2).set_visible(true);    //Delete Shipping Option  

                return true;
            }
        }

        function PageLevelTreeOnItemSelect(selectedMenuId, contextDataNode, sender, eventArgs) {
            switch (selectedMenuId) {
                //                case 'cmTreeAddShipper':         
                //                    sender.hide();         
                //                    nodeType ='Shipper';         
                //                    AddTreeNode(contextDataNode, treeObject);                     
                //                    break;         
                //                case 'cmTreeDeleteShipper' :         
                //                    sender.hide();         
                //                    nodeType ='Shipper';         
                //                    RemoveTreeNode(contextDataNode);                     
                //                    break;         
                case 'cmTreeAddSO':
                    sender.hide();
                    nodeType = 'Shipping Option';
                    AddTreeNode(contextDataNode, treeObject);
                    break;

                case 'cmTreeDeleteSO':
                    sender.hide();
                    nodeType = 'Shipping Option';
                    RemoveTreeNode(contextDataNode);
                    break;

            }
        }
        //Calling after adding a empty node.     
        function PageLevelAddEmptyTreeNode(contextMenuData, treeObject, newNode) {
            return true;
        }

        //Calling after click on the remove node context menu
        function PageLevelRemoveTreeNode(contextMenuData) {
            nodeId = contextMenuData.get_id();
            var parentNode = contextMenuData.get_parentNode()
            var isDelete;

            if (nodeType == 'Shipper') {
                isDelete = DeleteShipper(nodeId);
            }
            else if (nodeType = 'Shipping Option') {
                isDelete = DeleteShippingMethod(nodeId);
            }

            if (isDelete == 'true') {
                var depth = parentNode.get_depth();
                isTreeCallback = 'false';
                if (depth == 0) {
                    if (parentNode.get_nodes().get_length() > 0) {
                        if (parentNode.get_nodes().getNode(0).get_id() != nodeId) {
                            var rootChildNode = parentNode.get_nodes().getNode(0);
                            rootChildNode.select();
                        }
                        else {
                            if (parentNode.get_nodes().get_length() > 1) {
                                var rootChildNode = parentNode.get_nodes().getNode(1);
                                rootChildNode.select();
                            }
                        }
                    }

                }
                else {
                    parentNode.select();
                }
            }

            return isDelete;
        }
        //Calling just after starting rename of a node
        function PageLevelRenameTreeNode(contextMenuData) {
            return true;
        }

        //Call after re-loading the Grid
        function PageLevelTreeLoad(sender, eventArgs) {
            return true;
        }

        // call call back method from here, and return the result
        function PageLevelCreateNewNode(newText, parentId, newNode) {
            var result;

            if (nodeType == 'Shipper') {
                result = CreateShipper(newText);
            }
            else if (nodeType == 'Shipping Option') {
                result = CreateShippingMethod(parentId, newText);
                document.getElementById("<%= hdnSelectedNode.ClientID%>").value = result;
            }

            return result;
        }

        //This will call immediately after creating a node. 
        function PageLevelLoadNewNode(selectedNodeId, eventArgs) {

        }

        // call back method from here, and return the result
        function PageLevelEditNode(id, newText, renamedNode) {

        }

        // this will call immediately after renamed of the node.
        function PageLevelLoadEditedNode(selectedNodeId, sender, eventArgs) {

        }
        // Calling after refresh the grid in callback mode
        function PageLevelRefreshTreeImmediate() {
            return null;
        }

        function PageLevelValidateNode(newText) {
            var returnVal = true;

            if (newText.match(/[&\<\>]/)) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheName %>' />");
                returnVal = false;
            }

            return returnVal;
        }
      
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="shipping-method">
        <div class="left-column">
            <div class="tree-header">
                <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
            </div>
            <div class="tree-container">
                <ComponentArt:TreeView ID="trvShippers" runat="server" SkinID="Default" DragAndDropEnabled="true"
                    DropRootEnabled="false" DropSiblingEnabled="true">
                    <ClientEvents>
                        <NodeBeforeMove EventHandler="NodeBeforeMoveEvent" />
                        <NodeMove EventHandler="NodeAfterMoveEvent" />
                        <ContextMenu EventHandler="tree_onContextMenu" />
                        <NodeBeforeRename EventHandler="tree_BeforeRename" />
                        <NodeBeforeSelect EventHandler="tree_onNodeBeforeSelect" />
                        <Load EventHandler="tree_Load" />
                    </ClientEvents>
                </ComponentArt:TreeView>
            </div>
            <asp:HiddenField ID="hdnSelectedNode" runat="server" />
            <ComponentArt:Menu ID="mnuShippingOptions" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem ID="cmTreeAddSO" Text="<%$ Resources:GUIStrings, AddShippingMethod %>"
                        Look-LeftIconUrl="cm-icon-add.png">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="cmTreeDeleteSO" Text="<%$ Resources:GUIStrings, DeleteShippingMethod %>"
                        Look-LeftIconUrl="cm-icon-delete.png">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="treeMenu_onItemClick" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>
        <div id="divShipperDetails" class="right-column" style="display: none;">
            <ComponentArt:TabStrip ID="tbShipperDetails" runat="server" SkinID="Default">
                <Tabs>
                    <ComponentArt:TabStripTab ID="TabStripTab1" runat="server" Text="<%$ Resources:GUIStrings, ShipperDetails %>">
                    </ComponentArt:TabStripTab>
                </Tabs>
            </ComponentArt:TabStrip>
            <div class="tab-content">
                <sm:ShipperDetail ID="ctlShipperDetails" runat="server" />
            </div>
        </div>
        <div id="divShippingMethodDetails" class="right-column" style="display: none;">
            <ComponentArt:TabStrip ID="TabStrip1" runat="server" SkinID="Default" MultiPageId="mltpNavigation">
                <Tabs>
                    <ComponentArt:TabStripTab ID="tabShippingMethodDetails" runat="server" Text="<%$ Resources:GUIStrings, Details %>">
                    </ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab ID="tabShippingOptionRates" runat="server" Text="<%$ Resources:GUIStrings, ShippingOptionRates %>">
                    </ComponentArt:TabStripTab>
                </Tabs>
            </ComponentArt:TabStrip>
            <div class="tab-content">
                <ComponentArt:MultiPage ID="mltpNavigation" runat="server" Width="100%">
                    <ComponentArt:PageView ID="pvShippingMethodDetails" runat="server">
                        <sm:ShippingMethodDetail ID="ShippingMethodDetail1" runat="server" />
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pvShippingOptionRates" runat="server">
                        <sm:ShippingOptionRates ID="ShippingOptionRates1" runat="server" />
                    </ComponentArt:PageView>
                </ComponentArt:MultiPage>
            </div>
        </div>
    </div>
</asp:Content>
