﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupManageShippingContainers %>"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="ManageShippingContainers.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageShippingContainers"
    StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <script type="text/javascript">
        var gridItem;
        var shippingContainerId;
        var shippingContainerStatus;
        var shippingContainerIdColumn = 11;
        var shippingContainerStatusColumn = 10;
        var shippingContainerPopup;

        function grdShippingContainers_onContextMenu(sender, eventArgs) {
            gridItem = eventArgs.get_item();
            grdShippingContainers.select(gridItem);
            var evt = eventArgs.get_event();

            menuItems = mnuShippingContainers.get_items();
            if (gridItem.getMember(shippingContainerStatusColumn).get_text() == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, Active %>' />" ||
                gridItem.getMember(shippingContainerStatusColumn).get_text() == '1') {
                menuItems.getItem(6).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MakeShippingContainerInactive %>' />");
            }
            else {
                menuItems.getItem(6).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MakeShippingContainerActive %>' />");
            }

            var uniqueId = gridItem.getMember(shippingContainerIdColumn).get_text();
            if (uniqueId != '' && uniqueId != emptyGuid) //only add
            {
                for (i = 0; i < menuItems.get_length() ; i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else {
                for (i = 0; i < menuItems.get_length() ; i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }

            mnuShippingContainers.showContextMenuAtEvent(evt);
        }

        function mnuShippingContainers_OnItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        shippingContainerId = emptyGuid;
                        ShowShippingContainer(shippingContainerId);
                        break;
                    case 'cmEdit':
                        shippingContainerId = gridItem.getMember(shippingContainerIdColumn).get_text();
                        ShowShippingContainer(shippingContainerId);
                        break;
                    case 'cmActive':
                        ChangeStatus(gridItem);
                        break;
                    case 'cmDelete':
                        if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteTheSelectedItem %>' />")) {
                            DeleteShippingContainer(gridItem);
                        }
                        break;
                }
            }
        }

        function DeleteShippingContainer(currentItem) {
            shippingContainerId = currentItem.getMember(shippingContainerIdColumn).get_text();
            //-- Call call back method to delete the ShippingContainer
            var strStatus = DeleteShippingContainerCB(shippingContainerId);
            if (strStatus.toLowerCase() == 'true') {
                if (grdShippingContainers.get_recordCount() > grdShippingContainers.get_pageSize()) {
                    if (grdShippingContainers.get_recordCount() % grdShippingContainers.get_pageSize() == 1) {
                        grdShippingContainers.previousPage();
                    }
                }
                //-- Call Grid Reload here
                ShowStatusWise();
            }
            else {
                alert(strStatus);
            }
        }

        function ShowShippingContainer(shippingContainerId) {
            window.location = "AddShippingContainer.aspx?shippingContainerId=" + shippingContainerId;
        }

        function ChangeStatus(currentItem) {
            var currentStatus = currentItem.getMember(shippingContainerStatusColumn).get_text();
            shippingContainerId = currentItem.getMember(shippingContainerIdColumn).get_text();
            //-- Call call back method to update status
            var strStatus = ChangeShippingContainerStatus(shippingContainerId, currentStatus);
            if (strStatus.toLowerCase() == 'true') {
                //-- Call Grid Reload here
                ShowStatusWise();
            }
            else {
                alert(strStatus);
            }
        }

        function SetArchivedStatus(chkStatusObject) {
            window["ShowArchived"] = chkStatusObject;
            ShowStatusWise();
        }

        function ShowStatusWise() {
            var actionColumnValue = '';
            var chkArchivedStatusObject = window["ShowArchived"];

            if (chkArchivedStatusObject != null && chkArchivedStatusObject.checked) {
                actionColumnValue = 'ShowArchived';
            }
            else {
                actionColumnValue = 'ShowActive';
            }
            LoadShippingContainers(actionColumnValue);
        }

        function LoadShippingContainers(actionColumnValue) {
            grdShippingContainers.set_callbackParameter(actionColumnValue);
            grdShippingContainers.callback();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="grid-utility clear-fix">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Filter %>"
                onclick="return onClickSearchonGrid('txtSearch', grdShippingContainers);" />
        </div>
        <div class="columns-right">
            <asp:CheckBox ID="chkShowArchived" runat="server" Text="<%$ Resources:GUIStrings, ShowInactiveShippingContainers %>"
                onclick="javascript:SetArchivedStatus(this);" />
        </div>
    </div>
    <ComponentArt:Menu ID="mnuShippingContainers" runat="server" SkinID="ContextMenu">
        <ClientEvents>
            <ItemSelect EventHandler="mnuShippingContainers_OnItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid ID="grdShippingContainers" SkinID="Default" runat="server" Width="100%"
        RunningMode="Callback" PagerInfoClientTemplateId="ShippingContainersPaginationTemplate"
        AllowEditing="true" AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="ShippingContainersSliderTemplateCached"
        SliderPopupClientTemplateId="ShippingContainersSliderTemplate" AutoCallBackOnUpdate="false"
        CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" CallbackCachingEnabled="true"
        AllowMultipleSelect="false" EditOnClickSelectedItem="false" OnBeforeCallback="grdShippingContainers_OnBeforeCallback">
        <ClientEvents>
            <ContextMenu EventHandler="grdShippingContainers_onContextMenu" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Title %>" DataField="Title" Align="left"
                        DataCellClientTemplateId="TitleHoverTemplate" Width="250" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Description1 %>" DataField="Description"
                        Align="left" AllowEditing="false" DataCellClientTemplateId="DescriptionHoverTemplate"
                        Width="100" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Length %>" DataField="Length" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="LengthHoverTemplate" Width="40" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Width %>" DataField="Width" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="WidthHoverTemplate" Width="40" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Height %>" DataField="Height" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="HeightHoverTemplate" Width="40" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, MaxWeight %>" DataField="MaxWeight" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="MaxWeightHoverTemplate" Width="110" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, MaxValue %>" DataField="MaxValue" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="MaxValueHoverTemplate" Width="100" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, PackingPreference %>" DataField="PackingPreference"
                        Align="left" AllowEditing="false" DataCellClientTemplateId="PackingPreferenceHoverTemplate"
                        Width="115" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, ShipperName %>" DataField="ShipperName"
                        Align="left" DataCellClientTemplateId="ShipperNameHoverTemplate" Width="140" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, BoxCode %>" DataField="BoxCode" Align="left"
                        DataCellClientTemplateId="BoxCodeHoverTemplate" Width="60" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        AllowSorting="True" HeadingText="<%$ Resources:GUIStrings, Status %>" Align="left"
                        Width="40" DataField="Status" DataCellClientTemplateId="StatusHoverTemplate" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="TitleHoverTemplate">
                <div title="## DataItem.GetMember('Title').get_text() ##">
                    ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DescriptionHoverTemplate">
                <div title="## DataItem.GetMember('Description').get_text() ##">
                    ## DataItem.GetMember('Description').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Description').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="LengthHoverTemplate">
                <div title="## DataItem.GetMember('Length').get_text() ##">
                    ## DataItem.GetMember('Length').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Length').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="WidthHoverTemplate">
                <div title="## DataItem.GetMember('Width').get_text() ##">
                    ## DataItem.GetMember('Width').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Width').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="HeightHoverTemplate">
                <div title="## DataItem.GetMember('Height').get_text() ##">
                    ## DataItem.GetMember('Height').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Height').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="MaxWeightHoverTemplate">
                <div title="## DataItem.GetMember('MaxWeight').get_text() ##">
                    ## DataItem.GetMember('MaxWeight').get_text() == "" ? "&nbsp;" : DataItem.GetMember('MaxWeight').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="MaxValueHoverTemplate">
                <div title="## DataItem.GetMember('MaxValue').get_text() ##">
                    ## DataItem.GetMember('MaxValue').get_text() == "" ? "&nbsp;" : DataItem.GetMember('MaxValue').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PackingPreferenceHoverTemplate">
                <div title="## DataItem.GetMember('PackingPreference').get_text() ##">
                    ## DataItem.GetMember('PackingPreference').get_text() == "" ? "&nbsp;" : DataItem.GetMember('PackingPreference').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ShipperNameHoverTemplate">
                <div title="## DataItem.GetMember('ShipperName').get_text() ##">
                    ## DataItem.GetMember('ShipperName').get_text() == "" ? "&nbsp;" : DataItem.GetMember('ShipperName').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="BoxCodeHoverTemplate">
                <div title="## DataItem.GetMember('BoxCode').get_text() ##">
                    ## DataItem.GetMember('BoxCode').get_text() == "" ? "&nbsp;" : DataItem.GetMember('BoxCode').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <div title="## DataItem.GetMember('Status').get_text() ##">
                    ## DataItem.GetMember('Status').get_text() == "1" ? "Active" : DataItem.GetMember('Status').get_text()
                    == "" ?"" : "Inactive" ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ShippingContainersSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" />
                    </p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdShippingContainers.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdShippingContainers.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ShippingContainersSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##
                    </p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdShippingContainers.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdShippingContainers.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ShippingContainersPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdShippingContainers), pageCount(grdShippingContainers),
                grdShippingContainers.RecordCount) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>
