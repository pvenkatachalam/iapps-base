﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupAddShippingContainer %>"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="AddShippingContainer.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddShippingContainer"
    StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <script type="text/javascript">
        var emptyGuid = '00000000-0000-0000-0000-000000000000'; // empty guid
       
        function ValidatePage()
        {
            var nameObject = document.getElementById("<%=txtName.ClientID%>").value;
            var descriptionObject = document.getElementById("<%=txtDescription.ClientID%>").value;
            var boxCodeObject = Trim(document.getElementById("<%=txtBoxCode.ClientID%>").value);
             var msg ='';
            
            if (Trim(nameObject) =='')
            {
                alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ShippingContainerNameShouldNotBeEmpty %>" />');
                return false;
            }
            if (nameObject!='')
                msg += ObjectLevelSpecialCharacterChecking(nameObject, 'Name');
            if (descriptionObject!='')                
                msg += ObjectLevelSpecialCharacterChecking(descriptionObject, 'Description');

            if (boxCodeObject!='') 
            {  
                if (!boxCodeObject.match(/^[a-zA-Z0-9]+$/))
                {
                    msg= AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, BoxCodeShouldContainOnlyAlphaNumericCharacters %>" />',msg);
                }              
            }
            
            var length = document.getElementById("<%=txtLength.ClientID%>").value;
            var width = document.getElementById("<%=txtWidth.ClientID%>").value;
            var height = document.getElementById("<%=txtHeight.ClientID%>").value;
            var maxWeight = document.getElementById("<%=txtMaxWeight.ClientID%>").value;
            var maxValue = document.getElementById("<%=txtMaxValue.ClientID%>").value;
            var packingPreference = document.getElementById("<%=txtPackingPreference.ClientID%>").value;

            var shippingMethodsObj = document.getElementById("<%=cmbShippingMethods.ClientID%>");
            var shippingMethods = cmbShippingMethods.get_text() ; // shippingMethodsObj.text ; // not possible here.
           
            if (Trim(length) !='')
            {
                if (isNaN(length) || length <= 0)
                {
                    msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, LengthShouldBeGreaterThanZero %>" />',msg);
                    
                }            
            }
            if (Trim(width) !='')
            {
                if (isNaN(width) || width <= 0)
                {
                    msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, WidthShouldBeGreaterThanZero %>" />',msg);
                    
                }            
            }
            if (Trim(height) !='')
            {
                if (isNaN(height) || height <= 0)
                {
                    msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, HeightShouldBeGreaterThanZero %>" />',msg);
                }            
            }
            if (Trim(maxWeight) !='')
            {
                if (isNaN(maxWeight) || maxWeight <= 0)
                {
                    msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxWeightShouldBeGreaterThanZero %>" />',msg);
                }            
            }
            if (Trim(maxValue) !='')
            {
                if (isNaN(maxValue) || maxValue <= 0)
                {
                    msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxValueShouldBeGreaterThanZero %>" />',msg);
                }            
            }
            if (Trim(packingPreference) !='')
            {
                if (isNaN(packingPreference) || packingPreference <= 0)
                {
                    msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PackingPreferenceShouldBeGreaterThanZero %>" />',msg);
                }            
            }
            
            if (shippingMethods !='' && boxCodeObject=='')
            {
                msg = AddIntoErrorMessage('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, BoxCodeShouldNotBeEmpty %>" />',msg);
            }
            if (msg =='')
            {
                return true;    
            }
            else
            {
                alert(msg);
                return false;
            }            
        }

        function AddIntoErrorMessage(errMsg, msg)
        {
            if(Trim(errMsg)!='')
                msg += "\n -" + errMsg;
            return msg;
        }
        function is_int(value){
            if((parseFloat(value) == parseInt(value)) && !isNaN(parseInt(value))){
                return true;
            } 
            else {
                return false;
            }
        }

        function ObjectLevelSpecialCharacterChecking(thisObj, thisObjName)
        {
            var msg = "";
            if (!thisObj.match(/^[a-zA-Z0-9. _'/->.%&$!|#:,]+$/))
            {
                if (IsNameAscii(thisObj)!='false')
                {
                  msg = AddIntoErrorMessage(stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CanHaveOnlyAlphanumericCharactersAnd %>' />", thisObjName),msg);            
                }
            }
            if(thisObj.indexOf('<') >=0 )
            {
                 msg = AddIntoErrorMessage(stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsNotAllowedIn0 %>' />", thisObjName),msg);
            }            
            if(Trim(thisObj) =='&#0;' || thisObj.indexOf('&#0;') >=0 )
            {
                msg = AddIntoErrorMessage(stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacter %>' />", thisObjName),msg); 
            }    
            return msg;
        }
        
        function ClosePage()
        {
            //javascript:parent.HidePopup();
            window.location = "ManageShippingContainers.aspx";
        }
        
        function trvShippingMethods_onNodeSelect(sender, eventArgs)
        {
          cmbShippingMethods.set_text(eventArgs.get_node().get_text());
          cmbShippingMethods.collapse();
        }

        function cmbShippingMethods_onLoad(sender, eventArgs) {
            comboBox_inputCtl = document.getElementById(sender.get_id() + "_Input");
            document.getElementById(cmbShippingMethods.ClientControlId + "_Input").value = "";
            if (comboBox_inputCtl != null && comboBox_inputCtl != undefined) {
                comboBox_inputCtl.disabled = true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="edit-section shipping-containers">
        <div class="columns">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Name1 %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="260"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" Width="260" TextMode="MultiLine"
                        CssClass="textBoxes"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Length %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtLength" runat="server" CssClass="textBoxes" Width="100" MaxLength="8"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Width %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtWidth" runat="server" CssClass="textBoxes" Width="100" MaxLength="8"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Height %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtHeight" runat="server" CssClass="textBoxes" Width="100" MaxLength="8"></asp:TextBox></div>
            </div>
        </div>
        <div class="columns">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxWeight %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMaxWeight" runat="server" CssClass="textBoxes" Width="100" MaxLength="8"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxValue %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMaxValue" runat="server" CssClass="textBoxes" Width="100" MaxLength="8"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PackingPreference %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtPackingPreference" runat="server" CssClass="textBoxes" Width="100"
                        MaxLength="8"></asp:TextBox></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ShipperName %>" /></label>
                <div class="form-value">
                    <ComponentArt:ComboBox ID="cmbShippingMethods" runat="server" SkinID="Default" DropDownHeight="297"
                        DropDownWidth="280" AutoHighlight="false" AutoComplete="false" Width="252">
                        <DropDownContent>
                            <ComponentArt:TreeView ID="trvShippingMethods" ExpandSinglePath="true" SkinID="Default"
                                AutoPostBackOnSelect="false" Height="293" Width="199" CausesValidation="false"
                                runat="server">
                                <ClientEvents>
                                    <NodeSelect EventHandler="trvShippingMethods_onNodeSelect" />
                                </ClientEvents>
                            </ComponentArt:TreeView>
                        </DropDownContent>
                        <ClientEvents>
                            <Load EventHandler="cmbShippingMethods_onLoad" />
                        </ClientEvents>
                    </ComponentArt:ComboBox>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings,
    BoxCode %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtBoxCode" runat="server" CssClass="textBoxes" Width="260" MaxLength="16"></asp:TextBox>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HdnId" Value="" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="StoreSetupHeaderButtons" runat="server">
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClientClick="return ValidatePage();"
        OnClick="btnSave_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
        OnClientClick="ClosePage(); return false;" />
</asp:Content>
