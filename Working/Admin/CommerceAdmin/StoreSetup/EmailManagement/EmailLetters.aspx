﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupEmailManagementManageEmailTemplates %>"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="EmailLetters.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.EmailLetters" StylesheetTheme="General" %>

<asp:Content ID="EmailTemplatesHeader" runat="server" ContentPlaceHolderID="StoreSetupHeader">
    <script type="text/javascript">
        var gridItem;
        function grdEmailLetter_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            gridItem = eventArgs.get_item();
            grdEmailLetter.select(gridItem);
            mnuEmailLetter.showContextMenuAtEvent(evt);
        }

        function grdEmailLetter_onCallbackComplete(sender, eventArgs) {
        }

        function mnuEmailLetter_onItemSelect(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();
            var templateId = gridItem.getMember("Id").get_text();
            switch (selectedMenuId) {
                case 'cmEdit':
                    window.location = "EmailLetterDetail.aspx?Id=" + templateId;
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="EmailListContent" runat="server" ContentPlaceHolderID="StoreSetupContentHolder">
    <div class="grid-utility clear-fix">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Filter %>"
                onclick="onClickSearchonGrid('txtSearch', grdEmailLetter);" />
        </div>
    </div>
    <ComponentArt:Grid ID="grdEmailLetter" SkinID="default" runat="server" Width="100%" RunningMode="Callback"
        PagerInfoClientTemplateId="EmailLetterPageTemplate" AllowEditing="true"
        AutoCallBackOnInsert="false" AutoCallBackOnUpdate="false"
        CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" CallbackCachingEnabled="true"
        AllowMultipleSelect="false" EditOnClickSelectedItem="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
        <ClientEvents>
            <ContextMenu EventHandler="grdEmailLetter_onContextMenu" />
            <CallbackComplete EventHandler="grdEmailLetter_onCallbackComplete" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Title %>" DataField="Title"
                        DataCellClientTemplateId="TitleHoverTemplate" Align="left" AllowEditing="False"
                        Width="500" FixedWidth="true" IsSearchable="true" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Subject1 %>" DataField="Subject"
                        Visible="True" Align="left" AllowEditing="False" Width="680" FixedWidth="true"
                        DataCellClientTemplateId="SubjectHoverTemplate" IsSearchable="true" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="TitleHoverTemplate">
                <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SubjectHoverTemplate">
                <span title="## DataItem.GetMember('Subject').get_text() ##">## DataItem.GetMember('Subject').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Subject').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EmailLetterPageTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdEmailLetter), pageCount(grdEmailLetter),
                grdEmailLetter.RecordCount) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuEmailLetter" SkinID="ContextMenu" runat="server">
        <ClientEvents>
            <ItemSelect EventHandler="mnuEmailLetter_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
