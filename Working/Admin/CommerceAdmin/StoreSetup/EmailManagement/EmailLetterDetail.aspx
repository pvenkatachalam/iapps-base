﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupEmailManagementEditEmailTemplate %>"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="EmailLetterDetail.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.EmailLetterDetail"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="te" TagName="Editor" Src="~/UserControls/General/HtmlEditor.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StoreSetupHeader">
    <script type="text/javascript">

    function RedirectToListing() {
        window.location = "EmailLetters.aspx";
        return false;
    }
    function openPreviewWindow() {
        
    }
    function ValidatePage()
    {
        var nameObject = document.getElementById("<%=txtSubject.ClientID%>").value;
        var descriptionObject = document.getElementById("<%=txtDescription.ClientID%>").value;    
        var fromEmail = document.getElementById("<%=txtFromEmail.ClientID%>").value;    
        var msg ='';

        if (nameObject!='')
            msg += ObjectLevelSpecialCharacterChecking(nameObject, 'Subject');
        else msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, SubjectCannotBeEmpty %>' />";
        if (descriptionObject!='')                
            msg += ObjectLevelSpecialCharacterChecking(descriptionObject, 'Description');   
        else msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DescriptionCannotBeEmpty %>' />";                     
        if (fromEmail!='')                
            msg += ObjectLevelSpecialCharacterCheckingExcludeEmail(fromEmail, 'From Email');
        else msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FromEmailCannotBeEmpty %>' />";                    
        if (msg =='')
        {
            return true;    
        }
        else
        {
            alert(msg);
            return false;
        }     
    }    

    function ObjectLevelSpecialCharacterCheckingExcludeEmail(thisObj, thisObjName)
    {
        var msg = "";
        if(thisObj.indexOf('<') >=0 )
        {
             msg += stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsNotAllowedIn0 %>' />", thisObjName)  ;
        }            
        if(Trim(thisObj) =='&#0;' || thisObj.indexOf('&#0;') >=0 )
        {
            msg += stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacter %>' />", thisObjName); 
        } 
	return msg;
    }
    
    function ObjectLevelSpecialCharacterChecking(thisObj, thisObjName)
    {
        var msg = ObjectLevelSpecialCharacterCheckingExcludeEmail(thisObj,thisObjName );
       
        if (msg =='' && !thisObj.match(/^[a-zA-Z0-9. _'/->.%&$!|#:,]+$/))
        {
            if (IsNameAscii(thisObj)!='false')
            {
               msg += stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CanHaveOnlyAlphanumericCharactersAnd %>' />", thisObjName);            
            }
        }
        return msg;
    }    
    </script>
</asp:Content>
<asp:Content ID="EmailDetailContent" runat="server" ContentPlaceHolderID="StoreSetupContentHolder">
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <div class="edit-section edit-email">
        <asp:ValidationSummary ID="valSumEmail" runat="server" EnableClientScript="true"
            ShowSummary="false" ValidationGroup="EmailGroup" DisplayMode="BulletList" ShowMessageBox="true" />
        <div class="form-row">
            <label class="form-label" for="<%=txtSubject.ClientID%>">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Subject %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtSubject" runat="server" Width="635" CssClass="textBoxes"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="reqSubject" runat="server"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValueForSubject %>"
                    ValidationGroup="EmailGroup" ControlToValidate="txtSubject"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexSubject" runat="server" Text="*" ValidationGroup="EmailGroup"
                    ValidationExpression="^[^<>()]+$" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValidCharactersForSubject %>"
                    ControlToValidate="txtSubject"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" for="<%=txtDescription.ClientID%>">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtDescription" runat="server" Width="635" CssClass="textBoxes"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="reqDescription" runat="server"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValueForDescription %>"
                    ValidationGroup="EmailGroup" ControlToValidate="txtDescription"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexDescription" runat="server" Text="*" ValidationGroup="EmailGroup"
                    ValidationExpression="^[^<>()]+$" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValidCharactersForDescription %>"
                    ControlToValidate="txtDescription"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label" for="<%=txtFromEmail.ClientID%>">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, FromEmail %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtFromEmail" runat="server" Width="635" CssClass="textBoxes"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="reqFromEmail" runat="server"
                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValueForFromEmail %>"
                    ValidationGroup="EmailGroup" ControlToValidate="txtFromEmail"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexFromEmail" runat="server" Text="*" ValidationGroup="EmailGroup"
                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValidFromEmailAddress %>"
                    ControlToValidate="txtFromEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-multi-row">
            <label class="form-label" for="<%=txtAvailableTags.ClientID%>">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SupportedMailMergeTags %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtAvailableTags" runat="server" Width="635" TextMode="MultiLine"
                    Rows="3" ReadOnly="true" CssClass="textBoxes"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Text %>" /></label>
            <div class="form-value">
                <te:Editor ID="ctlEditor" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="StoreSetupHeaderButtons" runat="server">
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" ValidationGroup="EmailGroup"
        OnClientClick="return ValidatePage();" />
    <asp:Button ID="btnPreview" runat="server" Text="<%$ Resources:GUIStrings, Preview %>"
        ToolTip="<%$ Resources:GUIStrings, Preview %>" CssClass="primarybutton"
        OnClientClick="return ValidatePage();" ValidationGroup="EmailGroup" />
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
        OnClientClick="return RedirectToListing();" />
</asp:Content>
