﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupManageTaxCategory %>"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="ManageTaxCategory.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageTaxCategory"
    StylesheetTheme="General" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <script type="text/javascript">
        var _delete = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Delete %>"/>';

        var titleColumnId = 0;
        var descriptionColumnId = 2;
        var commodityCodeColumnId = 1;
        var idColumnId = 3;
        var isUsedColumnId = 5;

        function saveCell(itemId, columnField, control) {
            var row = grdTaxCategory.GetRowFromClientId(itemId);
            var newValue = control.value;

            if (row != null) {
                // Check if value was changed
                var oldValue = row.GetMember(columnField).Value != null ? row.GetMember(columnField).Value : "";
                if (oldValue != newValue) {
                    // Get column index for SetValue
                    var col = 0;
                    for (var i = 0; i < grdTaxCategory.Table.Columns.length; i++) {
                        if (grdTaxCategory.Table.Columns[i].DataField == columnField) {
                            col = i;
                            break;
                        }
                    }
                    var errorMessage = ValidateRow(newValue, col, row);
                    if (errorMessage == "")
                        row.SetValue(col, newValue, true);
                    else {
                        alert(errorMessage);
                        control.value = oldValue;
                        setTimeout(function () { control.focus() }, 10);
                        return false;
                    }
                }
            }
            return true;
        }

        function CheckIfValueExists(newValue, columNumber, row) {
            var exists = false;
            var gridItem;
            var itemIndex = 0;
            while (gridItem = grdTaxCategory.get_table().getRow(itemIndex)) {
                if (gridItem.get_index() != row.get_index()) {
                    if (gridItem.getMemberAt(columNumber).Value != null &&
                        gridItem.getMemberAt(columNumber).Value.toLowerCase() == newValue.toLowerCase()) {
                        exists = true;
                        break;
                    }
                }
                itemIndex++;
            }
            return exists;
        }

        function ValidateRow(newValue, columNumber, row) {
            var errorMessage = "";
            if (newValue == null) newValue = "";
            switch (columNumber) {
                case titleColumnId:
                    if (newValue == "")
                        errorMessage += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, TitleCanNotBeEmpty %>' />\n";
                    else if (newValue.match(/[&\<\>]/))
                        errorMessage += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheTitle %>' />\n";
                    else if (CheckIfValueExists(newValue, columNumber, row))
                        errorMessage += "Title already Exists.";
                    break;
                case commodityCodeColumnId:
                    if (newValue == "")
                        errorMessage += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CommodityCodeCanNotBeEmpty %>' />\n";
                    else if (newValue.match(/[&\<\>]/))
                        errorMessage += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheCommodityCode %>' />\n";
                    else if (CheckIfValueExists(newValue, columNumber, row))
                        errorMessage += "Commodity Code already Exists.";
                    break;
                case descriptionColumnId:
                    if (newValue != "" && newValue.match(/[&\<\>]/))
                        errorMessage += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheDescription %>' />\n";
                    break;
            }
            return errorMessage;
        }

        function ValidateOnSubmit() {
            var gridItem;
            var itemIndex = 0;
            var errorMessage = "";
            while (gridItem = grdTaxCategory.get_table().getRow(itemIndex)) {
                if (gridItem.getMemberAt(titleColumnId).Value != null || gridItem.getMemberAt(commodityCodeColumnId).Value != null) {
                    for (var i = 0; i < 3; i++) {
                        var rowError = ValidateRow(gridItem.getMemberAt(i).Value, i, gridItem);
                        if (rowError != "")
                            errorMessage += stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, TaxCategoryValidationErrorFormat %>' />", (itemIndex + 1), rowError);
                    }
                }
                itemIndex++;
            }

            if (errorMessage != "") {
                alert(errorMessage);
                return false;
            }

            return true;
        }

        function addMoreRows() {
            for (var i = 0; i < 5; i++) {
                grdTaxCategory.get_table().addEmptyRow(grdTaxCategory.Data.length + i);
            }

            return false;
        }

        function deleteRow(itemId) {
            if (window.confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisItem %>"/>')) {
                var row = grdTaxCategory.GetRowFromClientId(itemId);
                if (row.getMemberAt(isUsedColumnId).Value == "1")
                    alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, YouCannotDeleteTaxCategory %>"/>');
                else
                    grdTaxCategory.deleteItem(row);
            }
            return false;
        }  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <p style="margin-bottom: 10px;">
        <em><%= GUIStrings.TaxCategoryHelpText %></em></p>
    <div class="grid-utility clear-fix">
        <div class="columns">
            <asp:HyperLink runat="server" ID="lnkDomesticTaxCategory" Text="<%$ Resources:GUIStrings, DomesticTaxCodes %>"></asp:HyperLink>&nbsp;|&nbsp;
            <asp:HyperLink runat="server" ID="lnkInternationTaxCategory" Text="<%$ Resources:GUIStrings, InternationalTaxCodes %>"></asp:HyperLink>
        </div>
        <div class="columns-right">
            <a href="#" onclick="return addMoreRows();" class="small-button">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, AddMore %>" /></a>
        </div>
    </div>
    <ComponentArt:Grid ID="grdTaxCategory" AllowTextSelection="true" EnableViewState="true"
        EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False"
        KeyboardEnabled="false" ShowFooter="false" SkinID="Default"
        RunningMode="Client" Width="100%" runat="server" PageSize="1000">
        <Levels>
            <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false" EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
                <Columns>
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Title %>" DataField="Title" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="TitleClientTemplate" Width="450"
                         />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, CommodityCode %>" DataField="CommodityCode"
                        Align="left" AllowEditing="false" DataCellClientTemplateId="CommodityCodeClientTemplate"
                        Width="150"  />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Description %>" DataField="Description"
                        Align="left" DataCellClientTemplateId="DescriptionClientTemplate" 
                        Width="450" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn AllowSorting="false" HeadingText="&nbsp;" FixedWidth="true"
                        DataCellClientTemplateId="DeleteClientTemplate" EditControlType="EditCommand"
                        Width="71" Align="Center" />
                    <ComponentArt:GridColumn DataField="IsUsed" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="TitleClientTemplate">
                <div title="## DataItem.GetMember('Title').get_text() ##">
                    <input class="textBoxes" type="text" value="## DataItem.GetMember('Title').get_text() ##"
                        style="width: 90%;" onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this);" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DescriptionClientTemplate">
                <div title="## DataItem.GetMember('Description').get_text() ##">
                    <input class="textBoxes" type="text" value="## DataItem.GetMember('Description').get_text() ##"
                        style="width: 90%;" onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this);" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CommodityCodeClientTemplate">
                <div title="## DataItem.GetMember('CommodityCode').get_text() ##">
                    <input class="textBoxes" type="text" value="## DataItem.GetMember('CommodityCode').get_text() ##"
                        style="width: 85%;" onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this);" />
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DeleteClientTemplate">
                <img alt="##_delete##" title="##_delete##" src="/iapps_images/cm-icon-delete.png" 
                    onclick="return deleteRow('## DataItem.ClientId ##')" ## DataItem.GetMember('CommodityCode').get_text() == "" ? "style='display:none;'" : "&nbsp;" ## />
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="StoreSetupHeaderButtons" runat="server">
    <asp:Button ID="btnSave" OnClick="btnSave_Click" CssClass="primarybutton"
        runat="server" OnClientClick="return ValidateOnSubmit();" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" />
</asp:Content>
