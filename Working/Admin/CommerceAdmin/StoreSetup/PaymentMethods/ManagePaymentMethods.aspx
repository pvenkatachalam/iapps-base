﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupPaymentMethodsManagePaymentMethods %>"
    Language="C#" MasterPageFile="~/StoreSetup/AdministrationMaster.master" AutoEventWireup="true"
    CodeBehind="ManagePaymentMethods.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManagePaymentMethods"
    StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="payment-methods">
        <div class="form-multi-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, AcceptedPaymentTypes %>" /></label>
            <div class="form-value">
                <table border="0" cellpadding="10" cellspacing="10" width="400">
                    <tr>
                        <td valign="middle">
                            <asp:CheckBox ID="chkCreditCard" runat="server" Checked="true" Enabled="false" Text="<%$ Resources:GUIStrings, CreditCard %>" />
                        </td>
                        <asp:Repeater ID="rptrPaymentTypes" runat="server" OnItemDataBound="rptrPaymentTypes_ItemDataBound">
                            <ItemTemplate>
                                <td>
                                    <asp:CheckBox ID="chkPaymentType" runat="server" Checked="true" Text='<%#DataBinder.Eval(Container.DataItem, "Name") %>' />
                                    <asp:HiddenField ID="hfId" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "Id") %>' />
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </table>
            </div>
            <div class="clear-fix">
            </div>
        </div>
        <h3>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CreditCardDetails %>" /></h3>
        <div class="form-multi-row">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, SupportedTypes %>" /></label>
            <div class="form-value">
                <asp:CheckBoxList ID="chklAcceptedCards" runat="server" RepeatDirection="Vertical" />
            </div>
            <div class="clear-fix">
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, PaymentGateway %>" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlGateways" runat="server" Width="260" />
            </div>
            <div class="clear-fix">
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="StoreSetupHeaderButtons" runat="server">
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnSave_Click" />
</asp:Content>
