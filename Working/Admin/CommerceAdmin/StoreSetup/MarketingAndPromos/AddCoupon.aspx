﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupMarketingandPromotionsAddaCoupon %>"
    Language="C#" MasterPageFile="~/StoreSetup/StoreSetupMaster.master" AutoEventWireup="true"
    CodeBehind="AddCoupon.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddCoupon" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <!-- Script for tooltip STARTS -->
    <script type="text/javascript">
        /** Methods for the dynamic tooltip **/
        var tooltipText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ThisWillCreateAUniqueCodeThatCanOnlyBeUsedOnceByOnePerson %>' />";
        var maxUsesText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, LeaveMaxUsesBlankForUnlimited %>' />";
        var maxUserText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, LeaveMaxUserBlankForUnlimited %>' />";
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
    </script>
    <!-- Script for tooltip ENDS -->
    <!-- Script for Calendar STARTS -->
    <script type="text/javascript">



        //Create date calendar methods
        function popUpStartDateCalendar(ctl) {
            var thisDate = iAppsCurrentLocalDate;
            if (document.getElementById("<%=hdnStartDate.ClientID%>").value != "") {
                thisDate = new Date(document.getElementById("<%=hdnStartDate.ClientID%>").value);
                calStartDate.setSelectedDate(thisDate)
            }

            if (!(calStartDate.get_popUpShowing()));
            calStartDate.show(ctl);
        }
        function calStartDate_onSelectionChanged(sender, eventArgs) {
            var selectedDate = calStartDate.getSelectedDate();
            document.getElementById("<%=txtStartDate.ClientID%>").value = calStartDate.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            document.getElementById("<%=hdnStartDate.ClientID%>").value = calStartDate.formatDate(selectedDate, calendarDateFormat);
        }

        //Changed date calendar methods
        function popUpEndDateCalendar() {
            var thisDate = iAppsCurrentLocalDate;
            if (document.getElementById("<%=hdnEndDate.ClientID%>").value != "") {
                thisDate = new Date(document.getElementById("<%=hdnEndDate.ClientID%>").value);
                calEndDate.setSelectedDate(thisDate);
            }

            if (!(calEndDate.get_popUpShowing()));
            calEndDate.show();
        }
        function calEndDate_onSelectionChanged(sender, eventArgs) {
            var selectedDate = calEndDate.getSelectedDate();
            document.getElementById("<%=txtEndDate.ClientID%>").value = calEndDate.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            document.getElementById("<%=hdnEndDate.ClientID%>").value = calEndDate.formatDate(selectedDate, calendarDateFormat);
        }

        function ValidateCouponCodeLength(sender, args) {
            args.IsValid = (args.Value.length >= 4);
        }

        function CheckForInvalidChars(sender, args) {
            if (args.Value.indexOf('<') >= 0) {
                args.IsValid = false;
            }
            if (Trim(args.Value) == '&#0;' || args.Value.indexOf('&#0;') >= 0) {
                args.IsValid = false;
            }
        }
        function ValidateForInvalidChars(sender, args) {
            CheckForInvalidChars(sender, args);
        }

        function ValidateForInvalidCharsCouponCode(sender, args) {
            CheckForInvalidChars(sender, args);
        }

        function ClosePage() {
            //javascript:parent.HidePopup();
            window.location = "ManageCoupons.aspx";
        }

        function trvShippingMethods_onNodeSelect(sender, eventArgs) {
            cmbShippingMethods.set_text(eventArgs.get_node().get_text());
            cmbShippingMethods.collapse();
        }

        var currentMaxDiscountAmount;
        function CouponTypeChanged() {
            var maxDiscountTextbox = document.getElementById('<%=txtMaxDiscount.ClientID %>');
            var discountTypeDropdown = document.getElementById('<%=ddlDiscountType.ClientID %>');
            var discountType = discountTypeDropdown.options[discountTypeDropdown.selectedIndex].text;
            if (document.getElementById("<%=  valShippingMethodRequired.ClientID %>") != null)
                ValidatorEnable(document.getElementById("<%=  valShippingMethodRequired.ClientID %>"), false);

            var restrictionId = "<%=ddlRestrictionType.ClientID %>";

            $("#" + restrictionId + " input").each(function () {
                if (discountType == "$ Refund")
                    $(this).attr("disabled", "disabled");
                else
                    $(this).removeAttr("disabled");
            });

            if (discountType == '$ off Order' || discountType == '$ off Shipping' ) {
                currentMaxDiscountAmount = maxDiscountTextbox.value;
                maxDiscountTextbox.value = '';
                maxDiscountTextbox.disabled = true;
                ValidatorEnable(document.getElementById("<%= valDiscountLessThan100Pct.ClientID %>"), false);
                if (discountType.indexOf('shipping') > 0) {
                    ValidatorEnable(document.getElementById("<%=  valShippingMethodRequired.ClientID %>"), true);
                }
            if(discountType != 'Flat rate shipping')
                ValidatorEnable(document.getElementById("<%= valMinPurchaseToCouponValue.ClientID %>"), true);

            }
            else {
                if (currentMaxDiscountAmount) {
                    maxDiscountTextbox.value = currentMaxDiscountAmount;
                }
                maxDiscountTextbox.disabled = false;
                ValidatorEnable(document.getElementById("<%= valDiscountLessThan100Pct.ClientID %>"), true);
                ValidatorEnable(document.getElementById("<%= valMinPurchaseToCouponValue.ClientID %>"), false);
            }
            //var shippingOptionDropdown = document.getElementById('<%=cmbShippingMethods.ID %>');
            if (document.getElementById("<%=  valShippingMethodRequired.ClientID %>") != null) {
                if (discountType.toLowerCase().indexOf('shipping') >= 0) {
                    // cmbShippingMethods.enable();
                    document.getElementById("<%= pnlShippingMethods.ClientID %>").style.display = "";
                    ValidatorEnable(document.getElementById("<%=  valShippingMethodRequired.ClientID %>"), true);
                }
                else {
                    // cmbShippingMethods.set_text('');
                    document.getElementById("<%= pnlShippingMethods.ClientID %>").style.display = "none";
                    //  cmbShippingMethods.disable();
                }
            }
        }       
    </script>
    <!-- Script for Calendar ENDS~ -->
    <!-- Script for popups -->
    <script type="text/javascript">
        var titleColumn = 0;
        var idColumn = 1;

        function OnRestrictionChange() {
            grdSelectedItem.selectAll();
            grdSelectedItem.deleteSelected();

            var selectedValue = $("#ddlRestrictionType").val();
            if (selectedValue == "None") {
                SetGridVisibility(false);
            }
            else {
                SetGridVisibility(true);
            }
        }

        //Navigation Category
        function ShowNavigationCategoryPopup() {
            var popupPath = "../../Popups/StoreManager/NavigationCategoryMultiSelect.aspx";
            navigationCategoryPopup = dhtmlmodal.open('NavigationCategorySearchPopup', 'iframe', popupPath, '', '');
        }

        function SetNavigationCategorys(selectedIdValues, selectedNameValues) {
            var count = 0;
            if (selectedNameValues != null && selectedNameValues != '' && selectedNameValues != 'undefined') {
                for (k = 0; k < selectedIdValues.length; k++) {
                    count++;
                    AddGridItems(selectedIdValues[k], selectedNameValues[k]);
                }
            }
        }

        //Product Type
        function ShowProductTypePopup() {
            OpeniAppsCommercePopup('ProductTypeMultiSelect', '', '');
        }

        function SetProductTypes(selectedIdValues, selectedNameValues) {
            var count = 0;
            if (selectedNameValues != null && selectedNameValues != '' && selectedNameValues != 'undefined') {
                for (k = 0; k < selectedIdValues.length; k++) {
                    count++;
                    AddGridItems(selectedIdValues[k], selectedNameValues[k]);
                }
            }
        }

        function fn_HideProductType() {
            CloseiAppsCommercePopup();
        }

        // Customer
        function ShowCustomerPopup() {
            var popupPath = "../../Popups/StoreManager/Customers/SearchCustomer.aspx?MultiSelect=true";
            customerSelectPopup = dhtmlmodal.open('CustomerSearchPopup', 'iframe', popupPath, '', '');
        }

        function UpdateSelectedCustomerInfo(gCustomerIds, arrCustomerInfo) {
            var arrCustomerIds = gCustomerIds.split(',');
            var arrCustomerDetails;
            var customerName = '';

            var count = 0;
            for (var i = 0; i < arrCustomerIds.length; i++) {
                if (arrCustomerInfo[arrCustomerIds[i]]) {
                    arrCustomerDetails = arrCustomerInfo[arrCustomerIds[i]].split('|');
                    customerName = arrCustomerDetails[0];
                    if (arrCustomerDetails[1] != null && arrCustomerDetails[1] != '') {
                        customerName = customerName + "," + arrCustomerDetails[1];
                    }
                    else if (arrCustomerDetails[3] != null && arrCustomerDetails[3] != '') {
                        customerName = arrCustomerDetails[3];

                    }
                    AddGridItems(arrCustomerIds[i], customerName);
                    count++;
                }
             }
        }

        function ClosePopupCustomerPopup() {
            customerSelectPopup.hide();
        }

        //Product
        function ShowProductPopup() {
            var popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx?ShowProducts=true";
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', '');
        }

        function CloseProductPopup(arrSelected) {
            var count = 0;
            if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                var selectedArray;
                var oneRowSKUIds;
                var oneRowSKUNames;

                var selectedOption = $("#ddlRestrictionType").val();
                for (k = 0; arrSelected.length; k++) {
                    selectedArray = arrSelected.pop();
                    if (selectedOption == 'Product') {
                        AddGridItems(selectedArray[0], selectedArray[2]);
                        count++;
                    }
                    else if (selectedOption == 'SKU') {
                        oneRowSKUIds = selectedArray[1];
                        oneRowSKUNames = selectedArray[3];

                        var arrOneRowIds = oneRowSKUIds.split(',');
                        var arrOneRowNames = oneRowSKUNames.split(',');
                        for (var i = 0; i < arrOneRowIds.length; i++) {
                            if (arrOneRowIds[i] != '') {
                                AddGridItems(arrOneRowIds[i], arrOneRowNames[i]);
                                count++;
                            }
                        }
                    }
                }
            }
            productSearchPopup.hide();
        }

        //SKU
        function ShowSKUPopup() {
            var popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx";
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', '');
        }

        function AddGridItems(selectedItemId, selectedItemTitle) {
            if (!DuplicateItemCheck(selectedItemId)) {
                //-- Add to the grid
                grdSelectedItem.beginUpdate();
                grdSelectedItem.get_table().addEmptyRow(0);
                gridItem = grdSelectedItem.get_table().getRow(0);
                grdSelectedItem.edit(gridItem);

                gridItem.SetValue(titleColumn, selectedItemTitle);
                gridItem.SetValue(idColumn, selectedItemId);

                grdSelectedItem.editComplete();
            }
        }

        function DuplicateItemCheck(thisId) {
            var len = grdSelectedItem.Data.length;
            var gItem;
            var othId;

            for (var i = 0; i < len; i++) {
                gItem = grdSelectedItem.get_table().getRow(i);
                if (gItem != null) {
                    othId = gItem.getMember(idColumn).get_value(); //Id
                    if (thisId == othId) {
                        return true;
                    }
                }
            }
            return false;
        }

        function AddItem() {
            var selectedOption = $("#ddlRestrictionType").val();

            if (selectedOption == 'SKU') {
                ShowSKUPopup();
            }
            else if (selectedOption == 'Product') {
                ShowProductPopup();
            }
            else if (selectedOption == 'ProductType') {
                ShowProductTypePopup();
            }
            else if (selectedOption == 'Navigation') {
                ShowNavigationCategoryPopup();
            }
            else if (selectedOption == 'Customer') {
                ShowCustomerPopup();
            }

            return false;
        }

        function RemoveItem() {
            grdSelectedItem.deleteSelected();

            return false;
        }

        function grdSelectedItem_onLoad(sender, eventArgs) {
            if ($("#<%= ddlRestrictionType.ClientID %>").val() == "None")
                SetGridVisibility(false);
            else {
                SetGridVisibility(true);
            }
        }

        function SetGridVisibility(canShow) {
            if (canShow) {
                $(".grid-utility").show();
                $(".scrollable-grid").show();
            }
            else {
                $(".grid-utility").hide();
                $(".scrollable-grid").hide();
            }
        }

        function OnCouponSave() {
            if (Page_ClientValidate('validateCoupon') == false)
                return false;
            var selectedIds = "";
            for (var i = 0; i < grdSelectedItem.Data.length; i++) {
                gItem = grdSelectedItem.get_table().getRow(i);
                if (gItem != null)
                    selectedIds += gItem.getMember(idColumn).get_value() + ",";
            }

            document.getElementById("<%=HdnSelectedItemIds.ClientID%>").value = selectedIds;

            return true;
        }
    </script>
    <!-- Script for popups ENDS~ -->
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <asp:ValidationSummary ID="valSummary" ValidationGroup="validateCoupon" runat="server"
        EnableClientScript="true" ShowMessageBox="true" ShowSummary="false" />
    <div class="edit-section add-coupon">
        <div class="columns">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Name1 %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="260" MaxLength="254"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valNameRequired" runat="server" ControlToValidate="txtName"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, CouponNameShouldNotBeEmpty %>"
                        Display="None" Text="*"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="valNameInvalidChars" runat="server" ControlToValidate="txtName"
                        ValidationGroup="validateCoupon" Display="None" Text="*" ErrorMessage="<%$ Resources:GUIStrings, CanNotHaveGreaterThanOrControlCharsInCouponName %>"
                        ClientValidationFunction="ValidateForInvalidChars">
                    </asp:CustomValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DiscountCode %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDiscountCode" runat="server" CssClass="textBoxes" Width="260"
                        MaxLength="16"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valCodeRequired" runat="server" ControlToValidate="txtDiscountCode"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, DiscountCodeCannotBeEmpty %>"
                        Display="None" Text="*"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="valCodeLength" runat="server" ControlToValidate="txtDiscountCode"
                        ValidationGroup="validateCoupon" Display="None" Text="*" ErrorMessage="<%$ Resources:GUIStrings, DiscountCodeShouldBeAtLeast4CharactersInLength %>"
                        ClientValidationFunction="ValidateCouponCodeLength">
                    </asp:CustomValidator>
                    <asp:CustomValidator ID="valCodeInvalidChars" runat="server" ControlToValidate="txtName"
                        ValidationGroup="validateCoupon" Display="None" Text="*" ErrorMessage="<%$ Resources:GUIStrings, CanNotHaveGreaterThanOrControlCharsInCouponCode %>"
                        ClientValidationFunction="ValidateForInvalidCharsCouponCode">
                    </asp:CustomValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    &nbsp;</label>
                <asp:Button ID="btnGenerateCode" runat="server" Text="<%$ Resources:GUIStrings, GenerateCode %>"
                    CssClass="small-button" ToolTip="<%$ Resources:GUIStrings, GenerateCode %>" OnClick="btnGenerateCode_Click" />
                <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(tooltipText, 300, '');positiontip(event);"
                    onmouseout="hideddrivetip();" />
                <asp:HiddenField ID="hdnIsGenerated" runat="server" />
            </div>
            <div class="form-row form-multi-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" Width="260" TextMode="MultiLine"
                        CssClass="textBoxes"></asp:TextBox>
            </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Discount %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDiscount" runat="server" CssClass="textBoxes itemSpacing" Width="80"
                        MaxLength="6"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="valDiscountRequired" runat="server" ControlToValidate="txtDiscount"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, CouponValueShouldBeGreaterThanZero %>"
                        Display="None" Text="*"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="valDiscountGreaterThanZero" runat="server" ControlToValidate="txtDiscount"
                        Operator="GreaterThan" Type="Double" CultureInvariantValues="true" ValidationGroup="validateCoupon"
                        ValueToCompare="0" ErrorMessage="<%$ Resources:GUIStrings, CouponValueShouldBeGreaterThanZero %>"
                        Display="None" Text="*">
                    </asp:CompareValidator>
                    <asp:CompareValidator ID="valDiscountLessThan100Pct" runat="server" ControlToValidate="txtDiscount"
                        Operator="LessThanEqual" Type="Double" CultureInvariantValues="true" ValidationGroup="validateCoupon"
                        ValueToCompare="100" ErrorMessage="<%$ Resources:GUIStrings, CouponValueCannotBeGreaterThan100 %>"
                        Display="None" Text="*">
                    </asp:CompareValidator>
                    <asp:DropDownList ID="ddlDiscountType" runat="server" Width="178" CssClass="itemSpacing"
                        onchange="CouponTypeChanged();" />
                </div>
            </div>
            <asp:Panel ID="pnlShippingMethods" runat="server" CssClass="form-row">
                <label class="form-label">
                    <%= GUIStrings.ShippingMethods %></label>
                <div class="form-value">
                    <ComponentArt:ComboBox ID="cmbShippingMethods" runat="server" SkinID="Default" DropDownHeight="297"
                        DropDownWidth="320" AutoHighlight="false" AutoComplete="false" Width="255">
                        <DropDownContent>
                            <ComponentArt:TreeView ID="trvShippingMethods" ExpandSinglePath="true" SkinID="Default"
                                AutoPostBackOnSelect="false" Height="293" Width="320" CausesValidation="false"
                                runat="server">
                                <ClientEvents>
                                    <NodeSelect EventHandler="trvShippingMethods_onNodeSelect" />
                                </ClientEvents>
                            </ComponentArt:TreeView>
                        </DropDownContent>
                    </ComponentArt:ComboBox>
                    <asp:RequiredFieldValidator ID="valShippingMethodRequired" runat="server" ControlToValidate="cmbShippingMethods"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectTheShippingOptionForShippingDiscount %>"
                        Display="None" Text="*"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MinPurchase %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMinPurchase" runat="server" CssClass="textBoxes" Width="80" MaxLength="8"></asp:TextBox>
                    <asp:CompareValidator ID="valMinimumPurchase" runat="server" ControlToValidate="txtMinPurchase"
                        ValueToCompare="0" Operator="GreaterThan" Type="Double" CultureInvariantValues="true"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, MinimumPurchaseShouldBeGreaterThanZero %>"
                        Display="None" Text="">
                    </asp:CompareValidator>
                    <asp:CompareValidator ID="valMinPurchaseToCouponValue" runat="server" ControlToValidate="txtMinPurchase"
                        Enabled="false" ControlToCompare="txtDiscount" Operator="GreaterThanEqual" Type="Double"
                        CultureInvariantValues="true" ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, CouponValueShouldBeLessThanMinimumPurchaseAmount %>"
                        Display="None" Text="*">
                    </asp:CompareValidator>
            </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxDiscountAmount %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMaxDiscount" runat="server" CssClass="textBoxes" Width="80" MaxLength="10"></asp:TextBox>
                    <asp:CompareValidator ID="valMaxDiscount" runat="server" ControlToValidate="txtMaxDiscount"
                        ValueToCompare="0" Operator="GreaterThan" Type="Double" CultureInvariantValues="true"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, MaximumDiscountAmountShouldBeGreaterThanZero %>"
                        Display="None" Text="*">
                    </asp:CompareValidator>
            </div>
        </div>
        </div>
        <div class="columns">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, StartDate %>" /></label>
                <div class="form-value calendar-value">
                    <asp:HiddenField ID="hdnStartDate" runat="server" />
                    <asp:TextBox ID="txtStartDate" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
                    <span>
                        <img id="StartDate_button" class="buttonCalendar" alt="<%= GUIStrings.OpenCalendar %>"
                            src="../../App_Themes/General/images/calendar-button.png" onclick="popUpStartDateCalendar(this);" />
                    </span>
                    <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="calStartDate"
                        MaxDate="9998-12-31" PopUpExpandControlId="StartDate_button">
                        <ClientEvents>
                            <SelectionChanged EventHandler="calStartDate_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                    <asp:RequiredFieldValidator ID="valStartDateRequired" runat="server" ControlToValidate="txtStartDate"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, StartDateShouldNotBeEmpty %>"
                        Display="None" Text="*"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="valStartDateFormat" runat="server" ControlToValidate="txtStartDate"
                        Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ValidationGroup="validateCoupon"
                        ErrorMessage="<%$ Resources:GUIStrings, StartDateIsNotInCorrectFormat %>" Display="None"
                        Text="*">
                    </asp:CompareValidator>
                    <asp:CompareValidator ID="valStartDateLaterThanToday" runat="server" ControlToValidate="txtStartDate"
                        Operator="GreaterThanEqual" Type="Date" CultureInvariantValues="true" ValidationGroup="validateCoupon"
                        ErrorMessage="<%$ Resources:GUIStrings, StartDateShouldNotBeLessThanToday %>"
                        Display="None" Text="*">
                    </asp:CompareValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EndDate %>" /></label>
                <div class="form-value calendar-value">
                    <asp:HiddenField ID="hdnEndDate" runat="server" />
                    <asp:TextBox ID="txtEndDate" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
                    <span>
                        <img id="EndDate_button" class="buttonCalendar" alt="<%= GUIStrings.OpenCalendar %>"
                            src="../../App_Themes/General/images/calendar-button.png" onclick="popUpEndDateCalendar(this);" />
                    </span>
                    <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="calEndDate"
                        MaxDate="9998-12-31" PopUpExpandControlId="EndDate_button">
                        <ClientEvents>
                            <SelectionChanged EventHandler="calEndDate_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                    <asp:CompareValidator ID="valEndDateFormat" runat="server" ControlToValidate="txtEndDate"
                        Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ValidationGroup="validateCoupon"
                        ErrorMessage="<%$ Resources:GUIStrings, EndDateIsNotInCorrectFormat %>" Display="None"
                        Text="*">
                    </asp:CompareValidator>
                    <asp:CompareValidator ID="valEndDateGreaterThanStartDate" runat="server" ControlToValidate="txtEndDate"
                        Operator="GreaterThan" Type="Date" CultureInvariantValues="true" ValidationGroup="validateCoupon"
                        ControlToCompare="txtStartDate" ErrorMessage="<%$ Resources:GUIStrings, EndDateMustBeGreaterThanStartDate %>"
                        Display="None" Text="*">
                    </asp:CompareValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxUsesperPerson %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMaxUsesPerson" runat="server" CssClass="textBoxes" Width="80"
                        MaxLength="4"></asp:TextBox>
                    <asp:CompareValidator ID="valMaxUsesPerson" runat="server" ControlToValidate="txtMaxUsesPerson"
                        ValueToCompare="0" Operator="GreaterThan" Type="Integer" CultureInvariantValues="true"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, MaximumUsesPerCustomerShouldBeGreaterThanZero %>"
                        Display="None" Text="">
                    </asp:CompareValidator>
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(maxUsesText, 260, '');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxUsesTotal %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMaxUsesTotal" runat="server" CssClass="textBoxes" Width="80"
                        MaxLength="6"></asp:TextBox>
                    <asp:CompareValidator ID="valMaxUsesTotal" runat="server" ControlToValidate="txtMaxUsesTotal"
                        ValueToCompare="0" Operator="GreaterThan" Type="Integer" CultureInvariantValues="true"
                        ValidationGroup="validateCoupon" ErrorMessage="<%$ Resources:GUIStrings, MaximumUsesShouldBeGreaterThanZero %>"
                        Display="None" Text="">
                    </asp:CompareValidator>
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(maxUserText, 260, '');positiontip(event);"
                        onmouseout="hideddrivetip();" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Stackable %>" /></label>
                <div class="form-value">
                    <asp:CheckBox runat="server" ID="cbkStackable" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AutoApply %>" /></label>
                <div class="form-value">
                    <asp:CheckBox runat="server" ID="cbkAutoApply" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, RestrictionType %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlRestrictionType" runat="server" Width="224" onchange="OnRestrictionChange();" ClientIDMode="Static">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, None %>" Value="None" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, SKU %>" Value="SKU"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Product %>" Value="Product"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, ProductType %>" Value="ProductType"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, NavigationCategory %>" Value="Navigation"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Customer %>" Value="Customer"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <asp:Panel ID="pnlActions" runat="server" CssClass="grid-utility">
                <asp:Button ID="btnAddRestriction" runat="server" Text="Add Item(s)" CssClass="button"
                    ToolTip="Add Items" OnClientClick="return AddItem();" />
                <asp:Button ID="btnRemoveRestriction" runat="server" Text="Remove Item(s)" CssClass="button"
                    ToolTip="Remove Items" OnClientClick="return RemoveItem();" />
            </asp:Panel>
            <div class="scrollable-grid" id="scrollableGrid" style="width: 99.7%;">
                <ComponentArt:Grid ID="grdSelectedItem" SkinID="Default" runat="server" Width="100%"
                    ShowHeader="false" ShowFooter="false" RunningMode="Client" AutoPostBackOnSelect="false"
                    EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" PageSize="10000" AllowEditing="false"
                    AutoCallBackOnUpdate="false" AllowMultipleSelect="true" AllowPaging="false">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                            SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                            ShowSelectorCells="false" ShowHeadingCells="false">
                            <Columns>
                                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Name %>" DataField="Title"
                                    AllowReordering="false" FixedWidth="true" AllowSorting="True" DataCellCssClass="last-data-cell"
                                    HeadingCellCssClass="last-heading-cell" DataCellClientTemplateId="SSNameHoverTemplate"
                                    IsSearchable="true" />
                                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientEvents>
                        <Load EventHandler="grdSelectedItem_onLoad" />
                    </ClientEvents>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="SSNameHoverTemplate">
                            <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                                == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <asp:HiddenField ID="HdnId" Value="" runat="server" />
    <asp:HiddenField ID="HdnSelectedItemIds" Value="" runat="server" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="StoreSetupHeaderButtons" runat="server">
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        OnClientClick="return OnCouponSave();" ToolTip="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ValidationGroup="validateCoupon" OnClick="btnSave_Click" />
 <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
        OnClientClick="ClosePage(); return false;" CausesValidation="false" />
    <script type="text/javascript">
        $(document).ready(function () {
            CouponTypeChanged();

            $(":checkbox[id='cbkAutoApply']").change(function () {
                if (this.checked) {
                    $("#cbkStackable").prop('checked', true);
                }
            });
            var autoApplyStackableMsg = "<%= GUIStrings.autoApplyStackableMsg %>"
            $("#cbkStackable").change(function () {
                if ($("#cbkAutoApply").is(":checked")) {
                    alert(autoApplyStackableMsg);
                    $("#cbkStackable").prop('checked', true);
                }
            });
        }
            );

    </script>
</asp:Content>
