﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupMarketingandPromotionsManageCoupons %>"
    Language="C#" MasterPageFile="~/StoreSetup/StoreSetupMaster.master" AutoEventWireup="true"
    CodeBehind="ManageCoupons.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageCoupons" StylesheetTheme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StoreSetupHeader" runat="server">
    <script type="text/javascript">
        var gridItem;
        var couponId;
        var couponStatus;
        var couponIdColumn = 9;
        var couponStatusColumn = 8;
        var couponPopup;

        function grdCoupons_onContextMenu(sender, eventArgs) {
            gridItem = eventArgs.get_item();
            grdCoupons.select(gridItem);
            var evt = eventArgs.get_event();

            menuItems = mnuCoupons.get_items();
            if (gridItem.getMember(couponStatusColumn).get_text() == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, Active %>' />" ||
                gridItem.getMember(couponStatusColumn).get_text() == '1') {
                menuItems.getItem(6).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MakeCouponInactive %>' />");
            }
            else {
                menuItems.getItem(6).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MakeCouponActive %>' />");
            }

            var uniqueId = gridItem.getMember(couponIdColumn).get_text();
            if (uniqueId != '' && uniqueId != emptyGuid) //only add
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }

            mnuCoupons.showContextMenuAtEvent(evt);
        }

        function mnuCoupons_OnItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        couponId = emptyGuid;
                        //ShowCouponPopup(gridItem);
                        ShowCoupon(couponId);
                        break;
                    case 'cmEdit':
                        couponId = gridItem.getMember(couponIdColumn).get_text();
                        //ShowCouponPopup(gridItem);
                        ShowCoupon(couponId);
                        break;
                    case 'cmActive':
                        ChangeStatus(gridItem);
                        break;
                    case 'cmDelete':
                        if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteTheSelectedItem %>' />")) {
                            DeleteCoupon(gridItem);
                        }
                        break;
                }
            }
        }

        function DeleteCoupon(currentItem) {
            couponId = currentItem.getMember(couponIdColumn).get_text();
            //-- Call call back method to delete the coupon
            var strStatus = DeleteCouponCB(couponId);
            if (strStatus.toLowerCase() == 'true') {
                if (grdCoupons.get_recordCount() > grdCoupons.get_pageSize()) {
                    if (grdCoupons.get_recordCount() % grdCoupons.get_pageSize() == 1) {
                        grdCoupons.previousPage();
                    }
                }
                //-- Call Grid Reload here
                ShowStatusWise();
            }
            else {
                alert(strStatus);
            }
        }

        function ShowCoupon(couponId) {
            window.location = "AddCoupon.aspx?couponId=" + couponId;
        }

        function ChangeStatus(currentItem) {
            var currentStatus = currentItem.getMember(couponStatusColumn).get_text();
            couponId = currentItem.getMember(couponIdColumn).get_text();
            //-- Call call back method to update status
            var strStatus = ChangeCouponStatus(couponId, currentStatus);
            if (strStatus.toLowerCase() == 'true') {
                //-- Call Grid Reload here
                ShowStatusWise();
            }
            else {
                alert(strStatus);
            }
        }

        function SetArchivedStatus(chkStatusObject) {
            window["ShowArchived"] = chkStatusObject;
            ShowStatusWise();
        }

        function SetGeneratedStatus(chkStatusObject) {
            window["GeneratedCoupons"] = chkStatusObject;
            ShowStatusWise();
        }

        function ShowStatusWise() {
            var actionColumnValue = '';
            var chkArchivedStatusObject = window["ShowArchived"];
            var chkGeneratedStatusObject = window["GeneratedCoupons"];

            if (chkArchivedStatusObject != null && chkArchivedStatusObject.checked) {
                actionColumnValue = 'ShowArchived';
            }
            else {
                actionColumnValue = 'ShowActive';
            }
            if (chkGeneratedStatusObject != null && chkGeneratedStatusObject.checked) {
                actionColumnValue = actionColumnValue + ';ShowGenerated';
            }
            else {
                actionColumnValue = actionColumnValue + ';ShowAuto';
            }

            LoadCouponGrid(actionColumnValue);
        }

        function LoadCouponGrid(actionColumnValue) {
            grdCoupons.set_callbackParameter(actionColumnValue);
            grdCoupons.callback();
        }
       
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="StoreSetupHeaderButtons" runat="server">
    <asp:HyperLink ID="hplNewCoupon" runat="server" Text="Add New Coupon" NavigateUrl="~/StoreSetup/MarketingAndPromos/AddCoupon.aspx?couponId=00000000-0000-0000-0000-000000000000"
        CssClass="primarybutton"></asp:HyperLink>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="grid-utility">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Filter %>"
                onclick="return onClickSearchonGrid('txtSearch', grdCoupons);" />
        </div>
        <div class="columns-right">
            <asp:CheckBox ID="chkShowArchived" runat="server" Text="<%$ Resources:GUIStrings, ShowInactiveCoupons %>"
                onclick="SetArchivedStatus(this);" />
            <asp:CheckBox ID="chkShowGeneratedCoupons" runat="server" Text="<%$ Resources:GUIStrings, ShowGeneratedCoupons %>"
                onclick="SetGeneratedStatus(this);" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Grid ID="grdCoupons" SkinID="Default" runat="server" Width="940" RunningMode="Callback"
        PagerInfoClientTemplateId="CouponsPaginationTemplate" AllowEditing="true" AutoCallBackOnInsert="false"
        SliderPopupCachedClientTemplateId="CouponsSliderTemplateCached" SliderPopupClientTemplateId="CouponsSliderTemplate"
        AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True"
        CallbackCachingEnabled="true" AllowMultipleSelect="false" EditOnClickSelectedItem="false"
        OnBeforeCallback="grdCoupons_OnBeforeCallback" LoadingPanelClientTemplateId="CouponsLoadingPanelTemplate">
        <ClientEvents>
            <ContextMenu EventHandler="grdCoupons_onContextMenu" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Name %>" DataField="Title" Align="left"
                        DataCellClientTemplateId="NameHoverTemplate" Width="350" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Code %>" DataField="Code" Align="left"
                        DataCellClientTemplateId="CodeHoverTemplate" Width="213" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Description1 %>" DataField="Description"
                        Align="left" AllowEditing="false" DataCellClientTemplateId="DescriptionHoverTemplate"
                        Width="150" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Generated %>" DataField="IsGenerated"
                        Align="left" AllowEditing="false" DataCellClientTemplateId="GeneratedHoverTemplate"
                        Width="90" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, NumUsed %>" DataField="Used" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="UsedHoverTemplate" Width="75" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Type %>" DataField="CouponType" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="TypeHoverTemplate" Width="120" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, StartDate %>" DataField="StartDate" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="StartHoverTemplate" Width="100"
                        FormatString="d" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, EndDate1 %>" DataField="EndDate" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="EndHoverTemplate" Width="100"
                        FormatString="d" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        AllowSorting="True" HeadingText="<%$ Resources:GUIStrings, Status %>" Align="left"
                        Width="75" DataField="Status" DataCellClientTemplateId="StatusHoverTemplate" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <div title="## DataItem.GetMember('Title').get_text() ##">
                    ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CodeHoverTemplate">
                <div title="## DataItem.GetMember('Code').get_text() ##">
                    ## DataItem.GetMember('Code').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Code').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DescriptionHoverTemplate">
                <div title="## DataItem.GetMember('Description').get_text() ##">
                    ## DataItem.GetMember('Description').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Description').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="GeneratedHoverTemplate">
                <div title="## DataItem.GetMember('IsGenerated').get_text() ##">
                    ## DataItem.GetMember('IsGenerated').get_text() == "1" ? "Yes" : DataItem.GetMember('IsGenerated').get_text()
                    == ""? "": "No" ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="UsedHoverTemplate">
                <div title="## DataItem.GetMember('Used').get_text() ##">
                    ## DataItem.GetMember('Used').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Used').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <div title="## DataItem.GetMember('CouponType').get_text() ##">
                    ## DataItem.GetMember('CouponType').get_text() == "" ? "&nbsp;" : DataItem.GetMember('CouponType').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StartHoverTemplate">
                <div title="## DataItem.GetMember('StartDate').get_text() ##">
                    ## DataItem.GetMember('StartDate').get_text() == "" ? "&nbsp;" : DataItem.GetMember('StartDate').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EndHoverTemplate">
                <div title="## DataItem.GetMember('EndDate').get_text() ##">
                    ## DataItem.GetMember('EndDate').get_text() == "" ? "&nbsp;" : DataItem.GetMember('EndDate').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <div title="## DataItem.GetMember('Status').get_text() ##">
                    ## DataItem.GetMember('Status').get_text() == "1" ? "Active" : DataItem.GetMember('Status').get_text()
                    == "" ?"" : "Inactive" ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CouponsSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdCoupons.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdCoupons.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CouponsSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdCoupons.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdCoupons.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CouponsPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdCoupons), pageCount(grdCoupons),
                grdCoupons.RecordCount) ##
            </ComponentArt:ClientTemplate>
             <ComponentArt:ClientTemplate ID="CouponsLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdCoupons) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuCoupons" runat="server" SkinID="ContextMenu">
        <ClientEvents>
            <ItemSelect EventHandler="mnuCoupons_OnItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
