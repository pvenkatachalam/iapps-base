﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupFeaturesManageFeatures %>"
    Language="C#" MasterPageFile="~/StoreSetup/StoreSetupMaster.master" StylesheetTheme="General"
    AutoEventWireup="true" CodeBehind="ManageFeatures.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageFeatures" %>

<asp:Content ID="header" runat="server" ContentPlaceHolderID="StoreSetupHeader">
    <script type="text/javascript">
        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdFeatures;
                menuObject = mnuFeatures;
                gridObjectName = "grdFeatures";
                uniqueIdColumn = 5;
                operationColumn = 4;
                selectedTreeNodeId = emptyGuid;
            }, 500);
        }

        function PageLevelCallbackComplete() {
        }

        function PageLevelOnContextMenu(menuItems) {
            var typeId = gridItem.getMember(typeIdColumn).get_text();
            if (gridItem.getMember(statusColumn).get_text() == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, Active %>' />") {
                menuItems.getItem(6).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MakeInactive %>' />");
            }
            else {
                menuItems.getItem(6).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MakeActive %>' />");
            }
            return true;
        }

        function PageLevelOnItemSelect(selectedMenuId, currentGridItem, sender, eventArgs) {
            if (selectedMenuId == 'cmManage') {
                ShowManageFeaturePopup(currentGridItem);
            }
            else if (selectedMenuId == 'cmActive') {
                ChangeStatus(currentGridItem);
            }
        }

        function PageLevelAddItem(currentGridItem) {
            return true;
        }

        function PageLevelEditItem(currentGridItem) {
            return true;
        }

        function PageLevelDeleteItem(currentGridItem) {
        }

        function PageLevelSaveRecord() {
            return true;
        }

        function PageLevelCancelEdit() {
            return true;
        }

        function PageLevelGridValidation(currentGridItem) {
            var thisName = Trim(document.getElementById(txtName).value);
            var thisDescription = Trim(document.getElementById(txtDescription).value);
            var msg = "";

            if (Trim(thisName) == "") {
                msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NameShouldNotBeEmpty %>' />";
            }
            else {
                msg += GridLevelSpecialCharacterChecking(thisName, 'Name');
            }

            if (Trim(thisDescription) == "") {

            }
            else {
                msg += GridLevelSpecialCharacterChecking(thisDescription, 'Description');
            }

            var len = gridObject.Data.length;
            var gItem;
            var nameOth;
            for (var i = 0; i < len; i++) {
                if (i != gridItem.Index) {
                    gItem = gridObject.get_table().getRow(i);
                    nameOth = gItem.getMember(0).get_value();
                    if (thisName.toLowerCase() == nameOth.toLowerCase()) {
                        msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DuplicateNameExisting %>' />";
                    }
                }
            }
            if (msg != "") {
                alert(msg);
                document.getElementById(txtName).focus();
                return false;
            }
            else {
                return true;
            }
        }

        var featurePopup;
        var popupPath;
        var nameIdColumn = 0;
        var typeIdColumn = 6;
        var statusColumn = 3;
        function ShowManageFeaturePopup(currentItem) {
            ClearFeaturePopupSessions();
            var typeId = currentItem.getMember(typeIdColumn).get_text();
            var featureId = currentItem.getMember(uniqueIdColumn).get_text();
            var featureName = currentItem.getMember(nameIdColumn).get_text();
            if (typeId == 1 || typeId == 2) //Automatic
            {
                popupPath = "../../Popups/StoreManager/ManageAutoFeatures.aspx?FeatureId=" + featureId + "&FeatureName=" + featureName + "&FeatureTypeId=" + typeId;
            }
            else    //Manual
            {
                popupPath = "../../Popups/StoreManager/ManageManualFeatures.aspx?FeatureId=" + featureId + "&FeatureName=" + featureName;
            }
            featurePopup = dhtmlmodal.open('ShowFeaturePopup', 'iframe', popupPath, '', 'width=850px,height=545px,center=1,resize=0,scrolling=1');
            featurePopup.onclose = function () {
                var a = document.getElementById('ShowFeaturePopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function HidePopup() {
            featurePopup.hide();
        }
        function ViewProducts(productId, skuId) {
            HidePopup();
            window.location = jCommerceAdminSiteUrl + '/StoreManager/Products/GeneralDetails.aspx?SelectedProductSkuIds=' + skuId +
                            '&SelectedProductId=' + productId;
        }
        function ChangeStatus(currentItem) {
            var currentStatus = currentItem.getMember(statusColumn).get_text();
            var featureId = currentItem.getMember(uniqueIdColumn).get_text();
            //-- Call call back method to update status
            var strStatus = ChangeFeatureStatus(featureId, currentStatus);
            if (strStatus.toLowerCase() == 'true') {
                //-- Call Grid Reload here
                ShowStatusWise();
            }
            else {
                alert(strStatus);
            }
        }
        function SetStatus(chkStatusObject) {
            window["ShowInactive"] = chkStatusObject;
            ShowStatusWise();
        }
        function ShowStatusWise() {
            var actionColumnValue = "";
            var chkStatusObject = window["ShowInactive"];
            if (chkStatusObject != null && chkStatusObject.checked) {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';DisableDisplayOrderSort' + ';ShowInactive';
            }
            else {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';DisableDisplayOrderSort' + ';ShowActive';
            }
            LoadGrid(actionColumnValue);
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="StoreSetupContentHolder" runat="server">
    <div class="grid-utility clear-fix">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Filter %>"
                onclick="onClickSearchonGrid('txtSearch', grdFeatures);" />
        </div>
        <div class="columns-right">
            <asp:CheckBox ID="chkShowInactive" runat="server" Text="<%$ Resources:GUIStrings, IncludeInactiveFeatures %>"
                onclick="javascript:SetStatus(this);" />
        </div>
    </div>
    <ComponentArt:Menu ID="mnuFeatures" runat="server" SkinID="ContextMenu">
        <ClientEvents>
            <ItemSelect EventHandler="menu_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid ID="grdFeatures" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
        PagerInfoClientTemplateId="FeaturesPaginationTemplate" AllowEditing="true" AutoCallBackOnInsert="false"
        SliderPopupCachedClientTemplateId="FeaturesSliderTemplateCached" SliderPopupClientTemplateId="FeaturesSliderTemplate"
        AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True"
        CallbackCachingEnabled="true" AllowMultipleSelect="false" EditOnClickSelectedItem="false"
        LoadingPanelClientTemplateId="FeaturesLoadingPanelTemplate">
        <ClientEvents>
            <ContextMenu EventHandler="grid_onContextMenu" />
            <SortChange EventHandler="grid_OnSortChange" />
            <CallbackComplete EventHandler="grid_onCallbackComplete" />
            <Load EventHandler="grid_onLoad" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        EditControlType="Custom" EditCellServerTemplateId="svtName" HeadingText="<%$ Resources:GUIStrings, Name %>"
                        DataField="Title" Align="left" DataCellClientTemplateId="NameHoverTemplate" Width="370" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Description %>" DataField="Description"
                        Align="left" EditControlType="Custom" EditCellServerTemplateId="svtCode" DataCellClientTemplateId="CodeHoverTemplate"
                        Width="370" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Type %>" DataField="FeatureTypeTitle"
                        Align="left" AllowEditing="false" DataCellClientTemplateId="TypeHoverTemplate"
                        Width="150" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="StatusTitle" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="StatusHoverTemplate" Width="125" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        AllowSorting="False" HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left"
                        EditCellServerTemplateId="svtActions" EditControlType="Custom" Width="80" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn DataField="FeatureTypeId" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <div title="## DataItem.GetMember('Title').get_text() ##">
                    ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CodeHoverTemplate">
                <div title="## DataItem.GetMember('Description').get_text() ##">
                    ## DataItem.GetMember('Description').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Description').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <div title="## DataItem.GetMember('FeatureTypeTitle').get_text() ##">
                    ## DataItem.GetMember('FeatureTypeTitle').get_text() == "" ? "&nbsp;" : DataItem.GetMember('FeatureTypeTitle').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <div title="## DataItem.GetMember('StatusTitle').get_text() ##">
                    ## DataItem.GetMember('StatusTitle').get_text() == "" ? "&nbsp;" : DataItem.GetMember('StatusTitle').get_text()
                    ##
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FeaturesSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##
                    </p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdFeatures.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdFeatures.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FeaturesSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##
                    </p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdFeatures.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdFeatures.RecordCount)
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FeaturesPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdFeatures), pageCount(grdFeatures),
                grdFeatures.RecordCount) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FeaturesLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdFeatures) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="svtName">
                <Template>
                    <asp:TextBox CssClass="textBoxes" ID="txtName" runat="server" Width="220">
                    </asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtCode">
                <Template>
                    <asp:TextBox CssClass="textBoxes" ID="txtDescription" runat="server" Width="220">
                    </asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtActions">
                <Template>
                    <table>
                        <tr>
                            <td>
                                <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                    runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11"
                                    onclick="gridObject_BeforeUpdate(); return false;" />
                            </td>
                            <td>
                                <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                    src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </ComponentArt:Grid>
</asp:Content>
