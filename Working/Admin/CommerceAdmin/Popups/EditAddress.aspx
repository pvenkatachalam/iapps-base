﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="EditAddress.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.EditAddress" StylesheetTheme="General" %>

<%@ Register Src="~/UserControls/General/InternationalAddress.ascx" TagName="InternationalAddress" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <link rel="Stylesheet" media="all" href="../css/Popup.css" />
    <script type="text/javascript">
        function CloseAddressPopup(addressId, parentControlId) {
            if (addressId != null && addressId.length != 36) {
                addressId = "";
                parentControlId = "";
            }

            parent.CloseEditAddressPopup(addressId, parentControlId);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <div class="gridContainer">
        <div class="columns">
            <span class="formRow">
                <uc1:InternationalAddress ID="cntrlAddress" runat="server" />
            </span>
        </div>
    </div>
    <div id="Loading" style="display: none;" class="footerContent">
        <asp:Localize runat="server" Text="Processing" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClientClick="return CloseAddressPopup();" />
    <asp:Button ID="btnClear" runat="server" Text="Clear" ToolTip="Clear"
        CssClass="button" CausesValidation="false" OnClick="btnClear_Click" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" CausesValidation="true" OnClick="btnSave_Click" />
</asp:Content>
