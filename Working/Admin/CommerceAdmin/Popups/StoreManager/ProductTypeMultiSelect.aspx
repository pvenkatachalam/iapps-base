﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.Commerce.Search.ProductTypeMultiSelect"
    StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectProductTypes %>" />
    </title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        var selectedProductTypes = [];
        var selectedProductTypeName = [];

        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');

            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    trvProductTypes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CollapseTree %>'/>";
                }
                else if (treeHandle.className == 'collapse') {
                    trvProductTypes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ExpandTree %>'/>";
                }
            }

            return false
        }

        function OnNodeCheckChanged(sender, eventArgs) {
            var node = eventArgs.get_node();
            var productTypeId = node.ID;
            var productTypeTitle = node.Text;

            if (node.get_checked()) {
                selectedProductTypes[selectedProductTypes.length] = productTypeId;
                selectedProductTypeName[selectedProductTypeName.length] = productTypeTitle;
            }
            else {
                var idLocation = selectedProductTypes.indexOf(productTypeId);

                if (idLocation >= 0) {
                    selectedProductTypes.splice(idLocation, 1);
                }

                var titleLocation = selectedProductTypeName.indexOf(ProductTypesTitle);
                if (titleLocation >= 0) {
                    selectedProductTypeName.splice(titleLocation, 1);
                }
            }
        }

        function SetSelectedProductTypes() {
            if (selectedProductTypes.length > 0) {
                parent.SetProductTypes(selectedProductTypes, selectedProductTypeName);
                ClosePopup();
            }
            else {
                alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, PleaseSelectAtLeastOneProductType %>"/>');
            }
        }

        function ClosePopup() {
            parent.fn_HideProductType();

            return false;
        }
    
    </script>
</head>
<body>
    <form id="frmProductType" runat="server">
    <div class="iapps-modal AddAttribute">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:GUIStrings, ProductTypes %>" />
            </h2>
        </div>
        <div class="modal-content clear-fix">
            <div id="divPageTopContainer" class="messageContainer" runat="server">
            </div>
            <div class="tree-header">
                <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
            </div>
            <div class="tree-container">
                <ComponentArt:TreeView ID="trvProductTypes" runat="server" SkinID="Default" AutoScroll="false"
                    FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
                    <ClientEvents>
                        <NodeCheckChange EventHandler="OnNodeCheckChanged" />
                    </ClientEvents>
                </ComponentArt:TreeView>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
                class="button" onclick="ClosePopup();" />
            <input type="button" value="<%= GUIStrings.Add %>" title="<%= GUIStrings.Add %>"
                class="primarybutton" onclick="SetSelectedProductTypes();" />
        </div>
    </div>
    </form>
</body>
</html>
