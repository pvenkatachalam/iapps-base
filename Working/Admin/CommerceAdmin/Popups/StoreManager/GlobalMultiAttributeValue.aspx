﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/PopupMaster.Master" AutoEventWireup="true" CodeBehind="GlobalMultiAttributeValue.aspx.cs" StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.OrderMultiAttributeValue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
<script type="text/javascript">
    var htmlText;

    function PasteContentToEditor(oureditor) {
        oureditor.set_html(htmlText);
    }

    function SaveAndClose(hiddenId, editorId) {
        var htmlvalue = '';
        var content = oureditor.get_html();
        var htmlvalue = content;
        parent.SetTextValue(hiddenId, htmlvalue);
        ClosePopup();
        return false;
    }

    function LoadHiddenText(hiddenId, editorId) {
        var inputHtml = parent.document.getElementById(hiddenId).value;
        htmlText = ConvertToHtml(inputHtml);
        while (inputHtml != htmlText) {
            inputHtml = htmlText;
            htmlText = ConvertToHtml(inputHtml);
        }
        htmlText = inputHtml;
    }
    function ClosePopup() {
        parent.UpdateAttributeValues();
        parent.pagepopup.hide();
        return false;
    }

</script>
    <script language="javascript" type="text/javascript">
        var item;
        var productType;

        function GlobalAttributeValue_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            if (GlobalAttributeValue.Data.length == 1 && item.getMember('Value').get_value() == null || item.getMember('Value').get_value() == '' || item.getMember('Id').get_value() == 'Add New Value') {
                var i = 0;
                for (i = 0; i < cmGlobalAttributeValue.get_items().get_length(); i++) {
                    if (cmGlobalAttributeValue.GetItems()[i].get_id() != 'Add')
                        cmGlobalAttributeValue.GetItems()[i].Visible = false;

                }
            }
            else {

                var i = 0;
                for (i = 0; i < cmGlobalAttributeValue.get_items().get_length(); i++) {
                        cmGlobalAttributeValue.GetItems()[i].Visible = true;
                }

            }

            
            cmGlobalAttributeValue.showContextMenu(eventArgs.get_event());
        }

        function ContextMenuClickHandlerGlobalAttribute(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();

            if (selectedMenuId == "Edit") {
                GlobalAttributeValue.edit(item);
                gridOperation = "Edit";

            }
            else if (selectedMenuId == "Add") {
            if (item.getMember('Value').get_value() == '' || item.getMember('Id').get_value() == 'Add New Value') {
                GlobalAttributeValue.edit(item);
                gridOperation = "Insert";
            }
            else {
                GlobalAttributeValue.beginUpdate();
                GlobalAttributeValue.get_table().addEmptyRow(0);
                GlobalAttributeValue.editComplete();
                gridOperation = "Insert";
                item = GlobalAttributeValue.get_table().getRow(0);
                //item.SetValue(0,productType);
                GlobalAttributeValue.edit(item);
            }
            }
            else if (selectedMenuId == "Delete") {
                if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, YouAreAboutToDeleteTheValue %>' />" + item.getMember('Value').get_value())) {
                    var actionsColumnIndex = 2;
                    var action = "Delete"
                    item.SetValue(actionsColumnIndex, action);
                    GlobalAttributeValue.callback();
                }
            }
        }

        function GlobalAttributeValue_BeforeUpdate() {
            if (ValidateValue()) {
                GlobalAttributeValue.editComplete();
            }
            return true;
        }

        function GlobalAttributeValueCancelEditedValue() {
            GlobalAttributeValue_Cancel();
        }

        function GlobalAttributeValue_Cancel() {
            if (gridOperation == 'Insert') {
                GlobalAttributeValue.editCancel();
                GlobalAttributeValue.deleteItem(item);
                item = null;
                gridOperation = 'Canceled';
                return false;
            }
            else {
                GlobalAttributeValue.editCancel();
                gridOperation = 'Canceled';
            }
        }

        function GlobalAttributeValue_onCallbackComplete(sender, eventArgs) {
            if (selectedMenu != '') {
                selectedMenu = '';
            }
        }

        function GlobalAttributeValue_BeforeInsert(sender, eventArgs) {
            GlobalAttributeValue.editComplete();
        }

        function setTitleValue(control, DataField) {
            var txtName = document.getElementById(control);
            var txtNamevalue = item.GetMember(DataField).Value;
            if (txtNamevalue == null || txtNamevalue == 'null')
                txtName.value = '';
            else
                txtName.value = txtNamevalue;
        }

        function getTitleValue(control) {
            var txtName = document.getElementById(control);
            return [txtName.value, txtName.value];
        }

        function getOperationValue() {
            return [gridOperation, gridOperation];
        }

        function ValidateValue() {
            var valueValid = true;

            if (Trim(document.getElementById(txtNameClientID).value) == "") {
                valueValid = false;
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAStringValue %>' />");
            }

            var patt1 = new RegExp("^[a-zA-Z0-9. _'-.&!|,]+$");

            if (valueValid && !patt1.test(document.getElementById(txtNameClientID).value)) {
                valueValid = false;
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ValueCanHaveOnlyAlphanumericCharacters %>' />");
            }

            if (!ValidateUniqueValues())
                valueValid = false;

            return valueValid;
        }

        function ValidateUniqueValues() {
            var isUnique = true;
            var txtValue;
            var gridItem;
            if (document.getElementById(txtNameClientID).value != "") {
                txtValue = document.getElementById(txtNameClientID).value;
                for (i = 0; i < GlobalAttributeValue.get_table().getRowCount(); i++) {
                    gridItem = GlobalAttributeValue.get_table().getRow(i);
                    if (txtValue == gridItem.getMember('Value').Value) {
                        isUnique = false;
                        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ValueAlreadyAdded %>' />");
                        break;
                    }
                }
            }
            return isUnique;
        }
        function GlobalAttributeValue_OnCallbackError(sender, eventArgs) {
            alert(eventArgs.get_errorMessage());
        }
                </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager" runat="server" />
<div class="popupDarkBoxShadow MultiAttribute">
    <div class="popupDarkContainer">
        <div class="boxHeader">
            <h3><asp:label ID="lblHeading" runat="server" Text="<%$ Resources:GUIStrings, AddEditStrings %>"></asp:label></h3>
        </div>
        <div class="popupBoxContent">
            <div class="popupGrid" style="padding: 0;">
                <div class="mainContent">
                    <div class="gridContainer" style="padding:10px 5px 0 5px;">
                         <ComponentArt:Grid ID="GlobalAttributeValue" SkinID="default" runat="server" Width="100%"
                            CallbackCachingEnabled="false" Height="300" PageSize="10" RunningMode="Callback"
                            AutoCallBackOnInsert="true" EnableViewState="false" AllowTextSelection="true" EditOnClickSelectedItem="false"
                            AutoCallBackOnUpdate="true" AllowPaging="True" ItemDraggingEnabled="false" AllowMultipleSelect="false">
                            <ClientEvents>
                                <ContextMenu EventHandler="GlobalAttributeValue_onContextMenu" />
                                <CallbackError EventHandler="GlobalAttributeValue_OnCallbackError" />
                            </ClientEvents>
                            <Levels>
                                <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="Row"
                                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                                    HeadingCellHoverCssClass="HeadingCell" HeadingCellActiveCssClass="HeadingCell"
                                    AllowSorting="false" HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                                    SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif"
                                    SortDescendingImageUrl="desc.gif" SortedDataCellCssClass="msSortCell" AllowGrouping="false"
                                    AlternatingRowCssClass="AlternateDataRow">
                                    <Columns>
                                       <ComponentArt:GridColumn DataField="Id" Visible="false"/>                                        
                                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" DataField="Value"
                                            HeadingText="<%$ Resources:GUIStrings, Value %>" Align="Left" FixedWidth="true" EditCellServerTemplateId="svtName"
                                            EditControlType="Custom" AllowEditing="True" Width="350" Visible="true" />
                                        <ComponentArt:GridColumn AllowReordering="false" AllowSorting="False" IsSearchable="false"
                                            HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left" EditControlType="Custom" EditCellServerTemplateId="svtInfo"
                                            AllowEditing="True" Width="100" FixedWidth="true" />
                                    </Columns>
                                </ComponentArt:GridLevel>
                            </Levels>
                            <ServerTemplates>
                                <ComponentArt:GridServerTemplate ID="svtName">
                                    <Template>
                                        <asp:TextBox CssClass="textBoxes" ID="txtName" runat="server" Width="145"></asp:TextBox>&nbsp;
                                        <span class="required">*</span>
                                    </Template>
                                </ComponentArt:GridServerTemplate>                                
                                <ComponentArt:GridServerTemplate ID="svtInfo">
                                    <Template>
                                        <table>
                                            <tr>
                                                <td>
                                                    <img alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>" src="/iapps_images/cm-icon-add.png"
                                                        id="ImageButton11" runat="server" causesvalidation="true" onclick="javascript:return GlobalAttributeValue_BeforeUpdate();" />
                                                </td>
                                                <td>
                                                    <img runat="server" alt="<%$ Resources:GUIStrings, Delete %>" causesvalidation="false" title="<%$ Resources:GUIStrings, Cancel %>" src="/iapps_images/cm-icon-delete.png"
                                                        id="ImageButton2" onclick="GlobalAttributeValueCancelEditedValue();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                            </ServerTemplates>
                        </ComponentArt:Grid>
                        <ComponentArt:Menu ID="cmGlobalAttributeValue" runat="server" SkinID="contextMenu">
                            <Items>
                                <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, AddValue %>" ID="Add" Look-LeftIconUrl="cm-icon-add.png"></ComponentArt:MenuItem>                                
                                <ComponentArt:MenuItem LookId="BreakItem"></ComponentArt:MenuItem>
                                <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, EditValue %>" ID="Edit"  Look-LeftIconUrl="cm-icon-edit.png"></ComponentArt:MenuItem>
                                <ComponentArt:MenuItem LookId="BreakItem"></ComponentArt:MenuItem>
                                <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, DeleteValue %>" ID="Delete" Look-LeftIconUrl="cm-icon-delete.png">
                                </ComponentArt:MenuItem>
                            </Items>
                            <ClientEvents>
                                <ItemSelect EventHandler="ContextMenuClickHandlerGlobalAttribute" />
                            </ClientEvents>
                        </ComponentArt:Menu>
                    </div>
                    <div class="footerContent">
                    <div style="padding:10px">
                        <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" runat="server" CssClass="button" />
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
