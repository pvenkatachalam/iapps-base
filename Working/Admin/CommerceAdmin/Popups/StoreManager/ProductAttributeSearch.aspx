﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="ProductAttributeSearch.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductAttributeSearch"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.pagepopup.hide();
            return false;
        }

        var gridItem;
        function grdSearchResult_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            gridItem = eventArgs.get_item();
            if (Trim(gridItem.getMember('AttributeValString').get_text()) != '') {
                grdSearchResult.select(gridItem);
                mnuSearchResult.showContextMenuAtEvent(e);
            }
        }

        function mnuSearchResult_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId == 'cmSelect') {
                //parent.selectedLookupValue = gridItem.getMember('AttributeValue').get_text();
                var selectedValue = GetSelectedValue(gridItem);
                var attrValueForDisplay;
                if (gridItem.getMember("Title") != null)
                    attrValueForDisplay = gridItem.getMember("Title").get_text();
                else
                    attrValueForDisplay = selectedValue;

                parent.AssignValueToGridTextBox(selectedValue, attrValueForDisplay);
                ClosePopup();
            }
        }

        function GetSelectedValue(selectedGridItem) {
            var selectedValue = selectedGridItem.getMember('AttributeValue').get_text();
            if (selectedGridItem.get_table().get_columns()[0].DataType == 4) {
                var index = selectedValue.indexOf(" ");
                if (index > 0) {
                    selectedValue = selectedValue.substr(0, index);
                }
            }

            return selectedValue;
        }

      

        function AddValue() {
            var itemArray = grdSearchResult.getSelectedItems();
            var length = itemArray.length;
            var attrValue;
            var attrValueForDisplay;
            if (length > 0) {
                for (var i = 0; i < length; i++) {
                    //attrValue = itemArray[i].getMember("AttributeValue").get_text();              
                    attrValue = GetSelectedValue(itemArray[i]);
                    
                    if (itemArray[i].getMember("Title") != null)
                        attrValueForDisplay = itemArray[i].getMember("Title").get_text();
                    else
                        attrValueForDisplay = attrValue;
                }
                if (attrValue != '') {
                    parent.AssignValueToGridTextBox(attrValue, attrValueForDisplay);
                    ClosePopup();
                }
                else {
                    alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleseSelectTheValueValueCanNotBeEmpty %>" />');
                }
            }
            else {
                alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseSelectTheValue %>" />');
            }
        }

        $(function () {
            $("[id$='txtSearch']").bind("enterKey", function (e) {
                onClickSearchonGrid('txtSearch', grdSearchResult);
            });
            $("[id$='txtSearch']").keydown(function (e) {
                if (e.keyCode == 13) {
                    $(this).trigger("enterKey");
                    e.preventDefault();
                }
            });
        });

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="grid-utility clear-fix">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 200px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Search %>"
                onclick="onClickSearchonGrid('txtSearch', grdSearchResult);" />
        </div>
    </div>
    <div class="scroll-grid-container">
        <ComponentArt:Grid ID="grdSearchResult" SkinID="Default" runat="server" Width="400"
        ShowHeader="false" ShowFooter="false" Height="360" RunningMode="Callback" AutoPostBackOnSelect="false"
        EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AllowEditing="false" AutoCallBackOnUpdate="true"
        AllowMultipleSelect="false" EditOnClickSelectedItem="false">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false" ShowHeadingCells="false">
                <Columns>
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="HoverTemplate">
                <span title="## DataItem.getCurrentMember().get_text() ##">## DataItem.getCurrentMember().get_text()
                    == "" ? "&nbsp;" : DataItem.getCurrentMember().get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdSearchResultLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdSearchResult) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ClientEvents>
            <ContextMenu EventHandler="grdSearchResult_onContextMenu" />
        </ClientEvents>
    </ComponentArt:Grid>
    </div>
    <ComponentArt:Menu ID="mnuSearchResult" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmSelect" Text="<%$ Resources:GUIStrings, Select %>" Look-LeftIconUrl="cm-icon-add.png"
                Look-LeftIconWidth="17" Look-LeftIconHeight="14">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="mnuSearchResult_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button" onclick="parent.pagepopup.hide();" />
    <input type="button" value="<%= GUIStrings.Add %>" title="<%= GUIStrings.Add %>"
        class="primarybutton" onclick="AddValue();" />
</asp:Content>
