﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="AddEditLogin.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddEditLogin" StylesheetTheme="General"
    Title="<%$ Resources:GUIStrings, iAPPSCommerceAddEditAccountLogins %>" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">

        function GenerateGuid() {
            document.getElementById("<%=txtPassword.ClientID%>").value = GeneratePassword();
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
<asp:ValidationSummary runat="server" ID="valSummary"  ShowMessageBox="true" ShowSummary="false"/>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, IsActive %>" /></label>
        <div class="form-value">
            <asp:RadioButtonList ID="rblIsActive" runat="server" RepeatColumns="2" RepeatDirection="Horizontal"
                RepeatLayout="Flow">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Yes %>" Value="1"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:GUIStrings, No %>" Value="0"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, IsLocked %>" /></label>
        <div class="form-value">
            <asp:Label ID="lblIsLocked" Text="<%$ Resources:GUIStrings, Yes %>" runat="server"></asp:Label>
            <asp:CheckBox ID="chkIsLocked" runat="server" Text="<%$ Resources:GUIStrings, Unlock %>" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CSRSecurityQuestion %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtSecurityQuestion" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CSRSecurityAnswer %>" /></label>
        <asp:TextBox ID="txtSecurityAnswer" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, FirstName %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtFName" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, LastName %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtLName" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
        </div>
    </div>
    <div class="form-row" id="spnUserName" runat="server">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, UserName %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtUsername" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ControlToValidate="txtUsername"
                Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterUserName %>"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Email %>" /></label>
        <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
        <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtEmail" runat="server"
            ControlToValidate="txtEmail" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterEmail %>"></asp:RequiredFieldValidator>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Password %>" /> <br />
            <asp:LinkButton ID="lbtnGeneratePwd" runat="server" Text="<%$ Resources:GUIStrings, Generate %>"
                OnClientClick="return GenerateGuid();"></asp:LinkButton></label>
        <div class="form-value">
            <asp:TextBox ID="txtPassword" runat="server" CssClass="textBoxes" Width="200" TextMode="Password"
                AutoCompleteType="Disabled"></asp:TextBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SecurityLevels %>" /></label>
        <div class="form-value">
            <asp:ListBox ID="lstSecurityLevel" runat="server" Rows="3" SelectionMode="Multiple"
                Width="210"></asp:ListBox>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Cancel %>" title="<%= GUIStrings.Cancel %>"
        class="button" onclick="parent.accountLoginPopup.hide();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" OnClick="btnSave_Click" />
</asp:Content>
