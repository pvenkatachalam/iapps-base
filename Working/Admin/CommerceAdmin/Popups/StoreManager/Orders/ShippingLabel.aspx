﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShippingLabel.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.StoreManager.Orders.ShippingLabel" MasterPageFile="~/Popups/PopupMaster.Master"
    StylesheetTheme="General" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" media="all" href="../../../css/Popup.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="popupDarkBoxShadow ShipOrder">
        <div class="popupDarkContainer">
            <div class="boxHeader">
                <h3>
                    <asp:Label ID="lblHeading" runat="server" Text="Shipping Label"></asp:Label></h3>
            </div>
            <div class="popupBoxContent">
                <img id="imgLabel" runat="server" alt="Label not available" />
                <div class="footerContent">
                    <input type="button" value="<%= GUIStrings.Print %>" class="button" onclick="window.print();" />
                    <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
                        class="button" onclick="parent.CloseShippingLabelPopup();" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
