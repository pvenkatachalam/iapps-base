﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceCapturePayment %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="CaptureAmount.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CaptureAmount" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script language="javascript" type="text/javascript">
        function SelectDifferentAmount() {

            var rbtnDifferentAmnt = document.getElementById('<%=rbtnDifferentAmt.ClientID %>');
            var rbtnOriginalAmt = document.getElementById('<%=rbtnOriginalAmt.ClientID %>');
            rbtnDifferentAmnt.ena
            rbtnDifferentAmnt.checked = true;
            rbtnOriginalAmt.checked = false;
            toggleValidatorsNew(rbtnDifferentAmnt);
        }
        function toggleValidatorsNew(radioButton) {

            if (radioButton.checked) {
                var myVal = document.getElementById('<%=cvDollarAmount.ClientID %>');
                ValidatorEnable(myVal, true);
                var myVal2 = document.getElementById('<%=cvLessThanEqualTo.ClientID %>');
                ValidatorEnable(myVal2, true);

            }
        }
        function toggleValidatorsOriginal(radioButton) {
            if (radioButton.checked) {
                var myVal = document.getElementById('<%=cvDollarAmount.ClientID %>');
                ValidatorEnable(myVal, false);
                var myVal2 = document.getElementById('<%=cvLessThanEqualTo.ClientID %>');
                ValidatorEnable(myVal2, false);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <asp:ValidationSummary ID="vsSummary" runat="server" ValidationGroup="CaptureAmount"
            ShowSummary="false" ShowMessageBox="true" />
        <asp:Literal ID="ltWarning" runat="server"></asp:Literal>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:RadioButton ID="rbtnOriginalAmt" Checked="true" runat="server" onclick="toggleValidatorsOriginal(this);"
                Text="<%$ Resources:GUIStrings, PendingAmount %>" GroupName="Amount" /></label>
        <div class="form-value">
            <asp:Literal ID="lblOriginalAmount" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:RadioButton ID="rbtnDifferentAmt" GroupName="Amount" runat="server" onclick="toggleValidatorsNew(this);"
                Text="<%$ Resources:GUIStrings, DifferentAmount %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtDifferentAmt" runat="server" CssClass="textBoxes" onclick="SelectDifferentAmount();"
                Width="75"></asp:TextBox>
            <asp:CompareValidator CultureInvariantValues="true" Type="Double" ControlToValidate="txtDifferentAmt"
                ValidationGroup="CaptureAmount" runat="server" Operator="DataTypeCheck" ID="cvDollarAmount"
                ErrorMessage="<%$ Resources:GUIStrings, DifferentAmountMustBeADollarAmount %>" Text="*" />
            <asp:CompareValidator CultureInvariantValues="true" ID="cvLessThanEqualTo" ControlToValidate="txtDifferentAmt"
                ValidationGroup="CaptureAmount" runat="server" Type="Double" Operator="LessThanEqual"
                ErrorMessage="<%$ Resources:GUIStrings, DifferentAmountMustBeLessThanOrEqualToOriginalAmount %>"
                Text="*" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnApply" runat="server" Text="<%$ Resources:GUIStrings, Apply %>" ToolTip="<%$ Resources:GUIStrings, Apply %>"
        CssClass="primarybutton" CausesValidation="false" ValidationGroup="CaptureAmount"
        OnClick="btnApply_Click" />
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClientClick="parent.captureAmountPopup.hide();return false;" />
</asp:Content>
