﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceShipOrder %>" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master"
    AutoEventWireup="true" CodeBehind="CancelOrder.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CancelOrder"
    StylesheetTheme="General" EnableEventValidation="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var orderBalanceText = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderBalanceWillCaptureRemainingFundsOnAllOrderPayments %>" />';
        var thisShipmentText = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ThisShipmentWillCaptureOnlyFundsNeededToCoverThePriceOfAllItemsInTheShipment %>" />';
        var nothingText = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CaptureNothingWillShipCurrentItemsWithoutCapturingAnyFunds %>" />';
    </script>
    <script type="text/javascript">
        function ResetTextbox(chk, remQuantity, txt) {
            var txtControl = document.getElementById(txt);
            var chkControl = document.getElementById(chk);
            if (chkControl.checked)
                txtControl.value = remQuantity;
            else
                txtControl.value = "";
        }

        function CheckCheckbox(chk, remQuantity, txt) {
            var txtControl = document.getElementById(txt);
            var chkControl = document.getElementById(chk);
            if (parseFloat(txtControl.value) > parseFloat(remQuantity))
                txtControl.value = remQuantity;
            if (txtControl.value == remQuantity)
                chkControl.checked = true;
            else
                chkControl.checked = false;

        }

        function DeleteConfirm() {
            if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DoYouWantToDeleteThisPackage %>' />")) {
                return true;
            }
            return false;
        }

        function OnClickValidate() {
            Page_ClientValidate('Page');
            if (Page_IsValid) {
                //showLoadingDiv();
                return true;
            }
            else {
                return false;
            }
        }

        
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
        ValidationGroup="Page" />
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ShipTo %>" /></label>
        <asp:Label ID="lblShipTo" runat="server" Text=""></asp:Label>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="plain-grid">
        <tr class="heading-row">
            <th width="100">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Quantity %>" />
            </th>
            <th>
                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ProductName_SKUName %>" />
            </th>
            <th width="160">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, RemainingToBeCanceled %>" />
            </th>
        </tr>
        <asp:Repeater ID="rptOrderShipping" runat="server" OnItemDataBound="rptShipmentItems_ItemDataBound">
            <ItemTemplate>
                <tr class="<%#Container.ItemIndex % 2 == 0 ? "row" : "alternate-row" %>">
                    <td class="data-cell" style="text-align: center;">
                        <asp:TextBox ID="txtQuantity" runat="server" CssClass="textBoxes" Width="60" />
                        <asp:CompareValidator ID="cmpQuantityType" CultureInvariantValues="true" Type="Double"
                            ControlToValidate="txtQuantity" ValidationGroup="Page" runat="server" Operator="DataTypeCheck"
                            ErrorMessage="<%$ Resources:GUIStrings, QtyMustBeNumeric %>" Text="*" />
                        <asp:CompareValidator CultureInvariantValues="true" ID="cvLessThanEqualTo" ControlToValidate="txtQuantity"
                            ValidationGroup="Page" runat="server" Type="Double" Operator="LessThanEqual"
                            Text="*" />
                        <asp:CompareValidator CultureInvariantValues="true" ID="cvGreaterThanEqualTo" ControlToValidate="txtQuantity"
                            ValidationGroup="Page" runat="server" Type="Double" Operator="GreaterThanEqual"
                            ValueToCompare="0" Text="*" ErrorMessage="<%$ Resources:GUIStrings, NegativeQty %>" />
                    </td>
                    <td class="data-cell">
                        <asp:Label ID="lblProductSKU" runat="server"></asp:Label>
                    </td>
                    <td class="data-cell">
                        <asp:Label ID="lblRemainQuantity" runat="server"></asp:Label>
                        <asp:HiddenField ID="hfOrderItemId" runat="server" />
                        <asp:HiddenField ID="hfSkuId" runat="server" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancelOrder" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, CancelOrder %>"
        ToolTip="<%$ Resources:GUIStrings, CancelOrder %>" OnClientClick="return OnClickValidate();"
        OnClick="btnCancelOrder_Click" ValidationGroup="Page" />
    <input id="btnCancel" class="button" value="<%= GUIStrings.Cancel %>"
        title="<%= GUIStrings.Cancel %>" type="button" onclick="parent.CloseCancelOrderPopup('');" />
</asp:Content>
