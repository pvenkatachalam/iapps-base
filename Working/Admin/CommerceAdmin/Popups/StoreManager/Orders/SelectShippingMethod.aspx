﻿<%@ Page Title="<%$ Resources:GUIStrings, ShippingOptionPopup %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="SelectShippingMethod.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SelectShippingMethod" StylesheetTheme="General" EnableEventValidation="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function onCloseClick() {
            parent.CloseShippingMethodPopup();
        }
        function UnCheckOther(cntrlValue) {
            var selector = "form input:radio";
            $(selector).attr('checked', false);
            selector = "input[value=" + cntrlValue + "]";
            $(selector).attr('checked', true);
        }
        function CheckSelect() {
            var n = $("input:checked").length;
            if (n <= 0) {
                alert('Please select a value');
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="scrollable-area">
        <asp:Repeater runat="server" ID="rptShippers" OnItemDataBound="rptShippers_ItemDataBound">
            <ItemTemplate>
                <h3>
                    <%# Eval("Name") %></h3>
                <asp:RadioButtonList runat="server" ID="rblOptions">
                </asp:RadioButtonList>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <asp:Panel ID="pnlNotice" runat="server" Visible="false">
     <hr />
        <p class="notice clear-fix">
            <asp:Image ID="btnUPS" runat="server" ImageUrl="~/images/UPS_LOGO_L.gif" AlternateText=""
                Style="float: left; margin-right: 30px;" />
            <em>
                <%= GUIStrings.UPSTrademarkNotice %></em>
        </p>
    </asp:Panel>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button runat="server" Text="Select" ID="btnSelect" OnClick="btnSelect_OnClick"
        OnClientClick="return CheckSelect();" CssClass="primarybutton" />
    <asp:Button ID="Button1" OnClientClick="onCloseClick();" runat="server" Text="Cancel"
        CssClass="button" />
</asp:Content>