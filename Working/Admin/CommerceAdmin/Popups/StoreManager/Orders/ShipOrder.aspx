﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceShipOrder %>" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master"
    AutoEventWireup="true" CodeBehind="ShipOrder.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ShipOrder"
    StylesheetTheme="General" EnableEventValidation="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        /** Methods for the dynamic tooltip **/
        var offsetfromcursorX = 12; //Customize x offset of tooltip
        var offsetfromcursorY = 10; //Customize y offset of tooltip
        var tooltipImage = "../../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var orderBalanceText = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderBalanceWillCaptureRemainingFundsOnAllOrderPayments %>" />';
        var thisShipmentText = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ThisShipmentWillCaptureOnlyFundsNeededToCoverThePriceOfAllItemsInTheShipment %>" />';
        var nothingText = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CaptureNothingWillShipCurrentItemsWithoutCapturingAnyFunds %>" />';
    </script>
    <script type="text/javascript">
        function ResetTextbox(chk, remQuantity, txt) {
            var txtControl = document.getElementById(txt);
            var chkControl = document.getElementById(chk);
            if (chkControl.checked)
                txtControl.value = remQuantity;
            else
                txtControl.value = "";
        }

        function CheckCheckbox(chk, remQuantity, txt) {
            var txtControl = document.getElementById(txt);
            var chkControl = document.getElementById(chk);
            if (parseFloat(txtControl.value) > parseFloat(remQuantity))
                txtControl.value = remQuantity;
            if (txtControl.value == remQuantity)
                chkControl.checked = true;
            else
                chkControl.checked = false;

        }
        function GetShipmentTotal(cntrl, ItemId) {
            var newQnty = cntrl.value;
            //here we need to disable all postback buttons.
            // This doesn't work with update panel
//            var objShipOrderButton = document.getElementById('<%= btnShipOrder.ClientID %>');
            //            objShipOrderButton.disabled = true;
                cbCaptureAmount.callback(newQnty, ItemId);
        }

        function CaptureAmount_CallbackComplete(sender, eventArgs) {
            //here we need to enable all postback buttons.
//            var objShipOrderButton = document.getElementById('<%= btnShipOrder.ClientID %>');

//            objShipOrderButton.disabled = false;
        }
        function DeleteConfirm() {
            if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DoYouWantToDeleteThisPackage %>' />")) {
                return true;
            }
            return false;
        }

        function OnClickValidate() {
            Page_ClientValidate('Page');

            if (Page_IsValid) {
                var quantity = 0;
                var skuId = '';
                var quantityArray = new Array();
                var returnValue = true;
                var totalQuantity = 0;
                var shippingCount = 0;
                $("input[id$='txtQuantity']").each(function () {
                    if ($(this).val() != 'undefined' && $(this).val().trim() != '') {
                        quantity = parseInt($(this).val().trim());
                        skuId = $(this).attr('SKU');
                        if (quantityArray[skuId]) {
                            quantityArray[skuId] += quantity;
                        }
                        else
                            quantityArray[skuId] = quantity;
                        totalQuantity += quantity;
                    }
                    shippingCount++;
                });
                if (shippingCount==0) {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ZeroShippment %>' />");
                    returnValue = false;
                }
                else if (totalQuantity == 0) {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, QtyMustBeGreaterThanZero %>' />" );
                    returnValue = false;
                }
                $("span[id$='lblRemainQuantity']").each(function () {
                    if (returnValue) {
                        quantity = parseInt($(this).text());
                        skuId = $(this).attr('SKU');
                        if (parseInt(quantityArray[skuId]) > quantity) {
                            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, QtyMustBeLessThanOrEqualToMax %>' />" + quantity);
                            returnValue = false;

                        }
                    }
                });
            }
            else {
                returnValue = false;
            }
            return returnValue;
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
        ValidationGroup="Page" />
    <div class="columns clear-fix" style="width: 520px;">
        <asp:PlaceHolder ID="phWarehouse" runat="server">
            <div class="rows">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Warehouse %>" /></label>
                <asp:DropDownList runat="server" ID="ddlWarehouse" AutoPostBack="true" OnSelectedIndexChanged="ddlWarehouse_OnSelectedIndexChanged" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phCaptureOptions" runat="server">
            <div class="rows capture-row">
                <label class="form-label">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, PaymentCapture %>" /></label>
                <ComponentArt:CallBack ID="cbCaptureAmount" runat="server" OnCallback="cbCaptureAmount_Callback"
                    PostState="false" CssClass="capture-options" >
                    <Content>
                        <asp:PlaceHolder runat="server" ID="phOrderBalance">
                            <asp:RadioButton ID="rbOrderBalance" runat="server" GroupName="CaptureGroup" Checked="true"
                                Value="0" />&nbsp;<img src="../../../App_Themes/General/images/icon-tooltip.gif"
                                    alt="" onmouseover="ddrivetip(orderBalanceText, 200, '');positiontip(event);"
                                    onmouseout="hideddrivetip();" /><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="phThisShipment">
                            <asp:RadioButton ID="rbThisShipment" runat="server" GroupName="CaptureGroup" Value="1"
                                Enabled="true" />&nbsp;<asp:Literal ID="ltlUnallocatedCapture" runat="server" />&nbsp;<img
                                    src="../../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseover="ddrivetip(thisShipmentText, 200, '');positiontip(event);"
                                    onmouseout="hideddrivetip();" /><br />
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="phNothing">
                            <asp:RadioButton ID="rbNothing" runat="server" GroupName="CaptureGroup" Text="<%$ Resources:GUIStrings, NoPayment %>"
                                Value="2" />&nbsp;<img src="../../../App_Themes/General/images/icon-tooltip.gif"
                                    alt="" onmouseover="ddrivetip(nothingText, 200, '');positiontip(event);" onmouseout="hideddrivetip();" /><br />
                        </asp:PlaceHolder>
                    </Content>
                    <ClientEvents>
                        <CallbackComplete EventHandler="CaptureAmount_CallbackComplete" />
                    </ClientEvents>
                </ComponentArt:CallBack>
                <div class="clear-fix">
                </div>
            </div>
        </asp:PlaceHolder>
        <div class="rows">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ShipTo %>" /></label>
            <asp:Label ID="lblShipTo" runat="server" Text=""></asp:Label>
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <div class="columns">
        <div style="display: none; float: left; width: 75%">
            <label class="formLabel">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Via %>" /></label>
            <asp:DropDownList ID="ddlDeliveryType" runat="server" Width="180">
                <asp:ListItem Text="<%$ Resources:GUIStrings, SelectOne %>" Value=""></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:LinkButton ID="lbtnAddNewShipment" runat="server" Text="<%$ Resources:GUIStrings, AddNewShipment %>"
            CssClass="small-button" ToolTip="<%$ Resources:GUIStrings, ClickToAddNewShipment %>"
            OnClick="lbtnAddNewShipment_Click" OnClientClick="return OnClickValidate();" />
    </div>
    <div class="clear-fix">
    </div>
    <asp:Repeater ID="rptContainers" runat="server">
        <HeaderTemplate>
            <div style="overflow: auto; width: 100%; height: 400px;">
        </HeaderTemplate>
        <ItemTemplate>
            <h4><asp:Literal ID="lblPackageNumber" runat="server" /></h4>
            <div class="grid-utility">
                <div class="columns">
                    <label class="form-label">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Tracking %>" /></label>
                    <asp:TextBox ID="txtTrackingNumber" runat="server" Width="200" CssClass="textBoxes"></asp:TextBox>
                    <asp:Button ID="btnDeleteShipment" runat="server" CssClass="small-button" AlternateText="<%$ Resources:GUIStrings, DeletePackage %>"
                        CommandName="index" OnCommand="btnDeleteShipment_Click" Text="<%$ Resources:GUIStrings, DeletePackage %>"
                        Style="float: right;" OnClientClick="return DeleteConfirm();" />
                </div>
                <div class="clear-fix">
                </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="plain-grid">
                <tr class="heading-row">
                    <th width="100">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Quantity %>" />
                    </th>
                    <th>
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ProductName_SKUName %>" />
                    </th>
                    <th width="150">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, RemainingToBeShipped %>" />
                    </th>
                </tr>
                <asp:Repeater ID='rptShipmentItems' runat="server">
                    <ItemTemplate>
                        <tr class="<%#Container.ItemIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <td class="data-cell" style="text-align: center;">
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="textBoxes" Width="60" />
                                <asp:CompareValidator ID="cmpQuantityType" CultureInvariantValues="true" Type="Double"
                                    ControlToValidate="txtQuantity" ValidationGroup="Page" runat="server" Operator="DataTypeCheck"
                                    ErrorMessage="<%$ Resources:GUIStrings, QtyMustBeNumeric %>" Text="*" />
                                <asp:CompareValidator CultureInvariantValues="true" ID="cvLessThanEqualTo" ControlToValidate="txtQuantity"
                                    ValidationGroup="Page" runat="server" Type="Double" Operator="LessThanEqual"
                                    Text="*" />
                                <asp:CompareValidator CultureInvariantValues="true" ID="cvGreaterThanEqualTo" ControlToValidate="txtQuantity"
                                    ValidationGroup="Page" runat="server" Type="Double" Operator="GreaterThanEqual"
                                    ValueToCompare="0" Text="*" ErrorMessage="<%$ Resources:GUIStrings, NegativeQty %>" />
                            </td>
                            <td class="data-cell">
                                <asp:Label ID="lblProductSKU" runat="server"></asp:Label>
                            </td>
                            <td class="data-cell">
                                <asp:Label ID="lblRemainQuantity" runat="server"></asp:Label>
                                <asp:HiddenField ID="hfOrderItemId" runat="server" />
                                <asp:HiddenField ID="hfSkuId" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </ItemTemplate>
        <FooterTemplate>
            </div>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input id="btnCancel" class="button" value="<%= GUIStrings.Cancel %>"
        title="<%= GUIStrings.Cancel %>" type="button" onclick="parent.CloseShipOrderPopup('');" />
    <asp:Button ID="btnShipOrder" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, ShipOrder %>"
        ToolTip="<%$ Resources:GUIStrings, ShipOrder %>" OnClientClick="return OnClickValidate();"
        OnClick="btnShipOrder_Click" ValidationGroup="Page" />
</asp:Content>
