﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceTrackYourOrder %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="TrackOrder.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TrackOrder" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <link rel="Stylesheet" media="all" href="../../../css/Popup.css" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <div class="clear-fix">
        <div style="float: left; width: 48%;">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ShipDate %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="ltlShipDate" runat="server" /></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DeliveryDate %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="ltlDeliveryDate" runat="server" /></div>
            </div>
            <div class="form-row" id="dvDeliveredAt" runat="server" visible="false">
                <label class="form-label">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, DeliveredAt %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="ltDeliveredAt" runat="server" /></div>
            </div>
        </div>
        <div style="float: left; width: 48%; margin-left: 20px;">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, TrackingNumber %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="ltlTrackingNumber" runat="server" /></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Message %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="ltlMessage" runat="server" /></div>
            </div>
            <div class="form-row" id="dvshipmentOrigin" runat="server">
                <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ShipmentOrigin %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="lblOrigin" runat="server" />
                </div>
            </div>
            <div class="form-row" id="trSigRequired" runat="server" visible="false">
                <label class="form-label"><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, SignatureRequirement %>" /></label>
                <div class="form-value text-value">
                    <asp:Literal ID="lblSignatureRe" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <div style="max-height: 185px; overflow-y: auto;">
        <table cellpadding="0" cellspacing="0" width="600" border="0" class="plain-grid">
            <asp:Repeater ID="rptTrackEvent" runat="server" OnItemDataBound="rptTrackEvents_ItemDataBound">
                <HeaderTemplate>
                    <tr class="heading-row">
                        <th>
                            <strong>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DateTime %>" /></strong>
                        </th>
                        <th>
                            <strong>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Activity %>" /></strong>
                        </th>
                        <th>
                            <strong>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Location %>" /></strong>
                        </th>
                        <th>
                            <strong>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Details %>" /></strong>
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="<%# Container.ItemIndex % 2 == 0 ? "row" : "alternate-row" %>">
                        <td class="data-cell">
                            <asp:Literal ID="ltlDateTime" runat="server" />
                        </td>
                        <td class="data-cell">
                            <asp:Literal ID="ltlActivity" runat="server" />
                        </td>
                        <td class="data-cell">
                            <asp:Literal ID="ltlLocation" runat="server" />
                        </td>
                        <td class="data-cell">
                            <asp:Literal ID="ltlDetails" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div class="clear-fix">
        <div style="float: left; margin: 30px 30px 30px 0; width: 20%;">
            <asp:Image ID="imgUPS" runat="server" ImageUrl="~/images/UPS_LOGO_L.gif" AlternateText="" />
        </div>
        <p style="float: left; width: 75%; margin-top: 30px;" class="notice">
            <%= GUIStrings.UPSTrademarkNotice %>
        </p>
    </div>
    <asp:Label ID="lblTrackingDetails" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button" onclick="parent.ClosePopup();" />
</asp:Content>
