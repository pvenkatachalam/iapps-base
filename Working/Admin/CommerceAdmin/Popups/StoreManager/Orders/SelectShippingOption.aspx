﻿<%@ Page Language="C#" MasterPageFile="~/Popups/PopupMaster.Master" AutoEventWireup="true" CodeBehind="SelectShippingOption.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SelectShippingOption" StylesheetTheme="General" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" media="all" href="../../../css/Popup.css" />
    <script type="text/javascript">
        function CloseShippingOptionPopup() {
            parent.selectShippingOptionPopup.hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="popupDarkBoxShadow SelectShippingOption">
        <div class="popupDarkContainer">
            <div class="boxHeader">
                <h3><asp:Literal ID="ltHeading" runat="server" Text="<%$ Resources:GUIStrings, ShippingOptions %>"></asp:Literal></h3>
            </div>
            <div class="popupBoxContent">
                <div class="popupGrid" style="padding: 0;">
                    <div class="mainContent">
                        <div class="gridContainer">
                            <p><strong>UPS</strong></p>
                            <asp:RadioButtonList ID="rblShippingOptions" runat="server" RepeatDirection="Vertical">
                                <asp:ListItem Text="1"></asp:ListItem>
                                <asp:ListItem Text="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="footerContent">
                            <asp:Button ID="btnSelect" runat="server" Text="Select Option" CssClass="primarybutton" />
                            <input type="button" value="Cancel" class="button" onclick="CloseShippingOptionPopup();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
