﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceOrderStatusChangeReason %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="StatusChangeReason.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StatusChangeReason" StylesheetTheme="General" ValidateRequest="true" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <link rel="Stylesheet" media="all" href="../../../css/Popup.css" />
    <script type="text/javascript">
        function ClosePopup(emailSuccess, type) {
            if (emailSuccess == "0")
                alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, FailedToSendEmailNotification %>" />');
            parent.HideReasonPopup(type);
        }
        function SetStatusChanged() {

            if (parent.isOrderStatusChanged != 'undefined') {
                parent.isOrderStatusChanged = true;
            }
        }
        function ValidateNotes(type) {
            var notes = Trim(document.getElementById("<%=txtReason.ClientID%>").value);

            if (notes == '') {
                alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ReasonCanNotBeEmpty %>" />');
                return false;
            }
            else {
                if (notes.match(/[\<\>]/)) {
                    alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheReason %>" />');
                    return false;
                }
                else if (notes.match(/&#/)) {
                    alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseRemoveAnyCharactersFromTheReason %>" />');
                    return false;
                }
                else {
                    if (type == "1")
                        SetStatusChanged();
                    return true;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <div class="form-row">
        <div class="form-value">
            <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="447" Rows="10"></asp:TextBox><asp:RequiredFieldValidator
                CultureInvariantValues="true" ID="rfvtxtReason" runat="server" ControlToValidate="txtReason"
                Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheReason %>"></asp:RequiredFieldValidator>
            <br />
            <asp:CheckBox ID="cbkNotifiyCustomer" runat="server" Visible="false" Text="<%$ Resources:GUIStrings, NotifyCustomer %>" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" OnClick="btnSave_OnClick" runat="server" CausesValidation="true" />
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" ToolTip="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" runat="server" />
</asp:Content>
