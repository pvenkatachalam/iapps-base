﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PersonalizedSKUAttribute.aspx.cs"
    ValidateRequest="false" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.StoreManager.Orders.PersonalizedSKUAttribute"
    StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register Src="~/UserControls/StoreManager/Products/NewDynamicAttribute.ascx"
    TagName="DynamicAttributes" TagPrefix="uc3" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <link href="../../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        function ClosePopup() {
            parent.ClosePopupPersonalizedAttribute();
        }
        function CheckValidations() {
            if (!Page_ClientValidate('PageLevel')) {
                return false;
            }
        }

        function ClosePopupWithPriceUpdate(orderItemId, price) {
            parent.ClosePopupPersonalizedAttributeWithPriceUpdate(orderItemId, price);
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="personalized-attributes-container">
        <div runat="server" id="dvPersonalizedPrice" visible="false">
            <b>
                <asp:Label ID="lblPersonlalizedPrice" runat="server" Text="<%$ Resources:GUIStrings, SpecifyPriceValue %>"></asp:Label></b>
            <asp:TextBox ID="txtPersonlalizedPrice" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtListPrice"
                ControlToValidate="txtPersonlalizedPrice" ErrorMessage="<%$ Resources:GUIStrings, ListPriceIsRequired %>"
                Text="*"></asp:RequiredFieldValidator>
            <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="rngtxtListPrice"
                Type="Currency" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtPersonlalizedPrice"
                ErrorMessage="<%$ Resources:GUIStrings, ListPriceMustBeValidCurrency %>" Text="*" />
        </div>
        <uc3:DynamicAttributes ID="skulevelAttributes" ValidationGroup="PageLevel" ComboSkinID="default"
            runat="server" />
        <asp:Label ID="lblNoRecord" runat="server" Visible="false" Text="<%$ Resources:GUIStrings, PersonalizedAttributeNotAvailable %>"></asp:Label>
    </div>
    <asp:ValidationSummary ID="valSum" runat="server" EnableClientScript="true" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="PageLevel" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnCancel" class="button" name="btnCancel" title="<%= GUIStrings.Close %>"
        value="<%= GUIStrings.Close %>" onclick="ClosePopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, SaveClose %>"
        ToolTip="<%$ Resources:GUIStrings, SaveClose %>" CssClass="primarybutton" ValidationGroup="PageLevel" />
</asp:Content>
