﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceViewShipments %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ViewShipments.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewShipments" StylesheetTheme="General" %>

<%@ Register TagPrefix="ts" TagName="TrackShipment" Src="~/UserControls/StoreManager/Orders/TrackShipments.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function OpenTrackingPopup(trackingNumber, shipperProvider) {
            popupPath = "TrackOrder.aspx?TrackingNumber=" + trackingNumber + "&ShipperProvider=" + shipperProvider;
            trackOrderPopup = dhtmlmodal.open('TrackPackage', 'iframe', popupPath, '', 'width=500px,height=500px,center=1,resize=0,scrolling=1');
            trackOrderPopup.onclose = function () {
                var a = document.getElementById('TrackPackage');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }
        function ClosePopup() {
            trackOrderPopup.hide();
        }
        function OpenShippingLabelPopup(orderShippingId, orderShipmentId, orderShipmentPackageId) {
            popupPath = "ShippingLabel.aspx?OrderShippingId=" + orderShippingId + "&OrderShipmentId=" + orderShipmentId + "&OrderShipmentPackageId=" + orderShipmentPackageId;
            viewLabelPopup = dhtmlmodal.open('ViewLabel', 'iframe', popupPath, '', 'width=500px,height=500px,center=0,resize=0,scrolling=1');
            viewLabelPopup.onclose = function () {
                var a = document.getElementById('ViewLabel');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }

        function CloseShippingLabelPopup() {
            viewLabelPopup.hide();
        }

        function ClosePopup() {
            trackOrderPopup.hide();
        }
        function OpenTrackingLicense() {
            $('#noticePopupOverlay').show();
            $('#noticePopup').show();
        }
        function HideTrackingLicense() {
            $('#noticePopupOverlay').hide();
            $('#noticePopup').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Repeater runat="server" ID="rptShipments">
        <ItemTemplate>
            <ts:TrackShipment ID="ctlTrackShipmenty" runat="server" />
        </ItemTemplate>
    </asp:Repeater>
    <div id="noticePopup" class="iapps-modal clear-fix" style="display: none; z-index: 9999;
        position: absolute; top: 2%; left: 10%; width: 550px;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.UPTrackingNoticeHeading %></h2>
        </div>
        <div class="modal-content clear-fix">
            <%= GUIStrings.UPSTrackingNotice%>
        </div>
        <div class="modal-footer">
            <input type="button" value="<%= GUIStrings.Close %>" class="button"
                onclick="HideTrackingLicense();" />
        </div>
    </div>
    <div id="noticePopupOverlay" style="display: none; opacity: 0.5;background: none repeat scroll 0 0 #A8A8A8; bottom: 0; left: 0; position: fixed; right: 0; top: 0; z-index: 9998;">
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" class="button"
        title="<%= GUIStrings.Close %>" onclick="parent.CloseViewShipmentsPopup();" />
</asp:Content>
