﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceRefundPayment %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="RefundAmount.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.RefundAmount" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script language="javascript" type="text/javascript">
        function OnClickValidate() {
            if (typeof Page_ClientValidate == 'function') {
                Page_ClientValidate('valRefundAmount');
            } 
        }
        function SelectDifferentAmount() {
            var rbtnDifferentAmnt = document.getElementById('<%=rbtnDifferentAmt.ClientID %>');
            var rbtnOriginalAmt = document.getElementById('<%=rbtnOriginalAmt.ClientID %>');
            rbtnDifferentAmnt.ena
            rbtnDifferentAmnt.checked = true;
            rbtnOriginalAmt.checked = false;
            toggleValidatorsNew(rbtnDifferentAmnt);
        }
        function toggleValidatorsNew(radioButton) {
            if (radioButton.checked) {
                var myVal = document.getElementById('<%=cvDollarAmount.ClientID %>');
                ValidatorEnable(myVal, true);
                var myVal2 = document.getElementById('<%=cvLessThanEqualTo.ClientID %>');
                ValidatorEnable(myVal2, true);
            }
        }
        function toggleValidatorsOriginal(radioButton) {
            if (radioButton.checked) {
                var myVal = document.getElementById('<%=cvDollarAmount.ClientID %>');
                ValidatorEnable(myVal, false);
                var myVal2 = document.getElementById('<%=cvLessThanEqualTo.ClientID %>');
                ValidatorEnable(myVal2, false);
            }
        }
        function ClosePopup(message) {
            if (message != "") {
                parent.displayMessage(message);
            }

            parent.refundAmountPopup.hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="vsSummary" runat="server" ValidationGroup="valRefundAmount" ShowMessageBox="true" ShowSummary="false" />
    <asp:HiddenField ID="hdnOrderPaymentId" runat="server" />
    <asp:PlaceHolder ID="phSelectCard" runat="server" Visible="false">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, RefundPopup_CreditCard %>" /><span class="req">*</span></label>
            <div class="form-value">
                <asp:DropDownList runat="server" ID="ddlSelectCard" OnSelectedIndexChanged="ddlSelectCard_SelectedIndexChanged" Width="262"
                    AutoPostBack="true" CssClass="selectbox" />
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-multi-row">
        <label class="form-label">
            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, RefundPopup_Amount %>" /><span class="req">*</span>
        </label>
        <div class="form-value">
            <div style="float: left;">
                <p>
                <asp:RadioButton ID="rbtnOriginalAmt" Checked="true" runat="server" onclick="toggleValidatorsOriginal(this);"
                    Text="<%$ Resources:GUIStrings, PendingAmount %>" GroupName="Amount" />
                </p>
                <p style="padding: 5px 10px 5px 20px;"><asp:Label ID="lblOriginalAmount" runat="server"></asp:Label></p>
            </div>
            <div style="float: left;margin-left:10px;">
                <p>
                <asp:RadioButton ID="rbtnDifferentAmt" GroupName="Amount" runat="server" onclick="toggleValidatorsNew(this);"
                    Text="<%$ Resources:GUIStrings, DifferentAmount %>" />
                </p>
                <p style="padding: 5px 10px 5px 20px;">
                <asp:TextBox ID="txtDifferentAmt" runat="server" CssClass="textbox" onclick="SelectDifferentAmount();" Width="75" />
                <asp:CompareValidator CultureInvariantValues="true" Type="Double" ControlToValidate="txtDifferentAmt"
                    ValidationGroup="valRefundAmount" runat="server" Operator="DataTypeCheck" ID="cvDollarAmount"
                    ErrorMessage="<%$ Resources:GUIStrings, DifferentAmountMustBeADollarAmount %>" Text="*" />
                <asp:CompareValidator CultureInvariantValues="true" ID="cvLessThanEqualTo" ControlToValidate="txtDifferentAmt"
                    ValidationGroup="valRefundAmount" runat="server" Type="Double" Operator="LessThanEqual"
                    ErrorMessage="<%$ Resources:GUIStrings, DifferentAmountMustBeLessThanOrEqualToOriginalAmount %>"
                    Text="*" />
                </p>
            </div>  
        </div>
    </div>
    <div class="form-multi-row">
        <label class="form-label"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, RefundReason %>" /><span class="req">*</span></label>
        <div class="form-value">
            <asp:TextBox runat="server" ID="txtReason" TextMode="MultiLine" Rows="5" CssClass="textbox" Width="258" />
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtReason" ControlToValidate="txtReason"
                ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheRefundReason %>"
                runat="server" ValidationGroup="valRefundAmount"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div id="Loading" style="display: none;" class="footerContent">
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProcessingPayment %>" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClientClick="return ClosePopup('');" />
    <asp:Button ID="btnSubmit" runat="server" Text="<%$ Resources:GUIStrings, SubmitForApproval %>"
        ToolTip="<%$ Resources:GUIStrings, SubmitForApproval %>" CssClass="primarybutton"
        OnClientClick="OnClickValidate();" CausesValidation="false" ValidationGroup="valRefundAmount"
        OnClick="btnSubmit_Click" />
</asp:Content>