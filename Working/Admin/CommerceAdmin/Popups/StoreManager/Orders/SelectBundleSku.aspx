﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectBundleSku.aspx.cs" MasterPageFile="~/Popups/PopupMaster.Master"  
Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SelectBundleSku" StylesheetTheme="General" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="popupDarkBoxShadow AddAttribute">
        <div class="popupDarkContainer">
            <div class="boxHeader">
                <h3><asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:GUIStrings, SelectSKU %>"></asp:Label></h3>
            </div>
            <div class="popupBoxContent">
                <div class="popupGrid" style="padding: 0;">
                    <p>
                        <asp:Label ID="lblPageInfo" runat="server" Text=""></asp:Label>
                    </p>
                    <div class="mainContent">
                        <div class="gridContainer" style="padding: 10px 5px 0 5px;">
                            <span class="formRow">
                                <label class="formLabel"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectSKU1 %>" /></label>
                                <asp:DropDownList runat="server" ID="ddlItemSkus" Width="150" />
                            </span>
                        </div>
                        <div class="footerContent">
                            <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, AddSKUClose %>" ToolTip="<%$ Resources:GUIStrings, Close %>" CssClass="button" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>