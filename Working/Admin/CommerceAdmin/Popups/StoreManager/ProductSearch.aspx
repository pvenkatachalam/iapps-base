﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.Commerce.Search.ProductSearch"
    StylesheetTheme="General" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Bridgeline.FW.Commerce.Controls" Namespace="Bridgeline.FW.Commerce.Controls"
    TagPrefix="sku" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProductSearch %>" />
    </title>
    <link href="../../css/Popup.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .slider_rail
        {
            position: relative;
            height: 20px;
            width: 155px;
            background: #FFFFFF url(../../App_Themes/General/images/slider_bg.gif) repeat-x;
        }
        .slider_handle
        {
            position: absolute;
            height: 20px;
            width: 22px;
        }
        div.productThumbnails span
        {
            float: left;
            padding: 4px 2px;
        }
    </style>
    <script type="text/javascript">
        function GetDialogArguments() {
            var clientParameters = getRadWindow().ClientParameters; //return the arguments supplied from the parent page
            return clientParameters;
        }

        function getRadWindow() {
            if (window.radWindow) {
                return window.radWindow;
            }
            if (window.frameElement && window.frameElement.radWindow) {
                return window.frameElement.radWindow;
            }
            return null;
        }
        var isCallFromRadEditor;
        var blContinue = true;
        var canSelectImage = 'false';
        var skuSelectMessage = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseSelectAtLeastOneSKU %>" />';

        //This is for InsertProductImage
        function CloseRadEditorDlg(imageParams) {
            var workLink = new Object();
            //create an object and set some custom properties to it
            if (imageParams != null && imageParams != '') {
                var arrayImg = imageParams.split(',');
                if (arrayImg.length > 1) {
                    workLink.src = arrayImg[0];
                    workLink.altText = arrayImg[1];
                    if (arrayImg.length > 2)
                        workLink.objectId = arrayImg[2];
                }
                else {
                    workLink.src = imageParams;
                    workLink.altText = '';
                }
            }
            if (EditorToUse.toLowerCase() == 'radeditor') {
                var radObj = getRadWindow();
                if (radObj != null) radObj.close(workLink);
            }
            else if (EditorToUse.toLowerCase() == 'ckeditor') {
                parent.fn_InsertProudctImageInEditor(workLink);
            }
        }

        function onCloseClick() {
            parent.window["SelectedProducts"] = '';
            if (canSelectImage == 'true') {
                if (isCallFromRadEditor) {
                    CloseRadEditorDlg();
                }
                else {
                    parent.CloseProductImagePopup('');
                }
            }
            else {
                if (EditorToUse.toLowerCase() == 'ckeditor') {
                    parent.fn_InsertProudctImageInEditor();
                }
                else {
                    parent.CloseProductPopup('');
                }
            }
        }

        function toggle(checkBox, count) {

            if (count > 0)
                cb.checked = true;
            else
                cb.checked = false;
        }

        function toggleDisplay(element, productCheckBox) {

            var productCB = document.getElementById(productCheckBox);
            var div = document.getElementById(element);

            if (!productCB.checked && div.style.display == "block")
                blContinue = true;

            if (productCB.checked && div.style.display == "none" && !blContinue) {
                alert(skuSelectMessage);
                productCB.checked = false;
            }

            if (blContinue) {

                blContinue = false;
                if (div.style.display == "none") {
                    div.style.display = "block";
                }
                else {

                    var inputCollection = div.getElementsByTagName("input");

                    var count = 0;
                    for (i = 0; i < inputCollection.length; i++) {
                        if (inputCollection[i].checked)
                            count++;
                    }


                    if (count == 0 && productCB.checked) {
                        blContinue = false;
                        return false;
                    } else {
                        blContinue = true;
                        div.style.display = "none";

                    }
                }
            }

        }

        function toggleDisplayAndButton(element, image, productCheckBox) {

            var productCB = document.getElementById(productCheckBox);
            var div = document.getElementById(element);

            if (div.style.display == "none" && !blContinue) {
                alert(skuSelectMessage);
                productCB.checked = false;
            }
            else if (!blContinue) {
                alert(skuSelectMessage);
            }

            if (blContinue) {


                if (div.style.display == "none") {
                    div.style.display = "block";
                    image.src = image.src.replace("snap-maximize.gif", "snap-minimize.gif");

                }
                else {

                    var inputCollection = div.getElementsByTagName("input");

                    var count = 0;
                    for (i = 0; i < inputCollection.length; i++) {
                        if (inputCollection[i].checked)
                            count++;
                    }

                    if (count == 0 && productCB.checked) {
                        // alert('Please select at least one SKU');
                        blContinue = false;
                        return false;
                    } else {
                        blContinue = true;
                        div.style.display = "none";
                        image.src = image.src.replace("snap-minimize.gif", "snap-maximize.gif");
                    }
                }
            }

        }

        function skuChecked(checkList, productCheckBox, cbAll) {
            var checkBoxList = document.getElementById(checkList);
            var checkAll = document.getElementById(cbAll);
            var inputCollection = checkBoxList.getElementsByTagName("input");
            var cb = document.getElementById(productCheckBox);
            var count = 0;
            for (i = 0; i < inputCollection.length; i++) {
                if (inputCollection[i].checked)
                    count++;
            }
            if (count > 0) {
                blContinue = true;
                cb.checked = true;
            }
            else {
                cb.checked = false;
            }

            if (count == inputCollection.length)
                checkAll.checked = true;
            else
                checkAll.checked = false;

        }

        function CheckAllSkus(checkAll, checkList) {
            var selectAllCheckbox = document.getElementById(checkAll);
            var checkBoxList = document.getElementById(checkList);
            var inputCollection = checkBoxList.getElementsByTagName("input");
            for (i = 0; i < inputCollection.length; i++) {
                inputCollection[i].checked = selectAllCheckbox.checked;
            }
        }
        function UncheckAllSkus(checkAll, checkList) {
            var selectAllCheckbox = document.getElementById(checkAll);
            var checkBoxList = document.getElementById(checkList);
            var inputCollection = checkBoxList.getElementsByTagName("input");
            for (i = 0; i < inputCollection.length; i++) {
                inputCollection[i].checked = false;
            }
            selectAllCheckbox.checked = false;
        }

        function SetAddButton() {

            if (!Page_ClientValidate()) {
                if (document.getElementById(bntAddClientID) != null)
                    document.getElementById(bntAddClientID).disabled = true;
                if (document.getElementById(btnAddMoreClientID) != null)
                    document.getElementById(btnAddMoreClientID).disabled = true;
            }
            else {
                if (document.getElementById(bntAddClientID) != null)
                    document.getElementById(bntAddClientID).disabled = false;
                if (document.getElementById(btnAddMoreClientID) != null)
                    document.getElementById(btnAddMoreClientID).disabled = false;
                SetRefreshPanelId("SearchPanel");
            }

        }
        function validateSKUS() {
            if (!blContinue)
                alert(skuSelectMessage);
            return blContinue;
        }


        var hdnIsCallbackProductIdClientID;
        var btnRefreshUpdatePanelClientID;
        var dpImageSizeClientID;
        function ProductItemSelected(selectedProductId) {

            document.getElementById(hdnIsCallbackProductIdClientID).value = selectedProductId;
            __doPostBack(btnRefreshUpdatePanelClientID, "");
        }

        function ShowContextMenuOnProductImageThumb(imagecontrol, evt) {

            ThumbnailProductImageMenu.set_contextData(imagecontrol)
            ThumbnailProductImageMenu.showContextMenuAtEvent(evt, imagecontrol);
            return false;
        }
        function InsertProductImage(sender, eventArgs) {
            var itemSelected = eventArgs.get_item().get_id();
            if (itemSelected == "SelectProductImage") {
                var imageControl = ThumbnailProductImageMenu.get_contextData();
                var imageId = imageControl.getAttribute("imageid");
                var imageFileName = imageControl.getAttribute("FileName");
                var imageTitle = imageControl.getAttribute("Title");
                var imageDescription = imageControl.getAttribute("Description");
                var imageHeight = imageControl.getAttribute("ImageHeight");
                var imageWidth = imageControl.getAttribute("ImageWidth");
                var imageUrl = imageControl.getAttribute("ImageSrc");
                var imageAltText = imageControl.getAttribute("AltText");
                var selectedImageType = GetSelectedImageType();
                imageFileName = imageFileName.replace(new RegExp("systhumb_", "g"), selectedImageType);
                imageUrl = imageUrl.replace(new RegExp("systhumb_", "g"), selectedImageType);
                if (isCallFromRadEditor) {
                    CloseRadEditorDlg(imageUrl + "," + imageAltText.replace(/'/g, "\'") + "," + imageId);
                }
                else {
                    parent.SelectedImageInImageContainer(imageId, imageFileName, imageDescription, imageTitle, imageHeight, imageWidth, imageUrl, imageAltText);
                    setTimeout("parent.viewPopupWindow.hide();", 500);
                }
            }
        }

        function GetSelectedImageType() {
            var selectedImageTypePrefix = "";
            var radioButtons = document.getElementsByName(rblImageSizeClientID);
            for (var x = 0; x < radioButtons.length; x++) {
                if (radioButtons[x].checked) {
                    selectedImageTypePrefix = radioButtons[x].value;
                }
            }
            return selectedImageTypePrefix;
        }
    </script>
</head>
<script type="text/javascript" src="../../common/js/JSMessages.js.aspx"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<% if (HttpContext.Current.IsDebuggingEnabled)
   { %>
    <script type="text/javascript" src="../../script/General.js"></script>
<% }
   else
   { %>
<script type="text/javascript" src="../../script/iapps-commerce.min.js"></script>
<% } %>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="popupOverlay" id="popupOverlay">
        <table border="0" cellpadding="0" cellspacing="0" width="800">
            <tr>
                <td height="400" align="center">
                    <img runat="server" src="../../App_Themes/General/images/spinner.gif" alt="<%$ Resources:GUIStrings, Loading %>" />
                </td>
            </tr>
        </table>
    </div>
    <cc1:ToolkitScriptManager EnablePageMethods="true" ID="sm1" EnableScriptGlobalization="true"
        ScriptMode="Release" runat="server" />
    <div class="popupBoxShadow productSearch">
        <div class="popupboxContainer">
            <div class="dialogBox">
                <asp:HiddenField ID="hdnRefreshPanelId" runat="server" />
                <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <!-- Message Information STARTS -->
                        <div id="divPageTopContainer" class="messageContainer" runat="server">
                        </div>
                        <!-- Message Information ENDS -->
                        <div class="middleContent">
                            <div class="formArea">
                                <div class="productSearchBox">
                                    <asp:TextBox ID="txtSearchTerm" runat="server" CssClass="textBoxes"></asp:TextBox>
                                    <div style="float: left;">
                                        <asp:RequiredFieldValidator CultureInvariantValues="true" Display="Dynamic" ID="reqSearch"
                                            ControlToValidate="txtSearchTerm" runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterASearchTerm %>"
                                            ValidationGroup="SearchGroup">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ID="regExSearch" runat="server"
                                            ControlToValidate="txtSearchTerm" ValidationExpression="^[^<>()&]+$" ErrorMessage="<%$ Resources:GUIStrings, SearchTermContainsInvalidCharacters %>"
                                            Text="*" ValidationGroup="SearchGroup">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                    <asp:Button ID="btnSearch" OnClientClick="SetAddButton();" Text="<%$ Resources:GUIStrings, Search %>"
                                        runat="server" OnClick="btnSearch_Click" ValidationGroup="SearchGroup" CssClass="button" />
                                    <asp:ValidationSummary ID="valSumSearch" runat="server" EnableClientScript="true"
                                        ShowSummary="true" ValidationGroup="SearchGroup" />
                                </div>
                                <h3><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SearchAndAddProducts %>" /></h3>
                                <div class="clearFix">&nbsp;</div>
                                <asp:Panel ID="pnlNoResults" runat="server" Visible="false">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NoSearchResults %>" />
                                </asp:Panel>
                                <div class="searchResultContainer" style="height: 340px; overflow: auto;">
                                    <asp:Panel ID="pnlSearchResults" runat="server">
                                        <asp:GridView runat="server" ID="gvProducts" EnableViewState="true" CssClass="Grid"
                                            HeaderStyle-CssClass="HeadingRow" GridLines="Both" Width="100%" OnRowCommand="gvProducts_RowCommand"
                                            OnRowDataBound="gv_DataBound" AutoGenerateColumns="False" PagerSettings-Visible="true"
                                            PageSize="6" AllowPaging="true" AlternatingRowStyle-CssClass="AlternateDataRow"
                                            RowStyle-CssClass="Row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemStyle Width="50" CssClass="DataCell" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hfProductId" runat="server" />
                                                        <asp:CheckBox ID="cbProductSelected" runat="server" />
                                                        <asp:ImageButton ID="btnOpenSkus" runat="server" ImageUrl="~/images/snap-maximize.gif" />
                                                        <div class="SKULayer" id="divSKUS" runat="server" style="display: none; left: 50px;
                                                            text-align: left;">
                                                            <div style="width: 100%;" class="HeadingRow">
                                                                <b>
                                                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectSKU %>" /></b></div>
                                                            <asp:CheckBox ID="cbSelectAll" runat="server" Text="<%$ Resources:GUIStrings, CheckAll %>" />
                                                            <asp:Button ID="btnClose" Visible="false" runat="server" Text="Close" ImageAlign="Right"
                                                                CssClass="button" />
                                                            <FWControls:MultiTextFieldCheckBoxList ID="cblSKUS" runat="server" />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Thumbnails">
                                                    <ItemStyle CssClass="DataCell" />
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgProductThumbnail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Name %>" ItemStyle-CssClass="DataCell">
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; height: 100%;" onclick="__doPostBack('gvProducts$ctl01$btnSortName','')">
                                                            <asp:LinkButton ID="btnSortName" runat="server" CommandName="Sort" CommandArgument="Name"
                                                                Text="<%$ Resources:GUIStrings, Name %>" /></div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltlName" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Description %>">
                                                    <ItemStyle CssClass="DataCell" />
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; height: 100%;" onclick="__doPostBack('gvProducts$ctl01$bntSortDescription','')">
                                                            <asp:LinkButton ID="bntSortDescription" runat="server" CommandName="Sort" CommandArgument="Description"
                                                                Text="Description" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltlDescription" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Quantity %>" ItemStyle-CssClass="DataCell">
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; height: 100%" onclick="__doPostBack('gvProducts$ctl01$btnSortQuantity','')">
                                                            <asp:LinkButton ID="btnSortQuantity" runat="server" CommandName="Sort" CommandArgument="Quantity"
                                                                Text="<%$ Resources:GUIStrings, Quantity %>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltlQuantity" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                                <table cellpadding="1" cellspacing="1" width="100%">
                                                    <tr class="Row">
                                                        <td>
                                                            <asp:ImageButton ID="btnPrev" OnClientClick="return validateSKUS();" runat="server"
                                                                CommandName="Prev" onmouseout="this.src='../../App_Themes/General/images/prev.gif';"
                                                                onmouseover="this.src='../../App_Themes/General/images/prev_hover.gif';" ImageUrl="~/App_Themes/General/images/prev.gif" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtSlider" runat="server" AutoPostBack="True" Text='<%# gvProducts.PageIndex + 1 %>'
                                                                OnTextChanged="txtSlider_TextChanged" Width="200px"></asp:TextBox>
                                                            <cc1:SliderExtender ID="SliderExtender1" TooltipText="<%$ Resources:GUIStrings, Page0 %>"
                                                                EnableHandleAnimation="false" RailCssClass="slider_rail" HandleCssClass="slider_handle"
                                                                runat="server" Orientation="Horizontal" HandleImageUrl="~/App_Themes/General/images/slider_grip.gif"
                                                                TargetControlID="txtSlider" Minimum="1" Steps='<%# gvProducts.PageCount %>' Maximum='<%# ((GridView)Container.NamingContainer).PageCount %>'>
                                                            </cc1:SliderExtender>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton OnClientClick="return validateSKUS();" ID="btnNext" runat="server"
                                                                CommandName="Next" onmouseout="this.src='../../App_Themes/General/images/next.gif';"
                                                                onmouseover="this.src='../../App_Themes/General/images/next_hover.gif';" ImageUrl="~/App_Themes/General/images/next.gif" />
                                                        </td>
                                                        <td style="width: 99%; border-left: 0px; border-bottom: 0px; text-align: right;"
                                                            align="right" class="DataCell">
                                                            Page <b>
                                                                <asp:Literal ID="ltlPageIndex" Text='<%# gvProducts.PageIndex + 1 %>' runat="server" /></b>
                                                            of <b>
                                                                <asp:Literal ID="ltlPageTotal" runat="server" Text='<%# gvProducts.PageCount %>' /></b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                        </asp:GridView>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <%--<asp:DropDownList ID="dpImageSize" runat="server" Visible="false"/>--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="upImageThumbNail" runat="server">
                    <ContentTemplate>
                        <asp:Label CssClass="thumbnailSizeNote" ID="lblImageSize" Text="<%$ Resources:GUIStrings, SelectTheImageSizeAndRightClickTheImageYouWantToInsert %>"
                            runat="server" Visible="false" />
                        <asp:RadioButtonList ID="rblImageSize" runat="server" Visible="false" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                        </asp:RadioButtonList>
                        <asp:PlaceHolder ID="phImage" runat="server"></asp:PlaceHolder>
                        <asp:HiddenField ID="hdnIsCallbackProductId" runat="server" />
                        <asp:Button ID="btnRefreshUpdatePanel" runat="server" Text="<%$ Resources:GUIStrings, Refresh %>"
                            Style="visibility: hidden; height: 1px; width: 1px;" OnClientClick="SetRefreshPanelId('ImagePanel');" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="clearFix">&nbsp;</div>
                <div class="footerContent">
                    <asp:Button ID="btnAddMore" runat="server" CssClass="primarybutton" CausesValidation="false"
                        Text="<%$ Resources:GUIStrings, AddSearchForMore %>" OnClick="btnAddMore_Click" OnClientClick="return validateSKUS();" />
                    <asp:Button ID="bntAdd" runat="server" CssClass="button" CausesValidation="false"
                        Text="<%$ Resources:GUIStrings, AddClose %>" OnClick="btnAdd_Click" OnClientClick="return validateSKUS();" />
                    <asp:Button ID="btnClose" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Close %>"
                        OnClick="btnClose_Click" />
                </div>
            </div>
        </div>
    </div>
    <ComponentArt:Menu ID="ThumbnailProductImageMenu" SkinID="ContextMenu" ContextMenu="Custom"
        runat="server">
        <Items>
            <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, SelectImage %>" ID="SelectProductImage" />
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="InsertProductImage" />
        </ClientEvents>
    </ComponentArt:Menu>
    </form>
    <script type="text/javascript" defer="defer">
        function SetRefreshPanelId(refreshPanel) {
            document.getElementById("<%=hdnRefreshPanelId.ClientID%>").value = refreshPanel;
        }
        function init() {
            Sys.Net.WebRequestManager.add_invokingRequest(beginRequest);
            Sys.Net.WebRequestManager.add_completedRequest(endRequest);
        }
        function beginRequest(sender, args) {
            var objPopupOverlay = document.getElementById("popupOverlay");
            if (objPopupOverlay != null && objPopupOverlay != undefined) {
                objPopupOverlay.style.display = "block";
            }
        }
        function endRequest(sender, args) {
            try {
                var objPopupOverlay = document.getElementById("popupOverlay");
                if (objPopupOverlay != null && objPopupOverlay != undefined) {
                    objPopupOverlay.style.display = "none";
                }
            }
            catch (exp) {
            }
        }
        init();
    </script>
</body>
</html>
