﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageManualFeatures.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageManualFeature"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function CloseFeaturePopup() {
            parent.featurePopup.hide();
            return false;
        }

        function RefreshFeatureForDisplayOrder(chkDisplayOrder) {
            var rowitem = grdManualFeatures.get_table().addEmptyRow(0);
            var gridc = grdManualFeatures.get_table().get_columns();
            grdManualFeatures.AllowPaging = true;
            var actionColumnValue = "";
            window["EnableDisplayOrderSort"] = chkDisplayOrder;
            //Sorting Disable
            if (chkDisplayOrder.checked) {
                grdManualFeatures.get_levels()[0].set_hoverRowCssClass("selected-row");
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';EnableDisplayOrderSort';
                grdManualFeatures.set_externalDropTargets(gridObjectName);
                grdManualFeatures.ItemDraggingEnabled = 1;
                SetSortingValue(false);
            }
            else {
                grdManualFeatures.get_levels()[0].set_hoverRowCssClass('');
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';DisableDisplayOrderSort';
                grdManualFeatures.set_externalDropTargets(gridObjectName);
                grdManualFeatures.ItemDraggingEnabled = 0;
                SetSortingValue(true);
            }
            LoadGrid(actionColumnValue);
        }

        //var gridItem ;
        var productIdColumn = 5;
        var featureIdColumn = 6;
        function grdManualFeatures_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            var menuItems = mnuManualFeatures.get_items();
            gridItem = eventArgs.get_item();

            var prodId = gridItem.getMember('Id').get_text();
            if (prodId != '' && prodId != emptyGuid) {
                for (i = 0; i < menuItems.get_length() ; i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else  //only add
            {
                for (i = 0; i < menuItems.get_length() ; i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                        menuItems.getItem(i).set_visible(true);
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }

            mnuManualFeatures.showContextMenuAtEvent(e);
        }
        function mnuManualFeatures_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        ShowAddProductPopup();
                        break;
                    case 'cmDelete':
                        RemoveProduct();
                        break;
                    case 'cmView':
                        ViewProducts();
                        break;
                }
            }
        }
        var productSearchPopup;
        var searchPageURL;
        function ShowAddProductPopup() {
            popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx?ShowProducts=true";
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
            productSearchPopup.onclose = function () {
                var a = document.getElementById('ProductSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function CloseProductPopup(arrSelected) {
            if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                var productIds = '';
                for (k = 0; arrSelected.length; k++) {
                    if (k == 0) {
                        productIds = arrSelected.pop()[0];
                    }
                    else {
                        productIds = productIds + ',' + arrSelected.pop()[0];
                    }
                }
                if (productIds != '')
                    AddProducts(productIds);
            }
            productSearchPopup.hide();
        }
        function AddProducts(productIds) {
            var featureId = gridItem.getMember(featureIdColumn).get_text();
            var isAdded = AddProductsToFeature(featureId, productIds);
            if (isAdded.toLowerCase() == 'true') {
                ShowStatusWise();
            }
            else {
                alert(isAdded);
            }
        }
        function RemoveProduct() {
            var productId = gridItem.getMember(productIdColumn).get_text();
            var featureId = gridItem.getMember(featureIdColumn).get_text();
            var isRemoved = RemoveFromFeature(featureId, productId);
            if (isRemoved.toLowerCase() == 'true') {
                ShowStatusWise();
            }
            else {
                alert(isRemoved);
            }
        }
        function ViewProducts() {
            var productId = gridItem.getMember(uniqueIdColumn).get_text();
            var skuId = emptyGuid;
            parent.ViewProducts(productId, skuId);
        }

        function SetStatus(chkStatusObject) {
            window["ShowInactive"] = chkStatusObject;
            ShowStatusWise();
        }
        function ShowStatusWise() {
            var chkObj = window["EnableDisplayOrderSort"];
            var displayOrder;
            if (chkObj != null && chkObj.checked) {
                displayOrder = ";EnableDisplayOrderSort";
                grdManualFeatures.ItemDraggingEnabled = 1;
                grdManualFeatures.AllowPaging = false;
                SetSortingValue(false);
            }
            else {
                SetSortingValue(true);
                if (chkObj == null)   // object is not there
                {
                    displayOrder = ";DisableDisplayOrderSort";
                }
                else // object is there but not checked
                {
                    displayOrder = ";DisableDisplayOrderSort";
                }
            }

            var actionColumnValue = "";
            var chkStatusObject = window["ShowInactive"];
            if (chkStatusObject != null && chkStatusObject.checked) {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + displayOrder + ';ShowInactive';
            }
            else {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + displayOrder + ';ShowActive';
            }
            LoadGrid(actionColumnValue);
        }
    </script>
    <script type="text/javascript">
        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdManualFeatures;
                menuObject = mnuManualFeatures;
                gridObjectName = "grdManualFeatures";
                uniqueIdColumn = 5;
                operationColumn = 4;
                selectedTreeNodeId = emptyGuid;
            }, 500);
        }

        function PageLevelCallbackComplete() {
        }
        function PageLevelDragAndDrop(draggedItem, targetItem, fromId, toNewPosition) {
            var featureId = draggedItem.getMember(featureIdColumn).get_text();
            var isMoved = DragAndDropFeatureProduct(featureId, fromId, toNewPosition);
            return isMoved;
        }
    </script>
    <style type="text/css">
    #<%= grdManualFeatures.ClientID %>_VerticalScrollDiv
    {
        max-height:320px !important;
    }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div style="width: 1158px; height: 590px">
        <div class="grid-utility">
            <div class="columns">
                <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                    style="width: 200px;" />
                <input type="button" class="small-button" value="<%= GUIStrings.Search %>" onclick="onClickSearchonGrid('txtSearch', grdManualFeatures);" />
            </div>
            <div class="right-column">
                <asp:CheckBox ID="chkShowInactive" runat="server" Text="<%$ Resources:GUIStrings, IncludeInactiveProducts %>"
                    onclick="javascript:SetStatus(this);" />
                <asp:CheckBox ID="chkDisplayOrder" runat="server" Text="<%$ Resources:GUIStrings, EnableWebsiteDisplaySorting %>"
                    onclick="javascript:RefreshFeatureForDisplayOrder(this);" />
            </div>
            <div class="clear-fix"></div>
        </div>
        <ComponentArt:Menu ID="mnuManualFeatures" runat="server" SkinID="ContextMenu">
            <Items>
                <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddProduct %>" Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, RemoveFromFeature %>"
                    Look-LeftIconUrl="cm-icon-delete.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="cmView" Text="<%$ Resources:GUIStrings, ViewProductDetails %>">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="mnuManualFeatures_onItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
        <ComponentArt:Grid SliderPopupClientTemplateId="grdManualFeaturesSliderTemplate"
            ID="grdManualFeatures" SkinID="ScrollingGrid" runat="server" Width="1158" RunningMode="Callback"
            PagerInfoClientTemplateId="grdManualFeaturesSliderPageInfoTemplate"
            AllowEditing="true" AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="cachedgrdManualFeaturesSliderTemplate"
            CallbackCachingEnabled="true" AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10"
            PageSize="5" AllowPaging="True" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="dragTemplate"
            ExternalDropTargets="grdManualFeatures" AllowMultipleSelect="false" Height="500">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Sequence" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Thumbnail %>"
                            DataField="Thumbnail" AllowReordering="false" FixedWidth="true"
                            DataCellClientTemplateId="ImageTemplate" IsSearchable="false" />
                        <ComponentArt:GridColumn Width="580" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                            DataField="Name" TextWrap="true"
                            FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="NameHoverTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, Status %>"
                            DataField="IsActive" IsSearchable="true"
                            DataCellCssClass="LastDataCell" HeadingCellCssClass="LastHeadingCell" FixedWidth="true"
                            AllowReordering="false" DataCellClientTemplateId="StatusHoverTemplate" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" Visible="false"
                            HeadingText="Actions" EditControlType="Custom" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="FeatureId" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <ContextMenu EventHandler="grdManualFeatures_onContextMenu" />
                <SortChange EventHandler="grid_OnSortChange" />
                <CallbackComplete EventHandler="grid_onCallbackComplete" />
                <Load EventHandler="grid_onLoad" />
                <ItemExternalDrop EventHandler="grid_OnExternalDrop" />
                <RenderComplete EventHandler="SetVerticalScroll" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ImageTemplate">
                    <img src="## DataItem.GetMember('Thumbnail').get_text() ##" alt="" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                    <div title="## DataItem.GetMember('Name').get_text() ##">
                        ## DataItem.GetMember('Name').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Name').get_text()
                    ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                    <div title="## DataItem.GetMember('IsActive').get_text()  ##">
                        ## DataItem.GetMember('IsActive').get_text() ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdManualFeaturesSliderTemplate">
                    <div class="SliderPopup">
                        <p>
                            ##_datanotload##
                        </p>
                        <p class="paging">
                            <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdManualFeatures.PageCount)##</span>
                            <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdManualFeatures.RecordCount)##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="cachedgrdManualFeaturesSliderTemplate">
                    <div class="SliderPopup">
                        <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMemberAt(1).Value ##
                        </p>
                        <p class="paging">
                            <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdManualFeatures.PageCount)##</span>
                            <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdManualFeatures.RecordCount)##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdManualFeaturesSliderPageInfoTemplate">
                    ##stringformat(Page0Of12items, currentPageIndex(grdManualFeatures), pageCount(grdManualFeatures),
                grdManualFeatures.RecordCount)##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="dragTemplate">
                    <div style="height: 100px; width: 100px;">
                        ## DataItem.getMember('Name').get_text() ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdManualFeaturesLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdManualFeatures) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" ToolTip="<%$ Resources:GUIStrings, Close %>"
        CssClass="primarybutton" runat="server" OnClientClick="return CloseFeaturePopup();"
        UseSubmitBehavior="false" />
</asp:Content>