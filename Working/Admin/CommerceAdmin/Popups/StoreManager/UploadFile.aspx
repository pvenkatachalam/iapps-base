<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UploadAssetFile"
    Theme="General" CodeBehind="UploadFile.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, UploadFile %>" /></title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        function closeIframe() {
            var message = document.getElementById(lblErrorLabel).value;
            if (message == '<asp:localize runat="server" text="<%$ Resources:GUIStrings, FileHasBeenUploadedSuccessfully %>"/>' || message == '<asp:localize runat="server" text="<%$ Resources:GUIStrings, FileUpdatedSuccessfully %>"/>') {
                if (window.parent.fileName.value == null || window.parent.fileName.value == '') {
                    window.parent.FileCancelOperation();
                }
            }

            parent.HTMLEditorPopup.hide();
        }
        function setFileOnPath(objFile) {
            var txtVal = document.getElementById("hdnName").value;

            document.getElementById(txtVal).value = objFile.value;
        }
        function changeFileButton(objButtonCntrl) {
            var objButton = document.getElementById(objButtonCntrl)
            var buttonClass = objButton.className;
            var oldButtonClass = "";
            var newButtonClass = "";
            var buttonClassNum = "";
            if (buttonClass.indexOf("Hover") > -1) {
                buttonClassNum = buttonClass.substring(6, 7);
                newButtonClass = "button" + buttonClassNum;
                oldButtonClass = "button" + buttonClassNum + "Hover";
                objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
            }
            else {
                buttonClassNum = buttonClass.substring(6, 7);
                oldButtonClass = "button" + buttonClassNum;
                newButtonClass = "button" + buttonClassNum + "Hover";
                objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
            }
        }

        function checkFileExtension(elem, allowedExt, message) {
            var filePath = elem.value;
            if (filePath.indexOf('.') == -1)
                return false;
            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            if (ext == allowedExt)
                return true;
            alert(message);
            return false;
        }

        function CheckMediaFile() {

            var txtVal = document.getElementById("hdnName").value;
            var ctlfakeFileControlOnImage = document.getElementById(txtVal);
            if (typeof window.parent.IsUploadFileExist == 'function') {
                if (window.parent.IsUploadFileExist(ctlfakeFileControlOnImage.value)) {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AFileWithThatNameAlreadyExistsPleaseSelectADifferentFile %>' />");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }

        }
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
        <div id="dialog" class="iapps-modal uploadItems">
            <div class="modal-header clear-fix">
                <h2>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, UploadFile %>" /></h2>
            </div>
            <div class="modal-content clear-fix">
                <div id="divPageTopContainer" style="width:200px !important" class="messageContainer" runat="server">
                </div>
                <div class="fileUpload">
                    <div class="realFileInputlabel">
                        <asp:Label ID="ErrorLabel" runat="server" ForeColor="red" />
                    </div>
                    <div class="realFileInput">
                        <asp:FileUpload ID="fileUpload" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                            CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" />
                        <div class="fakeFile" style="display: none;">
                            <asp:TextBox ID="fakeFileControlOnImage" runat="server" ReadOnly="true" CssClass="textBoxes floatLeft"
                                Width="140"></asp:TextBox>
                            <asp:Button ID="btnBrowseOnImage" CssClass="button" Text="<%$ Resources:GUIStrings, Browse %>"
                                runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnUploadImage" runat="server" Text="<%$ Resources:GUIStrings, UploadFile %>"
                    CssClass="primarybutton" OnClick="uploadImage_Click" />
                <asp:Button ID="btnCancel" OnClientClick="closeIframe();return false;" runat="server"
                    Text="<%$ Resources:GUIStrings, Close %>" UseSubmitBehavior="false" CssClass="button buttonSpace" />
            </div>
            <asp:HiddenField ID="hdnMessage" runat="server" />
        </div>
        <asp:ValidationSummary ID="validationSummary" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <div class="validationControl">
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="RFVFileName" runat="server"
                ControlToValidate="fileUpload" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAFile %>"></asp:RequiredFieldValidator>
            <asp:PlaceHolder ID="phHiddenFields" runat="server"></asp:PlaceHolder>
        </div>
    </form>
</body>
</html>
