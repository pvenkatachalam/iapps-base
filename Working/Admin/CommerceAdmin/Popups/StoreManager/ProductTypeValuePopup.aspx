﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.Master"
    CodeBehind="ProductTypeValuePopup.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductTypeValuePopup"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    trvProductTypes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CollapseTree %>'/>";
                }
                else if (treeHandle.className == 'collapse') {
                    trvProductTypes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ExpandTree %>'/>";
                }
            }
            return false
        }

        function AddValue() {
            var selectedNode = trvProductTypes.get_selectedNode();
            if (selectedNode != null) {
                parent.AssignValueToGridTextBox(selectedNode.get_id(),selectedNode.get_text());
                ClosePopup();
            }
            else {
                alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseSelectAProductType %>" />');
            }

        }

        function ClosePopup() {
            parent.pagepopup.hide();
            return false;
        }
    
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="tree-header">
        <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
    </div>
    <div class="tree-container">
        <ComponentArt:TreeView ID="trvProductTypes" runat="server" SkinID="Default" AutoScroll="false"
            FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
            <ClientEvents>
            </ClientEvents>
        </ComponentArt:TreeView>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Add %>" title="<%= GUIStrings.Add %>"
        class="primarybutton" onclick="AddValue();" />
    <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button" onclick="ClosePopup();" />
</asp:Content>
