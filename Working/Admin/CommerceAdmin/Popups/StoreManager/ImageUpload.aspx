﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageUpload.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ImageUpload" StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ImageUpload %>" /></title>
    <link href="../../css/Popup.css" type="text/css" rel="Stylesheet" media="all" />
    <script type="text/javascript">
        function closePopup() {
            parent.RefreshGrid();
            parent.pagepopup.hide();
        }
    </script>
</head>
<body>
    <form id="frmUploadForm" runat="server" method="post">
        <div class="iapps-modal ImageUpload">
            <div class="modal-header">
                <h2><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ImageUpload %>" /></h2>
            </div>
            <div class="modal-content clear-fix">
                <script type="text/javascript">

                    var params = {};

                    var attributes = {
                        id: "flashfilesupload",
                        name: "flashfilesupload"
                    };

                    var flashvars = { "listViewButton.visible": "false",
                        "thumbViewButton.visible": "false",
                        "sortButton.visible": "false",
                        "uploadButton.visible": "false",
                        "language.autoDetect": "true",
                        formName: "<%=frmUploadForm.ClientID%>",
                        uploadUrl: "ProductImageProcessor.aspx",
                        useExternalInterface: "Yes",
                        "fileFilter.types": "*.jpg,*.jpeg,*.gif,*.png,*.bmp,*.tif|*.jpg:*.jpeg:*.gif:*.png:*.bmp:*.tif"
                    };

                    swfobject.embedSWF("ElementITMultiPowUpload3.0.swf", "flashObject", "325", "275", "10.0.0", "expressInstall.swf", flashvars, params, attributes);
    
                    function MultiPowUpload_onComplete(type, fileIndex)
                    {
                        window.alert(stringformat('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OfFileCompletedSuccessfully %>" />', type, Flash.fileList()[fileIndex].name));
                    }
                    function ged()
                    {
                        var randomnumber=Math.floor(Math.random()*11);
                        return randomnumber;
                    }
                    function mysubmit()
                    {
                        var Flash;
                        if(document.embeds && document.embeds.length>=1 && navigator.userAgent.indexOf("Safari") == -1)
                        {
                            Flash = document.getElementById("EmbedFlashFilesUpload");
                        }
                        else
                        {
                            Flash = document.getElementById("flashfilesupload");
                        }
                        var submitElement = document.getElementById("submitted");
                        submitElement.value="true";
                        // server side call;
                        var uniqeElement = GetUniqueUploadID();
                        document.getElementById("ProgressID").value = uniqeElement;
                        var selectedTreeNodeId = "";
                        Flash.uploadAll();
	                    return false;
                    }
                    // this function is the custom function added to the flash and is always called just when upload button is called.
                    // here the fileSize variable has been named wrongly - it actually refers to the number of files which are being uploaded.
                    // so do not use the numberOfFiles for any purpose
    //                                function downloadInfo(fileSize, numberOfFiles)
    //                                {
    //                                    if (fileSize > 0) {
    //                                        var progressId = document.getElementById("ProgressID").value;
    //                                        var winl = (screen.width - 300) / 2;
    //                                        var wint = (screen.height - 380) / 2;
    //                                        var winprops = 'height=' + 380 + ',width=' + 300 + ',top=' + wint + ',left=' + winl + 'resizable=0,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes';
    //                                        var strUrl = "UploadProgress.aspx?ProgressID=" + progressId + "&noOfFiles=" + fileSize;
    //                                        window.open(strUrl, "UploadProgress", winprops);
    //                                    }
    //                                    else {
    //                                        alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NoFilesHaveBeenSelectedForUpload %>" />');
    //                                    }
    //                                }
                </script>
                <div id="flashObject">
        
                </div>
                <script type="text/javascript">
                    // this is required because - sometimes flash does not get initialized in its normal way.
                    window.FlashFilesUpload = document.getElementById("flashfilesupload");
                </script>
            </div>
            <div class="modal-footer">
                <input name="Closebtn" onclick="closePopup();" type="button" value="<%= GUIStrings.Close %>" 
                    class="button" title="<%= GUIStrings.Close %>" />
                <input name="submitbtn" title="<%= GUIStrings.UploadFiless %>" onclick="return mysubmit();" type="button"
                    value="<%= GUIStrings.UploadFiless %>" class="primarybutton" />
                <input type="hidden" id="submitted" value="false" />
                <input type="hidden" id="Tick" runat="server" />
                <input type="hidden" id="ProgressID" runat="server" />
                <asp:HiddenField runat="server" ID="hdnProductId" />
            </div>
        </div>
        <iapps:CommerceStateControl ID="imageUploadState" runat="server"></iapps:CommerceStateControl>
    </form>
</body>
</html>
