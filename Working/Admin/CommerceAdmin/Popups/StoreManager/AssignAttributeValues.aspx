﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AssignAttributeValues.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AssignAttributeValues" StylesheetTheme="General" %>

<%@ Register TagPrefix="pa" TagName="ProductAttributes" Src="~/UserControls/StoreManager/Products/AttributeTree.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AssignAttributeValues %>" /></title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    treeAttributes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CollapseTree %>' />";
                }
                else if (treeHandle.className == 'collapse') {
                    treeAttributes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ExpandTree %>' />";
                }
            }
            return false;
        }
        function ClosePopup() {
            parent.pagepopup.hide();
            return false;
        }
        
        function AttributesTreeNodeSelect(sender, eventArgs)
        {
            var attributeId =  window["SelectedNodeId"] ;
            var callBackPTAttr = window["cbkAttributeValues"];
            if (callBackPTAttr!= null)
            {
                callBackPTAttr.Callback(attributeId);
            }
            //LoadProductTypeAttributes(productTypeId);
        }
        
        function SelectAllCheckBox(obj, allIds)
        {
        
           var chkBox =document.getElementById(obj.id);
           
            var callBackPTAttr = window["cbkAttributeValues"];
            selectedValues ='';
            if (callBackPTAttr!= null)
            {
                if(chkBox.checked)
                {
                    var tab = document.getElementById("chkAttributeValues");
                    for (var i = 1; i < tab.rows.length; i++)
                    {
                        var elem = document.getElementById("chkAttributeValues_"+ i);
                        elem.checked=true;
                    }
                    selectedValues = allIds;
                    //callBackPTAttr.Callback("SA");
                }    
                else
                {
                    var tab = document.getElementById("chkAttributeValues");
                    for (var i = 1; i < tab.rows.length; i++)
                    {
                        var elem = document.getElementById("chkAttributeValues_"+ i);
                        elem.checked=false;
                    }                
                    //callBackPTAttr.Callback("USA");
                }
            }
        }
        var selectedValues = '';
        function GetSelectedCheckBoxItems(itemObj, itemValue)
        {        
            //alert(itemObj);
            //alert(itemValue);
            if (itemObj.checked)
            {
                selectedValues = selectedValues + itemValue + ',';
            }
            else
            {
                selectedValues = selectedValues.replace((itemValue + ',') ,"");
            }
        }
        
        
        function SetSelectedItems()
        {
            var tab = document.getElementById("chkAttributeValues");
            var isValuesSelected = false;
            if (tab!=null)
            {
                for (var i = 1; i < tab.rows.length; i++)
                {
                    var elem = document.getElementById("chkAttributeValues_"+ i);
                    if (elem.checked)
                    {
                        //alert(document.getElementById("chkAttributeValues_span_"+ i).getAttribute("objId"));    
                        itemValue = document.getElementById("chkAttributeValues_span_"+ i).getAttribute("objId");
                        selectedValues = selectedValues.replace((itemValue + ',') ,""); //if already existing simply remove that
                        selectedValues = selectedValues + itemValue + ',';
                        isValuesSelected = true;
                    }
                }
            } 
            //alert(tab.rows.length);
            //if (!isValuesSelected && tab.rows.length > 0)
            //{
            //    alert("Select at least one attribute value");
            //    return false;
            //}        
            //alert(selectedValues);
            var hdnClientObj = document.getElementById(hdnClientId);
            hdnClientObj.value = selectedValues;
            //alert(hdnClientObj.value);
            hdnClientObj.text = selectedValues;
            //alert(hdnClientObj.text); 
            return true;         
        }
        
    </script>

</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="iapps-modal AssignAttribute">
        <div class="modal-header clear-fix">
            <h2><asp:Label ID="lblMsg" runat="server" Text="<%$ Resources:GUIStrings, AddAttributeValuesforProductType %>"></asp:Label></h2>
        </div>
        <div class="modal-content clear-fix">
            <p>
                <asp:Label ID="lblPageInfo" runat="server" Text=""></asp:Label>
            </p>
            <!-- Message Information STARTS -->
            <div id="divPageTopContainer" class="messageContainer" runat="server">
            </div>
            <!-- Message Information ENDS -->
            <!-- Left Column STARTS -->
            <div class="left-column">
                <h4><%= GUIStrings.AttributeGroupsAndAttributes %></h4>
                <div class="tree-header">
                    <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
                </div>
                <div class="tree-container" style="height: 300px;">
                    <pa:ProductAttributes ID="ctlProductAttributes" runat="server"></pa:ProductAttributes>
                </div>
            </div>
            <!-- Left Column ENDS -->
            <!-- Right Column STARTS -->
            <div class="right-column">
                <h4><%= GUIStrings.AttributeValues %></h4>
                <div class="content">
                    <asp:RadioButtonList ID="rblAttributeType" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Product %>" Value="Product" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, SKU %>" Value="SKU"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:CheckBox runat="server" ID="chkIsRequired" Text="IsRequired" />
                    <div class="checkboxContainer">
                        <ComponentArt:CallBack ID="cbkAttributeValues" runat="server" EnableViewState="true">
                            <Content>
                                <asp:CheckBoxList ID="chkAttributeValues" runat="server"></asp:CheckBoxList>
                            </Content>
                        </ComponentArt:CallBack>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnCancel" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" 
                runat="server" OnClientClick="return ClosePopup();" />
            <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                runat="server" OnClick="btnSave_Click" OnClientClick="return SetSelectedItems();" />
            <asp:HiddenField ID="selectedValueIds" runat="server" />
        </div>
    </div>
    </form>
</body>
<script type="text/javascript">
<%=cbkAttributeValues.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr><td><%= GUIStrings.Loading %></td></tr></table>';
</script>
</html>