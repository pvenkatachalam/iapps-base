﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" StylesheetTheme="General"
    AutoEventWireup="true" CodeBehind="DiscountAndRangesPriceSets.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.DiscountAndRangesPriceSets" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            if (ValidateOnClose()) {
                parent.disountPopup.hide();
                parent.RefreshThisPage();
            }
            return false;
        }

        function ValidateOnClose() {
            var len = gridObject.Data.length
            var minItem, maxItem, highestMax = 0;

            var gItem;

            for (var i = 0; i < len; i++) {
                gItem = gridObject.get_table().getRow(i);
                minItem = gItem.getMember(0).get_value();
                maxItem = gItem.getMember(1).get_value();

                if (maxItem == null || maxItem == 0) {
                    highestMax = 1234567890;
                }
                else {
                    if (highestMax < maxItem) {
                        highestMax = maxItem;
                    }
                }
            }
            if (highestMax != 1234567890) {
                if (!confirm(stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ThereWillNotBeAnyDiscountProvidedForMoreThanQuantity %>' />", highestMax))) {
                    return false;
                }
            }
            return true;
        }

        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdDiscountRanges;
                menuObject = mnuDiscountRanges;
                gridObjectName = "grdDiscountRanges";
                operationColumn = 3;
                uniqueIdColumn = 4;

            }, 500);
        }

        function PageLevelCallbackComplete() {


        }

        function PageLevelOnContextMenu(menuItems) {
            return true;
        }

        function PageLevelOnItemSelect(selectedMenuId, currentGridItem, sender, eventArgs) {

        }

        function PageLevelAddItem(currentGridItem) {
            return true;
        }

        function PageLevelEditItem(currentGridItem) {
            return true;
        }

        function PageLevelDeleteItem(currentGridItem) {
            //Call Callback method to delete.. 
            var discountRangeId = gridItem.getMember(4).get_text();
            var isResult = DeleteDiscountRangePriceSet(discountRangeId);
            if (isResult.toLowerCase() == "true") {
                isLoadPageAfterEachDelete = false;
            }
            return isResult;
        }



        function PageLevelSaveRecord() {

            return true;
        }

        function PageLevelCancelEdit() {
            return true;
        }

        function PageLevelGridValidation(currentGridItem) {
            if (Page_ClientValidate('ValGroup1') == false)
                return false;
            return ValidateMinMaxRange();
        }

        function ValidateMinMaxRange() {
            var currentMin, currentMax, currentDiscount;
            currentMin = parseFloat(document.getElementById(txtMinQty).value);
            currentDiscount = parseFloat(document.getElementById(txtNumericValue).value);

            if (currentMin == 0) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MinimumQtyShouldNotBeZero %>' />");
                return false;
            }
            if (currentDiscount > 100) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DiscountPercentageShouldBeLessThan %>' />");
                return false;
            }


            var len = gridObject.Data.length
            var minItem, maxItem;

            var gItem;

            if (document.getElementById(txtMaxQty).value != null && document.getElementById(txtMaxQty).value != '') {
                currentMax = parseFloat(document.getElementById(txtMaxQty).value);
            }

            if (currentMax != null && currentMin > currentMax) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MinimumQtyShouldBeLessThanMaximumQuantity %>' />");
                return false;
            }

            if (gridObject.Data.length == 1) {
                return true;
            }

            for (var i = 0; i < len; i++) {
                if (i != gridItem.Index) {
                    gItem = gridObject.get_table().getRow(i);
                    minItem = gItem.getMember(0).get_value();
                    maxItem = gItem.getMember(1).get_value()
                    if (currentMin >= minItem && currentMin <= maxItem) {
                        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MinimumQuantityConflictsWithExistingRange %>' />");
                        return false;
                    }
                    if (currentMax == null) {
                        currentMax = 1234567890; // it should be double max value
                    }
                    if (currentMax != null && currentMax >= minItem && currentMax <= maxItem) {
                        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, MaximumQuantityConflictsWithExistingRange %>' />");
                        return false;
                    }
                }
            }

            if (maxItem != null && maxItem != 0) //This is holding the value of last record 
            {
                if ((currentMin) > (maxItem + 1)) //!= (message come for 9-6)
                {
                    if (!confirm(stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, TheRangeBetweenMayBeMissing %>' />", (maxItem + 1), (currentMin)))) {
                        return false;
                    }
                }
            }

            return true;
        }
        
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:Menu ID="mnuDiscountRanges" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddDiscountRange %>"
                Look-LeftIconUrl="cm-icon-add.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmEdit" Text="<%$ Resources:GUIStrings, EditDiscountRange %>"
                Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, DeleteDiscountRange %>"
                Look-LeftIconUrl="cm-icon-delete.png">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="menu_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid ID="grdDiscountRanges" SliderPopupClientTemplateId="grdDiscountRangesSliderTemplate"
        SkinID="Default" runat="server" Width="500" RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        PagerInfoClientTemplateId="grdDiscountRangesSliderPageInfoTemplate" AllowEditing="true"
        SliderPopupCachedClientTemplateId="cachedgrdDiscountRangesSliderTemplate" CallbackCachingEnabled="true"
        AutoPostBackOnInsert="false" AutoPostBackOnSelect="false" AutoCallBackOnColumnReorder="false"
        AutoCallBackOnCheckChanged="false" AutoCallBackOnUpdate="false" EditOnClickSelectedItem="false"
        AutoCallBackOnInsert="false" CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True"
        AllowMultipleSelect="false" LoadingPanelClientTemplateId="grdDiscountRangesLoadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, MinQty %>"
                        EditControlType="Custom" DataField="MinQuantity" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="MinQtyHoverTemplate"
                        IsSearchable="false" EditCellServerTemplateId="SvtMinQty" Align="Right" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, MaxQty %>"
                        DataField="MaxQuantity" SortedDataCellCssClass="SortedDataCell" EditControlType="Custom"
                        FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="MaxQtyHoverTemplate"
                        IsSearchable="true" EditCellServerTemplateId="svtMaxQty" Align="Right" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, DiscountAmount %>"
                        DataField="DiscountAmount" IsSearchable="true" EditControlType="Custom" SortedDataCellCssClass="SortedDataCell"
                        Align="Right" FixedWidth="true" FormatString="N" AllowReordering="false" DataCellClientTemplateId="DiscountHoverTemplate"
                        EditCellServerTemplateId="svtDiscount" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                        AllowSorting="False" HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="Center"
                        DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell" EditCellServerTemplateId="svtActions"
                        EditControlType="Custom" Width="99" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grid_onContextMenu" />
            <SortChange EventHandler="grid_OnSortChange" />
            <CallbackComplete EventHandler="grid_onCallbackComplete" />
            <Load EventHandler="grid_onLoad" />
        </ClientEvents>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="SvtMinQty">
                <Template>
                    <asp:TextBox ID="txtMinQty" MaxLength="14" runat="server" CssClass="textBoxes" Width="90"></asp:TextBox>
                    <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rvMinQty" runat="server"
                        ControlToValidate="txtMinQty" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAMinimumQuantity %>"
                        ValidationGroup="ValGroup1"></asp:RequiredFieldValidator>
                    <asp:CompareValidator CultureInvariantValues="true" ID="RvtxtMinQty" ControlToValidate="txtMinQty"
                        runat="server" ValidationGroup="ValGroup1" Operator="DataTypeCheck" Type="Integer"
                        ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidMinimumQuantity %>" Text="*"></asp:CompareValidator>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtMaxQty">
                <Template>
                    <asp:TextBox ID="txtMaxQty" MaxLength="14" runat="server" CssClass="textBoxes" Width="90"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ID="RvtxtMaxQty" ControlToValidate="txtMaxQty"
                        runat="server" ValidationGroup="ValGroup1" Operator="DataTypeCheck" Type="Integer"
                        ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidMaximumQuantity %>" Text="*"></asp:CompareValidator>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtDiscount">
                <Template>
                    <asp:TextBox ID="txtDiscount" MaxLength="14" runat="server" CssClass="textBoxes"
                        Width="90"></asp:TextBox>
                    <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rvDiscount" runat="server"
                        ControlToValidate="txtDiscount" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterADiscountPercentage %>"
                        ValidationGroup="ValGroup1"></asp:RequiredFieldValidator>
                    <asp:CompareValidator CultureInvariantValues="true" ID="RvtxtDiscount" ControlToValidate="txtDiscount"
                        runat="server" ValidationGroup="ValGroup1" Operator="DataTypeCheck" Type="Double"
                        ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDiscountAmount %>" Text="*"></asp:CompareValidator>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtActions">
                <Template>
                    <table>
                        <tr>
                            <td>
                                <input type="image" alt="<%=JSMessages.Save %>" title="<%=JSMessages.Save %>"
                                    src="/iapps_images/cm-icon-add.png" id="ImageButton11" onclick="gridObject_BeforeUpdate();return false;" />
                            </td>
                            <td>
                                <img alt="<%= JSMessages.Cancel %>" title="<%=JSMessages.Cancel %>"
                                    src="/iapps_images/cm-icon-delete.png" id="Img4" onclick="CancelClicked();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="MinQtyHoverTemplate">
                <span title="## DataItem.GetMember('MinQuantity').get_text() ##">## DataItem.GetMember('MinQuantity').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('MinQuantity').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="MaxQtyHoverTemplate">
                <span title="## DataItem.GetMember('MaxQuantity').get_text() ##">## DataItem.GetMember('MaxQuantity').get_text()
                    == "" || DataItem.GetMember('MaxQuantity').get_text() == "0" ? "&nbsp;" : DataItem.GetMember('MaxQuantity').get_text()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DiscountHoverTemplate">
                <span title="## DataItem.GetMember('DiscountAmount').get_text() ##">## DataItem.GetMember('DiscountAmount').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('DiscountAmount').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdDiscountRangesSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdDiscountRanges.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdDiscountRanges.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cachedgrdDiscountRangesSliderTemplate">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdDiscountRanges.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdDiscountRanges.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdDiscountRangesSliderPageInfoTemplate">
                ##stringformat(Page0Of12items, currentPageIndex(grdDiscountRanges), pageCount(grdDiscountRanges),
                grdDiscountRanges.RecordCount)##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="dragTemplate">
                <div style="height: 100px; width: 100px;">
                    ## DataItem.getMemberAt(0).get_text() ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdDiscountRangesLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdDiscountRanges) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
        ShowSummary="False" ValidationGroup="ValGroup1" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" ToolTip="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" runat="server" OnClientClick="return ClosePopup();" UseSubmitBehavior="false" />
</asp:Content>
