﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceFindACustomer %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="SearchCustomer.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SearchCustomer" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var gridItem;
        var arrCustomerInfo = new Array();

        function grdCustomerSearch_onContextMenu(sender, eventArgs) {
            if (typeof (mnuCustomerSearch) != 'undefined') {
                var evt = eventArgs.get_event();
                gridItem = eventArgs.get_item();
                mnuCustomerSearch.showContextMenuAtEvent(evt);
            }
        }

        function mnuCustomerSearch_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId == 'cmSelect') {
                //alert(gridItem.getMember('UserId').get_text());
                parent.SelectedCustomerId = (gridItem.getMember('Id').get_text());
                if (typeof parent.SelectedCustomerName != "undefined") {
                    parent.SelectedCustomerName = stringformat('<asp:localize runat="server" text="<%$ Resources:GUIStrings, Fullname %>"/>', (gridItem.getMember('FirstName').get_text()), (gridItem.getMember('LastName').get_text()));
                }

                ClosePopup();

            }
        }

        function passSelectedCustomerInfo() {
            var gSelectedCustomerIds = "";
            var hdnSelectedCustomers = document.getElementById("hdnSelectedCustomers");
            if (hdnSelectedCustomers.value.length > 1) {
                gSelectedCustomerIds = hdnSelectedCustomers.value.substring(1, hdnSelectedCustomers.value.length - 1);
            }
            parent.UpdateSelectedCustomerInfo(gSelectedCustomerIds, arrCustomerInfo);
            ClosePopup();
            return false;
        }

        function ClosePopup() {
            //added to fix component art problem with popup
            setTimeout('parent.ClosePopupCustomerPopup();', 10);
            // return false;
        }

        function CancelPopup() {
            parent.SelectedCustomerId = parent.emptyGuid;
            ClosePopup();
        }

        function grdCustomerListing_onPageIndexChange() {
            grdCustomerSearch.set_callbackParameter(document.getElementById("<%=txtSearch.ClientID%>").value);
        }

        function grdCustomerListing_onSortChange() {
            grdCustomerSearch.set_callbackParameter(document.getElementById("<%=txtSearch.ClientID%>").value);
        }

        function toggleCustomerSelection(objCheckBox) {
            var custId = objCheckBox.value;
            var hdnSelectedCustomers = document.getElementById("hdnSelectedCustomers");
            if (objCheckBox.checked) {
                var hdnCustInfoId = "hdnCust" + custId;
                AddWithComma(hdnSelectedCustomers, objCheckBox.value);
                arrCustomerInfo[objCheckBox.value] = document.getElementById(hdnCustInfoId).value;
            }
            else {
                RemoveWithComma(hdnSelectedCustomers, objCheckBox.value);
            }
        }

        function grdCustomerListing_onCallbackComplete() {
            var hdnSelectedCustomerIds = document.getElementById("hdnSelectedCustomers").value;
            var arrCustomerIds = hdnSelectedCustomerIds.split(',');
            for (var i = 0; i < arrCustomerIds.length; i++) {
                if (arrCustomerIds[i] != "") {
                    var chkCustomer = document.getElementById("chkCust" + arrCustomerIds[i]);
                    if (chkCustomer != null) {
                        chkCustomer.checked = true;
                    }
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="grid-utility">
        <div class="columns">
            <asp:TextBox ID="txtSearch" runat="server" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
                CssClass="textBoxes" Width="240"></asp:TextBox>
            <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:GUIStrings, Go %>" CssClass="small-button"
                OnClick="btnSearch_Search" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <input type="text" id="hdnSelectedCustomers" class="hide" />
    <ComponentArt:Menu ID="mnuCustomerSearch" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmSelect" Text="<%$ Resources:GUIStrings, SelectCustomer %>"
                Look-LeftIconUrl="cm-icon-add.png" Look-LeftIconWidth="17" Look-LeftIconHeight="14">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="mnuCustomerSearch_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid SkinID="Default" ID="grdCustomerSearch" SliderPopupClientTemplateId="CustomerSearchSliderTemplate"
        SliderPopupCachedClientTemplateId="CustomerSearchSliderTemplateCached" Width="100%"
        runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoCustomersFound %>"
        AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" PagerInfoClientTemplateId="CustomerSearchPaginationTemplate"
        CallbackCachingEnabled="false" SearchOnKeyPress="true" ManualPaging="true"
        LoadingPanelClientTemplateId="CustomerSearchLoadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataCellClientTemplateId="SelectCustomerTemplate" Width="40" />
                    <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, CustomerName %>"
                        DataField="LastName" Align="Left" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="CustomerNameHoverTemplate" />
                    <ComponentArt:GridColumn Width="250" HeadingText="<%$ Resources:GUIStrings, CustomerEmail %>"
                        DataField="Email" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="EmailHoverTemplate" />
                    <ComponentArt:GridColumn DataField="AccountNumber" IsSearchable="false" Align="Left" Visible="false" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Address %>" Width="430" TextWrap="true"
                        DataField="AddressLine1" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AddressHoverTemplate" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="FirstName" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="City" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="State" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="Zip" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="StateCode" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdCustomerSearch_onContextMenu" />
            <PageIndexChange EventHandler="grdCustomerListing_onPageIndexChange" />
            <SortChange EventHandler="grdCustomerListing_onSortChange" />
            <CallbackComplete EventHandler="grdCustomerListing_onCallbackComplete" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="SelectCustomerTemplate">
                <input type="checkbox" id="chkCust## DataItem.GetMember('Id').get_text() ##" value="## DataItem.GetMember('Id').get_text() ##"
                    onclick="toggleCustomerSelection(this);" />
                &nbsp;
                <input type="hidden" id="hdnCust## DataItem.GetMember('Id').get_text() ##" value="## DataItem.GetMember('LastName').get_text()
                                        + '|' + DataItem.GetMember('FirstName').get_text()
                                        + '|' + DataItem.GetMember('AccountNumber').get_text()
                                        + '|' + DataItem.GetMember('Email').get_text() ##" />
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerNameHoverTemplate">
                <span title="## DataItem.GetMember('LastName').get_text() ##,&nbsp;## DataItem.GetMember('FirstName').get_text() ##">## DataItem.GetMember('LastName').get_text() == "" ? "&nbsp;" : DataItem.GetMember('LastName').get_text()
                    ## ## DataItem.GetMember('LastName').get_text() != "" ? ",&nbsp;" : "" ## ## DataItem.GetMember('FirstName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('FirstName').get_text() ## </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="AccountNumberHoverTemplate">
                <span title="## DataItem.GetMember('AccountNumber').get_text() ##">## DataItem.GetMember('AccountNumber').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('AccountNumber').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EmailHoverTemplate">
                <span title="## DataItem.GetMember('Email').get_text() ##">## DataItem.GetMember('Email').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Email').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="AddressHoverTemplate">
                <span title="## DataItem.GetMember('AddressLine1').get_text() ##,&nbsp;## DataItem.GetMember('City').get_text() ## ,&nbsp;## DataItem.GetMember('State').get_text() ##,&nbsp;## DataItem.GetMember('Zip').get_text() ##">## DataItem.GetMember('AddressLine1').get_text() == "" ? "&nbsp;" : DataItem.GetMember('AddressLine1').get_text()
                    ## ## DataItem.GetMember('AddressLine1').get_text() != "" ? ",&nbsp;" : "" ## ##
                    DataItem.GetMember('City').get_text() == "" ? "&nbsp;" : DataItem.GetMember('City').get_text()
                    ## ## DataItem.GetMember('City').get_text() != "" ? ",&nbsp;" : "" ## ## DataItem.GetMember('StateCode').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('StateCode').get_text() ## ## DataItem.GetMember('Zip').get_text()
                    != "" ? ",&nbsp;" : "" ## ## DataItem.GetMember('Zip').get_text() == "" ? "&nbsp;"
                    : DataItem.GetMember('Zip').get_text() ## </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerSearchSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##
                    </p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdCustomerSearch.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdCustomerSearch.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerSearchSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##
                    </p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdCustomerSearch.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdCustomerSearch.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerSearchPaginationTemplate">
                ##stringformat(Page0Of12items, currentPageIndex(grdCustomerSearch), pageCount(grdCustomerSearch),
                grdCustomerSearch.RecordCount)##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerSearchLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdCustomerSearch) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" runat="server" OnClientClick="return CancelPopup();"
        UseSubmitBehavior="false" />
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Add %>" ToolTip="<%$ Resources:GUIStrings, Add %>"
        CssClass="primarybutton" runat="server" OnClientClick="return passSelectedCustomerInfo();"
        UseSubmitBehavior="false" Visible="false" />
</asp:Content>
