﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    StylesheetTheme="General" CodeBehind="SuggestedAddresses.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SuggestedAddresses" %>

<%@ Register Src="~/UserControls/StoreManager/Customers/SelectSuggestedAddress.ascx"
    TagName="SelectSuggestedAddress" TagPrefix="uc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function onCloseClick() {
            parent.CloseShippingMethodPopup();
        }
        function UseMyAddressClick() {
            if (parent.hdnIsValidatedClientID != null && parent.hdnIsValidatedClientID != 'undefined') {
                parent.document.getElementById(parent.hdnIsValidatedClientID).value = 1;
            }
            parent.DisplayAddressAndClose("", 1);
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <uc1:SelectSuggestedAddress ID="SelectSuggestedAddress1" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" OnClientClick="javascript:parent.CloseAddressSuggestionPopup()" Text="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" />
    <asp:Button ID="btnSelectSuggestion" runat="server" Text="<%$ Resources:GUIStrings, Select %>" OnClick="btnSelectSuggestion_Click" CssClass="primarybutton" />
</asp:Content>
