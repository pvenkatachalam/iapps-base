﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddFacet.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddFacet"
    StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AddFacet %>" /></title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        var _cancel = '<asp:Localize runat="server" Text="<%$ Resources:JSMessages, Cancel %>"/>';
        var _save = '<asp:Localize runat="server" Text="<%$ Resources:JSMessages, Save %>"/>';
        //------------------- For Calender in grid - Start---------------------------
        var iframeObject = null;
        function CalendarTo_OnChange(sender, eventArgs) {
            var selectedDate = CalendarTo.getSelectedDate();
            var txtBoxObject = document.getElementById(txtToDate);
            txtBoxObject.value = CalendarTo.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
        }

        function popUpToCalendar() {
            var txtToTextBox = document.getElementById(txtToDate);
            if (txtToTextBox.disabled == false) {
                var thisDate = iAppsCurrentLocalDate;
                //var txtToTextBox = document.getElementById(textBoxId);

                if (Trim(txtToTextBox.value) == "<asp:Localize runat='server' text='<%$ Resources:GUIStrings, ToDate %>'/>" || Trim(txtToTextBox.value) == "") {
                    txtToTextBox.value = "";
                }
                if (txtToTextBox.value != "<asp:Localize runat='server' text='<%$ Resources:GUIStrings, ToDate %>'/>" && Trim(txtToTextBox.value) != "") {
                    if (validate_dateSummary(txtToTextBox) == true)// available in validation.js 
                    {
                        thisDate = new Date(txtToTextBox.value);
                        CalendarTo.setSelectedDate(thisDate);
                    }
                }
                else {
                    //thisDate.setDate(thisDate.getDate() -1);
                    //CalendarTo.setSelectedDate(thisDate);
                }
                if (!(CalendarTo.get_popUpShowing()));
                CalendarTo.show();
            }
        }

        //--------------- For Calender in grid -- End --------------------------------

        var item;
        var selectAction;
        var rowCount = '';
        var emptyGuid = '00000000-0000-0000-0000-000000000000';
        var isnew = false;
        var isDragEnabled = false;
        function ClosePopup() {
            parent.pagepopup.hide();
            return false;
        }

        /** Methods to set the pagination details **/
        function setFacetValue(control, DataField) {
            var value = item.GetMember(DataField).Value;
            if (value != null) {
                var txtControl = document.getElementById(control);
                txtControl.value = value;
                txtControl.value = Trim(txtControl.value.replace(/&lt;/g, '<').replace(/&gt;/g, '>'));

                if (txtControl.value == null || txtControl.value == 'null')
                    txtControl.value = '';
            }
        }

        function getFacetValue(control, DataField) {
            var txtControl = document.getElementById(control);
            var facetValue = Trim(txtControl.value.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
            return [facetValue, facetValue];
        }

        function setFacetValueCal(control, DataField) {
            var data = item.GetMember(DataField).get_object();
            var facetDate = new Date(data);
            if (facetDate == "" || facetDate == null || data == undefined) {
                var curDate = iAppsCurrentLocalDate;
                facetDate = new Date(curDate);
            }
            var expDate;
            if (control == "Picker1") {
                Picker1.setSelectedDate(facetDate);
            }
            else if (control == "Picker2") {
                Picker2.setSelectedDate(facetDate);
            }
            else if (control == "txtToDate") {

                var txtToTextBox = document.getElementById(txtToDate);
                if (data == "" || dateFormat == null || data == undefined)
                    txtToTextBox.value = "";
                else
                    txtToTextBox.facetDate = Calendar1.formatDate(facetDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            }

        }

        function getFacetValueCal(control, DataField) {
            var facetDate;
            if (control == "Picker1") {
                facetDate = Picker1.formatDate(Picker1.getSelectedDate(), shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            }
            else if (control == "Picker2") {
                facetDate = Picker2.formatDate(Picker2.getSelectedDate(), shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            }
            else if (control == "txtToDate") {

                var txtToTextBox = document.getElementById(txtToDate);
                if (txtToTextBox.value == "<asp:Localize runat='server' text='<%$ Resources:GUIStrings, ToDate %>'/>")
                    facetDate = "";
                else
                    facetDate = txtToTextBox.value;
            }

            if (facetDate == "" || facetDate == null || facetDate == undefined) {
                //var curDate = iAppsCurrentLocalDate;
                //facetDate = curDate;
                facetDate = "";
            }

            return [facetDate, facetDate];
        }

        function getFacetOperation() {
            return [currentAction, currentAction];
        }

        function grdFacetsWithText_onItemSelect(sender, eventArgs) {

        }

        function grdFacetsWithCal_onItemSelect(sender, eventArgs) {

        }

        function grdFacetsWithText_onContextMenu(sender, eventArgs) {

            CancelFacetUpdate();
            var e = eventArgs.get_event();
            item = eventArgs.get_item();

            factRangeId = item.getMember(5).get_text();
            var menuItems = mnuFacetsWithText.get_items();
            if (factRangeId != '' && factRangeId != emptyGuid && factRangeId != 0) //only add
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() != 'cmAdd') {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }

            mnuFacetsWithText.showContextMenuAtEvent(e);
        }

        function mnuFacetsWithText_onItemSelect(sender, eventArgs) {
            var itemSelected = eventArgs.get_item().get_id();
            switch (itemSelected) {
                case "cmAdd":
                    AddFacetText();
                    if (isDragEnabled)
                        grdFacetsWithText.ItemDraggingEnabled = 0;
                    ToggleSaveButton(false);
                    break;
                case "cmEdit":
                    grdFacetsWithText.edit(item);
                    if (isDragEnabled)
                        grdFacetsWithText.ItemDraggingEnabled = 0;
                    ToggleSaveButton(false);
                    break;
                case "cmDelete":
                    grdFacetsWithText.deleteItem(item);
                    if (grdFacetsWithText.get_recordCount() == 0) {
                        grdFacetsWithText.get_table().addEmptyRow(0);
                    }
                    if (isDragEnabled)
                        grdFacetsWithText.ItemDraggingEnabled = 1;
                    break;
            }
        }

        function grdFacetsWithCal_onContextMenu(sender, eventArgs) {
            CancelCalUpdate();
            var e = eventArgs.get_event();
            item = eventArgs.get_item();

            factRangeId = item.getMember(5).get_text();
            var menuItems = mnuFacetWithCal.get_items();
            if (factRangeId != '' && factRangeId != emptyGuid && factRangeId != 0) //only add
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() != 'cmCalAdd') {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }

            mnuFacetWithCal.showContextMenuAtEvent(e);
        }

        function SaveFacetRecord() {
            if (Page_ClientValidate('GridLevel') == true) {
                if (item.getMemberAt(5).Value == 0)
                    item.SetValue(5, rowCount);
                grdFacetsWithText.editComplete();
                item = null;
                if (isDragEnabled)
                    grdFacetsWithText.ItemDraggingEnabled = 1;
                isnew = false;
                ToggleSaveButton(true);
            }
        }

        function CancelFacetUpdate() {
            if (isnew)
                grdFacetsWithText.deleteItem(item);
            else
                grdFacetsWithText.editCancel();
            item = null;
            if (isDragEnabled)
                grdFacetsWithText.ItemDraggingEnabled = 1;
            isnew = false;
            ToggleSaveButton(true);
        }

        function Picker1_OnDateChange() {
            var fromDate = Picker1.getSelectedDate();
            Calendar1.setSelectedDate(fromDate);
        }

        function Calendar1_OnChange() {
            var fromDate = Calendar1.getSelectedDate();
            Picker1.setSelectedDate(fromDate);
        }

        function Picker2_OnDateChange() {
            var fromDate = Picker2.getSelectedDate();
            Calendar2.setSelectedDate(fromDate);
        }

        function Calendar2_OnChange() {
            var toDate = Calendar2.getSelectedDate();
            Picker2.setSelectedDate(toDate);
        }

        function Button1_OnClick(button) {
            if (Calendar1.get_popUpShowing()) {
                Calendar1.hide();
            }
            else {
                var date = Picker1.getSelectedDate();
                Calendar1.setSelectedDate(date);
                Calendar1.show(button);
            }
        }

        function Button2_OnClick(button) {
            if (Calendar2.get_popUpShowing()) {
                Calendar2.hide();
            }
            else {
                var date = Picker2.getSelectedDate();
                Calendar2.setSelectedDate(date);
                Calendar2.show(button);
            }
        }

        function ShowDateTimeGrid(cntrl) {
            var divGrid = document.getElementById("divDateTimeGrid");
            divGrid.style.display = "block";
            //Add manual display order for range type
            var ddlObj = document.getElementById("ddlOrder");
            if (ddlObj.length < 3) ddlObj.options[ddlObj.length] = new Option("Manual", "3")
        }
        function HideDateTimeGrid(cntrl) {
            var divGrid = document.getElementById("divDateTimeGrid");
            divGrid.style.display = "none";
            //Remove manual display order for value type
            var ddlObj = document.getElementById("ddlOrder");
            if (ddlObj.length == 3) ddlObj.options.remove(2);
        }
        function ShowTextGrid(cntrl) {
            var divGrid = document.getElementById("divTextGrid");
            divGrid.style.display = "block";
            //Add manual display order for range type
            var ddlObj = document.getElementById("ddlOrder");

            if (ddlObj.length < 3) ddlObj.options[ddlObj.length] = new Option("Manual", "3")
        }
        function HideTextGrid(cntrl) {
            var divGrid = document.getElementById("divTextGrid");
            divGrid.style.display = "none";

            //Remove manual display order for value type
            var ddlObj = document.getElementById("ddlOrder");
            if (ddlObj.length == 3) ddlObj.options.remove(2);
        }

        function AddFacetText() {
            isnew = false;
            var gridc = grdFacetsWithText.get_table().get_columns();
            grdFacetsWithText.CurrentPageIndex = 0;
            selectedAction = "Insert";
            if (rowCount == '')
                rowCount = grdFacetsWithText.Data.length;
            if (rowCount == 1 && (item.getMember('Id').get_value() == null || item.getMember('Id').get_value() == '')) {
                grdFacetsWithText.edit(item);
                item.SetValue(5, 0);
            }
            else {
                item = null;
                grdFacetsWithText.get_table().addEmptyRow(0);
                item = grdFacetsWithText.get_table().getRow(0);
                grdFacetsWithText.edit(item);
                rowCount++;
                item.SetValue(5, rowCount);
                isnew = true;
            }
        }

        function AddFacetCal() {
            isnew = false;
            var gridc = grdFacetsWithCal.get_table().get_columns();
            grdFacetsWithCal.CurrentPageIndex = 0;
            selectedAction = "Insert";
            if (rowCount == '')
                rowCount = grdFacetsWithCal.Data.length;
            if (rowCount == 1 && (item.getMember('Id').get_value() == null || item.getMember('Id').get_value() == '')) {
                grdFacetsWithCal.edit(item);
                item.SetValue(5, 0);
            }
            else {
                item = null;
                grdFacetsWithCal.get_table().addEmptyRow(0);
                item = grdFacetsWithCal.get_table().getRow(0);
                grdFacetsWithCal.edit(item);
                rowCount++;
                item.SetValue(5, rowCount);
                isnew = true;
            }
        }

        function RefreshTextGridForDisplayOrder(cntrl) {
            var rblObject = document.forms[0].elements["rdlAttributeType"];

            if (rblObject[1].checked == true) {
                var gridc = grdFacetsWithText.get_table().get_columns();
                var actionColumnValue = "";
                ddlObject = document.getElementById(cntrl);
                if (ddlObject.options[ddlObject.selectedIndex].text == "Manual") {
                    grdFacetsWithText.sort(6, false);
                    gridc[0].AllowSorting = false;
                    gridc[1].AllowSorting = false;
                    gridc[2].AllowSorting = false;
                    gridc[3].AllowSorting = false;
                    gridc[4].AllowSorting = false;
                    grdFacetsWithText.set_pageSize(5000);
                    grdFacetsWithText.ItemDraggingEnabled = 1;
                    isDragEnabled = true;
                }
                else {
                    gridc[0].AllowSorting = true;
                    gridc[1].AllowSorting = true;
                    gridc[2].AllowSorting = true;
                    gridc[3].AllowSorting = true;
                    gridc[4].AllowSorting = true;
                    grdFacetsWithText.get_levels()[0].set_hoverRowCssClass('');
                    grdFacetsWithText.set_pageSize(10);
                    grdFacetsWithText.ItemDraggingEnabled = 0;
                    isDragEnabled = false;
                }
            }
        }

        function RefreshCalGridForDisplayOrder(cntrl) {
            var rblObject = document.forms[0].elements["rdlAttributeType"];

            if (rblObject[1].checked == true) {
                var gridc = grdFacetsWithCal.get_table().get_columns();
                var actionColumnValue = "";
                ddlObject = document.getElementById(cntrl);
                if (ddlObject.options[ddlObject.selectedIndex].text == "Manual") {
                    grdFacetsWithCal.sort(6, false);
                    gridc[0].AllowSorting = false;
                    gridc[1].AllowSorting = false;
                    gridc[2].AllowSorting = false;
                    gridc[3].AllowSorting = false;
                    gridc[4].AllowSorting = false;
                    grdFacetsWithCal.set_pageSize(5000);
                    grdFacetsWithCal.ItemDraggingEnabled = 1;
                    isDragEnabled = true;
                }
                else {
                    gridc[0].AllowSorting = true;
                    gridc[1].AllowSorting = true;
                    gridc[2].AllowSorting = true;
                    gridc[3].AllowSorting = true;
                    gridc[4].AllowSorting = true;
                    grdFacetsWithCal.get_levels()[0].set_hoverRowCssClass('');
                    grdFacetsWithCal.set_pageSize(10);
                    grdFacetsWithCal.ItemDraggingEnabled = 0;
                    isDragEnabled = false;
                }
            }
        }

        function mnuFacetWithCal_onItemSelect(sender, eventArgs) {
            var itemSelected = eventArgs.get_item().get_id();
            switch (itemSelected) {
                case "cmCalAdd":
                    AddFacetCal();
                    if (isDragEnabled)
                        grdFacetsWithCal.ItemDraggingEnabled = 0;
                    ToggleSaveButton(false);
                    break;
                case "cmCalEdit":
                    grdFacetsWithCal.edit(item);
                    if (isDragEnabled)
                        grdFacetsWithCal.ItemDraggingEnabled = 0;
                    ToggleSaveButton(false);
                    break;
                case "cmCalDelete":
                    grdFacetsWithCal.deleteItem(item);
                    if (grdFacetsWithCal.get_recordCount() == 0) {
                        grdFacetsWithCal.get_table().addEmptyRow(0);
                    }
                    if (isDragEnabled)
                        grdFacetsWithCal.ItemDraggingEnabled = 1;
                    break;
            }
        }

        function ValidateForDates(sender, args) {
            var fromDate = Picker1.getSelectedDate();
            var toDate = Picker2.getSelectedDate();
            if (toDate != "") {
                if (fromDate > toDate) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
            else {
                args.IsValid = true;
            }
        }


        function SaveCalRecord() {
            if (Page_ClientValidate('GridLevelCal') == true) {
                if (item.getMemberAt(5).Value == 0)
                    item.SetValue(5, rowCount);
                grdFacetsWithCal.editComplete();
                item = null;
                if (isDragEnabled)
                    grdFacetsWithCal.ItemDraggingEnabled = 1;
                isnew = false;
                ToggleSaveButton(true);
            }
        }

        function CancelCalUpdate() {
            if (isnew)
                grdFacetsWithCal.deleteItem(item);
            else
                grdFacetsWithCal.editCancel();
            item = null;
            if (isDragEnabled)
                grdFacetsWithCal.ItemDraggingEnabled = 1;
            isnew = false;
            ToggleSaveButton(true);
        }
        function CheckForAllowedCharacters() {

        }
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="iapps-modal AddFacets">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Label ID="lblHeading" runat="server" Text="<%$ Resources:GUIStrings, AddAFacet %>"></asp:Label></h2>
        </div>
        <div class="modal-content clear-fix">
            <p>
                <asp:Label ID="lblPageInfo" runat="server" Text=""></asp:Label>
            </p>
            <div id="divPageTopContainer" class="messageContainer" runat="server" />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="PageLevel" />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="GridLevel" />
            <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="True"
                ShowSummary="False" ValidationGroup="GridLevelCal" />
            <span class="formRow">
                <label for="<%=txtName.ClientID%>" class="formLabel">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Name %>" /></label>
                <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="200" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvTxtName" ValidationGroup="PageLevel"
                    runat="server" ControlToValidate="txtName" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterName %>"
                    Text="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="cvTxtName" runat="server" ValidationGroup="PageLevel"
                    ControlToValidate="txtName" ValidationExpression="^[^<>&]+$" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterValidCharactersForFacetName %>"
                    Text="*"></asp:RegularExpressionValidator>
            </span><span class="formRow">
                <label for="<%=txtLimit.ClientID%>" class="formLabel">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Limit %>" /></label>
                <asp:TextBox ID="txtLimit" runat="server" CssClass="textBoxes" Width="20"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvTxtLimit" ControlToValidate="txtLimit"
                    ValidationGroup="PageLevel" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterLimit %>"
                    Text="*"></asp:RequiredFieldValidator>
                <asp:CompareValidator CultureInvariantValues="true" ID="cvTxtLimit" ControlToValidate="txtLimit"
                    ValidationGroup="PageLevel" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAnAppropriateValueForLimit %>"
                    Operator="DataTypeCheck" Type="Integer" Text="*"></asp:CompareValidator>
            </span><span class="formRow">
                <label for="<%=ddlOrder.ClientID%>" class="formLabel">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DisplayOrder %>" /></label>
                <asp:DropDownList ID="ddlOrder" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Name1 %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Count %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Manual %>" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </span><span class="formRow">
                <label for="<%=chkAllowMultiple.ClientID%>" class="formLabel">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MultipleSelect %>" /></label>
                <asp:CheckBox ID="chkAllowMultiple" runat="server" />
            </span><span class="formRow">
                <asp:RadioButtonList ID="rdlAttributeType" runat="server" RepeatDirection="Horizontal"
                    Style="float: left;">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Value %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Range %>" Value="1"></asp:ListItem>
                </asp:RadioButtonList>
            </span>
            <div id="divTextGrid">
                <ComponentArt:Grid SkinID="Default" ID="grdFacetsWithText" SliderPopupClientTemplateId="grdFacetsWithTextSliderTemplate"
                    Width="660" runat="server" PageSize="10" AllowEditing="true" RunningMode="Client"
                    CssClass="Grid sGrid" EditOnClickSelectedItem="false" AutoCallBackOnInsert="false"
                    AutoCallBackOnUpdate="false" AutoCallBackOnDelete="false" PreBindServerTemplates="true"
                    SliderPopupCachedClientTemplateId="cachedgrdFacetsWithTextSliderTemplate" CallbackCachingEnabled="false"
                    EnableViewState="True" ItemDraggingEnabled="false" Height="275" ExternalDropTargets="grdFacetsWithText"
                    EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" PagerInfoClientTemplateId="grdFacetsWithTextSliderPageInfoTemplate">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                            EditCommandClientTemplateId="EditCommandTemplate" EditCellCssClass="EditDataCell"
                            EditFieldCssClass="EditDataField">
                            <Columns>
                                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, From %>"
                                    HeadingCellCssClass="FirstHeadingCell" DataField="From" DataCellCssClass="FirstDataCell"
                                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                                    EditControlType="Custom" EditCellServerTemplateId="SVTFrom" CustomEditSetExpression="setFacetValue"
                                    CustomEditGetExpression="getFacetValue" DataCellClientTemplateId="FromHoverTemplate"
                                    AllowSorting="False" />
                                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, To %>" DataField="To"
                                    SortedDataCellCssClass="SortedDataCell" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                                    FixedWidth="true" AllowReordering="false" EditControlType="Custom" EditCellServerTemplateId="SVTTo"
                                    DataCellClientTemplateId="ToHoverTemplate" AllowSorting="False" />
                                <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, DisplayLink %>"
                                    DataField="DisplayText" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="DataCell"
                                    HeadingCellCssClass="HeadingCell" FixedWidth="true" AllowReordering="false" EditControlType="Custom"
                                    EditCellServerTemplateId="SVTDisplay" DataCellClientTemplateId="DisplayHoverTemplate"
                                    AllowSorting="False" />
                                <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, AlternateText %>"
                                    DataField="AlternateText" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="DataCell"
                                    HeadingCellCssClass="HeadingCell" FixedWidth="true" AllowReordering="false" EditControlType="Custom"
                                    EditCellServerTemplateId="SVTAlternate" DataCellClientTemplateId="AltHoverTemplate"
                                    AllowSorting="False" />
                                <ComponentArt:GridColumn Width="99" AllowSorting="false" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                                    DataCellCssClass="LastDataCell" EditControlType="EditCommand" Align="Center"
                                    FixedWidth="true" HeadingCellCssClass="LastHeadingCell" AllowReordering="false" />
                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                <ComponentArt:GridColumn DataField="Sequence" Visible="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientEvents>
                        <ContextMenu EventHandler="grdFacetsWithText_onContextMenu" />
                        <ItemSelect EventHandler="grdFacetsWithText_onItemSelect" />
                        <ItemExternalDrop EventHandler="grdFacetsWithText_OnExternalDrop" />
                    </ClientEvents>
                    <ServerTemplates>
                        <ComponentArt:GridServerTemplate ID="SVTFrom">
                            <Template>
                                <asp:TextBox ID="txtFrom" runat="server" CssClass="textBoxes" Width="80"></asp:TextBox>
                                <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtFrom" ControlToValidate="txtFrom"
                                    ValidationGroup="GridLevel" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAnApporpriateValue %>"
                                    Operator="DataTypeCheck" Text="*"></asp:CompareValidator>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtFrom" ControlToValidate="txtFrom"
                                    ValidationGroup="GridLevel" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterFromValue %>"
                                    Text="*"></asp:RequiredFieldValidator>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                        <ComponentArt:GridServerTemplate ID="SVTTo">
                            <Template>
                                <asp:TextBox ID="txtTo" runat="server" CssClass="textBoxes" Width="80"></asp:TextBox>
                                <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtTo" ControlToValidate="txtTo"
                                    ValidationGroup="GridLevel" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAnAppropriateValueForTo %>"
                                    Operator="DataTypeCheck" Text="*"></asp:CompareValidator>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                        <ComponentArt:GridServerTemplate ID="SVTDisplay">
                            <Template>
                                <asp:TextBox ID="txtDisplay" runat="server" CssClass="textBoxes" Width="160" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtDisplay" ControlToValidate="txtDisplay"
                                    ValidationGroup="GridLevel" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterDisplayText %>"
                                    Text="*"></asp:RequiredFieldValidator>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                        <ComponentArt:GridServerTemplate ID="SVTAlternate">
                            <Template>
                                <asp:TextBox ID="txtAlternate" runat="server" CssClass="textBoxes" Width="70" MaxLength="50"></asp:TextBox>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                    </ServerTemplates>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="FromHoverTemplate">
                            <div title="## DataItem.GetMember('From').get_text() ##">
                                ## DataItem.GetMember('From').get_text() == "" ? "&nbsp;" : DataItem.GetMember('From').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="ToHoverTemplate">
                            <div title="## DataItem.GetMember('To').get_text() ##">
                                ## DataItem.GetMember('To').get_text() == "" ? "&nbsp;" : DataItem.GetMember('To').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="DisplayHoverTemplate">
                            <div title="## DataItem.GetMember('DisplayText').get_text() ##">
                                ## DataItem.GetMember('DisplayText').get_text() == "" ? "&nbsp;" : DataItem.GetMember('DisplayText').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="AltHoverTemplate">
                            <div title="## DataItem.GetMember('AlternateText').get_text() ##">
                                ## DataItem.GetMember('AlternateText').get_text() == "" ? "&nbsp;" : DataItem.GetMember('AlternateText').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="dragTemplate">
                            <div style="height: 100px; width: 100px;">
                                ## DataItem.getMember('From').get_text() ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                            <a href="javascript:SaveFacetRecord();">
                                <img src="/iapps_images/cm-icon-add.png" border="0" alt="##_save##" title="##_save##" /></a>
                            | <a href="javascript:CancelFacetUpdate();">
                                <img src="/iapps_images/cm-icon-delete.png" border="0" alt="##_cancel##" title="##_cancel##" /></a>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdFacetsWithTextSliderTemplate">
                            <div class="SliderPopup">
                                <p>
                                </p>
                                <p class="paging">
                                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdFacetsWithText.PageCount)##</span>
                                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdFacetsWithText.RecordCount)##</span>
                                </p>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="cachedgrdFacetsWithTextSliderTemplate">
                            <div class="SliderPopup">
                                <h5>
                                    ## DataItem.GetMemberAt(0).Value ##</h5>
                                <p>
                                    ## DataItem.GetMemberAt(1).Value ##</p>
                                <p class="paging">
                                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdFacetsWithText.PageCount)##</span>
                                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdFacetsWithText.RecordCount)##</span>
                                </p>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdFacetsWithTextSliderPageInfoTemplate">
                            ##stringformat(Page0Of12items, currentPageIndex(grdFacetsWithText), pageCount(grdFacetsWithText),
                            grdFacetsWithText.RecordCount)##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
            <ComponentArt:Menu ID="mnuFacetsWithText" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddFacetRange %>" Look-LeftIconUrl="cm-icon-add.png">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="cmEdit" Text="<%$ Resources:GUIStrings, EditFacetRange %>"
                        Look-LeftIconUrl="cm-icon-edit.png">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, DeleteFacetRange %>"
                        Look-LeftIconUrl="cm-icon-delete.png">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="mnuFacetsWithText_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
            <div id="divDateTimeGrid">
                <ComponentArt:Grid SkinID="Default" ID="grdFacetsWithCal" SliderPopupClientTemplateId="grdFacetsWithCalSliderTemplate"
                    Width="660" runat="server" PageSize="10" AllowEditing="true" RunningMode="Client"
                    CssClass="Grid sGrid" EditOnClickSelectedItem="false" AutoCallBackOnInsert="false"
                    AutoCallBackOnUpdate="false" AutoCallBackOnDelete="false" Height="200" SliderPopupCachedClientTemplateId="cachedgrdFacetsWithCalSliderTemplate"
                    CallbackCachingEnabled="false" EnableViewState="True" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
                    PagerInfoClientTemplateId="grdFacetsWithCalSliderPageInfoTemplate" ItemDraggingEnabled="false"
                    ExternalDropTargets="grdFacetsWithCal">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                            EditCommandClientTemplateId="EditCommandTemplateCal" EditCellCssClass="EditDataCell"
                            EditFieldCssClass="EditDataField">
                            <Columns>
                                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, From %>"
                                    HeadingCellCssClass="FirstHeadingCell" DataField="From" DataCellCssClass="FirstDataCell"
                                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                                    EditControlType="Custom" EditCellServerTemplateId="SVTFromCal" DataType="System.DateTime"
                                    FormatString="d" DataCellClientTemplateId="FromCalHoverTemplate" AllowSorting="False" />
                                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, To %>" DataField="To"
                                    SortedDataCellCssClass="SortedDataCell" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                                    FixedWidth="true" AllowReordering="false" EditControlType="Custom" EditCellServerTemplateId="SVTToCal"
                                    DataType="System.DateTime" FormatString="d" DataCellClientTemplateId="ToCalHoverTemplate"
                                    AllowSorting="False" />
                                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, DisplayLink %>"
                                    DataField="DisplayText" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="DataCell"
                                    HeadingCellCssClass="HeadingCell" FixedWidth="true" AllowReordering="false" EditControlType="Custom"
                                    EditCellServerTemplateId="SVTDisplayCal" DataCellClientTemplateId="DisplayCalHoverTemplate" />
                                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, AlternateText %>"
                                    DataField="AlternateText" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="DataCell"
                                    HeadingCellCssClass="HeadingCell" FixedWidth="true" AllowReordering="false" EditControlType="Custom"
                                    EditCellServerTemplateId="SVTAlternateCal" DataCellClientTemplateId="AltCalHoverTemplate"
                                    AllowSorting="False" />
                                <ComponentArt:GridColumn Width="99" AllowSorting="false" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                                    DataCellCssClass="LastDataCell" EditControlType="EditCommand" Align="Center"
                                    FixedWidth="true" HeadingCellCssClass="LastHeadingCell" AllowReordering="false" />
                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                <ComponentArt:GridColumn DataField="Sequence" Visible="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientEvents>
                        <ContextMenu EventHandler="grdFacetsWithCal_onContextMenu" />
                        <ItemSelect EventHandler="grdFacetsWithCal_onItemSelect" />
                        <ItemExternalDrop EventHandler="grdFacetsWithCal_OnExternalDrop" />
                    </ClientEvents>
                    <ServerTemplates>
                        <ComponentArt:GridServerTemplate ID="SVTFromCal">
                            <Template>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <ComponentArt:Calendar ID="Picker1" runat="server" PickerFormat="Custom" PickerCustomFormat="MMMM d yyyy"
                                                ControlType="Picker" PickerCssClass="picker" ClientSideOnSelectionChanged="Picker1_OnDateChange" />
                                        </td>
                                        <td>
                                            <img id="calendar_from_button" alt="<%= GUIStrings.CalendarButton %>" class="calendar_button"
                                                src="../../App_Themes/General/images/btn_calendar.gif" onclick="Button1_OnClick(this);" />
                                            <ComponentArt:Calendar runat="server" ID="Calendar1" ControlType="Calendar" PopUp="Custom"
                                                PopUpExpandControlId="calendar_from_button" ClientSideOnSelectionChanged="Calendar1_OnChange"
                                                SkinID="Default">
                                            </ComponentArt:Calendar>
                                        </td>
                                    </tr>
                                </table>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                        <ComponentArt:GridServerTemplate ID="SVTToCal">
                            <Template>
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <ComponentArt:Calendar ID="Picker2" runat="server" PickerFormat="Custom" PickerCustomFormat="MMMM d yyyy"
                                                ControlType="Picker" PickerCssClass="picker" ClientSideOnSelectionChanged="Picker2_OnDateChange" />
                                        </td>
                                        <td>
                                            <img id="calendar_to_button" alt="<%= GUIStrings.CalendarButton %>" class="calendar_button"
                                                src="../../App_Themes/General/images/btn_calendar.gif" onclick="Button2_OnClick(this);" />
                                            <ComponentArt:Calendar runat="server" ID="Calendar2" ControlType="Calendar" PopUp="Custom"
                                                PopUpExpandControlId="calendar_to_button" ClientSideOnSelectionChanged="Calendar2_OnChange"
                                                SkinID="Default">
                                            </ComponentArt:Calendar>
                                        </td>
                                    </tr>
                                </table>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                        <ComponentArt:GridServerTemplate ID="SVTDisplayCal">
                            <Template>
                                <asp:TextBox ID="txtDisplayDateTime" runat="server" CssClass="textBoxes" Width="90"
                                    MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtDisplayDateTime"
                                    ControlToValidate="txtDisplayDateTime" ValidationGroup="GridLevelCal" runat="server"
                                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterDisplayText %>" Text="*"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="dateValidator" ClientValidationFunction="ValidateForDates"
                                    ControlToValidate="txtDisplayDateTime" runat="server" ValidationGroup="GridLevelCal"
                                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, FacetToDateShouldBeGreaterThanFacetFromDate %>"
                                    Display="None"></asp:CustomValidator>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                        <ComponentArt:GridServerTemplate ID="SVTAlternateCal">
                            <Template>
                                <asp:TextBox ID="txtAlternate" runat="server" CssClass="textBoxes" Width="90" MaxLength="50"></asp:TextBox>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                    </ServerTemplates>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="FromCalHoverTemplate">
                            <div title="## DataItem.GetMember('From').get_text() ##">
                                ## DataItem.GetMember('From').get_text() == "" ? "&nbsp;" : DataItem.GetMember('From').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="ToCalHoverTemplate">
                            <div title="## DataItem.GetMember('To').get_text() ##">
                                ## DataItem.GetMember('To').get_text() == "" ? "&nbsp;" : DataItem.GetMember('To').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="DisplayCalHoverTemplate">
                            <div title="## DataItem.GetMember('DisplayText').get_text() ##">
                                ## DataItem.GetMember('DisplayText').get_text() == "" ? "&nbsp;" : DataItem.GetMember('DisplayText').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="AltCalHoverTemplate">
                            <div title="## DataItem.GetMember('AlternateText').get_text() ##">
                                ## DataItem.GetMember('AlternateText').get_text() == "" ? "&nbsp;" : DataItem.GetMember('AlternateText').get_text()
                                ##</div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdFacetWithCalDragged">
                            <div style="height: 100px; width: 100px;">
                                ## DataItem.getMember('DisplayText').get_text() ##
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="EditCommandTemplateCal">
                            <a href="javascript:SaveCalRecord();">
                                <img src="/iapps_images/cm-icon-add.png" border="0" alt="##_save##" title="##_save##" /></a>
                            | <a href="javascript:CancelCalUpdate();">
                                <img src="/iapps_images/cm-icon-delete.png" border="0" alt="##_cancel##" title="##_cancel##" /></a>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdFacetsWithCalSliderTemplate">
                            <div class="SliderPopup">
                                <p>
                                    ##_datanotload##</p>
                                <p class="paging">
                                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdFacetsWithCal.PageCount)##</span>
                                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdFacetsWithCal.RecordCount)##</span>
                                </p>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="cachedgrdFacetsWithCalSliderTemplate">
                            <div class="SliderPopup">
                                <h5>
                                    ## DataItem.GetMemberAt(0).Value ##</h5>
                                <p>
                                    ## DataItem.GetMemberAt(1).Value ##</p>
                                <p class="paging">
                                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdFacetsWithCal.PageCount)##</span>
                                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdFacetsWithCal.RecordCount)##</span>
                                </p>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdFacetsWithCalSliderPageInfoTemplate">
                            ##stringformat(Page0Of12items, currentPageIndex(grdFacetsWithCal), pageCount(grdFacetsWithCal),
                            grdFacetsWithCal.RecordCount)##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
            <ComponentArt:Menu ID="mnuFacetWithCal" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem ID="cmCalAdd" Text="<%$ Resources:GUIStrings, AddFacetRange %>"
                        Look-LeftIconUrl="cm-icon-add.png">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="cmCalEdit" Text="<%$ Resources:GUIStrings, EditFacetRange %>"
                        Look-LeftIconUrl="cm-icon-edit.png">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="cmCalDelete" Text="<%$ Resources:GUIStrings, DeleteFacetRange %>"
                        Look-LeftIconUrl="cm-icon-delete.png">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="mnuFacetWithCal_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>
        <div class="modal-footer clear-fix">
            <asp:Button ID="btnCancel" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
                CssClass="button" runat="server" OnClientClick="return ClosePopup();" />
            <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
                CssClass="primarybutton" runat="server" OnClick="btnSave_Click" CausesValidation="true"
                ValidationGroup="PageLevel" />
        </div>
    </div>
    <script type="text/javascript">
        function grdFacetsWithCal_OnExternalDrop(sender, eventArgs) {
            if (eventArgs.get_targetControl().get_id() == document.getElementById('<%=grdFacetsWithCal.ClientID%>').id) {
                var targetItem = eventArgs.get_target();
                var newRow;
                if (targetItem != null) {
                    grdFacetsWithCal.deleteItem(eventArgs.get_item());
                    newRow = grdFacetsWithCal.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    grdFacetsWithCal.deleteItem(eventArgs.get_item());
                    newRow = grdFacetsWithCal.get_table().addEmptyRow();
                }
                grdFacetsWithCal.beginUpdate();
                newRow.setValue(0, eventArgs.get_item().getMember('From').get_text());
                newRow.setValue(1, eventArgs.get_item().getMember('To').get_text());
                newRow.setValue(2, eventArgs.get_item().getMember('DisplayText').get_text());
                newRow.setValue(3, eventArgs.get_item().getMember('AlternateText').get_text());
                newRow.setValue(5, eventArgs.get_item().getMember('Id').get_text());
                grdFacetsWithCal.endUpdate();
            }
        }
        function grdFacetsWithText_OnExternalDrop(sender, eventArgs) {
            if (eventArgs.get_targetControl().get_id() == document.getElementById('<%=grdFacetsWithText.ClientID%>').id) {
                var targetItem = eventArgs.get_target();
                var newRow;
                if (targetItem != null) {
                    grdFacetsWithText.deleteItem(eventArgs.get_item());
                    newRow = grdFacetsWithText.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    grdFacetsWithText.deleteItem(eventArgs.get_item());
                    newRow = grdFacetsWithText.get_table().addEmptyRow();
                }
                grdFacetsWithText.beginUpdate();
                newRow.setValue(0, eventArgs.get_item().getMember('From').get_text());
                newRow.setValue(1, eventArgs.get_item().getMember('To').get_text());
                newRow.setValue(2, eventArgs.get_item().getMember('DisplayText').get_text());
                newRow.setValue(3, eventArgs.get_item().getMember('AlternateText').get_text());
                newRow.setValue(5, eventArgs.get_item().getMember('Id').get_text());
                grdFacetsWithText.endUpdate();
            }
        }

        function ToggleSaveButton(showHide) {
            document.getElementById("<%=btnSave.ClientID%>").disabled = !showHide;

            //disable the display order checkbox while we edit or add
            document.getElementById('ddlOrder').disabled = !showHide;
        }
    </script>
    </form>
</body>
</html>
