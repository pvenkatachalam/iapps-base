﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectProductImagePopup.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SelectProductImagePopup"
    StylesheetTheme="General" %>
<%@ Register TagPrefix="UC" TagName="SearchFilter" Src="~/UserControls/General/ProductSearchFilter.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var product;

        function FormatQuantity(strQty, strUnlimited) {
            if (strUnlimited == "true")
                return '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Unlimited %>"/>';

            if (strUnlimited == "" || isNaN(parseFloat(strQty)))
                return "0";

            if (parseFloat(strQty) < 0)
                return "0";

            return strQty;
        }

        function FormatStatus(strIsActive) {
            if (strIsActive == "true")
                return '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Active %>"/>';
            else
                return '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, InActive %>"/>';
        }

        function grdProductList_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            var gridItem = eventArgs.get_item();
            productImage = new Object();
            productImage.Id = gridItem.getMember(0).get_text();
            productImage.altText = gridItem.getMember(2).get_text();
            productImage.src = gridItem.getMember(1).get_text();
            productImage.objectId = gridItem.getMember(5).get_text();
            
            menuItems = mnuProductsList.get_items();

            mnuProductsList.showContextMenuAtEvent(e);
        }

        function mnuProductsList_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            productImage.src = productImage.src.replace(new RegExp("systhumb_", "g"), selectedMenuId);
            CloseProductPopup(productImage);
        }

        function getRadWindow() {
            if (window.radWindow) {
                return window.radWindow;
            }
            if (window.frameElement && window.frameElement.radWindow) {
                return window.frameElement.radWindow;
            }
            return null;
        }

        function CloseProductPopup(productImage) {
            if (EditorToUse.toLowerCase() == 'radeditor') {
                parent.fn_InsertProductImageInRadEditor(productImage);
            }
            else if (EditorToUse.toLowerCase() == 'ckeditor') {
                parent.fn_InsertProudctImageInEditor(productImage);
            }
            parent.CloseiAppsCommercePopup();
            return false;
        }

    </script>
    <style type="text/css">
    #<%= grdProductList.ClientID %>_VerticalScrollDiv
    {
        max-height:240px !important;        
    }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:SearchFilter ID="searchFilter" runat="server" />
    <ComponentArt:Grid SkinID="Default" ID="grdProductList" SliderPopupClientTemplateId="ProductListSliderTemplate"
        SliderPopupCachedClientTemplateId="ProductListSliderTemplateCached" runat="server"
        RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="4" Sort="ProductName ASC"
        PagerInfoClientTemplateId="ProductListPaginationTemplate" CallbackCachingEnabled="false"
        SearchOnKeyPress="false" ManualPaging="true" Width="100%" Height="240"
        LoadingPanelClientTemplateId="ProductListLoadingPanelTemplate" AllowVerticalScrolling="true">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn Width="110" HeadingText="<%$ Resources:GUIStrings, Thumbnail %>"
                        DataField="FileName" Align="Left" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="FileNameHoverTemplate" TextWrap="true" />
                    <ComponentArt:GridColumn Width="369" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                        DataField="ProductName" Align="Left" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="NameHoverTemplate" TextWrap="true" />
                    <ComponentArt:GridColumn Width="129" HeadingText="<%$ Resources:GUIStrings, SKU %>"
                        DataField="SKU" Align="Left" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="SkuHoverTemplate"
                        TextWrap="true" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="ProductTypeName" IsSearchable="true" Align="Left" AllowReordering="false"
                        DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell"
                        FixedWidth="true" DataCellClientTemplateId="TypeHoverTemplate" TextWrap="true" />
                    <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdProductList_onContextMenu" />
            <RenderComplete EventHandler="SetVerticalScroll" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="FileNameHoverTemplate">                
                <img src="## DataItem.GetMember('FileName').get_text() ##" alt="## DataItem.GetMember('ProductName').get_text() ##"
                    style="## DataItem.GetMember('ProductName').get_text() == '' ? 'display:none;': '' ##" />
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('ProductName').get_text() ##">## DataItem.GetMember('ProductName').get_text()
                                == "" ? "&nbsp;" : DataItem.GetMember('ProductName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SkuHoverTemplate">
                <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <span title="## DataItem.GetMember('ProductTypeName').get_text() ##">## DataItem.GetMember('ProductTypeName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ProductTypeName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductList.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdProductList.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductList.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdProductList.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListPaginationTemplate">
                ##stringformat(Page0Of12items, currentPageIndex(grdProductList), pageCount(grdProductList),
                grdProductList.RecordCount)##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdProductList) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuProductsList" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuProductsList_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return CloseProductPopup('');" />
</asp:Content>
