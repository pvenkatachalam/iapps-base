﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductSearchCA.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.StoreManager.ProductSearchCA" StylesheetTheme="General" %>
<%@ Register assembly="Bridgeline.FW.Commerce.Controls" namespace="Bridgeline.FW.Commerce.Controls" tagprefix="sku" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title><asp:localize runat="server" Text="<%$ Resources:GUIStrings, ProductSearch %>" /></title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <link href="../../css/Products.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var defaultSelectedSku;
        
        function saveCell() {
           
            var selectedSKus =skuSelector.GetSelectedSKU();
           // alert(selectedSKus);
            var itemArray = grdSearchProducts.getSelectedItems();

            var itemIndex = 0;

            itemArray[0].Data[1] = selectedSKus.join(',');   

        }
     
        function restoreCell() {   
            var itemArray = grdSearchProducts.getSelectedItems();
            if (itemArray[0]) {
                var selectedSku = itemArray[0].getMemberAt(1).get_text();
                //alert(selectedSku);
                if (selectedSku.length > 0)
                    skuSelector.SetSelectedSKU(selectedSku);
            }
                           
        }
        
        function onSaveClick()
        {
            var grdResults = window["grdSearchProducts"];
            if(grdResults!="undefined" && grdResults!=null)
            {
                var checkedItems = GetCheckedItems();
                if(checkedItems!="" && checkedItems!="undefined")
                    {
                     parent.window["SelectedProducts"] = checkedItems;
                     parent.ClosePopup(checkedItems);
                    }
                else
                    {
                        alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, PleaseSelectAtLeastOneProduct %>"/>');
                    }
            }    
        }
        
        function onCloseClick()
        {
            parent.window["SelectedProducts"] ='';
            parent.ClosePopup();
        }

        function GetCheckedItems() {
            var checkedItems = new Array();
            var grdResults = window["grdSearchProducts"];
            var gridItem;
            var itemIndex = 0;
            while(gridItem = grdResults.get_table().getRow(itemIndex)) {
                if (gridItem.get_cells()[0].get_value()) {
                    var prodSku = new Array();
                    prodSku[0] = gridItem.getMemberAt(6).get_text();
                    prodSku[1] = gridItem.getMemberAt(1).get_text();
                    checkedItems[checkedItems.length] = prodSku;
                }
                itemIndex++;
            }
            return checkedItems;
        }
        
        function changeImageBorder(imgObject) {
            if(imgObject.className == "") {
                imgObject.className = "hover";
            }
            else if(imgObject.className == "hover") {
                imgObject.className = "";
            }
        }
        
        
        function ChangeSpecialCharacters(textToCleanUp, convertQuote) {
            textToCleanUp = unescape(textToCleanUp);
            if (convertQuote) {
                textToCleanUp = replacedString = textToCleanUp.replace(new RegExp("\"", "g"), "&quote;");
            }
            textToCleanUp = replacedString = textToCleanUp.replace(new RegExp("#%c", "g"), "&");
            textToCleanUp = replacedString = textToCleanUp.replace(new RegExp("#%", "g"), ";");
            textToCleanUp = replacedString = textToCleanUp.replace(new RegExp("&Lt;", "g"), "<");
            return textToCleanUp;
        }
    </script>
</head>
<body class="popupBody">
    <form id="form1" runat="server">
        <div class="popupBoxShadow productSearch">
            <div class="popupboxContainer">
                <div class="dialogBox">
                    <!-- Message Information STARTS -->
                    <div id="divPageTopContainer" class="messageContainer" runat="server">
                    </div>
                    <!-- Message Information ENDS -->
                    <h3>
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SearchAndAddProducts %>" /></h3>
                    <div class="middleContent">
                        <div class="formArea">
                            <div class="productSearchBox">
                                <asp:ValidationSummary ID="valSumSearch" runat="server" EnableClientScript="true"
                                    ShowSummary="true" ValidationGroup="SearchGroup" />
                                <asp:TextBox ID="txtSearchTerm" runat="server" CssClass="textBoxes"></asp:TextBox>
                                <div style="float:left;">
                                  <asp:RequiredFieldValidator CultureInvariantValues="true" Display="Dynamic" ID="reqSearch" ControlToValidate="txtSearchTerm"
                                    runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterASearchTerm %>"  ValidationGroup="SearchGroup">
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic" ID="regExSearch" runat="server"
                                    ControlToValidate="txtSearchTerm" ValidationExpression="^[^<>()]+$" ErrorMessage="<%$ Resources:GUIStrings, SearchTermContainsInvalidCharacters %>"
                                    Text="*" ValidationGroup="SearchGroup">
                                </asp:RegularExpressionValidator>
                                </div>
                                <asp:Button ID="btnSearch" Text="<%$ Resources:GUIStrings, Search %>" runat="server" OnClick="btnSearch_Click"
                                    ValidationGroup="SearchGroup" CssClass="button" />
                              
                            </div>
                            <asp:Panel ID="pnlSearchResults" runat="server">
                                <ComponentArt:Grid SkinID="Default" ID="grdSearchProducts" runat="server" RunningMode="Client"
                                    EmptyGridText="<%$ Resources:GUIStrings, NoRecordsFound %>" AutoPostBackOnSelect="false" PageSize="3" PagerInfoClientTemplateId="PagerTemplate">
                                    <Levels>
                                        <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="Id"
                                            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                                            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                            <Columns>
                                                <ComponentArt:GridColumn ColumnType="CheckBox" Width="20" AllowEditing="true" />
                                                
                                                <ComponentArt:GridColumn HeadingText="SelectedSKUs" Visible="false" />
                                                
                                                <ComponentArt:GridColumn Width="140" HeadingText="<%$ Resources:GUIStrings, Thumbnails %>" HeadingCellCssClass="FirstHeadingCell" DataField="Thumbnail" 
                                                    DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                                                    AllowReordering="false" FixedWidth="true" Align="Center" DataCellClientTemplateId="SvtThumbnail" />
                                                    
                                                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, Name %>" DataField="Name" IsSearchable="true" DataCellClientTemplateId="TemplateName"
                                                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true" Align="Left" />
                                                    
                                                <ComponentArt:GridColumn Width="60" HeadingText="<%$ Resources:GUIStrings, SKU %>"
                                                    SortedDataCellCssClass="SortedDataCell" FixedWidth="true" AllowReordering="false" Align="Right" visible="true"
                                                    DataCellServerTemplateId="svtSKUSelector"/>
                                                    
                                                <ComponentArt:GridColumn Width="195" HeadingText="<%$ Resources:GUIStrings, Description %>" DataField="Description"  FixedWidth="true"
                                                    SortedDataCellCssClass="SortedDataCell" HeadingCellCssClass="HeadingCell" DataCellClientTemplateId="TemplateDesc"
                                                    AllowReordering="false" Align="Left" />
                                                    
                                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                                <ComponentArt:GridColumn Visible="false" />
                                                <ComponentArt:GridColumn DataField="Quantity" HeadingText="<%$ Resources:GUIStrings, Quantity %>" Visible="true" Width="60"/>
                                            </Columns>
                                        </ComponentArt:GridLevel>
                                    </Levels>
                                    <ClientTemplates>
                                        <ComponentArt:ClientTemplate ID="PagerTemplate">
                                            ## stringformat(Page0Of1Items, currentPageIndex(grdSearchProducts), pageCount(grdSearchProducts), grdSearchProducts.RecordCount) ##
                                        </ComponentArt:ClientTemplate>
                                        <ComponentArt:ClientTemplate ID="SvtThumbnail">
                                            <img src="## DataItem.GetMember('Thumbnail').get_value() ##" alt="## DataItem.GetMember('Name').get_value() ##" title="## DataItem.GetMember('Name').get_value() ##" />
                                        </ComponentArt:ClientTemplate>
                                        <ComponentArt:ClientTemplate ID="TemplateName">
                                            <span title="## DataItem.GetMember('Name').get_value() ##">## DataItem.GetMember('Name').get_value() ##</span>
                                        </ComponentArt:ClientTemplate>
                                        <ComponentArt:ClientTemplate ID="TemplateDesc">
                                            <span title="## removeHTMLTags(ChangeSpecialCharacters(DataItem.GetMember('Description').get_value(), true)) ##">## ChangeSpecialCharacters(DataItem.GetMember('Description').get_value(), false) ##</span>
                                        </ComponentArt:ClientTemplate>
                                    </ClientTemplates>
                                    <ServerTemplates>                                    
                                        <ComponentArt:GridServerTemplate ID="svtSKUSelector" runat="server">
                                            <Template>                                            
                                                <sku:SKUSelector runat="server" ID="skuSelector" AutoPostbackOnClose="false" CarryForwardSelectedState="false"
                                                    UsePreviousSelectedState="false" OnCheckChangedCall="saveCell();"
                                                    OnCloseClientFunction="saveCell();" OnExpandClientFunction="restoreCell();">
                                                    <SKUList DataTextField="SKU" DataValueField="Id"></SKUList>
                                                </sku:SKUSelector>
                                            </Template>
                                        </ComponentArt:GridServerTemplate>
                                       </ServerTemplates>
                                </ComponentArt:Grid>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="footerContent">
                        <input type="button" id="btnAdd" value="<%= GUIStrings.Add %>" class="primarybutton" onclick="onSaveClick();" />
                        <input type="button" id="BtnClose" value="<%= GUIStrings.Close %>" class="button" title="<%= GUIStrings.Close %>" onclick="onCloseClick();" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
