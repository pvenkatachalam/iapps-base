﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="MultiAttributeValue.aspx.cs" StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.MultiAttributeValue" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var htmlText;

        function PasteContentToEditor(oureditor) {
            oureditor.set_html(htmlText);
        }

        function SaveAndClose(hiddenId, editorId) {
            var htmlvalue = '';
            var content = oureditor.get_html();
            var htmlvalue = content;
            parent.SetTextValue(hiddenId, htmlvalue);
            ClosePopup();
            return false;
        }

        function LoadHiddenText(hiddenId, editorId) {
            var inputHtml = parent.document.getElementById(hiddenId).value;
            htmlText = ConvertToHtml(inputHtml);
            while (inputHtml != htmlText) {
                inputHtml = htmlText;
                htmlText = ConvertToHtml(inputHtml);
            }
            htmlText = inputHtml;
        }
        function ClosePopup() {
            parent.UpdateAttributeValues();
            parent.pagepopup.hide();
            return false;
        }
        var item;
        var productType;

        function ProductAttributeValue_onContextMenu(sender, eventArgs) {
            item = eventArgs.get_item();
            if (ProductAttributeValue.Data.length == 1 && item.getMember('Value').get_value() == null || item.getMember('Value').get_value() == '' || item.getMember('Id').get_value() == 'Add New Value') {
                var i = 0;
                for (i = 0; i < cmProductAttributeValue.get_items().get_length(); i++) {
                    if (cmProductAttributeValue.GetItems()[i].get_id() != 'Add')
                        cmProductAttributeValue.GetItems()[i].Visible = false;

                }
            }
            else {

                var i = 0;
                for (i = 0; i < cmProductAttributeValue.get_items().get_length(); i++) {
                    cmProductAttributeValue.GetItems()[i].Visible = true;
                }

            }

            //ProductAttributeValue.select(item);
            /* if(browser == "ie")
            {       
            var evt = eventArgs.get_event();
            cmProductAttributeValue.showContextMenu(evt.clientX, evt.clientY);
            cmProductAttributeValue.set_contextData(item);
            }
            else {  cmProductAttributeValue.showContextMenu(eventArgs.get_event())};*/
            cmProductAttributeValue.showContextMenu(eventArgs.get_event());
        }

        function ContextMenuClickHandlerMultiAttribute(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();

            if (selectedMenuId == "Edit") {
                ProductAttributeValue.edit(item);
                gridOperation = "Edit";

            }
            else if (selectedMenuId == "Add") {
                if (item.getMember('Value').get_value() == '' || item.getMember('Id').get_value() == 'Add New Value') {
                    ProductAttributeValue.edit(item);
                    gridOperation = "Insert";
                }
                else {
                    ProductAttributeValue.beginUpdate();
                    ProductAttributeValue.get_table().addEmptyRow(0);
                    ProductAttributeValue.editComplete();
                    gridOperation = "Insert";
                    item = ProductAttributeValue.get_table().getRow(0);
                    //item.SetValue(0,productType);
                    ProductAttributeValue.edit(item);
                }
            }
            else if (selectedMenuId == "Delete") {
                if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, YouAreAboutToDeleteTheValue %>' />" + item.getMember('Value').get_value())) {
                    var actionsColumnIndex = 2;
                    var action = "Delete"
                    item.SetValue(actionsColumnIndex, action);
                    ProductAttributeValue.callback();
                }
            }
        }

        function ProductAttributeValue_BeforeUpdate() {
            if (ValidateValue()) {
                ProductAttributeValue.editComplete();
            }
            return true;
        }

        function ProductAttributeValueCancelEditedValue() {
            ProductAttributeValue_Cancel();
        }

        function ProductAttributeValue_Cancel() {
            if (gridOperation == 'Insert') {
                ProductAttributeValue.editCancel();
                ProductAttributeValue.deleteItem(item);
                item = null;
                gridOperation = 'Canceled';
                return false;
            }
            else {
                ProductAttributeValue.editCancel();
                gridOperation = 'Canceled';
            }
        }

        function ProductAttributeValue_onCallbackComplete(sender, eventArgs) {
            if (selectedMenu != '') {
                selectedMenu = '';
            }
        }

        function ProductAttributeValue_BeforeInsert(sender, eventArgs) {
            ProductAttributeValue.editComplete();
        }

        function setTitleValue(control, DataField) {
            var txtName = document.getElementById(control);
            var txtNamevalue = item.GetMember(DataField).Value;
            if (txtNamevalue == null || txtNamevalue == 'null')
                txtName.value = '';
            else
                txtName.value = txtNamevalue;
        }

        function getTitleValue(control) {
            var txtName = document.getElementById(control);
            return [txtName.value, txtName.value];
        }

        function getOperationValue() {
            return [gridOperation, gridOperation];
        }

        function ValidateValue() {
            var valueValid = true;

            if (Trim(document.getElementById(txtNameClientID).value) == "") {
                valueValid = false;
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAStringValue %>' />");
            }

            var patt1 = new RegExp("^[a-zA-Z0-9. _'-.&!|,]+$");

            if (valueValid && !patt1.test(document.getElementById(txtNameClientID).value)) {
                valueValid = false;
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ValueCanHaveOnlyAlphanumericCharacters %>' />");
            }

            if (!ValidateUniqueValues())
                valueValid = false;

            return valueValid;
        }

        function ValidateUniqueValues() {
            var isUnique = true;
            var txtValue;
            var gridItem;
            if (document.getElementById(txtNameClientID).value != "") {
                txtValue = document.getElementById(txtNameClientID).value;
                for (i = 0; i < ProductAttributeValue.get_table().getRowCount(); i++) {
                    gridItem = ProductAttributeValue.get_table().getRow(i);
                    if (txtValue == gridItem.getMember('Value').Value) {
                        isUnique = false;
                        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ValueAlreadyAdded %>' />");
                        break;
                    }
                }
            }
            return isUnique;
        }
        function ProductAttributeValue_OnCallbackError(sender, eventArgs) {
            alert(eventArgs.get_errorMessage());
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:Grid ID="ProductAttributeValue" SkinID="default" runat="server" Width="515"
        CallbackCachingEnabled="false" PageSize="10" RunningMode="Callback" AutoCallBackOnInsert="true"
        EnableViewState="false" AllowTextSelection="true" EditOnClickSelectedItem="false"
        AutoCallBackOnUpdate="true" AllowPaging="True" ItemDraggingEnabled="false" AllowMultipleSelect="false">
        <ClientEvents>
            <ContextMenu EventHandler="ProductAttributeValue_onContextMenu" />
            <CallbackError EventHandler="ProductAttributeValue_OnCallbackError" />
            <RenderComplete EventHandler="SetGridStyles" />
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" DataField="Value"
                        HeadingText="<%$ Resources:GUIStrings, Value %>" Align="Left" FixedWidth="true" EditCellServerTemplateId="svtName"
                        EditControlType="Custom" AllowEditing="True" Width="320" Visible="true" />
                    <ComponentArt:GridColumn AllowReordering="false" AllowSorting="False" IsSearchable="false"
                        HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left" EditControlType="Custom"
                        EditCellServerTemplateId="svtInfo" AllowEditing="True" Width="80" FixedWidth="true" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="svtName">
                <Template>
                    <asp:TextBox CssClass="textBoxes" ID="txtName" runat="server" Width="145"></asp:TextBox>&nbsp;
                    <span class="required">*</span>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtInfo">
                <Template>
                    <table>
                        <tr>
                            <td>
                                <img alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>" src="/iapps_images/cm-icon-add.png"
                                    id="ImageButton11" runat="server" causesvalidation="true" onclick="javascript:return ProductAttributeValue_BeforeUpdate();" />
                            </td>
                            <td>
                                <img runat="server" alt="<%$ Resources:GUIStrings, Delete %>" causesvalidation="false"
                                    title="<%$ Resources:GUIStrings, Cancel %>" src="/iapps_images/cm-icon-delete.png"
                                    id="ImageButton2" onclick="ProductAttributeValueCancelEditedValue();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="cmProductAttributeValue" runat="server" SkinID="contextMenu">
        <Items>
            <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, AddValue %>" ID="Add" Look-LeftIconUrl="cm-icon-add.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, EditValue %>" ID="Edit" Look-LeftIconUrl="cm-icon-edit.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem Text="<%$ Resources:GUIStrings, DeleteValue %>" ID="Delete" Look-LeftIconUrl="cm-icon-delete.png">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="ContextMenuClickHandlerMultiAttribute" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" runat="server" CssClass="primarybutton" />
</asp:Content>
