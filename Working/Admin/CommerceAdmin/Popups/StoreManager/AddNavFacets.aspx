﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.Master"
    CodeBehind="AddNavFacets.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.StoreManager.AddNavFacets"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="pa" TagName="ProductAttributes" Src="~/UserControls/StoreManager/Products/AttributeTree.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var gridOperationColumnIndex = 0;
        var gridOperationParameterIndex = 1;
        var selectedTreeNodeId;

        function onSaveClick() {
            var grdResults = window["grdAttributeFacets"];
            if (grdResults != "undefined" && grdResults != null) {
                var checkedItems = GetCheckedItems();
                if (checkedItems != "" && checkedItems != "undefined") {
                    parent.window["SelectedAttributeId"] = selectedTreeNodeId;
                    parent.window["SelectedFacets"] = checkedItems;
                    parent.ClosePopup();
                }
                else {
                    alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, PleaseSelectAtLeastOneFace %>"/>');
                }
            }
        }

        function cancelPopup() {
            parent.window["SelectedAttributeId"] = null;
            parent.window["SelectedFacets"] = null;
            parent.ClosePopup();
        }

        function GetCheckedItems() {
            var checkedItems = new Array();
            var grdResults = window["grdAttributeFacets"];
            var gridItem;
            var itemIndex = 0;
            while (gridItem = grdResults.get_table().getRow(itemIndex)) {
                if (gridItem.get_cells()[0].get_value()) {
                    checkedItems[checkedItems.length] = gridItem.getMember('Id').get_text();
                }
                itemIndex++;
            }
            return checkedItems;
        }


        function LoadNavFacetGrid() {
            if (grdAttributeFacets != undefined) {
                var rowitem = grdAttributeFacets.get_table().addEmptyRow(0);
                grdAttributeFacets.edit(rowitem);
                rowitem.SetValue(gridOperationColumnIndex, 'Load'); // action column
                rowitem.SetValue(gridOperationParameterIndex, selectedTreeNodeId); // action column
                grdAttributeFacets.editComplete();
                grdAttributeFacets.callback();
            }
        }

        function grdAttributeFacet_Load(sender, eventArgs) {
            //  LoadGrid();
        }
        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    treeAttributes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CollapseTree %>'/>";
                }
                else if (treeHandle.className == 'collapse') {
                    treeAttributes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings,ExpandTree %>'/>";
                }
            }
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="left-column">
        <div class="tree-header">
            <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings,ExpandTree %>" /></a>
        </div>
        <div class="tree-container">
            <pa:ProductAttributes ID="ctlProductAttributes" runat="server"></pa:ProductAttributes>
        </div>
    </div>
    <div class="right-column">
        <ComponentArt:Grid SkinID="Default" ID="grdAttributeFacets" SliderPopupClientTemplateId="attributeFacetsSliderTemplate"
            SliderPopupCachedClientTemplateId="attributeFacetsSliderTemplateCached" runat="server"
            RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" AllowEditing="true"
            AutoCallBackOnInsert="true" PagerInfoClientTemplateId="attributeFacetsPaginationTemplat"
            CallbackCachingEnabled="true" OnBeforeCallback="grdAttributeFacets_BeforeCallback"
            EditOnClickSelectedItem="false">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn ColumnType="CheckBox" Width="30" AllowEditing="True" Align="Center" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, FacetName %>"
                            DataField="Title" Align="Left" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                            AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                        <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, Limit %>"
                            DataField="Limit" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                            AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="LimitHoverTemplate" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, DisplayOrder1 %>"
                            DataField="DisplayOrderString" SortedDataCellCssClass="SortedDataCell" Align="Left"
                            AllowReordering="false" FixedWidth="true" IsSearchable="true" DataCellClientTemplateId="DisplayOrderHoverTemplate" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <Load EventHandler="grdAttributeFacet_Load" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                    <div title="## DataItem.GetMember('Title').get_text() ##">
                        ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="LimitHoverTemplate">
                    <div title="## DataItem.GetMember('Limit').get_text() ##">
                        ## DataItem.GetMember('Limit').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Limit').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="DisplayOrderHoverTemplate">
                    <div title="## DataItem.GetMember('DisplayOrderString').get_text() ##">
                        ## DataItem.GetMember('DisplayOrderString').get_text() == "" ? "&nbsp;" : DataItem.GetMember('DisplayOrderString').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="attributeFacetsSliderTemplate">
                    <div class="SliderPopup">
                        <p>
                            ##_datanotload##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdAttributeFacets.PageCount
                                ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdAttributeFacets.RecordCount
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="attributeFacetsSliderTemplateCached">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMemberAt(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMemberAt(1).Value ##</p>
                        <p class="paging">
                            <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdAttributeFacets.PageCount
                                ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdAttributeFacets.RecordCount
                                    ##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="attributeFacetsPaginationTemplat">
                    ## stringformat(Page0Of1Items, currentPageIndex(grdAttributeFacets), pageCount(grdAttributeFacets),
                    grdAttributeFacets.RecordCount) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="BtnClose" value="<%= GUIStrings.Close %>" class="button"
        title="<%= GUIStrings.Close %>" onclick="cancelPopup();" />
    <input type="button" id="btnAdd" value="<%= GUIStrings.Add %>" class="primarybutton"
        onclick="onSaveClick();"  />
    <script type="text/javascript">
        function AttributeTreeNodeSelect(isGroup) {
            LoadNavFacetGrid();
        }
    </script>
</asp:Content>
