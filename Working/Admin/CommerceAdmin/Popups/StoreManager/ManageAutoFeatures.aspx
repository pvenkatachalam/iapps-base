﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAutoFeatures.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ManageAutoFeatures"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.featurePopup.hide();
            return false;
        }

        var gridItem;
        var productIdColumn = 7; ;
        var featureIdColumn = 8;
        var includedColumn = 5;
        function grdAutoFeatures_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            gridItem = eventArgs.get_item();
            var menuItems = mnuAutoFeatures.get_items();
            if (gridItem.getMember(includedColumn).get_text() == "true") {
                menuItems.getItem(0).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ExcludeFromFeature %>' />");
            }
            else {
                menuItems.getItem(0).set_text("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IncludeInFeature %>' />");
            }
            mnuAutoFeatures.showContextMenuAtEvent(e);
        }
        function mnuAutoFeatures_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmDelete':
                        if (gridItem.getMember(includedColumn).get_text() == "true") {
                            RemoveProduct();
                        }
                        else {
                            IncludeProduct();
                        }
                        break;
                    case 'cmView':
                        ViewProducts();
                        break;
                }
            }
        }
        function RemoveProduct() {
            var productId = gridItem.getMember(productIdColumn).get_text();
            var featureId = gridItem.getMember(featureIdColumn).get_text();
            var isRemoved = ExcludeFromFeature(featureId, productId);
            if (isRemoved.toLowerCase() == 'true') {
                ShowStatusWise();
            }
            else {
                alert(isRemoved);
            }
        }
        function IncludeProduct() {
            var productId = gridItem.getMember(productIdColumn).get_text();
            var featureId = gridItem.getMember(featureIdColumn).get_text();
            var isRemoved = IncludeInFeature(featureId, productId);
            if (isRemoved.toLowerCase() == 'true') {
                ShowStatusWise();
            }
            else {
                alert(isRemoved);
            }
        }
        function ViewProducts() {
            var productId = gridItem.getMember(uniqueIdColumn).get_text();
            var skuId = emptyGuid;

            parent.ViewProducts(productId, skuId);
        }
        function SetStatus(chkStatusObject) {
            window["ShowInactive"] = chkStatusObject;
            ShowStatusWise();
        }
        function SetFeatureStatus(chkStatusObject) {
            window["ShowExcluded"] = chkStatusObject;
            ShowStatusWise();
        }
        function ShowStatusWise() {
            var actionColumnValue = "";
            var chkStatusObject = window["ShowInactive"];
            if (chkStatusObject != null && chkStatusObject.checked) {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';DisableDisplayOrderSort' + ';ShowInactive';
            }
            else {
                actionColumnValue = 'Load' + ';' + selectedTreeNodeId + ';DisableDisplayOrderSort' + ';ShowActive';
            }

            var chkFeatureStatusObject = window["ShowExcluded"];
            if (chkFeatureStatusObject != null && chkFeatureStatusObject.checked) {
                actionColumnValue = actionColumnValue + ';ShowExcluded';
            }
            else {
                actionColumnValue = actionColumnValue + ';No';
            }
            LoadGrid(actionColumnValue);
        }        
    </script>
    <!-- Grid management code -->
    <script type="text/javascript">
        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdAutoFeatures;
                menuObject = mnuAutoFeatures;
                gridObjectName = "grdAutoFeatures";
                uniqueIdColumn = 7;
                operationColumn = 6;
                selectedTreeNodeId = emptyGuid;
            }, 500);
        }

        function PageLevelCallbackComplete() {
        }        
    </script>
     <style type="text/css">
    #<%= grdAutoFeatures.ClientID %>_VerticalScrollDiv
    {
        max-height:320px !important;        
    }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="grid-utility">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 200px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Search %>"
                onclick="onClickSearchonGrid('txtSearch', grdAutoFeatures);" />
        </div>
        <div class="right-column">
            <asp:CheckBox ID="chkShowInactive" runat="server" Text="<%$ Resources:GUIStrings, IncludeInactiveProducts %>"
                onclick="javascript:SetStatus(this);" />
            <asp:CheckBox ID="ChkShowExcluded" runat="server" Text="<%$ Resources:GUIStrings, IncludeExcludedFeatures %>"
                onclick="javascript:SetFeatureStatus(this);" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Menu ID="mnuAutoFeatures" runat="server" SkinID="ContextMenu">
        <Items>
            <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, ExcludeFromFeature %>"
                Look-LeftIconUrl="cm-icon-delete.png">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem LookId="BreakItem">
            </ComponentArt:MenuItem>
            <ComponentArt:MenuItem ID="cmView" Text="<%$ Resources:GUIStrings, ViewProductDetails %>">
            </ComponentArt:MenuItem>
        </Items>
        <ClientEvents>
            <ItemSelect EventHandler="mnuAutoFeatures_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
    <ComponentArt:Grid SliderPopupClientTemplateId="grdAutoFeaturesSliderTemplate" ID="grdAutoFeatures"
        SkinID="Default" runat="server" Width="760" RunningMode="Callback"
        PagerInfoClientTemplateId="grdAutoFeaturesSliderPageInfoTemplate" AllowEditing="true"
        AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="cachedgrdAutoFeaturesSliderTemplate"
        CallbackCachingEnabled="true" AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10"
        PageSize="5" AllowPaging="True" ItemDraggingEnabled="false" ExternalDropTargets="grdAutoFeatures"
        AllowMultipleSelect="false" AllowVerticalScrolling="true"
        LoadingPanelClientTemplateId="grdAutoFeaturesLoadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Thumbnail %>"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="ImageTemplate" 
                        IsSearchable="false" DataField="ThumbNail" />
                    <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings, Name1 %>" TextWrap="true"
                        DataField="Title" FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="NameHoverTemplate"
                        IsSearchable="true" />
                    <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, SoldCount %>"
                        DataField="SoldCount" FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="SoldHoverTemplate" />
                    <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, CreatedDate %>"
                        DataField="CreatedDate" FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="CreatedDateHoverTemplate" />
                    <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="StatusTitle" FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="StatusHoverTemplate"
                        IsSearchable="true" />
                    <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, Included %>"
                        DataField="IsIncluded" SortedDataCellCssClass="SortedDataCell" DataCellCssClass="LastDataCell"
                        HeadingCellCssClass="LastHeadingCell" FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="IncludedHoverTemplate" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" Visible="false" HeadingCellCssClass="LastHeadingCell"
                        HeadingText="Actions" EditControlType="Custom" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="FeatureId" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdAutoFeatures_onContextMenu" />
            <SortChange EventHandler="grid_OnSortChange" />
            <CallbackComplete EventHandler="grid_onCallbackComplete" />
            <Load EventHandler="grid_onLoad" />
            <RenderComplete EventHandler="SetVerticalScroll" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ImageTemplate">
                <img src="## DataItem.GetMember(0).get_text() ##" alt="" />
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <div title="## DataItem.GetMember('Title').get_text() ##">
                    ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SoldHoverTemplate">
                <div title="## DataItem.GetMember('SoldCount').get_text() ##">
                    ## DataItem.GetMember('SoldCount').get_text() == "" ? "&nbsp;" : DataItem.GetMember('SoldCount').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CreatedDateHoverTemplate">
                <div title="## DataItem.GetMember('CreatedDate').get_text() ##">
                    ## DataItem.GetMember('CreatedDate').get_text() == "" ? "&nbsp;" : DataItem.GetMember('CreatedDate').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <div title="## DataItem.GetMember('StatusTitle').get_text()  ##">
                    ## DataItem.GetMember('StatusTitle').get_text() ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="IncludedHoverTemplate">
                <div title="## DataItem.GetMember('IsIncluded').get_text() == 'true' ? 'Yes' : 'No' ##">
                    ## DataItem.GetMember('IsIncluded').get_text() == 'true' ? 'Yes' : 'No' ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdAutoFeaturesSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdAutoFeatures.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdAutoFeatures.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="cachedgrdAutoFeaturesSliderTemplate">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdAutoFeatures.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdAutoFeatures.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdAutoFeaturesSliderPageInfoTemplate">
                ##stringformat(Page0Of12items, currentPageIndex(grdAutoFeatures), pageCount(grdAutoFeatures),
                grdAutoFeatures.RecordCount)##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdAutoFeaturesLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdAutoFeatures) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" ToolTip="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" runat="server" OnClientClick="return ClosePopup();" UseSubmitBehavior="false" />
</asp:Content>
