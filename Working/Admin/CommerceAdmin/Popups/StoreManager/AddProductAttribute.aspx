﻿<%@ Page Title="<%$ Resources:GUIStrings, AddAttributes %>" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master"
    AutoEventWireup="true" CodeBehind="AddProductAttribute.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.StoreManager.AddProductAttribute"
    StylesheetTheme="General" EnableEventValidation="false" %>

<%@ Register TagPrefix="pa" TagName="ProductAttributes" Src="~/UserControls/StoreManager/Products/AttributeTree.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">

        function SaveAndClosePopup() {
            if (isSku == 'true') {
                parent.reloadSKUAttribute(checkedNodes);
            }
            else {
                parent.reloadProductAttribute(checkedNodes);
            }
            ClosePopup();
        }
        function ClosePopup() {
            parent.pagepopup.hide();
            return false;
        }
        var selectedNodes = '';
        var checkedNodes = [];
        function AttributeTree_CheckChange(sender, eventArgs) {
            var node = eventArgs.get_node();
            var nodeId = node.get_id();
            var index = findIndex(checkedNodes, nodeId);
            if (node.Checked) {
                if (index >= 0)
                    checkedNodes[index] = nodeId;
                else
                    checkedNodes[checkedNodes.length] = nodeId;
            }
            else {
                if (index >= 0)
                    checkedNodes.remove(index);
            }
            //  selectedNodes = checkedNodes.join(",");
        }

        function findIndex(arr, value) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i] == value)
                    return i;
            }
            return -1;
        }

        Array.prototype.remove = function (from, to) {
            var rest = this.slice((to || from) + 1 || this.length);
            this.length = from < 0 ? this.length + from : from;
            return this.push.apply(this, rest);
        };

        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    treeAttributes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CollapseTree %>'/>";
                }
                else if (treeHandle.className == 'collapse') {
                    treeAttributes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ExpandTree %>'/>";
                }
            }
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <div class="tree-header">
        <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
    </div>
    <div class="tree-container">
        <pa:ProductAttributes ID="ctlProductAttributes" runat="server" EnableContextMenu="false"
            ShowCheckBox="false" ShowCheckBoxForAttribute="true" HideEmptyGroups="true">
        </pa:ProductAttributes>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings,  Cancel %>"
        CssClass="button" runat="server" OnClientClick="return ClosePopup();" />
    <asp:Button ID="btnAdd" Text="<%$ Resources:GUIStrings, Add %>" ToolTip="<%$ Resources:GUIStrings, Add %>"
        CssClass="primarybutton" runat="server" UseSubmitBehavior="false" />
</asp:Content>
