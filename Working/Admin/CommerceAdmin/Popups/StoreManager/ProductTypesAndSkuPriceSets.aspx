﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" StylesheetTheme="General"
    AutoEventWireup="true" CodeBehind="ProductTypesAndSkuPriceSets.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popup.StoreManager.ProductAndSkuPriceSets" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePopup() {
            parent.RefreshThisPage();
            parent.productTypePopup.hide();
            return false;
        }
        function OnNodeSelect(sender, eventArgs) {
            var productTypeId = eventArgs.get_node().get_id();
            var productType = eventArgs.get_node().get_text();
            //       grdAvailableSKUs.set_callbackParameter(productTypeId);        
            //       grdAvailableSKUs.callback();
            document.getElementById('selectedProductType').innerHTML = productType;
            grdAvailableSKUs.setProperty('EmptyGridText', '<b><asp:localize runat="server" text="<%$ Resources:GUIStrings, Loading %>"/></b>');
            grdAvailableSKUs.render();
            CallBackObject.callback(productTypeId);
        }

        function tree_NodeCheckChange(sender, eventArgs) {
            if (eventArgs.get_node().get_checked() == true) {
                if (eventArgs.get_node().get_nodes().get_length() > 0) {
                    var result = confirm("<asp:localize runat='server' text='<%$ Resources:GUIStrings, DoYouWantToSelectAllSubitems %>'/>")
                    if (result == true) eventArgs.get_node().checkAll();
                }
            }
            else if (eventArgs.get_node().get_showCheckBox() != null || eventArgs.get_node().get_showCheckBox() == 1) {
                if (eventArgs.get_node().get_nodes().get_length() > 0) {
                    var result = confirm("<asp:localize runat='server' text='<%$ Resources:GUIStrings, DoYouWantToDeselectAllSubitems %>'/>")
                    if (result == true) eventArgs.get_node().unCheckAll();
                }
            }
            trvProductTypes.render();
        }

        function AddProductType() {
            var treeNodes = trvProductTypes.get_nodes();
            InsertSelectedProductType(treeNodes.getNode(0));
        }
        var gridItem;
        function InsertSelectedProductType(node) {
            //var childNodes = node.getNode(0).get_nodes();        
            var childNodes = node.get_nodes();
            var currentNodeId = node.get_id();
            var currentNodeText = node.get_text();
            var urlPath = '';
            if (node.get_checked() == true) {
                if (!DuplicateProductTypeCheck(currentNodeId)) {
                    urlPath = GetPathUptoNode(node);
                    //-- Add to the grid
                    if (urlPath != '') {
                        grdSelectProductTypes.beginUpdate();
                        grdSelectProductTypes.get_table().addEmptyRow(0);
                        gridItem = grdSelectProductTypes.get_table().getRow(0);
                        grdSelectProductTypes.edit(gridItem);
                        gridItem.SetValue(0, urlPath);
                        gridItem.SetValue(1, currentNodeId);
                        grdSelectProductTypes.editComplete();
                    }
                }
                //-- deselect this node
                node.set_checked(false);
            }
            for (var i = 0; i < childNodes.get_length(); i++) {
                InsertSelectedProductType(childNodes.getNode(i));
            }
        }
        function GetPathUptoNode(snode) {
            var urlPath = '';
            var depth = snode.get_depth();
            for (var i = depth; i > 0; i--) {
                if (urlPath == '') {
                    urlPath = snode.get_text();
                }
                else {
                    urlPath = snode.get_text() + '\\' + urlPath;
                }
                snode = snode.get_parentNode();
            }
            return urlPath;
        }
        function DuplicateProductTypeCheck(thisId) {
            var len = grdSelectProductTypes.Data.length;
            var gItem;
            var othId;

            for (var i = 0; i < len; i++) {
                gItem = grdSelectProductTypes.get_table().getRow(i);
                othId = gItem.getMember(1).get_value();
                if (thisId == othId) {
                    return true;
                }
            }
            return false;
        }

        function RemoveProductType() {
            grdSelectProductTypes.deleteSelected();
        }
        var productTitleColumn = 0;
        var skuColumn = 1;
        var skuTitleColumn = 2;
        var skuIdColumn = 3;
        var productIdColumn = 4;

        var selectedProductTitle;
        var selectedSku;
        var selectedSkuTitle;
        var selectedSkuId;
        var selectedProductId;

        function CallComplete(sender, eventargs) {
            var prdTitle;
            var sku;
            var title;
            var skuId;
            var prdId;

            grdAvailableSKUs.selectAll();
            grdAvailableSKUs.deleteSelected();

            for (var i = gridItemArray.length - 1; i >= 0; i--) {
                prdTitle = gridItemArray[i][0];
                sku = gridItemArray[i][1];
                title = gridItemArray[i][2];
                skuId = gridItemArray[i][3];
                prdId = gridItemArray[i][4];

                //-- Add to the grid
                grdAvailableSKUs.beginUpdate();
                grdAvailableSKUs.get_table().addEmptyRow(0);
                gridItem = grdAvailableSKUs.get_table().getRow(0);
                gridItem.setValue(0, prdTitle);
                gridItem.setValue(1, sku);
                gridItem.setValue(2, title);
                gridItem.setValue(3, skuId);
                gridItem.setValue(4, prdId);
                grdAvailableSKUs.edit(gridItem);
            }
            grdAvailableSKUs.setProperty('EmptyGridText', '<b><asp:localize runat="server" text="<%$ Resources:GUIStrings, NoItemsFound %>"/></b>');
            grdAvailableSKUs.render();
        }

        function AddAllProducts() {
            grdAvailableSKUs.selectAll();
            AddSku();
            grdSelectedSKUs.sort(0, false);
        }
        function ClearAllProducts() {
            grdSelectedSKUs.selectAll();
            grdSelectedSKUs.deleteSelected();
        }
        function AddProducts() {
            //Take all selected items
            //loop all selected items and findout distinct product ids
            //loop through the grid and add all records which are matching with product ids
            //Add to grid
            var selectedItemsArray = grdAvailableSKUs.getSelectedItems();
            var selectedProductIds = '';
            for (var i = 0; i < selectedItemsArray.length; i++) {
                if (selectedProductIds != null && selectedProductIds != '') {
                    selectedProductIds = selectedProductIds + ',';
                }
                selectedProductIds = selectedProductIds + selectedItemsArray[i].getMember(productIdColumn).get_value();
            }

            var len = grdAvailableSKUs.Data.length;
            var gItem;
            var othId;
            for (var i = 0; i < len; i++) {
                gItem = grdAvailableSKUs.get_table().getRow(i);
                othId = gItem.getMember(productIdColumn).get_value(); //Product Id
                if (selectedProductIds.indexOf(othId) >= 0) {
                    grdAvailableSKUs.select(gItem);
                    AddSku();
                }
            }
            grdSelectedSKUs.sort(0, false);
        }
        function RemoveProducts() {
            var selectedItemsArray = grdSelectedSKUs.getSelectedItems();
            var selectedProductIds = '';
            for (var i = 0; i < selectedItemsArray.length; i++) {
                if (selectedProductIds != null && selectedProductIds != '') {
                    selectedProductIds = selectedProductIds + ',';
                }
                selectedProductIds = selectedProductIds + selectedItemsArray[i].getMember(productIdColumn).get_value();
            }

            grdSelectedSKUs.unSelectAll();
            var len = grdSelectedSKUs.Data.length;
            var gItem;
            var othId;
            for (var i = 0; i < len; i++) {
                gItem = grdSelectedSKUs.get_table().getRow(i);
                if (gItem != null) {
                    othId = gItem.getMember(productIdColumn).get_value(); //Product Id
                    if (selectedProductIds.indexOf(othId) >= 0) {
                        grdSelectedSKUs.select(gItem, true);
                    }
                }
            }
            RemoveSku();
        }
        function AddSelectedSku() {
            AddSku();
            grdSelectedSKUs.sort(0, false);
        }
        function AddSku() {
            var selectedItemsArray = grdAvailableSKUs.getSelectedItems();
            for (var i = 0; i < selectedItemsArray.length; i++) {
                selectedProductTitle = selectedItemsArray[i].getMember(productTitleColumn).get_value();
                selectedSku = selectedItemsArray[i].getMember(skuColumn).get_value();
                selectedSkuTitle = selectedItemsArray[i].getMember(skuTitleColumn).get_value();
                selectedSkuId = selectedItemsArray[i].getMember(skuIdColumn).get_value();
                selectedProductId = selectedItemsArray[i].getMember(productIdColumn).get_value();

                if (!DuplicateSkusCheck(selectedSkuId)) {
                    //-- Add to the grid
                    grdSelectedSKUs.beginUpdate();
                    grdSelectedSKUs.get_table().addEmptyRow(0);
                    gridItem = grdSelectedSKUs.get_table().getRow(0);
                    grdSelectedSKUs.edit(gridItem);

                    gridItem.SetValue(productTitleColumn, selectedProductTitle);
                    gridItem.SetValue(skuColumn, selectedSku);
                    gridItem.SetValue(skuTitleColumn, selectedSkuTitle);
                    gridItem.SetValue(skuIdColumn, selectedSkuId);
                    gridItem.SetValue(productIdColumn, selectedProductId);

                    grdSelectedSKUs.editComplete();
                }
                grdAvailableSKUs.unSelectAll();
            }
        }
        function DuplicateSkusCheck(thisId) {
            var len = grdSelectedSKUs.Data.length;
            var gItem;
            var othId;

            for (var i = 0; i < len; i++) {
                gItem = grdSelectedSKUs.get_table().getRow(i);
                if (gItem != null) {
                    othId = gItem.getMember(skuIdColumn).get_value(); //Sku Id
                    if (thisId == othId) {
                        return true;
                    }
                }
            }
            return false;
        }
        function RemoveSku() {
            grdSelectedSKUs.deleteSelected();
        }
    
    </script>
    <style type="text/css">
    #<%= grdAvailableSKUs.ClientID %>_VerticalScrollDiv,
    #<%= grdSelectedSKUs.ClientID %>_VerticalScrollDiv
    {
        height:240px !important;
    }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="leftColumn">
        <h4>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ProductTypes %>" /></h4>
        <div class="scroll-box">
            <ComponentArt:TreeView ID="trvProductTypes" runat="server" SkinID="Default" AutoScroll="false"
                FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
                <ClientEvents>
                    <NodeCheckChange EventHandler="tree_NodeCheckChange" />
                    <NodeSelect EventHandler="OnNodeSelect" />
                </ClientEvents>
            </ComponentArt:TreeView>
        </div>
        <div class="heading-row">
            <h4>
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AvailableSKUs %>" /></h4>
            <span id="selectedProductType">Click a node in above tree</span>
            <div class="clear-fix">
            </div>
        </div>
        <div class="grid-utility">
            <input type="text" id="txtAvailableSku" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 180px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Search %>"
                onclick="onClickSearchonGrid('txtAvailableSku', grdAvailableSKUs);" />
            <div class="clear-fix">
            </div>
        </div>
        <ComponentArt:Grid ID="grdAvailableSKUs" SkinID="Default" runat="server" Width="100%"
            ShowHeader="false" ShowFooter="false" RunningMode="Client" SearchOnKeyPress="true"
            AutoPostBackOnSelect="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            PageSize="10" AllowPaging="true" AllowEditing="false" AutoCallBackOnUpdate="false"
            AllowMultipleSelect="true" AllowVerticalScrolling="true">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="138" HeadingText="<%$ Resources:GUIStrings, SKU %>" DataField="SKU"
                            HeadingGripImageWidth="6" HeadingGripImageHeight="8" AllowReordering="false"
                            FixedWidth="true" DataCellClientTemplateId="SSSkuHoverTemplate" IsSearchable="true"
                            AllowSorting="True" />
                        <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                            DataField="ProductTitle" DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" DataCellClientTemplateId="SSProductNoHoverTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn Width="136" HeadingText="<%$ Resources:GUIStrings, Name1 %>"
                            DataField="Title" AllowReordering="false" DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell"
                            FixedWidth="true" AllowSorting="True" DataCellClientTemplateId="SSNameHoverTemplate"
                            IsSearchable="true" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <RenderComplete EventHandler="SetVerticalScroll" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="APProductNoHoverTemplate">
                    <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="APSkuHoverTemplate">
                    <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="APNameHoverTemplate">
                    <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
        <ComponentArt:CallBack ID="CallBackObject" runat="server" PostState="true">
            <Content Visible="false">
                <div runat="server" id="phContainer">
                </div>
            </Content>
            <ClientEvents>
                <CallbackComplete EventHandler="CallComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <div class="centerColumn">
        <div class="topRow">
            <input type="button" value="<%= GUIStrings.AddRight %>" class="gray-right-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.AddRight %>" onclick="AddProductType();" /><br />
            <input type="button" value="<%= GUIStrings.RemoveLeft %>" class="gray-left-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.Remove %>" onclick="RemoveProductType();" />
        </div>
        <div class="bottomRow">
            <input type="button" value="<%= GUIStrings.AddAllProducts %>" class="gray-right-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.AddAllProducts %>" onclick="AddAllProducts();" /><br />
            <input type="button" value="<%= GUIStrings.ClearAllProducts %>" class="gray-left-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.ClearSelectedProducts %>" onclick="ClearAllProducts();" /><br />
            <input type="button" value="<%= GUIStrings.AddProductRight %>" class="gray-right-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.Add %>" onclick="AddProducts();" /><br />
            <input type="button" value="<%= GUIStrings.RemoveProductLeft %>" class="gray-left-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.Remove %>" onclick="RemoveProducts();" /><br />
            <input type="button" value="<%= GUIStrings.AddSKURight %>" class="gray-right-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.AddSKU %>" onclick="AddSelectedSku();" /><br />
            <input type="button" value="<%= GUIStrings.RemoveSKULeft %>" class="gray-left-arrow-button"
                style="width: 150px;" title="<%= GUIStrings.RemoveSKU %>" onclick="RemoveSku();" />
        </div>
    </div>
    <div class="rightColumn">
        <h4>
            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, SelectedProductTypes %>" /></h4>
        <div class="scroll-box">
            <ComponentArt:Grid ID="grdSelectProductTypes" SkinID="Default" runat="server" Width="358"
                Height="220" ShowHeader="false" ShowFooter="false" RunningMode="Client" AutoPostBackOnSelect="false"
                EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" ScrollBar="On" 
                AllowEditing="false" AutoCallBackOnUpdate="false" AllowMultipleSelect="true" PageSize="1000"
                AllowPaging="false">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                        DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                        AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                        ShowSelectorCells="false" ShowHeadingCells="false">
                        <Columns>
                            <ComponentArt:GridColumn Width="332" HeadingText="<%$ Resources:GUIStrings, Name1 %>"
                                DataField="Title" HeadingGripImageWidth="6" HeadingGripImageHeight="8" AllowReordering="false"
                                FixedWidth="true" AllowSorting="True" DataCellClientTemplateId="NameHoverTemplate"
                                IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                        <span title="## DataItem.GetMember(0).get_text() ##">## DataItem.GetMember(0).get_text()
                            == "" ? "&nbsp;" : DataItem.GetMember(0).get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
        <h4>
            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, SelectedSKUs %>" /></h4>
        <div class="grid-utility">
            <input type="text" id="txtSearchSku" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 180px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Search %>"
                onclick="onClickSearchonGrid('txtSearchSku', grdSelectedSKUs);" />
            <div class="clear-fix">
            </div>
        </div>
        <ComponentArt:Grid ID="grdSelectedSKUs" SkinID="Default" runat="server" Width="100%"
            ShowHeader="false" ShowFooter="false" RunningMode="Client" AutoPostBackOnSelect="false"
            EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" PageSize="1000" AllowEditing="false"
            AutoCallBackOnUpdate="false" AllowMultipleSelect="true" AllowPaging="true" AllowVerticalScrolling="true">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="138" HeadingText="<%$ Resources:GUIStrings, SKU %>" DataField="SKU"
                            HeadingGripImageWidth="6" HeadingGripImageHeight="8" AllowReordering="false"
                            FixedWidth="true" DataCellClientTemplateId="SSSkuHoverTemplate" IsSearchable="true"
                            AllowSorting="True" />
                        <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                            DataField="ProductTitle" DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell"
                            AllowReordering="false" FixedWidth="true" AllowSorting="True" DataCellClientTemplateId="SSProductNoHoverTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn Width="136" HeadingText="<%$ Resources:GUIStrings, Name1 %>"
                            DataField="Title" HeadingGripImageWidth="6" HeadingGripImageHeight="8" AllowReordering="false"
                            FixedWidth="true" AllowSorting="True" DataCellClientTemplateId="SSNameHoverTemplate"
                            IsSearchable="true" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <RenderComplete EventHandler="SetVerticalScroll" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="SSProductNoHoverTemplate">
                    <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SSSkuHoverTemplate">
                    <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SSNameHoverTemplate">
                    <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                        == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" title="<%= GUIStrings.Cancel %>" value="<%= GUIStrings.Cancel %>"
        class="button" onclick="ClosePopup();" />
    <asp:Button ID="btnSave" runat="server" ToolTip="<%$ Resources:GUIStrings, Save %>" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" OnClick="btnSave_Click" />
</asp:Content>
