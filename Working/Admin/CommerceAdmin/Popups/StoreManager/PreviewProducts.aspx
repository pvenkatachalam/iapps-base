﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewProducts.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.PreviewProducts"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var _totalProducts = "<%=GUIStrings.TotalProducts %>";

        function ShowTotalProducts() {
            setTimeout(HidePreviewProductsSlider, 1);

            return _totalProducts + grdPreviewProducts.RecordCount;
        }

        function HidePreviewProductsSlider() {
            $("#" + grdPreviewProducts.get_id() + "_footer table").find("td").first().hide();
        }
    </script>
    <style type="text/css">
    #<%= grdPreviewProducts.ClientID %>_VerticalScrollDiv
    {
        max-height:420px !important;        
    }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:Grid SkinID="Default" ID="grdPreviewProducts" runat="server" RunningMode="Client"
        EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AutoPostBackOnSelect="false"
        PagerInfoClientTemplateId="grdPreviewProductsSliderPageInfoTemplate" AllowEditing="true"
        PageSize="1000" AllowPaging="False" ItemDraggingEnabled="false" AllowMultipleSelect="false"
        AllowVerticalScrolling="true">
        <Levels>
            <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Thumbnail %>" 
                        DataField="Thumbnail" IsSearchable="true" AllowReordering="false" FixedWidth="true" Align="Center"
                        DataCellClientTemplateId="SvtThumbnail" />
                    <ComponentArt:GridColumn Width="280" HeadingText="<%$ Resources:GUIStrings, Name1 %>"
                        DataField="Name" IsSearchable="true" DataCellClientTemplateId="TemplateName" TextWrap="true"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        Align="Left" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, SKU %>" DataField="SKU"
                        SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                        Align="Left" />
                    <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="Type" DefaultSortDirection="Descending" SortedDataCellCssClass="SortedDataCell"
                        FixedWidth="true" AllowReordering="false" Align="Left" Visible="true" />
                    <ComponentArt:GridColumn Width="195" HeadingText="<%$ Resources:GUIStrings, Description %>"
                        DataField="Description" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                        HeadingCellCssClass="HeadingCell" DataCellClientTemplateId="TemplateDesc" AllowReordering="false"
                        Align="Left" Visible="false" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, DateCreated %>"
                        DataField="LastModifiedDate" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                        DataCellCssClass="LastDataCell"
                        HeadingCellCssClass="LastHeadingCell" FormatString="d" AllowReordering="false" Align="Left" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="grdPreviewProductsSliderPageInfoTemplate">
                ## ShowTotalProducts() ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SvtThumbnail">
                <img src="## DataItem.GetMember('Thumbnail').get_value() ##" alt="## DataItem.GetMember('Name').get_value() ##"
                    title="## DataItem.GetMember('Name').get_value() ##" />
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TemplateName">
                <span title="## DataItem.GetMember('Name').get_value() ##">## DataItem.GetMember('Name').get_value()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TemplateDesc">
                <span title="## DataItem.GetMember('Description').get_value() ##">## DataItem.GetMember('Description').get_value()
                    ##</span>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ClientEvents>
            <RenderComplete EventHandler="SetVerticalScroll" />
        </ClientEvents>
    </ComponentArt:Grid>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="BtnClose" value="<%= GUIStrings.Close %>" class="button"
        title="<%= GUIStrings.Close %>" onclick="parent.pagepopup.hide();" />
</asp:Content>
