﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.Commerce.Search.NavigationCategoryMultiSelect"
    StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectNavigationCategory %>" />
    </title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        var selectedNavigationCategorys = [];
        var selectedNavigationCategoryName = [];
        
        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');

            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    trvNavigationCategorys.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CollapseTree %>' />";
                }
                else if (treeHandle.className == 'collapse') {
                    trvNavigationCategorys.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ExpandTree %>' />";
                }
            }

            return false
        }

        function OnNodeCheckChanged(sender, eventArgs) {         
            var node = eventArgs.get_node();
            var NavigationCategoryId = node.ID;
            var NavigationCategoryTitle = node.Text;

            if (node.get_checked()) {
                selectedNavigationCategorys[selectedNavigationCategorys.length] = NavigationCategoryId;
                selectedNavigationCategoryName[selectedNavigationCategoryName.length] = NavigationCategoryTitle;
            }
            else {
                var idLocation = selectedNavigationCategorys.indexOf(NavigationCategoryId);
                if (idLocation >= 0) {
                    selectedNavigationCategorys.splice(idLocation, 1);
                }

                var titleLocation = selectedNavigationCategoryName.indexOf(NavigationCategoryTitle);
                if (titleLocation >= 0) {
                    selectedNavigationCategoryName.splice(titleLocation, 1);
                }
            }
        }

        function SetSelectedNavigationCategorys() {
            if (selectedNavigationCategorys.length > 0) {
                parent.SetNavigationCategorys(selectedNavigationCategorys, selectedNavigationCategoryName);
                ClosePopup();
            }
            else {
                alert('<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseSelectAtLeastOneNavigationCategory %>' />');
            }
        }

        function ClosePopup() {
            parent.navigationCategoryPopup.hide();

            return false;
        }
    
    </script>
</head>
<body>
    <form id="frmNavigationCategory" runat="server">
    <div class="iapps-modal AddAttribute">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.SelectNavigationCategory %></h2>
        </div>
        <div class="modal-content clear-fix">
            <div id="divPageTopContainer" class="messageContainer" runat="server">
            </div>
            <div class="tree-header">
                <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                    <%= GUIStrings.ExpandTree %></a>
            </div>
            <div class="tree-container">
                <ComponentArt:TreeView ID="trvNavigationCategorys" runat="server" SkinID="Default"
                    AutoScroll="false" FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
                    <ClientEvents>
                        <NodeCheckChange EventHandler="OnNodeCheckChanged" />
                    </ClientEvents>
                </ComponentArt:TreeView>
            </div>
            <asp:SiteMapDataSource ID="SiteMapDSNav" runat="server" SiteMapProvider="folderNavProvider"
                ShowStartingNode="True" />
        </div>
        <div class="modal-footer">
            <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
                class="button" onclick="ClosePopup();" />
            <input type="button" value="<%= GUIStrings.Add %>" title="<%= GUIStrings.Add %>"
                class="primarybutton" onclick="SetSelectedNavigationCategorys();" />
        </div>
    </div>
    </form>
</body>
</html>
