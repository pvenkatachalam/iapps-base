﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductSearch2.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.StoreManager.ProductSearch2" StylesheetTheme="General" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Bridgeline.FW.Commerce.Controls" Namespace="Bridgeline.FW.Commerce.Controls"
    TagPrefix="sku" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProductSearch %>" /></title>
    <link href="../../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
    <style type="text/css">
        .slider_rail
        {
            position: relative;
            height: 20px;
            width: 155px;
            background: #FFFFFF url(../../App_Themes/General/images/slider_bg.gif) repeat-x;
        }
        .slider_handle
        {
            position: absolute;
            height: 20px;
            width: 22px;
        }
    </style>
    <link href="../../css/Products.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        var blContinue = true;
        var skuSelectMessage = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PleaseSelectAtLeastOneSKU %>"/>';
        function onCloseClick() {
            parent.window["SelectedProducts"] = '';
            parent.ClosePopup('');
        }

        function toggle(checkBox, count) {

            if (count > 0)
                cb.checked = true;
            else
                cb.checked = false;
        }

        function toggleDisplay(element, productCheckBox) {


          
            var productCB = document.getElementById(productCheckBox);
            var div = document.getElementById(element);

            if (!productCB.checked && div.style.display == "block")
                blContinue = true;

            if (productCB.checked && div.style.display == "none" && !blContinue) {
                alert(skuSelectMessage);
                productCB.checked = false;
            }
                
            
            if (blContinue) {

                blContinue = false;               
                if (div.style.display == "none") {
                    div.style.display = "block";                   
                }
                else {

                    var inputCollection = div.getElementsByTagName("input");

                    var count = 0;
                    for (i = 0; i < inputCollection.length; i++) {
                        if (inputCollection[i].checked)
                            count++;
                    }


                    if (count == 0 && productCB.checked) {                      
                        blContinue = false;
                        return false;
                    } else {
                        blContinue = true;
                        div.style.display = "none";

                    }
                }
            }
            
        }

        function toggleDisplayAndButton(element, image, productCheckBox) {

            var productCB = document.getElementById(productCheckBox);
            var div = document.getElementById(element);

            if (div.style.display == "none" && !blContinue) {
                alert(skuSelectMessage);
                productCB.checked = false;
            }
            else if (!blContinue) {
                alert(skuSelectMessage);
            }
            
            if (blContinue) {
              

                if (div.style.display == "none") {
                    div.style.display = "block";
                    image.src = image.src.replace("snap-maximize.gif", "snap-minimize.gif");
                }
                else {

                    var inputCollection = div.getElementsByTagName("input");

                    var count = 0;
                    for (i = 0; i < inputCollection.length; i++) {
                        if (inputCollection[i].checked)
                            count++;
                    }

                    if (count == 0 && productCB.checked) {
                       // alert('Please select at least one SKU');
                        blContinue = false;
                        return false;
                    } else {
                        blContinue = true;
                        div.style.display = "none";
                        image.src = image.src.replace("snap-minimize.gif", "snap-maximize.gif");
                    }
                }
            }
        }

        function skuChecked(checkList, productCheckBox, cbAll) {
            var checkBoxList = document.getElementById(checkList);
            var checkAll = document.getElementById(cbAll);
            var inputCollection = checkBoxList.getElementsByTagName("input");
            var cb = document.getElementById(productCheckBox);
            var count = 0;
            for (i = 0; i < inputCollection.length; i++) {
                if (inputCollection[i].checked)
                    count++;
            }
            if (count > 0) {
                blContinue = true;
                cb.checked = true;
            }
            else {
                cb.checked = false;
            }

            if (count == inputCollection.length)
                checkAll.checked = true;
            else
                checkAll.checked = false;

        }

        function CheckAllSkus(checkAll, checkList) {
            var selectAllCheckbox = document.getElementById(checkAll);
            var checkBoxList = document.getElementById(checkList);
            var inputCollection = checkBoxList.getElementsByTagName("input");
            for (i = 0; i < inputCollection.length; i++) {
                inputCollection[i].checked = selectAllCheckbox.checked;
            }
        }
        function UncheckAllSkus(checkAll, checkList) {
            var selectAllCheckbox = document.getElementById(checkAll);
            var checkBoxList = document.getElementById(checkList);
            var inputCollection = checkBoxList.getElementsByTagName("input");
            for (i = 0; i < inputCollection.length; i++) {
                inputCollection[i].checked = false;
            }
            selectAllCheckbox.checked = false;
        }

        function SetAddButton() {

            if (!Page_ClientValidate()) {
                document.getElementById(bntAddClientID).disabled = true;
            }

        }
        function validateSKUS() {          
            if (!blContinue)
                alert(skuSelectMessage);
            return blContinue;
        }
    </script>

</head>
<body class="popupBody">
    <form id="form1" runat="server">
    <div class="popupOverlay" id="popupOverlay">
        <table border="0" cellpadding="0" cellspacing="0" width="750">
            <tr>
                <td height="400" align="center">
                    <img runat="server" src="../../App_Themes/General/images/spinner.gif" alt="<%$ Resources:GUIStrings, Loading %>" />
                </td>
            </tr>
        </table>
    </div>
    <cc1:ToolkitScriptManager EnablePageMethods="true" ID="sm1" EnableScriptGlobalization="true"
        ScriptMode="Release" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/script/webkit.js" />
        </Scripts>
    </cc1:ToolkitScriptManager>

    <div class="popupBoxShadow productSearch">
        <div class="popupboxContainer"> 
            <div class="dialogBox">
                <!-- Message Information STARTS -->
                <div id="divPageTopContainer" class="messageContainer" runat="server">
                </div>
                <!-- Message Information ENDS -->
                <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">

                <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div class="middleContent">
                            <div class="formArea">
                                <div class="productSearchBox">
                                  <asp:ValidationSummary ID="valSumSearch" runat="server" EnableClientScript="true"
                                        ShowSummary="true" ValidationGroup="SearchGroup" />
                                    <asp:TextBox ID="txtSearchTerm" runat="server" CssClass="textBoxes"></asp:TextBox>
                                    <div style="float: left;">
                                        <asp:RequiredFieldValidator CultureInvariantValues="true" Display="Dynamic" ID="reqSearch" ControlToValidate="txtSearchTerm"
                                            runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterASearchTerm %>"  ValidationGroup="SearchGroup">
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" ID="regExSearch" runat="server"
                                            ControlToValidate="txtSearchTerm" ValidationExpression="^[^<>()&]+$" ErrorMessage="<%$ Resources:GUIStrings, SearchTermContainsInvalidCharacters %>"
                                            Text="*" ValidationGroup="SearchGroup">
                                        </asp:RegularExpressionValidator>
                                    </div>
                                    <asp:Button ID="btnSearch" OnClientClick="SetAddButton();" Text="<%$ Resources:GUIStrings, Search %>" runat="server" OnClick="btnSearch_Click"
                                        ValidationGroup="SearchGroup" CssClass="button" />
                                </div>
                                <h3><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SearchAndAddProducts %>" /></h3>
                                <div class="clearFix">&nbsp;</div>
                                <asp:Panel ID="pnlNoResults" runat="server" Visible="false">
                                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, NoSearchResults %>" />
                                </asp:Panel>
                                <div class="searchResultContainer" style="height:375px;overflow:auto;">
                                    <asp:Panel ID="pnlSearchResults" runat="server">
                                        <asp:GridView runat="server" ID="gvProducts" EnableViewState="true" CssClass="Grid"
                                            HeaderStyle-CssClass="HeadingRow" GridLines="Both" Width="100%" OnRowCommand="gvProducts_RowCommand"
                                            OnRowDataBound="gv_DataBound" AutoGenerateColumns="False" PagerSettings-Visible="true"
                                            PageSize="6" AllowPaging="true" AlternatingRowStyle-CssClass="AlternateDataRow"
                                            RowStyle-CssClass="Row">
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemStyle Width="50" CssClass="DataCell" />
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hfProductId" runat="server" />
                                                        <asp:CheckBox ID="cbProductSelected" runat="server" />
                                                        <asp:ImageButton ID="btnOpenSkus" runat="server" ImageUrl="~/images/snap-maximize.gif" />
                                                        <div class="SKULayer" id="divSKUS" runat="server" style="display: none; left: 50px;
                                                            text-align: left;">
                                                            <div style="width: 100%;" class="HeadingRow">
                                                                <b><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectSKU %>" /></b></div>
                                                            <asp:CheckBox ID="cbSelectAll" runat="server" Text="<%$ Resources:GUIStrings, CheckAll %>" />
                                                            <asp:Button ID="btnClose" Visible="false" runat="server" Text="<%$ Resources:GUIStrings, Close %>" ImageAlign="Right"
                                                                CssClass="button" />
                                                            <FWControls:MultiTextFieldCheckBoxList ID="cblSKUS" runat="server" />
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Thumbnails">
                                                    <ItemStyle CssClass="DataCell" />
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgProductThumbnail" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Name %>" ItemStyle-CssClass="DataCell">
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; height: 100%;" onclick="__doPostBack('gvProducts$ctl01$btnSortName','')">
                                                            <asp:LinkButton ID="btnSortName" runat="server" CommandName="Sort" CommandArgument="Name"
                                                                Text="<%$ Resources:GUIStrings, Name %>" /></div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltlName" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Description %>">
                                                    <ItemStyle CssClass="DataCell" />
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; height: 100%;" onclick="__doPostBack('gvProducts$ctl01$bntSortDescription','')">
                                                            <asp:LinkButton ID="bntSortDescription" runat="server" CommandName="Sort" CommandArgument="Description"
                                                                Text="<%$ Resources:GUIStrings, Description %>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltlDescription" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Quantity %>" ItemStyle-CssClass="DataCell">
                                                    <HeaderTemplate>
                                                        <div style="width: 100%; height: 100%" onclick="__doPostBack('gvProducts$ctl01$btnSortQuantity','')">
                                                            <asp:LinkButton ID="btnSortQuantity" runat="server" CommandName="Sort" CommandArgument="Quantity"
                                                                Text="<%$ Resources:GUIStrings, Quantity %>" />
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Literal ID="ltlQuantity" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>
                                                                                    
                                                <table cellpadding="1" cellspacing="1" width="100%">
                                                    <tr class="Row">
                                                        <td>
                                                            <asp:ImageButton ID="btnPrev" OnClientClick="return validateSKUS();" runat="server" CommandName="Prev" onmouseout="this.src='../../App_Themes/General/images/prev.gif';"
                                                                onmouseover="this.src='../../App_Themes/General/images/prev_hover.gif';" ImageUrl="~/App_Themes/General/images/prev.gif" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtSlider" runat="server" AutoPostBack="True" Text='<%# gvProducts.PageIndex + 1 %>'
                                                                OnTextChanged="txtSlider_TextChanged" Width="200px"></asp:TextBox>
                                                            <cc1:SliderExtender ID="SliderExtender1" TooltipText="<%$ Resources:GUIStrings, Page0 %>" EnableHandleAnimation="false"
                                                                RailCssClass="slider_rail" HandleCssClass="slider_handle" runat="server" Orientation="Horizontal"
                                                                HandleImageUrl="~/App_Themes/General/images/slider_grip.gif" TargetControlID="txtSlider"
                                                                Minimum="1" Steps='<%# gvProducts.PageCount %>' Maximum='<%# ((GridView)Container.NamingContainer).PageCount %>'>
                                                            </cc1:SliderExtender>
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton OnClientClick="return validateSKUS();" ID="btnNext" runat="server" CommandName="Next" onmouseout="this.src='../../App_Themes/General/images/next.gif';"
                                                                onmouseover="this.src='../../App_Themes/General/images/next_hover.gif';" ImageUrl="~/App_Themes/General/images/next.gif" />
                                                        </td>
                                                        <td style="width: 99%; border-left: 0px; border-bottom: 0px; text-align: right;"
                                                            align="right" class="DataCell">
                                                            Page <b>
                                                                <asp:Literal ID="ltlPageIndex" Text='<%# gvProducts.PageIndex + 1 %>' runat="server" /></b>
                                                            of <b>
                                                                <asp:Literal ID="ltlPageTotal" runat="server" Text='<%# gvProducts.PageCount %>' /></b>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                        </asp:GridView>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="footerContent">
                            <asp:Button ID="bntAdd" runat="server" CssClass="primarybutton" CausesValidation="false"
                                Text="<%$ Resources:GUIStrings, Add %>" OnClick="btnAdd_Click" OnClientClick="return validateSKUS();" />
                            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClick="btnClose_Click" />
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="bntAdd" />
                        <asp:PostBackTrigger ControlID="btnClose" />
                    </Triggers>
                </asp:UpdatePanel>

                </asp:Panel>
            </div>
        </div>
    </div>
    </form>
    <script type="text/javascript" defer="defer">
        function init() {
            Sys.Net.WebRequestManager.add_invokingRequest(beginRequest);
            Sys.Net.WebRequestManager.add_completedRequest(endRequest);
        }
        function beginRequest(sender, args) {
            var objPopupOverlay = document.getElementById("popupOverlay");
            if (objPopupOverlay != null && objPopupOverlay != undefined) {
                objPopupOverlay.style.display = "block";
            }
        }
        function endRequest(sender, args) {
            try {
                var objPopupOverlay = document.getElementById("popupOverlay");
                if (objPopupOverlay != null && objPopupOverlay != undefined) {
                    objPopupOverlay.style.display = "none";
                }
            }
            catch (exp) {
            }
        }
        init();

    </script>
</body>
</html>
