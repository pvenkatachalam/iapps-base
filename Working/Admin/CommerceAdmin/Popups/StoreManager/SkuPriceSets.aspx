﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" StylesheetTheme="General"
    EnableEventValidation="false" AutoEventWireup="true" CodeBehind="SkuPriceSets.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SkuPriceSets" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var _PageIndexChangeConfirmation = '<asp:Localize runat="server" Text="<%$ Resources:JSMessages, PageIndexChangeConfirmation %>" />';
        var gridItem;
        var productSearchPopup;
        var searchPageURL;
        var lastAction = '';
        var contentChanged = false;

        function CloseThisPopup() {
            parent.skuPopup.hide();
            parent.RefreshThisPage();
            return true;
        }

        function ValidateAndClosePopup() {
            var isApplyValidation = true;
            if (grdPriceSetSKUs.RecordCount == 0) {
                isApplyValidation = false;
            } else if (grdPriceSetSKUs.RecordCount == 1) {
                gridItem = grdPriceSetSKUs.get_table().getRow(0);
                if (gridItem.getMember(0).get_value() == null || gridItem.getMember(0).get_value() == '') {
                    isApplyValidation = false;
                }
            }
            if (isApplyValidation) {
                try {
                    if (Page_ClientValidate('ValGroup1') == true) {
                        //CloseThisPopup();
                        contentChanged = false;
                    }
                    else {
                        return false;
                    }
                }
                catch (ex) {
                    //Todo: DE1083: IE8 ERROR ON REMOVING SKU AND HITING SAVE BUTTON
                    //Page_ClientValidate() failed in IE8 while remove a SKU and click on save. because it is getting the client textbox objects like (txtMinQty) of removed row..
                    //I hope it may be rectified by the latest CA Grid
                }
            }
            else
                return true;
        }

        function grdPriceSetSKUs_onContextMenu(sender, eventArgs) {
            gridItem = eventArgs.get_item();
            var menuItems = mnuPriceSetSKU.get_items();
            menuItems.getItem(1).set_visible(true);
            menuItems.getItem(2).set_visible(true);
            if (gridItem.getMember(0).get_value() == null || gridItem.getMember(0).get_value() == '') {
                menuItems.getItem(1).set_visible(false);
                menuItems.getItem(2).set_visible(false);
            }
            var e = eventArgs.get_event();
            mnuPriceSetSKU.showContextMenuAtEvent(e);
        }

        function ContentChanged() {
            contentChanged = true;
        }

        function grdPriceSetSkus_onPageIndexChange(sender, eventArgs) {
            var r = true;

            if (contentChanged)
                r = confirm(_PageIndexChangeConfirmation);

            contentChanged = false;
            if (r == false)
                eventArgs.set_cancel(true);
        }

        function grdPriceSetSkus_CallbackComplete(sender, eventArgs) {
            grdPriceSetSKUs.set_callbackParameter('');

            if (lastAction == 'Delete' && sender.get_recordCount() != 0) {
                if (sender.get_recordCount() % sender.get_pageSize() == 0) {
                    sender.previousPage();
                }
            }
        }

        function mnuPriceSetSKU_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        lastAction = 'Add';
                        ShowAddProductPopup();
                        break;
                    case 'cmDelete':
                        lastAction = 'Delete';
                        var skuId = gridItem.getMember(6).get_text();
                        grdPriceSetSKUs.set_callbackParameter('R' + skuId);
                        grdPriceSetSKUs.callback();
                        break;
                }
            }
        }

        function ShowAddProductPopup() {
            productSearchPopup = OpeniAppsCommercePopup('AddProductPopup', 'showProducts=false')
            productSearchPopup.onclose = function () {
                var a = document.getElementById('AddProductPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        function CloseProductPopup(arrSelected) {
            productSearchPopup.hide();
            if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                var skuIds = '';
                while (arrSelected.length != 0) {
                    skuIds += ',' + arrSelected.pop()[1];
                }
                if (skuIds != '') {

                    skuIds = skuIds.substring("1");

                    AddSkus(skuIds);
                }
                else {
                    alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, ThereIsNoSkuToAdd %>" />');
                }
            }
        }

        function AddSkus(skuIds) {
            if (skuIds != null && skuIds != '') {
                grdPriceSetSKUs.set_callbackParameter(skuIds);
                grdPriceSetSKUs.callback();
            }
            else {
                alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, ThereIsNoSkuToAdd %>" />');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div style="width: 1160px; height: 588px;">
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False" ValidationGroup="ValGroup1" />
        <div>
            <ComponentArt:Menu ID="mnuPriceSetSKU" runat="server" SkinID="ContextMenu">
                <Items>
                    <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddASKU %>" Look-LeftIconUrl="cm-icon-add.png">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem LookId="BreakItem">
                    </ComponentArt:MenuItem>
                    <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, RemoveSKU %>" Look-LeftIconUrl="cm-icon-delete.png">
                    </ComponentArt:MenuItem>
                </Items>
                <ClientEvents>
                    <ItemSelect EventHandler="mnuPriceSetSKU_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
            <ComponentArt:Grid ID="grdPriceSetSKUs" SliderPopupClientTemplateId="grdPriceSetSKUsSliderTemplate"
                SkinID="Default" runat="server" Width="1160" RunningMode="Callback" PagerInfoClientTemplateId="grdPriceSetSKUsSliderPageInfoTemplate"
                AllowEditing="false" AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="cachedgrdPriceSetSKUsSliderTemplate"
                CallbackCachingEnabled="false" AutoCallBackOnUpdate="false" PageSize="10" AllowPaging="True"
                EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AllowMultipleSelect="false"
                ManualPaging="true" EditOnClickSelectedItem="false" OnBeforeCallback="grdPriceSetSKUs_OnBeforeCallback"
                LoadingPanelClientTemplateId="grdPriceSetSKUsLoadingPanelTemplate">
                <Levels>
                    <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                        DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                        AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                        ShowSelectorCells="false">
                        <Columns>
                            <ComponentArt:GridColumn Width="283" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                                AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="ProductNoHoverTemplate"
                                IsSearchable="True" DataField="ParentProductTitle" AllowSorting="False" />
                            <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, SKU %>" DataField="SKU"
                                FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="SKUHoverTemplate"
                                IsSearchable="True" AllowSorting="False" />
                            <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, MinQty %>"
                                DataField="MinQuantity" FixedWidth="true" AllowReordering="false" DataCellServerTemplateId="MinQtyServerTemplate"
                                AllowSorting="False" IsSearchable="False" />
                            <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, MaxQty %>"
                                DataField="MaxQuantity" FixedWidth="true" AllowReordering="false" DataCellServerTemplateId="MaxQtyServerTemplate"
                                AllowSorting="False" IsSearchable="False" />
                            <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                                DataField="ListPrice" FixedWidth="true" AllowReordering="false" DataCellClientTemplateId="ListPriceHoverTemplate"
                                IsSearchable="False" AllowSorting="False" Align="Right" FormatString="c" />
                            <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, PriceSetPrice %>"
                                DataField="Price" FixedWidth="true" AllowReordering="false" DataCellServerTemplateId="PriceServerTemplate"
                                AllowSorting="False" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                            <ComponentArt:GridColumn DataField="SKUId" Visible="false" IsSearchable="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientEvents>
                    <ContextMenu EventHandler="grdPriceSetSKUs_onContextMenu" />
                    <CallbackComplete EventHandler="grdPriceSetSkus_CallbackComplete" />
                    <PageIndexChange EventHandler="grdPriceSetSkus_onPageIndexChange" />
                </ClientEvents>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="MinQtyServerTemplate">
                        <Template>
                            <asp:TextBox runat="server" Text='<%# Container.DataItem["MinQuantity"] %>' ID="txtMinQuantity"
                                onchange="ContentChanged()" Width="70px" CssClass="textBoxes" MaxLength="14"></asp:TextBox>
                            <asp:CompareValidator CultureInvariantValues="true" ID="RvtxtMinQty" ControlToValidate="txtMinQuantity"
                                runat="server" ValidationGroup="ValGroup1" Operator="DataTypeCheck" Type="Double"
                                ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidMinimumQuantity %>" Text="*"></asp:CompareValidator>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfMinQty" runat="server"
                                ControlToValidate="txtMinQuantity" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAMinimumQuantity %>"
                                ValidationGroup="ValGroup1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfMinQtyNotZero" runat="server"
                                InitialValue="0" ControlToValidate="txtMinQuantity" Text="*" ErrorMessage="<%$ Resources:GUIStrings, MinimumQtyShouldNotBeZero %>"
                                ValidationGroup="ValGroup1"></asp:RequiredFieldValidator>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="MaxQtyServerTemplate">
                        <Template>
                            <asp:TextBox runat="server" MaxLength="14" Text='<%# Container.DataItem["MaxQuantity"] ==DBNull.Value ||(System.Double)Container.DataItem["MaxQuantity"] ==0.0  ? "" : Container.DataItem["MaxQuantity"] %>'
                                ID="txtMaxQuantity" Width="70px" CssClass="textBoxes" onchange="ContentChanged()"></asp:TextBox>
                            <asp:CompareValidator CultureInvariantValues="true" ID="CGtxtMaxQty" ControlToValidate="txtMaxQuantity"
                                runat="server" ValidationGroup="ValGroup1" Operator="DataTypeCheck" Type="Double"
                                ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidMaximumQuantity %>" Text="*"></asp:CompareValidator>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="PriceServerTemplate">
                        <Template>
                            <asp:TextBox runat="server" MaxLength="14" Text='<%# Container.DataItem["Price"] %>'
                                onchange="ContentChanged()" ID="txtPrice" Width="100px" CssClass="textBoxes"
                                Style="text-align: right"></asp:TextBox>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rvPrice" runat="server"
                                ControlToValidate="txtPrice" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAPrice %>"
                                ValidationGroup="ValGroup1"></asp:RequiredFieldValidator>
                            <asp:CompareValidator CultureInvariantValues="true" ID="cvPrice" ControlToValidate="txtPrice"
                                runat="server" ValidationGroup="ValGroup1" Operator="DataTypeCheck" Type="Double"
                                ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidPrice %>" Text="*"></asp:CompareValidator>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="ProductNoHoverTemplate">
                        <span title="## DataItem.GetMember('ParentProductTitle').get_text() ##">## DataItem.GetMember('ParentProductTitle').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ParentProductTitle').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="SKUHoverTemplate">
                        <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="MinQtyHoverTemplate">
                        <input type="text" value="## DataItem.GetMember('MinQuantity').get_text() ##" style="width: 80px"
                            class="textBoxes" />
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="MaxQtyHoverTemplate">
                        <input type="text" value="## DataItem.GetMember('MaxQuantity').get_text() ##" style="width: 80px"
                            class="textBoxes" />
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ListPriceHoverTemplate">
                        <span title="## DataItem.GetMember('ListPrice').get_text() ##">## DataItem.GetMember('ListPrice').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ListPrice').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="PBPHoverTemplate">
                        <input type="text" value="## DataItem.GetMember('Price').get_text() ##" style="width: 120px"
                            class="textBoxes" />
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdPriceSetSKUsSliderTemplate">
                        <div class="SliderPopup">
                            <p>
                                ##_datanotload##
                            </p>
                            <p class="paging">
                                <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdPriceSetSKUs.PageCount)##</span>
                                <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdPriceSetSKUs.RecordCount)##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="cachedgrdPriceSetSKUsSliderTemplate">
                        <div class="SliderPopup">
                            <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                            <p>
                                ## DataItem.GetMemberAt(1).Value ##
                            </p>
                            <p class="paging">
                                <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdPriceSetSKUs.PageCount)##</span>
                                <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdPriceSetSKUs.RecordCount)##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdPriceSetSKUsSliderPageInfoTemplate">
                        ##stringformat(Page0Of12items, currentPageIndex(grdPriceSetSKUs), pageCount(grdPriceSetSKUs),grdPriceSetSKUs.RecordCount)##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdPriceSetSKUsLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdPriceSetSKUs) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" runat="server" OnClientClick="return CloseThisPopup();" UseSubmitBehavior="false"
        CausesValidation="false" />
    <asp:Button ID="btnSaveClose" Text="<%$ Resources:GUIStrings, SaveClose %>" ToolTip="<%$ Resources:GUIStrings, SaveClose %>"
        CssClass="primarybutton" runat="server" UseSubmitBehavior="true" OnClick="btnSaveClose_Click" OnClientClick="return ValidateAndClosePopup();"
        ValidationGroup="ValGroup1" CausesValidation="true" />
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" runat="server" UseSubmitBehavior="true" OnClick="btnSave_Click" OnClientClick="return ValidateAndClosePopup();"
        ValidationGroup="ValGroup1" CausesValidation="true" />
</asp:Content>