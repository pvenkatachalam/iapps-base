﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddProductSkuPopup.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddProductSkuPopup" EnableEventValidation="false"
    StylesheetTheme="General" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="UC" TagName="SearchFilter" Src="~/UserControls/General/ProductSearchFilter.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">

    <script type="text/javascript">
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";

        var blContinue = true;
        var skuSelectMessage = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, PleaseSelectAtLeastOneSKU %>" />';
        function onCloseClick() {
            parent.window["SelectedProducts"] = '';
            parent.CloseProductPopup('');
            return false;
        }

        function ValidateAdd() {
            document.getElementById("<%= bntAdd.ClientID %>").disabled = false;
            //   document.getElementById("<%= btnAddMore.ClientID %>").disabled = false;
            return true;
        }

        var typewatch = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            }
        })();

        var doPostBack = "<%=!IsPostBack%>" == "True";

        $(document).ready(function () {

            $("#<%=searchFilter.SearchBoxClientId %>").keyup(function () {

                typewatch(function () {
                    if ($("#<%=searchFilter.SearchBoxClientId %>").val() != "") {

                        $("#<%=searchFilter.SearchButtonClientId %>").click();
                    }

                }, 350);

            });
            if (doPostBack) {
                $('#hfSelectedIds').val(parent.strSelectedProductIds);
                __doPostBack();
            }
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">

    <div class="clear-fix">
        <div class="left-column">
            <asp:HiddenField ID="hfSelectedIds" runat="server" ClientIDMode="Static" />
            <UC:SearchFilter ID="searchFilter" runat="server" />

            <asp:UpdatePanel runat="server" ID="panel">
                <ContentTemplate>
                    <div class="grid-scroll-section">

                        <asp:GridView runat="server" ID="gvProducts" EnableViewState="true" CssClass="plain-grid"
                            HeaderStyle-CssClass="heading-row" GridLines="Both" OnRowCommand="gvProducts_RowCommand"
                            OnRowDataBound="gv_DataBound" AutoGenerateColumns="False" PagerSettings-Visible="true"
                            PageSize="6" AllowPaging="true" AlternatingRowStyle-CssClass="alternate-row" PagerSettings-Position="Top"
                            Width="100%" ShowHeaderWhenEmpty="true" RowStyle-CssClass="row" PagerStyle-CssClass="grid-footer-row grid-footer">
                            <Columns>
                                <asp:TemplateField HeaderText="" Visible="true">
                                    <ItemStyle CssClass="data-cell" Width="10" />
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelectSku" runat="server" OnCheckedChanged="addProduct_Click" AutoPostBack="true" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, ProductName %>">
                                    <ItemStyle CssClass="data-cell" Width="330" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="btnSortName" runat="server" CommandName="Sort" CommandArgument="ProductName"
                                            Text="<%$ Resources:GUIStrings, ProductName %>" CssClass="asc" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="max-height: 72px; overflow: hidden;">
                                            <asp:HiddenField ID="hfSkuId" runat="server" />
                                            <asp:HiddenField ID="hfProductId" runat="server" />
                                            
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td valign="top" style="width: 80px; vertical-align: middle;">
                                                        <asp:LinkButton runat="server" OnClick="addProduct_Click" ID="imageAdd" Visible="false"> </asp:LinkButton>
                                                        <asp:Image ID="imgProductThumbnail" runat="server" CssClass="thumb" />
                                                    </td>
                                                    <td valign="top" style="vertical-align: middle;">
                                                        <asp:Literal ID="ltlName" runat="server" Visible="false" />
                                                        <asp:HyperLink ID="hlName" runat="server" Target="_blank"></asp:HyperLink>
                                                        <asp:LinkButton ID="lbName" runat="server" Visible="false" OnClick="addProduct_Click"></asp:LinkButton>
                                                        <%-- <asp:UpdatePanel runat="server">
                                                <ContentTemplate>--%>
                                                        <%--<asp:Button runat="server" ID="addProduct" OnClick="addProduct_Click" />--%>
                                                        <%--    </ContentTemplate>
                                            </asp:UpdatePanel>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, SKU %>">
                                    <ItemStyle CssClass="data-cell" Width="180" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="bntSortSku" runat="server" CommandName="Sort" CommandArgument="SKU"
                                            Text="<%$ Resources:GUIStrings, SKU %>" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div style="max-height: 72px; overflow: hidden;">
                                            <asp:Literal ID="ltlSku" runat="server" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Quantity %>">
                                    <ItemStyle CssClass="data-cell" Width="80" HorizontalAlign="Right" />
                                    <HeaderStyle HorizontalAlign="Right" CssClass="right-align" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="btnSortQuantity" runat="server" CommandName="Sort" CommandArgument="Quantity"
                                            Text="<%$ Resources:GUIStrings, Quantity %>" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlQuantity" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, AvailableToSell %>">
                                    <ItemStyle CssClass="data-cell" Width="140" HorizontalAlign="Right" />
                                    <HeaderStyle HorizontalAlign="Right" CssClass="right-align" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="btnSortAvailableToSell" runat="server" CommandName="Sort" CommandArgument="AvailableToSell"
                                            Text="<%$ Resources:GUIStrings, AvailableToSell %>" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlAvailableToSell" runat="server" />
                                        <asp:Image ID="imgAvailableToSell" runat="server" ImageUrl="../../App_Themes/General/images/icon-tooltip.gif"
                                            onmouseout="hideddrivetip();" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:GUIStrings, Status %>">
                                    <ItemStyle CssClass="data-cell" Width="70" />
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <asp:Literal ID="ltlStatus" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <div class="pager clear-fix">
                                    <div class="pager-buttons">
                                        <asp:ImageButton ID="btnPrev" runat="server" CommandName="Prev" ImageUrl="~/App_Themes/General/images/prev.gif" />
                                        <asp:ImageButton ID="btnNext" runat="server" CommandName="Next" ImageUrl="~/App_Themes/General/images/next.gif" />
                                    </div>
                                    <div class="pager-info">
                                        <%= GUIStrings.Page %>&nbsp;<asp:Literal ID="ltlPageIndex" Text='<%# gvProducts.PageIndex + 1 %>' runat="server" />&nbsp;of&nbsp;<asp:Literal ID="ltlPageTotal" runat="server" Text='<%# gvProducts.PageCount %>' />
                                    </div>
                                </div>
                                <table cellpadding="1" cellspacing="1" width="100%">
                                    <tr>
                                        <td align="left"></td>
                                        <td align="right" class="grid-footer-text"></td>
                                    </tr>
                                </table>
                            </PagerTemplate>
                            <EmptyDataTemplate>
                                <div class="grid-no-items">
                                    <asp:Localize ID="lcNoResults" runat="server" Text="<%$ Resources:GUIStrings, NoItemsFound %>" />
                                </div>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="right-column">
            <h2><%= GUIStrings.SelectedProducts %></h2>
            <div class="grid-scroll-section">
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Always">
                    <ContentTemplate>
                        <table width="100%" class="grid">
                            <asp:Repeater runat="server" ID="rptSelectedProducts" OnItemDataBound="rptSelectedProducts_ItemDataBound">
                                <ItemTemplate>
                                    <tr class="<%# Container.ItemIndex % 2 == 0 ? "odd-row" : "even-row" %>">
                                        <td>
                                            <asp:Literal ID="name" runat="server" />
                                            <asp:HiddenField runat="server" ID="hfProductId" />
                                            <asp:HiddenField runat="server" ID="hfSkuId" />
                                        </td>
                                        <td width="30">
                                            <asp:ImageButton runat="server" ID="btnRemove" ImageUrl="/iapps_images/cm-icon-delete.png" OnClick="btnRemove_Click" AlternateText="<%$ Resources:GUIStrings, Remove %>" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <div style="float: right;">
        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
            OnClientClick="return onCloseClick();" />
        <asp:Button ID="btnAddMore" runat="server" CssClass="primarybutton" CausesValidation="false" Visible="false"
            Text="<%$ Resources:GUIStrings, AddSearchForMore %>" OnClick="btnAddMore_Click" OnClientClick="return ValidateAdd();" />
        <asp:Button ID="bntAdd" runat="server" CssClass="primarybutton" CausesValidation="false"
            Text="<%$ Resources:GUIStrings, Add %>" OnClick="btnAdd_Click" OnClientClick="return ValidateAdd();" />
    </div>
</asp:Content>