<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ImportRestocking" StylesheetTheme="General" CodeBehind="ImportRestocking.aspx.cs"
    ClientIDMode="Static" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function closeIframe() {
            parent.importRestockingPopup.hide();

            return false;
        }
        function closeIframeAndRefresh() {
            parent.CloseImportRestockingPopup();
            parent.importRestockingPopup.hide();

            return false;
        }
        function setFileOnPath(objFile) {
            var txtVal = document.getElementById("hdnName").value;

            document.getElementById(txtVal).value = objFile.value;
        }
        function changeFileButton(objButtonCntrl) {
            var objButton = document.getElementById(objButtonCntrl)
            var buttonClass = objButton.className;
            var oldButtonClass = "";
            var newButtonClass = "";
            var buttonClassNum = "";
            if (buttonClass.indexOf("Hover") > -1) {
                buttonClassNum = buttonClass.substring(6, 7);
                newButtonClass = "button" + buttonClassNum;
                oldButtonClass = "button" + buttonClassNum + "Hover";
                objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
            }
            else {
                buttonClassNum = buttonClass.substring(6, 7);
                oldButtonClass = "button" + buttonClassNum;
                newButtonClass = "button" + buttonClassNum + "Hover";
                objButton.className = buttonClass.replace(oldButtonClass, newButtonClass);
            }
        }

        function checkFileExtension(elem, allowedExt, message) {
            var filePath = elem.value;
            if (filePath.indexOf('.') == -1)
                return false;
            var validExtensions = new Array();
            var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();

            if (ext == allowedExt)
                return true;
            alert(message);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <p style="padding-bottom: 10px;">
    <asp:HyperLink ID="hplDownloadTemplate" Target="_blank" runat="server" Text="<%$ Resources:GUIStrings, Restocking_DownloadTemplate %>" /></p>
    <asp:Label ID="Label1" runat="server" ForeColor="red" />
    <div class="fileUpload">
        <div class="realFileInputlabel">
            <asp:Label ID="ErrorLabel" runat="server" ForeColor="red" />
        </div>
        <div class="realFileInput">
            <asp:FileUpload ID="fileUpload" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" />
            <div class="fakeFile" style="display:none;">
                <asp:TextBox ID="fakeFileControlOnImage" runat="server" ReadOnly="true" CssClass="textBoxes floatLeft"
                    Width="140"></asp:TextBox>
                <asp:Button ID="btnBrowseOnImage" CssClass="button" Text="<%$ Resources:GUIStrings, Browse %>"
                    runat="server" />
            </div>
        </div>
    </div>
    <asp:PlaceHolder ID="phHiddenFields" runat="server"></asp:PlaceHolder>
    <asp:ValidationSummary ID="validationSummary" runat="server" ShowMessageBox="True"
        ShowSummary="False" />
    <asp:RequiredFieldValidator CultureInvariantValues="true" ID="RFVFileName" runat="server"
        Display="None" ControlToValidate="fileUpload" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAFile %>"></asp:RequiredFieldValidator>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" OnClientClick="return closeIframe();" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        UseSubmitBehavior="false" CssClass="button" />
    <asp:Button ID="btnUploadImage" runat="server" Text="<%$ Resources:GUIStrings, UploadFile %>"
        OnClick="uploadImage_Click" CssClass="primarybutton" />
    <asp:HiddenField ID="hdnMessage" runat="server" />
</asp:Content>
