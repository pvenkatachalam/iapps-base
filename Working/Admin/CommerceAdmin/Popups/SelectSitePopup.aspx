﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="SelectSitePopup.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.SelectSitePopup" StylesheetTheme="General" %>

<asp:Content ID="Head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function tvSiteHierarchy_onSelect(sender, eventArgs) {
            //$("#hdnSelectedId").val(tvSiteHierarchy.SelectedNode.Value);
        }

        function btnSelect_OnClick(sender, eventArgs) {
        	parent.selectedSiteId = tvSiteHierarchy.SelectedNode.Value;
        	parent.selectedSiteName = tvSiteHierarchy.SelectedNode.Text;
        	return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvSiteHierarchy"
        runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
        DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
        <CustomAttributeMappings>
            <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
            <ComponentArt:CustomAttributeMapping From="Id" To="Value" />
        </CustomAttributeMappings>
        <ClientEvents>
            <NodeSelect EventHandler="tvSiteHierarchy_onSelect" />
        </ClientEvents>
    </ComponentArt:TreeView>
    <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnSiteId" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnParentSiteId" runat="server" ClientIDMode="Static" Value="" />
    <asp:XmlDataSource ID="xmlDataSource" runat="server" />
</asp:Content>
<asp:Content ID="Footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button runat="server" ID="btnClose" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSelect" CssClass="primarybutton" runat="server" OnClientClick="btnSelect_OnClick()" OnClick="btnSelect_Click" Text="<%$ Resources:GUIStrings, Select %>" />
</asp:Content>
