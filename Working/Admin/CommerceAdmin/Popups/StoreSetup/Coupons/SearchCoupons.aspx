﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceFindaCoupon %>" Language="C#"
    MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="SearchCoupons.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SearchCoupons" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdCoupons_onContextMenu(sender, eventArgs) {
            var gridItem = eventArgs.get_item();
            grdCoupons.select(gridItem);
            var evt = eventArgs.get_event();
            mnuCoupons.showContextMenuAtEvent(evt);
        }

      

        function ValidateSearch() {
            var srchText = document.getElementById('txtSearch').value;

            if (srchText == "" || srchText == "<%= GUIStrings.TypeHereToFilterResults %>")
            {
                alert("<%= JSMessages.PleaseEnterValueToSearch %>");
                return false;
            }

            return true;
        }

        function SetGeneratedStatus(chkStatusObject) {
            window["GeneratedCoupons"] = chkStatusObject;
            ShowStatusWise();
        }

        function ShowStatusWise() {
            var actionColumnValue = '';
            var chkGeneratedStatusObject = window["GeneratedCoupons"];

            if (chkGeneratedStatusObject != null && chkGeneratedStatusObject.checked) {
                actionColumnValue = actionColumnValue + ';ShowGenerated';
            }
            else {
                actionColumnValue = actionColumnValue + ';ShowAuto';
            }

            LoadCouponGrid(actionColumnValue);
        }

        function LoadCouponGrid(actionColumnValue) {
            grdCoupons.set_callbackParameter(actionColumnValue);
            grdCoupons.callback();
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:Menu ID="mnuCoupons" runat="server" SkinID="ContextMenu" AutoPostBackOnSelect="true"
        OnItemSelected="mnuCoupons_OnItemSelected">
        <Items>
            <ComponentArt:MenuItem ID="cmSelect" Text="<%$ Resources:GUIStrings, SelectCoupon %>" Look-LeftIconUrl="cm-icon-add.png" Look-LeftIconWidth="17" Look-LeftIconHeight="14">
            </ComponentArt:MenuItem>
        </Items>
    </ComponentArt:Menu>
    <div class="grid-utility">
     <div class="columns">
         <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
         <input type="button" class="small-button" value="<%= GUIStrings.Filter %>"
                onclick="return onClickSearchonGrid('txtSearch', grdCoupons);" />
    </div>
      <div class="columns-right">
            <asp:CheckBox ID="chkShowGeneratedCoupons" runat="server" Text="<%$ Resources:GUIStrings, ShowGeneratedCoupons %>"
                onclick="SetGeneratedStatus(this);" />
        </div>
        <div class="clear-fix">
        </div>
        </div>
    <ComponentArt:Grid ID="grdCoupons" SkinID="Default" runat="server" Width="670" RunningMode="Callback"
        PagerInfoClientTemplateId="CouponsPaginationTemplate" AllowEditing="true" AutoCallBackOnInsert="false"
        SliderPopupCachedClientTemplateId="CouponsSliderTemplateCached" SliderPopupClientTemplateId="CouponsSliderTemplate"
        AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True"
        CallbackCachingEnabled="true" AllowMultipleSelect="false" EditOnClickSelectedItem="false"
        OnBeforeCallback="grdCoupons_OnBeforeCallback" LoadingPanelClientTemplateId="CouponsLoadingPanelTemplate">
        <ClientEvents>
            <ContextMenu EventHandler="grdCoupons_onContextMenu" />
          
        </ClientEvents>
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, CouponName %>" DataField="Title" Align="left"
                        DataCellClientTemplateId="NameHoverTemplate" Width="200" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, CouponCode %>" DataField="Code" Align="left"
                        DataCellClientTemplateId="CodeHoverTemplate" Width="140" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                        HeadingText="<%$ Resources:GUIStrings, Description %>" DataField="Description" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="DescriptionHoverTemplate" Width="170" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="false"
                        HeadingText="<%$ Resources:GUIStrings, NumUsed %>" DataField="Used" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="UsedHoverTemplate" Width="80" />
                    <ComponentArt:GridColumn AllowReordering="false"  FixedWidth="true" IsSearchable="false"
                        HeadingText="<%$ Resources:GUIStrings, Type %>" DataField="CouponType" Align="left"
                        AllowEditing="false" DataCellClientTemplateId="TypeHoverTemplate" Width="180" />
                    <ComponentArt:GridColumn DataField="ID" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <div title="## DataItem.GetMember('Title').get_text() ##">
                    ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CodeHoverTemplate">
                <div title="## DataItem.GetMember('Code').get_text() ##">
                    ## DataItem.GetMember('Code').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Code').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DescriptionHoverTemplate">
                <div title="## DataItem.GetMember('Description').get_text() ##">
                    ## DataItem.GetMember('Description').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Description').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="UsedHoverTemplate">
                <div title="## DataItem.GetMember('Used').get_text() ##">
                    ## DataItem.GetMember('Used').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Used').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <div title="## DataItem.GetMember('CouponType').get_text() ##">
                    ## DataItem.GetMember('CouponType').get_text() == "" ? "&nbsp;" : DataItem.GetMember('CouponType').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CouponsSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdCoupons.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdCoupons.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CouponsSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdCoupons.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdCoupons.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CouponsPaginationTemplate">
                ##stringformat(Page0Of12items, currentPageIndex(grdCoupons), pageCount(grdCoupons),
                grdCoupons.RecordCount)##
            </ComponentArt:ClientTemplate>
             <ComponentArt:ClientTemplate ID="CouponsLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdCoupons) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
        class="button" onclick="parent.couponSearchPopup.hide();" />
</asp:Content>
