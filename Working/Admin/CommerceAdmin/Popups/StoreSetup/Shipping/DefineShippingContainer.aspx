﻿<%@ Page Language="C#" MasterPageFile="~/Popups/PopupMaster.Master" AutoEventWireup="true" CodeBehind="DefineShippingContainer.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.DefineShippingContainer" StylesheetTheme="General" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" media="all" href="../../../css/Popup.css" />
    <script type="text/javascript">
        function CloseShippingContainerPopup() {
            parent.defineShippingContainerPopup.hide();
        }
        var lengthTextLower = '<%= GUIStrings.SCLength %>'.toLowerCase();
        var widthTextLower = '<%= GUIStrings.SCWidth %>'.toLowerCase();
        var heightTextLower = '<%= GUIStrings.SCHeight %>'.toLowerCase();
        function RemoveDefaultText(strOption, objTextbox) {
            var textboxValue = objTextbox.value.toLowerCase();
            switch (strOption) {
                case '<%= GUIStrings.SCLength.ToLower() %>':
                    if (textboxValue == lengthTextLower) {
                        objTextbox.value = '';
                    }
                    break;
                case '<%= GUIStrings.SCWidth.ToLower() %>':
                    if (textboxValue == widthTextLower) {
                        objTextbox.value = '';
                    }
                    break;
                case '<%= GUIStrings.SCHeight.ToLower() %>':
                    if (textboxValue == heightTextLower) {
                        objTextbox.value = '';
                    }
                    break;
            }
        }
        function SetDefaultText(strOption, objTextbox) {
            var textboxValue = objTextbox.value.toLowerCase();
            switch (strOption) {
                case '<%= GUIStrings.SCLength.ToLower() %>':
                    if (textboxValue == '') {
                        objTextbox.value = '<%= GUIStrings.SCLength %>';
                    }
                    break;
                case '<%= GUIStrings.SCWidth.ToLower() %>':
                    if (textboxValue == '') {
                        objTextbox.value = '<%= GUIStrings.SCWidth %>';
                    }
                    break;
                case '<%= GUIStrings.SCHeight.ToLower() %>':
                    if (textboxValue == '') {
                        objTextbox.value = '<%= GUIStrings.SCWidth %>';
                    }
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="popupDarkBoxShadow DefineShippingContainer">
        <div class="popupDarkContainer">
            <div class="boxHeader">
                <h3><asp:Literal ID="ltHeading" runat="server" Text="<%$ Resources:GUIStrings, ShippingContainer %>"></asp:Literal></h3>
            </div>
            <div class="popupBoxContent">
                <div class="popupGrid" style="padding: 0;">
                    <div class="mainContent">
                        <div class="gridContainer">
                            <div class="formRow">
                                <label class="formLabel"><sup>*</sup><%= GUIStrings.SCName %></label>
                                <asp:TextBox ID="txtName" runat="server" Width="200" CssClass="textBoxes"></asp:TextBox>
                            </div>
                            <div class="formRow">
                                <label class="formLabel"><%= GUIStrings.SCDescription %></label>
                                <asp:TextBox ID="txtDescription" runat="server" Width="200" CssClass="textBoxes" TextMode="MultiLine" Rows="3"></asp:TextBox>
                            </div>
                            <div class="formRow">
                                <label class="formLabel"><sup>*</sup><%= GUIStrings.SCContainerSize %></label>
                                <asp:TextBox ID="txtLength" runat="server" Width="50" CssClass="textBoxes" Text="Length" onfocus="RemoveDefaultText('length', this);" onblur="SetDefaultText('length', this);"></asp:TextBox>
                                <asp:TextBox ID="txtWidth" runat="server" Width="50" CssClass="textBoxes" Text="Width" onfocus="RemoveDefaultText('width', this);" onblur="SetDefaultText('width', this);"></asp:TextBox>
                                <asp:TextBox ID="txtHeight" runat="server" Width="50" CssClass="textBoxes" Text="Height" onfocus="RemoveDefaultText('height', this);" onblur="SetDefaultText('height', this);"></asp:TextBox>
                                <label><asp:Literal ID="ltMeasurement" runat="server" Text="cm"></asp:Literal></label>
                            </div>
                            <div class="formRow">
                                <label class="formLabel"><sup>*</sup><%= GUIStrings.SCMaxWeight %></label>
                                <asp:TextBox ID="txtMaxWeight" runat="server" Width="50" CssClass="textBoxes"></asp:TextBox>
                                <label><asp:Literal ID="ltWeightUnit" runat="server" Text="Kg"></asp:Literal></label>
                            </div>
                            <div class="formRow">
                                <label class="formLabel"><%= GUIStrings.SCMaxValue %></label>
                                <asp:TextBox ID="txtMaxValue" runat="server" Width="50" CssClass="textBoxes"></asp:TextBox>
                                <label><asp:Literal ID="ltCurrency" runat="server" Text="SFr."></asp:Literal></label>
                            </div>
                            <div class="formRow">
                                <label class="formLabel"><%= GUIStrings.SCBoxCode %></label>
                                <asp:TextBox ID="txtBoxCode" runat="server" Width="50" CssClass="textBoxes"></asp:TextBox>
                            </div>
                        </div>
                        <div class="footerContent">
                            <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
                            <input type="button" value="Cancel" class="button" onclick="CloseShippingContainerPopup();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
