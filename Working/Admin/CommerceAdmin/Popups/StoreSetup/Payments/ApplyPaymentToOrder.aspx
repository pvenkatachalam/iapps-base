﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="ApplyPaymentToOrder.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ApplyPaymentToOrder"
    StylesheetTheme="General" %>

<%@ Register Src="~/UserControls/General/InternationalAddress.ascx" TagName="InternationalAddress"
    TagPrefix="uc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function() {
            setTimeout(function() {
                $("#<%= txtCardNumber.ClientID %>").focus();
            }, 200);
        });

        function OnClickValidate() {
            var valResult = CheckPaymentType();
            //            if (valResult == true) {
            if (typeof Page_ClientValidate == 'function') {
                Page_ClientValidate();
            }
            //            }
            return valResult;
        }
        //Credit card validation
        function checkCreditCard(cardnumber, cardname) {
            var initialCardNumber = document.getElementById("<%=hfCardNumber.ClientID%>").value;
            if (cardnumber != initialCardNumber) {
                // Array to hold the permitted card characteristics
                var cards = new Array();
                // Define the cards we support. You may add addtional card types.

                //  Name:      As in the selection box of the form - must be same as user's
                //  Length:    List of possible valid lengths of the card number for the card
                //  prefixes:  List of possible prefixes for the card
                //  checkdigit Boolean to say whether there is a check digit

                cards[0] = { name: "Visa",
                    length: "13,16",
                    prefixes: "4",
                    checkdigit: true
                };
                cards[1] = { name: "MasterCard",
                    length: "16",
                    prefixes: "51,52,53,54,55",
                    checkdigit: true
                };
                cards[2] = { name: "DinersClub",
                    length: "14,16",
                    prefixes: "36,54,55",
                    checkdigit: true
                };
                cards[3] = { name: "CarteBlanche",
                    length: "14",
                    prefixes: "300,301,302,303,304,305",
                    checkdigit: true
                };
                cards[4] = { name: "American Express",
                    length: "15",
                    prefixes: "34,37",
                    checkdigit: true
                };
                cards[5] = { name: "Discover",
                    length: "16",
                    prefixes: "6",
                    checkdigit: true
                };
                cards[6] = { name: "JCB",
                    length: "16",
                    prefixes: "35",
                    checkdigit: true
                };
                cards[7] = { name: "enRoute",
                    length: "15",
                    prefixes: "2014,2149",
                    checkdigit: true
                };
                cards[8] = { name: "Solo",
                    length: "16,18,19",
                    prefixes: "6334, 6767",
                    checkdigit: true
                };
                cards[9] = { name: "Switch",
                    length: "16,18,19",
                    prefixes: "4903,4905,4911,4936,564182,633110,6333,6759",
                    checkdigit: true
                };
                cards[10] = { name: "Maestro",
                    length: "12,13,14,15,16,18,19",
                    prefixes: "5018,5020,5038,6304,6759,6761",
                    checkdigit: true
                };
                cards[11] = { name: "VisaElectron",
                    length: "16",
                    prefixes: "417500,4917,4913,4508,4844",
                    checkdigit: true
                };

                // Establish card type
                var cardType = -1;
                for (var i = 0; i < cards.length; i++) {

                    // See if it is this card (ignoring the case of the string)
                    if (cardname.toLowerCase() == cards[i].name.toLowerCase()) {
                        cardType = i;
                        break;
                    }
                }

                // If card type not found, report an error
                if (cardType == -1) {
                    return "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, UnknownCardType %>' />";

                }

                // Ensure that the user has provided a credit card number
                if (cardnumber.length == 0) {
                    return "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NoCardNumberProvided %>' />";
                }

                // Now remove any spaces from the credit card number
                cardnumber = cardnumber.replace(/\s/g, "");

                // Check that the number is numeric
                var cardNo = cardnumber
                var cardexp = /^[0-9]{13,19}$/;
                if (!cardexp.exec(cardNo)) {
                    return "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CreditCardNumberIsInInvalidFormat %>' />";
                }

                // Now check the modulus 10 check digit - if required
                if (cards[cardType].checkdigit) {
                    var checksum = 0;                                  // running checksum total
                    var mychar = "";                                   // next char to process
                    var j = 1;                                         // takes value of 1 or 2

                    // Process each digit one by one starting at the right
                    var calc;
                    for (i = cardNo.length - 1; i >= 0; i--) {

                        // Extract the next digit and multiply by 1 or 2 on alternative digits.
                        calc = Number(cardNo.charAt(i)) * j;

                        // If the result is in two digits add 1 to the checksum total
                        if (calc > 9) {
                            checksum = checksum + 1;
                            calc = calc - 10;
                        }

                        // Add the units element to the checksum total
                        checksum = checksum + calc;

                        // Switch the value of j
                        if (j == 1) { j = 2 } else { j = 1 };
                    }

                    // All done - if checksum is divisible by 10, it is a valid modulus 10.
                    // If not, report an error.
                    if (checksum % 10 != 0) {
                        return "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CreditCardNumberIsInvalid %>' />";
                    }
                }

                // The following are the card-specific checks we undertake.
                var LengthValid = false;
                var PrefixValid = false;
                var undefined;

                // We use these for holding the valid lengths and prefixes of a card type
                var prefix = new Array();
                var lengths = new Array();

                // Load an array with the valid prefixes for this card
                prefix = cards[cardType].prefixes.split(",");

                // Now see if any of them match what we have in the card number
                for (i = 0; i < prefix.length; i++) {
                    var exp = new RegExp("^" + prefix[i]);
                    if (exp.test(cardNo)) PrefixValid = true;
                }

                // If it isn't a valid prefix there's no point at looking at the length
                if (!PrefixValid) {
                    return "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CreditCardNumberIsInvalid %>' />";
                }

                // See if the length is valid for this card
                lengths = cards[cardType].length.split(",");
                for (j = 0; j < lengths.length; j++) {
                    if (cardNo.length == lengths[j]) LengthValid = true;
                }

                // See if all is OK by seeing if the length was valid. We only check the 
                // length if all else was hunky dory.
                if (!LengthValid) {
                    return "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CreditCardNumberHasAnInappropriateNumberOfDigits %>' />";
                };
            }
            // The credit card is in the required format.
            return "true";
        }

        function CheckPaymentType() {
            var errorMsg ='';
            if (Page_IsValid)
            {
                var cardTypeCtrl = document.getElementById('<%=ddlCardType.ClientID %>');
                var cardType = cardTypeCtrl.options[cardTypeCtrl.selectedIndex].text;
                    
                var cardNumber = document.getElementById('<%=txtCardNumber.ClientID %>').value;
                
                var cardValMsg = checkCreditCard(cardNumber, cardType);
                if (cardValMsg != "true") {
                    //alert(cardValMsg);
                    errorMsg += cardValMsg+"\n";
                    
                }
                

                //              Should not validated address on payment  
                //                if(isCityValid != 'True')
                //                {
                //                    errorMsg += "Please enter a valid city or zipcode. \n";
                //                }
                
                if( errorMsg =='')
                {
                    return true;    
                }
                else
                {
                    alert(errorMsg);
                    return false;
                }
                
            }
            else
            {
                return false;
            }
        }
        function CheckExpiryDate(source, arguments) {
            var data = arguments.Value.split('');
            arguments.IsValid = false; //set IsValid property to false

            var ddlYear = document.getElementById('<%=ddlYear.ClientID %>');
            var ddlMonth = document.getElementById('<%=ddlMonth.ClientID %>');
            var currentYear = <%=DateTime.Now.Year.ToString() %>;
            var currentMonth = <%=DateTime.Now.Month.ToString() %> ;         
         
            if(parseInt(ddlMonth.value) >= parseInt(currentMonth) && parseInt(ddlYear.value) >= parseInt(currentYear) || parseInt(ddlYear.value) > parseInt(currentYear)  )
                arguments.IsValid = true;
            
        }
        
        var onAccountPaymentTypeId =2;// TODO: render ID of the OnAccount paymentType from Server side
        var onCODPaymentTypeId=4;
        function ddlPaymentMethod_onchange()
        {

            var divToHide = document.getElementById("<%=phCreditCardPayments.ClientID %>");
            var ddlPM = document.getElementById("<%=ddlPaymentMethod.ClientID %>");
            var btnOnAccount = document.getElementById("<%=btnSave.ClientID %>");
            
            var btnCC= document.getElementById("<%=btnApply.ClientID %>");
            divToHide.style.display="block";
            btnOnAccount.setAttribute("style","display:none");
            btnCC.setAttribute("style","display:inline");
                  
            var selVal = ddlPM.options[ddlPM.selectedIndex].value;
            if(selVal==onAccountPaymentTypeId ||selVal==onCODPaymentTypeId)   
            {
                divToHide.style.display="none";
                btnOnAccount.setAttribute("style","display:inline");
                btnCC.setAttribute("style","display:none");
            }
        }
        function PageLevelNextAction() {
            $('#<%=btnApply.ClientID%>').click()
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:UpdatePanel runat="server" ID="upPanel">
        <ContentTemplate>
            <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
                EnableClientScript="true" />
            <asp:HiddenField ID="hfCardNumber" runat="server" />
            <table class="plain-grid" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr class="heading-row">
                        <th>
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, OrderNum %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, OrderDate %>" />
                        </th>
                        <th class="right-align">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, GrandTotal %>" />
                        </th>
                        <th class="right-align">
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, CurrentBalance %>" />
                        </th>
                        <th class="right-align">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, PaymentAmount %>" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="row">
                        <td class="data-cell">
                            <asp:Label ID="lblOrderNo" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="data-cell">
                            <asp:Label ID="lblOrderDate" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="data-cell right-align">
                            <asp:Label ID="lblGrandTotal" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="data-cell right-align">
                            <asp:Label ID="lblCurrentBalance" runat="server" Text=""></asp:Label>
                        </td>
                        <td class="data-cell right-align">&nbsp;
                    <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="rfvtxtPaymentAmt"
                        Display="None" ControlToValidate="txtPaymentAmt" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterThePaymentAmount %>"></asp:RequiredFieldValidator>
                            <asp:CompareValidator CultureInvariantValues="true" runat="server" ID="rgtxtPaymentAmt"
                                Display="None" ControlToValidate="txtPaymentAmt" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheValidPaymentAmount %>"
                                Type="Currency" Operator="DataTypeCheck"></asp:CompareValidator>
                            <asp:CompareValidator CultureInvariantValues="true" runat="server" ID="cvtxtpaymentAmt"
                                Display="None" ControlToValidate="txtPaymentAmt" Text="*" ErrorMessage="<%$ Resources:GUIStrings, AmountShouldBeLessThanOrEqualToCurrentBalance %>"
                                Type="Currency" Operator="LessThanEqual" ValueToCompare="0"></asp:CompareValidator>
                            <asp:TextBox ID="txtPaymentAmt" runat="server" CssClass="textBoxes" Width="75"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="payment-form">
                <div class="form-row">
                    <label class="form-label" for="<%=ddlPaymentMethod.ClientID%>">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, PaymentMethod %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlPaymentMethod" runat="server" AutoPostBack="true" Width="270"
                            OnSelectedIndexChanged="ddlPaymentMethod_SelectedIndexChanged" />
                    </div>
                </div>
                <asp:PlaceHolder ID="phCreditCardPayments" runat="server">
                    <div class="columns">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, SavedCards %>" /></label>
                            <div class="form-value">
                                <asp:DropDownList AutoPostBack="true" CausesValidation="false" ID="ddlSaveCards"
                                    runat="server" Width="270" OnSelectedIndexChanged="ddlSaveCards_SelectedIndexChanged" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, CardType %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlCardType" runat="server" Width="270" />
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlCardType" runat="server"
                                    ControlToValidate="ddlCardType" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectACardType %>"
                                    InitialValue="00000000-0000-0000-0000-000000000000"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, CardNumber %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textBoxes" MaxLength="16"
                                    Width="260"></asp:TextBox>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtCardNumber" ControlToValidate="txtCardNumber"
                                    runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheCreditCardNumber %>"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, NameOnCard %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtNameOnCard" runat="server" CssClass="textBoxes" Width="260"></asp:TextBox>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtNameOnCard" ControlToValidate="txtNameOnCard"
                                    runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheNameOnCard %>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="revtxtNameOnCard" ControlToValidate="txtNameOnCard"
                                    ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInNameOnCard %>"
                                    Text="*"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, ExpirationDate %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlMonth" runat="server">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Month %>" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, January %>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, February %>" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, March %>" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, April %>" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, May %>" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, June %>" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, July %>" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, August %>" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, September %>" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, October %>" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, November %>" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, December %>" Value="12"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlExpiryMonth"
                                    runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectTheExpirationMonth %>"
                                    ControlToValidate="ddlMonth" InitialValue="0"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddlYear" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlExpiryYear" runat="server"
                                    Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectTheExpirationYear %>"
                                    ControlToValidate="ddlYear" InitialValue="0"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvExpirationYear" runat="server" ClientValidationFunction="CheckExpiryDate"
                                    ErrorMessage="<%$ Resources:GUIStrings, CardIsExpired %>" Text="*" ControlToValidate="ddlYear"
                                    OnServerValidate="cvYear_Validate" />
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phCVV" runat="server" Visible="false">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, CVV %>" /><span
                                        class="req">*</span></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtCvv" runat="server" CssClass="textBoxes" Width="50" MaxLength="4"></asp:TextBox>
                                    <asp:CompareValidator CultureInvariantValues="true" ID="cvtxtCvv" runat="server"
                                        ControlToValidate="txtCvv" Operator="DataTypeCheck" Type="Integer" Text="*" ErrorMessage="<%$ Resources:GUIStrings, CVVNumberShouldBeBetween100And9999 %>"></asp:CompareValidator>
                                    <asp:RegularExpressionValidator ID="rvtxtCvv" runat="server" ControlToValidate="txtCvv"
                                        ValidationExpression="[0-9]{1,4}" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidCVVNumber %>">
                                    </asp:RegularExpressionValidator>
                                    <asp:RequiredFieldValidator CultureInvariantValues="true" ID="RangeValidator1" runat="server"
                                        ControlToValidate="txtCvv" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterACVVNumber %>" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="phApprovalCode" runat="server" Visible="false">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, ApprovalCode %>" /></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtApprovalCode" runat="server" CssClass="textBoxes" Width="260"></asp:TextBox>
                                    <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ControlToValidate="txtApprovalCode"
                                        ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInDescription %>"
                                        Text="*"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtDescription" runat="server" Width="260" CssClass="textBoxes"></asp:TextBox>
                                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="txtDescription"
                                    ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInDescription %>"
                                    Text="*"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phUpdatePaymentInformation" runat="server" Visible="true">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Literal ID="ltlSaveLabel" runat="server" Text="<%$ Resources:GUIStrings, SavePaymentInformation %>" /></label>
                                <div class="form-value">
                                    <asp:CheckBox ID="cbUpdatePaymentInfo" title="<%$ Resources:GUIStrings, SavesOrUpdatesPaymentInformationInTheCustomersProfile %>"
                                        runat="server" Checked="true" />
                                    <%--Hiding nick name text box for now will need to adjust ui, code behind exists for this control so just need to decide how to hide/show it.--%>
                                    <asp:TextBox ID="cardNickName" runat="server" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="columns">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, SavedAddresses %>" /></label>
                            <div class="form-value">
                                <asp:DropDownList AutoPostBack="true" CausesValidation="false" ID="ddlSameAsShipping"
                                    runat="server" Width="270" OnSelectedIndexChanged="ddlSameAsShipping_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <uc1:InternationalAddress ID="cntrlAddress" runat="server" IsFirstNameVisible="false"
                            IsLastNameVisible="false" />
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phCODPayment" runat="server">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <asp:Repeater runat="server" ID="rptCODPayments">
                            <HeaderTemplate>
                                <tr>
                                    <th>
                                        <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, Shipping %>" />
                                    </th>
                                    <th align="right">
                                        <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, Payment %>" />
                                    </th>
                                    <th align="right">
                                        <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, CODFee %>" />
                                    </th>
                                    <th align="right">
                                        <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings, TotalPayment %>" />
                                    </th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="55%" style="padding-left: 5px;">
                                        <asp:Label ID="lblCODShipping" runat="server"></asp:Label>
                                    </td>
                                    <td width="15%" align="right">
                                        <asp:Label ID="lblPayment" runat="server"></asp:Label>
                                    </td>
                                    <td width="15%" align="right">
                                        <asp:Label ID="lblCODCharge" runat="server"></asp:Label>
                                    </td>
                                    <td width="15%" align="right" style="padding-right: 5px;">
                                        <asp:Label ID="lblTotalCODPayment" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnAddressId" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phGiftCardPayment" runat="server">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:GUIStrings, GiftCardNumber %>" /></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtGiftCardNumber" runat="server" CssClass="textBoxes" Width="260"></asp:TextBox>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="RequiredFieldValidator2"
                                ControlToValidate="txtGiftCardNumber" runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheGiftCardNumber %>"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div class="clear-fix">
                </div>
                <asp:PlaceHolder runat="server" ID="phNotice">
                    <p class="notice">
                        <em>NOTICE: The address validation functionality will validate P.O. Box addresses; however,
                UPS does not provide delivery to P.O. boxes. The customer must make every effort
                to obtain a street address. Attempts by the customer to ship to a P.O. Box via UPS
                may result in additional charges.</em>
                    </p>
                </asp:PlaceHolder>
            </div>
        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>

<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClientClick="parent.applyPaymentPopup.hide();return false;" />

            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
                CssClass="primarybutton" CausesValidation="false" />

            <asp:Button ID="btnApply" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
                CssClass="primarybutton" CausesValidation="false" OnClientClick="return OnClickValidate();"
                OnClick="btnApply_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
