﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" 
    CodeBehind="HTMLEditor.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.HTMLEditor" StylesheetTheme="General" %>
<%@ Register Src="../UserControls/General/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="uc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
<link href="../css/Popup.css" rel="Stylesheet" media="all" type="text/css" />
<script type="text/javascript">
    var htmlText;

    function PasteContentToEditor(oureditor) {
        if (EditorToUse.toLowerCase() == 'ckeditor')
            oureditor.setData(htmlText);
        else 
            oureditor.set_html(htmlText);
    }

    function SaveAndClose(hiddenId, editorId) {
        var htmlvalue = '';
        var content='';
        if (EditorToUse.toLowerCase() == 'ckeditor')
            content = oureditor.getData();
        else
            content = oureditor.get_html(true);
        var htmlvalue = content;
        parent.SetTextValue(hiddenId, htmlvalue);
        ClosePopup();
        return false;
    }
    function ClosePopup(hiddenId) {
        parent.HideEditorPopup();
    }
    function LoadHiddenText(hiddenId, editorId) {
        var inputHtml = parent.document.getElementById(hiddenId).value;
        htmlText = ConvertToHtml(inputHtml);
        while (inputHtml != htmlText) {
            inputHtml = htmlText;
            htmlText = ConvertToHtml(inputHtml);
        }
        htmlText = inputHtml;
     
    }
</script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <uc1:HtmlEditor ID="HtmlEditor1" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" runat="server" CssClass="primarybutton" />
</asp:Content>
