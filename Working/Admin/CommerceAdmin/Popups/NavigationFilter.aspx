﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NavigationFilter.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Popups.NavigationFilter" StylesheetTheme="General" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NavigationFilter %>" />
    </title>
</head>
<body class="popupBody">
    <script>
        function addRow() {
            cbAddRow.Callback();
        }

        function cbAddRow_OnCallbackComplete(obj, e) {

        }

    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_initializeRequest(initializeRequest);
        prm.add_endRequest(endRequest);

        function initializeRequest(sender, args) {
            var i;
            var rowCount = document.getElementById("<%=hfRowCount.ClientID %>").value;
        }

        function endRequest(sender, args) {

            var rowCount = document.getElementById("<%=hfRowCount.ClientID %>").value;
            var updatedData = document.getElementById("hfUpdatedData").value;
            var rowsDataArr = updatedData.split("$");

            for (i = 6; i <= rowCount; i++) {

                var updatedDataArr = rowsDataArr[i - 6].split(",");

                if (updatedDataArr[0] != '')
                    document.getElementById("ddlField" + i).selectedIndex = updatedDataArr[0];
                if (updatedDataArr[1] != '') {
                    //document.getElementById("ddlOperator" + i).selectedIndex =0;
                    var ddlOpr = document.getElementById("ddlOperator" + i);
                    //var str = ;
                    var ccd = $find("cddOperator" + i);
                    if (ccd) ccd._onParentChange(null, false);

                    ccd._selectedValue = updatedDataArr[1];




                    //document.getElementById("ddlOperator" + i).selectedIndex = updatedDataArr[1];
                }
                if (updatedDataArr[2] != '')
                    document.getElementById("txtValue" + i).value = updatedDataArr[2];
            }

            rowCount = eval(rowCount) + 1;
            document.getElementById("<%=hfRowCount.ClientID %>").value = rowCount;

            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, UpdationCompleted %>' />");
        }
            
            

    </script>
    <div class="popupBoxShadow">
        <div class="popupboxContainer">
            <div class="dialogBox NavigationFilter">
                <h3>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NavigationFilter %>" /></h3>
                <div class="middleContent">
                    <div class="formArea">
                        <div class="formRow">
                            <div class="firstField">
                                &nbsp;</div>
                            <div class="secondField">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Field %>" /></div>
                            <div class="thirdField">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Operator %>" /></div>
                            <div class="fourthField">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Value %>" /></div>
                        </div>
                        <div class="clearFix">
                            &nbsp;</div>
                        <div class="formRow">
                            <div class="firstField">
                                1.</div>
                            <div class="secondField">
                                <asp:DropDownList ID="ddlField1" runat="server" Width="200">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField1" runat="server"
                                ControlToValidate="ddlField1" Display="None"></asp:RequiredFieldValidator>
                            <div class="thirdField">
                                <asp:DropDownList ID="ddlOperator1" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator1" runat="server"
                                ControlToValidate="ddlOperator1"></asp:RequiredFieldValidator>
                            <div class="fourthField">
                                <asp:TextBox ID="txtValue1" runat="server" CssClass="textBoxes" Width="150"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue1" runat="server"
                                ControlToValidate="txtValue1"></asp:RequiredFieldValidator>
                            <div class="fifthField">
                                <asp:DropDownList ID="ddlCatalog1" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearFix">
                            &nbsp;</div>
                        <div class="formRow">
                            <div class="firstField">
                                2.</div>
                            <div class="secondField">
                                <asp:DropDownList ID="ddlField2" runat="server" Width="200">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField2" runat="server"
                                ControlToValidate="ddlField2"></asp:RequiredFieldValidator>
                            <div class="thirdField">
                                <asp:DropDownList ID="ddlOperator2" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator2" runat="server"
                                ControlToValidate="ddlOperator2"></asp:RequiredFieldValidator>
                            <div class="fourthField">
                                <asp:TextBox ID="txtValue2" runat="server" CssClass="textBoxes" Width="150"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue2" runat="server"
                                ControlToValidate="txtValue2"></asp:RequiredFieldValidator>
                            <div class="fifthField">
                                <asp:DropDownList ID="ddlCatalog2" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearFix">
                            &nbsp;</div>
                        <div class="formRow">
                            <div class="firstField">
                                3.</div>
                            <div class="secondField">
                                <asp:DropDownList ID="ddlField3" runat="server" Width="200">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField3" runat="server"
                                ControlToValidate="ddlField3"></asp:RequiredFieldValidator>
                            <div class="thirdField">
                                <asp:DropDownList ID="ddlOperator3" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator3" runat="server"
                                ControlToValidate="ddlOperator3"></asp:RequiredFieldValidator>
                            <div class="fourthField">
                                <asp:TextBox ID="txtValue3" runat="server" CssClass="textBoxes" Width="150"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue3" runat="server"
                                ControlToValidate="txtValue3"></asp:RequiredFieldValidator>
                            <div class="fifthField">
                                <asp:DropDownList ID="ddlCatalog3" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearFix">
                            &nbsp;</div>
                        <div class="formRow">
                            <div class="firstField">
                                4.</div>
                            <div class="secondField">
                                <asp:DropDownList ID="ddlField4" runat="server" Width="200">
                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField4" runat="server"
                                ControlToValidate="ddlField4"></asp:RequiredFieldValidator>
                            <div class="thirdField">
                                <asp:DropDownList ID="ddlOperator4" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator4" runat="server"
                                ControlToValidate="ddlOperator4"></asp:RequiredFieldValidator>
                            <div class="fourthField">
                                <asp:TextBox ID="txtValue4" runat="server" CssClass="textBoxes" Width="150"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue4" runat="server"
                                ControlToValidate="txtValue4"></asp:RequiredFieldValidator>
                            <div class="fifthField">
                                <asp:DropDownList ID="ddlCatalog4" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearFix">
                            &nbsp;</div>
                        <div class="formRow">
                            <div class="firstField">
                                5.</div>
                            <div class="secondField">
                                <asp:DropDownList ID="ddlField5" runat="server" Width="200">
                                    <asp:ListItem Text="Select" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField5" runat="server"
                                ControlToValidate="ddlField5"></asp:RequiredFieldValidator>
                            <div class="thirdField">
                                <asp:DropDownList ID="ddlOperator5" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator5" runat="server"
                                ControlToValidate="ddlOperator5"></asp:RequiredFieldValidator>
                            <div class="fourthField">
                                <asp:TextBox ID="txtValue5" runat="server" CssClass="textBoxes" Width="150"></asp:TextBox>
                            </div>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue5" runat="server"
                                ControlToValidate="txtValue5"></asp:RequiredFieldValidator>
                            <div class="fifthField">
                                <asp:DropDownList ID="ddlCatalog5" runat="server" Width="150">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="clearFix">
                            &nbsp;</div>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" EnableViewState="true">
                            <ContentTemplate>
                                <%--<ComponentArt:CallBack ID="cbAddRow" runat="server" OnCallback="cbAddRow_Callback">
                            <Content>--%>
                                <asp:PlaceHolder ID="phRowHolder" runat="server" EnableViewState="true"></asp:PlaceHolder>
                                <%--     </Content>
                                 <ClientEvents>
                                 <CallbackComplete EventHandler="cbAddRow_OnCallbackComplete" />
                                 </ClientEvents>
                            </ComponentArt:CallBack>--%>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="lbtnAdd" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <%-- </Content>
                            </ComponentArt:CallBack> --%>
                        <asp:HiddenField ID="hfRowCount" Value="5" runat="server" />
                        <asp:HiddenField ID="hfUpdatedData" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="<%$ Resources:GUIStrings, PleaseFillAllTheFieldsOperatorsAndValuesAndAddNewRow %>"
                            ShowSummary="false" ShowMessageBox="true" />
                    </div>
                    <div class="formRow">
                        <asp:LinkButton ID="lbtnAdd" runat="server" Text="<%$ Resources:GUIStrings, AddRow %>"
                            CausesValidation="false" OnClick="lbtnAdd_Click"></asp:LinkButton>
                        <asp:LinkButton ID="lbtnRemove" runat="server" Text="<%$ Resources:GUIStrings, RemoveRow %>"></asp:LinkButton>
                    </div>
                    <div class="formRow">
                        <p>
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, AdvancedFilterConditions %>" /></p>
                        <asp:TextBox ID="txtAdvancedFilter" runat="server" CssClass="textBoxes" Width="400"></asp:TextBox>
                    </div>
                </div>
                <div class="footerContent">
                    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                        ToolTip="<%$ Resources:GUIStrings, Save %>" runat="server" CausesValidation="false" />
                    <input type="button" id="BtnCancel" runat="server" value="<%$ Resources:GUIStrings, Cancel %>"
                        class="button" title="<%$ Resources:GUIStrings, Cancel %>" onclick="parent.pagepopup.hide();" />
                </div>
            </div>
        </div>
    </div>
    <div>
        <ajaxToolkit:CascadingDropDown ID="cddField1" runat="server" TargetControlID="ddlField1"
            Category="Field" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator1" runat="server" TargetControlID="ddlOperator1"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField1" />
        <ajaxToolkit:CascadingDropDown ID="cddField2" runat="server" TargetControlID="ddlField2"
            Category="Field" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator2" runat="server" TargetControlID="ddlOperator2"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField2" />
        <ajaxToolkit:CascadingDropDown ID="cddField3" runat="server" TargetControlID="ddlField3"
            Category="Field" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator3" runat="server" TargetControlID="ddlOperator3"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField3" />
        <ajaxToolkit:CascadingDropDown ID="cddField4" runat="server" TargetControlID="ddlField4"
            Category="Field" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator4" runat="server" TargetControlID="ddlOperator4"
            Category="Operator" PromptText="Select" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField4" />
        <ajaxToolkit:CascadingDropDown ID="cddField5" runat="server" TargetControlID="ddlField5"
            Category="Field" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator5" runat="server" TargetControlID="ddlOperator5"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField5" />
    </div>
    </form>
</body>
</html>
