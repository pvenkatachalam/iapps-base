﻿<%@ Page Title="iAPPS Marketier: License" Language="C#" MasterPageFile="~/Popups/PopupMaster.Master" AutoEventWireup="true" 
CodeBehind="LicenseWarning.aspx.cs" Inherits="iAPPSMarketier.LicenseWarning" StylesheetTheme="General" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Popup.css" rel="Stylesheet" media="all" />
    <script type="text/javascript">
        function ClosePopup() {
            parent.licenseWarningWindow.hide();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="popupBoxShadow licenseWarning">
        <div class="popupboxContainer">
            <div class="dialogBox">
                <h3><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Warning %>" /></h3>
                <asp:PlaceHolder ID="pldContentManager" runat="server" Visible="false">
                    <div class="warningLeftColumn">
                        <asp:Image ID="imgCMS" runat="server" ImageUrl="~/images/cm-logo.png"
                            AlternateText="<%$ Resources:GUIStrings, iAPPSContentManager %>" />
                    </div>
                    <div class="warningRightColumn">
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ContentManagerEmpowersNontechnicalUsersToQuicklyAndEasilyCreate %>" /></p>
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AccessToContentManagerRequiresALicense %>" /></p>
                        <p style="height: 40px;">&nbsp;</p>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="pldAnalytics" runat="server" Visible="false">
                    <div class="warningLeftColumn">
                        <asp:Image ID="imgAnalytics" runat="server" ImageUrl="~/images/analytics-logo.png"
                            AlternateText="<%$ Resources:GUIStrings, iAPPSAnalytics %>" />
                    </div>
                    <div class="warningRightColumn">
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AnalyticsIsANextGenerationWebAnalyticsProduct %>" /></p>
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AccessToAnalyticsRequiresALicense %>" /></p>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="pldMarketier" runat="server" Visible="false">
                    <div class="warningLeftColumn">
                        <asp:Image ID="imgMarketier" runat="server" ImageUrl="~/images/marketier-logo.png"
                            AlternateText="<%$ Resources:GUIStrings, iAPPSMarketier %>" />
                    </div>
                    <div class="warningRightColumn">
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MarketierEmpowersYourMarketersToEasilyCreatePersonalized %>" /></p>
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AccessToMarketierRequiresALicense %>" /></p>
                        <p style="height: 50px;">&nbsp;</p>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="pldCommerce" runat="server" Visible="false">
                    <div class="warningLeftColumn">
                        <asp:Image ID="imgCommerce" runat="server" ImageUrl="~/images/commerce-logo.png"
                            AlternateText="<%$ Resources:GUIStrings, iAPPSCommerce %>" />
                    </div>
                    <div class="warningRightColumn">
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CommerceDeliversARobust %>" /></p>
                        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AccessToCommerceRequiresALicense %>" /></p>
                        <p style="height: 70px;">&nbsp;</p>
                    </div>
                </asp:PlaceHolder>
                <div class="clearFix">&nbsp;</div>
                <div class="footerContent">
                    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>"
                        OnClientClick="return ClosePopup();" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
