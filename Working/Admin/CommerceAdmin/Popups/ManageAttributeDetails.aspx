﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManageAttributeDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.ManageAttributeDetails"
    StylesheetTheme="General" ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".attribute-details .tab-section").iAppsTabs();
            $("#txtMinDate").datepicker({ onSelect: function (date) { $("#txtMinValue").val(date) } });
            $("#txtMaxDate").datepicker({ onSelect: function (date) { $("#txtMaxValue").val(date) } });

            $(".attribute-details").on("change", "#ddlDataType", function () {
                OnAttributeTypeChange(true);
            });

            $(".attribute-details").on("change", "#ddlMinDate", function () {
                OnDateChange();
                if ($(this).val() == "0") {
                    $("#txtMinDate").val("");
                    $("#txtMinValue").val("");
                }
                else {
                    $("#txtMinDate").val($(this).val());
                    $("#txtMinValue").val($(this).val());
                }
            });

            $(".attribute-details").on("change", "#ddlMaxDate", function () {
                OnDateChange();
                if ($(this).val() == "0") {
                    $("#txtMaxDate").val("");
                    $("#txtMaxValue").val("");
                }
                else {
                    $("#txtMaxDate").val($(this).val());
                    $("#txtMaxValue").val($(this).val());
                }
            });

            OnDateChange();
            OnAttributeTypeChange(false);
        });

        function OnDateChange() {
            if ($("#ddlMaxDate").val() == "0")
                $("#txtMaxDate").prop("disabled", false).prop("readonly", false);
            else
                $("#txtMaxDate").prop("disabled", true).prop("readonly", true);

            if ($("#ddlMinDate").val() == "0")
                $("#txtMinDate").prop("disabled", false).prop("readonly", false);
            else
                $("#txtMinDate").prop("disabled", true).prop("readonly", true);
        }

        function OnAttributeTypeChange(change) {
            var selectedType = $("#ddlDataType option:selected").text();

            $(".attribute-details .tab-section").iAppsTabs("setVisibility", 2, !change && $("#hdnId").val() != "");

            if (selectedType == "String")
                $("#attribute_validation").show();
            else
                $("#attribute_validation").hide();

            if (selectedType == "Integer" || selectedType == "Decimal")
                $("#attribute_range").show();
            else
                $("#attribute_range").hide();

            if (selectedType == "Date") {
                $("#attribute_daterange").show();
            }
            else {
                $("#attribute_daterange").hide();
            }
        }

        function upValues_OnLoad(sender, eventArgs) {
            $(".possible-values").gridActions({
                objList: $(".collection-table"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upValues_Callback(sender, eventArgs);
                }
            });
        }

        function upValues_OnRenderComplete(sender, eventArgs) {
            $(".possible-values").gridActions("bindControls");
        }

        function upValues_OnCallbackComplete(sender, eventArgs) {
            $(".possible-values").gridActions("displayMessage");

            $("#txtName").val("");
            $("#txtValueCode").val("");
        }

        function upValues_Callback(sender, eventArgs) {
            var doCallback = true;
            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");
                    break;
                case "Edit":
                    doCallback = false;
                    $("#txtName").val(eventArgs.selectedItem.getValue("Title"));
                    $("#txtValueCode").val(eventArgs.selectedItem.getValue("Code"));
                    $("#btnAdd").hide();
                    $("#btnUpdate").show();
                    break;
            }

            if (doCallback) {
                upValues.set_callbackParameter(JSON.stringify(gridActionsJson));
                upValues.callback();
            }
        }

        function AddAttributeValue() {
            if (Page_ClientValidate("valProperties") == true) {
                gridActionsJson.CustomAttributes["Title"] = $("#txtName").val();
                gridActionsJson.CustomAttributes["Code"] = $("#txtValueCode").val();

                $(".possible-values").gridActions("callback", "Add");
            }
            return false;
        }

        function UpdateAttributeValue() {
            if (Page_ClientValidate("valProperties") == true) {
                gridActionsJson.CustomAttributes["Title"] = $("#txtName").val();
                gridActionsJson.CustomAttributes["Code"] = $("#txtValueCode").val();

                $(".possible-values").gridActions("callback", "Update");
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valDetails" runat="server" ShowSummary="false" ShowMessageBox="True"
        ValidationGroup="valDetails" />
    <div class="tab-section">
        <ul>
            <li><span><a href="javascript://"><%= GUIStrings.Details %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.Values %></a></span></li>
        </ul>
        <div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Title %><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="260" />
                    <asp:HiddenField ID="hdnId" runat="server" />
                    <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server" ValidationGroup="valDetails"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtTitle" ControlToValidate="txtTitle"
                        ErrorMessage="<%$ Resources: JSMessages, TitleIsNotValid %>"
                        runat="server" ValidateType="Title" ValidationGroup="valDetails" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Description %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="258" TextMode="MultiLine" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtDescription" ControlToValidate="txtDescription"
                        ErrorMessage="<%$ Resources: JSMessages, DescriptionIsNotValid %>"
                        runat="server" ValidateType="Description" ValidationGroup="valDetails" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Type %><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlDataType" runat="server" Width="270" />
                </div>
            </div>
            <div class="form-row" id="attribute_validation">
                <label class="form-label"><%= GUIStrings.Validation %></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlValidation" runat="server" Width="268" />
                </div>
            </div>
            <div id="attribute_daterange">
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.MinValue %></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlMinDate" runat="server" Width="124">
                            <asp:ListItem Text="Specific Date" Value="0" />
                            <asp:ListItem Text="Current Date" Value="[CURRENTDATE]" />
                        </asp:DropDownList>
                        <asp:TextBox ID="txtMinDate" runat="server" CssClass="textBoxes" Width="132" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.MaxValue %></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlMaxDate" runat="server" Width="124">
                            <asp:ListItem Text="Specific Date" Value="0" />
                            <asp:ListItem Text="Current Date" Value="[CURRENTDATE]" />
                        </asp:DropDownList>
                        <asp:TextBox ID="txtMaxDate" runat="server" CssClass="textBoxes" Width="132" />
                    </div>
                </div>
            </div>
            <div id="attribute_range">
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.MinValue %></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMinValue" runat="server" CssClass="textBoxes" Width="260" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.MaxValue %></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMaxValue" runat="server" CssClass="textBoxes" Width="260" />
                    </div>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Code %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtCode" runat="server" CssClass="textBoxes" Width="260" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtCode" ControlToValidate="txtCode"
                        ErrorMessage="<%$ Resources: JSMessages, CodeIsNotValid %>"
                        runat="server" ValidateType="Description" ValidationGroup="valDetails" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    &nbsp;</label>
                <asp:CheckBox ID="chkIsSearchable" runat="server" Text="<%$ Resources:GUIStrings, Searchable %>" />
            </div>
        </div>
        <div class="possible-values" style="display: none;">
            <div class="form-row">
                <label class="form-label">&nbsp;</label>
                <div class="form-value checkbox-value">
                    <asp:CheckBox ID="chkIsMultiValued" runat="server" Text="<%$ Resources:GUIStrings, AllowMultipleValues %>" />
                </div>
            </div>
            <iAppsControls:CallbackPanel ID="upValues" runat="server" OnClientCallbackComplete="upValues_OnCallbackComplete"
                OnClientRenderComplete="upValues_OnRenderComplete" OnClientLoad="upValues_OnLoad">
                <ContentTemplate>
                    <asp:ValidationSummary ID="valValues" ValidationGroup="valProperties" runat="server" ShowSummary="false"
                        ShowMessageBox="True" />
                    <div class="form-section">
                        <h3>Properties</h3>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Value %>" /><span class="req">&nbsp;*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtName" runat="server" Width="200" />
                                <asp:RequiredFieldValidator ID="reqtxtName" runat="server" ControlToValidate="txtName" ValidationGroup="valProperties"
                                    Display="None" ErrorMessage="<%$ Resources:JSMessages, NameIsRequired %>" />
                                <asp:CompareValidator ID="cvtxtName" runat="server" ControlToValidate="txtName"
                                    Operator="DataTypeCheck" Display="None" ValidationGroup="valProperties" Enabled="false" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Code %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtValueCode" runat="server" Width="200" />
                                <iAppsControls:iAppsCustomValidator ID="cvtxtValueCode" ControlToValidate="txtValueCode"
                                    ErrorMessage="<%$ Resources: JSMessages, CodeIsNotValid %>"
                                    runat="server" ValidateType="Description" ValidationGroup="valProperties" />
                            </div>
                        </div>
                        <div class="button-row">
                            <asp:Button ID="btnAdd" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Add %>"
                                ToolTip="<%$ Resources:GUIStrings, Add %>" OnClientClick="return AddAttributeValue();"
                                ClientIDMode="Static" ValidationGroup="valProperties" />
                            <asp:Button ID="btnUpdate" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Update %>"
                                ToolTip="<%$ Resources:GUIStrings, Update %>" OnClientClick="return UpdateAttributeValue();"
                                ClientIDMode="Static" ValidationGroup="valProperties" Style="display: none;" />
                        </div>
                    </div>
                    <div class="grid-section">
                        <asp:ListView ID="lvValues" runat="server" ItemPlaceholderID="phValues">
                            <LayoutTemplate>
                                <div class="collection-table">
                                    <asp:PlaceHolder ID="phValues" runat="server" />
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class='<%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "row" : "alternate-row" %>'>
                                    <div class="collection-row grid-item clear-fix" objectid='<%# Eval("Id") %>'>
                                        <div class="first-cell">
                                            <%# Container.DataItemIndex + 1%>
                                        </div>
                                        <div class="middle-cell">
                                            <%# String.Format("{0} {1} {2}", Eval("Title"), 
                                            Eval("Code") == null || Eval("Code").Equals(string.Empty) ? "" : "|", 
                                            Eval("Code"))%>
                                        </div>
                                        <div class="last-cell">
                                            <%# String.Format("<a href='#' objectId='{0}' class='edit-grid-item'>{1}</a> | <a href='#' objectId='{0}' class='delete-grid-item'>{2}</a>",
                                            Eval("Id"), GUIStrings.Edit, GUIStrings.Delete) %>
                                        </div>
                                        <input type="hidden" class="hdnTitle" datafield="Title" value='<%# Eval("Title") %>' />
                                        <input type="hidden" class="hdnCode" datafield="Code" value='<%# Eval("Code") %>' />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>

                </ContentTemplate>
            </iAppsControls:CallbackPanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSaveClose" runat="server" Text="<%$ Resources:GUIStrings, SaveAndClose %>" ValidationGroup="valDetails"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAndClose %>" OnClick="btnSaveClose_Click" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ValidationGroup="valDetails"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
</asp:Content>
