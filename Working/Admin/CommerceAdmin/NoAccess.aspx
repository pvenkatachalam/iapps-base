<%@ Page Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true"
    StylesheetTheme="General" Inherits="NoAccess" Title="iAPPS Content Manager: Access Denied" CodeBehind="NoAccess.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainPlaceHolder" runat="Server">
    <div class="error">
        <div class="boxShadow">
            <div class="boxContainer">
                <div class="boxHeader">
                    <h5><asp:localize runat="server" text="<%$ Resources:GUIStrings, AccessDenied %>" /></h5>
                </div>
                <div class="paddingContainer">
                    <div class="boxContent">
                        <p>
                            <asp:localize runat="server" text="<%$ Resources:GUIStrings, SorryYouAreNotAuthorizedToViewThisPage %>" /><br /><br />
                            <asp:localize runat="server" text="<%$ Resources:GUIStrings, PleaseContactTheAdministratorOrGoToTheControlCenterAndTryAgain %>" />&nbsp;
                            <asp:HyperLink ID="gotoControlCenter" runat="server" Text="<%$ Resources:GUIStrings, ControlCenter %>" NavigateUrl="~/General/ControlCenter.aspx"></asp:HyperLink>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
