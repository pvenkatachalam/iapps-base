﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsManageProductImages %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    AutoEventWireup="true" CodeBehind="ProductImages.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductImages"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var gridOperationColumnIndex = 10;      //GridOperation
        var gridParameterColumnIndex = 11;      //GridParameters
        var selectedItem;
        var selectedOperation = "";

        function RefreshGrid() {
            grdImages.Callback();
        }

        function ShowImageUploadPopup() {
            popupPath = '../../Popups/StoreManager/ImageUpload.aspx';
            pagepopup = dhtmlmodal.open('ImageUpload', 'iframe', popupPath, '', 'width=370px,height=430px,center=1,resize=0,scrolling=0');
            pagepopup.onclose = function () {
                var a = document.getElementById('ImageUpload');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("blank.html");
                return true;
            }
        }

        function onRowSelect()
        {
            //Fill the details
            callbackSKUs.callback(selectedItem.GetMember('ParentImage.Id').get_text());
            var mainImagePrefix = document.getElementById("<%=hdnMainImgPrefix.ClientID%>").value;
                    var previewImagePrefix = document.getElementById("<%=hdnPreviewImgPrefix.ClientID%>").value;
            var imageURL = selectedItem.GetMember('ParentImage.RelativePath').get_text();
            var imageName = imageURL.substring(imageURL.lastIndexOf("/"));
            imageURL = imageURL.substring(0, imageURL.lastIndexOf("/"));
            var previewImageURL = "";
            if (mainImagePrefix != "" && imageName.indexOf(mainImagePrefix) > -1) {
                previewImageURL = imageName.replace(mainImagePrefix, previewImagePrefix);
            }
            else {
                previewImageURL = imageName.replace("/", "/" + previewImagePrefix);
            }
            document.getElementById('<%=hdnMainImageId.ClientID%>').value = selectedItem.GetMember('ParentImage.Id').get_text();
                    document.getElementById('<%=previewImage.ClientID%>').src = imageURL + previewImageURL;
            document.getElementById('<%=previewImage.ClientID%>').setAttribute("alt", selectedItem.GetMember('ParentImage.AltText').get_text());
            document.getElementById('<%=txtFileName.ClientID%>').value = selectedItem.GetMember('ParentImage.FileName').get_text();
            document.getElementById('<%=txtImageName.ClientID%>').value = selectedItem.GetMember('ParentImage.Title').get_text();
            document.getElementById('<%=txtAltText.ClientID%>').value = selectedItem.GetMember('ParentImage.AltText').get_text();
            document.getElementById('<%=txtImageDescription.ClientID%>').value = selectedItem.GetMember('ParentImage.Description').get_text();
        }

        function grdImages_onLoad(sender, eventArgs) {
           
            if (grdImages.get_recordCount() > 0)
            {
                if (grdImages.getSelectedItems().length == 0) {
                    grdImages.select(grdImages.get_table().getRow(0), true);
                } else {
                    //grdImages.select(grdImages.getSelectedItems()[0]);
                    selectedItem = grdImages.getSelectedItems()[0];
                    onRowSelect();
                }
            }

            //grdImages.render();
        }

        
        function mnuImage_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            switch (selectedMenuId) {
                case "cmAdd":
                    ShowImageUploadPopup();
                    break;
                case "cmRemove":
                    if (grdImages.get_recordCount() > 1)
                        grdImages.scrollBy(-1);
                    selectedOperation = "Remove";
                    grdImages.edit(selectedItem);
                    selectedItem.setValue(gridOperationColumnIndex, selectedOperation);
                    grdImages.editComplete();
                    grdImages.Callback();
                    break;
            }
        }


        function grdImages_OnExternalDrop(sender, eventArgs) {
            
            // The image being dragged for changing the seqence
            var dragImageId = eventArgs.get_item().getMember("ParentImage.Id").get_text();
            // targetItem is the image onto which the dragged image is being dropped
            var targetItem = eventArgs.get_target();
            var targetItemId = null;
            if (targetItem != null) {
                targetItemId = targetItem.getMember("ParentImage.Id").get_text();
            }
            if (targetItemId != null && targetItemId != dragImageId) {
                var rowitem = grdImages.get_table().addEmptyRow(0);
                var gridc = grdImages.get_table().get_columns();
                var param = dragImageId + ";" + targetItemId;
                rowitem.SetValue(gridParameterColumnIndex, param);
                rowitem.setValue(gridOperationColumnIndex, "ChangeSequence");
                grdImages.editComplete();
                grdImages.Callback();
            }
        }

        function grdImages_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            mnuImage.showContextMenuAtEvent(e);
            grdImages.select(eventArgs.get_item());
        }

        function selectRow()
        {

        }
        function grdImages_onItemSelect(sender, eventArgs) {
            selectedItem = eventArgs.get_item();

            onRowSelect();
        }
        function grdImages_onCallbackComplete(sender, eventArgs) {
            
            var imageCount = grdImages.get_recordCount();
            if (imageCount > 0 )
            {
                if (grdImages.getSelectedItems().length == 0) {
                    grdImages.select(grdImages.get_table().getRow(0), true)
                    selectedOperation = "";
                }
                else {
                    //grdImages.select(grdImages.getSelectedItems()[0]);
                    selectedItem = grdImages.getSelectedItems()[0];
                    onRowSelect();
                    

                }
            }

            grdImages.render();
            if (imageCount == 0) {
                document.getElementById('<%=previewImage.ClientID%>').src = "../../App_Themes/General/images/no-image.gif";
                document.getElementById('<%=txtFileName.ClientID%>').value = "";
                document.getElementById('<%=txtImageName.ClientID%>').value = "";
                document.getElementById('<%=txtAltText.ClientID%>').value = "";
                document.getElementById('<%=txtImageDescription.ClientID%>').value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <div class="columns image-list-column">
        <asp:HiddenField ID="hdnPreviewImgPrefix" runat="server" />
        <asp:HiddenField ID="hdnMainImgPrefix" runat="server" />
        <ComponentArt:Grid ID="grdImages" SkinID="Default" runat="server" Width="100%" ShowHeader="false"
            ShowFooter="false" RunningMode="Callback" PageSize="1000" AutoPostBackOnSelect="false"
            EmptyGridText="<%$ Resources:GUIStrings, NoImagesFound %>" AllowEditing="true"
            AutoCallBackOnUpdate="true" ItemDraggingEnabled="true" ItemDraggingClientTemplateId="dragTemplate"
            ExternalDropTargets="grdImages">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowHeadingCells="false" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                    ShowTableHeading="false" ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn Width="60" DataField="RelativePath" DataCellClientTemplateId="ProductImagesTemplates"
                            DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell" AllowReordering="false"
                            FixedWidth="true" Align="Center" />
                        <ComponentArt:GridColumn DataField="ParentImage.Title" AllowEditing="False" Visible="true"
                            DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell" DataCellClientTemplateId="ImageTitleHoverTemplate"
                            TextWrap="true" />
                        <ComponentArt:GridColumn DataField="ParentImage.FileName" Visible="false" />
                        <ComponentArt:GridColumn DataField="ParentImage.Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="ParentImage.RelativePath" Visible="false" />
                        <ComponentArt:GridColumn DataField="ParentImage.AltText" Visible="false" />
                        <ComponentArt:GridColumn DataField="ParentImage.Description" Visible="false" />
                        <ComponentArt:GridColumn DataField="Width" Visible="false" />
                        <ComponentArt:GridColumn DataField="Height" Visible="false" />
                        <ComponentArt:GridColumn Visible="false" HeadingText="GridOperation" />
                        <ComponentArt:GridColumn Visible="false" HeadingText="GridParameters" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="ProductImagesTemplates">
                    <img src="## DataItem.GetMember('RelativePath').get_text() ##" alt="## DataItem.GetMember('ParentImage.AltText').get_text() ##" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="dragTemplate">
                    <div style="height: 100px; width: 100px;">
                        ## DataItem.getMember('ParentImage.Title').get_text() ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ImageTitleHoverTemplate">
                    <span title="## DataItem.getMember('ParentImage.Title').get_text() ##">## DataItem.getMember('ParentImage.Title').get_text()
                        ##</span>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ItemSelect EventHandler="grdImages_onItemSelect" />
                <Load EventHandler="grdImages_onLoad" />
                <ItemExternalDrop EventHandler="grdImages_OnExternalDrop" />
                <CallbackComplete EventHandler="grdImages_onCallbackComplete" />
                <ContextMenu EventHandler="grdImages_onContextMenu" />
            </ClientEvents>
        </ComponentArt:Grid>
    </div>
    <div class="columns" style="width: 65.5%">
        <div class="button-row">
            <asp:HyperLink ID="hplUploadImage" runat="server" Text="<%$ Resources:GUIStrings, UploadNewImage %>"
                NavigateUrl="#" onclick="return ShowImageUploadPopup();" CssClass="small-button" Style="float: left;"
                Title="<%$ Resources:GUIStrings, ClickToUploadNewImage %>" />
            <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
                ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" ValidationGroup="SubmitGroup" />
            <div class="clear-fix">
            </div>
        </div>
        <div class="image-column">
            <asp:Image ID="previewImage" runat="server" ImageUrl="~/App_Themes/General/images/no-image.gif" />
            <ComponentArt:Menu ID="mnuImage" runat="server" SkinID="ContextMenu">
                <ClientEvents>
                    <ItemSelect EventHandler="mnuImage_onItemSelect" />
                </ClientEvents>
            </ComponentArt:Menu>
        </div>
        <div class="image-form">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, FileName1 %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtFileName" runat="server" CssClass="disabled" Width="325" Enabled="false" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ImageTitle1 %>" /><span
                        class="req">*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtImageName" runat="server" CssClass="textBoxes" Width="325" />
                    <asp:RequiredFieldValidator CultureInvariantValues="true" ID="reqImageName" runat="server"
                        ControlToValidate="txtImageName" ValidationGroup="SubmitGroup" Display="None"
                        SetFocusOnError="true" ErrorMessage="*" Text="<%$ Resources:GUIStrings, ImageNameIsRequired %>">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ALTText1 %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtAltText" runat="server" CssClass="textBoxes" Width="325"> </asp:TextBox>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" style="float: left;">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtImageDescription" runat="server" TextMode="MultiLine" Width="327"
                        Rows="5"></asp:TextBox>
                    <asp:HiddenField ID="hdnMainImageId" runat="server" />
                </div>
                <div class="clear-fix">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="">
                    <%= GUIStrings.SKU %></label>
                <div class="form-value">
                    <ComponentArt:CallBack runat="server" ID="callbackSKUs" PostState="false">
                        <Content>
                            <iapps:SKUSelector runat="server" ID="chkListSKUs" AutoPostbackOnClose="false" CarryForwardSelectedState="false"
                                UsePreviousSelectedState="false">
                                <SKUList ID="skuList" runat="server" RepeatDirection="Horizontal" RepeatColumns="1"
                                    CssClass="test">
                                </SKUList>
                            </iapps:SKUSelector>
                        </Content>
                    </ComponentArt:CallBack>
                </div>
            </div>
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="List" EnableClientScript="true"
        ShowMessageBox="true" ShowSummary="true" />
</asp:Content>
