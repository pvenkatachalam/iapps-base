﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsSEOTerms %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    StylesheetTheme="General" AutoEventWireup="true" CodeBehind="SEOTerms.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Products.SEOTerms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
<div class="product-seo">
    <div class="button-row">
        <asp:Button ID="btnReset" CssClass="button" runat="server" Text="<%$ Resources:GUIStrings, Reset %>"
            ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClick="btnReset_OnClick" />
        <asp:Button ID="btnSave" OnClick="btnSave_OnClick" CssClass="primarybutton"
            runat="server" ValidationGroup="PageLevel" onblur="return true;" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, SaveChangesToTheSEOTerms %>" />
        <asp:ValidationSummary ID="valSum" runat="server" EnableClientScript="true" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="PageLevel" />
        <iapps:CommerceStateControl ID="commerceState" runat="server">
        </iapps:CommerceStateControl>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SEOTitle1 %>" /><span class="req">*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtSEOTitle" runat="server" CssClass="textBoxes" Width="1010" MaxLength="512"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="reqtxtSEOTitle" runat="server"
                ValidationGroup="PageLevel" ControlToValidate="txtSEOTitle" ErrorMessage="<%$ Resources:GUIStrings, SEOTitleIsRequired %>"
                Text="*"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="seoTitleRegex" runat="server" ControlToValidate="txtSEOTitle"
                ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSEOTitle %>"
                Text="*" ValidationGroup="PageLevel">
            </asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SEOProductTitleH1tag %>" /><span class="req">*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtSEOProductTitle" runat="server" CssClass="textBoxes" Width="1010"
                MaxLength="512"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="reqtextSEOProductTitle"
                runat="server" ValidationGroup="PageLevel" ControlToValidate="txtSEOProductTitle"
                ErrorMessage="<%$ Resources:GUIStrings, SEOProductTitleIsRequired %>" Text="*"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="seoProductTitleRegex" runat="server" ControlToValidate="txtSEOProductTitle"
                ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSEOProductTitle %>"
                Text="*" ValidationGroup="PageLevel">
            </asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SEOKeywords %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtSEOKeywords" runat="server" CssClass="textBoxes" Width="1010"
                MaxLength="1024"></asp:TextBox>
            <asp:RegularExpressionValidator ID="seoKeywordsRegex" runat="server" ControlToValidate="txtSEOKeywords"
                ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSEOKeywords %>"
                Text="*" ValidationGroup="PageLevel">
            </asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SEOFriendlyURLPageName %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtSEOFriendlyURL" runat="server" CssClass="textBoxes" Width="1010"
                MaxLength="1024"></asp:TextBox>
            <asp:RegularExpressionValidator ID="seoFriendlyURL" runat="server" ControlToValidate="txtSEOFriendlyURL"
                ValidationExpression="^[0-9a-zA-Z-_/]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidcharactersinSEOFriendlyUrl %>"
                Text="*" ValidationGroup="PageLevel">
            </asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label" style="float: left;">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtSEODescription" runat="server" TextMode="MultiLine" Rows="10"
                Width="1013" MaxLength="2048"></asp:TextBox>
            <asp:RegularExpressionValidator ID="seoDescription" runat="server" ControlToValidate="txtSEODescription"
                ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSEODescription %>"
                Text="*" ValidationGroup="PageLevel">
            </asp:RegularExpressionValidator>
        </div>
        <div class="clear-fix"></div>
    </div>
</div>
</asp:Content>
