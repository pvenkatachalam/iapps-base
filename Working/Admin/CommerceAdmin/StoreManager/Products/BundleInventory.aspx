﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsManageInventoryandShipping %>" Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    AutoEventWireup="true" CodeBehind="BundleInventory.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.BundleInventory" StylesheetTheme="General" %>
<%@ Register assembly="Bridgeline.FW.Commerce.Controls" namespace="Bridgeline.FW.Commerce.Controls" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <cc1:CommerceStateControl runat="server" id="commerceState" />
    <div class="InventoryShipping BundleInventory">
        <span class="formRow">
            <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, SKU1 %>"/></label>
            <asp:TextBox runat="server" ReadOnly="true" ID="txtBundleSKU" Width="200" CssClass="textBoxes textBoxesDisabled" />
        </span>
        <span class="formRow">
            <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, AvailableToSell1 %>"/></label>
            <asp:TextBox runat="server" ReadOnly="true" ID="txtBundleQty" Width="75" CssClass="textBoxes textBoxesDisabled" />
        </span>
        <div class="gridContainer">
            <ComponentArt:Grid SkinId="Default" ID="grdBundleInventory" SliderPopupClientTemplateId="BundleInventorySliderTemplate" 
                SliderPopupCachedClientTemplateId="BundleInventorySliderTemplateCached" Width="930" runat="server" RunningMode="Callback" 
                EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10"
                PagerInfoClientTemplateId="BundleInventoryPaginationTemplate" CallbackCachingEnabled="true" SearchOnKeyPress="true">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                        <Columns>
                            <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, Thumbnail %>" HeadingCellCssClass="FirstHeadingCell"  Align="Center" AllowSorting="False" 
                                DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true" DataField="Thumbnail"
                                 AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="ImageHoverTemplate" 
                                 />
                                 
                            <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings, ProductNameSKUs %>" HeadingCellCssClass="FirstHeadingCell" DataField="ProductName" Align="Left" 
                                DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                                 AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate"
                                 />
                            <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, OnHand %>" DataField="QuantityOnHand" SortedDataCellCssClass="SortedDataCell" Align="Right"
                                 AllowReordering="false" FixedWidth="true" IsSearchable="false"
                                DataCellClientTemplateId="QtyOnHandHoverTemplate" />
                                
                            <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, Allocated %>" HeadingCellCssClass="FirstHeadingCell" DataField="AllocatedQuantity" Align="Right" 
                                DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"
                                 AllowReordering="false" FixedWidth="true"
                                DataCellClientTemplateId="AllocatedHoverTemplate" />
                                
                            <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, AvailableToSell %>" HeadingCellCssClass="FirstHeadingCell" Align="Right" 
                                DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" IsSearchable="true"  DataField="QuantityOnHand - AllocatedQuantity"
                                 AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AvailableToSellHoverTemplate"
                                />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                        <span title="## DataItem.GetMember('ProductName').get_text() ##">## DataItem.GetMember('ProductName').get_text() == "" ? "&nbsp;" : DataItem.GetMember('ProductName').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="QtyOnHandHoverTemplate">
                        <span title="## DataItem.GetMember('QuantityOnHand').get_text() ##">## DataItem.GetMember('QuantityOnHand').get_text() == "" ? "0" : DataItem.GetMember('QuantityOnHand').get_text()!="999999999"?DataItem.GetMember('QuantityOnHand').get_text():"unlimited" ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="AllocatedHoverTemplate">
                        <span title="## DataItem.GetMember('AllocatedQuantity').get_text() ##">## DataItem.GetMember('AllocatedQuantity').get_text() == "" ? "0" : DataItem.GetMember('AllocatedQuantity').get_text() ##</span>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ImageHoverTemplate">
                        <img src="## DataItem.GetMember('Thumbnail').get_text() == "" ? "../../App_Themes/General/images/last.gif" : DataItem.GetMember('Thumbnail').get_text() ##" alt="## DataItem.GetMember('ProductName').get_text() ##" title="## DataItem.GetMember('ProductName').get_text() ##" />
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="AvailableToSellHoverTemplate">
                        <span title="## DataItem.GetMember(4).get_text() ##">##DataItem.GetMember('QuantityOnHand').get_text()!="9999999"? DataItem.GetMember(4).get_text() == "" ? "0" : DataItem.GetMember(4).get_text():"unlimited" ##</span>
                    </ComponentArt:ClientTemplate>

                    <ComponentArt:ClientTemplate ID="BundleInventorySliderTemplate">
                        <div class="SliderPopup">
                            <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, DataNotLoaded %>"/></p>
                            <p class="paging">
                                <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdBundleInventory.PageCount) ##</span>
                                <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdBundleInventory.RecordCount) ##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="BundleInventorySliderTemplateCached">
                        <div class="SliderPopup">
                            <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                            <p>## DataItem.GetMemberAt(1).Value ##</p>
                            <p class="paging">
                                <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdBundleInventory.PageCount) ##</span>
                                <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdBundleInventory.RecordCount) ##</span>
                            </p>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="BundleInventoryPaginationTemplate">
                        ## stringformat(Page0Of1Items, currentPageIndex(grdBundleInventory), pageCount(grdBundleInventory), grdBundleInventory.RecordCount) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
    <div class="footerContent">
        <asp:Button ID="btnSave" Visible="false" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
        <asp:Button ID="btnCancel" OnClick="btnCancel_OnClick" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" />
    </div>
</asp:Content>
