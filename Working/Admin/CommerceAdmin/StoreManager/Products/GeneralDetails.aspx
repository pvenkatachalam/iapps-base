﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsGeneralDetails %>"
    Language="C#" ValidateRequest="false" MasterPageFile="~/StoreManager/Products/ProductDetails.master" MaintainScrollPositionOnPostback="true"
    AutoEventWireup="true" EnableEventValidation="false" CodeBehind="GeneralDetails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.GeneralDetails" StylesheetTheme="General"  EnableViewStateMac="False" ViewStateEncryptionMode="Never"  %>

<%@ Register Assembly="Bridgeline.FW.Commerce.Controls" Namespace="Bridgeline.FW.Commerce.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="Bridgeline.FW.UI.ControlLibrary" Namespace="Bridgeline.FW.UI.ControlLibrary"
    TagPrefix="cc2" %>
<%@ Register Src="~/UserControls/StoreManager/Products/ProductLevelAttributes.ascx"
    TagName="ProductLevelAttributes" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/StoreManager/Products/ProductSKUImage.ascx" TagName="ProductSKUImage"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/StoreManager/Products/SKUAttributes.ascx" TagName="SKUAttributes"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/General/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/StoreManager/Products/NewDynamicAttribute.ascx"
    TagName="DynamicAttributes" TagPrefix="uc3" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var tooltipIconSrc = '../../App_Themes/General/images/icon-tooltip.gif';
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        //<![CDATA[
        function trvProductTypes_onNodeSelect(sender, eventArgs) {
            ddlProductType.set_text(eventArgs.get_node().get_text());
            ddlProductType.collapse();
            //Todo:Call a method to do callback or postback
        }

        function ShowAttributesPopup(isSku) {
            popupPath = '../../Popups/StoreManager/AddProductAttribute.aspx?isSku=' + isSku;
            pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=562px,height=480px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function () {
                var a = document.getElementById('ShowAttributes');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        /******************************* Added for MS ***********************************************/
        function UpdateAttributeValues() {
            document.getElementById("<%=hfDeleteVal.ClientID %>").value = "popup";
            UpdateMultiStringHiddenVariable();
            __doPostBack();
        }

        function ShowModalPopup(AttId, ProdId, isSku, ctrlId, skuID) {
            popupPath = '../../Popups/StoreManager/MultiAttributeValue.aspx?ProductId=' + ProdId + '&AttributeId=' + AttId + '&isSku=' + isSku + '&ctrlId=' + ctrlId + '&SkuId=' + skuID;
            //alert(popupPath);
            //popupPath = '../../Popups/StoreManager/MultiAttributeValue.aspx?callback=' + callbackId.Id + '&ProductId='+ProdId+'&AttributeId=' + AttId;            
            pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=330px,height=438px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function () {
                var a = document.getElementById('ShowAttributes');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        /***************End**********************************************************************************************/

        function confirmDelete(skuId) {
            //        //var confirmMessage =CheckDeleteProductSKU(skuId,'0');
            //        
            //        if(confirmMessage.substring(0,1)=='~')
            //        {
            //            alert(confirmMessage.substring(1));
            //            return false;
            //        }
            //        if(confirm(confirmMessage))
            //            return true;
            //        else
            //            return false;

            var referedProduct = CheckDeleteProductSKU(skuId, '0');
            var objReferedProduct = eval('(' + referedProduct + ')');
            var returnVal = false;
            var isdeletemsg = objReferedProduct[0].ReturnVal;

            if (isdeletemsg == 1) {
                if (confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, YouCannotDeleteASKUAlreadyPartOfAnOrderDoYouWantToMakeInactive %>"/>')) {
                    document.getElementById("<%=hfDeleteVal.ClientID %>").value = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, InActive %>'/>";
                    returnVal = true;
                }

            }
            else if (isdeletemsg == 2) {
                var msg = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, YouCannotDeleteASKUAlreadyPartOfAnBundle %>"/>';
                var count = 0;
                for (var i = 0; i < objReferedProduct.length; i++) {
                    msg += " \n " + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ReferedProductSKUs %>'/>" + " \n";
                    count = i + 1;
                    msg += count + ") " + objReferedProduct[i].SKU + " \n";
                }
                alert(msg);
                returnVal = false;

            }
            else if (isdeletemsg == 3) {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, YouCannotDeleteASKUAlreadyPartOfCart %>'/>");
                returnVal = false;
            }
            else if (isdeletemsg == 4) {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, YouCannotDeleteASKUAlreadyPartOfWishlist %>'/>");
                returnVal = false;
            }
            else if (isdeletemsg == 5) {

                var msg = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, YouCannotDeleteASkuThatIsPartOfProductRelation %>'/>";

                var count = 0;
                for (var i = 0; i < objReferedProduct.length; i++) {
                    msg += " \n" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, RelatedProducts1 %>'/>" + " \n";
                    count = i + 1;
                    msg += count + ") " + objReferedProduct[i].ProductTitle + " \n";
                }
                alert(msg);
                returnVal = false;

            }
            else {
                if (window.jNewSKU != undefined && jNewSKU == "true") {
                    alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, NoSKUFoundToDelete %>'/>");
                    returnVal = false;
                }
                else {
                    var message = objReferedProduct[0].Message;
                    if (confirm(message))
                        returnVal = true;
                }

            }
            return returnVal;
            //return false; //TO DO : change to returnVal

        }

        var jProductOrSku = null;
        function ShowIndexTermPopup(listBox, productOrSku) {

            var objectId = "";
            var selectedTaxonomyIds = "";
            var listBox = document.getElementById(listBox);
            for (i = 0; i < listBox.options.length; i++) {
                if (selectedTaxonomyIds != "")
                    selectedTaxonomyIds = selectedTaxonomyIds + ',' + listBox.options[i].value;
                else
                    selectedTaxonomyIds = listBox.options[i].value;
            }

            jProductOrSku = productOrSku;
            if (productOrSku == "Product")
                objectId = jCommerceProductID;
            else if (productOrSku == "SKU") {
                if (window.jNewSKU != undefined && jNewSKU == "true")
                    objectId = "";
                else
                    objectId = jCommerceProductSKUID;
            }

            var customAttributes = {}
            customAttributes["SelectedTaxonomyIds"] = selectedTaxonomyIds;
            OpeniAppsAdminPopup("AssignIndexTermsPopup", "ObjectId=" + objectId, "SetIndexTerm", customAttributes);
        }

        function SetIndexTerm() {
            var listBox;
            var hdnIndexTermsList;
            if (jProductOrSku == "Product") {
                listBox = $("#<%=listAssignedIndexTerms.ClientID%>");
                hdnIndexTermsList = $("#<%=hdnIndexTermsList.ClientID%>");
            }
            else {
                listBox = $("#<%=listAssignedIndexTermsSKU.ClientID%>");
                hdnIndexTermsList = $("#<%=hdnIndexTermsListSKU.ClientID%>");
            }

            listBox.empty();
            hdnIndexTermsList.val("");
            if (typeof popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "undefined" &&
                        popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "") {
                var taxonomyJson = JSON.parse(popupActionsJson.CustomAttributes["SelectedTaxonomyJson"]);

                $.each(taxonomyJson, function () {
                    listBox.append($("<option />").val(this.Id).html(this.Title));
                });

                hdnIndexTermsList.val(JSON.stringify(taxonomyJson));
            }
        }

        function updateValidator(obj) {
            var enableValidator = false;
            if (obj.childNodes[1].value > 0) enableValidator = true;

            var txtBackOrder = document.getElementById("<%=txtBackOrderLimit.ClientID%>");
            if (enableValidator == false) {
                txtBackOrder.value = "";
                txtBackOrder.disabled = true;
            }
            else
                txtBackOrder.disabled = false;

            var rfvBackOrderLimit = document.getElementById("<%=rfvtxtBackOrderLimit.ClientID%>");
            var rvBackOrderLimit = document.getElementById("<%=rvtxtBackOrderLimit.ClientID%>");
            ValidatorEnable(rfvBackOrderLimit, enableValidator);
            ValidatorEnable(rvBackOrderLimit, enableValidator);
        }
        //]]>
    </script>
    <style type="text/css">
        div.product-details .sku-properties .tri-state-checkbox {
            display: block;
            padding-bottom: 8px;
        }

		div.product-details .sku-properties span {
		    display: inline;
		}
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server" />
    <div class="button-row">
        <asp:Button ID="btnDeleteSKU" OnClick="btnDelete_OnClick" CssClass="button"
            runat="server" Text="<%$ Resources:GUIStrings, DeleteSKU %>" ToolTip="<%$ Resources:GUIStrings, DeleteSKU %>" />
        <asp:Button ID="btnCancel" CssClass="button" runat="server" OnClick="btnCancel_OnClick"
            Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
        <asp:Button ID="btnSave" OnClick="btnSave_OnClick" CssClass="primarybutton"
            runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
            ValidationGroup="PageLevel" />
    </div>
    <div class="clear-fix">
        <div class="columns" style="width: 79%;">
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Type %>" /><span
                    class="req">*</span></label>
            <div class="form-value">
                <ComponentArt:ComboBox ID="ddlProductType" runat="server" SkinID="Default" DropDownHeight="297"
                    DropDownWidth="320" AutoHighlight="false" AutoComplete="false" Width="300">
                    <DropDownContent>
                        <ComponentArt:TreeView ID="trvProductTypes" ExpandSinglePath="true" SkinID="Default"
                            OnNodeSelected="trvProductType_onSelect" AutoPostBackOnSelect="true" Height="293"
                            Width="319" CausesValidation="false" runat="server">
                            <ClientEvents>
                                <NodeSelect EventHandler="trvProductTypes_onNodeSelect" />
                            </ClientEvents>
                        </ComponentArt:TreeView>
                    </DropDownContent>
                </ComponentArt:ComboBox>
                <asp:HiddenField runat="server" ID="hdnProductTypeID" />
                 <asp:HiddenField ID="hdnSKUFilterAttribute" runat="server" />
                <asp:RequiredFieldValidator ID="rvProductType" runat="server" ControlToValidate="ddlProductType"
                        ValidationGroup="PageLevel" ErrorMessage="<%$ Resources:GUIStrings, ProductTypeIsRequired %>"
                    Text="" Enabled="<%# ddlProductType.Enabled%>"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, ProductName %>" /><span
                    class="req">*</span></label>
            <div class="form-value">
                    <asp:TextBox ID="txtProdName" runat="server" Width="790" MaxLength="255"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtProdName"
                        ValidationGroup="PageLevel" ControlToValidate="txtProdName" ErrorMessage="<%$ Resources:GUIStrings, ProductNameIsRequired %>"
                    Text="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regextxtProdName" ValidationGroup="PageLevel"
                        ControlToValidate="txtProdName" ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInProductName %>"
                    Text="*"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="regextxtProdNameLength" ValidationGroup="PageLevel"
                        ControlToValidate="txtProdName" ValidationExpression="^[\s\S]{0,255}$" ErrorMessage="<%$ Resources:GUIStrings, ProductNameCannotExceed255Characters %>"
                    Text="*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ShortbrDescription %>" /></label>
            <div class="form-value">
                <uc2:HtmlEditor runat="Server" ID="txtProductShortDescription" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
            <div class="form-value">
                <uc2:HtmlEditor runat="Server" ID="txtProdDescription" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, ProductNumber1 %>" /><span
                    class="req">*</span></label>
            <div class="form-value">
                <asp:TextBox ID="txtProdNumber" runat="server" Width="300"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtProdNumber"
                        ValidationGroup="PageLevel" ControlToValidate="txtProdNumber" ErrorMessage="<%$ Resources:GUIStrings, ProductNumberIsRequired %>"
                    Text="*"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ValidationGroup="PageLevel"
                        ControlToValidate="txtProdNumber" ValidationExpression="^[a-zA-Z0-9-_]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInProductNumber %>"
                    Text="*"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator runat="server" ID="regextxtProdNumberLength" ValidationGroup="PageLevel"
                        ControlToValidate="txtProdNumber" ValidationExpression="^[\s\S]{0,50}$" ErrorMessage="<%$ Resources:GUIStrings, ProductNumberCannotExceed50Characters %>"
                    Text="*"></asp:RegularExpressionValidator>
                <asp:CustomValidator ID="cvProductCode" runat="server" Display="Dynamic" ControlToValidate="txtProdNumber"
                    ValidationGroup="PageLevel" OnServerValidate="txtProdNumber_Validate" Text="*"
                        ErrorMessage="<%$ Resources:GUIStrings, ProductWithThisCodeAlreadyExists %>" />
            </div>
        </div>
        <asp:PlaceHolder ID="phTaxCategory" runat="server">
            <div class="form-row">
                <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, TaxCategory %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlTaxCategory" runat="server" Width="310">
                    </asp:DropDownList>
                </div>
            </div>
        </asp:PlaceHolder>
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, IndexTerms1 %>" />
                    <img src="../../App_Themes/General/images/icon-tooltip.gif" alt="" onmouseout="hideddrivetip();" onmouseover="ddrivetip('<%= GUIStrings.Products_ProductIndexTermsHelp %>', 300,'');positiontip(event);" />
                <br />
                <a href="javascript:ShowIndexTermPopup('<%=listAssignedIndexTerms.ClientID%>','Product');">
                        <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, AssignTerms %>" /></a>
            </label>
            <div class="form-value">
                <asp:ListBox ID="listAssignedIndexTerms" CssClass="textBoxes textBoxesWidth" runat="server"
                    Width="310" SelectionMode="Multiple" Enabled="false"></asp:ListBox>
                <asp:HiddenField ID="hdnIndexTermsList" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, Promotions %>"></asp:Localize></label>
            <div class="form-value">
                <asp:CheckBoxList ID="chkFeatures" runat="server" RepeatDirection="Horizontal" Width="320">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, PromoteAsNewItem %>"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, PromoteAsTopSeller %>"></asp:ListItem>
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div class="product-image-column">
        <uc2:ProductSKUImage runat="server" ID="prodSkuImage" />
    </div>
    </div>
    <div id="SKUContainer" class="snap-container clear-fix">
        <ComponentArt:Snap ID="snapSKUInformation" runat="server" Width="100%" CurrentDockingContainer="SKUContainer"
            DockingContainers="SKUContainer" DockingStyle="TransparentRectangle" MustBeDocked="true"
            DraggingMode="FreeStyle" DraggingStyle="SolidOutline" IsCollapsed="true">
            <Header>
                <div class="snap-header clear-fix">
                    <img id="Img1" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, CollpaseSKUInformation %>"
                        src="~/App_Themes/General/images/snap-collapse.png" onclick="snapSKUInformation.toggleExpand();" />
                    <h3><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SKUSpecificInformation %>" /></h3>
                    <asp:LinkButton runat="server" ID="lnkBtnAddSku" Text="Add SKU" OnClick="linBtnAddSku_OnClick"
                        CssClass="addSKULink small-button" />
                    </div>
            </Header>
            <CollapsedHeader>
                <div class="snap-header clear-fix">
                    <img id="skuSnapImg" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, ExpandSKUinformation %>"
                        src="~/App_Themes/General/images/snap-expand.png" onclick="snapSKUInformation.toggleExpand();" />
                    <h3><asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, SKUSpecificInformation %>" /></h3>
                    </div>
            </CollapsedHeader>
            <Content>
                <div class="snap-content clear-fix">
                    <div class="columns" style="width: 47%;">
                        <div class="form-row">
                            <label class="form-label" for="<%=txtSku.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SKU %>" /><span class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtSku" runat="server" Width="350"></asp:TextBox>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="RequiredFieldValidator1"
                                    ValidationGroup="PageLevel" ControlToValidate="txtSku" ErrorMessage="<%$ Resources:GUIStrings, SKUIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="regextxtSku" ValidationGroup="PageLevel"
                                    ControlToValidate="txtSku" ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSKU %>"
                                    Text="*"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator runat="server" ID="regextxtSkuLength" ValidationGroup="PageLevel"
                                    ControlToValidate="txtSku" ValidationExpression="^[\s\S]{0,255}$" ErrorMessage="<%$ Resources:GUIStrings, BundleSkuCannotExceed255Characters %>"
                                    Text="*"></asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="cv_txtSku" runat="server" Display="Dynamic" ControlToValidate="txtSku"
                                    ValidationGroup="PageLevel" OnServerValidate="txtSku_Validate" Text="*" ErrorMessage="<%$ Resources:GUIStrings, SKUAlreadyExist %>" />
                                <asp:Label ID="lblSKUMulti" runat="server" Text="Multi" Visible="false" ForeColor="Red"></asp:Label>
                                <cc1:SKUSelector runat="server" ID="skuSelector" EnableViewState="true" AutoPostbackOnClose="true"
                                    OnOpenCloseClick="skuSelector_OnOpenCloseClick" OnExpandClientFunction="ToggleSnapPosition(\'open\');"
                                    OnCloseClientFunction="ToggleSnapPosition(\'close\');" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtSkuTitle.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtSkuTitle" runat="server" Width="350"></asp:TextBox>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="RequiredFieldValidator2"
                                    ValidationGroup="PageLevel" ControlToValidate="txtSkuTitle" ErrorMessage="<%$ Resources:GUIStrings, SKUTitleIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="regextxtSkuTitle" ValidationGroup="PageLevel"
                                    ControlToValidate="txtSkuTitle" ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSKUTitle %>"
                                    Text="*"></asp:RegularExpressionValidator>
                                <asp:RegularExpressionValidator runat="server" ID="regextxtSkuTitleLength" ValidationGroup="PageLevel"
                                    ControlToValidate="txtSkuTitle" ValidationExpression="^[\s\S]{0,555}$" ErrorMessage="<%$ Resources:GUIStrings, SkuTitleCannotExceed255Characters %>"
                                    Text="*"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtListPrice.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ListPrice %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtListPrice" runat="server" Width="75"></asp:TextBox>
                                <asp:Label ID="lblListPriceMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                    Visible="false" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtListPrice"
                                    ValidationGroup="PageLevel" ControlToValidate="txtListPrice" ErrorMessage="<%$ Resources:GUIStrings, ListPriceIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="rngtxtListPrice"
                                    Type="Currency" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtListPrice"
                                    ErrorMessage="<%$ Resources:GUIStrings, ListPriceMustBeValidCurrency %>" ValidationGroup="PageLevel"
                                    Text="*" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtUnitPrice.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, UnitCost1 %>" /><span
                                    class="req">*</span></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtUnitPrice" runat="server" Width="75"></asp:TextBox>
                                <asp:Label ID="lblUnitPriceMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                    Visible="false" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtUnitPrice"
                                    ValidationGroup="PageLevel" ControlToValidate="txtUnitPrice" ErrorMessage="<%$ Resources:GUIStrings, UnitPriceIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator1"
                                    Type="Currency" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtUnitPrice"
                                    ErrorMessage="<%$ Resources:GUIStrings, UnitPriceMustBeValidCurrency %>" ValidationGroup="PageLevel"
                                    Text="*" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtMeasure.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Measure1 %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtMeasure" runat="server" Text="<%$ Resources:GUIStrings, Each %>"
                                    Width="75"></asp:TextBox>
                                <asp:Label ID="lblMeasureMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                    Visible="false" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtMeasure"
                                    ValidationGroup="PageLevel" ControlToValidate="txtMeasure" ErrorMessage="<%$ Resources:GUIStrings, MeasureIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator runat="server" ID="regextxtMeasure" ValidationGroup="PageLevel"
                                    ControlToValidate="txtMeasure" ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInMeasure %>"
                                    Text="*"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtOrderMin.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderMinimum %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtOrderMin" runat="server" Width="75" Text="1"></asp:TextBox>
                                <asp:Label ID="lblOrderMinMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                    Visible="false" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtOrderMin"
                                    ValidationGroup="PageLevel" ControlToValidate="txtOrderMin" ErrorMessage="<%$ Resources:GUIStrings, OrderMinimumIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator2"
                                    Type="double" MinimumValue="0.0001" MaximumValue="922337203685477.58" ControlToValidate="txtOrderMin"
                                    ErrorMessage="<%$ Resources:GUIStrings, OrderMinimumShouldAValidPositiveNumber %>"
                                    ValidationGroup="PageLevel" Text="*" />
                                <asp:CustomValidator ID="cvtxtOrderMin" runat="server" Display="Dynamic" ControlToValidate="txtOrderMin"
                                    ValidationGroup="PageLevel" OnServerValidate="txtOrderMin_Validate" Text="*"
                                    ErrorMessage="<%$ Resources:GUIStrings, OrderMinimumShouldBeInMultiplesOfOrderIncrement %>" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtOrderIncrement.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderIncrement %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtOrderIncrement" runat="server" Width="75" Text="1"></asp:TextBox>
                                <asp:Label ID="lblOrderIncrementMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                    Visible="false" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtOrderIncrement"
                                    ValidationGroup="PageLevel" ControlToValidate="txtOrderIncrement" ErrorMessage="<%$ Resources:GUIStrings, OrderIncrementIsRequired %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator3"
                                    Type="double" MinimumValue="0.0001" MaximumValue="922337203685477.58" ControlToValidate="txtOrderIncrement"
                                    ErrorMessage="<%$ Resources:GUIStrings, OrderIncrementShouldAValidPositiveNumber %>"
                                    ValidationGroup="PageLevel" Text="*" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label" for="<%=txtHandlingCharges.ClientID%>">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, HandlingCharges %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtHandlingCharges" runat="server" Text="0" Width="75"></asp:TextBox>
                                <asp:Label ID="lblHandlingChargesMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                    Visible="false" ForeColor="Red"></asp:Label>
                                <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="RequiredFieldValidator3"
                                    ValidationGroup="PageLevel" ControlToValidate="txtHandlingCharges" ErrorMessage="<%$ Resources:GUIStrings, HandlingChargeIsRequiredEnter0IfYouNoHandlingCharge %>"
                                    Text="*"></asp:RequiredFieldValidator>
                                <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator4"
                                    Type="Currency" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtHandlingCharges"
                                    ErrorMessage="<%$ Resources:GUIStrings, HandlingChargeMustBeValidCurrency %>"
                                    ValidationGroup="PageLevel" Text="*" />
                            </div>
                        </div>
                        <div runat="server" id="divBackOrderProperties" visible="false">
                            <div class="form-row">
                                <label class="form-label">
                                    &nbsp;</label>
                                <div class="form-value">
                                    <cc2:TriStateCheckBox ID="chkAvailableForBackOrder" runat="server" Text="<%$ Resources:GUIStrings, AvailableForBackOrder %>"
                                        Style="margin-left: 3px;" onclick="updateValidator(this);" CssClass="tristate-checkbox" />
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="form-label" for="<%=txtBackOrderLimit.ClientID%>">
                                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, BackOrderLimit %>" /></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtBackOrderLimit" runat="server" Text="0" Width="75"></asp:TextBox>
                                    <asp:Label ID="lblBackOrderLimitMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                        Visible="false" ForeColor="Red"></asp:Label>
                                    <%--<asp:CompareValidator CultureInvariantValues="true" ControlToValidate="txtBackOrderLimit"
                                    ID="cvtxtBackOrderQuantity" Operator="DataTypeCheck" Type="Integer" ValidationGroup="PageLevel"
                                    runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidIntegerValueForBackOrderQuantity %>"
                                    Text="*" />--%>
                                    <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="rfvtxtBackOrderLimit"
                                        ValidationGroup="PageLevel" ControlToValidate="txtBackOrderLimit" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterABackOrderLimit %>"
                                        Text="*"></asp:RequiredFieldValidator>
                                    <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="rvtxtBackOrderLimit"
                                        Type="double" MinimumValue="1" MaximumValue="922337203685477.58" ControlToValidate="txtBackOrderLimit"
                                        ErrorMessage="<%$ Resources:GUIStrings, BackOrderLimitShoulBeAValidPositiveNumberAndGreaterThanZero %>"
                                        ValidationGroup="PageLevel" Text="*" />
                                </div>
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phDimension" runat="server">
                            <div class="form-row">
                                <label class="form-label" for="<%=txtLength.ClientID%>">
                                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ProductDimensions %>" /><span class="req">*</span></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtLength" runat="server" Width="75" Text="1" ToolTip="<%$ Resources:GUIStrings, Length %>"
                                        Style="margin-right: 5px;" CssClass="textBoxes"></asp:TextBox>
                                    <asp:Label ID="lblLengthMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                        Visible="false" ForeColor="Red"></asp:Label>
                                    <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator5"
                                        Type="double" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtLength"
                                        ErrorMessage="<%$ Resources:GUIStrings, LengthShouldHaveAValidPositiveNumber %>"
                                        ValidationGroup="PageLevel" Text="*" Display="None" />
                                    <asp:TextBox ID="txtWidth" runat="server" Width="75" Text="1" ToolTip="<%$ Resources:GUIStrings, Width %>"
                                        Style="margin-right: 5px;" CssClass="textBoxes"></asp:TextBox>
                                    <asp:Label ID="lblWidthMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                        Visible="false" ForeColor="Red"></asp:Label>
                                    <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator7"
                                        Type="double" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtWidth"
                                        ErrorMessage="<%$ Resources:GUIStrings, WidthShouldHaveAValidPositiveNumber %>"
                                        ValidationGroup="PageLevel" Text="*" Display="None" />
                                    <asp:TextBox ID="txtHeight" runat="server" Width="75" Text="1" ToolTip="<%$ Resources:GUIStrings, Height %>"
                                        Style="margin-right: 5px;" CssClass="textBoxes"></asp:TextBox>
                                    <asp:Label ID="lblHeightMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                        Visible="false" ForeColor="Red"></asp:Label>
                                    <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator6"
                                        Type="double" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtHeight"
                                        ErrorMessage="<%$ Resources:GUIStrings, HeightShouldHaveAValidPositiveNumber %>"
                                        ValidationGroup="PageLevel" Text="*" Display="None" />
                                    <asp:Label ID="lblUnitOfMeasureDimensions" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="form-label" for="<%=txtWeight.ClientID%>">
                                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Weight %>" /></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtWeight" runat="server" Width="75" Text=""></asp:TextBox>
                                    <asp:Label ID="lblWeightMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                        Visible="false" ForeColor="Red"></asp:Label>
                                    <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator8"
                                        Type="double" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtWeight"
                                        ErrorMessage="<%$ Resources:GUIStrings, WeightShouldHaveAValidPositiveNumber %>"
                                        ValidationGroup="PageLevel" Text="*" />
                                    <asp:Label ID="lblUnitOfMeasureWeight" runat="server"></asp:Label>
                                </div>
                            </div>
                            <asp:PlaceHolder runat="server" Visible="false" ID="phMaxDiscount">
                                <div class="form-row">
                                    <label class="form-label" for="<%=txtMaximumDiscountPercentage.ClientID%>">
                                        <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, MaximumDiscountPercentage %>" /></label>
                                    <div class="form-value">
                                        <asp:TextBox ID="txtMaximumDiscountPercentage" runat="server" Width="75" Text=""></asp:TextBox>
                                        <asp:Label ID="lblMaxDiscountMulti" runat="server" Text="<%$ Resources:GUIStrings, Multi %>"
                                            Visible="false" ForeColor="Red"></asp:Label>
                                        <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="RangeValidator9"
                                            Type="double" MinimumValue="0" MaximumValue="100" ControlToValidate="txtMaximumDiscountPercentage"
                                            ErrorMessage="<%$ Resources:GUIStrings, PercentageShouldBeAValueBetween0And100 %>"
                                            ValidationGroup="PageLevel" Text="*" />
                                        <asp:Label ID="Label2" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <div class="form-row">
                                <label class="form-label">
                                    &nbsp;</label>
                                <div class="form-value">
                                    <cc2:TriStateCheckBox ID="chkSelfShippable" runat="server" Text="<%$ Resources:GUIStrings, SelfShippable %>"
                                        Style="margin-left: 3px;" CssClass="tristate-checkbox" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="plchIndexTermSKU" runat="server">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, IndexTerms1 %>" /><br />
                                    <a href="javascript:ShowIndexTermPopup('<%=listAssignedIndexTermsSKU.ClientID%>','SKU');">
                                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AssignTerms %>" /></a>
                                </label>
                                <div class="form-value">
                                    <asp:ListBox ID="listAssignedIndexTermsSKU" CssClass="textBoxes textBoxesWidth" runat="server"
                                        Width="300" SelectionMode="Multiple" Enabled="false" Rows="5"></asp:ListBox>
                                    <asp:HiddenField ID="hdnIndexTermsListSKU" runat="server" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="plchIndexTermSKUMulti" runat="server">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, IndexTerms1 %>" /></label>
                                <div class="form-value">
                                    <asp:Label ID="lblIndexTermSKUMulti" Text="<%$ Resources:GUIStrings, MultiSKUSelected %>"
                                        ForeColor="Red" runat="server"></asp:Label>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <div class="columns" style="width: 50%; margin-left: 30px;">
                        <uc3:DynamicAttributes ID="SKUAttributes1" runat="server" ValidationGroup="PageLevel"
                            ComboSkinID="default" />
                        <div class="sku-properties">
                            <cc2:TriStateCheckBox ID="chkIsActive" runat="server" Text="<%$ Resources:GUIStrings, IsAvailableToSell %>"
                                Style="margin-left: 3px;" CssClass="tristate-checkbox" />
                            <cc2:TriStateCheckBox ID="chkIsOnline" runat="server" Text="<%$ Resources:GUIStrings, IsAvailableToFrontendSiteCustomers %>"
                                Style="margin-left: 3px;" CssClass="tristate-checkbox" />
                            <cc2:TriStateCheckBox ID="chkUnlimitedQuantity" runat="server" Text="<%$ Resources:GUIStrings, UnlimitedQuantity %>"
                                Style="margin-left: 3px;" Enabled='<%# storeWarehouseTypeId != 3 %>' CssClass="tristate-checkbox" />
                            <asp:PlaceHolder ID="phSKUDetails" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </div>
            </Content>
        </ComponentArt:Snap>
    </div>
    <div id="GlobalContainer" class="snap-container clear-fix" style="border-top: 0;">
        <ComponentArt:Snap ID="snapGlobalInfo" runat="server" Width="100%" CurrentDockingContainer="GlobalContainer"
            DockingContainers="GlobalContainer" DockingStyle="TransparentRectangle" MustBeDocked="true"
            DraggingMode="FreeStyle" DraggingStyle="SolidOutline" IsCollapsed="true">
            <Header>
                <div class="snap-header clear-fix">
                    <img id="Img2" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, CollpaseSKUInformation %>"
                        src="../../App_Themes/General/images/snap-collapse.png" onclick="snapGlobalInfo.toggleExpand();" />
                    <h3><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProductInformation %>" /></h3>
                </div>
            </Header>
            <CollapsedHeader>
                <div class="snap-header clear-fix">
                    <img id="Img3" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, ExpandProductInformation %>"
                        src="~/App_Themes/General/images/snap-expand.png" onclick="snapGlobalInfo.toggleExpand();" />
                    <h3><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProductInformation %>" /></h3>
                </div>
            </CollapsedHeader>
            <Content>
                <div class="snap-content clear-fix">
                    <div class="columns" style="width: 47%;">
                        <h4><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AdvancedDetails %>" /></h4>
                        <div class="greyBorderBox scrollingBox">
                            <asp:CheckBox ID="cbIsActive" runat="server" Text="<%$ Resources:GUIStrings, IsAvailableToSell %>" CssClass="advanced-detail" />
                            <asp:CheckBoxList ID="chkAdvanceDetail" runat="server" RepeatDirection="Vertical" CssClass="advanced-detail" />
                        </div>
                    </div>
                    <div class="columns" style="width: 50%; margin-left: 35px;">
                        <uc3:DynamicAttributes ID="ProductLevelAttributes1" runat="server" CssClass="greyBorderBox scrollingBox"
                            ValidationGroup="PageLevel" ComboSkinID="default" />
                    </div>
                </div>
            </Content>
        </ComponentArt:Snap>
       
    </div>
    <asp:ValidationSummary ID="valSum" runat="server" EnableClientScript="true" ShowMessageBox="true"
        ShowSummary="false" ValidationGroup="PageLevel" />
    <asp:HiddenField ID="hfDeleteVal" Value="delete" runat="server" />
    <script type="text/javascript">
        function SetPosition(destObj, srcObj) {
            var objLeft = srcObj.offsetLeft;
            var objTop = srcObj.offsetTop;
            var objParent = srcObj.offsetParent;
            while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
                objLeft += objParent.offsetLeft;
                objTop += objParent.offsetTop;
                objParent = objParent.offsetParent;
            }
            destObj.style.left = (browser == "ie") ? (objLeft + "px") : ((objLeft + 14) + "px");
            destObj.style.top = (browser == "ie") ? ((objTop + 18) + "px") : ((objTop + 25) + "px");
        }
        function ToggleSnapPosition(controlEvent) {
            var snapObject = document.getElementById(snapClientId);
            switch (controlEvent) {
                case 'open':
                    snapObject.style.position = "";
                    break;
                case 'close':
                    snapObject.style.position = "relative";
                    break;
            }
        }
        
    </script>
    <script type="text/javascript">
        function SkuClick(chkSku) {
            alert(chkSku.ID);
        }

        document.onkeypress = stopRKey;
    </script>
</asp:Content>
