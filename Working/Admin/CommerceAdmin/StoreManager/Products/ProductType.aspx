﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsManageProductTypes %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductsMaster.master"
    AutoEventWireup="true" CodeBehind="ProductType.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductType"
    StylesheetTheme="General" %>
<%@ Register TagPrefix="pt" TagName="ProductType" Src="~/UserControls/StoreManager/Products/ProductTypeTree.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    trvProductTypes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CollapseTree %>'/>";
                }
                else if (treeHandle.className == 'collapse') {
                    trvProductTypes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ExpandTree %>'/>";
                }
            }
            return false;
        }
        function tbsAttributes_onTabSelect(sender, eventArgs) {
            tbsAttributes.render();
        }


        function PageLevelChecking(eventObject, menuObject) {
            var currentTreeNode = eventObject.get_node();
            var indexOfSelectedNode = currentTreeNode.get_depth();
            var menuItems = menuObject.get_items();
            var isAnyMenuVisible = false;
            if (indexOfSelectedNode == 0)   // Only Add Context is avialable for the first node
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAddPT') {
                        isAnyMenuVisible = true;
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(true);
                    isAnyMenuVisible = true;
                }
            }
            return isAnyMenuVisible;
        }

        function grdProductAttributes_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            item = eventArgs.get_item();
            productTypeAttributeId = item.getMember(productTypeAttributeIdColumn).get_text();
            var menuItems = mnuAttributes.get_items();
            if (productTypeAttributeId != '' && productTypeAttributeId != emptyGuid) //only add
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                        //
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }
            mnuAttributes.showContextMenuAtEvent(e);
        }

        function SaveRecord() {
        }
        function CancelUpdate() {
        }
        function ShowAttributesPopup(isEdit) {
            //alert(sequence);
            productTypeId = window["SelectedNodeId"];
            popupPath = '../../Popups/StoreManager/AssignAttributeValues.aspx?ProductTypeId=' + productTypeId + '&AttributeId=' + attributeId + '&ProductTypeAttributeId=' + productTypeAttributeId + '&Sequence=' + sequence + '&DisplayPersonalized=true';
            pagepopup = dhtmlmodal.open('ShowAttributeValues', 'iframe', popupPath, '', 'width=562px,height=480px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function () {
                var a = document.getElementById('ShowAttributeValues');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function mnuAttributes_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var contextDataNode = menuItem.get_parentMenu().get_contextData();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        attributeId = emptyGuid;

                        if (grdProductAttributes.Data.length == 1 && (item.getMember('Id').get_value() == null || item.getMember('Id').get_value() == '')) {
                            sequence = grdProductAttributes.get_table().getRowCount();
                        }
                        else {
                            sequence = grdProductAttributes.get_table().getRowCount() + 1;
                        }
                        productTypeAttributeId = emptyGuid;
                        ShowAttributesPopup(0);
                        break;
                    case 'cmEdit':
                        attributeId = item.getMember(attributeIdColumn).get_text();
                        sequence = item.getMember(sequenceColumn).get_text();
                        productTypeAttributeId = item.getMember(productTypeAttributeIdColumn).get_text();
                        ShowAttributesPopup(1);
                        break;
                    case 'cmDelete':
                        var result;
                        result = window.confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisItem %>"/>');
                        if (result == true) {
                            productTypeAttributeId = item.getMember(productTypeAttributeIdColumn).get_text();
                            DeleteProductTypeAttribute(productTypeAttributeId);
                        }
                        if (result != null && result == true) {
                            ProductTypeTreeNodeSelect(null, null); //loading the grid after delete...
                        }
                        //else 
                        //{   
                        //    alert('Error: Delete item action failed. ' + result);
                        //}
                        break;
                }
            }
        }
        var emptyGuid = '00000000-0000-0000-0000-000000000000';
        var productTypeIdColumn = 3;
        var attributeIdColumn = 4;
        var sequenceColumn = 6;
        var productTypeAttributeIdColumn = 7;

        var productTypeId;
        var attributeId;
        var sequence;
        var item;
        var productTypeAttributeId;
        function ResetValues() {
            ProductTypeTreeNodeSelect(null, null);
            tbsAttributes.selectTabById("tabDetails");
            return false;
        }
        function ProductTypeTreeNodeSelect(sender, eventArgs) {

            productTypeId = window["SelectedNodeId"];
            var gridProductAttribute = window["grdProductAttributes"];
            if (gridProductAttribute != null && productTypeId != null && productTypeId != '' && productTypeId != emptyGuid) {
                var strFriendlyName;
                var cmsPageId;
                var isDownloadableMedia = "false";

                var taxCategoryId = '00000000-0000-0000-0000-000000000000';
                if (productTypeId.length == emptyGuid.length) {
                    strFriendlyName = selectedTreeNode.getProperty('FriendlyName');
                    cmsPageId = selectedTreeNode.getProperty('CMSPageId');
                    isDownloadableMedia = selectedTreeNode.getProperty('IsDownloadableMedia');

                    var productTypeDetail = GetProductTypeDetail(productTypeId);
                    var arrName = productTypeDetail.split("@;@");
                    if (arrName.length > 1) {
                        strFriendlyName = arrName[0];
                        cmsPageId = arrName[1];
                        if (arrName.length > 2)
                            isDownloadableMedia = arrName[2];
                        if (arrName.length > 3)
                            taxCategoryId = arrName[3];
                    }
                    //alert(strFriendlyName);

                    document.getElementById(seoNameClientId).value = strFriendlyName;
                    setCMSPageDropDownValue(ddlPageClientId, cmsPageId);
                    setTaxCategoryDropDownValue(ddlTaxCategoryClientId, taxCategoryId);
                    if (isDownloadableMedia == "true")
                        document.getElementById(downloadableMediaClientId).checked = true;
                    else
                        document.getElementById(downloadableMediaClientId).checked = false;

                    tbsAttributes.selectTabById("tabAttributes");

                    var rowitem = gridProductAttribute.get_table().addEmptyRow(0);
                    gridProductAttribute.edit(rowitem);
                    rowitem.SetValue(productTypeIdColumn, productTypeId); // productTypeId column
                    gridProductAttribute.editComplete();
                    gridProductAttribute.callback();
                }
            }
            //LoadProductTypeAttributes(productTypeId);
        }

        function ReLoadGrid() {
            if (pagepopup != null) { pagepopup.hide(); }
            ProductTypeTreeNodeSelect(null, null);   //loading the grid after save...          
        }
        function grdProductAttributes_GridLoad(sender, eventArgs) {
            if (trvProductTypes != null && selectedTreeNode == null) {
                var treeNodes = trvProductTypes.get_nodes();
                var childNodes = treeNodes.getNode(0).get_nodes();
                if (childNodes.get_length() >= 1) {
                    //treeNodes.getNode(0).Expand();
                    var firstNodeId = childNodes.getNode(0).get_id();
                    setTimeout(function () { trvProductTypes.selectNodeById(firstNodeId) }, 1000);
                }
            }
        }
        function setCMSPageDropDownValue(controlName, uniqueValue) {

            var dropDownListControl = document.getElementById(controlName);
            if (dropDownListControl != null && dropDownListControl.length > 0) {
                var selectedItemId = uniqueValue;
                dropDownListControl[0].selected = true;
                if (selectedItemId != null) {
                    for (i = 0; i < dropDownListControl.length; i++) {
                        if (dropDownListControl[i].value == selectedItemId) {
                            dropDownListControl[i].selected = true;
                        }
                    }
                }
            }
        }
        function setTaxCategoryDropDownValue(controlName, uniqueValue) {

            var dropDownListControl = document.getElementById(controlName);
            if (dropDownListControl != null && dropDownListControl.length > 0) {
                var selectedItemId = uniqueValue;
                dropDownListControl[0].selected = true;
                if (selectedItemId != null) {
                    for (i = 0; i < dropDownListControl.length; i++) {
                        if (dropDownListControl[i].value == selectedItemId) {
                            dropDownListControl[i].selected = true;
                        }
                    }
                }
            }
        }
        function ClientSideValidation() {
            var isValid = true;
            var seoName = Trim(document.getElementById(seoNameClientId).value);
            if (seoName == null || seoName == '') {
                alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, SEOFriendlyNameShouldNotBeBlank %>"/>');
                isValid = false;
            }
            else if (seoName.indexOf('/') >= 0) {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsNotAllowedInSEOFriendlyName %>'/>");
                isValid = false;
            }
            else if (seoName.indexOf('\\') >= 0) {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsNotAllowedInSEOFriendlyName1 %>'/>");
                isValid = false;
            }
            else if (!seoName.match(/^[a-zA-Z0-9_\-]+$/)) {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, SEOUrlFriendlyNameCanHaveOnlyAlphanumericCharactersAndPleaseReenter %>'/> \n");
                isValid = false;
            }

            return isValid;
        }     
    </script>

</asp:Content>
<asp:Content ID="productTypeContent" ContentPlaceHolderID="productContentHolder"
    runat="server">
    <div class="product-type">
        <!-- Left Column STARTS -->
        <div class="left-column">
            <div class="tree-header">
                <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
            </div>
            <div class="tree-container">
                <pt:ProductType ID="ctlProductType" runat="server"></pt:ProductType>
            </div>
        </div>
        <!-- Left Column ENDS -->
        <!-- Right Column STARTS -->
        <div class="right-column">
            <div class="tab-nav">
                <ComponentArt:TabStrip ID="tbsAttributes" runat="server" SkinID="Default" MultiPageId="mltpProductTypes">
                    <Tabs>
                        <ComponentArt:TabStripTab ID="tabAttributes" runat="server" Text="<%$ Resources:GUIStrings, Attributes %>">
                        </ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabDetails" runat="server" Text="<%$ Resources:GUIStrings, Details %>">
                        </ComponentArt:TabStripTab>
                    </Tabs>
                    <ClientEvents>
                        <TabSelect EventHandler="tbsAttributes_onTabSelect" />
                    </ClientEvents>
                </ComponentArt:TabStrip>
            </div>
            <ComponentArt:MultiPage ID="mltpProductTypes" runat="server" Width="100%">
                <PageViews>
                    <ComponentArt:PageView ID="pvAttributes" runat="server">
                        <div class="tab-content">
                            <ComponentArt:Menu ID="mnuAttributes" runat="server" SkinID="ContextMenu">
                                <ClientEvents>
                                    <ItemSelect EventHandler="mnuAttributes_onItemSelect" />
                                </ClientEvents>
                            </ComponentArt:Menu>
                            <ComponentArt:Grid SkinID="Default" ID="grdProductAttributes" SliderPopupClientTemplateId="productAttributeSliderTemplate"
                                SliderPopupCachedClientTemplateId="productAttributeSliderTemplateCached" Width="100%"
                                runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
                                AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" PagerInfoClientTemplateId="productAttributePaginationTemplate"
                                CallbackCachingEnabled="true" LoadingPanelClientTemplateId="productAttributeLoadingPanelTemplate">
                                <Levels>
                                    <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                                        DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                                        AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                        SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                                        ShowSelectorCells="false">
                                        <Columns>
                                            <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, GroupName %>"
                                                DataField="GroupTitle" Align="Left" IsSearchable="true" AllowReordering="false"
                                                FixedWidth="true" DataCellClientTemplateId="GroupNameHoverTemplate" />
                                            <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, AttributeName %>"
                                                DataField="AttributeTitle" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                                                AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AttributeNameHoverTemplate" />
                                            <ComponentArt:GridColumn Width="185" HeadingText="<%$ Resources:GUIStrings, Values %>"
                                                DataField="Values" SortedDataCellCssClass="SortedDataCell" Align="Left" HeadingCellCssClass="LastHeadingCell"
                                                DataCellCssClass="LastDataCell" AllowReordering="false" FixedWidth="true" IsSearchable="true"
                                                DataCellClientTemplateId="ValuesHoverTemplate" />
                                            <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, ProductTypeId %>"
                                                DataField="ProductTypeId" Visible="false" />
                                            <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, AttributeId %>"
                                                DataField="AttributeId" Visible="false" />
                                            <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, GroupId %>" DataField="GroupId"
                                                Visible="false" />
                                            <ComponentArt:GridColumn DataField="Sequence" Visible="false" />
                                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                        </Columns>
                                    </ComponentArt:GridLevel>
                                </Levels>
                                <ClientEvents>
                                    <ContextMenu EventHandler="grdProductAttributes_onContextMenu" />
                                    <Load EventHandler="grdProductAttributes_GridLoad" />
                                </ClientEvents>
                                <ClientTemplates>
                                    <ComponentArt:ClientTemplate ID="GroupNameHoverTemplate">
                                        <div title="## DataItem.GetMember('GroupTitle').get_text() ##">
                                            ## DataItem.GetMember('GroupTitle').get_text() == "" ? "&nbsp;" : DataItem.GetMember('GroupTitle').get_text()
                                            ##</div>
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="AttributeNameHoverTemplate">
                                        <div title="## DataItem.GetMember('AttributeTitle').get_text() ##">
                                            ## DataItem.GetMember('AttributeTitle').get_text() == "" ? "&nbsp;" : DataItem.GetMember('AttributeTitle').get_text()
                                            ##</div>
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="ValuesHoverTemplate">
                                        <div title="## DataItem.GetMember('Values').get_text() ##">
                                            ## DataItem.GetMember('Values').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Values').get_text()
                                            ##</div>
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                                        <a href="javascript:SaveRecord();">
                                            <img src="/iapps_images/cm-icon-add.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Save %>"
                                                title="<%$ Resources:GUIStrings, Save %>" /></a> | <a href="javascript:CancelUpdate();">
                                                    <img src="/iapps_images/cm-icon-delete.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Cancel %>"
                                                        title="<%$ Resources:GUIStrings, Cancel %>" /></a>
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="productAttributeSliderTemplate">
                                        <div class="nSliderPopup">
                                            <p>
                                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                                            <p class="npaging">
                                                <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductAttributes.PageCount)
                                                    ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdProductAttributes.RecordCount
                                                        ##</span>
                                            </p>
                                        </div>
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="productAttributeSliderTemplateCached">
                                        <div class="nSliderPopup">
                                            <h5>
                                                ## DataItem.GetMemberAt(0).Value ##</h5>
                                            <p>
                                                ## DataItem.GetMemberAt(1).Value ##</p>
                                            <p class="npaging">
                                                <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductAttributes.PageCount)
                                                    ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdProductAttributes.RecordCount
                                                        ##</span>
                                            </p>
                                        </div>
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="productAttributePaginationTemplate">
                                        ## stringformat(Page0Of1Items, currentPageIndex(grdProductAttributes), pageCount(grdProductAttributes),
                                        grdProductAttributes.RecordCount) ##
                                    </ComponentArt:ClientTemplate>
                                    <ComponentArt:ClientTemplate ID="productAttributeLoadingPanelTemplate">
                                        ## GetGridLoadingPanelContent(grdProductAttributes) ##
                                    </ComponentArt:ClientTemplate>
                                </ClientTemplates>
                            </ComponentArt:Grid>
                        </div>
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pvDetails" runat="server">
                        <div class="tab-content">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SEOFriendlyName %>" /></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtSeoName" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox></div>
                            </div>
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProductDisplayTemplate %>" /></label>
                                <div class="form-value">
                                    <asp:DropDownList ID="ddlPages" runat="server" Width="210" />
                                </div>
                            </div>
                            <asp:PlaceHolder ID="phTaxCategory" runat="server">
                                <div class="form-row">
                                    <label class="form-label">
                                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, DefaultTaxCategory %>" />
                                    </label>
                                    <div class="form-value">
                                        <asp:DropDownList ID="ddlTaxCategory" runat="server" Width="210">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <div class="form-row">
                                <label class="form-label">
                                    &nbsp;
                                </label>
                                <div class="form-value">
                                    <asp:CheckBox ID="chkDownloadableMedia" runat="server" Text="<%$ Resources:GUIStrings, IsDownloadableMedia %>" /></div>
                            </div>
                            <div class="tab-footer">
                                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Reset %>"
                                    ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
                                    OnClientClick="return ResetValues();" />
                                <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                                    ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                                    OnClick="btnSave_Click" OnClientClick="return ClientSideValidation();" />
                            </div>
                        </div>
                    </ComponentArt:PageView>
                </PageViews>
            </ComponentArt:MultiPage>
        </div>
    </div>
</asp:Content>
