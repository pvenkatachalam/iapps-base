﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsManageInventory %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    CodeBehind="InventoryAndRestocking.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Products.InventoryAndRestocking"
    StylesheetTheme="General" %>

<%@ Register Src="~/UserControls/StoreManager/Products/ProductSKURestocking.ascx"
    TagName="Restockings" TagPrefix="cc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var isDirty = false;
        function enableDisableReason(selCntrl) {
            if (selCntrl.selectedIndex == selCntrl.length - 1) {
                document.getElementById('<%=txtOthers.ClientID %>').disabled = false;
                document.getElementById('<%=txtOthers.ClientID %>').className = "textBoxes";
            }
            else {
                document.getElementById('<%=txtOthers.ClientID %>').disabled = true;
                document.getElementById('<%=txtOthers.ClientID %>').className = "textBoxesDisabled";
                document.getElementById('<%=txtOthers.ClientID %>').value = '';
            }
        }

        function enableDisableLowQtyAlert(selCntrl) {
            if (selCntrl.selectedIndex == selCntrl.length - 1) {
                document.getElementById('<%=txtLowQtyAlert.ClientID %>').disabled = false;
                document.getElementById('<%=txtLowQtyAlert.ClientID %>').className = "textBoxes";
            }
            else {
                document.getElementById('<%=txtLowQtyAlert.ClientID %>').disabled = true;
                document.getElementById('<%=txtLowQtyAlert.ClientID %>').className = "textBoxesDisabled";
                document.getElementById('<%=txtLowQtyAlert.ClientID %>').value = document.getElementById('<%=hdnDefaultLowQtyAlert.ClientID %>').value;
            }
        }

        function SumQuantity() {
            isDirty = true;
            var label = document.getElementById('<%=lblTotalInStock.ClientID %>');
            if (label) {
                var sum = 0;
                var obj = $("input[id*=_txtSetQtyHand_]");
                obj.each(function (variable) {
                    sum += parseFloat(obj[variable].value);
                });
                label.innerHTML = sum;
            }
        }

        function togglePanel(checkBox) {
            var enabled = !checkBox.checked;
            var panel = document.getElementById('<%=pnlInventory.ClientID %>');
            if (enabled) {
                panel.style.display = "block";
            }
            else {
                panel.style.display = "none";
            }
        }
        function causeValidation(chk, drpReason) {
            var chkbox = document.getElementById(chk);
            if (chkbox.checked) {
                Page_IsValid = true;
                ValidatorUpdateIsValid();
                if (typeof (ValidatorOnSubmit) == "function")
                    ValidatorOnSubmit();
                //
                ValidatorEnable(document.getElementById('<%=rfvOthers.ClientID  %>'), false);
                ValidatorEnable(document.getElementById('<%=rfvtxtOthers.ClientID  %>'), false);
                ValidatorEnable(document.getElementById('<%=revtxtOthers.ClientID  %>'), false);
                Page_IsValid = true;
                Page_BlockSubmit = false;
                WebForm_OnSubmit();
                return true;
            }
            else {
                if (!isDirty) {
                    //if the value is empty then disable the ddlreason required field validtor
                    ValidatorEnable(document.getElementById('<%=rfvOthers.ClientID  %>'), false);
                }
                else {
                    //enable it
                    ValidatorEnable(document.getElementById('<%=rfvOthers.ClientID  %>'), true);
                    if (document.getElementById('<%=ddlReason.ClientID  %>').value == "0")
                        alert('<%= GUIStrings.PleaseChooseAReason %>');
                }
            }
            var isValid = false;
            if (Page_ClientValidate('Inventory') == true)
                isValid = true;
            var control = document.getElementById(drpReason);
            if (control.selectedIndex == control.length - 1)
                if (Page_ClientValidate('Others') == true && isValid) {
                    // alert('valid');
                    return true;
                }
                else
                    return false;
            return isValid;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <div class="button-row" runat="server" id="divButtons">
        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Reset %>"
            ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClick="btnCancel_Click" CausesValidation="false" />
        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, Save %>" CausesValidation="false" OnClick="btnSave_Click" />
        <div class="clear-fix">
        </div>
    </div>
    <div class="form-row">
        <iapps:CommerceStateControl ID="commerceState" runat="server" />
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SKU %>" /></label>
        <asp:DropDownList runat="server" ID="ddlSkus" OnSelectedIndexChanged="ddlSkus_OnSelectedIndexChanged"
            Width="300" AutoPostBack="true" >
        </asp:DropDownList>
    </div>
    <div class="form-row" runat="server" id="divUnlimited">
        <label class="form-label">
            &nbsp;</label>
        <asp:CheckBox ID="chkUnlimitedQty" runat="server" onclick="javascript:togglePanel(this);"
            Text="<%$ Resources:GUIStrings, UnlimitedQuantity %>" Enabled='<%# storeWarehouseTypeId != 3 %>' />
    </div>
    <asp:Panel runat="server" ID="pnlInventory">
        <asp:PlaceHolder ID="phAlertReason" runat="server">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, LowQuantityAlert %>" /></label>
                <div class="form-value">
                    <asp:DropDownList runat="server" ID="ddlLowQtyAlert" onChange="enableDisableLowQtyAlert(this);"
                        Width="230">
                        <asp:ListItem Text="<%$  Resources:GUIStrings, DefaultSettings %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, CustomValue %>" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:HiddenField ID="hdnDefaultLowQtyAlert" runat="server" Value="3" />
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/App_Themes/General/images/RedInfo.gif"
                        AlternateText="<%$ Resources:GUIStrings, LowInventoryAlert %>" Visible="false" />
                    <asp:TextBox ID="txtLowQtyAlert" runat="server" Width="56" MaxLength="18" Style="margin-left:5px;"></asp:TextBox>
                    <asp:CompareValidator CultureInvariantValues="true" ControlToValidate="txtLowQtyAlert"
                        ID="cvtxtLowQtyAlert" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidIntegerValueForLowQuantityAlert %>"
                        Text="*" Type="Integer" ValidationGroup="Inventory" runat="server" />
                    <asp:Image ID="imgNegativeAlert" runat="server" ImageUrl="~/App_Themes/General/images/RedInfo.gif"
                        AlternateText="<%$ Resources:GUIStrings, LowInventoryAlert %>" Visible="false" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Reason1 %>" /></label>
                <asp:DropDownList ID="ddlReason" runat="server" Width="230" onChange="enableDisableReason(this);">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Others %>" Value="Others"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvOthers" ControlToValidate="ddlReason"
                    ValidationGroup="Inventory" InitialValue="0" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseChooseAReason %>"
                    Text="*">  </asp:RequiredFieldValidator>
                <asp:TextBox ID="txtOthers" runat="server" CssClass="textBoxesDisabled" Width="617"
                    Enabled="false" Style="margin-left:5px;"></asp:TextBox>
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtOthers" ValidationGroup="Others"
                    ControlToValidate="txtOthers" runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAReasonForChaningOrderQuantity %>"
                    Text="*">  </asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revtxtOthers" ValidationGroup="Inventory" runat="server"
                    ControlToValidate="txtOthers" ValidationExpression="^[^<>&]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInReason %>"
                    Text="*"></asp:RegularExpressionValidator>
            </div>
        </asp:PlaceHolder>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="grid-view"
            id="gridview">
            <asp:Repeater runat="server" ID="rptWarehouseInventory" ClientIDMode="Predictable">
                <HeaderTemplate>
                    <thead>
                        <tr>
                            <th>
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Warehouse %>" />
                            </th>
                            <th>
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, InStock %>" /></label>
                            </th>
                            <th>
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, AllocatedQuantity %>" />
                            </th>
                            <th>
                                <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, AvailableToSell %>" />
                            </th>
                            <th>
                                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, BackOrdered %>" />
                            </th>
                        </tr>
                    </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tbody>
                        <tr>
                            <td>
                                <asp:HiddenField runat="server" ID="hdnInventoryId" Value='<%#DataBinder.Eval(Container.DataItem, "Id")%>' />
                                <asp:HiddenField runat="server" ID="hdnWarehouseId" Value='<%#DataBinder.Eval(Container.DataItem, "WarehouseId")%>' />
                                <%#DataBinder.Eval(Container.DataItem, "Warehouse.Title")%>
                            </td>
                            <td>
                                <asp:TextBox ID="txtSetQtyHand" runat="server" CssClass="textBoxes" Width="100" MaxLength="18"
                                    ClientIDMode="Predictable" Text='<%#DataBinder.Eval(Container.DataItem, "Quantity")%>'
                                    onblur="SumQuantity();" Enabled='<%# (int)DataBinder.Eval(Container.DataItem, "Warehouse.WarehouseTypeId")!=3 %>'></asp:TextBox>
                                <asp:CompareValidator CultureInvariantValues="true" ControlToValidate="txtSetQtyHand"
                                    ID="cvtxtSetQtyHand" Operator="DataTypeCheck" Type="Integer" ValidationGroup="Inventory"
                                    runat="server" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidIntegerValueForQuantityOnHand %>"
                                    Text="*" />
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "AllocatedQuantity")%>
                            </td>
                            <td>
                                <%# (Convert.ToDouble(DataBinder.Eval(Container.DataItem, "AvailableToSell")) - Convert.ToDouble(DataBinder.Eval(Container.DataItem, "SKU.BackOrderLimit"))) > 0 ? 
                                        Convert.ToDouble(DataBinder.Eval(Container.DataItem, "AvailableToSell")) - Convert.ToDouble(DataBinder.Eval(Container.DataItem, "SKU.BackOrderLimit")) : 0%>
                            </td>
                            <td>
                                <asp:Literal ID="ltlBackOrderAllocatedQuantity" runat="server"></asp:Literal>
                                <%#  (Convert.ToDouble(DataBinder.Eval(Container.DataItem, "BackOrderAllocatedQuantity"))).ToString()  %>
                            </td>
                        </tr>
                    </tbody>
                </ItemTemplate>
            </asp:Repeater>
            <asp:PlaceHolder runat="server" ID="phTotal">
                <tr>
                    <td>
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Total %>" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTotalInStock"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTotalWaitingtoShip"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTotalAvailableToSell"></asp:Label>
                    </td>
                    <td>
                        <asp:Label runat="server" ID="lblTotalBackOrdered"></asp:Label>
                    </td>
                </tr>
            </asp:PlaceHolder>
        </table>
        <cc1:Restockings runat="Server" ID="ccRestockings" />
    </asp:Panel>
</asp:Content>
