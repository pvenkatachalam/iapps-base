﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsViewAllProducts %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductsMaster.master"
    AutoEventWireup="true" CodeBehind="ViewAllProducts.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewAllProducts"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="pl" TagName="ProductList" Src="~/UserControls/StoreManager/Products/ProductListing.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function trvProductTypes_onNodeSelect(sender, eventArgs) {
            ddlProductType.set_text(eventArgs.get_node().get_text());
            ddlProductType.collapse();
            //Todo:Call a method to do callback or postback
        }
        function toggleDropdownEnabled(objCheckbox) {
            if (objCheckbox.checked) {
                ddlProductType.disable();
            }
            else {
                ddlProductType.enable();
            }
        }    
    </script>
</asp:Content>
<asp:Content ID="AllProductsContent" ContentPlaceHolderID="productContentHolder"
    runat="server">
    <div class="grid-utility">
        <asp:PlaceHolder ID="phDetailedSearch" runat="server" Visible="false">
            <div class="columns">
                <label class="form-label" for="<%=txtProductSearch.ClientID%>">
                    <asp:Literal ID="ltKeyword" runat="server" Text="<%$ Resources:GUIStrings, ProductName %>" /></label>
                <asp:TextBox ID="txtProductSearch" Width="250" runat="server" CssClass="textBoxes"
                    ToolTip="<%$ Resources:GUIStrings, TypeProductSKUTitle %>" />
                <asp:HiddenField runat="server" ID="hdnProductTypeID" />
            </div>
            <asp:PlaceHolder ID="phSKU" runat="server" Visible="false">
                <div class="columns">
                    <label class="form-label" for="<%=txtSkuSearch.ClientID%>">
                        <%= GUIStrings.SKU %></label>
                    <asp:TextBox ID="txtSkuSearch" Width="150" runat="server" CssClass="textBoxes" ToolTip="<%$ Resources:GUIStrings, TypeSKUNumber %>" />
                </div>
            </asp:PlaceHolder>
            <div class="columns">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Type %>" /></label>
                <ComponentArt:ComboBox ID="ddlProductType" runat="server" SkinID="Default" DropDownHeight="297"
                    DropDownWidth="285" AutoHighlight="false" AutoComplete="false" Width="215">
                    <DropDownContent>
                        <ComponentArt:TreeView ID="trvProductTypes" ExpandSinglePath="true" SkinID="Default"
                            OnNodeSelected="trvProductType_onSelect" AutoPostBackOnSelect="false" Height="293"
                            Width="285" CausesValidation="false" runat="server">
                            <ClientEvents>
                                <NodeSelect EventHandler="trvProductTypes_onNodeSelect" />
                            </ClientEvents>
                        </ComponentArt:TreeView>
                    </DropDownContent>
                </ComponentArt:ComboBox>
                <asp:CheckBox ID="chkAllProductTypes" runat="server" Text="<%$ Resources:GUIStrings, SearchAllProductTypes %>" />
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phSimpleSearch" runat="server" Visible="true">
            <div class="columns">
                <label class="form-label" for="<%=txtSearch.ClientID%>">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, KeywordFilter %>" /></label>
                <asp:TextBox ID="txtSearch" Width="350" runat="server" CssClass="textBoxes" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>" />
            </div>
        </asp:PlaceHolder>
        <div class="columns">
            <label class="form-label" style="min-width: 10px;">
                &nbsp;</label>
            <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:GUIStrings, Go %>"
                OnClick="btnSearch_Click" CssClass="small-button" />
            <asp:Button ID="btnClear" runat="server" Text="<%$ Resources:GUIStrings, Clear %>"
                OnClick="btnClear_Click" CssClass="small-button" Visible="false" />
        </div>
        <div class="columns-right">
            <label class="form-label">
                <%= GUIStrings.ViewBy %></label>
            <asp:DropDownList ID="ddlViewBy" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlViewBy_SelectedIndexChanged">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Bundle %>" Value="<%$ Resources:GUIStrings, Bundle %>" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Products %>" Value="<%$ Resources:GUIStrings, Products %>" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, SKUs %>" Value="<%$ Resources:GUIStrings, SKUs %>"
                    Selected="True" />
            </asp:DropDownList>
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <pl:ProductList ID="ctlProductList" runat="server" />
</asp:Content>
