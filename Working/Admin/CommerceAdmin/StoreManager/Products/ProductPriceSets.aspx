﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsPriceSets %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    AutoEventWireup="true" CodeBehind="ProductPriceSets.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductPriceSets"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="ps" TagName="PriceSets" Src="~/UserControls/ContextMenu/PriceSetList.ascx" %>
<%@ Register Assembly="Bridgeline.FW.Commerce.Controls" Namespace="Bridgeline.FW.Commerce.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdProductPriceSet_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            var priceSetListItem = eventArgs.get_item();
            var priceSetTitle = priceSetListItem.getMember(0).get_text();
            menuItems = mnuProductPriceSets.get_items();
            mnuProductPriceSets.showContextMenuAtEvent(e);
        }

        function ContextMenu_SaveClick(selectList) {
            var lstCntrl = document.getElementById(selectList);
            var priceSets = new Array();
            var index = 0;
            for (var i = 0; i < lstCntrl.length; i++) {
                if (lstCntrl[i].selected) {
                    priceSets[index++] = lstCntrl[i].value;
                }
            }
            AddPriceSetToProduct(priceSets);
            grdProductPriceSet.callback();
            mnuProductPriceSets.hide();
            return false;
        }

        function mnuProductPriceSets_onItemSelect(sender, selectedMenuId) {
            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmManagePriceSets':
                        window.location = '../../StoreSetup/PriceSets/ManagePriceSets.aspx';
                        break;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <div class="grid-utility clear-fix">
        <cc1:CommerceStateControl runat="server" ID="commerceState" />
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                style="width: 260px;" />
            <input type="button" class="small-button" value="<%= GUIStrings.Search %>"
                onclick="onClickSearchonGrid('txtSearch', grdProductPriceSet);" />
        </div>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdProductPriceSet" SliderPopupClientTemplateId="ProductListingSliderTemplate"
        SliderPopupCachedClientTemplateId="ProductListingSliderTemplateCached" Width="100%"
        runat="server" RunningMode="Client" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" PagerInfoClientTemplateId="ProductListingPaginationTemplate"
        CallbackCachingEnabled="true" SearchOnKeyPress="true">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id"
                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                ShowTableHeading="false" ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, Name %>"
                        DataField="Title" Align="Left" IsSearchable="true" TextWrap="true"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                    <ComponentArt:GridColumn Width="105" HeadingText="<%$ Resources:GUIStrings, StartDate %>"
                        DataField="StartDate" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="StartDateHoverTemplate" DataType="System.DateTime"
                        FormatString="d" />
                    <ComponentArt:GridColumn Width="105" HeadingText="<%$ Resources:GUIStrings, EndDate %>"
                        DataField="EndDate" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="EndHoverTemplate" DataType="System.DateTime" FormatString="d" />
                    <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="Type" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="TypeHoverTemplate" />
                    <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, IsSale %>"
                        DataField="IsSale" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="IsSaleHoverTemplate" />
                    <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, NumGroups %>"
                        DataField="CustomerGroupCount" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="GroupsHoverTemplate" />
                    <ComponentArt:GridColumn Width="68" HeadingText="<%$ Resources:GUIStrings, NumSKUs %>"
                        DataField="SkuCount" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="SKUsHoverTemplate" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdProductPriceSet_onContextMenu" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StartDateHoverTemplate">
                <span title="## DataItem.GetMember('StartDate').get_text() ##">## DataItem.GetMember('StartDate').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('StartDate').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EndHoverTemplate">
                <span title="## DataItem.GetMember('EndDate').get_text() ##">## DataItem.GetMember('EndDate').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('EndDate').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <span title="## DataItem.GetMember('Type').get_text() ##">## DataItem.GetMember('Type').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Type').get_text() == "1"? "Auto":"Manual"
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="IsSaleHoverTemplate">
                <span title="## DataItem.GetMember('IsSale').get_text() ##">## DataItem.GetMember('IsSale').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('IsSale').get_text() == "true"? "Yes":"No"
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="GroupsHoverTemplate">
                <span title="## DataItem.GetMember('CustomerGroupCount').get_text() ##">## DataItem.GetMember('CustomerGroupCount').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('CustomerGroupCount').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SKUsHoverTemplate">
                <span title="## DataItem.GetMember('SkuCount').get_text() ##">## DataItem.GetMember('SkuCount').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('SkuCount').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" />
                    </p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNavigationCategories.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdNavigationCategories.RecordCount
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##
                    </p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductPriceSet.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdProductPriceSet.RecordCount
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdProductPriceSet), pageCount(grdProductPriceSet),
                grdProductPriceSet.RecordCount) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuProductPriceSets" runat="server" SkinID="ContextMenu" ExpandOnClick="true">
        <ServerTemplates>
            <ComponentArt:NavigationCustomTemplate ID="ExistingPriceSets">
                <Template>
                    <ps:PriceSets ID="ctlPriceSets" runat="server" />
                </Template>
            </ComponentArt:NavigationCustomTemplate>
        </ServerTemplates>
    </ComponentArt:Menu>
</asp:Content>
