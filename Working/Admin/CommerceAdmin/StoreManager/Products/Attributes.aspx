﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true"
    CodeBehind="Attributes.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Attributes" Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsManageProductAttributes %>"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="pa" TagName="ProductAttributes" Src="~/UserControls/StoreManager/Products/AttributeTree.ascx" %>
<%@ Register TagPrefix="fa" TagName="Facet" Src="~/UserControls/StoreManager/Products/Facets.ascx" %>
<asp:Content ID="attributesHead" ContentPlaceHolderID="head" runat="server">
    <!-- Record Count etc.. page related -->
    <script type="text/javascript">
        var IsPersonalized
        var attrType;
        var attrTypeName;
        var isGroupLocal;

        function ToggleTree() {
            var treeHandle = document.getElementById('ToggleHandle');
            if (treeHandle != null) {
                if (treeHandle.className == 'expand') {
                    treeAttributes.expandAll();
                    treeHandle.className = 'collapse';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CollapseTree %>'/>";
                }
                else if (treeHandle.className == 'collapse') {
                    treeAttributes.collapseAll();
                    treeHandle.className = 'expand';
                    treeHandle.innerHTML = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ExpandTree %>'/>";
                }
            }
            return false
        }

        var currentAttributeNode;
        var isCurrentNodeAGroup;
        var isCurrentAttribUnderCustomerGroup = false;
        var isCurrentAttribUnderOrderGroup = false;

        function LoadAttributeDetails(node, attrRenameVal, isGroup) {
            currentAttributeNode = node;
            isCurrentNodeAGroup = isGroup;
            isCurrentAttribUnderCustomerGroup = false;
            isCurrentAttribUnderOrderGroup = false;
            var isFaceted = node.getProperty('IsFaceted');

            var attrValuesCount = node.getProperty('AttributeValuesCount');
            IsPersonalized = node.getProperty('IsPersonalized');

            if (!isGroup) {
                var groupId = node.get_parentNode().get_id().toString().toLowerCase();
                if (groupId == jCustomerAttributeGroupId)
                    isCurrentAttribUnderCustomerGroup = true;
                else if (groupId == jOrderAttributeGroupId)
                    isCurrentAttribUnderOrderGroup = true;
            }

            if (attrRenameVal == null) {
                var attrName = node.get_text();
                attrType = node.getProperty('AttributeType');
                attrTypeName = node.getProperty('AttributeTypeName');
                var attrCode = node.getProperty('AttributeCode');

                var isSerchable = node.getProperty('IsSearchable');

                var isRequired = node.getProperty('IsRequired');
                tbsAttributes.selectTabById("tabAttributeDetail");

                if (attrName != null && attrName != "undefined" && isGroup == false) {
                    document.getElementById("<%=txtAttributeName.ClientID %>").innerHTML = attrName;
                }
                else {
                    document.getElementById("<%=txtAttributeName.ClientID %>").innerHTML = "";
                }
                if (attrType != null && attrType != "undefined") {
                    document.getElementById("<%=ddlAttributeType.ClientID%>").value = attrType;
                }
                else
                    document.getElementById("<%=ddlAttributeType.ClientID%>").value = "String";  //string


                if (attrCode != null && attrCode != "undefined") {
                    document.getElementById("<%=txtAttributeCode.ClientID%>").value = attrCode;
                }
                else {
                    document.getElementById("<%=txtAttributeCode.ClientID%>").value = "";
                }


                document.getElementById("<%=chkFaceted.ClientID %>").checked = (isFaceted == 'True');

                if (isFaceted == 'False' || IsPersonalized == 'True') {
                    tbsAttributes.findTabById("tabAttributeFacets").set_visible(false);
                }
                else if (isFaceted == 'True') {
                    tbsAttributes.findTabById("tabAttributeFacets").set_visible(true);
                }


                if (IsPersonalized == 'True') {
                    document.getElementById("<%=chkPersonalization.ClientID %>").checked = true;
                }
                else
                    document.getElementById("<%=chkPersonalization.ClientID %>").checked = false;


                document.getElementById("<%=chkSearchable.ClientID %>").checked = (isSerchable == 'True');

            }
            else {
                document.getElementById("<%=txtAttributeName.ClientID %>").value = attrRenameVal;
            }


            //System groups can not be editable
            if (node.getProperty('IsSystem') == 'True' || isGroup == true) {
                tbsAttributes.findTabById("tabAttributeFacets").set_visible((isFaceted == 'True'));

                tbsAttributes.findTabById("tabAttributeValue").set_visible(false);
                tbsAttributes.findTabById("tabAttributeValue").set_enabled(false);
                document.getElementById("<%=txtAttributeName.ClientID %>").disabled = true;
                document.getElementById("<%=ddlAttributeType.ClientID%>").disabled = true;
                document.getElementById("<%=txtAttributeCode.ClientID%>").disabled = true;


                document.getElementById("<%=hfDDL.ClientID%>").value = "true";
                document.getElementById("<%=chkFaceted.ClientID %>").disabled = true;
                document.getElementById("<%=chkSearchable.ClientID %>").disabled = true;
                document.getElementById("<%=chkPersonalization.ClientID %>").disabled = true;

                document.getElementById("<%=btnSave.ClientID %>").disabled = true;
                document.getElementById("<%=btnCancel.ClientID %>").disabled = true;
            }
            else {
                tbsAttributes.findTabById("tabAttributeFacets").set_visible(isFaceted == 'True');

                if (isCurrentAttribUnderCustomerGroup || isCurrentAttribUnderOrderGroup) {
                    document.getElementById("<%=chkFaceted.ClientID %>").disabled = true;
                    document.getElementById("<%=chkSearchable.ClientID %>").disabled = true;
                    document.getElementById("<%=chkPersonalization.ClientID %>").disabled = true;
                    tbsAttributes.findTabById("tabAttributeFacets").set_visible(false);

                }
                else {
                    document.getElementById("<%=chkFaceted.ClientID %>").disabled = false;
                    document.getElementById("<%=chkSearchable.ClientID %>").disabled = false;
                    document.getElementById("<%=chkPersonalization.ClientID %>").disabled = false;
                    ToggleOtherCheckBoxes();

                }

                tbsAttributes.findTabById("tabAttributeValue").set_visible(true);
                tbsAttributes.findTabById("tabAttributeValue").set_enabled(true);
                document.getElementById("<%=txtAttributeName.ClientID %>").disabled = false;

                if (IsAttributeDataTypeChangeable(SelectedNodeId) != 'True') {
                    document.getElementById("<%=ddlAttributeType.ClientID%>").disabled = true;
                    document.getElementById("<%=hfDDL.ClientID%>").value = "true";
                }
                else {
                    document.getElementById("<%=ddlAttributeType.ClientID%>").disabled = false;
                    document.getElementById("<%=hfDDL.ClientID%>").value = "false";
                }

                document.getElementById("<%=txtAttributeCode.ClientID%>").disabled = false;
                document.getElementById("<%=btnSave.ClientID %>").disabled = false;
                document.getElementById("<%=btnCancel.ClientID %>").disabled = false;
            }

            toggleAppropriateCheckBoxes(false, false);
        }

        function toggleAppropriateCheckBoxes(overrideCurrentSettingIfDefault, resetValues) {
            var ddlAttributeType = document.getElementById("<%=ddlAttributeType.ClientID%>");
            var chkFaceted = document.getElementById("<%=chkFaceted.ClientID %>");
            var chkSearchable = document.getElementById("<%=chkSearchable.ClientID %>");
            var chkPersonalization = document.getElementById("<%=chkPersonalization.ClientID %>");
            if (ddlAttributeType.selectedIndex >= 0) {
                var selectedAttribute = ddlAttributeType.options[ddlAttributeType.selectedIndex].text;
                if (!isCurrentAttribUnderOrderGroup) {
                    if (selectedAttribute == 'HTML' || selectedAttribute == 'File Upload'
                || selectedAttribute == 'Asset Library File' || selectedAttribute == 'Content Library'
                || selectedAttribute == 'Image Library' || selectedAttribute == 'Page Library') {
                        chkFaceted.disabled = true;
                        chkSearchable.disabled = true;

                        chkFaceted.checked = false;
                        chkSearchable.checked = false;

                        tbsAttributes.findTabById("tabAttributeValue").set_visible(false);
                        tbsAttributes.findTabById("tabAttributeFacets").set_visible(false);

                        if (selectedAttribute == 'Asset Library File' || selectedAttribute == 'Content Library'
                        || selectedAttribute == 'Image Library' || selectedAttribute == 'Page Library') {
                            chkPersonalization.disabled = true;
                            chkPersonalization.checked = false;
                        }
                        else {
                            chkPersonalization.disabled = false;
                        }
                    }
                    else if (overrideCurrentSettingIfDefault) {
                        chkFaceted.disabled = false;
                        chkSearchable.disabled = false;

                        if (resetValues) {
                            chkFaceted.checked = true;
                            chkSearchable.checked = true;
                            chkPersonalization.checked = false;
                        }

                        tbsAttributes.findTabById("tabAttributeValue").set_visible(true);
                        tbsAttributes.findTabById("tabAttributeFacets").set_visible(chkFaceted.checked);
                    }
                }
            }
        }

        function ddlAttributeType_onChange() {

            //check if the attribute is of type HTML then disable all the check boxes.
            toggleAppropriateCheckBoxes(true, true);
        }

    </script>
    <!-- Tree related code -->
    <script type="text/javascript">
        //Tree related
        function AttributeTreeNodeSelect(isGroup) {
            document.getElementById('<%=ctlProductAttributes.SelectedNodeField.ClientID%>').value = selectedTreeNodeId;
            LoadAttributeDetails(selectedTreeNode, null, isGroup);
            isGroupLocal = isGroup;
            grdAttrValuesCount = null;
        }

        function ValidateForDuplicateAttributes(sender, args) {
            var newAttrName = args.Value;
            var returnval = CheckForDuplicate(selectedTreeNode, Trim(newAttrName), false)
            args.IsValid = returnval;
        }
    </script>
    <!--  scripts for Attribute value grid  -->
    <script type="text/javascript">
        //scripts for Attribute value grid

        //Variable declaration for attribute values

        var sequenceColumn = 0;
        var nameColumn = 1;
        var codeColumn = 2;
        var numericValueColumn = 3;
        var actionColumn = 5;
        var IdColumn = 6;
        var attributeIdColumn = 7;
        var selectedAction;
        var grdAttrValuesCount = null;

        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdAttributeValue;
                menuObject = mnuAttributeValue;
                gridObjectName = "grdAttributeValue";
                uniqueIdColumn = 6;
                operationColumn = 4;
            }, 500);
        }

        function PageLevelCallbackComplete() {
            if (selectedTreeNode != null) {
                var gridc = grdAttributeValue.get_table().get_columns();
                if (selectedTreeNode.getProperty('AttributeType') == "String") {
                    gridc[numericValueColumn].set_visible(true);
                    //gridc[numericValueColumn].set_width(118);
                }
                else {
                    gridc[numericValueColumn].set_visible(false);
                    //gridc[numericValueColumn].set_width(0);
                    //gridc[numericValueColumn - 1].set_width(315);
                }
                grdAttributeValue.render();
            }

            //Required values count to disable attribute datatype in detail screen 
            grdAttrValuesCount = grdAttributeValue.get_recordCount();
            if (grdAttrValuesCount == 1) {
                if (grdAttributeValue.get_table().getRow(0).getMember('Id').get_text() == '')
                    grdAttrValuesCount = 0;
            }

        }

        function PageLevelOnContextMenu(menuItems) {
            return true;
        }

        function PageLevelOnItemSelect(selectedMenuId, currentGridItem, sender, eventArgs) {
            if (selectedMenuId == 'cmDefault') {
                SetAsDefault(currentGridItem);
            }
        }

        function PageLevelAddItem(currentGridItem) {
            return true;
        }

        function PageLevelEditItem(currentGridItem) {
            return true;
        }

        function PageLevelDeleteItem(currentGridItem) {
            var isPageLevelDelete = DeleteAttributeValueCheck(currentGridItem.getMember('Id').get_value());
            return isPageLevelDelete;
        }

        function SetAsDefault(currentGridItem) {
            var isUpdated = 'false';
            var uniqueId = currentGridItem.getMember('Id').get_value();
            isUpdated = SetAttributeValueAsDefault(uniqueId);
            if (isUpdated = 'true') {
                LoadGrid('');
            }
        }

        function PageLevelSaveRecord() {
            return true;
        }

        function PageLevelCancelEdit() {
            return true;
        }

        function PageLevelGridValidation(currentGridItem) {
            return ValidateAttributeValueGrid(currentGridItem);
        }

        function PageLevelDragAndDrop(draggedItem, targetItem, fromId, toPosition) {
            var isMoved = DragAndDropAttributeValue(fromId, toPosition);
            return isMoved;
        }

        function ValidateAttributeValueGrid(currentGridItem) {
            var isValid = true;
            var attrType = selectedTreeNode.getProperty('AttributeTypeName');
            var attrVal = Trim(document.getElementById(txtName).value);
            var attrCode = Trim(document.getElementById(txtCode).value);
            var msg = "";
            var dateValidatorControl = document.getElementById('<%=valDateFormat.ClientID %>');
            var intValidatorControl = document.getElementById('<%=valIntegerFormat.ClientID %>');
            var doubleValidatorControl = document.getElementById('<%=valDoubleFormat.ClientID %>');

            var dummyTextBox = document.getElementById('<%=dummyTxtAttribValue.ClientID %>');

            dummyTextBox.value = attrVal; // for the purpose of validation


            if (Trim(attrVal) == "") {
                msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAAttributeValueName %>'/> \n";
            }
            else {
                if (attrType == "Int32") {
                    ValidatorEnable(intValidatorControl, true);
                    if (!intValidatorControl.isvalid) {
                        msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, NameCanHaveOnlyIntegerValueAsTheAttributeDatatypeIsInteger %>'/> \n";
                    }
                }
                else if (attrType == "Boolean") {
                    if (attrVal.toLowerCase() != 'false' && attrVal.toLowerCase() != 'true') {
                        msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, NameCanHaveOnlyTrueOrFalseAsTheAttributeDatatypeIsBoolean %>'/> \n";
                    }
                }
                else if (attrType == "DateTime") {
                    ValidatorEnable(dateValidatorControl, true);
                    if (!dateValidatorControl.isvalid) {
                        msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, AttributeDatatypeIsDateTimePleaseEnterValidDateAsMonthDayAndFourDigitYear %>'/> \n";
                    }
                }
                else if (attrType == "Double") {
                    ValidatorEnable(doubleValidatorControl, true);
                    if (!doubleValidatorControl.isvalid) {
                        msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, NameCanHaveOnlyDecimalValueAsTheAttributeDatatypeIsDouble %>'/> \n";
                    }
                }

                if (attrVal.indexOf('&#0;') >= 0) {
                    msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacterItIsNotAllowedPleaseReenter %>'/> \n";
                }
            }
            if (attrCode.indexOf('&#0;') >= 0) {
                msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacterItIsNotAllowedPleaseReenter %>'/> \n";
            }

            var numericVal = Trim(document.getElementById(txtNumericValue).value);
            if (numericVal != "") {
                if (!numericVal.match(/^[0-9]+$/)) {
                    msg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, NumericalsCanHaveOnlyNumericCharacters %>'/> \n";
                }
            }
            if (Trim(document.getElementById(txtName).value) != "") {
                msg += CheckDuplicateAttribute(currentGridItem, Trim(document.getElementById(txtName).value));
            }

            ValidatorEnable(intValidatorControl, false);
            ValidatorEnable(dateValidatorControl, false);
            ValidatorEnable(doubleValidatorControl, false);

            if (msg != "") {
                alert(msg);
                isValid = false;
            }
            else {
                isValid = true;
            }
            return isValid;
        }

        function CheckDuplicateAttribute(currentGridItem, attributeName) {
            var isDuplicate;

            if (gridOperation == 'Insert')
                isDuplicate = DuplicateAttributeValueCheck(selectedTreeNodeId, '', attributeName);
            else if (gridOperation == 'Edit')
                isDuplicate = DuplicateAttributeValueCheck(selectedTreeNodeId, currentGridItem.getMember('Id').get_text(), attributeName);


            if (isDuplicate == 'true') {
                errorMsg = '<asp:localize runat="server" text="<%$ Resources:GUIStrings, AttributeNameCannotHaveDuplicateValues %>"/>';
            }
            else {
                errorMsg = "";
            }
            return errorMsg;
        }


        function getOperationValue(control) {
            //we are not using this function now.
            return [selectedAction, selectedAction];
        }
        
    </script>
    <!--Tab selection code -->
    <script type="text/javascript">
        //Tab selection code
        function tbsAttributes_onBeforeSelect(sender, eventArgs) {
            var tabId = eventArgs.get_tab().get_id();
            if (tabId == 'tabAttributeFacets') {
                // Call the grid load function for facet.
                if (typeof (SelectedNodeId) != 'undefined')
                    loadFacetGrid(SelectedNodeId);
            }
            else if (tabId == "tabAttributeValue") {
            }
        }

        function tbsAttributes_onTabSelect(sender, eventArgs) {
            var tabId = eventArgs.get_tab().get_id();
            if (tabId == "tabAttributeValue") {
                LoadGrid('');
            }
            else if (tabId == "tabAttributeDetail") {
                LoadAttributeDetails(selectedTreeNode, null, isGroupLocal);
            }
        }

        function ToggleOtherCheckBoxes() {
            if (document.getElementById("<%=chkPersonalization.ClientID %>").checked == true) {
                document.getElementById("<%=chkFaceted.ClientID %>").disabled = true;
                document.getElementById("<%=chkSearchable.ClientID %>").disabled = true;
                document.getElementById("<%=chkFaceted.ClientID %>").checked = false;
                document.getElementById("<%=chkSearchable.ClientID %>").checked = false;
                tbsAttributes.findTabById("tabAttributeFacets").set_visible(false);
            } else {
                /*document.getElementById("<%=chkFaceted.ClientID %>").disabled = false;
                document.getElementById("<%=chkSearchable.ClientID %>").disabled = false;
                tbsAttributes.findTabById("tabAttributeFacets").set_visible(true);*/
                toggleAppropriateCheckBoxes(true, false);
            }

        }
        function IsAttributeSelected() {
            if (typeof (SelectedNodeId) != 'undefined') {
                return true;
            }
            else {
                var msg = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseSelectAnAttributeToSave %>'/> \n"
                alert(msg);
                return false;
            }

        }
    </script>
    <link href="../../css/Products.css" rel="Stylesheet" media="all" type="text/css" />
</asp:Content>
<asp:Content ID="attributesContent" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <div class="attributes">
        <h1>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Attributes %>" /></h1>
        <!-- Left Column STARTS -->
        <div class="left-column">
            <div class="tree-header">
                <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
            </div>
            <div class="tree-container">
                <pa:ProductAttributes ID="ctlProductAttributes" runat="server"></pa:ProductAttributes>
            </div>
        </div>
        <!-- Left Column ENDS -->
        <!-- Right Column STARTS -->
        <div class="right-column">
            <ComponentArt:TabStrip ID="tbsAttributes" runat="server" SkinID="Default" MultiPageId="mltpAttributes">
                <Tabs>
                    <ComponentArt:TabStripTab ID="tabAttributeDetail" runat="server" Text="<%$ Resources:GUIStrings, Details %>"
                        DefaultSubGroupTabSpacing="10">
                    </ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab ID="tabAttributeValue" runat="server" Text="<%$ Resources:GUIStrings, Values %>">
                    </ComponentArt:TabStripTab>
                    <ComponentArt:TabStripTab ID="tabAttributeFacets" runat="server" Text="<%$ Resources:GUIStrings, Facets %>">
                    </ComponentArt:TabStripTab>
                </Tabs>
                <ClientEvents>
                    <TabBeforeSelect EventHandler="tbsAttributes_onBeforeSelect" />
                    <TabSelect EventHandler="tbsAttributes_onTabSelect" />
                </ClientEvents>
            </ComponentArt:TabStrip>
            <ComponentArt:MultiPage ID="mltpAttributes" runat="server" CssClass="MultiPage">
                <ComponentArt:PageView ID="pvAttributeDetail" runat="server">
                    <div class="tab-content">
                        <div class="form-row">
                            <label for="<%=txtAttributeName.ClientID%>" class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Name1 %>" /></label>
                            <div class="form-value">
                                <asp:Label ID="txtAttributeName" runat="server"></asp:Label></div>
                        </div>
                        <div class="form-row">
                            <label for="<%=ddlAttributeType.ClientID%>" class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Type1 %>" /></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlAttributeType" runat="server" Width="208" Enabled="false"
                                    EnableViewState="true">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-row">
                            <label for="<%=txtAttributeCode.ClientID%>" class="form-label">
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Code1 %>" /></label>
                            <div class="form-value">
                                <asp:TextBox ID="txtAttributeCode" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox></div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                &nbsp;</label>
                            <div class="form-value">
                                <asp:CheckBox ID="chkFaceted" runat="server" Text="<%$ Resources:GUIStrings, Faceted %>" /></div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                &nbsp;</label>
                            <div class="form-value">
                                <asp:CheckBox ID="chkSearchable" runat="server" Text="<%$ Resources:GUIStrings, Searchable %>" /></div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                &nbsp;</label>
                            <div class="form-value">
                                <asp:CheckBox ID="chkPersonalization" onclick="ToggleOtherCheckBoxes();" runat="server"
                                    Text="<%$ Resources:GUIStrings, Personalization %>" />
                            </div>
                        </div>
                        <asp:HiddenField ID="hfDDL" runat="server" />
                    </div>
                    <div class="tab-footer">
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
                            ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClick="btnCancel_OnClick" />
                        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
                            ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return IsAttributeSelected();"
                            OnClick="btnSave_OnClick" ValidationGroup="AttributeDetails" CausesValidation="true" />
                        <asp:ValidationSummary ShowSummary="false" ID="valSummary" runat="server" EnableClientScript="true"
                            ValidationGroup="AttributeDetails" ShowMessageBox="true" />
                    </div>
                </ComponentArt:PageView>
                <ComponentArt:PageView ID="pvAttributeValues" runat="server">
                    <div class="tab-content">
                        <asp:CheckBox ID="chkDisplayOrder" runat="server" Text="<%$ Resources:GUIStrings, EnableWebsiteDisplaySorting %>"
                            Style="float: right; margin: 5px; clear: both;" onclick="RefreshGridForDisplayOrder(this);" />
                        <ComponentArt:Menu ID="mnuAttributeValue" runat="server" SkinID="ContextMenu" Width="120">
                            <ClientEvents>
                                <ItemSelect EventHandler="menu_onItemSelect" />
                            </ClientEvents>
                        </ComponentArt:Menu>
                        <ComponentArt:Grid SliderPopupClientTemplateId="attributeValueSliderTemplate" ID="grdAttributeValue"
                            SkinID="Default" runat="server" RunningMode="Callback" PagerInfoClientTemplateId="attributeValuePaginationTemplate"
                            AllowEditing="true" AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="attributeValueSliderTemplateCached"
                            CallbackCachingEnabled="false" AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10"
                            PageSize="10" AllowPaging="True" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="dragTemplate"
                            ExternalDropTargets="grdAttributeValue" AllowMultipleSelect="false" Width="100%"
                            LoadingPanelClientTemplateId="attributeValueLoadingPanelTemplate">
                            <ClientEvents>
                                <ContextMenu EventHandler="grid_onContextMenu" />
                                <ItemExternalDrop EventHandler="grid_OnExternalDrop" />
                                <SortChange EventHandler="grid_OnSortChange" />
                                <CallbackComplete EventHandler="grid_onCallbackComplete" />
                                <Load EventHandler="grid_onLoad" />
                            </ClientEvents>
                            <Levels>
                                <ComponentArt:GridLevel DataKeyField="Sequence" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                    SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                                    ShowSelectorCells="false">
                                    <ConditionalFormats>
                                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('IsDefault').get_text()=='true'"
                                            RowCssClass="default-row" SelectedRowCssClass="selected-row" />
                                    </ConditionalFormats>
                                    <Columns>
                                        <ComponentArt:GridColumn DataField="Sequence" Visible="false" />
                                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                                            EditControlType="Custom" EditCellServerTemplateId="svtName" HeadingText="<%$ Resources:GUIStrings, Name %>"
                                            DataField="Value" Align="left" DataCellClientTemplateId="NameHoverTemplate" Width="260" />
                                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                                            HeadingText="<%$ Resources:GUIStrings, Code %>" DataField="Code" Align="left"
                                            EditControlType="Custom" EditCellServerTemplateId="svtCode" DataCellClientTemplateId="CodeHoverTemplate"
                                            Width="140" />
                                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                                            HeadingText="<%$ Resources:GUIStrings, Numerical %>" DataField="NumericValue"
                                            Align="left" EditControlType="Custom" EditCellServerTemplateId="svtNumericValue"
                                            DataCellClientTemplateId="NumericalHoverTemplate" Width="50" Visible="false" />
                                        <ComponentArt:GridColumn IsSearchable="false" Visible="false" EditControlType="Custom"
                                            DataField="Operation" EditCellServerTemplateId="svtOperation" />
                                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                                            AllowSorting="False" HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left"
                                            EditCellServerTemplateId="svtActions" EditControlType="Custom" Width="50" DataCellCssClass="last-data-cell"
                                            HeadingCellCssClass="last-heading-cell" />
                                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                        <ComponentArt:GridColumn DataField="AttributeID" Visible="false" />
                                        <ComponentArt:GridColumn DataField="IsDefault" Visible="false" />
                                    </Columns>
                                </ComponentArt:GridLevel>
                            </Levels>
                            <ClientTemplates>
                                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                                    <div title="## DataItem.GetMember('Value').get_text() ##">
                                        ## DataItem.GetMember('Value').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Value').get_text()
                                        ##</div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="CodeHoverTemplate">
                                    <div title="## DataItem.GetMember('Code').get_text() ##">
                                        ## DataItem.GetMember('Code').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Code').get_text()
                                        ##</div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="NumericalHoverTemplate">
                                    <div title="## DataItem.GetMember('NumericValue').get_text() ##">
                                        ## DataItem.GetMember('NumericValue').get_text() == "" ? "&nbsp;" : DataItem.GetMember('NumericValue').get_text()
                                        ##</div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="dragTemplate">
                                    <div style="height: 100px; width: 100px;">
                                        ## DataItem.getMember('Value').get_text() ##</div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="attributeValueSliderTemplate">
                                    <div class="SliderPopup">
                                        <p>
                                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                                        <p class="paging">
                                            <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdAttributeValue.PageCount)
                                                ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdAttributeValue.RecordCount)
                                                    ##</span>
                                        </p>
                                    </div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="attributeValueSliderTemplateCached">
                                    <div class="SliderPopup">
                                        <h5>
                                            ## DataItem.GetMemberAt(0).Value ##</h5>
                                        <p>
                                            ## DataItem.GetMemberAt(1).Value ##</p>
                                        <p class="paging">
                                            <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdAttributeValue.PageCount)
                                                ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdAttributeValue.RecordCount)
                                                    ##</span>
                                        </p>
                                    </div>
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="attributeValuePaginationTemplate">
                                    ## stringformat(Page0Of1Items, currentPageIndex(grdAttributeValue), pageCount(grdAttributeValue),
                                    grdAttributeValue.RecordCount) ##
                                </ComponentArt:ClientTemplate>
                                <ComponentArt:ClientTemplate ID="attributeValueLoadingPanelTemplate">
                                    ## GetGridLoadingPanelContent(grdAttributeValue) ##
                                </ComponentArt:ClientTemplate>
                            </ClientTemplates>
                            <ServerTemplates>
                                <ComponentArt:GridServerTemplate ID="svtName">
                                    <Template>
                                        <asp:TextBox CssClass="textBoxes" ID="txtName" runat="server" Width="250">
                                        </asp:TextBox>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                                <ComponentArt:GridServerTemplate ID="svtCode">
                                    <Template>
                                        <asp:TextBox CssClass="textBoxes" ID="txtCode" runat="server" Width="130">
                                        </asp:TextBox>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                                <ComponentArt:GridServerTemplate ID="svtNumericValue">
                                    <Template>
                                        <asp:TextBox CssClass="textBoxes" ID="txtNumericValue" runat="server" Width="120">
                                        </asp:TextBox>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                                <ComponentArt:GridServerTemplate ID="svtOperation">
                                    <Template>
                                        <asp:TextBox CssClass="textBoxes" ID="txtOperation" runat="server">
                                        </asp:TextBox>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                                <ComponentArt:GridServerTemplate ID="svtActions">
                                    <Template>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                                        runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11"
                                                        onclick="gridObject_BeforeUpdate();return false;" />
                                                </td>
                                                <td>
                                                    <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                                        src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked();" />
                                                </td>
                                            </tr>
                                        </table>
                                    </Template>
                                </ComponentArt:GridServerTemplate>
                            </ServerTemplates>
                        </ComponentArt:Grid>
                        <asp:TextBox CssClass="textBoxes" ID="dummyTxtAttribValue" runat="server" Style="display: none">
                        </asp:TextBox>
                        <asp:CompareValidator ID="valDateFormat" runat="server" ControlToValidate="dummyTxtAttribValue"
                            Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ErrorMessage="*"
                            Display="None" Text="*" Enabled="false">                                                 
                        </asp:CompareValidator>
                        <asp:CompareValidator ID="valDoubleFormat" runat="server" ControlToValidate="dummyTxtAttribValue"
                            Operator="DataTypeCheck" Type="Double" CultureInvariantValues="true" ErrorMessage="*"
                            Display="None" Text="*" Enabled="false">                                                 
                        </asp:CompareValidator>
                        <asp:CompareValidator ID="valIntegerFormat" runat="server" ControlToValidate="dummyTxtAttribValue"
                            Operator="DataTypeCheck" Type="Integer" CultureInvariantValues="true" ErrorMessage="*"
                            Display="None" Text="*" Enabled="false">                                                 
                        </asp:CompareValidator>
                    </div>
                </ComponentArt:PageView>
                <ComponentArt:PageView ID="pvAttributeFacets" runat="server">
                    <div class="tab-content">
                        <fa:Facet ID="ucFacet" runat="server" />
                    </div>
                </ComponentArt:PageView>
            </ComponentArt:MultiPage>
        </div>
        <!-- Right Column ENDS -->
        <div class="clear-fix">
        </div>
    </div>
</asp:Content>
