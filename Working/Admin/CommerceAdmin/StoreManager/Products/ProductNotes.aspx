﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductNotes.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Products.ProductNotes" 
MasterPageFile="~/StoreManager/Products/ProductDetails.master" StylesheetTheme="General" %>
<%@ Register src="~/UserControls/General/Notes.ascx" tagname="Notes" tagprefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <uc:Notes ID="ctlNotes" runat="server"  />
    <iapps:CommerceStateControl ID="commerceState" runat="server"></iapps:CommerceStateControl>
</asp:Content>