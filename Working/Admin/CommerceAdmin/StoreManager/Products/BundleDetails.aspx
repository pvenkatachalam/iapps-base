﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsBundleDetails %>"
    Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    CodeBehind="BundleDetails.aspx.cs" AutoEventWireup="true" ValidateRequest="false"
    EnableEventValidation="false" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.BundleDetails" StylesheetTheme="General" %>

<%@ Register Src="~/UserControls/General/HtmlEditor.ascx" TagName="HtmlEditor" TagPrefix="uc2" %>
<%@ Register Assembly="Bridgeline.FW.Commerce.Controls" Namespace="Bridgeline.FW.Commerce.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="~/UserControls/StoreManager/Products/ProductSKUImage.ascx" TagName="ProductSKUImage"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/StoreManager/Products/ProductLevelAttributes.ascx"
    TagName="ProductLevelAttributes" TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/StoreManager/Products/NewDynamicAttribute.ascx"
    TagName="DynamicAttributes" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager1" />
    <script type="text/javascript">
        //<![CDATA[
        function trvProductTypes_onNodeSelect(sender, eventArgs) {
            ddlProductType.set_text(eventArgs.get_node().get_text());
            ddlProductType.collapse();
            //Todo:Call a method to do callback or postback
        }

        function ShowAttributesPopup(isSku) {
            popupPath = '../../Popups/StoreManager/AddProductAttribute.aspx?isSku=' + isSku;
            pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=562px,height=480px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function () {
                var a = document.getElementById('ShowAttributes');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }


        function reloadSKUAttribute(attributeArray) {
            cbSKUAttributes.callback(attributeArray);
        }

        function reloadProductAttribute(attributeArray) {
            cbProductAttributes.callback(attributeArray);
        }

        function updateMinPrice(sender, eventargs) {
            cbPrices.callback();
        }

        function roundNumber(num, dec) {
            var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
            // Math.round( Math.round( num * Math.pow( 10, dec + 1 ) ) / Math.pow( 10, 1 ) ) / Math.pow(10,dec);
            return result;
        }

        function forceTwoDecimalPlaces(num) {
            var snum = num.toString();
            var numParts = snum.split(".");
            if (numParts.length > 1) {
                if (numParts[1].length == 1) {
                    return num += "0";
                }
            }
            else {
                return num += ".00";
            }
        }

        function updatePrice(sender, eventargs) {
            var minPrice = document.getElementById(lblMinPriceCtrl).innerHTML;
            minPrice = parseFloat(minPrice);
            var bundlePrice = document.getElementById(txtBundlePriceCtrl).value;
            document.getElementById(lblSavingsCtrl).innerHTML = roundNumber(minPrice - bundlePrice, 2);
        }

        function grdBundleItems_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            var menuItems = mnuBundleItems.get_items();
            gridItem = eventArgs.get_item();


            var prodId = gridItem.getMember('Id').get_text();
            if (prodId != '' && prodId != emptyGuid) {
                for (i = 0; i < menuItems.get_length() ; i++) {
                    menuItems.getItem(i).set_visible(true);
                }
            }
            else  //only add
            {
                for (i = 0; i < menuItems.get_length() ; i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                        menuItems.getItem(i).set_visible(true);
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }

            mnuBundleItems.showContextMenuAtEvent(e);
        }

        function mnuBundleItems_onItemSelect(sender, eventArgs) {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        if (window.bundleProdId) {
                            ShowAddProductPopup();
                            break;
                        }
                        else {
                            alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, BundleDetailsMustBeSavedBeforeProductsCanBeAdded %>"/>');
                            break;
                        }
                    case 'cmEdit':
                        grdBundleProducts.edit(gridItem);
                        break;
                    case 'cmDelete':
                        if (window.confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, AreYouSureYouWantToRemoveThisItem %>"/>'))
                            RemoveProduct();
                        break;
                }
            }
        }

        function SaveRecord() {
            var oldQty = gridItem.getMember(3).get_value();
            grdBundleProducts.editComplete();
            var currentQty = gridItem.getMember(3).get_value();
            var fltCurrentQty = parseFloat(currentQty);
            var skuOrderIncrement = parseFloat(gridItem.getMember(9).get_value());
            var skuOrderMinimum = parseFloat(gridItem.getMember(10).get_value());

            //        if(fltCurrentQty < skuOrderMinimum)
            //        {
            //            alert('Quantity must be greater than ' + skuOrderMinimum);
            //            grdBundleProducts.edit(gridItem);
            //            gridItem.SetValue(3, oldQty); 
            //            return;       
            //        }
            //        else{
            //            var testDiv = String(fltCurrentQty / skuOrderIncrement);
            //            if (testDiv.indexOf('.') != -1)
            //            {
            //                alert('Quantity must be an increment of ' + skuOrderIncrement);
            //                grdBundleProducts.edit(gridItem);
            //                gridItem.SetValue(3, oldQty);
            //                return;
            //            }
            //        }

            if (fltCurrentQty < 0) {
                alert('<asp:localize runat="server" text="<%$ Resources:GUIStrings, QuantityCannotBeNegative %>"/>');
                grdBundleProducts.edit(gridItem);
                gridItem.SetValue(3, oldQty);
                return;
            }

            var bundleItemId = gridItem.getMember(6).get_value();

            grdBundleProducts.set_callbackParameter('updateQuantity:' + bundleItemId + '|' + currentQty);
            grdBundleProducts.callback();
        }

        function CancelClicked() {
            grdBundleProducts.editCancel();
            gridItem = null;
        }

        //TextBox
        // set value method for setExpression
        function setValue(control, DataField) {
            //alert('setvalue');
            var value = gridItem.GetMember(DataField).Value;
            var txtControl = document.getElementById(control);
            if (value != '' && txtControl != null && value != null) {
                value = value + '';
                txtControl.value = value.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
                if (txtControl.value == null || txtControl.value == 'null')
                    txtControl.value = '';
            }
        }
        // get value method for setExpression
        function getValue(control, DataField) {
            var PageName;
            var txtControl = document.getElementById(control);
            if (txtControl != null) {
                PageName = Trim(txtControl.value.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
            }
            gridItem.GetMember(DataField).Value = PageName;
            return [PageName, PageName];
        }

        var productSearchPopup;
        var searchPageURL;
        function ShowAddProductPopup() {
            popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx?hideBundles=true";
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
            productSearchPopup.onclose = function () {
                var a = document.getElementById('ProductSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function CloseProductPopup(arrSelected) {
            productSearchPopup.hide();
            if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                var bundleItems = '';
                var skuIds = '';
                var skuIdArr;

                //build a string of guids to represent the selected products and their skus.  the form will be:
                //product|sku,sku,sku...||product|sku,sku,sku||product....
                for (k = 0; arrSelected.length; k++) {
                    var arrElem = arrSelected.pop();
                    if (k == 0) {
                        bundleItems = arrElem[0];
                        skuIds = arrElem[1];

                        bundleItems = bundleItems + "@" + skuIds;
                    }
                    else {
                        bundleItems = bundleItems + '|' + arrElem[0];
                        skuIds = arrElem[1];

                        bundleItems = bundleItems + "@" + skuIds;
                    }
                }
                if (bundleItems != '')
                    AddProducts(bundleItems);
            }

        }



        function AddProducts(bundleItems) {
            AddProductsToBundle(bundleProdId, bundleItems);

            grdBundleProducts.callback();
        }

        function RemoveProduct() {
            RemoveItemFromBundle(gridItem.getMember(6).get_text());

            grdBundleProducts.callback();
        }
        function CheckValidations() {
            if (!Page_ClientValidate('PageLevel')) {
                return false;
            }
        }


        function ShowIndexTermPopup(listBox) {

            var objectId = "";
            var selectedTaxonomyIds = "";
            var listBox = document.getElementById(listBox);
            for (i = 0; i < listBox.options.length; i++) {
                if (selectedTaxonomyIds != "")
                    selectedTaxonomyIds = selectedTaxonomyIds + ',' + listBox.options[i].value;
                else
                    selectedTaxonomyIds = listBox.options[i].value;
            }

            if (typeof bundleProdId != 'undefined')
                objectId = bundleProdId;

            var customAttributes = {}
            customAttributes["SelectedTaxonomyIds"] = selectedTaxonomyIds;
            OpeniAppsAdminPopup("AssignIndexTermsPopup", "ObjectId=" + objectId, "SetIndexTerm", customAttributes);
        }


        function SetIndexTerm() {
            var listBox = $("#<%=listAssignedIndexTerms.ClientID%>");
            var hdnIndexTermsList = $("#<%=hdnIndexTermsList.ClientID%>");

            listBox.empty();
            hdnIndexTermsList.val("");
            if (typeof popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "undefined" &&
                        popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "") {
                var taxonomyJson = JSON.parse(popupActionsJson.CustomAttributes["SelectedTaxonomyJson"]);

                $.each(taxonomyJson, function () {
                    listBox.append($("<option />").val(this.Id).html(this.Title));
                });

                hdnIndexTermsList.val(JSON.stringify(taxonomyJson));
            }
        }
        /******************************* Added for MS ***********************************************/
        function UpdateAttributeValues() {
            document.getElementById("<%=hfDeleteVal.ClientID %>").value = "popup";
            UpdateMultiStringHiddenVariable();
            __doPostBack();
        }

        function ShowModalPopup(AttId, ProdId, isSku, ctrlId, skuID) {
            popupPath = '../../Popups/StoreManager/MultiAttributeValue.aspx?ProductId=' + ProdId + '&AttributeId=' + AttId + '&isSku=' + isSku + '&ctrlId=' + ctrlId + '&SkuId=' + skuID;
            //alert(popupPath);
            //popupPath = '../../Popups/StoreManager/MultiAttributeValue.aspx?callback=' + callbackId.Id + '&ProductId='+ProdId+'&AttributeId=' + AttId;            
            pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=330px,height=438px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function () {
                var a = document.getElementById('ShowAttributes');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        /***************End**********************************************************************************************/

        //]]>
    </script>
    <cc1:CommerceStateControl runat="server" ID="commerceState" />
    <div class="button-row">
        <asp:Button ID="btnCancel" Text="<%$ Resources:GUIStrings, Cancel %>" OnClick="btnCancel_OnClick"
            ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" runat="server" />
        <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" OnClientClick="return CheckValidations();"
            OnClick="btnSave_OnClick" ToolTip="<%$ Resources:GUIStrings, Save %>" runat="server"
            ValidationGroup="PageLevel" CssClass="primarybutton" />
        <asp:ValidationSummary ID="valSum" runat="server" EnableClientScript="true" ShowMessageBox="true"
            ShowSummary="false" ValidationGroup="PageLevel" />
        <asp:HiddenField ID="hfDeleteVal" Value="delete" runat="server" />
    </div>
    <div class="bundle-details">
        <div class="clear-fix">
            <div class="columns" style="width: 50%;">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ProductType %>" /></label>
                    <div class="form-value">
                        <ComponentArt:ComboBox ID="ddlProductType" runat="server" SkinID="Default" DropDownHeight="297"
                            DropDownWidth="245" AutoHighlight="false" AutoComplete="false" Width="380">
                            <DropDownContent>
                                <ComponentArt:TreeView ID="trvProductTypes" ExpandSinglePath="true" SkinID="Default"
                                    AutoPostBackOnSelect="true" Height="293" Width="244" runat="server"
                                    OnNodeSelected="trvProductType_onSelect">
                                    <ClientEvents>
                                        <NodeSelect EventHandler="trvProductTypes_onNodeSelect" />
                                    </ClientEvents>
                                </ComponentArt:TreeView>
                            </DropDownContent>
                        </ComponentArt:ComboBox>
                        <asp:HiddenField runat="server" ID="hdnProductTypeID" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, BundleName %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtBundleName" runat="server" Width="370" MaxLength="255"></asp:TextBox>
                        <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtBundleName"
                            ValidationGroup="PageLevel" ControlToValidate="txtBundleName" ErrorMessage="<%$ Resources:GUIStrings, BundleNameIsRequired %>"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="regextxtBundleName" ValidationGroup="PageLevel"
                            ControlToValidate="txtBundleName" ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInBundleName %>"
                            Text="*"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator runat="server" ID="regextxtBundleNameLength" ValidationGroup="PageLevel"
                            ControlToValidate="txtBundleName" ValidationExpression="^[\s\S]{0,255}$" ErrorMessage="<%$ Resources:GUIStrings, BundleNameCannotExceed255Characters %>"
                            Text="*"></asp:RegularExpressionValidator>
                        <asp:HiddenField runat="server" ID="hdnActiveProdId" EnableViewState="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, BundleCode %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtBundleCode" runat="server" Width="370" MaxLength="50"></asp:TextBox>
                        <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="RequiredFieldValidator1"
                            ValidationGroup="PageLevel" ControlToValidate="txtBundleCode" ErrorMessage="<%$ Resources:GUIStrings, BundleCodeIsRequired %>"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ValidationGroup="PageLevel"
                            ControlToValidate="txtBundleCode" ValidationExpression="^[a-zA-Z0-9-]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInBundleCode %>"
                            Text="*"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator runat="server" ID="regextxtBundleCodeLength" ValidationGroup="PageLevel"
                            ControlToValidate="txtBundleCode" ValidationExpression="^[\s\S]{0,50}$" ErrorMessage="<%$ Resources:GUIStrings, BundleCodeCannotExceed50Characters %>"
                            Text="*"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, BundleSKU %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtBundleSKU" runat="server" CssClass="textBoxes" Width="370" MaxLength="255"></asp:TextBox>
                        <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtBundleSKU"
                            ValidationGroup="PageLevel" ControlToValidate="txtBundleSKU" ErrorMessage="<%$ Resources:GUIStrings, BundleSKUIsRequired %>"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator runat="server" ID="regextxtBundleSKU" ValidationGroup="PageLevel"
                            ControlToValidate="txtBundleSKU" ValidationExpression="^[^<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInBundleSKU %>"
                            Text="*"></asp:RegularExpressionValidator>
                        <asp:RegularExpressionValidator runat="server" ID="regextxtBundleSkuLength" ValidationGroup="PageLevel"
                            ControlToValidate="txtBundleSKU" ValidationExpression="^[\s\S]{0,255}$" ErrorMessage="<%$ Resources:GUIStrings, BundleSkuCannotExceed255Characters %>"
                            Text="*"></asp:RegularExpressionValidator>
                        <asp:HiddenField runat="server" ID="hdnSkuID" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, IndexTerms1 %>" /><br />
                        <a href="javascript:ShowIndexTermPopup('<%=listAssignedIndexTerms.ClientID%>');">
                            <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, AssignTerms %>" /></a>
                    </label>
                    <div class="form-value">
                        <asp:ListBox ID="listAssignedIndexTerms" CssClass="textBoxes textBoxesWidth" runat="server"
                            Width="380" SelectionMode="Multiple" Enabled="false"></asp:ListBox>
                        <asp:HiddenField ID="hdnIndexTermsList" runat="server" />
                    </div>
                </div>
            </div>
            <div class="columns" style="width: 50%;">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Image1 %>" /></label>
                    <div class="form-value">
                        <uc2:ProductSKUImage runat="server" ID="prodSkuImage" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, HandlingCharge %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtHandlingCharge" runat="server" Width="75"></asp:TextBox>
                        <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtHandlingCharge"
                            ValidationGroup="PageLevel" ControlToValidate="txtHandlingCharge" ErrorMessage="<%$ Resources:GUIStrings, HandlingChargeIsRequired %>"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="rngtxtHandlingCharge"
                            Type="Currency" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtHandlingCharge"
                            ErrorMessage="<%$ Resources:GUIStrings, HandlingChargeMustBeValidCurrency %>"
                            ValidationGroup="PageLevel" Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, TotalMininumItemPrice %>" /></label>
                    <div class="form-value">
                        <ComponentArt:CallBack runat="server" ID="cbPrices">
                            <Content>
                                <asp:Label ID="lblMinPrice" runat="server"></asp:Label>
                            </Content>
                            <ClientEvents>
                                <CallbackComplete EventHandler="updatePrice" />
                            </ClientEvents>
                        </ComponentArt:CallBack>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, TotalBundlePrice %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtBundlePrice" runat="server" CssClass="textBoxes" Width="75" onblur="updatePrice();"></asp:TextBox>
                        <asp:RequiredFieldValidator CultureInvariantValues="true" runat="server" ID="reqtxtBundlePrice"
                            ValidationGroup="PageLevel" ControlToValidate="txtBundlePrice" ErrorMessage="<%$ Resources:GUIStrings, TotalBundlePriceIsRequired %>"
                            Text="*"></asp:RequiredFieldValidator>
                        <asp:RangeValidator CultureInvariantValues="true" runat="server" ID="rngtxtBundlePrice"
                            Type="Currency" MinimumValue="0" MaximumValue="922337203685477.58" ControlToValidate="txtBundlePrice"
                            ErrorMessage="<%$ Resources:GUIStrings, TotalBundlePriceMustBeValidCurrency %>"
                            ValidationGroup="PageLevel" Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Savings1 %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblSavings" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <h3>
            <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, IncludedProducts %>" /></h3>
        <ComponentArt:Menu ID="mnuBundleItems" runat="server" SkinID="ContextMenu">
            <ClientEvents>
                <ItemSelect EventHandler="mnuBundleItems_onItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
        <ComponentArt:Grid ID="grdBundleProducts" SkinID="ScrollingGrid" runat="server" Width="100%"
            ShowHeader="false" ShowFooter="false" Height="310" RunningMode="Callback" PageSize="6"
            AutoPostBackOnSelect="false" EmptyGridText="<%$ Resources:GUIStrings, NoProductsAdded %>"
            ScrollBar="On" AllowEditing="true" AutoCallBackOnUpdate="false" EditOnClickSelectedItem="false"
            CallbackCachingEnabled="true" CallbackCacheLookAhead="10">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="50" HeadingText="" DataField="ProductCode" AllowReordering="false"
                            FixedWidth="true" AllowSorting="False" DataCellClientTemplateId="SNoHoverTemplate"
                            HeadingCellClientTemplateId="SNoHeadingHoverTemplate" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, Thumbnail %>"
                            DataField="Thumbnail" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                            DataCellClientTemplateId="ImageHoverTemplate" />
                        <ComponentArt:GridColumn Width="490" HeadingText="<%$ Resources:GUIStrings, ProductNameSKU %>"
                            DataField="ProductName" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Qty %>"
                            AllowEditing="True" DataField="Quantity" AllowReordering="false" FixedWidth="true"
                            AllowSorting="False" EditControlType="Custom" EditCellServerTemplateId="QtyServerTemplate" />
                        <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                            DataField="PriceRange" FixedWidth="true" DataCellClientTemplateId="UnitPriceHoverTemplate" />
                        <ComponentArt:GridColumn Width="100" AllowSorting="false" HeadingText="Actions" EditControlType="Custom"
                            Align="Center" FixedWidth="true" AllowReordering="false" EditCellServerTemplateId="svtActions" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="SKUsAsString" Visible="false" />
                        <ComponentArt:GridColumn DataField="MinPrice" Visible="false" />
                        <ComponentArt:GridColumn DataField="SkuOrderIncrement" Visible="false" />
                        <ComponentArt:GridColumn DataField="SkuOrderMinimum" Visible="false" />
                        <ComponentArt:GridColumn DataField="OldQuantity" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="QtyServerTemplate">
                    <Template>
                        <asp:TextBox ID="txtQty" runat="server" CssClass="textBoxes" Width="40"></asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtActions">
                    <Template>
                        <table>
                            <tr>
                                <td>
                                    <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                        runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11" onclick="SaveRecord(); return false;" />
                                </td>
                                <td>
                                    <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                        src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked()" />
                                </td>
                            </tr>
                        </table>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="SNoHeadingHoverTemplate">
                    <span id="Span1" runat="server" title="<%$ Resources:GUIStrings, ProductCode %>"
                        class="HeadingCellText">#</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="SNoHoverTemplate">
                    <span title="## DataItem.GetMember('ProductCode').get_text() ##">## DataItem.GetMember('ProductCode').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ProductCode').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ImageHoverTemplate">
                    <img src="## DataItem.GetMember('Thumbnail').get_text() == '' ? '../../App_Themes/General/images/last.gif' : DataItem.GetMember('Thumbnail').get_text() ##" alt="" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                    <span title="## DataItem.GetMember('ProductName').get_text() ##">## DataItem.GetMember('ProductName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ProductName').get_text() ##</span><br />
                    <span title="## DataItem.GetMember('SKUsAsString').get_text() ##">## DataItem.GetMember('SKUsAsString').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('SKUsAsString').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="QuantityEditTemplate">
                    <input type="text" value="## DataItem.GetMember('Quantity').get_text() ##" style="width: 60px"
                        class="textBoxes" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="UnitPriceHoverTemplate">
                    <span title="## DataItem.GetMember('PriceRange').get_text() ##">## DataItem.GetMember('PriceRange').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('PriceRange').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="DragTemplate">
                    <div style="height: 100px; width: 100px;">
                        ## DataItem.getMember('ProductName').get_text() ##
                    </div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <ContextMenu EventHandler="grdBundleItems_onContextMenu" />
                <CallbackComplete EventHandler="updateMinPrice" />
            </ClientEvents>
        </ComponentArt:Grid>
        <div class="snap-container" id="bundleDescription" style="margin-top: 20px;">
            <ComponentArt:Snap ID="snapDescription" runat="server" Width="100%" CurrentDockingContainer="bundleDescription"
                DockingContainers="bundleDescription" DockingStyle="TransparentRectangle" MustBeDocked="true"
                DraggingMode="FreeStyle" DraggingStyle="SolidOutline" IsCollapsed="true">
                <Header>
                    <div class="snap-header clear-fix">
                        <img id="Img1" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, CollpaseDescription %>"
                            src="~/App_Themes/General/images/snap-collapse.png" onclick="snapDescription.toggleExpand();" />
                        <h3>
                            <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></h3>
                    </div>
                </Header>
                <CollapsedHeader>
                    <div class="snap-header clear-fix">
                        <img id="Img2" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, ExpandDescription %>"
                            src="~/App_Themes/General/images/snap-expand.png" onclick="snapDescription.toggleExpand();" />
                        <h3>
                            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></h3>
                    </div>
                </CollapsedHeader>
                <Content>
                    <div class="snap-content clear-fix">
                        <div class="columns" style="width: 50%;">
                            <div class="form-row">
                                <label class="form-label" style="display: block; padding-bottom: 5px; float: none;">
                                    <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, ShortDescription %>" /></label>
                                <div class="form-value">
                                    <uc2:HtmlEditor runat="Server" ID="txtProductShortDescription" Height="250" Width="575" />
                                </div>
                            </div>
                        </div>
                        <div class="columns" style="float: right; width: 50%;">
                            <div class="form-row">
                                <label class="form-label" style="display: block; float: none; padding-bottom: 5px;">
                                    <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
                                <div class="form-value">
                                    <uc2:HtmlEditor runat="Server" ID="txtProdDescription" Height="250" Width="590" />
                                </div>
                            </div>
                        </div>
                    </div>
                </Content>
            </ComponentArt:Snap>
        </div>
        <div id="GlobalContainer" class="snap-container" style="border-top: 0;">
            <ComponentArt:Snap ID="snapGlobalInfo" runat="server" Width="100%" CurrentDockingContainer="GlobalContainer"
                DockingContainers="GlobalContainer" DockingStyle="TransparentRectangle" MustBeDocked="true"
                DraggingMode="FreeStyle" DraggingStyle="SolidOutline" IsCollapsed="true">
                <Header>
                    <div class="snap-header clear-fix">
                        <img id="Img3" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, CollpaseSKUInformation %>"
                            src="~/App_Themes/General/images/snap-collapse.png" onclick="snapGlobalInfo.toggleExpand();" />
                        <h3><asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, GlobalInformation %>" /></h3>
                    </div>
                </Header>
                <CollapsedHeader>
                    <div class="snap-header clear-fix">
                        <img id="Img5" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, ExpandSKUinformation %>"
                            src="../../App_Themes/General/images/snap-expand.png" onclick="snapGlobalInfo.toggleExpand();" />
                        <h3><asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, GlobalInformation %>" /></h3>
                    </div>
                </CollapsedHeader>
                <Content>
                    <div class="snap-content">
                        <div class="columns" style="width: 550px; margin-right: 10px;">
                            <uc3:DynamicAttributes ID="ProductLevelAttributes2" ValidationGroup="PageLevel" ComboSkinID="default" EnableViewState="true" ClientIDMode="Inherit"
                                runat="server" />
                        </div>
                        <div class="columns" style="width: 229px; margin-right: 10px;">
                            <h4>
                                <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:GUIStrings, AdvancedProduct %>" /></h4>
                            <div class="greyBorderBox scrollingBox">
                                <asp:CheckBoxList ID="chkAdvanceDetail" runat="server" CssClass="checkbox-list" />
                            </div>
                        </div>
                        <div class="columns" style="width: 200px; margin-right: 10px;">
                            <h4>
                                <asp:Localize ID="Localize21" runat="server" Text="<%$ Resources:GUIStrings, AdvancedSKU %>" /></h4>
                            <div class="greyBorderBox scrollingBox checkbox-list">
                                <div class="form-row" style="padding-bottom: 0;">
                                    <asp:CheckBox ID="chkIsActive" runat="server" Text="<%$ Resources:GUIStrings, IsAvailableToSell %>"
                                        Style="margin-left: 3px;" />
                                </div>
                                <div class="form-row" style="padding-bottom: 0;">
                                    <asp:CheckBox ID="chkIsOnline" runat="server" Text="<%$ Resources:GUIStrings, IsAvailableToFrontendSiteCustomers %>"
                                        Style="margin-left: 3px;" />
                                </div>
                                <asp:CheckBoxList ID="chkSKUDetails" runat="server" />
                            </div>
                        </div>
                        <div class="columns" style="width: 200px;">
                            <h4>
                                <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings, Features %>" /></h4>
                            <div class="greyBorderBox scrollingBox checkbox-list">
                                <asp:CheckBoxList ID="chkFeatures" runat="server" />
                            </div>
                        </div>
                        <div class="clear-fix">
                        </div>
                    </div>
                </Content>
            </ComponentArt:Snap>
        </div>
    </div>
</asp:Content>
