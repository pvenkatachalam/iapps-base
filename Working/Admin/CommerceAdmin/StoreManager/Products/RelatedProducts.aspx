﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RelatedProducts.aspx.cs" Title="<%$ Resources: GUIStrings, iAPPSCommerceRelatedProducts %>"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Products.RelatedProducts" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var productListItem;
        var IdColumn = 0;
        var ProductIdColumn = 1;
        var productId;
        
        function grdProductListing_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            productListItem = eventArgs.get_item();
            productId = productListItem.getMember(ProductIdColumn).get_text();
            menuItems = mnuProductsList.get_items();
            for (i = 0; i < menuItems.get_length(); i++) {
                menuItems.getItem(i).set_visible(true);
                if (menuItems.getItem(i).get_id() == 'cmDelete' && productId == "") {
                    menuItems.getItem(i).set_visible(false);
                }
            }
            mnuProductsList.showContextMenuAtEvent(e);
        }

        var productSearchPopup;
        function ShowAddProductPopup() {
            var popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx?ShowProducts=true";
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
            productSearchPopup.onclose = function () {
                var a = document.getElementById('ProductSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function CloseProductPopup(arrSelected) {
            if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                var productIds = '';
                for (k = 0; arrSelected.length; k++) {
                    if (k == 0) {
                        productIds = arrSelected.pop()[0];
                    }
                    else {
                        productIds = productIds + ',' + arrSelected.pop()[0];
                    }
                }
                if (productIds != '')
                    AddProducts(productIds);
            }
            productSearchPopup.hide();
        }
        function AddProducts(productIds) {
            grdProductListing.set_callbackParameter("Insert:" + productIds);
            grdProductListing.callback();
        }

        function mnuProductsList_onItemSelect(sender, eventArgs) {

            var menuItem = eventArgs.get_item();
            var contextDataNode = menuItem.get_parentMenu().get_contextData();
            var selectedMenuId = menuItem.get_id();
            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        ShowAddProductPopup();
                        break;
                    case "cmDelete":
                        if (confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, AreYouSureYouWantToRemoveThisProduct %>"/>')) {
                            grdProductListing.set_callbackParameter("Delete:" + productId);
                            if (grdProductListing.get_recordCount() > grdProductListing.get_pageSize()) {
                                if (grdProductListing.get_recordCount() % grdProductListing.get_pageSize() == 1) {
                                    grdProductListing.previousPage();
                                }
                            }

                            grdProductListing.callback();
                        }
                }
            }
        }


        function grdProductListing_OnExternalDrop(sender, eventArgs) {

            var targetObjectName = eventArgs.get_targetControl().getProperty("GlobalAlias");
            if (targetObjectName == "grdProductListing") {
                var draggedItem = eventArgs.get_item();
                var fromScriptId = null;
                var toScriptId = null;
                var fromSequence = null;
                var toSequence = null;
                if (draggedItem != null) {
                    fromScriptId = draggedItem.getMember('Id').get_value();
                    fromSequence = draggedItem.getMember('RowNumber').get_value();
                }
                var targetItem = eventArgs.get_target();
                if (targetItem != null) {
                    toScriptId = targetItem.getMember('Id').get_value();
                    toSequence = targetItem.getMember('RowNumber').get_value();
                    if (fromScriptId != null && fromSequence != null && toSequence != null) {
                        grdProductListing.set_callbackParameter("ReOrder:" + fromScriptId + ";" + fromSequence + ";" + toSequence);
                        grdProductListing.callback();
                    }
                }
            }
        }

        function RefreshGridForDisplayOrderRelatedProduct(chkDisplayOrder) {
            var rowitem = grdProductListing.get_table().addEmptyRow(0);
            var gridc = grdProductListing.get_table().get_columns();
            var actionColumnValue = "";
            window["EnableDisplayOrderSort"] = chkDisplayOrder.checked;
            if (chkDisplayOrder.checked) {
                grdProductListing.get_levels()[0].set_hoverRowCssClass("selected-row");
                actionColumnValue = "yes";
                grdProductListing.set_externalDropTargets("grdProductListing");
                grdProductListing.set_pageSize(5000);
                grdProductListing.ItemDraggingEnabled = "true";
                // grdProductListing.ItemDraggingClientTemplateId="dragTemplate";
            }
            else {
                grdProductListing.get_levels()[0].set_hoverRowCssClass("");
                actionColumnValue = "no";
                grdProductListing.set_externalDropTargets("");
                grdProductListing.set_pageSize(10);
                grdProductListing.ItemDraggingEnabled = "false";
                // grdProductListing.ItemDraggingClientTemplateId="";
            }
            grdProductListing.set_callbackParameter("EnableSort:" + actionColumnValue);
            grdProductListing.callback();
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <iapps:CommerceStateControl ID="commerceState" runat="server">
    </iapps:CommerceStateControl>
    <div class="grid-utility">
        <div class="columns">
            <label class="form-label" style="float: left; width: 90px; line-height: 24px; margin-bottom: 0px;">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RelationType %>" /></label>
            <div class="form-value">
                <asp:DropDownList runat="server" ID="ddlProductRelType" AutoPostBack="true" Width="120"
                    OnSelectedIndexChanged="ddlProductRelType_OnSelectedIndexChanged" />
            </div>
        </div>
        <div class="right-column">
            <asp:CheckBox ID="chkEnableDisplaySort" runat="server" Text="<%$ Resources:GUIStrings, EnableSiteDisplaySort %>"
                onclick="javascript:RefreshGridForDisplayOrderRelatedProduct(this);" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdProductListing" SliderPopupClientTemplateId="ProductListingSliderTemplate"
        SliderPopupCachedClientTemplateId="ProductListingSliderTemplateCached" Width="100%"
        runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" Sort="RowNumber ASC"
        PagerInfoClientTemplateId="ProductListingPaginationTemplate" CallbackCachingEnabled="false"
        SearchOnKeyPress="false" ManualPaging="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="dragTemplate"
        ExternalDropTargets="grdProductListing" AllowMultipleSelect="false">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" DataField="RowNumber"
                        HeadingText="<%$ Resources:GUIStrings, Display %>" AllowEditing="false" Align="Right"
                        FixedWidth="true" EditControlType="Custom" Width="60" Visible="true" />
                    <ComponentArt:GridColumn Width="475" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                        DataField="ProductTitle" Align="Left" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                    <ComponentArt:GridColumn Width="100" DataField="Title" IsSearchable="false" Visible="false" />
                    <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="TypeTitle" IsSearchable="true" Align="Left" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="TypeHoverTemplate" />
                    <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                        DataField="RealListPrice" IsSearchable="false" Align="Right" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="ListPriceHoverTemplate" />
                    <ComponentArt:GridColumn Width="110" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                        Visible="false" DataField="ListPrice" IsSearchable="false" Align="Right" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="RealListPriceHoverTemplate" />
                    <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings, AvailableToSell %>"
                        DataField="Quantity" IsSearchable="false" Align="Right" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="QuantityHoverTemplate" FormatString="N0" />
                    <ComponentArt:GridColumn Width="75" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="IsActiveDescription" Align="Left" AllowReordering="false" FixedWidth="true"
                        IsSearchable="true" DataCellClientTemplateId="StatusHoverTemplate" />
                    <ComponentArt:GridColumn DataField="SalePrice" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="IsUnlimited" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdProductListing_onContextMenu" />
            <ItemExternalDrop EventHandler="grdProductListing_OnExternalDrop" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="dragTemplate">
                <div style="height: 100px; width: 100px;">
                    ## DataItem.getMember('ProductTitle').get_text() ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SkuHoverTemplate">
                <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <span title="## DataItem.GetMember('TypeTitle').get_text() ##">## DataItem.GetMember('TypeTitle').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('TypeTitle').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ListPriceHoverTemplate">
                <span title="## DataItem.GetMember('ListPrice').get_text() ##">## DataItem.GetMember('ListPrice').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('ListPrice').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ClientTemplate1">
                <span title="## DataItem.GetMember('RealListPrice').get_text() ##">## DataItem.GetMember('RealListPrice').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('RealListPrice').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="QuantityHoverTemplate">
                <span title="## DataItem.GetMember('Quantity').get_text() ##">##DataItem.GetMember('IsUnlimited').get_text()
                    == "true" ? "unlimited" : ((DataItem.GetMember('ProductId').get_text()!="" && DataItem.GetMember('Quantity').get_text()
                    == "") ? "0" : DataItem.GetMember('Quantity').get_text()) ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <span title="## DataItem.GetMember('IsActiveDescription').get_text() ##">## DataItem.GetMember('IsActiveDescription').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('IsActiveDescription').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductListing.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdProductListing.RecordCount
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductListing.PageCount)
                            ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdProductListing.RecordCount
                                ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdProductListing), pageCount(grdProductListing),
                grdProductListing.RecordCount) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuProductsList" runat="server" SkinID="ContextMenu">
        <ClientEvents>
            <ItemSelect EventHandler="mnuProductsList_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
