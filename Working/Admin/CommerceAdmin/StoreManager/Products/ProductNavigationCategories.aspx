﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerProductsNavigationCategories %>" Language="C#" MasterPageFile="~/StoreManager/Products/ProductDetails.master"
    AutoEventWireup="true" CodeBehind="ProductNavigationCategories.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductNavigationCategories" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
   
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <iapps:CommerceStateControl ID="commerceState" runat="server"></iapps:CommerceStateControl>
     <div class="grid-utility">
        <div class="columns">
            <input type="text" id="txtSearch" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>" 
                style="width: 260px;" />
            
            <asp:Button runat="server" ClientIDMode="Static" ID="btnSearch" Text="<%$ Resources:GUIStrings, Search %>"
            CssClass="small-button" value="<%= GUIStrings.Search %>"
                OnClientClick="onClickSearchonGrid('txtSearch', grdNavigationCategories);" UseSubmitBehavior="false" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Grid SkinId="Default" ID="grdNavigationCategories" SliderPopupClientTemplateId="ProductListingSliderTemplate" 
        SliderPopupCachedClientTemplateId="ProductListingSliderTemplateCached" Width="100%" runat="server" RunningMode="Callback" 
        EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" OnBeforeCallback="grdNavigationCategories_BeforeCallback"
        PagerInfoClientTemplateId="ProductListingPaginationTemplate" CallbackCachingEnabled="true" SearchOnKeyPress="true">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id"
                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8" 
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                ShowTableHeading="false" ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="450" HeadingText="<%$ Resources:GUIStrings, Name %>"
                        DataField="DisplayTitle" Align="Left" AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="NameHoverTemplate" />
                    <ComponentArt:GridColumn Width="550" HeadingText="<%$ Resources:GUIStrings, Path %>" DataField="Path" IsSearchable="true" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="SkuHoverTemplate" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="Status" IsSearchable="true" Align="Left"
                        AllowReordering="false" FixedWidth="true"
                        DataCellClientTemplateId="TypeHoverTemplate" />
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="IsExcluded" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdNavigationCategories_onContextMenu" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('DisplayTitle').get_text() ##">## DataItem.GetMember('DisplayTitle').get_text() == "" ? "&nbsp;" : DataItem.GetMember('DisplayTitle').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SkuHoverTemplate">
                <span title="## DataItem.GetMember('Path').get_text() ##">## DataItem.GetMember('Path').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Path').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <span title="## DataItem.GetMember('Status').get_text() ##">## DataItem.GetMember('Status').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Status').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplate">
                <div class="SliderPopup">
                    <p><asp:localize runat="server" text="<%$ Resources:GUIStrings, DataNotLoaded %>"/></p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNavigationCategories.PageCount) ##</span>
                        <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdNavigationCategories.RecordCount ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNavigationCategories.PageCount) ##</span>
                        <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, grdNavigationCategories.RecordCount ##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductListingPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdNavigationCategories), pageCount(grdNavigationCategories), grdNavigationCategories.RecordCount) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuNavCategory" runat="server" SkinID="ContextMenu">
              
        <ClientEvents>
            <ItemSelect EventHandler="mnuNavCategory_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>