﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductMedia.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductMedia"
    MasterPageFile="~/StoreManager/Products/ProductDetails.master" StylesheetTheme="General"
     Title="<%$ Resources: GUIStrings, iAPPSCommerceDownloadableMedia %>" %>

<%@ Register Assembly="Bridgeline.FW.Commerce.Controls" Namespace="Bridgeline.FW.Commerce.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var productMediaIdColumn = 0;
        var SKUIdColumn = 2;
        var productMediaId;
        var menuItems;
        var expDateColumn = 9;
        var sizeIdColumn = 13;
        var statusIdColumn = 10;
        function SetGridLevelGeneralVariablesinPage() {
            setTimeout(function () {
                gridObject = grdProductMedia;
                menuObject = mnuProductsMediaList;
                gridObjectName = "grdProductMedia";
                uniqueIdColumn = 0;
                operationColumn = 11;
                selectedTreeNodeId = emptyGuid;
            }, 500);
        }
        function PageLevelOnContextMenu(menuItems) {
            var status = gridItem.getMember(statusIdColumn).get_text();

            //if staus is Active show InActice else show Active
            if (status == "Active") {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmActive') {
                        menuItems.getItem(i).set_visible(false);
                    }
                    if (menuItems.getItem(i).get_id() == 'cmInActive') {
                        menuItems.getItem(i).set_visible(true);
                    }
                }

            }

            if (status == "InActive") {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmInActive') {
                        menuItems.getItem(i).set_visible(false);
                    }
                    if (menuItems.getItem(i).get_id() == 'cmActive') {
                        menuItems.getItem(i).set_visible(true);
                    }
                }

            }
            return true;
        }



        var gridOperation;

        function PageLevelOnItemSelect(selectedMenuId, currentGridItem, sender, eventArgs) {

            if (selectedMenuId == 'cmInActive') {

                gridItem = currentGridItem;
                var productMediaId = gridItem.getMember(uniqueIdColumn).get_text();
                var isResult = MakeInactiveProductMedia(productMediaId);
                if (isResult.toLowerCase() == "true") {
                    alert(__JSMessages["RecordInActiveSuccessfully"]);

                    gridOperation = "InActive";
                    grdProductMedia.edit(gridItem);
                    gridItem.SetValue(operationColumn, gridOperation);
                    isGridCallback = true;
                    grdProductMedia.editComplete();
                    grdProductMedia.callback();
                }
            }

            if (selectedMenuId == 'cmActive') {

                gridItem = currentGridItem;
                var productMediaId = gridItem.getMember(uniqueIdColumn).get_text();
                var isResult = MakeActiveProductMedia(productMediaId);
                if (isResult.toLowerCase() == "true") {
                    alert(__JSMessages["RecordActiveSuccessfully"]);

                    gridOperation = "Active";
                    grdProductMedia.edit(gridItem);
                    gridItem.SetValue(operationColumn, gridOperation);
                    isGridCallback = true;
                    grdProductMedia.editComplete();
                    grdProductMedia.callback();
                }
            }

        }


        function PageLevelAddItem(currentGridItem) {
            if (pckExpDate.getSelectedDate() != '' && pckExpDate.getSelectedDate() != null) {
                pckExpDate.clearSelectedDate();
            }
            return true;

        }

        function PageLevelEditItem(currentGridItem) {
            return true;
        }

        function PageLevelDeleteItem(currentGridItem) {
            return true
        }

        function PageLevelSaveRecord() {
            return true;
        }

        function PageLevelCancelEdit() {
            return true;
        }

        function PageLevelGridValidation(currentGridItem) {


            var isValid = true;
            var thisFileName = Trim(document.getElementById(txtFileName).value);
            var thisName = Trim(document.getElementById(txtName).value);
            var msg = "";

            if (Trim(thisName) == "") {
                msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, TitleShouldNotBeEmptyPleaseReEnter %>' />";
            }
            else {
                msg += GridLevelSpecialCharacterChecking(thisName, 'Name');
            }

            if (Trim(thisFileName) == "") {
                msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FileNameShouldNotBeEmpty %>' />";
            }

            var len = gridObject.Data.length;
            var gItem;
            var nameOth;
            for (var i = 0; i < len; i++) {
                if (i != gridItem.Index) {
                    gItem = gridObject.get_table().getRow(i);
                    nameOth = gItem.getMember(3).get_value();
                    if (thisName.toLowerCase() == nameOth.toLowerCase()) {
                        msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ATitleWithThatNameAlreadyExistsPleaseReEnter %>' />";
                    }
                }
            }

            for (var i = 0; i < len; i++) {
                if (i != gridItem.Index) {
                    gItem = gridObject.get_table().getRow(i);
                    nameOth = gItem.getMember(5).get_value();
                    var fileName = thisFileName.replace(/(\s)/g, "-"); //In order for the file name to be checked, need to replace spaces with -
                    if (fileName.toLowerCase() == nameOth.toLowerCase()) {
                        msg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AFileWithThatNameAlreadyExistsPleaseReUpload %>' />";
                    }
                }
            }


            //Expiratio date can be empty
            if (pckExpDate.getSelectedDate() != '' && pckExpDate.getSelectedDate() != null) {
                //if (gridItem.getMember(uniqueIdColumn).get_value() == '' || gridItem.getMember(uniqueIdColumn).get_value() == null || gridItem.getMember(uniqueIdColumn).get_value() == emptyGuid) {
                var ExpDate = new Date(pckExpDate.getSelectedDate());
                var toDay = iAppsCurrentLocalDate;

                var startDateMil = ExpDate.getTime();
                var toDayMil = toDay.getTime();

                if (startDateMil < toDayMil) {
                    //sometime date may be same but time makes problem.
                    if (ExpDate.getFullYear() < toDay.getFullYear()) {
                        msg += '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpirationDateShouldNotBeLessThanToday %>" />';
                    }
                    else if (ExpDate.getMonth() < toDay.getMonth()) {
                        msg += '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpirationDateShouldNotBeLessThanToday %>" />';
                    }
                    else if (ExpDate.getDate() < toDay.getDate()) {
                        msg += '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpirationDateShouldNotBeLessThanToday %>" />';
                    }
                }
                //}
            }

            var thisMaxDownloads = Trim(document.getElementById(txtMaxDownloads).value);
            if (Trim(thisMaxDownloads) != "" && !isPositiveNumber(thisMaxDownloads))
                msg += '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseEnterANumberGreaterThanMinus1ForMaxNumberOfDownloads %>" />';

            var thisMaxDaysAvailable = Trim(document.getElementById(txtMaxDaysAvailable).value);
            if (Trim(thisMaxDaysAvailable) != "" && !isPositiveNumber(thisMaxDaysAvailable))
                msg += '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseEnterANumberGreaterThanMinus1ForMaxDaysAvailable %>" />';

            if (msg != "") {
                alert(msg);
                isValid = false;
            }
            else {
                isValid = true;
            }

            return isValid;
        }

        function PageLevelDeleteItem(currentGridItem) {
            //Call Callback method to delete.. 
            var productMediaId = gridItem.getMember(uniqueIdColumn).get_text();
            var isResult = DeleteProductMedia(productMediaId);

            if (isResult.toLowerCase() == 'true') {
                if (grdProductMedia.get_recordCount() > grdProductMedia.get_pageSize()) {
                    if (grdProductMedia.get_recordCount() % grdProductMedia.get_pageSize() == 1) {
                        grdProductMedia.previousPage();
                    }
                }
            }
            return isResult;
        }

        function pckExp_OnDateChange() {
            var expDate = pckExpDate.getSelectedDate();
            calExpDate.setSelectedDate(expDate);
        }

        function calExpDate_OnChange() {
            var expDate = calExpDate.getSelectedDate();
            pckExpDate.setSelectedDate(expDate);
        }

        //Calendar Control
        // set value method for setExpression
        function setCalendarValue(control, DataField) {
            var data = gridItem.GetMember(DataField).get_object();
            var value = new Date(data);
            if (control == 'ExpDate') {
                if (value == "" || value == null || data == undefined) {
                }
                else {
                    pckExpDate.setSelectedDate(value);
                }
            }
        }

        // get value method for setExpression
        function getCalendarValue(control, DataField) {
            var expDate;
            if (control == "ExpDate") {
                if (pckExpDate.getSelectedDate() != pckExpDate.get_maxDate()) {
                    expDate = pckExpDate.formatDate(pckExpDate.getSelectedDate(), shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
                }
            }
            return [expDate, expDate];
        }

        function Button_OnClick(button) {
            if (calExpDate.get_popUpShowing()) {
                calExpDate.hide();
            }
            else {

                var data = gridItem.GetMember(expDateColumn).get_object();
                var value = new Date(data);
                if (value == "" || value == null || data == undefined) {
                    var value = new Date(9999, 12, 31);
                }

                var expdate = pckExpDate.getSelectedDate();


                if (data != null) {
                    calExpDate.setSelectedDate(expdate);
                }
                else {
                    calExpDate.clearSelectedDate();
                }

                calExpDate.show(button);
            }
        }

        function grid_onCallbackError(sender, eventArgs) {
            alert(__JSMessages["ErrorWhileSavingTheRecord"]);
            grdProductMedia.callback();
        }
        var Id = '';
        var SKUId = '';
        var fileName;
        var fileType;
        // Calls while upload a file
        function fileUploadShow() {
            if (gridItem.getMember(productMediaIdColumn).get_text() != "[object Object]" && gridItem.getMember(productMediaIdColumn).get_text() != null && gridItem.getMember(productMediaIdColumn).get_text() != '') // id column
            {
                Id = gridItem.getMember(productMediaIdColumn).get_text();
            }
            else
            {
                Id= '';
            }
            var hdnSKU = document.getElementById('<%=hdnSKUId.ClientID%>');
            
            if (hdnSKU != null && hdnSKU.value != '') // id column
            {
                SKUId = hdnSKU.value;
            }
            fileName = document.getElementById(txtFileName);
            var ddlControl = document.getElementById(ddlTypeTemp);
            var typeIndex  = ddlControl.selectedIndex;
            var fileType = ddlControl.options[typeIndex].text;

            var uplaodPath = "<%=ResolveUrl("~/Popups/StoreManager/UploadFile.aspx")%>";
            
            uplaodPath += '?Id=' + Id + '&SKUId=' + SKUId + '&IsMedia=true' + '&FileType='+ fileType;
            
            HTMLEditorPopup = dhtmlmodal.open('FileUps', 'iframe', uplaodPath, '', 'width=290px,height=216px,center=1,resize=0,scrolling=1');
            HTMLEditorPopup.onclose = function () {
                var a = document.getElementById('FileUps');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("/blank.html");
                return true;
            }

        }
        //Calling from UploadFile.cs
        function SetUploadedDetails(fileSize, retFileName) {
            gridItem.SetValue(sizeIdColumn, fileSize, true);
            fileName.value = retFileName;
        }

        function IsUploadFileExist(thisFileName)
        {
            var len = gridObject.Data.length;
            var gItem;
            var nameOth;
            var bIsUploadFileExist = false;
         for (var i = 0; i < len; i++) {
                if (i != gridItem.Index) {
                    gItem = gridObject.get_table().getRow(i);
                    nameOth = gItem.getMember(5).get_value();
                    var fileName = thisFileName.replace(/(\s)/g, "-"); //In order for the file name to be checked, need to replace spaces with -
                    if (fileName.toLowerCase() == nameOth.toLowerCase()) {
                        bIsUploadFileExist = true;
                    }
                }
            }
            return bIsUploadFileExist;
        }
        function microsoftMouseMove() {
            var IfrRef = document.getElementById('DivShim');
            if (window.event.x != IfrRef.left && window.event.y != IfrRef.top) {
                IfrRef.left = window.event.x;
                IfrRef.top = window.event.y;
            }

        }
        
        var tooltipIconSrc = '../../App_Themes/General/images/icon-tooltip.gif';
        var tooltipImage = "../../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        var MDAText = "<%= GUIStrings.MDA %>";
        var MNDText = "<%= GUIStrings.MND %>";
        var ExpirationDateText = "<%= GUIStrings.ExpirationDate %>";

        function ShowHelpText(strTooltip) {
            var strHTML = '';
            switch (strTooltip) {
                case 'mnd':
                    strHTML = "<img alt='<%= GUIStrings.MaxNumDownloads %>' onmouseover=\"ddrivetip('<%= GUIStrings.MaxNumDownloads %>', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='" + tooltipIconSrc + "' />";
                    return strHTML;
                    break;
                case 'mda':
                    strHTML = "<img alt='<%= GUIStrings.MaxDaysAvailable %>' onmouseover=\"ddrivetip('<%= GUIStrings.MaxDaysAvailable %>', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='" + tooltipIconSrc + "' />";
                    return strHTML;
                case 'ExpirationDate':
                    strHTML = "<img alt='<%= GUIStrings.ExpirationDateHelpText %>' onmouseover=\"ddrivetip('<%= GUIStrings.ExpirationDateHelpText %>', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='" + tooltipIconSrc + "' />";
                    return strHTML;
            }
        }

        window["UploadFileType"] = "Other";
    </script>
</asp:Content>
<asp:Content ID="ProductMediaContent" ContentPlaceHolderID="ProductDetailsContentHolder" runat="server">
    <iapps:commercestatecontrol id="commerceState" runat="server">
    </iapps:commercestatecontrol>
    <div class="grid-utility">
        <div class="form-row">
            <label class="form-label" style="float: left; min-width: 75px; padding-right: 5px;"><%= GUIStrings.SKUSelector %></label>
            <div class="form-value">
                <cc1:skuselector runat="server" id="skuSelector" enableviewstate="true" autopostbackonclose="true"
                    carryforwardselectedstate="true" usepreviousselectedstate="true" onopencloseclick="skuSelector_OnOpenCloseClick" />
                <asp:HiddenField ID="hdnSKUId" runat="server" />
            </div>
        </div>
    </div>
    <componentart:grid skinid="Default" id="grdProductMedia" runat="server" width="940"
        runningmode="Client" pagerinfoclienttemplateid="ProductMediaListingPaginationTemplate"
        allowediting="true" autocallbackoninsert="false" sliderpopupcachedclienttemplateid="ProductMediaListingSliderTemplateCached"
        sliderpopupclienttemplateid="ProductMediaListingSliderTemplate" autocallbackonupdate="false"
        callbackcachelookahead="10" pagesize="10" allowpaging="True" callbackcachingenabled="true"
        allowmultipleselect="false" editonclickselecteditem="false" emptygridtext="<%$ Resources:GUIStrings, NoItemsFound %>">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" 
                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
                HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8" 
                SortImageHeight="7" AllowGrouping="false" AllowSorting="false"
                ShowTableHeading="false" ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="SKUId" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn Width="165" HeadingText="<%$ Resources:GUIStrings, Title %>"
                        DataField="Title" Align="Left" IsSearchable="true" AllowReordering="false" TextWrap="true"
                        FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" EditCellServerTemplateId="NameServerTemplate"
                        EditControlType="Custom" />
                    <ComponentArt:GridColumn Width="75" HeadingText="<%$ Resources:GUIStrings, Type %>"
                        DataField="MediaTypeId" IsSearchable="true" Align="Left"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="TypeHoverTemplate"
                        EditCellServerTemplateId="TypeServerTemplate" EditControlType="Custom" />
                    <ComponentArt:GridColumn Width="172" HeadingText="<%$ Resources:GUIStrings, FileName %>"
                        Visible="true" DataField="FileName" IsSearchable="false" Align="Left" TextWrap="true"
                        AllowReordering="false" FixedWidth="true" EditControlType="Custom" DataCellClientTemplateId="FileNameHoverTemplate"
                        EditCellServerTemplateId="FileNameServerTemplate" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Description %>"
                        DataField="Description" IsSearchable="false" Align="Left"
                        AllowReordering="false" FixedWidth="true" EditControlType="Custom" DataCellClientTemplateId="DescHoverTemplate"
                        EditCellServerTemplateId="DescriptionServerTemplate" Visible="false" />
                    <ComponentArt:GridColumn Width="50" DataField="MaxNumDownloads" IsSearchable="false"
                        Align="Left" AllowReordering="false"
                        FixedWidth="true" HeadingCellClientTemplateId="MaxNumDownloadsHeadingTemplate" 
                        EditControlType="Custom" EditCellServerTemplateId="MaxDownloadsServerTemplate" DataCellClientTemplateId="DataCellMNDTemplate" />
                    <ComponentArt:GridColumn Width="50" DataField="MaxDaysAvailable"
                        Align="Left" AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="MaxDownloadsHeadingTemplate"
                        IsSearchable="true" Visible="true" EditControlType="Custom" EditCellServerTemplateId="MaxDaysServerTemplate"  DataCellClientTemplateId="DataCellMDATemplate"/>
                    <ComponentArt:GridColumn Width="135" HeadingText=""
                        DataField="ExpirationDate" SortedDataCellCssClass="SortedDataCell" Align="Left"
                        AllowReordering="false" FixedWidth="true" IsSearchable="true" EditControlType="Custom"
                        EditCellServerTemplateId="ExpirationDateTemplate" DataType="System.DateTime"
                        FormatString="d" DataCellClientTemplateId="DataCellExpDateTemplate" HeadingCellClientTemplateId="ExpDateHT" />
                    <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="StatusDescription" Align="Left"
                        AllowReordering="false" FixedWidth="true" IsSearchable="true" AllowEditing="false" />
                    <ComponentArt:GridColumn Width="50" AllowSorting="false" HeadingText="<%$ Resources:GUIStrings, Actions %>"
                        DataCellCssClass="DataCell" EditControlType="Custom" Align="Center" FixedWidth="true"
                        HeadingCellCssClass="HeadingCell" AllowReordering="false" EditCellServerTemplateId="svtActions" />
                    <ComponentArt:GridColumn DataField="MediaType" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="Size" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grid_onContextMenu" />
            <Load EventHandler="grid_onLoad" />
            <CallbackError EventHandler="grid_onCallbackError" />
            <SortChange EventHandler="CancelClicked" />
        </ClientEvents>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="NameServerTemplate">
                <Template>
                    <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="120"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="TypeServerTemplate">
                <Template>
                    <asp:DropDownList ID="ddlType" runat="server" Width="70" Rows="3">
                    </asp:DropDownList>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="FileNameServerTemplate">
                <Template>
                    <asp:TextBox ID="txtFileName" runat="server" CssClass="textBoxes" Width="80" ReadOnly="true"></asp:TextBox>
                    <img alt="<%$ Resources:GUIStrings, Browse %>" id="a1" src="~/App_Themes/General/images/icon-browse.gif"
                        runat="server" onclick="fileUploadShow();" />
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="DescriptionServerTemplate">
                <Template>
                    <asp:TextBox ID="txtDescription" TextMode="MultiLine" Rows="3" Columns="7" runat="server"
                        CssClass="textBoxes" Width="90"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="ExpirationDateTemplate">
                <Template>
                    <table border="0" cellpadding="0" cellspacing="0" width="135">
                        <tr>
                            <td>
                                <ComponentArt:Calendar ID="pckExpDate" runat="server" PickerFormat="Custom" PickerCustomFormat="MMMM d yyyy"
                                    ControlType="Picker" PickerCssClass="picker" ClientSideOnSelectionChanged="pckExp_OnDateChange"   />
                            </td>
                            <td>
                                <img id="calendar_to_button" runat="server" alt="<%$ Resources:GUIStrings, CalendarButton %>"
                                    class="calendar_button" src="../../App_Themes/General/images/Calender-btn.gif"
                                    onclick="Button_OnClick(this);" />
                                <ComponentArt:Calendar runat="server" ID="calExpDate" ControlType="Calendar" PopUp="Custom"
                                    PopUpExpandControlId="calendar_to_button" ClientSideOnSelectionChanged="calExpDate_OnChange"
                                    SkinID="Default" AllowMultipleSelection="false" AllowWeekSelection="false" AllowMonthSelection="false" >
                                        <FooterClientTemplate>
                                            <a  onclick="pckExpDate.ClearSelectedDate();calExpDate.Hide();">Clear Selected Date</a>
                                        </FooterClientTemplate>
                                </ComponentArt:Calendar>
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="MaxDownloadsServerTemplate">
                <Template>
                    <asp:TextBox ID="txtMaxDownloads" runat="server" CssClass="textBoxes" Width="40"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="MaxDaysServerTemplate">
                <Template>
                    <asp:TextBox ID="txtMaxDaysAvailable" runat="server" CssClass="textBoxes" Width="40"></asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtActions">
                <Template>
                    <table>
                        <tr>
                            <td>
                                <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                    runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11"
                                    onclick="gridObject_BeforeUpdate();return false;" />
                            </td>
                            <td>
                                <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                    src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="MaxNumDownloadsHeadingTemplate">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            ## MNDText ##
                        </td>
                        <td>
                            ## ShowHelpText('mnd') ##
                        </td>
                    </tr>
                </table>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="MaxDownloadsHeadingTemplate">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            ## MDAText ##
                        </td>
                        <td>
                            ## ShowHelpText('mda') ##
                        </td>
                    </tr>
                </table>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DataCellExpDateTemplate">
                <span title="## DataItem.GetMember('ExpirationDate').get_text() == '' ? 'N/A' : DataItem.GetMember('ExpirationDate').get_text()##">## DataItem.GetMember('ExpirationDate').get_text() == '' ? 'N/A' : DataItem.GetMember('ExpirationDate').get_text()##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ExpDateHT">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>## ExpirationDateText ##</td>
                        <td>## ShowHelpText('ExpirationDate') ##</td>
                    </tr>
                </table>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DataCellMNDTemplate">
                    <span>## DataItem.GetMember('MaxNumDownloads').get_text() == 0 ? 'N/A' : DataItem.GetMember('MaxNumDownloads').get_text()## </span>
            </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="DataCellMDATemplate">
                    <span>## DataItem.GetMember('MaxDaysAvailable').get_text() == 0 ? 'N/A' : DataItem.GetMember('MaxDaysAvailable').get_text()## </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DescHoverTemplate">
                <span title="## DataItem.GetMember('Description').get_text() ##">## DataItem.GetMember('Description').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Description').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
                <span title="## DataItem.GetMember('MediaType').get_text() ##">## DataItem.GetMember('MediaType').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('MediaType').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="FileNameHoverTemplate">
                <span title="## DataItem.GetMember('FileName').get_text() ##">## DataItem.GetMember('FileName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('FileName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <span title="## DataItem.GetMember('StatusDescription').get_text() ##">## DataItem.GetMember('StatusDescription').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('StatusDescription').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductMediaListingSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductMedia.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdProductMedia.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductMediaListingSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductMedia.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdProductMedia.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductMediaListingPaginationTemplate">
                ##stringformat(Page0Of12items, currentPageIndex(grdProductMedia), pageCount(grdProductMedia),
                grdProductMedia.RecordCount)##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </componentart:grid>
    <componentart:menu id="mnuProductsMediaList" runat="server" skinid="ContextMenu">
        <ClientEvents>
            <ItemSelect EventHandler="menu_onItemSelect" />
        </ClientEvents>
    </componentart:menu>
</asp:Content>