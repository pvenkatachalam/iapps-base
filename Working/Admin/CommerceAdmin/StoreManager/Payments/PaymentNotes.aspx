﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentNotes.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.PaymentNotes"
    MasterPageFile="~/StoreManager/Payments/PaymentsMaster.master" StylesheetTheme="General"
    Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupPaymentMethodsManagePaymentNotes %>" %>

<%@ Register Src="~/UserControls/General/PaymentNotes.ascx" TagName="Notes" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PaymentContentHolder" runat="server">
    <uc:Notes ID="ctlNotes" runat="server" />
</asp:Content>
