﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreSetupPaymentMethodsViewAllPayments %>" Language="C#" MasterPageFile="~/StoreManager/Payments/PaymentsMaster.master" 
    AutoEventWireup="true" CodeBehind="ViewAllPayments.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewAllPayments" StylesheetTheme="General" %>
<%@ Register TagPrefix="pl" TagName="PaymentListing" Src="~/UserControls/StoreSetup/PaymentMethods/PaymentListing.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PaymentContentHolder" runat="server">
    <pl:PaymentListing id="ctlPaymentListing" runat="server"></pl:PaymentListing>
</asp:Content>
