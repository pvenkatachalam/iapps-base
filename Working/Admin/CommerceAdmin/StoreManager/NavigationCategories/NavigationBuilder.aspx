﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true"
    CodeBehind="NavigationBuilder.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.NavigationBuilder"
    Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerNavigationBuilderManageNavigationCategories %>"
    StylesheetTheme="General" EnableEventValidation="false" %>

<%@ Register TagPrefix="ft" TagName="Filters" Src="~/UserControls/StoreManager/Products/NavItemFilter.ascx" %>
<%@ Register TagPrefix="fc" TagName="Facets" Src="~/UserControls/StoreManager/Products/NavFacetsGrid.ascx" %>
<%@ Register TagPrefix="pp" TagName="ProductsPreview" Src="~/UserControls/StoreManager/Products/ProductsPreview.ascx" %>
<%@ Register TagPrefix="cp" TagName="ConnectPage" Src="~/UserControls/ContextMenu/CreateAndConnectCategory.ascx" %>
<%@ Register TagPrefix="nd" TagName="NavDetails" Src="~/UserControls/StoreManager/Products/NavCategoryDetails.ascx" %>
<asp:Content ID="navigationBuilderHeaderContent" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var FilterIdAndProdRelationType = '';

        function loadNavFacetGrid() {
            grdNavFacets.set_callbackParameter(selectedNodeId);
            grdNavFacets.callback();
        }


        function SetTreeLevelGeneralVariablesinPage() {
            setTimeout(function () {
                treeObject = treeNavigation;
                treeMenuObject = mnuNavigation;
                treeObjectName = "treeNavigation";
            }, 500);
        }

        function PageLevelOnNodeSelect(sender, eventArgs) {
            var navFilterId = selectedTreeNode.getProperty('FilterId');
            var selectedTabId = tbsNavigationFilters.getSelectedTab().get_id();
            switch (selectedTabId) {
                case 'tabProducts':
                    //setTimeout(function () {
                    grdPreviewProducts.set_callbackParameter(navFilterId);
                    grdPreviewProducts.callback();
                    //}, 700);
                    break;
                case 'tabFilter':
                    LoadFilter(selectedNodeId, navFilterId);
                    break;
                case 'tabFacets':
                    loadNavFacetGrid();
                    break;
                case 'tabDetails':
                    UpdateNavCategoryDetails(selectedNodeId);
                    break;
                case "tabProdRelation":
                    LoadFilterProductRelated(selectedNodeId);
                    break;
            }
        }
        function PageLevelOnNodeBeforeSelect(sender, eventArgs) {
            return true;
        }

        function PageLevelTreeOnContextMenu(menuItems, indexOfSelectedNode) {
            ClearCreateAndConnnectUCData();
            if (indexOfSelectedNode >= 0) {
                if (selectedTreeNode.getProperty("CssClass") == "GrayedTreeNode") {
                    menuItems.getItem(6).set_text("<asp:localize runat='server' text='<%$ Resources:GUIStrings, MakeVisible %>'/>");
                }
                else {
                    menuItems.getItem(6).set_text("<asp:localize runat='server' text='<%$ Resources:GUIStrings, MakeInVisible %>'/>");
                }
            }
            return true;
        }

        function ClearCreateAndConnnectUCData() {
            document.getElementById(txtCategoryName).value = '';
            document.getElementById(lbConnectSelectedTemplate).selectedIndex = -1;
        }

        function mnuNavigation_onItemSelect(contextDataNode, selectedMenuId) {

            if (selectedMenuId == 'cmTreeDelete') {
                mnuNavigation.hide();
                RemoveTreeNode(contextDataNode);

            }
            else if (selectedMenuId == 'cmTreeRename') {
                mnuNavigation.hide();
                RenameTreeNode(contextDataNode);
            }
            else if (selectedMenuId == 'cmTreePage') {
                ConnectCMSPage(contextDataNode);
            }
            else if (selectedMenuId == 'cmTreeChangeStatus') {
                mnuNavigation.hide();
                nodeId = contextDataNode.get_id();
                nodeText = contextDataNode.get_text();
                var navtext = mnuNavigation.findItemById(selectedMenuId).get_text();

                //var navtext = eventArgs.get_item().get_text();
                if (navtext == "<asp:localize runat='server' text='<%$ Resources:GUIStrings, MakeVisible %>'/>") {
                    result = MakeNavNodeVisible(nodeId);
                    if (result == "True") {
                        contextDataNode.setProperty("CssClass", "TreeNode");
                        contextDataNode.setProperty("HoverCssClass", "HoverTreeNode");
                        contextDataNode.setProperty("SelectedCssClass", "SelectedTreeNode");
                        contextDataNode.set_toolTip(nodeText);
                    }
                }
                else {
                    result = MakeNavNodeInVisible(nodeId);
                    if (result == "True") {
                        contextDataNode.setProperty("CssClass", "GrayedTreeNode");
                        contextDataNode.setProperty("HoverCssClass", "GrayedTreeNode");
                        contextDataNode.setProperty("SelectedCssClass", "SelectedGrayedTreeNode");
                        contextDataNode.set_toolTip("Invisible");
                    }
                }
            }
        }



        function PageLevelAddEmptyTreeNode(contextMenuData, treeObject, newNode) {
            newNode.setProperty("CssClass", "GrayedTreeNode");
            newNode.setProperty("HoverCssClass", "GrayedTreeNode");
            newNode.setProperty("SelectedCssClass", "SelectedGrayedTreeNode");
        }

        function PageLevelRemoveTreeNode(contextMenuData) {
            var navFilterId = contextMenuData.getProperty('FilterId');
            var navPageId = contextMenuData.getProperty('TargetId');
            var confirmResult = true; ;
            if (navFilterId != null && navFilterId != '' & navFilterId != emptyGuid) {
                confirmResult = window.confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, FilterAttachedWithThisNavAreYouSureYouWantToDeleteThisNavItem %>"/>');
            }
            else if (navPageId != null && navPageId != '' & navPageId != emptyGuid) {
                confirmResult = window.confirm('<asp:localize runat="server" text="<%$ Resources:GUIStrings, PageAttachedWithThisNavAreYouSureYouWantToDeleteThisNavItem %>"/>');
            }
            if (confirmResult) {
                nodeId = contextMenuData.get_id();
                var parentNode = contextMenuData.get_parentNode();
                var result = DeleteNavNode(nodeId);

                if (result == "True") {
                    var depth = parentNode.get_depth();
                    isTreeCallback = 'false';
                    if (depth == 0) {
                        if (parentNode.get_nodes().get_length() > 0 && parentNode.get_nodes().getNode(0).get_id() != nodeId) {
                            var rootChildNode = parentNode.get_nodes().getNode(0);
                            rootChildNode.select();
                        }
                        else {
                            tbsNavigationFilters.selectTabById('tabProducts');
                            grdPreviewProducts.get_table().clearData();
                            selectedTreeNode = null;
                        }
                    }
                    else {
                        parentNode.select();
                    }
                }



                return result;
            }
            else {
                return null;
            }
        }

        function PageLevelRenameTreeNode(contextMenuData) {
            return true;
        }

        function PageLevelTreeLoad(sender, eventArgs) {
            return true;
        }

        // call call back method from here, and return the result
        function PageLevelCreateNewNode(newText, parentId, newNode) {
            var result = CreateNavNode(newText, parentId);
            return result;
        }

        //This will call immediately after creating a node. Instead of this we can select that node there, but it may make someother error, eg.it calls tree_onNodeBeforeSelect etc..
        function PageLevelLoadNewNode(selectedNodeId, eventArgs) {

            var navFilterId = emptyGuid; // eventArgs.get_node().getProperty('FilterId'); //This point there is no filter Id
            var selectedTabId = tbsNavigationFilters.getSelectedTab().get_id();
            if (selectedTabId == 'tabFilter') LoadFilter(selectedNodeId, navFilterId);
            if (selectedTabId == 'tabProdRelation') {
                LoadFilterProductRelated(selectedNodeId);
            }
        }

        // call call back method from here, and return the result
        function PageLevelEditNode(id, newText, renamedNode) {
            return EditNavNode(id, newText);
        }
        // this will call immediately after renamed of the node.
        function PageLevelLoadEditedNode(selectedNodeId, sender, eventArgs) {

            var navFilterId = treeNavigation.get_selectedNode().getProperty('FilterId');
            var selectedTabId = tbsNavigationFilters.getSelectedTab().get_id();
            if (selectedTabId == 'tabFilter') LoadFilter(selectedNodeId, navFilterId);

            if (selectedTabId == 'tabProdRelation') {
                LoadFilterProductRelated(selectedNodeId);
            }
        }

        function PageLevelRefreshTreeImmediate() {
            if (selectedNodeId != null)
                cbTreeRefresh.Callback(selectedNodeId);
        }     
    </script>
    <script type="text/javascript">
    var selectedPage;
    var selectedPageURL;
    var selectedPageId;

    var pagepopup;
    var adminpath;

    function LoadFilter(selectedNavNodeId, navFilterId) {
    
        if (treeOperation != 'CreateNewNode') // Rename is going on; should not load the grid here. while rename the new node, we have to select the node, that time should not load the grid
        {
            isTreeCallback = 'true';
            updateNavFilter<%=ctlFilters.ClientID %>(selectedNavNodeId + "," + navFilterId);
            //isTreeCallback = 'false'; // Todo: This we have to update in the updateNavFilter method
        }
    }
    


    function LoadFilterProductRelated(selectedNavNodeId) {
    
               var filterIds =GetNavNodProductRelationId(selectedNodeId);
               var filterGroup = filterIds.split("|");
               navFilterId ='';

               if(filterGroup.length >1)
               {
                FilterIdAndProdRelationType = filterGroup[0].split(",");

                if (FilterIdAndProdRelationType.length > 1) {
                    navFilterId = FilterIdAndProdRelationType[0];
                    FilterIdAndProdRelationType= FilterIdAndProdRelationType[1];
                    
                }
                }
    
        if (treeOperation != 'CreateNewNode') // Rename is going on; should not load the grid here. while rename the new node, we have to select the node, that time should not load the grid
        {
            isTreeCallback = 'true';
            updateNavFilter<%=ctlProdRelation.ClientID %>(selectedNavNodeId + "," + navFilterId);
            //isTreeCallback = 'false'; // Todo: This we have to update in the updateNavFilter method
        }
    }

    function ShowProducts(contextMenuData) {
        selectedNodeId = contextMenuData.get_id();
        var navFilterId = contextMenuData.getProperty('FilterId');
        adminpath = '../../Popups/StoreManager/PreviewProducts.aspx?selectedNodeId=' + selectedNodeId + '&navFilterId=' + navFilterId;
        pagepopup = dhtmlmodal.open('ShowProducts', 'iframe', adminpath, '', 'width=900px,height=480px,center=1,resize=0,scrolling=1');
        pagepopup.onclose = function() {
            var a = document.getElementById('ShowProducts');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function ConnectCMSPage(contextMenuData) {
        nodeId = contextMenuData.get_id();
        isTreeCallback = 'true';
        ShowPagePopup();
        isTreeCallback = 'false';
    }

    function ShowPagePopup() {
        OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
    }

    function SelectPage() {
        var relativeUrl = popupActionsJson.CustomAttributes["SelectedUrl"];
        selectedPageURL = relativeUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
        selectedPageId = popupActionsJson.SelectedItems.first();
        UpdateConnectedPageId(nodeId, selectedPageId);
        alert(stringformat("<asp:localize runat='server' text='<%$ Resources:GUIStrings, ThisNavNodeConnectedSuccessfullyTo0 %>'/>", popupActionsJson.CustomAttributes["SelectedUrl"]));
        return true;
    }

    function setNavFilterId(filterId) {
        selectedTreeNode.setProperty('FilterId', filterId);
    }

    function ToggleTree() {
        var treeHandle = document.getElementById('ToggleHandle');
        if (treeHandle != null) {
            if (treeHandle.className == 'expand') {
                treeNavigation.expandAll();
                treeHandle.className = 'collapse';
            }
            else if (treeHandle.className == 'collapse') {
                treeNavigation.collapseAll();
                treeHandle.className = 'expand';
            }
        }
        return false;
    }

    function tbsNavigationFilters_onTabBeforeSelect(sender, eventArgs) {
        if (selectedTreeNode != null) {
            var navFilterId = selectedTreeNode.getProperty('FilterId');
            var selectedTabId = eventArgs.get_tab().get_id();
            switch (selectedTabId) {
                case 'tabProducts':
                    setTimeout(function() {

                        grdPreviewProducts.set_callbackParameter(navFilterId);
                        grdPreviewProducts.callback();
                    }, 500);
                    break;
                case 'tabFilter':
                    LoadFilter(selectedNodeId, navFilterId);
                    break;
                case 'tabFacets':
                    loadNavFacetGrid();
                    break;
                case 'tabDetails':
                    UpdateNavCategoryDetails(selectedNodeId);
                    break;
                case "tabProdRelation":
                LoadFilterProductRelated(selectedNodeId);
                break;
            }
        }
        else {
            eventArgs.set_cancel(true);
        }

    }


    </script>
    <script type="text/javascript">
        function CloseConnectPage() {
            mnuNavigation.hide();
            return false;
        }

        function ValidateCatName(catName, isCurrentNode) {
            var errorMsg = "";
            if (catName == "") {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CategoryNameCouldNotBeEmpty %>'/> \n";

            }
            else {
                if (!catName.match(/^[a-zA-Z0-9. _'\-<>.&$!|#:,]+$/)) {
                    if (IsNameAscii(catName) != 'false') {
                        errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, NodeNameCanHaveOnlyAlphanumericCharactersAndPleaseReenter %>'/> \n";

                    }
                }

                //            if(catName.match(/<([\S\s]*?)>/i))
                //            {
                //                if(!catName.match(/<b[^>]*>([\S\s]*?)<\/b>/i) && !catName.match(/<i[^>]*>([\S\s]*?)<\/i>/i) && !catName.match(/<strong[^>]*>([\S\s]*?)<\/string>/i) && !catName.match(/<em[^>]*>([\S\s]*?)<\/em>/i))
                //                {
                //                    errorMsg += "Node name can have only <b>, <i>, <em> and <strong> tags with proper ending."; 
                //                }
                //            } 

                var regExpResult = catName.match(/([\<])([^\>]{1,})*([\>])/gi);
                if (regExpResult != null) {
                    var isHtmlTag = false;
                    var currentVal;
                    for (i = 0; i < regExpResult.length; i++) {
                        currentVal = regExpResult[i].toLowerCase();

                        if (currentVal != '<b>' && currentVal != '</b>' && currentVal != '<i>' && currentVal != '</i>' && currentVal != '<strong>' && currentVal != '</strong>' && currentVal != '<em>' && currentVal != '</em>') {
                            isHtmlTag = true;
                        }

                    }

                    if (isHtmlTag == true) {
                        errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, NodeNameCanHaveOnlybiemAndstrongTags %>'/>";
                    }

                }

                var canCreate = true;
                if (isCurrentNode) {
                    canCreate = CheckForDuplicate(selectedTreeNode, Trim(catName).replace(/</g, "&lt;"), false);
                }
                else {
                    canCreate = CheckForDuplicatesWithParentNode(selectedTreeNode, Trim(catName).replace(/</g, "&lt;"), false);
                }

                if (canCreate == false) {
                    errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, DuplicateNameInThisLevelPleaseChooseADifferentName %>'/> \n";
                }
            }

            return errorMsg;
        }

        function CreateCategoryAndPage(catName, selectedTemplateId) {
            //alert('test');
            //var currentNode = treeNavigation.findNodeById(selectedNodeId);



            var errorMsg = ValidateCatName(catName, false);

            if (selectedTemplateId == "") {
                if (errorMsg != "") {
                    errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseSelectATemplate %>'/> \n";
                }
                else {
                    errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseSelectATemplate %>'/> \n";
                }

            }

            if (errorMsg != '') {
                alert(errorMsg);
            }
            else {
                var catId = CreateNavNodeAndConnectNewPage(selectedTreeNode.get_id(), catName, selectedTemplateId);

                treeNavigation.beginUpdate();

                var subNode = new ComponentArt.Web.UI.TreeViewNode();
                subNode.set_text(catName.replace(/</g, "&lt;"));
                subNode.set_id(catId);

                subNode.setProperty("CssClass", "GrayedTreeNode");
                subNode.setProperty("HoverCssClass", "GrayedTreeNode");
                subNode.setProperty("SelectedCssClass", "SelectedGrayedTreeNode");
                subNode.set_toolTip("Invisible");

                selectedTreeNode.get_nodes().add(subNode);
                treeNavigation.endUpdate();
                treeNavigation.render();
                mnuNavigation.hide();
            }


        }
    </script>
</asp:Content>
<asp:Content ID="navigationBuilderContent" ContentPlaceHolderID="mainPlaceHolder"
    runat="server">
    <div class="navigation-categores clear-fix">
        <h1 class="clear-fix" style="width: 100%;">
            <asp:Label ID="subPageHeader" runat="server" Text="<%$ Resources:GUIStrings, ManageNavigationCategories %>"></asp:Label></h1>
        <!-- Left Column STARTS -->
        <div class="left-column">
            <div class="tree-header">
                <a href="#" onclick="return ToggleTree();" class="expand" id="ToggleHandle">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExpandTree %>" /></a>
            </div>
            <div class="tree-container">
                <ComponentArt:CallBack ID="cbTreeRefresh" runat="server" OnCallback="cbRefreshTree_onCallBack"
                    CacheContent="false">
                    <Content>
                        <ComponentArt:TreeView ID="treeNavigation" runat="server" SkinID="Default" AutoScroll="false"
                            FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
                            <ClientEvents>
                                <ContextMenu EventHandler="tree_onContextMenu" />
                                <NodeBeforeRename EventHandler="tree_BeforeRename" />
                                <NodeBeforeSelect EventHandler="tree_onNodeBeforeSelect" />
                                <NodeSelect EventHandler="tree_onNodeSelect" />
                                <Load EventHandler="tree_Load" />
                            </ClientEvents>
                        </ComponentArt:TreeView>
                    </Content>
                    <LoadingPanelClientTemplate>
                    </LoadingPanelClientTemplate>
                </ComponentArt:CallBack>
                <asp:XmlDataSource ID="CMDataSource" runat="server" DataFile="<%$ Resources:GUIStrings, Xml_MenuDataURL %>">
                </asp:XmlDataSource>
                <ComponentArt:Menu ID="mnuNavigation" runat="server" SkinID="ContextMenu" ExpandOnClick="true">
                    <%--DataSourceID ="CMDataSource">--%>
                    <ServerTemplates>
                        <ComponentArt:NavigationCustomTemplate ID="ExistingTemplates">
                            <Template>
                                <cp:ConnectPage ID="ctlConnectPage" runat="server"></cp:ConnectPage>
                            </Template>
                        </ComponentArt:NavigationCustomTemplate>
                    </ServerTemplates>
                </ComponentArt:Menu>
            </div>
        </div>
        <!-- Left Column ENDS -->
        <!-- Right Column STARTS -->
        <div class="right-column">
            <div style="padding: 10px 10px 0 0;">
                <ComponentArt:TabStrip ID="tbsNavigationFilters" runat="server" SkinID="Default"
                    MultiPageId="mltpNavigation">
                    <Tabs>
                        <ComponentArt:TabStripTab ID="tabProducts" runat="server" Text="<%$ Resources:GUIStrings, Products %>">
                        </ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabFilter" runat="server" Text="<%$ Resources:GUIStrings, Filters %>">
                        </ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabFacets" runat="server" Text="<%$ Resources:GUIStrings, Facets %>">
                        </ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabDetails" runat="server" Text="<%$ Resources:GUIStrings, Details %>">
                        </ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabProdRelation" runat="server" Text="<%$ Resources:GUIStrings, RelatedProducts %>">
                        </ComponentArt:TabStripTab>
                    </Tabs>
                    <ClientEvents>
                        <TabBeforeSelect EventHandler="tbsNavigationFilters_onTabBeforeSelect" />
                    </ClientEvents>
                </ComponentArt:TabStrip>
            </div>
            <div style="border-top: solid 1px #d2d2d2; position: relative; top: -1px;">
                <ComponentArt:MultiPage ID="mltpNavigation" runat="server">
                    <ComponentArt:PageView ID="pvPreview" runat="server">
                        <h3>
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PreviewProducts %>" /></h3>
                        <pp:ProductsPreview ID="ctlProductsPreview" runat="server" />
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pvFilters" runat="server">
                        <h3>
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Filters %>" /></h3>
                        <ft:Filters ID="ctlFilters" runat="server" />
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pvFacets" runat="server">
                        <fc:Facets ID="ctlNavFacets" runat="server" />
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pvDetails" runat="server">
                        <div class="content-grid">
                            <nd:NavDetails ID="ctlNavDetails" runat="server" />
                        </div>
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pvProdRelation" runat="server">
                        <h3>
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RelatedProducts %>" /></h3>
                        <ft:Filters ID="ctlProdRelation" runat="server" />
                    </ComponentArt:PageView>
                </ComponentArt:MultiPage>
            </div>
        </div>
        <!-- Right Column ENDS -->
        <asp:SiteMapDataSource ID="SiteMapDSNav" runat="server" SiteMapProvider="folderNavProvider"
            ShowStartingNode="True" />
    </div>
    <script type="text/javascript">
<%=cbTreeRefresh.ClientID%>.LoadingPanelClientTemplate = '<table width="200" height="265" cellspacing="0" cellpadding="0" border="0"><tr><td align="center"><table cellspacing="0" cellpadding="0" border="0"><tr><td class="imageLibraryLoading"><%= GUIStrings.Loading %></td><td><img src ="../../images/spinner.gif" width="16" height="16" border="0"></td></tr></table></td></tr></table>';
    </script>
</asp:Content>
