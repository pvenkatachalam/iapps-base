﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersManagerCustomerGroups %>"
    Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerMaster.master"
    AutoEventWireup="true" CodeBehind="CustomerGroupDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CustomerGroupDetails"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="GM" TagName="GroupMembers" Src="~/UserControls/StoreManager/Customers/GroupMembers.ascx" %>
<%@ Register TagPrefix="iTerms" TagName="IndexTerms" Src="~/UserControls/StoreManager/Index/AssignIndexTermsWithListbox.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">   
    <script type="text/javascript">
        var confirmMsg = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, AreYouSureYouWantToClearTheForm %>'/>";
        function populateHiddenIndexList(sender, eventArgs) {
            var listBox = document.getElementById(ListBoxName);
            var listcount = listBox.options.length;
            var valuetohiddenfield = "";
            var texttohiddenfield = "";
            for (i = 0; i < listcount; i++) {
                valuetohiddenfield += listBox.options[i].value + ",";
                texttohiddenfield += listBox.options[i].text + ",";
            }
            document.getElementById(TaxonomyIds).value = valuetohiddenfield;
            document.getElementById(IndexTextClientId).value = texttohiddenfield;
        }
        function searchGrdBtn() {
            var selGrid = mltpGroups.get_selectedPage();
            if (selGrid.get_id() == "pgvActive") {
                onClickSearchonGrid('txtSearchGroups', grdActivatedGroups);
            }
            else if (selGrid.get_id() == "pgvDeactivated") {
                onClickSearchonGrid('txtSearchGroups', grdDeActivatedGroups);
            }
        }
        function searchGrd() {
            var searchText = document.getElementById(SearchClientId).value;
            var selGrid = mltpGroups.get_selectedPage();
            if (selGrid.get_id() == "pgvActive") {
                //Loading grid if data already exists
                if (activeTableData) {
                    grdActivatedGroups.get_table().Data = activeTableData;
                    grdActivatedGroups.get_table().Grid.Data = activeTableData;
                    grdActivatedGroups.render();
                    activeTableData = null;
                }

                grdActivatedGroups.Search(searchText, false);
                //if record count is 0 placing the data in tableData
                if (grdActivatedGroups.RecordCount == 0 && activeTableData == null) {
                    activeTableData = grdActivatedGroups.get_table().get_data();
                    grdActivatedGroups.get_table().ClearData();
                    grdActivatedGroups.render();
                }
            }
            else if (selGrid.get_id() == "pgvDeactivated") {

                //Loading grid if data already exists
                if (deactiveTableData) {
                    grdDeActivatedGroups.get_table().Data = deactiveTableData;
                    grdDeActivatedGroups.get_table().Grid.Data = deactiveTableData;
                    grdDeActivatedGroups.render();
                    deactiveTableData = null;
                }

                grdDeActivatedGroups.Search(searchText, false);
                //if record count is 0 placing the data in tableData
                if (grdDeActivatedGroups.RecordCount == 0 && deactiveTableData == null) {
                    deactiveTableData = grdDeActivatedGroups.get_table().get_data();
                    grdDeActivatedGroups.get_table().ClearData()
                    grdDeActivatedGroups.render();
                }
            }
        }

        function validateGroupName(oSrc, args) {
            var GroupPattern = /^[a-zA-Z0-9 ]+$/

            if (args.Value.search(GroupPattern) == -1 || args.Value.length > 256) // If match failed
                args.IsValid = false;
            else
                args.IsValid = true;

        }       
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="CustomerContentHolder" runat="server">
     <script type="text/javascript" src="../../script/StoreManager/Customer/AddModifyGroup.js""></script>
     <div class="customer-groups">
        <div class="left-column">
            <h3>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SearchSelectGroup %>" /></h3>
            <div class="grid-utility">
                <input type="text" runat="server" id="txtSearchGroups" class="textBoxes" title="<%$ Resources:GUIStrings, TypeHereToFilter %>"
                    style="width: 200px;" />
                <asp:Button ID="btnFilter" runat="server" Text="<%$ Resources:GUIStrings, Go %>"
                    ToolTip="<%$ Resources:GUIStrings, FilterCustomerGroups %>" CssClass="small-button"
                    OnClientClick="searchGrdBtn(); return false;" />
            </div>
            <div class="grid-container">
                <ComponentArt:Menu ID="modifyDeleteMenu" runat="server" SkinID="ContextMenu" Width="140">
                    <ClientEvents>
                        <ItemSelect EventHandler="group_onItemSelect" />
                    </ClientEvents>
                </ComponentArt:Menu>
                <ComponentArt:TabStrip ID="tbsGroups" runat="server" SkinID="Default" MultiPageId="mltpGroups">
                    <Tabs>
                        <ComponentArt:TabStripTab ID="tabActiveGroup" Text="<%$ Resources:GUIStrings, Active %>">
                        </ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabDeactivatedGroup" Text="<%$ Resources:GUIStrings, Deactivated %>">
                        </ComponentArt:TabStripTab>
                    </Tabs>
                    <ClientEvents>
                        <TabSelect EventHandler="TabSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:TabStrip>
                <ComponentArt:MultiPage ID="mltpGroups" runat="server">
                    <ComponentArt:PageView ID="pgvActive" runat="server">
                        <!-- Active Groups Starts -->
                        <div class="scroll-box">
                            <ComponentArt:Grid EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" ID="grdActivatedGroups"
                                EditOnClickSelectedItem="false" SkinID="Default" AllowEditing="true" ShowHeader="False"
                                PageSize="12" ShowFooter="false" Width="292" GroupingNotificationPosition="TopRight" runat="server"
                                AutoCallBackOnUpdate="false" RunningMode="callback" AllowMultipleSelect="false"
                                CallbackCachingEnabled="true" CallbackCacheLookAhead="10" Height="255">
                                <ClientEvents>
                                    <ContextMenu EventHandler="modifyDeleteGroup_onContextMenu" />
                                    <ItemSelect EventHandler="modifyDeleteGroupItem_onSelect" />
                                    <ItemBeforeSelect EventHandler="modifyDeleteGroupItem_beforeSelect" />
                                    <BeforeCallback EventHandler="CheckSessionEventHandler" />
                                    <ItemBeforeInsert EventHandler="CheckSessionEventHandler" />
                                    <ItemBeforeUpdate EventHandler="CheckSessionEventHandler" />
                                    <ItemBeforeDelete EventHandler="CheckSessionEventHandler" />
                                </ClientEvents>
                                <Levels>
                                    <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                                        DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                                        AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                        SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                                        ShowSelectorCells="false" ShowHeadingCells="false">
                                        <Columns>
                                            <ComponentArt:GridColumn AllowEditing="false" Align="left" DataField="Id"
                                                Visible="false" IsSearchable="false" />
                                            <ComponentArt:GridColumn EditControlType="Default" AllowReordering="False" DataField="Title"
                                                DataCellCssClass="LastDataCell" Width="262" IsSearchable="true" DataCellClientTemplateId="TitleRowToolTip" />
                                        </Columns>
                                    </ComponentArt:GridLevel>
                                </Levels>
                                <ClientTemplates>
                                    <ComponentArt:ClientTemplate ID="TitleRowToolTip">
                                        <span title="## ChangeSpecialCharacters(DataItem.getMember('Title').get_text()) ##">
                                            ## ChangeSpecialCharacters(DataItem.getMember('Title').get_text()) == "" ? "&nbsp;"
                                            : ChangeSpecialCharacters(DataItem.getMember('Title').get_text()) ## </span>
                                    </ComponentArt:ClientTemplate>
                                </ClientTemplates>
                            </ComponentArt:Grid>
                        </div>
                        <!-- Active Groups Ends -->
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pgvDeactivated" runat="server">
                        <!-- Deactivated Groups Starts -->
                        <div class="scroll-box">
                            <ComponentArt:Grid ID="grdDeActivatedGroups" EditOnClickSelectedItem="false" SkinID="Default"
                                AllowEditing="true" ShowHeader="False" PageSize="12" ShowFooter="false"
                                Width="292" SearchText="" GroupingNotificationText="" GroupingNotificationPosition="TopRight"
                                runat="server" CallbackCachingEnabled="true" CallbackCacheLookAhead="10" RunningMode="callback"
                                EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" AllowMultipleSelect="false">
                                <ClientEvents>
                                    <ContextMenu EventHandler="modifyDeleteGroup_onContextMenu" />
                                    <ItemSelect EventHandler="modifyDeleteGroupItem_onSelect" />
                                    <BeforeCallback EventHandler="CheckSessionEventHandler" />
                                    <ItemBeforeInsert EventHandler="CheckSessionEventHandler" />
                                    <ItemBeforeUpdate EventHandler="CheckSessionEventHandler" />
                                    <ItemBeforeDelete EventHandler="CheckSessionEventHandler" />
                                </ClientEvents>
                                <Levels>
                                    <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                                        DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                                        AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                        SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                                        ShowSelectorCells="false" ShowHeadingCells="false">
                                        <Columns>
                                            <ComponentArt:GridColumn AllowEditing="false" Align="left" DataField="Id"
                                                Visible="false" IsSearchable="false" />
                                            <ComponentArt:GridColumn EditControlType="Default" AllowReordering="False" DataField="Title"
                                                DataCellCssClass="LastDataCell" Width="262" IsSearchable="true" DataCellClientTemplateId="TitlesRowToolTip" />
                                        </Columns>
                                    </ComponentArt:GridLevel>
                                </Levels>
                                <ClientTemplates>
                                    <ComponentArt:ClientTemplate ID="TitlesRowToolTip">
                                        <span title="## ChangeSpecialCharacters(DataItem.getMember('Title').get_text()) ##">
                                            ## ChangeSpecialCharacters(DataItem.getMember('Title').get_text()) == "" ? "&nbsp;"
                                            : ChangeSpecialCharacters(DataItem.getMember('Title').get_text()) ## </span>
                                    </ComponentArt:ClientTemplate>
                                </ClientTemplates>
                            </ComponentArt:Grid>
                        </div>
                        <!-- Deactivated Groups Ends -->
                    </ComponentArt:PageView>
                </ComponentArt:MultiPage>
            </div>
        </div>
        <div class="right-column clear-fix">
            <h3>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectedGroup %>" /></h3>
            <div class="right-section">
                <ComponentArt:CallBack ID="ModifyDeleteGroupCallBack" runat="server">
                    <Content>
                        <div class="columns">
                            <div class="form-row clear-fix">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, GroupName1 %>" /></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtGroupName" runat="server" Width="200" CssClass="textBoxes"></asp:TextBox></div>
                            </div>
                            <div class="form-row clear-fix">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
                                <div class="form-value">
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="200"
                                        TextMode="MultiLine" Rows="6"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="form-row clear-fix">
                                <iTerms:IndexTerms ID="indexs" runat="server" />
                            </div>
                            <div class="form-row clear-fix">
                                <label class="form-label">
                                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SecurityLevel1 %>" /><br />
                                    <asp:label runat="server" CssClass="coaching-text" Text="<%$ Resources:GUIStrings, HoldCtrlToSelectMultiple %>" />
                                </label>
                                <div class="form-value">
                                    <asp:ListBox ID="lstSecurityLevels" runat="server" Width="204" Rows="4" SelectionMode="Multiple">
                                        <asp:ListItem Text="<%$ Resources:GUIStrings, Customer %>"></asp:ListItem>
                                    </asp:ListBox>
                                </div>
                            </div>
                        </div>
                        <div class="clear-fix"></div>
                    </Content>
                    <ClientEvents>
                        <BeforeCallback EventHandler="CheckSessionEventHandler" />
                        <CallbackComplete EventHandler="GroupCallBack_Completed" />
                    </ClientEvents>
                </ComponentArt:CallBack>
                <div class="bottom-section">
                    <GM:GroupMembers ID="ctlGroupMembers" runat="server" />
                    <div class="clear-fix">&nbsp;</div>
                </div>
                <div class="footerContent">
                    <input type="hidden" runat="server" value="" id="IndexTermsList" />
                    <input type="hidden" runat="server" value="" id="IndexTermsText" />
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, ClearForm %>"
                        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
                        OnClientClick="var yesOrNo = window.confirm(confirmMsg);if(yesOrNo==false){return false;}else{Group_ClearForm();return false;}" />
                    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                        OnClientClick="var selItems=grdActivatedGroups.getSelectedItems();if(selItems.length<=0){alert('Please select a group');return false;}else{populateHiddenIndexList();}" />
                </div>
            </div>
        </div>
        <input type="hidden" runat="server" id="SelectedGroupId" />
        <asp:HiddenField ID="HdnSelectedTab" runat="server" Value="0" />
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" HeaderText=""
            DisplayMode="List" ShowMessageBox="True" />
        <asp:RequiredFieldValidator CultureInvariantValues="true" ID="RFVGroupName" runat="server"
            Display="None" SetFocusOnError="true" ControlToValidate="txtGroupName" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterANameForThisGroup %>"></asp:RequiredFieldValidator>
        <asp:CustomValidator ID="groupNameCheckCustomValidator" runat="server" ControlToValidate="txtGroupName"
            Display="None" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidGroupNameItShouldContainOnlyAlphanumericCharactersWithSpace %>"
            ClientValidationFunction="validateGroupName">
        </asp:CustomValidator>
        <asp:CustomValidator ID="DescriptionValidator" ClientValidationFunction="validator"
            runat="server" ControlToValidate="txtDescription" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDescription %>"
            Display="None" SetFocusOnError="true"></asp:CustomValidator>
        <script type="text/javascript">
            document.getElementById(SaveBtnClientId).disabled = true;
            document.getElementById(SaveBtnClientId).className += " disabled";
        </script>
    </div>
</asp:Content>
