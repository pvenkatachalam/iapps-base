﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersCustomerOrders %>" Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="CustomerOrderList.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CustomerOrderList" StylesheetTheme="General" %>
<%@ Register TagPrefix="CO" TagName="Orders" Src="~/UserControls/StoreManager/Orders/OrderListing.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CustomerDetailsHolder" runat="server">
    <div class="customer-orders">
        <CO:Orders id="ctlOrderListing" runat="server"></CO:Orders>
    </div>
</asp:Content>
