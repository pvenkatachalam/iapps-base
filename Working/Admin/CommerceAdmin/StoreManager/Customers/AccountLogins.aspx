﻿<%@ Page Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" StylesheetTheme="General" CodeBehind="AccountLogins.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AccountLogins" Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersAccountLogins %>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CustomerDetailsHolder" runat="server">
    <script type="text/javascript">
        function SearchLoginsGrid() {
            var str = document.getElementById("txtFilter").value;
            if (str == "")
                alert("A");
            else
                grdAccountLogins.Search(str, true);
        }

        var selectedItem;
        var isApproved;
        var isLocked;

        function grdAccountLogins_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            selectedItem = eventArgs.get_item();
            grdAccountLogins.select(selectedItem, false);
            var selectedItemId = selectedItem.getMember("User.ProviderUserKey").get_text();
            isApproved = selectedItem.getMember("User.IsApproved").get_text();
            isLocked = selectedItem.getMember("User.IsLockedOut").get_text();
            menuItems = mnuAccountLogins.get_items();
            if (selectedItemId == '' || selectedItemId == null) {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd')
                        menuItems.getItem(i).Visible = true;
                    else
                        menuItems.getItem(i).Visible = false;
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmStatus') {
                        if (isApproved == 'true' && isLocked == 'false')
                            menuItems.getItem(i).set_text('<%= GUIStrings.MakeInactive %>');
                        else if (isApproved == 'false')
                            menuItems.getItem(i).set_text('<%= GUIStrings.MakeActive %>');
                        else if (isLocked == 'true')
                            menuItems.getItem(i).set_text('<%= GUIStrings.UnlockUser %>');
                    }
                }
            }
            mnuAccountLogins.showContextMenuAtEvent(e, selectedItemId);
        }
        function mnuAccountLogins_onItemSelect(sender, eventArgs) {
            var itemSelected = eventArgs.get_item().get_id();
            var emptyGuid = "00000000-0000-0000-0000-000000000000";
            var selectedItemId = selectedItem.getMember("User.ProviderUserKey").get_text();
            switch (itemSelected) {
                case "cmAdd":
                    ShowAccountLoginPopup(emptyGuid);
                    break;
                case "cmEdit":
                    ShowAccountLoginPopup(selectedItemId);
                    break;
                case "cmStatus":
                    if (isApproved == 'true' && isLocked == 'false') {

                        var status = MakeUserInactive(selectedItemId);
                        if (status == 'True')
                            RefreshGrid();
                    }
                    else if (isApproved == 'false') {
                        var status = MakeUserActive(selectedItemId);
                        if (status == 'True')
                            RefreshGrid();
                    }
                    else if (isLocked == 'true') {
                        var status = UnlockUser(selectedItemId);
                        if (status == 'True')
                            RefreshGrid();
                    }
                    break;
                case "cmReset":
                    if (confirm("<asp:localize runat='server' text='<%$ Resources:GUIStrings, AreYouSureYouWantToResetThePassword %>'/>")) {
                        var status = ResetPassword(selectedItemId);
                        if (status == 'True')
                            alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, PasswordResetSuccessful %>'/>");
                    }
                    break;
            }
        }
        function ShowAccountLoginPopup(objUserId) {
            popupPath = '../../Popups/StoreManager/AddEditLogin.aspx?userId=' + objUserId;
            accountLoginPopup = dhtmlmodal.open('AddEditLogin', 'iframe', popupPath, '', 'width=500px,height=430px,center=1,resize=0,scrolling=0');
            accountLoginPopup.onclose = function () {
                var a = document.getElementById('AddEditLogin');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("blank.html");
                return true;
            }
        }

        function RefreshGrid() {
            grdAccountLogins.set_callbackParameter(document.getElementById("<%=chkShowInactive.ClientID%>").checked);
            grdAccountLogins.Callback();
        }
    </script>
    <div class="customer-logins">
        <div class="grid-utility">
            <div class="columns">
                <input type="text" title="<%= GUIStrings.TypeHereToFilterResults %>" style="width:260px;" class="textBoxes" id="txtFilter" />
                <input type="button" value=<%= GUIStrings.Filter %> class="small-button" onclick="SearchLoginsGrid();" />
            </div>
            <div class="columns" style="float:right;">
                <asp:CheckBox ID="chkShowInactive" runat="server" Text="<%$ Resources:GUIStrings, ShowInactiveLogins %>"
                    AutoPostBack="true" OnCheckedChanged="chkShowInactive_CheckedChanged" />
            </div>
            <div class="clear-fix"></div>
        </div>
        <ComponentArt:Grid ID="grdAccountLogins" SkinID="Default" runat="server" RunningMode="Callback"
            PagerInfoClientTemplateId="AccountLoginsPageInfoTemplate" AllowEditing="true"
            AutoCallBackOnInsert="false" CallbackCachingEnabled="true" AutoCallBackOnUpdate="false"
            CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" AllowMultipleSelect="false"
            EditOnClickSelectedItem="false" LoadingPanelClientTemplateId="grdAccountLoginsLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="User.ProviderUserKey" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                    ShowSelectorCells="false">
                    <Columns>
                        <ComponentArt:GridColumn Width="195" HeadingText="<%$ Resources:GUIStrings, FirstName %>"
                            AllowReordering="false" FixedWidth="true" IsSearchable="true" DataField="UserProfile.FirstName" />
                        <ComponentArt:GridColumn Width="191" HeadingText="<%$ Resources:GUIStrings, LastName %>"
                            DataField='UserProfile.LastName' FixedWidth="true" AllowReordering="false" IsSearchable="true" />
                        <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, UserId %>"
                            DataField="User.UserName" FixedWidth="true" AllowReordering="false" IsSearchable="true" />
                        <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Status %>"
                            DataField="User.IsApproved" DataCellClientTemplateId="StatusTemplate" 
                            FixedWidth="true" AllowReordering="false" IsSearchable="true" />
                        <ComponentArt:GridColumn Width="154" HeadingText="<%$ Resources:GUIStrings, LastLogin %>"
                            DataField="User.LastLoginDate" DataCellClientTemplateId="LastLoginTemplate" 
                            FixedWidth="true" AllowReordering="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="User.ProviderUserKey" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="User.IsLockedOut" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <ContextMenu EventHandler="grdAccountLogins_onContextMenu" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="StatusTemplate">
                    ## DataItem.getMember('User.IsApproved').get_text() == 'true' && DataItem.getMember('User.IsLockedOut').get_text()
                    =='false' ? 'Active' :'' ## ## DataItem.getMember('User.IsApproved').get_text()
                    == 'false' ? 'Inactive' :'' ## ## DataItem.getMember('User.IsLockedOut').get_text()
                    == 'true' ? 'Locked' :'' ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="LastLoginTemplate">
                    ## DataItem.getMember('User.LastLoginDate').get_text() == '1/1/0001 12:00:00 AM'
                    ? '' : DataItem.getMember('User.LastLoginDate').get_text() ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                    <a href="javascript:SaveRecord();">
                        <img src="/iapps_images/cm-icon-add.png" border="0" runat="server" alt="<%$ Resources:GUIStrings, Save %>"
                            title="<%$ Resources:GUIStrings, Save %>" />
                    </a>
                    <label>
                        |
                    </label>
                    <a href="javascript:CancelUpdate();">
                        <img src="/iapps_images/cm-icon-delete.png" border="0" runat="server"
                            alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>" />
                    </a>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="AccountLoginsPageInfoTemplate">
                    ##stringformat(Page0Of1Items, currentPageIndex(grdAccountLogins), pageCount(grdAccountLogins),
                    grdAccountLogins.RecordCount)##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdAccountLoginsLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdAccountLogins) ##
            </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
        <ComponentArt:Menu ID="mnuAccountLogins" runat="server" SkinID="ContextMenu">
            <Items>
                <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddLogin %>"
                    Look-LeftIconUrl="cm-icon-add.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="cmEdit" Text="<%$ Resources:GUIStrings, EditLogin %>"
                    Look-LeftIconUrl="cm-icon-edit.png">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="cmStatus" Text="<%$ Resources:GUIStrings, MakeActive %>">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem LookId="BreakItem">
                </ComponentArt:MenuItem>
                <ComponentArt:MenuItem ID="cmReset" Text="<%$ Resources:GUIStrings, ResetPassword %>"
                    AutoPostBackOnSelect="true">
                </ComponentArt:MenuItem>
            </Items>
            <ClientEvents>
                <ItemSelect EventHandler="mnuAccountLogins_onItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
    </div>
</asp:Content>
