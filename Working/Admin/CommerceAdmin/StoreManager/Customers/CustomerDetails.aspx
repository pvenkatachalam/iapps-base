﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersManageCustomerDetails %>"
    Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="CustomerDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CustomerDetails"
    Theme="General" EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register TagPrefix="GA" TagName="GlobalAttributes" Src="~/UserControls/StoreManager/GlobalLevelAttributes.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    //Birth date calendar methods
    function popUpBirthdateCalendar() {
        var thisDate = iAppsCurrentLocalDate;
        if (document.getElementById("<%=hdnBirthDate.ClientID%>").value != "") {
            thisDate = new Date(document.getElementById("<%=hdnBirthDate.ClientID%>").value);
            <%=calBirthDate.ClientID%>.setSelectedDate(thisDate);
        }

        if (!(calBirthDate.get_popUpShowing()));
            calBirthDate.show();
    }

    function calBirthDate_onSelectionChanged(sender, eventArgs) {
        var selectedDate = calBirthDate.getSelectedDate();
        document.getElementById("<%=txtBirthDate.ClientID%>").value = calBirthDate.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
        document.getElementById("<%=hdnBirthDate.ClientID%>").value = calBirthDate.formatDate(selectedDate, calendarDateFormat);
    }

var isUserNameAsEmail;
var isNewUser;
var customerId;

function ValidateCustomerDetails() {
    var errorMsg = "";
    var firstName =Trim(document.getElementById("<%=txtFirstName.ClientID %>").value);
    var isExpress =Trim(document.getElementById("<%=HdnIsExpress.ClientID %>").value);
    if(firstName == "" && isExpress == "0")
    {
        errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, FirstNameCouldNotBeEmpty %>'/> \n";              
    }    
    else {
        
        if (firstName != "" && firstName.match(/[\<\>]/))
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, FirstNameCanHaveOnlyAlphanumericCharactersAndSpace %>'/> \n";
        }
    }
    
    var lastName =Trim(document.getElementById("<%=txtLastName.ClientID %>").value);
    if(lastName == "" && isExpress == "0")
    {
        errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, LastNameCouldNotBeEmpty %>'/> \n";              
    }    
    else
    {
        if (lastName != "" && lastName.match(/[\<\>]/))
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, LastNameCanHaveOnlyAlphanumericCharactersAndSpace %>'/> \n";
        }
    }
    
    var companyName =  Trim(document.getElementById("<%=txtCompanyName.ClientID %>").value);
    if(companyName != '')
    {
        if(companyName.match(/[\<\>]/))
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheCompanyName %>'/> \n";
        }
        else if(companyName.indexOf('&#0;') >=0 )
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacter %>'/> \n"; 
        }    
    }
    
    var userNameControl = document.getElementById("<%=txtUserName.ClientID %>");
    if(userNameControl !=null && userNameControl.disabled !=true )
    {
        var userName =Trim(userNameControl.value);
        if(userName == "")
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, UserNameCouldNotBeEmpty %>'/> \n";              
        }    
        else
        {
            if (userName.match(/[\<\>'"]/))
            {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, UserNameCanHaveOnlyAlphanumericCharactersAndSpace %>'/> \n";
            }
        }
    }
    
    var birthDate =Trim(document.getElementById("<%=txtBirthDate.ClientID %>").value);
    if(birthDate != '')
    {
//        if(!birthDate.match(/^(?=\d)(?:(?:(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})|(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))|(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2}))($|\ (?=\d)))?(((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$/))
//        {
//            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterValidBirthDateAsMonth %>'/> \n";    
        //        }   

//        var validateddate = iAppsCurrentLocalDate;
//        try {
//                validateddate = calBirthDate.formatDate(document.getElementById("<%=txtBirthDate.ClientID%>").value, shortCultureDateFormat);
//                }catch(err){}

//            if (validateddate == iAppsCurrentLocalDate)
//                errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterValidBirthDateAsMonth %>'/> \n"; 
    }    
    
    
    
    var email =  Trim(document.getElementById("<%=txtEmail.ClientID %>").value);
    var emailText = document.getElementById("<%=lblEmail.ClientID %>").innerHTML;
   
    //if(isUserNameAsEmail !=null && isUserNameAsEmail =='True')
    if (email == '') errorMsg += stringformat("<asp:localize runat='server' text='<%$ Resources:GUIStrings, CouldNotBeEmpty %>'/> \n", emailText);
    
    if(email !='')
    {
        if(!email.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/))
        {
            errorMsg += stringformat("<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAValidEmailIn0Field %>'/> \n", emailText);
        }
    }

    var txtHomePhone = Trim(document.getElementById("<%=txtHomePhone.ClientID %>").value);
    if (txtHomePhone != "" && !txtHomePhone.match(/^[a-zA-Z0-9() +-.]+$/)) errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAValidHomePhoneNumber %>'/> \n";

    var txtMobilePhone = Trim(document.getElementById("<%=txtMobilePhone.ClientID %>").value);
    if (txtMobilePhone != "" && !txtMobilePhone.match(/^[a-zA-Z0-9() +-.]+$/)) errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAValidMobilePhoneNumber %>'/> \n";

    var txtOtherPhone = Trim(document.getElementById("<%=txtOtherPhone.ClientID %>").value);
    if (txtOtherPhone != "" && !txtOtherPhone.match(/^[a-zA-Z0-9() +-.]+$/)) errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAValidOtherPhoneNumber %>'/> \n";
    
    
    var secQuestion =  Trim(document.getElementById("<%=txtSecurityQuestion.ClientID %>").value);
    if(secQuestion != '')
    {
        if(secQuestion.match(/[\<\>]/))
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheSecurityQuestion %>'/> \n";    
        }
        else if(secQuestion.indexOf('&#0;') >=0 )
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacterSecurity %>'/> \n"; 
        }    
    }
    
    var secAnswer =  Trim(document.getElementById("<%=txtSecurityAnswer.ClientID %>").value);
    if(secAnswer != '')
    {
        if(secAnswer.match(/[\<\>]/))
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheSecurityAnswer %>'/> \n";    
        }
        else if(secAnswer.indexOf('&#0;') >=0 )
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacterItIsNotAllowedInSecurityAnswer %>'/> \n"; 
        }    
    }
    
    var passwordControl = document.getElementById("<%=txtInitialPassword.ClientID %>");
    if(passwordControl !=null)
    {
        var password =Trim(passwordControl.value);
    }
    
    if(isNewUser !=null && isNewUser =='True')
    {
//        var passwordControl = document.getElementById("<%=txtInitialPassword.ClientID %>");
//        if(passwordControl !=null)
//        {
//            var password =Trim(passwordControl.value);
            if(password == "")
            {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PasswordCouldNotBeEmpty %>'/> \n";              
            }  
        //}
    }
    if(password.match(/[\<\>]/))
    {
        errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromThePassword %>'/> \n";    
    }
    else if(password.indexOf('&#0;') >=0 )
    {
        errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacterItIsNotAllowedInPassword %>'/> \n"; 
    }
  
    var accNumber =  Trim(document.getElementById("<%=txtAccNumber.ClientID %>").value);
    if(accNumber != '')
    {
        if(accNumber.match(/[\<\>]/))
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheAccountNumber %>'/> \n";    
        }
        else if(accNumber.indexOf('&#0;') >=0 )
        {
            errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, IsUnicodeCombinationOfNewLineCharacterItIsNotAllowedInAccountNumber %>'/> \n"; 
        }    
    }
       
       
    if(customerId!="00000000-0000-0000-0000-000000000000")
    {
        var rbtnInactive= document.getElementById("<%=rbtnActiveNo.ClientID %>");
        if(rbtnInactive.checked == true)
        {
            var hasActiveorders = IsCustomerHasActiveOrders(customerId);
            
            if(hasActiveorders =='True')
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, YouCanNotMakeTheCustomerInactiveActiveOrdersAreAssociatedWithTheCustomer %>'/> \n";
        }
    }
    
    if(errorMsg!='')
    {
        alert(errorMsg);
        return false;
    }   
    else
    {
        if(Page_ClientValidate('CustomerDetails'))
            return true;
        else
            return false;    
    }
    
}

function GenerateGuid()
{   
    document.getElementById("<%=txtInitialPassword.ClientID%>").value =GeneratePassword()  ;
    return false;
}

        function ShowImagePopup() {
            OpeniAppsAdminPopup("SelectImageLibraryPopup", "ForceEnableSelection=true", "SelectImage");
            return false;
        }

        function SelectImage() {
            imageUrl = popupActionsJson.CustomAttributes.ImageUrl;
            imageFileName = popupActionsJson.CustomAttributes.ImageFileName;
            imageId = popupActionsJson.SelectedItems[0];

            imageUrl = imageUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
            imageUrl = imageUrl.replace(new RegExp(imageFileName, "gi"), "thumbs/thumb_" + imageFileName);
            $("#imgImageURL").prop("src", imageUrl);
            $("#hdnImageId").val(imageId);
        }

        function ClearImageUrlValue() {
            $("#imgImageURL").prop("src", "/iapps_images/no_photo.jpg");
            $("#hdnImageId").val("");

            return false;
        }

        $(document).ready(function () {
            if (!(hasCMSLicense && hasCMSPermission)) {
                $('div.#divPhoto').hide();
            }
        });

    </script>
</asp:Content>
<asp:Content ID="CustomerDetailsContent" ContentPlaceHolderID="CustomerDetailsHolder"
    runat="server">
    <div class="customer-details">
        <div class="button-row">
            <asp:ValidationSummary ShowSummary="false" ID="valSummary" runat="server" EnableClientScript="true"
                ValidationGroup="CustomerDetails" ShowMessageBox="true" />
            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClick="btnCancel_Click" />
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                ToolTip="<%$ Resources:GUIStrings, Save %>" CausesValidation="true" ValidationGroup="CustomerDetails"
                CssClass="primarybutton" OnClick="btnSave_Click" OnClientClick="return ValidateCustomerDetails();" />
        </div>
        <div class="columns">
            <div class="form-row profile-picture" id="divPhoto">
                <label class="form-label">
                    <asp:Image runat="server" ID="imgImageURL" ImageUrl="/iapps_images/no_photo.jpg"
                        ClientIDMode="Static" /></label>
                <div class="form-value">
                    <a href="#" class="edit-image" onclick="return ShowImagePopup();">
                        <asp:Localize ID="Localize21" runat="server" Text="<%$ Resources:SiteSettings, EditProfilePicture %>" /></a>
                    <a href="#" class="remove-image" onclick="return ClearImageUrlValue();">
                        <asp:Localize ID="Localize22" runat="server" Text="<%$ Resources:SiteSettings, Remove %>" /></a>
                    <asp:HiddenField ID="hdnImageId" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtFirstName.ClientID%>">
                    <asp:HiddenField runat="server" ID="HdnIsExpress" />
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, FirstName1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtLastName.ClientID%>">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, LastName1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator cultureinvariantvalues="true" ID="rfTxtLastName" runat="server"
                        ValidationGroup="CustomerDetails" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterLastName %>"
                        Text="*" ControlToValidate="txtLastName" EnableClientScript="true" Display="None"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtCompanyName.ClientID%>">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, CompanyName1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtBirthDate.ClientID%>">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, BirthDate1 %>" />
                </label>
                <div class="form-value calendar-value">
                    <asp:HiddenField ID="hdnBirthDate" runat="server" />
                    <asp:TextBox ID="txtBirthDate" runat="server" CssClass="textBoxes" Width="287"></asp:TextBox>
                    <span>
                        <img id="birthdate_calendar" class="buttonCalendar" runat="server" alt="<%$ Resources:GUIStrings, OpenCalendar %>"
                            src="../../App_Themes/General/images/calendar-button.png" onclick="popUpBirthdateCalendar(this);" />
                    </span>
                    <ComponentArt:Calendar runat="server" SkinID="Default" ID="calBirthDate" MaxDate="9998-12-31"
                        PickerFormat="Custom">
                        <ClientEvents>
                            <SelectionChanged EventHandler="calBirthDate_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Gender1 %>" />
                </label>
                <div class="form-value">
                    <asp:RadioButton ID="rbtnMale" runat="server" Text="<%$ Resources:GUIStrings, Male %>"
                        GroupName="Gender" Checked="true" />
                    <asp:RadioButton ID="rbtnFemale" runat="server" Text="<%$ Resources:GUIStrings, Female %>"
                        GroupName="Gender" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtHomePhone.ClientID%>">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, HomePhone1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtHomePhone" runat="server" CssClass="textBoxes" MaxLength="20"
                        Width="300"></asp:TextBox>
                    <asp:RegularExpressionValidator ControlToValidate="txtHomePhone" runat="server" ValidationGroup="CustomerDetails"
                        ID="revtxtPhone" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidHomePhoneNumber %>"
                        ValidationExpression="^[a-zA-Z0-9() +-.]+$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtMobilePhone.ClientID%>">
                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, MobilePhone1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtMobilePhone" runat="server" CssClass="textBoxes" MaxLength="20"
                        Width="300"></asp:TextBox>
                    <asp:RegularExpressionValidator ControlToValidate="txtMobilePhone" runat="server"
                        ValidationGroup="CustomerDetails" ID="RegularExpressionValidator1" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAMobileValidPhoneNumber %>"
                        ValidationExpression="^[a-zA-Z0-9() +-.]+$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtOtherPhone.ClientID%>">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, OtherPhone1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtOtherPhone" runat="server" CssClass="textBoxes" MaxLength="20"
                        Width="300"></asp:TextBox>
                    <asp:RegularExpressionValidator ControlToValidate="txtOtherPhone" runat="server"
                        ValidationGroup="CustomerDetails" ID="RegularExpressionValidator2" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidPhoneNumberForOtherPhone %>"
                        ValidationExpression="^[a-zA-Z0-9() +-.]+$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtAccNumber.ClientID%>">
                    <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, AccountNumber1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtAccNumber" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="columns">
            <div class="form-row">
                <label class="form-label" id="lblEmail" runat="server" for="<%=txtEmail.ClientID%>">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Email1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
                </div>
            </div>
            <div class="form-row" runat="server" id="spanUserName">
                <label class="form-label" for="<%=txtUserName.ClientID%>">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, UserName1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtUserName" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
                    <asp:RequiredFieldValidator cultureinvariantvalues="true" ID="rfTxtUserName" runat="server"
                        ValidationGroup="CustomerDetails" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterUserName %>"
                        Text="*" ControlToValidate="txtUserName" EnableClientScript="true" Display="None"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="form-row" id="spanPassword" runat="server">
                <input style="display: none" type="password" name="foilautofill" />
                <label class="form-label" for="<%=txtInitialPassword.ClientID%>">
                    <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, Password1 %>" />
                    <br />
                    <asp:LinkButton ID="lbtnGeneratePassword" runat="server" Text="<%$ Resources:GUIStrings, Generate %>"
                        ToolTip="<%$ Resources:GUIStrings, GeneratePassword %>" OnClientClick="return GenerateGuid();"></asp:LinkButton>
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtInitialPassword" runat="server" CssClass="textBoxes" Width="300"
                        TextMode="Password"></asp:TextBox>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtSecurityQuestion.ClientID%>">
                    <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, CSRSecurityQuestion1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtSecurityQuestion" runat="server" CssClass="textBoxes" Width="300"
                        autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtSecurityAnswer.ClientID%>">
                    <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, CSRSecurityAnswer1 %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtSecurityAnswer" runat="server" CssClass="textBoxes" Width="300"
                        autocomplete="off"></asp:TextBox>
                </div>
            </div>
            <div class="form-row checkbox-row">
                <label class="form-label" for="<%=chklstCustomerType.ClientID%>">
                    <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, CustomerGroup1 %>" />
                </label>
                <div class="form-value">
                    <asp:CheckBoxList ID="chklstCustomerType" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, IsBadCustomer1 %>" />
                </label>
                <div class="form-value">
                    <asp:RadioButton ID="rbtnYesBad" runat="server" Text="Yes" GroupName="BadCustomer" />
                    <asp:RadioButton ID="rbtnNoBad" runat="server" Text="No" GroupName="BadCustomer" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, IsActive1 %>" />
                </label>
                <div class="form-value">
                    <asp:RadioButton ID="rbtnActiveYes" runat="server" Text="<%$ Resources:GUIStrings, Yes %>"
                        GroupName="Active" Checked="true" />
                    <asp:RadioButton ID="rbtnActiveNo" runat="server" Text="<%$ Resources:GUIStrings, No %>"
                        GroupName="Active" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, IsLocked1 %>" />
                </label>
                <div class="form-value">
                    <asp:RadioButton ID="rbtnLockedYes" runat="server" Text="<%$ Resources:GUIStrings, Yes %>"
                        GroupName="Locked" />
                    <asp:RadioButton ID="rbtnLockedNo" runat="server" Text="<%$ Resources:GUIStrings, No %>"
                        GroupName="Locked" Checked="true" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, IsOnMailingList1 %>" />
                </label>
                <div class="form-value">
                    <asp:RadioButton ID="rbtnMailingOn" runat="server" Text="<%$ Resources:GUIStrings, Yes %>"
                        GroupName="MailingList" Checked="true" />
                    <asp:RadioButton ID="rbtnMailingOff" runat="server" Text="<%$ Resources:GUIStrings, No %>"
                        GroupName="MailingList" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize23" runat="server" Text="Is Approved:" />
                </label>
                <div class="form-value">
                    <asp:RadioButton ID="rbtnApprovedOn" runat="server" Text="<%$ Resources:GUIStrings, Yes %>"
                        GroupName="Approved" Checked="true" />
                    <asp:RadioButton ID="rbtnApprovedOff" runat="server" Text="<%$ Resources:GUIStrings, No %>"
                        GroupName="Approved" />
                </div>
            </div>
        </div>
        <div class="clear-fix">
        </div>
        <div id="globalAttributesContainer" class="snap-container">
            <ComponentArt:Snap ID="snapGlobalAttributes" runat="server" Width="100%" CurrentDockingContainer="globalAttributesContainer"
                DockingContainers="SKUContainer" DockingStyle="TransparentRectangle" MustBeDocked="true"
                DraggingMode="FreeStyle" DraggingStyle="SolidOutline" IsCollapsed="true">
                <Header>
                    <div class="snap-header">
                        <img id="Img1" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, CollpaseGlobalAttributesForCustomer %>"
                            src="~/App_Themes/General/images/snap-collapse.png" onclick="snapGlobalAttributes.toggleExpand();" />
                        <h3>
                            <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings, GlobalAttributes %>" />
                        </h3>
                        <div class="clear-fix">
                        </div>
                    </div>
                </Header>
                <CollapsedHeader>
                    <div class="snap-header">
                        <img id="Img2" class="snapToggle" runat="server" alt="<%$ Resources:GUIStrings, ExpandGlobalAttributesForCustomer %>"
                            src="~/App_Themes/General/images/snap-expand.png" onclick="snapGlobalAttributes.toggleExpand();" />
                        <h3>
                            <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:GUIStrings, GlobalAttributes %>" />
                        </h3>
                        <div class="clear-fix">
                        </div>
                    </div>
                </CollapsedHeader>
                <Content>
                    <div class="snap-content">
                        <GA:GlobalAttributes HighlightUnMappedAttribute="true" HighlighCssClass="unmappedAttributes"
                            ID="globalAttributes" runat="server" ValidationGroup="CustomerDetails" />
                    </div>
                </Content>
            </ComponentArt:Snap>
            <div class="clear-fix">
            </div>
        </div>
    </div>
</asp:Content>
