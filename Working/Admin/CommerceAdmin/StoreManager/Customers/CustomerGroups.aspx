﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersManagerCustomerGroups %>" Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerMaster.master"
    AutoEventWireup="true" CodeBehind="CustomerGroups.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CustomerGroups" StylesheetTheme="General" %>
<%@ Register TagPrefix="GM" TagName="GroupMembers" Src="~/UserControls/StoreManager/Customers/GroupMembers.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CustomerContentHolder" runat="server">
    <div class="topPaddingContent"></div>
    <div class="tabContent CustomerDetails">
        <h3><asp:Label ID="lblCustomerHeading" runat="server" Text="<%$ Resources:GUIStrings, ManageCustomerGroups %>"></asp:Label></h3>
        <div class="leftColumn" style="margin-left:5px;margin-bottom:5px;width:253px;">
            <div class="boxSubHeader">
                <h5><asp:localize runat="server" text="<%$ Resources:GUIStrings, SearchSelectGroup %>"/></h5>
            </div>
            <div class="gridContainer">
                <div class="KeyPressSearchContainer">
                    <input type="text" id="txtSearch" class="textBoxes" value="<%= GUIStrings.TypeHereToFilterResults %>" style="width:180px;float:left;"
                        onfocus="clearDefaultText(this);" onblur="setDefaultText(this);" />
                    <asp:Button ID="btnFilter" runat="server" Text="<%$ Resources:GUIStrings, Go %>" ToolTip="<%$ Resources:GUIStrings, FilterCustomerGroups %>" CssClass="button"
                        />
                </div>
                <ComponentArt:Menu ID="mnuGroups" runat="server" SkinID="ContextMenu">
                    <Items>
                        <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddGroup %>" Look-LeftIconUrl="cm-icon-add.png"></ComponentArt:MenuItem>
                        <ComponentArt:MenuItem LookId="BreakItem"></ComponentArt:MenuItem>
                        <ComponentArt:MenuItem ID="cmEdit" Text="<%$ Resources:GUIStrings, EditGroup %>" Look-LeftIconUrl="cm-icon-edit.png"></ComponentArt:MenuItem>
                        <ComponentArt:MenuItem LookId="BreakItem"></ComponentArt:MenuItem>
                        <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, DeleteGroup %>" Look-LeftIconUrl="cm-icon-delete.png"></ComponentArt:MenuItem>
                    </Items>
                </ComponentArt:Menu>
                <ComponentArt:TabStrip ID="tbsGroups" runat="server" SkinID="Default" MultiPageId="mltpGroups">
                    <Tabs>
                        <ComponentArt:TabStripTab ID="tabActiveGroup" Text="<%$ Resources:GUIStrings, Active %>"></ComponentArt:TabStripTab>
                        <ComponentArt:TabStripTab ID="tabDeactivatedGroup" Text="<%$ Resources:GUIStrings, Deactivated %>"></ComponentArt:TabStripTab>
                    </Tabs>
                </ComponentArt:TabStrip>
                <ComponentArt:MultiPage ID="mltpGroups" runat="server">
                    <ComponentArt:PageView ID="pgvActive" runat="server">
                        <!-- Active Groups Starts -->
                        <div class="subTabContent">
                            <ComponentArt:Grid ID="grdActiveGroups" SkinID="ScrollingGrid" runat="server" Width="242" Height="390"
                                ShowHeader="false" ShowFooter="false" RunningMode="Client"
                                AutoPostBackOnSelect="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" ScrollBar="On" PageSize="17" AllowPaging="true"
                                AllowEditing="false" AutoCallBackOnUpdate="false" AllowMultipleSelect="true">
                                <Levels>
                                    <ComponentArt:GridLevel DataKeyField="PostId" ShowTableHeading="false" ShowSelectorCells="false"
                                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow" ShowHeadingCells="false"
                                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                        <Columns>
                                            <ComponentArt:GridColumn Width="238" HeadingText="<%$ Resources:GUIStrings, PaymentMethods %>" DataField="Subject" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" 
                                                 HeadingGripImageWidth="6" HeadingGripImageHeight="8" AllowReordering="false" FixedWidth="true"
                                                AllowSorting="True" DataCellClientTemplateId="PaymentHoverTemplate" IsSearchable="true" />
                                            <ComponentArt:GridColumn DataField="PostId" Visible="false" IsSearchable="false" />
                                        </Columns>
                                    </ComponentArt:GridLevel>
                                </Levels>
                                <ClientTemplates>
                                    <ComponentArt:ClientTemplate ID="ClientTemplate1">
                                        <span title="## DataItem.GetMember('Subject').get_text() ##">## DataItem.GetMember('Subject').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Subject').get_text() ##</span>
                                    </ComponentArt:ClientTemplate>
                                </ClientTemplates>
                            </ComponentArt:Grid>
                        </div>
                        <!-- Active Groups Ends -->
                    </ComponentArt:PageView>
                    <ComponentArt:PageView ID="pgvDeactivated" runat="server">
                        <!-- Deactivated Groups Starts -->
                        <div class="subTabContent">
                            <ComponentArt:Grid ID="grdDeactivatedGroups" SkinID="ScrollingGrid" runat="server" Width="242" Height="390"
                                ShowFooter="false" RunningMode="Client" ShowHeader="false"
                                AutoPostBackOnSelect="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" ScrollBar="On" PageSize="17" AllowPaging="true"
                                AllowEditing="false" AutoCallBackOnUpdate="false" AllowMultipleSelect="true">
                                <Levels>
                                    <ComponentArt:GridLevel DataKeyField="PostId" ShowTableHeading="false" ShowSelectorCells="false"
                                        RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                                        HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                                        HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow" ShowHeadingCells="false"
                                        HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                                        SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                                        SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                                        <Columns>
                                            <ComponentArt:GridColumn Width="238" HeadingText="<%$ Resources:GUIStrings, PaymentMethods %>" DataField="Subject" DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell" 
                                                 HeadingGripImageWidth="6" HeadingGripImageHeight="8" AllowReordering="false" FixedWidth="true"
                                                AllowSorting="True" DataCellClientTemplateId="PaymentHoverTemplate" IsSearchable="true" />
                                            <ComponentArt:GridColumn DataField="PostId" Visible="false" IsSearchable="false" />
                                        </Columns>
                                    </ComponentArt:GridLevel>
                                </Levels>
                                <ClientTemplates>
                                    <ComponentArt:ClientTemplate ID="ClientTemplate2">
                                        <span title="## DataItem.GetMember('Subject').get_text() ##">## DataItem.GetMember('Subject').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Subject').get_text() ##</span>
                                    </ComponentArt:ClientTemplate>
                                </ClientTemplates>
                            </ComponentArt:Grid>
                        </div>
                        <!-- Deactivated Groups Ends -->
                    </ComponentArt:PageView>
                </ComponentArt:MultiPage>
            </div>
        </div>
        <div class="rightColumn" style="width:677px;margin-bottom:5px;">
            <div class="boxSubHeader">
                <h5><asp:localize runat="server" text="<%$ Resources:GUIStrings, SelectedGroup %>"/></h5>
            </div>
            <span class="formRow">
                <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, GroupName1 %>"/></label>
                <asp:TextBox ID="txtGroupName" runat="server" Width="200" CssClass="textBoxes"></asp:TextBox>
            </span>
            <span class="formRow">
                <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, Description1 %>"/></label>
                <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="204" TextMode="MultiLine" Rows="4"></asp:TextBox>
            </span>
            <span class="formRow">
                <label class="formLabel"><asp:localize runat="server" text="<%$ Resources:GUIStrings, SecurityLevel1 %>"/><br /><asp:localize runat="server" text="<%$ Resources:GUIStrings, HoldCtrlToSelectMultiple %>"/></label>
                <asp:ListBox ID="lstSecurityLevels" runat="server" Width="204" Rows="4">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Customer %>"></asp:ListItem>
                </asp:ListBox>
            </span>
            <div class="subContentHolder">
                <div class="boxSubHeader">
                    <h5><asp:localize runat="server" text="<%$ Resources:GUIStrings, GroupMembers %>"/></h5>
                </div>
                <GM:GroupMembers ID="ctlGroupMembers" runat="server" />
                <div class="clearFix">&nbsp;</div>
            </div>
            <div class="footerContent">
                <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                    />
                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
                    />
            </div>
        </div>
    </div>
</asp:Content>
