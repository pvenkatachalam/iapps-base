﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersManageShipping %>"
    Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" EnableEventValidation="false" CodeBehind="Shipping.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Shipping" StylesheetTheme="General" %>

<%@ Register Src="../../UserControls/General/InternationalAddress.ascx" TagName="InternationalAddress" TagPrefix="uc1" %>
<%@ Register TagPrefix="sc" TagName="ScriptManager" Src="~/UserControls/General/ScriptManagerControl.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var gridItem;
        var emptyGuid = '00000000-0000-0000-0000-000000000000'; // empty guid

        function grid_onContextMenu(sender, eventArgs) {
            gridObject = sender;
            gridItem = eventArgs.get_item();
            gridObject.select(gridItem);
            var e = eventArgs.get_event();

            menuItems = mnuShipping.get_items();
            if (ContextMenuGeneralChecking(menuItems)) {
                mnuShipping.showContextMenuAtEvent(e);
            }
        }

        function ContextMenuGeneralChecking(menuItems) {
            var isAnyMenuVisible = false;
            var uniqueId = gridItem.getMember('Id').get_text();
            if (uniqueId != '' && uniqueId != emptyGuid) //only add
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    menuItems.getItem(i).set_visible(true);
                    isAnyMenuVisible = true;
                }
            }
            else {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                        isAnyMenuVisible = true;
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }
            return true;
        }

        // Grid Context menu selection
        function menu_onItemSelect(sender, eventArgs) {
            menuObject = sender;
            var menuItem = eventArgs.get_item();
            //var contextDataNode = menuItem.get_parentMenu().get_contextData();
            var selectedMenuId = menuItem.get_id();

            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        AddGridItem();
                        break;
                    case 'cmEdit':
                        EditGridItem();
                        break;
                    case 'cmDelete':
                        DeleteSelectedGridItem();
                        break;

                }
            }
        }

        function AddGridItem() {
            clbkShippingDetail.callback(emptyGuid);
        }

        function EditGridItem() {
            var id = gridItem.getMember('Id').get_text();
            clbkShippingDetail.callback(id);
            UpdateValidatorDisplay();
        }

        function UpdateValidatorDisplay() {
            if ((typeof (Page_Validators) != "undefined") && (Page_Validators != null)) {
                var i;
                for (i = 0; i < Page_Validators.length; i++) {
                    ValidatorUpdateDisplay(Page_Validators[i]);
                }
            }
        }

        function DeleteSelectedGridItem() {
            if (confirm("<asp:localize runat='server' text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteTheSelectedItem %>'/>")) {
                var id = gridItem.getMember('Id').get_text();
                gridObject.set_callbackParameter(id);
                gridObject.callback();
                clbkShippingDetail.callback(emptyGuid);
            }
        }

        function clbkShippingDetail_CallbackComplete(sender, eventArgs) {
            hideTopMessage();
        }
        function hideTopMessage() {
            var divObj = document.getElementById(jdivPageTopContainerId);
            if (divObj != null) {
                divObj.innerHTML = '';
                divObj.style.display = "none";
            }
        }
        function CheckAddressType() {
            var errorMsg = '';
            if (Page_ClientValidate('vgShipping') == false) {
                return false;
            }
            else {
                var radioCreditCard = document.getElementById('<%= rbtnResidence.ClientID %>');
                var radioDebitCard = document.getElementById('<%= rbtnOffice.ClientID %>');
                if (!radioCreditCard.checked && !radioDebitCard.checked) {
                    errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseSelectAAddressType %>'/>";
                }

                var isAddressVerified = validateCity();

                if (isAddressVerified == '0') {
                    errorMsg = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAValidCityOrZipcode %>'/> \n";
                }
                else if (isAddressVerified == '2') {
                    errorMsg = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, AddressCannotBeValidated %>'/> \n";
                }
                if (errorMsg != '') {
                    alert(errorMsg);
                    return false;
                }
                else {
                    return true;
                }
            }
        }
        function PageLevelNextAction() {
            $('#<%=btnSave.ClientID%>').click()
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CustomerDetailsHolder" runat="server">
    <sc:ScriptManager runat="server" ID="sManager" />
    <asp:UpdatePanel runat="server" ID="uplPayments">
        <ContentTemplate>
            <div id="divPageTopContainer" class="messageContainer" runat="server" visible="false" style="margin-bottom: 20px;"
                enableviewstate="false">
            </div>
            <div class="customer-shipping">

                <div class="button-row">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button"
                        CausesValidation="false" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                        OnClientClick="return CheckAddressType();" CausesValidation="true" OnClick="btnSave_Click" />
                    <asp:ValidationSummary ID="valSummary" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="vgShipping" />
                </div>
                <asp:PlaceHolder runat="server" ID="selectShippingAddress">
                    <div class="edit-header clear-fix">
                        <div class="columns">
                            <h3>
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SelectShippingAddress %>" /></h3>
                        </div>
                        <div class="columns">
                            <asp:DropDownList ID="ddlShippings" runat="server" Width="270" AutoPostBack="true" Style="float: left;" />
                            <asp:CheckBox ID="isPrimay" runat="server" GroupName="AddressType" Text="Default"
                                Checked="true" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div class="clear-fix">
                    <div class="columns">
                        <h3>
                            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, ShippingAddressDetails %>" /></h3>
                        <asp:Button ID="btnDelete" runat="server" Text="<%$ Resources:GUIStrings, DeleteShipping %>" CausesValidation="false"
                            CssClass="small-button" />
                    </div>
                    <div class="columns rightColumn">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AddressType1 %>" /></label>
                            <asp:RadioButton ID="rbtnResidence" runat="server" GroupName="AddressType" Text="<%$ Resources:GUIStrings, Residence %>"
                                Checked="true" />
                            <asp:RadioButton ID="rbtnOffice" runat="server" GroupName="AddressType" Text="<%$ Resources:GUIStrings, Business %>" />
                        </div>
                        
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, NickName1 %>" /></label>
                            <asp:TextBox ID="txtNickName" runat="server" Width="260" CssClass="textBoxes" MaxLength="255" autocomplete="off"></asp:TextBox>
                            <asp:RegularExpressionValidator runat="server" ID="revtxtNickName" ControlToValidate="txtNickName"
                                ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInNickName %>"
                                Text="*" ValidationGroup="vgShipping"></asp:RegularExpressionValidator>
                        </div>
                        <uc1:InternationalAddress ID="cntrlAddress" runat="server" ValidationGroup="vgShipping" />
                    </div>
                </div>
            </div>
            <asp:PlaceHolder runat="server" id="phNotice">
                <p class="notice clear-fix"><em><%= GUIStrings.UPSPOBoxValidationNotice %></em></p>
            </asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
