﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersCustomerNotes %>"
    Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="CustomerNotes.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CustomerNotes"
    Theme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ValidateNotes() {
            var notes = Trim(document.getElementById("<%=txtNotes.ClientID%>").value);

            if (notes == '' || notes == "<%= GUIStrings.AddANote %>") {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, NotesCanNotBeEmptyPleaseReenter %>'/>");
                return false;
            }
            else {
                if (notes.match(/[&\<\>]/)) {
                    alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheNotes %>'/>");
                    return false;
                }
                else {
                    return true;
                }
            }
        }

        function GetHoverText(text) {
            return text.replace(/<br\/>/g, "\n");
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CustomerDetailsHolder" runat="server">
    <div class="grid-utility">
        <div class="columns">
            <asp:TextBox ID="txtNotes" runat="server" CssClass="textBoxes" Width="500" TextMode="MultiLine"
                Rows="3" ToolTip="<%$ Resources:GUIStrings, AddANote %>" style="font-size: 13px;" />
        </div>
        <div class="columns">
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                OnClick="btnSave_Click" ToolTip="<%$ Resources:GUIStrings, SaveNote %>" CssClass="primarybutton"
                OnClientClick="return ValidateNotes();" style="height: 30px !important" />
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <ComponentArt:Grid ID="grdCustomerNotes" SkinID="Default" runat="server" Width="100%"
        RunningMode="Callback" PagerInfoClientTemplateId="CustomerNotesPaginationTemplate"
        AllowEditing="true" AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="CustomerNotesSliderTemplateCached"
        SliderPopupClientTemplateId="CustomerNotesSliderTemplate" AutoCallBackOnUpdate="false"
        CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" CallbackCachingEnabled="true"
        AllowMultipleSelect="false" EditOnClickSelectedItem="false" LoadingPanelClientTemplateId="grdCustomerNotesLoadingPanelTemplate">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="LogId" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, EntryDate %>"
                        DataField="CreatedDate" IsSearchable="true" Align="Left" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="DateHoverTemplate" />
                    <ComponentArt:GridColumn Width="600" HeadingText="<%$ Resources:GUIStrings, EntryText %>"
                        DataField="ChangedItems" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="TextHoverTemplate" TextWrap="true" />
                    <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, ModifiedBy %>"
                        DataField="ModifiedBy" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" />
                    <ComponentArt:GridColumn DataField="LogId" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="DateHoverTemplate">
                <span title="## DataItem.GetMember('CreatedDate').get_text() ##">## DataItem.GetMember('CreatedDate').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('CreatedDate').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="TextHoverTemplate">
                <span title="## GetHoverText( DataItem.GetMember('ChangedItems').get_text()) ##">##
                    DataItem.GetMember('ChangedItems').get_text() == "" ? "&nbsp;" :DataItem.GetMember('ChangedItems').get_text()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerNotesSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        ##_datanotload##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(page0Of1, DataItem.PageIndex + 1, grdCustomerNotes.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdCustomerNotes.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerNotesSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        <span class="pages">##stringformat(page0Of1, DataItem.PageIndex + 1, grdCustomerNotes.PageCount)##</span>
                        <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdCustomerNotes.RecordCount)##</span>
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerNotesPaginationTemplate">
                ##stringformat(Page0Of1Items, currentPageIndex(grdCustomerNotes), pageCount(grdCustomerNotes),
                grdCustomerNotes.RecordCount)##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="grdCustomerNotesLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdCustomerNotes) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</asp:Content>
