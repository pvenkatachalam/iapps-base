﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersManagePaymentMethods %>"
    Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" CodeBehind="Payments.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Payments"
    StylesheetTheme="General" EnableEventValidation="false" %>

<%@ Register Src="../../UserControls/General/InternationalAddress.ascx" TagName="InternationalAddress"
    TagPrefix="uc1" %>
<%@ Register TagPrefix="sc" TagName="ScriptManager" Src="~/UserControls/General/ScriptManagerControl.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
    function CheckPaymentType() 
    {
        debugger;
        var errorMsg = '';
        var pageValid = Page_ClientValidate("vgPayment");
        if (pageValid) 
        {
            var radioCreditCard = document.getElementById('<%= rbtnCreditCard.ClientID %>');
            if (!radioCreditCard.checked)//&& !radioDebitCard.checked
            {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseSelectAPaymentType %>'/> \n";
                returnVal = false;
            }

            var cardTypeCtrl = document.getElementById('<%=ddlCardType.ClientID %>');
            var cardType = cardTypeCtrl.options[cardTypeCtrl.selectedIndex].text;
            var cardNumber = document.getElementById('<%=txtCardNumber.ClientID %>').value;
            var cardValMsg = checkCreditCard(cardNumber, cardType);
            if (cardValMsg != 'true')
                errorMsg += cardValMsg + "\n";

            if (errorMsg == '') 
            {
                return true;
            }
            else 
            {
                alert(errorMsg);
                return false;
            }
        }
        else 
        {
            return false;
        }
    }
   
   function openAddressSuggestion()   {
 
            popupPath = "/CommerceAdmin/Popups/StoreManager/Customers/SuggestedAddresses.aspx";
            addressSuggestionPopup = dhtmlmodal.open('AddressSuggestion', 'iframe', popupPath, '', 'width=500px,height=500px,center=1,resize=0,scrolling=1');
            addressSuggestionPopup.onclose = function () {
                var a = document.getElementById('AddressSuggestion');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }
        
        function CloseAddressSuggestionPopup() {
            addressSuggestionPopup .hide();
        }
   
      function PageLevelNextAction() {
            $('#<%=btnSave.ClientID%>').click()
        }
    

    function checkCreditCard (cardnumber, cardname) {
        var initialCardNumber = document.getElementById("<%=hfCardNumber.ClientID%>").value;

     if(cardnumber!=initialCardNumber)
     { 
     
      // Array to hold the permitted card characteristics
      var cards = new Array();

      // Define the cards we support. You may add addtional card types.
  
      //  Name:      As in the selection box of the form - must be same as user's
      //  Length:    List of possible valid lengths of the card number for the card
      //  prefixes:  List of possible prefixes for the card
      //  checkdigit Boolean to say whether there is a check digit
  
      cards [0] = {name: "Visa", 
                   length: "13,16", 
                   prefixes: "4",
                   checkdigit: true};
      cards [1] = {name: "MasterCard", 
                   length: "16", 
                   prefixes: "51,52,53,54,55",
                   checkdigit: true};
      cards [2] = {name: "DinersClub", 
                   length: "14,16", 
                   prefixes: "36,54,55",
                   checkdigit: true};
      cards [3] = {name: "CarteBlanche", 
                   length: "14", 
                   prefixes: "300,301,302,303,304,305",
                   checkdigit: true};
      cards[4] = { name: "American Express", 
                   length: "15", 
                   prefixes: "34,37",
                   checkdigit: true};
      cards [5] = {name: "Discover", 
                   length: "16", 
                   prefixes: "6",
                   checkdigit: true};
      cards [6] = {name: "JCB", 
                   length: "16", 
                   prefixes: "35",
                   checkdigit: true};
      cards [7] = {name: "enRoute", 
                   length: "15", 
                   prefixes: "2014,2149",
                   checkdigit: true};
      cards [8] = {name: "Solo", 
                   length: "16,18,19", 
                   prefixes: "6334, 6767",
                   checkdigit: true};
      cards [9] = {name: "Switch", 
                   length: "16,18,19", 
                   prefixes: "4903,4905,4911,4936,564182,633110,6333,6759",
                   checkdigit: true};
      cards [10] = {name: "Maestro", 
                   length: "12,13,14,15,16,18,19", 
                   prefixes: "5018,5020,5038,6304,6759,6761",
                   checkdigit: true};
      cards [11] = {name: "VisaElectron", 
                   length: "16", 
                   prefixes: "417500,4917,4913,4508,4844",
                   checkdigit: true};
               
      // Establish card type
      var cardType = -1;
      for (var i=0; i<cards.length; i++) {

        // See if it is this card (ignoring the case of the string)
        if (cardname.toLowerCase () == cards[i].name.toLowerCase()) {
          cardType = i;
          break;
        }
      }
  
      // If card type not found, report an error
      if (cardType == -1) {
         return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, UnknownCardType %>'/>";
     
      }
   
      // Ensure that the user has provided a credit card number
      if (cardnumber.length == 0)  {     
         return ""; 
      }
    
      // Now remove any spaces from the credit card number
      cardnumber = cardnumber.replace (/\s/g, "");
  
      // Check that the number is numeric
      var cardNo = cardnumber
      var cardexp = /^[0-9]{13,19}$/;
      if (!cardexp.exec(cardNo))  {
         return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CreditCardNumberIsInInvalidFormat %>'/>"; 
      }
       
      // Now check the modulus 10 check digit - if required
      if (cards[cardType].checkdigit) {
        var checksum = 0;                                  // running checksum total
        var mychar = "";                                   // next char to process
        var j = 1;                                         // takes value of 1 or 2
  
        // Process each digit one by one starting at the right
        var calc;
        for (i = cardNo.length - 1; i >= 0; i--) {
    
          // Extract the next digit and multiply by 1 or 2 on alternative digits.
          calc = Number(cardNo.charAt(i)) * j;
    
          // If the result is in two digits add 1 to the checksum total
          if (calc > 9) {
            checksum = checksum + 1;
            calc = calc - 10;
          }
    
          // Add the units element to the checksum total
          checksum = checksum + calc;
    
          // Switch the value of j
          if (j ==1) {j = 2} else {j = 1};
        } 
  
        // All done - if checksum is divisible by 10, it is a valid modulus 10.
        // If not, report an error.
        if (checksum % 10 != 0)  {     
         return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CreditCardNumberIsInvalid %>'/>"; 
        }
      }  

      // The following are the card-specific checks we undertake.
      var LengthValid = false;
      var PrefixValid = false; 
      var undefined; 

      // We use these for holding the valid lengths and prefixes of a card type
      var prefix = new Array ();
      var lengths = new Array ();
    
      // Load an array with the valid prefixes for this card
      prefix = cards[cardType].prefixes.split(",");
      
      // Now see if any of them match what we have in the card number
      for (i=0; i<prefix.length; i++) {
        var exp = new RegExp ("^" + prefix[i]);
        if (exp.test (cardNo)) PrefixValid = true;
      }
      
      // If it isn't a valid prefix there's no point at looking at the length
      if (!PrefixValid) {     
         return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CreditCardNumberIsInvalid %>'/>"; 
      }
    
      // See if the length is valid for this card
      lengths = cards[cardType].length.split(",");
      for (j=0; j<lengths.length; j++) {
        if (cardNo.length == lengths[j]) LengthValid = true;
      }
  
      // See if all is OK by seeing if the length was valid. We only check the 
      // length if all else was hunky dory.
      if (!LengthValid) {
         return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, CreditCardNumberHasAnInappropriateNumberOfDigits %>'/>"; 
      };   
      }
      // The credit card is in the required format.
      return "true";
    }
    function CheckExpiryDate(source, arguments) {
            var data = arguments.Value.split('');
            arguments.IsValid = false; //set IsValid property to false

            var ddlYear = document.getElementById('<%=ddlExpiryYear.ClientID %>');
            var ddlMonth = document.getElementById('<%=ddlExpiryMonth.ClientID %>');
            var currentYear = <%=DateTime.Now.Year.ToString() %>;
            var currentMonth = <%=DateTime.Now.Month.ToString() %> ;         
         
            if(parseInt(ddlMonth.value) >= parseInt(currentMonth) && parseInt(ddlYear.value) >= parseInt(currentYear) || parseInt(ddlYear.value) > parseInt(currentYear)  )
                arguments.IsValid = true;
            
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CustomerDetailsHolder" runat="server">
    <sc:ScriptManager runat="server" ID="sManager" />
    <asp:UpdatePanel runat="server" ID="uplPayments">
        <ContentTemplate>
            <div class="customer-payments clear-fix">
                <div id="divPageTopContainer" class="messageContainer" runat="server" visible="false"
                    enableviewstate="false">
                </div>
                <div class="button-row">
                    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                        CausesValidation="false" OnClientClick="return CheckPaymentType();" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
                        OnClick="btnCancel_Click" CausesValidation="false" />
                    <asp:ValidationSummary ID="valSummary" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="vgPayment"  />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="vgAddress"  />
                </div>
                <div class="edit-header">
                    <div class="columns">
                        <h3>
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, SelectPaymentMethod %>" /></h3>
                    </div>
                    <div class="columns">
                        <asp:DropDownList ID="ddlPayments" runat="server" Width="465" AutoPostBack="true" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="columns">
                    <h3>
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, PaymentDetails %>" /></h3>
                    <asp:Button ID="btnDelete" runat="server" Text="<%$ Resources:GUIStrings, DeletePayment %>"
                        CssClass="small-button" />
                </div>
                <div class="columns rightColumn">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PaymentType1 %>" />
                        </label>
                        <div class="form-value">
                            <asp:RadioButton ID="rbtnCreditCard" runat="server" GroupName="Card" Text="<%$ Resources:GUIStrings, CreditCard %>"
                                Checked="true" />
                        </div>
                    </div>
                     <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize9" runat="server" Text="Alias:" /><span class="req">*</span>
                        </label>
                        <div class="form-value">
                            <asp:TextBox runat="server" MaxLength="255" ID="nickName" Width="270"></asp:TextBox>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="RequiredFieldValidator1" runat="server"
                                ControlToValidate="nickName" Text="*" ErrorMessage="<%$ Resources:GUIStrings, AliasIsRequired %>" 
                                ValidationGroup="vgPayment"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, CardType1 %>" /><span class="req">*</span>
                        </label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlCardType" runat="server" Width="270" />
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlCardType" runat="server"
                                ControlToValidate="ddlCardType" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectACardType %>"
                                InitialValue="00000000-0000-0000-0000-000000000000" ValidationGroup="vgPayment"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, CardNumber1 %>" /><span class="req">*</span>
                        </label>
                        <div class="form-value">
                            <asp:TextBox ID="txtCardNumber" runat="server" CssClass="textBoxes" MaxLength="16"
                                Width="260"></asp:TextBox>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtCardNumber" ControlToValidate="txtCardNumber"
                                runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheCreditCardNumber %>"
                                ValidationGroup="vgPayment" ></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Expiration1 %>" /><span class="req">*</span>
                        </label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlExpiryMonth" runat="server" Width="80">
                                <asp:ListItem Text="MM" Value="0"></asp:ListItem>
                                <asp:ListItem Text="01" Value="1"></asp:ListItem>
                                <asp:ListItem Text="02" Value="2"></asp:ListItem>
                                <asp:ListItem Text="03" Value="3"></asp:ListItem>
                                <asp:ListItem Text="04" Value="4"></asp:ListItem>
                                <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                <asp:ListItem Text="06" Value="6"></asp:ListItem>
                                <asp:ListItem Text="07" Value="7"></asp:ListItem>
                                <asp:ListItem Text="08" Value="8"></asp:ListItem>
                                <asp:ListItem Text="09" Value="9"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlExpiryMonth"
                                runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectTheExpirationMonth %>"
                                ControlToValidate="ddlExpiryMonth" InitialValue="0" ValidationGroup="vgPayment"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlExpiryYear" runat="server" Width="100">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlExpiryYear" runat="server"
                                Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectTheExpirationYear %>"
                                ControlToValidate="ddlExpiryYear" InitialValue="0" ValidationGroup="vgPayment"></asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvExpirationYear" runat="server" ErrorMessage="<%$ Resources:GUIStrings, CardIsExpired %>"
                                Text="*" ClientValidationFunction="CheckExpiryDate" ControlToValidate="ddlExpiryYear"
                                OnServerValidate="cvYear_Validate" ValidationGroup="vgPayment" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, NameOnCard %>" /><span class="req">*</span>
                        </label>
                        <div class="form-value">
                            <asp:TextBox ID="txtNameOnCard" runat="server" CssClass="textBoxes" Width="260" MaxLength="100"></asp:TextBox>
                            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtNameOnCard" ControlToValidate="txtNameOnCard"
                                runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheNameOnCard %>"
                                ValidationGroup="vgPayment"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator runat="server" ID="revtxtNameOnCard" ControlToValidate="txtNameOnCard"
                                ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInNameOnCard %>"
                                Text="*" ValidationGroup="vgPayment"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="form-row">
                        <h5 style="font-weight: bold;">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, BillingAddress %>" /></h5>
                    </div>
                    <uc1:InternationalAddress ID="cntrlAddress" runat="server" IsFirstNameVisible="false"
                        IsLastNameVisible="false" ValidationGroup="vgPayment" />
                    <asp:HiddenField ID="hfCardNumber" runat="server" />
                </div>
            </div>
            <asp:PlaceHolder ID="phNotice" runat="server">
            <p class="notice">
                <em>NOTICE: The address validation functionality will validate P.O. Box addresses; however,
                    UPS does not provide delivery to P.O. boxes. The customer must make every effort
                    to obtain a street address. Attempts by the customer to ship to a P.O. Box via UPS
                    may result in additional charges.</em></p></asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
