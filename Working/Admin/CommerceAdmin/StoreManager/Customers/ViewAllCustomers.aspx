﻿<%@ Page Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersViewAllCustomers %>"
    Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerMaster.master"
    Theme="General" AutoEventWireup="true" CodeBehind="ViewAllCustomers.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ViewAllCustomers" %>

<asp:Content ID="AllCustomersContent" ContentPlaceHolderID="CustomerContentHolder"
    runat="server">
    <script type="text/javascript">
        var CustomerListItem;
        var IdColumn = 0;
        var CustomerIdColumn = 0;
        var UserIdColumn = 0;
        var CustomerId;
        var UserId;
        var emptyGuid = '00000000-0000-0000-0000-000000000000'; // empty guid
        var customerName;
        var customerNameColumnId = 2;
        var firstNameColumnId = 1;

        function grdCustomerListing_onContextMenu(sender, eventArgs) {
            var e = eventArgs.get_event();
            CustomerListItem = eventArgs.get_item();
            CustomerId = CustomerListItem.getMember(CustomerIdColumn).get_text();
            UserId = CustomerListItem.getMember(UserIdColumn).get_text();
            customerName = CustomerListItem.getMember(firstNameColumnId).get_text() + ' ' + CustomerListItem.getMember(customerNameColumnId).get_text();
            menuItems = mnuCustomerList.get_items();
            if (UserId == '' || UserId == emptyGuid) //only add
            {
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'cmAdd') {
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }
            mnuCustomerList.showContextMenuAtEvent(e);
        }
        function mnuCustomerList_onItemSelect(sender, eventArgs) {

            var menuItem = eventArgs.get_item();
            var contextDataNode = menuItem.get_parentMenu().get_contextData();
            var selectedMenuId = menuItem.get_id();


            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmDetails':
                        window.location = 'CustomerDetails.aspx?CustomerId=' + UserId;
                        break;
                    case 'cmEditBilling':
                        window.location = 'Payments.aspx?CustomerId=' + UserId;
                        break;
                    case 'cmEditShipping':
                        window.location = 'Shipping.aspx?CustomerId=' + UserId;
                        break;
                    case 'cmEditNotes':
                        window.location = 'CustomerNotes.aspx?CustomerId=' + UserId;
                        break;
                    case 'cmViewOrders':
                        window.location = 'CustomerOrderList.aspx?CustomerId=' + UserId;
                        break;
                    case 'cmLoginAccounts':
                        window.location = 'AccountLogins.aspx?CustomerId=' + UserId;
                        break;
                    case 'cmAdd':
                        window.location = 'CustomerDetails.aspx?CustomerId=00000000-0000-0000-0000-000000000000';
                        break;
                    case 'cmDelete':
                        if (confirm("<asp:localize runat='server' text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteCustomer %>'/>")) {
                            var deleteStatus = DeleteCustomer(UserId);
                            if (deleteStatus == 'true') {
                                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, CustomerDeletedSuccessfully %>'/>");
                                grdCustomerListing.deleteItem(CustomerListItem);
                                if (grdCustomerListing.get_recordCount() != 0) {
                                    if (grdCustomerListing.get_recordCount() % grdCustomerListing.get_pageSize() == 0) {
                                        grdCustomerListing.previousPage();
                                    }
                                }
                                else {
                                    //grdCustomerListing.beginUpdate();
                                    grdCustomerListing.get_table().addEmptyRow(0);
                                    // grdCustomerListing.editComplete();
                                    // grdCustomerListing.refresh();
                                }
                            }
                            else {
                                //alert("You can not delete the selected Customer, orders are associated with the Customer." );
                                alert(deleteStatus);
                            }
                        }

                        break;
                    case 'cmCreateOrder':
                        ClearOrderRelatedSessions();
                        var isShippingAddressIsThere = CheckShippingAddressExist(UserId);
                        if (isShippingAddressIsThere != 'True') {
                            alert(isShippingAddressIsThere);
                            window.location = '../../StoreManager/Customers/Shipping.aspx?ReturnUrl=../../StoreManager/Orders/NewOrders.aspx&OrderId=00000000-0000-0000-0000-000000000000&RedirectURL=~/StoreManager/Customers/ViewAllCustomers.aspx&CustomerId=' + UserId;
                            return;
                        }
                        window.location = '../../StoreManager/Orders/NewOrders.aspx?OrderId=00000000-0000-0000-0000-000000000000&CustomerId=' + UserId + '&Name=' + customerName + '&RedirectURL=~/StoreManager/Customers/ViewAllCustomers.aspx';
                        break;

                }
            }
        }

        function grdCustomerListing_onPageIndexChange(sender, eventArgs) {
            grdCustomerListing.set_callbackParameter(document.getElementById("<%=txtSearchCustomer.ClientID%>").value);
        }

        function grdCustomerListing_onSortChange() {
            grdCustomerListing.set_callbackParameter(document.getElementById("<%=txtSearchCustomer.ClientID%>").value);
        }
        
    </script>
    <div class="grid-utility">
        <asp:TextBox ID="txtSearchCustomer" CssClass="textBoxes" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
            Width="300" runat="server" />
        <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:GUIStrings, Filter %>"
            OnClick="btnSearch_Search" CssClass="small-button" />
        <asp:LinkButton ID="lbtnExportAll" runat="server" Text="Export Customers" CssClass="export" />
    </div>
    <ComponentArt:Grid SkinID="Default" ID="grdCustomerListing" SliderPopupClientTemplateId="CustomerListingSliderTemplate"
        SliderPopupCachedClientTemplateId="CustomerListingSliderTemplateCached" Width="100%"
        CssClass="plain-grid" runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
        AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="20" PagerInfoClientTemplateId="CustomerListingPaginationTemplate"
        CallbackCachingEnabled="false" SearchOnKeyPress="true" ManualPaging="true" LoadingPanelClientTemplateId="CustomerListingLoadingPanelTemplate"
        Sort="LastName ASC">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
                ShowSelectorCells="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="FirstName" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, CustomerName %>"
                        DataField="LastName" Align="Left" IsSearchable="true" AllowReordering="false"
                        FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                    <ComponentArt:GridColumn Width="187" HeadingText="<%$ Resources:GUIStrings, Email %>"
                        DataField="Email" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="EmailHoverTemplate" />
                    <ComponentArt:GridColumn Width="166" HeadingText="<%$ Resources:GUIStrings, Address1 %>"
                        DataField="AddressLine1" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AddressHoverTemplate" />
                    <ComponentArt:GridColumn Width="165" HeadingText="<%$ Resources:GUIStrings, City %>"
                        DataField="City" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="CityHoverTemplate" />
                    <ComponentArt:GridColumn Width="90" HeadingText="<%$ Resources:GUIStrings, Phone %>"
                        DataField="HomePhone" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="PhoneHoverTemplate" />
                    <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, CompanyName %>"
                        DataField="CompanyName" IsSearchable="false" Align="Left" Visible="false" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="CompanyNameHoverTemplate" />
                    <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Status %>"
                        DataField="IsActive" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                        AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="StatusHoverTemplate" />
                    <ComponentArt:GridColumn DataField="StateCode" Visible="false" IsSearchable="false" />
                    <ComponentArt:GridColumn DataField="Zip" Visible="false" IsSearchable="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ContextMenu EventHandler="grdCustomerListing_onContextMenu" />
            <PageIndexChange EventHandler="grdCustomerListing_onPageIndexChange" />
            <SortChange EventHandler="grdCustomerListing_onSortChange" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="NameHoverTemplate">
                <span title="## DataItem.GetMember('LastName').get_text() ##,&nbsp;## DataItem.GetMember('FirstName').get_text() ##">
                    ## DataItem.GetMember('LastName').get_text() == "" ? "&nbsp;" : DataItem.GetMember('LastName').get_text()
                    ## ## DataItem.GetMember('LastName').get_text() != "" ? ",&nbsp;" : "" ## ## DataItem.GetMember('FirstName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('FirstName').get_text() ## </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EmailHoverTemplate">
                <span title="## DataItem.GetMember('Email').get_text() ##">## DataItem.GetMember('Email').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('Email').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="AddressHoverTemplate">
                <span title="## DataItem.GetMember('AddressLine1').get_text() ##">## DataItem.GetMember('AddressLine1').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('AddressLine1').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CityHoverTemplate">
                <span title="## DataItem.GetMember('City').get_text() + ',&nbsp;' + DataItem.GetMember('StateCode').get_text() + ' ' + DataItem.GetMember('Zip').get_text() ##">
                    ## DataItem.GetMember('City').get_text() == "" ? "&nbsp;" : DataItem.GetMember('City').get_text()
                    ## ## DataItem.GetMember('City').get_text() != "" ? ",&nbsp;" : "" ## ## DataItem.GetMember('StateCode').get_text()
                    ## &nbsp; ## DataItem.GetMember('Zip').get_text() ## </span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PhoneHoverTemplate">
                <span title="## DataItem.GetMember('HomePhone').get_text() ##">## DataItem.GetMember('HomePhone').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('HomePhone').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CompanyNameHoverTemplate">
                <span title="## DataItem.GetMember('CompanyName').get_text() ##">## DataItem.GetMember('CompanyName').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('CompanyName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                <span title="## DataItem.GetMember('IsActive').get_text() ##">## DataItem.GetMember('IsActive').get_text()
                    == "true" ? "Active" : DataItem.GetMember('IsActive').get_text() == "false" ? "Inactive":""
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerListingSliderTemplate">
                <div class="SliderPopup">
                    <p>
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                    <p class="paging">
                        ## stringformat(Page0Of1, DataItem.PageIndex + 1, grdCustomerListing.PageCount)
                        ## ## stringformat(Item0Of1, DataItem.PageIndex + 1, grdCustomerListing.PageCount)
                        ##
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerListingSliderTemplateCached">
                <div class="SliderPopup">
                    <h5>
                        ## DataItem.GetMemberAt(0).Value ##</h5>
                    <p>
                        ## DataItem.GetMemberAt(1).Value ##</p>
                    <p class="paging">
                        ## stringformat(Page0Of1, DataItem.PageIndex + 1, grdCustomerListing.PageCount)
                        ## ## stringformat(Item0Of1, DataItem.PageIndex + 1, grdCustomerListing.PageCount)
                        ##
                    </p>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerListingPaginationTemplate">
                ## stringformat(Page0Of1Items, currentPageIndex(grdCustomerListing), pageCount(grdCustomerListing),
                grdCustomerListing.RecordCount) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="CustomerListingLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdCustomerListing) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="mnuCustomerList" runat="server" SkinID="ContextMenu">
        <ClientEvents>
            <ItemSelect EventHandler="mnuCustomerList_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Menu>
</asp:Content>
