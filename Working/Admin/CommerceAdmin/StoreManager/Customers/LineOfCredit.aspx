﻿<%@ Page Language="C#" MasterPageFile="~/StoreManager/Customers/CustomerDetailsMaster.master"
    AutoEventWireup="true" StylesheetTheme="General" CodeBehind="LineOfCredit.aspx.cs"
    EnableEventValidation="false" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.LineOfCredit" Title="<%$ Resources:GUIStrings, iAPPSCommerceStoreManagerCustomersLineOfCredit %>" %>

<%@ Register Src="../../UserControls/General/InternationalAddress.ascx" TagName="InternationalAddress" TagPrefix="uc1" %>
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function DisableLOC() {
            return window.confirm("<asp:localize runat='server' text='<%$ Resources:GUIStrings, AreYouSureYouWantToDisableLOC %>'/>");
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CustomerDetailsHolder" runat="server">
    <asp:PlaceHolder ID="phLocInfo" runat="server">
        <div class="customer-loc">
            <div class="button-row">
                <asp:ValidationSummary ID="valSummary" runat="server" ShowMessageBox="true" ShowSummary="false" />
                <asp:Button ID="btnDisable" runat="server" Text="<%$ Resources:GUIStrings, Disable %>" Visible="false"
                    ToolTip="<%$ Resources:GUIStrings, Disable %>" CssClass="button" OnClientClick="return DisableLOC();" />
                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                    ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" CausesValidation="false" />
                <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                    ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
            </div>
            <div class="columns">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AccountNumber1 %>" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="textBoxes" Width="260"></asp:TextBox>
                        <asp:RegularExpressionValidator runat="server" ID="revtxtAccountNumber" ControlToValidate="txtAccountNumber"
                            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInAccountNumber %>"
                            Text="*"></asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="reqtxtAccountNumber" ControlToValidate="txtAccountNumber"
                            runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAccountNumber %>"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <uc1:InternationalAddress ID="cntrlAddress" runat="server" />
                <div class="clear-fix">
                </div>
            </div>
            <asp:Panel ID="pnlLocInfo" runat="server" Visible="false">
                <div class="form-row">
                    <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CreditLimit %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblCreditLmit" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CreditTerms %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblCreditTerms" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AvailableLimit %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblAvailableLimit" runat="server"></asp:Label>
                    </div>
                </div>
            </asp:Panel>
            <asp:Repeater ID="rptInvoice" runat="server">
                <HeaderTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" class="invoice plain-grid" width="100%">
                        <tr class="heading-row">
                            <th>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, InvoiceID %>" />
                            </th>
                            <th>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
                            </th>
                            <th>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, InvoiceTotalAmount %>" />
                            </th>
                            <th>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AmountOutstanding %>" />
                            </th>
                            <th>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, InvoiceDate %>" />
                            </th>
                            <th>
                                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, InvoiceDueDate %>" />
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="row">
                        <td class="data-cell">
                            <%# Eval("InvoiceId") %>
                        </td>
                        <td class="data-cell">
                            <%# Eval("Description") %>
                        </td>
                        <td class="data-cell">
                            <%#String.Format("{0:c}", Eval("TotalAmount")) %>
                        </td>
                        <td class="data-cell">
                            <%# String.Format("{0:c}",Eval("OutstandingAmount")) %>
                        </td>
                        <td class="data-cell">
                            <%# String.Format("{0:d}",Eval("InvoiceDate")) %>
                        </td>
                        <td class="data-cell">
                            <%# String.Format("{0:d}", Eval("DueDate"))%>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="alternate-row">
                        <td class="data-cell">
                            <%# Eval("InvoiceId") %>
                        </td>
                        <td class="data-cell">
                            <%# Eval("Description") %>
                        </td>
                        <td class="data-cell">
                            <%#String.Format("{0:c}", Eval("TotalAmount")) %>
                        </td>
                        <td class="data-cell">
                            <%# String.Format("{0:c}",Eval("OutstandingAmount")) %>
                        </td>
                        <td class="data-cell">
                            <%# String.Format("{0:d}",Eval("InvoiceDate")) %>
                        </td>
                        <td class="data-cell">
                            <%# String.Format("{0:d}", Eval("DueDate"))%>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </asp:PlaceHolder>
</asp:Content>
