﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderNotes.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.OrderNotes"  MasterPageFile="~/StoreManager/Orders/OrderMaster.master" StylesheetTheme="General" %>
<%@ Register src="~/UserControls/General/Notes.ascx" tagname="Notes" tagprefix="uc" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <asp:HyperLink ID="hplOrder" runat="server" Text="< Back to Order" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="OrderContentHolder" runat="server">
    <uc:Notes ID="ctlNotes" runat="server"  />
</asp:Content>