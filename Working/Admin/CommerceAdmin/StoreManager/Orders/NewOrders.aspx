﻿<%@ Page Title="iAPPS Commerce: Store Manager: Orders: Place New Order" Language="C#"
    MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="NewOrders.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.NewOrders" StylesheetTheme="General" ViewStateEncryptionMode="Never" %>

<%@ Register TagPrefix="ps" TagName="Shipment" Src="~/UserControls/StoreManager/Orders/Shipment.ascx" %>
<%@ Register TagPrefix="ps" TagName="Payment" Src="~/UserControls/StoreSetup/PaymentMethods/AddNewPayment.ascx" %>
<%@ Register TagPrefix="ps" TagName="Coupon" Src="~/UserControls/StoreSetup/Coupons/AddNewCoupon.ascx" %>
<%@ Register TagPrefix="GA" TagName="GlobalAttributes" Src="~/UserControls/StoreManager/GlobalLevelAttributes.ascx" %>
<%@ Register Src="../../UserControls/StoreManager/Orders/OrderTotals.ascx" TagName="OrderTotals"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/StoreManager/Orders/RMAListingControl.ascx"
    TagName="RMAListingControl" TagPrefix="uc1" %>
<asp:Content ID="cphHead" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        //<![CDATA[

        var customerSearchPopup;
        var emptyGuid = '00000000-0000-0000-0000-000000000000'; // empty guid
        var SelectedCustomerId = emptyGuid;
        //var OrderId;
        function displayMessage(strmessage) {

            if (strmessage != "")
                alert(strmessage);

            var currentUrl = document.URL;

            if (currentUrl.indexOf('OrderId=00000000-0000-0000-0000-000000000000') > 0 && OrderId != undefined && OrderId != null) {
                currentUrl = currentUrl.replace('OrderId=00000000-0000-0000-0000-000000000000', 'OrderId=' + OrderId);
            }
            window.location = currentUrl;
        }

        function displayMessage(strmessage, orderid) {

            if (strmessage != "")
                alert(strmessage);

            var currentUrl = document.URL;

            if (currentUrl.indexOf('OrderId=00000000-0000-0000-0000-000000000000') > 0 && orderid != undefined && orderid != null) {
                currentUrl = currentUrl.replace('OrderId=00000000-0000-0000-0000-000000000000', 'OrderId=' + orderid);
            }
            window.location = currentUrl;
        }

        function ShowSearchCustomerPopup() {
            popupPath = "../../Popups/StoreManager/Customers/SearchCustomer.aspx";
            customerSearchPopup = dhtmlmodal.open('CustomerSearchPopup', 'iframe', popupPath, 'Customer Search Popup', 'width=705px,height=535px,center=1,resize=0,scrolling=1');
            customerSearchPopup.onclose = function () {
                var a = document.getElementById('CustomerSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        var reloadAfterAddingDestination = true;
        function ClosePopupCustomerPopup() {
            customerSearchPopup.hide();
            if (emptyGuid != SelectedCustomerId) {
                var isShippingAddressIsThere = CheckShippingAddressExist(SelectedCustomerId);
                if (isShippingAddressIsThere != 'True') {
                    var currentUrl = document.URL;
                    currentUrl = currentUrl.replace('?', '&');
                    currentUrl = currentUrl.replace('#', '');
                    window.location = jCommerceAdminSiteUrl + '/StoreManager/Customers/Shipping.aspx?ReturnUrl=' + currentUrl + '&CustomerId=' + SelectedCustomerId;
                    return;
                }
                document.getElementById("<%=hdnCustomerId.ClientID%>").value = SelectedCustomerId;
                OrderCallback.callback('Customer:' + SelectedCustomerId);

                var divShippingDetails = document.getElementById("<%=divShipmentDetailsContainer.ClientID%>");
                var btnCreateOrder = document.getElementById("<%=btnCreateOrder.ClientID%>");
                $(btnCreateOrder).css('display', 'inline');
                $(btnCreateOrder).css('float', 'right');
                $(divShippingDetails).css('display', 'block');
            }
            return true;
        }

        function ShowCouponSearchPopup(orderId) {
            popupPath = "../../Popups/StoreSetup/Coupons/SearchCoupons.aspx?orderId=" + orderId;
            couponSearchPopup = dhtmlmodal.open('CouponSearchPopup', 'iframe', popupPath, 'Coupon Search Popup', 'width=710px,height=435px,center=1,resize=0,scrolling=1');
            couponSearchPopup.onclose = function () {
                var a = document.getElementById('CouponSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                //                var currentUrl = document.URL;
                //                window.location = currentUrl;
                return true;
            }
        }

        function ShowShipOrderPopup(orderShippingId) {
            popupPath = "../../Popups/StoreManager/Orders/ShipOrder.aspx?OrderShippingId=" + orderShippingId;
            shipOrderPopup = dhtmlmodal.open('ShipOrder', 'iframe', popupPath, 'Ship Order', 'width=790px,height=560px,center=1,resize=0,scrolling=1');
            shipOrderPopup.onclose = function () {
                var a = document.getElementById('ShipOrder');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }

        function CloseShipOrderPopup(message) {

            //shipOrderPopup.hide();
            if (message != '') {
                alert(message);
                //window.location.reload();
                //window.location.href=window.location.href;
                //  var path = unescape(window.location.href);
                // window.location.replace(path);
                var currentUrl = document.URL;
                window.location = currentUrl;
            }
            else { shipOrderPopup.hide(); }
            return true;
        }

        function ShowCancelOrderPopup(orderShippingId) {
            popupPath = "../../Popups/StoreManager/Orders/CancelOrder.aspx?OrderShippingId=" + orderShippingId;
            cancelOrderPopup = dhtmlmodal.open('CancelOrder', 'iframe', popupPath, 'Cancel Order', 'width=790px,height=560px,center=1,resize=0,scrolling=1');
            cancelOrderPopup.onclose = function () {
                var a = document.getElementById('CancelOrder');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }

        function CloseCancelOrderPopup(message) {
            cancelOrderPopup.hide();
            if (message != '') {
                alert(message);
                var currentUrl = document.URL;
                window.location = currentUrl;
            }
            return true;
        }

        function ShowViewShipmentsPopup(orderShippingId) {
            popupPath = "../../Popups/StoreManager/Orders/ViewShipments.aspx?OrderShippingId=" + orderShippingId;
            viewShipmentPopup = dhtmlmodal.open('ShipOrder', 'iframe', popupPath, 'Ship Order', 'width=675px,height=560px,center=1,resize=0,scrolling=1');
            viewShipmentPopup.onclose = function () {
                var a = document.getElementById('ShipOrder');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
            return false;
        }


        function CloseViewShipmentsPopup() {
            viewShipmentPopup.hide();
            return true;
        }

        function IsCustomerSelected() {
            var objValue = document.getElementById("<%=hdnCustomerId.ClientID%>").value;
            if (objValue == '' || objValue == emptyGuid) {
                alert('Please select a customer.');
                return false;
            }
            else {
                var isShippingAddressIsThere = CheckShippingAddressExist(objValue);
                if (isShippingAddressIsThere != 'True') {
                    var currentUrl = document.URL;
                    currentUrl = currentUrl.replace('?', '&');
                    currentUrl = currentUrl.replace('#', '');
                    window.location = "../../StoreManager/Customers/Shipping.aspx?ReturnUrl=" + currentUrl + "&CustomerId=" + objValue;
                    return false;
                }
                return true;
            }
        }

        function Order_CallbackComplete(sender, eventArgs) {
            if (isOrderStatusChanged == true) {
                var currentUrl = document.URL;
                //ClearOrderRelatedSessions();
                window.location = currentUrl;
            }
            ToggleSlideImage();
        }

        function AddNew_CallbackComplete(sender, eventArgs) {
            //Only in case of deleting a shippment..If I use callback, while editing time, txtQty textbox is not available
            //but as per this code, some unwanted refresh is there. because it has completed one calback and wanted to render the screen.. next for this redirect
            var loadPage = false;
            var currentUrl = document.URL;
            if (currentUrl.indexOf('OrderId=00000000-0000-0000-0000-000000000000') > 0 && OrderId != undefined && OrderId != null && OrderId != '00000000-0000-0000-0000-000000000000') {
                currentUrl = currentUrl.replace('OrderId=00000000-0000-0000-0000-000000000000', 'OrderId=' + OrderId);
            }

            if (reloadAfterAddingDestination && loadPage) {
                window.location = currentUrl;
            }
            addShippingFromCustomer = true;
        }

        var orderStatus;
        function ShowReasonPopup(status) {
            ShowReasonModal('../../Popups/StoreManager/Orders/StatusChangeReason.aspx?Status=' + status + '&PrevStatus=' + orderStatus + '&OrderId=' + OrderId);
        }

        function ShowRefundRejectPopup(refundPaymentId) {
            ShowReasonModal('../../Popups/StoreManager/Orders/StatusChangeReason.aspx?Type=Refund&RefundPaymentId=' + refundPaymentId);
            return false;
        }

        function ShowReasonModal(popupPath) {
            reasonPopup = dhtmlmodal.open('ShowReasonPopup', 'iframe', popupPath, 'ShowReasonPopup', 'width=405px,height=330px,center=1,resize=0,scrolling=1');
            reasonPopup.onclose = function () {
                var a = document.getElementById('ShowReasonPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        var isOrderStatusChanged = false;
        function HideReasonPopup(type) {
            reasonPopup.hide();

            if (type == "1") {
                if (isOrderStatusChanged == true)
                    OrderCallback.callback('OrderStatus:');
                else {
                    var ddl = document.getElementById("<%=ddlStatus.ClientID %>");
                    if (ddl != null)
                        ddl.options[0].selected = true;

                }
            }
            else {
                displayMessage('');
            }
        }

        function onstatusChange(sender) {
            orderStatus = sender.options[0].text;
            var status = sender.value;
            ShowReasonPopup(status);
        }

        var gridItem;
        var currentGrid;
        function mnuShipment_onItemSelect(sender, eventArgs) {
            reloadAfterDelete = false;
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();
            currentUserObject = sender;
            var thisRowIndex = gridItem.getMember(idColumn).get_value();
            var isDownloadableMedia = gridItem.getMember(isDownloadableMediaColumn).get_value();
            //alert(gridItem.getMember(qtyColumn).get_value());

            if (selectedMenuId != null) {
                switch (selectedMenuId) {
                    case 'cmAdd':
                        ShowAddProductPopup();
                        break;
                    case 'cmEdit':
                        var isBundleChild = CheckIsBundleChild(thisRowIndex);
                        var isBundleCompositionModified = CheckBundleCompositionModified(thisRowIndex);
                        if (isBundleChild == 'False' && isBundleCompositionModified == 'False') {
                            if (!isDownloadableMedia)
                                currentGrid.edit(gridItem);
                            else
                                alert('Downloadble media item cannot be edited');
                        }
                        else if (isBundleChild != 'False') {
                            alert('Item is a child of a bundle, only the parent order item bundle can be edited');
                        }
                        else {
                            alert("This bundle's composition has been modified. This order item cannot be edited, only deleted.");
                        }
                        break;
                    case 'cmDelete':
                        var isBundleChild = CheckIsBundleChild(thisRowIndex);
                        if (isBundleChild == 'False') {
                            if (confirm("Are you sure you want to delete this item?")) {
                                reloadAfterDelete = true;
                                currentGrid.set_callbackParameter('Delete:' + thisRowIndex);
                                currentGrid.callback();

                            }
                        }
                        else {
                            alert('Item is a child of a bundle, only the parent order item bundle can be deleted');
                        }
                        break;
                }
            }
        }
        var reloadAfterDelete;
        function ShowCaptureAmountPopup(orderPaymentId) {
            popupPath = "../../Popups/StoreManager/Orders/CaptureAmount.aspx?OrderPaymentId=" + orderPaymentId;
            captureAmountPopup = dhtmlmodal.open('CaptureAmount', 'iframe', popupPath, 'Capture Amount Popup', 'width=395px,height=165px,center=1,resize=0,scrolling=1');
            captureAmountPopup.onclose = function () {
                var a = document.getElementById('CaptureAmount');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function ShowRefundAmountPopup(qsName, orderPaymentId) {
            popupPath = stringformat("../../Popups/StoreManager/Orders/RefundAmount.aspx?{0}={1}", qsName, orderPaymentId);
            refundAmountPopup = dhtmlmodal.open('RefundAmount', 'iframe', popupPath, 'Refund Amount Popup', 'width=395px,height=165px,center=1,resize=0,scrolling=1');
            refundAmountPopup.onclose = function () {
                var a = document.getElementById('RefundAmount');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function ShowApplyPaymentPopup(orderId) {
            ShowApplyPaymentPopup(orderId, 0)
        }

        function ShowApplyPaymentPopup(orderId, isNew) {
            $("#<%= txtExternalOrderNumber.ClientID %>").focus();

            popupPath = "../../Popups/StoreSetup/Payments/ApplyPaymentToOrder.aspx?OrderIdForPayment=" + orderId + "&IsNew=" + isNew;
            applyPaymentPopup = dhtmlmodal.open('ApplyPaymentToOrder', 'iframe', popupPath, 'Apply Payment to Order', 'width=705px,height=450,center=1,resize=0,scrolling=1');
            applyPaymentPopup.onclose = function () {
                var a = document.getElementById('ApplyPaymentToOrder');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                var currentUrl = document.URL;
                // window.location = currentUrl;
                return true;
            }
        }

        function CloseApplyPaymentPopup() {
            applyPaymentPopup.hide();
            //runAllCallbacks();
          //  window.location = currentUrl;
            //OrderCallback.callback('DisplayTotal:');
        }

        function fnSelectCC(ccnumber) {
            if (browser != 'mozilla') {
                copyToClipboard(ccnumber);
                alert('Encrypted credit card has been copied to your clipboard.');
            }
            else //mozilla show an alert
            {
                alert('Your browser doesn\'t support copying to clipboard, please copy from here \n ' + ccnumber);
            }
        }

        function copyToClipboard(s) {

            if (window.clipboardData && clipboardData.setData) {
                clipboardData.setData("Text", s);
            }
        }

        function CloseProductPopup(arrSelected) {
            productSearchPopup.hide();
            if (arrSelected != '') {
                if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
                    var skuIds = '';
                    while (arrSelected.length != 0) {
                        skuIds += ',' + arrSelected.pop()[1];
                    }
                    if (skuIds != '') {
                        skuIds = skuIds.substring("1");
                        AddSkus(skuIds);
                        var divPaymentsAndCoupons = document.getElementById("<%=divPaymentsAndCoupons.ClientID%>");
                        divPaymentsAndCoupons.style.display = "block";
                    }
                }
            }
        }

        function SaveExternalPo(txtControl) {
            if (OrderId != emptyGuid) {
                var control = document.getElementById(txtControl);
                UpdateExternalPO(OrderId, control.value);
            }
        }
        $(document).ready(function () {
            ToggleSlideImage();
        });
        function ToggleSlideImage() {
            //hide the all of the element with class msg_body
            $('.snap-content').hide();
            //toggle the componenet with class msg_body
            $('.snap-toggle').unbind('click').click(function () {
                $(this).parents('.snap-header').next('.snap-content').slideToggle('normal', function () {
                    if ($(this).is(':hidden')) {
                        $(this).prev('.snap-header').children('.snap-toggle').attr('src', '../../App_Themes/General/images/snap-expand.png');
                    }
                    else {
                        $(this).prev('.snap-header').children('.snap-toggle').attr('src', '../../App_Themes/General/images/snap-collapse.png');
                    }
                });
            });
        }
        function fn_OpenReturnItems() {
            var popupPath = 'InitiateReturn.aspx';
            viewPopupWindow = dhtmlmodal.open('', '', popupPath, '', '');
        }
        function fn_RedirectForReturns() {
            window.location = 'ReturnDetails.aspx';
        }
        function OrderError_onCallbackError(sender, eventArgs) {

            alert('Order Callback error: ' + eventArgs.get_message());
        }
        function AddNewCallback_onCallbackError(sender, eventArgs) {

            alert('Destination callback error: ' + eventArgs.get_message());
        }

        function Coupon_onCallbackError(sender, eventArgs) {

           alert('Coupon callback error: ' + eventArgs.get_message());
        }
        function DestSummary_onCallbackError(sender, eventArgs) {

           // alert('DestSummary callback error: ' + eventArgs.get_message());
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <div class="order-details">
        <ComponentArt:Menu ID="mnuShipment" runat="server" SkinID="ContextMenu">
            <ClientEvents>
                <ItemSelect EventHandler="mnuShipment_onItemSelect" />
            </ClientEvents>
        </ComponentArt:Menu>
        <ComponentArt:CallBack ID="OrderCallback" OnCallback="Order_Callback" runat="server">
            <Content>
                <div class="page-header clear-fix">
                    <h1><asp:Literal ID="ltOrderNo" runat="server" Text="NEW ORDER"></asp:Literal></h1>
                    <asp:Button ID="btnCreateOrder" runat="server" Text="Place Order" Style="display: none;
                        float: right;" ToolTip="Place Order" CssClass="primarybutton" UseSubmitBehavior="true" />
                    <asp:Button ID="btnDeleteOrder" runat="server" OnClick="btnDeleteOrder_Click" Text="Cancel Order"
                        Visible="false" ToolTip="Cancel Order" CssClass="button" UseSubmitBehavior="true"
                        Style="float: right; margin-left: 10px;" />
                </div>
                <hr />
                <asp:HyperLink ID="hplOrderNotes" runat="server" Text="Order Notes" CssClass="small-button"
                    Style="float: right;" />
                <asp:Button ID="btnEmailReceipt" runat="server" Text="Email Receipt" CssClass="small-button"
                    OnClick="btnEmailReceipt_Click" Style="float: right;" />
                <div class="columns" style="width: 450px">
                    <h3><%= GUIStrings.OrderDetails %></h3>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.Name1 %></label>
                        <div class="form-value">
                            <asp:HyperLink ID="hplName" runat="server" Text="Select Customer" ToolTip="Click to select a customer"
                                NavigateUrl="#" CssClass="selectCustomer" ></asp:HyperLink>
                            <asp:HiddenField ID="hdnCustomerId" runat="server" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.OrderDateColon %></label>
                        <div class="form-value">
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.OrderNumber1 %></label>
                        <div class="form-value">
                            <asp:Label ID="lblOrderNumber" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.CustomerPO1 %></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtExternalOrderNumber" runat="server" MaxLength="100" Width="211"
                                CssClass="textbox"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.OrderStatus1 %></label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlStatus" runat="server" Width="218" onchange="onstatusChange(this);"
                                CssClass="selectbox">
                                <asp:ListItem Text="Pending Shipment" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblOrderStatus" runat="server" Visible="false" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.AccountNumber1 %></label>
                        <div class="form-value">
                            <asp:Label ID="lblAccountNumber" runat="server" Text=""></asp:Label>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.CustomerEmail1 %></label>
                        <div class="form-value">
                            <asp:HyperLink ID="hplCustomerEmail" runat="server" NavigateUrl="#"></asp:HyperLink>
                        </div>
                    </div>
                    <div class="form-row" id="spnPlacedBy" runat="server">
                        <label class="form-label">
                            <%= GUIStrings.PlacedBy1 %></label>
                        <div class="form-value">
                            <asp:Label ID="lblPlacedBy" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <uc1:OrderTotals ID="ucOrderTotal" runat="server" />
                <div class="clear-fix" style="margin-bottom: 30px;">
                    &nbsp;</div>
                <div id="globalAttributesContainer" runat="server" visible="false" class="snap-container">
                    <div class="snap-header">
                        <img class="snap-toggle" alt="Collpase Global Attributes" src="../../App_Themes/General/images/snap-expand.png" />
                        <h3><%= GUIStrings.GlobalAttributes %></h3>
                        <asp:Button ID="btnSaveGlobalAttributes" runat="server" ToolTip="Save Global Attributes" style="float: right;"
                            Text="Save Global Attributes" CssClass="small-button" OnClick="btnSaveGlobalAttributes_Click" />
                        <div class="clear-fix"></div>
                    </div>
                    <div class="snap-content">
                        <GA:GlobalAttributes ID="globalAttributes" runat="server" />
                    </div>
                </div>
            </Content>
            <ClientEvents>
                <CallbackComplete EventHandler="Order_CallbackComplete" />
                <CallbackError EventHandler="OrderError_onCallbackError" />
            </ClientEvents>
        </ComponentArt:CallBack>
        <div id="divPaymentsAndCoupons" runat="server">
            <ComponentArt:CallBack ID="CouponCallback" runat="server" OnCallback="CouponCallback_callback" PostState="true">
                <Content>
                    <ps:Coupon runat="server" ID="ctlCoupons" />
                </Content>
             <ClientEvents>
                <CallbackError EventHandler="Coupon_onCallbackError" />
            </ClientEvents>
            </ComponentArt:CallBack>
            <ps:Payment ID="ctlPayments" runat="server" />
        </div>
        <div class="snap-container" style="border-top: 0;" id="divReturns" runat="server" visible="false">
            <div class="snap-header">
                <img class="snap-toggle" alt="Collpase Global Attributes" src="../../App_Themes/General/images/snap-expand.png" />
                <h3>
                    <%= GUIStrings.Order_Returns %></h3>
                <asp:HyperLink runat="server" ID="lnkReturnItems" NavigateUrl="javascript: fn_OpenReturnItems('/StoreManager/Orders/InitiateReturn.aspx');"
                    Text="Return Items" Style="float: right;" CssClass="small-button" />
                <div class="clear-fix">
                </div>
            </div>
            <div class="snap-content">
                <uc1:RMAListingControl ID="RMAListing" runat="server" HideFooter="true" />
            </div>
        </div>
        <div class="ShipmentDetailsContainer" id="divShipmentDetailsContainer" runat="server"
            style="display: none;">
              <h1 style="margin-top: 30px;">
                <%= GUIStrings.Orders_ShippingSummary %></h1>
            <asp:Button ID="lbtnAddShippingAddress" runat="server" Text="Add new Destination"
                CssClass="button" OnClientClick="return IsCustomerSelected();" CausesValidation="false"
                OnClick="lbtnAddShippingAddress_Click" Style="margin: 35px 30px 0 30px;"></asp:Button>
            <div class="clear-fix">
            </div>
            <ComponentArt:CallBack ID="AddNewCallback" OnCallback="AddNew_Callback" runat="server" 
                PostState="true">
                <Content>
              
                    <asp:DataList runat="server" ID="dlShippingList" OnItemDataBound="dlShippingList_ItemDataBound"
                        Width="99%">
                        <ItemTemplate>
                            <ps:Shipment ID="ctlShipment" runat="server" />
                        </ItemTemplate>
                    </asp:DataList>
                </Content>
                <LoadingPanelClientTemplate>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center">
                                Loading...
                            </td>
                        </tr>
                    </table>
                </LoadingPanelClientTemplate>
                <ClientEvents>
                    <CallbackComplete EventHandler="AddNew_CallbackComplete" />
                    <CallbackError EventHandler="AddNewCallback_onCallbackError" />
                </ClientEvents>
            </ComponentArt:CallBack>
        </div>
        <script type="text/javascript">
            SelectedCustomerId = document.getElementById("<%=hdnCustomerId.ClientID%>").value;

            document.onkeypress = stopRKey;
        </script>
    </div>
</asp:Content>
