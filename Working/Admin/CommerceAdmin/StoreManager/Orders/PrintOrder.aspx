﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintOrder.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.PrintOrder" %>
<%@ Register TagPrefix="OR" TagName="Ticket" Src="~/UserControls/StoreManager/Orders/PrintTicket.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:localize runat="server" text="<%$ Resources:GUIStrings, iAPPSCommercePickTicket %>"/></title>
    <style type="text/css">
        body, table { padding: 10px; font-size: 12px; font-family:Arial; }
        .oddRow td, .evenRow td, .headRow th { border-bottom: dashed 1px #cccccc; padding-bottom: 3px; }
        .header { font-size: 18px; font-weight: bold; padding: 5px 0; border-bottom: solid 3px #000000; text-align: center; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td colspan="2">
                    <asp:Image ID="imgLogo" runat="server" ImageUrl="~/images/commerce-logo.png" AlternateText="<%$ Resources:GUIStrings, iAPPSCommerce %>" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="rptOrders" runat="server" OnItemDataBound="rptOrders_DataBound">
                        <ItemTemplate>
                            <or:ticket id="ctlPrintTickets" runat="server"></or:ticket>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
