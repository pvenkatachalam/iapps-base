﻿<%@ Page Title="iAPPS Commerce: Store Manager: Orders: Manager Orders" Language="C#"
    MasterPageFile="~/StoreManager/Orders/OrderMaster.master" AutoEventWireup="true"
    CodeBehind="SiteOrderListing.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SiteOrderListing"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="CO" TagName="Orders" Src="~/UserControls/StoreManager/Orders/OrderListing.ascx" %>
<asp:Content ID="orderContent" runat="server" ContentPlaceHolderID="cphHeaderButtons">
    <asp:HyperLink ID="hplNewOrder" runat="server" Text="Place New Order" NavigateUrl="~/StoreManager/Orders/NewOrders.aspx?OrderId=00000000-0000-0000-0000-000000000000"
        CssClass="primarybutton"></asp:HyperLink>
</asp:Content>
<asp:Content ID="contentOrderListing" runat="server" ContentPlaceHolderID="OrderContentHolder">
    <div class="order-list">
        <CO:Orders ID="ctlOrderListing" runat="server" DefaultDateRangeOption="PredefinedRange">
        </CO:Orders>
    </div>
</asp:Content>
