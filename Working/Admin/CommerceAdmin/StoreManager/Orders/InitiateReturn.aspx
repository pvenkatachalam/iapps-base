﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InitiateReturn.aspx.cs" StylesheetTheme="General"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.InitiateReturn" MasterPageFile="~/Popups/iAPPSPopup.Master"
    EnableEventValidation="false" %>
<%@ Register src="../../UserControls/StoreManager/Orders/SelectReturnItems.ascx" tagname="SelectReturnItems" tagprefix="uc3" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server"></asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <uc3:SelectReturnItems ID="ucSelectItemsForReturn" runat="server" IsListEditable="true" ValidationGroup="vgInitiation" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, Continue %>" onclick="btnContinue_Click" CssClass="primarybutton"  ValidationGroup="vgInitiation" />
    <a href="javascript: parent.viewPopupWindow.hide();" class="button"><%= SiteSettings.Returns_CancelReturn%></a>
</asp:Content>
