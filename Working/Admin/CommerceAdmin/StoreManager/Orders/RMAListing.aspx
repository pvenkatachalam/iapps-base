﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RMAListing.aspx.cs" StylesheetTheme="General"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.StoreManager.Orders.RMAListing" MasterPageFile="~/MainMaster.Master"
    EnableEventValidation="false" %>

<%@ Register src="../../UserControls/StoreManager/Orders/RMAListingControl.ascx" tagname="RMAListingControl" tagprefix="uc1" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="head">
    <script type="text/javascript">
        function fn_SetDefaultText(objTextbox) {
            var v_DefaultSearchText = '<%= GUIStrings.RMA_SearchHelp%>';
            if (objTextbox.value.toLowerCase() == '') {
                $(objTextbox).val(v_DefaultSearchText);
                $(objTextbox).addClass('default-text');
            }
            else if (objTextbox.value == v_DefaultSearchText) {
                $(objTextbox).val('');
                $(objTextbox).removeClass('default-text');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <uc1:RMAListingControl ID="RMAListingControl1" runat="server" />
</asp:Content>
