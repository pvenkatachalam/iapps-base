﻿<%@ Page runat="server" Title="<%$ Resources:GUIStrings, iAPPSCommerceProductReviewSearch %>"
    Language="C#" MasterPageFile="~/StoreManager/Reviews/ReviewsMaster.Master" AutoEventWireup="true"
    Theme="General" CodeBehind="Search.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Search" %>

<%@ Register TagPrefix="iAPPS" TagName="DateRange" Src="~/UserControls/General/DateRange.ascx" %>
<%@ Register TagPrefix="iAPPS" TagName="Review" Src="~/UserControls/StoreManager/Reviews/ReviewListing.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphhead" runat="server">
    <script type="text/javascript">
        var hdnProductIds = "<%= hdnProductIds.ClientID %>";
        var selectedProducts;

        function displaySelectProductsPopup() {
            if ($("input#" + hdnProductIds).val() == "")
                addProductsPopup();
            else
                $("#selectProducts").dialog("open");
        }

        function addProductsPopup() {
            var popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx?showProducts=true&IncludeSkuNumber=false";

            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');

            productSearchPopup.onclose = function () {
                var a = document.getElementById('ProductSearchPopup');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function CloseProductPopup(arrSelected) {
            if (arrSelected != undefined && arrSelected != null & arrSelected != "") {
                if (typeof selectedProducts == "undefined")
                    selectedProducts = [];

                var product;
                var currentArr;
                while (arrSelected.length != 0) {
                    product = {};
                    currentArr = arrSelected.pop();
                    product.productId = currentArr[0];
                    product.productName = currentArr[2];

                    selectedProducts[selectedProducts.length] = product;
                }

                //selectedProducts = products;

                displaySelectedProductsList();
            }

            productSearchPopup.hide();
            CloseSelectProductsLayer();
        }

        function removeProductFromFilter(productId) {
            var products = [];

            for (var i = 0; i < selectedProducts.length; i++) {
                if (selectedProducts[i].productId != productId) {
                    products[products.length] = selectedProducts[i];
                }
            }

            selectedProducts = products;

            displaySelectedProductsList();
        }

        function displaySelectedProductsList() {
            var selectedProductsDisplay = "<table>";
            var selectedProductIds = "";
            var selectedProductsString = "";

            for (var i = 0; i < selectedProducts.length; i++) {
                selectedProductsDisplay += "<tr><td>" + selectedProducts[i].productName + "</td>";
                selectedProductsDisplay += "<td><a href=\"#\" onclick=\"removeProductFromFilter('" + selectedProducts[i].productId + "');\">[x]</a></td></tr>";
                if (i == 0)
                    $(".product-filter a").html(selectedProducts[i].productName);

                if (i < selectedProducts.length - 1) {
                    selectedProductIds += selectedProducts[i].productId + ",";
                    selectedProductsString += selectedProducts[i].productId + "," + selectedProducts[i].productName + ",";
                }
                else {
                    selectedProductIds += selectedProducts[i].productId;
                    selectedProductsString += selectedProducts[i].productId + "," + selectedProducts[i].productName;
                }
            }

            selectedProductsDisplay += "</table>";

            $("input#" + hdnProductIds).val(selectedProductIds);
            $("#selProductList").html(selectedProductsDisplay);
        }

        function CloseSelectProductsLayer() {
            $("#selectProducts").dialog("close");
        }

        function SearchReviews(keyword, startDate, endDate, status, productIds) {
            var reviewFilter = {};

            reviewFilter.strKeyword = keyword;

            reviewFilter.strStartDate = startDate;
            reviewFilter.strEndDate = endDate;
            reviewFilter.strProductIds = productIds;
            reviewFilter.strStatus = status;
            SearchReviewListing(reviewFilter);
        }

    </script>
</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ScriptManager runat="server" ID="sManager" ScriptMode="Debug" EnableCdn="true" EnablePartialRendering="true" />
    <asp:UpdatePanel runat="server" ID="upSearchReviews" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="grid-utility">
                <div class="rows">
                    <div class="columns">
                        <label class="form-label">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, KeywordFilter %>" /></label>
                        <asp:TextBox ID="txtKeywords" runat="server" CssClass="textBoxes" Width="240" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>" />
                    </div>
                    <div class="columns">
                        <label class="form-label">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DateFilter %>" /></label>
                        <iapps:DateRange ID="dateRange" runat="server" />
                    </div>
                    <div class="columns">
                        <label class="form-label">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, ProductFilter %>" /></label>
                        <div class="product-filter">
                            <a href="#" onclick="displaySelectProductsPopup();">
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, SelectProducts %>" /></a>
                        </div>
                        <asp:HiddenField ID="hdnProductIds" runat="server" />
                    </div>
                    <div class="columns">
                        <label class="form-label">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, StatusFilter %>" /></label>
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="120">
                            <asp:ListItem Value="0" runat="server" Text="<%$ Resources:GUIStrings, All %>" />
                            <asp:ListItem Value="4" runat="server" Text="<%$ Resources:GUIStrings, Approved %>" />
                            <asp:ListItem Value="1" runat="server" Text="<%$ Resources:GUIStrings, Unapproved %>" />
                        </asp:DropDownList>
                    </div>
                    <div class="columns">
                        <label class="form-label">
                            &nbsp;</label>
                        <asp:Button ID="btnSearch" runat="server" CssClass="small-button" Text="<%$ Resources:GUIStrings, Search %>" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="clear-fix">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <iapps:Review ID="reviewListing" runat="server" DisplayType="Search" />
</asp:Content>
