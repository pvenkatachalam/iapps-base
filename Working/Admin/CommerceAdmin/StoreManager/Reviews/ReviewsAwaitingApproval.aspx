<%@ Page runat="server" Title="<%$ Resources:GUIStrings, iAPPSCommerceProductReviewsAwaitingApproval %>"
    Language="C#" MasterPageFile="~/StoreManager/Reviews/ReviewsMaster.Master" AutoEventWireup="true"
    Theme="General" CodeBehind="ReviewsAwaitingApproval.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ReviewsAwaitingApproval" %>

<%@ Register TagPrefix="iAPPS" TagName="ProductTypeTree" Src="~/UserControls/StoreManager/Reviews/ProductTypeTree.ascx" %>
<%@ Register TagPrefix="iAPPS" TagName="Review" Src="~/UserControls/StoreManager/Reviews/ReviewListing.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphhead" runat="server">
    <script type="text/javascript">
        var selectedProductTypeId;

        function trvProductTypes_NodeSelected(tree) {
            selectedProductTypeId = tree.get_selectedNode().get_id();

            setTimeout(function () {
                ddlProductType.set_text(tree.get_selectedNode().get_text());
                ddlProductType.collapse();
            }, 100);
        }

        function SearchReviews() {
            var reviewFilter = {};
            reviewFilter.strProductTypeId = selectedProductTypeId;
            SearchReviewListing(reviewFilter);

            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="grid-utility">
        <div class="rows">
            <div class="columns">
                <label class="form-label"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProductType %>" /></label>
                <ComponentArt:ComboBox ID="ddlProductType" runat="server" SkinID="Default" DropDownHeight="250"
                    DropDownWidth="320" AutoHighlight="false" AutoComplete="false" Width="340">
                    <DropDownContent>
                        <iAPPS:ProductTypeTree ID="productTypeTree1" runat="server" NodeSelectJavascriptFunction="trvProductTypes_NodeSelected" />
                    </DropDownContent>
                </ComponentArt:ComboBox>
            </div>
            <div class="columns">
                <label class="form-label">&nbsp;</label>
                <asp:Button ID="btnSearch" runat="server" CssClass="small-button" Text="<%$ Resources:GUIStrings, Search %>"
                    OnClientClick="return SearchReviews();" />
            </div>
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <iAPPS:Review ID="reviewListing" runat="server" DisplayType="AwaitingApproval" />
</asp:Content>
