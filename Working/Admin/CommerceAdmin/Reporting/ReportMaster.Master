﻿<%@ Master Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true" CodeBehind="ReportMaster.master.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ReportMaster" %>
<%@ Register TagPrefix="rpDr" TagName="ReportDateRange" Src="~/UserControls/Reporting/ReportDateRange.ascx" %>
<%@ Register Assembly="Bridgeline.iAPPS.Admin.Commerce.Web" Namespace="Bridgeline.iAPPS.Admin.Commerce.Web" TagPrefix="expt" %>
    
<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript" defer="defer">
        function init() {
            Sys.Net.WebRequestManager.add_invokingRequest(beginRequest);
            Sys.Net.WebRequestManager.add_completedRequest(endRequest);
        }
        function beginRequest(sender, args) {
            ShowLoadingModalBox();
        }
        function endRequest(sender, args) {
            try {
                CloseLoadingModalBox();
            }
            catch (exp) {
            }
        }
        $(document).ready(function() {
            init();
        });
    </script>
<asp:ContentPlaceHolder ID="ReportsContentHead" runat="server"></asp:ContentPlaceHolder></asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <asp:ScriptManager ID="smReports" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="upReports" runat="server">
        <ContentTemplate>
            <div class="reports">
                <h1><asp:Literal ID="subPageHeader" runat="server" /></h1>
                <asp:HyperLink ID="hplBackOrderLog" runat="server" Text="<%$ Resources:GUIStrings, BackOrderLog %>" NavigateUrl="~/Reporting/BackOrders/BackOrderLog.aspx" CssClass="backorder-log" Visible="false" />
                <expt:ReportExportButton Id="reportExporter" runat="server" Text="<%$ Resources:GUIStrings, Export %>" CssClass="export"></expt:ReportExportButton>
                <rpDr:ReportDateRange id="ctlReportDateRange" runat="server"></rpDr:ReportDateRange>
                <div class="clear-fix"></div>
                <div class="nav-links">
                    <asp:HyperLink ID="hplReportBySales" Text="<%$ Resources:GUIStrings, Sales %>" NavigateUrl="~/Reporting/Sales/SalesByCategory.aspx" runat="server" />
                    <asp:HyperLink ID="hplReportByOrders" Text="<%$ Resources:GUIStrings, Orders %>" NavigateUrl="~/Reporting/Orders/OrdersByProduct.aspx" runat="server" />
                    <asp:HyperLink ID="hplReportSalesTax" Text="<%$ Resources:GUIStrings, SalesTax %>" NavigateUrl="~/Reporting/SalesTax/SalesTax.aspx" runat="server" />
                    <asp:HyperLink ID="hplReportShipperCost" Text="<%$ Resources:GUIStrings, ShippingCharges %>" NavigateUrl="~/Reporting/ShipperCost/ShipperCost.aspx" runat="server" />
                    <asp:HyperLink ID="hplReportAbandonedCarts" Text="<%$ Resources:GUIStrings, AbandonedCarts %>" NavigateUrl="~/Reporting/AbandonedCarts/AbandonedCarts.aspx" runat="server" />
                    <asp:HyperLink ID="hplProductManagement" Text="<%$ Resources:GUIStrings, ProductManagement %>" NavigateUrl="~/Reporting/ProductManagement/LowInventory.aspx" runat="server" />
                    <asp:HyperLink ID="hplCustomer" Text="<%$ Resources:GUIStrings, Customers %>" NavigateUrl="~/Reporting/Customers/CustomerStatistics.aspx" runat="server" />
                    <asp:HyperLink ID="hplProductMedia" Text="<%$ Resources:GUIStrings, ProductMedia %>" NavigateUrl="~/Reporting/ProductMedia/MediaDownloads.aspx" runat="server" />
                    <asp:HyperLink ID="hplBackOrders" Text="<%$ Resources:GUIStrings, BackOrders %>" NavigateUrl="~/Reporting/BackOrders/BackOrdersBySKU.aspx" runat="server" />
                </div>
                <div class="subnav-links">
                    <asp:PlaceHolder ID="phSalesLinks" runat="server" Visible="false">
                        <asp:HyperLink ID="hplSalesByCategory" runat="server" Text="<%$ Resources:GUIStrings, SalesByCategory %>" NavigateUrl="~/Reporting/Sales/SalesByCategory.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplSalesByProduct" runat="server" Text="<%$ Resources:GUIStrings, SalesByProduct %>" NavigateUrl="~/Reporting/Sales/SalesByProduct.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplSalesBySKU" runat="server" Text="<%$ Resources:GUIStrings, SalesBySKU %>" NavigateUrl="~/Reporting/Sales/SalesBySKU.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplSalesByRegion" runat="server" Text="<%$ Resources:GUIStrings, SalesByRegion %>" NavigateUrl="~/Reporting/Sales/SalesByRegion.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplSalesByPromoCode" runat="server" Text="<%$ Resources:GUIStrings, SalesByPromotionCode %>" NavigateUrl="~/Reporting/Sales/SalesByPromoCode.aspx"></asp:HyperLink>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="plOrderLinks" runat="server" Visible="false">
                        <asp:HyperLink ID="hplOrdersByProduct" runat="server" Text="<%$ Resources:GUIStrings, OrdersByProduct %>" NavigateUrl="~/Reporting/Orders/OrdersByProduct.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplOrdersByShipDate" runat="server" Text="<%$ Resources:GUIStrings, OrdersByDate %>" NavigateUrl="~/Reporting/Orders/OrdersByDate.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplOrderStatistics" runat="server" Text="<%$ Resources:GUIStrings, OrderStatistics %>" NavigateUrl="~/Reporting/Orders/OrderStatistics.aspx"></asp:HyperLink>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phProductLinks" runat="server" Visible="false">
                        <asp:HyperLink ID="hplLowIventory" runat="server" Text="<%$ Resources:GUIStrings, LowInventory %>" NavigateUrl="~/Reporting/ProductManagement/LowInventory.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplOrphanProducts" runat="server" Text="<%$ Resources:GUIStrings, ProductsMissingCategories %>" NavigateUrl="~/Reporting/ProductManagement/OrphanProducts.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hplProductMissingImages" runat="server" Text="<%$ Resources:GUIStrings, SKUsMissingImages %>" NavigateUrl="~/Reporting/ProductManagement/ProductMissingImages.aspx"></asp:HyperLink>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="phSalesTaxLinks" runat="server" Visible="false">
                        <asp:HyperLink ID="hlSalesTax" runat="server" Text="<%$ Resources:GUIStrings, SalesTax %>" NavigateUrl="~/Reporting/SalesTax/SalesTax.aspx"></asp:HyperLink>
                        <asp:HyperLink ID="hlSalesTaxByCategory" runat="server" Text="<%$ Resources:GUIStrings, SalesTaxByCategory %>" NavigateUrl="~/Reporting/SalesTax/SalesTaxByCategory.aspx"></asp:HyperLink>
                    </asp:PlaceHolder>
                </div>
                <asp:ContentPlaceHolder ID="ReportContentHolder" runat="server"></asp:ContentPlaceHolder>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
