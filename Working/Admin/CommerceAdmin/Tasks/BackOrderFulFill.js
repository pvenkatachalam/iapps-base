﻿/*********************************************************************************
* BackOrder Fullfillment Process
*********************************************************************************/

var strURLs = new Array(
                        "http://trunk.iapps.cms/commerceadmin/Tasks/FulFillBackOrder.aspx?PassKey=123456"
					   );

var webObj = WScript.CreateObject("MSXML2.ServerXMLHTTP");

var lResolve = 5 * 1000;
var lConnect = 5 * 1000;
var lSend = 30000 * 1000;
var lReceive = 30000 * 1000;
webObj.setTimeouts(lResolve, lConnect, lSend, lReceive);

for (var i = 0; i < strURLs.length; i++) {

    for (var l = 0; l < 15; l++) {
        webObj.open("GET", strURLs[i], false);
        try {
            webObj.send();
            if (webObj.status != 200 || String(webObj.statusText).match(/Database is currently unavailable/gi) != null) {
                WScript.echo("Hitting URL : " + strURLs[i]);
            }
            else
                break;
        }
        catch (e) {
            WScript.echo("Exception Found: " + strURLs[i]);
        }
    }
}