﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopViewedProducts.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TopViewedProducts" %>
<script type="text/javascript">
    function grdTopViewedProduct_onContextMenu(sender, eventArgs) {
        var evt = eventArgs.get_event();
        mnuTopViewedProduct.showContextMenuAtEvent(evt);
    }
    function mnuTopViewedProduct_onItemSelect(sender, eventArgs) {
    }
    function CreateTooltip(dataItemObject) {
        var navigationHierarchy = dataItemObject.GetMember('NavigationCategories').Text;
        var strHTML = SplitNavCategory(navigationHierarchy);
        strHTML = "<img onmouseover=\"ddrivetip('" + strHTML + "', 300,'');positiontip(event);\" onmouseout='hideddrivetip();' src='" + tooltipIconSrc + "' />";
        return strHTML;
    }
    //method to return the first category
    function FirstCategory(strNavCategories) {
        var arrNavigtaion = strNavCategories.split(',');
        var strHTML = "";
        if (arrNavigtaion.length > 0)
            strHTML = arrNavigtaion[0];
        return strHTML;
    }
</script>
<ComponentArt:Menu ID="mnuTopViewedProduct" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuTopViewedProduct_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<ComponentArt:Grid SkinID="Default" ID="grdTopViewedProduct" Width="100%" runat="server"
    RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" CallbackCachingEnabled="false"
    SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="grdTopViewedProductLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
            HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AllowSorting="true">
            <Columns>
                <ComponentArt:GridColumn Width="280" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                    DataField="Title" DataCellClientTemplateId="NameHoverTemplate" IsSearchable="true"
                    AllowReordering="false" FixedWidth="true" AllowSorting="False" TextWrap="true" />
                <ComponentArt:GridColumn Width="300" HeadingText="<%$ Resources:GUIStrings, NavigationCategory %>"
                    DataField="NavigationCategories" DataCellClientTemplateId="NavigationHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                    TextWrap="true" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, ProductViews  %>"
                    DataField="ProductViews" Align="Right" DataCellClientTemplateId="ProductViewsHoverTemplate"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" AllowSorting="False" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Details %>"
                    DataField="NavigationCategories" AllowReordering="false" FixedWidth="true" AllowSorting="False"
                    DataCellClientTemplateId="DetailsHoverTemplate" IsSearchable="true" Align="Center" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" AllowSorting="False" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameHoverTemplate">
            <span title="## DataItem.getMember('Title').get_text() ##">## DataItem.getMember('Title').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="NavigationHoverTemplate">
            <span title="## FirstCategory(DataItem.getMember('NavigationCategories').get_text()) ##">
                ## FirstCategory(DataItem.getMember('NavigationCategories').get_text()) ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductViewsHoverTemplate">
            <span title="## DataItem.getMember('ProductViews').get_text() ##">## DataItem.getMember('ProductViews').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DetailsHoverTemplate">
            ## CreateTooltip(DataItem) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdTopViewedProductLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdTopViewedProduct) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
