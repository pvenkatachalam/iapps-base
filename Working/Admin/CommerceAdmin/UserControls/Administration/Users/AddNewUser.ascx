<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.AddNewUser" %>
<script type="text/javascript">
    function validateUsername(oSrc, args) {
        var Emailpattern = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$/
        var UserPattern = /^[a-zA-Z0-9@.\-_]+[ ]*$/

        if (args.Value.indexOf("@") != -1 || args.Value.indexOf(".") != -1) // then Email validation
        {
            if (args.Value.search(Emailpattern) == -1 || (args.Value.length < 5 || args.Value.length > 256)) // If match failed
                args.IsValid = false;
            else
                args.IsValid = true;
        }
        else {
            if (args.Value.search(UserPattern) == -1 || (args.Value.length < 5 || args.Value.length > 256)) // If match failed
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function populateHiddenIndexList() {
        var listBox = document.getElementById(ListBoxName);
        var listcount = listBox.options.length;
        var valuetohiddenfield = "";
        var texttohiddenfield = "";
        for (i = 0; i < listcount; i++) {
            valuetohiddenfield += listBox.options[i].value + ",";
            texttohiddenfield += listBox.options[i].text + ",";
        }
        document.getElementById(TaxonomyIds).value = valuetohiddenfield;
        document.getElementById(IndexTextClientId).value = texttohiddenfield;
        listBox = document.getElementById(PermissionClientId);
        listcount = listBox.options.length;
        valuetohiddenfield = "";
        for (i = 0; i < listcount; i = i + 1) {
            valuetohiddenfield += listBox.options[i].value + "," + listBox.options[i].text + ",";
        }
        document.getElementById(HdnPermissionListClientId).value = valuetohiddenfield;
    }

    function popUpExpiryDateCalendar(ctl) {
        var thisDate = iAppsCurrentLocalDate;
        if (document.getElementById("<%=hdnDate.ClientID%>").value != "") {
            thisDate = new Date(document.getElementById("<%=hdnDate.ClientID%>").value);
            expirationCalendar.setSelectedDate(thisDate)
        }

        if (!(expirationCalendar.get_popUpShowing()));
        expirationCalendar.show(ctl);
    }

    function calExpiryDate_onSelectionChanged(sender, eventArgs) {
        var selectedDate = expirationCalendar.getSelectedDate();
        if (selectedDate <= iAppsCurrentLocalDate) {
            alert("<asp:localize runat='server' text='<%$ Resources:SiteSettings, TheExpirationDateCanNotBeBeforeTomorrowsDateAndDateFormatShouldBeMmddyyyy %>'/>");
        }
        else {
            document.getElementById("<%=expiryDate.ClientID%>").value = expirationCalendar.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
            document.getElementById("<%=hdnDate.ClientID%>").value = expirationCalendar.formatDate(selectedDate, calendarDateFormat);
        }
    }

    function OnArrowClick(lstSite, lstGroup, lstPermission, clickMode, addOrModifyUserPage) {
        lstSite = document.getElementById("<%= sites.ClientID %>");
        lstGroup = document.getElementById("<%= groups.ClientID %>");
        lstPermission = document.getElementById("<%= permissions.ClientID %>");

        //This if for adding permission for both addnew user & modify/delete user page.
        if (clickMode == 1) {
            //Move to Permission listbox
            var siteOptions = GetSelectedTextWithValueOfLstBox(lstSite);
            var groupOptions = GetSelectedTextWithValueOfLstBox(lstGroup);

            MoveSiteGroupsToPermission(siteOptions, groupOptions, lstPermission);
        }
        else if (clickMode == 0) {
            //Remove from Pemrisision listbox
            RemoveSelectedItemsFromLstBox(lstPermission);
        }
    }
    function GroupListCallback(selectedValue) {
        GroupCallBack.callback(selectedValue);
    }

    function ValidateEditUser() {
        if (Page_ClientValidate("valAddUser") == true) {
            var lstPermission = document.getElementById("<%= permissions.ClientID %>");
            if (lstPermission.options.length == 0) {
                alert(__JSMessages["PleaseSelectAtLeastOnePermissionForThisUser"]);
                return false;
            }

            listcount = lstPermission.options.length;
            var valuetohiddenfield = "";
            for (i = 0; i < listcount; i = i + 1) {
                valuetohiddenfield += lstPermission.options[i].value + "," + lstPermission.options[i].text + ",";
            }
            document.getElementById("<%= PermissionList.ClientID %>").value = valuetohiddenfield;

            var hdnPermission = document.getElementById("<%= HdnSelectedPermissions.ClientID %>");
            hdnPermission.value = "";
            for (var i = 0; i < lstPermission.options.length; i++) {
                hdnPermission.value += lstPermission.options[i].value + ",";
            }

            return true;
        }
        else {
            return false;
        }
    }

    function ShowImagePopup() {
        OpeniAppsAdminPopup("SelectImageLibraryPopup", "ForceEnableSelection=true", "SelectImage");
        return false;
    }

    function SelectImage() {
        imageUrl = popupActionsJson.CustomAttributes.ImageUrl;
        imageFileName = popupActionsJson.CustomAttributes.ImageFileName;
        imageId = popupActionsJson.SelectedItems[0];

        imageUrl = imageUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
        imageUrl = imageUrl.replace(new RegExp(imageFileName, "gi"), "thumbs/thumb_" + imageFileName);
        $("#imgImageURL").prop("src", imageUrl);
        $("#hdnImageId").val(imageId);
    }

    function ClearImageUrlValue() {
        $("#imgImageURL").prop("src", "/iapps_images/no_photo.jpg");
        $("#hdnImageId").val("");

        return false;
    }

</script>
<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="true" ValidationGroup="valAddUser" />
<div class="edit-section add-user">
    <div class="form-row profile-picture">
        <label class="form-label">
            <asp:Image runat="server" ID="imgImageURL" ImageUrl="/iapps_images/no_photo.jpg"
                ClientIDMode="Static" /></label>
        <div class="form-value">
            <a href="#" class="edit-image" onclick="return ShowImagePopup();">
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:SiteSettings, EditProfilePicture %>" /></a>
            <a href="#" class="remove-image" onclick="return ClearImageUrlValue();">
                <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:SiteSettings, Remove %>" /></a>
            <asp:HiddenField ID="hdnImageId" runat="server" ClientIDMode="Static" />
        </div>
    </div>
    <div class="columns">
        <div class="form-row">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:SiteSettings, FirstName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="firstName" runat="server" CssClass="textBoxes" Width="400" />
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="firstNameValidator"
                    ControlToValidate="firstName" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterTheUsersFirstName %>"
                    Display="None" SetFocusOnError="true" runat="server" ValidationGroup="valAddUser" />
                <asp:RegularExpressionValidator ID="userfirstNameValidator" ControlToValidate="firstName"
                    ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidFirstName %>" Display="None"
                    SetFocusOnError="true" runat="server" ValidationExpression="[a-zA-Z0-9 ']+[ ]*" ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, LastName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="lastName" runat="server" CssClass="textBoxes" Width="400" />
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="lastNameValidator"
                    ControlToValidate="LastName" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterTheUsersLastName %>"
                    Display="None" runat="server" ValidationGroup="valAddUser" />
                <asp:RegularExpressionValidator ID="userlastNameValidator" ControlToValidate="lastName"
                    ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidLastName %>" Display="None"
                    SetFocusOnError="true" runat="server" ValidationExpression="[a-zA-Z0-9 ']+[ ]*" ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:SiteSettings, Email %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="email" runat="server" CssClass="textBoxes" Width="400" />
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="emailValidator" ControlToValidate="email"
                    ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterTheEmailAddressForTheUser %>"
                    Display="None" runat="server" ValidationGroup="valAddUser" />
                <asp:RegularExpressionValidator ID="userEmailValidator" ControlToValidate="email"
                    ErrorMessage="<%$ Resources:SiteSettings, TheEmailAddressIsInvalidPleaseEnsureItIsFormattedAs %>"
                    Display="None" SetFocusOnError="true" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*"
                    ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:SiteSettings, EmailNotification %>" /></label>
            <div class="form-value">
                <asp:RadioButton ID="doNotify" Checked="true" Text="<%$ Resources:SiteSettings, On %>"
                    GroupName="EmailNotification" runat="server" />
                <asp:RadioButton ID="noNotify" Text="<%$ Resources:SiteSettings, Off %>" GroupName="EmailNotification"
                    runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:SiteSettings, ExpirationDate %>" /></label>
            <div class="form-value calendar-value">
                <asp:HiddenField ID="hdnDate" runat="server" />
                <asp:TextBox ID="expiryDate" runat="server" CssClass="textBoxes" Width="267" AutoCompleteType="none"></asp:TextBox>
                <span>
                    <img id="calendar_button" class="buttonCalendar" alt="<%= SiteSettings.OpenCalendar %>"
                        src="../../App_Themes/General/images/calendar-button.png" onclick="popUpExpiryDateCalendar(this);" />
                </span>
                <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="expirationCalendar"
                    MaxDate="9998-12-31" PopUpExpandControlId="calendar_button">
                    <ClientEvents>
                        <SelectionChanged EventHandler="calExpiryDate_onSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:Calendar>
                <asp:CompareValidator CultureInvariantValues="true" ID="cvexpiryDate" runat="server"
                    ControlToValidate="expiryDate" Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:SiteSettings, PleseEnterValidExpiryDate %>"
                    Display="None" ValidationGroup="valAddUser" />
                <asp:RangeValidator CultureInvariantValues="true" ID="expiryDateValidator" runat="server"
                    ControlToValidate="expiryDate" ErrorMessage="<%$ Resources:SiteSettings, TheExpirationDateCanNotBeBeforeTomorrowsDateAndDateFormatShouldBeMmddyyyy %>"
                    Display="None" SetFocusOnError="true" MaximumValue="9999-12-31" MinimumValue="1900-01-01"
                    Type="Date" ValidationGroup="valAddUser" />
            </div>
        </div>
    </div>
    <div class="columns right-column">
        <div class="form-row">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:SiteSettings, UserName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="userName" runat="server" CssClass="textBoxes" Width="400" />
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="usernameValidator"
                    ControlToValidate="userName" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAUsernameContainingAtLeastFiveCharacters %>"
                    Display="None" runat="server" ValidationGroup="valAddUser" />
                <asp:CustomValidator ID="userNameCheckCustomValidator" runat="server" ControlToValidate="userName"
                    Display="None" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAValidUserNameItShouldBeAtLeastFiveAlphanumericCharactersInLength %>"
                    ClientValidationFunction="validateUsername" ValidationGroup="valAddUser" />
                <%-- <asp:regularexpressionvalidator id="userNameCheckValidator" controltovalidate="userName"
    errormessage="<%$ Resources:ValidationMessages,UserNameNotValid %>" display="None"
            setfocusonerror="true" runat="server" validationexpression="([a-zA-Z0-9@.]+[ ]*){5,256}" />--%>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:SiteSettings, ExternalPassword %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="externalPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                    Width="400" />
                <asp:RequiredFieldValidator CultureInvariantValues="true" ID="externalPasswordValidator"
                    ControlToValidate="externalPassword" ErrorMessage="<%$ Resources:SiteSettings, PleaseEnterAPasswordForTheUser %>"
                    Display="None" runat="server" ValidationGroup="valAddUser" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:SiteSettings, ConfirmPassword %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="confirmPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                    Width="400" />
                <asp:CompareValidator CultureInvariantValues="true" ID="externalPasswordCompareValidator"
                    ControlToValidate="externalPassword" ControlToCompare="confirmPassword" Display="None"
                    ErrorMessage="<%$ Resources:SiteSettings, ThePasswordsYouEnteredDoNotMatchPleaseConfirmThePassword %>"
                    runat="server" ValidationGroup="valAddUser" />
            </div>
        </div>
    </div>
    <div class="clear-fix">
    </div>
    <div class="bottom-section clear-fix">
        <h4>
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:SiteSettings, AddUserPermissions %>" /></h4>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:SiteSettings, Sites %>" /></label>
            <div class="form-value">
                <asp:ListBox ID="sites" onchange="GroupListCallback(this.value);" runat="server"
                    Rows="6" SelectionMode="Single" Width="390" />
            </div>
        </div>
        <div class="action-column">
            <img src="../../../App_Themes/General/images/permission-plus.gif" alt="Plus" title="Plus" />
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:SiteSettings, Groups %>" /></label>
            <div class="form-value">
                <ComponentArt:CallBack OnCallback="GroupCallBack_OnCallback" ID="GroupCallBack" runat="server">
                    <Content>
                        <asp:ListBox SelectionMode="multiple" ID="groups" runat="server" Rows="6" Width="390" />
                    </Content>
                    <ClientEvents>
                        <BeforeCallback EventHandler="CheckSessionEventHandler" />
                    </ClientEvents>
                </ComponentArt:CallBack>
            </div>
        </div>
        <div class="action-column">
            <img src="../../../App_Themes/General/images/arrowRight.gif" id="LftArrow" runat="server"
                alt="<%$ Resources:SiteSettings, AddPermission %>" class="arrowImage actionImage"
                title="<%$ Resources:SiteSettings, AddPermission %>" /><br />
            <img src="../../../App_Themes/General/images/arrowLeft.gif" id="RhtArrow" runat="server"
                alt="<%$ Resources:SiteSettings, DeletePermission %>" class="actionImage" title="<%$ Resources:SiteSettings, DeletePermission %>" />
        </div>
        <div class="columns">
            <label class="form-label">
                <span class="req">&nbsp;*</span><asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:SiteSettings, CurrentPermissions %>" /></label>
            <div class="form-value">
                <asp:ListBox ID="permissions" runat="server" Rows="6" SelectionMode="multiple" EnableViewState="true"
                    Width="390" />
            </div>
        </div>
    </div>
</div>
<input type="hidden" runat="server" value="" id="PermissionList" />
<asp:HiddenField ID="HdnSelectedPermissions" runat="server" Value="" />
<asp:HiddenField ID="hdnUserId" runat="server" Value="" />
