﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AllCommerceUsers.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AllCommerceUsers" %>
<script type="text/javascript" language="javascript">
    function grdCommerceUser_onCallbackComplete(sender, eventArgs) {
        SetColumnVisibility();
    }

    function grdCommerceUser_onLoad(sender, eventArgs) {
        SetColumnVisibility();
    }

    function grdCommerceUser_onContextMenu(sender, eventArgs) {
        gridItem = eventArgs.get_item();
        var evt = eventArgs.get_event();
        menuItems = mnuCommerceUser.get_items();

        var statusMnuItem = menuItems.getItemById("DeactivateMenuItem");
        var deleteMnuItem = menuItems.getItemById("DeleteUserMenuItem");
        if (gridItem.getMember("UserId").get_text().toLowerCase() == jUserId.toLowerCase()) {
            statusMnuItem.set_visible(false);
            deleteMnuItem.set_visible(false);
        }
        else {
            statusMnuItem.set_visible(true);
            deleteMnuItem.set_visible(true);

            //the user is deactivated. so the context menu should show the activate menu
            if (gridItem.getMember('IsActivated').get_text() == "true")
                statusMnuItem.set_text(__JSMessages["ActivateUser"]);
            else
                statusMnuItem.set_text(__JSMessages["DeactivateUser"]);
        }

        mnuCommerceUser.showContextMenuAtEvent(evt);
    }

    function mnuCommerceUser_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var selectedMenuId = menuItem.get_id();

        var selectedUserId = gridItem.getMember("UserId").get_text();
        var selectedUserName = gridItem.getMember("UserName").get_text();
        var currentPermission = gridItem.getMember("AllPermissions").get_text();
        var memberType = 1;

        if (selectedMenuId != null) {
            var isConfirm = false;
            switch (selectedMenuId) {
                case 'DeleteUserMenuItem':
                case 'DeactivateMenuItem':
                    var deleteUser = selectedMenuId == "DeleteUserMenuItem" ? true : false;
                    var activateUser = FormatStatusColumn(gridItem.getMember("IsActivated").get_text()) == __JSMessages["ActiveStatus"] ? false : true;
                    if (deleteUser)
                        isConfirm = window.confirm(__JSMessages["AreYouSureYouWantToDeleteTheSelectedRecord"]);
                    else if (activateUser)
                        isConfirm = window.confirm(__JSMessages["AreYouSureYouWantToActivateTheSelectedUserGroup"]);
                    else
                        isConfirm = window.confirm(__JSMessages["AreYouSureYouWantToDactivateTheSelectedUserGroup"]);

                    if (isConfirm) {
                        var count = 0;
                        if (deleteUser || !activateUser)
                            count = CheckUserWorklows(selectedUserId, memberType, selectedUserName, deleteUser ? currentPermission : null);

                        if (count == -1) {
                            var sitesString = GetSiteLists(currentPermission);
                            alert(deleteUser ? __JSMessages["YouDoNotHavePermissionToDeleteThisUserFromTheFollowingSites"] : __JSMessages["YouDoNotHavePermissionToDeactivateThisUserFromTheFollowing"] + " " + sitesString); //DeactivatePermissionDenied
                        }
                        else if (count == 1) {
                            alert(deleteUser ? __JSMessages["ErrorDeleteFailed"] : __JSMessages["ErrorDeactivationFailed"]);
                        }
                        else if (count == 2) {
                            //If any siteadmins are trying to delete the install admins, then it is not allowed.
                            alert(__JSMessages["YouDoNotHavePermissionToDeleteDeactivate"]);
                        }
                        else {
                            //Delete the user directly by using the call back method.     
                            var statusOfOperation = DeleteDeactivate(selectedUserId, selectedUserName, deleteUser ? 1 : (activateUser ? 4 : 2), null);
                            if (statusOfOperation == "True") {
                                grdCommerceUser.callback();
                            }
                        }
                    }
                    break;
                case 'ViewPermissions':
                    var permissionHtml = "";
                    var permissionsArr = currentPermission.split("#");
                    for (var i = 0; i < permissionsArr.length - 1; i++) {
                        permissionHtml += "<li>" + permissionsArr[i] + "</li>";
                    }
                    if (permissionHtml != "")
                        $("#detailedPermissions p").html("<ul>" + permissionHtml + "</ul>");
                    $("#detailedPermissions h4").html(selectedUserName);
                    $("#detailedPermissions").dialog("open");
                    break;
                case 'EditUserMenuItem':
                    window.location = jCommerceAdminUrl + "/Administration/Users/AddNewUser.aspx?UserId=" + selectedUserId;
                    break;
            }
        }
    }

    function ClosePermissionsLayer() {
        $("#detailedPermissions").dialog("close");
    }

    function SearchCommerceUsers() {
        grdCommerceUser.set_callbackParameter(stringformat("{0}^{1}", $("#<%= txtSearch.ClientID %>").val(), $("#<%=ddlRole.ClientID %>").val()));
        grdCommerceUser.callback();
    }

    function SetColumnVisibility() {
        var gridc = grdCommerceUser.get_table().get_columns();
        var selectedRoleId = $("#<%= ddlRole.ClientID %>").val();
        if (selectedRoleId == "<%= IAPPSUSERSID %>") {
            gridc[3].set_visible(false);
            gridc[4].set_visible(false);
            gridc[11].set_visible(true);
        }
        else {
            gridc[3].set_visible(true);
            gridc[4].set_visible(true);
            gridc[11].set_visible(false);
        }
        grdCommerceUser.render();
    }

    function FormatStatusColumn(txtStatus) {
        if (txtStatus.toLowerCase() == "true")
            return __JSMessages["InActiveStatus"];
        else
            return __JSMessages["ActiveStatus"];
    }

    function CheckUserWorklows(selectedUserIdInGrid, memberType, username, sitegroupId) {
        var returnValueOfMethod = IsMemberHavingAnyWorkflow(selectedUserIdInGrid, memberType, username, sitegroupId); //1 : MemberType
        return returnValueOfMethod;
    }
</script>
<div class="grid-utility">
    <asp:TextBox ID="txtSearch" runat="server" Width="280" CssClass="textBoxes" ToolTip="<%$Resources:GUIStrings, TypeHereToFilterResults%>" />
    <asp:DropDownList ID="ddlRole" runat="server" Width="280" />
    <input type="button" class="small-button" value="<%= GUIStrings.Search %>"
        onclick="SearchCommerceUsers();" />
</div>
<ComponentArt:Grid AllowMultipleSelect="false" EmptyGridText="<%$ Resources:GUIStrings, NoUsersFound %>"
    RunningMode="Callback" SkinID="Default" ID="grdCommerceUser" SliderPopupClientTemplateId="grdCommerceUserSliderTemplate"
    SliderPopupCachedClientTemplateId="cachedgrdCommerceUserSliderTemplate" PagerInfoClientTemplateId="grdCommerceUserPageInfoTemplate"
    Width="996" runat="server" PageSize="10" CallbackCacheLookAhead="10" CallbackCachingEnabled="true"
    AutoAdjustPageSize="true" LoadingPanelClientTemplateId="grdCommerceUserLoadingPanelTemplate" Sort="UserName ASC">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="UserId" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn FixedWidth="true" Width="165" DataField="UserName" HeadingText="<%$ Resources:GUIStrings, UserName %>"
                    AllowReordering="false" DataCellClientTemplateId="grdCommerceUserUserNameRowToolTipAll" />
                <ComponentArt:GridColumn FixedWidth="true" Width="105" DataField="LastName" HeadingText="<%$ Resources:GUIStrings, LastName %>"
                    AllowReordering="false" DataCellClientTemplateId="grdCommerceUserLastNameRowToolTipAll" />
                <ComponentArt:GridColumn FixedWidth="true" Width="105" DataField="FirstName" HeadingText="<%$ Resources:GUIStrings, FirstName %>"
                    AllowReordering="false" DataCellClientTemplateId="grdCommerceUserFirstNameRowToolTipAll" />
                <ComponentArt:GridColumn FixedWidth="true" Width="165" DataField="GroupName" HeadingText="<%$ Resources:GUIStrings, GroupName %>"
                    DefaultSortDirection="Descending" DataCellClientTemplateId="grdCommerceUserGroupNameRowToolTipAll" />
                <ComponentArt:GridColumn Width="200" DataField="Permissions" HeadingText="<%$ Resources:GUIStrings, CurrentPermissions %>"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="grdCommerceUserPermissionsRowToolTipAll" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="UserId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="AllPermissions" Visible="false"
                    IsSearchable="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="Email" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="ExpiryDate" Visible="false"
                    IsSearchable="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="EmailNotification" Visible="false"
                    IsSearchable="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="IsActivated" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataCellClientTemplateId="grdCommerceUserIsActivatedToolTipAll" Width="60" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="ProductSuite" HeadingText="<%$ Resources:GUIStrings, ProductSuite %>" 
                    IsSearchable="false" AllowReordering="false" Width="200" DataCellClientTemplateId="grdCommerceUserProductSuiteRowToolTipiAPPS" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <BeforeCallback EventHandler="CheckSessionEventHandler" />
        <ContextMenu EventHandler="grdCommerceUser_onContextMenu" />
        <CallbackComplete EventHandler="grdCommerceUser_onCallbackComplete" />
        <Load EventHandler="grdCommerceUser_onLoad" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="grdCommerceUserSliderTemplate">
            <div class="nSliderPopup">
                <p>
                    ##_datanotload##</p>
                <p class="npaging">
                    ## stringformat(Page0Of1, DataItem.PageIndex + 1, grdCommerceUser.PageCount) ##
                    ## stringformat(Item0Of1, DataItem.PageIndex + 1, grdCommerceUser.RecordCount) ##
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="cachedgrdCommerceUserSliderTemplate">
            <div class="nSliderPopup">
                <h5>
                    ## DataItem.GetMember('UserName').Value ##</h5>
                <p>
                    ## DataItem.GetMember('LastName').Value + ', ' + DataItem.GetMember('FirstName').Value
                    ##</p>
                <p>
                    ## DataItem.GetMember('GroupName').Value ##</p>
                <p>
                    ## DataItem.GetMember('Email').Value ##</p>
                <p>
                    ## DataItem.GetMember('Permissions').Value ##</p>
                <p class="npaging">
                    ## stringformat(Page0Of1, DataItem.PageIndex + 1, grdCommerceUser.PageCount) ##
                    ## stringformat(Item0Of1, DataItem.PageIndex + 1, grdCommerceUser.RecordCount) ##
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserPageInfoTemplate">
            ## stringformat(Page0Of1Items, currentPageIndex(grdCommerceUser), pageCount(grdCommerceUser),
            grdCommerceUser.RecordCount) ##
        </ComponentArt:ClientTemplate>
        <%-- Tool Tip for Datagrid Start --%>
        <ComponentArt:ClientTemplate ID="grdCommerceUserUserNameRowToolTipAll">
            <span title="## ChangeSpecialCharacters(DataItem.getMember('UserName').get_text()) ##">
                ## DataItem.getMember('UserName').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserLastNameRowToolTipAll">
            <span title="## ChangeSpecialCharacters(DataItem.getMember('LastName').get_text()) ##">
                ## DataItem.getMember('LastName').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserFirstNameRowToolTipAll">
            <span title="## ChangeSpecialCharacters(DataItem.getMember('FirstName').get_text()) ##">
                ## DataItem.getMember('FirstName').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserGroupNameRowToolTipAll">
            <span title="## ChangeSpecialCharacters(DataItem.getMember('GroupName').get_text()) ##">
                ## DataItem.getMember('GroupName').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserPermissionsRowToolTipAll">
            <span title="## ChangeSpecialCharacters(DataItem.getMember('Permissions').get_text()) ##">
                ## DataItem.getMember('Permissions').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserProductSuiteRowToolTipiAPPS">
            <span title="## ChangeSpecialCharacters(DataItem.getMember('ProductSuite').get_text()) ##">
                ## DataItem.getMember('ProductSuite').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserIsActivatedToolTipAll">
            <span title="## FormatStatusColumn(DataItem.getMember('IsActivated').get_text()) ##">
                ## FormatStatusColumn(DataItem.getMember('IsActivated').get_text()) ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdCommerceUserLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdCommerceUser) ##
        </ComponentArt:ClientTemplate>
        <%-- Tool Tip for Datagrid End --%>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuCommerceUser" SkinID="ContextMenu" runat="server">
    <ClientEvents>
        <ItemSelect EventHandler="mnuCommerceUser_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<div id="detailedPermissions"style="display: none;">
    <h4></h4>
    <p>No permission assigned for the selected user.</p>
    <div class="clearFix"></div>
</div>
<script type="text/javascript">
    $("#detailedPermissions").dialog({
        autoOpen: false,
        modal: true,
        width: "377px",
        title: "<%= SiteSettings.CurrentPermissions %>",
        buttons: {
            "<%= SiteSettings.Close %>": function () { $(this).dialog("close"); }
        }
    });
</script>
