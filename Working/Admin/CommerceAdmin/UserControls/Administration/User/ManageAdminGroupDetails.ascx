﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ManageAdminGroupDetails.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageAdminGroupDetails" %>
<script type="text/javascript">
    function UpdateAvailableUser() {
        grdAvailableUsers.set_callbackParameter($("#ddlType").val());
        grdAvailableUsers.callback();

        return false;
    }

    function AddSelectedUserToGroup() {
        for (var selIndex = 0; selIndex < grdAvailableUsers.getSelectedItems().length; selIndex++) {
            var gSelItem = grdAvailableUsers.getSelectedItems()[selIndex];

            if (grdSelectedUsers.getItemFromKey(0, gSelItem.getMemberAt(0).get_text()) == null) {
                grdSelectedUsers.beginUpdate();

                var newItem = grdSelectedUsers.get_table().addEmptyRow(0);
                grdSelectedUsers.edit(newItem);

                newItem.setValue(0, gSelItem.getMemberAt(0).get_text());
                newItem.setValue(1, gSelItem.getMemberAt(1).get_text());
                newItem.setValue(2, $("#ddlType").val());
                grdSelectedUsers.editComplete();
            }
        }
        grdAvailableUsers.unSelectAll();
    }

    function RemoveSelectedUserFromGroup() {
        grdSelectedUsers.deleteSelected();
    }

    function validateGroupName(oSrc, args) {
        var GroupPattern = /^[a-zA-Z0-9 ]+$/

        if (args.Value.search(GroupPattern) == -1 || args.Value.length > 256) // If match failed
            args.IsValid = false;
        else
            args.IsValid = true;
    }

    function validateDescription(sender, args) {
        var description = args.Value;
        var compare1 = "<";
        var compare2 = ">";

        if (description.search(compare1) == -1 || description.search(compare2) == -1)
            args.IsValid = true;
        else
            args.IsValid = false;
    }

    function ValidateEditGroup() {
        if (Page_ClientValidate("valSaveUser") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
    ValidationGroup="valSaveUser" />
<div class="dark-section-header clear-fix">
    <h2><%= GUIStrings.GROUPDETAILS %></h2>
</div>
<div class="top-section">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, GroupNameColon %>" /><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtGroupName" CssClass="textBoxes" runat="server" Width="280" />
            <asp:RequiredFieldValidator ID="RFVGroupName" runat="server" Display="None" SetFocusOnError="true" ValidationGroup="valSaveUser"
                ControlToValidate="txtGroupName" ErrorMessage="<%$ Resources: JSMessages, GroupNameRequired %>"></asp:RequiredFieldValidator>
            <asp:CustomValidator ID="groupNameCheckCustomValidator" runat="server" ControlToValidate="txtGroupName" ValidationGroup="valSaveUser"
                Display="None" ErrorMessage="<%$ Resources: JSMessages, GroupNameNotValid %>"
                ClientValidationFunction="validateGroupName">
            </asp:CustomValidator>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, DescriptionColon %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtDescription" CssClass="textBoxes" runat="server" Width="280"
                TextMode="MultiLine" />
            <asp:CustomValidator ID="DescriptionValidator" ClientValidationFunction="validateDescription"
                runat="server" ControlToValidate="txtDescription" ErrorMessage="<%$ Resources: JSMessages, DescriptionNotValid %>"
                Display="None" SetFocusOnError="true"></asp:CustomValidator>
        </div>
    </div>
</div>
<div class="dark-section-header clear-fix">
    <h2><%= GUIStrings.AddUsersGroups %></h2>
</div>
<div class="bottom-section clear-fix">
    <div class="columns">
        <h4><%= GUIStrings.SelectUserGroups %></h4>
        <div class="grid-utility clear-fix">
            <div class="search-box iapps-client-grid-search">
                <input id="txtSearch" type="text" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                    style="width: 160px;" />
                <input type="button" class="button small-button" value="<%= GUIStrings.Filter %>" onclick="SearchClientGrid('txtSearch', grdAvailableUsers);" />
            </div>
            <select onchange="UpdateAvailableUser();" id="ddlType">
                <option value="1"><%= GUIStrings.User %></option>
                <option value="2"><%= GUIStrings.Group %></option>
            </select>
        </div>
        <div class="scroll-box">
            <ComponentArt:Grid ID="grdAvailableUsers" SkinID="Default" runat="server" Width="100%"
                AllowEditing="true" EditOnClickSelectedItem="false" RunningMode="Callback" CssClass="fat-grid"
                ShowFooter="false" LoadingPanelClientTemplateId="grdAdminGroupAvailableLoadingPanelTemplate" EmptyGridText="<%$ Resources:GUIStrings, NoUsersFound %>">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate" />
                            <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                        <Template>
                            <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["Id"]%>'>
                                <div class="first-cell">
                                    <%# Container.DataItem["CurrentRowIndex"]%>
                                </div>
                                <div class="middle-cell">
                                    <%# Container.DataItem["Title"]%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdAdminGroupAvailableLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdAvailableUsers) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
    <div class="action-column">
        <img id="rightArrow" src="~/App_Themes/General/images/action-right-arrow.gif" title="<%$ Resources:GUIStrings, ClickToAdd %>"
            alt="<%$ Resources:GUIStrings, ClickToAdd %>" runat="server" onclick="return AddSelectedUserToGroup();" /><br />
        <img id="leftArrow" src="~/App_Themes/General/images/action-left-arrow.gif" title="<%$ Resources:GUIStrings, ClickToRemove %>"
            alt="<%$ Resources:GUIStrings, ClickToRemove %>" runat="server" onclick="return RemoveSelectedUserFromGroup();" />
    </div>
    <div class="columns right-column">
        <h4>
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, SelectedUsersGroups %>" /></h4>
        <div class="scroll-box">
            <ComponentArt:Grid ID="grdSelectedUsers" SkinID="Default" runat="server" Width="100%"
                RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdAdminGroupSelectedLoadingPanelTemplate"
                EmptyGridText="<%$ Resources:GUIStrings, NoUsersGroupsSelected %>">
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Title" DataCellClientTemplateId="DetailsClientTemplateSelected" />
                            <ComponentArt:GridColumn DataField="Type" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplateSelected" runat="server">
                        <Template>
                            <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["Id"]%>'>
                                <div class="first-cell">
                                    <%# GetGridItemRowNumber(Container.DataItem)%>
                                </div>
                                <div class="middle-cell">
                                    <%# Container.DataItem["Title"]%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="DetailsClientTemplateSelected">
                        <div class="collection-row grid-item clear-fix">
                            <div class="first-cell">
                                ## GetGridItemRowNumber(grdSelectedUsers, DataItem) ##
                            </div>
                            <div class="middle-cell">
                                ## DataItem.getMember('Title').get_text() ##
                            </div>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdAdminGroupSelectedLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdSelectedUsers) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</div>
