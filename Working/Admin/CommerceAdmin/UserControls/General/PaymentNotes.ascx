﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentNotes.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.General.PaymentNotes" %>
<script type="text/javascript">
    function ValidateNotes() {
        var notes = Trim(document.getElementById("<%=txtNotes.ClientID%>").value);

        if (notes == '' || notes == "<%= GUIStrings.AddANote %>") {
            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NotesCanNotBeEmpty %>' />");
            return false;
        }
        else {
            if (notes.match(/[\<\>]/)) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheNotes %>' />");
                return false;
            }
            else {
                return true;
            }
        }
    }

    function GetHoverText(text) {
        return text.replace(/<br\/>/g, "\n");
    }
    var gridItem;
    function mnuNote_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var selectedMenuId = menuItem.get_id();
        if (selectedMenuId == 'cmSelect') {
            var id = gridItem.getMember(4).get_value();
            var objectType = gridItem.getMember(5).get_value();
            if (objectType == 'Order') {
                window.location = '../../StoreManager/Orders/NewOrders.aspx?OrderId=' + id;
            }
        }
    }
    function grdNotes_onContextMenu(sender, eventArgs) {
        gridItem = eventArgs.get_item();
        var objectType = gridItem.getMember("OrderId").get_value();
        if (objectType == 'Order') {
            mnuNote.showContextMenuAtEvent(e);
        }
    }
</script>
<div class="button-row clear-fix">
    <asp:HyperLink ID="hplVieworder" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, ViewOrder %>"
        NavigateUrl="~/StoreManager/Orders/NewOrders.aspx" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, SaveNote %>" CssClass="primarybutton"
        OnClientClick="return ValidateNotes();" OnClick="btnSave_Click" />
</div>
<div class="grid-utility clear-fix">
    <div class="columns">
        <asp:TextBox ID="txtNotes" runat="server" CssClass="textBoxes" Width="500" TextMode="MultiLine"
            ToolTip="<%$ Resources:GUIStrings, AddANote %>" Rows="3"></asp:TextBox>
    </div>
</div>
<ComponentArt:Grid ID="grdNotes" SkinID="Default" runat="server" Width="100%" RunningMode="Client"
    PagerInfoClientTemplateId="CustomerNotesPaginationTemplate" AllowEditing="true"
    AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="CustomerNotesSliderTemplateCached"
    SliderPopupClientTemplateId="CustomerNotesSliderTemplate" AutoCallBackOnUpdate="false"
    CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" CallbackCachingEnabled="true"
    AllowMultipleSelect="false" EditOnClickSelectedItem="false" Sort="CreatedDate Desc">
    <ClientEvents>
        <ContextMenu EventHandler="grdNotes_onContextMenu" />
    </ClientEvents>
    <Levels>
        <ComponentArt:GridLevel DataKeyField="LogId" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings,  EntryDate  %>"
                    DataField="ModifiedDate" IsSearchable="true" Align="Left" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="DateHoverTemplate" />
                <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings,  Amount  %>"
                    DataField="Amount" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AmountHoverTemplate"
                    FormatString="c" />
                <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings,  CapturedAmount  %>"
                    DataField="CapturedAmount" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="CapturedAmountHoverTemplate"
                    FormatString="c" />
                <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings,  LogMessage  %>"
                    DataField="LogMessage" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="LogMessageHoverTemplate" />
                <ComponentArt:GridColumn Width="139" HeadingText="<%$ Resources:GUIStrings,  CurrentStatus  %>"
                    DataField="CurrentStatus" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="CurrentStatusHoverTemplate" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings,  PreviousStatus  %>"
                    DataField="PreviousStatus" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="PreviousStatusHoverTemplate" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings,  ModifiedBy  %>"
                    DataField="ModifiedBy" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="ModifiedByHoverTemplate"
                    Visible="false" />
                <ComponentArt:GridColumn DataField="LogId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="CustomerId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="ModifiedBy" Visible="false" IsSearchable="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="DateHoverTemplate">
            <span title="## DataItem.GetMember('ModifiedDate').get_text() ##">## DataItem.GetMember('ModifiedDate').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ModifiedDate').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="AmountHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('Amount').get_text()) ##">## DataItem.GetMember('Amount').get_text()
                == "" ? "&nbsp;" :DataItem.GetMember('Amount').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CapturedAmountHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('CapturedAmount').get_text()) ##">##
                DataItem.GetMember('CapturedAmount').get_text() == "" ? "&nbsp;" :DataItem.GetMember('CapturedAmount').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="LogMessageHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('LogMessage').get_text()) ##">## DataItem.GetMember('LogMessage').get_text()
                == "" ? "&nbsp;" :DataItem.GetMember('LogMessage').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CurrentStatusHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('CurrentStatus').get_text()) ##">##
                DataItem.GetMember('CurrentStatus').get_text() == "" ? "&nbsp;" :DataItem.GetMember('CurrentStatus').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PreviousStatusHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('PreviousStatus').get_text()) ##">##
                DataItem.GetMember('PreviousStatus').get_text() == "" ? "&nbsp;" :DataItem.GetMember('PreviousStatus').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('Customer').get_text()) ##">## DataItem.GetMember('Customer').get_text()
                == "" ? "&nbsp;" :DataItem.GetMember('Customer').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ModifiedByHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('ModifiedBy').get_text()) ##">## DataItem.GetMember('ModifiedBy').get_text()
                == "" ? "&nbsp;" :DataItem.GetMember('ModifiedBy').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNotesSliderTemplate">
            <div class="SliderPopup">
                <p>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings,  DataNotLoaded  %>" />
                </p>
                <p class="paging">
                    <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNotes.PageCount)
                        ## ## stringformat(Item0Of1, DataItem.PageIndex + 1, grdNotes.RecordCount) ##
                    </span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNotesSliderTemplateCached">
            <div class="SliderPopup">
                <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##
                </p>
                <p class="paging">
                    <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNotes.PageCount)
                        ## ## stringformat(Item0Of1, DataItem.PageIndex + 1, grdNotes.RecordCount) ##
                    </span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNotesPaginationTemplate">
            ## stringformat(Page0Of1Items, currentPageIndex(grdNotes), pageCount(grdNotes),
            grdNotes.RecordCount) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuNote" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem ID="cmSelect" Text="<%$ Resources:GUIStrings,  EditOrder  %>">
        </ComponentArt:MenuItem>
    </Items>
    <ClientEvents>
        <ItemSelect EventHandler="mnuNote_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
