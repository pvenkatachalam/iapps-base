﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateRange.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.General.DateRange" %>
<script type="text/javascript">
function CalendarFrom_OnChange_<%=this.ClientID %>(sender, eventArgs)
{
    var selectedDate = <%=CalendarFrom.ClientID%>.getSelectedDate();
    var date = iAppsCurrentLocalDate;
    if(selectedDate > date)
    {
        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DateShouldNotExceedTodaysDate %>' />");
        selectedDate = date;       
    }
    else
    {
        document.getElementById("<%=txtFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(selectedDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
        document.getElementById("<%=hdnFromDate.ClientID%>").value = <%=CalendarFrom.ClientID%>.formatDate(selectedDate, calendarDateFormat);  
    }
}
function CalendarTo_OnChange_<%=this.ClientID %>(sender, eventArgs)
{
    var selectedToDate = <%=CalendarTo.ClientID%>.getSelectedDate();
    var selectedFromDate = <%=CalendarFrom.ClientID%>.getSelectedDate();

    if(selectedToDate < selectedFromDate)
    {
        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ToDateShouldNotBeLessThanFromDate %>' />");
    }
    else
    {
        document.getElementById("<%=txtToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(selectedToDate, shortCultureDateFormat);  //shortCultureDateFormat: being rendered from MainMaster.cs
        document.getElementById("<%=hdnToDate.ClientID%>").value = <%=CalendarTo.ClientID%>.formatDate(selectedToDate, calendarDateFormat);  
    }
}
function popUpFromCalendar_<%=this.ClientID %>(textBoxId, hiddenFieldId)
{
//    if(document.getElementById("<%=txtFromDate.ClientID%>").disabled == false)
//    {
        var thisDate = iAppsCurrentLocalDate;
        var textBoxObject = document.getElementById(textBoxId);
        var hiddenFieldObject = document.getElementById(hiddenFieldId);
        if(Trim(textBoxObject.value) == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FromDate %>' />" || Trim(textBoxObject.value) == "")
        {
            textBoxObject.value = "";
        }
        if(textBoxObject.value != "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FromDate %>' />" && Trim(textBoxObject.value) != "" )
        {

            ValidatorEnable(document.getElementById("<%= valFromDate.ClientID %>"), true);
             if(document.getElementById("<%= valFromDate.ClientID %>").isvalid)
            {
                thisDate = new Date(hiddenFieldObject.value);  
                <%=CalendarFrom.ClientID%>.setSelectedDate(thisDate);  
            }
        }
        else
        {
           //<%=CalendarFrom.ClientID%>.setSelectedDate(thisDate);
        }
        if(!(<%=CalendarFrom.ClientID%>.get_popUpShowing())); 
            <%=CalendarFrom.ClientID%>.show();
//    }
}
function popUpToCalendar_<%=this.ClientID %>(textBoxId, hiddenFieldId)
{
//    if(document.getElementById("<%=txtToDate.ClientID%>").disabled == false)
//    {
        var thisDate = iAppsCurrentLocalDate;
        var textBoxObject = document.getElementById(textBoxId);
        var hiddenFieldObject = document.getElementById(hiddenFieldId);
        if(Trim(textBoxObject.value) == "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ToDate %>' />" || Trim(textBoxObject.value) == "")
        {
            textBoxObject.value = "";
        }
        if(textBoxObject.value != "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ToDate %>' />"  && Trim(textBoxObject.value) != "")
        {

            ValidatorEnable(document.getElementById("<%= valToDate.ClientID %>"), true);
             if(document.getElementById("<%= valToDate.ClientID %>").isvalid)
            {
                thisDate = new Date(hiddenFieldObject.value);    
                <%=CalendarTo.ClientID%>.setSelectedDate(thisDate);
            }
        }
        else
        {
            //<%=CalendarTo.ClientID%>.setSelectedDate(thisDate);
        }
        if(!(<%=CalendarTo.ClientID%>.get_popUpShowing())); 
            <%=CalendarTo.ClientID%>.show();
//    }
}

function setPosition(destObj, srcObj) {
    var objLeft   = srcObj.offsetLeft;
    var objTop    = srcObj.offsetTop;
    var objParent = srcObj.offsetParent;
    while( objParent.tagName.toUpperCase() != "BODY" && objParent.offsetParent != null ) {
        objLeft  += objParent.offsetLeft;
        objTop   += objParent.offsetTop;
        objParent = objParent.offsetParent;
    }
    destObj.style.top = (objTop + 22) + "px";
    destObj.style.left = (objLeft - 180) + "px";
}

function toggleCustomRange(dropdown) {
    var spanId = dropdown.id.replace('ddlPredefindedRange','spanCustomDateRange');
    if(dropdown.value == 'Custom') {
        $('#' + spanId).show();
    }
    else {
        $('#' + spanId).hide();
    }

}

function CloseCustomRange(spanCustomDateRange) {
    $('#' + spanCustomDateRange).hide();
}

</script>
<div class="date-range">
    <asp:DropDownList ID="ddlPredefindedRange" runat="server" Width="120" onchange="javascript:toggleCustomRange(this);">
        <asp:ListItem Text="<%$ Resources:GUIStrings, CurrentDay %>" Value="CurrentDay"
            Selected="True"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, CurrentWeek %>" Value="CurrentWeek"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, MonthToDate %>" Value="MonthToDate"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, QuarterToDate %>" Value="QuarterToDate"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, YearToDate %>" Value="YearToDate"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last30Days %>" Value="Last30days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last60Days %>" Value="Last60days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last180Days %>" Value="Last180days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, Last365Days %>" Value="Last365days"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:GUIStrings, CustomDateRange %>" Value="Custom"></asp:ListItem>
    </asp:DropDownList>
    <div class="date-range-popup" id="spanCustomDateRange" runat="server">
        <div class="form-row">
            <div class="form-value" style="text-align: right; width: 100%;">
                <a href="" runat="server" id="btnCloseCustomRange"><%= GUIStrings.Close %></a>
            </div>
        </div>
        <div class="form-row">
            <div class="form-value calendar-value">
                <span>
                    <asp:TextBox ID="txtFromDate" runat="server" Text="<%$ Resources:GUIStrings, FromDate %>"
                        CssClass="textBoxes" Width="184" ClientIDMode="Static"></asp:TextBox>
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, FromDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectFromDate %>" ID="imgFromDate" runat="server"
                        CssClass="calenderButton" />
                </span>
                <asp:HiddenField ID="hdnFromDate" runat="server" />
                <asp:CompareValidator CultureInvariantValues="true" ID="valFromDate" runat="server"
                    Type="Date" ControlToValidate="txtFromDate" Operator="DataTypeCheck" ValidationGroup="DateRange"
                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDateForFromDate %>"
                    Text="*" Style="float: left;"></asp:CompareValidator>
                <componentart:calendar runat="server" id="CalendarFrom" skinid="Default">
                </componentart:calendar>
            </div>
        </div>
        <div class="form-row">
            <div class="form-value calendar-value">
                <span>
                    <asp:TextBox ID="txtToDate" runat="server" Text="<%$ Resources:GUIStrings, ToDate %>"
                        CssClass="textBoxes" Width="184" ClientIDMode="Static"></asp:TextBox>
                    <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, ToDate %>"
                        ToolTip="<%$ Resources:GUIStrings, SelectToDate %>" ID="imgToDate" runat="server"
                        CssClass="calenderButton" />
                </span>
                <asp:HiddenField ID="hdnToDate" runat="server" />
                <asp:CompareValidator CultureInvariantValues="true" ID="valToDate" runat="server"
                    Type="Date" ControlToValidate="txtToDate" Operator="DataTypeCheck" ValidationGroup="DateRange"
                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDateForToDate %>"
                    Text="*" Style="float: left;"></asp:CompareValidator>
                <componentart:calendar runat="server" id="CalendarTo" skinid="Default">
                </componentart:calendar>
            </div>
        </div>
    </div>
</div>
<asp:ValidationSummary runat="server" ShowMessageBox="true" ShowSummary="false" ID="valSummary"
    ValidationGroup="DateRange" />
