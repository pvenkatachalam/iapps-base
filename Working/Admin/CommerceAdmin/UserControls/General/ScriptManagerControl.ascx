﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ScriptManagerControl.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ScriptManagerControl" %>

<asp:ScriptManager ID="sManager" runat="server" ScriptMode="Release" EnableCdn="true" />
<script language="javascript" type="text/javascript" defer="defer">

    function init() {
        Sys.Net.WebRequestManager.add_invokingRequest(beginRequest);
        Sys.Net.WebRequestManager.add_completedRequest(endRequest);
    }
    window.onload = function () {
        init();
    }

    function beginRequest(sender, args) {

        ShowLoadingModalBox();
    }

    function endRequest(sender, args) {

        try {
            CloseLoadingModalBox();
        } catch (exp) {

        }
    }
                      
</script>
