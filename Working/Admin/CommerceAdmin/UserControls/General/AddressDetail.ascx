﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressDetail.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.AddressDetail" %>
<script type="text/javascript">
    $(function () {
        $(".address-details").on("change", ".address-country", function () {
            pnlState.callback();
        });
    });
</script>
<div class="address-details">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, AddressLine1 %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtAddress1" runat="server" />
            <iAppsControls:iAppsCustomValidator ID="cv_txtAddress1" ControlToValidate="txtAddress1"
                FieldName="<%$ Resources:GUIStrings, AddressLine1 %>" Required="true"
                runat="server" ValidateType="PlainText" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, AddressLine2 %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtAddress2" runat="server" />
            <iAppsControls:iAppsCustomValidator ID="cv_txtAddress2" ControlToValidate="txtAddress2"
                FieldName="<%$ Resources:GUIStrings, AddressLine2 %>" runat="server" ValidateType="PlainText" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AddressLine3 %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtAddress3" runat="server" />
            <iAppsControls:iAppsCustomValidator ID="cv_txtAddress3" ControlToValidate="txtAddress3"
                FieldName="<%$ Resources:GUIStrings, AddressLine3 %>" runat="server" ValidateType="PlainText" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, City %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtCity" runat="server" />
            <iAppsControls:iAppsCustomValidator ID="cv_txtCity" ControlToValidate="txtCity"
                FieldName="<%$ Resources:GUIStrings, City %>" Required="true"
                runat="server" ValidateType="PlainText" />
        </div>
    </div>
    <iAppsControls:CallbackPanel ID="pnlState" runat="server">
        <ContentTemplate>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Country %>" />
                </label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="address-country" />
                    <iAppsControls:iAppsCustomValidator ID="cv_ddlCountry" ControlToValidate="ddlCountry"
                        FieldName="<%$ Resources:GUIStrings, Country %>" Required="true" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, State %>" />
                </label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlState" runat="server" CssClass="address-state" />
                    <iAppsControls:iAppsCustomValidator ID="cv_ddlState" ControlToValidate="ddlState"
                        FieldName="<%$ Resources:GUIStrings, State %>" Required="true" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ZipCode %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtZipCode" runat="server" />
                    <iAppsControls:iAppsCustomValidator ID="cv_txtZipCode" ControlToValidate="txtZipCode"
                        FieldName="<%$ Resources:GUIStrings, ZipCode %>" Required="true" runat="server" />
                </div>
            </div>
            
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, Phone %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtPhone" runat="server" />
            <iAppsControls:iAppsCustomValidator ID="cv_txtPhone" ControlToValidate="txtPhone"
                FieldName="<%$ Resources:GUIStrings, Phone %>" runat="server" />
        </div>
    </div>
</div>
