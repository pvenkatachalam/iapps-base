﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Notes.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.General.Notes" %>
<script type="text/javascript">
    function ValidateNotes() {
        var notes = Trim(document.getElementById("<%=txtNotes.ClientID%>").value);

        if (notes == '' || notes == "<%= GUIStrings.AddANote %>") {
            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NotesCanNotBeEmpty %>' />");
            return false;
        }
        else {
            if (notes.match(/[\<\>]/)) {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheNotes %>' />");
                return false;
            }
            else {
                return true;
            }
        }
    }

    function GetHoverText(text) {
        return text.replace(/<br\/>/g, "\n");
    }
    var gridItem;
    function mnuNote_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var selectedMenuId = menuItem.get_id();
        if (selectedMenuId == 'cmSelect') {
            var id = gridItem.getMember(4).get_value();
            var objectType = gridItem.getMember(5).get_value();
            if (objectType == 'Order') {
                window.location = '../../StoreManager/Orders/NewOrders.aspx?OrderId=' + id;
            }
        }
    }
    function grdNotes_onContextMenu(sender, eventArgs) {
        gridItem = eventArgs.get_item();
        var objectType = gridItem.getMember(5).get_value();
        if (objectType == 'Order') {
            mnuNote.showContextMenuAtEvent(e);
        }
    }
</script>
<div class="button-row">
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, SaveNote %>" CssClass="primarybutton"
            OnClientClick="return ValidateNotes();" OnClick="btnSave_Click" />
</div>
<div class="grid-utility clear-fix">
    <div class="columns">
        <asp:TextBox ID="txtNotes" runat="server" CssClass="textBoxes" Width="500" TextMode="MultiLine"
            ToolTip="<%$ Resources:GUIStrings, AddANote %>" Rows="3"></asp:TextBox>
    </div>
</div>
<ComponentArt:Grid ID="grdNotes" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
    PagerInfoClientTemplateId="CustomerNotesPaginationTemplate" AllowEditing="true"
    AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="CustomerNotesSliderTemplateCached"
    SliderPopupClientTemplateId="CustomerNotesSliderTemplate" AutoCallBackOnUpdate="false"
    CallbackCacheLookAhead="10" PageSize="10" AllowPaging="True" CallbackCachingEnabled="true"
    AllowMultipleSelect="false" EditOnClickSelectedItem="false" Sort="CreatedDate Desc">
    <ClientEvents>
        <ContextMenu EventHandler="grdNotes_onContextMenu" />
    </ClientEvents>
    <Levels>
        <ComponentArt:GridLevel DataKeyField="LogId" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, EntryDate %>"
                    DataField="CreatedDate" IsSearchable="true" Align="Left" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="DateHoverTemplate" />
                <ComponentArt:GridColumn Width="600" HeadingText="<%$ Resources:GUIStrings, EntryText %>"
                    DataField="ChangedItems" IsSearchable="false" Align="Left" TextWrap="true" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="TextHoverTemplate" />
                <ComponentArt:GridColumn Width="180" HeadingText="<%$ Resources:GUIStrings, ModifiedBy %>"
                    DataField="ModifiedBy" IsSearchable="false" Align="Left" AllowReordering="false"
                    FixedWidth="true" />
                <ComponentArt:GridColumn DataField="LogId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="ObjectType" Visible="false" IsSearchable="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="DateHoverTemplate">
            <span title="## DataItem.GetMember('CreatedDate').get_text() ##">## DataItem.GetMember('CreatedDate').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('CreatedDate').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TextHoverTemplate">
            <span title="## GetHoverText( DataItem.GetMember('ChangedItems').get_text()) ##">##
                DataItem.GetMember('ChangedItems').get_text() == "" ? "&nbsp;" :DataItem.GetMember('ChangedItems').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNotesSliderTemplate">
            <div class="SliderPopup">
                <p>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DataNotLoaded %>" /></p>
                <p class="paging">
                    ## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNotes.PageCount) ## ## stringformat(Item0Of1,
                    DataItem.PageIndex + 1, grdNotes.RecordCount) ##
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNotesSliderTemplateCached">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##</p>
                <p class="paging">
                    ## stringformat(Page0Of1, DataItem.PageIndex + 1, grdNotes.PageCount) ## ## stringformat(Item0Of1,
                    DataItem.PageIndex + 1, grdNotes.RecordCount) ##
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNotesPaginationTemplate">
            ## stringformat(Page0Of1Items, currentPageIndex(grdNotes), pageCount(grdNotes),
            grdNotes.RecordCount) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuNote" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem ID="cmSelect" Text="<%$ Resources:GUIStrings, EditOrder %>" Look-LeftIconUrl="cm-icon-edit.png"  Look-LeftIconWidth="17" Look-LeftIconHeight="14" />
    </Items>
    <ClientEvents>
        <ItemSelect EventHandler="mnuNote_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
