﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TodaysSummary.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TodaysSummary" %>
<p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings,  TotalSales  %>" /></p>
<p class="green-text"><asp:Literal ID="litTotalSales" runat="server"></asp:Literal></p>
<hr />
<p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings,  NumTransactions  %>" /></p>
<p class="green-text"><asp:Literal ID="litTransactions" runat="server"></asp:Literal></p>
<hr />
<p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings,  AvgTxnSize  %>" /></p>
<p class="green-text"><asp:Literal ID="litAvgTxnSize" runat="server"></asp:Literal></p>
<hr />
<p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings,  NumAbandonedCarts  %>" /></p>
<p class="green-text"><asp:Literal ID="litAbandonedCarts" runat="server"></asp:Literal></p>