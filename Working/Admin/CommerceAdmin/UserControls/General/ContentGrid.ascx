<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Controls.UserControlContentGrid" %>
<%@ Register TagPrefix="ContextForm" TagName="ContentDefinition" Src="~/UserControls/ContextMenu/TypeOfContent.ascx" %>
<!-- Script Related to Content Grid -->
<script type="text/javascript">
    /* global variable to set the grid operation wheen user inserts / updates or deletes operations */
    var contentGridOperation;
    var item;
    var pagePopupWindow;
    var isContentGridCallback = false
    // function is called by the grid to set the operation value from the global variable into the DataItem 
    //represented by the current Grid Item

    function IsDefaultContentId(thisRowContentId) {
        if ((thisRowContentId != null && defaultContentId != null) &&
        (defaultContentId.toLowerCase() == thisRowContentId.toLowerCase())) {
            return true;
        }
        else {
            return false;
        }
    }

    function ValidateContentGrid() {
        var ObjTitleClientId = document.getElementById(txtTitleClientID);
        var ObjDescriptionClientId = document.getElementById(txtDescriptionClientID);
        ObjDescriptionClientId.value = ObjDescriptionClientId.value.replace("\"", "");
        ObjDescriptionClientId.value = ObjDescriptionClientId.value.replace(new RegExp("\n", "g"), " ");

        if (ObjTitleClientId.value == 'null' || Trim(ObjTitleClientId.value) == '') {
            alert('Please enter a title.'); //it is there by using validator control
            return false;
        }

        var regularEval = new RegExp("[\<\>]", "i");
        var blnEval1 = regularEval.test(ObjTitleClientId.value);
        if (blnEval1) {
            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SpecialCharactersAreNotAllowed %>" />');
            // to move focus on particular object
            document.getElementById(txtTitleClientID).focus();
            return false;
        }
        var blnEval = regularEval.test(ObjDescriptionClientId.value);
        if (blnEval) {
            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SpecialCharactersAreNotAllowed %>" />');
            // to move focus on particular object
            document.getElementById(txtDescriptionClientID).focus();
            return false;
        }

        return true;
    }

    function setContentTitleValue(control, DataField) {
        var txtTitle = document.getElementById(control);
        var txtTitlevalue = item.GetMember(DataField).Value;
        if (txtTitlevalue == null || txtTitlevalue == 'null')
            txtTitle.value = '';
        else
            txtTitle.value = txtTitlevalue;
    }
    function getContentTitleValue(control) {
        var txtTitle = document.getElementById(control);
        return [txtTitle.value, txtTitle.value];
    }

    function setContentDescriptionValue(control, DataField) {
        var txtDesc = document.getElementById(control);
        var txtDescvalue = item.GetMember(DataField).Value;
        if (txtDescvalue == null || txtDescvalue == 'null')
            txtDesc.value = '';
        else
            txtDesc.value = txtDescvalue;
    }
    function getContentDescriptionValue(control) {
        var txtDesc = document.getElementById(control);
        return [txtDesc.value, txtDesc.value];
    }

    function getOperationValue() {
        return [contentGridOperation, contentGridOperation];
    }

    function gridContent_GridLoad(sender, eventArgs) {
    }

    function gridContent_onCallbackComplete(sender, eventArgs) {
        isContentGridCallback = false;
        if (contentGridOperation == "Saved" && selectedMenu == 'AddContent') {
            if (gridContent.Data.length > 0) {
                gridContent.select(gridContent.get_table().getRow(gridContent.Data.length - 1), false);
                var selectedItemArray = gridContent.getSelectedItems();
                if (selectedItemArray.length > 0) {
                    var contentId = selectedItemArray[0].getMember(contentIdColumn).get_text();
                    var returnString = SetAsDefaultContentInNav(contentId, curSelectedNavId);
                    if (returnString == "True") {
                        SetNavDefaultContentId(contentId);
                    }
                    else {
                        alert(returnString);
                    }
                }
            }
        }

        //display only one record
        //        if (gridContent.Data.length > 1) {
        //            gridContent.filter("Id='" + defaultContentId + "'");
        //        }        
    }

    function grdContent_onContextMenu(sender, eventArgs) {
        if ((msContentGridContextMenu != null) &&
            (associatedContentFolderId != emptyGuid && associatedContentFolderId != 'undefined' && associatedContentFolderId != undefined)
            ) {
            var menuItems = msContentGridContextMenu.get_items();
            var isAnyMenuVisible = false;
            if (gridContent.Data.length == 1 && (eventArgs.get_item().getMember('Id').get_value() == null || eventArgs.get_item().getMember('Id').get_value() == '')) { // incase only one blank row
                var i = 0;
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'AddContent') {
                        isAnyMenuVisible = true;
                        menuItems.getItem(i).set_visible(true);
                    }
                    else {
                        menuItems.getItem(i).set_visible(false);
                    }
                }
            }
            else {
                var i = 0;
                isAnyMenuVisible = true;
                for (i = 0; i < menuItems.get_length(); i++) {
                    if (menuItems.getItem(i).get_id() == 'AddContent') {
                        menuItems.getItem(i).set_visible(false);
                    }
                    else {
                        menuItems.getItem(i).set_visible(true);
                    }
                }
            }

            if (contentGridOperation != "Update" && contentGridOperation != 'Insert') {
                HtmlValue = '';
                item = eventArgs.get_item();
                gridContent.select(item);
                if (isAnyMenuVisible) {
                    var evt = eventArgs.get_event();
                    msContentGridContextMenu.showContextMenuAtEvent(evt);
                    msContentGridContextMenu.set_contextData(item);
                }
            }
        }
    }

    function gridContent_BeforeUpdate(fromPopup) {
        setTimeout(function () {
            if (CheckSession()) {
                var contentId = '00000000-0000-0000-0000-000000000000';
                //reset hidden object
                if (ValidateContentGrid() == true) {
                    SaveContent();
                }
                else {
                    isContentUpdated = 0;
                    SaveContent();
                }
            }
        }, 500);
    }

    function SaveContent() {
        if (!isContentGridCallback) {
            if (CheckSession()) {
                if (contentType != 'Content' && TemplateId != null && TemplateId != '') {
                    item.SetValue(templateIdColumn, TemplateId, true);
                }
                gridContent.editComplete();
                isContentGridCallback = true;
                contentGridOperation = 'Saved';
            }
        }
    }

    function ContentCancelEditedValue() {
        if (contentGridOperation != 'Canceled') {
            HtmlValue = HtmlOldValue; // if the user update and press cancel it has to move to previous value
            if (gridContent.Data.length == 1 && item != null && (item.getMember('Id').get_value() == null || item.getMember('Id').get_value() == '')) {
                item.SetValue(objectTypeNameColumn, ''); // object type column
                gridContent.get_table().ClearData();
                gridContent.editCancel();
                item = null;
                contentGridOperation = 'Canceled';
                if (gridContent.Data.length == 0) {
                    gridContent.get_table().addEmptyRow(0);
                }
            }
            else {
                gridContent_Cancel();
            }
        }
        isContentUpdated = 0;
    }

    function gridContent_BeforeInsert(sender, eventArgs) {
        if (!isContentGridCallback) {
            if (CheckSession()) {
                var returnValue = ValidatorFocus();
                if (returnValue == true) {
                    gridContent.editComplete();
                    //isContentGridCallback = true;
                }
                else {
                    eventArgs.set_cancel(true);
                }
            }
        }
    }
    function gridContent_Cancel() {
        if (contentGridOperation == 'Insert') {
            gridContent.editCancel();
            gridContent.deleteItem(item);
            item = null;
            contentGridOperation = 'Canceled';
            return false;
        }
        else {
            gridContent.editCancel();
            contentGridOperation = 'Canceled';
        }
    }
</script>
<script type="text/javascript">
    var contentType;
    var TemplateId;
    var orderNoColumn = 0;
    var objectTypeNameColumn = 3;
    var actionColumn = 8;
    var indexTermsColumn = 9;
    var contentIdColumn = 10;
    var xmlColumn = 11;
    var templateIdColumn = 13;
    var directoryColumn = 14;
    var pageIdColumn = 15;
    var workFlowLinkColumn = 17;
    var frontEndHtmlColumn = 18;
    var originalTitleIdColumn = 19;
    var titleColumn = 1;

    // START added by AV for performance
    var XmlFormXmlStringColumn = 22;
    var XmlFormXsltStringColumn = 23;
    var isContentUpdatedColumn = 24;

    // END added by AV for performance

    var directoryId; // id of the selected directory
    var contentDefinitionPopup;
    var HtmlValue;      //holding xmlstring or test value here
    var HtmlOldValue;

    var selectedNode;
    var termswin;
    var selectedMenu;
    var viewPopupWindow; //handler of view page using this content window
    var GLObjectTypeId = 7;
    var isContentUpdated = 0;

    function contentContextMenuClickHandler(selectedMenuId) {
        if (!isContentGridCallback) {
            if (CheckSession()) {
                selectedMenu = selectedMenuId;

                if (selectedMenuId == "AddContent") {
                    if (gridContent.Data.length == 1 && (item.getMember('Id').get_value() == null || item.getMember('Id').get_value() == '')) {
                        gridContent.edit(item);
                        item.SetValue(orderNoColumn, 1, true);
                    }
                    else {
                        gridContent.beginUpdate();
                        gridContent.get_table().addEmptyRow(0);
                        gridContent.editComplete();
                        //isContentGridCallback = true;
                        item = gridContent.get_table().getRow(0);
                        gridContent.edit(item);
                        item.SetValue(orderNoColumn, gridContent.get_table().getRowCount(), true);
                    }
                    item.SetValue(objectTypeNameColumn, contentType, true); // Object Type Name
                    item.SetValue(directoryColumn, directoryId, true);
                    contentGridOperation = "Insert";
                }

                if (selectedMenuId == "EditContent") {
                    var contentId = item.getMember(contentIdColumn).get_text();
                    // START added by AV for performance reasons - to calculate values for FrontEndHtml Columns using callback
                    var contentXmlString = item.getMember(xmlColumn).get_text();
                    var xmlFormXmlString = item.getMember(XmlFormXmlStringColumn).get_text();
                    var xmlFormXsltString = item.getMember(XmlFormXsltStringColumn).get_text();

                    // callback method:
                    var handledFrontEndHTMLFromServer = HandleXMLFormPublicDecode(contentXmlString, xmlFormXmlString, xmlFormXsltString);
                    item.SetValue(frontEndHtmlColumn, handledFrontEndHTMLFromServer, true);

                    // END  added by AV for performance reasons - to calculate values for Text & FrontEndHtml Columns using callback
                    contentGridOperation = "Update";
                    item.SetValue(directoryColumn, directoryId, true);
                    gridContent.edit(item);
                }
                if (selectedMenuId == "SetAsDefault") {
                    var contentId = item.getMember(contentIdColumn).get_text();
                    var returnString = SetAsDefaultContentInNav(contentId, curSelectedNavId);
                    if (returnString == "True") {
                        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, SuccessfullyUpdated %>' />");
                    }
                    else {
                        alert(returnString);
                    }
                }
                if (selectedMenuId == "DeleteContent") {
                    var contentId = item.getMember(contentIdColumn).get_text();
                    if (window.confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DoYouWantToDeleteThisContent %>' />")) {
                        var returnString = DeleteContentFromNavNode(contentId, curSelectedNavId);
                        if (returnString != "True") {
                            alert(returnString);
                        }
                        else {
                            if (associatedContentFolderId != emptyGuid && associatedContentFolderId != 'undefined' && associatedContentFolderId != undefined) {
                                LoadContent(associatedContentFolderId);
                            }
                        }
                    }
                }
            }
        }
    }

    //call this function to display the contentDefinitionPopup or Tereric Editor
    function LoadContentDefinitionTemplate() {
        if (item == null) {
            return;
        }
        var contentId = '00000000-0000-0000-0000-000000000000';
        if (HtmlValue == '') {
            HtmlValue = item.getMember(xmlColumn).get_text();
        }
        if (item.getMember(objectTypeNameColumn).get_text() != "[object Object]" && item.getMember(objectTypeNameColumn).get_text() != null && item.getMember(objectTypeNameColumn).get_text() != '') // content type column
        {
            contentType = item.getMember(objectTypeNameColumn).get_text();
        }
        if (item.getMember(contentIdColumn).get_text() != "[object Object]" && item.getMember(contentIdColumn).get_text() != null && item.getMember(contentIdColumn).get_text() != '') // id column
        {
            contentId = item.getMember(contentIdColumn).get_text();
        }
        if (item.getMember(templateIdColumn).get_text() != "[object Object]" && item.getMember(templateIdColumn).get_text() != null && item.getMember(templateIdColumn).get_text() != '') // Template id column
        {
            TemplateId = item.getMember(templateIdColumn).get_text();
        }
        CloseEditor = 'false';
        contentDefinitionPopup = null;

        if (contentType == 'Content') {
            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, FreeFormCanNotBeEditHere %>" />');
        }
        else {
            if (TemplateId != null && TemplateId != '' && TemplateId != '00000000-0000-0000-0000-000000000000') {
                OpeniAppsAdminPopup("ContentDefinitionPopup", "ShowFooter=true&TemplateId=" + TemplateId + "&Id=" + contentId);
            }
            else {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ErrorTemplateLoadActionedFailed %>' />");
            }
        }

    }

    function SetValue(control, DataField, DataItem) {
        var value = item.GetMember(DataField).Value;
        var txtControl = document.getElementById(control);
        txtControl.value = value;
    }

    //these methods call from popups
    //calling from ContentDefinitionPopup.aspx
    function SetXML(xml, convertedHtml) {
        HtmlValue = xml;
        HtmlOldValue = item.getMember(xmlColumn).get_text();
        item.SetValue(xmlColumn, xml, true);

        item.SetValue(frontEndHtmlColumn, convertedHtml, true);
        isContentUpdated = 1;
        item.SetValue(isContentUpdatedColumn, 1, true);
    }
    //calling from TypeOfContent
    function SetContentType(contentDetailsType, selectedTemplateId) {
        contentType = contentDetailsType;
        TemplateId = selectedTemplateId;
        item.SetValue(templateIdColumn, selectedTemplateId, true);
    }

    function TextBind(title, text, type) {
        var bindtext = "";
        var textSeparator = browser == "ie" ? "\n" : ": ";
        bindtext = title;
        if (type == "Content") {
            bindtext = text != "" ? bindtext + textSeparator + ChangeSpecialCharacters(text.substring(0, 160)) : bindtext;
        }
        return bindtext;
    }

    function LoadContent(selectedDirectoryId) {
        var contentGrid = window["gridContent"];
        directoryId = selectedDirectoryId;
        var actionColumnValue = "loadgrid" + ";DisableDisplayOrderSort";
        if (contentGrid != null) {
            var rowitem = contentGrid.get_table().addEmptyRow(0);
            contentGrid.edit(rowitem);
            rowitem.SetValue(directoryColumn, directoryId); // directory column
            rowitem.SetValue(actionColumn, actionColumnValue); // action column
            contentGridOperation = 'Load';
            contentGrid.editComplete();
            if (defaultContentId == null) {
                defaultContentId = emptyGuid;
            }
            if (defaultContentId != null) {
                contentGrid.set_callbackParameter("'" + defaultContentId + "'");
            }
            contentGrid.callback();
            isContentGridCallback = false;
        }
    }
</script>
    <input type="hidden" id="hdnRoleId" runat="server" enableviewstate="true" />
    <ComponentArt:Grid ID="gridContent" CallbackCachingEnabled="true" SkinID="default"
        runat="server" Width="100%" PageSize="1" EnableViewState="True" RunningMode="Callback"
        AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AllowPaging="True"
        CallbackCacheLookAhead="10" ItemDraggingEnabled="false" AutoAdjustPageSize="true"
        AllowMultipleSelect="false" ShowFooter="false">
        <ClientEvents>
            <ContextMenu EventHandler="grdContent_onContextMenu" />
            <ItemBeforeInsert EventHandler="gridContent_BeforeInsert" />
            <Load EventHandler="gridContent_GridLoad" />
            <CallbackComplete EventHandler="gridContent_onCallbackComplete" />
        </ClientEvents>
        <Levels>
           <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
                <ConditionalFormats>
                    <ComponentArt:GridConditionalFormat ClientFilter="IsDefaultContentId(DataItem.GetMember('Id').Value)"
                        RowCssClass="ArchiveDataRow" SelectedRowCssClass="SelectedRow" />
                </ConditionalFormats>
                <Columns>
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" DataField="OrderNo"
                        HeadingText="<%$ Resources:GUIStrings, Display %>" AllowEditing="false" Align="Right"
                        FixedWidth="true" EditControlType="Custom" Width="10" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" EditControlType="Custom"
                        EditCellServerTemplateId="svtTitle" HeadingText="<%$ Resources:GUIStrings, Title %>"
                        DataField="Title" FixedWidth="true" Align="left" Width="250" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" DataField="Description"
                        Align="left" EditControlType="Custom" EditCellServerTemplateId="svtDescription" Visible="false"
                        Width="95" FixedWidth="true" HeadingText="<%$ Resources:GUIStrings, Description %>" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" AllowEditing="false"
                        HeadingText="<%$ Resources:GUIStrings, Type %>" DataField="ObjectTypeName"
                        Align="left" Width="90" FixedWidth="true" EditControlType="Custom" EditCellServerTemplateId="svtObjectTypeName" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" AllowEditing="false"
                        HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="Status" Align="left"
                        Width="60" FixedWidth="true" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" AllowEditing="false"
                        HeadingText="<%$ Resources:GUIStrings, Created %>" DataField="CreatedByFullName"
                        Align="left" Width="50" FixedWidth="true" Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" AllowEditing="false"
                        HeadingText="<%$ Resources:GUIStrings, LastEdited %>" DataField="ModifiedDate"
                        Align="left" Width="105" FixedWidth="true" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" AllowEditing="false"
                        HeadingText="<%$ Resources:GUIStrings, EditedBy %>" DataField="ModifiedByFullName"
                        Align="left" Width="135" FixedWidth="true" />
                    <ComponentArt:GridColumn AllowReordering="false" AllowSorting="False" IsSearchable="false"
                        HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left" EditControlType="Custom"
                        EditCellServerTemplateId="svtInfo" Width="50" FixedWidth="true"
                        DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell" />
                    <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" HeadingText="<%$ Resources:GUIStrings, IndexingTerms %>"
                        DataField="IndexingTerms" Visible="false" Align="left" Width="0" FixedWidth="true"
                        EditControlType="Custom" EditCellServerTemplateId="svtIndexTerms" />
                    <ComponentArt:GridColumn AllowReordering="false" HeadingText="<%$ Resources:GUIStrings, Id %>"
                        DataField="Id" IsSearchable="false" Align="left" Width="0" FixedWidth="true"
                        Visible="false" />
                    <ComponentArt:GridColumn AllowReordering="false" DataField="ContentDetails" IsSearchable="false"
                        Width="0" FixedWidth="true" Visible="false" EditControlType="Custom" EditCellServerTemplateId="svtContentDetails" />
                    <ComponentArt:GridColumn AllowReordering="false" HeadingText="<%$ Resources:GUIStrings, ObjectTypeId %>"
                        DataField="ObjectTypeId" IsSearchable="false" Align="left" Width="0" FixedWidth="true"
                        Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="TemplateId"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="DirectoryId"
                        Width="0" Visible="false" EditControlType="Custom" EditCellServerTemplateId="svtInvisible1" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="PageId"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="CreatedDate"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="WorkflowLink"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="FrontEndHtml"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="OrginalTitle"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="DirectoryId"
                        Width="0" Visible="false" EditControlType="Custom" EditCellServerTemplateId="svtInvisible1" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="Text"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="XmlFormXmlString"
                        Width="0" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="XmlFormXsltString"
                        Width="0" Visible="false" />
                     <ComponentArt:GridColumn FixedWidth="true" IsSearchable="false" DataField="IsContentUpdated"
                        Width="0" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate CausesValidation="True" ID="svtTitle">
                <Template>
                    <asp:TextBox CssClass="textBoxes" ID="txtTitle" runat="server" Width="80">
                    </asp:TextBox>&nbsp;<span class="required">*</span>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtDescription">
                <Template>
                    <asp:TextBox CssClass="mcsvDescription" ID="txtDescription" runat="server" TextMode="MultiLine">
                    </asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtObjectTypeName">
                <Template>
                    <asp:HiddenField ID="txtObjectTypeName" runat="server" />
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtContentDetails">
                <Template>
                    <asp:HiddenField ID="txtContentDetails" runat="server" />
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtIndexTerms">
                <Template>
                    <asp:TextBox CssClass="textBoxes" ID="txtIndexTerms" runat="server"> </asp:TextBox>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtInfo">
                <Template>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <input alt="<%$ Resources:GUIStrings, Edit %>" type="image" runat="server" title="<%$ Resources:GUIStrings, Edit %>"
                                    src="/iapps_images/cm-icon-edit.png" id="ImageButton2" onclick="LoadContentDefinitionTemplate();return false;"
                                    causesvalidation="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                    src="/iapps_images/cm-icon-add.png" id="ImageButton11" runat="server"
                                    causesvalidation="true" onclick="gridContent_BeforeUpdate(false);" />
                            </td>
                            <td>
                                <img runat="server" alt="<%$ Resources:GUIStrings, Cancel %>" causesvalidation="false"
                                    title="<%$ Resources:GUIStrings, Cancel %>" src="/iapps_images/cm-icon-delete.png"
                                    id="ImageButton4" onclick="ContentCancelEditedValue();" />
                            </td>
                        </tr>
                    </table>
                </Template>
            </ComponentArt:GridServerTemplate>
            <ComponentArt:GridServerTemplate ID="svtInvisible1">
                <Template>
                    <asp:HiddenField ID="txtDirectoryId" runat="server" />
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </ComponentArt:Grid>
    <ComponentArt:Menu ID="msContentGridContextMenu" SkinID="ContextMenu" Width="220"
        runat="server" ExpandOnClick="true">
        <ServerTemplates>
            <ComponentArt:NavigationCustomTemplate ID="addContentDefinition">
                <Template>
                    <ContextForm:ContentDefinition ID="cmAddContentDefinition" runat="server" IsLoadFreeForm="false" />
                </Template>
            </ComponentArt:NavigationCustomTemplate>
        </ServerTemplates>
    </ComponentArt:Menu>
