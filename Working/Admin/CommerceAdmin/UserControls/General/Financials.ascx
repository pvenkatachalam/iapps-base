﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Financials.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Financials" %>
<table border="0" cellpadding="5" cellspacing="0" width="100%" class="summary-table">
    <tr class="row">
        <td align="left"><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, WTD %>" /></td>
        <td align="right"><asp:Literal ID="litWTD" runat="server"></asp:Literal></td>
    </tr>
    <tr class="alternate-row">
        <td align="left"><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, MTD %>" /></td>
        <td align="right"><asp:Literal ID="litMTD" runat="server"></asp:Literal></td>
    </tr>
    <tr class="row">
        <td align="left"><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, QTD %>" /></td>
        <td align="right"><asp:Literal ID="litQTD" runat="server"></asp:Literal></td>
    </tr>
    <tr class="alternate-row">
        <td align="left"><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, YTD %>" /></td>
        <td align="right"><asp:Literal ID="litYTD" runat="server"></asp:Literal></td>
    </tr>
</table>