﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InternationalAddress.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.General.InternationalAddress" %>
<script type="text/javascript">
    function validateCity() {
        var errorMsg = '';
        var city = document.getElementById('<%=txtCity.ClientID %>').value;
        var stateId = document.getElementById('<%=ddlState.ClientID %>').value;
        var zipCode = document.getElementById('<%=txtZip.ClientID %>').value;
        var street1 = document.getElementById('<%=txtAddress1.ClientID %>').value;
        var street2 = document.getElementById('<%=txtAddress2.ClientID %>').value;
        var countryDDL = document.getElementById('<%=ddlCountry.ClientID %>');
        var country = countryDDL.options[countryDDL.selectedIndex].value;

        var isAddressVerified = IsFullAddressValid(street1, street2,city,stateId, country,zipCode);
        return isAddressVerified;
    }

    function ddlstate_onchange(obj) {      
        var countryDDL = document.getElementById('<%=ddlCountry.ClientID %>');            
        document.getElementById("<%=hfCountryValue.ClientID %>").value = countryDDL.options[countryDDL.selectedIndex].value;       
        document.getElementById("<%=hfState.ClientID %>").value = obj.value;
        $("[id$=hfIsAddressValidated]").val('0'); 
    }

    function ddlcounty_onchange(obj) {                          
        $("#<%=txtCounty.ClientID%>" ).val(obj.value);        
    }

    function ddlCountry_onchange(obj) {
        <%=clbkState.ClientID%>.callback(obj.value);
        $("[id$=hfIsAddressValidated]").val('0'); 
    }

    function ValidatePhone(src, args) {     
        args.IsValid = true;
        var phoneValue='';
        var phoneRE ="<asp:localize runat='server' text='<%$ Resources:GUIStrings, PhoneRegex  %>'/>";
        var countryDDL = document.getElementById('<%=ddlCountry.ClientID %>');
        if(countryDDL.options[countryDDL.selectedIndex].value == usId && document.getElementById('<%= mTxtPhone.ClientID %>_unmasked'))
        {              
            phoneValue = document.getElementById('<%= mTxtPhone.ClientID %>_unmasked').value;  
                 
            if(phoneValue != null && Trim(phoneValue)== ''){                   
                src.errormessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAPhoneNumber %>'/>";
                args.IsValid  = false;                    
            }
            else if (!phoneValue.match(/^\d{10}$/)){ //if(!phoneValue.match(/^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$/)){
                src.errormessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAValidPhoneNumber %>'/>";
                    args.IsValid  = false;                    
                }
        }
        else if (document.getElementById('<%= txtPhone.ClientID %>'))
        {                
            phoneValue = document.getElementById('<%= txtPhone.ClientID %>').value; 
            if(phoneValue != null && Trim(phoneValue)== ''){
                src.errormessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, PleaseEnterAPhoneNumber %>'/>";
                args.IsValid  = false;                    
            }
        }
           
}

function ValidateZip(src, args){
    args.IsValid = true;
    var zipRE ="<asp:localize runat='server' text='<%$ Resources:GUIStrings, ZipCodeRegex %>'/>";
    var countryDDL = document.getElementById('<%=ddlCountry.ClientID %>');
        
    if(countryDDL.options[countryDDL.selectedIndex].value == usId){            
        if(!args.Value.match(/^\d{5}([\-]\d{4})?$/)) args.IsValid  = false;           
    }
}
    var addressInfo; 
    function stateLoaded()
    {
        if(addressInfo)
        {
            $( "#<%=txtCity.ClientID%>" ).val(addressInfo.City);
            $( "#<%=ddlState.ClientID%>" ).val(addressInfo.StateId);            
        }
        generateCountyDropDown();
    }

function Changed( textControl ) {
    $("[id$=hfIsAddressValidated]").val('0'); 
}

</script>
<asp:Panel runat="server" ID="pnlFirstName" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FirstName1 %>" /><span class="req">*</span>
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" autocomplete="off"></asp:TextBox>
        <asp:RegularExpressionValidator runat="server" ID="revtxtName" ControlToValidate="txtFirstName"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInFirstName %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup" ></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtFirstName"
            runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterFirstName %>"
            ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlLastName" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, LastName1 %>" /><span class="req">*</span>
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtLastName" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" autocomplete="off"></asp:TextBox>
        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ControlToValidate="txtLastName"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInLastName %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup"></asp:RegularExpressionValidator>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtLastName"
            runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterLastName %>"
            ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlAddressLine1" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, AddressLine1 %>" /><span class="req">*</span>
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtAddress1" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" onchange="javascript:Changed(this);" autocomplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAddress1"
            runat="server" Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAddressLine1 %>"
            ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ID="revtxtAddress1" ControlToValidate="txtAddress1"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInAddressLine1 %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup"></asp:RegularExpressionValidator>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlAddressLine2" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, AddressLine2 %>" />
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtAddress2" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" onchange="javascript:Changed(this);" autocomplete="off"></asp:TextBox>
        <asp:RegularExpressionValidator runat="server" ID="revtxtAddress2" ControlToValidate="txtAddress2"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInAddressLine2 %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup"></asp:RegularExpressionValidator>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlAddressLine3" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, AddressLine3 %>" />
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtAddress3" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" onchange="javascript:Changed(this);" autocomplete="off"></asp:TextBox>
        <asp:RegularExpressionValidator runat="server" ID="revtxtAddress3" ControlToValidate="txtAddress3"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInAddressLine3 %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup"></asp:RegularExpressionValidator>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlZipcode" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, ZipPostalCode1 %>" /><span class="req">*</span>
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtZip" runat="server" Width="260" CssClass="textBoxes" MaxLength="50" onchange="javascript:Changed(this);" autocomplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvtxtZip" ControlToValidate="txtZip" runat="server"
            Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAZipCode %>" ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
        <asp:CustomValidator ID="cvtxtZip" runat="server" ControlToValidate="txtZip" Text="*"
            ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidZipCode %>" ClientValidationFunction="ValidateZip"
            ValidationGroup="ReplaceThisValidationGroup"></asp:CustomValidator>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlCountry" EnableViewState="true" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Country1 %>" /><span class="req">*</span>
    </label>
    <div class="form-value">
        <asp:DropDownList ID="ddlCountry" runat="server" Width="270" EnableViewState="true"
            AutoPostBack="false" onchange="ddlCountry_onchange(this)" />
        <asp:RequiredFieldValidator ID="reqddlCountry" ControlToValidate="ddlCountry" runat="server"
            Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectACountry %>" InitialValue="00000000-0000-0000-0000-000000000000"
            ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
    </div>
</asp:Panel>
<ComponentArt:CallBack ID="clbkState" runat="server" EnableViewState="true" CacheContent="false" 
    PostState="false" LoadingPanelFadeMaximumOpacity="25" LoadingPanelFadeDuration="10">
    <ClientEvents>
        <CallbackComplete EventHandler="stateLoaded" />
    </ClientEvents>
    <Content>
        <asp:PlaceHolder ID="phCountryRelated" runat="server">
            <asp:Panel runat="server" ID="pnlState" EnableViewState="true" CssClass="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, State1 %>" />
                </label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlState" runat="server" Width="270" EnableViewState="true"
                        AutoPostBack="false" onchange="ddlstate_onchange(this)" />
                    <asp:TextBox runat="server" ID="txtState" Width="260" MaxLength="255" Visible="false" autocomplete="off"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvddlState" ControlToValidate="ddlState" runat="server"
                        Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAState %>" InitialValue="00000000-0000-0000-0000-000000000000"
                        ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator ID="rvStateText" ControlToValidate="txtState" runat="server"
                        Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAState %>" 
                        ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlPhone" CssClass="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, Phone1 %>" /><span class="req">*</span>
                </label>
                <asp:Panel ID="pnlUSPhone" runat="server" CssClass="form-value">
                    <ComponentArt:MaskedInput runat="server" ID="mTxtPhone" AcceptedCharacters="1234567890-. ()"
                        Width="260" MaxLength="15" Transform="Telephone_NorthAmerica" SkinID="Default" />
                </asp:Panel>
                <asp:Panel ID="pnlOtherPhone" runat="server" CssClass="form-value" Visible="false">
                    <asp:TextBox ID="txtPhone" runat="server" Width="260" CssClass="textBoxes" MaxLength="50" />
                </asp:Panel>
                <asp:CustomValidator ID="cvtxtPhone" ControlToValidate="mTxtPhone" runat="server" Text="*"
                    ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidPhoneNumber %>" ClientValidationFunction="ValidatePhone"
                    ValidationGroup="ReplaceThisValidationGroup"></asp:CustomValidator>
            </asp:Panel>
        </asp:PlaceHolder>
    </Content>
</ComponentArt:CallBack>

<asp:Panel runat="server" ID="pnlCity" CssClass="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, City1 %>" /><span class="req">*</span>
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtCity" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" onchange="javascript:Changed(this);" autocomplete="off"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvtxtCity" ControlToValidate="txtCity" runat="server"
            Text="*" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterTheCity %>" 
            ValidationGroup="ReplaceThisValidationGroup"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator runat="server" ID="revtxtCity" ControlToValidate="txtCity"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInCity %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup"></asp:RegularExpressionValidator>
    </div>
</asp:Panel>

<asp:Panel runat="server" ID="pnlCounty" CssClass="form-row county-row" style="display:none;">
    <label class="form-label">
        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, County %>"  />
    </label>
    <div class="form-value">
        <div style="display: none;">
            <asp:TextBox ID="txtCounty" runat="server" CssClass="textBoxes" Width="260" MaxLength="255" onchange="javascript:Changed(this);" autocomplete="off"></asp:TextBox>
        </div>
        <asp:DropDownList runat="server" ID="countySelection" Width="270" onchange="ddlcounty_onchange(this)" ></asp:DropDownList>

        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator2" ControlToValidate="txtCounty"
            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInCity %>"
            Text="*" ValidationGroup="ReplaceThisValidationGroup"></asp:RegularExpressionValidator>
    </div>
</asp:Panel>



<asp:HiddenField ID="hfCountryValue" runat="server" />
<asp:HiddenField ID="hfState" runat="server" />
<asp:HiddenField ID="hfIsAddressValidated" runat="server" Value="1" />
<script type="text/javascript">
    
$( document ).ready(function() {

    if($("#<%=countySelection.ClientID%> option").length > 1)
    {
        $(".county-row").show();
    }else{
        $(".county-row").hide();
    }

        $( "#<%=txtZip.ClientID%>" ).change(function() {
            populateAddress($("#<%=txtZip.ClientID%>").val());
        });

        $( "#<%=txtCity.ClientID%>" ).change(function() {
            generateCountyDropDown();
        });
    });
    <%=clbkState.ClientID%>.LoadingPanelClientTemplate = '<table cellpadding="0" cellspacing="0" border="0" width="50%"><tr><td height="15" width="50%" align="center"><img src="../../images/spinner.gif" alt="<%= GUIStrings.Loading %>" /></td></tr></table>';

    function populateAddress(zipCode)
    {    
        $.ajax({
            type: "POST",
            url: "/CommerceAdmin/DataHelper.asmx/GetLocationInfo",
            data: "{'zip': '"+zipCode+"'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(msg) {
                addressInfo = jQuery.parseJSON(msg.d);
                if(addressInfo.StateId != "00000000-0000-0000-0000-000000000000")
                {
                    var selectedCountry = $( "#<%=ddlCountry.ClientID%>" ).val();
                    if(selectedCountry.toLowerCase() != usId.toLowerCase())
                    {
                        $( "#<%=ddlCountry.ClientID%>" ).val(usId);
                        ddlCountry_onchange($( "#<%=ddlCountry.ClientID%>" ).get(0));
                    }
                    else
                    {
                        stateLoaded();
                    }
                  
                }
                else
                {
                    $(".county-row").hide();
                }
            }
        });
    }

    function generateCountyDropDown()
    {    
       

        var selectedCountry = $( "#<%=ddlCountry.ClientID%>" ).val();        
        if(selectedCountry.toLowerCase() == usId.toLowerCase())
        {
            var zipCode = $("#<%=txtZip.ClientID%>" ).val();
            var city = $("#<%=txtCity.ClientID%>" ).val();
            $.ajax({
                type: "POST",
                url: "/CommerceAdmin/DataHelper.asmx/GetCountyInfo",
                data: "{'zip': '"+zipCode+"', 'city' : '"+city+"'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $('#countySelection').empty();
                    var obj = jQuery.parseJSON(msg.d);
                    if(obj)
                    {
                        if(obj.length > 1)
                        {
                            $(".county-row").show();
                        }
                        else 
                        {
                            $(".county-row").hide();
                        }

                        if(obj.length  > 0)
                        {
                            $("#<%=txtCounty.ClientID%>").val(obj[0]);
                            $("#<%=countySelection.ClientID%>").empty();
                            $.each(obj, function(i, optionHtml){
                                $("#<%=countySelection.ClientID%>").append("<option>"+optionHtml+"</option>");
                            });
                        }
                    }
                    else{
                        $("#<%=txtCounty.ClientID%>").val('');
                    }
                
              
                }
            });
        }
        else
        {
            $(".county-row").hide();            
        }
    }


</script>
