﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HtmlEditor.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.General.HtmlEditor" %>
<script type="text/javascript">
    var oureditor;
    var adminSiteUrl = jCMSAdminSiteUrl + "/";
    var jTemplateType = '0';

    function OnClientLoad(editor, args) {
        oureditor = editor;
        if (window.PasteContentToEditor) {
            PasteContentToEditor(oureditor);
        }
    }
    function ConvertToHtml(str) {
        var replacedString = str;
        replacedString = replacedString.replace(new RegExp("&amp;", "g"), "&");
        replacedString = replacedString.replace(new RegExp("&lt;", "g"), "<");
        replacedString = replacedString.replace(new RegExp("&gt;", "g"), ">");
        replacedString = replacedString.replace(new RegExp("&quot;", "g"), "\"");
        replacedString = replacedString.replace(new RegExp("&apos;", "g"), "'");
        replacedString = replacedString.replace(new RegExp("&slash;", "g"), "\\");
        return replacedString;
    }
    function CloseDialog() {
        window.parent.CloseEditor = 'true';
        parent.contentDefinitionPopup.hide();
    }

    if (EditorToUse.toLowerCase() == 'ckeditor') {
        //setting the editor global variable when the editor initializes
        $(document).ready(function () {
            CKEDITOR.on('instanceReady', function (ev) {
                ckEditor = ev.editor;
                OnClientLoad(ckEditor, null);
            });
        });
    }
</script>

<rade:radeditor id="RadEditor1" runat="server" visible="true" toolsfile="/iAPPS_Editor/CommerceAdminToolsFile.xml"
    toolbarmode="ShowOnFocus" onclientload="OnClientLoad" newlinebr="false" allowscripts="true"
    width="210" height="200" backcolor="White" toolswidth="718" editmodes="Design,HTML"
    borderwidth="0">
    <content>
    </content>
    <CssFiles>
        <radE:EditorCssFile Value="/iAPPS_Editor/authorStyles.css" />
    </CssFiles>
</rade:radeditor>

<CKEditor:CKEditorControl ID="ckEditor1" runat="server" AutoGrowMinHeight="200" EnterMode="P"
    BasePath="~/iAPPS_Editor" Width="908" Height="400" Entities="true" HtmlEncodeOutput="true"
    IgnoreEmptyParagraph="true" BrowserContextMenuOnCtrl="true" EntitiesGreek="true"
    ScaytAutoStartup="false" ShiftEnterMode="BR" TemplatesReplaceContent="false"
    ForceSimpleAmpersand="true" ToolbarCanCollapse="false">
</CKEditor:CKEditorControl>
<asp:PlaceHolder runat="server" ID="phScriptTags" />