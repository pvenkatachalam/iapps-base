﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearchFilter.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.General.ProductSearchFilter" %>
<script type="text/javascript">
    function ValidateSearchClick() {
        var keyword = document.getElementById("<%=txtSearchTerm.ClientID %>").value;
        var productType = ddlProductType.get_text();

        if (keyword == "<%= GUIStrings.ProductSearchDefaultText %>")
            keyword = "";

        if (keyword == "" && productType == "") {
            if (typeof btnAdd != 'undefined' && document.getElementById(btnAdd))
                document.getElementById(btnAdd).disabled = true;
            if (typeof btnAddMore != 'undefined' && document.getElementById(btnAddMore))
                document.getElementById(btnAddMore).disabled = true;

            alert("<%= GUIStrings.PleaseEnterAKeywordOrSelectATypeToSearch %>");
            return false;
        }
        else {
            if (typeof btnAdd != 'undefined' && document.getElementById(btnAdd))
                document.getElementById(btnAdd).disabled = false;
            if (typeof btnAddMore != 'undefined' && document.getElementById(btnAddMore))
                document.getElementById(btnAddMore).disabled = false;

            return true;
        }

    }

    function trvProductTypes_onNodeSelect(sender, eventArgs) {
        ddlProductType.set_text(eventArgs.get_node().get_text());
        ddlProductType.collapse();
    }

    function fn_ClearProductType() {
        var $objSearchTextBox = $('#<%= txtSearchTerm.ClientID %>');
        if ($objSearchTextBox.val() != '') {
            $objSearchTextBox.val($objSearchTextBox.prop('title'));
            $objSearchTextBox.addClass('defaultText');
        }
        ddlProductType.set_text("");
        return false;
    }

    $(document).ready(function () {        
        $("#<%=ddlProductType.ClientID%>_Input").attr("placeholder", "<%= GUIStrings.ProductType1 %>");
    });

</script>
<div class="search-panel clear-fix">
    <div class="columns">
        <asp:TextBox ID="txtSearchTerm" runat="server" CssClass="textBoxes" Width="350" ToolTip="<%$ Resources:GUIStrings, ProductSearchDefaultText %>" />
    </div>
    <div class="columns">
        <ComponentArt:ComboBox ID="ddlProductType" runat="server" SkinID="Default" DropDownHeight="297" 
            DropDownWidth="285" AutoHighlight="false" AutoComplete="false" Width="240">
            <DropDownContent>
                <ComponentArt:TreeView ID="trvProductTypes" ExpandSinglePath="true" SkinID="Default"
                    AutoPostBackOnSelect="false" Height="293" Width="285" CausesValidation="false"
                    runat="server" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                    <ClientEvents>
                        <NodeSelect EventHandler="trvProductTypes_onNodeSelect" />
                    </ClientEvents>
                </ComponentArt:TreeView>
            </DropDownContent>
        </ComponentArt:ComboBox>
    </div>

    <div class="columns">
        <asp:UpdatePanel runat="server" ID="UpdatePanel1">
            <ContentTemplate>
                <asp:Button ID="btnSearch" Text="<%$ Resources:GUIStrings, Search %>" CssClass="small-button" runat="server" OnClick="btnSearch_Click" OnClientClick="return ValidateSearchClick();" />

                <asp:Button ID="btnClear" runat="server" Text="<%$ Resources:GUIStrings, Clear %>" CssClass="small-button" OnClientClick="return fn_ClearProductType();" />

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
