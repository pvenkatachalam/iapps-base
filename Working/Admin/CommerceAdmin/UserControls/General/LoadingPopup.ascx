﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoadingPopup.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.LoadingPopup" %>
<script type="text/javascript">
    function ShowLoadingModalBox() {
        $('#LoadingModalPopup').dialog('open');
        return false;
    }
    //method to close the dialog through code
    function CloseLoadingModalBox() {
        $('#LoadingModalPopup').dialog('close');

        // To avoid scrollbars in IE8
        if ($.browser.msie && $.browser.version <= 8) {
            setTimeout(function () {
                $("body form").nextAll("div").each(function () {
                    if ($(this).css("height") == "1px" && $(this).css("width") == "1px")
                        $(this).hide();
                });
            }, 1000);
        }

        return false;
    }
</script>
<!-- HTML required for Loading Modal Popup STARTS -->
<div style="visibility:hidden;">
    <div id="LoadingModalPopup" title="Please wait...">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td align="center" valign="middle" height="100">
                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/images/PleaseWait.gif" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#LoadingModalPopup").dialog({ modal: true, bgiframe: true, autoOpen: false, resizable: false });
    });
</script>
<!-- HTML required for Loading Modal Popup ENDS -->