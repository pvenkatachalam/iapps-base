﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderShipping.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Order_Shipping" %>
<%@ Register TagPrefix="oi" TagName="OrderItems" Src="~/UserControls/StoreManager/Orders/OrderItems.ascx" %>
<!-- Order Item Listing STARTS -->
<oi:OrderItems ID="ctlOrderItems" runat="server"></oi:OrderItems>
<!-- Order Item Listing ENDS -->
<!-- Payment Information STARTS -->
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td class="header">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="header"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ShippingInformation %>" /></td>
    </tr>
    <tr>
        <td>
            <br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AddressType %>" /></strong><asp:Literal ID="lblAddressType" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Addressee %>" /></strong><asp:Literal ID="lblAddressee" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ShippingMethod %>" /></strong><asp:Literal ID="lblShippingMethod" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Address %>" /></strong>
            <asp:Literal ID="lblAddressTo" runat="server"></asp:Literal>
            <asp:Literal ID="lblAddress1" runat="server"></asp:Literal>
            <asp:Literal ID="lblAddress2" runat="server"></asp:Literal>
            <asp:Literal ID="lblCity" runat="server"></asp:Literal>,&nbsp;
            <asp:Literal ID="lblState" runat="server"></asp:Literal>&nbsp;
            <asp:Literal ID="lblZip" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Phone %>" /></strong><asp:Literal ID="lblPhone" runat="server"></asp:Literal><br />
        </td>
    </tr>
</table>
<!-- Payment Information ENDS -->