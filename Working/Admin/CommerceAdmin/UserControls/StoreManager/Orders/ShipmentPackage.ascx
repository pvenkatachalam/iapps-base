﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentPackage.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ShipmentPackage" %>

<script type="text/javascript">
//    function UpdateShippingPrice_= cbkShippingCharge.ClientID () {
//    alert('I m in updateShippingPrice');
//    //two call backs are happening same time. one is capture and this.
//        //cbkShippingCharge.callback();
//    }
</script>
<div class="columns"
    <span class="formRow">
        <label class="formLabel">
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ShippingPrice %>" /></label>
        <asp:Label ID="lblShippingCharge" runat="server"></asp:Label>                                    
    </span>
    <span class="formRow">
        <label class="formLabel">
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ContainerWeight %>" /></label>
        <asp:TextBox ID="txtContainerWeight" runat="server" CssClass="textBoxes" Width="100"></asp:TextBox>                                    
    </span>
</div>
<asp:HiddenField ID="hdnPackageNo" runat="server" />

<ComponentArt:Grid ID="grdShipOrder" AllowTextSelection="true" EnableViewState="true"
    EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False" CssClass="Grid"
    KeyboardEnabled="false" ShowFooter="false" SkinID="Default" FooterCssClass="GridFooter" 
    ItemDraggingEnabled="false" ItemDraggingClientTemplateId="dragTemplate" ExternalDropTargets="grdShipOrder" SelectedRowCssClass="SelectedRow"
    RunningMode="Client" Width="665" runat="server" PageSize="1000" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
    <ClientEvents>
        <ItemExternalDrop EventHandler="grdShipOrder_OnExternalDrop" />
    </ClientEvents>
    <Levels>
        <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="Id"
            RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
            HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
            HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
            HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="Row" GroupHeadingCssClass="GroupHeading"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            AllowSorting="false" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
            EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField">
            <Columns>
                <ComponentArt:GridColumn Width="90" Align="Center" AllowReordering="False" FixedWidth="true"
                    HeadingText="<%$ Resources:GUIStrings, Thumbnail %>" DataCellServerTemplateId="ThumbnailTemplate"
                     DataField="OrderItem.SKUId" />
                <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="150"
                    HeadingText="<%$ Resources:GUIStrings, ProductName %>" DataCellClientTemplateId="ProductNameHoverTemplate"
                    DataField="OrderItem.SKU.ParentProduct.Title"  />
                <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="100"
                    HeadingText="<%$ Resources:GUIStrings, SKU %>" DataCellClientTemplateId="SKUHoverTemplate"
                    DataField="OrderItem.SKU.SKU"  />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, QtyOrdered %>" DataField="OrderItem.Quantity"
                    DataCellClientTemplateId="QtyOrderedHoverTemplate" Width="90" AllowReordering="False"
                    FixedWidth="true" Align="Right"  />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, RemainingQty %>" AllowReordering="False"
                    FixedWidth="true" Width="100" DataField="RemainingQuantity" DataCellClientTemplateId="RemainingQtyHoverTemplate"
                    Align="Right"  />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, ShipQty %>" DataField="Quantity"
                    DataCellServerTemplateId="ShipQtyTemplate"  AllowReordering="False" FixedWidth="true"
                    Align="Left" AllowSorting="False" Width="70"  />
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, ShipAll %>" DataField="OrderItem.TotalViews"
                    DataCellServerTemplateId="ShipAllTemplate" AllowReordering="False" FixedWidth="true"
                    Align="Center" Width="60" AllowSorting="False"  />
                <ComponentArt:GridColumn DataField="OrderItem.Id" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="ProductNameHoverTemplate">
            <div title="## DataItem.GetMember('OrderItem.SKU.ParentProduct.Title').get_text() ##">
                ## DataItem.GetMember('OrderItem.SKU.ParentProduct.Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('OrderItem.SKU.ParentProduct.Title').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SKUHoverTemplate">
            <div title="## DataItem.GetMember('OrderItem.SKU.SKU').get_text() ##">
                ## DataItem.GetMember('OrderItem.SKU.SKU').get_text() == "" ? "&nbsp;" : DataItem.GetMember('OrderItem.SKU.SKU').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QtyOrderedHoverTemplate">
            <div title="## DataItem.GetMember('OrderItem.Quantity').get_text() ##">
                ## DataItem.GetMember('OrderItem.Quantity').get_text() == "" ? "&nbsp;" : DataItem.GetMember('OrderItem.Quantity').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="RemainingQtyHoverTemplate">
            <div title="## DataItem.GetMember('RemainingQuantity').get_text() ##">
                ## DataItem.GetMember('RemainingQuantity').get_text() == "" ? "&nbsp;" : DataItem.GetMember('RemainingQuantity').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="dragTemplate">
            <div style="height: 100px; width: 100px;">
                ## DataItem.GetMember('OrderItem.SKU.ParentProduct.Title').get_text() ##</div>
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
    <ServerTemplates>
        <ComponentArt:GridServerTemplate ID="ShipQtyTemplate">
            <Template>
                <%--<input type="text" value="## DataItem.GetMember('RemainingToShip').get_text() ##" id="shippedQty" style="width:65px;" class="textBoxes" />--%>
                <asp:TextBox runat="server" ID="txtQuantity" Text='<%# Container.DataItem["Quantity"] %>'
                    Style="width: 65px;" CssClass="textBoxes"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" CultureInvariantValues="true" runat="server"
                    Type="Double" ControlToValidate="txtQuantity" ValueToCompare='<%# Container.DataItem["Quantity"].ToString() %>'
                    Operator="LessThanEqual" ErrorMessage="<%$ Resources:GUIStrings, QuantityToShipMustBeLessThanOrEqualToRemainingQuantity %>"
                    Text="*" ValidationGroup="Page"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator2" CultureInvariantValues="true" runat="server"
                    Type="Double" ControlToValidate="txtQuantity" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:GUIStrings, QuantityToShipMustBeAValidDecimalNumber %>"
                    Text="*" ValidationGroup="Page"></asp:CompareValidator>
            </Template>
        </ComponentArt:GridServerTemplate>
        <ComponentArt:GridServerTemplate ID="ShipAllTemplate">
            <Template>
                <asp:CheckBox runat="server" ID="chkShipAll" Checked="true"></asp:CheckBox>
                <%--<input type="checkbox" id="chkShipAll" checked="checked" onchange="ResetTextbox(this,'## DataItem.GetMember('RemainingToShip').get_text()','<%=txtQuantity.ClientID%>')" />--%>
            </Template>
        </ComponentArt:GridServerTemplate>
        <ComponentArt:GridServerTemplate ID="ThumbnailTemplate">
            <Template>
                <asp:Image ID="ImgThumbNail" runat="server" ImageUrl="~/App_Themes/General/images/no-image.gif"
                    AlternateText="<%$ Resources:GUIStrings, NoImageFound %>" />
            </Template>
        </ComponentArt:GridServerTemplate>
    </ServerTemplates>
</ComponentArt:Grid>
