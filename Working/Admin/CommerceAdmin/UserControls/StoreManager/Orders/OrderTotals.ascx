﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderTotals.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Orders.OrderTotals" %>
<div class="columns thin-column" style="width: 248px;">
    <h3><%= GUIStrings.PaymentSummary %></h3>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.OrderTotal1 %></label>
        <div class="form-value">
            <asp:Label ID="lblOrderTotal" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Tax1 %></label>
        <div class="form-value">
            <asp:Label ID="lblTax" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Shipping1 %></label>
        <div class="form-value">
            <asp:Label ID="lblShipping" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.CODCharges1 %></label>
        <div class="form-value">
            <asp:Label ID="lblCODCharge" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.ShippingDiscounts1 %></label>
        <div class="form-value">
            <asp:Label ID="lblShippingDiscounts" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.OrderDiscounts1 %></label>
        <div class="form-value">
            <asp:Label ID="lblTotalDiscounts" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.GRANDTOTAL1 %></label>
        <div class="form-value">
            <strong>
                <asp:Label ID="lblGrandTotal" runat="server" Text="$0.00" CssClass="formValue"></asp:Label></strong>
        </div>
    </div>
</div>
<div class="columns thin-column" style="width: 248px; margin-left: 40px;">
    <h3>&nbsp;</h3>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.PaymentTotal1 %></label>
        <div class="form-value">
            <asp:Label ID="lblPaymentTotal" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
       <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.RefundTotal %></label>
        <div class="form-value">
            <asp:Label ID="lblRefundTotal" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.PaymentRemaining1 %></label>
        <div class="form-value">
            <asp:Label ID="lblPaymentRemaining" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
        </div>
    </div>
</div>