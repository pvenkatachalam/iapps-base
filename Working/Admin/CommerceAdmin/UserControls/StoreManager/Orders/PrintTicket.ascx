﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrintTicket.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.PrintTicket" %>
<%@ Register TagPrefix="PS" TagName="Payment" Src="~/UserControls/StoreManager/Orders/PaymentSummary.ascx" %>
<%@ Register TagPrefix="OS" TagName="Shipping" Src="~/UserControls/StoreManager/Orders/OrderShipping.ascx" %>
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="border: solid 3px #000000;">
    <tr>
        <td colspan="2" class="header">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DetailsForOrder %>" /><asp:Literal ID="ltHeaderOrderNo" runat="server"></asp:Literal>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderPlaced %>" /></strong><asp:Literal ID="ltlDate" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderNumber %>" /></strong><asp:Literal ID="ltlOrderNumber" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderStatus1 %>" /></strong><asp:Literal ID="ltlOrderStatus" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CustomerName1 %>" /></strong><asp:Literal ID="ltlName" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CustomerEmail %>" /></strong><asp:Literal ID="ltlCustomerEmail" runat="server"></asp:Literal><br />
            <strong><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AccountNumber %>" /></strong><asp:Literal ID="ltlAccountNumber" runat="server"></asp:Literal><br />
            <asp:PlaceHolder ID="trPlacedBy" runat="server">
                <strong><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, PlacedBy %>" /></strong><asp:Literal ID="ltlPlacedBy" runat="server"></asp:Literal><br />
            </asp:PlaceHolder>
        </td>
    </tr>
    <tr>
        <td class="header" colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="header">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ItemsOrdered %>" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Repeater ID="rptShippings" runat="server" OnItemDataBound="rptShippings_DataBound">
                <ItemTemplate>
                    <OS:Shipping ID="ctlShipping" runat="server"></OS:Shipping>
                </ItemTemplate>
            </asp:Repeater>
        </td>
    </tr>
    <tr>
        <td class="header" colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" class="header">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PaymentSummary %>" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <PS:Payment ID="ctlPaymentSummary" runat="server"></PS:Payment>
        </td>
    </tr>
</table>
<br style="page-break-after: always;" />