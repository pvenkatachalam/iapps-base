﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Shipment.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.Shipment" %>
<script type="text/javascript"> 
    var _cancel = '<asp:Localize runat="server" Text="<%$ Resources:JSMessages, Cancel %>"/>';
    var idColumn = 0;
    var snoColumn = 1;
    var qtyColumn = 2;
    var titleColumn = 3;
    var priceColumn = 4;
    var discountColumn = 5;
    var itemTotalColumn = 6;
    var shippedQtyColumn = 7
    var statusColumn = 8;
    var PersonalizedAttributeColumn = 9;
    var actionColumn = 10;
    var skuIdColumn = 11 ;
    var shippingAddressIdColumn = 12;
    var shippingMethodIdColumn = 13;
    
    var lblShipmentSubtotalColumn = 14;
    var lblTaxColumn = 15;
    var lblShippingColumn = 16;
    var lblSubtotalColumn = 17;
    var taxAmountColumn = 18;
    var shippingChargeColumn = 19;
    var parentOrderItemIdColumn = 20;
    var orderIdColumn = 24;
    var isDownloadableMediaColumn = 25;
    var ordershippingIdColumn = 26;
    var isBackOrdershippingColumn = 27;
    
    function grdShipment_noContextMenu(sender, eventArgs) 
    {
    }

    function grdShipment_onContextMenu(sender, eventArgs) 
    {
        if (!isFinite(sender.SliderPosition))
        {
            sender.SliderPosition = 0;
        }
        if (sender.SliderPosition == "NaN")
        {
            sender.SliderPosition = 0;
        }
        if (sender.SliderPosition == "Infinity")
        {
            sender.SliderPosition = 0;
        } 
        currentGrid = sender;
        gridItem = eventArgs.get_item();
        if(gridItem.get_table().get_level() == 0)
        {
        if (typeof (mnuShipment) != "undefined") {
            var menuItems = mnuShipment.get_items();
            if(menuItems.get_length() !=0)
            {
                menuItems.getItem(1).set_visible(true);
                menuItems.getItem(2).set_visible(true);
                menuItems.getItem(3).set_visible(true);
                menuItems.getItem(4).set_visible(true);
            }
            if (sender.Data.length == 1 &&
            (gridItem.getMember(skuIdColumn).get_value() == null ||
              gridItem.getMember(skuIdColumn).get_value() == '' || gridItem.getMember(skuIdColumn).get_value() == emptyGuid)) {
              if(menuItems.get_length() !=0)
                {
                menuItems.getItem(1).set_visible(false);
                menuItems.getItem(2).set_visible(false);
                menuItems.getItem(3).set_visible(false);
                menuItems.getItem(4).set_visible(false);
                }
            }
            
            if(gridItem.getMember(qtyColumn).get_value() == gridItem.getMember(shippedQtyColumn).get_value())
            {   
                if(menuItems.get_length() !=0)
                {           
                menuItems.getItem(1).set_visible(false);
                menuItems.getItem(2).set_visible(false);
                menuItems.getItem(3).set_visible(false);
                menuItems.getItem(4).set_visible(false);
                }
            }
            
            if(parseFloat(gridItem.getMember(shippedQtyColumn).get_value()) > 0)
            {
                if(menuItems.get_length() !=0)
                {
                    menuItems.getItem(4).set_visible(false);
                }
            }
            var e = eventArgs.get_event();
            mnuShipment.showContextMenuAtEvent(e);
        }
        }
    }
    
//    function mnuShipment_onItemSelect(sender,eventArgs)
//    {
//        var menuItem = eventArgs.get_item();
//        var selectedMenuId = menuItem.get_id();
//        currentUserObject = sender;
//        var thisRowIndex = gridItem.getMember(snoColumn).get_value();
//        if (selectedMenuId != null) 
//        {
//            switch (selectedMenuId) 
//            {
//            case 'cmAdd':
//                ShowAddProductPopup();
//                break;
//            case 'cmEdit':
//                currentGrid.edit(gridItem);
//                break;    
//            case 'cmDelete':
//                currentGrid.set_callbackParameter('Delete:' + thisRowIndex);
//                currentGrid.callback();            
//                break;
//            }
//         }
//      }
           
      function grdShipment_ItemSelect(sender,eventArgs)
      {
        currentUserObject = sender;
      }
      //var isCallingForBindShippingOptions = true;
      function grdShipment_CallbackComplete<%=grdShipment.ClientID %>(sender,eventArgs)
      {  
          
          var loadPage;
          var thisOrderId = OrderId;
          var currentUrl = document.URL;
           if (thisOrderId=='00000000-0000-0000-0000-000000000000') {
                var row = <%=grdShipment.ClientID%>.get_table().getRow(0);
                thisOrderId = row.getMember(orderIdColumn).get_text();
            }
            
            if (currentUrl.indexOf('OrderId=00000000-0000-0000-0000-000000000000') > 0 && thisOrderId != undefined && thisOrderId != null) {
                currentUrl = currentUrl.replace('OrderId=00000000-0000-0000-0000-000000000000', 'OrderId=' + thisOrderId);
            }
          
            if(thisOrderId != undefined && thisOrderId != null)
                loadPage = true;
            
              if(loadPage && reloadAfterDelete)
              {
                if(currentUrl.search("&Msg=EmailMsg") > -1)
                {
                    currentUrl = currentUrl.replace("&Msg=EmailMsg","");
                }
                window.location.href = currentUrl;
              }
              else
              {
                  runAllCallbacks();                         
                  OrderCallback.callback('DisplayTotal:'); //Display Total
                  AddNewCallback.callback();
              }
      }

    var shippingPopup;
    function ShowShippingMethodPopup(orderShippingId,addressId,shippingMethodId){
      var popupPath = "../../Popups/StoreManager/Orders/SelectShippingMethod.aspx?OrderShippingId=" + orderShippingId + "&AddressId="+addressId+ "&shippingMethodId=" + shippingMethodId;
        shippingPopup = dhtmlmodal.open('ShippingPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
        shippingPopup.onclose = function () {
            var a = document.getElementById('ShippingPopup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

        function CloseShippingMethodPopup() {
        shippingPopup.hide();
//grdShipment.set_callbackParameter("UpdatePrice:");
//grdShipment.callback();
// cbDestinationSummary.callBack();

        runAllCallbacks();
        OrderCallback.callback('DisplayTotal:'); //Display Total
    }

    var productSearchPopup;

    function GetFullUrl(virtPath) {
        var fullpath = "../../" + virtPath.replace("~", "");
        return fullpath;
    }

    function ConfirmDelete(thisGridObject, orderShippingId) {
        if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisWillDeleteAllTheItemsInIt %>' />")) {
            AddNewCallback.callback('Delete:' + orderShippingId);
            return false; //Check it.. no postback again...
        }
        else {
            return false;
        }
    }

    function ShowAddProductPopup() {
        var popupPath = "../../Popups/StoreManager/AddProductSkuPopup.aspx?showSKUs=true";
        productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
        productSearchPopup.onclose = function () {
            var a = document.getElementById('ProductSearchPopup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function ShowBundleSkuSelectPopup(orderItemId) {
        var isBundleCompositionModified = CheckBundleCompositionModified(orderItemId);
        if (isBundleCompositionModified != 'False') {
            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ThisBundlesCompositionHasBeenModified %>' />");
            return;
        }

        var popupPath = "../../Popups/StoreManager/Orders/SelectBundleSku.aspx?orderItemId=" + orderItemId;
        bundleSkuSelectPopup = dhtmlmodal.open('BundleSkuSelectPopup', 'iframe', popupPath, '', 'width=250px,height=145px,center=1,resize=0,scrolling=1');
        bundleSkuSelectPopup.onclose = function () {
            var a = document.getElementById('BundleSkuSelectPopup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function ClosePopupBundleSkuSelectPopup() {
        grdShipment.set_callbackParameter("BundleSkuChange:");
        grdShipment.callback();
        bundleSkuSelectPopup.hide();
    }

    function AddSkus(skuIds) {
        if (skuIds != null && skuIds != '') {
            //var obj = document.getElementById(currentGridClientObjectId);
            //Checking Inventory
            var inventoryCheckingMsg = ValidateInventory(skuIds, '1', '1', 'Add');
            if (inventoryCheckingMsg == 'True') {
                //If the item is getting added in BO shipping, and there are 2 available shipping, and the item is available, then prompt the user to select the correct available shipping
                var isBackOrdershipping = gridItem.getMember(isBackOrdershippingColumn).get_value();
                var orderShippingId = gridItem.getMember(ordershippingIdColumn).get_value();
                var orderShippingChkMsg = '';
                if (isBackOrdershipping)
                {
                    var orderId = gridItem.getMember(orderIdColumn).get_value();
                    orderShippingChkMsg = ValidateOrderShipping(skuIds,orderId,orderShippingId);
                    if (orderShippingChkMsg != 'True') {
                        alert(orderShippingChkMsg);
                    }
                }
                if(orderShippingChkMsg == '' || orderShippingChkMsg == 'True')
                {
                    currentGrid.set_callbackParameter(skuIds);
                    currentGrid.callback();
                }
            }
            else {
                alert(inventoryCheckingMsg);
            }
        }
    }

    function GetShippingAddressSelectedValue(thisObject,
        addressObjId,
        shipmentSubtotalObjId,
        taxObjId,
        shippingObjId,
        subtotalObjId, thisShippingId
        ) {

        
        var selectedItem = thisObject.options[thisObject.selectedIndex];
        var selectedItemValue = selectedItem.value;

        if(selectedItemValue == "-1")
        {            
            var currentUrl = document.URL;
            currentUrl = currentUrl.replace('?', '&');
            currentUrl = currentUrl.replace('#', '');
            window.location = jCommerceAdminSiteUrl + '/StoreManager/Customers/Shipping.aspx?ReturnUrl=' + currentUrl + '&CustomerId=' + SelectedCustomerId+'&OrderShippingId='+thisShippingId;
            return;
        }

        var shippingAddressId = selectedItemValue; //.substring(0,36);
        //Todo:Make a callback to get the address
        var shippingAddress = GetShippingAddress(shippingAddressId, thisShippingId); //selectedItemValue.substring(37);

        //alert("Value: " + selectedItem.value + "\nText: " + selectedItem.text);
        document.getElementById(addressObjId).innerHTML = shippingAddress;
            runAllCallbacks();
            OrderCallback.callback('DisplayTotal:'); //Display Total   
    }

    function GetShippingMethodSelectedValue(thisObject,
        shipmentSubtotalObjId,
        taxObjId,
        shippingObjId,
        subtotalObjId,
        shippingAddressObjId,
        thisShippingId
        ) {

        var trvObject = eval(thisObject);
        var selectedItem = trvObject.get_selectedNode(); //thisObject.options[thisObject.selectedIndex];
        //        //Get Selected value from shipping method
        var currentShippingAddressObj = document.getElementById(shippingAddressObjId);
        var selectedShippingAddress = currentShippingAddressObj.options[currentShippingAddressObj.selectedIndex];
        //        //Todo:Make a calback to get the calculated value
        var selectedShippingMethodValue = selectedItem.get_id()//.value;
        var selectedShippingMethodTitle = selectedItem.get_text();
        var shippingAmount = SaveChangedShippingOption(selectedShippingMethodValue, thisShippingId, selectedShippingMethodTitle);
        runAllCallbacks();
        OrderCallback.callback('DisplayTotal:'); //Display Total

    }


    function CancelClicked() {
        currentGrid.editCancel();
        gridItem = null;
    }

    // Save the modified record
    function SaveRecord() {
           
        var oldQty = gridItem.getMember(qtyColumn).get_value();
        
        currentGrid.editComplete();
        var currentQty = gridItem.getMember(qtyColumn).get_value();
        if (currentQty < 0) {
            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, QuantityShouldNotBeNegative %>" />');
            currentGrid.edit(gridItem);
            gridItem.SetValue(qtyColumn, oldQty);
            return;
        }

        var changedQty = parseFloat(currentQty) - parseFloat(oldQty);
        var skuIds = gridItem.getMember(skuIdColumn).get_value();

        var isMinAndIncrValid = CheckQtyMinAndIncrement(skuIds, currentQty);
        if (isMinAndIncrValid != 'True') {
            alert(isMinAndIncrValid);
            currentGrid.edit(gridItem);
            gridItem.SetValue(qtyColumn, oldQty);
            return;
        }

        if (changedQty > 0) // Increased Qty here
        {
            var inventoryCheckingMsg = ValidateInventory(skuIds, changedQty, currentQty, 'Edit');    //Todo: we need to change the Datatype of ChangedQty

            var orderItemId = gridItem.getMember(idColumn).get_value();

            if (inventoryCheckingMsg != 'True') {
                alert(inventoryCheckingMsg);
                currentGrid.edit(gridItem);
                gridItem.SetValue(qtyColumn, oldQty);
                return;
            }
            inventoryCheckingMsg = ValidateInventoryForBundle(orderItemId, currentQty);
            if (inventoryCheckingMsg != 'True') {
                alert(inventoryCheckingMsg);
                currentGrid.edit(gridItem);
                gridItem.SetValue(qtyColumn, oldQty);
                return;
            }
        }
        var thisRowIndex = gridItem.getMember(idColumn).get_value();
        currentGrid.set_callbackParameter('Update:' + thisRowIndex + ':' + gridItem.getMember(qtyColumn).get_value());
        //Added timeout for javascript component art error
        setTimeout('currentGrid.callback();', 0);

    }

    //TextBox
    // set value method for setExpression
    function setValue(control, DataField) {
        
        
        var value = gridItem.GetMember(DataField).Value;
        var txtControl = document.getElementById(control);
        if (value != '' && txtControl != null && value != null) {
            value = value + '';
            txtControl.value = value.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
            txtControl.focus();
            if (txtControl.value == null || txtControl.value == 'null')
                txtControl.value = '';
                
        }
    }
    // get value method for setExpression
    function getValue(control, DataField) {

        var PageName;
        var txtControl = document.getElementById(control);
        if (txtControl != null) {
            PageName = Trim(txtControl.value.replace(/</g, '&lt;').replace(/>/g, '&gt;'));
        }
        gridItem.GetMember(DataField).Value = PageName;
        return [PageName, PageName];
    }

    function GenerateTitle(DataItem) {
        var itemTitle = DataItem.GetMember('Title').get_text();
        var itemParentOrderItemId = DataItem.GetMember('ParentOrderItemId').get_text();
        var itemId = DataItem.GetMember('Id').get_text();
        var itemProductTitle = DataItem.GetMember('ProductName').get_text();
        var itemBackOrderAvailabilityDate = DataItem.GetMember('BackOrderAvailabilityDate').get_text();
        itemTitle = itemProductTitle + "<br>" + itemTitle;
         var bundleSKUCount = DataItem.GetMember('BundleProductSKUCount').get_text();
        if(itemBackOrderAvailabilityDate != "")
        {
            itemTitle = itemTitle + "<br>" + itemBackOrderAvailabilityDate;
        }

        if (itemTitle == "") {
            return "&nbsp;";
        }
        else {
            if (itemParentOrderItemId == emptyGuid) {
                return itemTitle
            }
             if(bundleSKUCount!="0" && bundleSKUCount!="1"){
                var returnStr = "";
                returnStr += "&nbsp;&nbsp;&nbsp;&nbsp;";
                returnStr += itemTitle;
                returnStr += "&nbsp;<a onclick='javascript:ShowBundleSkuSelectPopup(\"";
                returnStr += itemId;
                returnStr += "\");'>" + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, SelectSKU %>' />" + "</a>";

                return returnStr;
            }
            else
            return itemTitle;
        }
    }

    /*Parse number to currency format:
    By Website Abstraction (www.wsabstract.com)
    and Java-scripts.net (www.java-scripts.net)
    */

    //Remove the $ sign if you wish the parse number to NOT include it
    var prefix = "$"
    var wd
    function ParseValue(thisone) {
        if (thisone.charAt(0) == "$")
            return thisone;
        wd = "w"
        var tempnum = thisone
        for (i = 0; i < tempnum.length; i++) {
            if (tempnum.charAt(i) == ".") {
                wd = "d"
                break
            }
        }
        if (wd == "w")
            thisone = prefix + tempnum + ".00"
        else {
            if (tempnum.charAt(tempnum.length - 2) == ".") {
                thisone = prefix + tempnum + "0"
            }
            else {
                tempnum = Math.round(tempnum * 100) / 100
                thisone = prefix + tempnum
                if (thisone.charAt(thisone.length - 2) == ".") //only one zero at the end
                {
                    thisone = thisone + "0"
                }
            }
        }
        return thisone;
    }
    

    function GeneratePersonalizedAttributeButton(itemId,strThisCtlId,IsbundleItem )
    {
    
        //var strThisCtlId = '<%=this.ClientID %>';
        if(IsbundleItem.toString().toLowerCase() == 'true')
        return ;
        
        var imgStr = "<img src=\"/iapps_images/cm-icon-edit.png\" alt=\"" + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, EditPersonalizedAttribute %>' />" + "\" title=\"" + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, EditPersonalizedAttribute %>' />\"";
        imgStr += " onclick=\"return OpenEditPersonalizedAttributePopup" + strThisCtlId  + "('" + itemId + "','" + strThisCtlId + "');\"";
        imgStr += "/>";
        
        return imgStr;    
        
    }
       
       function OpenEditPersonalizedAttributePopup<%=grdShipment.ClientID%>(itemId, thisGrid)
         {
                currentGrid = eval(thisGrid);
                //var theGrid = document.getElementById(gridId);
                var row = <%=grdShipment.ClientID%>.GetRowFromClientId(itemId);

                if(row == null) return ;
                
                
                var orderItemId = row.getMember(idColumn).get_text();
                var status = row.getMember(statusColumn).get_text();
                
                //if(status =='Shipped') 
                //{
                //        alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PersonalizedAttributeCannotBeModifiedForAShippedOrderItem %>' />");
                //        return ;
                //}
  
                if(orderItemId !='' && orderItemId !=undefined && orderItemId!='00000000-0000-0000-0000-000000000000')
                {
                        var isBundleChild = CheckIsBundleChild(orderItemId);
                        if (isBundleChild == 'True') {
                        
                            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ItemIsAChildOfABundlePersonalizedAttributeOfThisOrderItemCannotBeModified %>' />");
                            return ;
                        }
                }
                
                
               
                    
                var orderItemId = row.getMember(idColumn).get_text();
                var skuId = row.getMember(skuIdColumn).get_text();

                
                var orderIdparams ;
                var customerId;
                
                if(OrderId =='' || OrderId ==undefined || OrderId=='00000000-0000-0000-0000-000000000000')
                orderIdparams = GetQueryString('OrderId');
                else 
                orderIdparams=OrderId;
                
                
                
                if(SelectedCustomerId =='' || SelectedCustomerId ==undefined || SelectedCustomerId=='00000000-0000-0000-0000-000000000000')
                customerId = GetQueryString('CustomerId');
                else 
                customerId = SelectedCustomerId;

                if(orderItemId  !=null && orderItemId!='' && orderItemId!='00000000-0000-0000-0000-000000000000')
                {
                    var popupPath = "../../Popups/StoreManager/Orders/PersonalizedSKUAttribute.aspx?SKUId=" + skuId + "&orderItemId=" + orderItemId + "&customerId=" + customerId + "&orderId=" + orderIdparams ;
                    EditPersonalizedAttribute = dhtmlmodal.open('EditPersonalizedAttribute', 'iframe', popupPath, '', 'width=750px,height=550px,center=1,resize=0,scrolling=1');
                    EditPersonalizedAttribute.onclose = function() {
                    var a = document.getElementById('EditPersonalizedAttribute');
                    var ifr = a.getElementsByTagName("iframe");
                    window.frames[ifr[0].name].location.replace("/blank.html");
                    return true;
                    }
                }
                return false ;                
         }
       
    function GetQueryString( name ){  
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]"); 
        var regexS = "[\\?&]"+name+"=([^&#]*)";  
        var regex = new RegExp( regexS );  
        var results = regex.exec( window.location.href ); 

        if( results == null )   
        return ""; 
            else   
        return results[1];
    
   
    }
    function ClosePopupPersonalizedAttribute() {
        EditPersonalizedAttribute.hide();
    }



    function ClosePopupPersonalizedAttributeWithPriceUpdate(orderItemId, price) {
        EditPersonalizedAttribute.hide();
        currentGrid.set_callbackParameter('UpdatePrice:' + orderItemId + ':' + price);
        //Added timeout for javascript component art error
        setTimeout('currentGrid.callback();', 0);
    }

    function fn_ShowFilename(objDataItem) {
        var v_iconURL = jAdminSiteUrl + objDataItem.getMember('FileIcon').get_text().replace('\\', '/');
        v_iconURL = v_iconURL.replace('~','');
        var v_HTML = '<div class="downloadable-media"><img src="' + v_iconURL + '" alt="" class="filetype" />';
        v_HTML += '<span>' + objDataItem.getMember('FileName').get_text() + '</span>';
        v_HTML +='<span>(' + objDataItem.getMember('FileSize').get_text() + ')</span></div>';
        return v_HTML;
    }
    function grdShipment_onCallbackError(sender, eventArgs)
    {
        alert("Grid Call back error:" + eventArgs.errorMessage);
    }

  
</script>
<div class="shipment-box">
    <div class="shipment-utility">
        <label>
            <strong><%= GUIStrings.Orders_ShippingAddress1 %></strong></label>
        <asp:DropDownList ID="ddlShippingAddress" runat="server" Width="204" CssClass="selectbox">
        </asp:DropDownList>
        <asp:Label ID="lblShippingAddress" runat="server" Text=""></asp:Label>
        <asp:Button ID="btnDeleteShipment"  runat="server" Text="<%$ Resources:GUIStrings, DeleteDestination %>" CssClass="small-button" Style="float:right;"/>
        <asp:HiddenField ID="hdnWaitingAck" runat="server" />
        <div class="clearFix">
        </div>
        <strong>
            <asp:Literal ID="ltBackOrderShiiping" runat="server" Text="<%$ Resources:GUIStrings, BackOrderShipping %>"></asp:Literal>
        </strong>
    </div>
    <div style="width: 100%;">
        <ComponentArt:Grid ID="grdShipment" SkinID="Default" runat="server" RunningMode="Callback"
            AllowEditing="true" AutoCallBackOnInsert="false" AutoCallBackOnUpdate="false"
            FillContainer="true" CallbackCacheLookAhead="10" CallbackCachingEnabled="true"
            PageSize="1000" AllowPaging="false" ShowFooter="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
            AllowMultipleSelect="false" EditOnClickSelectedItem="false" OnBeforeCallback="grdShipment_OnBeforeCallback">
            <ClientEvents>
                <ItemSelect EventHandler="grdShipment_ItemSelect" />
                <ItemBeforeSelect EventHandler="grdShipment_ItemSelect" />
                <CallbackError EventHandler="grdShipment_onCallbackError" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="Id"
                    DataMember="OrderItem" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" AlternatingRowCssClass="alternate-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="SequenceNo" AllowEditing="False" HeadingText="<%$ Resources:GUIStrings, SNo %>"
                            Width="25" Visible="false" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            EditControlType="Custom" EditCellServerTemplateId="svtQty" HeadingText="<%$ Resources:GUIStrings, Qty %>"
                            DataField="Qty" Align="Right" DataCellClientTemplateId="QtyHoverTemplate" FormatString="N0"
                            Width="30"  />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            HeadingText="<%$ Resources:GUIStrings, ProductNameSKU %>" DataField="Title"
                            Align="left" EditControlType="Custom" DataCellClientTemplateId="ProductNameHoverTemplate"
                            Width="230" TextWrap="true" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            FormatString="c" HeadingText="<%$ Resources:GUIStrings, Price %>" DataField="Price"
                            Align="Right" EditControlType="Custom" DataCellClientTemplateId="PriceHoverTemplate"
                            Width="70" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            FormatString="c" HeadingText="<%$ Resources:GUIStrings, Discount %>" DataField="Discount"
                            Align="Right" EditControlType="Custom" Visible="true" DataCellClientTemplateId="DiscountHoverTemplate"
                            Width="70" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            FormatString="c" HeadingText="<%$ Resources:GUIStrings, ItemTotal %>" DataField="ItemTotal"
                            Align="Right" EditControlType="Custom" DataCellClientTemplateId="ItemTotalHoverTemplate"
                            Width="70" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            EditControlType="Custom" HeadingText="<%$ Resources:GUIStrings, ShippedQty %>"
                            FormatString="N0" DataField="ShippedQty" Align="Right" DataCellClientTemplateId="ShippedQtyHoverTemplate"
                            Width="80" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            HeadingText="<%$ Resources:GUIStrings, Status %>" DataField="OrderItemStatus"
                            Align="left" EditControlType="Custom" DataCellClientTemplateId="StatusHoverTemplate"
                            Width="80" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                            HeadingText="<%$ Resources:GUIStrings, Personalize %>" DataField="" Align="Center"
                            EditControlType="Custom" DataCellClientTemplateId="PersonalizedAttributeTemplate"
                            Width="70" AllowSorting="False" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                            AllowSorting="False" HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left"
                            DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell"
                            EditCellServerTemplateId="svtActions" EditControlType="Custom" Width="50" />
                        <ComponentArt:GridColumn DataField="SkuId" Visible="false" />
                        <ComponentArt:GridColumn DataField="ShippingAddressId" Visible="false" />
                        <ComponentArt:GridColumn DataField="ShippingMethodId" Visible="false" />
                        <ComponentArt:GridColumn DataField="lblShipmentSubtotalId" Visible="false" />
                        <ComponentArt:GridColumn DataField="lblTaxId" Visible="false" />
                        <ComponentArt:GridColumn DataField="lblShippingId" Visible="false" />
                        <ComponentArt:GridColumn DataField="lblSubtotalId" Visible="false" />
                        <ComponentArt:GridColumn DataField="ShippingTaxAmount" Visible="false" />
                        <ComponentArt:GridColumn DataField="ShippingChargeAmount" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsFreeShipping" Visible="false" />
                        <ComponentArt:GridColumn DataField="ParentOrderItemId" Visible="false" />
                        <ComponentArt:GridColumn DataField="ProductName" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsBundle" Visible="false" />
                        <ComponentArt:GridColumn DataField="OrderId" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsDownloadableMedia" Visible="false" />
                        <ComponentArt:GridColumn DataField="OrderShippingId" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsBackOrderShipping" Visible="false" />
                        <ComponentArt:GridColumn DataField="BackOrderAvailabilityDate" Visible="false" />
                        <ComponentArt:GridColumn DataField="BundleProductSKUCount" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="Id"
                    DataMember="OrderItemMedia" RowCssClass="row" DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell"
                    HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text"
                    SelectedRowCssClass="selected-row" ShowHeadingCells="false" AllowSorting="false"
                    AllowReordering="false" AllowGrouping="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn IsSearchable="false" DataField="FileIcon" Visible="false" />
                        <ComponentArt:GridColumn IsSearchable="true" DataField="Title" Visible="false" />
                        <ComponentArt:GridColumn AllowReordering="false" IsSearchable="true" FixedWidth="true"
                            DataField="FileName" Align="left" DataCellClientTemplateId="MediaFileNameHoverTemplate"
                            Width="800" DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell" />
                        <ComponentArt:GridColumn IsSearchable="true" DataField="FileSize" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="MediaTypeHoverTemplate">
                    <div style="text-align: center;">
                        ## if (DataItem.GetMember("FileIcon").Value == null || DataItem.GetMember("FileIcon").Value
                        == "") { ""; } else { '<img src="" alt="" />'; } ##
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="MediaFileNameHoverTemplate">
                    ## fn_ShowFilename(DataItem) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="QtyHoverTemplate">
                    <div title="## DataItem.GetMember('Qty').get_text() ##">
                        ## DataItem.GetMember('Qty').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Qty').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ShippedQtyHoverTemplate">
                    <div title="## DataItem.GetMember('ShippedQty').get_text() ##">
                        ## DataItem.GetMember('ShippedQty').get_text() == "" ? "&nbsp;" : DataItem.GetMember('ShippedQty').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ProductNameHoverTemplate">
                    <div title="## DataItem.GetMember('Title').get_text() ##">
                        ## GenerateTitle(DataItem) ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PriceHoverTemplate">
                    <div title="## DataItem.GetMember('Price').get_text() ##">
                        ## DataItem.GetMember('ParentOrderItemId').get_text() != '00000000-0000-0000-0000-000000000000'
                        && (DataItem.GetMember('Price').get_text() == "" || DataItem.GetMember('Price').get_text()
                        == "$0.00") ? "&nbsp;" : DataItem.GetMember('Price').get_text() ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="DiscountHoverTemplate">
                    <div title="## DataItem.GetMember('Discount').get_text() ##">
                        ## DataItem.GetMember('Discount').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Discount').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ItemTotalHoverTemplate">
                    <div title="## DataItem.GetMember('ItemTotal').get_text() ##">
                        ## DataItem.GetMember('ParentOrderItemId').get_text() != '00000000-0000-0000-0000-000000000000'
                        && (DataItem.GetMember('ItemTotal').get_text() == "" || DataItem.GetMember('ItemTotal').get_text()
                        == "$0.00") ? "&nbsp;" : DataItem.GetMember('ItemTotal').get_text() ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                    <div title="## DataItem.GetMember('OrderItemStatus').get_text() ##">
                        ## DataItem.GetMember('OrderItemStatus').get_text() == "" ? "&nbsp;" : DataItem.GetMember('OrderItemStatus').get_text()
                        ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PersonalizedAttributeTemplate">
                    ##GeneratePersonalizedAttributeButton(DataItem.ClientId,DataItem.get_table().Grid.get_id(),DataItem.GetMember('IsBundle').get_text())##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ShipmentSliderTemplate">
                    <div class="SliderPopup">
                        <p>
                            ##_datanotload##</p>
                        <p class="paging">
                            <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdShipment.PageCount)##</span>
                            <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdShipment.RecordCount)##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ShipmentSliderTemplateCached">
                    <div class="SliderPopup">
                        <h5>
                            ## DataItem.GetMemberAt(0).Value ##</h5>
                        <p>
                            ## DataItem.GetMemberAt(1).Value ##</p>
                        <p class="paging">
                            <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdShipment.PageCount)##</span>
                            <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdShipment.RecordCount)##</span>
                        </p>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ShipmentPaginationTemplate">
                    ##stringformat(Page0Of12items, currentPageIndex(grdShipment), pageCount(grdShipment),
                    grdShipment.RecordCount)##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ShipmentLoadingPanelTemplate">
                ## GetPageLoadingPanelContent() ##
            </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="svtQty">
                    <Template>
                        <asp:TextBox CssClass="textBoxes" ID="txtQty" MaxLength="8" runat="server" Width="45" >
                        </asp:TextBox>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="svtActions">
                    <Template>
                        <table>
                            <tr>
                                <td>
                                    <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                        runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11"
                                        onclick="SaveRecord();return false;" />
                                </td>
                                <td>
                                    <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                        src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked();" />
                                </td>
                            </tr>
                        </table>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <%--<ComponentArt:GridServerTemplate ID="svtActionShowReturns">
                    <Template>
                        <table>
                            <tr>
                                <td>
                                    <asp:HyperLink ID="hpReturns" runat="server" Visible="false" ImageUrl="~/images/returns.jpg"
                                        NavigateUrl="OrderReturns.aspx"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </Template>
                </ComponentArt:GridServerTemplate>--%>
            </ServerTemplates>
        </ComponentArt:Grid>
    </div>
    <ComponentArt:CallBack ID="cbDestinationSummary" OnCallback="DestinationSummary_Callback"
        runat="server">
        <Content>
            <div class="shipping-section">
                <asp:Button ID="btnViewShipment" runat="server" Text="<%$ Resources:GUIStrings, ViewShipments %>"
                    ToolTip="<%$ Resources:GUIStrings, ViewShipments %>" CssClass="button"
                    UseSubmitBehavior="false" />
                <asp:Button ID="btnShip" runat="server" Text="<%$ Resources:GUIStrings, ShipOrder %>"
                    ToolTip="<%$ Resources:GUIStrings, ShipOrder %>" CssClass="button"
                    UseSubmitBehavior="false" />
                <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, CancelOrder %>"
                    ToolTip="<%$ Resources:GUIStrings, CancelOrder %>" CssClass="button"
                    UseSubmitBehavior="true" OnClick="btnCancel_OnClick" />
            </div>
            <div class="payment-summary">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DestinationSubtotal %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblShipmentSubtotal" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Tax1 %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblTax" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, ShippingHandling %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblShipping" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ShippingDiscount %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblShippingDiscount" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, OrderDiscount %>" /></label>
                    <div class="form-value">
                        <asp:Label ID="lblDiscount" runat="server" Text="$0.00" CssClass="formValue"></asp:Label>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="lblSelected" runat="server" Text="<%$ Resources:GUIStrings, Selected %>" /><br />
                        <asp:HyperLink ID="lnkSelectShippingMethod" runat="server" Text="<%$ Resources:GUIStrings,  SelectShippingMethod%>"></asp:HyperLink></label>
                    <div class="form-value">
                        <asp:Label ID="lblSelShippingMethod" runat="server"></asp:Label><br />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <strong>
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Subtotal %>" /></strong></label>
                    <div class="form-value">
                        <asp:Label ID="lblSubtotal" runat="server" Text="$0.00" Font-Bold="true" CssClass="formValue"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="clearFix">
            </div>
            <asp:Label ID="lblAdditionalPayment" Visible="false" Text="<%$ Resources:GUIStrings, AdditionalPaymentIsRequiredToShipOrder %>"
                runat="server" CssClass="hilightBlock" />
            <asp:Label ID="lblWaitingAcknowledgement" Visible="false" Text="<%$ Resources:GUIStrings, WaitingWarehouseAcknowledgement %>"
                runat="server" CssClass="hilightBlock" />
             <asp:Label ID="lblWaitingAcknowledgementCancel" Visible="false" Text="<%$ Resources:GUIStrings, WaitingWarehouseAcknowledgementCancel %>"
                runat="server" CssClass="hilightBlock" />
        </Content>
        <ClientEvents>
            <CallbackError EventHandler="DestSummary_onCallbackError" />
        </ClientEvents>
    </ComponentArt:CallBack>
</div>
