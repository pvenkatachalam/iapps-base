﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderListing.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.OrderListing" %>
<%@ Register Src="~/UserControls/General/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<script type="text/javascript">
    var CustomerId;
    var OrderId;
    var OrderIdColumnId=0
    var customerIdColumnId=1;
    var orderStatusColumnId=2;
    var OrderListItem;
    var orderStatus;
    var menuIdscolumnId =3;

    function mnuOrders_onLoad(sender,eventArgs){
    }

    function grdOrderListing_onContextMenu(sender, eventArgs) {
        var evt = eventArgs.get_event();
        OrderListItem = eventArgs.get_item();
        OrderId = OrderListItem.getMember(OrderIdColumnId).get_text();
        menuItems = mnuOrders.get_items();
        if (OrderId =='' || OrderId == emptyGuid ) //only add
        {
        }
        else
        {
            var menuIds = OrderListItem.getMember(menuIdscolumnId).get_text();
            var menuArray = menuIds.split(',');
            //  menuIds =',' + menuIds + ',';
            var hideNext=false;
            for(i=8;i<menuItems.get_length();i++)
            {
                var value =menuItems.getItem(i).get_value();
                //  menuItems.getItem(i).set_visible(true);
                if(value!='')
                {
                    //   value = ',' +value +',';
                    if(!contains(menuArray,value))//(menuIds.indexOf(value)==-1) //(!contains(menuArray,value))
                    {
                        menuItems.getItem(i).set_visible(false);
                        hideNext =true;
                    }
                    else
                    {
                        menuItems.getItem(i).set_visible(true);
                    }
                }
                //                else if(hideNext)
                //                {
                //                    menuItems.getItem(i).set_visible(false);
                //                    hideNext =false;
                //                }
            }
        }
        mnuOrders.showContextMenuAtEvent(evt);
    }
    function mnuOrders_onItemSelect(sender, eventArgs) {
        CustomerId = OrderListItem.getMember(customerIdColumnId).get_text();
        var value = eventArgs.get_item().get_value();
        if(value!='')
            ShowReasonPopup(value);
        else
        {
            var menuItem = eventArgs.get_item();
            var selectedMenuId = menuItem.get_id();

            var orderURL = '<%=ResolveUrl("~/StoreManager/Orders/NewOrders.aspx") %>' ;
         var customerURL ='<%=ResolveUrl("~/StoreManager/Customers/CustomerDetails.aspx") %>'
            if(selectedMenuId =='cmEdit')
            {
                ClearOrderRelatedSessions();
                window.location = orderURL + '?OrderId=' + OrderId + '&CustomerId=' + CustomerId;
            }
            else if(selectedMenuId == 'cmNotes')
            {
                window.location = '../../StoreManager/Orders/OrderNotes.aspx?OrderId=' + OrderId;
            }
            else if(selectedMenuId =='cmAdd')
            {
                ClearOrderRelatedSessions();
                var queryCustomerId = getQueryVariable('CustomerId');
                //if (CustomerId!='' && CustomerId!=emptyGuid)
                if(queryCustomerId != undefined)
                {
                    var isShippingAddressIsThere = CheckShippingAddressExist(queryCustomerId);
                    if (isShippingAddressIsThere != 'True' )
                    {
                        alert(isShippingAddressIsThere);
                        window.location = '../../StoreManager/Customers/Shipping.aspx?ReturnUrl=../../StoreManager/Orders/NewOrders.aspx&OrderId=00000000-0000-0000-0000-000000000000&CustomerId=' +queryCustomerId;
                        return;
                    }
                    window.location = orderURL + '?OrderId=' + emptyGuid + '&CustomerId=' + queryCustomerId;
                }
                else
                {
                    window.location = orderURL + '?OrderId=' + emptyGuid;
                }
            }
            else if(selectedMenuId=='cmCustomer')
                window.location = customerURL + '?CustomerId=' + CustomerId;
        }
    }
    function ShowReasonPopup(status) {
        orderStatus = OrderListItem.getMember(orderStatusColumnId).get_text();
        popupPath = '../../Popups/StoreManager/Orders/StatusChangeReason.aspx?Status='+ status +'&PrevStatus=' + orderStatus + '&OrderId=' + OrderId;
        reasonPopup = dhtmlmodal.open('ShowReasonPopup', 'iframe', popupPath, 'ShowReasonPopup', 'width=405px,height=330px,center=1,resize=0,scrolling=1');
        reasonPopup.onclose = function() {
            var a = document.getElementById('ShowReasonPopup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function HideReasonPopup()
    {
        reasonPopup.hide();
        <%= grdOrderListing.ClientID %>.callback();
    }

    function grdOrderListing_onPageIndexChange(sender, eventArgs)
    {
        grdOrderListing.set_callbackParameter(document.getElementById("<%=txtOrderSearch.ClientID%>").value);
    }

    function grdOrderListing_onSortChange()
    {
        grdOrderListing.set_callbackParameter(document.getElementById("<%=txtOrderSearch.ClientID%>").value);
    }

    function contains(a, obj) {
        var inc = a.length;
        while (inc--) {
            if (a[inc] === obj) {
                return true;
            }
        }
        return false;
    }

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
    }

    function savePrintCheckbox(itemId, checkBox){
        var row = grdOrderListing.GetRowFromClientId(itemId);
        UpdatePrintOrderIds(row.GetMember(0).Value, checkBox.checked);
        SetCheckAll();
        return true;
    }

    function CheckPickSelected(columnField){
        var checked = GetPrintOrderIdsCount() > 0;

        if (!checked)
            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouHaveNotSelectedAnyOrder %>" />');
        else
        {
            CheckAllItems(false, false)
            window.open("../../StoreManager/Orders/PickTicket.aspx" );
        }

        return false;
    }

    function CheckAllItems(wasChecked, updateSession){
        var grid = grdOrderListing;
        var gridItem;
        var itemIndex = 0;
        var columnNumber = 17;

        while(gridItem = grid.get_table().getRow(itemIndex)){
            if (updateSession)
                UpdatePrintOrderIds(gridItem.GetMember(0).Value, wasChecked);

            if (wasChecked)
                $("input#chkItem_" + itemIndex).attr("checked", "checked");
            else
                $("input#chkItem_" + itemIndex).removeAttr("checked");

            itemIndex++;
        }

        return false;
    }

    function SetCheckAll() {
        var rowCount = grdOrderListing.Table.getRowCount();
        var selectedCount = 0;

        for (var i = 0; i < rowCount; i++) {
            var itemCheckbox = $("input#chkItem_" + i);

            if (itemCheckbox.is(":checked")) {
                selectedCount++;
            }
        }

        if (selectedCount == rowCount && selectedCount != 0)
            $("input#chkSelectAll").attr("checked", "checked");
        else
            $("input#chkSelectAll").removeAttr("checked");
    }
</script>
<div class="grid-utility">
    <div class="rows">
        <div class="columns">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, KeywordFilter %>" /></label>
            <asp:TextBox type="text" ID="txtOrderSearch" CssClass="textBoxes" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
                Width="175" runat="server" />
            <asp:RegularExpressionValidator runat="server" ID="revtxtOrderSearch" ControlToValidate="txtOrderSearch"
                ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:GUIStrings, InvalidCharactersInSearch %>"
                Text="<%$ Resources:GUIStrings, InvalidCharactersInSearch %>"></asp:RegularExpressionValidator>
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderDate %>" /></label>
            <uc1:DateRange ID="ucDateRange" runat="server" />
            <ComponentArt:Calendar ID="pickerFrom" runat="server" ControlType="Picker" PickerFormat="Custom" PickerCustomFormat="h:mm:ss TT">
            </ComponentArt:Calendar>
            <ComponentArt:Calendar ID="pickerTo" runat="server" ControlType="Picker" PickerFormat="Custom" PickerCustomFormat="h:mm:ss TT">
            </ComponentArt:Calendar>
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderStatus %>" /></label>
            <asp:ListBox ID="lstOrderStatus" runat="server" SelectionMode="Multiple" Rows="3" />
        </div>
        <div class="columns">
            <label class="form-label">
                &nbsp;</label>
            <asp:Button ID="btnFilter" runat="server" Text="<%$ Resources:GUIStrings, Filter %>"
                ToolTip="<%$ Resources:GUIStrings, FilterOrders %>" CssClass="small-button"
                OnClick="btnFilter_Click" />
            <asp:Button ID="btnClear" runat="server" Text="<%$ Resources:GUIStrings, Clear %>"
                ToolTip="<%$ Resources:GUIStrings, ClearFilter %>" CssClass="small-button"
                OnClick="btnClear_Click" Visible="false" />
        </div>
        <div class="columns columns-right">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderTotal %>" /></label>
            <asp:Label ID="lblOrderTotal" runat="server"></asp:Label>
        </div>
        <div class="clear-fix">
        </div>
    </div>
    <asp:PlaceHolder ID="phPickOrder" runat="server">
        <div class="rows clear-fix">
            <div class="columns">
                <input type="checkbox" id="chkSelectAll" onclick="CheckAllItems(this.checked, true)" />
                <asp:Button ID="btnPrintOrder" runat="server" Text="<%$ Resources:GUIStrings, PickTicket %>"
                    ToolTip="<%$ Resources:GUIStrings, PickTicket %>" OnClientClick="return CheckPickSelected('PickOrder');"
                    CssClass="small-button" />
            </div>
            <div class="columns" style="float: right;">
                <asp:LinkButton ID="lbtnExport" runat="server" Text="Export Orders" CssClass="export" style="background-color: transparent;" />
            </div>
        </div>
    </asp:PlaceHolder>
</div>
<ComponentArt:Grid SkinID="Default" ID="grdOrderListing" SliderPopupClientTemplateId="OrderListingSliderTemplate"
    SliderPopupCachedClientTemplateId="OrderListingSliderTemplateCached" Width="100%"
    runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" Sort="OrderDate DESC"
    PagerInfoClientTemplateId="OrderListingPaginationTemplate" CallbackCachingEnabled="false"
    SearchOnKeyPress="true" ManualPaging="true" EditOnClickSelectedItem="false"
    LoadingPanelClientTemplateId="OrderListingLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="CustomerId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="OrderStatus" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="MenuIds" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn Width="25" DataField="PickOrder" IsSearchable="false" Align="Center"
                    AllowReordering="false" FixedWidth="true" HeadingCellClientTemplateId="HeaderTemplatePickOrder"
                    DataCellClientTemplateId="PickOrderTemplate" AllowSorting="False" />
                <ComponentArt:GridColumn Width="135" HeadingText="<%$ Resources:GUIStrings, OrderDate1 %>"
                    DataField="OrderDate" IsSearchable="false" Align="Left" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="OrderDateHoverTemplate" />
                <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, OrderNum %>"
                    DataField="PurchaseOrderNumber" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    IsSearchable="true" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="OrderNumberHoverTemplate" />
                <ComponentArt:GridColumn Width="126" HeadingText="<%$ Resources:GUIStrings, CustomerName %>"
                    DataField="CustomerFullName" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                  <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, Email %>"
                    DataField="Email" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="EmailHoverTemplate" />
                <ComponentArt:GridColumn Width="75" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataField="OrderStatus" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="StatusHoverTemplate" />
                <ComponentArt:GridColumn Width="55" HeadingText="<%$ Resources:GUIStrings, TotalQty %>"
                    DataField="NumSkus" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="SkusHoverTemplate"
                    FormatString="N" Visible="false" />
                <ComponentArt:GridColumn Width="60" HeadingText="<%$ Resources:GUIStrings, ItemTotal %>"
                    DataField="OrderTotal" FormatString="c" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="ItemTotalHoverTemplate"
                    Visible="false" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Refund %>"
                    DataField="RefundTotal" FormatString="c" IsSearchable="false" Align="Right"
                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="RefundHoverTemplate" />
                <ComponentArt:GridColumn Width="60" HeadingText="<%$ Resources:GUIStrings, Shipping %>"
                    DataField="TotalShippingCharge" FormatString="c" IsSearchable="false" Align="Right"
                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="ShippingHoverTemplate" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Discount %>"
                    DataField="TotalDiscount" FormatString="c" IsSearchable="false" Align="Right"
                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="DiscountHoverTemplate" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Tax %>"
                    DataField="TotalTax" IsSearchable="true" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="TaxHoverTemplate"
                    FormatString="c" />
                <ComponentArt:GridColumn Width="65" HeadingText="<%$ Resources:GUIStrings, Total %>"
                    DataField="GrandTotal" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="TotalHoverTemplate"
                    FormatString="c" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdOrderListing_onContextMenu" />
        <PageIndexChange EventHandler="grdOrderListing_onPageIndexChange" />
        <SortChange EventHandler="grdOrderListing_onSortChange" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="OrderNumberHoverTemplate">
            <span title="## DataItem.GetMember('PurchaseOrderNumber').get_text() ##">## DataItem.GetMember('PurchaseOrderNumber').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('PurchaseOrderNumber').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="OrderDateHoverTemplate">
            <span title="## DataItem.GetMember('OrderDate').get_text() ##">## DataItem.GetMember('OrderDate').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('OrderDate').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="NameHoverTemplate">
            <span title="## DataItem.GetMember('CustomerFullName').get_text() ##">## DataItem.GetMember('CustomerFullName').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('CustomerFullName').get_text() ##</span>
        </ComponentArt:ClientTemplate>
         <ComponentArt:ClientTemplate ID="EmailHoverTemplate">
            <span title="## DataItem.GetMember('Email').get_text() ##">## DataItem.GetMember('Email').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Email').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TaxHoverTemplate">
            <span title="## DataItem.GetMember('TotalTax').get_text() ##">## DataItem.GetMember('TotalTax').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('TotalTax').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
            <span title="## DataItem.GetMember('OrderStatus').get_text() ##">## DataItem.GetMember('OrderStatus').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('OrderStatus').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkusHoverTemplate">
            <span title="## DataItem.GetMember('NumSkus').get_text() ##">## DataItem.GetMember('NumSkus').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('NumSkus').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ItemTotalHoverTemplate">
            <span title="## DataItem.GetMember('OrderTotal').get_text() ##">## DataItem.GetMember('OrderTotal').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('OrderTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ShippingHoverTemplate">
            <span title="## DataItem.GetMember('TotalShippingCharge').get_text() ##">## DataItem.GetMember('TotalShippingCharge').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('TotalShippingCharge').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="RefundHoverTemplate">
            <span title="## DataItem.GetMember('RefundTotal').get_text() ##">## DataItem.GetMember('RefundTotal').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('RefundTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DiscountHoverTemplate">
            <span title="## DataItem.GetMember('TotalDiscount').get_text() ##">## DataItem.GetMember('TotalDiscount').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('TotalDiscount').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PickOrderTemplate">
            <input type="checkbox" id="chkItem_## DataItem.get_index() ##" ## DataItem.GetMember('PickOrder').Value ? 'checked' : '' ##
                style= "## grdOrderListing.RecordCount > 0 ? '' : 'display:none'  ##"
                onClick="savePrintCheckbox('## DataItem.ClientId ##', this);" />
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplatePickOrder">
            <span>&nbsp;</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TotalHoverTemplate">
            <span title="## DataItem.GetMember('GrandTotal').get_text() ##">## DataItem.GetMember('GrandTotal').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('GrandTotal').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="OrderListingSliderTemplate">
            <div class="SliderPopup">
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdOrderListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdOrderListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="OrderListingSliderTemplateCached">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##</p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdOrderListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdOrderListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="OrderListingPaginationTemplate">
            ## stringformat(Page0Of12items, currentPageIndex(grdOrderListing), pageCount(grdOrderListing),
            grdOrderListing.RecordCount) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="OrderListingLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdOrderListing) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuOrders" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuOrders_onItemSelect" />
        <Load EventHandler="mnuOrders_onLoad" />
    </ClientEvents>
</ComponentArt:Menu>
<ComponentArt:Menu ID="mnuCustomerOrders" runat="server" SkinID="ContextMenu">
    <Items>
        <ComponentArt:MenuItem ID="cmViewOrder" Text="<%$ Resources:GUIStrings, ViewOrder %>">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem LookId="BreakItem">
        </ComponentArt:MenuItem>
        <ComponentArt:MenuItem ID="cmCustomerNewOrder" Text="<%$ Resources:GUIStrings, PlaceNewOrder %>"
            Look-LeftIconUrl="cm-icon-add.png">
        </ComponentArt:MenuItem>
    </Items>
</ComponentArt:Menu>