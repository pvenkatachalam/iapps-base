﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderItems.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.OrderItems" %>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <thead>
            <tr class="headRow">
                <th align="left">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
                </th>
                <th align="left">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Status %>" />
                </th>
                <th align="right">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Qty %>" />
                </th>
                <th align="right">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YourPrice %>" />
                </th>
                <th align="right">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderTotal %>" />
                </th>
            </tr>
        </thead>
        <tbody class="cartItems">
            <asp:Repeater ID="rptOrderItems" runat="server" OnItemDataBound="rptOrderItems_DataBound">
                <ItemTemplate>
                    <tr class="<%#Container.ItemIndex % 2 == 0 ? "oddRow" : "evenRow" %>">
                        <td align="left" style="max-width: 519px">
                            <asp:Label ID="lblProductSKU" runat="server"></asp:Label><br />
                            <asp:Label ID="lblProductName" runat="server"></asp:Label>

                            <asp:Repeater ID="rptPersonalizedAttributes" runat="server" OnItemDataBound="rptPersonalizedAttributes_ItemDataBound">
                                <HeaderTemplate>
                                    <ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li><asp:Label ID="lblAttribute" runat="server"></asp:Label>
                                        <asp:Label style="font-weight:bold" ID="lblAttributeValue" runat="server"></asp:Label></li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>

                            <asp:Repeater ID="rptBundle" runat="server" OnItemDataBound="rptBundle_DataBound">
                                <HeaderTemplate>
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="right" style="max-width: 519px">
                                            <asp:Label ID="lblBundleItemSku" runat="server" Text=""></asp:Label>&nbsp;<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Qty %>" /> (<asp:Label
                                                ID="lblQty" runat="server" Text=""></asp:Label>)<br />
                                            <asp:HyperLink ID="hlProductName" runat="server" Text="" NavigateUrl="#"></asp:HyperLink>
                                            <asp:Repeater ID="rptPersonalizedAttributes" runat="server" OnItemDataBound="rptPersonalizedAttributes_ItemDataBound">
                                                <HeaderTemplate>
                                                    <ul>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li><asp:Label ID="lblAttribute" runat="server"></asp:Label>
                                                        <asp:Label style="font-weight:bold" ID="lblAttributeValue" runat="server"></asp:Label>
                                                    </li>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblItemOrderStatus" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblOrderItemQty" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblYourPrice" runat="server"></asp:Label>&nbsp;
                            <asp:Label ID="lblYourMeasure" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Label ID="lblSubTotal" runat="server"></asp:Label>
                        </td>
                        <td align="right">
                           <asp:HyperLink ID="hpReturns" runat="server" Visible="false" ImageUrl="~/images/returns.jpg" NavigateUrl="OrderReturns.aspx"></asp:HyperLink>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>
