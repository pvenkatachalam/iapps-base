﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrackShipmentPackage.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TrackShipmentPackage" %>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, TrackingURL %>" /></label>
    <div class="form-value">
        <asp:Label ID="lblTrack" runat="server" /><asp:Literal ID="litTermsConditions" runat="server" />
    </div>
</div>
<div class="clear-fix" style="margin-bottom: 10px;">
    <ComponentArt:Grid ID="grdShippedOrder" AllowTextSelection="true" EnableViewState="true"
        EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False" KeyboardEnabled="false"
        ShowFooter="false" SkinID="Default" RunningMode="Client" Width="700" runat="server"
        EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
        <Levels>
            <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" DataKeyField="PostId"
                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" ShowFooterRow="false"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                AllowSorting="false" SortImageHeight="7" AllowGrouping="false" EditFieldCssClass="EditDataField"
                EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
                <Columns>
                    <ComponentArt:GridColumn Visible="false" DataCellServerTemplateId="SVTthumbnail"
                        DataField="OrderShipmentItem.OrderItem.SKU.Id" />
                    <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="90"
                        HeadingText="<%$ Resources:GUIStrings, Thumbnail %>" DataCellClientTemplateId="ThumbnailTemplate"
                        DataField="OrderShipmentItem.ProductImageUrl" />
                    <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="250"
                        HeadingText="<%$ Resources:GUIStrings, ProductName %>" DataCellClientTemplateId="ProductNameHoverTemplate"
                        DataField="OrderShipmentItem.OrderItem.SKU.ParentProduct.Title" TextWrap="true" />
                    <ComponentArt:GridColumn Align="Left" AllowReordering="False" FixedWidth="true" Width="120"
                        HeadingText="<%$ Resources:GUIStrings, SKU %>" DataCellClientTemplateId="SKUHoverTemplate"
                        DataField="OrderShipmentItem.OrderItem.SKU.SKU" TextWrap="true" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, QtyShipped %>"
                        DataField="Quantity" DataCellClientTemplateId="QtyOrderedHoverTemplate" Width="70"
                        AllowReordering="False" FixedWidth="true" Align="Right" />
                    <ComponentArt:GridColumn DataField="PostId" IsSearchable="false" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ThumbnailTemplate">
                <img src="## DataItem.GetMember('OrderShipmentItem.ProductImageUrl').get_text() ##"
                    alt="## DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.ParentProduct.Title').get_text() ##" />
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ProductNameHoverTemplate">
                <div title="## DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.ParentProduct.Title').get_text() ##">
                    ## DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.ParentProduct.Title').get_text()
                    == "" ? "&nbsp;" : DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.ParentProduct.Title').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SKUHoverTemplate">
                <div title="## DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.SKU').get_text() ##">
                    ## DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.SKU').get_text() == "" ?
                    "&nbsp;" : DataItem.GetMember('OrderShipmentItem.OrderItem.SKU.SKU').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="QtyOrderedHoverTemplate">
                <div title="## DataItem.GetMember('Quantity').get_text() ##">
                    ## DataItem.GetMember('Quantity').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Quantity').get_text()
                    ##</div>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="SVTthumbnail">
                <Template>
                    <asp:Image ID="ImgThumbNail" runat="server" ImageUrl="~/App_Themes/General/images/no-image.gif"
                        AlternateText="<%$ Resources:GUIStrings, NoImageFound %>" />
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </ComponentArt:Grid>
</div>
