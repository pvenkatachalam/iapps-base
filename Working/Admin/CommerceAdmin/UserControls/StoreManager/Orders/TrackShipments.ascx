﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrackShipments.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.TrackShipments" %>
<%@ Register TagPrefix="OR" TagName="TrackShipmentPk" Src="~/UserControls/StoreManager/Orders/TrackShipmentPackage.ascx" %>
<div class="clear-fix">
    <div class="first-column" id="divShippedTo" runat="server">
        <div class="rows">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ShippedTo %>" /></label>
            <asp:Label ID="lblShippedTo" runat="server" Width="250"></asp:Label>
        </div>
        <div class="rows">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PackagesCount %>" /></label>
            <asp:Label ID="lblPackages" runat="server"></asp:Label>
        </div>
    </div>
    <div class="second-column" id="divDateShipped" runat="server">
        <div class="rows">
            <label class="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, DateShipped %>" /></label>
            <asp:Label ID="lblDateShipped" runat="server"></asp:Label>
        </div>
        <div class="rows">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, DeliveryMethod %>" /></label>
            <asp:Label ID="lblDeliveryMethod" runat="server"></asp:Label>
        </div>
        <div class="rows">
            <label class="form-label">
                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Warehouse %>" /></label>
            <asp:Label ID="lblWarehouse" runat="server"></asp:Label>
        </div>
    </div>
</div>
<asp:Repeater ID="rptTrackingPackages" runat="server" OnItemDataBound="rptTrackingPackages_DataBound">
    <ItemTemplate>
        <OR:TrackShipmentPk ID="ctlTrackShipmentPackage" runat="server"></OR:TrackShipmentPk>
    </ItemTemplate>
</asp:Repeater>
