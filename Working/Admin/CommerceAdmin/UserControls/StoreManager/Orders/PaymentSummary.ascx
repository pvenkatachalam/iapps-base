﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentSummary.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.PaymentSummary" %>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <col align="left" width="90%" />
    <col align="right" width="10%" />
    <tr class="oddRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderTotal1 %>" />
        </td>
        <td align="right">
            <asp:Literal ID="lblOrderTotal" runat="server" Text="$0.00"></asp:Literal>
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Tax1 %>" />
        </td>
        <td align="right">
            <asp:Literal ID="lblTax" runat="server" Text="$0.00"></asp:Literal>
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Shipping1 %>" />
        </td>
        <td align="right">
            <asp:Literal ID="lblShipping" runat="server" Text="$0.00"></asp:Literal>
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CODCharges %>" />
        </td>
        <td align="right">
            <asp:Literal ID="lblCODCharge" runat="server" Text="$0.00"></asp:Literal>
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ShippingDiscounts %>" />
        </td>
        <td align="right">
            <asp:Literal ID="lblShippingDiscounts" runat="server" Text="$0.00"></asp:Literal>
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, OrderDiscounts %>" />
        </td>
        <td align="right">
            <asp:Literal ID="lblTotalDiscounts" runat="server" Text="$0.00"></asp:Literal>
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, GRANDTOTAL %>" />
        </td>
        <td align="right">
            <strong><asp:Literal ID="lblGrandTotal" runat="server" Text="$0.00"></asp:Literal></strong>
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PaymentTotal %>" />
        </td>
        <td align="right">
            <strong><asp:Literal ID="lblPaymentTotal" runat="server" Text="$0.00"></asp:Literal></strong>
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PaymentRemaining %>" />
        </td>
        <td align="right">
            <strong><asp:Literal ID="lblPaymentRemaining" runat="server" Text="$0.00"></asp:Literal></strong>
        </td>
    </tr>
</table>
