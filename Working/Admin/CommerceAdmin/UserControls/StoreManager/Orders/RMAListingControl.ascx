﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RMAListingControl.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Orders.RMAListingControl" %>
<%@ Register Src="../../General/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<div class="rma">
    <div id="dvFilter" runat="server">
        <h1>
            <%= GUIStrings.RMA_Returns %></h1>
        <div class="grid-utility">
            <div class="columns">
                <label class="form-label">
                    <%= GUIStrings.RMA_KeywordFilter %></label>
                <asp:TextBox ID="txtSearch" runat="server" Width="410" CssClass="textBoxes" ToolTip="<%$ Resources:GUIStrings, RMA_SearchHelp %>" />
            </div>
            <div class="columns">
                <label class="form-label">
                    <%= GUIStrings.Status %></label>
                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectbox" Width="145"
                    AppendDataBoundItems="true">
                    <asp:ListItem Text="All" Value="-1" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="columns">
                <label class="form-label">
                    <%= GUIStrings.RMA_DateRange%></label>
                <uc1:DateRange ID="RMADateRange" runat="server" />
            </div>
            <div class="columns">
                <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:GUIStrings, RMA_Filter %>"
                    OnClick="btnSearch_Click" CssClass="small-button" />
            </div>
            <div class="clearFix">
            </div>
        </div>
    </div>
    <asp:ObjectDataSource ID="odsReturns" runat="server" TypeName="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Orders.RMAListingControl"
        MaximumRowsParameterName="pageSize" StartRowIndexParameterName="rowIndex" SelectMethod="GetRMAListing"
        OnSelecting="odsReturns_Selecting" SortParameterName="sortExpression" SelectCountMethod="GetRecordCount" EnablePaging="true"
        OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="lstReturns" Name="sortExpression" PropertyName="SelectedValue"
                Type="String" DefaultValue="RMACode" />
            <asp:ControlParameter ControlID="lstReturns" Name="pageSize" PropertyName="SelectedValue"
                Type="Int32" DefaultValue="10" />
            <asp:ControlParameter ControlID="lstReturns" Name="rowIndex" PropertyName="SelectedIndex"
                Type="Int32" />
            <asp:ControlParameter ControlID="txtSearch" Name="searchKeyword" PropertyName="Text"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ListView ID="lstReturns" runat="server" ItemPlaceholderID="itemContainer" DataKeyNames="Id"
        ClientIDMode="AutoID" DataSourceID="odsReturns" OnItemCommand="lstReturns_ItemCommand">
        <LayoutTemplate>
            <table width="100%" cellspacing="0" cellpadding="0" class="grid-view" id="gridview">
                <thead>
                    <tr>
                        <th>
                            <asp:LinkButton ID="lbtnRMA" runat="server" Text="<%$ Resources:GUIStrings, RMA_Code %>"
                                CommandName="Sort" CommandArgument="RMACode" />
                        </th>
                        <th>
                            <asp:LinkButton ID="lbtnCustomer" runat="server" Text="<%$ Resources:GUIStrings, RMA_Customer %>"
                                CommandName="Sort" CommandArgument="CustomerName" />
                        </th>
                        <th id="thOrderNumber" runat="server">
                            <asp:LinkButton ID="lbtnOrderNo" runat="server" Text="<%$ Resources:GUIStrings, RMA_OrderNumber%>"
                                CommandName="Sort" CommandArgument="PurchaseOrderNumber" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:GUIStrings,RMA_Type %>'></asp:Localize>
                        </th>
                        <th>
                            <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:GUIStrings,Status %>'></asp:Localize>
                        </th>
                        <th>
                            <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:GUIStrings, RMA_CreatedOn %>'></asp:Localize>
                        </th>
                        <th>
                            <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:GUIStrings, RMA_CreatedBy %>'></asp:Localize>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemContainer" runat="server" />
                </tbody>
                <asp:PlaceHolder ID="phFooter" runat="server">
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <asp:DataPager ID="dpReturns" runat="server" PageSize="10">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="false" ShowLastPageButton="false"
                                            NextPageImageUrl="~/App_Themes/General/images/next-page.png" RenderNonBreakingSpacesBetweenControls="false"
                                            PreviousPageImageUrl="~/App_Themes/General/images/previous-page.png" ButtonCssClass="pager-button" />
                                        <asp:TemplatePagerField>
                                            <PagerTemplate>
                                                <div class="pager">
                                                    <%# string.Format(JSMessages.Page0Of1Items, (Container.StartRowIndex / Container.PageSize) + 1, 
                                                    Math.Ceiling((double)Container.TotalRowCount / Container.PageSize), Container.TotalRowCount)%>
                                                </div>
                                            </PagerTemplate>
                                        </asp:TemplatePagerField>
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </tfoot>
                </asp:PlaceHolder>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
                <td>
                    <asp:HyperLink ID="hpRMACode" runat="server" Text='<%# Eval("RMACode").ToString().Substring(0, 5) + "..." %>'
                        ToolTip='<%# Eval("RMACode") %>' NavigateUrl='<%# String.Format("~/StoreManager/Orders/RMAReceiver.aspx?CurrentRMAId={0}", Eval("Id")) %>' />
                </td>
                <td>
                    <asp:Literal ID="litFirstName" runat="server" Text='<%# Eval("CustomerFirstName") %>' />
                    <asp:Literal ID="litLastName" runat="server" Text='<%# Eval("CustomerLastName") %>' />
                </td>
                <td id="tdOrderNumber" runat="server">
                    <asp:HyperLink ID="hplOrder" NavigateUrl='<%# String.Format("~/StoreManager/Orders/NewOrders.aspx?OrderId={0}", Eval("OrderId")) %>'
                        runat="server" Text='<%# Eval("PurchaseOrderNumber") %>' />
                </td>
                <td>
                    <asp:Literal ID="litResolution" runat="server" />
                </td>
                <td id="tdStatus" runat="server">
                    <asp:Literal ID="litStatus" runat="server" />
                </td>
                <td>
                    <asp:Literal ID="litCreatedDate" runat="server" Text='<%# Eval("CreatedDate") %>' />
                </td>
                <td>
                    <asp:Literal ID="litCreatedBy" runat="server" Text='<%# Eval("CreatedByFullName") %>' />
                </td>
                <%--<td>
                        <asp:LinkButton ID="lbtnGenerateSlip" runat="server" CommandName="GenerateSlip" CommandArgument='<%# Eval("Id") %>' Text="Generate Slip" />
                        <asp:HyperLink Id="hplView" runat="server" NavigateUrl='<%# String.Format("RMAReceiver.aspx?CurrentRMAId={0}", Eval("Id")) %>'
                        Text="<%$ Resources:GUIStrings, ViewDetails %>"></asp:HyperLink>
                    </td>--%>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="grid-emptyText">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, GridView_NoItems %>" /></div>
        </EmptyDataTemplate>
    </asp:ListView>
</div>
