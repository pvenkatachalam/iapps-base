﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupMembers.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.GroupMembers" %>
<div class="columns">
    <h4>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AllAvailableCustomers %>" /></h4>
    <div class="grid-utility">
        <input type="text" id="txtAllUsersSearch" onkeydown="javascript:if(event.keyCode == 13) { btnSearch.click(); }" class="textBoxes" title="<%= GUIStrings.TypeHereToFilter %>"
            style="width: 180px;" />
        <input type="button" id="btnSearch" class="small-button" value="<%= GUIStrings.Search %>"
            onclick="onClickSearchonGrid('txtAllUsersSearch', ModifyDeleteAvailableUsers);" />
    </div>
    <ComponentArt:Grid ID="ModifyDeleteAvailableUsers" EditOnClickSelectedItem="false"
        SkinID="ScrollingGrid" AllowEditing="true" AllowMultipleSelect="true" ShowHeader="False"
        PageSize="10" ShowFooter="false" ExternalDropTargets="SelectedUsersGroups" Height="360"
        runat="server" CallbackCachingEnabled="true" CallbackCacheLookAhead="10" RunningMode="callback"
        EmptyGridText="<%$ Resources:GUIStrings, NoUsersFound %>" ItemDraggingEnabled="true"
        LoadingPanelClientTemplateId="ModifyDeleteAvailableLoadingPanelTemplate"
        BorderColor="#d2d2d2" BorderStyle="Solid" BorderWidth="1">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false">
                <Columns>
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Id" IsSearchable="false" AllowEditing="false"
                        Align="left" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="UserName" IsSearchable="false"
                        AllowReordering="False" EditControlType="Default" Visible="false" Width="260" />
                    <ComponentArt:GridColumn FixedWidth="true" AllowSorting="true" IsSearchable="true"
                        HeadingText="<%$ Resources:GUIStrings, FirstName %>" DataField="FirstName"
                        AllowReordering="False" EditControlType="Default" Width="130" DataCellClientTemplateId="ModifyDeleteAvailableUsersFirstNameRowToolTip" />
                    <ComponentArt:GridColumn FixedWidth="true" AllowSorting="true" IsSearchable="true"
                        HeadingText="<%$ Resources:GUIStrings, LastName %>" DataField="LastName" AllowReordering="False"
                        EditControlType="Default" Width="116" DataCellClientTemplateId="ModifyDeleteAvailableUsersLastNameRowToolTip" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemExternalDrop EventHandler="SelectedUsersGroups_onItemExternalDrop" />
            <BeforeCallback EventHandler="CheckSessionEventHandler" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ModifyDeleteAvailableUsersFirstNameRowToolTip">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('FirstName').get_text()!=''?DataItem.getMember('FirstName').get_text():DataItem.getMember('UserName').get_text()) ##">
                    ## DataItem.getMember('FirstName').get_text()!=''?DataItem.getMember('FirstName').get_text():DataItem.getMember('UserName').get_text()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ModifyDeleteAvailableUsersLastNameRowToolTip">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('LastName').get_text()) ##">
                    ## DataItem.getMember('LastName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="ModifyDeleteAvailableLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(ModifyDeleteAvailableUsers) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</div>
<div class="columns" style="width: 30px; margin-top: 150px;">
    <img id="rightArrow" src="~/App_Themes/General/images/wfarrowRight.gif" title="<%$ Resources:GUIStrings, ClickToAdd %>"
        alt="<%$ Resources:GUIStrings, ClickToAdd %>" runat="server" />
    <img id="leftArrow" src="~/App_Themes/General/images/wfArrowLeft.gif" title="<%$ Resources:GUIStrings, ClickToRemove %>"
        alt="<%$ Resources:GUIStrings, ClickToRemove %>" runat="server" />
    <asp:HiddenField runat="server" ID="SelectedTabIndex" Value="0" />
</div>
<style type="text/css">
    #<%= SelectedUsersGroups.ClientID %>_VerticalScrollDiv
    {
        height:328px !important;        
    }
    </style>
<div class="columns">
    <h4>
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SelectedCustomers %>" /></h4>
    <div class="grid-utility">
        &nbsp;
        <input type="text" style="visibility: hidden;" />
    </div>
    <ComponentArt:Grid ID="SelectedUsersGroups" EditOnClickSelectedItem="false" SkinID="ScrollingGrid"
        AllowEditing="true" ShowHeader="False" PageSize="10" ShowFooter="false" runat="server"
        EnableViewState="true" Width="100%" AllowMultipleSelect="true" RunningMode="client"
        CallbackCachingEnabled="true" CallbackCacheLookAhead="10" EmptyGridText="<%$ Resources:GUIStrings, NoUsersGroupsSelected %>"
        AutoCallBackOnUpdate="false" AutoCallBackOnInsert="false" ItemDraggingEnabled="true"
        ExternalDropTargets="ModifyDeleteAvailableUsers" AllowVerticalScrolling="true"
        LoadingPanelClientTemplateId="SelectedUsersGroupsLoadingPanelTemplate" BorderColor="#d2d2d2" BorderStyle="Solid" BorderWidth="1">
        <Levels>
            <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
                DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false">
                <Columns>
                    <ComponentArt:GridColumn FixedWidth="true" AllowEditing="false" Align="left" DataField="MemberId"
                        Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" EditControlType="Default" AllowReordering="False"
                        HeadingText="<%$ Resources:GUIStrings, FirstName %>" DataField="MemberName"
                        Width="165" DataCellClientTemplateId="SelectedUsersGroupsMemberNameRowToolTip" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="MemberType" EditControlType="Default"
                        AllowReordering="False" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="LastName" EditControlType="Default"
                        HeadingText="<%$ Resources:GUIStrings, LastName %>" DataCellCssClass="last-data-cell"
                        HeadingCellCssClass="last-heading-cell" AllowReordering="False" Visible="true"
                        Width="165" DataCellClientTemplateId="SelectedUsersGroupsLastNameRowToolTip" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="UserName" IsSearchable="false"
                        AllowReordering="False" EditControlType="Default" Visible="false" Width="260" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientEvents>
            <ItemExternalDrop EventHandler="SelectedUsersGroups_onItemRemove" />
            <RenderComplete EventHandler="SetVerticalScroll" />
        </ClientEvents>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ScrollPopupTemplate3">
                <div class="sScrollPopup smallScrollPopup">
                    <h5>
                        ## DataItem.GetMember("LastName").Value ##, ## DataItem.GetMember("MemberName").Value
                        ##</h5>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SelectedUsersGroupsMemberNameRowToolTip">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('MemberName').get_text()!=''?DataItem.getMember('MemberName').get_text():DataItem.getMember('UserName').get_text()) ##">
                    ## DataItem.getMember('MemberName').get_text()!=''?DataItem.getMember('MemberName').get_text():DataItem.getMember('UserName').get_text()
                    ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SelectedUsersGroupsLastNameRowToolTip">
                <span title="## ChangeSpecialCharacters(DataItem.getMember('LastName').get_text()) ##">
                    ## DataItem.getMember('LastName').get_text() ##</span>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="SelectedUsersGroupsLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(SelectedUsersGroups) ##
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Grid>
</div>
