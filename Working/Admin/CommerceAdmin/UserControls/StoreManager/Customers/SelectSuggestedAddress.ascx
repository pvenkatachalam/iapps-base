﻿<%@ Control Language="C#" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.SelectSuggestedAddress" CodeBehind="SelectSuggestedAddress.ascx.cs"
    AutoEventWireup="true" %>
<div class="selectShippingHolder">
    <script type="text/javascript">
        function onCloseClick() {
            parent.CloseShippingMethodPopup();
        }
        function UseMyAddressClick() {
            if (parent.hdnIsValidatedClientID != null && parent.hdnIsValidatedClientID != 'undefined') {
                parent.document.getElementById(parent.hdnIsValidatedClientID).value = 1;
            }
            parent.DisplayAddressAndClose("", 1);
        }
    </script>
    <div class="clear-fix">
        <div class="left-column">
            <p><strong><%= GUIStrings.YouEntered%></strong></p>
            <p><asp:Label ID="lblEnteredAddress" runat="server" /></p>
        </div>
        <div class="right-column">
            <div style="text-align: right;">
                <asp:Button ID="btnUseThisAddress" runat="server" OnClientClick="UseMyAddressClick()"
                    Text="<%$ Resources:GUIStrings, UseMyAddress %>" CssClass="button" />
            </div>
        </div>
    </div>
    <hr />
    <div class="clear-fix">
        <div class="left-column">
            <asp:Image ImageUrl="~/images/UPS_LOGO_L.gif" AlternateText="" runat="server" />
        </div>
        <div class="right-column">
            <p><strong><%= GUIStrings.AddressSuggestion %></strong></p>
            <div style="overflow: auto; width: 100%; height: 200px;">
                <asp:RadioButtonList ID="rblSuggestedAddresses" runat="server" Width="100%" CssClass="address-list" />
                <asp:RequiredFieldValidator ID="rfvSuggesstedAddress" ControlToValidate="rblSuggestedAddresses"
                    Display="Dynamic" ErrorMessage="<%$ Resources:GUIStrings, PleaseSelectAnAddress %>" Style="color: Red"
                    runat="server" />
            </div>
        </div>
    </div>
    <hr />
    <p class="notice">
        <em>
            <%= GUIStrings.UPSTrademarkNotice %></em>
    </p>
    <p class="notice">
        <em>
            <%= GUIStrings.UPSAddressValidationNotice %></em>
    </p>
    <p class="notice">
        <em>
            <%= GUIStrings.UPSPOBoxValidationNotice%></em>
    </p>
</div>
