﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReviewListing.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ReviewListing" %>
<script type="text/javascript">
    var editedRowIndex;
    var gridCommentsAction = "";
    var selectedCommentIds = "";
    var callbackMessage = "";

    var titleColumn = 5;
    var ratingColumn = 4;
    var commentsColumn = 6;
    var statusColumn = 7;

    var isSelectAll = false;
    function displayCommentOptions(rowIndex) {
        var commentStatus = gridComments.Table.GetRow(rowIndex).GetMember('Status').get_value();

        var commentActions = [];

        if (commentStatus != 4) {
            commentActions[commentActions.length] = "<a class=\"approve\" href=\"javascript: //\" onclick=\"approveComment('" + rowIndex + "');\">" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Approve %>'/>" + "</a>";
        }
        else {
            commentActions[commentActions.length] = "<a class=\"unapprove\" href=\"javascript: //\" onclick=\"unapproveComment('" + rowIndex + "');\">" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Unapprove %>'/>" + "</a>";
        }

        if (commentStatus != 3) {
            commentActions[commentActions.length] = "<a class=\"delete\" href=\"javascript: //\" onclick=\"deleteComment('" + rowIndex + "');\">" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Delete %>'/>" + "</a>";
        }

        commentActions[commentActions.length] = "<a class=\"edit\" href=\"javascript: //\" onclick=\"displayEditComment('" + rowIndex + "');\">" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Edit %>'/>" + "</a>";

        var commentString = "";

        for (var i = 0; i < commentActions.length; i++) {
            if (i < commentActions.length - 1) {
                commentString += commentActions[i] + " | ";
            }
            else {
                commentString += commentActions[i];
            }
        }

        return commentString;
    }

    function displayCommentStatus(rowIndex) {
        var commentStatus = gridComments.Table.GetRow(rowIndex).GetMember('Status').get_value();

        switch (commentStatus) {
            case 1:
                return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Unapproved %>'/>";
                break;
            case 3:
                return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Deleted %>'/>";
                break;
            case 4:
                return "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Approved %>'/>";
        }
    }

    function displayPartialComment(rowIndex) {
        var maxCharacters = 75;
        var commentId = gridComments.Table.GetRow(rowIndex).GetMember('Id').get_value();
        var commentsString = gridComments.Table.GetRow(rowIndex).GetMember('Comments').get_value();

        setTimeout(function () {
            if (commentsString.length >= maxCharacters) {
                commentsString = commentsString.substring(0, maxCharacters);

                var moreLink = "<a class=\"readMore\" href=\"javascript: //\" onclick=\"displayEntireComment('" + rowIndex + "');\">" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, More %>'/>" + "</a>";

                $("span#" + commentId + "_visibleComments").html(commentsString + " " + moreLink);
            }
            else {
                $("span#" + commentId + "_visibleComments").text(commentsString);
            }
        }, 10);
    }

    function displayEntireComment(rowIndex) {
        var commentsString = gridComments.Table.GetRow(rowIndex).GetMember('Comments').get_value();
        $("div#commentsPreview div.modal-content").html(formatHtml(commentsString));

        $("#commentsPreview").dialog("open");
    }

    function closeCommentsPreview() {
        $("#commentsPreview").dialog("close");
    }

    function displayEditComment(rowIndex) {
        var titleString = gridComments.Table.GetRow(rowIndex).GetMember('CommentTitle').get_value();
        var commentsString = gridComments.Table.GetRow(rowIndex).GetMember('Comments').get_value();
        var ratingsString = gridComments.Table.GetRow(rowIndex).GetMember('RatingValue').get_value();
        $("div#editComments input#txtTitle").val(titleString);
        $("div#editComments input#txtRatingValue").val(ratingsString);
        $("div#editComments textarea#txtComments").val(commentsString);

        $("#editComments").iAppsDialog("open");
        fn_InitializeRating();

        editedRowIndex = rowIndex;
    }

    function closeEditComments() {
        $("div#editComments input#txtTitle").val("");
        $("div#editComments input#txtRatingValue").val("");
        $("div#editComments textarea#txtComments").val("");

        $("#editComments").iAppsDialog("close");

        editedRowIndex = null;
    }

    function displayShortDate(dateString) {
        var date = new Date(dateString);

        var month = date.getMonth();
        if (month < 10) {
            month = "0" + month;
        }

        var day = date.getDate();
        if (day < 10) {
            day = "0" + day;
        }

        var year = date.getFullYear();

        return month + "/" + day + "/" + year;
    }

    function approveComment(rowIndex) {
        var row = gridComments.Table.GetRow(rowIndex);

        gridCommentsAction = "Approve";
        gridComments.set_callbackParameter(getCallbackParameter());
        gridComments.edit(row);

        row.SetValue(statusColumn, 4);

        gridComments.editComplete();
        gridComments.callback();

        gridCommentsAction = "";

        gridComments.set_callbackParameter("");

        callbackMessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Thecommenthasbeensuccessfullyapproved %>'/>";
    }

    function unapproveComment(rowIndex) {
        var row = gridComments.Table.GetRow(rowIndex);

        gridCommentsAction = "Unapprove";
        gridComments.set_callbackParameter(getCallbackParameter());
        gridComments.edit(row);

        row.SetValue(statusColumn, 1);

        gridComments.editComplete();
        gridComments.callback();

        gridCommentsAction = "";

        gridComments.set_callbackParameter("");

        callbackMessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Thecommenthasbeensuccessfullyunapproved %>'/>";
    }

    function deleteComment(rowIndex) {
        var row = gridComments.Table.GetRow(rowIndex);

        gridCommentsAction = "Delete";
        gridComments.set_callbackParameter(getCallbackParameter());
        gridComments.edit(row);

        row.SetValue(statusColumn, 3);

        gridComments.editComplete();
        gridComments.callback();

        gridCommentsAction = "";

        gridComments.set_callbackParameter("");

        callbackMessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Thecommenthasbeensuccessfullydeleted %>'/>";
    }

    function editComment(rowIndex) {
        var row = gridComments.Table.GetRow(rowIndex);

        var updatedTitle = $("div#editComments input#txtTitle").val();
        var updatedRating = $("div#editComments input#txtRatingValue").val();
        var updatedComments = $("div#editComments textarea#txtComments").val();

        gridCommentsAction = "Update"
        gridComments.set_callbackParameter(getCallbackParameter());
        gridComments.edit(row);

        row.SetValue(titleColumn, updatedTitle);
        row.SetValue(ratingColumn, updatedRating);
        row.SetValue(commentsColumn, updatedComments);

        gridComments.editComplete();
        gridComments.callback();

        gridCommentsAction = "";

        gridComments.set_callbackParameter("");

        callbackMessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Thecommenthasbeensuccessfullyupdated %>'/>";
    }

    function saveComment(rowIndex) {
        editComment(rowIndex, false);

        closeEditComments();
    }

    function saveAndApproveComment(rowIndex) {
        var row = gridComments.Table.GetRow(rowIndex);

        var updatedTitle = $("div#editComments input#txtTitle").val();
        var updatedRating = $("div#editComments input#txtRating").val();
        var updatedComments = $("div#editComments textarea#txtComments").val();

        gridCommentsAction = "SaveAndApprove"
        gridComments.set_callbackParameter(getCallbackParameter());
        gridComments.edit(row);

        row.SetValue(titleColumn, updatedTitle);
        row.SetValue(ratingColumn, updatedRating);
        row.SetValue(commentsColumn, updatedComments);

        gridComments.editComplete();
        gridComments.callback();

        gridCommentsAction = "";

        gridComments.set_callbackParameter("");

        callbackMessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Thecommenthasbeensuccessfullyupdatedandapproved %>'/>";

        closeEditComments();
    }

    function getCallbackParameter() {
        return gridCommentsAction + "|" + selectedCommentIds;
    }

    function convertArrayToString(array) {
        var output = "";

        for (var i = 0; i < array.length; i++) {
            if (i < array.length - 1) {
                output += array[i] + ",";
            }
            else {
                output += array[i];
            }
        }

        return output;
    }

    function formatHtml(input) {
        var output = "<p>" + input.replace(/^\s+|\s+$/g, "") + "</p>";

        output = output.replace(/\n\n/g, "</p><p>");
        output = output.replace(/\n/g, "<br />");

        return output;
    }

    function selectAllComments(checkBox) {
        isSelectAll = !isSelectAll;

        for (var i = 0; i < gridComments.Table.GetRowCount() ; i++) {
            var commentId = gridComments.Table.GetRow(i).GetMember('Id').get_value();

            var checkbox = $("input#" + commentId + "_select");

            if (isSelectAll) {
                $(checkbox).attr("checked", true);
            }
            else {
                $(checkbox).attr("checked", false);
            }
        }
    }

    function performBulkAction(selectedAction) {

        if (selectedAction != "") {
            var selectedIds = [];

            for (var i = 0; i < gridComments.Table.GetRowCount() ; i++) {
                var commentId = gridComments.Table.GetRow(i).GetMember('Id').get_value();

                var checkbox = $("input#" + commentId + "_select");

                if ($(checkbox).attr("checked")) {
                    selectedIds[selectedIds.length] = commentId;
                }
            }

            if (selectedIds.length > 0) {
                selectedCommentIds = convertArrayToString(selectedIds);

                if (selectedAction == "Approve" || selectedAction == "Unapprove" || selectedAction == "Delete") {
                    if (selectedAction == "Approve") {
                        gridCommentsAction = "BulkApprove";
                    }
                    else if (selectedAction == "Unapprove") {
                        gridCommentsAction = "BulkUnapprove";
                    }
                    else {
                        gridCommentsAction = "BulkDelete";
                    }

                    gridComments.set_callbackParameter(getCallbackParameter());

                    gridComments.callback();

                    gridCommentsAction = "";
                    selectedCommentIds = "";

                    gridComments.set_callbackParameter("");

                    callbackMessage = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Thebulkactionhasbeenperformedsuccessfully %>'/>";
                }
            }
            else {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, Pleasecheckatleastonereviewtomodify %>'/>");
            }
        }
        else {
            alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, Pleaseselectanactiontoperform %>'/>");
        }

        return false;
    }

    function gridComments_CallbackComplete(sender, eventArgs) {
        if (callbackMessage != "") {
            alert(callbackMessage);

            callbackMessage = "";
        }

        if (gridComments.RecordCount > 0)
            $(".grid-filter-row").show();

        isSelectAll = false;

        $("input#chkSelectAll").attr("checked", false);

        gridComments.unSelectAll();
    }

    function gridComments_RenderComplete(sender, eventArgs) {
        if (gridComments.RecordCount > 0)
            $(".grid-filter-row").show();
        else
            $(".grid-filter-row").hide();

        $("#" + sender.get_id() + "_dom").css("height", "auto");
    }


    function SearchReviewListing(reviewFilter) {
        gridComments.set_callbackParameter("Search|" + JSON.stringify(reviewFilter));
        gridComments.callback();
    }

    function fn_DisplayStarRating(rating) {
        var html = '';
        for (i = 0; i < 5; i++) {

            html += i < rating ? '<img src="/commerceadmin/App_Themes/General/images/fullstar.png" alt="" />' : '<img src="/commerceadmin/App_Themes/General/images/star.png" alt="" />';
        }
        return html;
    }

    function fn_InitializeRating() {
        var selectedStar = '/commerceadmin/App_Themes/General/images/fullstar.png';
        var star = '/commerceadmin/App_Themes/General/images/star.png';
        var ratingValue = $('#txtRatingValue').val();
        $('.star-rating input[type=image]').each(function (index) {
            $(this).prop('src', index < ratingValue ? selectedStar : star);
        });

        $('.star-rating input[type=image]').on('click', function () {
            $(this).prop('src', selectedStar);
            actualStar = selectedStar;
            $('#txtRatingValue').val($(this).val());
            fn_InitializeRating();
            return false;
        });

        $('.star-rating input[type=image]').hover(function () {
            actualStar = $(this).attr('src');
            $(this).prop('src', selectedStar);
        },
        function () {
            $(this).prop('src', actualStar);
        });
    }
</script>
<div class="grid-filter-row" style="display: none;">
    <input type="checkbox" id="chkSelectAll" onclick="selectAllComments(this);" />
    <asp:Button ID="btnApprove" runat="server" Text="<%$ Resources:GUIStrings, Approve %>"
        ToolTip="<%$ Resources:GUIStrings, Approve %>" OnClientClick="return performBulkAction('Approve');"
        CssClass="small-button" />
    <asp:Button ID="btnDelete" runat="server" Text="<%$ Resources:GUIStrings, Delete %>"
        ToolTip="<%$ Resources:GUIStrings, Delete %>" OnClientClick="return performBulkAction('Delete');"
        CssClass="small-button" />
</div>
<ComponentArt:Grid ID="gridComments" CallbackCachingEnabled="true" SkinID="Default"
    runat="server" Width="100%" PageSize="20" EnableViewState="True" RunningMode="Callback"
    AutoCallBackOnInsert="true" AutoCallBackOnUpdate="true" AllowPaging="True" AutoAdjustPageSize="true"
    ManualPaging="true" CallbackCacheLookAhead="10" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="dragTemplate"
    PagerInfoClientTemplateId="gridCommentsPaginationTemplate" AutoPostBackOnCheckChanged="false"
    SliderPopupClientTemplateId="gridCommentsSliderTemplate" LoadingPanelClientTemplateId="gridCommentsLoadingPanelTemplate"
    EmptyGridText="<%$Resources:GUIStrings, NoItemsFound %>">
    <ClientEvents>
        <CallbackComplete EventHandler="gridComments_CallbackComplete" />
        <RenderComplete EventHandler="gridComments_RenderComplete" />
    </ClientEvents>
    <Levels>
        <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="false"
                    AllowSorting="False" DataCellClientTemplateId="ctSelectComment" Width="20" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="true"
                    DataField="ProductTypeName" Align="left" runat="server" HeadingText="<%$ Resources:GUIStrings, ProductType1 %>"
                    TextWrap="true" Width="140" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="true"
                    DataField="ProductTitle" Align="left" runat="server" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                    TextWrap="true" Width="265" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="true"
                    DataField="RatingValue" Align="left" runat="server" HeadingText="<%$ Resources:GUIStrings, Rating %>"
                    TextWrap="true" Width="110" DataCellClientTemplateId="CTStarRating" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="true"
                    DataField="CommentTitle" Align="left" runat="server" HeadingText="<%$ Resources:GUIStrings, Title %>"
                    TextWrap="true" Width="200" Visible="false" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="true"
                    AllowSorting="false" DataField="Comments" Align="left" runat="server" HeadingText="<%$ Resources:GUIStrings, NameComment %>"
                    TextWrap="true" DataCellClientTemplateId="ctReviewDetails" Width="320" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="false"
                    AllowSorting="false" DataField="Status" Align="left" runat="server" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataCellClientTemplateId="ctStatus" Width="78" />
                <ComponentArt:GridColumn AllowReordering="false" FixedWidth="true" IsSearchable="false"
                    DataField="CreatedDate" Align="left" FormatString="MM/dd/yyyy" runat="server"
                    HeadingText="<%$ Resources:GUIStrings, DatePosted %>" Width="100" />
                <ComponentArt:GridColumn DataField="PostedBy" Visible="false" />
                <ComponentArt:GridColumn DataField="Email" Visible="false" />
                <ComponentArt:GridColumn DataField="UserId" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="ParentId" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="ProductId" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="RatingId" IsSearchable="false" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="CTStarRating">
            ## fn_DisplayStarRating(DataItem.getMember('RatingValue').get_value()); ##
            
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ctSelectComment">
            <input id="## DataItem.getMember('Id').get_value()
    ##_select"
                type="checkbox" />
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ctReviewDetails">
            <div class="commentAuthor">
                <span class="postedBy">## DataItem.getMember('PostedBy').get_value(); ##</span>
            </div>
            <div class="commentDetails">
                <span id="## DataItem.getMember('Id').get_value()
    ##_visibleComments"></span>## displayPartialComment(DataItem.get_index()); ##
            </div>
            <div class="commentActions">
                ## displayCommentOptions(DataItem.get_index()); ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ctStatus">
            ## displayCommentStatus(DataItem.get_index()); ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="gridCommentsPaginationTemplate">
            ## stringformat(Page0Of1Items, currentPageIndex(gridComments), pageCount(gridComments),
            gridComments.RecordCount) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="gridCommentsLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(gridComments) ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="gridCommentsSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##
                </p>
                <p class="paging">
                    <span class="pages">## stringformat(Page0Of1, DataItem.PageIndex + 1, gridComments.PageCount)
                        ##</span> <span class="items">## stringformat(Item0Of1, DataItem.Index + 1, gridComments.RecordCount)
                            ##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<div id="commentsPreview" class="iapps-modal" style="display: none;">
    <div class="modal-content clear-fix">
    </div>
    <div class="modal-footer clear-fix">
        <input type="button" class="button" onclick="closeCommentsPreview();"
            value="<%= GUIStrings.Close %>" />
    </div>
</div>
<div id="editComments" class="iapps-modal" style="display: none; width: 525px;">
    <div class="modal-header clear-fix">
        <h2>
            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, EditComments %>" /></h2>
    </div>
    <div class="modal-content clear-fix" style="">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /></label>
            <div class="form-value">
                <input id="txtTitle" type="text" class="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Rating %>" /></label>
            <div class="form-value star-rating">
                <input type="image" src="/commerceadmin/App_Themes/General/images/star.png" alt="" value="1" />
                <input type="image" src="/commerceadmin/App_Themes/General/images/star.png" alt="" value="2" />
                <input type="image" src="/commerceadmin/App_Themes/General/images/star.png" alt="" value="3" />
                <input type="image" src="/commerceadmin/App_Themes/General/images/star.png" alt="" value="4" />
                <input type="image" src="/commerceadmin/App_Themes/General/images/star.png" alt="" value="5" />
                <input id="txtRatingValue" type="hidden" class="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, Comments %>" /></label>
            <div class="form-value">
                <textarea id="txtComments" rows="10" cols="70" class="textBoxes"></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer clear-fix">
        <input type="button" class="button" onclick="closeEditComments();"
            value="<%= GUIStrings.Cancel %>" />
        <input type="button" class="primarybutton" onclick="saveAndApproveComment(editedRowIndex);"
            value="<%= GUIStrings.SaveAndApprove1 %>" />
        <input type="button" class="primarybutton" onclick="saveComment(editedRowIndex);"
            value="<%= GUIStrings.Save %>" />
    </div>
</div>
<div id="selectProducts" class="iapps-modal" style="display: none;">
    <div class="modal-header clear-fix">
        <h2>
            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, SelectProducts %>" /></h2>
    </div>
    <div class="modal-content">
        <a href="#" onclick="addProductsPopup();" class="add-more">Add More</a>
        <div id="selProductList">
        </div>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="CloseSelectProductsLayer();" value="<%= SiteSettings.Close %>"
            class="button" />
    </div>
</div>
<script type="text/javascript">
    $("#selectProducts").dialog({ autoOpen: false, modal: true, width: "345px", dialogClass: "iapps-popup-layer" })
    //$("#editComments").dialog({ autoOpen: false, modal: true, width: "570px", dialogClass: "iapps-popup-layer" })
    $("#commentsPreview").dialog({ autoOpen: false, modal: true, width: "345px", dialogClass: "iapps-popup-layer" })
</script>
