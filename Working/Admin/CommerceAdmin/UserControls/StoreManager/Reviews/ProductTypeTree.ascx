﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTypeTree.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Reviews.ProductTypeTree" %>
<script type="text/javascript">
    //This function passes the objects to the global script
    function SetTreeLevelGeneralVariablesinPage() {
        setTimeout(function() {
            treeObject = trvProductTypes;
            treeMenuObject = mnuProductTypes;
            treeObjectName = "trvProductTypes";
        }, 500);
    }
    
</script>

<ComponentArt:TreeView ID="trvProductTypes" runat="server" SkinID="Default" AutoScroll="false"
    FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
    <ClientEvents>
    </ClientEvents>
</ComponentArt:TreeView>
