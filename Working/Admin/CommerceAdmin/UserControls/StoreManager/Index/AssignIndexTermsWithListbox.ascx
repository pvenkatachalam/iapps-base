<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AssignIndexTermsWithListbox" %>
<script type="text/javascript">
    var termswin;
    var taxonomiesArr = null;
    var selectedTaxonomyIds;

    function openPopup() {

        selectedTaxonomyIds = "";
        var listBox = document.getElementById("<%=newIndexTerms.ClientID%>");

        for (i = 0; i < listBox.options.length; i++) {
            if (selectedTaxonomyIds != "")
                selectedTaxonomyIds = selectedTaxonomyIds + ',' + listBox.options[i].value;
            else
                selectedTaxonomyIds = listBox.options[i].value;
        }

        var customAttributes = {}
        customAttributes["SelectedTaxonomyIds"] = selectedTaxonomyIds;
        OpeniAppsAdminPopup("AssignIndexTermsPopup", "ObjectId=" + objectIdVar, "SetIndexTerm", customAttributes);
    }

    function SetIndexTerm() {

        var listBox = $("#<%=newIndexTerms.ClientID%>");
        var hdnIndexTerms = $("#<%=hdnSelectedIndexTerms.ClientID%>");

        listBox.empty();
        if (typeof popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "undefined" &&
                        popupActionsJson.CustomAttributes["SelectedTaxonomyJson"] != "") {
            var taxonomyJson = JSON.parse(popupActionsJson.CustomAttributes["SelectedTaxonomyJson"]);

            $.each(taxonomyJson, function () {
                listBox.append($("<option />").val(this.Id).html(this.Title));
            });

            hdnIndexTerms.val(JSON.stringify(taxonomyJson));
        }
    }
</script>
<label class="form-label">
    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Tags %>" /><br />
    <a href="#" onclick="openPopup(); return false;" runat="server" id="assignIndexTermHrf">
        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, AssignTags %>" /></a></label>
<div class="form-value">
    <asp:ListBox ID="newIndexTerms" runat="server" SelectionMode="Multiple" Width="204">
    </asp:ListBox>
    <%--<input id="hdnSelectedTaxonomyIds1" type="hidden" runat="server" />--%>
    <asp:HiddenField runat="server" ID="hdnSelectedIndexTerms" />
</div>
