﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListing.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductListing" %>
<script type="text/javascript">
    var productListItem;
    var IdColumn = 0;
    var ProductIdColumn = 1;
    var IsDownloadableMediaColumnInProductGrid = 12;
    var IsDownloadableMediaColumnInProductSKUGrid = 10;
    var skuId;
    var IsDownloadableMedia;
    var productId;

    function grdSkuListing_onContextMenu(sender, eventArgs) {
        var e = eventArgs.get_event();
        productListItem = eventArgs.get_item();

        skuId = productListItem.getMember(IdColumn).get_text();
        IsDownloadableMedia = productListItem.getMember(IsDownloadableMediaColumnInProductSKUGrid).get_text();
        productId = productListItem.getMember(ProductIdColumn).get_text();
        menuItems = mnuProductsList.get_items();
        if (productId != '' && productId != emptyGuid) //only add
        {
            for (i = 0; i < menuItems.get_length() ; i++) {
                if (menuItems.getItem(i).get_id() == 'cmDelete') {
                    menuItems.getItem(i).set_text('Delete ' + (viewBy == 'Sku' ? 'SKU' : viewBy))

                }
                if (menuItems.getItem(i).get_id() == 'cmProductMedia') {
                    //if product type is of media type, then only show, else hide it
                    if (IsDownloadableMedia == "true")
                        menuItems.getItem(i).set_visible(true);
                    else
                        menuItems.getItem(i).set_visible(false);
                }
                else
                    menuItems.getItem(i).set_visible(true);
            }
        }
        else {
            for (i = 0; i < menuItems.get_length() ; i++) {
                if (menuItems.getItem(i).get_id() == 'cmAdd' || menuItems.getItem(i).get_id() == 'cmAddBundle') {
                }
                else {
                    menuItems.getItem(i).set_visible(false);
                }
            }
        }

        menuItems.getItemById("cmAssignIndexTerm").set_visible(false);
        mnuProductsList.showContextMenuAtEvent(e);
    }
    function grdProductListing_onContextMenu(sender, eventArgs) {
        var e = eventArgs.get_event();
        productListItem = eventArgs.get_item();
        skuId = productListItem.getMember(IdColumn).get_text();
        productId = productListItem.getMember(ProductIdColumn).get_text();
        IsDownloadableMedia = productListItem.getMember(IsDownloadableMediaColumnInProductGrid).get_text();
        menuItems = mnuProductsList.get_items();

        if (productId != '' && productId != emptyGuid) //only add
        {
            for (i = 0; i < menuItems.get_length() ; i++) {
                if (menuItems.getItem(i).get_id() == 'cmDelete') {
                    menuItems.getItem(i).set_text('Delete ' + viewBy)

                }
                if (menuItems.getItem(i).get_id() == 'cmProductMedia') {
                    //if product type is of media type, then only show, else hide it
                    if (IsDownloadableMedia == "true")
                        menuItems.getItem(i).set_visible(true);
                    else
                        menuItems.getItem(i).set_visible(false);
                }
                else
                    menuItems.getItem(i).set_visible(true);

            }
        }
        else {
            for (i = 0; i < menuItems.get_length() ; i++) {
                if (menuItems.getItem(i).get_id() == 'cmAdd' || menuItems.getItem(i).get_id() == 'cmAddBundle') {
                }
                else {
                    menuItems.getItem(i).set_visible(false);
                }
            }
        }

        menuItems.getItemById("cmAssignIndexTerm").set_visible(false);
        mnuProductsList.showContextMenuAtEvent(e);
    }
    function grdBundleListing_onContextMenu(sender, eventArgs) {
        var e = eventArgs.get_event();
        productListItem = eventArgs.get_item();
        skuId = productListItem.getMember(IdColumn).get_text();
        productId = productListItem.getMember(ProductIdColumn).get_text();
        menuItems = mnuProductsList.get_items();
        if (productId != '' && productId != emptyGuid) //only add
        {
            for (i = 0; i < menuItems.get_length() ; i++) {
                if (menuItems.getItem(i).get_id() == 'cmDelete') {
                    menuItems.getItem(i).set_text('Delete a ' + viewBy)

                }

                //the bundle will not have context menu for manage product media 
                if (menuItems.getItem(i).get_id() == 'cmProductMedia')
                    menuItems.getItem(i).set_visible(false);
                else
                    menuItems.getItem(i).set_visible(true);
            }
        }
        else {
            for (i = 0; i < menuItems.get_length() ; i++) {
                if (menuItems.getItem(i).get_id() == 'cmAdd' || menuItems.getItem(i).get_id() == 'cmAddBundle') {
                }
                else {
                    menuItems.getItem(i).set_visible(false);
                }
            }
        }

        menuItems.getItemById("cmAssignIndexTerm").set_visible(false);
        mnuProductsList.showContextMenuAtEvent(e);
    }
    function mnuProductsList_onItemSelect(sender, eventArgs) {

        var menuItem = eventArgs.get_item();
        var contextDataNode = menuItem.get_parentMenu().get_contextData();
        var selectedMenuId = menuItem.get_id();

        //need a better way to do this that doesn't depend on the absolute client id of the rbShowBundle control... (DM)
        var showBundle = viewBy == "Bundle" ? true : false;

        if (skuId == '')
            skuId = '00000000-0000-0000-0000-000000000000';

        if (selectedMenuId != null) {
            switch (selectedMenuId) {
                case 'cmAdd':
                    window.location = 'GeneralDetails.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000' +
                                        '&SelectedProductId=00000000-0000-0000-0000-000000000000';
                    break;
                case 'cmAddBundle':
                    window.location = 'BundleDetails.aspx?SelectedProductSkuIds=00000000-0000-0000-0000-000000000000' +
                                        '&SelectedProductId=00000000-0000-0000-0000-000000000000';
                    break;
                case 'cmDetails':
                    if (showBundle) {
                        window.location = 'BundleDetails.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
		                                            skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    else {
                        window.location = 'GeneralDetails.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
		                                            skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    break;
                case 'cmPricing':
                    window.location = 'ProductPriceSets.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
		                                            skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
                case 'cmPromos':
                    break;
                case 'cmShipping':
                    break;
                case 'cmInventory':
                    if (showBundle) {
                        window.location = 'BundleInventory.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                    skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    else {
                        window.location = 'InventoryAndRestocking.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                    skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    break;
                case 'cmImages':
                    window.location = 'ProductImages.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
                case 'cmProdTypes':
                    window.location = 'ProductType.aspx';
                    break;
                case 'cmProdAttributes':
                    window.location = 'Attributes.aspx';
                    break;
                case 'cmNavCategories':
                    window.location = 'ProductNavigationCategories.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
                case 'cmSeoTerms':
                    window.location = 'SEOTerms.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
                case 'cmNotes':

                    window.location = 'ProductNotes.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                    skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
                case 'cmRelatedProducts':
                    window.location = 'RelatedProducts.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                    skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
                case 'cmInventoryNotes':
                    if (viewBy == "Sku") {
                        window.location = 'InventoryNotes.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                    skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    else {
                        window.location = 'InventoryNotes.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    break;
                case 'cmAssignIndexTerm':
                    if (viewBy == "Sku") {
                        ShowIndexTermPagePopup(skuId, 206);
                    }
                    else if (viewBy == "Product") {
                        ShowIndexTermPagePopup(productId, 205);
                    }
                    else if (viewBy == "Bundle") {
                        ShowIndexTermPagePopup(productId, 221);
                    }
                    break;
                case "cmDelete":
                    var returnVal;

                    if (viewBy == "Sku") {
                        var referedProduct = CheckDeleteProductSKU(skuId, '0');
                        var objReferedProduct = eval('(' + referedProduct + ')');
                        returnVal = objReferedProduct[0].ReturnVal;
                    }
                    else if (viewBy == "Product") {
                        var referedProduct = CheckDeleteProductSKU(productId, "1");
                        var objReferedProduct = eval('(' + referedProduct + ')');
                        returnVal = objReferedProduct[0].ReturnVal;
                    }
                    else if (viewBy == "Bundle") {
                        var referedProduct = CheckDeleteProductSKU(productId, "1");
                        var objReferedProduct = eval('(' + referedProduct + ')');
                        returnVal = objReferedProduct[0].ReturnVal;
                    }
                    //                    if(confirmText.substring(0,1)=='~')
                    //                    {
                    //                        alert(confirmText.substring(1));
                    //                        return false;
                    //                    }
                    var isdelete = true;
                    if (returnVal == 1) {
                        if (confirm(stringformat('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouCannotDeleteA0ThatIsPartOfAnOrder %>" />', (viewBy == 'Sku' ? 'SKU' : viewBy)))) {
                            isdelete = false;
                        }
                        else {
                            return false;
                        }

                    }
                    else if (returnVal == 2) {

                        var msg = stringformat('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouCannotDeleteA0ThatIsPartOfAnBundle %>" />', (viewBy == 'Sku' ? 'SKU' : viewBy));
                        var count = 0;
                        for (var i = 0; i < objReferedProduct.length; i++) {
                            msg += " \n " + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, ReferedProductSKUs %>'/>" + " \n";
                            count = i + 1;
                            msg += count + ") " + objReferedProduct[i].SKU + " \n";
                        }
                        alert(msg);
                        return false;
                    }
                    else if (returnVal == 3) {
                        alert(stringformat('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouCannotDeleteA0ThatIsPartOfCart %>" />', viewBy))
                        return false;
                    }
                    else if (returnVal == 4) {
                        alert(stringformat('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouCannotDeleteA0ThatIsPartOfWishlist %>" />', viewBy))
                        return false;
                    }
                    else if (returnVal == 5) {
                        var msg = stringformat('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YouCannotDeleteA0ThatIsPartOfProductRelation %>" />', viewBy);

                        var count = 0;
                        for (var i = 0; i < objReferedProduct.length; i++) {
                            msg += " \n" + "<asp:localize runat='server' text='<%$ Resources:GUIStrings, RelatedProducts1 %>'/>" + " \n";
                            count = i + 1;
                            msg += count + ") " + objReferedProduct[i].ProductTitle + " \n";
                        }
                        alert(msg);
                        return false;
                    }
                    else {
                        var message = objReferedProduct[0].Message;
                        if (!confirm(message)) return false;
                    }


    if (viewBy == "Bundle") {
        confirmText = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisBundle %>' />";
                    }

                    //                        if(confirm(returnVal))
                    //                        {
                    if (viewBy == "Sku") {
                        DeleteSKU(skuId, isdelete);

                        if (grdSkuListing.get_recordCount() > grdSkuListing.get_pageSize()) {
                            if (grdSkuListing.get_recordCount() % grdSkuListing.get_pageSize() == 1) {
                                grdSkuListing.previousPage();
                            }
                        }
                        grdSkuListing.callback();
                        //<%= grdProductListing.ID %>.callback();
                        //grdProductListing.callback();
                    }
                    else if (viewBy == "Product") {
                        DeleteProduct(productId, isdelete);
                        if (grdProductListing.get_recordCount() > grdProductListing.get_pageSize()) {
                            if (grdProductListing.get_recordCount() % grdProductListing.get_pageSize() == 1) {
                                grdProductListing.previousPage();
                            }
                        }
                        grdProductListing.callback();
                    }
                    else if (viewBy == "Bundle") {
                        DeleteBundle(productId, isdelete);
                        if (grdBundleListing.get_recordCount() > grdBundleListing.get_pageSize()) {
                            if (grdBundleListing.get_recordCount() % grdBundleListing.get_pageSize() == 1) {
                                grdBundleListing.previousPage();
                            }
                        }
                        grdBundleListing.callback();
                    }
                    //}
                    break;
                case 'cmProductMedia':
                    window.location = 'ProductMedia.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
                                                skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    break;
            }
        }
    }
    var taxonomiesArr;
    function ShowIndexTermPagePopup(ObjectId, ObjectType) {
        var tokenValue = GetToken(jCMSProductId, 'CMS');
        var queryString = '?Token=' + tokenValue + "&ObjectId=" + ObjectId + "&ObjectType=" + ObjectType;
        adminpath = jCMSAdminSiteUrl + '/Administration/Index/AssignIndexTermsPopup.aspx' + queryString;
        termswin = dhtmlmodal.open('IndexTerms', 'iframe', adminpath, '', 'width=800px,height=500px,center=1,resize=0,scrolling=1');
        termswin.onclose = function () {
            var a = document.getElementById('IndexTerms');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function FormatQuantity(strQty, strUnlimited) {

        var resourceString = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Unlimited %>"/>';

        if (strUnlimited == "true") {
            return resourceString;
        }
        if (strUnlimited == "" || isNaN(parseFloat(strQty)))
            return "0";

        if (parseFloat(strQty) < 0)
            return "0";

        return strQty;
    }



</script>
<ComponentArt:Grid SkinID="Default" ID="grdProductListing" SliderPopupClientTemplateId="ProductListingSliderTemplate"
    SliderPopupCachedClientTemplateId="ProductListingSliderTemplateCached" Width="100%"
    runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" Sort="ProductTitle ASC"
    PagerInfoClientTemplateId="ProductListingPaginationTemplate" CallbackCachingEnabled="false"
    SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="ProductListingLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn Width="350" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                    DataField="ProductTitle" Align="Left" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="NameHoverTemplate" TextWrap="true" />
                <ComponentArt:GridColumn Width="100" DataField="Title" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, Type %>"
                    DataField="TypeTitle" IsSearchable="true" Align="Left" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="TypeHoverTemplate" TextWrap="true" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, ProductId %>"
                    DataField="ProductIDUser" Align="Left" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="ProductIDHoverTemplate" TextWrap="true" />
                <ComponentArt:GridColumn Width="110" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                    Visible="false" DataField="RealListPrice" IsSearchable="false" Align="Right"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="RealListPriceHoverTemplate"
                    FormatString="C" />
                <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                    DataField="ListPrice" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="ListPriceHoverTemplate" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, Quantity %>"
                    DataField="Quantity" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="QuantityHoverTemplate" FormatString="N" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, AvailableToSell %>"
                    DataField="AvailableToSell" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="AvailableToSellHoverTemplate" FormatString="N" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataField="IsActiveDescription" Align="Left" AllowReordering="false" FixedWidth="true"
                    IsSearchable="true" DataCellClientTemplateId="StatusHoverTemplate" />
                <ComponentArt:GridColumn DataField="SalePrice" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="IsUnlimited" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="IsDownloadableMedia" Visible="false" IsSearchable="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdProductListing_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameHoverTemplate">
            <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductIDHoverTemplate">
            <span title="## DataItem.GetMember('ProductIDUser').get_text() ##">## DataItem.GetMember('ProductIDUser').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductIDUser').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuHoverTemplate">
            <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TypeHoverTemplate">
            <span title="## DataItem.GetMember('TypeTitle').get_text() ##">## DataItem.GetMember('TypeTitle').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('TypeTitle').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ListPriceHoverTemplate">
            <span title="## DataItem.GetMember('ListPrice').get_text() ##">## DataItem.GetMember('ListPrice').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ListPrice').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="RealListPriceHoverTemplate">
            <span title="## DataItem.GetMember('RealListPrice').get_text() ##">## DataItem.GetMember('RealListPrice').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('RealListPrice').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QuantityHoverTemplate">
            <span title="## FormatQuantity (DataItem.GetMember('Quantity').get_text(), DataItem.GetMember('IsUnlimited').get_text()) ##">## FormatQuantity (DataItem.GetMember('Quantity').get_text(), DataItem.GetMember('IsUnlimited').get_text())
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="AvailableToSellHoverTemplate">
            <span title="## FormatQuantity (DataItem.GetMember('AvailableToSell').get_text(), DataItem.GetMember('IsUnlimited').get_text()) ##">## FormatQuantity (DataItem.GetMember('AvailableToSell').get_text(), DataItem.GetMember('IsUnlimited').get_text())
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
            <span title="## DataItem.GetMember('IsActiveDescription').get_text() ##">## DataItem.GetMember('IsActiveDescription').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('IsActiveDescription').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductListingSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdProductListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductListingSliderTemplateCached">
            <div class="SliderPopup">
                <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdProductListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdProductListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductListingPaginationTemplate">
            ##stringformat(Page0Of12items, currentPageIndex(grdProductListing), pageCount(grdProductListing),
            grdProductListing.RecordCount)##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductListingLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdProductListing) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Grid SkinID="Default" ID="grdBundleListing" SliderPopupClientTemplateId="BundleListingSliderTemplate"
    SliderPopupCachedClientTemplateId="BundleListingSliderTemplateCached" Width="100%"
    runat="server" RunningMode="Callback" EmptyGridText="No items found" AutoAdjustPageSize="true"
    AutoPostBackOnSelect="false" PageSize="10" Sort="ProductTitle ASC" PagerInfoClientTemplateId="BundleListingPaginationTemplate"
    CallbackCachingEnabled="false" SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="BundleListingLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn Width="450" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                    DataField="ProductTitle" Align="Left" IsSearchable="true" AllowReordering="false"
                    FixedWidth="true" TextWrap="true" DataCellClientTemplateId="NameBundleHoverTemplate" />
                <ComponentArt:GridColumn Width="100" IsSearchable="false" DataField="Title" Visible="false" />
                <ComponentArt:GridColumn Width="250" HeadingText="<%$ Resources:GUIStrings, Type %>"
                    DataField="TypeTitle" IsSearchable="true" Align="Left" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="TypeBundleHoverTemplate" TextWrap="true" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, ProductId %>"
                    DataField="ProductIDUser" Align="Left" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="ProductIDHoverTemplate2" TextWrap="true" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                    DataField="ListPrice" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="BundleListPriceHoverTemplate" FormatString="c" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, AvailableToSell %>"
                    DataField="Quantity" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="BundleQuantityHoverTemplate" FormatString="N" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataField="IsActiveDescription" Align="Left" AllowReordering="false" FixedWidth="true"
                    IsSearchable="true" DataCellClientTemplateId="BundleStatusHoverTemplate" />
                <ComponentArt:GridColumn DataField="SalePrice" Visible="false" IsSearchable="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdBundleListing_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameBundleHoverTemplate">
            <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductIDHoverTemplate2">
            <span title="## DataItem.GetMember('ProductIDUser').get_text() ##">## DataItem.GetMember('ProductIDUser').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductIDUser').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuBundleHoverTemplate">
            <span title="## DataItem.GetMember('Title').get_text() ##">## DataItem.GetMember('Title').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TypeBundleHoverTemplate">
            <span title="## DataItem.GetMember('TypeTitle').get_text() ##">## DataItem.GetMember('TypeTitle').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('TypeTitle').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleListPriceHoverTemplate">
            <span title="## DataItem.GetMember('ListPrice').get_text() ##">## DataItem.GetMember('ListPrice').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ListPrice').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleQuantityHoverTemplate">
            <span title="## DataItem.GetMember('Quantity').get_text() ##">##(DataItem.GetMember('Id').get_text()!=""
                && DataItem.GetMember('Quantity').get_text() == "") ? "0" : DataItem.GetMember('Quantity').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleStatusHoverTemplate">
            <span title="## DataItem.GetMember('IsActiveDescription').get_text() ##">## DataItem.GetMember('IsActiveDescription').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('IsActiveDescription').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleListingSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdBundleListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdBundleListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleListingSliderTemplateCached">
            <div class="SliderPopup">
                <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdBundleListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdBundleListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleListingPaginationTemplate">
            ##stringformat(Page0Of12items, currentPageIndex(grdBundleListing), pageCount(grdBundleListing),
            grdBundleListing.RecordCount)##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="BundleListingLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdBundleListing) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Grid SkinID="Default" ID="grdSkuListing" SliderPopupClientTemplateId="SkuListingSliderTemplate"
    SliderPopupCachedClientTemplateId="SkuListingSliderTemplateCached" Width="100%"
    runat="server" RunningMode="Callback" EmptyGridText="No items found" AutoAdjustPageSize="true"
    AutoPostBackOnSelect="false" PageSize="10" Sort="ProductTitle ASC" PagerInfoClientTemplateId="SkuListingPaginationTemplate"
    CallbackCachingEnabled="false" SearchOnKeyPress="false" ManualPaging="true" LoadingPanelClientTemplateId="SkuListingLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="ProductId" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn Width="250" HeadingText="<%$ Resources:GUIStrings, ProductName %>"
                    DataField="ProductTitle" Align="Left" IsSearchable="true" AllowReordering="false"
                    FixedWidth="true" TextWrap="true" DataCellClientTemplateId="NameHoverSkuTemplate" />
                <ComponentArt:GridColumn Width="250" HeadingText="<%$ Resources:GUIStrings, SKU %>"
                    DataField="SKU" IsSearchable="true" Align="Left" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="SkuHoverSkuTemplate" TextWrap="true" />
                <ComponentArt:GridColumn Width="125" HeadingText="<%$ Resources:GUIStrings, Type %>"
                    DataField="TypeTitle" IsSearchable="true" Align="Left" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="TypeHoverSkuTemplate" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, ProductId %>"
                    DataField="ProductIDUser" Align="Left" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="ProductIDHoverTemplate3" TextWrap="true" />
                <ComponentArt:GridColumn Width="60" HeadingText="<%$ Resources:GUIStrings, ListPrice %>"
                    DataField="ListPrice" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="ListPriceHoverSkuTemplate" FormatString="c" />
                <ComponentArt:GridColumn Width="65" HeadingText="<%$ Resources:GUIStrings, Quantity %>"
                    DataField="Quantity" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="QuantityHoverSkuTemplate" FormatString="N" />
                <ComponentArt:GridColumn Width="100" HeadingText="<%$ Resources:GUIStrings, AvailableToSell %>"
                    DataField="AvailableToSell" IsSearchable="false" Align="Right" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="AvailableToSellHoverSkuTemplate"
                    FormatString="N" />
                <ComponentArt:GridColumn Width="50" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataField="IsActiveDescription" Align="Left" AllowReordering="false" FixedWidth="true"
                    IsSearchable="true" DataCellClientTemplateId="StatusHoverSkuTemplate" />
                <ComponentArt:GridColumn DataField="IsUnlimited" Visible="false" IsSearchable="false" />
                <ComponentArt:GridColumn DataField="IsDownloadableMedia" Visible="false" IsSearchable="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdSkuListing_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameHoverSkuTemplate">
            <span title="## DataItem.GetMember('ProductTitle').get_text() ##">## DataItem.GetMember('ProductTitle').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductTitle').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ProductIDHoverTemplate3">
            <span title="## DataItem.GetMember('ProductIDUser').get_text() ##">## DataItem.GetMember('ProductIDUser').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ProductIDUser').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuHoverSkuTemplate">
            <span title="## DataItem.GetMember('SKU').get_text() ##">## DataItem.GetMember('SKU').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('SKU').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TypeHoverSkuTemplate">
            <span title="## DataItem.GetMember('TypeTitle').get_text() ##">## DataItem.GetMember('TypeTitle').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('TypeTitle').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ListPriceHoverSkuTemplate">
            <span title="## DataItem.GetMember('ListPrice').get_text() ##">## DataItem.GetMember('ListPrice').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('ListPrice').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="QuantityHoverSkuTemplate">
            <span title="## FormatQuantity(DataItem.GetMember('Quantity').get_text(), DataItem.GetMember('IsUnlimited').get_text()) ##">## FormatQuantity(DataItem.GetMember('Quantity').get_text(), DataItem.GetMember('IsUnlimited').get_text())
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="AvailableToSellHoverSkuTemplate">
            <span title="## FormatQuantity(DataItem.GetMember('AvailableToSell').get_text(), DataItem.GetMember('IsUnlimited').get_text()) ##">## FormatQuantity(DataItem.GetMember('AvailableToSell').get_text(), DataItem.GetMember('IsUnlimited').get_text())
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="StatusHoverSkuTemplate">
            <span title="## DataItem.GetMember('IsActiveDescription').get_text() ##">## DataItem.GetMember('IsActiveDescription').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('IsActiveDescription').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuListingSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdSkuListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdSkuListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuListingSliderTemplateCached">
            <div class="SliderPopup">
                <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdSkuListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdSkuListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuListingPaginationTemplate">
            ##stringformat(Page0Of12items, currentPageIndex(grdSkuListing), pageCount(grdSkuListing),
            grdSkuListing.RecordCount)##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SkuListingLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdSkuListing) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuProductsList" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuProductsList_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
