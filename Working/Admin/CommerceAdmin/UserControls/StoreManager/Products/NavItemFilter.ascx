﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavItemFilter.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.NavItemFilter" %>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<style>
    .custom-combobox {
        position: relative;
        display: inline-block;
        float: left;
        width: 100%;
    }

    .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
    }

    .custom-combobox-input {
        margin: 0;
        padding: 4px 5px;
        background: #ffffff;
        color: black;
        border-top-left-radius: 0px;
        border-bottom-left-radius: 0px;
        font-family: Arial;
        border-right: none;
        width: 80%;
    }


    .ui-autocomplete {
        height: 250px;
        overflow-y: scroll;
    }

    .ui-menu-item {
        font-family: Arial;
        font-size: 11px;
        list-style-image: none;
    }

    .custom-combobox-toggle {
        background: #ffffff;
        border-top-right-radius: 0px;
        border-bottom-right-radius: 0px;
        border-left: none;
        color: black;
    }

        .custom-combobox-toggle.ui-state-hover {
            background: #ffffff;
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
            border-left: none;
            color: black;
            border-color: #d3d3d3;
        }

    .ui-menu-item a {
        text-decoration: none;
        display: block;
        line-height: 1.5;
        zoom: 1;
        border-radius: 0px;
        border: 0px;
    }

        .ui-menu-item a:hover {
            text-decoration: none;
            display: block;
            line-height: 1.5;
            zoom: 1;
            border-radius: 0px;
            background: #EEEEEE;
            border-color: #EEEEEE;
        }

    .ui-menu-item:hover {
        text-decoration: none;
        display: block;
        line-height: 1.5;
        zoom: 1;
        border-radius: 0px;
        background: #EEEEEE;
        border-color: #EEEEEE;
    }

    .ui-state-hover,
    .ui-widget-content .ui-state-hover,
    .ui-widget-header .ui-state-hover,
    .ui-state-focus,
    .ui-widget-content .ui-state-focus,
    .ui-widget-header .ui-state-focus {
        background: none;
        background: #EEEEEE;
        border-color: #EEEEEE;
    }
</style>
<script>


 (function( $ ) {
    $.widget( "custom.combobox", {
        _create: function() {
            this.wrapper = $( "<span>" )
              .addClass( "custom-combobox" )
              .insertAfter( this.element );
 
            this.element.hide();
            this._createAutocomplete();
            this._createShowAllButton();
        },
 
        _createAutocomplete: function() {
            var selected = this.element.children( ":selected" ),
              value = selected.val() ? selected.text() : "";
 
            this.input = $( "<input>" )
              .appendTo( this.wrapper )
              .val( value )
              .attr( "title", "" )
              .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
              .autocomplete({
                  delay: 0,
                  minLength: 0,
                  source: $.proxy( this, "_source" )
              })
              .tooltip({
                  tooltipClass: "ui-state-highlight"
              });
 
            this._on( this.input, {
                autocompleteselect: function( event, ui ) {
                    ui.item.option.selected = true;
                    this._trigger( "select", event, {
                        item: ui.item.option
                    });            
                },
 
                autocompletechange: "_removeIfInvalid"
            });
        },
 
        _createShowAllButton: function() {
            var input = this.input,
              wasOpen = false;
 
            $( "<a>" )
              .attr( "tabIndex", -1 )
              .attr( "title", "Show All Items" )
              .tooltip()
              .appendTo( this.wrapper )
              .button({
                  icons: {
                      primary: "ui-icon-triangle-1-s"
                  },
                  text: false
              })
              .removeClass( "ui-corner-all" )
              .addClass( "custom-combobox-toggle ui-corner-right" )
              .mousedown(function() {
                  wasOpen = input.autocomplete( "widget" ).is( ":visible" );
              })
              .click(function() {
                  input.focus();
 
                  // Close if already visible
                  if ( wasOpen ) {
                      return;
                  }
 
                  // Pass empty string as value to search for, displaying all results
                  input.autocomplete( "search", "" );
              });
        },
 
        _source: function( request, response ) {
            var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
            response( this.element.children( "option" ).map(function() {
                var text = $( this ).text();
                if ( this.value && ( !request.term || matcher.test(text) ) )
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }) );
        },
 
        _removeIfInvalid: function( event, ui ) {
 
            // Selected an item, nothing to do
            if ( ui.item ) {
                return;
            }
 
            // Search for a match (case-insensitive)
            var value = this.input.val(),
              valueLowerCase = value.toLowerCase(),
              valid = false;
            this.element.children( "option" ).each(function() {
                if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                    this.selected = valid = true;
                    return false;
                }
            });
 
            // Found a match, nothing to do
            if ( valid ) {
                return;
            }
 
            // Remove invalid value
            this.input
              .val( "" )
              .attr( "title", value + " didn't match any item" )
              .tooltip( "open" );        
            this.element[0].selectedIndex = 0;
            this.element.change();
            this.element.blur();
            // this.element.val( "" );
            this._delay(function() {
                this.input.tooltip( "close" ).attr( "title", "" );
            }, 2500 );
            // this.input.autocomplete( "instance" ).term = "";
        },
 
        _destroy: function() {
            this.wrapper.remove();
            this.element.show();
        }
    });
})( jQuery );



           function renderComboBox()
           {
               $(function() {
                   $( ".comboselect" ).combobox({ 
                       select: function (event, ui) { 
                           $(ui.item.parentElement).change();
                           $(ui.item.parentElement).blur();                 
                       } 
                   });        
               });   
               $(".custom-combobox-input").on("click", function () {
                   $(this).select();
               });

               $(".custom-combobox-toggle").on("click", function () {                   
                   $(this).siblings(".custom-combobox-input").select();
               });
     
           }
           </script>
<script type="text/javascript">
    var defaultLogicalOperator = "--";
    var fieldColumnNo = 2;
    var operatorColumnNo = 3;
    var attributeValueColumnNo = 11;
    var attributeValueForDisplayColumnNo = 4;
    var attributeIdColumnNo = 7;
    var operatorValueColumnNo = 8;
    var attributeDatatypeColumnNo = 10;
    var emptyGuid = "00000000-0000-0000-0000-000000000000";
    var selectedLookupValue;

    var LogicalOperatorColumnNo = 5;
    var navFilterId = 'bf037cf2-8c83-4186-a5c8-8b5f79445481';
    var currentNavIdFilterId;
    var isLastRowDelete = false;


    function updateNavFilter<%=this.ClientID %>(str) {
        isLastRowDelete = false;
        if (grdFilter != null) <%=grdFilter.ClientID%>.dispose();
        currentNavIdFilterId = str;
        //CallBack1.callback(str);
            
        if(FilterIdAndProdRelationType !=undefined && FilterIdAndProdRelationType!='')
        {
            if(document.getElementById("<%=ddlRelationType.ClientID %>") !=null && document.getElementById("<%=ddlRelationType.ClientID %>") !=undefined )
            {
                setSelectedIndex(document.getElementById("<%=ddlRelationType.ClientID %>"),FilterIdAndProdRelationType);
            
                str = str + ","  +document.getElementById("<%=ddlRelationType.ClientID %>").value;
            }
        }
        <%=CallBack1.ClientID%>.callback(str);      

       // setTimeout('renderComboBox<%=this.ClientID %>()',500);
    }

    function PopulateOperatorDropDown<%=this.ClientID %>(ddlControlClientId, index, row) {
        var ddlControl = document.getElementById(ddlControlClientId);
        var myOption;
        var attrDatatype = '';

        var ddlField = document.getElementById("<%=this.ClientID %>ddlField_" + index + "_" + row.getMember(fieldColumnNo).get_column().get_dataField());
        if (ddlField != null && ddlField.value != null) {
            var fieldVal = ddlField.value.split("|");
            if (fieldVal.length > 1)
                attrDatatype = fieldVal[1];
        }

        //var strOperator = "=,!=,<,>,<=,>=";
        var strOperator;

        if (attrDatatype == '' || attrDatatype == 'undefined' ||  attrDatatype == "System.Boolean")
            strOperator = "=|equal,!=|not equal";
        else if(attrDatatype == "string" || attrDatatype == "System.String" )
            strOperator = "=|equal,!=|not equal,like|contains";
        else
            strOperator = "=|equal,!=|not equal,<|less than,>|greater than,<=|less than or equal to,>=|greater than or equal to";

        var operatorArr = strOperator.split(",");

        if (ddlControl.length > 0) {
            ddlControl.innerHTML = "";
        }

        ddlControl.options[ddlControl.length] = new Option("Select", "Select");
        var optionItem;
        for (i = 0; i < operatorArr.length; i++) {
            optionItem = operatorArr[i].split('|');
            if (optionItem.length > 1) {
                myOption = new Option(optionItem[1], optionItem[0]) ///document.createElement("Option");
                ddlControl.options[ddlControl.length] = myOption;
            }
        }
    }
        
    function setSelectedIndex(s, v) {
        for ( var i = 0; i < s.options.length; i++ ) {  
            if ( s.options[i].value == v ) {      
                s.options[i].selected = true;    
                return;
            } 
        }
    }    

    function onddlRelationType_change(selValue)
    {
        var ddl = document.getElementById("<%=ddlRelationType.ClientID %>");
        //    FilterIdAndProdRelationType =selValue;// ddl.options[ddl.selectedIndex].value;
        var filterIds =GetNavNodProductRelationId(selectedNodeId);
        var filterGroup = filterIds.split("|");
        navFilterId ='';
        if(filterGroup.length >1)
        {
            for(var ci=0;ci<filterGroup.length;ci++)
            {
                FilterIdAndProdRelationType = filterGroup[ci].split(",");
                if (FilterIdAndProdRelationType.length > 0) {
                    if(selValue== FilterIdAndProdRelationType[1])
                    {
                        navFilterId = FilterIdAndProdRelationType[0];
                        //updateNavFilter<%=this.ClientID %>(selectedNavNodeId + "," + navFilterId);
                        <%=CallBack1.ClientID%>.callback(selectedNodeId + "," + navFilterId +","  +ddl.value );
                        break;
                    }
                }
            }
        }
    }

    function saveCell<%=this.ClientID %>(itemId, columnField, newValue) {
        var row = <%=grdFilter.ClientID%>.GetRowFromClientId(itemId);

        if (row != null) {
            // Check if value was changed
            var oldValue = row.GetMember(columnField).Value;
            if (oldValue != newValue) {
                // Get column index for SetValue
                var col = 0;
                for (var i = 0; i < <%=grdFilter.ClientID%>.Table.Columns.length; i++) {
                        if (<%=grdFilter.ClientID%>.Table.Columns[i].DataField == columnField) {
                            col = i;                         
                            break;
                        }
                    }
                    row.SetValue(col, newValue, true);
                    if(col == attributeValueForDisplayColumnNo) row.SetValue(attributeValueColumnNo,newValue, true);
                }
        }
        renderComboBox();
            return true;
        }

        function saveDDLFieldValue<%=this.ClientID %>(itemId, newValue, newText) {
        var row = <%=grdFilter.ClientID%>.GetRowFromClientId(itemId);


        if (newValue != null && newValue != 'undefined') {
            var fieldArr = newValue.split('|');
            if (fieldArr != null && fieldArr.length > 0) {
                if (fieldArr[0] != newText) {
                    row.SetValue(attributeIdColumnNo, fieldArr[0], true);
                }
                else {
                    row.SetValue(attributeIdColumnNo, emptyGuid, true);
                }

                row.SetValue(attributeDatatypeColumnNo, fieldArr[1], true);
                row.SetValue(fieldColumnNo,fieldArr[2],true);
            }
        }


    }

    function saveDDLOperatorValue<%=this.ClientID %>(itemId, newValue) {
        var row = <%=grdFilter.ClientID%>.GetRowFromClientId(itemId);
        row.SetValue(operatorValueColumnNo, newValue, true);
    }

    function FillAttributes<%=this.ClientID %>(dropdownlist) {
        var ddlControl = document.getElementById(dropdownlist);
        var myOption;
        var row, col;
        col = 0;

        if (ddlControl.length > 0) {
            ddlControl.innerHTML = "";
        }


        ddlControl.options[ddlControl.length] = new Option("Select", "Select");

        for (row = 0; row < attributeArray.length; row++) {
            col = 0;
            myOption = new Option(attributeArray[row][col + 1], attributeArray[row][col]) ///document.createElement("Option");
            ddlControl.options[ddlControl.length] = myOption;
        }
    }



    var selectedDataType = '';
    function GenerateAttributesDropDown<%=this.ClientID %>(dataItem, selectedValue) {
        var str;
        var strCompareValue='';
        str = "<div class='ui-widget dropdown'><Select class='comboselect' data-clientid='"+dataItem.ClientId+"' data-index='"+dataItem.get_index() +"' id='<%=this.ClientID %>ddlField_" + dataItem.get_index() + "_" + dataItem.getCurrentMember().get_column().get_dataField() + "' ";
        str = str + "onchange=\"FillOperators<%=this.ClientID %>('" + dataItem.ClientId + "','" + dataItem.get_index() + "');\" style=\"width:245px; \" ";
        str = str + "onBlur=\"saveDDLFieldValue<%=this.ClientID %>('" + dataItem.ClientId + "',this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);  \"  >";
        str = str + "<OPTION value=\"Select\"><asp:Localize runat='server' Text='<%$ Resources:GUIStrings, Select %>' /></OPTION>";
            
        var row = <%=grdFilter.ClientID%>.GetRowFromClientId(dataItem.ClientId);
        var selectedCompareValue= row.getMember('AttributeId').get_text();

        for (row = 0; row < attributeArray.length; row++) {
            if (selectedCompareValue =='00000000-0000-0000-0000-000000000000') //For direct properties 
            {   strCompareValue = attributeArray[row][1];
                selectedCompareValue =selectedValue;
            }
            else
            {
                strCompareValue= attributeArray[row][0].split('|')[0];              
            }
            if (selectedCompareValue ==strCompareValue) {
                str = str + "<OPTION SELECTED value=\"" + attributeArray[row][0] + "\" title=\""+ attributeArray[row][1] +"\" >" + attributeArray[row][1] + "</OPTION>";
                var selectedVal = attributeArray[row][0];

                var sValArr = selectedVal.split('|');

                if (sValArr.length > 1) {
                    selectedDataType = sValArr[1];
                }

                //selectedDataType = 
                // FillOperators( dataItem.ClientId,row); 
            }
            else {
                str = str + "<OPTION value=\"" + attributeArray[row][0] + "\" title=\""+ attributeArray[row][1] +"\" >" + attributeArray[row][1] + "</OPTION>";
            }
        }
        str = str + "</Select></div><br><div class='required' style='display:none; font-weight:bold; margin-left:247px; color:red; font-size:large;'>*</div>";
        return str;
    }

    function GenerateOperatorsDropDown<%=this.ClientID %>(dataItem, selectedValue) {
        var strOperator;


        var str;
        if (selectedDataType == '' || selectedDataType == 'undefined' || selectedDataType == "System.Boolean") {
            strOperator = "=|equal,!=|not equal";
        }
        else if( selectedDataType == "string" || selectedDataType == "System.String")
        {
            strOperator = "=|equal,!=|not equal,like|contains";
        }
        else {
            strOperator = "=|equal,!=|not equal,<|less than,>|greater than,<=|less than or equal to,>=|greater than or equal to";
        }
        selectedDataType = '';
        var operatorArr = strOperator.split(",");
        str = "<Select style='width:100px;' id='<%=this.ClientID %>ddlOperator_" + dataItem.get_index() + "_" + dataItem.getCurrentMember().get_column().get_dataField() + "'  ";
        str = str + "onBlur=\"saveCell<%=this.ClientID %>('" + dataItem.ClientId + "', '" + dataItem.getCurrentMember().get_column().get_dataField() + "',this.options[this.selectedIndex].text); saveDDLOperatorValue<%=this.ClientID %>('" + dataItem.ClientId + "',this.options[this.selectedIndex].value) \">";
        str = str + "<OPTION value=\"Select\"><asp:Localize runat='server' Text='<%$ Resources:GUIStrings, Select %>' /></OPTION>";

        var optionItem;
        for (i = 0; i < operatorArr.length; i++) {
            optionItem = operatorArr[i].split('|');
            if (optionItem.length > 1) {
                if (selectedValue == optionItem[1]) {
                    str = str + "<OPTION SELECTED value='" + optionItem[0] + "'>" + optionItem[1] + "</OPTION>";
                }
                else {
                    str = str + "<OPTION value='" + optionItem[0] + "'>" + optionItem[1] + "</OPTION>";
                }

            }
        }
        str = str + "</Select>";
        return (str);
    }

    function GenerateLogicalOperatorDropDown<%=this.ClientID %>(dataItem, selectedValue) {
        var str;
        var strOperator = "--,OR,AND";
        str = "<Select id='<%=this.ClientID %>ddlBoolOpr_" + dataItem.get_index() + "_" + dataItem.getCurrentMember().get_column().get_dataField() + "' onchange=\"saveCell<%=this.ClientID %>('" + dataItem.ClientId + "', '" + dataItem.getCurrentMember().get_column().get_dataField() + "',this.options[this.selectedIndex].text); boolOperatorChangeEvent<%=this.ClientID %>(" + DataItem.get_index() + ",this.options[this.selectedIndex].text,'" +   DataItem.getCurrentMember().get_value() +"',this );\"  ";
        str = str + "onBlur=\"saveCell<%=this.ClientID %>('" + dataItem.ClientId + "', '" + dataItem.getCurrentMember().get_column().get_dataField() + "',this.options[this.selectedIndex].text);\">";
        var operatorArr = strOperator.split(",");
        for (i = 0; i < operatorArr.length; i++) {
            if (selectedValue == operatorArr[i]) {
                str = str + "<OPTION SELECTED value=" + operatorArr[i] + ">" + operatorArr[i] + "</OPTION>";
            }
            else {
                str = str + "<OPTION value=" + operatorArr[i] + ">" + operatorArr[i] + "</OPTION>";
            }
        }
        str = str + "</Select>";

        return str;

    }

    function getFieldSelectValue<%=this.ClientID %>(control) {
        var ddlFieldControl = document.getElementById(control);
        var selectedValue = ddlFieldControl.value;
        return [selectedValue, selectedValue];
    }


    function FillOperators<%=this.ClientID %>(itemId, index) {
        var row = <%=grdFilter.ClientID%>.GetRowFromClientId(itemId);
        var datafield = <%=grdFilter.ClientID%>.Table.Columns[3].DataField;

        var elementId = "<%=this.ClientID %>ddlOperator_" + index + "_" + datafield;
        PopulateOperatorDropDown<%=this.ClientID %>(elementId, index, row);
    }



    function boolOperatorChangeEvent<%=this.ClientID %>(rowNumber, selectedValue, prevval,sender) {

        var filterCondition;           
        if (rowNumber == <%=grdFilter.ClientID%>.get_table().getRowCount() - 1) {
            <%=grdFilter.ClientID%>.get_table().addRow();
            filterCondition = document.getElementById("<%=txtAdvancedFilter.ClientID %>").value;
            filterCondition = filterCondition + " " + selectedValue + " " + (rowNumber + 2);
            document.getElementById("<%=txtAdvancedFilter.ClientID %>").value = filterCondition;
                
            //Disable all open parenthesis and enable close parenthesis from current row and above 
            if(IsOpenParenthesisClicked == true && OpenParenthesisIndex >0)
            {
                var openInpBtn;
                var closeInpBtn;
                for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
                    openInpBtn = document.getElementById("openParenthesis" + i);
                    if (openInpBtn != null) {
                        openInpBtn.disabled = true;
                        openInpBtn.className = 'button disabled';
                    }

                    closeInpBtn = document.getElementById("closeParenthesis" + i);
                    if (closeInpBtn != null) {
                        if (i >= (OpenParenthesisIndex - 1)) {
                            closeInpBtn.disabled = false;
                            closeInpBtn.className = 'button';
                        }
                        else {
                            closeInpBtn.disabled = true;
                            closeInpBtn.className = 'button disabled';
                        }
                    }
                        

                }
            }

                
        }
        else {
            if (selectedValue != defaultLogicalOperator) {
                filterCondition = document.getElementById("<%=txtAdvancedFilter.ClientID %>").value ;
                var startIndex = filterCondition.indexOf(rowNumber+1);
                var endIndex = filterCondition.indexOf(rowNumber+2);
                var replacestring;
                    
                if(startIndex >=0  && endIndex >0 && endIndex >startIndex)
                {
                        
                    replacestring = filterCondition.slice(startIndex, (endIndex+(("'"+(rowNumber+2)+"'").length-2)));
                       
                    var countOpen=0;
                    var countClose=0;
                    for(var i=0; i<replacestring.length;i++)
                    {                            
                        if(replacestring.substr(i,1) == ")")
                            countClose = countClose +1;
                                
                        if(replacestring.substr(i,1) == "(")
                            countOpen =countOpen +1;
                    }
                        
                    var newReplaceString = (rowNumber+1);
                    //construct if any close parenthesis
                    for(var cp=1; cp<=countClose;cp++)
                    {
                        newReplaceString += ")"
                    }                       
                        
                    newReplaceString +=" " + selectedValue +" " ;
                       
                    //construct if any open parenthesis
                    for(var op=1; op<=countOpen;op++)
                    {
                        newReplaceString += "("
                    } 
                        
                    newReplaceString += (rowNumber+2);
                        
                        
                    filterCondition = filterCondition.replace(replacestring,  newReplaceString); 
                }              


                document.getElementById("<%=txtAdvancedFilter.ClientID %>").value = filterCondition;
                }
               
            }
        renderComboBox();

        }

        function Callback1_OnCallbackComplete(sender, eeventargs) {
            isTreeCallback = 'false';
            renderComboBox();
        }
        
        var IsOpenParenthesisClicked =false;
        var OpenParenthesisIndex =0;
        function openParenthesisClick<%=this.ClientID %>(sender, index) {
            var txtFilterCond = document.getElementById("<%=txtAdvancedFilter.ClientID %>");
            
            IsOpenParenthesisClicked =true;
            OpenParenthesisIndex = index;
            var startPos = txtFilterCond.value.indexOf(index);

            if (startPos >= 0) {
                txtFilterCond = txtFilterCond.value.replace(index, "(" + index);
            }

            document.getElementById("<%=txtAdvancedFilter.ClientID %>").value = txtFilterCond;

            //Disable all open parenthesis and enable close parenthesis from current row and above 
            var openInpBtn;
            var closeInpBtn;
            for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
                openInpBtn = document.getElementById("openParenthesis" + i);
                if (openInpBtn != null) {
                    openInpBtn.disabled = true;
                    openInpBtn.className = 'button disabled';
                }

                closeInpBtn = document.getElementById("closeParenthesis" + i);
                if (closeInpBtn != null) {
                    if (i >= (index - 1)) {
                        closeInpBtn.disabled = false;
                        closeInpBtn.className = 'button';
                    }
                    else {
                        closeInpBtn.disabled = true;
                        closeInpBtn.className = 'button disabled';
                    }
                }

            }
        }
        function closeParenthesisClick<%=this.ClientID %>(sender, index) {
        var txtFilterCond = document.getElementById("<%=txtAdvancedFilter.ClientID %>");

            txtFilterCond = txtFilterCond.value.replace(index, index + ")");

            document.getElementById("<%=txtAdvancedFilter.ClientID %>").value = txtFilterCond;

            //enable all open parenthesis and disable all close parenthesis 
            var openInpBtn;
            var closeInpBtn;
            for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
            openInpBtn = document.getElementById("openParenthesis" + i);
            if (openInpBtn != null) {
                openInpBtn.disabled = false;
                openInpBtn.className = 'button';
            }

            closeInpBtn = document.getElementById("closeParenthesis" + i);
            if (closeInpBtn != null) {
                closeInpBtn.disabled = true;
                closeInpBtn.className = 'button disabled';
            }

        }
            
        IsOpenParenthesisClicked =false;

    }
    function ClearParenthesis<%=this.ClientID %>() {
        var txtFilterCond = document.getElementById("<%=txtAdvancedFilter.ClientID %>");

        txtFilterCond = txtFilterCond.value.replace(/\(/g, "").replace(/\)/g, "");
        document.getElementById("<%=txtAdvancedFilter.ClientID %>").value = txtFilterCond;
            
        //enable all open parenthesis and disable all close parenthesis 
        var openInpBtn;
        var closeInpBtn;
        for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
            openInpBtn = document.getElementById("openParenthesis" + i);
            if (openInpBtn != null) {
                openInpBtn.disabled = false;
                openInpBtn.className = 'button';
            }

            closeInpBtn = document.getElementById("closeParenthesis" + i);
            if (closeInpBtn != null) {
                closeInpBtn.disabled = true;
                closeInpBtn.className = 'button disabled';
            }

        }
            
        IsOpenParenthesisClicked =false;

        return false;
    }

    function DeleteRow<%=this.ClientID %>(dataItemClientId, index) {
        if(<%=grdFilter.ClientID%>.get_table().getRowCount() == 1) { isLastRowDelete= true;}
            <%=grdFilter.ClientID%>.deleteItem(<%=grdFilter.ClientID%>.getItemFromClientId(dataItemClientId));
                       
            var filterCondition = ''; //= document.getElementById("<%=txtAdvancedFilter.ClientID %>").value;

            if (index > 0 && index == (<%=grdFilter.ClientID%>.get_table().getRowCount())) {
                var lastRow = <%=grdFilter.ClientID%>.get_table().getRow(<%=grdFilter.ClientID%>.get_table().getRowCount() - 1);
                
                lastRow.setValue(5, defaultLogicalOperator, true);
            }

            for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
                var row = <%=grdFilter.ClientID%>.get_table().getRow(i);

                if(row.getMember('LogicalOperator').get_text() != defaultLogicalOperator)
                {
                    filterCondition = filterCondition + (i + 1) + " " + row.getMember('LogicalOperator').get_text() + " ";
                }
                else
                {
                    if( i == <%=grdFilter.ClientID%>.get_table().getRowCount()-1)
                        filterCondition = filterCondition + (i + 1) ;
                    else
                        filterCondition = filterCondition + (i + 1) + " OR "; // if -- is selected put default OR
                        
                }
            }

            if (index == 0 && <%=grdFilter.ClientID%>.get_table().getRowCount() < 1) {
                <%=grdFilter.ClientID%>.get_table().addRow();
                filterCondition = '1';
            }

            IsOpenParenthesisClicked =false;
            filterCondition = filterCondition.replace(defaultLogicalOperator, '');

            document.getElementById("<%=txtAdvancedFilter.ClientID %>").value = Trim(filterCondition);
        renderComboBox();

        }

        function ValidateAttributeValues<%=this.ClientID %>(executeSave, ctlType) {
        var errorMsg = '';
        var rowMsg = '';

        var row;
        var attrDatatype;
        var attrField;
        var attrOperator;
        var attrValue;            
        var rowcount = <%=grdFilter.ClientID%>.get_table().getRowCount();
            

        for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
                rowMsg = "In Row " + (i + 1) + ",\n";
                row = <%=grdFilter.ClientID%>.get_table().getRow(i);

            //alert("ddlField_" + i + row.getMember(fieldColumnNo).get_column().get_dataField());

                var ddlField = document.getElementById("<%=this.ClientID %>ddlField_" + i + "_" + row.getMember(fieldColumnNo).get_column().get_dataField());
            if (ddlField != null && ddlField.value != null) {
                var fieldVal = ddlField.value.split("|");
                if (fieldVal.length > 1)
                    attrDatatype = fieldVal[1];
            }
            else {
                attrDatatype = row.getMember(attributeDatatypeColumnNo).get_text();
            }

            attrField = Trim(row.getMember("AttributeName").get_text());
            attrOperator = Trim(row.getMember("Operator").get_text());
            attrValue = Trim(row.getMember("AttributeValue").get_text());

            var dateValidatorControl = document.getElementById('<%=valDateFormat.ClientID %>');
            var intValidatorControl = document.getElementById('<%=valIntegerFormat.ClientID %>');
            var doubleValidatorControl = document.getElementById('<%=valDoubleFormat.ClientID %>');

            var dummyTextBox = document.getElementById('<%=dummyTxtAttribValue.ClientID %>');
            
            dummyTextBox.value = attrValue; // for the purpose of validation


            if (attrField == '' || attrField == 'Select') {
                rowMsg += "\t <asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseSelectTheField %>' />\n";
            }

            if (attrOperator == '' || attrOperator == 'Select') {
                rowMsg += "\t <asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseSelectTheOperator %>' />\n";
            }

            if (attrValue == '') {
                rowMsg += "\t <asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ValueCanNotBeEmptyPleaseEnterTheValue %>' /> \n";
                }
                else if (attrField != '' && attrField != 'Select' && attrValue != '') {
                    attrDatatype = attrDatatype.replace('System.', '');
                    if (attrDatatype == "Int16" || attrDatatype == "Int32" || attrDatatype == "Int64") {

                        ValidatorEnable(intValidatorControl, true);
                        if (!intValidatorControl.isvalid) {
                            rowMsg += "\t " + stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsOfDatatypeIntegerPleaseEnterAValidIntegerData %>' />", attrField) + ")\n";
                    }
                }
                else if (attrDatatype == "Boolean") {
                    //if (attrValue.toLowerCase() != 'false' && attrValue.toLowerCase() != 'true') {
                    if (attrValue != 'true' && attrValue != 'false') {
                        rowMsg += "\t " + stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsOfDatatypeBooleanOnlyFalseAndTrueAreAcceptedValuesForBooleanDataType %>' />", attrField) + ")\n";
                    }
                }
                else if (attrDatatype == "DateTime") {
                    ValidatorEnable(dateValidatorControl, true);
                    if(!dateValidatorControl.isvalid)
                    {                           
                        rowMsg += "\t " + stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsOfDatatypeDatePleaseEnterAValidDateValue %>' />", attrField) + ")\n";
                }
            }
            else if (attrDatatype == "Double") {
                ValidatorEnable(doubleValidatorControl, true);
                if (!doubleValidatorControl.isvalid) 
                {
                    rowMsg += "\t " + stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsOfDatatypeDoublePleaseEnterAValidDecimalValue %>' />", attrField) + ") \n";
            }
        }
        else if(attrDatatype == "Guid") 
        {
            if(attrValue.length !=36)
            {
                rowMsg += "\t " + stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsOfDatatypeGuidPleaseEnterAValidGuidValue %>' />", attrField) + ") \n";
            }
            else if(attrValue ==emptyGuid)
            {
                rowMsg += "\t " + stringformat("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, CanNotBeEmptyGuidPleaseEnterAValidGuidValue %>' />", attrField) + ") \n";  
                }
                        
                        
        }
                    
}

    if (rowMsg != ("In Row " + (i + 1) + ",\n")) {
        errorMsg += rowMsg;
    }
    ValidatorEnable(intValidatorControl, false);
    ValidatorEnable(dateValidatorControl, false);
    ValidatorEnable(doubleValidatorControl, false);



}

            //This is to allow saving when  there is only one filter condition, remove and save to work
    if(isLastRowDelete==true && rowcount==1 &&  (attrOperator == '' || attrOperator == 'Select' || attrOperator == '' || attrOperator == 'Select' || attrValue == '')) { isLastRowDelete=false;errorMsg = '';}
            
    if(IsOpenParenthesisClicked == true)
    {
        errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseCloseParenthesisProperly %>' />";
    }
            
    if(document.getElementById("<%=txtMaxRecord.ClientID %>")!=null && document.getElementById("<%=txtMaxRecord.ClientID %>")!=undefined)
            {
                if(document.getElementById("<%=txtMaxRecord.ClientID %>").value=="")
            document.getElementById("<%=txtMaxRecord.ClientID %>").value="-1";
                  
                  
                if(document.getElementById("<%=txtMaxRecord.ClientID %>").value!="-1" )
        {
            var intvalue = parseInt(document.getElementById("<%=txtMaxRecord.ClientID %>").value);
                  
                    if(intvalue.toString() == "NaN")
                        errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterValidMaxNoOfRecord %>' />";
                }   
                
            }
            
            if (errorMsg == '') {
                if (executeSave == true) {
                    SaveFilter<%=this.ClientID %>(ctlType);
                    return false;
                }
                else {
                    return true
                }
            }
            else {
                alert(errorMsg);
                return false;
            }
        }


        function ConcatenateAllFilterItems<%=this.ClientID %>() {
        var row;
        var attrDatatype;
        var attrField;
        var attrOperator;
        var attrValue;
        var gridFilterItems = '';
        var attrId;
        var attrDatatype;

        for (var i = 0; i < <%=grdFilter.ClientID%>.get_table().getRowCount(); i++) {
            row = <%=grdFilter.ClientID%>.get_table().getRow(i);
            attrField = Trim(row.getMember("AttributeName").get_text());
            attrOperator = Trim(row.getMember("Operator").get_text());
            attrValue = "'" + Trim(row.getMember("AttributeValue").get_text()) + "'";
            attrId = Trim(row.getMember(attributeIdColumnNo).get_text());

                
            var ddlField = document.getElementById("<%=this.ClientID %>ddlField_" + i + "_" + row.getMember(fieldColumnNo).get_column().get_dataField());
            if (ddlField != null && ddlField.value != null) {
                var fieldVal = ddlField.value.split("|");
                if (fieldVal.length > 1)
                    attrDatatype = fieldVal[1];
            }
            else {
                attrDatatype = row.getMember(attributeDatatypeColumnNo).get_text();
            }

            if(typeof(attrDatatype) !="undefined")
            {   
                attrDatatype = attrDatatype.replace('System.', '');
                gridFilterItems += (attrId + "|" + attrField + "|" + attrOperator + "|" + attrValue.replace(/\,/g, "&Comma;").replace(/\|/g, "&Pipe;") + "|" + attrDatatype + ",");
            }
        }


        return gridFilterItems;

    }


    function PreviewProducts<%=this.ClientID %>(itemId) {
        //StoreInSession
        if(ValidateAttributeValues<%=this.ClientID %>(false))
        {
            var gridFilterItems = ConcatenateAllFilterItems<%=this.ClientID %>();
                
            StoreInSession("GridFilterItems", gridFilterItems);
            if (document.getElementById("<%=txtAdvancedFilter.ClientID %>") != null && document.getElementById("<%=txtAdvancedFilter.ClientID %>").value != null)
                StoreInSession("FilterCondition", document.getElementById("<%=txtAdvancedFilter.ClientID %>").value);
                    
            StoreInSession("OrderByProperty", document.getElementById("<%=ddlSortBy.ClientID %>").value);
                    
                

            var previewProductsPath = '../../Popups/StoreManager/PreviewProducts.aspx?navFilterId=' + navFilterId;
            pagepopup = dhtmlmodal.open('PreviewProducts', 'iframe', previewProductsPath, '', 'width=900px,height=515px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function() {
                var a = document.getElementById('PreviewProducts');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                
                return true;
            }
        }

        return false;
    }

    var currentRowTextBoxId;
    var currentRow;

    function OpenValueLookUpPopup<%=this.ClientID %>(txtId, itemId) {
        var row = <%=grdFilter.ClientID%>.GetRowFromClientId(itemId);
        var attrId = row.getMember(attributeIdColumnNo).get_text();
        $("#"+txtId).closest("tr").find(".required").hide();
        
       
        var attrTitle = row.getMember(fieldColumnNo).get_text();
        var attrValue = row.getMember(attributeValueColumnNo).get_text();
        currentRowTextBoxId = txtId;
        currentRow =row;

        if((attrId == emptyGuid || attrId.length < 36) && attrTitle == "")
        {            
            $("#"+txtId).closest("tr").find(".required").toggle();
            return;
        }


        var valueLookUpPath = '../../Popups/StoreManager/ProductAttributeSearch.aspx?AttributeId=' + attrId + '&AttributeTitle=' + attrTitle;
        if(attrTitle== 'ProductTypeID') 
        {
            valueLookUpPath= '../../Popups/StoreManager/ProductTypeValuePopup.aspx?AttributeValue=' + attrValue;
            pagepopup = dhtmlmodal.open('ValueLookup', 'iframe', valueLookUpPath, '', 'width=250px,height=510px,center=1,resize=0,scrolling=1');
        }
        else
            pagepopup = dhtmlmodal.open('ValueLookup', 'iframe', valueLookUpPath, '', 'width=460px,height=500px,center=1,resize=0,scrolling=1');
                
        pagepopup.onclose = function() {
            var a = document.getElementById('ValueLookup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");            
            return true;
        }

    }

    function AssignValueToGridTextBox(value, displaytext) {            
        if (currentRowTextBoxId != null && currentRowTextBoxId != 'undefined') {
            var txt = document.getElementById(currentRowTextBoxId);
            if (txt != null)
            {
                txt.value = displaytext;     
                currentRow.setValue(attributeValueColumnNo,value,true);    
                currentRow.setValue(attributeValueForDisplayColumnNo,displaytext,true);           
            }
        }        
        renderComboBox();
    }

    function ResetFilter<%=this.ClientID %>() {
        //Callback1
        isLastRowDelete = false;
        updateNavFilter<%=this.ClientID %>(currentNavIdFilterId);
        return false;
    }

    function SaveFilter<%=this.ClientID %>(ctlType) {
        var gridFilterItems = ConcatenateAllFilterItems<%=this.ClientID %>();
            var filterCondition = document.getElementById("<%=txtAdvancedFilter.ClientID %>").value;
        var orderByProperty = document.getElementById("<%=ddlSortBy.ClientID %>").value;
        var result;
            
        if (ctlType == 'None') {
            result = SaveNavFilter(gridFilterItems, filterCondition, orderByProperty);
        }
        else if (ctlType == 'ProductRelation') {
            result= SaveNavProductRelationFilter(gridFilterItems, filterCondition, orderByProperty,document.getElementById("<%=ddlRelationType.ClientID %>").value,document.getElementById("<%=txtMaxRecord.ClientID %>").value);
            }
            else {
                alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SaveMethodNotImplemented %>" />');
            }
            
            
            
        if(result=='True')
        {
            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, FilterSavedSuccessfully %>' />");
        }
        else
        {   
            alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ErrorWhileSavingFilter %>' />");
        }

        RefreshTree();

    }
	
    document.write('<div id="tooltip" style="Z-Index:2000; BORDER-RIGHT: #666666 1px solid; PADDING-RIGHT: 1px; BORDER-TOP: #666666 1px solid; PADDING-LEFT: 1px; VISIBILITY: hidden; PADDING-BOTTOM: 1px; FONT: 10px/12px Arial,Helvetica,sans-serif; BORDER-LEFT: #666666 1px solid; COLOR: #333333; PADDING-TOP: 1px; BORDER-BOTTOM: #666666 1px solid; POSITION: absolute; BACKGROUND-COLOR: #ffffff; layer-background-color: #ffffcc"></div>'); var posX = 0;
    var posY = 0;
    function showToolTip(x) {

        var lobjCurrentDropdown = window.event.srcElement;
        posX = getX(lobjCurrentDropdown);
        posY = getY(lobjCurrentDropdown);
        if (x == 1) { //A tooltip must be shown
            if (lobjCurrentDropdown.ToolText != '' && lobjCurrentDropdown.ToolText != null)
                document.all.tooltip.innerHTML = lobjCurrentDropdown.ToolText;
            else {
                if (lobjCurrentDropdown.length > 0)
                    if (lobjCurrentDropdown.selectedIndex >= 0)
                        document.all.tooltip.innerHTML = lobjCurrentDropdown.options[lobjCurrentDropdown.selectedIndex].text;
                    else
                        document.all.tooltip.innerHTML = '';
                else
                    document.all.tooltip.innerHTML = '';
            }
            document.all.tooltip.style.left = posX + 2;
            document.all.tooltip.style.top = posY - 20;
            document.all.tooltip.style.visibility = "visible";
        }
        else { //A tootip must not be shown
            document.all.tooltip.style.visibility = "hidden";
        }
    }


    function getX(obj) {
        return (obj.offsetParent == null ? obj.offsetLeft : obj.offsetLeft + getX(obj.offsetParent));
    }
    function getY(obj) {
        return (obj.offsetParent == null ? obj.offsetTop : obj.offsetTop + getY(obj.offsetParent));
    }

    </script>

    <asp:Panel ID="pnlProdRelationType" runat="server" CssClass="tab-intro">
    <div class="form-row">
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectARelationTypeFromTheDropDownAndDefineTheRulesForTheRelation %>" />
    </div>
    <div class="form-row clear-fix">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RelationType %>" /></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlRelationType" runat="server" onChange="onddlRelationType_change(this.options[selectedIndex].value);">
            </asp:DropDownList>
            <label id="lblRelationType" title="<%$ Resources:GUIStrings, RelationType1 %>"
                runat="server">
            </label>
        </div>
    </div>
</asp:Panel>
<ComponentArt:CallBack ID="CallBack1" runat="server" OnCallback="Callback1_onCallBack"
    EnableViewState="true" CacheContent="false" PostState="false" CssClass="nav-filter">
    <Content>
        <ComponentArt:Grid ID="grdFilter" AllowTextSelection="true" EnableViewState="true"
            EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False" KeyboardEnabled="false"
            ShowFooter="false" SkinID="Default" RunningMode="Client" Width="100%" Height="350"
            runat="server" PageSize="100">
            <Levels>
                <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="Row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                    HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="Row"
                    GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" AllowSorting="false" SortImageHeight="7" AllowGrouping="false"
                    AlternatingRowCssClass="AlternateDataRow" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
                    EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
                    <Columns>
                        <ComponentArt:GridColumn DataCellClientTemplateId="EditCellLabelTemplate" Width="14"
                            Align="Center" AllowReordering="False" FixedWidth="true" HeadingCellCssClass="no-border"
                            DataCellCssClass="no-border" />
                        <ComponentArt:GridColumn DataCellClientTemplateId="EditCellOpenParenthesisTemplate"
                            Align="Center" AllowReordering="False" FixedWidth="true" Width="16" />
                        <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Field %>" DataField="AttributeName"
                            DataCellClientTemplateId="EditFieldDropDownList" Width="255" AllowReordering="False"
                            FixedWidth="true" HeadingCellCssClass="heading-cell" />
                        <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Operator %>" DataField="OperatorTitle"
                            DataCellClientTemplateId="EditOperatorDropDownList" Width="100" AllowReordering="False"
                            FixedWidth="true" HeadingCellCssClass="heading-cell" />
                        <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Value %>" DataField="AttributeValueForDisplay"
                            DataCellClientTemplateId="EditCellTemplate" Width="255" AllowReordering="False"
                            FixedWidth="true" HeadingCellCssClass="heading-cell" />
                        <ComponentArt:GridColumn HeadingText=" " DataField="LogicalOperator" DataCellClientTemplateId="EditLogicalDropdown"
                            Width="55" AllowReordering="False" FixedWidth="true" HeadingCellCssClass="no-border"
                            DataCellCssClass="no-border" />
                        <ComponentArt:GridColumn DataCellClientTemplateId="EditCellCloseParenthesisTemplate"
                            Width="20" AllowReordering="False" FixedWidth="true" HeadingCellCssClass="no-border"
                            DataCellCssClass="no-border" />
                        <ComponentArt:GridColumn DataField="AttributeId" Visible="false" FixedWidth="true" />
                        <ComponentArt:GridColumn DataField="Operator" Visible="false" FixedWidth="true" />
                        <ComponentArt:GridColumn DataCellClientTemplateId="DeleteCellTemplate" Width="16"
                            AllowReordering="False" FixedWidth="true" DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell no-border" />
                        <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, AttributeDataType %>"
                            Visible="false" FixedWidth="true" />
                        <ComponentArt:GridColumn DataField="AttributeValue" Visible="false" FixedWidth="true" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="EditCellLabelTemplate">
                    <label>
                        ## DataItem.get_index()+1 ##</label>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="serverDDL">
                    <Template>
                        <asp:DropDownList ID="ddlField" runat="server" Width="160" AutoPostBack="false">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
                        </asp:DropDownList>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:Grid>
        <div class="columns" id="leftSection" runat="server">
            <asp:Label ID="lblSearchLabel" runat="server"><p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AdvancedFilterConditions %>" /></p></asp:Label>
            <asp:TextBox ID="txtAdvancedFilter" runat="server" CssClass="textBoxesDisabled" Width="350"
                onfocus="this.blur();" Enabled="true"></asp:TextBox>
        </div>
        <div class="columns" id="dvMaxRecSection" runat="server">
            <asp:Label ID="lblMaxRecord" runat="server"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MaxProductsToShow %>" /></asp:Label><br />
            <asp:TextBox ID="txtMaxRecord" runat="server" Width="115" Enabled="true" CssClass="textBoxes"></asp:TextBox>
        </div>
        <div class="columns" id="rightSection" runat="server">
            <asp:Label ID="lblSortBy" runat="server"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SortBy %>" /></asp:Label><br />
            <asp:DropDownList ID="ddlSortBy" runat="server" Width="160" AutoPostBack="false">
            </asp:DropDownList>
        </div>
    </Content>
    <ClientEvents>
        <CallbackComplete EventHandler="Callback1_OnCallbackComplete" />
    </ClientEvents>
</ComponentArt:CallBack>
<asp:TextBox CssClass="textBoxes" ID="dummyTxtAttribValue" runat="server" Style="display: none">
</asp:TextBox>
<asp:CompareValidator ID="valDateFormat" runat="server" ControlToValidate="dummyTxtAttribValue"
    Operator="DataTypeCheck" Type="Date" CultureInvariantValues="true" ErrorMessage="*"
    Display="None" Text="*" Enabled="false">                                                 
</asp:CompareValidator>
<asp:CompareValidator ID="valDoubleFormat" runat="server" ControlToValidate="dummyTxtAttribValue"
    Operator="DataTypeCheck" Type="Double" CultureInvariantValues="true" ErrorMessage="*"
    Display="None" Text="*" Enabled="false">                                                 
</asp:CompareValidator>
<asp:CompareValidator ID="valIntegerFormat" runat="server" ControlToValidate="dummyTxtAttribValue"
    Operator="DataTypeCheck" Type="Integer" CultureInvariantValues="true" ErrorMessage="*"
    Display="None" Text="*" Enabled="false">                                                 
</asp:CompareValidator>
<asp:PlaceHolder ID="phRowHolder" runat="server"></asp:PlaceHolder>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="<%$ Resources:GUIStrings, PleaseFillAllTheRequiredFieldsToSaveTheFilter %>"
    ShowSummary="false" ShowMessageBox="true" />
<div class="clear-fix">
</div>
<div class="tab-footer">
    <asp:Button ID="btnPreview" runat="server" Text="<%$ Resources:GUIStrings, PreviewProducts %>"
        ToolTip="<%$ Resources:GUIStrings, PreviewProducts %>" CssClass="button" />
    <asp:Button ID="btnClearParenthesis" runat="server" Text="<%$ Resources:GUIStrings, ClearParenthesis %>"
        ToolTip="<%$ Resources:GUIStrings, ClearParenthesis %>" CssClass="button"
        CausesValidation="false" />
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Reset %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
        CausesValidation="false" />
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        ToolTip="<%$ Resources:GUIStrings, Save %>" runat="server" CausesValidation="true" />
</div>
<asp:HiddenField ID="hfData" runat="server" />
