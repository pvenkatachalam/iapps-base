﻿c<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FilterCriteria.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.FilterCriteria" %>
<h3><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Filters %>" /></h3>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<div class="NavigationFilter">
<script>
    function updateControls(str)
    {
    
        __doPostBack('<%=UpdatePanel1.ClientID %>',"UpdateFilter," + str);
    }
</script>
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
         
    <div class="formRow">
        <div class="firstField">&nbsp;</div>
        <div class="secondField"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Field %>" /></div>
        <div class="thirdField"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Operator %>" /></div>
        <div class="fourthField"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Value %>" /></div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server"  OnLoad ="UpdatePanel1_Load"  >
        <ContentTemplate>  
    <div class="formRow">
        <div class="firstField">1.</div>
        <div class="secondField">
            <asp:DropDownList ID="ddlField1" runat="server" Width="190">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField1" runat="server" ControlToValidate ="ddlField1" Display="None"></asp:RequiredFieldValidator>
        </div>
        <div class="thirdField">
            <asp:DropDownList ID="ddlOperator1" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator1" runat="server" ControlToValidate="ddlOperator1"></asp:RequiredFieldValidator>
        </div>
        <div class="fourthField">
            <asp:TextBox ID="txtValue1" runat="server" CssClass="textBoxes" Width="130"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue1" runat="server" ControlToValidate="txtValue1"></asp:RequiredFieldValidator>
        </div>
        <div class="fifthField">
            <asp:DropDownList ID="ddlCatalog1" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="formRow">
        <div class="firstField">2.</div>
        <div class="secondField">
            <asp:DropDownList ID="ddlField2" runat="server" Width="190">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField2" runat="server" ControlToValidate="ddlField2"></asp:RequiredFieldValidator>
        </div>
        <div class="thirdField">
            <asp:DropDownList ID="ddlOperator2" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator2" runat="server" ControlToValidate="ddlOperator2"></asp:RequiredFieldValidator>
        </div>
        <div class="fourthField">
            <asp:TextBox ID="txtValue2" runat="server" CssClass="textBoxes" Width="130"></asp:TextBox>  
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue2" runat="server" ControlToValidate="txtValue2" ></asp:RequiredFieldValidator>
        </div>
        <div class="fifthField">
            <asp:DropDownList ID="ddlCatalog2" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="formRow">
        <div class="firstField">3.</div>
        <div class="secondField">
            <asp:DropDownList ID="ddlField3" runat="server" Width="190">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField3" runat="server" ControlToValidate="ddlField3"></asp:RequiredFieldValidator>
        </div>
        <div class="thirdField">
            <asp:DropDownList ID="ddlOperator3" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator3" runat="server" ControlToValidate="ddlOperator3"></asp:RequiredFieldValidator>
        </div>
        <div class="fourthField">
            <asp:TextBox ID="txtValue3" runat="server" CssClass="textBoxes" Width="130"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue3" runat="server" ControlToValidate="txtValue3" ></asp:RequiredFieldValidator>
        </div>
        <div class="fifthField">
            <asp:DropDownList ID="ddlCatalog3" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="formRow">
        <div class="firstField">4.</div>
        <div class="secondField">
            <asp:DropDownList ID="ddlField4" runat="server" Width="190">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField4" runat="server" ControlToValidate="ddlField4" ></asp:RequiredFieldValidator>
        </div>
        <div class="thirdField">
            <asp:DropDownList ID="ddlOperator4" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator4" runat="server" ControlToValidate="ddlOperator4"></asp:RequiredFieldValidator>
        </div>
        <div class="fourthField">
            <asp:TextBox ID="txtValue4" runat="server" CssClass="textBoxes" Width="130"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue4" runat="server" ControlToValidate="txtValue4"></asp:RequiredFieldValidator>
        </div>
        <div class="fifthField">
            <asp:DropDownList ID="ddlCatalog4" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="formRow">
        <div class="firstField">5.</div>
        <div class="secondField">
            <asp:DropDownList ID="ddlField5" runat="server" Width="190">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlField5" runat="server" ControlToValidate="ddlField5"></asp:RequiredFieldValidator>
        </div>
        <div class="thirdField">
            <asp:DropDownList ID="ddlOperator5" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Select %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvddlOperator5" runat="server" ControlToValidate="ddlOperator5" ></asp:RequiredFieldValidator>
        </div>
        <div class="fourthField">
            <asp:TextBox ID="txtValue5" runat="server" CssClass="textBoxes" Width="130"></asp:TextBox>
            <asp:RequiredFieldValidator CultureInvariantValues="true" ID="rfvtxtValue5" runat="server" ControlToValidate="txtValue5" ></asp:RequiredFieldValidator>
        </div>
        <div class="fifthField">
            <asp:DropDownList ID="ddlCatalog5" runat="server" Width="130">
                <asp:ListItem Text="<%$ Resources:GUIStrings, SelectCatalog %>" Value="Select"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div id="divCascading" runat ="server">
     <ajaxToolkit:CascadingDropDown ID="cddField1" runat="server" TargetControlID="ddlField1"
            Category="Field"  PromptText="<%$ Resources:GUIStrings, Select %>"  LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator1" runat="server" TargetControlID="ddlOperator1"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField1"  />
            
        <ajaxToolkit:CascadingDropDown ID="cddField2" runat="server" TargetControlID="ddlField2"
            Category="Field"  PromptText="<%$ Resources:GUIStrings, Select %>"  LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator2" runat="server" TargetControlID="ddlOperator2"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField2"  />
            
        <ajaxToolkit:CascadingDropDown ID="cddField3" runat="server" TargetControlID="ddlField3"
            Category="Field"  PromptText="<%$ Resources:GUIStrings, Select %>"  LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator3" runat="server" TargetControlID="ddlOperator3"
            Category="Operator" PromptText="Select" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField3"  />
            
        <ajaxToolkit:CascadingDropDown ID="cddField4" runat="server" TargetControlID="ddlField4"
            Category="Field"  PromptText="<%$ Resources:GUIStrings, Select %>"  LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator4" runat="server" TargetControlID="ddlOperator4"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField4"  />
            
        <ajaxToolkit:CascadingDropDown ID="cddField5" runat="server" TargetControlID="ddlField5"
            Category="Field"  PromptText="<%$ Resources:GUIStrings, Select %>"  LoadingText="<%$ Resources:GUIStrings, LoadingFields %>"
            ServiceMethod="GetAttributesForCDD" />
        <ajaxToolkit:CascadingDropDown ID="cddOperator5" runat="server" TargetControlID="ddlOperator5"
            Category="Operator" PromptText="<%$ Resources:GUIStrings, Select %>" LoadingText="<%$ Resources:GUIStrings, LoadingOperators %>"
            ServiceMethod="GetOperatorsForCDD" ParentControlID="ddlField5"  />
    </div>
          
            <asp:PlaceHolder ID="phRowHolder" runat="server"   ></asp:PlaceHolder>           
        </ContentTemplate>
        <Triggers>                               
            <asp:AsyncPostBackTrigger ControlID="lbtnAdd" />   
            <asp:AsyncPostBackTrigger ControlID ="lbtnRemove" />                                  
        </Triggers>
    </asp:UpdatePanel>
    
    <asp:HiddenField ID="hfRowCount" Value="5" runat="server"/>   
    <asp:HiddenField ID="hfUpdatedData"  runat="server"/>        
    <asp:ValidationSummary ID="ValidationSummary1" runat="server"  HeaderText="<%$ Resources:GUIStrings, PleaseFillAllTheFieldsOperatorsAndValuesAndAddNewRow %>"  ShowSummary="false" ShowMessageBox="true"  />
    <div class="formRow">                        
        <asp:LinkButton ID="lbtnAdd" runat="server" Text="<%$ Resources:GUIStrings, AddRow %>" CausesValidation="true" OnClick="lbtnAdd_Click" ></asp:LinkButton>
        <asp:LinkButton ID="lbtnRemove" runat="server" Text="<%$ Resources:GUIStrings, RemoveRow %>" CausesValidation="false" OnClick="lbtnRemove_Click"></asp:LinkButton>
    </div>
    <div class="formRow">
        <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AdvancedFilterConditions %>" /></p>
        <asp:TextBox ID="txtAdvancedFilter" runat="server" CssClass="textBoxes" Width="400"></asp:TextBox>
    </div>
    <div class="footerContent">
        <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" runat="server" 
            CausesValidation="false" onclick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CausesValidation="false"  CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClick="btnCancel_Click" />
    </div>
    
</div>