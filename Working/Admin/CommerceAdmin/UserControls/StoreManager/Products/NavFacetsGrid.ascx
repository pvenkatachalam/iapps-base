﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavFacetsGrid.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.NavFacetsGrid" %>
<script type="text/javascript">
    function SetGridLevelGeneralVariablesinPage() {
        setTimeout(function () {
            gridObject = grdNavFacets;
            menuObject = mnuFacets;
            gridObjectName = "grdNavFacets";
            uniqueIdColumn = 6;
            operationColumn = 4;
        }, 500);
    }
    function PageLevelDragAndDrop(draggedItem, targetItem, fromId, toPosition) {
        //here call the callback method to re-order
        var targetItemId = targetItem.getMember("Id").get_text();
        var result = ReorderNavFacets(selectedNodeId, fromId, targetItemId);
        if (result.toLowerCase() == 'true') {
            loadNavFacetGrid();
        }
        else {
            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ErrorReordering %>" />');
    }
        //then return false it wont call the existing general load method
    return 'false';
}
var facetPopup;
function ShowFacetPopup(isEdit, factId) {
    popupPath = '../../Popups/StoreManager/AddNavFacets.aspx?NavId=' + selectedNodeId;
    facetPopup = dhtmlmodal.open('AddFacet', 'iframe', popupPath, '', 'width=956px,height=485px,center=1,resize=0,scrolling=1');
    facetPopup.onclose = function () {
        var a = document.getElementById('AddFacet');
        var ifr = a.getElementsByTagName('iframe');
        window.frames[ifr[0].name].location.replace("about:blank");
        return true;
    }
}

function ClosePopup() {
    var selectedFacetIDs = window["SelectedFacets"];
    var selectedAttributeId = window["SelectedAttributeId"];
    if (selectedFacetIDs != null && selectedFacetIDs != '' && selectedFacetIDs != "undefined" && selectedAttributeId != null) {
        var retVal = AddNavFacets(selectedNodeId, selectedAttributeId, selectedFacetIDs);
        if (retVal.toLowerCase() == 'true') {
            //alert('Added SuccessFully');
            loadNavFacetGrid();
        }
    }
    facetPopup.hide();
}

function grdNavFacets_onContextMenu(sender, eventArgs) {
    var evt = eventArgs.get_event();
    mnuFacets.showContextMenuAtEvent(evt);
    var selectedGridItem = eventArgs.get_item().getMember("Id").get_text();
    mnuFacets.set_contextData(selectedGridItem);
}




function mnuFacets_onItemSelect(sender, eventArgs) {
    var menuItem = eventArgs.get_item();
    var facetId = menuItem.get_parentMenu().get_contextData();
    var selectedMenuId = menuItem.get_id();
    if (selectedMenuId != null) {
        switch (selectedMenuId) {
            case 'cmFacetsAdd':
                ShowFacetPopup(0, emptyGuid);
                break;
            case 'cmFacetsDelete':
                var userResult;
                userResult = window.confirm('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisItem %>" />');
                    if (userResult == true) {
                        var result = RemoveNavFacets(selectedNodeId, facetId);
                    }
                    if (result.toLowerCase() == 'true') {
                        loadNavFacetGrid(); //loading the grid after delete...
                    }
                    else {
                        alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ErrorDeleteItemActionFailed %>" />' + result);
                    }

                    break;
            }
        }
    }

</script>
<p style="text-align: right; padding: 10px;">
    <asp:CheckBox ID="chkDisplayOrder" runat="server" Text="<%$ Resources:GUIStrings, EnableWebsiteDisplaySorting %>"
        onclick="javascript:RefreshGridForDisplayOrder(this);" />
</p>
<ComponentArt:Menu ID="mnuFacets" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuFacets_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<ComponentArt:Grid SkinID="Default" ID="grdNavFacets" SliderPopupClientTemplateId="navFacetsSliderTemplate"
    SliderPopupCachedClientTemplateId="navFacetsSliderTemplateCached" Width="100%"
    runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" AllowEditing="true"
    AutoCallBackOnInsert="true" PagerInfoClientTemplateId="navFacetsPaginationTemplat"
    CallbackCachingEnabled="true" OnBeforeCallback="grdNavFacets_BeforeCallback"
    EditOnClickSelectedItem="false" ItemDraggingEnabled="true" ExternalDropTargets="grdNavFacets"
    ItemDraggingClientTemplateId="dragTemplate" LoadingPanelClientTemplateId="navFacetsLoadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn Visible="false" HeadingText="<%$ Resources:GUIStrings, OperationColumn %>" />
                <ComponentArt:GridColumn Visible="false" HeadingText="<%$ Resources:GUIStrings, OperationParameterColumn %>" />
                <ComponentArt:GridColumn Visible="false" DataField="Id" />
                <ComponentArt:GridColumn Width="350" HeadingText="<%$ Resources:GUIStrings, AttributeName %>"
                    DataField="AttributeTitle" Align="Left" IsSearchable="true" AllowReordering="false"
                    FixedWidth="true" DataCellClientTemplateId="NameHoverTemplate" />
                <ComponentArt:GridColumn Width="350" HeadingText="<%$ Resources:GUIStrings, FacetName %>"
                    DataField="Title" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="LimitHoverTemplate" />
                <ComponentArt:GridColumn Width="83" HeadingText="<%$ Resources:GUIStrings, DisplayOrder %>"
                    DataField="Sequence" Align="Left" AllowReordering="false" HeadingCellCssClass="LastHeadingCell"
                    DataCellCssClass="LastDataCell" FixedWidth="true" IsSearchable="true" DataCellClientTemplateId="DisplayOrderHoverTemplate" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdNavFacets_onContextMenu" />
        <ItemExternalDrop EventHandler="grid_OnExternalDrop" />
        <Load EventHandler="grid_onLoad" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameHoverTemplate">
            <div title="## DataItem.GetMember('AttributeTitle').get_text() ##">
                ## DataItem.GetMember('AttributeTitle').get_text() == "" ? "&nbsp;" : DataItem.GetMember('AttributeTitle').get_text()
                ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="LimitHoverTemplate">
            <div title="## DataItem.GetMember('Title').get_text() ##">
                ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DisplayOrderHoverTemplate">
            <div title="## DataItem.GetMember('Sequence').get_text() ##">
                ## DataItem.GetMember('Sequence').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Sequence').get_text()
                ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="navFacetsSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdNavFacets.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdNavFacets.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="navFacetsSliderTemplateCached">
            <div class="SliderPopup">
                <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdNavFacets.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdNavFacets.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="dragTemplate">
            <div style="height: 100px; width: 100px;">
                ## DataItem.getMember('Title').get_text() ##
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="navFacetsPaginationTemplat">
            ##stringformat(Page0Of12items, currentPageIndex(grdNavFacets), pageCount(grdNavFacets),
            grdNavFacets.RecordCount)##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="navFacetsLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdNavFacets) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
