﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsPreview.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ProductsPreview" %>
<script type="text/javascript">
    var productListItem;
    var skuId;
    var productId;
    var isBundle = false;
    var ProductIdColumn = 6;
    var SkuIdColumn = 7;
    var IsBundleColumn = 8;

    function mnuProductsPreview_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var contextDataNode = menuItem.get_parentMenu().get_contextData();
        var selectedMenuId = menuItem.get_id();

        if (selectedMenuId != null) {
            switch (selectedMenuId) {
                case 'cmEdit':
                    if (isBundle) {
                        window.location = '../Products/BundleDetails.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
		                                            skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    else {
                        window.location = '../Products/GeneralDetails.aspx?<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_SKU_IDs%>=' +
		                                            skuId + '&<%=Bridgeline.FW.Commerce.UI.NavigationConstants.SELECTED_PRODUCT_ID%>=' + productId;
                    }
                    break;
                case 'cmRemove':
                    ExcludeProductFromNav(selectedNodeId, productId);
                    grdPreviewProducts.callback();
                    break;
            }
        }

    }
    function grdPreviewProducts_onContextMenu(sender, eventArgs) {
        var e = eventArgs.get_event();
        productListItem = eventArgs.get_item();
        productId = productListItem.getMember(ProductIdColumn).get_text();
        skuId = productListItem.getMember(SkuIdColumn).get_text();
        isBundle = productListItem.getMember(IsBundleColumn).get_text().toLowerCase() == "true" ? true : false;

        if (skuId.indexOf(",") > -1)
            skuId = skuId.substring(0, skuId.indexOf(","));

        mnuProductsPreview.showContextMenuAtEvent(e);
    }

</script>
<ComponentArt:Grid SkinID="Default" ID="grdPreviewProducts" runat="server" RunningMode="Callback"
    ManualPaging="true" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoPostBackOnSelect="false" PageSize="10" PagerInfoClientTemplateId="PagerTemplate"
    Width="100%" LoadingPanelClientTemplateId="PreviewProductLoadingPanelTemplate"
    Sort="Name">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn Width="90" HeadingText=" " DataField="Thumbnail" IsSearchable="true"
                    AllowReordering="false" FixedWidth="true" Align="Center" DataCellClientTemplateId="SvtThumbnail"
                    AllowSorting="False" />
                <ComponentArt:GridColumn Width="250" HeadingText="<%$ Resources:GUIStrings, Name %>"
                    DataField="Name" IsSearchable="true" DataCellClientTemplateId="TemplateName"
                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                    Align="Left" TextWrap="true" />
                <ComponentArt:GridColumn Width="205" HeadingText="<%$ Resources:GUIStrings, SKU %>"
                    DataField="SKU" AllowSorting="False" SortedDataCellCssClass="SortedDataCell"
                    TextWrap="true" AllowReordering="false" FixedWidth="true" Align="Left" DataCellClientTemplateId="SKUHover" />
                <ComponentArt:GridColumn Width="160" HeadingText="<%$ Resources:GUIStrings, Description %>"
                    AllowSorting="False" DataField="Description" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    HeadingCellCssClass="HeadingCell" DataCellClientTemplateId="TemplateDesc" AllowReordering="false"
                    Align="Left" Visible="false" />
                <ComponentArt:GridColumn Width="96" HeadingText="<%$ Resources:GUIStrings, DateCreated %>"
                    DataField="LastModifiedDate" FormatString="d" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    DataType="System.DateTime" AllowReordering="false" Align="Left" />
                <ComponentArt:GridColumn Width="80" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataField="Status" FixedWidth="true" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" Align="Left" DataCellCssClass="last-data-cell" HeadingCellCssClass="last-heading-cell" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                <ComponentArt:GridColumn DataField="SkuIds" Visible="false" />
                <ComponentArt:GridColumn DataField="IsBundle" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdPreviewProducts_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="PagerTemplate">
            ##stringformat(Page0Of12items, currentPageIndex(grdPreviewProducts), pageCount(grdPreviewProducts),
            grdPreviewProducts.RecordCount)##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SvtThumbnail">
            <img src="## DataItem.GetMember('Thumbnail').get_value() ##" alt="## DataItem.GetMember('Name').get_value() ##"
                title="## DataItem.GetMember('Name').get_value() ##" />
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TemplateName">
            <span title="## DataItem.GetMember('Name').get_value() ##">## DataItem.GetMember('Name').get_value()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TemplateDesc">
            <span title="## DataItem.GetMember('Description').get_value() ##">## DataItem.GetMember('Description').get_value()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="SKUHover">
            <span title="## DataItem.GetMember('SKU').get_value() ##">## DataItem.GetMember('SKU').get_value()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PreviewProductLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdPreviewProducts) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuProductsPreview" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuProductsPreview_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
