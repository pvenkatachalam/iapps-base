﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavCategoryDetails.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.NavCategoryDetails" %>
<%@ Register TagPrefix="uc" TagName="Grid" Src="~/UserControls/General/ContentGrid.ascx" %>
<script type="text/javascript">
    var curSelectedNavId;
    var defaultContentId;
    var associatedContentFolderId;
    var emptyGuid = '00000000-0000-0000-0000-000000000000'; // empty guid

    function SetNavDefaultContentId(contentId) {
        selectedTreeNode.setProperty('DefaultContentId', contentId);
    }

    function SetNavContentFolderId(contentFolderId) {
        selectedTreeNode.setProperty('ContentFolderId', contentFolderId);
    }

    function UpdateNavCategoryDetails(selectedId) {
        navDetailsCallback.callback(selectedId);
        curSelectedNavId = selectedId;
        associatedContentFolderId = selectedTreeNode.getProperty('ContentFolderId');
        defaultContentId = selectedTreeNode.getProperty('DefaultContentId');
        if (associatedContentFolderId == emptyGuid) {
            alert("No associated content folder found, related contents cannot be created. Please contact your administrator");
            return;
        }
        if (associatedContentFolderId == 'undefined' || associatedContentFolderId == undefined)
            RefreshTree();
        LoadContent(associatedContentFolderId);
        
    }

    function ValidateData(navTitle, navSeoUrl) {
        var errorMsg = ValidateCatName(navTitle, true);

        if (navSeoUrl == null || navSeoUrl == '') {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, SEOFriendlyURLShouldNotBeBlank %>' />\n";

        }
        else if (navSeoUrl.indexOf('/') >= 0) {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsNotAllowedInSEOFriendlyURL %>' />\n";
        }
        else if (navSeoUrl.indexOf('\\') >= 0) {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, IsNotAllowedInSEOFriendlyName %>' />\n ";
        }
        else if (!navSeoUrl.match(/^[a-zA-Z0-9_\-]+$/)) {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, SEOFriendlyURLNameCanHaveOnlyAlphanumericCharacters %>' />\n";
        }

    return errorMsg;
}

function SaveNavDetails() {
    var navTitle = Trim(document.getElementById("<%=txtName.ClientID%>").value);
        var navDescription = document.getElementById("<%=txtDescription.ClientID%>").value;
        var navSeoUrl = Trim(document.getElementById("<%=txtSeoName.ClientID%>").value);

        var errorMsg = ValidateData(navTitle, navSeoUrl);
        if (errorMsg != '') {
            alert(errorMsg);
        }
        else {
            var result = UpdateNavNode(curSelectedNavId, navTitle, navSeoUrl, navDescription);
            if (result == 'True') {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NavNodeDetailsSavedSuccessfully %>' />");
            }
            else {
                alert("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ErrorWhileSavingNavNodeDetails %>' />");
            }
            RefreshTree();
        }
        //UpdateNavNode
        return false;
    }

    function ResetNavDetails() {
        UpdateNavCategoryDetails(curSelectedNavId);

        return false;
    }
</script>
<ComponentArt:CallBack ID="navDetailsCallback" CssClass="tab-content" runat="server"
    OnCallback="navDetailsCallback_Callback">
    <Content>
        <div id="spanName" class="form-row" runat="server">
            <label id="lblName" class="form-label" runat="server">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Name %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <label id="lblSeoUrl" class="form-label" runat="server">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SEOFriendlyName %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtSeoName" runat="server" CssClass="textBoxes" Width="300"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="300"
                    Rows="3"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Template %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtTemplateName" runat="server" CssClass="textBoxes" Width="300"
                    Rows="3" Enabled="false"></asp:TextBox>
            </div>
        </div>
    </Content>
</ComponentArt:CallBack>
<div class="nav-content">
    <uc:Grid ID="ucGrid" runat="server" ApplyContentFilter="true" />
</div>
<div class="tab-footer">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Reset %>"
        ToolTip="<%$ Resources:GUIStrings, Reset %>" CssClass="button"
        OnClientClick="return ResetNavDetails();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        OnClientClick="return SaveNavDetails()" />
</div>
