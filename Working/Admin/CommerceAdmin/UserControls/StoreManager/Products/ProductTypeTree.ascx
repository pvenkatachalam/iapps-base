﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTypeTree.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.ProductTypeTree" %>
<!-- Page Level Tree management code -->
<script type="text/javascript">
    var copyParent = 'false';
    //This has to set first .. to pass the objects to the global script
    function SetTreeLevelGeneralVariablesinPage() {
        setTimeout(function () {
            treeObject = trvProductTypes;
            treeMenuObject = mnuProductTypes;
            treeObjectName = "trvProductTypes";
        }, 500);
    }
    //Calling after node select
    function PageLevelOnNodeSelect(sender, eventArgs) {
        window["SelectedNodeId"] = selectedNodeId;
        ProductTypeTreeNodeSelect(null, eventArgs);
    }
    //Calling before selecting a node
    function PageLevelOnNodeBeforeSelect(sender, eventArgs) {
        return true;
    }
    //Calling begore displaying context menu
    function PageLevelTreeOnContextMenu(menuItems, indexOfSelectedNode) {
        if (indexOfSelectedNode == 0) {
            menuItems.getItem(0).set_text('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AddProductType %>" />');
        }
        else {
            menuItems.getItem(0).set_text('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AddSubProductType %>" />');
        }
        copyParent = 'false';
        return true;
    }
    //Calling after a context menu selected
    function PageLevelTreeOnItemSelect(selectedMenuId, contextDataNode, sender, eventArgs) {
        if (selectedMenuId == 'cmTreeCopyNew') {
            copyParent = 'true';
            AddTreeNode(contextDataNode, treeObject)
        }
    }
    //Calling after adding a empty node. if u want u can change the styles.. etc for the new node here    
    function PageLevelAddEmptyTreeNode(contextMenuData, treeObject, newNode) {
        return true;
    }
    //Calling after click on the remove node context menu
    function PageLevelRemoveTreeNode(contextMenuData) {
        nodeId = contextMenuData.get_id();
        var result = DeleteProductType(nodeId);
        return result;
    }
    //Calling just after starting rename of a node
    function PageLevelRenameTreeNode(contextMenuData) {
        return true;
    }
    //Call after re-loading the Grid
    function PageLevelTreeLoad(sender, eventArgs) {
        return true;
    }

    // call call back method from here, and return the result
    function PageLevelCreateNewNode(newText, parentId, newNode) {
        var result = CreateProductTypeWithCopy(newText, parentId, copyParent);
        copyParent = 'false';
        return result;
    }

    //This will call immediately after creating a node. Instead of this we can select that node in global script, but it may make someother error, eg.it calls tree_onNodeBeforeSelect etc..    
    function PageLevelLoadNewNode(selectedNodeId, eventArgs) {
        window["SelectedNodeId"] = selectedNodeId;
        ProductTypeTreeNodeSelect(null, eventArgs);

        RefreshTree();
    }

    // call call back method from here, and return the result
    function PageLevelEditNode(id, newText, renamedNode) {
        return EditProductType(id, newText);
    }
    // this will call immediately after renamed of the node.
    function PageLevelLoadEditedNode(selectedNodeId, sender, eventArgs) {
        window["SelectedNodeId"] = selectedNodeId;
        ProductTypeTreeNodeSelect(sender, eventArgs);
    }
    // Calling after refresh the grid in callback mode
    function PageLevelRefreshTreeImmediate() {
        if (window["SelectedNodeId"] != null)
            cbTreeRefresh.Callback(window["SelectedNodeId"]);
    }     
</script>
<ComponentArt:CallBack ID="cbTreeRefresh" runat="server" OnCallback="cbRefreshTree_onCallBack"
    CacheContent="false">
    <Content>
        <ComponentArt:TreeView ID="trvProductTypes" runat="server" SkinID="Default" AutoScroll="false"
            FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
            <ClientEvents>
                <ContextMenu EventHandler="tree_onContextMenu" />
                <NodeBeforeRename EventHandler="tree_BeforeRename" />
                <NodeBeforeSelect EventHandler="tree_onNodeBeforeSelect" />
                <%--         <NodeRename EventHandler="TreeView_NodeRenamed" />--%>
                <%--        <NodeSelect EventHandler="tree_onNodeSelect" />--%>
                <Load EventHandler="tree_Load" />
            </ClientEvents>
        </ComponentArt:TreeView>
    </Content>
    <LoadingPanelClientTemplate>
    </LoadingPanelClientTemplate>
</ComponentArt:CallBack>
<ComponentArt:Menu ID="mnuProductTypes" runat="server" SkinID="ContextMenu">
    <%--SiteMapXmlFile="~/xml/ProductTypeTreeContextMenu.xml" >--%>
    <ClientEvents>
        <ItemSelect EventHandler="treeMenu_onItemClick" />
    </ClientEvents>
</ComponentArt:Menu>
