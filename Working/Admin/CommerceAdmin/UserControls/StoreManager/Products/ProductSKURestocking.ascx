﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSKURestocking.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.ProductSKURestocking" %>
<div class="gridContainer">
    <asp:ObjectDataSource ID="odsRestocking" runat="server" EnablePaging="true" EnableViewState="true"
        EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.ProductSKURestocking"
        OnSelecting="odsRestocking_Selecting" MaximumRowsParameterName="pageSize" StartRowIndexParameterName="rowIndex"
        SortParameterName="sortExpression" SelectCountMethod="GetRecordCount" SelectMethod="GetRecords" />
    <asp:ListView ID="lstRestocking" runat="server" ItemPlaceholderID="itemContainer"
        DataSourceID="odsRestocking" DataKeyNames="Id">
        <LayoutTemplate>
            <h3>
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Restockings %>" /></h3>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="grid-view"
                id="gridview">
                <thead>
                    <tr>
                        <th>
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:SiteSettings, Restocking_ExpectedDate1 %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:SiteSettings, Restocking_PONo %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:SiteSettings, Restocking_Warehouse %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:SiteSettings, Restocking_ExpectedQty %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:SiteSettings, Restocking_ItemComments %>" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemContainer" runat="server" />
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5" align="right">
                            <asp:DataPager ID="dpRestocking" runat="server" PageSize="10">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="false" ShowLastPageButton="false"
                                        NextPageImageUrl="~/App_Themes/General/images/next-page.png" RenderNonBreakingSpacesBetweenControls="false"
                                        PreviousPageImageUrl="~/App_Themes/General/images/previous-page.png" ButtonCssClass="pager-button" />
                                    <asp:TemplatePagerField>
                                        <PagerTemplate>
                                            <div class="pager">
                                                <%# string.Format(JSMessages.Page0Of1Items, (Container.StartRowIndex / Container.PageSize) + 1, 
                                                    Math.Ceiling((double)Container.TotalRowCount / Container.PageSize), Container.TotalRowCount)%>
                                            </div>
                                        </PagerTemplate>
                                    </asp:TemplatePagerField>
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class='<%# Container.DataItemIndex % 2 == 0 ? "row" : "alternate-row" %>'>
                <td>
                    <asp:Literal ID="ltRestockingDate" runat="server" Text='<%# Convert.ToDateTime(Eval("RestockingDate")).ToString("d") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltRestockingName" runat="server" Text='<%# Eval("RestockingTitle") %>' />
                </td>
                <td>
                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("WarehouseName") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltExpectedQuantity" runat="server" Text='<%# Eval("ExpectedQuantity") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltComments" runat="server" Text='<%# Eval("Comments") %>' />
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</div>
