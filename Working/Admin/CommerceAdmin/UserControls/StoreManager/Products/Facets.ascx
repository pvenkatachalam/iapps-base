﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Facets.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.Facets" %>
<script type="text/javascript">
    var attributeFacetItem;
    function loadFacetGrid(attributeId) {
        grdAttributeFacets.set_callbackParameter(attributeId);
        grdAttributeFacets.callback();
    }

    function ShowFacetPopup(isEdit, factId) {
        popupPath = '../../Popups/StoreManager/AddFacet.aspx?AttributeId=' + SelectedNodeId + '&IsEdit=' + isEdit + '&AttributeType=' + attrTypeName + '&FacetId=' + factId;
        pagepopup = dhtmlmodal.open('AddFacet', 'iframe', popupPath, '', 'width=700px,height=510px,center=1,resize=0,scrolling=1');
        pagepopup.onclose = function () {
            var a = document.getElementById('AddFacet');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function grdAttributeFacets_onContextMenu(sender, eventArgs) {
        var e = eventArgs.get_event();
        attributeFacetItem = eventArgs.get_item();

        factId = attributeFacetItem.getMember(3).get_text();
        var menuItems = mnuFacets.get_items();
        if (factId != '' && factId != emptyGuid) //only add
        {
            for (i = 0; i < menuItems.get_length(); i++) {
                menuItems.getItem(i).set_visible(true);
            }
        }
        else {
            for (i = 0; i < menuItems.get_length(); i++) {
                if (menuItems.getItem(i).get_id() != 'cmFacetsAdd') {
                    menuItems.getItem(i).set_visible(false);
                }
            }
        }

        mnuFacets.showContextMenuAtEvent(e);
    }

    function mnuFacets_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var contextDataNode = menuItem.get_parentMenu().get_contextData();
        var selectedMenuId = menuItem.get_id();
        if (selectedMenuId != null) {
            switch (selectedMenuId) {
                case 'cmFacetsAdd':
                    ShowFacetPopup(0, emptyGuid);
                    break;
                case 'cmFacetsEdit':
                    factId = attributeFacetItem.getMember(3).get_text();
                    ShowFacetPopup(1, factId);
                    break;
                case 'cmFacetsDelete':
                    var result;
                    result = window.confirm('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisItem %>" />');
                    if (result == true) {
                        factId = attributeFacetItem.getMember(3).get_text();
                        var dbResult = DeleteAttributeFacet(factId);

                        if (dbResult != null && dbResult.toLowerCase() == 'true') {
                            loadFacetGrid(SelectedNodeId); //loading the grid after delete...
                        }
                        else {
                            alert('<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ErrorDeleteItemActionFailed %>" />' + dbResult);
                        }
                    }
                    break;
            }
        }
    }
        
</script>
<ComponentArt:Menu ID="mnuFacets" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuFacets_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<ComponentArt:Grid SkinID="Default" ID="grdAttributeFacets" SliderPopupClientTemplateId="attributeFacetsSliderTemplate"
    SliderPopupCachedClientTemplateId="attributeFacetsSliderTemplateCached" Width="620"
    runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" AllowEditing="true"
    AutoCallBackOnInsert="true" PagerInfoClientTemplateId="attributeFacetsPaginationTemplat"
    CallbackCachingEnabled="true" OnBeforeCallback="grdAttributeFacets_BeforeCallback"
    EditOnClickSelectedItem="false">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" AlternatingRowCssClass="alternate-row"
            HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
            SortDescendingImageUrl="desc.gif" SortImageWidth="8" SortImageHeight="7" AllowGrouping="false"
            AllowSorting="false">
            <Columns>
                <ComponentArt:GridColumn Width="260" HeadingText="<%$ Resources:GUIStrings, AttributeName %>"
                    DataField="Title" Align="Left" IsSearchable="true" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="NameHoverTemplate" />
                <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, Limit %>"
                    DataField="Limit" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="LimitHoverTemplate" />
                <ComponentArt:GridColumn Width="120" HeadingText="<%$ Resources:GUIStrings, DisplayOrder %>"
                    DataField="DisplayOrderString" Align="Left" AllowReordering="false" FixedWidth="true"
                    IsSearchable="true" DataCellClientTemplateId="DisplayOrderHoverTemplate" DataCellCssClass="last-data-cell"
                    HeadingCellCssClass="last-heading-cell" />
                <ComponentArt:GridColumn DataField="Id" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdAttributeFacets_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="NameHoverTemplate">
            <div title="## DataItem.GetMember('Title').get_text() ##">
                ## DataItem.GetMember('Title').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Title').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="LimitHoverTemplate">
            <div title="## DataItem.GetMember('Limit').get_text() ##">
                ## DataItem.GetMember('Limit').get_text() == "" ? "&nbsp;" : DataItem.GetMember('Limit').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DisplayOrderHoverTemplate">
            <div title="## DataItem.GetMember('DisplayOrderString').get_text() ##">
                ## DataItem.GetMember('DisplayOrderString').get_text() == "" ? "&nbsp;" : DataItem.GetMember('DisplayOrderString').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="attributeFacetsSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##</p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdAttributeFacets.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdAttributeFacets.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="attributeFacetsSliderTemplateCached">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##</p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdAttributeFacets.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdAttributeFacets.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="attributeFacetsPaginationTemplat">
            ##stringformat(Page0Of12items, currentPageIndex(grdAttributeFacets), pageCount(grdAttributeFacets),
            grdAttributeFacets.RecordCount)##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
