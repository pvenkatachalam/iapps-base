﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicAttributes.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.DynamicAttributes" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<script type="text/javascript">
        function ShowAttributesPopup(isSku) 
        {
          var  popupPath ='<%=ResolveUrl("~/Popups/StoreManager/AddProductAttribute.aspx")%>' + '?callback=' + '<%=grdAttributes.ClientID %>';
         // alert(popupPath);
          pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=562px,height=480px,center=1,resize=0,scrolling=1');
            pagepopup.onclose = function() {
                var a = document.getElementById('ShowAttributes');
                var ifr = a.getElementsByTagName('iframe');
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }
        
        function reloadSKUAttribute(attributeArray)
        {
            cbSKUAttributes.callback(attributeArray);
        }  
        
      function ChangeCombo(combo, chkItem,title)
      {
        var drop = combo +  '_DropDownContent';
        var text = combo + '_Input';
        var comboTextCtrl =document.getElementById(text);
        var comboDropCtrl =document.getElementById(drop);
        if(chkItem.checked)
        {
              comboTextCtrl.value = title;
        }
        comboDropCtrl.Style ="display:none;";
            //comboCtrl .collapse();
      }

    var previousTxtValue;      
      function SetValueInGrid(txtbox,gridIndex)
      {
        previousTxtValue =txtbox.value;
      //  grdAttributes.get_table().getRow(gridIndex).setValue(4,txt);
      var gridRow = grdAttributes.get_table().getRow(gridIndex);
//        grdAttributes.edit(gridRow);
        gridRow.setValue(3,previousTxtValue);
//       grdAttributes.editComplete();
        
      }
      
      function txtbox_OnChange(txtbox)
      {
      }
</script>

<div class="sectionHeader">
    <h4>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Attributes %>" /></h4>
    <label>
        |</label><a href="#" onclick="ShowAttributesPopup(true);"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AddAttributes %>" /></a>
</div>
<div class="greyBorderBox ScrollingBox">
    <ComponentArt:Grid SkinID="Default" ID="grdAttributes" runat="server" EnableViewState="true"
        RunningMode="Callback" CallbackCachingEnabled="true" EmptyGridText="<%$ Resources:GUIStrings, NoAttributesAdded %>"
        AutoPostBackOnSelect="false" PageSize="10" PagerInfoClientTemplateId="PagerTemplate"
        Width="700" OnAfterCallback="grdAttributes_AfterCallback" ShowFooter="False"
        AllowEditing="true" EditOnClickSelectedItem="false" AutoCallBackOnUpdate="false" AutoCallBackOnInsert="false">
        <Levels>
            <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" ShowHeadingCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                DataKeyField="Id" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" AllowEditing="False" />
                    <ComponentArt:GridColumn DataField="Title" AllowEditing="False" />
                    <ComponentArt:GridColumn Width="700" HeadingText="" HeadingCellCssClass="FirstHeadingCell" AllowEditing="false"
                        DataField="Title"  DataCellCssClass="FirstDataCell" SortedDataCellCssClass="SortedDataCell"
                        IsSearchable="true"  AllowReordering="false"
                        FixedWidth="false" Align="Center" DataCellServerTemplateId="STAttribute" AllowSorting="False" />
                    <ComponentArt:GridColumn Visible="false" DataField="Title" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="STAttribute" Width="700">
                <Template>
                    <asp:PlaceHolder ID="phAttribute" runat="server"></asp:PlaceHolder>
                    <%--<asp:TextBox runat="server" Text='<%# Container.DataItem["Title"] %>' ID="lblAttribute" Width="80px" ></asp:TextBox>--%>
                    <%--<iapps:LabelControl ID="lblAttribute" runat="server" />--%>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
    </ComponentArt:Grid>
</div>
