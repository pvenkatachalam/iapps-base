﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewDynamicAttribute.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.Products.NewDynamicAttribute" %>
<script type="text/javascript">
    var newAttributeArray = null;
    var HTMLEditorPopup;
    function ShowAttributesPopup(callbackId, displayPersonalizednodes) {

        var popupPath = '<%=ResolveUrl("~/Popups/StoreManager/AddProductAttribute.aspx")%>' + '?callback=' + callbackId + '&DisplayPersonalized=' + displayPersonalizednodes;
        // alert(popupPath);
        pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=255px,height=530px,center=1,resize=0,scrolling=1');
        pagepopup.onclose = function () {
            var a = document.getElementById('ShowAttributes');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }


    function reloadAttributes(callbackId, attributeArray) {
        var cbAttributesCntrl = window[callbackId];
        cbAttributesCntrl.callback(attributeArray);
        newAttributeArray = attributeArray;

    }

    function cbSKUAttributes_CallbackComplete(sender, eventArgs) {
        if (newAttributeArray != null) {
            __doPostBack();
            var input = newAttributeArray;
            var startsWith = sender.Id.replace("cbSKUAttributes", "");
            for (var i = 0; i < input.length; i++) {
                var cntrlToValId = startsWith + input[i].replace(new RegExp("-", "g"), "_") + "_Input";
                var valCntrlId = startsWith + input[i].replace(new RegExp("-", "g"), "_") + "_Input_val";
                var cntrlToVal = document.getElementById(cntrlToValId);
                var valCntrl = document.getElementById(valCntrlId);
                if (cntrlToVal != null && valCntrl != null) {
                    if (Page_Validators != null) {
                        Page_Validators[Page_Validators.length] = valCntrl;
                    }
                    ValidatorHookupControl(cntrlToVal, valCntrl);
                    ValidatorEnable(valCntrl, true);
                    newAttributeArray = null;
                    ValidatorUpdateDisplay(valCntrl);
                    ValidatorValidate(valCntrl);
                }
            }
        }
    }

    function ChangeCombo(combo, chkItem, title) {
        //       var drop = combo +  '_DropDownContent';
        var text = combo + '_Input';
        var comboTextCtrl = document.getElementById(text);
        var textValue = comboTextCtrl.value;
        textValue += ' ';
        //        var comboDropCtrl =document.getElementById(drop);
        if (chkItem.checked) {
            textValue += escape(title);
        }
        else {
            textValue = textValue.replace(title + ' ', '');
        }
        textValue = textValue.replace(/^\s+|\s+$/g, "")
        comboTextCtrl.value = unescape(textValue);
    }

    function AttrCntrlDeleteAttribute(callbackId, attributeId) {
        if (confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, AreYouSureYouWantToDeleteThisAttribute %>' />")) {
            var callCntrl = callbackId;
            callCntrl.callback('Delete~' + attributeId);
            var startsWith = callbackId.Id.replace("cbSKUAttributes", "");
            var validatorId = startsWith + attributeId.replace(new RegExp("-", "g"), "_") + "_Input_val";
            var valControl = document.getElementById(validatorId);
            if (valControl != null)
                ValidatorEnable(valControl, false);
        }
    }

    function Combo_OnLoad(sender, eventArgs) {
        sender._OriginalExpand = sender.Expand;
        sender.Expand = function () {
            var objParentTag = $('#' + sender.Id).parents('.greyBorderBox');
            sender.DropDownOffsetX = -($(objParentTag).scrollLeft());
            sender.DropDownOffsetY = -($(objParentTag).scrollTop());
            this._OriginalExpand();
        };
    }

    function ShowEditorPopup(popupPath) {
        HTMLEditorPopup = dhtmlmodal.open('HTMLEditorPopup', 'iframe', popupPath, '', 'width=705px,height=535px,center=1,resize=0,scrolling=1');
        HTMLEditorPopup.onclose = function () {
            var a = document.getElementById('HTMLEditorPopup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }

    function HideEditorPopup() {
        HTMLEditorPopup.hide();

    }

    function SetTextValue(hiddenId, htmlValue) {
        var hidden = document.getElementById(hiddenId);
        hidden.value = htmlValue;
    }


</script>
<div class="section-header clear-fix">
    <asp:Panel runat="server" ID="pnlAddAttributes">
        <h4>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Attributes %>" /></h4>
        <label>|</label>
        <a id="lnkAddAttribute" runat="server" class="small-button">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AddAttributes %>" /></a>
    </asp:Panel>
</div>
<div class="greyBorderBox scrollingBox attributesBox">
    <ComponentArt:CallBack ID="cbSKUAttributes" runat="server" PostState="true" OnCallback="cbSKUAttributes_Callback"
        Debug="false" EnableViewState="false" CacheContent="false">
        <Content>
            <asp:PlaceHolder ID="phAttribute" runat="server"></asp:PlaceHolder>
        </Content>
        <ClientEvents>
            <CallbackComplete EventHandler="cbSKUAttributes_CallbackComplete" />
        </ClientEvents>
    </ComponentArt:CallBack>
    <asp:HiddenField ID="hfMultiStringPopup" Value="false" runat="server" />
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('input[dummyattribute="dummyValue"]').each(function () {
            if ($(this).attr('title') != null && $(this).attr('title') != 'undefined') {
                if ($(this).attr('title') != 'NULL')
                    $(this).val($(this).attr('title'));
                else
                    $(this).val('');
            }
        });
    });

    function UpdateMultiStringHiddenVariable() {
        document.getElementById("<%=hfMultiStringPopup.ClientID %>").value = "true";
    }
</script>
