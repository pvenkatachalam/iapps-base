﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeTree.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AttributeTree" %>
<!-- Page Level Tree management code -->
<script type="text/javascript">
var nodeType ='';

//This has to set first .. to pass the objects to the global script
function SetTreeLevelGeneralVariablesinPage()
{
    setTimeout(function() 
    {
        treeObject = treeAttributes;
        if(typeof(mnuAttributes) != 'undefined')
            treeMenuObject = mnuAttributes;
        treeObjectName = "treeAttributes";
    }, 500);
}
//Calling after node select
function PageLevelOnNodeSelect(sender, eventArgs)
{
     indexOfSelectedNode = eventArgs.get_node().get_depth();
     if (indexOfSelectedNode > 1) // First Level is Group
    {
    	AttributeTreeNodeSelect(false);        
    }
    else
    {
        AttributeTreeNodeSelect(true);
    }
}
//Calling before selecting a node
function PageLevelOnNodeBeforeSelect(sender, eventArgs)
{
    return true;
}
//Calling begore displaying context menu
function PageLevelTreeOnContextMenu(menuItems, indexOfSelectedNode) {
    
    if(selectedTreeNode.getProperty('IsSystem') != 'True')
    {
        switch (indexOfSelectedNode) {
            case 0:
                menuItems.getItem(0).set_visible(true);     //Add Group
                menuItems.getItem(1).set_visible(false);    //Break item
                menuItems.getItem(2).set_visible(false);    //Add Attribute
                menuItems.getItem(3).set_visible(false);    //Break item
                menuItems.getItem(4).set_visible(false);    //Rename Group
                menuItems.getItem(5).set_visible(false);    //Break item
                menuItems.getItem(6).set_visible(false);    //Delete Group
                menuItems.getItem(7).set_visible(false);    //Break item
                menuItems.getItem(8).set_visible(false);    //Rename Attribute
                menuItems.getItem(9).set_visible(false);    //Break item
                menuItems.getItem(10).set_visible(false);   //Delete Attribute
                break;
            case 1:

                var isCustomerGroup = selectedTreeNode.getProperty('IsCustomerGroup');
                var isOrderGroup = selectedTreeNode.getProperty('IsOrderGroup');

                menuItems.getItem(0).set_visible(false);    //Add Group
                menuItems.getItem(1).set_visible(false);    //Break item
                menuItems.getItem(2).set_visible(true);     //Add Attribute

                if (isCustomerGroup != 'True' && isOrderGroup != 'True') {
                    menuItems.getItem(3).set_visible(true);     //Break item
                    menuItems.getItem(4).set_visible(true);     //Rename Group
                    menuItems.getItem(5).set_visible(true);     //Break item
                    menuItems.getItem(6).set_visible(true);     //Delete Group
                }
                else {
                    menuItems.getItem(3).set_visible(false);     //Break item
                    menuItems.getItem(4).set_visible(false);     //Rename Group
                    menuItems.getItem(5).set_visible(false);     //Break item
                    menuItems.getItem(6).set_visible(false);     //Delete Group
                }
                menuItems.getItem(7).set_visible(false);    //Break item
                menuItems.getItem(8).set_visible(false);    //Rename Attribute
                menuItems.getItem(9).set_visible(false);    //Break item
                menuItems.getItem(10).set_visible(false);   //Delete Attribute
                break;
            case 2:
                menuItems.getItem(0).set_visible(false);    //Add Group
                menuItems.getItem(1).set_visible(false);    //Break item
                menuItems.getItem(2).set_visible(false);    //Add Attribute
                menuItems.getItem(3).set_visible(false);    //Break item
                menuItems.getItem(4).set_visible(false);    //Rename Group
                menuItems.getItem(5).set_visible(false);    //Break item
                menuItems.getItem(6).set_visible(false);    //Delete Group
                menuItems.getItem(7).set_visible(false);    //Break item
                menuItems.getItem(8).set_visible(true);     //Rename Attribute
                menuItems.getItem(9).set_visible(true);     //Break item
                menuItems.getItem(10).set_visible(true);    //Delete Attribute
                break;
        }
        return true;
   }
   else
   {
        return false;
   }
}
//Calling after a context menu selected
function PageLevelTreeOnItemSelect(selectedMenuId, contextDataNode, sender, eventArgs)
{
    switch (selectedMenuId) 
    {
        case 'cmTreeAddGroup':
            sender.hide();
            nodeType ='Group';
            AddTreeNode(contextDataNode, treeObject);            
            break;
        case 'cmTreeAddAttribute':
            sender.hide();
            nodeType ='Attribute';
            AddTreeNode(contextDataNode, treeObject);            
            break;
        case 'cmTreeDeleteGroup' :
            sender.hide();
            nodeType ='Group';
            RemoveTreeNode(contextDataNode);            
            break;
        case 'cmTreeDeleteAttribute' :
            sender.hide();
            nodeType ='Attribute';
            RemoveTreeNode(contextDataNode);            
            break;
        case 'cmTreeRenameGroup':
            sender.hide();
            nodeType ='Group';
            RenameTreeNode(contextDataNode);            
            break;
        case 'cmTreeRenameAttribute':
            sender.hide();
            nodeType ='Attribute';
            RenameTreeNode(contextDataNode);            
            break;
    }        
}    
//Calling after adding a empty node. if u want u can change the styles.. etc for the new node here    
function PageLevelAddEmptyTreeNode(contextMenuData, treeObject,newNode)
{
    if (nodeType == 'Group')
    {
        newNode.setProperty("CssClass", "GrayedTreeNode");
        newNode.setProperty("HoverCssClass", "GrayedTreeNode");
        newNode.setProperty("SelectedCssClass","SelectedGrayedTreeNode");  
    }    
    return true;
}
//Calling after click on the remove node context menu
function PageLevelRemoveTreeNode(contextMenuData)
{
    nodeId = contextMenuData.get_id();
    var isDelete ;
    if (nodeType == 'Group')
    {
        isDelete = DeleteAttributeGroupCheck(nodeId);
        if(isDelete =='true')        
        {
            isDelete = DeleteAttributeGroup(nodeId);
        }
    }       
    else if (nodeType == 'Attribute')
    {
        isDelete = DeleteAttributeCheck(nodeId);
        if(isDelete =='true')
        {
            isDelete=DeleteAttribute(nodeId);
        }
    }
    
    return isDelete;
}
//Calling just after starting rename of a node
function PageLevelRenameTreeNode(contextMenuData)
{
    return true;
}
//Call after re-loading the Grid
function PageLevelTreeLoad(sender, eventArgs)
{
    return true;
}

// call call back method from here, and return the result
function PageLevelCreateNewNode(newText,parentId, newNode) {
    
    var result ;
    if (nodeType == 'Group')
    {
        result = CreateAttributeGroup(newText); 
    }
    else if (nodeType == 'Attribute') {

    if (parentId.toString().toLowerCase() == jCustomerAttributeGroupId || parentId.toString().toLowerCase() == jOrderAttributeGroupId) {
        result = CreateAttributeExtended(newText, parentId, false, false, false);
        newNode.setProperty("IsFaceted", "False");
        newNode.setProperty("IsSearchable", "False");
        newNode.setProperty("IsPersonalized", "False");
    }
    else {
        result = CreateAttribute(newText, parentId);
        newNode.setProperty("IsFaceted", "True");
        newNode.setProperty("IsSearchable", "True");
    }
    }
    return result ;
}
//This will call immediately after creating a node. Instead of this we can select that node in global script, but it may make someother error, eg.it calls tree_onNodeBeforeSelect etc..    
function PageLevelLoadNewNode(selectedNodeId,eventArgs)
{
    if (nodeType == 'Attribute')
    {
        AttributeTreeNodeSelect(false);
        //LoadAttributeDetails(eventArgs.get_node(),eventArgs.get_newText());    
    }    
} 
// call call back method from here, and return the result
function PageLevelEditNode(id, newText, renamedNode)
{
    var result ;
    if (nodeType == 'Group')
    {
        result = RenameAttributeGroup(renamedNode.get_id(), newText); 
    }
    else if (nodeType == 'Attribute')
    {
        result = RenameAttribute(renamedNode.get_id(), newText); 
    }
    return result ;
}    
// this will call immediately after renamed of the node.
function PageLevelLoadEditedNode(selectedNodeId,sender, eventArgs)
{
    if (nodeType == 'Attribute')
    {
        AttributeTreeNodeSelect(false);
        //LoadAttributeDetails(eventArgs.get_node(),eventArgs.get_newText());      
    } 
}
// Calling after refresh the grid in callback mode
function PageLevelRefreshTreeImmediate()
{
    return null;
}

var selectedNode = "";
function SetDefaultAttributeNodeId(nodeId) {
    var fnToCall = function () { SetDefaultAttributeNodeId1(nodeId); };
    setTimeout(fnToCall, 500);
}

function SetDefaultAttributeNodeId1(nodeId) {
    treeAttributes.SelectNodeById(nodeId);
    selectedNode = treeAttributes.get_selectedNode();
    if (selectedNode != null)
        selectedNode.expand();
}
</script>
<asp:XmlDataSource ID="xmlDataSource" runat="server"></asp:XmlDataSource>
<ComponentArt:TreeView ID="treeAttributes" runat="server" SkinID="Default" AutoScroll="false" 
    FillContainer="true" AutoPostBackOnSelect="false" MultipleSelectEnabled="false">
    <ClientEvents>
        <ContextMenu EventHandler="tree_onContextMenu" />
        <NodeBeforeRename EventHandler="tree_BeforeRename" />
        <NodeBeforeSelect EventHandler="tree_onNodeBeforeSelect" />
        <Load EventHandler="tree_Load" />  
    </ClientEvents>
</ComponentArt:TreeView>
<asp:HiddenField runat="server" ID="hdnSelectedTreeNode" />
<ComponentArt:Menu ID="mnuAttributes" runat="server" SkinID="ContextMenu">
    
    <ClientEvents>
        <ItemSelect EventHandler="treeMenu_onItemClick" />
    </ClientEvents>
</ComponentArt:Menu>