﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GlobalLevelAttributes.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.StoreManager.GlobalLevelAttributes" %>
<script type="text/javascript">
    var HTMLEditorPopup;
    function ChangeCombo(combo, chkItem, title) {
        var text = combo + '_Input';
        var comboTextCtrl = document.getElementById(text);
        var textValue = comboTextCtrl.value;
        textValue += ' ';
        if (chkItem.checked) {
            textValue += escape(title);
        }
        else {
            textValue = textValue.replace(title + ' ', '');
        }
        textValue = textValue.replace(/^\s+|\s+$/g, "")
        comboTextCtrl.value = unescape(textValue);
    }
    function ShowEditorPopup(popupPath) {           

        HTMLEditorPopup = dhtmlmodal.open('HTMLEditorPopup', 'iframe', popupPath, '', 'width=705px,height=535px,center=1,resize=0,scrolling=1');
        HTMLEditorPopup.onclose = function () {
            var a = document.getElementById('HTMLEditorPopup');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }
    }
    
    function Combo_OnLoad(sender, eventArgs) {
    }
    function HideEditorPopup() {
        HTMLEditorPopup.hide();
    }
    function SetTextValue(hiddenId, htmlValue) {
        var hidden = document.getElementById(hiddenId);
        hidden.value = htmlValue;
    }
    function ShowModalPopup(AttId, ObjectID, ctrlId, globalObject) {
        
        popupPath = '../../Popups/StoreManager/GlobalMultiAttributeValue.aspx?ObjectID=' + ObjectID + '&AttributeId=' + AttId + '&ctrlId=' + ctrlId + '&globalObject=' + globalObject;
        pagepopup = dhtmlmodal.open('ShowAttributes', 'iframe', popupPath, '', 'width=330px,height=438px,center=1,resize=0,scrolling=1');
        pagepopup.onclose = function () {
            var a = document.getElementById('ShowAttributes');
            var ifr = a.getElementsByTagName('iframe');
            window.frames[ifr[0].name].location.replace("about:blank");
            return true;
        }

    }
    function UpdateAttributeValues() {
        __doPostBack();
    }    
</script>
<div class="sectionHeader">
    <asp:PlaceHolder ID="phAttribute" runat="server"></asp:PlaceHolder>
</div>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $('input[dummyattribute="dummyValue"]').each(function () {
            if ($(this).attr('title') != null && $(this).attr('title') != 'undefined') {
                if ($(this).attr('title') != 'NULL')
                    $(this).val($(this).attr('title'));
                else
                    $(this).val('');
            }
        });
    });
</script>