<%@ Control Language="C#" AutoEventWireup="True" Codebehind="RecurrenceControl.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.RecurrenceControl" %>
<script type="text/javascript">
    function EveryDayClicked() {
        document.getElementById("txtWeekly").value = '';
        document.getElementById("chkSunDay").checked = false;
        document.getElementById("chkThursDay").checked = false;
        document.getElementById("chkMonDay").checked = false;
        document.getElementById("chkFriDay").checked = false;
        document.getElementById("chkTuesDay").checked = false;
        document.getElementById("chkSatDay").checked = false;
        document.getElementById("chkWednesDay").checked = false;

        document.getElementById("txtMonthlyDay").value = '';
        document.getElementById("txtMonthlyDayMonth").value = '';

        document.getElementById("txtMonthlyWeekdayMonth").value = '';

        document.getElementById("txtYearlyMonthDay").value = '';
    }
    function EveryWeekDayClicked() {
        document.getElementById("txtDailyDays").value = '';

        document.getElementById("txtWeekly").value = '';
        document.getElementById("chkSunDay").checked = false;
        document.getElementById("chkThursDay").checked = false;
        document.getElementById("chkMonDay").checked = false;
        document.getElementById("chkFriDay").checked = false;
        document.getElementById("chkTuesDay").checked = false;
        document.getElementById("chkSatDay").checked = false;
        document.getElementById("chkWednesDay").checked = false;

        document.getElementById("txtMonthlyDay").value = '';
        document.getElementById("txtMonthlyDayMonth").value = '';

        document.getElementById("txtMonthlyWeekdayMonth").value = '';

        document.getElementById("txtYearlyMonthDay").value = '';
    }
    function WeeklyClicked() {
        document.getElementById("txtDailyDays").value = '';

        document.getElementById("txtMonthlyDay").value = '';
        document.getElementById("txtMonthlyDayMonth").value = '';

        document.getElementById("txtMonthlyWeekdayMonth").value = '';

        document.getElementById("txtYearlyMonthDay").value = '';
    }
    function MonthlyDayClicked() {
        document.getElementById("txtDailyDays").value = '';

        document.getElementById("txtWeekly").value = '';
        document.getElementById("chkSunDay").checked = false;
        document.getElementById("chkThursDay").checked = false;
        document.getElementById("chkMonDay").checked = false;
        document.getElementById("chkFriDay").checked = false;
        document.getElementById("chkTuesDay").checked = false;
        document.getElementById("chkSatDay").checked = false;
        document.getElementById("chkWednesDay").checked = false;

        document.getElementById("txtMonthlyWeekdayMonth").value = '';

        document.getElementById("txtYearlyMonthDay").value = '';
    }
    function MonthlyWeekDayClicked() {
        document.getElementById("txtDailyDays").value = '';

        document.getElementById("txtWeekly").value = '';
        document.getElementById("chkSunDay").checked = false;
        document.getElementById("chkThursDay").checked = false;
        document.getElementById("chkMonDay").checked = false;
        document.getElementById("chkFriDay").checked = false;
        document.getElementById("chkTuesDay").checked = false;
        document.getElementById("chkSatDay").checked = false;
        document.getElementById("chkWednesDay").checked = false;

        document.getElementById("txtMonthlyDay").value = '';
        document.getElementById("txtMonthlyDayMonth").value = '';

        document.getElementById("txtYearlyMonthDay").value = '';
    }
    function YearlyEveryDayClicked() {
        document.getElementById("txtDailyDays").value = '';

        document.getElementById("txtWeekly").value = '';
        document.getElementById("chkSunDay").checked = false;
        document.getElementById("chkThursDay").checked = false;
        document.getElementById("chkMonDay").checked = false;
        document.getElementById("chkFriDay").checked = false;
        document.getElementById("chkTuesDay").checked = false;
        document.getElementById("chkSatDay").checked = false;
        document.getElementById("chkWednesDay").checked = false;

        document.getElementById("txtMonthlyDay").value = '';
        document.getElementById("txtMonthlyDayMonth").value = '';

        document.getElementById("txtMonthlyWeekdayMonth").value = '';
    }
</script>
<table cellpadding="0" cellspacing="0" width="100%" class="recurrenceControlTableStyle"
    border="0">
    <tr>
        <th colspan="2" class="MonthHeader">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Daily %>" />
        </th>
    </tr>
    <tr>
        <td class="recurranceProp">
            <input id="rbDailyDays" type="radio" name="rbMain" runat="server" onclick="EveryDayClicked()" />
        </td>
        <td class="recurrancePropDaily">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Every %>" /><input type="text"
                id="txtDailyDays" class="textBoxes recurrance18width" runat="server" />
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Days %>" />
        </td>
    </tr>
    <tr>
        <td class="recurrancePropWeekday">
            <input id="rbDailyWeekday" type="radio" name="rbMain" runat="server" onclick="EveryWeekDayClicked()" />
        </td>
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EveryWeekday %>" />
        </td>
    </tr>
    <tr>
        <th colspan="2" class="MonthHeader">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Weekly %>" />
        </th>
    </tr>
    <tr>
        <td class="recurrancerbWeekly">
            <input id="rbWeekly" type="radio" name="rbMain" runat="server" onclick="WeeklyClicked()" />
        </td>
        <td class="recurrance3height">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RecurColon %>" /><input
                type="text" id="txtWeekly" class="textBoxes recurrance18width" runat="server" /><asp:Localize
                    runat="server" Text="<%$ Resources:GUIStrings, WeeksOn %>" />
        </td>
    </tr>
    <tr>
        <td class="recurrance4Height">
        </td>
        <td>
            <table id="tblcblWeeklyWeekdays" cellspacing="0" cellpadding="0" border="0" class="tblcblWeeklyWeekdays">
                <tr>
                    <td>
                        <input id="chkSunDay" type="checkbox" runat="server" />
                        <label for="chkSunDay">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Sunday %>" /></label>
                    </td>
                    <td>
                        <input id="chkThursDay" type="checkbox" runat="server" />
                        <label for="chkThursDay">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Thursday %>" /></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkMonDay" type="checkbox" runat="server" />
                        <label for="chkMonDay">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Monday %>" /></label>
                    </td>
                    <td>
                        <input id="chkFriDay" type="checkbox" runat="server" /><label for="chkFriDay"><asp:Localize
                            runat="server" Text="<%$ Resources:GUIStrings, Friday %>" /></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkTuesDay" type="checkbox" runat="server" /><label for="chkTuesDay"><asp:Localize
                            runat="server" Text="<%$ Resources:GUIStrings, Tuesday %>" /></label>
                    </td>
                    <td>
                        <input id="chkSatDay" type="checkbox" runat="server" /><label for="chkSatDay"><asp:Localize
                            runat="server" Text="<%$ Resources:GUIStrings, Saturday %>" /></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="chkWednesDay" type="checkbox" runat="server" /><label for="chkWednesDay"><asp:Localize
                            runat="server" Text="<%$ Resources:GUIStrings, Wednesday %>" /></label>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th colspan="2" class="MonthHeader">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Monthly %>" />
        </th>
    </tr>
    <tr>
        <td class="recurrance5width">
            <input id="rbMonthlyDay" type="radio" name="rbMain" runat="server" onclick="MonthlyDayClicked()" />
        </td>
        <td class="recurrance6height">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DayOfEveryMonths %>" />
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DayColon %>" /><input
                id="txtMonthlyDay" type="text" class="textBoxes recurrance18width" runat="server" />
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MonthsColon %>" /><input
                type="text" id="txtMonthlyDayMonth" class="textBoxes recurrance18width" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="recurrance7width">
            <input id="rbMonthlyWeekday" type="radio" name="rbMain" runat="server" onclick="MonthlyWeekDayClicked()" />
        </td>
        <td class="recurrance8height">
            The
            <select id="ddlMonthlyWeekNumber" class="NormalText" runat="server">
                <option selected="selected" value="1">first</option>
                <option value="2">second</option>
                <option value="3">third</option>
                <option value="4">fourth</option>
                <option value="-1">last</option>
            </select>
            &nbsp;<select id="ddlMonthlyWeekday" class="NormalText" runat="server">
                <option selected="selected" value="1000000">Sunday</option>
                <option value="0100000">Monday</option>
                <option value="0010000">Tuesday</option>
                <option value="0001000">Wednesday</option>
                <option value="0000100">Thursday</option>
                <option value="0000010">Friday</option>
                <option value="0000001">Saturday</option>
            </select>
            of every
            <input type="text" id="txtMonthlyWeekdayMonth" class="textBoxes recurrance18width"
                runat="server" />
            month(s)
        </td>
    </tr>
    <tr>
        <th colspan="2" class="MonthHeader">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Yearly %>" />
        </th>
    </tr>
    <tr>
        <td class="recurrancerbWeekly">
            <input id="rbYearlyMonthDay" type="radio" name="rbMain" runat="server" onclick="YearlyEveryDayClicked()" />
        </td>
        <td class="recurrance9Height">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Every %>" />
            <select id="ddlYearlyMonth" class="NormalText" runat="server">
                <option selected="selected" value="1" title='<%$ Resources:GUIStrings, January %>'>
                </option>
                <option value="2" title='<%$ Resources:GUIStrings, February %>'></option>
                <option value="3" title='<%$ Resources:GUIStrings, March %>'></option>
                <option value="4" title='<%$ Resources:GUIStrings, April %>'></option>
                <option value="5" title='<%$ Resources:GUIStrings, May %>'></option>
                <option value="6" title='<%$ Resources:GUIStrings, June %>'></option>
                <option value="7" title='<%$ Resources:GUIStrings, July %>'></option>
                <option value="8" title='<%$ Resources:GUIStrings, August %>'></option>
                <option value="9" title='<%$ Resources:GUIStrings, September %>'></option>
                <option value="10" title='<%$ Resources:GUIStrings, October %>'></option>
                <option value="11" title='<%$ Resources:GUIStrings, November %>'></option>
                <option value="12" title='<%$ Resources:GUIStrings, December %>'></option>
            </select>
            &nbsp;<input type="text" id="txtYearlyMonthDay" class="textBoxes recurrance18width"
                runat="server" />
        </td>
    </tr>
    <tr>
        <td class="recurrance4Height">
            <input id="rbYearlyMonthWeekday" type="radio" name="rbMain" runat="server" />
        </td>
        <td>
            <%--The--%>
            <select id="ddlYearlyWeekNumber" class="NormalText" runat="server">
                <option selected="selected" value="1" title="<%$ Resources:GUIStrings, First %>">
                </option>
                <option value="2" title="<%$ Resources:GUIStrings, Second %>"></option>
                <option value="3" title="<%$ Resources:GUIStrings, Third %>"></option>
                <option value="4" title="<%$ Resources:GUIStrings, Fourth %>"></option>
                <option value="-1" title="<%$ Resources:GUIStrings, Last %>"></option>
            </select>
            &nbsp;<select id="ddlYearlyWeekday" class="NormalText" runat="server">
                <option selected="selected" value="1000000" title="<%$ Resources:GUIStrings, Sunday %>">
                </option>
                <option value="0100000" title="<%$ Resources:GUIStrings, Monday %>"></option>
                <option value="0010000" title="<%$ Resources:GUIStrings, Tuesday %>"></option>
                <option value="0001000" title="<%$ Resources:GUIStrings, Wednesday %>"></option>
                <option value="0000100" title="<%$ Resources:GUIStrings, Thursday %>"></option>
                <option value="0000010" title="<%$ Resources:GUIStrings, Friday %>"></option>
                <option value="0000001" title="<%$ Resources:GUIStrings, Saturday %>"></option>
            </select>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MonthColon %>" />
            <select id="ddlYearlyMonth2" class="NormalText" runat="server">
                <option selected="selected" value="1" title="<%$ Resources:GUIStrings, January %>">
                </option>
                <option value="2" title="<%$ Resources:GUIStrings, February %>"></option>
                <option value="3" title="<%$ Resources:GUIStrings, March %>"></option>
                <option value="4" title="<%$ Resources:GUIStrings, April %>"></option>
                <option value="5" title="<%$ Resources:GUIStrings, May %>"></option>
                <option value="6" title="<%$ Resources:GUIStrings, June %>"></option>
                <option value="7" title="<%$ Resources:GUIStrings, July %>"></option>
                <option value="8" title="<%$ Resources:GUIStrings, August %>"></option>
                <option value="9" title="<%$ Resources:GUIStrings, September %>"></option>
                <option value="10" title="<%$ Resources:GUIStrings, October %>"></option>
                <option value="11" title="<%$ Resources:GUIStrings, November %>"></option>
                <option value="12" title="<%$ Resources:GUIStrings, December %>"></option>
            </select>
        </td>
    </tr>
    <tr>
        <th colspan="2" class="MonthHeader">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RangeOfRecurrence %>" />
        </th>
    </tr>
    <tr>
        <td class="recurrance7width">
            <input id="rbRangeNoEndDate" type="radio" name="rbRange" runat="server" checked="true" />
        </td>
        <td class="recurrance10height">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NoEndDate %>" />
        </td>
    </tr>
    <tr>
        <td class="recurrance7width">
            <input id="rbRangeOccurrences" type="radio" name="rbRange" runat="server" />
        </td>
        <td class="recurrance11height">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EndAftertxtRangeOccurrencesOccurrences %>" /><input
                type="text" id="txtRangeOccurrences" class="textBoxes recurrance18width" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="recurrancewidth11">
            <input id="rbRangeDate" type="radio" name="rbRange" runat="server" />
        </td>
        <td>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EndByColon %>" />
            <input type="text" value="12/31/2007" id="txtRangeDate" class="textBoxes recurrance12width"
                runat="server" />
        </td>
    </tr>
</table>
