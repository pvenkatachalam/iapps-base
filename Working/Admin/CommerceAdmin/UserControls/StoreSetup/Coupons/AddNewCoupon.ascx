﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewCoupon.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddNewCoupon" %>
<script type="text/javascript" language="javascript">
    var confirmMsg = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToRemoveThisCouponFromTheOrder %>" />';
    function removeCoupon() {
        var yesOrNo = window.confirm(confirmMsg);
        if (yesOrNo == 'false')
            return false;
        else 
            return true;
    }
</script>
<div class="snap-container" style="border-top: 0;">
    <div class="snap-header">
        <img class="snap-toggle" alt="Collpase coupons" src="../../App_Themes/General/images/snap-expand.png" />
        <h3>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Coupon %>" /></h3>
        <asp:LinkButton ID="lbtnAddNewCoupon" runat="server" Text="<%$ Resources:GUIStrings, AddCoupon %>"
            CssClass="small-button" Style="float: right;"></asp:LinkButton>
        <div class="clearFix">
        </div>
    </div>
    <div class="snap-content">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="grid-view">
            <thead>
                <tr>
                    <th>
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, CouponName %>" />
                    </th>
                    <th>
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, CouponCode %>" />
                    </th>
                    <th>
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, CouponType %>" />
                    </th>
                    <th>
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, CouponValue %>" />
                    </th>
                    <th>
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Actions %>" />
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater runat="server" ID="rptCoupons">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hdnCouponId" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                                <asp:Label ID="lblCouponName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Title") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblDiscountCode" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblDiscountType" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblValue" runat="server" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lbtmDeleteCoupon" runat="server" ToolTip="<%$ Resources:GUIStrings, RemoveCouponFromOrder %>"
                                    OnClientClick="javascript:return removeCoupon()" OnClick="lbtmDeleteCoupon_Click" Text="<%$ Resources:GUIStrings, DeleteCoupon %>" CouponId='<%# DataBinder.Eval(Container.DataItem,"Id") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <asp:Label ID="lblNoCoupons" runat="server" Text="<%$ Resources:GUIStrings, NoCouponAppliedToOrder %>" />
    </div>
</div>
