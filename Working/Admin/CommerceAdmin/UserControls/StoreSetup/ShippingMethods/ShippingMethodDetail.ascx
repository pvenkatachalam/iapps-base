﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingMethodDetail.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ShippingMethodDetail" %>
<script type="text/javascript">
    function SetShippingOptionDetails(selectedNode) {
        var name = selectedNode.get_text();
        var description = selectedNode.getProperty('Description');
        var daysToShip = selectedNode.getProperty('DaysToShip');
        var status = selectedNode.getProperty('Status');
        var isInternational = selectedNode.getProperty('IsInternational');
        var serviceCode = selectedNode.getProperty('ServiceCode');
        var extShipCode = selectedNode.getProperty('ExternalShipCode');

        if (name != null && name != "undefined")
            document.getElementById("<%=txtName.ClientID%>").value = selectedNode.get_text();
        else
            document.getElementById("<%=txtName.ClientID%>").value = '';

        if (description != null && description != "undefined")
            document.getElementById("<%=txtDescription.ClientID%>").value = description;
        else
            document.getElementById("<%=txtDescription.ClientID%>").value = '';

        if (daysToShip != null && daysToShip != 'undefined' && daysToShip != '0')
            document.getElementById("<%=txtDaysToShip.ClientID%>").value = daysToShip;
        else
            document.getElementById("<%=txtDaysToShip.ClientID%>").value = '';

        if (status != null && status != "undefined") {
            if (status == 1)
                document.getElementById("<%=ddlIsActive.ClientID%>").value = "True";
            else
                document.getElementById("<%=ddlIsActive.ClientID%>").value = "False";
        }

        if (isInternational != null && isInternational != "undefined") {
            if (isInternational.toLowerCase() == 'true')
                document.getElementById("<%=ddlIsInternational.ClientID%>").value = "True";
            else
                document.getElementById("<%=ddlIsInternational.ClientID%>").value = "False";
        }

        if (serviceCode != null && serviceCode != 'undefined' && serviceCode != '0')
            document.getElementById("<%=txtServiceCode.ClientID%>").value = serviceCode;
        else
            document.getElementById("<%=txtServiceCode.ClientID%>").value = '';

        if (extShipCode != null && extShipCode != 'undefined' && extShipCode != '0')
            document.getElementById("<%=txtExtShipCode.ClientID%>").value = extShipCode;
        else
            document.getElementById("<%=txtExtShipCode.ClientID%>").value = '';

        LoadGrid('');
    }

    function CancelClicked() {
        SetShippingOptionDetails(selectedTreeNode);
        return false;
    }

    function ValidateShippingOptionDetails() {

        var errorMsg = "";
        var name = Trim(document.getElementById("<%=txtName.ClientID %>").value);
        var description = Trim(document.getElementById("<%=txtDescription.ClientID %>").value);
        var daysToship = Trim(document.getElementById("<%=txtDaysToShip.ClientID %>").value);
        var serviceCode = Trim(document.getElementById("<%=txtServiceCode.ClientID %>").value);
        var extShipCode = Trim(document.getElementById("<%=txtExtShipCode.ClientID %>").value);
        var intValidatorControl = document.getElementById('<%=cvtxtDaysToShip.ClientID %>');

        if (name == '') {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NameCanNotBeEmpty %>' />\n";
        }
        else {
            if (name.match(/[&\<\>]/)) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheName %>' />\n";
            }
        }

        var returnval = CheckForDuplicate(selectedTreeNode, Trim(name), false)
        if (returnval == false) errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, DuplicateNameInThisLevelPleaseChooseADifferentName %>' />"


        if (description != '') {
            if (description.match(/[&\<\>]/)) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheDescription %>' />\n";
            }
        }

        if (daysToship != '') {
            ValidatorEnable(intValidatorControl, true);
            if (!intValidatorControl.isvalid) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAValidNumberInDaysToShip %>' />\n";
        }
        ValidatorEnable(intValidatorControl, false);
    }

   <%-- if (serviceCode != '') {
        if (serviceCode.match(/[&\<\>]/)) {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheDescription %>' />\n";
            }
        }--%>

        if (extShipCode != '') {
            if (extShipCode.match(/[&\<\>]/)) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheDescription %>' />\n";
            }
        }

        if (errorMsg == '') {
            return true;
        }
        else {
            alert(errorMsg);
            return false;
        }

    }

</script>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Name %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="500"></asp:TextBox>
    </div>
</div>
<div class="form-multi-row">
    <label class="form-label">
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtDescription" runat="server" Width="500" CssClass="textBoxes"
            TextMode="MultiLine" Rows="3"></asp:TextBox>
    </div>
    <div class="clear-fix"></div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DaysToShip %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtDaysToShip" runat="server" Width="500" CssClass="textBoxes"></asp:TextBox>
        <asp:CompareValidator ID="cvtxtDaysToShip" CultureInvariantValues="true" ControlToValidate="txtDaysToShip"
            runat="server" Type="Integer" Operator="DataTypeCheck" Display="None" ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidNumberInDaysToShip %>" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, IsActive %>" /></label>
    <div class="form-value">
        <asp:DropDownList ID="ddlIsActive" runat="server" Width="270" AutoPostBack="false">
            <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" Value="True"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" Value="False"></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, IsInternational %>" /></label>
    <div class="form-value">
        <asp:DropDownList ID="ddlIsInternational" runat="server" Width="270" AutoPostBack="false">
            <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" Value="True"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" Value="False"></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, ServiceCode %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtServiceCode" runat="server" Width="500" CssClass="textBoxes"></asp:TextBox>
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ExternalShipCode %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtExtShipCode" runat="server" Width="500" CssClass="textBoxes"></asp:TextBox>
    </div>
</div>
<div class="tab-footer">
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        CausesValidation="true" OnClientClick="return ValidateShippingOptionDetails();"
        OnClick="btnSave_Click" />
</div>
