﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingOptions.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ShippingOptions" %>
<script type="text/javascript">
    function SetGridLevelGeneralVariablesinPage() {
        setTimeout(function () {
            gridObject = grdDeliveryRates;
            menuObject = mnuDeliveryCharges;
            gridObjectName = "grdDeliveryRates";
            uniqueIdColumn = 4;
            operationColumn = 5;
        }, 500);
    }

    function PageLevelCallbackComplete() {
    }

    function PageLevelOnContextMenu(menuItems) {
        return true;
    }

    function PageLevelOnItemSelect(selectedMenuId, currentGridItem, sender, eventArgs) {
    }

    function PageLevelAddItem(currentGridItem) {

        txtSAControl = document.getElementById(txtStartAmount);

        if (gridObject.Data.length == 1) {
            txtSAControl.value = 0;
            txtSAControl.disabled = true;
        }
        return true;
    }

    function PageLevelEditItem(currentGridItem) {
        txtSAControl = document.getElementById(txtStartAmount);
        if (currentGridItem.getMember('StartAmount').get_text() == '0' || currentGridItem.getMember('StartAmount').get_text() == '0.0000') txtSAControl.disabled = true;
        return true;
    }

    function PageLevelDeleteItem(currentGridItem) {
        return 'true';
    }
    function PageLevelSaveRecord() {
        return true;
    }

    function PageLevelCancelEdit() {
        return true;
    }

    function PageLevelGridValidation(currentGridItem) {
        var returnVal = true;
        var moneyMaxValue = 922337203685477.5807;
        var startAmount = Trim(document.getElementById(txtStartAmount).value);
        var endAmount = currentGridItem.getMember('EndAmount').get_text(); //Trim(document.getElementById(lblEndAmount).value) ;        
        var shippingPrice = Trim(document.getElementById(txtShippingPrice).value);
        if (endAmount == '') endAmount = 0;
        var isRangeValid = 'true'
        var errorMsg = ''

        var doubleValidatorControl = document.getElementById('<%=cvDoubleFormat.ClientID %>');

        var dummyTextBox = document.getElementById('<%=dummyTxtValue.ClientID %>');
        dummyTextBox.value = startAmount;

        ValidatorEnable(doubleValidatorControl, true);
        if (startAmount == '') {
            errorMsg = errorMsg + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAStartAmount %>' />\n";
        }
        else if (!doubleValidatorControl.isvalid) {
            errorMsg = errorMsg + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAValidStartAmount %>' />\n";
        }
        else {
            if (gridObject.Data.length != 1) {
                if (gridOperation == "Insert")
                    isRangeValid = IsShippingOptionRatesRangeValid("00000000-0000-0000-0000-000000000000", selectedTreeNodeId, startAmount, endAmount);
                else {
                    var id = currentGridItem.getMember('Id').get_text();
                    isRangeValid = IsShippingOptionRatesRangeValid(id, selectedTreeNodeId, startAmount, endAmount);

                }
                if (isRangeValid == 'false') {
                    errorMsg = errorMsg + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAValidStartAmount %>' />\n";

                }

            }
        }

        dummyTextBox.value = shippingPrice;
        if (shippingPrice == '')
            errorMsg = errorMsg + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAShippingPrice %>' />\n";
        else if (!doubleValidatorControl.isvalid || startAmount > moneyMaxValue) {
            errorMsg = errorMsg + "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseEnterAValidShippingPrice %>' />\n";
        }

        ValidatorEnable(doubleValidatorControl, false);

        if (errorMsg != '') {
            alert(errorMsg);
            returnVal = false;
        }
        return returnVal;
    }

    function num(textbox, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;

        return ((k > 47 && k < 58) || (k == 46 && textbox.value.indexOf('.') < 0));
    }
</script>
<ComponentArt:Menu ID="mnuDeliveryCharges" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="menu_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<ComponentArt:Grid ID="grdDeliveryRates" SliderPopupClientTemplateId="DeliveryRatesSliderTemplate"
    SkinID="Default" runat="server" RunningMode="Callback" PagerInfoClientTemplateId="DeliveryRatesPaginationTemplate"
    AllowEditing="true" AutoCallBackOnInsert="false" SliderPopupCachedClientTemplateId="DeliveryRatesSliderTemplateCached"
    CallbackCachingEnabled="true" AutoCallBackOnUpdate="false" CallbackCacheLookAhead="10"
    PageSize="10" AllowPaging="True" ItemDraggingEnabled="false" AllowMultipleSelect="false"
    EditOnClickSelectedItem="false" Width="100%">
    <ClientEvents>
        <ContextMenu EventHandler="grid_onContextMenu" />
        <CallbackComplete EventHandler="grid_onCallbackComplete" />
        <Load EventHandler="grid_onLoad" />
    </ClientEvents>
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="false" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                    EditControlType="Custom" EditCellServerTemplateId="svtStartAmount" HeadingText="<%$ Resources:GUIStrings, StartAmount %>"
                    DataField="StartAmount" Align="Right" DataCellClientTemplateId="StartAmountHoverTemplate"
                    Width="140" AllowSorting="False" />
                <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                    HeadingText="<%$ Resources:GUIStrings, EndAmount %>" DataField="EndAmount"
                    Align="Right" EditControlType="Custom" DataCellClientTemplateId="EndAmountHoverTemplate"
                    Width="140" AllowSorting="False" />
                <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                    HeadingText="<%$ Resources:GUIStrings, ShippingPrice %>" DataField="ShippingPrice"
                    Align="Right" EditControlType="Custom" EditCellServerTemplateId="svtShippingPrice"
                    DataCellClientTemplateId="ShippingPriceHoverTemplate" Width="140" AllowSorting="False" />
                <ComponentArt:GridColumn AllowReordering="false" IsSearchable="false" FixedWidth="true"
                    AllowSorting="False" HeadingText="<%$ Resources:GUIStrings, Actions %>" Align="left"
                    EditCellServerTemplateId="svtActions" EditControlType="Custom" Width="80"
                    DataCellCssClass="data-cell last-data-cell" HeadingCellCssClass="heading-cell last-heading-cell" />
                <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn IsSearchable="false" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="StartAmountHoverTemplate">
            <div title="## DataItem.GetMember('StartAmount').get_text() ##">
                ## DataItem.GetMember('StartAmount').get_text() == "" ? "&nbsp;" : DataItem.GetMember('StartAmount').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="EndAmountHoverTemplate">
            <div title="## DataItem.GetMember('EndAmount').get_text() ##">
                ## DataItem.GetMember('EndAmount').get_text() == "" || DataItem.GetMember('EndAmount').get_text()=='0'
                ||DataItem.GetMember('EndAmount').get_text()=='0.0000' ? "&nbsp;" : DataItem.GetMember('EndAmount').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="ShippingPriceHoverTemplate">
            <div title="## DataItem.GetMember('ShippingPrice').get_text() ##">
                ## DataItem.GetMember('ShippingPrice').get_text() == "" ? "&nbsp;" : DataItem.GetMember('ShippingPrice').get_text()
                ##</div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DeliveryRatesSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##</p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdDeliveryRates.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdDeliveryRates.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DeliveryRatesSliderTemplateCached">
            <div class="SliderPopup">
                <h5>
                    ## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##</p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdDeliveryRates.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdDeliveryRates.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="DeliveryRatesPaginationTemplate">
            ##stringformat(Page0Of12items, currentPageIndex(grdDeliveryRates), pageCount(grdDeliveryRates),
            grdDeliveryRates.RecordCount)##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
    <ServerTemplates>
        <ComponentArt:GridServerTemplate ID="svtStartAmount">
            <Template>
                <asp:TextBox CssClass="textBoxes" ID="txtStartAmount" runat="server" Width="120">
                </asp:TextBox>
            </Template>
        </ComponentArt:GridServerTemplate>
        <ComponentArt:GridServerTemplate ID="svtShippingPrice">
            <Template>
                <asp:TextBox CssClass="textBoxes" ID="txtShippingPrice" runat="server" Width="120">
                </asp:TextBox>
            </Template>
        </ComponentArt:GridServerTemplate>
        <ComponentArt:GridServerTemplate ID="svtActions">
            <Template>
                <table>
                    <tr>
                        <td>
                            <input type="image" alt="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                                runat="server" src="/iapps_images/cm-icon-add.png" id="ImageButton11"
                                onclick="gridObject_BeforeUpdate();return false;" />
                        </td>
                        <td>
                            <img alt="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                                src="/iapps_images/cm-icon-delete.png" id="Img4" runat="server" onclick="CancelClicked();" />
                        </td>
                    </tr>
                </table>
            </Template>
        </ComponentArt:GridServerTemplate>
    </ServerTemplates>
</ComponentArt:Grid>
<asp:TextBox CssClass="textBoxes" ID="dummyTxtValue" runat="server" Style="display: none"></asp:TextBox>
<asp:CompareValidator ID="cvDoubleFormat" runat="server" ControlToValidate="dummyTxtValue"
    Operator="DataTypeCheck" Type="Double" CultureInvariantValues="true" ErrorMessage="*"
    Display="None" Text="*" Enabled="false" />
