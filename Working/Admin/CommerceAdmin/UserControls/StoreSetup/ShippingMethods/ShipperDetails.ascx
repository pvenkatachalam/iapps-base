﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipperDetails.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.ShipperDetails" %>
<script type="text/javascript">
    function SetShipperDetails(selectedNode) {
        var name = selectedNode.get_text();
        var description = selectedNode.getProperty('Description');
        var trackingURL = selectedNode.getProperty('TrackingURL');
        var status = selectedNode.getProperty('Status');

        if (name != null && name != "undefined")
            document.getElementById("<%=txtName.ClientID%>").value = name;
        else
            document.getElementById("<%=txtName.ClientID%>").value = '';

        if (description != null && description != "undefined")
            document.getElementById("<%=txtDescription.ClientID%>").value = description;
        else
            document.getElementById("<%=txtDescription.ClientID%>").value = '';

        if (trackingURL != null && trackingURL != 'undefined')
            document.getElementById("<%=txtTrackingURL.ClientID%>").value = trackingURL;
        else
            document.getElementById("<%=txtTrackingURL.ClientID%>").value = '';


        if (status != null && status != "undefined") {
            if (status == 1)
                document.getElementById("<%=ddlIsActive.ClientID%>").value = "True";
            else
                document.getElementById("<%=ddlIsActive.ClientID%>").value = "False";
        }

    }

    function ShipperDetailsCancelClicked() {
        SetShipperDetails(selectedTreeNode);
        return false;
    }

    function validateShipperDetails() {

        var errorMsg = "";
        var name = Trim(document.getElementById("<%=txtName.ClientID %>").value);
        var description = Trim(document.getElementById("<%=txtDescription.ClientID %>").value);
        var trackingURL = Trim(document.getElementById("<%=txtTrackingURL.ClientID %>").value);

        if (name == '') {
            errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, NameCanNotBeEmpty %>' />\n";
        }
        else {
            if (name.match(/[&\<\>]/)) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheName %>' />\n";
            }
        }

        if (description != '') {
            if (description.match(/[&\<\>]/)) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheDescription %>' />\n";
            }
        }

        if (trackingURL != '') {
            if (trackingURL.match(/[\<\>]/)) {
                errorMsg += "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, PleaseRemoveAnyAndCharactersFromTheTrackingURL %>' />\n";
            }
        }

        if (errorMsg == '') {
            return true;
        }
        else {
            alert(errorMsg);
            return false;
        }

    }
</script>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Name %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" Width="500"></asp:TextBox>
    </div>
</div>
<div class="form-multi-row">
    <label class="form-label">
        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Description1 %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtDescription" runat="server" Width="500" CssClass="textBoxes"
            TextMode="MultiLine" Rows="3"></asp:TextBox>
    </div>
    <div class="clear-fix">
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, TrackingURL %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtTrackingURL" runat="server" Width="500" CssClass="textBoxes"></asp:TextBox>
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, IsActive %>" /></label>
    <div class="form-value">
        <asp:DropDownList ID="ddlIsActive" runat="server" Width="270" AutoPostBack="false">
            <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" Value="True"></asp:ListItem>
            <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" Value="False"></asp:ListItem>
        </asp:DropDownList>
    </div>
</div>
<div class="tab-footer">
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        OnClientClick="return validateShipperDetails();" OnClick="btnSave_Click" />
</div>
