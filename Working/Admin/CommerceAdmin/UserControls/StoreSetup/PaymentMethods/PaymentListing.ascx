﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentListing.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.PaymentListing" %>
<%@ Register Src="~/UserControls/General/DateRange.ascx" TagName="DateRange" TagPrefix="uc1" %>
<script type="text/javascript">
    //<![CDATA[

    var paymentId;
    var orderId;
    var orderPaymentId;
    var paymentIdColumnId = 7;
    var paymentStatusIdColumnId = 9;
    var orderIdColumnId = 10;
    var orderPaymentIdColumnId = 11;

    var customerSearchPopup;
    var SelectedCustomerId = emptyGuid;
    var SelectedCustomerName = '';

    function grdPaymentListing_onContextMenu(sender, eventArgs) {
        var evt = eventArgs.get_event();
        var paymentListItem = eventArgs.get_item();
        paymentId = paymentListItem.getMember(paymentIdColumnId).get_text();
        orderId = paymentListItem.getMember(orderIdColumnId).get_text();
        orderPaymentId = paymentListItem.getMember(orderPaymentIdColumnId).get_text();

        var menuItems = mnuPayments.get_items();
        for (i = 0; i < menuItems.get_length() ; i++) {
            if (menuItems.getItem(i).get_id() == 'cmApprove' || menuItems.getItem(i).get_id() == 'cmReject') {
                paymentStatusId = paymentListItem.getMember(paymentStatusIdColumnId).get_text();
                if (paymentStatusId == 11)
                    menuItems.getItem(i).set_visible(true);
                else
                    menuItems.getItem(i).set_visible(false);
            }
        }

        mnuPayments.showContextMenuAtEvent(evt);
    }

    function mnuPayments_onItemSelect(sender, eventArgs) {
        var menuItem = eventArgs.get_item();
        var selectedMenuId = menuItem.get_id();
        mnuPayments.hide();
        if (selectedMenuId == 'cmNotes') {
            window.location = '<%=ResolveUrl("~/StoreManager/Payments/PaymentNotes.aspx") %>' + '?PaymentId=' + paymentId;
        }
        else if (selectedMenuId == 'cmOrder') {
            window.location = '<%=ResolveUrl("~/StoreManager/Orders/NewOrders.aspx") %>' + '?OrderId=' + orderId;
        }
        else if (selectedMenuId == 'cmApprove') {

            ShowLoadingModalBox();
            var msg = ApproveRefund(orderPaymentId);
            CloseLoadingModalBox();
            alert(msg);
            grdPaymentListing.callback();
        }
        else if (selectedMenuId == 'cmReject') {
            ShowReasonModal('../../Popups/StoreManager/Orders/StatusChangeReason.aspx?Type=Refund&RefundOrderPaymentId=' + orderPaymentId);
        }
}

function ShowReasonModal(popupPath) {
    reasonPopup = dhtmlmodal.open('ShowReasonPopup', 'iframe', popupPath, 'ShowReasonPopup', 'width=405px,height=330px,center=1,resize=0,scrolling=1');
    reasonPopup.onclose = function () {
        var a = document.getElementById('ShowReasonPopup');
        var ifr = a.getElementsByTagName('iframe');
        window.frames[ifr[0].name].location.replace("about:blank");
        return true;
    }
}

function HideReasonPopup(type) {
    reasonPopup.hide();
    grdPaymentListing.callback();
}

function ShowSearchCustomerPopup() {
    popupPath = "../../Popups/StoreManager/Customers/SearchCustomer.aspx";
    customerSearchPopup = dhtmlmodal.open('CustomerSearchPopup', 'iframe', popupPath, '', 'width=705px,height=535px,center=1,resize=0,scrolling=1');
    customerSearchPopup.onclose = function () {
        var a = document.getElementById('CustomerSearchPopup');
        var ifr = a.getElementsByTagName('iframe');
        window.frames[ifr[0].name].location.replace("about:blank");
        return true;
    }
}

function ClosePopupCustomerPopup() {
    customerSearchPopup.hide();
    if (SelectedCustomerId != '' && SelectedCustomerId.length > 0 && SelectedCustomerId != '00000000-0000-0000-0000-000000000000') {
        document.getElementById("<%=hdnCustomerId.ClientID%>").value = SelectedCustomerId;
            document.getElementById("<%=hlCustomer.ClientID%>").innerHTML = SelectedCustomerName;
            document.getElementById("<%=hdnCustomerName.ClientID%>").value = SelectedCustomerName;
        }
        return true;
    }

    function FormatStatusColumn(statusText) {
        if (statusText != "")
            statusText = statusText.replace("Refund ", "");
        else
            statusText = "&nbsp;";

        if (statusText.indexOf("Waiting") > -1)
            statusText = "<span class='pendingAction'>" + statusText + "</span>";

        return statusText;
    }

    function FormatTypeColumn(typeId, typeText, paymentAmount) {
        if (paymentAmount < 0 && typeId == 1) {
            typeText = "Refund";
        }

        return typeText;
    }
    function LoadPaymentStatus(paymentType) {
        cbFilter.callback(paymentType);
        ShowLoadingModalBox();
    }
    function cbFilter_CallbackComplete() {
        CloseLoadingModalBox();
    }

</script>
<div class="grid-utility">
    <div class="columns">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Client %>" /></label>
        <div class="client-filter">
            <a href="#" onclick="ShowSearchCustomerPopup(); return false;" id="hlCustomer" runat="server">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectAClient %>" /></a>
        </div>
        <asp:HiddenField ID="hdnCustomerId" runat="server" />
        <asp:HiddenField ID="hdnCustomerName" runat="server" />
    </div>
    <div class="columns">
        <label class="form-label">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PaymentType %>" /></label>
        <asp:DropDownList ID="ddlPaymentType" runat="server" Width="120" onchange="LoadPaymentStatus(this.value);">
            <asp:ListItem Text="<%$ Resources:GUIStrings, SelectOne %>" Value="0"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="columns">
        <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Status1 %>" /></label>
        <ComponentArt:CallBack ID="cbFilter" runat="server">
            <Content>
                <asp:DropDownList ID="ddlStatus" runat="server" Width="175" />
            </Content>
            <ClientEvents>
                <CallbackComplete EventHandler="cbFilter_CallbackComplete" />
            </ClientEvents>
        </ComponentArt:CallBack>
    </div>
    <div class="columns">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ProcessorTXNId %>" /></label>
        <asp:TextBox ID="txtTXNId" runat="server" CssClass="textBoxes" Width="180" ToolTip="<%$ Resources:GUIStrings, TypeTransactionNumberHere %>"></asp:TextBox>
    </div>
    <div class="columns">
        <label class="form-label">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PaymentDate %>" /></label>
        <uc1:DateRange ID="ctlPaymentDate" runat="server" DefaultOption="PredefinedRange" />
    </div>
    <div class="columns">
        <label class="form-label">
            &nbsp;</label>
        <asp:Button ID="btnGo" runat="server" Text="<%$ Resources:GUIStrings, Filter %>"
            ToolTip="<%$ Resources:GUIStrings, FilterPayments %>" CssClass="small-button"
            OnClick="btnGo_Click" />
    </div>
    <div class="clear-fix">
    </div>
</div>
<ComponentArt:Grid SkinID="Default" ID="grdPaymentListing" SliderPopupClientTemplateId="PaymentListingSliderTemplate"
    SliderPopupCachedClientTemplateId="PaymentListingSliderTemplateCached" Width="100%"
    runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
    AutoAdjustPageSize="true" AutoPostBackOnSelect="false" PageSize="10" PagerInfoClientTemplateId="PaymentListingPaginationTemplate"
    CallbackCachingEnabled="false" SearchOnKeyPress="true" ManualPaging="true"
    LoadingPanelClientTemplateId="PaymentListingLoadingPanelTemplate" Sort="Payment.CreatedDate DESC">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="Payment.Id" RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif"
            DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
            AlternatingRowCssClass="alternate-row" HeadingTextCssClass="heading-text" SelectedRowCssClass="selected-row"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AllowSorting="true" ShowTableHeading="false"
            ShowSelectorCells="false">
            <Columns>
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, PaymentDate1 %>"
                    DataField="Payment.CreatedDate" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="PaymentDateHoverTemplate" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, PaymentType %>"
                    DataField="Payment.PaymentType.Name" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="PaymentTypeHoverTemplate" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, Status %>"
                    DataField="Payment.PaymentStatus.Name" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="StatusHoverTemplate" />
                <ComponentArt:GridColumn Width="83" HeadingText="<%$ Resources:GUIStrings, Amount %>"
                    DataField="Amount" IsSearchable="false" Align="Right" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="AmountHoverTemplate"
                    FormatString="c" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, OrderNum %>"
                    DataField="Order.PurchaseOrderNumber" IsSearchable="true" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="OrderNumbmerHoverTemplate" />
                <ComponentArt:GridColumn Width="200" HeadingText="<%$ Resources:GUIStrings, CustomerName %>"
                    DataField="Order.Customer.FirstName" IsSearchable="false" Align="Left" SortedDataCellCssClass="SortedDataCell"
                    AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="CustomerNameHoverTemplate" />
                <ComponentArt:GridColumn Width="150" HeadingText="<%$ Resources:GUIStrings, ProcessorTXNId1 %>"
                    DataField="Payment.ProcessorTransactionId" IsSearchable="false" Align="Left"
                    SortedDataCellCssClass="SortedDataCell" AllowReordering="false" FixedWidth="true"
                    DataCellClientTemplateId="TxnIdHoverTemplate" />
                <ComponentArt:GridColumn DataField="Payment.Id" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="Order.Customer.FullName" IsSearchable="false"
                    Visible="false" />
                <ComponentArt:GridColumn DataField="Payment.PaymentStatus.Id" IsSearchable="false"
                    Visible="false" />
                <ComponentArt:GridColumn DataField="Order.Id" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="Payment.Amount" IsSearchable="false" Visible="false" />
                <ComponentArt:GridColumn DataField="Payment.PaymentType.Id" IsSearchable="false"
                    Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientEvents>
        <ContextMenu EventHandler="grdPaymentListing_onContextMenu" />
    </ClientEvents>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="AmountHoverTemplate">
            <span title="## DataItem.GetMember('Amount').get_text() ##">## DataItem.GetMember('Amount').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Amount').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="OrderNumbmerHoverTemplate">
            <span title="## DataItem.GetMember('Order.PurchaseOrderNumber').get_text() ##">## DataItem.GetMember('Order.PurchaseOrderNumber').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Order.PurchaseOrderNumber').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
            <span title="## FormatStatusColumn(DataItem.GetMember('Payment.PaymentStatus.Name').get_text()) ##">## FormatStatusColumn(DataItem.GetMember('Payment.PaymentStatus.Name').get_text())
                ## </span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PaymentTypeHoverTemplate">
            <span title="## DataItem.GetMember('Payment.PaymentType.Name').get_text() ##">## FormatTypeColumn(DataItem.GetMember('Payment.PaymentType.Id').get_text(),
                DataItem.GetMember('Payment.PaymentType.Name').get_text(), DataItem.GetMember('Payment.Amount').get_text())
                ## </span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PaymentDateHoverTemplate">
            <span title="## DataItem.GetMember('Payment.CreatedDate').get_text() ##">## DataItem.GetMember('Payment.CreatedDate').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Payment.CreatedDate').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TxnIdHoverTemplate">
            <span title="## DataItem.GetMember('Payment.ProcessorTransactionId').get_text() ##">## DataItem.GetMember('Payment.ProcessorTransactionId').get_text() == "" ? "&nbsp;"
                : DataItem.GetMember('Payment.ProcessorTransactionId').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="CustomerNameHoverTemplate">
            <span title="## DataItem.GetMember('Order.Customer.FullName')##">## DataItem.GetMember('Order.Customer.FullName').get_text()
                ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="TotalHoverTemplate">
            <span title="## DataItem.GetMember('Payment.Amount').get_text() ##">## DataItem.GetMember('Payment.Amount').get_text()
                == "" ? "&nbsp;" : DataItem.GetMember('Payment.Amount').get_text() ##</span>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PaymentListingSliderTemplate">
            <div class="SliderPopup">
                <p>
                    ##_datanotload##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdPaymentListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdPaymentListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PaymentListingSliderTemplateCached">
            <div class="SliderPopup">
                <h5>## DataItem.GetMemberAt(0).Value ##</h5>
                <p>
                    ## DataItem.GetMemberAt(1).Value ##
                </p>
                <p class="paging">
                    <span class="pages">##stringformat(Page0Of1, DataItem.PageIndex + 1, grdPaymentListing.PageCount)##</span>
                    <span class="items">##stringformat(Item0Of1, DataItem.Index + 1, grdPaymentListing.RecordCount)##</span>
                </p>
            </div>
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PaymentListingPaginationTemplate">
            ##stringformat(Page0Of12items, currentPageIndex(grdPaymentListing), pageCount(grdPaymentListing),
            grdPaymentListing.RecordCount)##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="PaymentListingLoadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdPaymentListing) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
<ComponentArt:Menu ID="mnuPayments" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuPayments_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
