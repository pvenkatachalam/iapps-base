﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNewPayment.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.AddNewPayment" %>
<script type="text/javascript" language="javascript">
    var payCancelConfirmMsg = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToCancelThisPayment %>" />';
    var payVoidConfirmMsg1 = '<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToVoidThisPayment %>" />';
    function toggleCaptureInfo(linkbtn, tableRow) {
        var linkbtn = document.getElementById(linkbtn);
        var tableRow = document.getElementById(tableRow);
        if (tableRow.style.display == "none") {
            linkbtn.src = linkbtn.src.replace("NicePlus.gif", "NiceMinus.gif");
            tableRow.style.display = "table-row";
            linkbtn.title = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, HideAdditionalCaptureDetails %>' />";
        }
        else {
            tableRow.style.display = "none";
            linkbtn.src = linkbtn.src.replace("NiceMinus.gif", "NicePlus.gif");
            linkbtn.title = "<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, ShowAdditionalCaptureDetails %>' />";
        }
    }
    function AddNewPayment_onCallbackError(sender, eventArgs) {
        //alert('Add New payment callback error: ' + eventArgs.get_message());
    }
    function AddNewPayment_CallbackComplete(sender, eventArgs) {
        if (typeof ToggleSlideImage == 'function')
            ToggleSlideImage();
    }
</script>
<div class="snap-container" style="border-top: 0;">
    <ComponentArt:CallBack ID="cbAddNewPayment" OnCallback="AddNewPayment_Callback" runat="server">
        <Content>
            <div class="snap-header">
                <img class="snap-toggle" alt="Collpase Global Attributes" src="../../App_Themes/General/images/snap-expand.png" />
                <h3>
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Payments %>" /></h3>
                <asp:PlaceHolder ID="phAddNewPayment" runat="server">
                    <asp:LinkButton ID="btnAddNewPayment" runat="server" Text="<%$ Resources:GUIStrings, AddNewPayment %>"
                        Style="float: right;" CssClass="small-button"></asp:LinkButton>
                    <asp:LinkButton ID="btnAddNewRefund" runat="server" Text="<%$ Resources:GUIStrings, AddNewRefund %>"
                        Style="float: right;" CssClass="small-button"></asp:LinkButton>
                </asp:PlaceHolder>
                <div class="clearFix">
                </div>
            </div>
            <div class="snap-content order-payment-snap">
                <asp:Panel ID="pnlWarning" runat="server" CssClass="warning" Visible="false">
                    <%= GUIStrings.ApprovalRejectWarning %>
                </asp:Panel>
                <table cellpadding="0" cellspacing="0" border="0" class="grid-view" width="100%">
                    <thead>
                        <tr>
                            <th colspan="2">
                                <%= GUIStrings.Name %>
                            </th>
                            <th>
                                <%= GUIStrings.Orders_CardType %>
                            </th>
                            <th>
                                <%= GUIStrings.Orders_CardNumber %>
                            </th>
                            <th>
                                <%= GUIStrings.Orders_ExpirationDate %>
                            </th>
                            <th>
                                <%= GUIStrings.Orders_Payment %>
                            </th>
                            <th>
                                <%= GUIStrings.Orders_CapturedAmount %>
                            </th>
                            <th>
                                <%= GUIStrings.Status %>
                            </th>
                            <th>
                                <%= GUIStrings.Actions %>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rptPayments" runat="server" OnItemDataBound="rptPayments_DataBound">
                            <ItemTemplate>
                                <tr id="trPaymentInfo" runat="server">
                                    <td width="1">
                                        <asp:ImageButton ID="lbtnShowCaptureInfo" runat="server" title="<%$ Resources:GUIStrings, ShowAdditionalCaptureDetails %>"
                                            ImageUrl="~/images/NicePlus.gif" />
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hfOrderPaymentId" runat="server" />
                                        <asp:Label ID="lblNameOnCard" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCardType" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCardNumber" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCardExpiry" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCapturedAmount" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                        <asp:Label ID="lblAuthCode" runat="server" Visible="false" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnCopy" runat="server" title="<%$ Resources:GUIStrings, CopyEncryptedCreditCardNumberToClipBoardForManualProcessing %>"
                                            Text="<%$ Resources:GUIStrings, Copy %>" Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnAuthorize" runat="server" title="<%$ Resources:GUIStrings, AuthorizeCreditCardForAmountOfPayment %>"
                                            OnClick="lbntAuthorize_Click" Text="<%$ Resources:GUIStrings, Authorize %>"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnCapture" runat="server" title="<%$ Resources:GUIStrings, CaptureFundsUpToAmountOfPayment %>"
                                            Text="<%$ Resources:GUIStrings, Capture %>"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnCancel" runat="server" OnClientClick="var yesOrNo = window.confirm(payCancelConfirmMsg);if(yesOrNo==false){return false;}"
                                            OnClick="btnCancel_Click" title="<%$ Resources:GUIStrings, MustBeReconciledWithCustomerThroughVirtualTerminalOrOtherMethod %>"
                                            Text="<%$ Resources:GUIStrings, ManualCancel %>"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnVoid" runat="server" OnClientClick="var yesOrNo = window.confirm(payVoidConfirmMsg1);if(yesOrNo==false){return false;}"
                                            title="<%$ Resources:GUIStrings, VoidPayment %>" OnClick="lbntVoid_Click" Text="<%$ Resources:GUIStrings, Void %>"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnRefund" runat="server" title="<%$ Resources:GUIStrings, RefundPayment %>"
                                            Text="<%$ Resources:GUIStrings, Refund %>"></asp:LinkButton>
                                        <asp:LinkButton ID="lbtnRefundAction" runat="server" Visible="false" OnClick="lbtnRefundAction_Click" />&nbsp;
                                        <asp:LinkButton ID="lbtnRejectRefund" runat="server" Visible="false" OnClick="lbtnRefundAction_Click"
                                            CommandName="Reject" />
                                    </td>
                                </tr>
                                <tr style="display: none;" id="trCaptureInfo" runat="server">
                                    <td colspan="9" style="padding: 0 !important;">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <asp:Repeater ID="rptCaptures" runat="server" OnItemDataBound="rptCaptures_DataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="border-bottom: 0px solid #869FC8;" colspan="2">
                                                            <asp:Literal ID="ltlCaptureAmount" runat="server" />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:PlaceHolder ID="phRefundDetails" runat="server" Visible="false">
                                                <tr>
                                                    <td colspan="2">
                                                        <asp:Literal ID="lblRefundReason" runat="server" />
                                                    </td>
                                                </tr>
                                            </asp:PlaceHolder>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:PlaceHolder ID="phNoResults" runat="server" Visible="false">
                            <tr>
                                <td colspan="9">
                                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, NoPaymentsForCurrentOrder %>" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                    </tbody>
                </table>
            </div>
        </Content>
        <LoadingPanelClientTemplate>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        Loading...
                    </td>
                </tr>
            </table>
        </LoadingPanelClientTemplate>
        <ClientEvents>
            <CallbackComplete EventHandler="AddNewPayment_CallbackComplete" />
            <CallbackError EventHandler="AddNewPayment_onCallbackError" />
        </ClientEvents>
    </ComponentArt:CallBack>
</div>
