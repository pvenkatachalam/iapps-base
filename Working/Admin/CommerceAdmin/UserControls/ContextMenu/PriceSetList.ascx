﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceSetList.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.UserControls.ContextMenu.PriceSetList" %>
<table border="0" cellpadding="0" cellspacing="10" class="context-menu-table">
    <tr>
        <td><asp:ListBox ID="lstPriceSets" runat="server" Width="265" Rows="4" SelectionMode="Multiple" /></td>
    </tr>
    <tr>
        <td align="right">
            <input type="button" value ="<%$ Resources:GUIStrings, Save %>" runat="server" id="btnSavePriceList" class ="button" />
        </td>
    </tr>
</table>