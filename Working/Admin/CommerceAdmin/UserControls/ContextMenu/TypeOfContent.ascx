<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAppsCMS.AdminControls.TypeOfContent" %>
<script type="text/javascript">
    function SelectedContentType(dwObjectId) {
        var selectedValue, selectedText;
        selectedValue = document.getElementById(dwObjectId).value;
        selectedText = document.getElementById(dwObjectId).options[document.getElementById(dwObjectId).selectedIndex].text;
        HideMenu();
        if (selectedValue == 'Free Form')
            window.parent.SetContentType('Content', '00000000-0000-0000-0000-000000000000');
        else
            window.parent.SetContentType(selectedText, selectedValue);
        window.parent.contentContextMenuClickHandler('AddContent');
        return false;
    }
    function HideMenu() {
        window.parent.msContentGridContextMenu.hide();
    }
</script>
<table border="0" cellpadding="0" cellspacing="0" style="background-color: #fff; width: 300px;">
    <tr>
        <td style="padding: 10px;">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ContentDefinitionTemplate %>" />
        </td>
        <td>
            <asp:DropDownList ID="dwContentTemplates" runat="server" Width="200" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="text-align: right; padding: 10px;">
            <input type="button" id="btnCreate" runat="server" value="<%$ Resources:GUIStrings, Create %>"
                class="button" title="<%$ Resources:GUIStrings, Create %>" />
        </td>
    </tr>
</table>
