﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateAndConnectCategory.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CreateAndConnectCategory" %>
<script type="text/javascript">
    var txtCategoryName;
    var lbConnectSelectedTemplate;

    function CreateConnectPage() {

        var catName = Trim(document.getElementById(txtCategoryName).value);
        var templateID = Trim(document.getElementById(lbConnectSelectedTemplate).value);


        CreateCategoryAndPage(catName, templateID);

    }
</script>
<div class="ContextMenuWrapper">
    <table border="0" cellpadding="0" cellspacing="0" class="context-menu-table">
        <tr>
            <td style="padding: 0 5px 10px 0; vertical-align: top; width: 100px;">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, CategoryName %>" />
            </td>
            <td style="padding-bottom: 10px; vertical-align: top;">
                <asp:TextBox ID="txtCatName" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="padding: 0 5px 10px 0; vertical-align: top; width: 100px;">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, TemplateName %>" />
            </td>
            <td style="padding-bottom: 10px; vertical-align: top;">
                <asp:ListBox ID="lstTemplates" runat="server" Width="210" Rows="5"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right" style="padding-top: 10px; vertical-align: top;">
                <input type="button" value="<%$ Resources:GUIStrings, Cancel %>" title="<%$ Resources:GUIStrings, Cancel %>"
                    runat="server" id="btnCancel" class="button" onclick="return CloseConnectPage();" />
                <input type="button" value="<%$ Resources:GUIStrings, Save %>" title="<%$ Resources:GUIStrings, Save %>"
                    runat="server" id="btnSave" class="primarybutton" onclick="CreateConnectPage();" />
            </td>
        </tr>
    </table>
</div>
