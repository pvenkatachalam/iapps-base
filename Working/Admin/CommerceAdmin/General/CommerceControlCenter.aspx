﻿<%@ Page Language="C#" MasterPageFile="~/MainMaster.Master" AutoEventWireup="true"
    CodeBehind="CommerceControlCenter.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Commerce.Web.CommerceControlCenter"
    Title="iAPPS Commerce: Control Center" StylesheetTheme="General" %>

<%@ Register TagPrefix="tvp" TagName="TopViewedProducts" Src="~/UserControls/Reporting/Products/TopViewedProducts.ascx" %>
<%@ Register TagPrefix="tpp" TagName="TopPurchasedProducts" Src="~/UserControls/Reporting/Products/TopPurchasedProducts.ascx" %>
<%@ Register TagPrefix="tpc" TagName="TopPurchasedCategories" Src="~/UserControls/Reporting/Products/TopSalesCategory.ascx" %>
<%@ Register TagPrefix="ts" TagName="TodaysSummary" Src="~/UserControls/General/TodaysSummary.ascx" %>
<%@ Register TagPrefix="fs" TagName="Financials" Src="~/UserControls/General/Financials.ascx" %>
<%@ Register TagPrefix="pi" TagName="ProductInventory" Src="~/UserControls/General/ProductInventory.ascx" %>
<%@ Register TagPrefix="rpDr" TagName="ReportDateRange" Src="~/UserControls/Reporting/ReportDateRange.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="DashboardHead" ContentPlaceHolderID="head" runat="server">
    <link href="../css/Dashboard.css" type="text/css" rel="Stylesheet" media="all" />
    <link href="../css/Reports.css" type="text/css" rel="Stylesheet" media="all" />
    <script type="text/javascript">
        var tooltipIconSrc = '../App_Themes/General/images/icon-tooltip.gif';
        var tooltipImage = "../App_Themes/General/images/tooltip-arrow.gif";
        var reverseTooltipImage = "../App_Themes/General/images/rev-tooltip-arrow.gif";
        var downTooltipImage = "../App_Themes/General/images/down-tooltip-arrow.gif";
        var reversedownTooltipImage = "../App_Themes/General/images/rev-down-tooltip-arrow.gif";
        //method to split comma separated navigation categories
        function SplitNavCategory(strNavCategories) {
            var arrNavigtaion = strNavCategories.split(',');
            var strHTML = "";
            for (var i = 0; i < arrNavigtaion.length; i++) {
                strHTML += arrNavigtaion[i] + "<br />";
            }
            return strHTML;
        }
    </script>

    <script language="javascript" type="text/javascript" defer="defer">
        function init() {
            Sys.Net.WebRequestManager.add_invokingRequest(beginRequest);
            Sys.Net.WebRequestManager.add_completedRequest(endRequest);
        }
        function beginRequest(sender, args) {
            ShowLoadingModalBox();
        }
        function endRequest(sender, args) {
            try {
                CloseLoadingModalBox();
            }
            catch (exp) {
            }
        }
        $(document).ready(function() {
            init();
        });
    </script>

</asp:Content>
<asp:Content ID="DashboardContent" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <cc1:ToolkitScriptManager EnablePageMethods="true" ID="sm1" EnableScriptGlobalization="true" ScriptMode="Release" EnableCdn="true" runat="server" />
    <div class="dashboard">
        <div class="left-column">
            <div class="box">
                <div class="calendar-header">
                    <h2><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings,  TodaysSummary  %>" /></h2>
                    <img src="../App_Themes/General/images/calendar-handle.png" alt="" class="left-handle" />
                    <img src="../App_Themes/General/images/calendar-handle.png" alt="" class="right-handle" />
                </div>
                <div class="box-content">
                    <ts:TodaysSummary ID="ctlTodaysSummary" runat="server" />
                </div>
            </div>
            <div class="box">
                <h3><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, FinancialSummary %>" /></h3>
                <div class="box-content">
                    <fs:Financials ID="ctlFinancials" runat="server" />
                </div>
            </div>
            <div class="box">
                <h3><asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, LowInventoryAlerts %>" /></h3>
                <div class="box-content">
                    <pi:ProductInventory ID="ctlProductInventory" runat="server" />
                </div>
            </div>
        </div>
        <div class="right-column">
            <rpDr:ReportDateRange ID="ctlReportDateRange" runat="server"></rpDr:ReportDateRange>
            <div class="clear-fix"></div>
            <h2 style="margin-top: 0;"><%= GUIStrings.TopViewedProducts %></h2>
            <tvp:TopViewedProducts ID="ctlTopViewedProducts" runat="server" />
            <h2><%= GUIStrings.TopPurchasedProducts %></h2>
            <tpp:TopPurchasedProducts ID="ctlTopPurchasedProducts" runat="server" />
            <h2><%= GUIStrings.TopPurchasedCategories %></h2>
            <tpc:TopPurchasedCategories ID="ctlTopPurchasedCategories" runat="server" />
        </div>
    </div>
    <div class="clear-fix"></div>
</asp:Content>