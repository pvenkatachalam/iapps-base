﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="HtmlEditor.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.HtmlEditor" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="HTMLEditor" Src="~/UserControls/Editor/HTMLEditor.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function Editor_GetDefaultData() {
            var htmlText = "";
            if (typeof popupActionsJson.CustomAttributes["HtmlContent"] != "undefined")
                htmlText = popupActionsJson.CustomAttributes["HtmlContent"];

            return htmlText;
        }

        function SaveHtmlEditor() {
            var htmlContent = GetEditorContent();
            popupActionsJson.CustomAttributes["HtmlContent"] = htmlContent;

            SelectiAppsAdminPopup();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:HTMLEditor ID="htmlEditor" runat="server" Width="810" Height="490" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" runat="server" CssClass="button cancel-button" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SaveHtmlEditor();" />
</asp:Content>
