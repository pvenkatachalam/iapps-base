<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.SelectAttributePopup" StylesheetTheme="General"
    CodeBehind="SelectAttributePopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="Attributes" Src="~/UserControls/Administration/ManageAttributes.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SelectListItems() {
            listItemsJson = [];
            var listItemsJsonTemp = [];
            if (popupActionsJson.CustomAttributes["ListItemsJson"] != "undefined" &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != null &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != "") {
                listItemsJsonTemp = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);
                if (listItemsJsonTemp.length > 0) {
                    $.each(listItemsJsonTemp, function () {
                        listItemsJson.push({ "Id": this.Id, "Title": this.Title });
                    });
                }
            }

            $.each(grdListContent.dataSource.data(), function (index, item) {
                listItemsJson.push({ "Id": item.Id, "Title": item.Title });
            });

            if (gridActionsJson.SelectedItems.length > 0 && !CheckListItemExist(gridActionsJson.SelectedItems.first())) {
                alert("<%= JSMessages.PleaseAddListItems %>");
        }
        else {
            popupActionsJson.CustomAttributes["ListItemsJson"] = JSON.stringify(listItemsJson);

            SelectiAppsAdminPopup();
        }

        return false;
    }

    var grdListContent;
    var listItemsJson = [];
    $(function () {
        if (popupActionsJson.CustomAttributes["ListItemsJson"] != "undefined" &&
            popupActionsJson.CustomAttributes["ListItemsJson"] != null &&
            popupActionsJson.CustomAttributes["ListItemsJson"] != "")
            listItemsJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);

        $.each(listItemsJson, function () {
            AddNewListItem(this.Id, this.Title);
        });

        grdListContent = $("#grdListContent").kendoGrid({
            dataSource: listItemsJson,
            schema: {
                model: { id: "Id" }
            },
            columns: [{
                field: "Title",
                template: kendo.template($("#grdListContentTemplate").html()),
                headerAttributes: {
                    style: "display: none"
                }
            }]
        }).data("kendoGrid");
    });

    function UpdateManualListItem(item, selected) {
        if (selected) {
            if (!CheckListItemExist(item.Key)) {
                listItemsJson.push({ "Id": item.Key, "Title": item.getValue("Title") });
                AddNewListItem(item.Key, item.getValue("Title"));
            }
        }
        else {
            DeleteListItem(item.Key);
        }
    }

    function DeleteManualListItem(id) {
        DeleteListItem(id);
        UnselectItem(id);
    }

    function DeleteListItem(id) {
        var $row = $(".selected-attributes .modal-grid-row[objectid='" + id + "']")
        var dataItem = grdListContent.dataItem($row.closest("tr"));

        $.each(listItemsJson, function (i) {
            if (this.Id.toString() == dataItem.Id.toString()) {
                listItemsJson.splice(i, 1);
                return false;
            }
        });

        grdListContent.dataSource.remove(dataItem);

        return false;
    }

    function AddNewListItem(id, title) {
        var dataSource = grdListContent.dataSource;
        var rowNumber = dataSource.data().length;

        var newRowItem = {};
        newRowItem.Id = id;
        newRowItem.Title = title;

        dataSource.insert(rowNumber, newRowItem);
    }

    function CheckListItemExist(id) {
        var exists = false;
        $.each(listItemsJson, function () {
            if (this.Id.toString() == id.toString()) {
                exists = true;
                return false;
            }
        });
        return exists;
    }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manual-list-left">
        <UC:Attributes ID="ucAttributes" runat="server" />
    </div>
    <div class="manual-list-right">
        <h2>
            <asp:Literal ID="ltGridHeading" runat="server" Text="<%$ Resources:GUIStrings, Attributes %>" /></h2>
        <div class="grid-section selected-attributes" id="grdListContent">
        </div>
        <script id="grdListContentTemplate" type="text/x-kendo-template">
            <div class="row">
                <div class="modal-grid-row clear-fix" objectId="#: Id #">
                    <div class="first-cell">
                        #: Title #
                    </div>
                    <div class="last-cell">
                        <a class="delete-list-item">
                            <img src="/Admin/App_Themes/General/images/icon-delete-cross.png" border="0" alt="X" onclick="DeleteManualListItem('#: Id #')" /></a>
                    </div>
                </div>
            </div>
        </script>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, Save%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SelectListItems();" />
</asp:Content>
