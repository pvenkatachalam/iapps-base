﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManageAttributeCategoryDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.ManageAttributeCategoryDetails"
    Theme="General" ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valDetails" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="attribute-validation">
        <div class="form-row">
            <label class="form-label">
                &nbsp;</label>
            <asp:CheckBox ID="chkIsRequired" runat="server" Text="Is Required?" />
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.ValidationMessage %></label>
            <asp:TextBox ID="txtErrorMessage" runat="server" CssClass="textBoxes" Width="260" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:HiddenField ID="hdnId" runat="server" />
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ValidationGroup="valDetails"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
</asp:Content>
