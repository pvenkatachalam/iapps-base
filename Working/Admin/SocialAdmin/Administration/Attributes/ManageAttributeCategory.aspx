<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    Theme="General" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageAttributeCategory" CodeBehind="ManageAttributeCategory.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="GroupSelector" Src="~/UserControls/General/GroupSelector.ascx" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">

        function upAttributeCategories_OnCallbackComplete(sender, eventArgs) {
            $(".attribute-categories").gridActions("displayMessage");
        }

        function upAttributeCategories_OnRenderComplete(sender, eventArgs) {
            $(".attribute-categories").gridActions("bindControls");
            $(".attribute-categories").iAppsSplitter();
        }

        function upAttributeCategories_OnLoad(sender, eventArgs) {
            $(".attribute-categories").gridActions({
                objList: $(".collection-table"),
                type: "list",
                pageSize: 10,
                onCallback: function (sender, eventArgs) {
                    upAttributeCategories_Callback(sender, eventArgs);
                }
            });
        }

        function upAttributeCategories_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems.first();

            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    OpeniAppsAdminPopup("SelectAttributePopup", "", "CloseSelectAttributes");
                    break;
                case "ManageAttributes":
                    doCallback = false;
                    window.location = "ManageAttributes.aspx";
                    break;
                case "EditProperties":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageAttributeCategoryDetails",
                        stringformat("AttributeId={0}&CategoryId={1}", selectedItemId, gridActionsJson.TabIndex));
                    break;
                case "Delete":
                    gridActionsJson.CustomAttributes["CategoryItemId"] = eventArgs.selectedItem.getValue("CategoryItemId");
                    doCallback = window.confirm("Are you sure you want to delete this attribute?");
                    break;
            }

            if (doCallback) {
                upAttributeCategories.set_callbackParameter(JSON.stringify(gridActionsJson));
                upAttributeCategories.callback();
            }
        }

        function CloseSelectAttributes() {
            gridActionsJson.CustomAttributes["ListItemsJson"] = popupActionsJson.CustomAttributes["ListItemsJson"];

            $(".attribute-categories").gridActions("callback", "AddListItem");
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="page-header-section clear-fix">
        <h1><%= GUIStrings.AttributeCategories %></h1>
        <a href="ManageAttributes.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, ManageAttributes %>" /></a>
    </div>
    <div class="page-content-section">
        <div class="tab-grid-section">
            <iAppsControls:CallbackPanel ID="upAttributeCategories" runat="server" OnClientCallbackComplete="upAttributeCategories_OnCallbackComplete"
                OnClientRenderComplete="upAttributeCategories_OnRenderComplete" OnClientLoad="upAttributeCategories_OnLoad">
                <ContentTemplate>
                    <iAppsControls:GridActions ID="gridActions" runat="server" />
                    <asp:ListView ID="lvManageAttributes" runat="server" ItemPlaceholderID="phManageAttributes">
                        <EmptyDataTemplate>
                            <div class="empty-list">
                                <%= GUIStrings.NoItemsFound%>
                            </div>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div class="collection-table fat-grid">
                                <asp:PlaceHolder ID="phManageAttributes" runat="server" />
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id")%>'>
                                    <div class="first-cell">
                                        <%# Eval("RowNumber")%>
                                    </div>
                                    <div class="middle-cell">
                                        <h4>
                                            <asp:Literal ID="ltTitle" runat="server" /></h4>
                                        <p>
                                            <asp:Literal ID="ltDescription" runat="server" />
                                        </p>
                                    </div>
                                    <div class="last-cell">
                                        <div class="child-row">
                                            <asp:Literal ID="ltType" runat="server" />
                                        </div>
                                        <div class="child-row">
                                            <asp:Literal ID="ltCreated" runat="server" />
                                        </div>
                                    </div>
                                    <input type="hidden" runat="server" id="hdnCategoryItemId" datafield="CategoryItemId" />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </ContentTemplate>
            </iAppsControls:CallbackPanel>
        </div>
    </div>
</asp:Content>
