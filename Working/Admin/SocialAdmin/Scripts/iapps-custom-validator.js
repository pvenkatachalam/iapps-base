﻿(function ($) {
    $.validator.addMethod("oneoftworequired", function (value, element, params) {
        var otherValue = '';

       
        if(typeof params != 'undefined' && params != null && params != '')
            otherValue = $(params).val();

       
        if ((value == '' || value == null) && (otherValue == null || otherValue == '')) {
            return false;
        }
        else {
            return true;
        }
    });

    $.validator.addMethod("channelrequired", function (value, element, params) {
        var ch = $("[id^=SelectedChannel][type=checkbox]")
        if (value != '' && (ch[0].checked == false && ch[1].checked == false && ch[2].checked == false))
            return false;
        else
            return true;
    });

    $.validator.addMethod("checkdaterange", function (value, element, params) {
        if (value == null || value == '')
            return true;

        var selectedDateTime = new Date(value);
        var selectedDateTimeinUTC = selectedDateTime.toUTCString();
        var currentDate = new Date();
        var currentDateinUTC = currentDate.toUTCString();
        if (selectedDateTimeinUTC <= currentDateinUTC)
            return false;
        else
            return true;
    });

    $.validator.addMethod('requiredif', function (value, element, parameters) {
        var id = '#' + parameters['dependentproperty'];

        // get the target value (as a string, 
        // as that's what actual value will be)
        var targetvalue = parameters['targetvalue'];
        targetvalue = (targetvalue == null ? '' : targetvalue).toString();

        // get the actual value of the target control
        // note - this probably needs to cater for more 
        // control types, e.g. radios
        var control = $(id);
        var controltype = control.attr('type');
        var actualvalue =
            controltype === 'checkbox' ?
            control.attr('checked').toString() :
            control.val();

        // if the condition is true, reuse the existing 
        // required field validator functionality
        if ($.trim(targetvalue) === $.trim(actualvalue) || ($.trim(targetvalue) === '*' && $.trim(actualvalue) !== ''))
            return $.validator.methods.required.call(
              this, value, element, parameters);

        return true;
    });    

    $.validator.unobtrusive.adapters.add("oneoftworequired", ["other"], function (options) {
       
        
        var prefix = getModelPrefix(options.element.name),
            other = options.params.other,
            fullOtherName = appendModelPrefix(other, prefix),
            element = $(options.form).find(":input[name=" + fullOtherName + "]")[0];
        if(element != undefined)
            options.rules["oneoftworequired"] = element;  // element becomes "params" in callback
        else
            options.rules["oneoftworequired"] = '';  // element becomes "params" in callback

        if (options.message) {
            options.messages["oneoftworequired"] = options.message;
        }
    });

    $.validator.unobtrusive.adapters.add("channelrequired", ["other"], function (options) {
        
        var prefix = getModelPrefix(options.element.name),
            other = options.params.other,
            fullOtherName = appendModelPrefix(other, prefix)

        options.rules["channelrequired"] = fullOtherName;  // element becomes "params" in callback
        if (options.message) {
            options.messages["channelrequired"] = options.message;
        }
    });
    
    $.validator.unobtrusive.adapters.add("checkdaterange", '', function (options) {
        options.rules["checkdaterange"] = true;
        if (options.message) {
            options.messages["checkdaterange"] = options.message;
        }
    });
    
    $.validator.unobtrusive.adapters.add('requiredif', ['dependentproperty', 'targetvalue'], function (options) {
            options.rules['requiredif'] = {
                dependentproperty: options.params['dependentproperty'],
                targetvalue: options.params['targetvalue']
            };
            options.messages['requiredif'] = options.message;
        });

    function getModelPrefix(fieldName) {
        return fieldName.substr(0, fieldName.lastIndexOf(".") + 1);
    }

    function appendModelPrefix(value, prefix) {
        if (value.indexOf("*.") === 0) {
            value = value.replace("*.", prefix);
        }
        return value;
    }

} (jQuery));