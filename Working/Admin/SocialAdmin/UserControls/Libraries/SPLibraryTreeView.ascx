﻿<%@ Control Language="C#" AutoEventWireup="True" Codebehind="SPLibraryTreeView.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.SPLibraryTreeView" %>

<script type="text/javascript">
    function spLibraryTreeView_onNodeBeforeSelect(sender, eventArgs) {
        var hfSPPSiteId = document.getElementById('<%=hfSPPSiteId.ClientID %>');
        var hfSPTreeTemplateType = document.getElementById('<%=hfSPTreeTemplateType.ClientID %>');
        if (typeof (LoadSPGrid) == "function")
            LoadSPGrid(hfSPPSiteId.value, eventArgs.get_node().get_value(), hfSPTreeTemplateType.value);
    }
</script>
<asp:HiddenField ID="hfSPPSiteId" runat="server" />
<asp:HiddenField ID="hfSPTreeTemplateType" runat="server" />
<div class="tree-container">
    <div class="tree-header clear-fix">
        <h2>
            <asp:Literal runat="server" ID="LrlTitle" Text="<%$ Resources:GUIStrings, SharepointLibrary %>"></asp:Literal>
        </h2>
    </div>
    <ComponentArt:TreeView SkinID="Default" EnableViewState="true" ID="spLibraryTreeView"
        runat="server" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
        <ClientEvents>
            <NodeSelect EventHandler="spLibraryTreeView_onNodeBeforeSelect" />
        </ClientEvents>
    </ComponentArt:TreeView>
</div>
