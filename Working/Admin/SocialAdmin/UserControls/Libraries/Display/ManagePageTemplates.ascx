<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManagePageTemplates.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManagePageTemplates" %>
<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<script type="text/javascript">
    function grdPageTemplate_OnLoad(sender, eventArgs) {
        $("#page_templates").gridActions({ objList: grdPageTemplate });
    }

    function LoadLibraryGrid() {
        $("#page_templates").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function UpdateHideActions() {
        var layoutFolderId = "<%= LayoutFolderId %>";
        if (treeActionsJson.FolderId.toLowerCase() == layoutFolderId.toLowerCase()) {
            gridActionsJson.HideActions = true;
            treeActionsJson.HideActions = true;
        }
    }

    function grdPageTemplate_OnRenderComplete(sender, eventArgs) {
        $("#page_templates").gridActions("bindControls");
        $("#page_templates").iAppsSplitter();
    }

    function grdPageTemplate_OnCallbackComplete(sender, eventArgs) {
        $("#page_templates").gridActions("displayMessage");
    }

    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;
        
        var templateType = 0;
        if (GetCurrentProductName() == "marketier")
            templateType = 1;
        switch (gridActionsJson.Action) {
            case "ViewPages":
                doCallback = false;
                OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=3&Id={0}&ProductId={1}&Name={2}", selectedItemId, jProductId, selectedItem.getMember('Title').get_value()));
                break;
            case "Archive":
                doCallback = false;
                var qString = stringformat("Mode=Warning&ObjectTypeId=3&Id={0}&Name={1}&ButtonText={2}", selectedItemId, selectedItem.getMember('Title').get_value(), "<%= GUIStrings.ArchiveTemplate %>");
                OpeniAppsAdminPopup("ViewPagesUsingObject", qString, "CloseArchiveTemplate");
                break;
            case "EditProperties":
                doCallback = false;
                OpeniAppsAdminPopup("ManageTemplateDetails", String.format("NodeId={0}&TemplateType={1}&TemplateId={2}", gridActionsJson.FolderId, templateType, selectedItemId), "RefreshTemplateLibrary");
                break;
            case "AddTemplate":
                doCallback = false;
                OpeniAppsAdminPopup("ManageTemplateDetails", String.format("NodeId={0}&TemplateType={1}", gridActionsJson.FolderId, templateType), "RefreshTemplateLibrary");
                break;
            case "Download":
                doCallback = false;
                $("#hdnTemplateId").val(selectedItemId);
                break;
        }

        if (doCallback) {
            grdPageTemplate.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdPageTemplate.callback();
        }
    }

    function RefreshTemplateLibrary() {
        $("#page_templates").gridActions("callback", { refreshTree: true });
    }

    function CloseArchiveTemplate() {
        $("#page_templates").gridActions("callback", "ArchiveTemplate");
    }

    function Grid_ItemSelect(sender, eventArgs) {
        sender.getItemByCommand("EditProperties").showButton();
        sender.getItemByCommand("Archive").show();
        sender.getItemByCommand("ViewPages").show();

        if (eventArgs.selectedItem.getMember("IsEditable").get_value() == false) {
            sender.getItemByCommand("EditProperties").hideButton();
            sender.getItemByCommand("Archive").hide();
        }
        else {
            if (eventArgs.selectedItem.getMember("Status").get_value() == 1) {
                sender.getItemByCommand("Archive").setText("<%= JSMessages.Archive %>");
                sender.getItemByCommand("Archive").setComandArgument("Archive");
            }
            else {
                sender.getItemByCommand("Archive").setText("<%= JSMessages.MakeActive %>");
                sender.getItemByCommand("Archive").setComandArgument("MakeActive");
            }
        }

        if (eventArgs.selectedItem.getMember("Type").get_value() == "2" || eventArgs.selectedItem.getMember("PagePart").get_value())
            sender.getItemByCommand("ViewPages").hide();

        if (GetCurrentProductName() == "marketier")
            sender.getItemByCommand("ViewPages").setText("<%= Bridgeline.iAPPS.Resources.iAppsActions.ViewEmailsusingthisTemplate %>");
        else
            sender.getItemByCommand("ViewPages").setText("<%= Bridgeline.iAPPS.Resources.iAppsActions.ViewPagesusingthisTemplate %>");
    }
</script>
<div id="page_templates">
    <div class="left-control">
        <UC:LibraryTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <asp:HiddenField ID="hdnTemplateId" runat="server" ClientIDMode="Static" />
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdPageTemplate" SkinID="Default" runat="server" Width="100%"
            RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdPageTemplateLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdPageTemplate_OnLoad" />
                <CallbackComplete EventHandler="grdPageTemplate_OnCallbackComplete" />
                <RenderComplete EventHandler="grdPageTemplate_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Status').Value==7"
                            RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('IsEditable').Value==false"
                            RowCssClass="disabled-row" SelectedRowCssClass="disabled-row" HoverRowCssClass="disabled-row" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="FileName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="ImageURL" Visible="false" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                        <ComponentArt:GridColumn DataField="StatusName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="CodeFile" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="PagePart" Visible="false" />
                        <ComponentArt:GridColumn DataField="Type" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="LayoutTemplateId" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsEditable" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <%--<div class="first-cell">                               
                          
                            </div>--%>
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["Title"].ToString() != Container.DataItem["Description"].ToString() ? Container.DataItem["Description"] : ""%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}{2}", GUIStrings.FileName, Container.DataItem["FileName"],
                                        Container.DataItem["CodeFile"].ToString().Trim() != string.Empty ? String.Format(" | {0}", Container.DataItem["CodeFile"]) : String.Empty)%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}",Container.DataItem["ModifiedDate"]),
                                        GUIStrings.By, Container.DataItem["ModifiedByFullName"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1} | <strong>{2}</strong>: {3}", GUIStrings.Status, Container.DataItem["StatusName"], 
                                        (Container.DataItem["Type"].ToString() == "2" ? GUIStrings.LayoutTemplate : GUIStrings.PagePart),
                                        (Container.DataItem["Type"].ToString() == "2" ? "True" : Container.DataItem["PagePart"]))%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdPageTemplateLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdPageTemplate) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</div>
