﻿/// <reference path="../jquery/jquery-1.10.2.min.js" />
/// <reference path="../knockout/knockout-3.1.0.js" />

var attributeViewModel;
function BindAttributeValues() {
    attributeViewModel = new AttributeViewModel(viewModelJson.Attributes);

    ko.applyBindings(attributeViewModel, attribute_values);

    $(".attribute-values img").each(function () {
        $(this).attr("src", ResolveClientUrl($(this).attr("src")));
    });

    $(".attribute-values .date-selector .picker").each(function () {
        $(this).datepicker({
            minDate: $.toDate($(this).attr("data-minValue")),
            maxDate: $.toDate($(this).attr("data-maxValue"))
        });

        $(this).siblings(".open-button").datepicker({
            minDate: $.toDate($(this).attr("data-minValue")),
            maxDate: $.toDate($(this).attr("data-maxValue"))
        });
    });
}

function SaveAttributeValues(validator) {
    if (typeof validator == "undefined")
        validator = "";

    if (typeof Page_ClientValidate === "function") {
        if (Page_ClientValidate(validator)) {
            return attributeViewModel.save();
        }
    } else {
        return attributeViewModel.save();
    }
}

function AttributeViewModel(items) {
    var self = this;

    self.Attributes = ko.observableArray([]);
    $.each(items, function (index, item) {
        self.Attributes.push(new Attribute(item));
    });

    self.validate = function () {
        var result = {};
        result.messages = [];
        $.each(self.Attributes(), function (index, vModel) {
            if (!vModel.IsValid())
                result.messages.push(vModel.ErrorMessage);
        });

        result.valid = result.messages.length == 0;

        return result;
    }

    self.save = function () {
        var result = self.validate();

        if (result.valid) {
            var saveJson = ko.toJSON(self);
            $("#hdnSaveAttribute").val(saveJson);
        }
        else {
            alert(result.messages.join("\n"));

            return false;
        }

        return true;
    }
}

function Attribute(data) {
    var self = this;

    self.Id = data.Id;
    self.Title = data.Title;
    self.IsEnum = data.IsEnum;
    self.IsMultiValued = data.IsMultiValued;
    self.Category = data.Category;
    self.ObjectId = data.ObjectId;
    self.ControlType = ko.observable(data.ControlType);

    self.Options = ko.observableArray(data.Options);

    self.PlaceHolder = data.PlaceHolder;

    self.ErrorMessage = data.Validation.ErrorMessage;
    self.MinValue = data.Validation.MinValue;
    self.MaxValue = data.Validation.MaxValue;

    if (data.ControlType == "checkbox") {
        if (data.SelectedValue == "true")
            self.SelectedValue = ko.observable(true);
        else
            self.SelectedValue = ko.observable(false);
    }
    else {
        self.SelectedValue = ko.observable(data.SelectedValue);
    }

    self.SelectedText = ko.observable(data.SelectedText);
    self.SelectedValues = ko.observableArray(data.SelectedValues);

    self.OpenHtmlEditor = function (item, event) {
        context = this;

        popupActionsJson.CustomAttributes["HtmlContent"] = HtmlDecode(context.SelectedValue());
        OpeniAppsAdminPopup("HtmlEditor", "", "CloseHtmlEditor");
    };

    self.OpenDateSelector = function (item, event) {
        $(event.target).siblings(".picker").datepicker("show");
    };

    self.OpenFileUploader = function (item, event) {
        context = this;

        OpeniAppsAdminPopup("FileUploader", stringformat("Type={0}&ObjectId={1}&Id={2}", context.Category, context.ObjectId, context.Id),
            "CloseFileUploader");
    };

    self.OpenPageLibrary = function (item, event) {
        context = this;

        OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPageLibrary");
    };

    self.OpenContentLibrary = function (item, event) {
        context = this;

        OpeniAppsAdminPopup("SelectContentLibraryPopup", "", "SelectContentLibrary");
    };

    self.OpenFileLibrary = function (item, event) {
        context = this;

        OpeniAppsAdminPopup("SelectFileLibraryPopup", "", "SelectFileLibrary");
    };

    self.OpenImageLibrary = function (item, event) {
        context = this;

        OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectImageLibrary");
    };

    self.RemoveSelectedValue = function (item, event) {
        context = this;

        context.SelectedValue("");
        context.SelectedText("");
    }

    self.IsValid = ko.computed(function () {
        var valid = true;
        var selectedValue = self.SelectedValue();
        if (selectedValue == undefined || selectedValue == EmptyGuid)
            selectedValue = "";

        if (data.Validation.IsRequired)
            valid = selectedValue != "" ? true : false;

        if (valid && selectedValue != "") {
            if (data.Validation.RegEx) {
                valid = new RegExp(data.Validation.RegEx).test(selectedValue);
            }

            var checkRange = false;
            var numericValue = Number(selectedValue);
            if (data.Validation.DataType == "integer") {
                checkRange = valid = Math.floor(numericValue) == numericValue;
            }

            if (data.Validation.DataType == "decimal") {
                checkRange = valid = $.isNumeric(selectedValue);
            }

            if (checkRange) {
                valid = numericValue >= data.Validation.MinValue && numericValue <= data.Validation.MaxValue;
            }
        }

        return valid;
    });

    self.DisplayTitle = ko.computed(function () {
        return stringformat("{0}{1}", self.Title, self.IsValid() ? "" : "<span class='req'>&nbsp;*</span>");
    });

    self.ValidationCss = ko.computed(function () {
        return self.IsValid() ? "" : "validation-error";
    });
}

function SelectPageLibrary() {
    if (context != null) {
        context.SelectedValue(popupActionsJson.SelectedItems.first());
        context.SelectedText(popupActionsJson.CustomAttributes["SelectedPageUrl"]);
    }
}

function SelectContentLibrary() {
    if (context != null) {
        context.SelectedValue(popupActionsJson.SelectedItems.first());
        context.SelectedText(popupActionsJson.CustomAttributes["ContentTitle"]);
    }
}

function SelectFileLibrary() {
    if (context != null) {
        var fileUrl = popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"];

        context.SelectedValue(popupActionsJson.SelectedItems.first());
        context.SelectedText(fileUrl);
    }
}

function SelectImageLibrary() {
    if (context != null) {
        context.SelectedValue(popupActionsJson.SelectedItems.first());
        context.SelectedText(popupActionsJson.CustomAttributes.ImageUrl);
    }
}

function CloseHtmlEditor() {
    var htmlText = "";
    if (typeof popupActionsJson.CustomAttributes["HtmlContent"] != "undefined")
        htmlText = popupActionsJson.CustomAttributes["HtmlContent"];

    if (context != null) {
        context.SelectedValue(HtmlEncode(htmlText));
        context.SelectedText(htmlText.substring(0, 10) + "...");
    }
}

function CloseFileUploader() {
    var uploadResultJson = JSON.parse(popupActionsJson.CustomAttributes["UploadResultJson"]);

    if (context != null) {
        context.SelectedValue(uploadResultJson[0].Url);
        context.SelectedText(uploadResultJson[0].Url);
    }
}

function HtmlEncode(value) {
    return $('<div/>').text(value).html();
}

function HtmlDecode(value) {
    return $('<div/>').html(value).text();
}

//jqAuto -- main binding (should contain additional options to pass to autocomplete)
//jqAutoSource -- the array of choices
//jqAutoValue -- where to write the selected value
//jqAutoSourceLabel -- the property that should be displayed in the possible choices
//jqAutoSourceInputValue -- the property that should be displayed in the input box
//jqAutoSourceValue -- the property to use for the value
ko.bindingHandlers.jqAuto = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        //handle disposal (if KO removes by the template binding)
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).autocomplete("destroy");
        });

        var options = valueAccessor() || {},
            allBindings = allBindingsAccessor(),
            unwrap = ko.utils.unwrapObservable,
            modelValue = allBindings.jqAutoValue,
            source = allBindings.jqAutoSource,
            valueProp = allBindings.jqAutoSourceValue,
            inputValueProp = allBindings.jqAutoSourceInputValue || valueProp,
            labelProp = allBindings.jqAutoSourceLabel || valueProp;

        //function that is shared by both select and change event handlers
        function writeValueToModel(valueToWrite) {
            if (ko.isWriteableObservable(modelValue)) {
                modelValue(valueToWrite);
            } else {  //write to non-observable
                if (allBindings['_ko_property_writers'] && allBindings['_ko_property_writers']['jqAutoValue'])
                    allBindings['_ko_property_writers']['jqAutoValue'](valueToWrite);
            }
        }

        //on a selection write the proper value to the model
        options.select = function (event, ui) {
            writeValueToModel(ui.item ? ui.item.actualValue : null);
        };

        //on a change, make sure that it is a valid value or clear out the model value
        options.change = function (event, ui) {
            var currentValue = $(element).val();
            var matchingItem = ko.utils.arrayFirst(unwrap(source), function (item) {
                return unwrap(item[inputValueProp]) === currentValue;
            });

            if (!matchingItem) {
                writeValueToModel(null);
            }
        }

        //handle the choices being updated in a DO, to decouple value updates from source (options) updates
        var mappedSource = ko.dependentObservable(function () {
            mapped = ko.utils.arrayMap(unwrap(source), function (item) {
                var result = {};
                result.label = labelProp ? unwrap(item[labelProp]) : unwrap(item).toString();  //show in pop-up choices
                result.value = inputValueProp ? unwrap(item[inputValueProp]) : unwrap(item).toString();  //show in input box
                result.actualValue = valueProp ? unwrap(item[valueProp]) : item;  //store in model
                return result;
            });
            return mapped;
        });

        //whenever the items that make up the source are updated, make sure that autocomplete knows it
        mappedSource.subscribe(function (newValue) {
            $(element).autocomplete("option", "source", newValue);
        });

        options.source = mappedSource();
        options.minLength = 0;

        //initialize autocomplete
        $(element).autocomplete(options);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        //update value based on a model change
        var allBindings = allBindingsAccessor(),
            unwrap = ko.utils.unwrapObservable,
            modelValue = unwrap(allBindings.jqAutoValue) || '',
            valueProp = allBindings.jqAutoSourceValue,
            inputValueProp = allBindings.jqAutoSourceInputValue || valueProp;

        //if we are writing a different property to the input than we are writing to the model, then locate the object
        if (valueProp && inputValueProp !== valueProp) {
            var source = unwrap(allBindings.jqAutoSource) || [];
            var modelValue = ko.utils.arrayFirst(source, function (item) {
                return unwrap(item[valueProp]) === modelValue;
            }) || {};  //probably don't need the || {}, but just protect against a bad value
        }

        //update the element with the value that should be shown in the input
        $(element).val(modelValue && inputValueProp !== valueProp ? unwrap(modelValue[inputValueProp]) : modelValue.toString());
    },
};

ko.bindingHandlers.jqAutoCombo = {
    init: function (element, valueAccessor) {
        var autoEl = $("#" + valueAccessor());
        $(element).click(function () {
            // close if already visible
            if (autoEl.autocomplete("widget").is(":visible")) {
                console.log("close");
                autoEl.autocomplete("close");
                return;
            }
            //autoEl.blur();
            console.log("search");
            autoEl.autocomplete("search", " ");
            autoEl.focus();
        });
    }
}

ko.observableArray.fn.sortByProperty = function (prop) {
    this.sort(function (obj1, obj2) {
        if (obj1[prop] == obj2[prop])
            return 0;
        else if (obj1[prop] < obj2[prop])
            return -1;
        else
            return 1;
    });
}

//Fix for IE11 stringify problem
if (typeof JSON2 !== 'object') {
    JSON2 = {};
}

(function () {
    'use strict';

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON2 !== 'function') {

        Date.prototype.toJSON2 = function () {

            return isFinite(this.valueOf())
                ? this.getUTCFullYear() + '-' +
                    f(this.getUTCMonth() + 1) + '-' +
                    f(this.getUTCDate()) + 'T' +
                    f(this.getUTCHours()) + ':' +
                    f(this.getUTCMinutes()) + ':' +
                    f(this.getUTCSeconds()) + 'Z'
                : null;
        };

        String.prototype.toJSON2 =
            Number.prototype.toJSON2 =
            Boolean.prototype.toJSON2 = function () {
                return this.valueOf();
            };
    }

    var cx,
        escapable,
        gap,
        indent,
        meta,
        rep;


    function quote(string) {

        // If the string contains no control characters, no quote characters, and no
        // backslash characters, then we can safely slap some quotes around it.
        // Otherwise we must also replace the offending characters with safe escape
        // sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
            var c = meta[a];
            return typeof c === 'string'
                ? c
                : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
        }) + '"' : '"' + string + '"';
    }


    function str(key, holder) {

        // Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

        // If the value has a toJSON2 method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON2 === 'function') {
            value = value.toJSON2(key);
        }

        // If we were called with a replacer function, then call the replacer to
        // obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

        // What happens next depends on the value's type.

        switch (typeof value) {
            case 'string':
                return quote(value);

            case 'number':

                // JSON2 numbers must be finite. Encode non-finite numbers as null.

                return isFinite(value) ? String(value) : 'null';

            case 'boolean':
            case 'null':

                // If the value is a boolean or null, convert it to a string. Note:
                // typeof null does not produce 'null'. The case is included here in
                // the remote chance that this gets fixed someday.

                return String(value);

                // If the type is 'object', we might be dealing with an object or an array or
                // null.

            case 'object':

                // Due to a specification blunder in ECMAScript, typeof null is 'object',
                // so watch out for that case.

                if (!value) {
                    return 'null';
                }

                // Make an array to hold the partial results of stringifying this object value.

                gap += indent;
                partial = [];

                // Is the value an array?

                if (Object.prototype.toString.apply(value) === '[object Array]') {

                    // The value is an array. Stringify every element. Use null as a placeholder
                    // for non-JSON2 values.

                    length = value.length;
                    for (i = 0; i < length; i += 1) {
                        partial[i] = str(i, value) || 'null';
                    }

                    // Join all of the elements together, separated with commas, and wrap them in
                    // brackets.

                    v = partial.length === 0
                        ? '[]'
                        : gap
                        ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']'
                        : '[' + partial.join(',') + ']';
                    gap = mind;
                    return v;
                }

                // If the replacer is an array, use it to select the members to be stringified.

                if (rep && typeof rep === 'object') {
                    length = rep.length;
                    for (i = 0; i < length; i += 1) {
                        if (typeof rep[i] === 'string') {
                            k = rep[i];
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                } else {

                    // Otherwise, iterate through all of the keys in the object.

                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = str(k, value);
                            if (v) {
                                partial.push(quote(k) + (gap ? ': ' : ':') + v);
                            }
                        }
                    }
                }

                // Join all of the member texts together, separated with commas,
                // and wrap them in braces.

                v = partial.length === 0
                    ? '{}'
                    : gap
                    ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}'
                    : '{' + partial.join(',') + '}';
                gap = mind;
                return v;
        }
    }

    // If the JSON2 object does not yet have a stringify method, give it one.

    if (typeof JSON2.stringify !== 'function') {
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"': '\\"',
            '\\': '\\\\'
        };
        JSON2.stringify = function (value, replacer, space) {

            // The stringify method takes a value and an optional replacer, and an optional
            // space parameter, and returns a JSON2 text. The replacer can be a function
            // that can replace values, or an array of strings that will select the keys.
            // A default replacer method can be provided. Use of the space parameter can
            // produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

            // If the space parameter is a number, make an indent string containing that
            // many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

                // If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

            // If there is a replacer, it must be a function or an array.
            // Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                    typeof replacer.length !== 'number')) {
                throw new Error('JSON2.stringify');
            }

            // Make a fake root object containing our value under the key of ''.
            // Return the result of stringifying the value.

            return str('', { '': value });
        };
    }


    // If the JSON2 object does not yet have a parse method, give it one.

    if (typeof JSON2.parse !== 'function') {
        cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
        JSON2.parse = function (text, reviver) {

            // The parse method takes a text and an optional reviver function, and returns
            // a JavaScript value if the text is a valid JSON2 text.

            var j;

            function walk(holder, key) {

                // The walk method is used to recursively walk the resulting structure so
                // that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.prototype.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


            // Parsing happens in four stages. In the first stage, we replace certain
            // Unicode characters with escape sequences. JavaScript handles many characters
            // incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

            // In the second stage, we run the text against regular expressions that look
            // for non-JSON2 patterns. We are especially concerned with '()' and 'new'
            // because they can cause invocation, and '=' because it can cause mutation.
            // But just to be safe, we want to reject all unexpected forms.

            // We split the second stage into 4 regexp operations in order to work around
            // crippling inefficiencies in IE's and Safari's regexp engines. First we
            // replace the JSON2 backslash pairs with '@' (a non-JSON2 character). Second, we
            // replace all simple value tokens with ']' characters. Third, we delete all
            // open brackets that follow a colon or comma or that begin the text. Finally,
            // we look to see that the remaining characters are only whitespace or ']' or
            // ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
                    .test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
                        .replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
                        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

                // In the third stage we use the eval function to compile the text into a
                // JavaScript structure. The '{' operator is subject to a syntactic ambiguity
                // in JavaScript: it can begin a block or an object literal. We wrap the text
                // in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

                // In the optional fourth stage, we recursively walk the new structure, passing
                // each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function'
                    ? walk({ '': j }, '')
                    : j;
            }

            // If the text is not JSON2 parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON2.parse');
        };
    }
}());
