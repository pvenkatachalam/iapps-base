<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove"
xmlns:resx="iAppsExtUri" exclude-result-prefixes="resx">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:template match="/">
    <xsl:for-each select="/action">
      <xsl:for-each select="row">
        <div class="row clear-fix">
          <xsl:for-each select="column">
            <div class="columns {@css}">
              <xsl:if test="@id != ''">
                <xsl:attribute name="id">
                  <xsl:value-of select="@id"/>
                </xsl:attribute>
              </xsl:if>
              <xsl:for-each select="item">
                <xsl:choose>
                  <xsl:when test="@type = 'button'">
                    <div class="{@css} {@mode}">
                      <input type="button" class="{@css} {@mode} command-button" commandArgument="{@commandArgument}" commandName="{@commandName}">
                        <xsl:attribute name="Value">
                          <xsl:value-of select="resx:GetResourceText(@text)"/>
                        </xsl:attribute>
                      </input>
                    </div>
                  </xsl:when>
                  <xsl:when test="@type = 'imageButton'">
                    <input type="image" class="{@css} {@mode} command-button" commandArgument="{@commandArgument}" commandName="{@commandName}" >
                      <xsl:attribute name="src">
                        <xsl:value-of select="resx:ResolveUrl(concat('~/App_Themes/General/images/tree/', @icon))"/>
                      </xsl:attribute>
                      <xsl:attribute name="Title">
                        <xsl:value-of select="resx:GetResourceText(@text)"/>
                      </xsl:attribute>
                    </input>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:for-each>
            </div>
          </xsl:for-each>
        </div>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
