﻿function iAppsCallbackController(controller, method, paramKeysJson, paramValues) {
    var callbackJson = {};

    if (typeof paramKeysJson != "undefined" && paramKeysJson != "" && paramValues.length > 0) {
        var paramKeys = JSON.parse(paramKeysJson)[paramValues.length];
        if (paramKeys != "" && paramKeys != ",") {
            $.each(paramKeys.split(','), function (i, p) {
                callbackJson[p] = paramValues[i];
            });
        }
    }

    var returnVal = null;
    var siteUrl = GetCurrentDomainUrl();
    siteUrl = siteUrl.replace(jPublicSiteUrl, jPublicSiteRootUrl);
    var callbackUrl = stringformat("{0}/api/{1}/{2}", siteUrl, controller, method);
    $.ajax({
        type: "GET",
        url: callbackUrl,
        data: callbackJson,
        dataType: "json",
        async: false,
        cache: false,
        success: function (msg) {
            returnVal = msg
        },
        error: function (a, status, error) {
            alert(stringformat("Status : {0} \nError : {1} \nURL : {2}, {3}, {4}", status, error, callbackUrl, paramKeysJson, paramValues));
        }
    });

    return returnVal;
}

function iAppsValidator_Validate(sender, args) {
    var validationJson = eval(sender.id + "_ValidateJson");

    var valid = true;
    var message = "";
    if (validationJson.Required && args.Value == "") {
        valid = false;
        if (validationJson.FieldName)
            message = "Enter value for " + validationJson.FieldName;
    }

    if (valid && validationJson.Type && args.Value != "") {
        valid = AdminCallback.ValidateByType(validationJson.Type, args.Value);
        if (validationJson.FieldName)
            message = "Enter a valid value for " + validationJson.FieldName;
    }

    args.IsValid = valid;
    if (!valid) {
        if(message != '')
            sender.errormessage = message;
    }
}

function ExecuteWebApi(method, service, endpoint, params) {
    var url = String.format("{0}/api/{1}/{2}", 
        jPublicSiteUrl, service.replace("Service", ""), endpoint).toLowerCase();

    if (method.toLowerCase() == "get")
        url = String.format("{0}?{1}", url, $.param(params));

    var returnVal = null;
    $.ajax({
        type: method,
        url: url,
        data: params,
        dataType: "json",
        async: false,
        cache: false,
        success: function (msg) {
            returnVal = msg
        },
        error: function (a, status, error) {
            alert(stringformat("Status : {0} \nError : {1} \nURL : {2}, {3}, {4}", status, error, callbackUrl, paramKeysJson, paramValues));
        }
    });

    return returnVal;
}

function CreateBitlyUrl(fullUrl) {
    var loginKey = 'iappsteam';
    var apiKey = 'R_2bf72d87350b9f68c091c7a785858b70';
    var format = 'json';
    var bitlyUrl = fullUrl;
    fullUrl = encodeURIComponent(fullUrl);
    var callbackUrl = stringformat("http://api.bitly.com/v3/shorten?login={0}&apiKey={1}&longUrl={2}&format={3}",
        loginKey, apiKey, fullUrl, format);

    $.ajax({
        type: "GET",
        url: callbackUrl,
        dataType: "json",
        async: false,
        cache: false,
        success: function (response) {
            if (response.status_code == 200)
                bitlyUrl = response.data.url;
        }
    });

    return bitlyUrl;
}