﻿$(function () {
    $.ajaxSetup({ cache: false });

    $(document).on("focus", ".textBoxes", function () {
        if ($(this).val() == $(this).attr("title")) {
            $(this).removeClass("defaultText");
            $(this).val("");
        }
    });
    $(document).on("blur", ".textBoxes", function () {
        if ($(this).attr("title") && $(this).attr("title") != "" &&
                ($(this).val() == "" || $(this).val() == $(this).attr("title"))) {
            $(this).addClass("defaultText");
            $(this).val($(this).attr("title"));
        }
    });

    $(document).on("keypress", ".iapps-client-grid-search", function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $(this).find("input.button").click();
            return false;
        }
    });

    $("input.read-only").each(function () {
        $(this).attr("readonly", "readonly");
    });

    try {
        $("#iApps_LoadingPanel").iAppsDialog();
        $(document).on("click", ".show-loading", function () {
            ShowiAppsLoadingPanel();
        });
    }
    catch (e) { }

    setTimeout(function () {
        // To enable CA combobox filtering 
        $(".comboBox input.combo-editable").prop("disabled", false);
        $(".comboBox input.combo-noneditable").prop("disabled", true);
    }, 100);

    SetTextboxDefaultValues();
    SetVerticalScrollGrid();

    $(".validation-summary-valid").each(function () {
        $(this).prepend($("<div class='validation-summary-close' />"));
    });

    $(document).on("click", ".validation-summary-close", function () {
        $(this).parents('.validation-summary-errors').addClass("validation-summary-valid");
    });

    InitializeToolTip();
});

function InitializeToolTip() {
    setTimeout(function () {
        if (typeof iAPPS_Tooltip != "undefined")
            iAPPS_Tooltip.Initialize('rel', 'title', 'bottom');
    }, 100);
}

function CreateToolTip(tipText) {
    return stringformat("<img class='help-icon' rel='tooltip' src='../App_Themes/General/images/icon-tooltip.gif' title='{0}' />", tipText);
}

function SetTextboxDefaultValues() {
    $(".textBoxes").each(function () {
        if ($(this).attr("title") && $(this).attr("title") != "" &&
                ($(this).val() == "" || $(this).val() == $(this).attr("title"))) {
            $(this).blur();
        }
    });
}

function ValidateSearchInput() {
    var inputValue = $(".search-box input.header-search").val();
    if (typeof inputValue == "undefined")
        inputValue = "";

    if (inputValue == "") {
        alert(__JSMessages["PleaseEnterASearchTerm"]);
        return false;
    }

    return true;
}

function GetRelativeUrl(fullUrl) {
    var relativeUrl = fullUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
    if (relativeUrl.charAt(0) != "/" && relativeUrl.indexOf("://") == -1)
        relativeUrl = "/" + relativeUrl;

    if (relativeUrl.indexOf("://") == -1)
        relativeUrl = relativeUrl.replace(new RegExp("//", "gi"), "/");

    return relativeUrl;
}

function SetVerticalScrollGrid() {
    setTimeout(function () {
        $(".tabular-grid").each(function () {
            SetVerticalScrollStyles($(this).prop("id"));
        });
    }, 200);
}

function SetVerticalScrollStyles(gridId) {
    var grid = $("#" + gridId);
    var scrollDiv = $("#" + gridId + "_VerticalScrollDiv");
    if (scrollDiv.length > 0 && !scrollDiv.hasClass("vertical-scroll-section")) {
        scrollDiv.addClass("vertical-scroll-section").css({ "height": grid.css("height") });
        grid.addClass("vertical-scroll-grid");
        var dummyCell = grid.find("td").filter(function () {
            return $(this).css("font-size") == "0px" || $(this).css("width") == "0px";
        });
        dummyCell.addClass("iapps-heading-scroll-cell").css({ "border-left": 0, "padding": "1px" });
        dummyCell.prev().css({ "border-right": 0 });
    }
}

function JumpToTemplatePreview(pageUrl) {
    if (pageUrl.indexOf("?") > -1)
        fullUrl = pageUrl + "&IsTemplatePreview=true&PageState=Preview";
    else
        fullUrl = pageUrl + "?IsTemplatePreview=true&PageState=Preview";

    window.open(fullUrl);
}

function fn_ShowSearchBox(link) {
    var parentLi = $("#" + link.id).parent("li");
    var linkLi = parentLi.find(".search-box");

    if (parentLi.hasClass("selected")) {
        linkLi.slideUp("fast");
        parentLi.removeClass("selected");
    }
    else {
        linkLi.slideDown("fast");
        parentLi.addClass("selected");
    }

    $(document).click(function (e) {
        if (!$(e.target).closest(parentLi).length && !$(e.target).closest(linkLi).length) {
            linkLi.slideUp("fast");
            parentLi.removeClass("selected");
        }
    });
}

function ShowiAppsLoadingPanel() {
    try {
        if ($("#iApps_LoadingPanel").length > 0) {
            $("#iApps_LoadingPanel").iAppsDialog("open");
        }
        else {
            var loadingPanel = $("<div id='iApps_LoadingPanel' />").hide();
            loadingPanel.append($("<div />")
                .addClass("iapps-modal loadingSpinner")
                .append($(stringformat('<img src="{0}" alt="Please wait..."  />', jRotatingCube)))
                );
            $("body").append(loadingPanel);
            loadingPanel.iAppsDialog("open");
        }
    }
    catch (e) { }
}

function ShowiAppsNotification(message, success, autoHide) {
    try {
        if ($("#iapps-notification").length > 0) {
            if (typeof success == "undefined")
                success = true;
            if (typeof autoHide == "undefined")
                autoHide = false;

            $("#iapps-notification").iAppsNotification({ success: success, autoHide: autoHide });
            $("#iapps-notification").iAppsNotification("show", message, { focus: true });

            $("#iapps-notification").data("CreatedTime", Math.round(+new Date() / 1000));
        }
        else {
            alert(message);
        }
    } catch (e) {
        alert(message);
    }
}

function CloseiAppsNotification() {
    try {
        var createdTime = $("#iapps-notification").data("CreatedTime");
        var timeNow = Math.round(+new Date() / 1000);
        if (createdTime == undefined)
            createdTime = timeNow - 10;

        if ($("#iapps-notification").length > 0 && (timeNow - createdTime) > 5)
            $("#iapps-notification").iAppsNotification("close");
    } catch (e) {
        console.log(e.message);
    }
}


function OpeniAppsShortcut(value) {
    $("<div />").html(value).iAppsDialog("open", { alertBox: true });
}

function CloseiAppsLoadingPanel() {
    $("#iApps_LoadingPanel").iAppsDialog("close");
}

function DisableLinks(selector) {
    $(selector).on("click", function (e) {
        e.stopPropagation();
        return false;
    });
}

function SearchClientGrid(txtBoxId, objGrid) {
    var txtValue = $("#" + txtBoxId).val();
    if (txtValue == $("#" + txtBoxId).attr("title"))
        txtValue = "";
    /*if (txtValue == "" || txtValue == $("#" + txtBoxId).attr("title")) {
    alert(EnterValue2Search);
    $("#" + txtBoxId).focus();
    return false;
    }
    else if (!txtValue.match(/^[a-zA-Z0-9. _']+$/)) {
    alert(InValidSearchTerm);
    $("#" + txtBoxId).focus();
    return false;
    }
    else
    {*/
    objGrid.Search(txtValue, false);
    document.getElementById(objGrid.get_id() + "_dom").style.height = "auto";

    return true;
    //}
}


function OpenIAppsModal(url) {
    dhtmlPopupModal = dhtmlmodal.open('', 'iframe', url, '', '');
}

var EmptyGuid = '00000000-0000-0000-0000-000000000000';
function stringformat(str)            //created by Adams to format the string for localization needs
{
    if (arguments.length == 0)
        return null;

    var str = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
        var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
        str = str.replace(re, arguments[i]);
    }
    return (str);
}

// Function to trim
function Trim(s) {
    // Remove leading spaces and carriage returns
    while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n') || (s.substring(0, 1) == '\r')) {
        s = s.substring(1, s.length);
    }
    // Remove trailing spaces and carriage returns
    while ((s.substring(s.length - 1, s.length) == ' ') || (s.substring(s.length - 1, s.length) == '\n') || (s.substring(s.length - 1, s.length) == '\r')) {
        s = s.substring(0, s.length - 1);
    }
    return s;
}

function CheckLicense(productName, showMessage) {
    var hasLicenese = true;
    var hasPermission = true;
    switch (productName.toLowerCase()) {
        case 'analyzer':
            hasLicenese = hasAnalyticsLicense;
            hasPermission = hasAnalyticsPermission;
            break;
        case 'commerce':
            hasLicenese = hasCommerceLicense;
            hasPermission = hasCommercePermission;
            break;
        case 'marketier':
            hasLicenese = hasMarketierLicense;
            hasPermission = hasMarketierPermission;
            break;
        case 'social':
            hasLicenese = hasSocialLicense;
            hasPermission = hasSocialPermission;
            break;
        case "cms":
            hasLicenese = hasCMSLicense;
            hasPermission = hasCMSPermission;
            break;
    }

    if (!hasLicenese || !hasPermission) {
        if (showMessage)
            OpenLicenseWarningPopup(productName, hasLicenese, hasPermission);
        return false;
    }

    return true;
}

function GetCurrentDomainUrl() {
    var currentDomain = jPublicSiteUrl;

    if (typeof editorContext == "undefined" || editorContext != 'SiteEditor') {
        if (jProductId == jCMSProductId)
            currentDomain = jCMSAdminSiteUrl;
        else if (jProductId == jCommerceProductId)
            currentDomain = jCommerceAdminUrl;
        else if (jProductId == jMarketierProductId)
            currentDomain = jmarketierAdminURL;
        else if (jProductId == jSocialProductId)
            currentDomain = jSocialAdminURL;
        else if (jProductId == jAnalyticsProductId)
            currentDomain = jAnalyticAdminSiteUrl;
    }

    return currentDomain;
}

function GetCurrentProductName() {
    var productName = "";
    if (jProductId == jCMSProductId)
        productName = "cms";
    else if (jProductId == jCommerceProductId)
        productName = "commerce";
    else if (jProductId == jMarketierProductId)
        productName = "marketier";
    else if (jProductId == jSocialProductId)
        productName = "social";
    else if (jProductId == jAnalyticsProductId)
        productName = "analyzer";

    return productName;
}

function GetCurrentVDName() {
    var currentUrl = GetCurrentDomainUrl();

    return currentUrl.substring(currentUrl.lastIndexOf("/") + 1);
}

function RedirectWithToken(productName, mUrl, applicationId) {
    ShowiAppsLoadingPanel();

    window.location = GetAdminUrlWithAuthToken(productName, mUrl, applicationId, true);
}

function GetjProductId(productName) {
    var productId = jCMSProductId;
    productName = productName.toLowerCase();
    if (productName == "commerce") {
        productId = jCommerceProductId;
    }
    else if (productName == "marketier") {
        productId = jMarketierProductId;
    }
    else if (productName == "analyzer") {
        productId = jAnalyticsProductId;
    }
    else if (productName == "social") {
        productId = jSocialProductId;
    }

    return productId;
}

function GetAdminUrlWithAuthToken(productName, mUrl, applicationId, isRedirect) {
    var productId = GetjProductId(productName);
    if (typeof applicationId == "undefined" || applicationId.length != 36)
        applicationId = jAppId;

    if (typeof isRedirect == "undefined" || isRedirect == "")
        isRedirect = false;

    if (typeof jProductId == "undefined" || productId != jProductId ||
        typeof jAppId == "undefined" || applicationId != jAppId) {
        var token = FWCallback.GetAuthToken(juserName, jUserId, productId, applicationId, isRedirect);
        if (token != "") {
            if (mUrl.indexOf("?") > -1)
                mUrl += "&Token=" + token;
            else
                mUrl += "?Token=" + token;
        }
    }

    return mUrl;
}

function GetTreeNodeCount(dataItem) {
    var noOfPages = 0;
    if (typeof dataItem.GetProperty("NoOfPages") != "undefined")
        noOfPages = parseInt(dataItem.GetProperty("NoOfPages"));
    if (typeof dataItem.GetProperty("StorageIndex") != "undefined")
        noOfPages = parseInt(dataItem.GetProperty("StorageIndex"));

    if (noOfPages > 0)
        return stringformat('<span class="tree-node-count">({0})</span>', noOfPages);

    return "";
}

function ResolveClientUrl(relativeUrl) {
    if (typeof jCurrentVDName != "undefined") {
        if (relativeUrl.charAt(0) == "~")
            relativeUrl = jCurrentVDName + relativeUrl.slice(1);
    }

    return relativeUrl;
}

function IsHomeNode(node) {
    return typeof node.GetProperty("HomeNode") != "undefined" && node.GetProperty("HomeNode") != "";
}

function GetTreeNodeIcons(dataItem) {
    var icons = "";
    if (IsHomeNode(dataItem))
        icons += stringformat('<img src="{0}" alt="{1}" />',
            ResolveClientUrl("~/App_Themes/General/images/tree/icon-home.png"), dataItem.GetProperty("HomeNode"));
    if (typeof dataItem.GetProperty("SecuredNode") != "undefined" && dataItem.GetProperty("SecuredNode") != "")
        icons += stringformat('<img src="{0}" alt="{1}" />',
            ResolveClientUrl("~/App_Themes/General/images/tree/icon-lock.png"), dataItem.GetProperty("SecuredNode"));
    if (typeof dataItem.GetProperty("LinkedNode") != "undefined" && dataItem.GetProperty("LinkedNode") != "")
        icons += stringformat('<img src="{0}" alt="{1}" />',
            ResolveClientUrl("~/App_Themes/General/images/tree/icon-linked.png"), dataItem.GetProperty("LinkedNode"));

    if (icons != "")
        icons = stringformat('<span class="tree-node-icons">{0}</span>', icons);

    return icons;
}

function GetClientTemplateHtml(dataItem, dataField) {
    var fieldValue = dataItem.getMember(dataField).get_text();

    return stringformat('<span title="{0}">{1}</span>', fieldValue.indexOf("<") == -1 ? fieldValue : "", fieldValue);
}

function GetGridItemRowNumber(objGrid, dataItem) {
    return gridActionsJson.PageSize * (gridActionsJson.PageNumber - 1) + dataItem.Index + 1;
}

function GetGridLoadingPanelContent(objGrid) {
    var height = 50;
    var width = 100;
    if (objGrid != "") {
        var gridDom = document.getElementById(objGrid.get_id() + "_dom");
        if (gridDom) {
            if (gridDom.offsetHeight > 0)
                height = gridDom.offsetHeight;
            if (gridDom.offsetWidth > 0)
                width = gridDom.offsetWidth;
        }
        var scrollBox = $("#" + objGrid.get_id()).parent(".scroll-box");
        if (scrollBox.length > 0)
            height = scrollBox.height();
        else {
            scrollBox = $("#" + objGrid.get_id()).parent(".scroll-section");
            if (scrollBox.length > 0)
                height = scrollBox.height();
        }
    }
    var html = "<table style='width:100%;'><tr><td class='grid-loading' style='height:{0}px;width:{1}px;'><div class='loading-spinner'><img src='{2}' border='0' alt='Loading...' /></div></td></tr></table>";

    return stringformat(html, height, width, jSpinnerUrl);
}

function GetTreeLoadingPanelContent(objTree) {
    var height = 100;
    var width = 100;
    if (objTree != "") {
        var treeDom = document.getElementById(objTree.get_id());
        if (treeDom) {
            if (treeDom.offsetHeight > 0)
                height = treeDom.offsetHeight;
            if (treeDom.offsetWidth > 0)
                width = treeDom.offsetWidth;
        }
    }
    var html = "<table><tr><td class='tree-loading' style='height:{0}px;width:{1}px;'><div class='loading-spinner'><img src='{2}' border='0' alt='Loading...' /></div></td></tr></table>";

    return stringformat(html, height, width, jSpinnerUrl);
}

function IsMarketierDirectory(node) {
    return node != null && node.getProperty("IsMarketierDir").toLowerCase() == "true";
}

function GetGridDragTemplate(title) {
    return "<div class='drag-template'>" + title + "</div>";
}

function GetGridDragTemplateById(objGrid, dragItem) {
    var objectId = dragItem.Key;
    var gridId = objGrid.get_id();

    if (typeof objectId != "undefined") {
        var $row = $("#" + gridId + " .grid-item[objectId='" + objectId + "']");

        if (!$row.hasClass("disable-move"))
            return "<div class='drag-template'>" + $row.parent().html() + "</div>";
        else
            return "";
    }
    else
        return "<div class='drag-template'>" + dragItem.getMember("Title").get_value() + "</div>";
}

function FormatGridHtml(gridHtml) {
    if (gridHtml == null)
        return "";

    return gridHtml.replace(new RegExp("#%cLt#%", "g"), "<");
}

function SetTaxonomyForList() {
    $(".view-tags").iAppsClickMenu({
        width: "150px",
        heading: __JSMessages["Tags"],
        enableLoadingPanel: true,
        create: function (e, menu) {
            var taxonomyNames = JSON.parse(GetTaxonomyJson(e.attr("objectId")));
            if (taxonomyNames.length > 0) {
                $.each(taxonomyNames, function () {
                    menu.addListItem(this.toString());
                });
            }
            else {
                menu.addListItem(__JSMessages["NoTagsAttached"]);
            }
        }
    });
}

function GetQueryStringValue(key) {
    var re = new RegExp('(?:\\?|&)' + key + '=(.*?)(?=&|$)', 'gi');
    var r = [], m;
    while ((m = re.exec(document.location.search)) != null) r.push(m[1]);
    return r;
}

function OpenResetCachePopup() {
    OpeniAppsAdminPopup("ResetCachePopup", "", "CloseResetCachePopup");

    return false;
}

function CloseResetCachePopup() {
    window.location = window.location;
}

function OpenCreateEmailPopup(querystring) {
    OpeniAppsMarketierPopup("ChooseTemplate", querystring, "CloseCreateEmailPopup");

    return false;
}

function CloseCreateEmailPopup() {
    JumpToEmailFrontPage(popupActionsJson.CustomAttributes["PageUrl"]);
}

//script taken from http://weblogs.asp.net/cprieto/archive/2010/01/03/handling-timezone-information-in-asp-net.aspx 
function setCookie(cookieName, cookieValue, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = cookieName + "=" + escape(cookieValue) + ((expiredays == null) ? "" : ";expires=" + exdate.toUTCString());
}

function getCookie(cookieName) {
    if (document.cookie.length > 0) {
        cookieStart = document.cookie.indexOf(cookieName + "=");
        if (cookieStart != -1) {
            cookieStart = cookieStart + cookieName.length + 1;
            cookieEnd = document.cookie.indexOf(";", cookieStart);
            if (cookieEnd == -1)
                cookieEnd = document.cookie.length;
            return unescape(document.cookie.substring(cookieStart, cookieEnd));
        }
    }
    return "";
}

function getUtcOffset() {
    return (new Date()).getTimezoneOffset();
}

function checkCookie() {
    var timeOffset = getCookie("TimeZoneOffset");
    if (timeOffset == null || timeOffset == "") {
        setCookie("TimeZoneOffset", getUtcOffset(), 1);
        //window.location.reload(); 
    }
}

function GetCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

function SetCookie(name, value) {
    var argv = SetCookie.arguments;
    var argc = SetCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape(value) +
((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
((path == null) ? "" : ("; path=" + path)) +
((domain == null) ? "" : ("; domain=" + domain)) +
((secure == true) ? "; secure" : "");
}

function getCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

function DeleteCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1); // This cookie is history
    var cval = GetCookie(name);
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}

function validKey(e) {
    var key;
    var keychar;
    var reg;

    if (window.event) {
        // for IE, e.keyCode or window.event.keyCode can be used
        key = e.keyCode;
    }
    else if (e.which) {
        // netscape
        key = e.which;
    }
    else {
        // no event, so pass through
        return true;
    }
    if (key > 31 || key == 8 || key == 16) {
        return true;
    }
    else {
        return false;
    }
}

//moved these methods from RadWindow.js
function GetDialogArguments() {
    var clientParameters = getRadWindow().ClientParameters; //return the arguments supplied from the parent page
    return clientParameters;
}
//moved these methods from RadWindow.js
function getRadWindow() {
    if (window.radWindow) {
        return window.radWindow;
    }
    if (window.frameElement && window.frameElement.radWindow) {
        return window.frameElement.radWindow;
    }
    return null;
}

function ChangeSpecialCharacters(str) {
    var replacedString = str;
    if (replacedString != null) {
        replacedString = replacedString.replace(new RegExp("&", "g"), "&amp;");
        replacedString = replacedString.replace(new RegExp("<", "g"), "&lt;");
        replacedString = replacedString.replace(new RegExp(">", "g"), "&gt;");
        replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;");
        replacedString = replacedString.replace(new RegExp("'", "g"), "\\'");
        replacedString = replacedString.replace(new RegExp("\n", "g"), " ");
    }
    return replacedString;
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
};

function NewGuid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
}

String.format = function () {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {
        var reg = new RegExp("\\{" + i + "\\}", "gm");
        s = s.replace(reg, arguments[i + 1]);
    }

    return s;
}

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function (prefix) {
    return (this.substr(0, prefix.length) === prefix);
}

String.prototype.equals = function (text) {
    return this.toString().toLowerCase() == text.toString().toLowerCase();
}

var dtCh = "/";
var minYear = 1900;
var maxYear = 2100;

function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag) {
    var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary(year) {
    // February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
}
function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i == 4 || i == 6 || i == 9 || i == 11) { this[i] = 30 }
        if (i == 2) { this[i] = 29 }
    }
    return this
}

function isDate(dtStr) {
    var daysInMonth = DaysArray(12)
    var pos1 = dtStr.indexOf(dtCh)
    var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
    var strMonth = dtStr.substring(0, pos1)
    var strDay = dtStr.substring(pos1 + 1, pos2)
    var strYear = dtStr.substring(pos2 + 1)
    strYr = strYear
    if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
    if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
    }
    month = parseInt(strMonth)
    day = parseInt(strDay)
    year = parseInt(strYr)
    if (pos1 == -1 || pos2 == -1) {
        alert(__JSMessages["TheDateFormatShouldBemmddyyyy"])
        return false
    }
    if (strMonth.length < 1 || month < 1 || month > 12) {
        alert(__JSMessages["PleaseEnterAValidMonth"])
        return false
    }
    if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
        alert(__JSMessages["PleaseEnterAValidDay"])
        return false
    }
    if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
        alert(stringformat(__JSMessages["PleaseEnterAValid4DigitYearBetween0And1"], minYear, maxYear))
        return false
    }
    if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
        alert(__JSMessages["PleaseEnterAValidDate"])
        return false
    }
    return true
}