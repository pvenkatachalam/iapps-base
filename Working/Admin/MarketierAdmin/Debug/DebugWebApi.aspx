<%@ Page Language="C#" AutoEventWireup="true" ClientIDMode="Static" %>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {

    });

    function SubmitData() {
        var url = $("#txtUrl").val();
        if (url == "") {
            alert("Please enter the URL.");
        }
        else {
            $("#btnSubmit").hide();
            $("#result_section").html("Processing...");
            $("#result_section").show();

            if ($("#ddlMethod").val() == "POST") {
                var postData = {};
                if ($("#txtPostParms").val() != "")
                    postData = JSON.parse($("#txtPostParms").val());

                $.post(url, postData, function (result) {
                    $("#result_status").html("Success");
                    $("#result").html(JSON.stringify(result));
                }).fail(function (jqXHR, status, error) {
                    $("#result_status").html("Failed");
                    $("#result").html(jqXHR.responseText);
                }).always(function () {
                    $("#btnSubmit").show();
                });
            }
            else {
                url = url + "?" + $("#txtPostParms").val();
                $.get(url, function (result) {
                    $("#result_status").html("Success");
                    $("#result").html(JSON.stringify(result));
                }).fail(function (jqXHR, status, error) {
                    $("#result_status").html("Failed");
                    $("#result").html(jqXHR.responseText);
                }).always(function () {
                    $("#btnSubmit").show();
                });
            }
        }
    }
</script>
<fieldset style="margin-top: 20px; padding: 20px; border: solid 1px #d2d2d2; width: 1000px; word-break: break-all;">
    <legend>WebApi Debugger</legend>
    <table border="0">
        <tr>
            <td>URL</td>
            <td>
                <input type="text" id="txtUrl" style="width: 600px;" />
            </td>
        </tr>
        <tr>
            <td>Method</td>
            <td>
                <select id="ddlMethod" style="width: 100px;">
                    <option>POST</option>
                    <option>GET</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Params</td>
            <td>
                <textarea id="txtPostParms" style="width: 600px;"></textarea>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <input id="btnSubmit" type="button" title="Submit" value="Submit" onclick="SubmitData();" /></td>
        </tr>
    </table>
    <div id="result_section" style="display: none;">
        <hr style="border-top: solid 1px #d2d2d2;" />
        <h3>Results</h3>
        <h4 id="result_status"></h4>
        <div id="result"></div>
    </div>
</fieldset>

