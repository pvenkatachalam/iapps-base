﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="EmailCampaigns.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.EmailCampaigns" %>
<script type="text/javascript">
    function upEmailCampaigns_OnCallbackComplete(sender, eventArgs) {
        $("#campaign_list").gridActions("displayMessage");
    }

    function upEmailCampaigns_OnRenderComplete(sender, eventArgs) {
        $("#campaign_list").gridActions("bindControls");
        $("#campaign_list").iAppsSplitter();
    }


    function upEmailCampaigns_OnLoad(sender, eventArgs) {
        $("#campaign_list").gridActions({
            objList: $(".collection-table"),
            type: "list",
            fixHeader: false,
            onCallback: function (sender, eventArgs) {
                upEmailCampaigns_Callback(sender, eventArgs);
            }
        });
    }

    function upEmailCampaigns_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        var selectedTitle = "";


        if (eventArgs && eventArgs.selectedItem)
            selectedTitle = eventArgs.selectedItem.getValue("Title");

        switch (gridActionsJson.Action) {

            case "Duplicate":
                doCallback = true;
                break;
            case "Edit":
                doCallback = false;
                JumpToEmailFrontPage(MarketierCallback.GetCampaignPageUrl(selectedItemId));
                break;
            case "Delete":
                doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisEmail %>");
                break;
            case "CancelledSend":
                doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToCancelThisEmail %>");
                break;
        }

        if (doCallback) {
            upEmailCampaigns.set_callbackParameter(JSON.stringify(gridActionsJson));
            upEmailCampaigns.callback();
        }
    }

    function Grid_ItemSelect(sender, eventArgs) {

        sender.getItemByCommand("CancelledSend").hideButton();

        if (gridActionsJson.TabIndex == 0 || gridActionsJson.TabIndex == 2) {
            sender.getItemByCommand("CancelledSend").showButton();
            sender.getItemByCommand("Delete").hideButton();
        }
    }

    function OpenContactLists() {
        OpeniAppsMarketierPopup('ViewAttachedContactLists', "CampaignId=" + gridActionsJson.SelectedItems[0], '');
    }

    function OpenViewEmailResultsPopup() {
        var selectedItemId = gridActionsJson.SelectedItems[0];
        OpeniAppsMarketierPopup("EmailRunHistory", "Id=" + selectedItemId);
    }
</script>

<div id="campaign_list" class="clear-fix">
    <div class="tab-grid-section">
        <iAppsControls:CallbackPanel ID="upEmailCampaigns" runat="server" OnClientCallbackComplete="upEmailCampaigns_OnCallbackComplete"
            OnClientRenderComplete="upEmailCampaigns_OnRenderComplete" OnClientLoad="upEmailCampaigns_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActions" runat="server" />
                <div class="grid-section">
                    <asp:ListView ID="lvEmailCampaigns" runat="server" ItemPlaceholderID="phEmailList">
                        <EmptyDataTemplate>
                            <div class="empty-list">
                                <asp:Literal ID="ltEmptyList" runat="server"></asp:Literal>
                            </div>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div class="collection-table fat-grid email-listing-grid">
                                <asp:PlaceHolder ID="phEmailList" runat="server" />
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                <div class="collection-row clear-fix grid-item" objectid='<%# Eval("CampaignId") %>'>
                                    <div class="large-cell">
                                        <h4 datafield="Title">
                                            <asp:Literal ID="ltEmailName" runat="server" /></h4>
                                        <p>
                                            <asp:Literal ID="ltLastModified" runat="server" />
                                        </p>
                                        <p>
                                            <asp:HyperLink ID="hplEmailStatistics" runat="server" NavigateUrl="javascript:OpenViewEmailResultsPopup();" Text="<%$ Resources:GUIStrings, EmailStatistics %>" CssClass="email-statistics-link" />
                                        </p>
                                        <asp:Panel ID="pnCurrentlySending" runat="server" Visible="false" CssClass="currently-sending">
                                            <asp:Image ID="imgSending" runat="server" ImageUrl="~/App_Themes/General/images/icon-sending.gif" AlternateText="<%$ Resources:GUIStrings, CurrentlySending %>" />
                                            <asp:Label ID="locCurrentlySending" runat="server" Text="<%$ Resources:GUIStrings, CurrentlySending %>" />
                                        </asp:Panel>
										<asp:LinkButton ID="lbtnWorkflowStatus" runat="server" OnClick="lbtnWorkflowStatus_Click" />
                                    </div>
                                    <div class="small-cell">
                                        <div class="child-row">
                                            <asp:Label ID="lblSubjectLine" runat="server" CssClass="left-margin" />
                                        </div>
                                        <div class="child-row">
                                            <asp:Label ID="lblScheduledSend" runat="server" CssClass="left-margin" />
                                        </div>
                                        <div class="child-row">
                                            <asp:Label ID="lblTotalRecipients" runat="server" CssClass="left-margin" />
                                            <asp:HyperLink ID="hplMoreContacts" runat="server" Visible="false" NavigateUrl="javascript:OpenContactLists();" CssClass="more-link" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</div>
