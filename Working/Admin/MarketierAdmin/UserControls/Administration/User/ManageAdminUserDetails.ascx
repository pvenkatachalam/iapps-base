﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ManageAdminUserDetails.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageAdminUserDetails"
    ClientIDMode="Static" %>
<script type="text/javascript">
    function popUpExpiryDateCalendar(ctl) {
        var thisDate = new Date();
        if ($('#hdnDate').val() != "") {
            thisDate = new Date($('#hdnDate').val());
            expirationCalendar.setSelectedDate(thisDate)
        }

        if (!(expirationCalendar.get_popUpShowing()));
        expirationCalendar.show(ctl);
    }

    function calExpiryDate_onSelectionChanged(sender, eventArgs) {
        var selectedDate = expirationCalendar.getSelectedDate();
        if (selectedDate <= new Date()) {
            alert("<%= GUIStrings.ExpiryDateShouldbeGreaterThanToday %>");
        }
        else {
            $('#txtExpiryDate').val(expirationCalendar.formatDate(selectedDate, shortCultureDateFormat));
            $('#hdnDate').val(expirationCalendar.formatDate(selectedDate, calendarDateFormat));
        }
    }
    function OnRightArrowClick() {
        lstSelectdSiteId = $('#lstSites option:selected').val();
        lstSelectdSiteText = $('#lstSites option:selected').text();
        lstSelectedGroupId = $('#lstGroupsInSite option:selected').val();

        if ((lstSelectdSiteId != '' && typeof (lstSelectdSiteId) != 'undefined') && (lstSelectedGroupId != '' && typeof (lstSelectedGroupId) != 'undefined')) {
            if (typeof previewerGrpId != "undefined") {
                var isPreviewerSelected = false;
                $('#lstGroupsInSite option:selected').each(function (i, selected) {
                    if (selected.value.toLowerCase() == previewerGrpId.toLowerCase())
                        isPreviewerSelected = true;
                });

                var hasExistingSitePermissions = false;
                $("#lstPermissions option").each(function (i, selected) {
                    if (selected.value.toLowerCase().indexOf(lstSelectdSiteId.toLowerCase()) != -1) {
                        hasExistingSitePermissions = true;
                    }
                });

                if (hasExistingSitePermissions) {
                    if (isPreviewerSelected) {
                        if ($('#lstGroupsInSite option:selected').length > 1) {
                            alert("<%= JSMessages.YouCannotAddPreviewerWithOthers %>");
                            return false;
                        }

                        var hasPreviewerPermissions = false;
                        $("#lstPermissions option").each(function (i, selected) {
                            if (selected.value.toLowerCase().indexOf(previewerGrpId.toLowerCase()) != -1) {
                                hasPreviewerPermissions = true;
                            }
                        });

                        if (!hasPreviewerPermissions) {
                            var proceed = window.confirm("<%= JSMessages.RemoveAllPermissionAddPreviewer %>");
                            if (proceed) {
                                $("#lstPermissions option").each(function (i, selected) {
                                    if (selected.value.toLowerCase().indexOf(lstSelectdSiteId.toLowerCase()) != -1)
                                        $(this).remove()
                                });
                            }
                            else {
                                return false;
                            }
                        }
                    }
                    else {
                        var objPreviewer = null;
                        var previewerValue = lstSelectdSiteId + ":" + previewerGrpId;
                        $('#lstPermissions option').each(function (i, selected) {
                            if (selected.value.toLowerCase() == previewerValue.toLowerCase())
                                objPreviewer = $(this);
                        });

                        if (objPreviewer != null) {
                            var proceed = window.confirm("<%= JSMessages.RemovePreviewerAddPermission %>");
                            if (proceed)
                                objPreviewer.remove();
                            else
                                return false;
                        }
                    }
                }
            }

            $('#lstGroupsInSite option:selected').each(function (i, selected) {
                var newText = lstSelectdSiteText + " : " + selected.text;
                var newValue = lstSelectdSiteId + ":" + selected.value;
                var existPermission = $('#lstPermissions>option[value="' + newValue + '"]').length;
                if (existPermission == 0) {
                    $("#lstPermissions").append("<option value=" + newValue + ">" + newText + "</option>");
                }
            });
        }
    }

    function OnLeftArrowClick() {
        if ($("#lstPermissions option:selected").val() == '' || $("#lstPermissions option:selected").val() == undefined) {
            alert(__JSMessages["PleaseSelectAtLeastOnePermissionForThisUser"]);
            return;
        }
        $('#lstPermissions option:selected').remove();
    }

    function GroupListCallback(selectedValue) {
        cbSiteWiseGroup.set_callbackParameter("FillGroup:" + selectedValue);
        cbSiteWiseGroup.callback(selectedValue);
    }

    function ValidateEditUser() {
        if (Page_ClientValidate("valSaveUser") == true) {
            var lstPermission = document.getElementById("<%= lstPermissions.ClientID %>");
            if (lstPermission.options.length == 0) {
                alert(__JSMessages["PleaseSelectAtLeastOnePermissionForThisUser"]);
                return false;
            }

            $('#HdnSelectedPermissions').val("");
            for (var i = 0; i < lstPermission.options.length; i++) {
                $('#HdnSelectedPermissions').val($('#HdnSelectedPermissions').val() + lstPermission.options[i].value + ",");
            }

            return true;
        }
        else {
            return false;
        }
    }

    function ShowImagePopup() {
        OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectImage");
        return false;
    }
    function SelectImage() {
        imageUrl = popupActionsJson.CustomAttributes.ImageUrl;
        imageFileName = popupActionsJson.CustomAttributes.ImageFileName;
        imageId = popupActionsJson.SelectedItems[0];

        imageUrl = imageUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
        imageUrl = imageUrl.replace(new RegExp(imageFileName, "gi"), "thumbs/thumb_" + imageFileName);
        $("#imgImageURL").prop("src", imageUrl);
        $("#hdnImageId").val(imageId);
    }
    function ClearImageUrlValue() {
        $("#imgImageURL").prop("src", "/Admin/App_Themes/General/images/no_photo.jpg");
        $("#hdnImageId").val("");

        return false;
    }

    function OpenAddGroup() {
        $("#txtGroupName").val();
        $("#txtGroupDescription").val();
        $("#addNewGroup").iAppsDialog("open");
        return false;
    }

    function CancelAddGroup() {
        $("#addNewGroup").iAppsDialog("close");
        return false;
    }

    function SaveAddGroup() {
        if (Page_ClientValidate("valAddGroup") == true) {
            lstSelectdSiteId = $('#lstSites option:selected').val();
            cbSiteWiseGroup.set_callbackParameter("AddNewGroup:" + lstSelectdSiteId);
            cbSiteWiseGroup.callback();

            $("#addNewGroup").iAppsDialog("close");
        }
        return false;
    }
</script>
<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
    ValidationGroup="valSaveUser" />
<div class="dark-section-header clear-fix">
    <h2><%= GUIStrings.USERDETAILS %></h2>
</div>
<asp:PlaceHolder ID="phActiveDirectory" runat="server">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, UserTypeColon %>" /></label>
        <div class="form-value">
            <asp:RadioButtonList AutoPostBack="true" OnSelectedIndexChanged="RdListUserType_OnSelectedIndexChanged"
                ID="RdListUserType" runat="server" RepeatDirection="horizontal">
                <asp:ListItem Value="AD" runat="server" Text="<%$ Resources:GUIStrings, ActiveDirectory %>"></asp:ListItem>
                <asp:ListItem Value="NE" runat="server" Text="<%$ Resources:GUIStrings, iAPPSContentManager %>"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, ADUserColon %>" /></label>
        <div class="form-value">
            <asp:DropDownList ID="dwADUser" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="dwADUser_OnSelectedIndexChanged"
                runat="server" Width="284">
            </asp:DropDownList>
        </div>
    </div>
    <hr />
</asp:PlaceHolder>
<div class="clear-fix profile-properties">
    <div class="profile-picture picture-column">
        <div class="form-row">
            <asp:Image runat="server" ID="imgImageURL" ImageUrl="~/App_Themes/General/images/no_photo.jpg" />
        </div>
        <div class="form-row">
            <a href="#" class="edit-image" onclick="return ShowImagePopup();">
                <%= GUIStrings.EditProfilePicture %></a> <a href="#" class="remove-image"
                    onclick="return ClearImageUrlValue();">
                    <%= GUIStrings.Remove %></a>
            <asp:HiddenField ID="hdnImageId" runat="server" ClientIDMode="Static" />
        </div>
    </div>
    <asp:Panel ID="pnlUserDetails" runat="server" CssClass="user-details">
        <div class="columns">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FirstName %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="textBoxes" Width="275" />
                    <asp:RequiredFieldValidator ID="reqFlFirstName" ControlToValidate="txtFirstName"
                        ErrorMessage="<%$ Resources: JSMessages, FirstNameRequired %>"
                        ValidationGroup="valSaveUser" Display="None" SetFocusOnError="true" runat="server"></asp:RequiredFieldValidator>
                    <iAppsControls:iAppsCustomValidator ID="cvtxtFirstName" ControlToValidate="txtFirstName"
                        ErrorMessage="<%$ Resources: JSMessages, FirstNameNotValid %>"
                        runat="server" ValidateType="Name" ValidationGroup="valSaveUser" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, LastName %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtLastName" runat="server" CssClass="textBoxes" Width="275" />
                    <asp:RequiredFieldValidator ID="reqFlLastName" ControlToValidate="txtLastName" ValidationGroup="valSaveUser"
                        ErrorMessage="<%$ Resources: JSMessages, LastNameRequired %>"
                        Display="None" runat="server"></asp:RequiredFieldValidator>
                    <iAppsControls:iAppsCustomValidator ID="cvtxtLastName" ControlToValidate="txtLastName"
                        ErrorMessage="<%$ Resources: JSMessages, LastNameNotValid %>"
                        runat="server" ValidateType="Name" ValidationGroup="valSaveUser" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Email %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="textBoxes" Width="275" />
                    <asp:RequiredFieldValidator ID="reqFlEmail" ControlToValidate="txtEmail" ErrorMessage="<%$ Resources: JSMessages, EmailRequired %>"
                        ValidationGroup="valSaveUser" Display="None" runat="server"></asp:RequiredFieldValidator>
                    <iAppsControls:iAppsCustomValidator ID="cvtxtEmail" ControlToValidate="txtEmail"
                        ErrorMessage="<%$ Resources: JSMessages, EmailNotValid %>"
                        runat="server" ValidateType="Email" ValidationGroup="valSaveUser" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, EmailNotification %>" /></label>
                <div class="form-value text-value">
                    <asp:RadioButton ID="rdNotifyOn" Checked="true" Text="<%$ Resources:GUIStrings, On %>"
                        GroupName="EmailNotification" runat="server" />
                    <asp:RadioButton ID="rdNotifyOff" Text="<%$ Resources:GUIStrings, Off %>" GroupName="EmailNotification"
                        runat="server" />
                </div>
            </div>
        </div>
        <div class="columns right-column">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, UserName %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtUserName" runat="server" CssClass="textBoxes" Width="275" />
                    <asp:RequiredFieldValidator ID="reqFlUserName" ControlToValidate="txtUserName" ValidationGroup="valSaveUser"
                        ErrorMessage="<%$ Resources: JSMessages, UserNameRequired %>"
                        Display="None" runat="server"></asp:RequiredFieldValidator>
                    <iAppsControls:iAppsCustomValidator ID="cvtxtUserName" ControlToValidate="txtUserName"
                        ErrorMessage="<%$ Resources: JSMessages, UserNameNotValid %>"
                        runat="server" ValidateType="UserName" ValidationGroup="valSaveUser" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Password %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtExternalPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                        Width="275" />
                    <asp:RequiredFieldValidator ID="reqFlExternalPassword" ControlToValidate="txtExternalPassword"
                        ValidationGroup="valSaveUser" ErrorMessage="<%$ Resources: JSMessages, PasswordRequired %>"
                        Display="None" runat="server"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regtxtExternalPassword" runat="server" ControlToValidate="txtExternalPassword"
                        ErrorMessage="<%$ Resources:JSMessages, InvalidCharactersInPassword %>" SetFocusOnError="True"
                        ValidationExpression="^[^<>]+$" ValidationGroup="valSaveUser" Display="None" />

                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, ConfirmPassword %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                        Width="275" />
                    <asp:CompareValidator ID="cmpVlConfirmPassword" ControlToValidate="txtExternalPassword"
                        ValidationGroup="valSaveUser" ControlToCompare="txtConfirmPassword" Display="None"
                        ErrorMessage="<%$ Resources: JSMessages, ConfirmPasswordRequired %>"
                        runat="server"></asp:CompareValidator>
                </div>
            </div>
            <asp:PlaceHolder ID="phExpiryDate" runat="server">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, ExpirationDateColon %>" /></label>
                    <div class="form-value calendar-value">
                        <asp:HiddenField ID="hdnDate" runat="server" />
                        <asp:TextBox ID="txtExpiryDate" runat="server" CssClass="textBoxes" Width="263" AutoCompleteType="none"></asp:TextBox>
                        <span>
                            <img id="calendar_button" class="buttonCalendar" alt="<%= GUIStrings.ShowCalendar %>"
                                src="/Admin/App_Themes/General/images/calendar-button.png" onclick="popUpExpiryDateCalendar(this);" />
                        </span>
                        <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="expirationCalendar"
                            MaxDate="9998-12-31" PopUpExpandControlId="calendar_button">
                            <ClientEvents>
                                <SelectionChanged EventHandler="calExpiryDate_onSelectionChanged" />
                            </ClientEvents>
                        </ComponentArt:Calendar>
                        <asp:CompareValidator ID="cmpVlExpiryDate" runat="server" ControlToValidate="txtExpiryDate"
                            ValidationGroup="valSaveUser" Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources: JSMessages, DateNotValid %>"
                            Display="None"></asp:CompareValidator>
                        <asp:RangeValidator ID="regVlExpiryDateValidator" runat="server" ControlToValidate="txtExpiryDate"
                            ValidationGroup="valSaveUser" ErrorMessage="<%$ Resources: JSMessages, DateNotValid %>"
                            Display="None" SetFocusOnError="true" MaximumValue="12/31/9998" MinimumValue="01/01/1900"
                            Type="Date"></asp:RangeValidator>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </asp:Panel>
</div>
<asp:PlaceHolder ID="phPermissions" runat="server">
    <div class="dark-section-header clear-fix">
        <h2><%= GUIStrings.UserSiteAccessHr %></h2>
    </div>
    <div class="bottom-section clear-fix">
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, SitesBr %>" /></label>
            <div class="form-value">
                <asp:ListBox ID="lstSites" onchange="GroupListCallback(this.value);" runat="server"
                    Rows="6" SelectionMode="Single" />
            </div>
        </div>
        <div class="action-column">
            <img src="/Admin/App_Themes/General/images/icon-plus.gif" alt="Plus" title="Plus" />
        </div>
        <div class="columns center-column form-edit-row">
            <label class="form-label">
                <%= GUIStrings.GroupsBr %>
                <a href="#" onclick="return OpenAddGroup();" class="small-button">
                    <%= GUIStrings.AddNewGroup %></a>
            </label>
            <div class="form-value">
                <iAppsControls:CallbackPanel ID="cbSiteWiseGroup" runat="server" CssClass="form-value">
                    <ContentTemplate>
                        <asp:ListBox SelectionMode="multiple" ID="lstGroupsInSite" runat="server" Rows="6" />
                    </ContentTemplate>
                </iAppsControls:CallbackPanel>
            </div>
        </div>
        <div class="action-column">
            <img src="~/App_Themes/General/images/action-right-arrow.gif" id="RhtArrow" runat="server" alt="<%$ Resources:GUIStrings, AddPermission %>"
                class="arrowImage actionImage" title="<%$ Resources:GUIStrings, AddPermission %>" /><br />
            <img src="~/App_Themes/General/images/action-left-arrow.gif" id="LftArrow" runat="server" alt="<%$ Resources:GUIStrings, DeletePermission %>"
                class="actionImage" title="<%$ Resources:GUIStrings, DeletePermission %>" />
        </div>
        <div class="columns">
            <label class="form-label">
                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, CurrentSitesAccess %>" /><span class="req">&nbsp;*</span></label>
            <div class="form-value">
                <asp:ListBox ID="lstPermissions" runat="server" Rows="6" SelectionMode="multiple" />
            </div>
        </div>
    </div>
</asp:PlaceHolder>
<div class="iapps-modal" id="addNewGroup" style="display: none;">
    <div class="modal-header clear-fix">
        <h2>
            <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, AddNewGroup %>" />
        </h2>
    </div>
    <div class="modal-content">
        <asp:ValidationSummary ID="valAddGroup" runat="server" ShowSummary="false" ShowMessageBox="True"
            ValidationGroup="valAddGroup" />
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings, GroupName %>" /><span class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtGroupName" runat="server" CssClass="textBoxes" Width="260" ClientIDMode="Static" />
                <asp:RequiredFieldValidator ID="reqtxtGroupName" runat="server" Display="None" SetFocusOnError="true"
                    ControlToValidate="txtGroupName" ErrorMessage="<%$ Resources: JSMessages, GroupNameRequired %>"
                    ValidationGroup="valAddGroup" />
                <iAppsControls:iAppsCustomValidator ID="cvtxtGroupName" ControlToValidate="txtGroupName"
                    ErrorMessage="<%$ Resources: JSMessages, GroupNameNotValid %>"
                    runat="server" ValidateType="Title" ValidationGroup="valAddGroup" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtGroupDescription" runat="server" CssClass="textBoxes" Width="260"
                    TextMode="MultiLine" ClientIDMode="Static" />
                <iAppsControls:iAppsCustomValidator ID="cvtxtGroupDescription" ControlToValidate="txtGroupDescription"
                    ErrorMessage="<%$ Resources: JSMessages, DescriptionNotValid %>"
                    runat="server" ValidateType="Description" ValidationGroup="valAddGroup" />
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <asp:Button ID="btnCancelGroup" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
            ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CancelAddGroup();"
            CausesValidation="false" />
        <asp:Button ID="btnSaveGroup" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            OnClientClick="return SaveAddGroup();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"
            ValidationGroup="valAddGroup" />
    </div>
</div>
<asp:HiddenField ID="HdnSelectedPermissions" runat="server" Value="" />
<asp:HiddenField ID="hdnUserId" runat="server" Value="" />
<asp:HiddenField ID="HdnExistingPermissions" runat="server" Value="" />
