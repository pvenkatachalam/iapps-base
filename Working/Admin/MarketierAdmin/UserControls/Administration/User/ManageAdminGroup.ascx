﻿<%@ Control Language="C#" AutoEventWireup="True" Codebehind="ManageAdminGroup.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageAdminGroup" %>
<script type="text/javascript">
    function grdAdminGroup_OnLoad(sender, eventArgs) {
        $(".manage-groups").gridActions({ objList: grdAdminGroup });
    }

    function grdAdminGroup_OnRenderComplete(sender, eventArgs) {
        $(".manage-groups").gridActions("bindControls");

        $(".view-permissions").iAppsClickMenu({
            width: "250px",
            heading: "<%= GUIStrings.Permissions %>",
            create: function (e, menu) {
                var permissions = grdAdminGroup.getItemFromKey(0, e.attr("objectId")).getMember("AllPermissions").get_value().split('#');
                if (permissions.length > 0) {
                    $.each(permissions, function () {
                        if (this.toString() != "")
                            menu.addListItem(this.toString());
                    });
                }
            }
        });
    }

    function grdAdminGroup_OnCallbackComplete(sender, eventArgs) {
        $(".manage-groups").gridActions("displayMessage");
    }

    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;
        switch (gridActionsJson.Action) {
            case "Edit":
                doCallback = false;
                EditAdminGroupDetails(selectedItemId);
                break;
            case "Add":
                doCallback = false;
                EditAdminGroupDetails(null);
                break;
            case "Delete":
                doCallback = DeleteAdminGroup(selectedItem, true);
                break;
            case "Deactivate":
                doCallback = DeleteAdminGroup(selectedItem, false, false);
                break;
            case "Activate":
                doCallback = DeleteAdminGroup(selectedItem, false, true);
                break;
            case "Permissions":
                doCallback = false;
                RedirectToAdminPage("ManageAdminUserPermissions",
                    stringformat("MemberType=2&MemberId={0}&MemberName={1}", selectedItemId, selectedItem.getMember("Title").get_text()));
                break;
            case "AddToList":
                doCallback = false;
                AddManualListItem(grdAdminGroup);
                break;
        }

        if (doCallback) {
            grdAdminGroup.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdAdminGroup.callback();
        }
    }

    function EditAdminGroupDetails(groupId) {
        RedirectToAdminPage("ManageAdminGroupDetails", groupId != null ? "GroupId=" + groupId : "");
    }

    function DeleteAdminGroup(selectedItem, deleteGroup, activateGroup) {
        var selectedGroupId = selectedItem.getMember("Id").get_text();
        var selectedGroupName = selectedItem.getMember("Title").get_text();
        var memberType = 2;

        if (deleteGroup) {
            isConfirm = window.confirm(GetMessage("DeleteConfirmation"));
            selectedOption = 1;
        }
        else if (activateGroup) {
            isConfirm = window.confirm(GetMessage("ActivateConfirmation"));
            selectedOption = 4;
        }
        else {
            isConfirm = window.confirm(GetMessage("DeactivateConfirmation"));
            selectedOption = 2;
        }

        if (isConfirm) {
            var count = 0;
            if (deleteGroup || !activateGroup)
                count = parseInt(CheckUserWorklowsForGroup(selectedGroupId, memberType, selectedGroupName, null));

            if (count > 0) {
                var popQueryString = "id=" + selectedGroupId + "&type=" + memberType + "&mode=" + selectedOption;
                OpeniAppsAdminPopup("DeactivateCMSGroup", popQueryString);
            }
            else {
                var statusOfOperation = DeleteDeactivateGroup(selectedGroupId, selectedGroupName, deleteGroup ? 1 : (activateGroup ? 3 : 2), null);
                if (statusOfOperation == "True")
                    return true;
                else
                    alert(statusOfOperation);
            }
        }

        return false;
    }

    function Grid_ItemSelect(sender, eventArgs) {
        if (eventArgs.selectedItem.getMember("Status").get_text() == "1") {
            sender.getItemByCommand("Deactivate").setText("<%= JSMessages.DeactivateGroup %>");
            sender.getItemByCommand("Deactivate").setComandArgument("Deactivate");
            sender.getItemByCommand("Deactivate").removeClass("activate");
            sender.getItemByCommand("Deactivate").addClass("deactivate");
        }
        else {
            sender.getItemByCommand("Deactivate").setText("<%= JSMessages.ActivateGroup %>");
            sender.getItemByCommand("Deactivate").setComandArgument("Activate");
            sender.getItemByCommand("Deactivate").removeClass("deactivate");
            sender.getItemByCommand("Deactivate").addClass("activate");
        }
    }
    // Copied from AddNewUser.js and removed all part
    function PopupReturnCall(returnValue) {
        if (returnValue == 'True') {
            grdAdminGroup.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdAdminGroup.callback();
        }
    }
</script>
<div class="manage-groups">
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="grdAdminGroup" SkinID="Default" runat="server" Width="100%" ManualPaging="true"
            RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdAdminGroupLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdAdminGroup_OnLoad" />
                <CallbackComplete EventHandler="grdAdminGroup_OnCallbackComplete" />
                <RenderComplete EventHandler="grdAdminGroup_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["Id"]%>'>
                            <%--<div class="first-cell">
                                <%# GetGridItemRowNumber(Container.DataItem, grdAdminGroup)%>
                            </div>--%>
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# String.Format("{0}", Container.DataItem["Description"])%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["Status"].ToString().ToLower() == "1" ? GUIStrings.Active : GUIStrings.InActive)%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="PopupTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item clear-fix">
                            <div class="first-cell">
                                <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                    value='<%# Container.DataItem["Id"]%>' />
                                <h4>
                                    <%# Container.DataItem["Title"]%></h4>
                            </div>
                            <div class="last-cell">
                                <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["Status"].ToString().ToLower() == "1" ? GUIStrings.Active : GUIStrings.InActive)%>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdAdminGroupLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdAdminGroup) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</div>
