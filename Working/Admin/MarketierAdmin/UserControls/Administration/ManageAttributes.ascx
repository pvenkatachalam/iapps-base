﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageAttributes.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageAttributes" %>

<%@ Register TagPrefix="UC" TagName="GroupSelector" Src="~/UserControls/General/GroupSelector.ascx" %>
<script type="text/javascript">

    function upManageAttributes_OnCallbackComplete(sender, eventArgs) {
        $("#manage_attributes").gridActions("displayMessage");
    }

    function upManageAttributes_OnRenderComplete(sender, eventArgs) {
        $("#manage_attributes").gridActions("bindControls");
        $("#manage_attributes").iAppsSplitter();
    }

    function upManageAttributes_OnLoad(sender, eventArgs) {
        $("#manage_attributes").gridActions({
            objList: $(".collection-table"),
            type: "list",
            pageSize: 10,
            onCallback: function (sender, eventArgs) {
                upManageAttributes_Callback(sender, eventArgs);
            },
            onItemCheckChange: function (sender, eventArgs) {
                upManageAttributes_CheckChange(sender, eventArgs);
            }
        });
    }

    function upManageAttributes_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems.first();

        switch (gridActionsJson.Action) {
            case "AddAttribute":
                doCallback = false;
                OpeniAppsAdminPopup("ManageAttributeDetails",
                    stringformat("GroupId={0}", treeActionsJson.FolderId), "RefreshAttributes");
                break;
            case "EditProperties":
                doCallback = false;
                OpeniAppsAdminPopup("ManageAttributeDetails",
                    stringformat("Id={0}&GroupId={1}", selectedItemId, treeActionsJson.FolderId), "RefreshAttributes");
                break;
            case "Delete":
                doCallback = window.confirm("Are you sure you want to delete this attribute?");
                break;
        }

        if (doCallback) {
            upManageAttributes.set_callbackParameter(JSON.stringify(gridActionsJson));
            upManageAttributes.callback();
        }
    }

    function upManageAttributes_CheckChange(sender, eventArgs) {
        if (typeof UpdateManualListItem == "function")
            UpdateManualListItem(eventArgs.Item, eventArgs.Selected);
    }

    function UnselectItem(id) {
        gridActionsJson.SelectedItems.remove(id);

        $("#manage_attributes").gridActions("bindControls", { forceBind: true });
    }

    function RefreshAttributes() {
        $("#manage_attributes").gridActions("callback", { refreshTree: true });
    }

    function LoadItemsGrid() {
        $("#manage_attributes").gridActions("selectNode", treeActionsJson.FolderId);
    }
</script>

<div id="manage_attributes">
    <div class="left-control">
        <UC:GroupSelector ID="groupSelector" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upManageAttributes" runat="server" OnClientCallbackComplete="upManageAttributes_OnCallbackComplete"
            OnClientRenderComplete="upManageAttributes_OnRenderComplete" OnClientLoad="upManageAttributes_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActions" runat="server" />
                <div class="grid-section">
                    <asp:ListView ID="lvManageAttributes" runat="server" ItemPlaceholderID="phManageAttributes">
                        <EmptyDataTemplate>
                            <div class="empty-list">
                                <%= GUIStrings.NoItemsFound%>
                            </div>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div class="collection-table attribute-list fat-grid">
                                <asp:PlaceHolder ID="phManageAttributes" runat="server" />
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <asp:PlaceHolder ID="DetailsTemplate" runat="server">
                                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                    <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id")%>'>
                                        <%--<div class="first-cell">
                                            <%# Eval("RowNumber")%>
                                        </div>--%>
                                        <div class="large-cell">
                                            <h4>
                                                <asp:Literal ID="ltTitle" runat="server" /></h4>
                                            <p>
                                                <asp:Literal ID="ltDescription" runat="server" />
                                            </p>
                                        </div>
                                        <div class="small-cell">
                                            <div class="child-row">
                                                <asp:Literal ID="ltType" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <asp:Literal ID="ltCreated" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="PopupTemplate" runat="server">
                                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                    <div class="modal-grid-row grid-item clear-fix" objectid='<%# Eval("Id")%>'>
                                        <div class="first-cell">
                                            <input type="checkbox" id="chkSelected" runat="server" class="item-selector"
                                                visible="false" value='<%# Eval("Id") %>' />
                                            <h4 datafield="Title">
                                                <asp:Literal ID="p_ltTitle" runat="server" /></h4>
                                        </div>
                                        <div class="last-cell">
                                            <div class="child-row">
                                                <asp:Literal ID="p_ltType" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</div>

