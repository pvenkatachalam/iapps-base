﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailSteps.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.EmailSteps" %>
<div class="steps clear-fix">
    <ul class="clear-fix">
        <li><asp:HyperLink ID="hplCreateEmail" runat="server" Text="<%$ Resources:GUIStrings, CreateEmail %>" NavigateUrl="~/Emails/ManageEmailDetails.aspx?step=1" /></li>
        <li><asp:HyperLink ID="hplChooseRecipients" runat="server" Text="<%$ Resources:GUIStrings, ChooseRecipients %>" NavigateUrl="~/Emails/ChooseRecipients.aspx?step=2" /></li>
        <li><asp:HyperLink ID="hplScheduleEmail" runat="server" Text="<%$ Resources:GUIStrings, ScheduleEmailSend %>" NavigateUrl="~/Emails/ScheduleEmail.aspx?step=3" /></li>
        <li><asp:HyperLink ID="hplConfirm" runat="server" Text="<%$ Resources:GUIStrings, Confirm %>" NavigateUrl="~/Emails/ConfirmSend.aspx?step=4" /></li>
    </ul>
</div>