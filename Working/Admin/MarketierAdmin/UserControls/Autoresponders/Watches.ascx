﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Watches.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.SearchOptions.Watches" %>
<script type="text/javascript">
    function ValidateCheckBoxList() {
        var list = document.getElementById('<%=cblWatches.ClientID %>').rows;
        var counter = 0;

        for (i = 0; i < list.length; i++) {
            if (list[i].cells[0].childNodes[0].type == "checkbox") {
                if (list[i].cells[0].childNodes[0].checked) {
                    counter++;

                    break;
                }
            }
        }

        if (counter == 0) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseselectaWatchedEvent %>'/>");

            return false;
        }
        else {
            return true;
        }
    }
</script>
<asp:PlaceHolder ID="phNegation" runat="server">
    <div class="form-row">
        <asp:CheckBox ID="chkNegation" runat="server" Text="<%$ Resources:GUIStrings, Selectusersthatdonotmeetthiscriteria %>" />
    </div>
</asp:PlaceHolder>
<div class="form-row checkbox-list">
    <asp:CheckBoxList runat="server" ID="cblWatches">
    </asp:CheckBoxList>
</div>