﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsSearch.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.SearchOptions.ProductsSearch" %>
<script type="text/javascript">
    var productSearchPopup;
    var strSelectedProductIds;
    function SearchProduct() {
        var qs = 'ShowProducts=true';
        OpeniAppsCommercePopup('AddProductPopup', '', '');
    }

    function ShowProductSearchPopup(popupPath) {
        productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', popupPath, '<asp:localize runat="server" text="<%$ Resources:JSMessages, ProductSearchPopup %>"/>', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
        productSearchPopup.onclose = function () {
            var a = document.getElementById('ProductSearchPopup');
            var ifr = a.getElementsByTagName("iframe");
            window.frames[ifr[0].name].location.replace("/blank.html");
            return true;
        }
    }

    function js_RemoveChar(replacedString) {
        replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;").replace(new RegExp("'", "g"), "&apos;");
        return replacedString;
    }

    function CloseProductPopup(arrSelected) {
        var productIds = '';
        var productNames = '';
        var selectedArray;
        var row;
        var existingValues = document.getElementById("<%=hdnSelectedProductIds.ClientID%>").value;
        if (existingValues == null)
            existingValues = '';
        if (arrSelected != null && arrSelected != '' && arrSelected != 'undefined') {
            for (k = 0; arrSelected.length; k++) {
                selectedArray = arrSelected.pop();
                if (productIds == '') {
                    if (existingValues.indexOf(selectedArray[0]) < 0) {
                        productIds = selectedArray[0];
                        productNames = selectedArray[2];
                    }
                }
                else {
                    if (existingValues.indexOf(selectedArray[0]) < 0) {
                        productIds = productIds + ',' + selectedArray[0];
                        productNames = productNames + ',' + selectedArray[2];
                    }
                }
            }
            if (document.getElementById("<%=hdnSelectedProductIds.ClientID%>").value != '')
                productIds = document.getElementById("<%=hdnSelectedProductIds.ClientID%>").value + ',' + productIds;
            if (document.getElementById("<%=hdnSelectedProductNames.ClientID%>").value != '')
                productNames = document.getElementById("<%=hdnSelectedProductNames.ClientID%>").value + ',' + productNames;
            document.getElementById("<%=hdnSelectedProductIds.ClientID%>").value = productIds;
            document.getElementById("<%=hdnSelectedProductNames.ClientID%>").value = productNames;

            strSelectedProductIds = productIds;
            PopulateControls(productIds, productNames)
        }
        CloseiAppsCommercePopup();
    }

    function PopulateControls(ids, names) {
        var idsArray = ids.split(",");
        var namesArray = names.split(",");
        var originalDiv = document.getElementById("divProductIds");
        var iIndex;
        originalDiv.innerHTML = "";
        var divHtml = '';
        for (iIndex = 0; iIndex < idsArray.length; iIndex++) {
            if (idsArray[iIndex] != null && idsArray[iIndex] != '') {
                var strSpan = "<div id=\"Span" + iIndex + "\" class=\"form-row\">";
                var strlbl = "<div id=\"Lbl" + iIndex + "\" for=\"Img" + iIndex + "\" class=\"form-label\">" + namesArray[iIndex].replace(new RegExp("&apos;", "g"), "'") + "</div>";
                var strImg = "<div><img class=\"deleteProduct\" src=\"/iapps_images/icon-delete-cross.png\" alt=\"" + js_RemoveChar(namesArray[iIndex]) + "\"  id=\"Img" + iIndex + "\" onclick=\"RemoveProduct('Span" + iIndex + "','" + idsArray[iIndex] + "','" + js_RemoveChar(namesArray[iIndex].replace(new RegExp("&apos;", "g"), "%27")) + "')\"" + "/></div>";
                strSpan = strSpan + strlbl + strImg + "</div>";
                divHtml = divHtml + strSpan;
            }
        }
        originalDiv.innerHTML = divHtml;
    }


    function RemoveProduct(SpanId, id, name) {
        name = name.replace(new RegExp("%27", "g"), "&apos;");
        name = name.replace(new RegExp('[*]', "g"), "%2A");
        var currentIds = document.getElementById("<%=hdnSelectedProductIds.ClientID%>").value;
        currentIds = currentIds.replace(new RegExp(id, "g"), "");
        currentIds = currentIds.replace(new RegExp(id, "g"), "");
        currentIds = currentIds.replace(new RegExp(",,", "g"), ",");
        if (currentIds == ",")
            currentIds = "";
        document.getElementById("<%=hdnSelectedProductIds.ClientID%>").value = currentIds;
        var currentNames = document.getElementById("<%=hdnSelectedProductNames.ClientID%>").value;
        currentNames = currentNames.replace(new RegExp(name, "g"), "");
        currentNames = currentNames.replace(new RegExp("[*]", "g"), "%2A")
        name = name.replace(new RegExp("&apos;", "g"), "'")
        currentNames = currentNames.replace(new RegExp(name, "g"), "");
        currentNames = currentNames.replace(new RegExp(id, "g"), "");
        currentNames = currentNames.replace(new RegExp(",,", "g"), ",");
        if (currentNames == ",")
            currentNames = "";
        document.getElementById("<%=hdnSelectedProductNames.ClientID%>").value = currentNames;
        strSelectedProductIds = currentIds;
        var originalDiv = document.getElementById("divProductIds");
        var oldSpan = document.getElementById(SpanId);
        originalDiv.removeChild(oldSpan);
    }

    function validateProductPurchase() {
        if (Trim(document.getElementById("<%=hdnSelectedProductIds.ClientID %>").value) == "") {
            return false;
        }
        return true;
    }
</script>
<div class="button-row">
    <input type="button" id="btnSearch" onclick="SearchProduct()" value="<%= GUIStrings.SearchProducts %>" class="button" />
    <asp:HiddenField ID="hdnSelectedProductIds" runat="server" />
    <asp:HiddenField ID="hdnSelectedProductNames" runat="server" />
</div>
<div id="divProductIds" class="checkbox-list clear-fix"></div>
