﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FromToDate.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.SearchOptions.FromToDate" %>
<script type="text/javascript">

    var <%=this.ClientID%>_textboxFrom;
    var <%=this.ClientID%>_textboxTo;
    function <%=this.ClientID %>_popCalendar(txtControl)
    {
        <%=this.ClientID%>_textboxFrom = txtControl;
        var thisDate = new Date (iAppsCurrentLocalDate);
        thisDate.setDate(thisDate.getDate() -1);
        obj = document.getElementById(txtControl).value;
        var cal=<%=this.LastLoginCalender.ClientID%> ;
        if (obj != null && obj != "")
        {
        }
        if(!(cal.get_popUpShowing())); 
        cal.show();
    }

    function <%=this.ClientID %>_popToCalendar(txtControl)
    {
        <%=this.ClientID%>_textboxTo = txtControl;
        var thisDate = new Date (iAppsCurrentLocalDate);
        thisDate.setDate(thisDate.getDate() -1);
        obj = document.getElementById(txtControl).value;
        var cal=<%=this.LastLoginToCalender.ClientID%> ;;
        if (obj != null && obj != "")
        {
        }
        if(!(cal.get_popUpShowing())); 
        cal.show();
    }

    function <%=this.ClientID%>_LastLoginCalender_onSelectionChanged(sender, eventArgs)
    { 
        var cal=sender;
        var selectedDate = cal.getSelectedDate();
        document.getElementById(<%=this.ClientID%>_textboxFrom).value = cal.formatDate(selectedDate,"MM/dd/yyyy");
    }
    function <%=this.ClientID%>_LastLoginToCalender_onSelectionChanged(sender, eventArgs)
    { 
        var cal=sender;
        var selectedDate = cal.getSelectedDate();
        document.getElementById(<%=this.ClientID%>_textboxTo).value = cal.formatDate(selectedDate,"MM/dd/yyyy");
    }

    function ValidateFromToDate()
    {
        var msg='';
        var startDate = document.getElementById("<%=this.ClientID%>_txtDate").value;
        var endDate = document.getElementById("<%=this.ClientID%>_txtToDate").value;



        if(Trim(startDate) == '')
        {
            alert('<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenterFromDate %>"/>');
            document.getElementById("<%=this.ClientID%>_txtDate").select();
            return false;
                     
        }
        else 
        {
            if (isDate(startDate)==false)
                return false;
        }

        if(Trim(endDate) == '')
        {
            alert('<asp:localize runat="server" text="<%$ Resources:JSMessages, PleaseenterToDate %>"/>');
            document.getElementById("<%=this.ClientID%>_txtToDate").select();
            return false;
        }
        else 
        {
            if (isDate(endDate)==false)
                return false;
        }
                
        startDate = new Date(startDate);
        endDate = new Date(endDate);

            
            
               
        var startDateMil = startDate.getTime();
        var endDateMil = endDate.getTime();
                
        if(endDateMil < startDateMil)
        {
            //sometime date may be same but time makes problem.
            if (endDate.getFullYear() < startDate.getFullYear())
            {
                alert('<asp:localize runat="server" text="<%$ Resources:JSMessages, ToDatemustbegreaterthanFromDate %>"/>');
                document.getElementById("<%=this.ClientID%>_txtToDate").select();
                return false;
            }
            else if (endDate.getMonth() < startDate.getMonth())
            {
                alert('<asp:localize runat="server" text="<%$ Resources:JSMessages, ToDatemustbegreaterthanFromDate %>"/>');
            document.getElementById("<%=this.ClientID%>_txtToDate").select();
            return false;
        }
        else if (endDate.getDate() < startDate.getDate())
        {
            alert( '<asp:localize runat="server" text="<%$ Resources:JSMessages, ToDatemustbegreaterthanFromDate %>"/>');
            document.getElementById("<%=this.ClientID%>_txtToDate").select();
            return false;
        }
}
        
             
    return true;
}
</script>
<div class="form-row">
    <asp:CheckBox ID="chkNegation" runat="server" Text="<%$ Resources:GUIStrings, Selectusersthatdonotmeetthiscriteria %>" />
</div>
<div class="from-to">
    <div class="form-row">
        <label class="form-label" for="<%=txtDate.ClientID%>">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, From %>" /></label>
        <div class="form-value calendar-value">
            <asp:TextBox ID="txtDate" runat="server" CssClass="textBoxes calendarDate" Text="<%#DateTime.Now.Date.ToString()%>" />
            <img id="calendar_button" alt="<%$ Resources:GUIStrings, ShowCalendar %>" title="<%$ Resources:GUIStrings, ShowCalendar %>" class="buttonCalendar"
                src="/iapps_images/calendar-button.png" runat="server" />
        </div>
        <asp:CompareValidator ID="cvtxtDate" runat="server" ControlToValidate="txtDate" Type="Date"
            Operator="DataTypeCheck" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseenteravalidFromDate %>"></asp:CompareValidator>
        <ComponentArt:Calendar runat="server" SkinID="Default" ID="LastLoginCalender" MaxDate="9998-12-31">
        </ComponentArt:Calendar>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=txtToDate.ClientID%>">
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, To %>" /></label>
        <div class="form-value calendar-value">
            <asp:TextBox ID="txtToDate" runat="server" CssClass="textBoxes calendarDate" Text="<%#DateTime.Now.Date.ToString()%>"></asp:TextBox>
            <img id="calendarTo_button" alt="<%$ Resources:GUIStrings, ShowCalendar %>" title="<%$ Resources:GUIStrings, ShowCalendar %>" class="buttonCalendar"
                src="/iapps_images/calendar-button.png" runat="server" />
        </div>
        <asp:CompareValidator ID="cvDateRange" runat="server" ControlToCompare="txtDate"
            Type="Date" Operator="GreaterThan" ControlToValidate="txtToDate" Text="*" ErrorMessage="<%$ Resources:JSMessages, Fromdateshouldbelessthantodate %>"></asp:CompareValidator>
        <asp:CompareValidator ID="cvtxtToDate" runat="server" ControlToValidate="txtToDate"
            Type="Date" Operator="DataTypeCheck" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseenteravalidToDate %>"></asp:CompareValidator>
        <ComponentArt:Calendar runat="server" SkinID="Default" ID="LastLoginToCalender" MinDate="1990-01-01"
            MaxDate="9998-12-31">
        </ComponentArt:Calendar>
    </div>
</div>
