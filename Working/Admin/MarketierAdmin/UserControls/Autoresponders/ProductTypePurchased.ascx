﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTypePurchased.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.SearchOptions.ProductTypePurchased" %>

<script type="text/javascript">
    var productTypes;
    var productTypesName;
    var productTypePopup;

    function SelectProductTypes() {
        if (typeof (strSelectedProductIds) != "undefined" && strSelectedProductIds != null && strSelectedProductIds.length > 0) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Tosearchbyproducttypepleaseremoveallproductsfromyoursearchcriteria %>'/>");
        }
        else {
            OpeniAppsCommercePopup('ProductTypeMultiSelect', '', '');
        }
    }

    function SetProductTypes(selectedProductTypes, selectedProductTypeName) {
        productTypes = selectedProductTypes;
        productTypesName = selectedProductTypeName;
        StoreProductTypes();
        DisplaySelectedProductTypes();
    }

    function RemoveSpecialCharacters(input) {
        var output = input;

        output.replace("\"", "&quot;");
        output.replace("'", "&apos;");

        return output;
    }

    function DisplaySelectedProductTypes() {
        var divProductTypes = document.getElementById("divProductTypes");
        var htmlString = "";
        for (var i = 0; i < productTypesName.length; i++) {
            htmlString += "<div class=\"form-row\">";
            htmlString += "<div class=\"form-label\">" + RemoveSpecialCharacters(productTypesName[i]) + "</div>";
            htmlString += "<div><img src=\"/iapps_images/icon-delete-cross.png\" onclick=\"RemoveProductType(" + i + ");\" alt=\"" + RemoveSpecialCharacters(productTypesName[i]) + "\" /></div>";
            htmlString += "</div>";
        }

        divProductTypes.innerHTML = htmlString;
    }

    function RemoveProductType(index) {
        productTypes.splice(index, 1);

        StoreProductTypes();
        DisplaySelectedProductTypes();
    }

    function LoadProductTypes(strIds, strDescriptions) {
        if (strIds.length > 0) {
            var productTypeIds = strIds.split(',');
            var productTypeDescriptions = strDescriptions.split(',');

            productTypes = [];

            for (var i = 0; i < productTypeIds.length; i++) {
                var productType = {};

                productType.Id = productTypeIds[i];
                productType.Description = productTypeDescriptions[i];

                productTypes[productTypes.length] = productType;
            }

            DisplaySelectedProductTypes();
        }
    }

    function StoreProductTypes() {
        var ids = "";
        var descriptions = "";

        for (var i = 0; i < productTypes.length; i++) {
            if (i == productTypes.length - 1) {
                ids += productTypes[i];
                descriptions += productTypesName[i];
            }
            else {
                ids += productTypes[i] + ",";
                descriptions += productTypesName[i] + ",";
            }
        }

        document.getElementById("<%= hdnIds.ClientID %>").value = ids;
        document.getElementById("<%= hdnDescriptions.ClientID %>").value = descriptions;
    }

    function ValidateProductTypePurchased() {
        if (Trim(document.getElementById("<%=hdnIds.ClientID %>").value) == "") {
            return false;
        }
        
        return true;
    }

    function fn_HideProductType() {
        CloseiAppsCommercePopup();
    }
</script>

<div class="button-row">
    <input type="button" id="btnSearch" onclick="SelectProductTypes();" value="<%= GUIStrings.SearchProductTypes %>"
        class="button" />
    <asp:HiddenField ID="hdnIds" runat="server" />
    <asp:HiddenField ID="hdnDescriptions" runat="server" />
</div>
<div id="divProductTypes" class="checkbox-list clear-fix">
</div>
