﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormSubmitted.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.SearchOptions.FormSubmitted" %>
<script type="text/javascript">
    function ValidateCheckBoxList() {
        var list = document.getElementById('<%=cbkForms.ClientID %>').rows;
        var counter = 0;
        for (i = 0; i < list.length; i++) {
            if (list[i].cells[0].childNodes[0].type == "checkbox") {
                if (list[i].cells[0].childNodes[0].checked) {
                    counter++;
                    break;
                }
            }
        }
        if (counter == 0) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseselectaform %>'/>");
            return false;
        }
        else {
            return true;
        }
    }
</script>
<asp:PlaceHolder ID="phNegation" runat="server">
    <div class="form-row">
        <asp:CheckBox ID="chkNegation" runat="server" Text="<%$ Resources:GUIStrings, Selectusersthatdonotmeetthiscriteria %>" />
    </div>
</asp:PlaceHolder>
<div class="form-row checkbox-list">
    <asp:CheckBoxList runat="server" ID="cbkForms" />
</div>