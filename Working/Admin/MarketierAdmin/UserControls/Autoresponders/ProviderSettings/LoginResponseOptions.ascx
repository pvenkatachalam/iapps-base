﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginResponseOptions.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Responses.ProviderSettings.LoginResponseOptions" %>
<%@ Register TagPrefix="ln" TagName="loggedInFromTo" Src="~/UserControls/Autoresponders/FromToDate.ascx" %>

<script type="text/javascript">
    function providerOptionValidation() {
        return ValidateFromToDate();
    }
</script>
<div class="loggedIn login clear-fix">
    <ln:loggedInFromTo ID="ctlLoggedInFromTo" runat="server" />
    <div class="form-row checkbox-row">
        <asp:CheckBox ID="chkbOnce" runat="server" Text="<%$ Resources:GUIStrings, SendAutoresponderOnlyOnce %>" />
    </div>
</div>
