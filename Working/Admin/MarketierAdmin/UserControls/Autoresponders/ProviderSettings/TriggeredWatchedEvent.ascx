﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TriggeredWatchedEvent.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.TriggeredWatchedEvent" %>
<%@ Register TagPrefix="we" TagName="WatchedEvents" Src="~/UserControls/Autoresponders/Watches.ascx" %>
<script type="text/javascript">
    function providerOptionValidation() {
        return ValidateCheckBoxList();
    }
</script>
<%--<div class="form-row">
    <label class="form-label">
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, WatchedEvents %>" /></label>
</div>--%>
<we:WatchedEvents ID="ctlWatchedEvents" runat="server" IsResponse="true" />
