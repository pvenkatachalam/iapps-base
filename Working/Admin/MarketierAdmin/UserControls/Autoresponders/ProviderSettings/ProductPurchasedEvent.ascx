﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductPurchasedEvent.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ProductPurchasedEvent" %>
<%@ Register TagPrefix="uc" TagName="ProductsPurchased" Src="~/UserControls/Autoresponders/ProductsSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductTypesPurchased" Src="~/UserControls/Autoresponders/ProductTypePurchased.ascx" %>

<script type="text/javascript">
    $(document).ready(function () {
        ToggleProductAndProductType();
    });

    function providerOptionValidation() {
        var returnValue = (validateProductPurchase() || ValidateProductTypePurchased());
        if (!returnValue) {
            alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseselectatleastoneproductorproducttype %>'/>");
        }
        return returnValue;
    }

    function ToggleProductAndProductType() {
        var rblProductSearch = document.getElementById("<%= rblProductSearch.ClientID %>");
        var radioButtons = rblProductSearch.getElementsByTagName("input");
        var selectedValue;
        if (radioButtons != null) {
            for (var i = 0; i < radioButtons.length; i++) {
                if (radioButtons[i].checked) {
                    selectedValue = radioButtons[i].value;
                }
            }
        }

        if (selectedValue != null) {
            var productSearchByProduct = document.getElementById("productSearchByProduct");
            var productSearchByProductType = document.getElementById("productSearchByProductType");
            switch (selectedValue) {
                case "product":
                    productSearchByProduct.style.display = "block";
                    productSearchByProductType.style.display = "none";
                    break;
                case "productType":
                    productSearchByProduct.style.display = "none";
                    productSearchByProductType.style.display = "block";
                    break;
            }
        }
    }
</script>
<div class="products-purchased">
    <asp:RadioButtonList ID="rblProductSearch" runat="server" RepeatLayout="UnorderedList" onclick="ToggleProductAndProductType();" CssClass="product-options clear-fix">
        <asp:ListItem Value="product" Text="<%$ Resources:GUIStrings, SpecificProducts %>" Selected="True"></asp:ListItem>
        <asp:ListItem Value="productType" Text="<%$ Resources:GUIStrings, ProductTypes1 %>"></asp:ListItem>
    </asp:RadioButtonList>
    <div class="productIds clear-fix">
        <div id="productSearchByProduct" style="display: block;">
            <uc:ProductsPurchased ID="ctlProductsPurchased" runat="server" />
        </div>
        <div id="productSearchByProductType" style="display: none;">
            <uc:ProductTypesPurchased ID="ctlProductTypesPurchased" runat="server" />
        </div>
    </div>
</div>
