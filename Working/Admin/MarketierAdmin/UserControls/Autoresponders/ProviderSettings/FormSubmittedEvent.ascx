﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormSubmittedEvent.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.FormSubmittedEvent" %>
<%@ Register TagPrefix="we" TagName="SubmittedForm" Src="~/UserControls/Autoresponders/FormSubmitted.ascx" %>
<script type="text/javascript">
    function providerOptionValidation() {
        return ValidateCheckBoxList();
    }
</script> 
<%--<div class="form-row">
    <label class="form-label"><asp:localize runat="server" text="<%$ Resources:GUIStrings, FormSubmission1 %>"/></label>
</div>--%>
<we:SubmittedForm ID="ctlFormSubmission" runat="server" IsResponse="true" />