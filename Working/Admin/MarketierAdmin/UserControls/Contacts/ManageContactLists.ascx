﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageContactLists.ascx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.Controls.ManageContactLists" %>

<%@ Register TagPrefix="UC" TagName="GroupSelector" Src="~/UserControls/General/GroupSelector.ascx" %>

<script type="text/javascript">
    function upContactLists_OnCallbackComplete(sender, eventArgs) {
        $("#contact_list").gridActions("displayMessage");
    }

    function upContactLists_OnRenderComplete(sender, eventArgs) {
        $("#contact_list").gridActions("bindControls");
        $("#contact_list").iAppsSplitter();
    }

    function upContactLists_OnLoad(sender, eventArgs) {
        $("#contact_list").gridActions({
            objList: $(".collection-table"),
            type: "list",
            onCallback: function (sender, eventArgs) {
                upContactLists_Callback(sender, eventArgs);
            },
            onItemCheckChange: function (sender, eventArgs) {
                upContactLists_CheckChange(sender, eventArgs);
            }
        });
    }

    function upContactLists_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        var selectedTitle = "";

        if (eventArgs && eventArgs.selectedItem)
            selectedTitle = eventArgs.selectedItem.getValue("Title");

        switch (gridActionsJson.Action) {
            case "AddToList":
                doCallback = false;
                break;
        }

        if (doCallback) {
            upContactLists.set_callbackParameter(JSON.stringify(gridActionsJson));
            upContactLists.callback();
        }
    }

    function upContactLists_CheckChange(sender, eventArgs) {
        if (typeof UpdateManualListItem == "function")
            UpdateManualListItem(eventArgs.Item, eventArgs.Selected);
    }

    function UnselectItem(id) {
        gridActionsJson.SelectedItems.remove(id);

        $("#contact_list").gridActions("bindControls", { forceBind: true });
    }

    function RefreshAttributes() {
        $("#contact_list").gridActions("callback", { refreshTree: true });
    }

    function LoadItemsGrid() {
        $("#contact_list").gridActions("selectNode", treeActionsJson.FolderId);
    }

</script>

<div id="contact_list" class="clear-fix">
    <div class="left-control">
        <UC:GroupSelector ID="contactGroups" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upContactLists" runat="server" OnClientCallbackComplete="upContactLists_OnCallbackComplete"
            OnClientRenderComplete="upContactLists_OnRenderComplete" OnClientLoad="upContactLists_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActions" runat="server" />
                <div class="grid-section">
                    <asp:ListView ID="lvContactLists" runat="server" ItemPlaceholderID="phContactList">
                        <EmptyDataTemplate>
                            <div class="empty-list">
                                No contact lists found!
                            </div>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div class="collection-table attribute-list fat-grid">
                                <asp:PlaceHolder ID="phContactList" runat="server" />
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <asp:PlaceHolder ID="DetailsTemplate" runat="server">
                                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                    <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id") %>'>
                                        <div class="first-cell">
                                            <%# Eval("RowNumber")%>
                                        </div>
                                        <div class="middle-cell">
                                            <h4>
                                                <asp:Literal ID="ltContactTitle" runat="server" />
                                                <asp:HiddenField ID="hdnContactListGroupId" runat="server" />
                                            </h4>
                                        </div>
                                        <div class="last-cell">
                                            <div class="child-row">
                                                <asp:Literal ID="ltContacts" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="PopupTemplate" runat="server">
                                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                    <div class="modal-grid-row grid-item clear-fix" objectid='<%# Eval("Id")%>'>
                                        <div class="first-cell">
                                            <input type="checkbox" id="chkSelected" runat="server" class="item-selector"
                                                visible="false" value='<%# Eval("Id") %>' />
                                            <h4 datafield="Title">
                                                <asp:Literal ID="ltPopupContactTitle" runat="server" />
                                            </h4>
                                            <asp:HiddenField ID="hdnPopupContactListGroupId" runat="server" />
                                        </div>
                                        <div class="last-cell">
                                            <div class="child-row">
                                                <h4>
                                                    <asp:Literal ID="ltPopupContacts" runat="server" />
                                                </h4>
                                                <input datafield="Contacts" type="hidden" id="hdnContacts" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</div>


