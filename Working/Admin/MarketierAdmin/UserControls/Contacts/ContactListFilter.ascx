﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactListFilter.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.ContactListFilter" %>
<style type="text/css">
    .ui-helper-hidden-accessible {
        display: none;
    }

    .ui-menu {
        background-color: #fff;
        border: solid 1px #d2d2d2;
        border-radius: 0 0 5px 5px;
        width: 500px;
    }

        .ui-menu .ui-menu-item {
            padding: 3px 10px;
            cursor: pointer;
        }

            .ui-menu .ui-menu-item:hover {
                background-color: #c9c9ca;
            }

            .ui-menu .ui-menu-item a:hover {
                text-decoration: none;
            }

    .form-value-dropdown, .form-value-box {
        float: left;
        width: 200px;
    }

    .header-text {
        margin-bottom: 10px;
    }

        .header-text > span {
            display: inline-block;
            margin-left: 40px;
        }

    .add-delete-section {
        margin-top: 10px;
        float: right;
    }

        .add-delete-section:first-of-type {
            margin-left: 10px;
        }

    .validationMessage {
        color: red;
        font-size: 25px;
    }

    .row-operators {
        float: right;
        margin: 5px 0;
    }

        .row-operators > a {
            border: 0;
            float: left;
            margin: 3px 0 0 10px;
        }

    .row {
        float: left;
        display: inline-block;
        margin-right: 20px;
        background: #fff;
        border-radius: 5px;
        padding: 3px;
        color: #999999;
    }

        .row > label {
            float: left;
            margin: 2px 0 0 10px;
            padding-top: 1px;
            min-width: 50px;
        }

        .row .value {
            float: left;
            margin-left: 5px;
        }

            .row .value > input[type=text] {
                float: left;
            }

            .row .value > input[type=text], .row .value > select {
                border: 0;
                background: none;
                width: 230px;
                color: #5e5858;
                font-weight: bolder;
            }

            .row .value > .dateSelector {
                width: 150px !important;
            }

            .row .value > input[type=text].bold-gray, .row .value > select.bold-gray {
                color: #5e5858;
                font-weight: bold;
            }

            .row .value > img {
                float: left;
                margin: 4px 10px;
            }

    .dateSelector-row {
    }

    .operators {
        float: left;
        margin: 3px 0 0 0;
    }

        .operators > a {
            display: inline-block;
            margin-right: 5px;
        }

    .checkbox-row {
        float: left;
        width: 23%;
        margin-right: 2%;
    }

        .checkbox-row input[type="checkbox"] {
            float: left;
            width: 13px;
            margin-top: 3px;
        }

        .checkbox-row span {
            float: left;
            width: 90%;
        }

    .invalid {
        border: 1px solid red !important;
    }

    .required-marker {
        color: red;
        vertical-align: middle;
        float: left;
    }

    .repeaterItem-Container {
        background: #DADADA;
        padding: 8px 13px;
        border-radius: 5px;
    }

    div.contact-lists-listing .or-container {
        margin-bottom: 0px;
    }
</style>

<script type="text/javascript">

    function getQueryStrings() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    var queryStrings = getQueryStrings();

    var debugMode = queryStrings['debugMode'] == 'true' ? true : false;

    var popupItem = function () {
        var self = this;
        self.Value = ko.observable();
        self.DisplayTitle = ko.observable();
        self.removeRow = function (control, parent) {
            alert('remove!' + control);
            parent.remove(control);
        }
        self.ID = ko.observable();
    }

    var Control = function () {
        var self = this;
        self.ID = ko.observable();
        self.Type = ko.observable();
        self.DataType = ko.observable();
        self.Category = ko.observable();
        self.ClassName = ko.observable();
        self.AssemblyName = ko.observable();
        self.IsRequired = ko.observable();
        self.Options = ko.observableArray();
        self.SelectedOption = ko.observable().extend({ notify: 'always' });
        self.Children = ko.observableArray();
        self.Parent = ko.observable();
        self.SelectedValues = ko.observableArray().extend({ notify: 'always' });
        self.OptionType = ko.observable();
        self.Value = ko.observable().extend({ notify: 'always' });
        self.Label = ko.observable();
        self.Validators = ko.observableArray();
        self.Level = ko.observable();
        self.Text = ko.observable();
        self.IsValid = ko.observable(false).extend({ notify: 'always' });
        self.Validators = ko.observableArray();
        self.ErrorMessage = ko.observable();
        self.ValidationSummary = ko.observableArray();
        self.allSelected = ko.observable(false);
        self.ValidationSummary.subscribe(function () {
            //console.log("Validation Summary now has " + self.ValidationSummary().length);

        });

        /*self.SelectedValues.subscribe(*/ko.computed(function () {
            if (self.Type() != undefined && self.Type().indexOf('checkbox') >= 0 || self.Type() == 'search') {
                //console.log("stepping into SelectedValues Validation for control " + self.ID());
                if (self.SelectedValues().length > 0) {
                    self.IsValid(true);
                    var temp = findParentContactFilter(self);
                    if (temp != undefined) {
                        temp.ValidationSummary.removeAll();
                    }
                }
                else if ((self.Options() != undefined && self.Options().length > 0) || self.Type().indexOf('checkboxTree') >= 0) {

                    if (self.Type() != 'checkboxTreeItem') {
                        self.ErrorMessage('* Required. Please select at least 1 value.');
                        self.IsValid(false);
                        var temp = findParentContactFilter(self);
                        if (temp != undefined && temp.ValidationSummary.indexOf(self.ErrorMessage()) < 0 && self.ErrorMessage() != '') {
                            temp.ValidationSummary.push('* Required. Please select at least 1 value.');
                        }
                    }

                } else {
                    if (self.ErrorMessage() != undefined && self.ErrorMessage() != "") {

                    }
                    else {
                        self.ErrorMessage('No options have been defined.');
                    }

                    self.IsValid(false);
                }
            }
            //console.log("stepping out of SelectedValues Validation for control " + self.ID());
            //console.log(self.ID() + "IsValid is now " + self.IsValid());
        }).extend({ rateLimit: 0 });

        self.SelectedValues.subscribe(function () {
            if (self.SelectedValues().length > 0) {
                var temp = '';

                for (var i = 0; i < self.SelectedValues().length; i++) {
                    temp += self.SelectedValues()[i] + ';';
                }
            }
        });

        ko.computed(function () {
            self.Children.removeAll();
            if (self.SelectedOption() != undefined) {

                GetNextControl(self, self.SelectedOption().ID);
                //if (self.Parent() != undefined && self.Type() == 'searchbox') {
                //    self.Parent().addFilterPart();
                //}
            }

        });

        ko.computed(function () {
            if (self.Type() != undefined && (self.Type() == 'dropdown' || self.Type() == 'searchbox')) {
                //console.log("stepping into SelectedOption Validation for control " + self.ID());
                if (!self.IsRequired() || self.SelectedOption() != undefined) {
                    self.IsValid(true);
                    var temp = findParentContactFilter(self);
                    if (temp != undefined) {
                        temp.ValidationSummary.removeAll();
                    }
                }
                else {
                    self.IsValid(false);
                    self.ErrorMessage('* Required.');
                    var temp = findParentContactFilter(self);
                    if (temp != undefined && temp.ValidationSummary.indexOf(self.ErrorMessage()) < 0) {
                        temp.ValidationSummary.push('* Required.');
                    }
                }
            }
            //console.log("stepping out of SelectedOption Validation for control " + self.ID());
            //console.log(self.ID() + "IsValid is now " + self.IsValid());
        }).extend({ rateLimit: 0 });

        /*self.Value.subscribe(*/ko.computed(function () {
            if (self.Type() != undefined && (self.Type() == 'date' || self.Type() == 'text')) {
                //console.log("stepping into Value Validation for control " + self.ID());

                var valid = true;
                var errorMessage = undefined;

                if (self.Value() == '' || self.Value() == undefined) {

                    //do nothing error message is already blank and valid is true, but skip rest of the checks
                    if (!self.IsRequired()) {

                    }
                    else {
                        valid = false;
                        errorMessage = '* Required.';
                    }
                }

                    //the value isn't empty we need to run through all the custom validators regardless if its required or not
                else {
                    //iterate through custom validators if any validators
                    for (var i = 0; self.Validators() != undefined && i < self.Validators().length && valid; i++) {
                        switch (self.Validators()[i].Type) {
                            case 'DataType':
                                if (self.Validators()[i].DataType == 'number') {
                                    valid = (parseFloat(self.Value()) == parseInt(self.Value())) && !isNaN(Number(self.Value()));
                                }
                                break;
                            case 'Compare':
                                if (self.Validators()[i].ValueToCompare != null && self.Validators()[i].ValueToCompare != undefined)
                                    valid = Number(self.Value()) != undefined && Number(self.Value()) >= Number(self.Validators()[i].ValueToCompare);
                                else if (self.Validators()[i].Operator == 'greaterThanEqual')
                                    if (self.Parent() != undefined) {
                                        for (var j = 0; i < self.Parent().Children().length; j++)
                                            if (self.Parent().Children()[j].ID() == self.Validators()[i].ControlToCompare) {
                                                valid = Number(self.Parent().Children()[j].Value()) <= Number(self.Value());
                                                break;
                                            }
                                    }
                                    else {
                                        valid = false;
                                    }
                                //else if (self.Validators()[i].Operator == 'lessThanEqual') {
                                //    for (var j = 0; i < self.Parent().Children().length; j++)
                                //        if (self.Parent().Children()[j].ID() == self.Validators()[i].ControlToCompare) {
                                //            if (!self.Parent().Children()[j].IsValid()) {
                                //                self.Parent().Children()[j].Value(self.Parent().Children()[j].Value());
                                //            }

                                //            break;
                                //        }
                                //}
                                break;
                            default:

                        }

                        if (!valid) {
                            errorMessage = self.Validators()[i].ErrorMessage;
                        }

                    }

                }

                self.ErrorMessage(errorMessage);
                self.IsValid(valid);

                if (!self.IsValid()) {
                    //errorMessage = self.Validators()[i].ErrorMessage;
                    var temp = findParentContactFilter(self);
                    if (temp != undefined && temp.ValidationSummary.indexOf(self.ErrorMessage()) < 0 && self.ErrorMessage() != undefined) {
                        temp.ValidationSummary.push(errorMessage);
                    }
                }
                else {
                    var temp = findParentContactFilter(self);
                    if (temp != undefined) {
                        temp.ValidationSummary.removeAll();
                    }
                }

                //console.log("stepping out of Value Validation for control " + self.ID());
                //console.log(self.ID() + "IsValid is now " + self.IsValid());
            }
        }).extend({ rateLimit: 0 });

        self.ShowSearchPopup = function (event) {
            context = this;

            if (context.ID() == "formSearch") {
                SearchForm(self.SelectedValues());
            } else {
                var queryString = "showSKUs=true";
                if (context.DataType == "product") {
                    queryString = "showProducts=true";
                }
                SearchProduct();
            }
        };

        self.getChildren = function (event) {
            if (event.SelectedOption() != undefined) {
                context = this;
                GetNextControl(context, context.SelectedOption().ID);

            }
        };

        self.addRow = function (control) {
            var tChild = control.Parent().Children().slice()[0];

            var temp = ko.toJS(tChild);
            var saveJson = JSON2.stringify(temp, replacer);

            var newChild = JSON.parse(saveJson);
            newChild.Children[0].SelectedOption = undefined;
            newChild.Children[0].Children = undefined;
            var control = LoadControl(newChild, control.Parent());
            control.Parent().Children.push(control);
        };

        self.removeRow = function (control) {

            if (control.Parent().Children().length > 1) {
                //validationsummary behaves weird when there is an validation error and you remove the repeater item who has the child that is causing the error
                //to avoid this, i am setting all children to not required before removing the repeater item this should clear all validation errors
                //on the repeater item that will be removed.
                hackChildren(control);
                control.Parent().Children.remove(control);
            }
            else {
                //The first child of the first repeater item is the dropdown we want to reset.
                self.Children()[0].SelectedOption(undefined);
                //alert('You cannot delete all of them');
            }
        };
        self.removeSelectedValues = function (control) {

            self.SelectedValues.remove(function (item) { return item == control.Value() });
        }
        self.selectAll = function () {
            var all = this.allSelected();
            var sel = this.SelectedValues();
            if (all) {
                this.SelectedValues([]);
                return true;
            }
            else {
                ko.utils.arrayForEach(this.Options(), function (option) {
                    sel.push(option.ID);
                });
            }
            this.SelectedValues(sel);
            return true;
        }

        //self.removeRow = function (control) {
        //    var repeaterItem = getRepeaterItem(control);

        //    if (repeaterItem != null && repeaterItem != undefined) {
        //        repeaterItem.Parent().Children.remove(repeaterItem);
        //    }
        //}
    }

    function hackChildren(control) {
        control.IsRequired(false);
        if (control.Children() != undefined) {
            $.each(control.Children(), function (key, value) {
                hackChildren(value);
            });
        }

    }

    function findParentContactFilter(Control) {
        if (Control == undefined) {
            return undefined;
        }

        if (Control.ID() == 'ddContactFilter') {
            return Control;
        }
        else {
            return findParentContactFilter(Control.Parent());
        }
    }

    function findCheckboxListTreeParent(control) {
        if (control.Type() == 'checkboxTree') {
            return control;
        }
        else {
            return findCheckboxListTreeParent(control.Parent());
        }
    }

    function getRepeaterItem(control) {
        if (control.ID().indexOf("RepeaterItem")) {
            return control;
        }
        else {
            return getRepeaterItem(control.Parent());
        }
    }

    function getTopControl(item) {
        if (!("Parent" in item) || item.Parent() == undefined)
            return item;
        else
            return getTopControl(item.Parent());
    }

    function getPopupItems(values, dataType) {
        var popupArray = new Array();
        for (index = 0; index < values.length; ++index) {

            if (values[index] != "") {
                var item = new popupItem();
                item.DisplayTitle("Loading...");
                item.ID = values[index];
                GetItemName(values[index], item, dataType);
                item.Value(values[index]);
                popupArray[index] = item;
            }
        }
        return popupArray;
    }

    function GetItemName(formid, item, dataType) {
        var functionName = dataType == "form" ? "GetFormName" : "GetProductName";
        if (functionName == "GetProductName") {
            item.DisplayTitle(formid.split("~")[1]);

        } else {
            searchRequest = $.ajax({
                type: 'Post',
                url: 'ManageContactFilters.aspx/' + functionName,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                data: JSON.stringify({ formId: formid }),
                error: function (jqXHR, textStatus, errorThrown) {
                    //console.log(errorThrown);
                }
            }).done(function (msg) {
                item.DisplayTitle(msg.d);
            });
        }
    }

    function validateValues(items) {
        try {
            for (var i = 0; i < items.length; i++) {

                var valid = items[i].IsValid();

                if (!valid && items[i].Type() == 'label') {
                    valid = true;
                }

                if (!valid) {
                    errors(errors + 1);
                }
                else if (items[i].Children().length > 0) {
                    validateValues(items[i].Children());
                }
            }
        } catch (err) {

        }

    }

    function LoadControl(data, parent) {
        var control = new Control();
        //if (data.ID == 'chkMyContacts') {
        //
        //}
        control.ID(data.ID);
        control.Type(data.Type);
        control.DataType(data.DataType);
        control.Category(data.Category);
        control.ClassName(data.ClassName);
        control.AssemblyName(data.AssemblyName);
        control.IsRequired(data.IsRequired ? data.IsRequired : false);
        control.Options(data.Options);
        control.OptionType(data.OptionType);
        control.Value(data.Value);
        control.Text(data.Text);
        control.Label(data.Label);
        control.Parent(parent);
        control.Level(data.Level);
        control.ErrorMessage(data.ErrorMessage);
        control.Validators(data.Validators);

        if (!(control.Type() == 'text' || control.Type() == 'dropdown' || control.Type().indexOf('checkbox') >= 0 && control.Type() != 'checkboxTreeItem' || control.Type() == 'date' || control.Type() == 'search')) {
            control.IsValid(true);
        }

        if (control.Type() == 'searchbox') {
            for (var i = 0 ; i < control.Parent().selections().length; i++) {
                var index = -1;

                for (var j = 0; j < control.Options().length && index == -1; j++) {
                    if (control.Options()[j].ID == control.Parent().selections()[i].ID) {
                        index = j;
                    }
                }

                var temp = control.Options.splice(index, 1);
                ////console.log('removed element: ' + temp);
            }
        }

        //if (control.Category() == 'Dynamic') {
        //    if (control.Type() == 'checkboxTree') {
        //        var temp = eval(control.ID());
        //        data.Children = temp;
        //    }
        //    else {
        //    //    control.Options(eval(control.ID()));
        //    }
        //}

        if (control.Options() != undefined && data.SelectedOption != undefined && data.SelectedOption != null && data.SelectedOption != '') {
            ko.utils.arrayFirst(control.Options(), function (option) {
                if (option.ID == data.SelectedOption.ID)
                    control.SelectedOption(option);
            });
        }

        if (data.SelectedValues != undefined && data.SelectedValues.length > 0) {
            control.SelectedValues(data.SelectedValues);
        }

        if (control.OptionType() == 'Header' || control.OptionType() == 'RepeaterItem') {
            if (data.Children == undefined || data.Children.length == 0) {
                GetNextControl(control, control.ID());
            }
            //else {
            //    loadChildren(data.Children, control);
            //}
        }

        //Load repeater children
        if (data.Children != undefined && data.Children.length > 0) {
            loadChildren(data.Children, control);
        }

        if (control.Options() != undefined) {
            control.Options.sortByProperty('Text');
        }

        return control;

    }

    function loadChildren(array, control) {
        var tArray = new Array();
        if (array != undefined) {
            $.each(array, function (index, val) {
                var tParent = control;
                if (control.Type() == 'checkboxTreeItem') {
                    tParent = findCheckboxListTreeParent(control);
                }
                var tControl = LoadControl(val, tParent);
                tArray.push(tControl);
            });
            control.Children(tArray);
        }
    }

    var productSearchPopup;
    var strSelectedProductIds = "";

    function SearchProduct() {
        //var popupPath = jmarketierAdminURL + "/Popups/ProductSearch.aspx?showSKUs=false";
        //var popupPath = jCommerceAdminSiteUrl + '/Popups/StoreManager/AddProductSkuPopup.aspx?ShowProducts=true';
        //ShowProductSearchPopup(popupPath);
        var queryString = "showProducts=true";
        strSelectedProductIds = "";

        if (context.SelectedValues().length > 0) {

            for (var i = 0 ; i < context.SelectedValues().length; i++) {
                strSelectedProductIds += context.SelectedValues()[i].split('~')[0] + ',';
            }

            strSelectedProductIds = strSelectedProductIds.replace(/,+$/, "");
        }

        productSearchPopup = OpeniAppsCommercePopup("AddProductPopup", queryString);
    }

    var formPopup;

    function SearchForm(selectedValues) {

        var formIds = selectedValues.join(":");
        formPopup = OpeniAppsAdminPopup("SelectManualListPopup", "ObjectTypeId=<%=  Bridgeline.FW.Global.ObjectType.iAppsForm%>&ObjectIds=" + formIds, "SelectForm");
    }

    function SelectForm() {
        var ids = "";
        var existingSelection = context.Value();
        var selectedItems = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);
        for (k = 0; selectedItems.length; k++) {
            var action = selectedItems[k];
            if (action == undefined)
                break;
            ids = ids + action.Id + ',';
        }
        ids = ids.replace(/,+$/, "");
        if (ids != "" && context != undefined && context != null) {
            context.SelectedValues(ids.split(','));

        } else {
            context.SelectedValues.removeAll();
        }
    }

    function CloseProductPopup(arrSelected) {
        if (context != null) {
            if (arrSelected) {
                var selectedArray;
                var ids = "";
                //strSelectedProductIds = "";
                var existingSelection = context.Value();
                for (k = 0; arrSelected.length; k++) {
                    selectedArray = arrSelected.pop();

                    ids = ids + selectedArray[0] + "~" + selectedArray[2] + ',';
                    //strSelectedProductIds += selectedArray[0] + ',';

                }
                ids = ids.replace(/,+$/, "");
                //strSelectedProductIds = strSelectedProductIds.replace(/,+$/, "");

                if (ids != "" && context != undefined && context != null) {
                    context.SelectedValues(ids.split(','));

                } else {
                    context.SelectedValues.removeAll();
                }
            }

        }
        productSearchPopup.hide();
    }

    function GetAvailableOptions(parentControl, controlId) {
        ///TODO: do something
    }
    var loadControlsFromServer = true;
    function GetNextControl(parentControl, optionId) {
        if (loadControlsFromServer) {
            var isDynamic = parentControl.ClassName() != "" ? true : false;
            $.ajaxSetup({ 'cache': false });

            var dynamicDataId = isDynamic ? parentControl.Category() : "";
            var dynamicClass = parentControl.ClassName() != undefined ? parentControl.ClassName() : "";
            var dynamicAssembly = parentControl.AssemblyName() != undefined ? parentControl.AssemblyName() : "";
            var parentOptionId = optionId;

            $.get("/marketieradmin/api/rule/getcontrols?dynamicClass=" + dynamicClass + "&parentOptionId=" + parentOptionId + "&dynamicDataId=" + dynamicDataId + "&dynamicAssembly=" + dynamicAssembly, function (data) {
                if (data) {
                    parentControl.Children.removeAll();
                    $.each(data, function (index, val) {
                        var tControl = LoadControl(val, parentControl);
                        if (parentControl.Type() == 'checkboxTree') {

                        }
                        parentControl.Children.push(tControl);
                    });
                    setupDateControls();
                }
            });
        }

    }

    function setupDateControls() {
        $(".dateSelector").datepicker({
            showOn: "both",
            buttonImage: "/marketieradmin/app_themes/general/images/calendar-button.png",
            buttonImageOnly: true,
            buttonText: "Select date"
        });
    }

    function GetDefaultControlData(controlArray, parent) {
        $.ajaxSetup({ 'cache': false });
        $.get('/marketieradmin/api/rule/getcontrol?ControlId=ddContactFilter', function (data) {
            if (data) {
                var tControl = LoadControl(data, parent);
                controlArray.push(tControl);
            }

        });
    }

    function transverseTree(control, parent) {
        if (control.IsRequired() && !control.IsValid() || (!control.IsRequired() && !control.IsValid())) {
            if (parent.errors.indexOf(control) < 0) {
                parent.errors.push(control);
            }
        }

        ko.utils.arrayForEach(control.Children(), function (control, errors) {
            transverseTree(control, parent);
        });
    }

    function FilterVM() {
        var self = this;
        self.filterParts = ko.observableArray([]);
        self.condition = "and";
        self.selections = ko.computed(function () {
            var temp = new Array();

            for (var i = 0; i < self.filterParts().length; i++) {
                if (self.filterParts()[i].SelectedOption() != undefined) {
                    temp.push(self.filterParts()[i].SelectedOption())
                }
            }

            return temp;
        });

        self.errors = ko.observableArray();

        ko.computed(function () {
            self.errors([]);
            ko.utils.arrayForEach(self.filterParts(), function (control) {
                try {
                    transverseTree(control, self)
                } catch (e) {
                    ////console.log(e.message);
                }
            });

        });

        ko.computed(function () {
            if (self.errors().length === 0 && self.filterParts().length > 0) {
                self.save();
                loadSearchObject();
            }
            else {
                var hdnValue = $("input[id$='hdnContactListFilterData']");
                hdnValue.val(undefined);
                LoadSearchResults(undefined);

            }
        }).extend({ throttle: 800 });

        self.selections.subscribe(function () {
            ////console.log('Selections');
            ////console.log(JSON.stringify(self.selections()));
        });

        self.addFilterPart = function () {
            if (loadControlsFromServer) {
                GetDefaultControlData(self.filterParts, self);
            }
        };

        self.removeFilterPart = function () {
            if (self.filterParts().length > 1) {
                //self.filterParts()
                if (this.SelectedOption() != undefined) {
                    self.filterParts()[self.filterParts().length - 1].Options.push(this.SelectedOption());
                    self.filterParts()[self.filterParts().length - 1].Options.sortByProperty('Text');
                }

                self.filterParts.remove(this);
            }
            else {
                self.filterParts()[0].SelectedOption(undefined);
            }

            this.ValidationSummary = ko.observableArray();
        }

        self.save = function () {
            try {
                //errors = 0;
                // check if there are any errors before we save
                //if (self.containers().length > 0) {
                //    $.each(self.containers(), function (index1, val1) {
                //        $.each(val1.ruleItems(), function (index2, val2) {
                //            validateValues(val2.Children());
                //        });
                //    });
                //}

                if (self.errors().length == 0) {
                    var hdnValue = $("input[id$='hdnContactListFilterData']");
                    var temp = ko.toJS(self);
                    var saveJson = JSON2.stringify(temp, replacer); //ko.toJSON(self, replacer); //
                    ////console.log("Saved JSON");
                    ////console.log(saveJson);
                    hdnValue.val(saveJson);
                    return true;
                }

                ShowiAppsNotification(self.errors()[0].ErrorMessage(), false, true);
                return false;
            } catch (err) {
                return false;
            }
        };
    }

    var searchInProgress = false;
    var searchRequest;
    var contactListId = $("input[id$='hdnContactListId']").val();

    function loadSearchObject() {
        if (searchInProgress && searchRequest != undefined) {
            searchRequest.abort();
        }
        contactListId = $("input[id$='hdnContactListId']").val();
        if (mrktStaticListSearchObject == undefined || isPostback) {
            isPostback = true;
            var hdnValue = $("input[id$='hdnContactListFilterData']");
            if (hdnValue.val() != undefined && hdnValue.val() != '') {
                searchInProgress = true;
                searchRequest = $.ajax({
                    type: 'Post',
                    url: 'ManageContactFilters.aspx/GetSearchObjectJSON',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ jsonData: hdnValue.val(), maxRecords: 30, ContactListId: contactListId }),
                    error: function (jqXHR, textStatus, errorThrown) {
                        ////console.log(errorThrown);
                    }
                }).done(function (msg) {
                    searchInProgress = false;
                    ////console.log('Search Object');
                    ////console.log(msg.d)
                    searchContacts(msg.d);
                });
            }
        }
        else {
            searchContacts(JSON2.stringify(mrktStaticListSearchObject));
            isPostback = true;
        }

    }

    function searchContacts(requestJson) {
        $("#previewLoading").show();
        $(".hideOnLoad").hide();
        var endpoint = '/marketieradmin/api/contact/search';
        var jsonData = requestJson;

        ////console.log('requestJson');
        ////console.log(requestJson);

        ////console.log('jsonData');
        ////console.log(jsonData);

        $.ajax({
            type: 'Post',
            url: endpoint,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: requestJson,
            error: function (jqXHR, textStatus, errorThrown) {
                ////console.log(errorThrown);
            }

        }).done(function (msg) {
            ////console.log('Contact Results');
            ////console.log(msg);
            LoadSearchResults(msg);
            $("#previewLoading").hide();
            $(".hideOnLoad").show();
        });

    }

    var filter;
    $(document).ready(function () {
        var hdnValue = $("input[id$='hdnContactListFilterData']").val();

        if (hdnValue != undefined && hdnValue == "" && mrktStaticListSearchObject != undefined) {
            loadSearchObject();
        }
        else
            if (hdnValue == undefined || hdnValue == "" || mrktStaticListSearchObject != undefined) {
                contactListId = $("input[id$='hdnContactListId']").val();
                if (contactListId == undefined || contactListId == "<%= Guid.Empty.ToString()%>") {
                    filter = new FilterVM();
                    filter.addFilterPart();
                    ko.cleanNode(koFilter);
                    ko.applyBindings(filter, koFilter);
                }
            }

        BindContactFilter();

        var checkedInputs = $('.treeNodeItem > input:checked');
        //var temp3 = temp.parentsUntil(".rootNode");

        $.each(checkedInputs, function (key, value) {
            var parent = findRootNodes(value);
            var temp = parent.find(".treeNodeItem:hidden").toggle();
        });

        //temp3.toggle();
    });

    function findRootNodes(object) {
        if ($(object).parent().hasClass("rootNode")) {
            return $(object).parent();
        }
        else {
            return findRootNodes($(object).parent());
        }

    }

    function resetFilters() {
        filter = new FilterVM();

        ko.cleanNode(koFilter);
        ko.applyBindings(filter, koFilter);
    }

    function toggleTree(item) {
        var toggleImage = $(item);
        if (toggleImage.attr("src") == "/marketieradmin/App_Themes/General/images/dashminus.gif") {
            toggleImage.attr("src", "/marketieradmin/App_Themes/General/images/dashplus.gif");
        } else {
            toggleImage.attr("src", "/marketieradmin/App_Themes/General/images/dashminus.gif");
        }

        toggleImage.parent().children(".treeNodeItem").toggle();
    }

    function Save() {
        return filter.save();
    }

    function BindContactFilter() {

        var hdnValue = $("input[id$='hdnContactListFilterData']").val();

        if (hdnValue != undefined && hdnValue != "") {

            filter = new FilterVM();
            ko.cleanNode(koFilter);
            ko.applyBindings(filter, koFilter);

            loadControlsFromServer = false;

            var ruleData = null;
            var container = null;
            var containerArray = new Array();

            var hdnCol = JSON.parse(hdnValue);
            $.each(hdnCol.filterParts, function (index1, val1) {
                var tControl = LoadControl(val1, filter);
                filter.filterParts.push(tControl);
            });
        }

        var temp = ko.toJS(filter);
        var saveJson = JSON2.stringify(temp, replacer);
        var newJson = saveJson;

        ////console.log("Loaded JSON");
        ////console.log(newJson);

        ////console.log("Hidden Field JSON:")
        ////console.log(hdnValue);

        loadControlsFromServer = true;
        setupDateControls();

    }
</script>
<script id="mainTemplate" type="text/html">
    <!-- ko if: Type() == 'searchbox'-->
    <a class="add-delete-section" data-bind='event: { click: $root.addFilterPart }, visible: SelectedOption() != undefined && ($root.filterParts().length - 1 == $index())'>
        <img src="/marketieradmin/App_Themes/General/images/icon-plus-box.png" alt="add" />
    </a>
    <a class="add-delete-section" data-bind='event: { click: $root.removeFilterPart }, visible: true || (($parent.filterParts().length - 1) != $index()) || $index() > 0'>
        <img src="/marketieradmin/App_Themes/General/images/icon-minus-box.png" alt="remove" />
    </a>

    <div data-bind="visible: SelectedOption() == undefined /*$parent.filterParts().length - 1 == $index()*/">
        <div class="clear-fix">
            <h2><%= GUIStrings.FilterCriteria %></h2>
            <span class="required-marker" style="display: inline-block; margin: 8px 10px;" data-bind="text: ErrorMessage(), visible: !IsValid()"></span>
        </div>

        <div class="clear-fix horizontal-padding">
            <div class="search-block clear-fix">
                <input id="searchButton" type="image" src="/marketieradmin/App_Themes/General/images/icon-search.png" onclick="$(this).siblings('#searchbox').trigger('focus'); return false;" />
                <input id="searchbox" placeholder="Type the name of the criteria by which you want to filter your list." style="width: 500px;" onfocus="$(this).autocomplete('search');"
                    data-bind="jqAuto: { autoFocus: true }, jqAutoSource: Options, jqAutoValue: SelectedOption, jqAutoSourceLabel: 'Text', jqAutoSourceInputValue: 'Text'" />
            </div>
        </div>
    </div>
    <!-- /ko -->

    <%--<div class="horizontal-padding" data-bind="template: 'controlTemplate'">--%>

    <!-- ko if: SelectedOption() != undefined -->

    <div class="clear-fix">
        <h2 data-bind='text: SelectedOption().Text'></h2>

        <div style="display: inline-block; margin: 8px 10px;">
            <ul style="list-style: none;" data-bind='foreach: ValidationSummary'>
                <li style="font-size: 13px;" class="validationMessage" data-bind='text: $data'></li>
            </ul>
        </div>
    </div>

    <!-- /ko -->
    <div class="horizontal-padding">
        <!-- ko template: 'controlTemplate' -->
        <!-- /ko -->
    </div>
</script>

<script id="controlTemplate" type="text/html">
    <!-- ko if: Type() == 'checkbox' -->
    <div style="display: inline-block;">
        <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>
        <!--<span class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>-->
    </div>
    <div class="clear-fix" style="margin-top: 20px;">
        <div class="checkbox-row" style="width: 100%;">
            <input type="checkbox" data-bind="click: selectAll, checked: allSelected" />
            <span data-bind='text: "Select All " + (/s$/.test($parent.SelectedOption().Text) ? $parent.SelectedOption().Text : $parent.SelectedOption().Text + "s")' style="font-weight: bold;"></span>
        </div>
    </div>
    <div style="max-height: 400px; overflow: auto; margin-bottom: 20px;" data-bind='foreach: Options'>
        <div class="checkbox-row">
            <input type='checkbox' data-bind='value: ID, checked: $parent.SelectedValues' />
            <span data-bind='    text: Text'></span>
        </div>
    </div>
    <!-- /ko -->

    <!-- ko if: Type() == 'search' -->
    <div style="display: inline-block;">
        <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>
        <!--<span class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>-->
    </div>
    <div class="clear-fix">
        <a href="javascript: //" class="button dark" style="margin-bottom: 10px; float: left; margin-left: -45px; margin-right: 20px;" data-bind="text: Category, click: ShowSearchPopup" />
    </div>

    <div data-bind="template: { name: 'popupItems', foreach: getPopupItems(SelectedValues(), DataType()) }"></div>

    <!-- /ko -->

    <!-- ko if: Type() == 'text'  -->

    <div style="display: inline-block; float: left; margin-right: 20px;">
        <!--<span class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>-->
        <div class="row" style="width: 275px" data-bind="css: { invalid: !IsValid() }">
            <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>

            <label class="form-label" data-bind='text: Label'>
                <span class="req">* </span>
            </label>
            <div class="value">
                <span class="required-marker" data-bind="visible: IsRequired()">*</span>
                <input style="width: 150px;" type="text" data-bind="value: Value, valueUpdate: 'afterkeydown'/* , attr: { required: IsRequired }*/ ">
            </div>
        </div>
    </div>

    <!-- /ko -->

    <!-- ko if: Type() == 'date'  -->
    <div style="display: inline-block; float: left; margin-right: 20px;">
        <!--<span class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>-->
        <div class="row dateSelector-row" data-bind="css: { invalid: !IsValid() }">
            <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>

            <%--<label class="form-label" data-bind='text: Label'>
                <span class="req">*</span>
            </label>--%>
            <div class="value">
                <span class="required-marker" data-bind="visible: IsRequired()">*</span>
                <input type="text" class="dateSelector" data-bind="value: Value, valueUpdate: 'afterkeydown'/*, attr: { required: IsRequired }*/" readonly>
            </div>
        </div>
    </div>
    <!-- /ko -->

    <!-- ko if: Type() == 'dropdown'  -->
    <div style="display: inline-block; float: left; margin-right: 20px;">
        <!--<span class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>-->
        <div class="row" data-bind="css: { invalid: !IsValid() }">
            <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>

            <%--<label class="form-label" data-bind='text: Label'>
                <span class="req">*</span>
            </label>--%>
            <div class="value">
                <span class="required-marker" data-bind="visible: IsRequired()">*</span>
                <select class="form-value-dropdown" data-bind='options: Options, optionsText: "Text", value: SelectedOption, optionsCaption: Label/*, event: { change: getChildren }*/'></select>
            </div>
        </div>
    </div>
    <!-- /ko -->

    <!-- ko if: Type() == 'checkboxTree' -->
    <div style="display: inline-block;">
        <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>
        <!--<span class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>-->
    </div>
    <div style="max-height: 300px; overflow: auto;" data-bind="    template: { name: 'checkBoxListTemplate', foreach: Children }"></div>
    <!-- /ko -->

    <!-- ko if: Type() != 'repeater' && Type() != 'checkboxTree' -->
    <div data-bind="template: { name: 'controlTemplate', foreach: Children }"></div>
    <!-- /ko -->

    <!-- ko if: Type() == 'repeater' -->

    <!-- ko template: { name: 'repeaterItemTemplate', foreach: Children } -->
    <!-- /ko -->

    <!-- /ko -->
</script>

<script id="checkBoxListTemplate" type="text/html">
    <div style="padding-left: 10px; display: none;" class="treeNodeItem" data-bind="visible: Level() <= 1, css: { 'rootNode': Level() == 1 }">
        <!--<span data-bind="text: Level()"></span>-->
        <img onclick="toggleTree(this);" data-bind="    visible: Children().length > 0, attr: { src: Level() < 1 ? '/marketieradmin/App_Themes/General/images/dashminus.gif' : '/marketieradmin/App_Themes/General/images/dashplus.gif' }" />
        <div style="width: 19px; height: 15px; float: left; display: block;" data-bind="    visible: Children().length == 0" />
        <input type='checkbox' data-bind='value: ID(), visible: Level() > 0, checked: Parent().SelectedValues /*, click: $root.toggleAssociation*/' />
        <span data-bind='text: Label()'></span>
        <!-- ko template: { name: 'checkBoxListTemplate', foreach: Children } -->
        <!-- /ko -->
    </div>
</script>
<script id="popupItems" type="text/html">
    <div class="checkbox-row">
        <a data-bind='    event: { click: $parent.removeSelectedValues }'>
            <img src="/Admin/App_Themes/General/images/icon-delete-cross.png" alt="remove row" /></a>&nbsp;<span data-bind='html: DisplayTitle()'></span>
    </div>
</script>

<script id="repeaterItemTemplate" type="text/html">
    <!-- ko if: $index() > 0 -->
    <div class="or-container clear-fix">
        <div class="orange-bottom-border clear-fix">
            <span class="orange-bg-container">
                <span class="hide text-container">or</span>
            </span>
        </div>
    </div>
    <!-- /ko -->
    <div class="repeaterItem-Container">
        <span data-bind='text: "index " + ($index() + 0) + " of ", visible: debugMode'></span>
        <span data-bind='text: Parent().Children().length, visible: debugMode'></span>

        <!-- ko template: { name: 'repeaterControlTemplate', foreach: Children } -->
        <!-- /ko -->

        <div class="row-operators">
            <a data-bind='event: { click: removeRow }'>
                <img src="/marketieradmin/App_Themes/General/images/icon-minus-small-box.png" alt="remove row" />
            </a>
            <a data-bind='event: { click: addRow }'>
                <img src="/marketieradmin/App_Themes/General/images/icon-plus-small-box.png" alt="add row" />
            </a>
        </div>
        <div class="clear-fix">&nbsp;</div>
    </div>
</script>

<script id="repeaterControlTemplate" type="text/html">
    <!-- ko if: Category() == 'Header' -->
    <%--<div class="header-text clear-fix">
            <!-- ko foreach: Children -->
        <span data-bind='text: Label'></span>
            <!-- /ko -->
    </div>--%>
    <!-- /ko -->

    <!-- ko if: Type() == 'dropdown' -->
    <div style="display: inline-block; float: left;">
        <div class="row" data-bind="css: { invalid: !IsValid() }">
            <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>
            <div class="value">
                <span class="required-marker" data-bind="visible: IsRequired()">*</span>
                <!-- ko if: Category() == 'Dynamic' -->
                <select class="form-value-dropdown" data-bind='options: Options, optionsText: function (item) { return item.Text }, value: SelectedOption, optionsCaption: Label/*, event: { change: getChildren }*/'></select>
                <!-- /ko -->

                <!-- ko if: Category() != 'Dynamic' -->
                <select class="form-value-dropdown" data-bind='options: Options, optionsText: "Text", value: SelectedOption, optionsCaption: Label/*, event: { change: getChildren }*/'></select>
                <!-- /ko -->
            </div>
        </div>
        <%--<span style="float: left; margin-right: 5px; display: block;" class="validationMessage" data-bind="text: '*', visible: !IsValid()"></span>--%>
    </div>
    <!-- /ko -->

    <!-- ko if: Type() == 'text' -->
    <div style="display: inline-block; float: left;">
        <div class="row" data-bind="css: { invalid: !IsValid() }">
            <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>

            <div class="value">
                <span class="required-marker" data-bind="visible: IsRequired()">*</span>
                <input type="text" data-bind="    value: Value, valueUpdate: 'afterkeydown'/* , attr: { required: IsRequired }*/ " class="form-value-box">
            </div>
        </div>
        <%--<span class="validationMessage" data-bind="    text: '*', visible: !IsValid()"></span>--%>
    </div>
    <!-- /ko -->

    <!-- ko if: Type() == 'checkbox' -->
    <div style="display: inline-block; float: left;">
        <div style="width: 100px; float: left; display: inline-block; margin-right: 20px; padding: 3px; width: 253px; background-color: white; border-radius: 5px;">
            <span style="color: red" data-bind="text: 'IsValid= ' + IsValid(), visible: debugMode"></span>
            <div class="value">
                <ul data-bind='foreach: Options'>
                    <li>
                        <input type='checkbox' data-bind='value: ID, checked: $parent.SelectedValues' />
                        <span data-bind='text: Text'></span>
                    </li>
                </ul>
            </div>
        </div>
        <%--<span class="validationMessage" data-bind="    text: '*', visible: !IsValid()"></span>--%>
    </div>
    <%--<select multiple="multiple" class="form-value-dropdown" data-bind='options: Options, optionsText: "Text", value: SelectedValues/*, optionsCaption: "Select your filter Attributes"/*, event: { change: getChildren }*/'></select>--%>
    <!-- /ko -->

    <!-- ko if: Type() == 'date'  -->
    <div class="row dateSelector-row" data-bind="css: { invalid: !IsValid() }">
        <%--  <label class="form-label" data-bind='text: Label'>
            <span class="req">*</span>
        </label>--%>
        <div class="value">
            <span class="required-marker" data-bind="visible: IsRequired()">*</span>
            <input type="text" class="dateSelector" data-bind="value: Value, valueUpdate: 'afterkeydown'/*, attr: { required: IsRequired }*/" readonly>
        </div>
    </div>
    <!-- /ko -->

    <!-- ko template: { name: 'repeaterControlTemplate', foreach: Children } -->
    <!-- /ko -->
</script>

<asp:HiddenField ID="hdnContactListFilterData" runat="server" />
<asp:HiddenField ID="hdnContactListId" runat="server" />
<div id="koFilter">
    <div data-bind='visible: debugMode'>
        <span style="color: red; display: block;">DEBUG MODE IS ON</span>

        <span style="color: red;" data-bind='text: "Total Errors = " + errors().length'></span>
    </div>
    <%--<!-- ko if: errors().length > 0 -->
    <div>
        <span>Please address the following issues before you continue</span>
        <ul data-bind="foreach: errors">
            <li data-bind='text: ErrorMessage'></li>
        </ul>
    </div>
    <!-- /ko -->--%>

    <div data-bind="foreach: filterParts">

        <!-- ko if: $index() != 0 -->
        <p class="and-container">-&nbsp;And&nbsp;-</p>
        <!-- /ko -->
        <%--<div style="padding: 0 35px 20px;" class="clear-fix">
            <ul style="list-style: disc;" data-bind='foreach: ValidationSummary'>
                <li style="font-size: 13px;" class="validationMessage" data-bind='text: $data'></li>
            </ul>
        </div>--%>

        <div class="container-box clear-fix" data-bind="template: 'mainTemplate'">
        </div>
    </div>
</div>