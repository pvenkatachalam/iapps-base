﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefineDistributionLists.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.DefineDistributionLists" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>

<p><%= GUIStrings.InstructionsSelectthelisttoappendtoorcreateanewlistandassignindexterms %></p>
<div class="list-container">
    <h3><%= GUIStrings.SelectContactLists %></h3>
    <div class="form-row">
        <asp:RadioButton ID="rdbNewDistributionList" onclick="SetOption1(this);" runat="server" Font-Bold="true"
            Text="<%$ Resources:GUIStrings, CreateNewDistributionList %>" CssClass="radioButtonConatiner" GroupName="DistributionList"
            Checked="true" />
    </div>
    <div class="form-row padding-left">
        <label class="form-label"><%= GUIStrings.Title %></label>
        <div class="form-value">
            <asp:TextBox ID="txtDistributionListName" runat="server" Width="200" />
        </div>
    </div>
    <div class="form-row padding-left">
        <label class="form-label"><%= GUIStrings.ContactListGroups %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlDistributionGroup" runat="server" Width="210" DataTextField="Title" DataValueField="Id" />
            <asp:XmlDataSource ID="xmlDataSource" EnableCaching="false" runat="server"></asp:XmlDataSource>

        </div>
    </div>
    <div class="form-row">
        <asp:RadioButton ID="rdbAppendDistributionList" runat="server" Text="<%$ Resources:GUIStrings, AppendtoExistingDistributionList %>"
            CssClass="radioButtonConatiner" onclick="SetOption2(this);" GroupName="DistributionList" Font-Bold="true" />
    </div>
    <div class="form-row padding-left">
        <ComponentArt:TreeView ID="trvAppendGroup" ExpandSinglePath="true" SkinID="Default"
            AutoPostBackOnSelect="false" Height="293" Width="90%" CausesValidation="false"
            runat="server">
        </ComponentArt:TreeView>
    </div>
    <div class="form-row">
        <asp:RadioButton ID="rdbNoDistributionList" onclick="SetOption3(this);" runat="server" Font-Bold="true"
            Text="<%$ Resources:GUIStrings, DonotcreateNewDistributionList %>" GroupName="DistributionList" />
    </div>
</div>
<div class="tags-container">
    <h3><%= GUIStrings.AssignIndexTermstoImportContacts %></h3>
    <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="255"></UC:AssignTags>
</div>
<script type="text/javascript">
    var strList = '';
    function ValidateWizard(clickObj) {

        if (document.getElementById("<%=rdbNewDistributionList.ClientID%>").checked == true) {
            var titleTagStr = document.getElementById("<%=txtDistributionListName.ClientID%>").value;
            var distGroup = document.getElementById("<%=ddlDistributionGroup.ClientID%>").value;

            if (distGroup == null || distGroup == "") {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Thereisnodistributiongroupsavailabletocreatenewdistributionlist %>'/>");
                return false;
            }

            if (Trim(titleTagStr) == "") {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseenteraDistributionListname1 %>'/>");
                document.getElementById("<%=txtDistributionListName.ClientID%>").select();
                return false;
            }

            if (titleTagStr.match(/[\<\>&]/)) {
                alert("<%= JSMessages.InvalidcharactersinthedistributionlistnamePleaseremovetheandcharacters %>");
                document.getElementById("<%=txtDistributionListName.ClientID%>").select();
                return false;
            }

            var CheckPostName = IsDistributionAvailable(titleTagStr);
            if (CheckPostName == "true") {
                alert("<%= JSMessages.ThedistributionlistnamealreadyexistsPleaseenterauniquename1 %>");
                return false;
            }
        }

        if (document.getElementById("<%=rdbAppendDistributionList.ClientID%>").checked == true) {

            var testvar = window["trvAppendGroup"];
            var childNodeArray = testvar.Nodes();
            var checkGrpSelected = GetChildNodeText(childNodeArray);
            if (checkGrpSelected == '') {
                alert("<%= JSMessages.Pleaseselectanexistingdistributionlist %>");
                return false;
            }

        }
        termswin = dhtmlmodal.open('ElaspedTime', 'iframe', '../Popups/ImportElapsedTime.aspx', 'ElaspedTime', 'width=275px,height=220px,center=1,resize=1,scrolling=1');
        var btnback = $("input[type=submit][id*=btnBack]");
        btnback.attr('disabled', true);
        btnback.className += " disabled";
        var btnimport = $("input[type=submit][id*=btnContinue]");
        btnimport.attr('disabled', true);
        btnimport.className += " disabled";
        var param = getSeletedValue();
        SaveStep3(param);
        return false;
    }


    function SaveBackStepvalue(clickObj) {
        var param = getSeletedValue();
        SaveBackDistributionValue(param);
        return true;
    }

    function getSeletedValue() {
        var testvar = window["trvAppendGroup"];
        var childNodeArray = testvar.Nodes();
        var checkGrpSelected = GetChildNodeText(childNodeArray);
        var param = '';
        if (document.getElementById("<%=rdbNewDistributionList.ClientID%>").checked == true) {
            param = 'opt1|' + document.getElementById("<%=txtDistributionListName.ClientID%>").value + '|' + document.getElementById("<%=ddlDistributionGroup.ClientID%>").value;

        }
        else if (document.getElementById("<%=rdbAppendDistributionList.ClientID%>").checked == true) {
            param = 'opt2|' + checkGrpSelected + '|';
        }
        else
            param = 'opt3||';

        param += '|' + getIndexTerms();
        return param;
    }

    function getIndexTerms() {
        var _ids = "";
        var assignedTags = AssignTags_GetSelectedTags();
        if (assignedTags != "") {
            var arr = JSON.parse(assignedTags);
            for (i in arr) {
                if (arr[i].Id != undefined) {
                    _ids += arr[i].Id + ",";
                }
            }
        }

        return _ids;
    }
    function CloseElapsedPopup() {
        window.location.href = "../Contacts/ManageContacts.aspx";
        return false;
    }

    function GetChildNodeText(ChildNode) {

        if (ChildNode.length > 0) {
            for (var i = 0; i < ChildNode.length; i++) {
                if (ChildNode[i].Checked) {
                    strList = strList + ChildNode[i].ID + ";";
                }
                //*****************find child Node until not ChildNode 
                GetChildNodeText(ChildNode[i].Nodes());
            }
        }
        //************return 
        return strList;
    }

    function GetChildNodeName(ChildNode) {
        if (ChildNode.length > 0) {
            for (var i = 0; i < ChildNode.length; i++) {
                if (ChildNode[i].Checked) {
                    //strList = strList+"|"+ChildNode[i].Text; 
                    strList = strList + ChildNode[i].Text + ",";
                }
                //*****************find child Node until not ChildNode 
                GetChildNodeName(ChildNode[i].Nodes());
            }
        }
        //************return 
        return strList;
    }


    function SetOption1(curOpt) {
        document.getElementById("<%=ddlDistributionGroup.ClientID%>").disabled = false;
        document.getElementById("<%=txtDistributionListName.ClientID%>").disabled = false;

    }

    function SetOption2(curOpt) {

        document.getElementById("<%=ddlDistributionGroup.ClientID%>").disabled = true;
        document.getElementById("<%=txtDistributionListName.ClientID%>").disabled = true;


    }

    function SetOption3(curOpt) {

        document.getElementById("<%=ddlDistributionGroup.ClientID%>").disabled = true;
        document.getElementById("<%=txtDistributionListName.ClientID%>").disabled = true;
    }

    function Combo_Collapse(obj, e) {
        var testvar = window["trvAppendGroup"];
        var childNodeArray = testvar.Nodes();
        var checkGrpSelected = GetChildNodeName(childNodeArray);
        obj.set_text(checkGrpSelected);
        strList = "";
    }
</script>

