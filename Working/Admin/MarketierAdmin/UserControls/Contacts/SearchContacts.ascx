﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchContacts.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.SearchContacts" %>
<style>
    .grid, #gridHeader .resultRow, #gridBody .resultRow {
        width: 100%;
    }

    #gridHeader h3 {
        margin-left: 10px;
        color: white;
    }

    .column {
        width: 23%;
        display: inline-block;
    }

        .column:first-child {
            margin-right: 140px;
        }

        .column:first-child, .column:last-child {
            margin-left: 40px;
        }

        .column:first-child {
            width: 6%;
        }

        .column:last-child {
            width: 29%;
        }

    #grid div div {
        text-align: justify;
    }

    #gridBody .resultRow:nth-child(2n) {
        background-color: #ebebeb;
    }

    #gridBody .resultRow {
        padding-top: 5px;
    }

        #gridBody .resultRow:last-child {
            margin-bottom: 30px;
        }

        #gridBody .resultRow:first-child {
            padding-top: 0px;
        }

    #gridHeader .resultRow {
        margin-bottom: 5px;
        padding: 5px 0;
    }

    #gridHeader {
        background-color: #ebebeb;
    }

        #gridHeader .resultRow:last-child {
            font-size: 14px;
            border-bottom: 1px solid #ebebeb;
            background-color: #ebebeb;
        }

    .content {
        margin-top: 50px;
    }

    #gridHeader div.table-header {
        background-color: #5e5858;
        border-radius: 5px 5px 0 0;
        padding: 7px 20px;
    }

    #gridHeader div.table-header h2 {
        color: #ebebeb;
        float: left;
        font-size: 16px;
        font-weight: bold;
    }

    #noOfRecords {
        float: right;
        color: #ebebeb;
    }
</style>

<script type="text/javascript">

    var MarketierImage = "../App_Themes/General/images/orange-square-bullet.png";
    var CommerceImage = "../App_Themes/General/images/green-square-bullet.png";
    var CMSImage = "../App_Themes/General/images/blue-square-bullet.png";
    var SocialImage = "../App_Themes/General/images/turquoise-square-bullet.png";
    var AnalyzerImage = "../App_Themes/General/images/purple-square-bullet.png";
    var SixImage = "../App_Themes/General/images/red-square-bullet.png";
    var AdminImage = "../App_Themes/General/images/red-square-bullet.png";

    var ContactModel = function () {

        var self = this;
        self.Id = ko.observable();
        self.FirstName = ko.observable();
        self.LastName = ko.observable();
        self.Email = ko.observable();
        self.Product = ko.observable();
        self.ProductImageUrl = ko.observable();
        self.Status = ko.observable();

    }

    var SearchGridVM = function () {
        var self = this;

        self.Results = ko.observableArray();
        self.TotalNumberOfRecords = ko.observable();

    }
    var searchGridVM = new SearchGridVM();

    function LoadSearchResults(data) {

        searchGridVM.TotalNumberOfRecords(0);
        searchGridVM.Results([]);
        if (data != undefined) {
            searchGridVM.TotalNumberOfRecords(data.TotalRecords)
            for (var i = 0; i < data.Items.length; i++) {
                var resultItem = new ContactModel();
                resultItem.FirstName(data.Items[i].FirstName);
                resultItem.LastName(data.Items[i].LastName);
                resultItem.Email(data.Items[i].Email);
                resultItem.Product(data.Items[i].ContactSourceId);
                resultItem.Id(data.Items[i].Id);

                switch (data.Items[i].ContactSourceId) {
                    case 1: resultItem.ProductImageUrl(MarketierImage);
                        break;
                    case 2: resultItem.ProductImageUrl(MarketierImage);
                        break;
                    case 3: resultItem.ProductImageUrl(MarketierImage);
                        break;
                    case 4: resultItem.ProductImageUrl(MarketierImage);
                        break;
                    case 5: resultItem.ProductImageUrl(MarketierImage);
                        break;
                    case 6: resultItem.ProductImageUrl(MarketierImage);
                        break;
                    case 7: resultItem.ProductImageUrl(AdminImage);
                        break;
                    case 8: resultItem.ProductImageUrl(CMSImage);
                        break;
                    case 9: resultItem.ProductImageUrl(CommerceImage);
                        break;
                    default:
                        resultItem.ProductImageUrl(MarketierImage);
                        break;
                }

                resultItem.Status(data.Items[i].Status);
                searchGridVM.Results.push(resultItem);
            }
        }
    }

    $(document).ready(function () {
        ko.applyBindings(searchGridVM, grid);
    });
</script>

<div class="content">
    <div id="grid">
        <div id="gridHeader">
            <div class="clear-fix table-header">
                <h2>Preview Results </h2>
                <div id="noOfRecords">
                    <em data-bind="text: 'Displaying ' + Results().length + ' of ' + (TotalNumberOfRecords() >= Results().length ? TotalNumberOfRecords() : undefined) + ' total records.'"></em>
                </div>
            </div>
            <div class="resultRow clear-fix">
                <div class="column">
                    Product
                </div>
                <div class="column">
                    Last Name
                </div>
                <div class="column">
                    First Name
                </div>
                <div class="column">
                    Email Address
                </div>
            </div>
        </div>

        <div id="previewLoading" style="display: none;">
            <div id="gridBody">
                <div class="resultRow" style="text-align: center;">
                    <img src="/MarketierAdmin/App_Themes/General/images/spinner.gif" alt="Please wait...">
                </div>
            </div>
        </div>

        <!-- ko if: Results().length <= 0 -->
        <div id="gridBody" class="hideOnLoad">
            <div class="resultRow" id="noResults" style="text-align: center;">
                <em>There are no records to show</em>
            </div>
        </div>
        <!-- /ko -->
        <div id="gridBody" class="hideOnLoad" data-bind="foreach: Results">
            <div class="resultRow clear-fix">
                <div class="column">
                    <img data-bind="attr: { src: ProductImageUrl }" />
                    <span data-bind="text: Product, visible: debugMode"></span>
                </div>
                <div class="column">
                    <span data-bind="text: LastName"></span>
                </div>
                <div class="column">
                    <span data-bind="text: FirstName"></span>
                </div>
                <div class="column">
                    <span data-bind="text: Email"></span>
                </div>
            </div>
        </div>
    </div>
</div>
