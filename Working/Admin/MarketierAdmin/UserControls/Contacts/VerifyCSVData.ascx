﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VerifyCSVData.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.UserControls.Contacts.VerifyCSVData" %>

<h3><%= Bridgeline.iAPPS.Resources.GUIStrings.CSVVerificationSummary %></h3>
<p><em><strong><%= Bridgeline.iAPPS.Resources.GUIStrings.Instructions %>:&nbsp;</strong></em><%= Bridgeline.iAPPS.Resources.GUIStrings.DownloadErroredAndDuplicateContactsInstructions %></p>
<div class="import-overview clear-fix">
    <div class="boxes">
        <p class="count">
            <asp:Literal ID="litTotalRecords" runat="server" Text="0" /></p>
        <p class="text"><%= Bridgeline.iAPPS.Resources.GUIStrings.ContactsInCSV %></p>
    </div>
    <div class="boxes">
        <p class="count">
            <asp:Literal ID="litErrorFreeContacts" runat="server" Text="0" /></p>
        <p class="text"><%= Bridgeline.iAPPS.Resources.GUIStrings.ErrorFreeContactsInCSV %></p>
    </div>
    <div class="boxes">
        <p class="count">
            <asp:HyperLink ID="hplContactsFailed" runat="server" Text="0" Visible="false" />
            <asp:Literal ID="lit0ContactsFailed" runat="server" Text="0" Visible="true" />
        </p>
        <p class="text"><%= Bridgeline.iAPPS.Resources.GUIStrings.ContactsWithErrorInCSV %></p>
    </div>
    <div class="boxes">
        <p class="count">
            <asp:HyperLink ID="hplDuplicateContacts" runat="server" Text="0" Visible="false" />
            <asp:Literal ID="lit0DuplicateContacts" runat="server" Text="0" Visible="true" />
        </p>
        <p class="text"><%= Bridgeline.iAPPS.Resources.GUIStrings.DuplicateContactsInCSV %></p>
    </div>
</div>

<h3>
    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, SampleofyourRecords %>" /></h3>
<asp:ListView ID="lvPreviewContacts" runat="server" ItemPlaceholderID="itemPlaceHolder">
    <LayoutTemplate>
        <table class="grid" width="100%" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th><asp:Localize ID="locLastName" runat="server" Text="<%$ Resources:GUIStrings, LastName %>" /></th>
                    <th><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FirstName %>" /></th>
                    <th><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, EmailAddress %>" /></th>
                </tr>
            </thead>
            <tbody>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr class="odd-row">
            <td><%# Eval("LastName")%></td>
            <td><%# Eval("FirstName")%></td>
            <td><%# Eval("Email")%></td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="even-row">
            <td><%# Eval("LastName")%></td>
            <td><%# Eval("FirstName")%></td>
            <td><%# Eval("Email")%></td>
        </tr>
    </AlternatingItemTemplate>
    <EmptyDataTemplate>
        No records found
    </EmptyDataTemplate>
</asp:ListView>
