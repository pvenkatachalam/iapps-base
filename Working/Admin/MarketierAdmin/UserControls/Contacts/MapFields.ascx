﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MapFields.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.MapFields" %>

<script type="text/javascript">
    var <%=this.ClientID %>currentPage = 0;
</script>

<table border="0" width="100%" id="<%=this.ClientID %>mapFields" class="grid">
    <thead>
        <tr>
            <th class="firstCell" width="25%">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, MarketierContactsField %>" />
            </th>
            <th width="30%">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, YourFields %>" /><br />
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Noteautomatchedifexact %>" />
            </th>
            <th width="15%">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RequiredforImport %>" /><br />
                <asp:CheckBox ID="chkSelectAll" runat="server" Text="<%$ Resources:GUIStrings, SelectAll %>" />
            </th>
            <th width="30%">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SampleofyourRecords %>" /><br />
                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="navigation-links">
                    <tr>
                        <td align="left" id="<%=this.ClientID %>previousLink"><a href="javascript:<%=this.ClientID %>NavigatePage('previous');"><<&nbsp;<asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Previous %>" /></a></td>
                        <td style="font-weight: normal; text-align: center; padding: 1px 0;">
                            <span id="<%=this.ClientID %>currentPosition" style="display: none;">0</span><span id="<%=this.ClientID %>totalContacts" style="display: none;">0</span>
                            <span id="<%=this.ClientID %>DisplayingRecords"></span>
                        </td>
                        <td align="right" id="<%=this.ClientID %>nextLink"><a href="javascript:<%=this.ClientID %>NavigatePage('next');""><asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, Next %>"/>&nbsp;>></a></td>
                    </tr>
                </table>
            </th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater ID="myRepeater" runat="server" OnItemDataBound="myRepeater_ItemDataBound">
            <ItemTemplate>
                <tr class="row">
                    <td>
                        <asp:Label ID="lbl1Value" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PropertyName") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DdlXlsColumnName" DataSource="<%# xlsColumnData %>" runat="server"
                            DataTextField="ColumnName" DataValueField="SelectedIndexValue">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:CheckBox ID="chk1Required" runat="server" Enabled='<%#DataBinder.Eval(Container.DataItem,"IsEnabled") %>'
                            Checked='<%#DataBinder.Eval(Container.DataItem,"IsRequired") %>' Text="" />
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="alternateRow">
                    <td>
                        <asp:Label ID="lbl1Value" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PropertyName") %>'></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="DdlXlsColumnName" DataSource="<%# xlsColumnData %>" runat="server"
                            DataTextField="ColumnName" DataValueField="SelectedIndexValue">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:CheckBox ID="chk1Required" runat="server" Enabled='<%#DataBinder.Eval(Container.DataItem,"IsEnabled") %>'
                            Checked='<%#DataBinder.Eval(Container.DataItem,"IsRequired") %>' Text="" />
                    </td>
                    <td>&nbsp;
                    </td>
                </tr>
            </AlternatingItemTemplate>
        </asp:Repeater>
    </tbody>
</table>
<script type="text/javascript">
    function <%=this.ClientID %>OnSelectChange(ddl, propertyName) {
        var chk = ddl.id;
        chk = document.getElementById(chk.replace('DdlXlsColumnName', 'chk1Required'));

        if (ddl.value == '(Ignore)') {
            if (chk.parentNode.disabled == false && chk.checked == true) {
                ddl.selectedIndex = 1;
                return;
            }
            chk.checked = false;
            chk.disabled = true;
            chk.readOnly = true;
            chk.parentNode.disabled = false;
        }
        else {
            chk.disabled = false;
            chk.readOnly = false;
            chk.parentNode.disabled = false;
            if (propertyName == 'EmailId') {
                chk.checked = true;
                chk.disabled = true;
            }
        }
        <%=this.ClientID %>UpdateSampleData();
    }
    function <%=this.ClientID %>SetCheckBoxValue(opt) {

        var repid = '<%=this.myRepeater.ClientID %>_ctl'; //ctl00_ctl00_cphContent_contentContacts_myWizard_ctlMapFields_myRepeater
        var optid;
        for (var x = 0; x <= <%=this.ClientID %>UserRowCount; x++) {
            if (x <= 9) { optid = repid + '0' + x + '_chk1Required'; } else { optid = repid + x + '_chk1Required'; }

            if (document.getElementById(optid).disabled == false)
                document.getElementById(optid).checked = opt.checked;
        }
    }
    function ValidateWizard(clickObj) {
        return true;
    }

    function SaveBackStepvalue(clickObj) {

        return true;
    }

    function <%=this.ClientID %>UpdateSampleData() {
        for (var x = 0; x <=<%=this.ClientID %>UserRowCount; x++) {

            if (x <= 9) {
                <%=this.ClientID %>Colnum = document.getElementById(<%=this.ClientID %>RepeaterCtl + '0' + x + '_DdlXlsColumnName').value;
            }
            else {
                <%=this.ClientID %>Colnum = document.getElementById(<%=this.ClientID %>RepeaterCtl + x + '_DdlXlsColumnName').value;
            }
            <%=this.ClientID %>Colnum = <%=this.ClientID %>Colnum - 1;
            if (<%=this.ClientID %>MultiArray[<%=this.ClientID %>currentPage] != undefined) {
                $('#<%=this.ClientID %>mapFields tbody tr').each(function () {
                    if (this.rowIndex != x + 1) return; // skip first row
                    if (<%=this.ClientID %>Colnum >= 0)
                        this.cells[3].innerHTML = <%=this.ClientID %>MultiArray[<%=this.ClientID %>currentPage][<%=this.ClientID %>Colnum];
                    else
                        this.cells[3].innerHTML = "&nbsp;";
                });
            }
        }
    }
    function <%=this.ClientID %>NavigatePage(action) {
        if (action == "next") {
            if (<%=this.ClientID %>currentPage == <%=this.ClientID %>UserColCount) {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Youareonthelastpage %>'/>");
            }
            else {
                <%=this.ClientID %>currentPage++;
            }
        }
        else if (action == "previous") {
            if (<%=this.ClientID %>currentPage == 0) {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Youareonthefirstpage %>'/>");
            }
            else {
                <%=this.ClientID %>currentPage--;
            }
        }
    $("#<%=this.ClientID %>currentPosition").html(<%=this.ClientID %>currentPage + 1);
        $("#<%=this.ClientID %>DisplayingRecords").html(stringformat("<%= GUIStrings.DisplayingnbspspanidthisClientIDcurrentPosition0spannbspofnbspspanidthisClientIDtotalContacts0span %>", <%=this.ClientID %>currentPage + 1, <%=this.ClientID %>UserColCount + 1));
        <%=this.ClientID %>UpdateSampleData();
    }
    function <%=this.ClientID %>ToggleNavigationLinks() {
        if (typeof (window['<%=this.ClientID %>UserColCount']) != 'undefined')
            if (<%=this.ClientID %>UserColCount == 0) {
                $("#<%=this.ClientID %>previousLink").css('visibility', 'hidden');
                $("#<%=this.ClientID %>nextLink").css('visibility', 'hidden');
            }
    }
    $(document).ready(function () {
        <%=this.ClientID %>ToggleNavigationLinks();
    <%=this.ClientID %>UpdateSampleData();
    $("#<%=this.ClientID %>currentPosition").html(<%=this.ClientID %>currentPage + 1);
    $("#<%=this.ClientID %>totalContacts").html(<%=this.ClientID %>UserColCount + 1);
    $("#<%=this.ClientID %>DisplayingRecords").html(stringformat("<%= GUIStrings.DisplayingnbspspanidthisClientIDcurrentPosition0spannbspofnbspspanidthisClientIDtotalContacts0span %>", <%=this.ClientID %>currentPage + 1, <%=this.ClientID %>UserColCount + 1));
});
</script>
