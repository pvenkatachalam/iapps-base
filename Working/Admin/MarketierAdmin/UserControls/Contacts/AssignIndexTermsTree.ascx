﻿<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="Bridgeline.iAppsCMS.AdminControls.EditIndexTermsTree" %>
<script type="text/javascript">
    var isContentAdmin;
    /** Methods to Expand/Collapse Tree **/
    function expandPageTree(objTag) {
        if (objTag.className == "expanded") {
            objTag.className = "collapsed";
            objTag.innerHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, CollapseTree %>'/>";
            indexTermsTree.expandAll();
        }
        else {
            objTag.className = "expanded";
            objTag.innerHTML = "<asp:localize runat='server' text='<%$ Resources:JSMessages, ExpandTree %>'/>";
            indexTermsTree.collapseAll();
        }
        return false;
    }
    /** Methods to Expand/Collapse Tree **/

    function OnNodeSelect(sender, eventArgs) {
        if (eventArgs.get_node().get_checked() == true) {
            if (eventArgs.get_node().get_nodes().get_length() > 0) {
                var result = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, DoYouWantToDeselectAllSubTerms %>'/>")
                if (result == true)
                    eventArgs.get_node().unCheckAll();
                else
                    eventArgs.get_node().set_checked(false);
            }
            else
                eventArgs.get_node().set_checked(false);

        }
        else if (eventArgs.get_node().get_showCheckBox() != null || eventArgs.get_node().get_showCheckBox() == 1) {
            if (eventArgs.get_node().get_nodes().get_length() > 0) {
                var result = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, DoYouWantToSelectAllSubTerms %>'/>")
                    if (result == true)
                        eventArgs.get_node().checkAll();
                    else
                        eventArgs.get_node().set_checked(true);
                }
                else
                    eventArgs.get_node().set_checked(true);
            }

        indexTermsTree.render();
    }

    function indexTermsTree_NodeCheckChange(sender, eventArgs) {
        if (eventArgs.get_node().get_checked() == true) {
            if (eventArgs.get_node().get_nodes().get_length() > 0) {
                var result = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, DoYouWantToSelectAllSubTerms %>'/>")
                if (result == true) eventArgs.get_node().checkAll();
            }
        }
        else if (eventArgs.get_node().get_showCheckBox() != null || eventArgs.get_node().get_showCheckBox() == 1) {
            if (eventArgs.get_node().get_nodes().get_length() > 0) {
                var result = confirm("<asp:localize runat='server' text='<%$ Resources:JSMessages, DoYouWantToDeselectAllSubTerms %>'/>")
                    if (result == true) eventArgs.get_node().unCheckAll();
                }
            }

        indexTermsTree.render();
    }

    var treeCMenuOperation;
    var currentMenu;

    function manageDataTreeView_onNodeBeforeRename(sender, eventArgs) {
        var selectedNode = new ComponentArt.Web.UI.TreeViewNode();
        selectedNode = eventArgs.get_node();
        var parentNode = selectedNode.get_parentNode();
        var rootNodes = parentNode.get_nodes();

        if (trim(eventArgs.get_newText()) == "") {
            eventArgs.set_cancel(true);
            if (treeCMenuOperation == "Add") {
                eventArgs.get_node().remove();
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseTryAddingTheTermAgainAndEnsureItIsNotBlank %>'/>");
            }
            else {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseTryRenamingTheTermAgainAndEnsureItIsNotBlank %>'/>");
            }
        }
        else {

            //if (eventArgs.get_newText().match(/^[a-zA-Z0-9. _]+$/))
            if (eventArgs.get_newText().match("[\<\>\"]")) {
                if (treeCMenuOperation == "Add") {
                    eventArgs.get_node().remove();
                }

                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseremoveanyandcharactersfromtheindextermPleasereenter %>'/>");
                eventArgs.set_cancel(true);

            }
            else {
                for (var i = 0; i < rootNodes.get_length() - 1; i++) {

                    if ((eventArgs.get_newText() == rootNodes.getNode(i).get_text())) {
                        if (eventArgs.get_node().get_id() != rootNodes.getNode(i).get_id()) {
                            if (treeCMenuOperation == "Add") {
                                eventArgs.get_node().remove();
                                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, ThetermyouretryingtoaddalreadyexistsPleasetryagainwithadifferentname %>'/>");
                            }
                            else {
                                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, TheTermYoureTryingToRenameAlreadyExistsPleaseTryAgainWithADifferentName %>'/>");
                            }
                            eventArgs.set_cancel(true);
                            //manageDataTreeView_onNodeRename(sender, eventArgs)


                        }
                        break;
                    }
                }
            }
        }
    }


    function trim(str) {
        if (!str || typeof str != 'string')
            return "";

        return str.replace(/^[\s]+/, '').replace(/[\s]+$/, '').replace(/[\s]{2,}/, ' ');
    }


    function manageDataTreeView_onNodeRename(sender, eventArgs) {
        var selectedNode = new ComponentArt.Web.UI.TreeViewNode();
        selectedNode = eventArgs.get_node();
        var parentNode = selectedNode.get_parentNode();
        var id;

        if (treeCMenuOperation == "Add") {
            // treeCallbackCtrl.callback("Add",parentNode.get_id(),selectedNode.get_text());

            id = CreateTaxonomyItem(selectedNode.get_text(), parentNode.get_id())

            if (id.toString().length == 36) {
                indexTermsTree.beginUpdate();
                eventArgs.get_node().set_id(id);
                indexTermsTree.endUpdate();
                //   indexTermsTree.render();
            }
        }
        else {
            //treeCallbackCtrl.callback("Rename",selectedNode.get_id(),selectedNode.get_text());              
            id = RenameTaxonomyItem(selectedNode.get_text(), selectedNode.get_id());
        }
    }

    function generateGuid() {
        var result, i, j;
        result = '';
        for (j = 0; j < 32; j++) {
            if (j == 8 || j == 12 || j == 16 || j == 20)
                result = result + '-';
            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
            result = result + i;
        }
        return result;
    }
</script>
<div>
    <div class="libraryTreeOuter">
        <div class="TreeHeading">
            <h5>
                <asp:Literal runat="server" ID="LrlTitle"></asp:Literal>
            </h5>
            <a href="#" onclick="return expandPageTree(this);" class="expanded"><asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, ExpandTree %>"/></a>
        </div>
        <div>
            <ComponentArt:TreeView SkinID="Default" Height="375" Width="325" ID="indexTermsTree"
                EnableViewState="false" runat="server">
                <ClientEvents>
                    <NodeCheckChange EventHandler="indexTermsTree_NodeCheckChange" />
                    <%--<NodeSelect EventHandler="OnNodeSelect" />--%>
                    <NodeBeforeRename EventHandler="manageDataTreeView_onNodeBeforeRename" />
                    <NodeRename EventHandler="manageDataTreeView_onNodeRename" />
                </ClientEvents>
            </ComponentArt:TreeView>
        </div>
    </div>
</div>
