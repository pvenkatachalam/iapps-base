﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectDataSource.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.SelectDataSource" %>

<script type="text/javascript">
    function setFileOnPath(objFile) {
        document.getElementById("<%=fakeFileControlOnImage.ClientID%>").value = objFile.value;
    }
    function ShowHideFileSelection() {
        var radCsv = document.getElementById('<%=rbtnCsv.ClientID %>');
        var divCsv = document.getElementById('<%=divCsv.ClientID %>');
        if (radCsv.checked)
            divCsv.style.display = '';
        else
            divCsv.style.display = 'none';
    }
</script>
<div class="import-options">
    <div class="columns">
        <asp:RadioButton runat="server" ID="rbtnCsv" Checked="true" Text="<%$ Resources:GUIStrings, UploadCSVFile %>" GroupName="DataSource" onclick="ShowHideFileSelection();" />
        <p class="help-text ">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Thisoptionwillallowyoutoimportcontactsfromacsvfile %>" />
        </p>
    </div>
    <div class="columns">
        <asp:RadioButton runat="server" ID="rbtnSFContact" Text="<%$ Resources:GUIStrings, SalesforcecomContacts %>" GroupName="DataSource" onclick="ShowHideFileSelection();" /><br />
        <p class="help-text salesforcebg">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ThisoptionwillallowyoutoimportallyourstrongSalesforcecomContactsstrongbasedonyourSalesforcesettingsunderadministrationmenu %>" />
        </p>
    </div>
    <div class="columns">
        <asp:RadioButton runat="server" ID="rbtnSFLead" Text="<%$ Resources:GUIStrings, SalesforcecomLeads %>" GroupName="DataSource" onclick="ShowHideFileSelection();" />
        <p class="help-text salesforcebg">
            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, SalesforcecomLeadsThisoptionwillallowyoutoimportallyourSalesforcecomLeadsbasedonyourSalesforcesettingsunderadministrationmenu %>" />
        </p>
    </div>
</div>
<div class="import-utility clear-fix">
    <div runat="server" id="divCsv">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, DataSource %>" /></label>
            <div class="form-value">
                <asp:FileUpload ID="fileUploadOnImage" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server" Width="225" onmouseout="setFileOnPath(this);"
                    CssClass="fileUpload" />
                <div class="fakeFile" style="display: none;">
                    <asp:TextBox ID="fakeFileControlOnImage" runat="server" ReadOnly="true" CssClass="textBoxes"
                        Width="315"></asp:TextBox>
                    <asp:Button ID="btnBrowseOnImage" CssClass="button dark" Text="<%$ Resources:GUIStrings, Browse %>" runat="server" />
                </div>
            </div>
        </div>
        <div class="form-row">
            <asp:CheckBox ID="chkOverwrite" runat="server" Text="<%$ Resources:GUIStrings, Overwriteexistingcontactpropertiesbasedonemailaddress %>" />
        </div>
        <div class="form-row">
            <asp:CheckBox ID="chkOverwriteWebsiteUsersAndCustomers" runat="server" Text="<%$ Resources:GUIStrings, Overwriteexistingcontentmanagerwebsiteuserandcommercecustomerpropertiesbasedonemailaddress %>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function ValidateWizard(clickObj) {
        var isValid = true;
        var radCsv = document.getElementById('<%=rbtnCsv.ClientID %>');
        if (radCsv.checked) {

            var selectedFile = document.getElementById("<%=fakeFileControlOnImage.ClientID %>");

            if (selectedFile.value == "") {
                alert("<%= JSMessages.Pleaseselectafiletoupload %>");
                isValid = false;
            }
            var seldata = selectedFile.value;
            var valid_extensions = /(.csv|.CSV|.Csv)$/i;
            if (seldata) {
                if (!valid_extensions.test(seldata)) {
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseuseacsvfileintheproperformattoimportcontacts %>'/>");
                    isValid = false;
                }
            }
        }
        return isValid;

        __doPostBack(clickObj.id, '');
    }
</script>

