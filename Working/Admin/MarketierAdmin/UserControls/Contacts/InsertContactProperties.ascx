﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InsertContactProperties.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.InsertContactProperties" %>
<script type="text/javascript">
    var iapps_contactpropertiestxt = '';
    function fn_ShowContactProperties(objTextbox) {
        $('#divInsertContactProperty').iAppsDialog('open');
        iapps_contactpropertiestxt = objTextbox;
        return false;
    }

    function fn_HideContactProperties() {
        $('#divInsertContactProperty').iAppsDialog('close');
        iapps_contactpropertiestxt = '';
        return false;
    }

    function fn_InsertContactProperty(objDropdown) {
        var iapps_textbox = $('#' + iapps_contactpropertiestxt);
        var iapps_dropdown = $('#' + objDropdown);
        switch (iapps_contactpropertiestxt) {
            case 'txtEmailSubject':
            case 'txtAutoResponderEmailSubject':
                iapps_textbox.val(iapps_textbox.val() + ' ' + iapps_dropdown.val());
                break;
            case 'txtSenderName':
            case 'txtSenderEmail':
                iapps_textbox.val(iapps_dropdown.val());
                break;
        }
        fn_HideContactProperties();
        return false;
    }
</script>
<div class="iapps-modal insert-contact-property" id="divInsertContactProperty" style="display: none;">
    <div class="modal-header clear-fix">
        <h2><%= Bridgeline.iAPPS.Resources.GUIStrings.InsertDynamicContactDetails %></h2>
    </div>
    <div class="modal-content clear-fix">
        <asp:DropDownList ID="ddlContactProperties" runat="server" Width="300" ClientIDMode="Static" />
    </div>
    <div class="modal-footer clear-fix">
        <asp:Button ID="btnCancelInsert" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return fn_HideContactProperties();" />
        <asp:Button ID="btnInsert" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Insert %>" OnClientClick="return fn_InsertContactProperty('ddlContactProperties');" />
    </div>
</div>
