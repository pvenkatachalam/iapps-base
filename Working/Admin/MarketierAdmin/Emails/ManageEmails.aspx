﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageEmails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageEmails" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="GroupSelector" Src="~/UserControls/General/GroupSelector.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            var params = $.getQueryString();
            if (params["RedirectFrom"] == "ALCampaign")
                OpenCreateEmailPopup("RedirectFrom=ALCampaign&GroupId=" + params["GroupId"]);
        });

        function upEmails_OnLoad() {
            $(".emails-list").gridActions({
                objList: $(".email-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upEmails_Callback(sender, eventArgs);
                }
            });
        }

        function upEmails_OnRenderComplete() {
            $(".emails-list").gridActions("bindControls");
            $(".emails-list").iAppsSplitter({ leftScrollSection: ".group-list" });
        }

        function upEmails_OnCallbackComplete() {
            $(".emails-list").gridActions("displayMessage");
        }

        function LoadItemsGrid() {
            $(".emails-list").gridActions("selectNode", treeActionsJson.FolderId);
        }

        function upEmails_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedFolderId = gridActionsJson.FolderId;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("Title");

            switch (gridActionsJson.Action) {
                case "AddNew":
                    doCallback = false;
                    OpenCreateEmailPopup("GroupId=" + selectedFolderId);
                    break;
                case "Edit":
                    doCallback = false;
                    JumpToEmailFrontPage(MarketierCallback.GetCampaignPageUrl(selectedItemId));
                    break;
                case "Delete":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisEmail %>");
                break;
            case "ViewEmailResults":
                doCallback = false;
                OpeniAppsMarketierPopup("EmailRunHistory", "Id=" + selectedItemId);
                break;
            case "Duplicate":
                doCallback = true;
                break;
            case "ExportUnsubscribed":
                doCallback = true;
                break;
            case "ViewLandingPages":
                doCallback = false;
                OpeniAppsMarketierPopup("ViewLandingPages", "IsResponse=false&CampaignId=" + selectedItemId, '');
                break;
            case "Move":
                doCallback = false;
                OpeniAppsMarketierPopup('MoveEmail', "EmailId=" + selectedItemId, "RefreshEmailCampaigns");
                break;
        }

        if (doCallback) {
            upEmails.set_callbackParameter(JSON.stringify(gridActionsJson));
            upEmails.callback();
        }
    }

    function Grid_ItemSelect(sender, eventArgs) {

        var hasAnalyticsLicense = $("#<%= hdnHasAnalyticsLicense.ClientID %>").val();

            sender.getItemByCommand("Edit").showButton();
            sender.getItemByCommand("Delete").showButton();

            if (treeActionsJson.NodeType == 0) {
                sender.getItemByCommand("Edit").hideButton();
                sender.getItemByCommand("Delete").hideButton();
                sender.getItemByCommand("ViewEmailResults").hide();
                sender.getItemByCommand("ViewLandingPages").hide();
                sender.getItemByCommand("Duplicate").hide();
                sender.getItemByCommand("Delete").hide();
                sender.getItemByCommand("CancelledSend").hide();
                sender.getItemByCommand("ExportUnsubscribed").hide();
            }

            if (!hasAnalyticsLicense) {
                sender.getItemByCommand("ViewLandingPages").hide();
            }

        }

        function RefreshEmailCampaigns() {
            $(".emails-list").gridActions("callback", { refreshTree: true });
        }

        function OpenViewEMailResultsPopup() {
            OpeniAppsMarketierPopup("EmailRunHistory", "Id=" + gridActionsJson.SelectedItems.first());
        }

        function fn_OpenEmailExport() {
            OpeniAppsMarketierPopup('ExportEmails', '', '');
        }

    </script>
</asp:Content>

<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="page-header-section clear-fix">
        <h1><%=  GUIStrings.ManageEmails %></h1>
        <a href="javascript: //" onclick="fn_OpenEmailExport()" class="csv-item"><%= Bridgeline.iAPPS.Resources.GUIStrings.ExportEmails %></a>
    </div>
        <asp:HiddenField ID="hdnHasAnalyticsLicense" runat="server" />
        <div class="clear-fix">
            <div class="left-control">
                <UC:GroupSelector ID="emailGroups" runat="server" />
            </div>
            <div class="right-control">
                <iAppsControls:CallbackPanel ID="upEmails" runat="server" OnClientCallbackComplete="upEmails_OnCallbackComplete"
                    OnClientRenderComplete="upEmails_OnRenderComplete" OnClientLoad="upEmails_OnLoad">
                    <ContentTemplate>
                        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                        <asp:ListView ID="lvEmails" runat="server" ItemPlaceholderID="phEmailList">
                            <EmptyDataTemplate>
                                <div class="empty-list">
                                    <%= GUIStrings.NoCampaignEmails %>
                                </div>
                            </EmptyDataTemplate>
                            <LayoutTemplate>
                                <div class="collection-table fat-grid email-list">
                                    <asp:PlaceHolder ID="phEmailList" runat="server" />
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                    <div class="collection-row clear-fix grid-item" objectid='<%# Eval("CampaignId") %>'>
                                        <div class="large-cell">
                                            <h4 datafield="Title">
                                                <asp:Literal ID="ltEmailName" runat="server" />
                                            </h4>
                                            <p>
                                                <asp:Literal ID="ltEmailDescription" runat="server" />
                                            </p>
                                        </div>
                                        <div class="small-cell">
                                            <div class="child-row">
                                                <asp:Literal ID="ltStatus" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <asp:Literal ID="ltUpdated" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <asp:Label ID="lblContacts" runat="server" CssClass="left-margin" />
                                                <%--  <asp:Label ID="lblOpens" runat="server" CssClass="left-margin" />
                                            <asp:Label ID="lblClicks" runat="server" CssClass="left-margin" />--%>
                                                <p>
                                                    <asp:HyperLink ID="hplMoreEmailInfo" runat="server" NavigateUrl="javascript:OpenViewEMailResultsPopup();" Text="Email Statistics"></asp:HyperLink>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </ContentTemplate>
                </iAppsControls:CallbackPanel>
            </div>
        </div>
        <p class="statistic-update-info clear-fix">
            <asp:Literal ID="ltStatisticsUpdated" runat="server" />
        </p>
</asp:Content>
