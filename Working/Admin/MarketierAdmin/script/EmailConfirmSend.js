﻿$(function () {
    parent.fn_ResetToolbar();

	$('.confirm-email')
		.find('.recurrence-options')
		.find('input[type="radio"]')
		.change(DeliveryOptionChange);
	
	document.getElementById('email_preview').src = GetFrontPageUrlWithToken(jPublicSiteUrl + '/' + parent.jPageUrl, "PageState=EmailView");
});

function DeliveryOptionChange() {
	switch ($(this).val()) {
		case 'None':
			HideRecurrenceOptions();
			break;
		case 'Daily':
			ToggleRecurrenceOptions($('.recurrence-options-daily'));
			break;
		case 'Weekly':
			ToggleRecurrenceOptions($('.recurrence-options-weekly'));
			break;
		case 'Monthly':
			ToggleRecurrenceOptions($('.recurrence-options-monthly'));
			break;
		case 'Yearly':
			ToggleRecurrenceOptions($('.recurrence-options-yearly'));
			break;
	}
}

function ToggleRecurrenceOptions(container) {
	var recurrenceOptionsContainer = $('.recurrence-options-container');

	HideRecurrenceOptions();

	recurrenceOptionsContainer
		.find(container)
		.removeClass('is-hidden')
		.addClass('is-visible');
}

function HideRecurrenceOptions() {
	var recurrenceOptionsContainer = $('.recurrence-options-container');
	recurrenceOptionsContainer
		.find('.is-visible')
		.removeClass('is-visible')
		.addClass('is-hidden');
}

function SaveEmailBasePage() {
    if (typeof parent.fn_SaveEmailAsDraft == "function")
        parent.fn_SaveEmailAsDraft();

    return true;
}

function fn_ShowRejectionNotesPopup() {
	OpeniAppsMarketierPopup('WorkflowRejectionNotes', '', 'fn_RejectionNotesCallback');

	return false;
}