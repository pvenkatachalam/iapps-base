﻿$(function () {
    var campaign = parent.fn_ResetToolbar();
    if (campaign.TotalContacts > 0)
        $("#btnSaveAndContinue").show();
    else
        $("#btnSaveAndContinue").hide();
});

function ToggleRecurrenceOptions(selection) {
	var optionContainer;

	HideRecurrenceOptions();

	switch (selection) {
		case 'None':
			break;
		case 'Daily':
			optionContainer = $('.recurrence-options-daily');
			break;
		case 'Weekly':
			optionContainer = $('.recurrence-options-weekly');
			break;
		case 'Monthly':
			optionContainer = $('.recurrence-options-monthly');
			break;
		case 'Yearly':
			optionContainer = $('.recurrence-options-yearly');
			break;
	}

	if (optionContainer != null) {
		$('.recurrence-options-container')
			.removeClass('is-hidden')
			.find(optionContainer)
			.removeClass('is-hidden')
			.addClass('is-visible');
	}

	fn_ReadjustPopupHeight();
}

function HideRecurrenceOptions() {
	$('.recurrence-options-container')
		.addClass('is-hidden')
		.find('.is-visible')
		.removeClass('is-visible')
		.addClass('is-hidden');
}

function fn_ReadjustPopupHeight() {
	var child = document.getElementsByClassName('container-main')[0];
	var frame = parent.document.getElementsByClassName('iapps-popup')[0];
	var popup = $(frame).parent()[0];

	var newFrameHeight = child.offsetHeight + 160;
	var newTopPosition = popup.offsetTop - ((newFrameHeight - frame.offsetHeight) / 2);

	$(popup).animate({ top: newTopPosition }, 200);
	$(frame).animate({ height: newFrameHeight }, 200);;
}