/* Copyright Bridgeline Software, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Software, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited */
var overlayDisplay = null;
var prevElement = null;
var prevOverlay = null;
var emptyLinks = "";
var dummyLinks = "#";
function setOverlay(destObj, srcObj) {
    var objLeft = srcObj.offsetLeft;
    var objTop = srcObj.offsetTop;
    var objParent = srcObj.parentNode;
    while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
        objLeft += objParent.offsetLeft;
        objTop += objParent.offsetTop;
        objParent = objParent.parentNode;
    }
    if (prevElement != null && prevElement.parentNode != null) {
        prevElement.parentNode.className = "percent";
        prevElement.className = "overlayArrow";
    }
    if (destObj.innerHTML != '') {
        destObj.style.left = (objLeft - 3) + "px";
        destObj.style.top = (objTop + 16) + "px";
        destObj.className = "overlayInfo";
        srcObj.parentNode.className = "percentHover";
        srcObj.className = "overlayArrowHover";
    }
}
function OverlayClick(objectId, overlayObject) {
    var parentElement = document.getElementById("overlayDetails");
    if (prevElement == null)
        prevElement = overlayObject;
    if (overlayDisplay != null && overlayDisplay.innerHTML != '') {
        if (parentElement.innerHTML.indexOf(overlayDisplay.innerHTML) > 0) {
            parentElement.removeChild(overlayDisplay);
        }
    }
    overlayDisplay = document.createElement("div");
    var currentDivId = S4();
    overlayDisplay.setAttribute("id", currentDivId);
    var overlayInfoText = GetOverlayDetails(objectId);
    if (overlayInfoText != "") {
        if (prevOverlay == overlayInfoText) {
            prevOverlay = null;
            setOverlay(overlayDisplay, overlayObject);
            overlayDisplay = null;
        }
        else {
            prevOverlay = overlayInfoText;
            overlayDisplay.innerHTML = overlayInfoText;
            parentElement.appendChild(overlayDisplay);
            setOverlay(overlayDisplay, overlayObject);
            prevElement = overlayObject;
        }
    }
    else {
        setOverlay(overlayDisplay, overlayObject);
        overlayDisplay = null;
        prevOverlay = null;
    }
}
function GetOverlayDetails(objectId) {
    var htmlItems = "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" class=\"overlayData\"><colgroup><col width=\"30\"/><col /></colgroup>";
    for (var i = 0; i < arrOverlay.length; i++) {
        if (arrOverlay[i][0] == objectId) {
            htmlItems += "<tr>";
            htmlItems += "<td class=\"underline\">" + arrOverlay[i][3] + "</td>";
            htmlItems += "<td class=\"underline\">Clicks</td>";
            htmlItems += "</tr>";
            htmlItems += "<tr>";
            htmlItems += "<td colspan=\"2\"><div class=\"overlayLinks topMargin\" onclick=\"ViewContactsForLink('" + objectId + "');\">" + __JSMessages["ViewContacts"] + "</div></td>";
            htmlItems += "</tr>";
            htmlItems += "<tr>";
            htmlItems += "<td colspan=\"2\"><div class=\"overlayLinks\" onclick=\"AssignContactsToDistributionList('" + objectId + "');\">" + __JSMessages["AssignContactstoDistributionList"] + "</div></td>";
            htmlItems += "</tr>";
        }
    }
    htmlItems += "</table>";
    return (htmlItems);
}
// Anchor Tags
function AnchorOverlay() {
    var parentElement = document.getElementById("overlay");
    var findAnchorMatched = false;
    var currentPageLinks = window.location + "";
    var isHttps_ForOverlay = -1;
    currentPageLinks = currentPageLinks.toLowerCase() + "#";
    var existingAnchors = document.getElementsByTagName("a");
    for (var count = 0; count < existingAnchors.length; count++) {
        isHttps_ForOverlay = -1;
        var currentAnchor = existingAnchors[count].href;
        findAnchorMatched = false;
        currentAnchor = currentAnchor.toLowerCase();
        if (currentAnchor != emptyLinks) {
            if (currentAnchor.indexOf("https://") > -1) {
                isHttps_ForOverlay = 1;
            }
            currentAnchor = isHttps_ForOverlay > -1 ? currentAnchor.replace("https://", "") : currentAnchor.replace("http://", "");
            currentAnchor = currentAnchor.replace("///", "/");
            currentAnchor = currentAnchor.replace("//", "/");

            currentAnchor = decodeURIComponent(currentAnchor);
        }
        for (var j = 0; j < arrOverlay.length; j++) {
            if (ifAnchorMatches(currentAnchor, arrOverlay[j][1])) {
                var htmlObject = document.createElement("div");
                var insertHTML = "<div class=\"overlayArrow\" onclick=\"OverlayClick('" + arrOverlay[j][0] + "', this);\">";
                insertHTML += arrOverlay[j][4] + "%";
                insertHTML += "</div>";
                htmlObject.innerHTML = insertHTML;
                setAnchorOverlay(htmlObject, existingAnchors[count]);
                parentElement.appendChild(htmlObject);
                findAnchorMatched = true;
            }
        }
        if (!findAnchorMatched && currentAnchor != emptyLinks && currentAnchor != dummyLinks && currentAnchor != currentPageLinks) {
            var htmlObject = document.createElement("div");
            var insertHTML = "<div class=\"overlayArrow\">";
            insertHTML += "0%";
            insertHTML += "</div>";
            htmlObject.innerHTML = insertHTML;
            setAnchorOverlay(htmlObject, existingAnchors[count]);
            parentElement.appendChild(htmlObject);
        }
    }
}
function setAnchorOverlay(destObj, srcObj) {
    var objLeft = srcObj.offsetLeft;
    var objTop = srcObj.offsetTop;
    var objParent = srcObj.offsetParent;
    while (objParent != null && objParent.tagName.toUpperCase() != "BODY") {
        objLeft += objParent.offsetLeft;
        objTop += objParent.offsetTop;
        objParent = objParent.offsetParent;
    }
    destObj.style.top = objTop + "px";
    destObj.style.left = (srcObj.offsetWidth + objLeft) + "px";
    destObj.className = "percent";
}

function escapeHTMLEncode(str) {
    var div = document.createElement('div');
    var text = document.createTextNode(str);
    div.appendChild(text);
    return div.innerHTML;
}

function ifAnchorMatches(currentAnchor, overLayAnchor) {
    if (currentAnchor.indexOf(overLayAnchor) > -1) {
        return true;
    }
    else {
        return false;
    }
}
function S4() {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
}
function ContainerSpecificAnchorOverlay() {
    var parentElement = document.getElementById("overlay");
    var currentPageLinks = window.location + "";
    var isHttps_ForOverlay = -1;
    var currentPageLinks = currentPageLinks.toLowerCase() + "#";
    var anchorMatched = false;
    //looping through the overlay data array
    for (var i = 0; i < arrOverlay.length; i++) {
        //finding container that has overlay data
        //var containerId = $('[FWContainerID=' + arrOverlay[i][5] + ']').attr('id');
        var containerId = '';
        if (arrOverlay[i][6] != '') {
            containerId = arrOverlay[i][6];
        }
        else {
            containerId = $('[FWContainerID=' + arrOverlay[i][5] + ']').attr('id');
        }
        debugger;
        //check if the element exists
        if (containerId != 'undefined' && containerId != null && containerId != undefined) {
            //get all the anchors present in the container
            var existingAnchors = document.getElementById(containerId).getElementsByTagName("a");
            //loop through the anchor collection
            for (var count = 0; count < existingAnchors.length; count++) {
                anchorMatched = false;
                var currentAnchor = existingAnchors[count].href;
                currentAnchor = currentAnchor.toLowerCase();
                if (currentAnchor != emptyLinks) {
                    if (currentAnchor.indexOf("https://") > -1) {
                        isHttps_ForOverlay = 1;
                    }
                    currentAnchor = isHttps_ForOverlay > -1 ? currentAnchor.replace("https://", "") : currentAnchor.replace("http://", "");
                    currentAnchor = currentAnchor.replace("///", "/");
                    currentAnchor = currentAnchor.replace("//", "/");
                    currentAnchor = decodeURIComponent(currentAnchor);
                }
                if (ifAnchorMatches(currentAnchor, arrOverlay[i][1])) {
                    var htmlObject = document.createElement("div");
                    var insertHTML = "<div class=\"overlayArrow\" onclick=\"OverlayClick('" + arrOverlay[i][0] + "', this);\">";
                    insertHTML += arrOverlay[i][4] + "%";
                    insertHTML += "</div>";
                    htmlObject.innerHTML = insertHTML;
                    setAnchorOverlay(htmlObject, existingAnchors[count]);
                    parentElement.appendChild(htmlObject);
                    //existingAnchors[count].href = "#";
                    anchorMatched = true;
                }
            }
        }
    }
    //set 0% for not clicked anchors
    //SetUnmarkedAnchors();
}

function SetUnmarkedAnchors() {
    var parentElement = document.getElementById("overlay");
    var currentPageLinks = window.location + "";
    var isHttps_ForOverlay = -1;
    var currentPageLinks = currentPageLinks.toLowerCase() + "#";
    var existingAnchors = document.getElementsByTagName("a");
    for (var count = 0; count < existingAnchors.length; count++) {
        var currentAnchor = existingAnchors[count].href;
        currentAnchor = currentAnchor.toLowerCase();
        if (currentAnchor != emptyLinks) {
            if (currentAnchor.indexOf("https://") > -1) {
                isHttps_ForOverlay = 1;
            }
            currentAnchor = isHttps_ForOverlay > -1 ? currentAnchor.replace("https://", "") : currentAnchor.replace("http://", "");
            currentAnchor = currentAnchor.replace("///", "/");
            currentAnchor = currentAnchor.replace("//", "/");
            currentAnchor = decodeURIComponent(currentAnchor);
            if (browser == "mozilla") {
                currentAnchor = isHttps_ForOverlay > -1 ? "https://" + currentAnchor : "http://" + currentAnchor;
            }
        }
        if (currentAnchor != emptyLinks && currentAnchor != dummyLinks && currentAnchor != currentPageLinks) {
            var htmlObject = document.createElement("div");
            var insertHTML = "<div class=\"overlayArrow\">";
            insertHTML += "0%";
            insertHTML += "</div>";
            htmlObject.innerHTML = insertHTML;
            setAnchorOverlay(htmlObject, existingAnchors[count]);
            parentElement.appendChild(htmlObject);
            existingAnchors[count].href = "#";
        }
    }
}

function ViewContactsForLink(objectId) {
    if (window.opener != null) {
        for (var i = 0; i < arrOverlay.length; i++) {
            if (arrOverlay[i][0] == objectId) {
                //var callbackParameter = "GetContacts," + arrOverlay[i][5] + "," + arrOverlay[i][1];
                var callbackParameter = "GetContacts," + arrOverlay[i][5] + "," + arrOverlay[i][6] + "," + arrOverlay[i][1]; //passing the controlid instead in containerid

                grdContacts.set_callbackParameter(callbackParameter);

                grdContacts.callback();

                ShowContactsPopup();
            }
        }
    }
}

function AssignContactsToDistributionList(objectId) {
    if (window.opener != null) {
        for (var i = 0; i < arrOverlay.length; i++) {
            if (arrOverlay[i][0] == objectId) {
                //window.opener.AssignContactsToDistributionList(iAPPS_campaignRunId, arrOverlay[i][5], arrOverlay[i][1]);
                var linkContainerId = arrOverlay[i][5];
                var controlId = arrOverlay[i][6];
                var targetId = arrOverlay[i][1];
                setSelectedIds(targetId, linkContainerId, controlId);
                distributionListWindow = OpeniAppsMarketierPopup("AssignToDistributionList", "RedirectFromOverlay=true&CampaignRunId=" + iAPPS_campaignRunId + "&LinkContainerId=" + linkContainerId + "&TargetId=" + targetId + "&ControlId=" + controlId);

                //window.close();
            }
        }
    }
}

function setSelectedIds(TargetId, LinkContainerId, ControlId) {
    var overlayHelper = JSON.parse($('#hdnOverlayHelper').val());

    $.each(overlayHelper, function (key) {
        if (overlayHelper[key].TargetId == TargetId && (overlayHelper[key].LinkContainerId == LinkContainerId || overlayHelper[key].ControlId == ControlId)) {
            selectedIds = toCommaDelimitedList(overlayHelper[key].Contacts);
        }
    });
}

function toCommaDelimitedList(listArr) {
    var string = "";

    for (var i = 0; i < listArr.length; i++) {
        if (i != 0) {
            string += ',';
        }
        string += listArr[i];
    }

    return string;
}

function grdContacts_onCallbackComplete(sender, eventArgs) {
    grdContacts.set_callbackParameter("");
}

function GetProductImage(objDataItem) {
    var productImage = "";
    var contactType = objDataItem.getMember("Type").get_text();

    switch (contactType) {
        case '0':
            //Marketier
            productImage = '<img src="../App_Themes/General/images/orange-square-bullet.png" alt="' + __JSMessages["iAPPSMarketierContact"] + '" title="' + __JSMessages["iAPPSMarketierContact"] + '" class="contactProductImage" style="margin: 0 auto;" />';
            break;
        case '1':
            //Commerce
            productImage = '<img src="../App_Themes/General/images/green-square-bullet.png" alt="' + __JSMessages["iAPPSCommerceCustomer"] + '" title="' + __JSMessages["iAPPSCommerceCustomer"] + '" class="contactProductImage" style="margin: 0 auto;" />';
            break;
        case '2':
            //CMS
            productImage = '<img src="../App_Themes/General/images/blue-square-bullet.png" alt="' + __JSMessages["iAPPSCMUser"] + '" title="' + __JSMessages["iAPPSCMUser"] + '" class="contactProductImage" style="margin: 0 auto;" />';
            break;
    }

    return productImage;
}

function ShowContactsPopup() {
    $('#divViewContacts').iAppsDialog('open');
}

function CloseContactsPopup() {
    $('#divViewContacts').iAppsDialog("close");
    grdContacts.set_callbackParameter("Clear");
    grdContacts.callback();
}