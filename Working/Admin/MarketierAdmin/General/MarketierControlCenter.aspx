﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="MarketierControlCenter.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.MarketierControlCenter" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="EmailCampaigns" Src="~/UserControls/Administration/EmailCampaigns.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <link href="/marketieradmin/common/css/kendo.dataviz.min.css" rel="Stylesheet" media="all" />
    <script type="text/javascript" src="/marketieradmin/common/kendo/kendo.dataviz.min.js"></script>

    <script type="text/javascript">
        function pageLoad() {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);
        }

        $(document).on('click', '.grid-item', gridItem_Click);

        function endRequestHandler(sender, args) {
            // Do your stuff
           // alert('Update Panel routine is now complete');
            UpdateChart();
        }

        $(document).ready(function () {

           

            var seriesData = [<%= GetAreaChartJson().Replace("\\","").Remove(0,1).Replace(","+"\"", ",") %>];

            $("#divContactChart").kendoChart({
                dataSource: {
                    data: seriesData
                },
                series: [{
                    type: "area",
                    field: "value",
                    categoryField: "month",
                    visibleInLegend: false,
                    color: "#62c0c6",
                    markers: {
                        visible: true,
                        background: "#62c0c6"
                    }
                }],
                tooltip: {
                    visible: true,
                    background: "#000000",
                    color: "#ffffff"
                },
                chartArea: {
                    background: "Transparent",
                    height: 200
                },
                categoryAxis: {
                    color: "#545c6b",
                    //background: "#545c6b",
                    majorGridLines: {
                        width: 0
                    },
                    line: {
                        visible: false
                    }
                },
                valueAxis: {
                    color: "#676060",
                    majorGridLines: {
                        width: 0
                    },
                    labels: {
                        visible: false
                    },
                    line: {
                        visible: false
                    }
                },
                panes: [{
                    clip: false
                }]
            });

           

            UpdateChart();

            if ($.getQueryString()['newCampaign'] == 'true') {
                OpenCreateEmailPopup();
            }
        });

        function UpdateChart() {
            var utilizedChartData = [<%= GetUtilizedChartDataJson().Remove(0,1).Replace("\\","").Replace("}"+"\"", "}") %>];
            var availableChartData = [<%= GetAvailableChartDataJson().Remove(0, 1).Replace("," + "\"", "")%>];

            $("#divEmailStatsChart").kendoChart({
                legend: {
                    visible: false
                },
                chartArea: {
                    background: "Transparent",
                    height: 200
                },
                seriesDefaults: {
                    type: "bar",
                    stack: {
                        type: "100%"
                    },
                    border: 0
                },
                series: [{
                    name: "Utilized",
                    data: utilizedChartData,
                    field: "value",
                    colorField: "valueColor",
                    color: "Transparent"
                }, {
                    name: "Available",
                    data: availableChartData,
                    color: "#c7c7c7"
                }],
                valueAxis: {
                    majorGridLines: {
                        width: 0
                    },
                    labels: {
                        visible: false
                    },
                    line: {
                        visible: false
                    }
                },
                categoryAxis: {
                    categories: [1, 2, 3, 4],
                    majorGridLines: {
                        width: 0
                    },
                    line: {
                        visible: false
                    },
                    labels: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    template: "#= series.name #: #= value #%"
                }
            });

        }
        function load() {
            //alert("Page is loaded");
        }

        function fn_ShowMonthlyView() {
            OpeniAppsMarketierPopup('EmailMonthlyView', '', '');
            return false;
        }

        function fn_RedirectToCMS() {
            var productName = 'cms';
            if (hasCMSLicense && hasCMSPermission) {
                RedirectWithToken(productName, '/admin/Libraries/Data/ManagePages.aspx');
            }
            else {
                CheckLicense(productName, true);
            }
        }

        function gridItem_Click(e) {
        	var campaignId = this.getAttribute('objectid');
        	var pageUrl = GetFrontPageUrlWithToken(MarketierCallback.GetCampaignPageUrl(campaignId), "PageState=EmailView");
        	var data = JSON.parse(MarketierCallback.GetEmailPreviewData(campaignId));

            document.getElementById('email_preview').src = jPublicSiteRootUrl + '/' + pageUrl;
            document.getElementById('<%= pnEmailPreview.ClientID %>').style.display = 'block';
        	document.getElementById('email_title').innerText = data.title;
        	document.getElementById('email_groupName').innerText = data.groupName;
        	document.getElementById('email_lastModified').innerText = data.lastModified;

        	$('#email_preview').load(function() { 
        		$(this).contents().find('body').css('overflow', 'hidden');
        	});
        }

    </script>

</asp:Content>
<asp:Content ID="header" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="clear-fix">
        <div class="left-column">
            <div class="button-section">
                <asp:LinkButton ID="lbtnEmailCampaign" runat="server" Text="<%$ Resources:GUIStrings, CreateEmailCampaign %>"
                    OnClientClick="return OpenCreateEmailPopup();" CssClass="primarybutton" />
                <asp:HyperLink ID="hplAutoresponders" runat="server" Text="<%$ Resources:GUIStrings, CreateAutoresponder %>"
                    NavigateUrl="~/Autoresponders/ManageAutoResponderDetails.aspx" CssClass="button" />
                <asp:HyperLink ID="hplContactLists" runat="server" Text="<%$ Resources:GUIStrings, CreateContactList %>"
                    NavigateUrl="~/Contacts/ManageContactLists.aspx" CssClass="button" />
                <asp:HyperLink ID="hplLandingPages" runat="server" Text="<%$ Resources:GUIStrings, CreateLandingPages %>"
                    NavigateUrl="#" CssClass="button" onclick="return fn_RedirectToCMS();" />
            </div>
            <asp:Panel ID="pnEmailPreview" runat="server" CssClass="email-preview-container">
                <div class="email-preview-wrapper">
                    <div class="email-preview-overlay"></div>
                    <iframe id="email_preview" class="email-preview"></iframe>
                </div>
                <p class="title">
                    <strong><span id="email_title"></span></strong>
                </p>
				<p>
					<strong>Group:</strong>
					<span id="email_groupName"></span>
				</p>
                <p>
					<strong>Last Modified:</strong>
                    <span id="email_lastModified"></span>
                </p>
            </asp:Panel>
        </div>
        <div class="right-column">
            <div class="form-row month-view">
                <label class="form-label"><a href="javascript://" onclick="return fn_ShowMonthlyView();"><%= GUIStrings.CalendarView %></a></label>
                <div class="form-value">
                    <asp:ImageButton ID="ibtnMonthlyView" runat="server" ImageUrl="~/App_Themes/General/images/calendar-button-month.png" AlternateText="Month View" OnClientClick="return fn_ShowMonthlyView();" Width="26" Height="25" />
                </div>
            </div>
            <UC:EmailCampaigns ID="ucEmailCampaignsScheduled" runat="server" />
            <div class="container-box">
                <div class="clear-fix">
                    <h2><%= GUIStrings.EmailCampaignStatistics %></h2>
                </div>
                <div class="clear-fix">
                    <div class="contact-statistics">
                        <h3><%= GUIStrings.ContactsAdded %></h3>
                        <div class="container-box">
                            <ul>
                                <li>
                                    <asp:Label ID="lblOverall" runat="server" Text="" /><br />
                                    <%= GUIStrings.Overall %>
                                </li>
                                <li>
                                    <asp:Label ID="lblMonthly" runat="server" Text="" /><br />
                                    <%= GUIStrings.Monthly %>
                                </li>
                                <li>
                                    <asp:Label ID="lblWeekly" runat="server" Text="" /><br />
                                    <%= GUIStrings.Weekly %>
                                </li>
                                <li>
                                    <asp:Label ID="lblDaily" runat="server" Text="" /><br />
                                    <%= GUIStrings.Daily %>
                                </li>
                            </ul>
                            <div id="divContactChart"></div>
                        </div>
                    </div>
                    <div class="email-statistics">
                        <h3><%= GUIStrings.YourCampaignSnapshot %></h3>
                     <%--  <asp:UpdatePanel ID="upPanelCampaigns" runat="server" UpdateMode="Conditional">
                           <ContentTemplate>--%>
                        <div class="container-box">                            
                            <div class="form-row">
                                <div class="form-value">
                                    <asp:DropDownList ID="ddlCampaigns" runat="server" OnSelectedIndexChanged="ddlCampaigns_SelectedIndexChanged" AutoPostBack="true" Width="257" />
                                    <asp:DropDownList ID="ddlPeriod" runat="server" OnSelectedIndexChanged="ddlPeriod_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Text="Last 30 Days" />
                                        <asp:ListItem Text="Last 60 Days" />
                                        <asp:ListItem Text="Last 90 Days" />
                                        <asp:ListItem Text="Last 180 Days" />
                                        <asp:ListItem Text="Last 365 Days" />
                                    </asp:DropDownList>
                                </div>
                            </div>                          
                            <div class="graph-container" id="divBarGraphContainer" runat="server">
                                <div id="divEmailStatsChart" class="graph-section"></div>
                                <div class="text-section">
                                    <p>
                                        <asp:Literal ID="ltSentText" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Literal ID="ltOpenedText" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Literal ID="ltClicksText" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Literal ID="ltBouncedText" runat="server" />
                                    </p>
                                </div>
                            </div>
                        </div>
                               <%--</ContentTemplate>
                           <Triggers>
                               <asp:AsyncPostBackTrigger ControlID="ddlCampaigns" />
                                <asp:AsyncPostBackTrigger ControlID="ddlPeriod" />
                           </Triggers>
                               </asp:UpdatePanel>--%>
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
