﻿<%@ Page Language="C#" MasterPageFile="DeveloperConfiguration.Master" AutoEventWireup="true" CodeBehind="EmailProcessorSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.EmailProcessorSettings" StylesheetTheme="General" %>

<asp:Content ID="cntContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="settings-container clear-fix">
        <asp:ValidationSummary ID="vsEmailProcessorSettings" runat="server" ValidationGroup="EmailProcessorSettings" ShowSummary="false" ShowMessageBox="True" />
        <div class="button-row">
            <asp:Button runat="server" ID="btnSave" ValidationGroup="EmailProcessorSettings" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
        </div>
		<div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locEnabled" runat="server" Text="<%$ Resources:SiteSettings, EmailProcessorEnabled %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.EmailProcessorEnabledTooltip %>" alt="" />
            </label>
            <div class="form-value text-value">
                <asp:CheckBox runat="server" ID="cbEnabled" ValidationGroup="EmailProcessorSettings" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSenderEmailOverride" runat="server" Text="<%$ Resources:SiteSettings, SenderEmailOverride %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SenderEmailOverrideTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSenderEmailOverride" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="youremail@yourdomain.com" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locReplyToEmail" runat="server" Text="<%$ Resources:SiteSettings, ReplyToEmail %>" />
                <asp:PlaceHolder ID="phReplyToEmailRequired" runat="server">
                    <span class="req">*</span>
                </asp:PlaceHolder>
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.ReplyToEmailTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtReplyToEmail" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="youremail@yourdomain.com" />
            </div>
           <%-- <asp:RequiredFieldValidator ID="rfvReplyToEmail" runat="server" ControlToValidate="txtReplyToEmail" ValidationGroup="EmailProcessorSettings"
                Display="None" ErrorMessage="<%$ Resources: JSMessages, ReplyToEmailRequired %>" />--%>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locReturnPath" runat="server" Text="<%$ Resources:SiteSettings, ReturnPath %>" />
                <asp:PlaceHolder ID="phReturnPathRequired" runat="server">
                    <span class="req">*</span>
                </asp:PlaceHolder>
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.ReturnPathTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtReturnPath" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="#RETURNPATH# or youremail@yourdomain.com" />
            </div>
            <asp:RequiredFieldValidator ID="rfvReturnPath" runat="server" ControlToValidate="txtReturnPath" ValidationGroup="EmailProcessorSettings"
                Display="None" ErrorMessage="<%$ Resources: JSMessages, ReturnPathRequired %>" />
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locReturnPathDomain" runat="server" Text="<%$ Resources:SiteSettings, ReturnPathDomain %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.ReturnPathDomainTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtReturnPathDomain" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="yourdomain.com" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSmtpServer" runat="server" Text="<%$ Resources:SiteSettings, SmtpServer %>" />
                <asp:PlaceHolder ID="phSmtpServerRequired" runat="server">
                    <span class="req">*</span>
                </asp:PlaceHolder>
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SmtpServerTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSmtpServer" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="smtp.yourdomain.com" />
            </div>
            <asp:RequiredFieldValidator ID="rfvSmtpServer" runat="server" ControlToValidate="txtSmtpServer" ValidationGroup="EmailProcessorSettings"
                Display="None" ErrorMessage="<%$ Resources: JSMessages, SmtpServerRequired %>" />
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSmtpPort" runat="server" Text="<%$ Resources:SiteSettings, SmtpPort %>" />
                <asp:PlaceHolder ID="phSmtpPortRequired" runat="server">
                    <span class="req">*</span>
                </asp:PlaceHolder>
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SmtpPortTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSmtpPort" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="25" />
            </div>
            <asp:RequiredFieldValidator ID="rfvSmtpPort" runat="server" ControlToValidate="txtSmtpPort" ValidationGroup="EmailProcessorSettings"
                Display="None" ErrorMessage="<%$ Resources: JSMessages, SmtpPortRequired %>" />
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSmtpAuthentication" runat="server" Text="<%$ Resources:SiteSettings, SmtpAuthentication %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SmtpAuthenticationTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:CheckBox runat="server" ID="cbSmtpAuthentication" ValidationGroup="EmailProcessorSettings" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSmtpUsername" runat="server" Text="<%$ Resources:SiteSettings, SmtpUsername %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SmtpUsernameTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSmtpUsername" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSmtpPassword" runat="server" Text="<%$ Resources:SiteSettings, SmtpPassword %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SmtpPasswordTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSmtpPassword" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locSmtpEnableSsl" runat="server" Text="<%$ Resources:SiteSettings, SmtpEnableSsl %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.SmtpEnableSslTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:CheckBox runat="server" ID="cbSmtpEnableSsl" ValidationGroup="EmailProcessorSettings" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="locBatchSize" runat="server" Text="<%$ Resources:SiteSettings, BatchSize %>" />
                <asp:PlaceHolder ID="phBatchSizeRequired" runat="server">
                    <span class="req">*</span>
                </asp:PlaceHolder>
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.BatchSizeTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtBatchSize" ValidationGroup="EmailProcessorSettings" CssClass="textBoxes" placeholder="25" />
            </div>
            <asp:RequiredFieldValidator ID="rfvBatchSize" runat="server" ControlToValidate="txtBatchSize" ValidationGroup="EmailProcessorSettings"
                Display="None" ErrorMessage="<%$ Resources: JSMessages, BatchSizeRequired %>" />
        </div>
    </div>
</asp:Content>
