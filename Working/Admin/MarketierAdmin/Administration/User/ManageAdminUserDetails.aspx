﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageAdminUserDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageAdminUserDetails"
    StylesheetTheme="General" ClientIDMode="Static" %>

<%@ Register TagPrefix="UC" TagName="EditAdminUserDetails" Src="~/UserControls/Administration/User/ManageAdminUserDetails.ascx" %>
<asp:Content ID="header" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:EditAdminUserDetails ID="editAdminUserDetail" runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            OnClick="btnSaveUser_Click" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"
            OnClientClick="return ValidateEditUser();" />
        <asp:Button ID="btnSaveAndPermission" runat="server" Text="<%$ Resources:GUIStrings, SaveAndEditPermission %>"
            OnClick="btnSaveAndEditPermission_Click" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAndEditPermission %>"
            OnClientClick="return ValidateEditUser();"  Visible="false"/>
        <a href="ManageAdminUser.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
