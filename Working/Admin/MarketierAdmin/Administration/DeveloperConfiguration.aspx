﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeveloperConfiguration.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.DeveloperConfiguration"
    MasterPageFile="DeveloperConfiguration.master" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".tabs").iAppsTabs();
        });

        function mnuCustomSettings_onItemSelect(sender, eventArgs) {

            var id = eventArgs.get_item().get_id();
            if (id == "cmAdd") {
                grdCustomSettings.get_table().addRow();
            }
            else if (id == "cmDelete") {
                grdCustomSettings.deleteItem(gridItem);
                if (grdCustomSettings.get_table().getRowCount() == 0)
                    grdCustomSettings.get_table().addRow();
            }
        }

        function grdCustomSettings_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            gridItem = eventArgs.get_item();
            mnuCustomSettings.showContextMenuAtEvent(evt);
        }

        function saveCell(itemId, columnField, newValue) {
            var row = grdCustomSettings.GetRowFromClientId(itemId);

            if (row != null) {
                // Check if value was changed
                var oldValue = row.GetMember(columnField).Value;
                if (oldValue != newValue) {
                    // Get column index for SetValue
                    var col = 0;
                    for (var i = 0; i < grdCustomSettings.Table.Columns.length; i++) {
                        if (grdCustomSettings.Table.Columns[i].DataField == columnField) {
                            col = i;
                            break;
                        }
                    }
                    row.SetValue(col, newValue, true);
                }
            }
            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="cntContent" ContentPlaceHolderID="cphContent" runat="server">
    <div class="settings-container clear-fix">
        <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true" ValidationGroup="callback" />
        <div class="button-row">
            <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" />
        </div>
        <div class="tabs clear-fix">
            <ul>
                <li><span><%= GUIStrings.GlobalSettings %></span></li>
                <li><span><%= GUIStrings.GeneralSiteSettings %></span></li>
                <li><span><%= GUIStrings.CustomSettings %></span></li>
            </ul>
            <div class="global-settings">
                <div class="form-row">
                    <label class="form-label">Admin Email:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAdminEmail" runat="server" CssClass="textBoxes  disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Analytics Product Id:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAnalyticsProductId" runat="server" CssClass="textBoxes disabledText disabled" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">AnalyticsAdminVDName:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAnalyticsAdminVDName" runat="server"
                            CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">AnalyticsProduct.Key:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAnalyticsProductKey" runat="server"
                            CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">cache.email:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtcacheemail" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">CMS Product Id:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtCMSProductId" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">CMS Product.Key:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtCMSProductKey" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">CMSAdminVDName:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtCMSAdminVDName" runat="server" CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">CommonLoginVD:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtCommonLoginVD" runat="server" CssClass="textBoxes"
                            ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">CommerceAdminVDName:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txteCommerceAdminVDName" runat="server"
                            CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">CommerceProduct.Key:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txteCommerceProductKey" runat="server"
                            CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Commerce Product Id:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtCommerceProductId" runat="server"
                            CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">ComponentArtScriptControls:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtComponentArtScriptControls"
                            runat="server" CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Context MenuXml:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtContextMenuXml" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">EventLog Application Key:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtFrontEndEventLogApplicationKey"
                            runat="server" CssClass="textBoxes" ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">FilesToOpenInBrowser:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtFilesToOpenInBrowser" runat="server"
                            CssClass="textBoxes" ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">iAPPS_Version:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtiAPPS_Version" runat="server" CssClass="textBoxes  disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">iApps.DateFormat:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtiAppsDateFormat" runat="server" CssClass="textBoxes"
                            ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">IAppsSystemUser Id:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtiAPPSUser" runat="server" CssClass="textBoxes"
                            ReadOnly="false" />
                        <asp:RegularExpressionValidator ID="retxtiAPPSUser" runat="server" ControlToValidate="txtiAPPSUser"
                            ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                            ValidationGroup="callback" ErrorMessage="Please enter a valid Guid in 'iAPPS System User' field"
                            Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">iAppsTimeoutInMinutes:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtiAppsTimeoutInMinutes" runat="server"
                            CssClass="textBoxes" ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Invisible TreeViewNode CssClass:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtInvisibleTreeViewNodeCssClass"
                            runat="server" CssClass="textBoxes" ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Invisible Selected NodeCssClass:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtInvisibleSelectedNodeCssClass"
                            runat="server" CssClass="textBoxes" ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">ISYSBinPath:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtISYSBinPath" runat="server" CssClass="textBoxes  disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">ISYSLicenseKey:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtISYSLicenseKey" runat="server" CssClass="textBoxes  disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">ISYSProductSearchIndex:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtISYSProductSearchIndex" runat="server"
                            CssClass="textBoxes" ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">JQueryGoogleCDN for Admin:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtjQueryGoogleCDN" runat="server"
                            CssClass="textBoxes  disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, MarketierAdminVD %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtMarketierAdminVD" CssClass="textBoxes"
                            ReadOnly="false" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, MarketierProductId %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtMarketierProductId" CssClass="textBoxes textBoxesDisabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">MarketierProduct.Key:</label><div class="form-value">
                        <asp:TextBox ID="txtMarketierProductKey" runat="server"
                            CssClass="textBoxes disabledText disabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">PageAccessXml:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtPageAccessXml" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">PublicSite.URL:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtPublicSiteURL" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Render Controls Attributes In Live:</label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlRenderControlsAttributesInLive" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Render iAPPS SEO Description:</label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlRenderiAPPSSEODescription" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Site Id:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtSiteId" runat="server" CssClass="textBoxes disabledText disabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">Use Cache:</label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlUseCache" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="general-settings">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, AdminSiteID %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAdminSiteID" runat="server" CssClass="textBoxes textBoxesDisabled" ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, CampaignDefaultPageTemplateId %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtCampaignDefaultPageTemplateId" CssClass="textBoxes" />
                        <asp:RegularExpressionValidator ID="retxtCampaignDefaultPageTemplateId" runat="server"
                            ControlToValidate="txtCampaignDefaultPageTemplateId" ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                            ValidationGroup="callback" ErrorMessage="<%$ Resources:JSMessages, PleaseenteravalidGuidinCampaignDefaultPageTemplateIdfield %>"
                            Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, CampaignPageMapNodeId %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtCampaignPageMapNodeId" CssClass="textBoxes" />
                        <asp:RegularExpressionValidator ID="retxtCampaignPageMapNodeId" runat="server" ControlToValidate="txtCampaignPageMapNodeId"
                            ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                            ValidationGroup="callback" ErrorMessage="<%$ Resources:JSMessages, PleaseenteravalidGuidinCampaignPageMapNodeIdfield %>"
                            Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ContactProfileIgnoreProperties %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtContactProfileIgnoreProperties" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, CustomUserProfileColumns %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtCustomUserProfileColumns" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, EnforceUnsubscribeLinkinCampaignEmail %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlEnforceUnsubscribe" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, FileUploadFileIconFolder %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtFileUploadFileIconFolder" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, FileUploadFileIconMapXml %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtFileUploadFileIconMapXml" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, LocalTimeZone %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlLocalTimeZone" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, MarketierContentLibraryId %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtMarketierContentLibraryId" CssClass="textBoxes textBoxesDisabled"
                            ReadOnly="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.MarketierEmailServerClientBasedIP %></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMarketierIP" runat="server" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.MonthlyEmailQuota%></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMonthlyEmailQuota" runat="server" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, UploadCSVFilePath %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtUploadCSVFilePath" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, UploadLogStatus %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtUploadLogStatus" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, UserProfileEmailColumnName %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtUserProfileEmailColumnName" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, TopNCsvRows %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtTopNCsvRows" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, XsltPath %>" /></label>
                    <div class="form-value">
                        <asp:TextBox runat="server" ID="txtXsltPath" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, UnsubscribePageID %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtUnsubscribePageId" runat="server" CssClass="textBoxes" />
                        <asp:RegularExpressionValidator ID="retxtUnsubscribePageId" runat="server" ControlToValidate="txtUnsubscribePageId"
                            ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                            ValidationGroup="callback" ErrorMessage="<%$ Resources:JSMessages, PleaseenteravalidGuidinUnsubscribePageIDfield %>"
                            Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings, OverrideDeletedUser %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlDeletedUser" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="LOverrideiAPPSServiceCheck" runat="server" Text="<%$ Resources:GUIStrings, OverrideiAPPSServiceCheck %>" /></label>
                    <div class="form-value">
                        <asp:CheckBox ID="cbxOverrideiAPPSServiceCheck" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:GUIStrings, FileAttachmentsMaxSizeinKB %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtFileAttachmentsMaxSize" runat="server" CssClass="textBoxes" />
                        <asp:CompareValidator ID="cvtxtFileAttachmentsMaxSize" runat="server" ControlToValidate="txtFileAttachmentsMaxSize"
                            Type="Integer" Operator="DataTypeCheck" ValidationGroup="callback" ErrorMessage="<%$ Resources:JSMessages, PleaseenteravalidnumberinFileAttachmentsMaxSizeinKBfield %>"
                            Text="*"></asp:CompareValidator>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize21" runat="server" Text="<%$ Resources:GUIStrings, OptoutofAllCampaigns %>" /></label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlOptOutCampaigns" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.SenderScoreURL %></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtSenderScoreURL" runat="server" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize22" runat="server" Text="<%$ Resources:GUIStrings, TestEmailDistributionListContactLimit %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTestEmailDistributionListLimit" runat="server" CssClass="textBoxes" />
                        <asp:CompareValidator ID="cvTestEmailDistributionListLimit" runat="server" ControlToValidate="txtTestEmailDistributionListLimit"
                            Type="Integer" Operator="DataTypeCheck" ValidationGroup="callback" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidIntegerForTestEmailDistributionListContactLimit %>"
                            Text="*" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        Include View Email In Browser Link By Default:</label>
                    <div class="form-value text-value">
                        <asp:CheckBox ID="cbIncludeViewEmailInBrowserLinkByDefault" runat="server" CssClass="checkBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        View Email In Browser URL:</label>
                    <div class="form-value">
                        <asp:TextBox ID="txtViewEmailInBrowser" runat="server" CssClass="textBoxes" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        Override Contact:</label>
                    <div class="form-value text-value">
                        <asp:CheckBox ID="chkOverrideContact" runat="server" CssClass="checkBoxes" />
                    </div>
                </div>
            </div>
            <div class="custom-settings">
                <ComponentArt:Menu ID="mnuCustomSettings" SkinID="ContextMenu" runat="server">
                    <Items>
                        <ComponentArt:MenuItem ID="cmAdd" Text="<%$ Resources:GUIStrings, AddSetting %>"
                            Look-LeftIconUrl="cm-icon-add.png">
                        </ComponentArt:MenuItem>
                        <ComponentArt:MenuItem LookId="BreakItem">
                        </ComponentArt:MenuItem>
                        <ComponentArt:MenuItem ID="cmDelete" Text="<%$ Resources:GUIStrings, DeleteSetting %>"
                            Look-LeftIconUrl="cm-icon-delete.png">
                        </ComponentArt:MenuItem>
                    </Items>
                    <ClientEvents>
                        <ItemSelect EventHandler="mnuCustomSettings_onItemSelect" />
                    </ClientEvents>
                </ComponentArt:Menu>
                <ComponentArt:Grid ID="grdCustomSettings" AllowTextSelection="true" EnableViewState="true"
                    EditOnClickSelectedItem="false" AllowEditing="true" ShowHeader="False" CssClass="tabular-grid"
                    KeyboardEnabled="false" ShowFooter="false" SkinID="Default"
                    RunningMode="Client" Width="805" runat="server" PageSize="1000">
                    <Levels>
                        <ComponentArt:GridLevel ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="row"
                            ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell" HeadingCellCssClass="HeadingCell"
                            HeadingCellHoverCssClass="HeadingCellHover" HeadingCellActiveCssClass="HeadingCellActive"
                            HeadingRowCssClass="heading-row" HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="selected-row"
                            GroupHeadingCssClass="GroupHeading" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            SortImageWidth="8" AllowSorting="false" SortImageHeight="7" AllowGrouping="false"
                            AlternatingRowCssClass="alternate-row" EditCellCssClass="EditDataCell" EditFieldCssClass="EditDataField"
                            EditCommandClientTemplateId="EditCommandTemplate" InsertCommandClientTemplateId="InsertCommandTemplate">
                            <Columns>
                                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Key %>" DataField="Key"
                                    DataCellClientTemplateId="KeyTemplate" AllowReordering="False" FixedWidth="true" />
                                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Value %>" DataField="Value"
                                    DataCellClientTemplateId="ValueTemplate" AllowReordering="False"
                                    FixedWidth="true" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="KeyTemplate">
                            <input id="txtKey" class="textBoxes" style="width: 95%;" type="text" value="## DataItem.getCurrentMember().get_value() ##"
                                onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this.value);" />
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="ValueTemplate">
                            <input id="txtValue" class="textBoxes" style="width: 95%;" type="text" value="## DataItem.getCurrentMember().get_value() ##"
                                onblur="saveCell('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this.value);" />
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                    <ClientEvents>
                        <ContextMenu EventHandler="grdCustomSettings_onContextMenu" />
                    </ClientEvents>
                </ComponentArt:Grid>
            </div>
        </div>
    </div>
</asp:Content>
