﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageLinks.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageLinks" StylesheetTheme="General" %>


<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".left-control .group-item").on("click", function () {
                $(this).addClass("selected");
                $(this).siblings().removeClass("selected");
                gridActionsJson.CustomAttributes["SelectedGroup"] = $.trim($(this).find("h4").html());
                $(".manage-links").gridActions("selectNode", $(this).find(".hdnGroupId").val());
            });

            $(".left-control .group-item[objectId='" + gridActionsJson.FolderId + "']").addClass("selected");
            if ($(".left-control .group-item.selected").length > 0)
                $(".left-control .grid-item-button, .right-control .grid-button").show();
            else
                $(".left-control .grid-item-button, .right-control .grid-button").hide();
        });

        function upLinks_OnLoad() {
            $(".manage-links").gridActions({
                objList: $(".link-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upLinks_Callback(sender, eventArgs);
                }
            });
        }

        function upLinks_OnRenderComplete() {
            $(".manage-links").gridActions("bindControls");
            $(".manage-links").iAppsSplitter({ leftScrollSection: ".group-list", maxHeight: 700 });

            if ($(".left-control .group-item.selected").length > 0)
                $(".left-control .grid-item-button").show();
            else
                $(".left-control .grid-item-button").hide();
        }

        function upLinks_OnCallbackComplete() {
            $(".manage-links").gridActions("displayMessage");
        }

        function upLinks_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedGroupId = $(".left-control .group-item.selected .hdnGroupId").val();

            switch (gridActionsJson.Action) {
                case "AddNewLink":
                    doCallback = false;
                    var qs = "GroupId=" + selectedGroupId;
                    OpeniAppsMarketierPopup('ManageLinkDetails', qs, 'fn_RefreshLinks');
                    break;
                case "Edit":
                    doCallback = false;
                    var selectedItemId = gridActionsJson.SelectedItems[0];
                    var qs = "Id=" + selectedItemId + "&GroupId=" + selectedGroupId;
                    OpeniAppsMarketierPopup('ManageLinkDetails', qs, 'fn_RefreshLinks');
                    break;
                case "Delete":
                    doCallback = window.confirm("<%= JSMessages.TemplateDeleteConfirmMessage %>");
                    break;
                case "EditParameters":
                    doCallback = false;
                    var qs = "Parameters=" + escape(eventArgs.selectedItem.getValue("Querystring"));
                    OpeniAppsMarketierPopup('ManagedLinkQuerystringEditor', qs, 'fn_RefreshLinks');
                    break;
            }

            if (doCallback) {
                upLinks.set_callbackParameter(JSON.stringify(gridActionsJson));
                upLinks.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            sender.getItemByCommand("Edit").showButton();
            sender.getItemByCommand("Delete").showButton();
        }

        function fn_RefreshLinks() {
            $(".manage-links").gridActions("callback");
        }

        function fn_OpenLinkGroup(operation) {
            switch (operation) {
                case 'new':
                    $('#<%= hdnSelectedId.ClientID %>').val('');
                    $('#<%= txtName.ClientID %>').val('');
                    break;
                default:
                    var selectedId = $(".left-control .group-item.selected .hdnGroupId").val();
                    var selectedName = $(".left-control .group-item.selected .hdnGroupName").val();
                    $('#<%= hdnSelectedId.ClientID %>').val(selectedId);
                $('#<%= txtName.ClientID %>').val(selectedName);
                break;
        }
        $('#divLinkGroups').iAppsDialog("open");
    }

    function fn_CloseLinkGroup() {
        $('#divLinkGroups').iAppsDialog("close");
        return false;
    }

    function fn_DeleteLinkGroup() {
        var result = window.confirm('<%=  JSMessages.AreYouSureYouWantToDeleteThisGroup %>');
        return false;
    }

    function fn_ShowLink() {
        $('#divAddLinks').iAppsDialog('open');
        return false;
    }

    function fn_HideLink() {
        $('#divAddLinks').iAppsDialog('close');
        return false;
    }

    function fn_AddLink() {
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (Page_ClientValidate("EmailLinks")) {
            if (gridActionsJson.Action.toLowerCase() == "edit") {
                gridActionsJson.CustomAttributes["LinkId"] = selectedItemId;
            }
            else if (gridActionsJson.Action.toLowerCase() == "addnewlink") {
                gridActionsJson.CustomAttributes["LinkId"] = '';
            }
            gridActionsJson.CustomAttributes["Title"] = $("#<%= txtLinkName.ClientID %>").val();
            gridActionsJson.CustomAttributes["UrlOnly"] = $("#<%= txtLinkURL.ClientID %>").val();
            gridActionsJson.CustomAttributes["GroupId"] = $(".left-control .group-item.selected .hdnGroupId").val();
            $(".manage-links").gridActions("callback", "Add");

            fn_HideLink();
        }

        return false;
    }
    </script>
</asp:Content>

<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= GUIStrings.Link %></h1>
    <div class="left-control">
        <div class="tree-header clear-fix">
            <h2>
                <asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, LinkGroups %>" /></h2>
            <div class="tree-actions clear-fix">
                <div class="row clear-fix">
                    <div class="columns">
                        <div class="grid-button">
                            <input type="button" id="btnAddNew" runat="server" class="grid-button" value="<%$ Resources:GUIStrings, AddGroup %>"
                                onclick="return fn_OpenLinkGroup('new');" />
                        </div>
                        <div class="grid-item-button" style="display: none;">
                            <input type="button" id="btnEdit" runat="server" class="grid-item-button edit-properties"
                                value="<%$ Resources:GUIStrings, Edit %>" onclick="return fn_OpenLinkGroup();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:ListView ID="lvLinkGroups" runat="server" ItemPlaceholderID="phLayoutList">
            <LayoutTemplate>
                <div class="group-container">
                    <div class="group-list">
                        <asp:PlaceHolder ID="phLayoutList" runat="server" />
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="group-item clear-fix" objectid="<%# Eval("Id") %>">
                    <div class="link-group">
                        <span><%# Eval("Description") %></span>
                        <input type="hidden" id="hdnGroupId" runat="server" class="hdnGroupId" value='<%# Eval("Id") %>' />
                        <input type="hidden" id="hdnGroupName" runat="server" class="hdnGroupName" value='<%# Eval("Description") %>' />
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
        <div class="iapps-modal link-group-popup" id="divLinkGroups" style="display: none;">
            <div class="modal-header clear-fix">
                <h2>
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, LinkGroup %>" /></h2>
            </div>
            <div class="modal-content clear-fix">
                <asp:ValidationSummary ID="vsLinkGroup" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="LinkGroup" />
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Name %>" /><span class="req">&nbsp;*</span></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtName" runat="server" Width="200" />
                        <asp:HiddenField ID="hdnSelectedId" runat="server" />
                        <asp:RequiredFieldValidator ID="rvName" runat="server" ControlToValidate="txtName" ValidationGroup="LinkGroup"
                            Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseProvideValueForName %>" />
                    </div>
                </div>
            </div>
            <div class="modal-footer clear-fix">
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return fn_CloseLinkGroup();" />
                <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" ValidationGroup="LinkGroup" />
            </div>
        </div>
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upLinks" runat="server" OnClientCallbackComplete="upLinks_OnCallbackComplete"
            OnClientRenderComplete="upLinks_OnRenderComplete" OnClientLoad="upLinks_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvLinks" runat="server" ItemPlaceholderID="phLinkList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            No links found!
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table fat-grid link-list">
                            <asp:PlaceHolder ID="phLinkList" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id") %>'>
                                <div class="large-cell">
                                    <h4 datafield="Title"><%# Eval("Title") %></h4>
                                    <p datafield="UrlOnly"><%# Eval("UrlOnly") %></p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row" datafield="Querystring">
                                        <%# Eval("Querystring") %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="iapps-modal add-links" id="divAddLinks" style="display: none;">
        <div class="modal-header clear-fix">
            <h2><%= GUIStrings.Link %></h2>
        </div>
        <div class="modal-content clear-fix">
            <asp:ValidationSummary ID="vsLinks" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="EmailLinks" />
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Name %><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtLinkName" runat="server" Width="300" />
                    <asp:RequiredFieldValidator ID="rvLinkName" runat="server" ControlToValidate="txtLinkName" ValidationGroup="EmailLinks"
                        Display="None" ErrorMessage="<%$ Resources:JSMessages, Pleaseenteralinkname %>" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.URL %><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtLinkURL" runat="server" Width="300" />
                    <asp:RequiredFieldValidator ID="rvLinkURL" runat="server" ControlToValidate="txtLinkURL" ValidationGroup="EmailLinks"
                        Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseenteralinkURL %>" />
                </div>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <asp:Button ID="btnCancelLink" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return fn_HideLink();" />
            <asp:Button ID="btnSaveLink" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" OnClientClick="return fn_AddLink();" />
        </div>
    </div>
</asp:Content>
