﻿<%@ Page Language="C#" MasterPageFile="~/Administration/DeveloperConfiguration.master" AutoEventWireup="true"
    CodeBehind="ManageSalesforceConfiguration.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageSalesforceConfiguration"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="mf" TagName="MapFields" Src="~/UserControls/Contacts/MapFields.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".tabs").iAppsTabs();
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="tabs">
        <ul>
            <li><span><%= GUIStrings.SalesforcecomAuthentication %></span></li>
            <li><span><%= GUIStrings.ContactFieldMapping %></span></li>
            <li><span><%= GUIStrings.LeadFieldMapping %></span></li>
        </ul>
        <div class="salesforce-authentication">
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.UserName %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtSalesForceUsername" runat="server" Width="200" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Password %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtSaleForcePassword" TextMode="Password" runat="server" Width="200" />
                </div>
            </div>
            <div class="button-row">
                <asp:Button runat="server" ID="btnLogin" Text="<%$ Resources:GUIStrings, Login %>"
                    ToolTip="<%$ Resources:GUIStrings, Login %>" CssClass="primarybutton" OnClick="btnLogin_Onclick" />
            </div>
        </div>
        <div class="salesforce-contacts">
            <mf:MapFields ID="ctlMapContactFields" runat="server" ImportConfigSessionVar="ImportContactSettings"
                OnLoadInputData="ctlMapFields_OnLoadMapData" />
        </div>
        <div class="salesforce-leads">
            <mf:MapFields ID="ctlMapLeadFields" runat="server" ImportConfigSessionVar="ImportLeadSettings"
                OnLoadInputData="ctlMapFields_OnLoadMapData" />
        </div>
    </div>
</asp:Content>
