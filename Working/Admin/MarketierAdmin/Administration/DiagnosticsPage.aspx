﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    StylesheetTheme="General" CodeBehind="DiagnosticsPage.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.DiagnosticsPage" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="diagnostics-page clear-fix">
        <div class="page-header clear-fix">
            <h1>
                Diagnostics Page</h1>
        </div>
        <asp:Repeater ID="rptGroup" runat="server">
            <ItemTemplate>
                <h2><%# Container.DataItem %></h2>
                <asp:Repeater ID="rptTests" runat="server">
                    <ItemTemplate>
                        <div class="<%# DataBinder.Eval(Container.DataItem, "Result") %>">
                            <h3><%# DataBinder.Eval(Container.DataItem, "Name")%></h3>
                            <p>
                                <%# DataBinder.Eval(Container.DataItem, "Description")%>
                            </p>
                            <p>
                                <%# DataBinder.Eval(Container.DataItem, "OutputMessage")%>
                                <%# DataBinder.Eval(Container.DataItem, "HelpText")%>
                                <%# DataBinder.Eval(Container.DataItem, "Link")%> 
                            </p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </ItemTemplate>
        </asp:Repeater>
        <%--<div class="Success">
            <h3>Heading</h3>
            <p>Description</p>
            <p>Message</p>
        </div>
        <div class="Error">
            <h3>Heading</h3>
            <p>Description</p>
            <p>Message</p>
        </div>
        <div class="Info">
            <h3>Heading</h3>
            <p>Description</p>
            <p>Message</p>
        </div>--%>
    </div>
</asp:Content>
