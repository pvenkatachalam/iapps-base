﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageContactFilters.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageContactFilters" StylesheetTheme="General" validateRequest="false" %>

<%@ Register TagPrefix="cf" TagName="ContactListFilter" Src="~/UserControls/Contacts/ContactListFilter.ascx" %>
<%@ Register TagPrefix="sc" TagName="SearchContacts" Src="~/UserControls/Contacts/SearchContacts.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function replacer(key, value) {
            if (key == "Parent" || value == null) return;
            else return value;
        }

        $(window).scroll(function() {
            if($(window).scrollTop() > 600) {
                $('#<%= btnSave2.ClientID %>').show();
            }
            else {
                $('#<%= btnSave2.ClientID %>').hide();
            }
        });

        $(function() {
            var btHtml = stringformat('<img src="{0}" alt="" class="back-to-top" />', ResolveClientUrl("~/App_Themes/General/images/back-to-top.png"));
            var backToTop = $(btHtml).bind("click", function () {
                $('html,body').animate({ scrollTop: 0 }, 'fast');
            });

            $("body").append(backToTop);

            $( window ).scroll(function() {
                var position = $(window).scrollTop();
                if(position == 0)
                {
                    backToTop.fadeOut();
                }else if (position > 100){
                    backToTop.fadeIn();
                }
            });
            fn_ToggleGlobal();
        });

        function fn_ToggleGlobal() {
            if ($('#staticList').is(':checked')) {
                $('#saveAsGloal').removeAttr('checked');
                $("#saveAsGloal").attr("disabled", true);
                
            }
            if ($('#dynamicList').is(':checked')) {
                $("#saveAsGloal").removeAttr("disabled");
            }
        }
    </script>
    <% if (isMac())
       { %>
    <style type="text/css">
        select {
            height: 22px;
        }
    </style>
    <% } %>
</asp:Content>

<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><%= ContactListId == Guid.Empty ? GUIStrings.CreateANewContactList : GUIStrings.EditContactList %> </h1>
        <asp:Button ID="btnSave1" Text="<%$ Resources:GUIStrings, Save %>" runat="server" OnClientClick="return Save();" CssClass="primarybutton position-bottom savebutton" OnClick="btnSave1_Click" />
        <asp:HyperLink ID="hplCancel" Text="<%$ Resources:GUIStrings, Cancel %>" runat="server" CssClass="button" NavigateUrl="~/Contacts/ManageContactLists.aspx" />
        <asp:LinkButton ID="lbtnExportContacts" Text="<%$ Resources:GUIStrings, ExportContactList %>" runat="server" CssClass="export-csv" />
    </div>
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="container-box clear-fix">
        <div class="clear-fix">
            <h2><%= GUIStrings.ListDetails %></h2>
        </div>
        <div class="horizontal-padding clear-fix">
            <div class="clear-fix display-table">
                <div class="first-column" style="border-right: 0;">
                    <div class="form-row">
                        <label class="form-label"><%= GUIStrings.ListName1 %></label>
                        <div class="form-value">
                            <asp:TextBox ID="txtName" runat="server" CssClass="textBoxes" placeholder="Name your list" Width="325" CausesValidation="true" required />
                            <asp:RequiredFieldValidator Style="color: red" runat="server" ID="rfvTxtName" ControlToValidate="txtName" ErrorMessage="Required"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div class="second-column">
                    <asp:PlaceHolder ID="phGroupName" runat="server" Visible="false">
                        <div class="form-row">
                            <label class="form-label"><%= GUIStrings.GroupName %></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlGroupName" runat="server" Width="285" />
                            </div>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
            <asp:PlaceHolder runat="server" ID="listDetails">
                <div class="clear-fix display-table">
                    <div class="first-column">
                        <div runat="server" id="divExistingFilters" class="form-row">
                            <label class="form-label"><%= GUIStrings.ApplyPreExistingFilterToList %></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlExistingFilter" runat="server" OnSelectedIndexChanged="ddlExistingFilter_SelectedIndexChanged" AutoPostBack="true" Width="267">
                                    <asp:ListItem Text="None" Value="-1" Selected="True" />
                                </asp:DropDownList>
                                <asp:LinkButton Style="display: inline-block; margin-left: 5px;" ID="lnkDeleteOption" runat="server" Visible="false" OnClick="lnkDeleteOption_Click" CausesValidation="false">Delete selected filter</asp:LinkButton>
                            </div>
                        </div>

                        <p style="margin-top: 20px;">
                            <asp:RadioButton GroupName="listType" ID="dynamicList" Text="<%$ Resources:GUIStrings, DynamicList %>" Font-Bold="true" runat="server" Checked="true" ClientIDMode="Static" onclick="fn_ToggleGlobal();" />
                        </p>
                        <p class="instructions">
                            <asp:Label AssociatedControlID="dynamicList" ID="dynamicListText" runat="server" Text="<%$ Resources:GUIStrings, DynamicListHelpText %>" />
                        </p>
                        <p>
                            <asp:RadioButton GroupName="listType" ID="staticList" Text="<%$ Resources:GUIStrings, StaticList %>" Font-Bold="true" runat="server" ClientIDMode="Static" onclick="fn_ToggleGlobal();" />
                        </p>
                        <p class="instructions">
                            <asp:Label AssociatedControlID="staticList" ID="staticListText" runat="server" Text="<%$ Resources:GUIStrings, StaticListHelpText %>" />
                        </p>
                    </div>
                    <div class="second-column">
                        <p>
                            <asp:CheckBox runat="server" ID="saveForFuture" Text="<%$ Resources:GUIStrings, SaveThisListAsAFilterSetForFutureUse %>" Font-Bold="true" />
                        </p>
                        <p class="instructions">
                            <asp:Label AssociatedControlID="saveForFuture" ID="futureLabel" runat="server" Text="<%$ Resources:GUIStrings, SaveThisListAsAFilterSetForFutureUseHelpText %>" />
                        </p>
                        <p id="pSaveAsGlobal">
                            <asp:CheckBox runat="server" ID="saveAsGloal" Text="<%$ Resources:GUIStrings, SaveThisListAsGlobal %>" Font-Bold="true" ClientIDMode="Static" />
                        </p>
                        <p class="instructions">
                            <asp:Label AssociatedControlID="saveAsGloal" ID="Label1" runat="server" Text="<%$ Resources:GUIStrings, SaveThisListAsGlobalHelpText %>" />
                        </p>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
    <div runat="server" style="" id="filterDiv">
        <cf:ContactListFilter ID="clFilter" runat="server" />
    </div>
    <div style="text-align: right;">
        <asp:Button ID="btnSave2" OnClientClick="return Save();" OnClick="btnSave1_Click" Text="<%$ Resources:GUIStrings, Save %>" runat="server" CssClass="primarybutton position-bottom savebutton" style="display: none;" />
    </div>
    <div>
        <sc:SearchContacts ID="scCtrl" runat="server" />
    </div>

    <script type="text/javascript">
        //var mrktContactProperties = %= RenderProfileJSON() %> ;
        //var mrktContactAttribute = %= RenderContactAttributesJSON() %> ;
        //var mrktCustomerGroups = %= RenderCustomerGroupsJSON() %> ;
        //var mrktForms = %= RenderFormsJSON() %> ;
        //var mrktSiteGroup = %= RenderSiteGroupJSON() %> ;
        //var mrktTags = %= RenderTagsJSON() %>;
        //var mrktSecurityLevels = %= RenderSecurityLevelJSON() %>;
        //var mrktWatchedEvents = %= RenderWatchedEventsJSON() %>;
        // mrktiAPPSContact = %= RenderContactSource() %>;
        //var mrktSites = %= RenderSitesJSON() %>;
        //var mrktSiteAttributes = %= RenderSiteAttributesJSON() %> ;
        var isPostback = "<%= IsPostBack %>" == "True";
        var mrktStaticListSearchObject = <%= RenderStaticListSearchObject(30)%>;
    </script>
</asp:Content>
