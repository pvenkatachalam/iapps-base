﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageContactDetails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageContactDetails" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="Attributes" Src="~/UserControls/General/AssignAttributes.ascx" %>
<%@ Register TagPrefix="UC" TagName="Address" Src="~/UserControls/General/AddressDetail.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
    	$(document).ready(function () {
            $(".tabs").iAppsTabs();
            $(".profile-container").on("click", ".toggle-contact-details", function () {
                var $container = $("#divAdditionalProperties");
                if ($container.hasClass("expanded")) {
                    $container.hide().removeClass("expanded");
                    $(this).text("Show additional details");
                }
                else {
                    $container.show().addClass("expanded");
                    $(this).text("Hide additional details");
                }
            });
        });

        function fn_ShowAdditionalProperties(itemClicked) {
            if ($('#divAdditionalProperties').hasClass('expanded')) {
                $('#divAdditionalProperties').hide().removeClass('expanded');
            }
            else {
                $('#divAdditionalProperties').show().addClass('expanded');
            }
        }

        function fn_AssignTags() {
            OpeniAppsAdminPopup("AssignIndexTermsPopup", "DisableEditing=true&SaveTags=true&ObjectTypeId=14&ObjectId=" + $("#hdnContactId").val());
            return false;
        }

        function fn_DeleteContact() {
            return confirm('<%= JSMessages.AreYouSureYouWantToDeleteThisContact %>');
        }

    	function fn_ActivateContact() {
    		return confirm('<%= JSMessages.AreYouSureYouWantToActivateThisContact %>');
    	}

    	function fn_OptOutOfEmails() {
    		var doCallback = false;

    		if ($('.emailsReceived').find(':checked').length == 0) {
    			alert('<%= JSMessages.PleaseSelectAtLeastOneEmail %>');
    		}
    		else {
    			doCallback = confirm('<%= JSMessages.AreYouSureYouWantToUnsubscribeThisContactFromTheSelectedEmailCampaigns %>');
    		}

    		return doCallback;
        }

        function fn_OptOutOfAllEmails() {
            return confirm('<%= JSMessages.AreYouSureYouWantToUnsubscribeThisContactFromAllEmailCampaigns %>');
        }

    	function fn_Resubscribe() {
    		var doCallback = false;

    		if ($('.emailsReceived').find(':checked').length == 0) {
    			alert('<%= JSMessages.PleaseSelectAtLeastOneEmail %>');
    		}
    		else {
    			doCallback = confirm('<%= JSMessages.AreYouSureYouWantToResubscribeThisContactToTheSelectedEmailCampaigns %>');
    		}

    		return doCallback;
    	}

        function fn_ChangeProfilePicture() {
            OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "fn_SelectImage");
        }

        function fn_SelectImage() {
            imageUrl = popupActionsJson.CustomAttributes.ImageUrl;
            imageFileName = popupActionsJson.CustomAttributes.ImageFileName;
            imageId = popupActionsJson.SelectedItems[0];

            imageUrl = imageUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
            imageUrl = imageUrl.replace(new RegExp(imageFileName, "gi"), "thumbs/thumb_" + imageFileName);
            $("#imgProfile").prop("src", imageUrl);
            $("#hdnImageId").val(imageId);
        }

        function fn_ClearImageUrlValue() {
            $("#imgProfile").prop("src", "/Admin/App_Themes/General/images/no-photo.png");
            $("#hdnImageId").val("");
        }
        var distributionListWindow;
        function fn_OpenAssignToDistributionLists() {
        	distributionListWindow = OpeniAppsMarketierPopup("AssignToDistributionList", "ContactId=" + $("#hdnContactId").val(), "fn_ContactListCallback");
            return false;

        }
        var listAdded = false;
        var errorMessage = "";
        function fn_ContactListCallback() {
            if (listAdded) {
                __doPostBack('<%= btnReloadContactLists.UniqueID %>', '')
            } else {
                ShowiAppsNotification(errorMessage, false, true);
            }
        }

    	function fn_EmailSearch_KeyPress(e) {
    		var keycode = (event.keyCode ? event.keyCode : event.which);

    		if ((keycode == '8' || keycode == '46') && $(e.currentTarget).val().trim() == '') {
    			$('#<%= ibEmailSearch.ClientID %>').click();
    		}

    		return false;
    	}

    	function fn_ContactListSearch_KeyPress(e) {
    		var keycode = (event.keyCode ? event.keyCode : event.which);

    		if ((keycode == '8' || keycode == '46') && $(e.currentTarget).val().trim() == '') {
    			$('#<%= ibContactListSearch.ClientID %>').click();
    		}

			return false;
    	}

    	function clientValidate() {
    		var contactId = $('#<%= hdnContactId.ClientID %>').val();
    		var email = $('#<%= txtEmail.ClientID %>').val();
    		var result = MarketierCallback.ValidateEmailUniqueness(contactId, email);
    		var isValid = false;

    		var buttons = {
    			add: $('#btnDialogAdd'),
    			addOverwrite: $('#btnDialogAddOverwrite'),
    			ok: $('#btnDialogOk')
    		};


    		switch (result) {
    			case 1: // Unique or same contact
    				isValid = true;
    				$('#<%= hdnOverwriteContact.ClientID %>').val('true');
    				break;
    			case 2: // Exists on another site, overridable
    				fn_ShowConfirmationDialog('<%= GUIStrings.ContactAddOverwriteWarningMessage %>', [buttons.add, buttons.addOverwrite]);
    				break;
    			case 3: // Exists on another site, not overridable
    				fn_ShowConfirmationDialog('<%= GUIStrings.ContactExistsOnAnotherSiteWarning %>', [buttons.add]);
    				break;
    			case 4: // Exists on current site
    				fn_ShowConfirmationDialog('<%= GUIStrings.ContactExistsOnCurrentSiteWarning %>', [buttons.ok]);
    				break;
    		}

    		return isValid;
    	}

    	function fn_ShowConfirmationDialog(message, buttons) {
    		$('#divExistingContactDialog').find('.modal-content').html(message);

    		for (var i = 0; i < buttons.length; i++) {
    			buttons[i].show();
    		}

    		$('#divExistingContactDialog').iAppsDialog('open');
    	}

    	function fn_LoadExistingContact() {
    		__doPostBack('<%= btnLoadExistingContact.UniqueID %>', '');
    	}

    	function fn_AddContact(overwrite) {
    		if (overwrite == true) {
    			$('#<%= hdnOverwriteContact.ClientID %>').val('true');
    		}

			__doPostBack('<%= btnSave.UniqueID %>', '')
		}

    	function fn_CloseDialog() {
    		$('#divExistingContactDialog').iAppsDialog('close');
    	}

    	function btnSave_ClientClick(e) {
    		var doCallback = false;

    		if (SaveAttributeValues() && clientValidate()) {
    			doCallback = true;
    		}

    		return doCallback;
    	}

    	function RemoveSelectedValue(e) {
    		$(e.currentTarget).parent().find('input[type="text"]').val('');
    	}

    	var selectedSiteId = null;
    	var selectedSiteName = null;
    	function OpenSiteSelector(e) {
    		OpeniAppsAdminPopup('SelectSitePopup', null, 'fn_SetSelectedSite');
    	}

    	function fn_SetSelectedSite()
    	{
    		if (selectedSiteId != null) {
    			$('#<%= hdnSelectedSiteId.ClientID %>').val(selectedSiteId);
    			$('#<%= txtPrimarySite.ClientID %>').val(selectedSiteName);
    		}
		}

    </script>
</asp:Content>
<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1 class="default">
            <asp:Literal ID="litPageHeader" runat="server" /></h1>
        <asp:Button ID="btnSave" runat="server" OnClientClick="return btnSave_ClientClick();" OnClick="btnSave_Click" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" />
		<asp:HyperLink ID="hplCancel" Text="<%$ Resources:GUIStrings, Close %>" runat="server" CssClass="button" NavigateUrl="~/Contacts/ManageContacts.aspx" />
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="container-box clear-fix">
        <div class="clear-fix">
            <h2 class="profile-image">
                <asp:Literal ID="litContactName" runat="server" /></h2>
        </div>
        <div class="clear-fix profile-container">
            <div class="columns one">
                <asp:Image ID="imgProfile" runat="server" ImageUrl="~/App_Themes/General/images/no-photo.png" BorderWidth="3" ClientIDMode="Static" />
                <asp:HiddenField ID="hdnImageId" runat="server" ClientIDMode="Static" />
                <p class="edit-image"><a href="javascript://" onclick="fn_ChangeProfilePicture();" class="edit"><%= GUIStrings.EditPhoto %></a></p>
                <p class="remove-image"><a href="javascript: //" onclick="fn_ClearImageUrlValue()" class="remove"><%= GUIStrings.Remove %></a></p>
            </div>
            <div class="columns two item-row-section">
                <asp:ValidationSummary ID="validationSummary" runat="server" ShowMessageBox="true" ShowSummary="false" />
                <div class="form-row">
                    <label>
                        <%= GUIStrings.Email %>
                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Text="*" Display="Dynamic"
							ErrorMessage="<%$ Resources:JSMessages, EmailisRequired %>" CssClass="validation-error" />
                        <asp:RegularExpressionValidator ID="revEmail" ControlToValidate="txtEmail" ErrorMessage="<%$ Resources:JSMessages, EmailNotValid %>" Display="Dynamic"
                            Text="*" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*" CssClass="validation-error" />							 
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtEmail" runat="server" />
                        <asp:HiddenField ID="hdnContactId" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
                <div class="form-row">
                    <label>
                        <%= GUIStrings.FirstName %>
                        <asp:RegularExpressionValidator runat="server" ID="revFirstName" ControlToValidate="txtFirstName" ValidationExpression="^[^&<>]+$" 
							ErrorMessage="<%$ Resources:JSMessages, Invalidcharactersinfirstname %>" Text="*" CssClass="validation-error" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtFirstName" runat="server" />
                    </div>
                </div>
				<div class="form-row">
                    <label>
                        <%= GUIStrings.MiddleName %>
                        <asp:RegularExpressionValidator runat="server" ID="revMiddleName" ControlToValidate="txtMiddleName" ValidationExpression="^[^&<>]+$" 
							ErrorMessage="<%$ Resources:JSMessages, Invalidcharactersinmiddlename %>" Text="*" CssClass="validation-error" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMiddleName" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label>
                        <%= GUIStrings.LastName %>
                        <asp:RegularExpressionValidator runat="server" ID="revLastName" ControlToValidate="txtLastName" CssClass="validation-error"
                            Text="*" ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:JSMessages, Invalidcharactersinlastname %>" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtLastName" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label>
                        <%= GUIStrings.CompanyName %>
                        <asp:RegularExpressionValidator runat="server" ID="revCompanyName" ControlToValidate="txtCompanyName" CssClass="validation-error"
                            ValidationExpression="^[^&<>]+$" ErrorMessage="<%$ Resources:JSMessages, InvalidcharactersinCompanyName %>" Text="*" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtCompanyName" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label>
                        <%= GUIStrings.HomePhone %>
                        <asp:RegularExpressionValidator ID="revHomePhone" runat="server" ControlToValidate="txtHomePhone" CssClass="validation-error" Text="*" 
                            ValidationExpression="^(\+?\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ErrorMessage="<%$ Resources:JSMessages, InvalidCharactersInHomePhone %>" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtHomePhone" runat="server" CssClass="phoneNumber" />
                    </div>
                </div>
                <div class="form-row">
                    <label>
                        <%= GUIStrings.MobilePhone %>
                        <asp:RegularExpressionValidator ID="revMobilePhone" runat="server" ControlToValidate="txtMobilePhone" Text="*" CssClass="validation-error"
							ValidationExpression="^(\+?\d{1,2}\s?)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ErrorMessage="<%$ Resources:JSMessages, InvalidCharactersInMobilePhone %>" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMobilePhone" runat="server" CssClass="phoneNumber" />
                    </div>
                </div>
                <div class="form-row">
                    <label><%= GUIStrings.EmailNotification %></label>
                    <div class="form-value">
                        <asp:RadioButtonList ID="rblEmailNotification" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Text="Yes" Selected="True" />
                            <asp:ListItem Text="No" />
                        </asp:RadioButtonList>
                    </div>
                </div>
				<iAppsControls:SecurityPanel ID="spRestrictedProperties" runat="server" PageName="ManageContactDetails" PanelName="pnlContactProperties">
					<ContentTemplate>
						<div class="form-row">
							<label><%= GUIStrings.PrimarySite %></label>
							<div class="form-value">
								<asp:HiddenField runat="server" ID="hdnSelectedSiteId" Value="00000000-0000-0000-0000-000000000000" />
								<div class="value-selector">
									<img class="open-button" src="/MarketierAdmin/App_Themes/General/images/browse-icon.png" onclick="OpenSiteSelector(this)" />
									<asp:TextBox ID="txtPrimarySite" runat="server" placeholder="Select site" ReadOnly="true" />
									<img class="remove-button" src="/MarketierAdmin/App_Themes/General/images/cm-icon-delete.png" onclick="RemoveSelectedValue(event)" />
								</div>
							</div>
						</div>
					</ContentTemplate>
				</iAppsControls:SecurityPanel>
                <div id="divAdditionalProperties" class="additional-properties">
                    <div class="form-row">
                        <label><%= GUIStrings.Gender %></label>
                        <div class="form-value">
                            <asp:RadioButtonList ID="rblGender" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem Text="Male" Value="M" Selected="True" />
                                <asp:ListItem Text="Female" Value="F" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="form-row">
                        <label>
                            <%= GUIStrings.BirthDate %>
                            <asp:CompareValidator ID="cvBirthDate" runat="server" Operator="DataTypeCheck" Type="Date" CssClass="validation-error"
                                ControlToValidate="txtBirthDate" Text="*" ErrorMessage="<%$ Resources:JSMessages, Birthdatehasinvalidformat %>" />
                        </label>
                        <div class="form-value">
                            <asp:TextBox ID="txtBirthDate" runat="server" />
                        </div>
                    </div>

                    <UC:Address ID="ucAddress" runat="server" EnableValidation="false" />
                    <UC:Attributes ID="ucAttributes" runat="server" Category="Contact" />
                </div>
                <p class="align-right">
                    <a href="#" class="toggle-contact-details">Show additional details</a>
                </p>
            </div>
            <div class="columns three">
                <asp:Panel runat="server" ID="pnlLastModifiedInfo">
                    <p class="star emphasize"><%= GUIStrings.Created %></p>
                    <p>
                        <asp:Literal ID="litCreatedDate" runat="server" Text="10 March, 2014" />
                    </p>
                    <p class="star emphasize second"><%= GUIStrings.Modified %></p>
                    <p>
                        <asp:Literal ID="litModifiedDate" runat="server" Text="13 March, 2014" />
                    </p>
                    <p class="star emphasize second"><%= GUIStrings.ContactType %></p>
                    <p>
                        <asp:Literal ID="ltContactType" runat="server" Text="[Lead]" />
                    </p>
					<p class="second position-bottom">
						<asp:Button ID="btnFullOptOut" runat="server" OnClick="btnOptOutAll_Click" OnClientClick="return fn_OptOutOfAllEmails();" CssClass="button dark" Text="<%$ Resources:GUIStrings, OptOutOfALLEmails %>" />
						<asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" OnClientClick="return fn_DeleteContact();" CssClass="button dark" Text="<%$ Resources:GUIStrings, DeleteContact %>" />
						<asp:Button ID="btnActivate" runat="server" OnClick="btnActivate_Click" OnClientClick="return fn_ActivateContact();" CssClass="button dark" Text="<%$ Resources:GUIStrings, ActivateContact %>" Visible="false" />
						<asp:Button ID="btnAssignTags" runat="server" OnClientClick="return fn_AssignTags();" CssClass="button dark" Text="<%$ Resources:GUIStrings, AssignTags %>" />
					</p>
                </asp:Panel>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlSubscriptions" runat="server" class="clear-fix tabs">
        <ul>
            <li><span><a href="javascript://"><%= GUIStrings.EmailsReceived %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.ContactLists %></a></span></li>
        </ul>
		<asp:Panel ID="pnlEmailsReceived" runat="server" CssClass="clear-fix" DefaultButton="ibEmailSearch">
			<asp:UpdatePanel ID="upnlEmailsReceived" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<div class="white-box">
						<div class="clear-fix">
							<asp:Button ID="btnOptOut" runat="server" OnClick="btnOptOut_Click" OnClientClick="return fn_OptOutOfEmails();" Text="<%$ Resources:GUIStrings, OptOut %>" CssClass="button minus" />
							<asp:Button ID="btnOptIn" runat="server" OnClick="btnOptIn_Click" OnClientClick="return fn_Resubscribe();" Text="<%$ Resources:GUIStrings, OptIn %>" CssClass="button plus" />
							<asp:Button ID="btnOptOutAll" runat="server" OnClick="btnOptOutAll_Click" OnClientClick="return fn_OptOutOfAllEmails();" 
								Text="<%$ Resources:GUIStrings, OptOutOfALLEmails %>" CssClass="primarybutton add" style="float: right;" />
							<div class="search-box">
								<asp:TextBox ID="txtEmailSearch" runat="server" ToolTip="Search" CssClass="search-text textBoxes" onkeyup="return fn_EmailSearch_KeyPress(event)" />
								<asp:ImageButton ID="ibEmailSearch" runat="server" ImageUrl="/Admin/App_Themes/General/images/search-button.png" CssClass="search-button" OnClick="ibEmailSearch_Click" />
							</div>
						</div>
						<hr />
						<div class="emailsReceived scroll-box">
							<asp:Repeater runat="server" ID="rptEmailsRecieved" OnItemDataBound="rptEmailsRecieved_ItemDataBound">
								<ItemTemplate>
									<div>
										<asp:HiddenField runat="server" ID="hdnCampaignId" Visible="false" />
										<asp:CheckBox ID="chkCampaign" runat="server" />
										<span style="float: right; padding-right: 20px; width: 100px; text-align: right;"><asp:Literal ID="ltLastSendDate" runat="server" /></span>
										<span style="float: right; font-weight: bold; "><asp:Literal ID="ltStatus" runat="server" /></span>
									</div>
								</ItemTemplate>
							</asp:Repeater>
							<asp:PlaceHolder ID="phNoEmailsRecieved" runat="server" Visible="false">
								<p class="no-items"><%= GUIStrings.ThisContactHasNotRecievedAnyEmails %></p>
							</asp:PlaceHolder>
						</div>
					</div>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="ibEmailSearch" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
		</asp:Panel>
        <asp:Panel ID="pnlContactLists" runat="server"  CssClass="clear-fix" DefaultButton="ibContactListSearch">
			<asp:UpdatePanel ID="upnlContactLists" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<div class="clear-fix optout-utility">
					</div>
					<div class="white-box">
						<div class="clear-fix">
							<asp:Button ID="btnRemoveFromList" runat="server" OnClick="btnRemoveFromList_Click" Text="<%$ Resources:GUIStrings, RemoveFromList %>" CssClass="button minus" />
							<asp:Button ID="btnAddToContactList" runat="server" OnClientClick=" return fn_OpenAssignToDistributionLists();" Text="<%$ Resources:GUIStrings, AddContactToAList %>" 
								CssClass="primarybutton add" style="float: right;" />
							<asp:Button ID="btnReloadContactLists" runat="server" OnClick="btnReloadContactLists_Click" style="display: none; float: right;" />
							<div class="search-box">
								<asp:TextBox ID="txtContactListSearch" runat="server" ToolTip="Search" CssClass="search-text textBoxes" onkeyup="return fn_ContactListSearch_KeyPress(event)" />
								<asp:ImageButton ID="ibContactListSearch" runat="server" ImageUrl="/Admin/App_Themes/General/images/search-button.png" CssClass="search-button" OnClick="ibContactListSearch_Click" />
							</div>
						</div>
						<hr />
						<div class="scroll-box">
							<asp:Repeater runat="server" ID="rptContactLists">
								<ItemTemplate>
									<div>
										<asp:HiddenField runat="server" ID="hdnItemId" Visible="false" Value='<%# Eval("Id") %>' />
										<asp:CheckBox ID="chkItem" runat="server" Text='<%# Eval("Title") %>' />
									</div>
								</ItemTemplate>
							</asp:Repeater>
							<asp:PlaceHolder ID="phContactNotIncluded" runat="server" Visible="false">
								<p class="no-items"><%= GUIStrings.ThisContactIsNotIncludedInAnyList %></p>
							</asp:PlaceHolder>
						</div>
					</div>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="ibContactListSearch" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
        </asp:Panel>
    </asp:Panel>
	<div class="button-row">
		<asp:Button ID="btnSaveBottom" runat="server" OnClientClick="return btnSave_ClientClick();" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" />
	</div>
	<asp:HiddenField ID="hdnOverwriteContact" runat="server" Value="false" />
	<asp:Button runat="server" ID="btnLoadExistingContact" OnClick="btnLoadExistingContact_Click" style="display: none;" />

	<div class="iapps-modal" id="divExistingContactDialog" style="display: none;">
		<div class="modal-header clear-fix">
			<h2><%= GUIStrings.Warning %></h2>
		</div>
		<div class="modal-content clear-fix">
			
		</div>
		<div class="modal-footer clear-fix">
			<input type="button" id="btnDialogCancel" class="button" value="<%= GUIStrings.Cancel %>" onclick="fn_CloseDialog()" />
			<input type="button" id="btnDialogOk" class="primarybutton" value="<%= GUIStrings.Ok %>" onclick="fn_LoadExistingContact()" style="display: none;" />
			<input type="button" id="btnDialogAdd" class="primarybutton" value="<%= GUIStrings.Add %>" onclick="fn_AddContact(false)" style="display: none;" />
			<input type="button" id="btnDialogAddOverwrite" class="primarybutton" value="<%= GUIStrings.AddOverwrite %>" onclick="fn_AddContact(true)" style="display: none;" />
		</div>
	</div>

</asp:Content>
