﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageContactLists.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageContactLists" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="GroupSelector" Src="~/UserControls/General/GroupSelector.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upContactLists_OnLoad() {
            $(".manage-contact-list").gridActions({
                objList: $(".manage-contact-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upContactLists_Callback(sender, eventArgs);
                }
            });
        }

        function upContactLists_OnRenderComplete() {
            $(".manage-contact-list").gridActions("bindControls");
            $(".manage-contact-list").iAppsSplitter({ leftScrollSection: ".group-list" });
        }

        function upContactLists_OnCallbackComplete() {
            $(".manage-contact-list").gridActions("displayMessage");
        }

        function LoadItemsGrid() {
            $(".manage-contact-list").gridActions("selectNode", treeActionsJson.FolderId);

        }

        function upContactLists_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";

            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("Title");

            switch (gridActionsJson.Action) {
                case "AssignTags":
                    var doCallback = false;
                    OpeniAppsAdminPopup('AssignIndexTermsPopup', "DisableEditing=true&SaveTags=true&ObjectTypeId=22&ObjectId=" + selectedItemId);
                    break;
                case "MoveContacts":
                    doCallback = false;
                    var queryString = "ContactListId=" + selectedItemId
                    OpeniAppsMarketierPopup('MoveContacts', queryString, "RefreshContactLists");
                    break;
                case "Delete":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisContactList %>");
                break;
            case "CreateTemplate":
                alert('');
                break;
            case "Export":
                break;
            case "Preview":
                doCallback = false;
                alert('');
                break;
            case "Edit":
                doCallback = false;
                window.location = "/marketieradmin/Contacts/ManageContactFilters.aspx?Id=" + selectedItemId;
                break;
            case "EditProperties":
                doCallback = false;
                alert('');
                break;
            case "CreateContactList":
                doCallback = false;
                window.location = "/marketieradmin/Contacts/ManageContactFilters.aspx?Id=00000000-0000-0000-0000-000000000000&FolderId=" + treeActionsJson.FolderId;
                break;
        }

        if (doCallback) {

            upContactLists.set_callbackParameter(JSON.stringify(gridActionsJson));
            upContactLists.callback();
        }
    }

    function Grid_ItemSelect(sender, eventArgs) {
        var status = eventArgs.selectedItem.getValue("Status");
        var isGlobal = eventArgs.selectedItem.getValue("IsGlobal");
        var fromParent = eventArgs.selectedItem.getValue("FromParent") == "true";
        var isSystem = eventArgs.selectedItem.getValue("IsSystem") == "true";

        if (treeActionsJson.NodeType == 0) {
            sender.getItemByCommand("Edit").hideButton();
            sender.getItemByCommand("Delete").hideButton();
            sender.getItemByCommand("AssignTags").hide();
            sender.getItemByCommand("Export").hide();
            sender.getItemByCommand("Delete").hide();
        }

        if (isSystem || fromParent)
            sender.getElementByName("more-actions").hide();
    }

    function RefreshContactLists() {
        $(".manage-contact-list").gridActions("callback", { refreshTree: true });
    }

    </script>
</asp:Content>

<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%=  GUIStrings.ManageContactLists %></h1>
    <div class="left-control">
        <UC:GroupSelector ID="contactGroups" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upContactLists" runat="server" OnClientCallbackComplete="upContactLists_OnCallbackComplete"
            OnClientRenderComplete="upContactLists_OnRenderComplete" OnClientLoad="upContactLists_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvContactLists" runat="server" ItemPlaceholderID="phContactList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <%= GUIStrings.NoContactLists %>
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table fat-grid contact-list">
                            <asp:PlaceHolder ID="phContactList" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="collection-row clear-fix grid-item" objectid='<%# Eval("ContactListId") %>'>
                                <div class="large-cell">
                                    <h4 datafield="Title">
                                        <asp:Literal ID="ltContactTitle" runat="server" />
                                    </h4>
                                    <p>
                                        <asp:Literal ID="ltListType" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Literal ID="ltContactDescription" runat="server" />
                                    </p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <asp:Literal ID="ltCreated" runat="server" />
                                    </div>
                                    <div class="child-row">
                                        <asp:Label ID="lblContacts" runat="server" CssClass="left-margin" />
                                    </div>
                                    <div class="child-row">
                                        <asp:Label ID="lblStatus" runat="server" CssClass="left-margin" />
                                        <input type="hidden" id="hdnIsGlobal" runat="server" datafield="IsGlobal" />
                                        <input type="hidden" id="hdnFromParent" runat="server" datafield="FromParent" />
                                        <input type="hidden" id="hdnIsSystem" runat="server" datafield="IsSystem" />
                                        <input type="hidden" id="hdnStatus" runat="server" datafield="Status" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
        <iAppsControls:GridActions ID="gridActionsBottom" runat="server" />
    </div>
</asp:Content>
