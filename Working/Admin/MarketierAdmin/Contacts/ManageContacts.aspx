﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageContacts.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageContacts" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
    	function upContacts_OnLoad() {
            $(".contacts-list").gridActions({
                objList: $(".item-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upContacts_Callback(sender, eventArgs);
                }
            });
		}

        function upContacts_OnRenderComplete() {
        	$(".contacts-list").gridActions("bindControls");
        }

        function upContacts_OnCallbackComplete() {
        	$(".contacts-list").gridActions("displayMessage");
        }

        function upContacts_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];

            switch (gridActionsJson.Action) {
            	case 'AddNew':
            		doCallback = false;
                    window.location = jmarketierAdminURL + '/Contacts/ManageContactDetails.aspx';
                    break;
                case 'Import':
                	doCallback = false;
                	window.location = jmarketierAdminURL + '/Contacts/ImportContacts.aspx';
                    break;
            	case 'Edit':
            		var qs = "Id=" + selectedItemId;
            		doCallback = false;
            		switch (gridActionsJson.TabIndex) {
            			case 1:
            				window.location = jmarketierAdminURL + '/Contacts/ManageContactDetails.aspx?' + qs;
            				break;
            			case 2:
            				OpeniAppsMarketierPopup('LeavingMarketierWarning', 'Product=Commerce', 'fn_RedirectToCustomerDetails');
            				break;
            			case 3:
            				OpeniAppsMarketierPopup('LeavingMarketierWarning', 'Product=CMS', 'fn_RedirectToUserDetails');
            				break;
            		}
	            	break;
            	case 'AssignTags':
            		doCallback = false;
            		var objectTypeId = 302;
            		switch (gridActionsJson.TabIndex) {
            			case 2:
            				objectTypeId = 211;
            				break;
            			case 3:
            				objectTypeId = 14;
            				break;
            		}
            		OpeniAppsAdminPopup("AssignIndexTermsPopup", stringformat("SaveTags=true&DisableEditing=true&ObjectTypeId={0}&ObjectId={1}", objectTypeId, selectedItemId));
            		break;
            	case 'Delete':
            		doCallback = confirm('<%= JSMessages.AreYouSureYouWantToDeleteThisContact %>');
            		break;
				case 'Activate':
					doCallback = confirm('<%= JSMessages.AreYouSureYouWantToActivateThisContact %>');
            		break;
				case 'AddToContactList':
					doCallback = false;
					OpeniAppsMarketierPopup('AssignToDistributionList', 'ContactId=' + selectedItemId, 'fn_ContactListCallback');
					break;
				case 'Unsubscribe':
					doCallback = false;
					OpeniAppsMarketierPopup('Unsubscribe', 'ContactId=' + selectedItemId);
					break;
                default:
                    break;
            }

            if (doCallback) {
                upContacts.set_callbackParameter(JSON.stringify(gridActionsJson));
                upContacts.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
        	var status = eventArgs.selectedItem.find('td')[4].innerText;

        	switch (status) {
        		case 'Active':
        			sender.getItemByCommand('Activate').hide();
        			break;
        		case 'Deleted':
        			sender.getItemByCommand('Delete').hide();
        	}
        }

        function fn_RedirectToCustomerDetails() {
        	var id = gridActionsJson.SelectedItems[0];
        	var token = FWCallback.GetContextUserAuthToken("Commerce");
        	window.location = jCommerceAdminSiteUrl + '/StoreManager/Customers/CustomerDetails.aspx?CustomerId=' + id + '&Token=' + token;
        }

        function fn_RedirectToUserDetails() {
        	var id = gridActionsJson.SelectedItems[0];
        	var token = FWCallback.GetContextUserAuthToken("CMS");
        	window.location = jCMSAdminSiteUrl + '/Administration/User/ManageWebsiteUserDetails.aspx?UserId=' + id + '&Token=' + token;
        }

        var errorMessage = '';
        function fn_ContactListCallback() {
        	if (errorMessage == '') {
        		ShowiAppsNotification('<%= String.Format(GUIStrings.ContactAddedToList, String.Empty) %>', true, true);
            } else {
            	ShowiAppsNotification(errorMessage, false, true);
            }
		}
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= GUIStrings.ManageContacts %></h1>
    <div class="contacts clear-fix">
        <iAppsControls:CallbackPanel ID="upContacts" runat="server" OnClientCallbackComplete="upContacts_OnCallbackComplete"
            OnClientRenderComplete="upContacts_OnRenderComplete" OnClientLoad="upContacts_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvContacts" runat="server" ItemPlaceholderID="phContacts">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            No items found!
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
						<table class="grid" width="100%">
                            <thead>
                                <tr>
                                    <th colspan="2">Contact</th>
                                    <th>Email</th>
									<th>Created</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="phContacts" runat="server" />
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="item-list collection-row grid-item" objectid='<%# Eval("Id") %>'>
                            <td><asp:Image ID="imgProfileImage" runat="server" ImageUrl="~/App_Themes/General/images/icon-no-image.png" /></td>
                            <td><%# String.Format("{0}, {1}", Eval("LastName"), Eval("FirstName")) %></td>
                            <td><%# Eval("Email") %></td>
							<td><%# Eval("CreatedDate", "{0:d}") %></td>
                            <td><%# Eval("Status") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</asp:Content>
