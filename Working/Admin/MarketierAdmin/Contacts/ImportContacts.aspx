﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ImportContacts.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ImportContacts" StylesheetTheme="General" %>

<%@ Register TagPrefix="sd" TagName="SelectDataSource" Src="~/UserControls/Contacts/SelectDataSource.ascx" %>
<%@ Register TagPrefix="mf" TagName="MapFields" Src="~/UserControls/Contacts/MapFields.ascx" %>
<%@ Register TagPrefix="vd" TagName="VerifyCSVData" Src="~/UserControls/Contacts/VerifyCSVData.ascx" %>
<%@ Register TagPrefix="dl" TagName="DistributionLists" Src="~/UserControls/Contacts/DefineDistributionLists.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function fn_CloseElapsedPopup() {
            window.location.href = jmarketierAdminURL + "/Contacts/ManageContacts.aspx";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="pageHeader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><%= GUIStrings.ImportContacts %></h1>
        <asp:LinkButton ID="lbtnSampleTemplate" runat="server" CssClass="help-text" Text="Download sample template" />
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="container-box clear-fix">
        <asp:Wizard ID="myWizard" runat="server" EnableViewState="true" ActiveStepIndex="0" Width="100%" CssClass="wizard"
            OnCancelButtonClick="OnCancelButtonClick" OnPreviousButtonClick="OnBack" OnNextButtonClick="OnNext" DisplaySideBar="false">
            <LayoutTemplate>
                <div class="top-navigation clear-fix">
                    <asp:PlaceHolder ID="headerPlaceHolder" runat="server" />
                    <asp:PlaceHolder ID="navigationPlaceHolder" runat="server" />
                </div>
                <asp:PlaceHolder ID="wizardStepPlaceHolder" runat="server" />
            </LayoutTemplate>
            <HeaderTemplate>
                <div class="header-container">
                    <h2 class="step1<%=myWizard.ActiveStepIndex%>">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SelectDataSource %>" /></h2>
                    <h2 class="step2<%=myWizard.ActiveStepIndex%>">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, MapFields %>" /></h2>
                    <h2 class="step3<%=myWizard.ActiveStepIndex%>">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, VerifyData %>" /></h2>
                    <h2 class="step4<%=myWizard.ActiveStepIndex%>">
                        <asp:Localize ID="Localize4" runat="server" Text="4. Define Contact List" /></h2>
                </div>
            </HeaderTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="<%$ Resources:GUIStrings, SelectDataSource1 %>">
                    <div class="clear-fix step1-content">
                        <sd:SelectDataSource ID="SelectDataSource1" runat="server" />
                    </div>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="<%$ Resources:GUIStrings, MapFields %>">
                    <div class="clear-fix step2-content">
                        <mf:MapFields ID="ctlMapFields" runat="server" ImportConfigSessionVar="ImportContact" OnLoadInputData="ctlMapFields_OnLoadMapData" />
                    </div>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="<%$ Resources:GUIStrings, MapFields %>">
                    <div class="clear-fix step3-content">
                        <vd:VerifyCSVData Id="ctlVerifyCSVData" runat="server" />
                    </div>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep4" runat="server" Title="<%$ Resources:GUIStrings, DefineDistributionLists %>">
                    <div class="clear-fix step4-content">
                        <dl:DistributionLists ID="ctlDistributionLists" runat="server" />
                    </div>
                </asp:WizardStep>
            </WizardSteps>
            <StartNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CssClass="button dark" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
                        CommandName="Cancel" />
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, Continue %>" ToolTip="<%$ Resources:GUIStrings, Continue %>" CssClass="primarybutton"
                        OnClientClick="return ValidateWizard(this)" CommandName="MoveNext" />
                </div>
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnBack" runat="server" Text="<%$ Resources:GUIStrings, Back %>" ToolTip="<%$ Resources:GUIStrings, Back %>" CssClass="button dark"
                        CommandName="MovePrevious" OnClientClick="return SaveBackStepvalue(this)" />
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CssClass="button dark" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
                        CommandName="Cancel" />
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, Continue %>" ToolTip="<%$ Resources:GUIStrings, Continue %>" CssClass="primarybutton"
                        OnClientClick="return ValidateWizard(this)" CommandName="MoveNext" />
                </div>
            </StepNavigationTemplate>
            <FinishNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnBack" runat="server" Text="<%$ Resources:GUIStrings, Back %>" ToolTip="<%$ Resources:GUIStrings, Back %>" CssClass="button dark"
                        CommandName="MovePrevious" OnClientClick="return SaveBackStepvalue(this)" />
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CssClass="button dark" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
                        CommandName="Cancel" />
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, Import %>" ToolTip="<%$ Resources:GUIStrings, Import %>" CssClass="primarybutton"
                        OnClientClick="return ValidateWizard(this)"
                        CommandName="MoveComplete" />
                </div>
            </FinishNavigationTemplate>
        </asp:Wizard>
    </div>
</asp:Content>

