﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" StylesheetTheme="General" AutoEventWireup="true" CodeBehind="ManageSubscriptions.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageSubscriptions" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        var txtEmail;
        var lbEmails;
        var chkCampaigns;
        var hdnEmails;

        var emails = [];

        $(document).ready(function () {
            txtEmail = document.getElementById("txtEmail");
            lbEmails = document.getElementById("lbEmails");
            chkCampaigns = document.getElementById("<%= chkCampaigns.ClientID %>");
            hdnEmails = document.getElementById("<%= hdnEmails.ClientID %>");

            var selectedValue = $("input[name='ctl00$cphContent$rblCampaign']:checked").val();

            switch (selectedValue.toLowerCase()) {
                case "all":
                    $(chkCampaigns).hide();
                    break;
                case "individual":
                    $(chkCampaigns).show();
                    break;
            }


            $(".tabs").iAppsTabs();
        });

        function AddEmailAddress() {
            var email = $(txtEmail).val();

            if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z][A-Z]+$/i.test(email)) {
                emails.push(email);

                DisplayEmails();

                $(txtEmail).val("");
                $(txtEmail).focus();
            }
            else {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseenteravalidemailaddress %>'/>");
            }
        }

        function RemoveEmailAddresses() {
            var selectedEmails = $(lbEmails).find("option:selected");

            if (selectedEmails.length > 0) {
                $(selectedEmails).each(function () {
                    var currentEmail = $(this).text();

                    for (var i = 0; i < emails.length; i++) {
                        if (emails[i] == currentEmail) {
                            emails.splice(i, 1);
                        }
                    }
                });

                DisplayEmails();
            }
            else {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseselectoneormoreemailaddressestoremove %>'/>");
            }
        }

        function DisplayEmails() {
            emails.sort();

            StoreEmails();

            $(lbEmails).empty();

            for (var i = 0; i < emails.length; i++) {
                $(lbEmails).append("<option>" + emails[i] + "</option>")
            }
        }

        function StoreEmails() {
            var emailsList = "";

            for (var i = 0; i < emails.length; i++) {
                if (i < emails.length - 1) {
                    emailsList += emails[i] + ",";
                }
                else {
                    emailsList += emails[i];
                }
            }

            $(hdnEmails).val(emailsList);
        }

        function ValidateEmails() {
            if (document.activeElement.id == "txtEmail") {
                AddEmailAddress();
                return false;
            }
            else {
                if (emails.length > 0) {
                    var selectedValue = $("input[name='ctl00$mainPlaceHolder$rblCampaign']:checked").val();

                    switch (selectedValue.toLowerCase()) {
                        case "all":
                            break;
                        case "individual":
                            if ($(chkCampaigns).find("input:checked").size() == 0) {
                                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseselectatleastonecampaign %>'/>");
                                return false;
                            }

                            break;
                    }
                }
                else {
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, Pleaseenteroneormoreemailaddresses %>'/>");
                    return false;
                }
            }
        }

        function rblCampaign_OnClick(radiobutton) {
            var campaignList = $("#campaignList");
            var selectedValue = $(radiobutton).val();

            switch (selectedValue.toLowerCase()) {
                case "all":
                    campaignList.hide();
                    break;
                case "individual":
                    campaignList.show();
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="pageHeader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><%= GUIStrings.ManageSubscriptions %></h1>
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="tabs clear-fix">
        <ul>
            <li><span><%= GUIStrings.ResubscribeContacts %></span></li>
            <li><span><%= GUIStrings.ExportUnsubscribeContacts %></span></li>
        </ul>
        <div class="resubscribe">
            <p>
                <strong class="colon"><%= GUIStrings.Instructions %></strong>&nbsp;<%= GUIStrings.Enteroneormoreemailaddressestoresubscribe %>
            </p>
            <asp:PlaceHolder ID="phResults" runat="server">
                <%--<p class="successMessage"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Theuserslistedbelowhavebeensuccessfullyresubscribed %>" /></p>--%>

                <ComponentArt:Grid ID="grdResults" runat="server" SkinID="Default" AllowPaging="True"
                    PageSize="10" EmptyGridText="<%$ Resources:GUIStrings, NoResultsFound %>" AllowMultipleSelect="false">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            AllowReordering="false" HeadingRowCssClass="heading-row" RowCssClass="row" SelectedRowCssClass="selected-row"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            AlternatingRowCssClass="alternate-row">
                            <Columns>
                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                <ComponentArt:GridColumn Width="300" AllowReordering="False" DataField="Email"
                                    runat="server" HeadingText="<%$ Resources:GUIStrings, EmailAddress %>" AllowEditing="True"
                                    AllowSorting="True" FixedWidth="true" />
                                <ComponentArt:GridColumn Width="300" AllowReordering="False" runat="server" HeadingText="<%$ Resources:GUIStrings, Campaign %>"
                                    DataField="Campaign" FixedWidth="true" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                </ComponentArt:Grid>
            </asp:PlaceHolder>
            <%--<asp:PlaceHolder ID="phNoResults" runat="server">
                <p class="warningMessage"><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Nounsubscribeduserswerefoundthatmatchedyourcriteria %>" /></p>
            </asp:PlaceHolder>--%>
            <p><%= GUIStrings.Pleaseentertheemailaddressestoresubscribe %></p>
            <table cellpadding="0" class="utility-table">
                <tr>
                    <td>
                        <input id="txtEmail" type="text" class="textBoxes" />
                    </td>
                    <td valign="top">
                        <input type="button" value="<%= GUIStrings.Add %>" class="small-button dark"
                            onclick="AddEmailAddress();" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <select id="lbEmails" multiple="multiple" size="7">
                        </select>
                    </td>
                    <td valign="top">
                        <input type="button" value="<%= GUIStrings.Remove %>" class="small-button dark"
                            onclick="RemoveEmailAddresses();" />
                        <asp:HiddenField ID="hdnEmails" runat="server" />
                    </td>
                </tr>
            </table>
            <p class="userInstructions">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Pleaseselectthecampaignstoresubscribefortheaboveemailaddresses %>" />
            </p>
            <asp:RadioButtonList ID="rblCampaign" runat="server">
                <asp:ListItem Value="All" runat="server" Text="<%$ Resources:GUIStrings, Resubscribetoallunsubscribedandfuturecampaigns %>"
                    onclick="rblCampaign_OnClick(this);" />
                <asp:ListItem Value="Individual" runat="server" Text="<%$ Resources:GUIStrings, Resubscribetoindividualcampaigns %>"
                    Selected="True" onclick="rblCampaign_OnClick(this);" />
            </asp:RadioButtonList>
            <div id="campaignList" class="scroll-box clear-fix">
                <asp:CheckBoxList ID="chkCampaigns" runat="server" />
            </div>
            <div class="button-row">
                <asp:Button ID="btnResubscribe" runat="server" OnClientClick="return ValidateEmails();"
                    CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Resubscribe %>" />
            </div>
        </div>
        <div class="unsubscribed-contacts">
            <div class ="form-row">
                <asp:Label ID="lblNotes" runat="server" />
            </div>
            <div class="form-row">                
                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="ddlCampaign" Text="<%$ Resources:GUIStrings, Email %>" CssClass="form-label" />
                <div class="form-value">
                    <asp:DropDownList ID="ddlCampaign" runat="server" />
                </div>
            </div>
            <div class="button-row">
                <asp:Button ID="btnExportUnsubscribed" runat="server" Text="<%$ Resources:GUIStrings, ExportUnsubscribed %>"
                    CssClass="primarybutton" />
            </div>
        </div>
    </div>
</asp:Content>
