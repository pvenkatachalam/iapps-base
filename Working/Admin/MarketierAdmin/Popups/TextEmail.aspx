﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="TextEmail.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.TextEmail" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var caret = 0;

        function fn_GetCaretPosition() {
            $this = $('#<%=txtTextEmail.ClientID%>');
            caret = getCaret($this);
        }

        function getCaret(el) {
            if (el.prop("selectionStart")) {
                return el.prop("selectionStart");
            } else if (document.selection) {
                el.focus();

                var r = document.selection.createRange();
                if (r == null) {
                    return 0;
                }

                var re = el.createTextRange(),
                        rc = re.duplicate();
                re.moveToBookmark(r.getBookmark());
                rc.setEndPoint('EndToStart', re);

                return rc.text.length;
            }
            return 0;
        }

        function appendAtCaret($target, caret, $value) {
            var value = $target.val();
            if (caret != value.length) {
                var startPos = $target.prop("selectionStart");
                var scrollTop = $target.scrollTop;
                $target.val(value.substring(0, caret) + ' ' + $value + ' ' + value.substring(caret, value.length));
                $target.prop("selectionStart", startPos + $value.length);
                $target.prop("selectionEnd", startPos + $value.length);
                $target.scrollTop = scrollTop;
            } else if (caret == 0) {
                $target.val($value + value);
            } else {
                $target.val(value + $value);
            }
        }

        function fn_InsertMergeField() {
            $this = $('#<%=txtTextEmail.ClientID%>');
            var selectedValue = $('#<%=ddlMergeFields.ClientID%>').val();
            if (selectedValue != '-1') {
                appendAtCaret($this, caret, selectedValue);
            }
        }

        function fn_InsertUnsubscribeLink(unsubscribePageId, containerId) {
            $this = $('#<%=txtTextEmail.ClientID%>');
            var unsubscribeLink = jPublicSiteUrl + '/marketiercampaign/[Unsubscribe:' + unsubscribePageId + '|' + containerId + ']';
            appendAtCaret($this, caret, unsubscribeLink);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="clear-fix">
        <div class="form-row" style="float: left;">
            <label class="form-label"><%= GUIStrings.SelectMergeField %></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlMergeFields" runat="server" Width="200" />
            </div>
        </div>
        <asp:HyperLink ID="hplUnsubscribeLink" runat="server" NavigateUrl="javascript://" Text="<%$ Resources:GUIStrings, InsertUnsubscribeLink %>" Style="float: right;" />
    </div>
    <div class="form-row">
        <asp:TextBox ID="txtTextEmail" runat="server" TextMode="MultiLine" Width="800" Height="450" 
            onblur="fn_GetCaretPosition();" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSave" runat="server" CssClass="primarybutton show-loading" Text="<%$ Resources:GUIStrings, Save %>" />
</asp:Content>
