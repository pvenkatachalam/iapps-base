﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ExportEmails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ExportEmails" StylesheetTheme="General" %>

<asp:Content ID="head" runat="server" ContentPlaceHolderID="cphHead">
    <script type="text/javascript">
        function FromDateCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = fromDateCalendar.getSelectedDate();
            document.getElementById('<%= txtFrom.ClientID %>').value = fromDateCalendar.formatDate(selectedDate, "MM/dd/yyyy");
        }

        function FromDatePopUpCalendar() {
            var thisDate = new Date();
            obj = document.getElementById('<%= txtFrom.ClientID %>').value;

            if (obj != null && obj != "") {
                fromDateCalendar.setSelectedDate(new Date(obj));
            }

            if (!(fromDateCalendar.get_popUpShowing())) {
                fromDateCalendar.show();
            }
        }

        function ToDateCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = toDateCalendar.getSelectedDate();
            document.getElementById('<%= txtTo.ClientID %>').value = toDateCalendar.formatDate(selectedDate, "MM/dd/yyyy");
        }

        function ToDatePopUpCalendar() {
            var thisDate = new Date();
            obj = document.getElementById('<%= txtTo.ClientID %>').value;

            if (obj != null && obj != "") {
                toDateCalendar.setSelectedDate(new Date(obj));
            }

            if (!(toDateCalendar.get_popUpShowing())) {
                toDateCalendar.show();
            }
        }

        function fn_DateRangeOnchange(ddlDateRange) {
            if ($(ddlDateRange).val() == '0') {
                $('.custom-date-range').show();
            }
            else {
                $('.custom-date-range').hide();
            }
        }
    </script>
</asp:Content>

<asp:Content ID="body" runat="server" ContentPlaceHolderID="cphContent">
    <asp:ValidationSummary ID="vsDateRange" runat="server" DisplayMode="BulletList" ShowMessageBox="true" ShowSummary="true" />
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.DateRange %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlDateRange" runat="server" onchange="fn_DateRangeOnchange(this);">
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last30Days %>" Value="30" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last60Days %>" Value="60" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last90days %>" Value="90" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last180Days %>" Value="180" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Last365Days %>" Value="365" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Custom %>" Value="0" />
            </asp:DropDownList>
        </div>
    </div>
    <div class="custom-date-range">
        <div class="form-row">
            <label class="form-label colon"><%= GUIStrings.From %></label>
            <div class="form-value calendar-value">
                <asp:TextBox ID="txtFrom" runat="server" />
                <asp:Image ID="imgBtnFrom" runat="server" AlternateText="<%$ Resources:GUIStrings, From %>" ClientIDMode="Static"
                    ImageUrl="~/App_Themes/General/images/calendar-button.png" onclick="FromDatePopUpCalendar();" />
                <ComponentArt:Calendar ID="fromDateCalendar" runat="server" SkinID="Default" MaxDate="9998-12-31" PopUpExpandControlId="imgBtnFrom">
                    <ClientEvents>
                        <SelectionChanged EventHandler="FromDateCalendar_onSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:Calendar>
            </div>
            <asp:CompareValidator ID="cvFromDate" runat="server" Operator="DataTypeCheck"
                ControlToValidate="txtFrom" Text="*" Type="Date" CssClass="validation-error"
                Display="Dynamic" ErrorMessage="From date has invalid date format" />
        </div>

        <div class="form-row">
            <label class="form-label colon"><%= GUIStrings.To %></label>
            <div class="form-value calendar-value">
                <asp:TextBox ID="txtTo" runat="server" />
                <asp:Image ID="imgBtnTo" runat="server" AlternateText="<%$ Resources:GUIStrings, To %>" ClientIDMode="Static"
                    ImageUrl="~/App_Themes/General/images/calendar-button.png" onclick="ToDatePopUpCalendar();" />
                <ComponentArt:Calendar ID="toDateCalendar" runat="server" SkinID="Default" MaxDate="9998-12-31" PopUpExpandControlId="imgBtnTo">
                    <ClientEvents>
                        <SelectionChanged EventHandler="ToDateCalendar_onSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:Calendar>
            </div>
            <asp:CompareValidator ID="cvToDate" runat="server" Operator="DataTypeCheck"
                ControlToValidate="txtFrom" Text="*" Type="Date" CssClass="validation-error"
                Display="Dynamic" ErrorMessage="To date has invalid date format" />

            <asp:CompareValidator ID="cvFromToDate" runat="server" Operator="GreaterThanEqual" Type="Date"
                CssClass="validation-error" Display="Dynamic" ControlToValidate="txtTo" Text="*" ControlToCompare="txtFrom"
                ErrorMessage="To date should be more than from date" />
        </div>
    </div>
    <div class="clear-fix">
        <div class="left-column columns">
            <p><strong><%= GUIStrings.CampaignStatus %></strong></p>
            <asp:CheckBoxList ID="chkStatusList" runat="server" CssClass="statusListControl">
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Draft %>" Value="1" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Active %>" Value="2" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Running %>" Value="5" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Completed %>" Value="3" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Archived %>" Value="4" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Cancelled %>" Value="6" Selected="True" />
            </asp:CheckBoxList>
        </div>
        <div class="right-column columns">
            <p><strong><%= GUIStrings.WorkflowState %></strong></p>
            <asp:CheckBoxList ID="chkWorkflowStatus" runat="server" CssClass="statusListControl">
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Editing %>" Value="1" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, PendingApproval %>" Value="2" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Rejected %>" Value="3" Selected="True" />
                <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, Approved %>" Value="4" Selected="True" />
            </asp:CheckBoxList>
        </div>
    </div>
</asp:Content>

<asp:Content ID="footer" runat="server" ContentPlaceHolderID="cphFooter">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnExport" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Export %>" OnClick="btnExport_Click" />
</asp:Content>
