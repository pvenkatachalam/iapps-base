﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="EmailMonthlyView.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.EmailMonthlyView" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server"></asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="legends clear-fix">
        <ul>
            <li class="green"><span><%= GUIStrings.SentCampaigns %></span></li>
            <li class="blue"><span><%= GUIStrings.ScheduledCampaigns %></span></li>
        </ul>
    </div>
    <h2><asp:Literal ID="ltMonth" runat="server" Text=""/></h2>
    <div class="clear-fix calendar-row">
        <div class="prev-month">
            <asp:ImageButton ID="ibtnPrevMonth" runat="server" ImageUrl="~/App_Themes/General/images/cal-prev-month.png" AlternateText="Go to previous month"  OnClick="ibtnPrevMonth_Click"/>
        </div>
        <div class="calendar-holder">
            <asp:Calendar ID="calMonthView" runat="server" ShowTitle="False" ShowNextPrevMonth="False"
                DayNameFormat="Full" FirstDayOfWeek="Sunday" CssClass="full-calendar" SelectionMode="None">
                <DayHeaderStyle CssClass="calendar-header" />
                <DayStyle CssClass="calendar-day" Width="136" Height="125" />
                <SelectedDayStyle CssClass="selected-day" />
                <TodayDayStyle BackColor="#d4e0e7" />
            </asp:Calendar>
        </div>
        <div class="next-month">
            <asp:ImageButton ID="ibtnNextMonth" runat="server" ImageUrl="~/App_Themes/General/images/cal-next-month.png" AlternateText="Go to next month" OnClick="ibtnNextMonth_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>"  OnClick="btnClose_Click"/>
</asp:Content>
