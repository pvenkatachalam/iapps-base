﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="RunSpamCheck.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.RunSpamCheck" StylesheetTheme="General" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Literal ID="litEmail" runat="server" Mode="PassThrough" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button cancel-button" />
</asp:Content>