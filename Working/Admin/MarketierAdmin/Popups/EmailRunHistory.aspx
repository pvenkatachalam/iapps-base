﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="EmailRunHistory.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.EmailRunHistory" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">


        function upEmailRunHistory_OnLoad() {
            $(".email-run-history").gridActions({
                objList: $(".item-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upEmailRunHistory_Callback(sender, eventArgs);
                }
            });
        }

        function upEmailRunHistory_OnRenderComplete() {
            $(".email-run-history").gridActions("bindControls");
        }

        function upEmailRunHistory_OnCallbackComplete() {
            $(".email-run-history").gridActions("displayMessage");
        }
        var iAPPS_OverlayPopup;
        function upEmailRunHistory_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var campaignId = $('#hdnCampaignId').val();

            switch (gridActionsJson.Action) {
                case 'Overlays':
                    doCallback = false;
                    var queryString = "CampaignId=" + campaignId + "&CampaignRunId=" + selectedItemId;
                    var width = 950;
                    var height = 600;
                    var left = parseInt((screen.availWidth / 2) - (width / 2));
                    var top = parseInt((screen.availHeight / 2) - (height / 2));
                    var windowOptions = 'status=1,toolbar=0,location=1,menubar=0,resizable=0,scrollbars=1,width=' + width + ',height=' + height + ',left=' + left + ',top=' + top;
                    iAPPS_OverlayPopup = window.open(jmarketierAdminURL + '/Popups/CampaignOverlay.aspx?' + queryString, 'OverlayWindow', windowOptions);
                    if (window.focus) {
                        iAPPS_OverlayPopup.focus();
                    }
                    break;
                case 'ExportRecipientActionsToCsv':
                    doCallback = false;
                    OpenExportActions();
                    break;
            }

            if (doCallback) {
                upEmailRunHistory.set_callbackParameter(JSON.stringify(gridActionsJson));
                upEmailRunHistory.callback();
            }
        }

        function RedirectToParent(urlToRedirect) {
            iAPPS_OverlayPopup.close();
            window.parent.location = urlToRedirect;

        }
        function OpenExportActions() {
            $('#divExportActions').iAppsDialog("open");
            return false;
        }

        function CloseExportActions() {
            $('#divExportActions').iAppsDialog("close");

            return false;
        }

        function Grid_ItemSelect(sender, eventArgs) {
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div>
        <iAppsControls:CallbackPanel ID="upEmailRunHistory" runat="server" OnClientCallbackComplete="upEmailRunHistory_OnCallbackComplete"
            OnClientRenderComplete="upEmailRunHistory_OnRenderComplete" OnClientLoad="upEmailRunHistory_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvRuns" runat="server" ItemPlaceholderID="phRunList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Localize ID="locRunDate" runat="server" Text="<%$ Resources:GUIStrings, NoEmailStatisticsAreAvailableForThisEmailCampaign %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <table class="grid" width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:Localize ID="locRunDate" runat="server" Text="<%$ Resources:GUIStrings, RunDate %>" /></th>
                                    <th>
                                        <asp:Localize ID="locSends" runat="server" Text="<%$ Resources:GUIStrings, Sends %>" /></th>
                                    <th>
                                        <asp:Localize ID="locDelivered" runat="server" Text="<%$ Resources:GUIStrings, Delivered %>" /></th>
                                    <th>
                                        <asp:Localize ID="locOpens" runat="server" Text="<%$ Resources:GUIStrings, Opens %>" /></th>
                                    <th>
                                        <asp:Localize ID="locUniqueOpens" runat="server" Text="<%$ Resources:GUIStrings, UniqueOpen %>" /></th>
                                    <th>
                                        <asp:Localize ID="locClicks" runat="server" Text="<%$ Resources:GUIStrings, Clicks %>" /></th>
                                    <th>
                                        <asp:Localize ID="locUnsubscribes" runat="server" Text="<%$ Resources:GUIStrings, Unsubscribes %>" /></th>
                                    <th>
                                        <asp:Localize ID="locBounces" runat="server" Text="<%$ Resources:GUIStrings, Bounces %>" /></th>
                                    <th>
                                        <asp:Localize ID="locTriggeredWatches" runat="server" Text="<%$ Resources:GUIStrings, TriggeredWatches %>" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="phRunList" runat="server" />
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="item-list collection-row grid-item" objectid='<%# Eval("Id") %>'>
                            <td>
                                <asp:Label ID="lblRunDate" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblSends" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblDelivered" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblOpens" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblUniqueOpen" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblClicks" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblUnsubscibes" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblBounces" runat="server" CssClass="left-margin" /></td>
                            <td>
                                <asp:Label ID="lblTriggeredWatches" runat="server" CssClass="left-margin" /></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div id="divExportActions" class="iapps-modal" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.SelectPropertiesToExport %></h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="form-row">
                <%= GUIStrings.Select %>
                <a id="A1" href="#" onclick="javascript: CheckBoxListSelect ('<%= cblSelectedProperties.ClientID %>',true)">
                    <%= GUIStrings.All %></a> | <a id="A2" href="#" onclick="javascript: CheckBoxListSelect ('<%= cblSelectedProperties.ClientID %>',false)">
                        <%= GUIStrings.None %></a>
            </div>
            <div class="form-row">
                <asp:CheckBoxList runat="server" ID="cblSelectedProperties" RepeatColumns="5"
                    RepeatDirection="Horizontal" RepeatLayout="Table">
                    <asp:ListItem Value="Email" Selected="True" Text="<%$ Resources:GUIStrings, Email %>" />
                    <asp:ListItem Value="Opens" Selected="True" Text="<%$ Resources:GUIStrings, Opens %>" />
                    <asp:ListItem Value="Clicks" Selected="True" Text="<%$ Resources:GUIStrings, Clicks %>" />
                    <asp:ListItem Value="Unsubscribes" Selected="True" Text="<%$ Resources:GUIStrings, Unsubscribes %>" />
                    <asp:ListItem Value="Bounces" Selected="True" Text="<%$ Resources:GUIStrings, Bounces %>" />
                    <asp:ListItem Value="TriggeredWatches" Selected="True" Text="<%$ Resources:GUIStrings, TriggeredWatches %>" />
                    <asp:ListItem Value="FirstName" Text="<%$ Resources:GUIStrings, FirstName %>" />
                    <asp:ListItem Value="LastName" Text="<%$ Resources:GUIStrings, LastName %>" />
                    <asp:ListItem Value="CompanyName" Text="<%$ Resources:GUIStrings, CompanyName %>" />
                    <asp:ListItem Value="BirthDate" Text="<%$ Resources:GUIStrings, BirthDate %>" />
                    <asp:ListItem Value="Gender" Text="<%$ Resources:GUIStrings, Gender %>" />
                    <asp:ListItem Value="Status" Text="<%$ Resources:GUIStrings, Status %>" />
                    <asp:ListItem Value="HomePhone" Text="<%$ Resources:GUIStrings, HomePhone %>" />
                    <asp:ListItem Value="MobilePhone" Text="<%$ Resources:GUIStrings, MobilePhone %>" />
                    <asp:ListItem Value="OtherPhone" Text="<%$ Resources:GUIStrings, OtherPhone %>" />
                    <asp:ListItem Value="Notes" Text="<%$ Resources:GUIStrings, Notes %>" />
                    <asp:ListItem Value="AddressLine1" Text="<%$ Resources:GUIStrings, AddressLine1 %>" />
                    <asp:ListItem Value="AddressLine2" Text="<%$ Resources:GUIStrings, AddressLine2 %>" />
                    <asp:ListItem Value="AddressLine3" Text="<%$ Resources:GUIStrings, AddressLine3 %>" />
                    <asp:ListItem Value="City" Text="<%$ Resources:GUIStrings, City %>" />
                    <asp:ListItem Value="State" Text="<%$ Resources:GUIStrings, State %>" />
                    <asp:ListItem Value="StateCode" Text="<%$ Resources:GUIStrings, StateCode %>" />
                    <asp:ListItem Value="CountryName" Text="<%$ Resources:GUIStrings, CountryName %>" />
                    <asp:ListItem Value="CountryCode" Text="<%$ Resources:GUIStrings, CountryCode %>" />
                    <asp:ListItem Value="Zip" Text="<%$ Resources:GUIStrings, ZipCode %>" />
                </asp:CheckBoxList>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>"
                class="button" onclick="return CloseExportActions();" />
            <asp:Button ID="btnExport" runat="server" Text="<%$ Resources:GUIStrings, Export %>"
                class="primarybutton" OnClick="btnExport_Click" OnClientClick="CloseExportActions();" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <p class="orange-help"><%= GUIStrings.OverlayHelpText %></p>
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button cancel-button" />
    <asp:HiddenField ID="hdnCampaignId" runat="server" ClientIDMode="Static" />
</asp:Content>
