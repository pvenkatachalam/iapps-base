﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ConfirmEmailSchedule.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ConfirmEmailSchedule" StylesheetTheme="General" ValidateRequest="false" Async ="true"%>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
	<link rel="stylesheet" type="text/css" href="../css/EmailBuilder.css" />
	<script type="text/javascript" src="../script/EmailConfirmSend.js"></script>
	<script type="text/javascript">
		function fn_RejectionNotesCallback() {
			$('#<%= hdnRejectionNotes.ClientID %>').val(popupActionsJson.CustomAttributes['rejectionNotes']);
			$('#<%= btnRejectPostback.ClientID %>').click();
		}
	</script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
	<div class ="emailbuilder-form-column-wrapper">
		<div class="emailbuilder-form-column confirm-column-left">
			<div class="confirm-email-preview-wrapper">
				<div class="confirm-email-preview-overlay"></div>
				<iframe id="email_preview" class="confirm-email-preview"></iframe>
			</div>
		</div>
		<div class="emailbuilder-form-column confirm-column-center">
			<div>
				<asp:Literal ID="ltPrevalidationWarnings" runat="server" />
			</div>
			<div class="email-details-form">
				<span class="emailbuilder-form-header"><%= GUIStrings.EmailDetails %></span>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.Template %></label>
					<asp:Literal ID="hplTemplateName" runat="server" />
					<%--<asp:LinkButton runat="server" ID="btnlSelectedTemplateText" OnClick="btnlSelectedTemplateText_Click" />--%>
				</div>
				
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.CampaignName1 %></label>
					<asp:Literal ID="lbtnEmailName" runat="server" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.EmailSubject %></label>
					<asp:Literal ID="lbtnEmailSubject" runat="server" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.SenderName %></label>
					<asp:Literal ID="lbtnSenderName" runat="server" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.SenderEmail %></label>
					<asp:Literal ID="lbtnSenderEmail" runat="server" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.ConfirmationEmail %></label>
					<asp:Literal ID="lbtnConfirmationEmail" runat="server" />
				</div>
			</div>
		</div>
		<div class="emailbuilder-form-column confirm-column-right">
			<div class="email-schedule-form">
				<span class="emailbuilder-form-header"><%= GUIStrings.Schedule %></span>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.Date %></label>
					<asp:LinkButton ID="lbtnStartDate" runat="server" OnClick="ScheduleDetail_Click" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.Time %></label>
					<asp:LinkButton ID="lbtnStartTime" runat="server" OnClick="ScheduleDetail_Click" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.TimeZone %></label>
					<asp:LinkButton ID="lbtnTimeZone" runat="server" OnClick="ScheduleDetail_Click" />
				</div>
				<div class="emailbuilder-form-field">
					<label class="emailbuilder-form-field-label"><%= GUIStrings.RecurringDeliveryOptions %></label>
					<asp:LinkButton ID="lbtnRecurringDeliveryOptions" runat="server" OnClick="ScheduleDetail_Click" />
				</div>
			</div>
			<div>
				<span class="emailbuilder-form-header"><%= GUIStrings.AddYourEmailCampaignToAWorkflow %></span>
				<div class="emailbuilder-form-field">
					<asp:DropDownList runat="server" ID="ddlWorkflowList" CssClass="dropdown-normal" />
					<asp:RequiredFieldValidator ID="rfvWorkflowList" runat="server" ControlToValidate="ddlWorkflowList" CssClass="validation-error" Text="<%$ Resources:JSMessages, Required %>" ValidationGroup="Workflow" Display="Dynamic" InitialValue="00000000-0000-0000-0000-000000000000" />
					<asp:LinkButton ID="lbtnSelectedWorkflow" runat="server" OnClick="lbtnSelectedWorkflow_Click" Visible="false" />
				</div>
			</div>
			<div class="email-selected-recipients">
				<span class="emailbuilder-form-header"><%= GUIStrings.Recipients %></span>
				<asp:Repeater runat="server" ID="rptRecipientsList">
					<HeaderTemplate>
						<ul class="email-recipient-list">
					</HeaderTemplate>
					<ItemTemplate>
						<li><asp:LinkButton ID="lbtnRecipientsListItem" runat="server" OnClick="RecipientsListItem_Click" Text="<%# Container.DataItem %>" /></li>
					</ItemTemplate>
					<FooterTemplate>
						</ul>
					</FooterTemplate>
				</asp:Repeater>
			</div>
		</div>
	</div>
	<asp:HiddenField ID="hdnRejectionNotes" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, CloseAndEdit %>" Visible="false" />
    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" CssClass="button" Text="<%$ Resources:GUIStrings, Save %>" Visible="false" />
	<asp:Button ID="btnSubmitIntoWorkflow" runat="server" OnClick="btnSubmitIntoWorkflow_Click" CssClass="button primarybutton" Text="<%$ Resources:GUIStrings, SubmitIntoWorkflow %>" ValidationGroup="Workflow" Visible="false" />
	<asp:Button ID="btnReject" runat="server" OnClientClick="return fn_ShowRejectionNotesPopup()" CssClass="button publishButton" Text="<%$ Resources:GUIStrings, Reject %>" Visible="false" />
	<asp:Button ID="btnRejectPostback" runat="server" OnClick="btnReject_Click" style="display:none;" />
	<asp:Button ID="btnApprove" runat="server" OnClick="btnApprove_Click" CssClass="button primarybutton" Text="<%$ Resources:GUIStrings, Approve %>" Visible="false" />
	<asp:Button ID="btnPublish" runat="server" OnClick="btnPublish_Click" CssClass="button primarybutton" Text="<%$ Resources:GUIStrings, Publish %>" Visible="false" />
	<asp:Button ID="btnApproveAndPublish" runat="server" OnClick="btnPublish_Click" CssClass="button primarybutton" Text="<%$ Resources:GUIStrings, ApproveAndPublish %>" Visible="false" />
	<asp:Button ID="btnManageEmails" runat="server" OnClick="btnManageEmails_Click" CssClass="button" Text="<%$ Resources:GUIStrings, ManageEmails %>" Visible="false" />
	<asp:Button ID="btnCancelEmailSend" runat="server" OnClick="btnCancelEmailSend_Click" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, CancelEmailSend %>" Visible="false" />
</asp:Content>