﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ChooseTemplate.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ChooseTemplate" StylesheetTheme="General" %>

<%@ Register TagPrefix="cp" TagName="ContactProperties" Src="~/UserControls/Contacts/InsertContactProperties.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.email-template-icon').click(templateClick);
        });

        function templateClick() {
            var selected = $(this);

            $('.email-template-selected').removeClass('email-template-selected');
            selected.addClass('email-template-selected');

            $("#hdnSelectedTemplateId").val(selected.parent().attr('data-id'));
        }

        function SelectTemplate() {
            popupActionsJson.SelectedItems.push($("#hdnSelectedTemplateId").val());

            CanceliAppsAdminPopup(true);
            return false;
        }

        function ValidateChooseTemplate() {
            var postback = Page_ClientValidate();
            if (postback)
                ShowiAppsLoadingPanel();

            return postback;
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="vsErrors" runat="server" CssClass="validation-errors" />
    <asp:Panel ID="pnlGroupSelector" runat="server" Visible="true">
        <div class="select-template-panel-top">
            <div class="section-header"><%= GUIStrings.SelectAGroupHeader %></div>
            <div class="form-row">
                <div class="form-value">
                    <asp:DropDownList ID="ddlGroups" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvGroups" runat="server" ControlToValidate="ddlGroups" CssClass="validation-error" Text="<%$ Resources:JSMessages, Required %>" InitialValue="00000000-0000-0000-0000-000000000000" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="select-template-panel-middle">
        <div class="section-header"><%= GUIStrings.SelectATemplateHeader %></div>
        <asp:CustomValidator ID="csvSelectedTemplate" runat="server" OnServerValidate="csvSelectedTemplate_ServerValidate" Text="<%$ Resources:JSMessages, MustSelectATemplate %>" Display="Dynamic" CssClass="validation-error" />
        <div class="template-list-wrapper">
            <asp:DataList runat="server" ID="lstEmailTemplates" RepeatDirection="Horizontal" RepeatColumns="6">
                <ItemTemplate>
                    <div class="email-template" data-id="<%# (Container.DataItem as Bridgeline.FW.SiteBlock.Template).Id %>">
                        <span class="email-template-title"><%# (Container.DataItem as Bridgeline.FW.SiteBlock.Template).Title %></span>
                        <asp:Image ID="imgTemplateImage" runat="server" AlternateText="<%# (Container.DataItem as Bridgeline.FW.SiteBlock.Template).Title %>"
                            ImageUrl="<%# (Container.DataItem as Bridgeline.FW.SiteBlock.Template).ImageURL %>" CssClass="email-template-icon" />
                    </div>
                </ItemTemplate>
            </asp:DataList>
        </div>
        <asp:HiddenField ID="hdnSelectedTemplateId" runat="server" ClientIDMode="Static" />
    </div>
    <asp:PlaceHolder ID="phEmailDetails" runat="server">
        <div class="select-template-panel-bottom">
            <asp:PlaceHolder ID="phEmails" runat="server">
                <div class="section-header"><%= GUIStrings.CreateEmailDetailsHeader %></div>
                <div class="emailbuilder-form-column-wrapper">
                    <div class="emailbuilder-form-column">
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.EmailName %>
                                <asp:RequiredFieldValidator ID="rfvCampaignName" runat="server" ControlToValidate="txtCampaignName" Text="<%$ Resources:JSMessages, Required %>" Display="Dynamic" CssClass="validation-error" />
                                <asp:CustomValidator ID="csvCampaignName" runat="server" ControlToValidate="txtCampaignName" Text="<%$ Resources:JSMessages, EmailNameExists %>" Display="Dynamic" CssClass="validation-error" OnServerValidate="csvCampaignName_ServerValidate" />
                            </label>
                            <div class="form-value">
                                <asp:TextBox runat="server" ID="txtCampaignName" Width="280" ClientIDMode="Static" placeholder="Title of your email campaign" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.EmailSubject %><br />
                                <a href="javascript://" onclick="return fn_ShowContactProperties('txtEmailSubject');"><%= GUIStrings.InsertDynamicContactDetails %></a>
                                <asp:RequiredFieldValidator ID="rfvEmailSubject" runat="server" ControlToValidate="txtEmailSubject" Text="<%$ Resources:JSMessages, Required %>" Display="Dynamic" CssClass="validation-error" />
                            </label>
                            <div class="form-value">
                                <asp:TextBox runat="server" ID="txtEmailSubject" Width="280" ClientIDMode="Static" placeholder="Subject line of your email" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.SenderName %><br />
                                <a href="javascript://" onclick="return fn_ShowContactProperties('txtSenderName');"><%= GUIStrings.InsertDynamicContactDetails %></a>
                                <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" ControlToValidate="txtSenderName" Text="<%$ Resources:JSMessages, Required %>" Display="Dynamic" CssClass="validation-error" />
                            </label>
                            <div class="form-value">
                                <asp:TextBox runat="server" ID="txtSenderName" Width="280" ClientIDMode="Static" placeholder="Name that your email will be sent from" />
                            </div>
                        </div>
                    </div>
                    <div class="emailbuilder-form-column">
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.SenderEmail %><br />
                                <a href="javascript://" onclick="return fn_ShowContactProperties('txtSenderEmail');"><%= GUIStrings.InsertDynamicContactDetails %></a>
                                <asp:RequiredFieldValidator ID="rfvSenderEmail" runat="server" ControlToValidate="txtSenderEmail" Text="<%$ Resources:JSMessages, Required %>" Display="Dynamic" CssClass="validation-error" />
                                <asp:RegularExpressionValidator ID="revSenderEmail" ControlToValidate="txtSenderEmail" Display="Dynamic"
                                    Text="<%$ Resources:JSMessages, InvalidFormat %>" runat="server" ValidationExpression="(^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$)|(^\[[\w]*:[\w]*\]$)" CssClass="validation-error" />
                            </label>
                            <div class="form-value">
                                <asp:TextBox runat="server" ID="txtSenderEmail" Width="280" ClientIDMode="Static" placeholder="Address that your email will be sent from" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.ConfirmationEmail %>
                                <asp:RegularExpressionValidator ID="revConfirmationEmail" ControlToValidate="txtConfirmationEmail"
                                    Text="<%$ Resources:JSMessages, InvalidFormatSeparateAddressWithComma %>" runat="server" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$" Display="Dynamic" CssClass="validation-error" />
                            </label>
                            <div class="form-value">
                                <asp:TextBox runat="server" ID="txtConfirmationEmail" Width="280" ClientIDMode="Static" placeholder="Address which will get confirmation of email send" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phAutoresponders" runat="server" Visible="false">
                <div class="emailbuilder-form-column-wrapper">
                    <div class="emailbuilder-form-column">
                        <div class="form-row">
                            <label class="form-label">
                                <%= GUIStrings.EmailSubject %><br />
                                <a href="javascript://" onclick="return fn_ShowContactProperties('txtAutoResponderEmailSubject');"><%= GUIStrings.InsertDynamicContactDetails %></a>
                                <asp:RequiredFieldValidator ID="rfvAutoresponderEmailSubject" runat="server" ControlToValidate="txtAutoResponderEmailSubject" Text="<%$ Resources:JSMessages, Required %>" Display="Dynamic" CssClass="validation-error" />
                            </label>
                            <div class="form-value">
                                <asp:TextBox runat="server" ID="txtAutoResponderEmailSubject" Width="280" ClientIDMode="Static" placeholder="Subject line of your email" />
                            </div>
                        </div>
                    </div>
                    <div class="emailbuilder-form-column">
                        <div class="form-row">
                            <label class="form-label" style="margin-top: 19px;"><%= GUIStrings.TimeSentAfterTrigger %></label>
                            <asp:RegularExpressionValidator ID="regTriggerTime" ControlToValidate="txtTimeValue" ValidationExpression="^\d+" Display="Dynamic" ErrorMessage="Enter only numbers" CssClass="validation-error" runat="server"></asp:RegularExpressionValidator>
                            <div class="form-value">
                                <asp:TextBox ID="txtTimeValue" runat="server" Width="50" />
                                <asp:DropDownList ID="ddlTimeMeasurement" runat="server" Style="margin-left: 10px;" OnSelectedIndexChanged="ddlTimeMeasurement_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Minutes %>" />
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Hours %>" />
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, Days %>" />
                                    <asp:ListItem Text="<%$ Resources:GUIStrings, MonthForTrigger %>" />
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
    </asp:PlaceHolder>
    <cp:ContactProperties ID="ctlContactProperties" runat="server" />
    <asp:HiddenField ID="hdnCampaignId" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button dark" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnContinue" runat="server" CssClass="primarybutton" OnClientClick="return ValidateChooseTemplate();" Text="<%$ Resources:GUIStrings, Continue %>" />
</asp:Content>
