﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="SendTestEmail.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.SendTestEmail" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".tabs").iAppsTabs();
        });

        function OnSendTestEmail() {
            var isValid = Page_ClientValidate();
            if (isValid)
                ShowiAppsLoadingPanel();

            return isValid;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valTestEmail" runat="server" ShowMessageBox="true" ShowSummary="false" />
    <div class="clear-fix tabs">
        <ul>
            <li><span><a href="javascript://"><%= GUIStrings.Emails %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.ContactLists %></a></span></li>
        </ul>       
        <div class="clear-fix">
            <p><asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EnterCommaSeperatedEmailAddresses %>" /></p>
            <asp:TextBox ID="txtEmails" runat="server" TextMode="MultiLine" Width="335" Height="100" />
             <iAppsControls:iAppsCustomValidator ID="cv_txtEmails" ControlToValidate="txtEmails"
                FieldName="<%$ Resources:GUIStrings, Email %>" runat="server" ValidateType="Emails" />
        </div>
         <div class="clear-fix" style="display: none;">
            <div class="checkbox-list">
                <asp:CheckBoxList ID="chkContactLists" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" CausesValidation="false" />
    <asp:Button ID="btnSend" runat="server" CssClass="primarybutton" OnClick="btnSend_Click" Text="<%$ Resources:GUIStrings, Send %>" OnClientClick="return OnSendTestEmail();" />
</asp:Content>
