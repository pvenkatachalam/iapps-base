﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="EmailBestPractice.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.EmailBestPractice" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server"></asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ul>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, WatchfortriggerwordsCompeteforyourbusinessBestpriceguaranteeareallwordsthattheunsolicitedemailfiltersarelookingforseebelow %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Makesureyouremailisactionableandunderstandablewithimagesturnedoff %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Keepitpersonalizedandsoundconversational %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, EmbedlinksthroughoutcontentbutavoidClickhere %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RecipientsreadthefirstparagraphandimmediatelyjumptoactionitemsFocusontheseareasastheyareusuallyviewableinpreviewwindows %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Completethefooterwithyourphysicaladdressoptoutprivacypolicylinksandasafesendermessage %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Dontputallofyourwebsitecontentinyouremailsmorewordsmeansmoredangeroftriggeringunsolicitedemailfilters %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, RemoveyoursalutationPersonalizewheneverpossibleButtheresabigdifferencebetweensendingcontentthatsrelatedtoarecentpurchaseandsimplypersonalizinganemailwithaDearNameSpamfilterscaneasilyflaganemailwithDearasapieceofspamWesuggestforgoingthesalutationcompletely %>" /></li>
        <li>
            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, GetridofbuzzandbulletedlistsSpamfiltersalsolookforspecificformattingandlanguagethatscreamsspamLikewiseusingtoomanyindustrybuzzwordscanmakeyourhelpfulmessageseemlikeitsasolicitationWriteemailsasyouwouldspeaktosomeone %>" /></li>
    </ul>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
