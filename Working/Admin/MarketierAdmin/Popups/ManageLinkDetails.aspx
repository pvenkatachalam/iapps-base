﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ManageLinkDetails.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageLinkDetails" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ddlMergeField_OnChange(ddlId, txtId) {
            var ddl = document.getElementById(ddlId);
            var txt = document.getElementById(txtId);
            var mergeField = ddl.options[ddl.selectedIndex].value;

            txt.style.display = 'none';
            if (mergeField == "(Other)")
                txt.style.display = '';
        }

        function fn_SavePage() {
            if (Page_ClientValidate('QueryBuilder')) {
                alert('valid');
            }
            else {
                alert('invalid');
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="vsLinks" runat="server" ShowMessageBox="true" ShowSummary="false" ValidationGroup="EmailLinks" />
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Name %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtLinkName" runat="server" Width="300" />
            <asp:RequiredFieldValidator ID="rvLinkName" runat="server" ControlToValidate="txtLinkName" ValidationGroup="EmailLinks"
                Display="None" ErrorMessage="<%$ Resources:JSMessages, Pleaseenteralinkname %>" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.URL %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtLinkURL" runat="server" Width="300" />
            <asp:RequiredFieldValidator ID="rvLinkURL" runat="server" ControlToValidate="txtLinkURL" ValidationGroup="EmailLinks"
                Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseenteralinkURL %>" />
        </div>
    </div>
    <p><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Addadditionalparameterstosendtothelinkspage %>" /></p>
    <asp:UpdatePanel ID="upQuerystringBuilder" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="button-row">
                <asp:Button ID="btnAddParameter" runat="server" Text="<%$ Resources:GUIStrings, AddParameter %>"
                    ToolTip="<%$ Resources:GUIStrings, AddNewParameter %>" CssClass="button" />
            </div>
            <asp:Repeater ID="rptQuerystring" runat="server">
                <HeaderTemplate>
                    <table class="grid" width="100%">
                        <thead>
                            <tr>
                                <th><%= GUIStrings.ParameterName %></th>
                                <th><%= GUIStrings.ParameterValue %></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtParameterName" runat="server" />
                            <%--<asp:RequiredFieldValidator ID="rvParameterName" runat="server" ControlToValidate="txtParameterName" Text=""
                                ErrorMessage="Please provide a value for parameter name" ValidationGroup="QueryBuilder" Display="None" />
                            <asp:RegularExpressionValidator ValidationGroup="QueryBuilder" ID="revParameterName" SetFocusOnError="true" runat="server" ControlToValidate="txtParameterName" 
                                Text="" ErrorMessage="<%$ Resources:JSMessages, Pleaseentervalidparametername %>" ValidationExpression="[a-zA-Z]{1,40}" Display="None" />--%>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlMergeField" runat="server" />
                            <asp:TextBox ID="txtCustomValue" runat="server" />
                        </td>
                        <td>
                            <asp:ImageButton runat="server" ID="ibtnRemove" AlternateText="<%$ Resources:GUIStrings, Remove %>" 
                                ImageUrl="~/App_Themes/General/images/icon-delete-cross.png" CommandName="delete" />
                            <asp:HiddenField runat="server" ID="hidPropId" />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                        </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" ValidationGroup="EmailLinks"/>
</asp:Content>
