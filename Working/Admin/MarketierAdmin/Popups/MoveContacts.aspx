﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="MoveContacts.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.MoveContacts" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server"></asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Group %><span class="req">&nbsp;*</span></label>
        <div class="form-value">           
            <asp:DropDownList ID="ddlGroups" runat="server" Width="200"  OnSelectedIndexChanged="ddlGroups_SelectedIndexChanged" AutoPostBack="true" >                       
            </asp:DropDownList>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnMoveContacts" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Move %>"  OnClick="btnMoveContacts_Click"/>
</asp:Content>