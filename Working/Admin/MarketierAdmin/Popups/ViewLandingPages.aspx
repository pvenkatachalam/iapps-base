﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ViewLandingPages.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ViewLandingPages" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">


        function upViewLandingPages_OnLoad() {
            $(".view-landing-pages").gridActions({
                objList: $(".item-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upViewLandingPages_Callback(sender, eventArgs);
                }
            });
        }

        function upViewLandingPages_OnRenderComplete() {
            $(".view-landing-pages").gridActions("bindControls");
        }

        function upViewLandingPages_OnCallbackComplete() {
            $(".view-landing-pages").gridActions("displayMessage");
        }

        function upViewLandingPages_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];           
            var siteEditorUrl = eventArgs.selectedItem.getValue("SiteEditorUrl");
            
            var visitorPaths = eventArgs.selectedItem.getValue("PathAnalysisUrl");
            
            switch (gridActionsJson.Action) {
                case 'Edit':
                    doCallback = false;
                    window.parent.location = siteEditorUrl;
                    break;
                case 'VisitorPaths':                   
                    doCallback = false;
                    window.parent.location = visitorPaths;
                    break;
            }

            if (doCallback) {
                upViewLandingPages.set_callbackParameter(JSON.stringify(gridActionsJson));
                upViewLandingPages.callback();
            }
        }
        </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div>
        <iAppsControls:CallbackPanel ID="upViewLandingPages" runat="server" OnClientCallbackComplete="upViewLandingPages_OnCallbackComplete"
            OnClientRenderComplete="upViewLandingPages_OnRenderComplete" OnClientLoad="upViewLandingPages_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvViewlandingPages" runat="server" ItemPlaceholderID="phViewLandingPages">  
                     <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Literal ID="ltEmptyList" runat="server" Text="<%$ Resources:GUIStrings, EmptyListMessageForViewLandingPages %>"></asp:Literal>
                        </div>
                    </EmptyDataTemplate>                  
                    <LayoutTemplate>
                        <asp:literal ID="ltNoData" runat="server" Visible="false"></asp:literal>
                        <table class="grid" width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:Localize ID="locPageName" runat="server" Text="<%$ Resources:GUIStrings, PageName %>" /></th>
                                    <th>
                                        <asp:Localize ID="locClicks" runat="server" Text="<%$ Resources:GUIStrings, Clicks %>" /></th>
                                    <th>
                                        <asp:Localize ID="locBounces" runat="server" Text="<%$ Resources:GUIStrings, Bounces %>" /></th>                                   
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="phViewLandingPages" runat="server" />
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="item-list collection-row grid-item" objectid='<%# Eval("PageId") %>'>
                            <td>
                                <asp:Label ID="lblPageTitle" runat="server" CssClass="left-margin"/>
                            </td>
                            <td>
                                <asp:Label ID="lblPageClicks" runat="server" CssClass="left-margin"/>
                            </td>
                            <td>
                                <asp:Label ID="lblPageBounces" runat="server" CssClass="left-margin"/>
                                  <input type="hidden" ID="hdnJumpToSiteEditor" runat="server" datafield="SiteEditorUrl"/>
                                  <input type="hidden" ID="hdnVisitorPaths" runat="server" datafield="PathAnalysisUrl"/>
                            </td>                  
                        </tr>
                    </ItemTemplate>
                </asp:ListView>                  
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">   
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>" CssClass="button cancel-button" />
    <asp:HiddenField ID="hdnCampaignId" runat="server" ClientIDMode="Static" />
</asp:Content>
