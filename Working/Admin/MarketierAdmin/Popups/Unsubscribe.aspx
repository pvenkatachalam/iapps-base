﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="Unsubscribe.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.Popups.Unsubscribe" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
	<script type="text/javascript">
		$(document).on('change', '#<%= radlstUnsubscribe.ClientID %>', unsubscribe_onChange);

		function unsubscribe_onChange(e) {
			if ($(e.currentTarget).find(':checked').val() == 'individual' && trvSubscribedEmails.Nodes().length > 0) {
				document.getElementsByClassName('email-tree')[0].style.display = 'block';
			}
			else {
				document.getElementsByClassName('email-tree')[0].style.display = 'none';
			}

			fn_ResetPopupHeight();
		}

		function treeview_onNodeCheck(sender, eventArgs) {
			var node = eventArgs.get_node();
			var children = node.Nodes();
			if (children.length > 0) {
				node.Expand();
				if (node.Checked == true) {
					node.checkAll();
				}
				else {
					node.unCheckAll();
				}

				node.ParentTreeView.render();
			}
		}

		function fn_ResetPopupHeight() {
			var child = document.getElementsByClassName('unsubscribe-popup')[0];
			var frame = parent.document.getElementsByClassName('iapps-popup')[0];
			var popup = $(frame).parent()[0];

			var newFrameHeight = child.offsetHeight;
			var newTopPosition = popup.offsetTop - ((newFrameHeight - frame.offsetHeight) / 2);

			$(popup).animate({ top: newTopPosition }, 200);
			$(frame).animate({ height: newFrameHeight }, 200);;
		}
	</script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
	<asp:RadioButtonList ID="radlstUnsubscribe" runat="server">
		<asp:ListItem Value="all" Text="<%$ Resources:GUIStrings, Unsubscribefromallcurrentandfuturecampaigns %>" Selected="True" />
		<asp:ListItem Value="individual" Text="<%$ Resources:GUIStrings, Unsubscribefromindividualcampaigns %>" />
	</asp:RadioButtonList>
	<div class="email-tree">
		<ComponentArt:TreeView ID="trvSubscribedEmails" runat="server" ExpandSinglePath="false" SkinID="Default" 
			AutoPostBackOnSelect="false" Height="250" CausesValidation="false">
			<ClientEvents>
				<NodeCheckChange EventHandler="treeview_onNodeCheck" />
			</ClientEvents>
		</ComponentArt:TreeView>
		<asp:XmlDataSource ID="xmldsSubscribedCampaigns" EnableCaching="false" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
	<asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnUnsubscribe" runat="server" OnClick="btnUnsubscribe_Click" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Unsubscribe %>" />
</asp:Content>


