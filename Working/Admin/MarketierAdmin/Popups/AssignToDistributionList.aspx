﻿<%@ Page runat="server" Title="<%$ Resources:GUIStrings, AssigntoDistributionList %>"
    Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="AssignToDistributionList.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.AssignToDistributionList"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%=ddlDistributionGroup.ClientID%>").attr("disabled", "disabled");
            $("#<%=txtListName.ClientID%>").attr("disabled", "disabled");
        });

        function clearListText(objTextbox) {
            if (objTextbox.value == "<%= JSMessages.EnterListName %>") {
                objTextbox.value = "";
            }
        }
        function setListText(objTextbox) {
            if (objTextbox.value == "") {
                objTextbox.value = "<%= JSMessages.EnterListName %>";
            }
        }
        function toggleMode(listClicked) {
            if (listClicked == 'ExistingList') {
                cmbGroupsAndLists.enable();
                $("#<%=ddlDistributionGroup.ClientID%>").attr("disabled", "disabled");
                $("#<%=txtListName.ClientID%>").attr("disabled", "disabled");
            }
            else if (listClicked == 'NewList') {
                cmbGroupsAndLists.disable();
                $("#<%=ddlDistributionGroup.ClientID%>").removeAttr("disabled");
                $("#<%=txtListName.ClientID%>").removeAttr("disabled");
            }
        }

        function trvDistributionGroup_onNodeSelect(sender, eventArgs) {
            cmbGroupsAndLists.set_text(eventArgs.get_node().get_text());
            cmbGroupsAndLists.collapse();
        }

        function AssignContacts() {
            
            var listId = "";
            var canProceed = true;

            if (document.getElementById("<%=rdbExistingList.ClientID %>").checked == true) {
                var node = trvDistributionGroup.get_selectedNode();

                if (node != null && node.get_id() != '') {
                    listId = node.get_id();
                }
                else {
                    canProceed = false;
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseselectaGroupandDistributionList %>'/>");
                }
            }
            else if (document.getElementById("<%=rdbNewList.ClientID %>").checked == true) {
                var isValidData = validateData();

                if (isValidData == '') {
                    var groupId = document.getElementById("<%=ddlDistributionGroup.ClientID %>").value;
                    var title = document.getElementById("<%=txtListName.ClientID %>").value;

                    listId = SaveDistributionList(groupId, title);
                }
                else {
                    canProceed = false;
                    alert(isValidData);
                }
            }

        if (canProceed == true) {
            if (!isFromOverlay) {
                var contactId = document.getElementById('<%= hdnContactId.ClientID %>').value;

                    var message = AssignContactsToDistributionList(listId, contactId);
                    if (message == "true") {
                        parent.listAdded = true;
                    } else {
                        if (message == "") {
                            message = "There was an error saving contact to list. Please try again."
                        }
                        parent.errorMessage = message;
                    }
                }
                else {
                    var distributionListUrl = AssignContactsToDistributionListForLink(listId, campaignRunId, linkContainerId, targetId, controlId);
                    window.parent.opener.RedirectToParent(distributionListUrl);
                }
            }

            return canProceed;;
            
        }

        function validateData() {
            var errorMsg = ''

            if (document.getElementById("<%=ddlDistributionGroup.ClientID %>").value == '00000000-0000-0000-0000-000000000000') {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseSelecttheDistributionList %>'/>";
                errorMsg += " \n";
            }

            var listName = Trim(document.getElementById("<%=txtListName.ClientID %>").value);

            if (listName == '<asp:localize runat="server" text="<%$ Resources:JSMessages, EnterListName %>"/>' || listName == '') {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:JSMessages, DistributionListNamecouldnotbeempty %>'/>";
                errorMsg += "\n"
            }
            else if (listName.match(/[\<\>]/)) {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseremoveanyandcharactersfromtheDistributionListName %>'/>";
                errorMsg += " \n";
            }
            else if (listName.indexOf('&#0;') >= 0) {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:JSMessages, isunicodecombinationofnewlinecharacteritisnotallowedinDistributionListNamePleasereenter %>'/>";
                errorMsg += " \n";
            }

            var IsListExists = IsDistributionAvailable(listName);

            if (IsListExists == "true") {
                errorMsg += "<asp:localize runat='server' text='<%$ Resources:JSMessages, ThedistributionlistnamealreadyexistsPleaseenterauniquename %>'/>";
            }

            return errorMsg;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, PleaseselecttheDistributionListinwhichtoassigntheselectedcontacts %>" /></p>
    <div class="form-row">
        <asp:RadioButton ID="rdbExistingList" runat="server" Text="<%$ Resources:GUIStrings, ExistingDistributionList %>"
            GroupName="DistributionList" CssClass="groupRadioButtons" Checked="true" />
    </div>
    <div class="form-row">
        <ComponentArt:ComboBox ID="cmbGroupsAndLists" runat="server" SkinID="Default" DropDownHeight="175"
            DropDownWidth="280" AutoHighlight="false" AutoComplete="false" Width="285">
            <DropDownContent>
                <ComponentArt:TreeView ID="trvDistributionGroup" ExpandSinglePath="false" SkinID="Default"
                    AutoPostBackOnSelect="false" Height="171" Width="279" CausesValidation="false"
                    runat="server">
                    <ClientEvents>
                        <NodeSelect EventHandler="trvDistributionGroup_onNodeSelect" />
                    </ClientEvents>
                </ComponentArt:TreeView>
            </DropDownContent>
        </ComponentArt:ComboBox>
        <asp:XmlDataSource ID="xmldsDistibutionGroupsAndLists" EnableCaching="false" runat="server"></asp:XmlDataSource>
    </div>
    <div class="form-row">
        <asp:RadioButton ID="rdbNewList" runat="server" Text="<%$ Resources:GUIStrings, NewDistributionList %>"
            GroupName="DistributionList" />
        <asp:DropDownList ID="ddlDistributionGroup" runat="server" Width="285">
            <asp:ListItem runat="server" Text="<%$ Resources:GUIStrings, SelectGroup %>" Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="form-row">
        <asp:TextBox ID="txtListName" runat="server" Text="<%$ Resources:GUIStrings, EnterListName %>"
            CssClass="textBoxes" Width="280" onfocus="clearListText(this);" onblur="setListText(this);"></asp:TextBox>
    </div>
	<asp:HiddenField ID="hdnContactId" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
	<asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" 
		ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="button cancel-button" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClientClick="return AssignContacts();" OnClick="btnSave_Click" />
</asp:Content>
