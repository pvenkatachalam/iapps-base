﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="MoreEmailsView.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.MoreEmailsView" StylesheetTheme="General"%>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Repeater ID="rptAttachedList" runat="server">
        <HeaderTemplate>
            <table class="grid">
                <thead>
                    <th style="width: 400px;"><%= Bridgeline.iAPPS.Resources.GUIStrings.Title %></th>
                    <th style="width: 200px;">Sent/Scheduled</th>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><asp:hyperlink ID="hplEmail" runat="server"></asp:hyperlink></td>
                <td style="text-align: right;"><asp:label ID="lblEmailStatus" runat="server"></asp:label></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
