<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.SelectEmailRecipients" StylesheetTheme="General"
    CodeBehind="SelectEmailRecipients.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" ValidateRequest="false" %>

<%@ Register TagPrefix="UC" TagName="ManageContactLists" Src="~/UserControls/Contacts/ManageContactLists.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SelectListItems() {
            listItemsJson = [];
            var listItemsJsonTemp = [];
            if (popupActionsJson.CustomAttributes["ListItemsJson"] != "undefined" &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != null &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != "") {
                listItemsJsonTemp = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);
                if (listItemsJsonTemp.length > 0) {
                    $.each(listItemsJsonTemp, function () {
                        listItemsJson.push({ "Id": this.Id, "Title": this.Title, "Contacts": this.Contacts });
                    });
                }
            }

            $.each(grdListContent.dataSource.data(), function (index, item) {
                listItemsJson.push({ "Id": item.Id, "Title": item.Title, "Contacts": item.Contacts });
            });

            popupActionsJson.CustomAttributes["ListItemsJson"] = JSON.stringify(listItemsJson);
            $("#<%= hdnJsonSelectedValues.ClientID %>").val(JSON.stringify(listItemsJson));

            return true;
        }

        var grdListContent;
        var listItemsJson = [];
        $(function () {
            listItemsJson = [];
            grdListContent = $("#grdListContent").kendoGrid({
                dataSource: listItemsJson,
                columns: [{
                    field: "Title",
                    template: kendo.template($("#grdListContentTemplate").html()),
                    headerAttributes: { style: "display: none" }
                }]
            }).data("kendoGrid");

            var selectedContacts = $("#<%= hdnSelectedContactLists.ClientID %>").val();
            if (typeof selectedContacts != "undefined" && selectedContacts != "") {
                var selectedContactsJson = JSON.parse(selectedContacts);

                $.each(selectedContactsJson, function () {
                    listItemsJson.push({ "Id": this.Id, "Title": this.Title, "Contacts": this.TotalContacts });                 
                    AddNewListItem(this.Id, this.Title, this.TotalContacts);
                });
            }
        });

        function UpdateManualListItem(item, selected) {
            if (selected) {
                if (!CheckListItemExist(item.Key)) {
                    listItemsJson.push({ "Id": item.Key, "Title": item.getValue("Title"), "Contacts": item.getValue("Contacts") });
                    AddNewListItem(item.Key, item.getValue("Title"), item.getValue("Contacts"));
                }
            }
            else {
                DeleteListItem(item.Key);
            }
        }

        function DeleteManualListItem(id) {
            DeleteListItem(id);
            UnselectItem(id);
        }

        function DeleteListItem(id) {
            var $row = $(".manual-list-right .modal-grid-row[objectid='" + id + "']")
            var dataItem = grdListContent.dataItem($row.closest("tr"));

            $.each(listItemsJson, function (i) {
                if (this.Id.toString() == dataItem.Id.toString()) {
                    listItemsJson.splice(i, 1);
                    return false;
                }
            });

            grdListContent.dataSource.remove(dataItem);

            UpdateTotalContacts();
            return false;
        }

        function AddNewListItem(id, title, contacts) {
            var dataSource = grdListContent.dataSource;
            var rowNumber = dataSource.data().length;

            var newRowItem = {};
            newRowItem.Id = id;
            newRowItem.Title = title;
            newRowItem.Contacts = parseInt(contacts);

            dataSource.insert(rowNumber, newRowItem);

            UpdateTotalContacts();
        }

        function UpdateTotalContacts() {
            var dataSource = grdListContent.dataSource;
            var total = 0;
            $(dataSource.view()).each(function (index, element) {
                total += element.Contacts;
            });

            $("#total_contacts").text(total);
        }

        function CheckListItemExist(id) {
            var exists = false;
            $.each(listItemsJson, function () {
                if (this.Id.toString() == id.toString()) {
                    exists = true;
                    return false;
                }
            });
            return exists;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:HiddenField ID="hdnJsonSelectedValues" runat="server" />
    <asp:HiddenField ID="hdnSelectedContactLists" runat="server" />
    <div class="manual-list-left">
        <UC:ManageContactLists ID="ucManageContactLists" runat="server" />
    </div>
    <div class="manual-list-right">
        <h2>
            <asp:Literal ID="ltGridHeading" runat="server" Text="<%$ Resources:GUIStrings, SelectedContactLists %>" /></h2>
        <div class="grid-section">
            <div class="row">
                <div class="modal-grid-row clear-fix">
                    <div class="first-cell">
                        <strong>
                            <%= Bridgeline.iAPPS.Resources.GUIStrings.TotalNumberOfContacts %>:
                        </strong>
                    </div>
                    <div class="last-cell" id="total_contacts">
                        0
                    </div>
                </div>
            </div>
            <div class="selected-attributes" id="grdListContent">
            </div>
            <script id="grdListContentTemplate" type="text/x-kendo-template">          
                <div class="row">
                    <div class="modal-grid-row clear-fix" objectId="#: Id #">
                        <div class="first-cell">
                            #: Title #
                        </div>
                        <div class="last-cell">
                            <a class="delete-list-item">
                                <img src="/Admin/App_Themes/General/images/icon-delete-cross.png" border="0" alt="X" onclick="DeleteManualListItem('#: Id #')" /></a>
                        </div>
                    </div>
                </div>
            </script>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SaveAndClose%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAndClose %>" OnClientClick="return SelectListItems();" OnClick="btnSelect_Click" />
    <asp:Button ID="btnSaveAndContinue" runat="server" Text="<%$ Resources:GUIStrings, SaveAndSchedule%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAndSchedule %>" OnClientClick="return SelectListItems();" OnClick="btnSaveAndContinue_Click" />
</asp:Content>
