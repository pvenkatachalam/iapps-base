﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ViewWatchedEvents.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ViewWatchedEvents" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <link href="../css/PopupStyle.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript">
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items%>'/>"; //added by adams
        var _cancel = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Cancel %>'/>";
        var _close = "<asp:localize runat='server' text='<%$ Resources:GUIStrings, Close %>'/>";

        var selectedGridItem;
        var watchGoalId;
        var suspendSelect = false;
        function IsEmptyRow() {
            var isEmpty = false;

            if (grdGoals.get_table().getRowCount() == 1) {
                tempItem = grdGoals.get_table().getRow(0);
                if (tempItem.getMemberAt("WatchName").Text == "" || tempItem.getMemberAt("WatchName").Text == null)
                    isEmpty = true;
            }

            return isEmpty;
        }
        function grdGoals_onContextMenu(sender, eventArgs) {
            var evt = eventArgs.get_event();
            //debugger;
            selectedGridItem = eventArgs.get_item();
            //grdGoals.select(selectedGridItem, false);
            var clickedItemLevel = selectedGridItem.get_table().get_level();
            mnuGoals.showContextMenuAtEvent(evt);
        }
        function mnuGoals_onItemSelect(sender, eventArgs) {
            var selectedMenuId = eventArgs.get_item().get_id();
            watchGoalId = selectedGridItem.getMember("Id").get_text();
            switch (selectedMenuId) {
                case "cmEdit":
                    if (!IsEmptyRow()) {
                        suspendSelect = true;
                        grdGoals.edit(selectedGridItem);
                    }
                    break;
                case "cmdEditCampaign":
                    if (parent.EditCampaign != undefined && parent.EditCampaign != null) {
                        parent.EditCampaign(parent.selectedGridItem);
                        parent.watchedGoalsWindow.hide();
                    }
                    else {
                        parent.window.location = jmarketierAdminURL + '/Campaigns/DefineCampaign.aspx?Id=' + campaignId;
                    }
                    break;
            }
        }
        function grdGoals_onItemSelect(sender, eventArgs) {
            if (!suspendSelect) {
                selectedGridItem = eventArgs.get_item();
                var watchId = selectedGridItem.getMember("WatchID").get_text();
                cbWatchedGoals.callback(watchId);
            }
        }

        function cbWatchedGoals_onCallbackComplete(sender, eventArgs) {
        }
        function CancelClicked() {
            suspendSelect = false;
            grdGoals.editCancel();
            grdGoals.select(selectedGridItem, false);
        }
        function SaveRecord() {
            suspendSelect = false;
            grdGoals.editComplete();
            grdGoals.select(selectedGridItem, false);
        }

        //------------------- For Calender in grid - Start---------------------------

        function CalendarTo_OnChange(sender, eventArgs) {
            var selectedDate = CalendarTo.getSelectedDate();
            var txtBoxObject = document.getElementById(txtToDate);
            txtBoxObject.value = CalendarTo.formatDate(selectedDate, "MM/dd/yyyy");
        }

        function popUpToCalendar() {
            CalendarTo.set_popUpExpandControlId('calendar_to_button');
            var txtToTextBox = document.getElementById(txtToDate);
            if (txtToTextBox.disabled == false) {
                var thisDate = new Date();
                //var txtToTextBox = document.getElementById(textBoxId);

                if (Trim(txtToTextBox.value) == "") {
                    txtToTextBox.value = "";
                }
                if (txtToTextBox.value != "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>" && Trim(txtToTextBox.value) != "") {
                    if (validate_dateSummary(txtToTextBox) == true)// available in validation.js 
                    {
                        thisDate = new Date(txtToTextBox.value);
                        CalendarTo.setSelectedDate(thisDate);
                    }
                }
                else {
                    //thisDate.setDate(thisDate.getDate() - 1);
                    //CalendarTo.setSelectedDate(thisDate);
                }
                if (!(CalendarTo.get_popUpShowing()));
                CalendarTo.show();
            }
        }

        function getValueCal(control, DataField) {
            var PageName;
            if (control == "txtToDate") {

                var txtToTextBox = document.getElementById(txtToDate);
                if (txtToTextBox.value == "<asp:localize runat='server' text='<%$ Resources:JSMessages, ToDate %>'/>")
                    PageName = "";
                else
                    PageName = txtToTextBox.value;
            }

            if (PageName == "" || PageName == null || PageName == undefined) {
                PageName = "";
            }

            return [PageName, PageName];
        }

        function setValueCal(control, DataField) {
            var data = selectedGridItem.getMember("TargetDate").get_text();
            var value = new Date(data);
            if (value == "" || value == null || data == undefined) {
                var curDate = new Date();
                value = new Date(curDate);
            }
            if (control == "txtToDate") {

                var txtToTextBox = document.getElementById(txtToDate);
                if (data == "" || dateFormat == null || data == undefined)
                    txtToTextBox.value = "";
                else
                    txtToTextBox.value = CalendarTo.formatDate(value, "MM/dd/yyyy");
            }

        }

        //--------------- For Calender in grid -- End --------------------------------
        function targetChart_onDataPointHover(sender, eventArgs) {
        }
        function targetChart_onDataPointExit(sender, eventArgs) {
        }

        function CreateStatusIcon(objDataItem) {
            var status = objDataItem.getMember("Status").get_text();
            switch (status) {
                case '1':
                    return '<img src="../App_Themes/General/images/red-bullet.gif" alt="" />';
                    break;
                case '2':
                    return '<img src="../App_Themes/General/images/yellow-bullet.gif" alt="" />';
                    break;
                case '4':
                    return '<img src="../App_Themes/General/images/green-bullet.gif" alt="" />';
                    break;
                default:
                    return '<img src="../App_Themes/General/images/red-bullet.gif" alt="" />';
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
<p class="instructions"><asp:localize ID="Localize1" runat="server" text="<%$ Resources:GUIStrings, InstructionsSelectaWatchfromthelistbelowinordertoviewactivityandprojectionsinthechart %>"/></p>
<div class="clear-fix">
    <ul>
        <li class="rundate"><asp:localize ID="Localize2" runat="server" text="<%$ Resources:GUIStrings, RunDate %>"/></li>
        <li class="publishdate"><asp:localize ID="Localize3" runat="server" text="<%$ Resources:GUIStrings, PublishDate %>"/></li>
        <li class="linear"><asp:localize ID="Localize4" runat="server" text="<%$ Resources:GUIStrings, LinearConversion %>"/></li>
        <li class="actual"><asp:localize ID="Localize5" runat="server" text="<%$ Resources:GUIStrings, ActualQty %>"/></li>
        <li class="projected"><asp:localize ID="Localize6" runat="server" text="<%$ Resources:GUIStrings, Projectedratio %>"/></li>
        <li class="ratio"><asp:localize ID="Localize7" runat="server" text="<%$ Resources:GUIStrings, Projectedtrend %>"/></li>
        <li class="currentDate"><asp:localize ID="Localize8" runat="server" text="<%$ Resources:GUIStrings, CurrentDate %>"/></li>
    </ul>
</div>
<div class="chartContainer" id="chartContainer" runat="server">
    <ComponentArt:CallBack ID="cbWatchedGoals" runat="server" Width="100%" Height="300" BackColor="White">
        <Content>
            <ComponentArtChart:Chart ID="targetChart" runat="server" SkinID="LineChart" width="950px" SaveImageOnDisk="false">
                <ClientEvents>
                    <DataPointHover EventHandler="targetChart_onDataPointHover" />
                    <DataPointExit EventHandler="targetChart_onDataPointExit" />
                </ClientEvents>
            </ComponentArtChart:Chart>
        </Content>
        <LoadingPanelClientTemplate>
            <table border="0" width="100%">
                <tr align="center">
                    <td valign="middle" height="350">
                        <asp:localize ID="Localize9" runat="server" text="<%$ Resources:GUIStrings, Loading %>"/>
                    </td>
                </tr>
            </table>
        </LoadingPanelClientTemplate>
        <ClientEvents>
            <CallbackComplete EventHandler="cbWatchedGoals_onCallbackComplete" />
        </ClientEvents>
    </ComponentArt:CallBack>
</div>
<div class="gridContainer" style="padding: 0 5px;">
    <ComponentArt:Grid ID="grdGoals" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound1 %>" SkinID="Default"
        EditOnClickSelectedItem="false" AllowEditing="true" runat="server"
        RunningMode="Callback" AutoAdjustPageSize="true" AutoPostBackOnSelect="false"
        PageSize="5" CallbackCachingEnabled="false" SearchOnKeyPress="false" Height="150"
        AutoCallBackOnUpdate="true" PagerInfoClientTemplateId="PagerInfoCT">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="Row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="DataCell"
                HeadingCellCssClass="HeadingCell" HeadingCellHoverCssClass="HeadingCellHover"
                HeadingCellActiveCssClass="HeadingCellActive" HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText" SelectedRowCssClass="SelectedRow" GroupHeadingCssClass="GroupHeading"
                SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="AlternateDataRow"
                EditCommandClientTemplateId="EditCommandTemplate" EditCellCssClass="EditDataCell"
                EditFieldCssClass="textBoxes" AllowSorting="false">
                <Columns>
                    <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="WatchName" runat="server" HeadingText="<%$ Resources:GUIStrings, Watch %>"
                        HeadingCellCssClass="FirstHeadingCell" Width="270" AllowReordering="false" AllowEditing="False" DataCellCssClass="FirstDataCell" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="Status" runat="server" HeadingText="<%$ Resources:GUIStrings, Status1 %>" DataCellCssClass="DataCell StatusCell"
                        HeadingCellCssClass="HeadingCell" Width="70" AllowReordering="false" Align="Center"
                        AllowEditing="False" DataCellClientTemplateId="StatusCT" />
                    <ComponentArt:GridColumn FixedWidth="true" runat="server" HeadingText="<%$ Resources:GUIStrings, CurrentActualQty %>" HeadingCellCssClass="HeadingCell"
                        Width="120" AllowReordering="false" DataField="CurrentActualQuantity" AllowEditing="False" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="TargetQty" runat="server" HeadingText="<%$ Resources:GUIStrings, TargetQty %>"
                        HeadingCellCssClass="HeadingCell" Width="70" AllowReordering="false" DataCellCssClass="CustomQtyCss" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="EstimatedCompletionQuantity"
                        runat="server" HeadingText="<%$ Resources:GUIStrings, EstCompletionQty %>" HeadingCellCssClass="HeadingCell" Width="120"
                        AllowReordering="false" AllowEditing="False" />
                    <ComponentArt:GridColumn FixedWidth="true" DataField="TargetDate" runat="server" HeadingText="<%$ Resources:GUIStrings, TargetCompletion %>" 
                        EditControlType="Custom" EditCellServerTemplateId="TargetCompletionEditTemplate" FormatString="MM/dd/yyyy"
                        HeadingCellCssClass="HeadingCell" AllowEditing="True" Width="150" AllowReordering="false" />
                    <ComponentArt:GridColumn IsSearchable="false" FixedWidth="true" AllowSorting="False"
                        Width="60" runat="server" HeadingText="<%$ Resources:GUIStrings, Actions %>" EditControlType="EditCommand" Align="Center"
                        AllowReordering="false" />
                    <ComponentArt:GridColumn DataField="WatchID" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="StatusCT">
                ## CreateStatusIcon(DataItem) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="PagerInfoCT">
                ## stringformat(Page0of12items, currentPageIndex('grdGoals'), pageCount('grdGoals'), grdGoals.RecordCount) ##
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="EditCommandTemplate">
                <a href="javascript:SaveRecord();">
                    <img src="/iapps_images/icon-add-blue.png" border="0" alt="##_close##" title="##_close##" /></a>
                | <a href="javascript:CancelClicked();">
                    <img src="/iapps_images/icon-delete-cross.png" border="0" alt="" title="##_cancel##" /></a>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
        <ServerTemplates>
            <ComponentArt:GridServerTemplate ID="TargetCompletionEditTemplate">
                <Template>
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="textBoxes" Width="90" Enabled="true" style="float: left;margin-right: 5px;"></asp:TextBox>
                    <img id="calendar_to_button"alt="<%= GUIStrings.CalendarButton %>" class="calendar_button" style="float: left;" src="../../App_Themes/General/images/Calender-btn.gif"
                        onclick="popUpToCalendar();" />
                    <ComponentArt:Calendar runat="server" ID="CalendarTo" SkinID="Default" PopUpExpandControlId="calendar_to_button"
                        ControlType="Calendar" PopUp="Custom">
                        <ClientEvents>
                            <SelectionChanged EventHandler="CalendarTo_OnChange" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </Template>
            </ComponentArt:GridServerTemplate>
        </ServerTemplates>
        <ClientEvents>
            <ContextMenu EventHandler="grdGoals_onContextMenu" />
            <ItemSelect EventHandler="grdGoals_onItemSelect" />
        </ClientEvents>
    </ComponentArt:Grid>
</div>
<ComponentArt:Menu ID="mnuGoals" runat="server" SkinID="ContextMenu">
    <ClientEvents>
        <ItemSelect EventHandler="mnuGoals_onItemSelect" />
    </ClientEvents>
</ComponentArt:Menu>
<script type="text/javascript">
    <%=cbWatchedGoals.ClientID%>.LoadingPanelClientTemplate = '<table border="0" width="100%"><tr align="center"><td valign="middle" height="350"><%=GUIStrings.Loading %></td></tr></table>';
</script>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>