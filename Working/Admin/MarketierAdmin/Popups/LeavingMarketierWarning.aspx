﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="LeavingMarketierWarning.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.Popups.LeavingMarketierWarning" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
	<div class="warning-column-left">
		<asp:Image ID="imgProductIcon" runat="server" CssClass="warning-productIcon" />
	</div>
	<div class="warning-column-right">
		<asp:Literal ID="ltWarningMessage" runat="server" />
	</div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
	<asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" CssClass="button" Text="<%$ Resources:GUIStrings, Continue %>" />
</asp:Content>
