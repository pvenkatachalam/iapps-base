﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ViewAttachedContactLists.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ViewAttachedContactLists" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Repeater ID="rptAttachedList" runat="server">
        <HeaderTemplate>
            <table class="grid">
                <thead>
                    <th style="width: 400px;"><%= Bridgeline.iAPPS.Resources.GUIStrings.Title %></th>
                    <th style="width: 200px;"><%= Bridgeline.iAPPS.Resources.GUIStrings.TotalContacts %></th>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("key")%> </td>
                <td style="text-align: right;"><%# Eval("value")%></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
