﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ScheduleEmailSend.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ScheduleEmailSend" StylesheetTheme="General" ValidateRequest="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" src="../script/ScheduleEmailSend.js"></script>
    <script type="text/javascript">
        $(document).on('change', '.recurrence-options input[type="radio"]', RecurrenceOptionChange);

        function RecurrenceOptionChange() {
            // hide any validation errors from delivery options but not end options
            document.getElementById('<%= vsDeliveryOptionsErrors.ClientID %>').hidden = true;
            $('.recurrence-options-container .validation-error').not('.recurrence-options-end .validation-error').css('display', 'none');

            ToggleRecurrenceOptions($(this).val());
        }

        function startDateCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = startDateCalendar.getSelectedDate();
            document.getElementById('<%= txtScheduleDate.ClientID %>').value = startDateCalendar.formatDate(selectedDate, "MM/dd/yyyy");
        }

        function endDateCalendar_onSelectionChanged(sender, eventArgs) {
            var selectedDate = endDateCalendar.getSelectedDate();
            document.getElementById('<%= txtRecurEndByDate.ClientID %>').value = endDateCalendar.formatDate(selectedDate, "MM/dd/yyyy");
        }

        function StartDatePopUpCalendar() {
            var thisDate = new Date();
            obj = document.getElementById('<%= txtScheduleDate.ClientID %>').value;

            if (obj != null && obj != "") {
                startDateCalendar.setSelectedDate(new Date(obj));
            }

            if (!(startDateCalendar.get_popUpShowing())) {
                startDateCalendar.show();
            }
        }

        function EndDatePopUpCalendar() {
            var thisDate = new Date();
            obj = document.getElementById('<%= txtRecurEndByDate.ClientID %>').value;

            if (obj != null && obj != "") {
                endDateCalendar.setSelectedDate(new Date(obj));
            }

            if (!(endDateCalendar.get_popUpShowing())) {
                endDateCalendar.show();
            }
        }

        function ddlEmailContentType_OnChange(select) {
            var ddlEmailPriority = $('#<%= ddlEmailTypePriority.ClientID %>');

            switch ($(select).val()) {
                case "1":
                case "2":
                    ddlEmailPriority.prop('disabled', true);
                    break;
                case "3":
                    ddlEmailPriority.prop('disabled', false);
                    break;
            }
        }

        function fn_ToggleTimezonOption() {
            if ($('#<%= chkUseTimezone.ClientID %>').is(':checked')) {
                $('#<%= ddlScheduleTimeZone.ClientID %>').attr('disabled', 'disabled');
            }
            else {
                $('#<%= ddlScheduleTimeZone.ClientID %>').removeAttr('disabled');
            }
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="container-main clear-fix">
        <asp:ValidationSummary ID="vsSchedule" runat="server" ValidationGroup="Schedule" CssClass="validation-errors" />
        <div class="left-column">
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Date %></label>
                <div class="form-value calendar-value">
                    <asp:TextBox ID="txtScheduleDate" runat="server" />
                    <img id="calendar_button_startdate" alt="<%= GUIStrings.Calendar %>" onclick="StartDatePopUpCalendar();"
                        class="calendar-button expCalendar_button" src="/iapps_images/calendar-button.png" />
                    <ComponentArt:Calendar ID="startDateCalendar" runat="server" SkinID="Default" MaxDate="9998-12-31" PopUpExpandControlId="calendar_button_startdate">
                        <ClientEvents>
                            <SelectionChanged EventHandler="startDateCalendar_onSelectionChanged" />
                        </ClientEvents>
                    </ComponentArt:Calendar>
                </div>
                <asp:RequiredFieldValidator ID="rfvScheduleDate" runat="server" ControlToValidate="txtScheduleDate"
                    ValidationGroup="Schedule" Text="*" Display="Dynamic" CssClass="validation-error"
                    ErrorMessage="Start date is required" />
                <asp:CompareValidator ID="cvScheduleDate" runat="server" Operator="DataTypeCheck"
                    ControlToValidate="txtScheduleDate" ValidationGroup="Schedule" Text="*" Type="Date" CssClass="validation-error"
                    Display="Dynamic" ErrorMessage="Start date has invalid date format" />
                <asp:CustomValidator ID="csvScheduleDate" runat="server" ControlToValidate="txtScheduleDate" Text="*"
                    Display="Dynamic" CssClass="validation-error" OnServerValidate="csvScheduleDate_ServerValidate"
                    ValidationGroup="Schedule" />
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Time %></label>
                <div class="form-value dropdowns">
                    <asp:DropDownList ID="ddlScheduleTimeHour" runat="server" CssClass="dropdown-small">
                        <asp:ListItem Text="1" Value="1" />
                        <asp:ListItem Text="2" Value="2" />
                        <asp:ListItem Text="3" Value="3" />
                        <asp:ListItem Text="4" Value="4" />
                        <asp:ListItem Text="5" Value="5" />
                        <asp:ListItem Text="6" Value="6" />
                        <asp:ListItem Text="7" Value="7" />
                        <asp:ListItem Text="8" Value="8" />
                        <asp:ListItem Text="9" Value="9" />
                        <asp:ListItem Text="10" Value="10" />
                        <asp:ListItem Text="11" Value="11" />
                        <asp:ListItem Text="12" Value="12" />
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlScheduleTimeMinute" runat="server" CssClass="dropdown-small">
                        <asp:ListItem Text="00" Value="00" />
                        <asp:ListItem Text="05" Value="05" />
                        <asp:ListItem Text="10" Value="10" />
                        <asp:ListItem Text="15" Value="15" />
                        <asp:ListItem Text="20" Value="20" />
                        <asp:ListItem Text="25" Value="25" />
                        <asp:ListItem Text="30" Value="30" />
                        <asp:ListItem Text="35" Value="35" />
                        <asp:ListItem Text="40" Value="40" />
                        <asp:ListItem Text="45" Value="45" />
                        <asp:ListItem Text="50" Value="50" />
                        <asp:ListItem Text="55" Value="55" />
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlScheduleTimePeriod" runat="server" CssClass="dropdown-small">
                        <asp:ListItem Text="AM" />
                        <asp:ListItem Text="PM" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-row" style="margin-bottom: 10px;">
                <div class="form-value checkbox-value">
                    <asp:CheckBox ID="chkUseTimezone" runat="server" Text="<%$ Resources:GUIStrings, UseLocationsTimeZone %>" onclick="fn_ToggleTimezonOption();" Font-Bold="true" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.TimeZone %></label>
                <div class="form-value checkbox-value">
                    <asp:DropDownList ID="ddlScheduleTimeZone" runat="server" CssClass="dropdown-normal" />
                </div>
            </div>
            <div class="form-row" style="margin-bottom: 10px;">
                <label class="form-label"><%= GUIStrings.RecurringDeliveryOptions %></label>
                <div class="form-value radio-buttons">
                    <p style="margin-bottom: 20px;">
                        <asp:Literal ID="litRunCount" runat="server" />
                    </p>
                    <asp:RadioButtonList ID="radLstRecurringDeliveryOptions" runat="server" CssClass="recurrence-options" RepeatLayout="UnorderedList">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, None %>" Value="None" Selected="True" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Daily %>" Value="Daily" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Weekly %>" Value="Weekly" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Monthly %>" Value="Monthly" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Yearly %>" Value="Yearly" />
                    </asp:RadioButtonList>
                </div>
            </div>
            <!-- Begin dynamic recurrence options sections -->
            <div class="recurrence-options-container is-hidden">
                <asp:ValidationSummary ID="vsDeliveryOptionsErrors" runat="server" ValidationGroup="DeliveryOptions" CssClass="validation-errors" />
                <hr style="margin: 15px 0;" />
                <div class="recurrence-options-daily is-hidden">
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton ID="radDailyEveryDay" runat="server" GroupName="Daily" Text="<%$ Resources:GUIStrings, Every %>" Checked="true" />
                            <asp:TextBox ID="txtEveryXDays" runat="server" Width="25" Text="1" />
                            <label><%= GUIStrings.Days %></label>
                            <asp:RequiredFieldValidator ID="rfvEveryXDays" runat="server" Enabled="false" ControlToValidate="txtEveryXDays"
                                Text="*" ErrorMessage="<%$ Resources:JSMessages, Numberofdayisrequired %>" Display="Dynamic"
                                CssClass="validation-error" ValidationGroup="DeliveryOptions" />
                            <asp:RangeValidator ID="rvEveryXDays" runat="server" ControlToValidate="txtEveryXDays" Type="Integer"
                                MinimumValue="1" MaximumValue="365" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidDay %>"
                                Display="Dynamic" ValidationGroup="DeliveryOptions" CssClass="validation-error" Enabled="false" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-value radio-buttons">
                            <asp:RadioButton ID="radDailyEveryWeekday" runat="server" GroupName="Daily" Text="<%$ Resources:GUIStrings, EveryWeekday2 %>" />
                        </div>
                    </div>
                </div>
                <div class="recurrence-options-weekly is-hidden">
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <label><%= GUIStrings.Every %></label>
                            <asp:TextBox ID="txtWeeklyRecurFreq" runat="server" Width="25" Text="1" />
                            <label><%= GUIStrings.WeeksOn %></label>

                            <asp:RequiredFieldValidator ID="rfvWeeklyRecurFreq" runat="server" Enabled="false" ValidationGroup="DeliveryOptions"
                                ControlToValidate="txtWeeklyRecurFreq" Text="*" ErrorMessage="<%$ Resources:JSMessages, Numberofweekisrequired %>"
                                CssClass="validation-error" Display="Dynamic" />
                            <asp:RangeValidator ID="rvWeeklyRecurFreq" runat="server" Enabled="false" Display="Dynamic" ValidationGroup="DeliveryOptions"
                                ControlToValidate="txtWeeklyRecurFreq" Type="Integer" MinimumValue="1" MaximumValue="52" Text="*"
                                CssClass="validation-error" ErrorMessage="<%$ Resources:JSMessages, Pleaseenteravalidweek %>" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-value check-boxes">
                            <asp:CustomValidator ID="csvWeeklyRecurDays" runat="server" Text="*" Enabled="false"
                                Display="Dynamic" CssClass="validation-error" OnServerValidate="csvWeeklyRecurDays_ServerValidate"
                                ValidationGroup="DeliveryOptions" ErrorMessage="<%$ Resources:JSMessages, Mustselectatleastoneday %>" />

                            <asp:CheckBoxList ID="chkLstWeeklyRecurDays" runat="server" RepeatDirection="Horizontal" RepeatColumns="4">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Sunday %>" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Monday %>" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Tuesday %>" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Wednesday %>" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Thursday %>" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Friday %>" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Saturday %>" />
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
                <div class="recurrence-options-monthly is-hidden">
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton ID="radMonthlyRecurEveryDayNum" runat="server" GroupName="Monthly" Text="<%$ Resources:GUIStrings, Day %>" Checked="true" />
                            <asp:TextBox ID="txtMonthlyRecurDayNum" runat="server" Width="25" Text="1" />
                            <label><%= GUIStrings.ofevery %></label>
                            <asp:TextBox ID="txtMonthlyRecurMonthFreq" runat="server" Width="25" Text="1" />
                            <label><%= GUIStrings.months %></label>

                            <asp:RequiredFieldValidator runat="server" ID="rfvMonthlyRecurDayNum" Enabled="false"
                                ControlToValidate="txtMonthlyRecurDayNum" Text="*" CssClass="validation-error" ValidationGroup="DeliveryOptions"
                                ErrorMessage="<%$ Resources:JSMessages, Pleaseselectadayinthemonth %>" Display="Dynamic" />
                            <asp:RangeValidator runat="server" ID="rvMonthlyRecurDayNum" ControlToValidate="txtMonthlyRecurDayNum"
                                Type="Integer" MinimumValue="1" MaximumValue="31" Text="*" CssClass="validation-error" Display="Dynamic"
                                ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidDay %>" Enabled="false" ValidationGroup="DeliveryOptions" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvMonthlyRecurMonthFreq" Enabled="false"
                                Display="Dynamic" ControlToValidate="txtMonthlyRecurMonthFreq" Text="*" ValidationGroup="DeliveryOptions"
                                ErrorMessage="<%$ Resources:JSMessages, Pleaseselectamonthinterval %>" CssClass="validation-error" />
                            <asp:RangeValidator runat="server" ID="rvMonthlyRecurMonthFreq" ControlToValidate="txtMonthlyRecurMonthFreq"
                                Type="Integer" MinimumValue="1" MaximumValue="12" Text="*" CssClass="validation-error" Display="Dynamic"
                                ErrorMessages="<%$ Resources:JSMessages, PleaseEnterAValidMonth %>" Enabled="false" ValidationGroup="DeliveryOptions" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton ID="radMonthlyRecurEveryDayType" runat="server" GroupName="Monthly" Text="<%$ Resources:GUIStrings, Every %>" />
                            <asp:DropDownList ID="ddlMonthlyRecurDay" runat="server">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, First %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Second %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Third %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Fourth %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Last %>" Value="5" />
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlMonthlyRecurDayType" runat="server">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Day %>" Value="8" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Weekday %>" Value="9" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Weekendday %>" Value="10" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Sunday %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Monday %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Tuesday %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Wednesday %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Thursday %>" Value="5" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Friday %>" Value="6" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Saturday %>" Value="7" />
                            </asp:DropDownList>
                            <asp:TextBox runat="server" ID="txtCustomMonths" Width="25" Text="1" />
                            <label><%= GUIStrings.months %></label>

                            <asp:RequiredFieldValidator runat="server" ID="rfvCustomMonths" Enabled="false" CssClass="validation-error"
                                Display="Dynamic" ControlToValidate="txtCustomMonths" Text="*" ValidationGroup="DeliveryOptions"
                                ErrorMessage="<%$ Resources:JSMessages, Pleaseselectamonthinterval %>" />
                            <asp:RangeValidator runat="server" ID="rvCustomMonths" ControlToValidate="txtCustomMonths"
                                Type="Integer" MinimumValue="1" MaximumValue="12" Text="*" CssClass="validation-error" Display="Dynamic"
                                ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidMonth %>" ValidationGroup="DeliveryOptions" />
                        </div>
                    </div>
                </div>
                <div class="recurrence-options-yearly is-hidden">
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton runat="server" ID="radYearlyRecurOnMonth" GroupName="Yearly" Text="<%$ Resources:GUIStrings, On %>" Checked="true" />
                            <asp:DropDownList runat="server" ID="ddlYearlyRecurMonth">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, January %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, February %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, March %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, April %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, May %>" Value="5" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, June %>" Value="6" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, July %>" Value="7" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, August %>" Value="8" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, September %>" Value="9" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, October %>" Value="10" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, November %>" Value="11" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, December %>" Value="12" />
                            </asp:DropDownList>
                            <asp:TextBox runat="server" ID="txtYearlyRecurMonthDay" Width="25" Text="1" />

                            <asp:RequiredFieldValidator runat="server" ID="rfvYearlyRecurMonthDay" Enabled="false"
                                ControlToValidate="txtYearlyRecurMonthDay" Text="*" CssClass="validation-error" Display="Dynamic"
                                ErrorMessage="<%$ Resources:JSMessages, Pleaseselectadayofmonth %>" ValidationGroup="DeliveryOptions" />
                            <asp:RangeValidator runat="server" ID="rvYearlyRecurMonthDay" ControlToValidate="txtYearlyRecurMonthDay"
                                Type="Integer" MinimumValue="1" MaximumValue="31" CssClass="validation-error" Display="Dynamic" Enabled="false"
                                Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidDay %>" ValidationGroup="DeliveryOptions" />
                            <asp:CustomValidator runat="server" ID="csvDaysInMonth" OnServerValidate="csvDaysInMonth_ServerValidate"
                                Text="*" CssClass="validation-error" Display="Dynamic" ControlToValidate="txtYearlyRecurMonthDay" Enabled="false"
                                ErrorMessage="<%$ Resources:JSMessages, DayenteredforRecurEveryisnotvalidforthemonthselected %>" ValidationGroup="DeliveryOptions" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton runat="server" ID="radYearlyRecurDayType" GroupName="Yearly" Text="<%$ Resources:GUIStrings, Onthe %>" />
                            <asp:DropDownList runat="server" ID="ddlYearlyRecurDay">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, First %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Second %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Third %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Fourth %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Last %>" Value="5" />
                            </asp:DropDownList>
                            <asp:DropDownList runat="server" ID="ddlYearlyRecurWeekDay">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Sunday %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Monday %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Tuesday %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Wednesday %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Thursday %>" Value="5" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Friday %>" Value="6" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Saturday %>" Value="7" />
                            </asp:DropDownList>
                            <label><%= GUIStrings.of %></label>
                            <asp:DropDownList runat="server" ID="ddlYearlyRecurMonth2">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, January %>" Value="1" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, February %>" Value="2" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, March %>" Value="3" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, April %>" Value="4" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, May %>" Value="5" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, June %>" Value="6" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, July %>" Value="7" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, August %>" Value="8" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, September %>" Value="9" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, October %>" Value="10" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, November %>" Value="11" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, December %>" Value="12" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="recurrence-options-end">
                    <hr />
                    <asp:ValidationSummary ID="vsEndOptions" runat="server" ValidationGroup="EndOptions" CssClass="validation-errors" />
                    <div class="form-row">
                        <div class="form-value radio-buttons">
                            <asp:RadioButton runat="server" ID="radRecurNoEndDate" GroupName="End" Text="<%$ Resources:GUIStrings, NoEndDate %>" Checked="true" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton runat="server" ID="radRecurEndAfter" GroupName="End" Text="<%$ Resources:GUIStrings, Endafter1 %>" />
                            <asp:TextBox runat="server" ID="txtMaxOccurances" Width="25" Text="1" />
                            <label><%= GUIStrings.occurrences %></label>

                            <asp:RangeValidator runat="server" ID="rvMaxOccurances" ControlToValidate="txtMaxOccurances" Enabled="false"
                                Type="Integer" MinimumValue="1" MaximumValue="100000" Text="*" CssClass="validation-error"
                                Display="Dynamic" ErrorMessage="<%$ Resources:JSMessages, Recurrencevalueshouldbelessthan100000 %>" ValidationGroup="EndOptions" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvMaxOccurances" Enabled="false" CssClass="validation-error"
                                Display="Dynamic" ValidationGroup="EndOptions" ControlToValidate="txtMaxOccurances"
                                Text="*" ErrorMessage="<%$ Resources:JSMessages, Pleaseenterarecurrencevalue %>" />
                            <asp:CustomValidator runat="server" ID="csvMaxOccurrenceMet" Enabled="false" Display="Dynamic"
                                CssClass="validation-error" ErrorMessage="" ControlToValidate="txtMaxOccurances"
                                OnServerValidate="csvMaxOccurrenceMet_ServerValidate" ValidationGroup="EndOptions" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-value mixed-controls">
                            <asp:RadioButton runat="server" ID="radRecurEndBy" GroupName="End" Text="<%$ Resources:GUIStrings, Endby %>" />
                            <div class="calendar-value">
                                <asp:TextBox runat="server" ID="txtRecurEndByDate" Width="100" />
                                <img id="calendar_button_enddate" alt="<%= GUIStrings.Calendar %>" onclick="EndDatePopUpCalendar();" class="calendar-button expCalendar_button" src="/iapps_images/calendar-button.png" />
                                <ComponentArt:Calendar runat="server" SkinID="Default" ID="endDateCalendar" MaxDate="9998-12-31" PopUpExpandControlId="calendar_button_enddate">
                                    <ClientEvents>
                                        <SelectionChanged EventHandler="endDateCalendar_onSelectionChanged" />
                                    </ClientEvents>
                                </ComponentArt:Calendar>
                            </div>
                            <asp:RequiredFieldValidator runat="server" Enabled="false" ID="rfvRecurEndByDate"
                                CssClass="validation-error" Display="Dynamic" ControlToValidate="txtRecurEndByDate" Text="*"
                                ErrorMessage="<%$ Resources:JSMessages, Endbydateisrequired %>" ValidationGroup="EndOptions" />
                            <asp:CompareValidator ID="cvRecurEndByDate" runat="server" Operator="DataTypeCheck" Type="Date" Enabled="false"
                                CssClass="validation-error" Display="Dynamic" ControlToValidate="txtRecurEndByDate" Text="*"
                                ErrorMessage="<%$ Resources:JSMessages, Endbydatehasinvaliddateformat %>" ValidationGroup="EndOptions" />
                            <asp:CustomValidator ID="csvRecurEndByDate" runat="server" ControlToValidate="txtRecurEndByDate" Text="*"
                                Display="Dynamic" CssClass="validation-error" OnServerValidate="csvRecurEndByDate_ServerValidate" Enabled="false"
                                ValidationGroup="EndOptions" ErrorMessage="<%$ Resources:JSMessages, Endbydateshouldnotbeearlierthanstartdate %>" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-column">
            <asp:Panel ID="pnlAdditionalOptions" runat="server">
                <div class="form-row">
                    <label class="form-label"><%= GUIStrings.DeliveryOptionsOrder %></label>
                    <div class="form-value">
                        <asp:DropDownList runat="server" ID="ddlEmailContentType" Width="250" onChange="ddlEmailContentType_OnChange(this)">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, SendHTMLOnly %>" Value="1" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, SendTextOnly %>" Value="2" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, SendHTMLText %>" Value="3" Selected="true" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value">
                        <asp:DropDownList runat="server" ID="ddlEmailTypePriority" Width="250">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, HTMLFirst %>" Value="1" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, TextFirst %>" Value="2" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value checkbox-value">
                        <asp:CheckBox runat="server" ID="chkUniqueRecipientsOnly" Text="<%$ Resources:GUIStrings, Onlysendemailtorecipientoncepercampaign %>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value checkbox-value">
                        <asp:CheckBox runat="server" ID="chkAutoUpdateLists" Text="<%$ Resources:GUIStrings, Useasnapshotofapreviousrunrecipientlist %>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value checkbox-value">
                        <asp:CheckBox runat="server" ID="chkSendToNotTriggered" Text="<%$ Resources:GUIStrings, Onlysendemailtorecipientsthathavenottriggeredcampaignwatches %>" />
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSaveAndClose" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, SaveAndClose %>" ValidationGroup="Schedule" />
    <asp:Button ID="btnSaveAndContinue" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, ReviewAndSend %>" ValidationGroup="Schedule" ClientIDMode="Static" />
</asp:Content>
