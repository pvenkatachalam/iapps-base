﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="WorkflowRejectionNotes.aspx.cs" 
	Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.Popups.WorkflowRejectionNotes" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
	<script type="text/javascript">
		function btnContinue_Click() {
			popupActionsJson.CustomAttributes['rejectionNotes'] = $('#<%= txtNotes.ClientID %>').val();
			CanceliAppsAdminPopup(true);
			return false;
		}
	</script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
	<asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Height="100" Width="400" placeholder="Tell other users why you are rejecting this campaign." />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
	<asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnContinue" runat="server" OnClientClick="return btnContinue_Click()" CssClass="button primarybutton" Text="<%$ Resources:GUIStrings, Continue %>" />
</asp:Content>
