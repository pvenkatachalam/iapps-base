﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CampaignOverlay.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.CampaignOverlay" EnableViewState="false" StylesheetTheme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, iAPPSMarketierOverlays %>" /></title>
    <!-- Mimic Internet Explorer 7 -->
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <link href="../css/Overlay.css" rel="Stylesheet" type="text/css" media="all" />
    <link href="../css/PopupStyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../script/SiteOverlay.js"></script>
    <script type="text/javascript">
        var selectedIds = "";
        var listAdded = false;
        var distributionListWindow = undefined;
        var Page0of12items = "<asp:localize runat='server' text='<%$ Resources:JSMessages, Page0of12items %>'/>"; //added by adams
        $(document).ready(function () {
            if (typeof ContainerSpecificAnchorOverlay == 'function') {
                setTimeout('ContainerSpecificAnchorOverlay()', 2000);
            }
        });
        $(document).resize(function () {
            if (typeof ContainerSpecificAnchorOverlay == 'function') {
                document.getElementById('overlay').innerHTML = '';
                document.getElementById('overlayDetails').innerHTML = '';
                setTimeout('ContainerSpecificAnchorOverlay()', 500);
            }
        });
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnOverlayHelper" runat="server" ClientIDMode="Static" />
        <div id="overlay" class="overlayPlaceHolder">
        </div>
        <div id="overlayDetails" class="overlayPlaceHolder">
        </div>
        <div class="iAPPS_CampaignHtmlContainer" id="phCampaignHtml" runat="server">
        </div>

        <div class="iapps-modal" id="divViewContacts" style="display: none;">
            <div class="modal-header clear-fix">
                <h2>
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ViewContacts %>" /></h2>
            </div>
            <div class="modal-content clear-fix">
                <ul class="productsLegend clear-fix">
                    <li class="label"><span>
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ContactType %>" /></span></li>
                    <li>
                        <img src="../App_Themes/General/images/green-square-bullet.png" alt="" />
                        <span>
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Commerce %>" /></span> </li>
                    <li>
                        <img src="../App_Themes/General/images/blue-square-bullet.png" alt="" />
                        <span>
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, ContentManager %>" /></span> </li>
                    <li>
                        <img src="../App_Themes/General/images/orange-square-bullet.png" alt="" />
                        <span>
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Marketier %>" /></span> </li>
                </ul>
                <ComponentArt:Grid SkinID="Default" ID="grdContacts" SliderPopupClientTemplateId="CustomSliderTemplate"
                    Width="632" runat="server" RunningMode="Callback" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>"
                    AutoPostBackOnSelect="false" PageSize="10" SearchOnKeyPress="false" Height="250" CssClass="tabular-grid">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" HeadingRowCssClass="heading-row"
                            SelectedRowCssClass="selected-row" AlternatingRowCssClass="alternate-row"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                            SortImageHeight="7" AllowGrouping="false">
                            <Columns>
                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                <ComponentArt:GridColumn DataField="Type" HeadingText="Product" 
                                    Align="Center" IsSearchable="true" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="ProductHoverTemplate"
                                    Width="60" />
                                <ComponentArt:GridColumn Width="95" runat="server" HeadingText="<%$ Resources:GUIStrings, LastName %>"
                                    DataField="LastName" Align="Left"
                                    IsSearchable="true" AllowReordering="false" FixedWidth="true" DataCellClientTemplateId="LastNameHoverTemplate" />
                                <ComponentArt:GridColumn Width="95" runat="server" HeadingText="<%$ Resources:GUIStrings, FirstName %>" DataField="FirstName"
                                    Align="Left" AllowReordering="false"
                                    FixedWidth="true" DataCellClientTemplateId="FirstNameHoverTemplate" />
                                <ComponentArt:GridColumn Width="160" runat="server" HeadingText="<%$ Resources:GUIStrings, EmailAddress %>" DataField="Email"
                                    Align="Left" AllowReordering="false"
                                    FixedWidth="true" DataCellClientTemplateId="EmailHoverTemplate" />
                                <ComponentArt:GridColumn Width="90" runat="server" HeadingText="<%$ Resources:GUIStrings, Status1 %>" DataField="Status" Align="Left"
                                    DataType="System.String" AllowReordering="false"
                                    FixedWidth="true" DataCellClientTemplateId="StatusHoverTemplate" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="CheckBoxTemplate">
                            <div>
                                <input type="checkbox" id="CheckAllFilteredContacts" onclick="grdContacts_CheckAll(this);" />
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="ProductHoverTemplate">
                            ## GetProductImage(DataItem) ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="LastNameHoverTemplate">
                            <span title="## DataItem.GetMember('LastName').get_text() ##">## DataItem.GetMember('LastName').get_text()
                                == "" ? "&nbsp;" : DataItem.GetMember('LastName').get_text() ##</span>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="FirstNameHoverTemplate">
                            <span title="## DataItem.GetMember('FirstName').get_text() ##">## DataItem.GetMember('FirstName').get_text()
                                == "" ? "&nbsp;" : DataItem.GetMember('FirstName').get_text() ##</span>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="EmailHoverTemplate">
                            <span title="## DataItem.GetMember('Email').get_text() ##">## DataItem.GetMember('Email').get_text()
                                == "" ? "&nbsp;" : DataItem.GetMember('Email').get_text() ##</span>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="StatusHoverTemplate">
                            ## switch(DataItem.GetMember("Status").get_text()) { case "1": var contactStatus
                            = "Active"; break; default: var contactStatus = "Inactive"; } ## <span title="## contactStatus ##">## contactStatus ##</span>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="CustomSliderTemplate">
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="CustomPaginationTemplate">
                            ## stringformat(Page0of12items, currentPageIndex('grdContacts'), pageCount('grdContacts'), grdContacts.RecordCount) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                    <ClientEvents>
                        <CallbackComplete EventHandler="grdContacts_onCallbackComplete" />
                    </ClientEvents>
                </ComponentArt:Grid>
            </div>
            <div class="modal-footer clear-fix">
                <input type="button" value="<%= GUIStrings.Close %>" title="<%= GUIStrings.Close %>" class="button" onclick="CloseContactsPopup();" />
            </div>
        </div>
    </form>
</body>
</html>
