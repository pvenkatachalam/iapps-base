﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="InsertManagedLink.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.InsertManagedLink" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".left-control .group-item").on("click", function () {
                $(this).addClass("selected");
                $(this).siblings().removeClass("selected");
                gridActionsJson.CustomAttributes["SelectedGroup"] = $.trim($(this).find("h4").html());
                $(".insert-managed-link").gridActions("selectNode", $(this).find(".hdnGroupId").val());
            });

            $(".left-control .group-item[objectId='" + gridActionsJson.FolderId + "']").addClass("selected");
            if ($(".left-control .group-item.selected").length > 0)
                $(".left-control .grid-item-button, .right-control .grid-button").show();
            else
                $(".left-control .grid-item-button, .right-control .grid-button").hide();
        });

        function upLinks_OnLoad() {
            $(".insert-managed-link").gridActions({
                objList: $(".link-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upLinks_Callback(sender, eventArgs);
                }
            });
        }

        function upLinks_OnRenderComplete() {
            $(".insert-managed-link").gridActions("bindControls");
            $(".insert-managed-link").iAppsSplitter({ leftScrollSection: ".group-list", maxHeight: 700 });

            if ($(".left-control .group-item.selected").length > 0)
                $(".left-control .grid-item-button").show();
            else
                $(".left-control .grid-item-button").hide();
        }

        function upLinks_OnCallbackComplete() {
            $(".insert-managed-link").gridActions("displayMessage");
        }

        function upLinks_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedGroupId = $(".left-control .group-item.selected .hdnGroupId").val();

            switch (gridActionsJson.Action) {
                case "AddNewLink":
                    doCallback = false;
                    break;
            }

            if (doCallback) {
                upLinks.set_callbackParameter(JSON.stringify(gridActionsJson));
                upLinks.callback();
            }
        }

        function fn_InsertManagedLink() {
            var selectedItem = $(".insert-managed-link").gridActions("getSelectedItem");
            if (selectedItem) {
                popupActionsJson.CustomAttributes["Id"] = selectedItem.Key;
                SelectiAppsAdminPopup();
            }
            else {
                alert("Please select a link to insert.");
        }

        return false;
    }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="left-control">
        <div class="tree-header clear-fix">
            <h2><asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, LinkGroups %>" /></h2>
        </div>
        <asp:ListView ID="lvLinkGroups" runat="server" ItemPlaceholderID="phLayoutList">
            <LayoutTemplate>
                <div class="group-container">
                    <div class="group-list">
                        <asp:PlaceHolder ID="phLayoutList" runat="server" />
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="group-item clear-fix" objectid="<%# Eval("Id") %>">
                    <div class="link-group">
                        <span><%# Eval("Description") %></span>
                        <input type="hidden" id="hdnGroupId" runat="server" class="hdnGroupId" value='<%# Eval("Id") %>' />
                        <input type="hidden" id="hdnGroupName" runat="server" class="hdnGroupName" value='<%# Eval("Description") %>' />
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upLinks" runat="server" OnClientCallbackComplete="upLinks_OnCallbackComplete"
            OnClientRenderComplete="upLinks_OnRenderComplete" OnClientLoad="upLinks_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <div class="grid-section">
                    <asp:ListView ID="lvLinks" runat="server" ItemPlaceholderID="phLinkList">
                        <EmptyDataTemplate>
                            <div class="empty-list">
                                No links found!
                            </div>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div class="collection-table fat-grid link-list">
                                <asp:PlaceHolder ID="phLinkList" runat="server" />
                            </div>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                <div class="modal-grid-row clear-fix grid-item" objectid='<%# Eval("Id") %>'>
                                    <div class="large-cell">
                                        <p datafield="URL"><%# Eval("URL") %></p>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnInsert" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Insert %>" OnClientClick="return fn_InsertManagedLink();" />
</asp:Content>
