﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="AutoresponderStatistics.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.AutoresponderStatistics" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.Name %></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltName" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.Status %></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltStatus" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.Delivered %></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltDelivered" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.Opens %></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltOpens" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.UniqueOpen %></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltUniqueOpens" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label colon"><%= GUIStrings.Clicks %></label>
        <div class="form-value text-value">
            <asp:Literal ID="ltClicks" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>