﻿<%@ Page runat="server" Title="<%$ Resources:GUIStrings, ElapsedTime %>" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master"
    AutoEventWireup="true" CodeBehind="ImportElapsedTime.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ImportElapsedTime"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function OpenImportSummary() {
            OpeniAppsMarketierPopup('ContactImportResult');
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p class="timer">
        <asp:Label ID="lblTimer" runat="server" />
    </p>
    <p>
        <asp:Label ID="lblStatus" runat="server" />
    </p>
    <asp:Literal ID="outputData" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnComplete" value="<%= GUIStrings.EMailMeWhenComplete %>" title="<%= GUIStrings.EMailMeWhenComplete %>"
        class="button cancel-button" onclick="return Update(this);" />
    <script type="text/javascript">
        function Update(btn) {
            if (btn.valueOf == "Close") {
                parent.fn_CloseElapsedPopup();
            }
            else {
                Save();
                window.onload = null;
                parent.fn_CloseElapsedPopup();
            }
        }
    </script>
</asp:Content>

