﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageAutoResponderDetails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageAutoResponderDetails" StylesheetTheme="General" %>

<%@ Register TagPrefix="cp" TagName="ContactProperties" Src="~/UserControls/Contacts/InsertContactProperties.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upAutoresponders_OnCallbackComplete(sender, eventArgs) {
            $(".email-section").gridActions("displayMessage");
        }

        function upAutoresponders_OnRenderComplete(sender, eventArgs) {
            $(".email-section").gridActions("bindControls");
            $(".email-section").iAppsSplitter();
        }


        function upAutoresponders_OnLoad(sender, eventArgs) {
            $(".email-section").gridActions({
                objList: $(".collection-table"),
                type: "list",
                hasBorder: true,
                onCallback: function (sender, eventArgs) {
                    upAutoresponders_Callback(sender, eventArgs);
                }
            });
        }

        function upAutoresponders_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedResponseId = $('#hdnResponseId').val();
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";

            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("Title");

            switch (gridActionsJson.Action) {

                case "AddNew":
                    doCallback = false;
                    OpenCreateEmailPopup("IsResponse=true&GroupId=" + selectedResponseId);
                    break;
                case "Edit":
                    doCallback = false;
                    JumpToEmailFrontPage(MarketierCallback.GetCampaignPageUrl(selectedResponseId, selectedItemId),
                        "Response=true&EmailId=" + selectedItemId + "&ResponseId=" + selectedResponseId);
                    break;
                case "Delete":
                    //This hidden value is not updating for some reason..
                    if ($("#<%= hdnNoOfEmails.ClientID %>").val() == "1") {
                        doCallback = window.confirm("Deleting all emails will make the autoresponder change to draft status");
                    }
                    else
                        doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisEmail %>");
                    break;
                case "TestEmail":
                    doCallback = false;
                    var qs = "CampaignId=" + selectedResponseId + "&EmailId=" + selectedItemId + "&GetFromVersion=false";
                    OpeniAppsMarketierPopup('SendTestEmail', qs, '');
                    break;
            }

            if (doCallback) {
                upAutoresponders.set_callbackParameter(JSON.stringify(gridActionsJson));
                upAutoresponders.callback();
            }
        }

        function OnClientClickSaveAsDraft(btnObj) {

            if (typeof providerOptionValidation == 'function')
                return providerOptionValidation();

            return true;
        }

        function ValidateRequiredFields(btnObj) {
            var postback = false;
            if (Page_ClientValidate()) {
                if (typeof providerOptionValidation == 'function')
                    return providerOptionValidation();
                postback = true;
            }
            return postback;
        }

    </script>
</asp:Content>

<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><%= GUIStrings.AddEditAutoresponders %></h1>
        <asp:Button ID="btnPublishTop" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Publish %>" OnClick="btnPublish_Click" OnClientClick="return ValidateRequiredFields();" />
        <asp:Button ID="btnSaveTop" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" OnClientClick="return ValidateRequiredFields();" Visible="true" />
        <a href="ManageAutoResponders.aspx" class="button"><%= GUIStrings.Cancel %></a>
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:HiddenField ID="hdnNoOfEmails" runat="server" />
    <asp:ValidationSummary ID="valAutoresponder" runat="server" ShowMessageBox="true"
        ShowSummary="false" DisplayMode="BulletList" />
    <div class="container-box clear-fix">
        <div class="details-section">
            <h3>1. <%= GUIStrings.AutoresponderDetails %></h3>
            <div class="form-row">
                <label class="form-label" for="<%=txtResponseName.ClientID%>">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Name %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtResponseName" runat="server" CssClass="textBoxes" Width="315" />
                    <asp:RequiredFieldValidator ID="rfvResponseName" runat="server" ControlToValidate="txtResponseName"
                        ErrorMessage="<%$ Resources:JSMessages, Pleaseenteraresponsename %>"
                        Display="None" CssClass="validation-error" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtResponseName" runat="server" ValidateType="Title"
                        ControlToValidate="txtResponseName" ErrorMessage="<%$ Resources:JSMessages, InvalidcharactersintheresponsenamePleaseremovetheandcharacters %>" />
                    <asp:HiddenField ID="hdnResponseId" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row" style="margin-bottom: 0;">
                <label class="form-label">&nbsp;</label>
                <div class="form-value">
                    <a href="javascript://" onclick="return fn_ShowContactProperties('txtSenderName');"><%= GUIStrings.InsertDynamicContactDetails %></a>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label" for="<%=txtSenderName.ClientID%>">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, SenderName %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtSenderName" runat="server" ClientIDMode="Static" Width="315" />
                    <asp:RequiredFieldValidator ID="rfvSenderName" runat="server" ControlToValidate="txtSenderName"
                        ErrorMessage="<%$ Resources:JSMessages, Pleaseenterasendername %>" Display="None" CssClass="validation-error" />
                    <asp:RegularExpressionValidator ID="rvtxtSenderName" runat="server" ValidationExpression="(^[^&lt;>]+$)|(^\[[\w]*:[\w]*\]$)"
                        ControlToValidate="txtSenderName" ErrorMessage="<%$ Resources:JSMessages, InvalidcharactersinthesendernamePleaseremovetheandcharacters %>" Display="None" CssClass="validation-error" />
                    <asp:HiddenField ID="hdnResponseName" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row" style="margin-bottom: 0;">
                <label class="form-label">&nbsp;</label>
                <div class="form-value">
                    <a href="javascript://" onclick="return fn_ShowContactProperties('txtSenderEmail');"><%= GUIStrings.InsertDynamicContactDetails %></a>
                </div>
            </div>
            <div class="form-row" style="margin-bottom: 20px;">
                <label class="form-label" for="<%=txtSenderEmail.ClientID%>">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, SenderEmail %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtSenderEmail" runat="server" ClientIDMode="Static" Width="315" />
                    <asp:RequiredFieldValidator ID="rfvSenderEmail" runat="server" ControlToValidate="txtSenderEmail"
                        ErrorMessage="<%$ Resources:JSMessages, Pleaseenterasenderemail %>" Display="None" CssClass="validation-error" />
                    <asp:RegularExpressionValidator ID="regSenderEmail" runat="server" ValidationExpression="(^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$)|(^\[[\w]*:[\w]*\]$)"
                        ControlToValidate="txtSenderEmail" ErrorMessage="<%$ Resources:JSMessages, EmailNotValid %>" Display="None" CssClass="validation-error" />
                </div>
            </div>
            <cp:ContactProperties ID="ctlContactProperties" runat="server" />
            <div class="form-row">
                <label class="form-label" for="<%=txtSenderEmail.ClientID%>">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtAutoresponderDescription" runat="server" Width="315" Height="100" TextMode="MultiLine" />
                </div>
            </div>
        </div>
        <div class="trigger-section">
            <h3>2. <%= GUIStrings.AutoresponderTriggers %></h3>
            <p class="help-text"><%= GUIStrings.InstructionsSelecttheuseractionsthatwilltriggeranemailresponse %></p>
            <asp:Panel runat="server" ID="pnlUserActions" CssClass="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, IdentifyUserActions %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlListOfProviders" AutoPostBack="true" runat="server" Width="150" />
                </div>
                <asp:Panel ID="pnlUserActionsHelp" runat="server" Visible="false" CssClass="help-text-exclamation">
                    <%= GUIStrings.TheUserActionCannotBeEditedOnceTheAutoresponderHasBeenCreated %>
                </asp:Panel>
            </asp:Panel>
            <div class="clear-fix">
                <asp:Panel ID="pnlDynamic" runat="server" />
            </div>
            <asp:HiddenField ID="TriggeredProviderOptionValue" runat="server" />
            <asp:HiddenField ID="TriggeredProviderName" runat="server" />
        </div>
    </div>


    <div class="container-box clear-fix step3-header">
        <h3>3. <%= GUIStrings.CreateAutoresponderEmailsAndSchedule %></h3>
        <span class="help-text">
            <asp:Literal ID="ltSaveHelpText" Text="<%$ Resources:GUIStrings, SaveTheAutoresponderDetailsToAddAndScheduleAnEmail %>" runat="server" /></span>
    </div>
    <asp:Panel ID="pnlScheduleEmail" runat="server" Visible="false">
        <div class="email-section">
            <iAppsControls:CallbackPanel ID="upAutoresponders" runat="server" OnClientCallbackComplete="upAutoresponders_OnCallbackComplete"
                OnClientRenderComplete="upAutoresponders_OnRenderComplete" OnClientLoad="upAutoresponders_OnLoad">
                <ContentTemplate>
                    <iAppsControls:GridActions ID="gridActions" runat="server" />
                    <div class="grid-section">
                        <asp:ListView ID="lvAutoresponders" runat="server" ItemPlaceholderID="phEmailList">
                            <EmptyDataTemplate>
                                <div class="empty-list">
                                    <%= GUIStrings.NoAutoresponderEmails%>
                                </div>
                            </EmptyDataTemplate>
                            <LayoutTemplate>
                                <div class="collection-table fat-grid">
                                    <asp:PlaceHolder ID="phEmailList" runat="server" />
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                                    <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id") %>'>
                                        <%--<div class="first-cell">
                                            </div>--%>
                                        <div class="large-cell">
                                            <h4 datafield="Title">
                                                <asp:Literal ID="ltEmailName" runat="server" />
                                            </h4>
                                            <p>
                                                <asp:Literal ID="ltTemplateName" runat="server" />
                                            </p>
                                        </div>
                                        <div class="small-cell">
                                            <div class="child-row">
                                                <asp:Literal ID="ltTriggerTime" runat="server" />
                                            </div>
                                            <div class="child-row">
                                                <asp:Label ID="lblDelivered" runat="server" CssClass="left-margin" />
                                                <asp:Label ID="lblOpens" runat="server" CssClass="left-margin" />
                                                <asp:Label ID="lblUniqueOpens" runat="server" CssClass="left-margin" />
                                                <asp:Label ID="lblClicks" runat="server" CssClass="left-margin" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </ContentTemplate>
            </iAppsControls:CallbackPanel>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlButtonBottomRow" runat="server" CssClass="button-row">
        <a href="ManageAutoResponders.aspx" class="button"><%= GUIStrings.Cancel %></a>
        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" OnClientClick="return ValidateRequiredFields();" Visible="true" />
        <asp:Button ID="btnPublish" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Publish %>" OnClick="btnPublish_Click" OnClientClick="return ValidateRequiredFields();" Visible="false" />
        <p>
            <asp:Literal ID="saveHelpText" Text="<%$ Resources:GUIStrings, SaveTheAutoresponderDetailsToAddAndScheduleAnEmail %>" runat="server" />
        </p>
    </asp:Panel>

</asp:Content>
