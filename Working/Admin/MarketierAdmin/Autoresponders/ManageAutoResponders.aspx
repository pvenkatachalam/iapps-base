﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManageAutoResponders.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Marketier.Web.ManageAutoResponders" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upAutoresponders_OnLoad() {
            $(".autoresponders-list").gridActions({
                objList: $(".autoresponders-emails"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upAutoresponders_Callback(sender, eventArgs);
                }
            });
        }

        function upAutoresponders_OnRenderComplete() {
            $(".autoresponders-list").gridActions("bindControls");
        }

        function upAutoresponders_OnCallbackComplete() {
            $(".autoresponders-list").gridActions("displayMessage");
        }

        function LoadItemsGrid() {
            $(".autoresponders-list").gridActions("selectNode", gridActionsJson.SelectedItems[0]);
        }

        function upAutoresponders_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("Title");

            switch (gridActionsJson.Action) {
                case "AddNewResponse":
                    doCallback = false;
                    var navigationUrl = jmarketierAdminURL + "/Autoresponders/ManageAutoResponderDetails.aspx";
                    window.location.assign(navigationUrl)
                    break;
                case "ViewEditResponse":
                    doCallback = false;
                    OpenEditAutoResponder();
                    break;
                case "DeleteResponse":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisAutoresponder %>");
                    break;
                case "Duplicate":
                    doCallback = true;
                    break;
                case "ViewLandingPages":
                    doCallback = false;
                    OpeniAppsMarketierPopup("ViewLandingPages", "IsResponse=true&CampaignId=" + selectedItemId, '');
                    break;
                case "MakeInactive":
                    doCallback = window.confirm("<%= JSMessages.MakeInactiveMessage %>");
                    break;
                case "MakeActive":
                    doCallback = window.confirm("<%= JSMessages.MakeActiveMessage %>");
                    break;

            }

            if (doCallback) {
                upAutoresponders.set_callbackParameter(JSON.stringify(gridActionsJson));
                upAutoresponders.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            var status = eventArgs.selectedItem.getValue("Status");
            sender.getItemByCommand("MakeActive").hide();
            sender.getItemByCommand("MakeInactive").show();
            sender.getItemByCommand("Edit").showButton();
            sender.getItemByCommand("Delete").showButton();

            if (status == 2) {
                sender.getItemByCommand("MakeActive").hide();               
            }
            else if (status == 5) {
                sender.getItemByCommand("MakeActive").show();
                sender.getItemByCommand("MakeInactive").hide();
            }
           
        }

        function RefreshEmailCampaigns() {
            $(".autoresponders-list").gridActions("callback");
        }

        function OpenEditAutoResponder() {
            var responseId = gridActionsJson.SelectedItems[0];
            var navigationUrl = jmarketierAdminURL + "/Autoresponders/ManageAutoResponderDetails.aspx?Id=" + responseId;
            window.location.assign(navigationUrl)
        }

    </script>
</asp:Content>

<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%=  GUIStrings.ManageAutoresponders %></h1>
    <iAppsControls:CallbackPanel ID="upAutoresponders" runat="server" OnClientCallbackComplete="upAutoresponders_OnCallbackComplete"
        OnClientRenderComplete="upAutoresponders_OnRenderComplete" OnClientLoad="upAutoresponders_OnLoad" CssClass="autoresponders-grid">
        <ContentTemplate>
            <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
            <asp:ListView ID="lvAutoresponders" runat="server" ItemPlaceholderID="phEmailList">
                <EmptyDataTemplate>
                    <div class="empty-list">
                        <%= GUIStrings.NoAutoresponders %>
                    </div>
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <div class="collection-table fat-grid autoresponders-emails">
                        <asp:PlaceHolder ID="phEmailList" runat="server" />
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                        <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id") %>'>
                            <div class="large-cell">
                                <h4 datafield="Title">
                                   <asp:Literal ID="ltAutoresponderTitle" runat="server" />
                                </h4>
                                <p>
                                    <asp:literal ID="ltAutoresponderDescription" runat="server" />
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <asp:Literal ID="ltStatus" runat="server" />
                                </div>
                                <div class="child-row">
                                    <asp:Literal ID="ltCreatedDate" runat="server" />
                                </div>
                                <div class="child-row">
                                    <asp:Label ID="lblDelivered" runat="server" CssClass="left-margin" />
                                    <asp:Label ID="lblOpens" runat="server" CssClass="left-margin" />
                                    <asp:Label ID="lblUniqueOpens" runat="server" CssClass="left-margin" />
                                    <asp:Label ID="lblClicks" runat="server" CssClass="left-margin" />
                                    <input type="hidden" id="hdnStatus" runat="server" datafield="Status" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
    <p class="statistic-update-info clear-fix"><asp:Literal ID="ltStatisticsUpdated" runat="server" /></p>
</asp:Content>
