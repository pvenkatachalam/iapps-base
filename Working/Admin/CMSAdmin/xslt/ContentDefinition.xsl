<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:asp="remove" xmlns:ComponentArt="remove" xmlns:RecurrenceControl="remove" xmlns:radE="remove" xmlns:CKEditor="remove">
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes"/>
  <xsl:param name="EditorToUse" />
  <xsl:template match="/">
    <table border="0" class="content-definition-table">
      <xsl:for-each select="//contentDefinitionNode">
        <tr>
          <!--<label for="label{@objectId}">-->
          <xsl:if test="@type!='hr' and @type != 'heading' and @type !='paragraph'">
            <xsl:choose>
              <xsl:when test="@required = 'yes'">
                <td valign="top">
                  <div class="form-label">
                    <xsl:value-of select="@label" />
                    <span class="req">*</span>
                  </div>
                </td>
              </xsl:when>
              <xsl:otherwise>
                <xsl:choose>
                  <xsl:when test="@type='recurrence'">
                    <td valign="top">
                      <div class="form-label">
                        <h3>
                          <xsl:value-of select="@label" />
                        </h3>
                      </div>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td valign="top">
                      <div class="form-label">
                        <xsl:value-of select="@label" />
                      </div>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <xsl:choose>
            <xsl:when test="@type='popup'">
              <script type="text/javascript">
                <![CDATA[
                                  function ClearPopupValue(textboxId, thumbImageId)
                                  {
                                    var objTextbox = document.getElementById(textboxId);
                                    if(objTextbox != null && objTextbox != undefined)
                                    {
                                      objTextbox.value = "";
                                    }
                                    if (typeof thumbImageId != "undefined")
                                    {
                                      var objThumb = document.getElementById(thumbImageId);
                                      if(objThumb != null && objThumb != undefined)
                                      {
                                        objThumb.src = "../App_Themes/General/images/no-thumbnail.png";
                                      }
                                    }
                                  }
                                  ]]>
              </script>
              <xsl:choose>
                <xsl:when test="@link='image'">
                  <td valign="top" class="form-value image-section">
                    <asp:HiddenField id="Hidden{@objectId}" ClientIDMode="Static" runat="server" Value="{@Id}"/>
                    <div class="left-section">
                      <xsl:choose>
                        <xsl:when test="@Thumb">
                          <asp:Image Id="Thumb{@objectId}" ClientIDMode="Static" runat="server" CssClass="thumb-image" ImageUrl="{@Thumb}" />
                        </xsl:when>
                        <xsl:otherwise>
                          <asp:Image Id="Thumb{@objectId}" ClientIDMode="Static" runat="server" CssClass="thumb-image" ImageUrl="~/App_Themes/General/images/no-thumbnail.png" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </div>
                    <div class="right-section">
                      <xsl:choose>
                        <xsl:when test="@Value">
                          <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{@Value}" CssClass="textBoxes disabled read-only" Width="550" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                        </xsl:when>
                        <xsl:otherwise>
                          <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{default}" CssClass="textBoxes disabled read-only" Width="550" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                        </xsl:otherwise>
                      </xsl:choose>
                      <div class="footer-section">
                        <asp:Button id="Button{@objectId}" runat="server" Text="..." CssClass="textBoxes" Width="25" OnClientClick="ShowImagePopup(TextBox{@objectId}, Hidden{@objectId}, Thumb{@objectId}); return false;"/>
                        <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear link value" Tooltip="Clear link value" onclick="ClearPopupValue('TextBox{@objectId}', 'Thumb{@objectId}');"></asp:Image>
                      </div>
                    </div>
                  </td>
                </xsl:when>
                <xsl:when test="@link='file'">
                  <td valign="top" class="form-value">
                    <asp:HiddenField id="Hidden{@objectId}" ClientIDMode="Static" runat="server" Value="{@Id}"/>
                    <xsl:choose>
                      <xsl:when test="@Value">
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{@Value}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{default}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:otherwise>
                    </xsl:choose>
                    <asp:Button id="Button{@objectId}" runat="server" Text="..." CssClass="textBoxes" Width="25" OnClientClick="ShowFilePopup(TextBox{@objectId}, Hidden{@objectId}); return false;"/>
                    <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear link value" Tooltip="Clear link value" onclick="ClearPopupValue('TextBox{@objectId}');"></asp:Image>
                  </td>
                </xsl:when>
                <xsl:when test="@link='page'">
                  <td valign="top" class="form-value">
                    <asp:HiddenField id="Hidden{@objectId}" ClientIDMode="Static" runat="server" Value="{@Id}"/>
                    <xsl:choose>
                      <xsl:when test="@Value">
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{@Value}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{default}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:otherwise>
                    </xsl:choose>
                    <asp:Button id="Button{@objectId}" runat="server" Text="..." CssClass="textBoxes" Width="25" OnClientClick="ShowPagePopup(TextBox{@objectId}, Hidden{@objectId}); return false;"/>
                    <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear link value" Tooltip="Clear link value" onclick="ClearPopupValue('TextBox{@objectId}');"></asp:Image>
                  </td>
                </xsl:when>
                <xsl:when test="@link='list'">
                  <td valign="top" class="form-value">
                    <asp:HiddenField id="Hidden{@objectId}" ClientIDMode="Static" runat="server" Value="{@Id}"/>
                    <xsl:choose>
                      <xsl:when test="@Value">
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{@Value}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{default}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:otherwise>
                    </xsl:choose>
                    <asp:Button id="Button{@objectId}" runat="server" Text="..." CssClass="textBoxes" Width="25" OnClientClick="ShowListPopup(TextBox{@objectId}, Hidden{@objectId}); return false;"/>
                    <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear link value" Tooltip="Clear link value" onclick="ClearPopupValue('TextBox{@objectId}');"></asp:Image>
                  </td>
                </xsl:when>
                <xsl:when test="@link='content'">
                  <td valign="top" class="form-value">
                    <asp:HiddenField id="Hidden{@objectId}" ClientIDMode="Static" runat="server" Value="{@Id}"/>
                    <xsl:choose>
                      <xsl:when test="@Value">
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{@Value}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{default}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThis(this);" Tooltip="{@Tooltip}" />
                      </xsl:otherwise>
                    </xsl:choose>
                    <asp:Button id="Button{@objectId}" runat="server" Text="..." CssClass="textBoxes" Width="25" OnClientClick="ShowContentPopup(TextBox{@objectId}, Hidden{@objectId}); return false;"/>
                  </td>
                </xsl:when>
                <xsl:when test="@link='product'">
                  <td valign="top" class="form-value">
                    <asp:HiddenField id="Hidden{@objectId}" ClientIDMode="Static" runat="server" Value="{@Id}"  />
                    <xsl:choose>
                      <xsl:when test="@Value">
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{@Value}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThisAndCheckCommerceLicense(this);" Tooltip="{@Tooltip}" />
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:TextBox id="TextBox{@objectId}" ClientIDMode="Static" runat="server" Text="{default}" CssClass="textBoxes" Width="595" onfocus="CanFocusOnThisAndCheckCommerceLicense(this);" Tooltip="{@Tooltip}" />
                      </xsl:otherwise>
                    </xsl:choose>
                    <asp:Button id="Button{@objectId}" runat="server" Text="..." CssClass="textBoxes" Width="25" OnClientClick="ShowProductPopup(TextBox{@objectId}, Hidden{@objectId}); return false;" />
                    <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear link value" Tooltip="Clear link value" onclick="ClearPopupValue('TextBox{@objectId}');" />
                  </td>
                </xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='editor'">
              <script type="text/javascript">
                <![CDATA[
                                function SetClassAndIdCKEditor() {
                                    var ckeditor_body = ckEditor.document.getBody();
                                    var ckEditor_bodyClass = ckeditor_body.getAttribute('class');
                                    var ckEditor_bodyId = ckeditor_body.getAttribute('id');
                                    var container_viewcssclass = '';
                                    var formObj = document.forms[0];
                                    var formAction = "";
                                    if (formObj) {
                                        formAction = formObj.action;
                                        var formActionParts = formAction.split("&");
                                        var actionPart = "";
                                        for (k = 0; k < formActionParts.length; k++) {
                                            actionPart = formActionParts[k].split("=");
                                            if (actionPart.length > 0) {
                                                if (actionPart[0].toLowerCase().indexOf("viewcssclass") != -1) {
                                                    container_viewcssclass = actionPart[1];
                                                    ckeditor_body.setAttribute('class', ckEditor_bodyClass != '' ? ckEditor_bodyClass + ' ' + container_viewcssclass : container_viewcssclass);
                                                    ckeditor_body.setAttribute('id', container_viewcssclass.replace('-', '_'));
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                function OnClientInit(editor) {
                                }
				                function OnClientLoad(editor) {
                            setTimeout(function() {
                              oureditor.repaint();
                              setTimeout("$('.content-definition-editor').scrollTop(0);", 500);
                              }, 1000);
		                        CopyStylesToEditor(editor);
                                setTimeout(function() {
                                    if (window.EditorOnLoad) {
                                        EditorOnLoad(editor);
                                    }
                                }, 250);

                                var sharepointText = editor.getToolByName("SharePoint").get_element();
                                if (sharepointText != null) {
                                    var parentCustomTool = sharepointText.parentNode;
                                    sharepointText.style.display = 'none';
                                    if (parentCustomTool != null) {
                                        parentCustomTool.style.width = 'auto';
                                        parentCustomTool.innerHTML = '<span class="sharepoint-text">' + sharepointText.getAttribute('title') + '</span>';
                                    }
                                }
		                        }
				                function CopyStylesToEditor(editor) {
					                var theDocBody = editor.get_document().body;
					                var containerCSS = "";
					                var formObj = document.forms[0];
					                var formAction = "";
					                if(formObj) {
						                formAction = formObj.action;
						                var formActionParts = formAction.split("&");
						                var actionPart = "";
						                for(k=0;k<formActionParts.length;k++) {
							                actionPart = formActionParts[k].split("=");
							                if(actionPart.length > 0) {
								                if(actionPart[0].toLowerCase().indexOf("viewcssclass") != -1) {
									                containerCSS = actionPart[1];
									                theDocBody.setAttribute("id",containerCSS.replace('-', '_').replace(' ', '_'));
                                                    theDocBody.setAttribute("class",containerCSS);
									                //RemoveOurSpan(theDocBody,containerCSS);
								                }
							                }
						                }
					                }
			                    }
                  
                                /*function RemoveOurSpanClass(editorBody,ourContainerCSS) {
                                    var arrSpan = editorBody.getElementsByTagName("span");
                                    for (j=0;j<arrSpan.length;j++) {
                                        if (arrSpan[j].className == ourContainerCSS) {
                                            arrSpan[j].className="";
                                        }
                                    }
                                }*/		
                    
				                function RemoveOurSpan(editorBody,ourContainerCSS) {
					                var arrSpan = editorBody.getElementsByTagName("span");
					                var spanContents = "";
					                for (m=0;m<arrSpan.length;m++) {
						                if (arrSpan[m].className == ourContainerCSS && ourContainerCSS != "") {
							                spanContents = arrSpan[m].innerHTML;
							                editorBody.removeChild(arrSpan[m]);
							                editorBody.innerHTML = spanContents;
						                }
					                }
				                }
                        
                        function get_allRadEditors() 
                        { 
                            var allRadEditors = []; 
                            var allRadControls = $telerik.radControls; 
     
                            for (var i = 0; i < allRadControls.length; i++) 
                            { 
                                var element = allRadControls[i]; 
         
                                if (Telerik.Web.UI.RadEditor && element instanceof Telerik.Web.UI.RadEditor) 
                                { 
                                    Array.add(allRadEditors, element); 
                                } 
                            } 
                            return allRadEditors; 
                        } 
                        
                        $(document).ready(function() {
                          if (EditorToUse.toLowerCase() == 'radeditor') {
                            setTimeout(function() {
                              for (var i = 0; i < get_allRadEditors().length; i++) { 
                                var curEditor = get_allRadEditors()[i]; 
                                curEditor.set_mode(1);
                              } 
                            }, 1000);
                          }
                          else if (EditorToUse.toLowerCase() == 'ckeditor') {
                            setTimeout("$('.cke_hc').removeClass('cke_hc');", 500);
                          }
                        });
				                ]]>
              </script>
              <xsl:choose>
                <xsl:when test="@select">
                  <xsl:choose>
                    <xsl:when test="$EditorToUse != 'ckeditor'">
                      <td valign="top" class="editorCell form-value">
                        <xsl:choose>
                          <xsl:when test="@Tooltip != ''">
                            <radE:RadToolTip ID="{@objectId}_Tooltip" runat="server" TargetControlID="{@objectId}" Text="{@Tooltip}"></radE:RadToolTip>
                          </xsl:when>
                        </xsl:choose>
                        <radE:RadEditor ID="{@objectId}" runat="server" OnClientLoad="OnClientLoad" OnClientInit="OnClientInit" OnClientModeChange="OnClientModeChange"
                            DictionaryPath="~/App_Data/RadSpell" EnableResize="false" ContentAreaCssFile="~/css/EditorContentArea.css"
                            ContentFilters="ConvertCharactersToEntities,FixUlBoldItalic,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent"
                            StripFormattingOptions="MSWordRemoveAll" NewLineMode="P" AllowScripts="true" Width="680" Height="450"
                            DialogsCssFile="~/iAPPS_Editor/radeditor_dialog.css" EditModes="Design,Html">
                          <Content>
                            <xsl:value-of select="@select"/>
                          </Content>
                          <CssFiles>
                            <radE:EditorCssFile Value="~/css/authorStyles.css" />
                          </CssFiles>
                        </radE:RadEditor>
                      </td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td valign="top" class="editorCell form-value">
                        <CKEditor:CKEditorControl ID="{@objectId}" runat="server" AutoGrowMinHeight="200" EnterMode="P" BasePath="~/iAPPS_Editor"
                            Width="680" Height="450" Entities="true" HtmlEncodeOutput="true" IgnoreEmptyParagraph="true"
                            BrowserContextMenuOnCtrl="true" EntitiesGreek="true" ScaytAutoStartup="true"
                            ShiftEnterMode="BR" TemplatesReplaceContent="false"
                            ForceSimpleAmpersand="true" ToolbarCanCollapse="false">
                          <xsl:value-of select="@select"/>
                        </CKEditor:CKEditorControl>
                      </td>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:choose>
                    <xsl:when test="$EditorToUse != 'ckeditor'">
                      <td valign="top" class="editorCell form-value">
                        <xsl:choose>
                          <xsl:when test="@Tooltip != ''">
                            <radE:RadToolTip ID="{@objectId}_Tooltip" runat="server" TargetControlID="{@objectId}" Text="{@Tooltip}"></radE:RadToolTip>
                          </xsl:when>
                        </xsl:choose>
                        <radE:RadEditor ID="{@objectId}" runat="server" OnClientLoad="OnClientLoad" OnClientModeChange="OnClientModeChange"
                            OnClientInit="OnClientInit" StripFormattingOptions="MSWordRemoveAll" DictionaryPath="~/App_Data/RadSpell" EnableResize="false"
                            ContentAreaCssFile="~/css/EditorContentArea.css"
                            ContentFilters="ConvertCharactersToEntities,FixUlBoldItalic,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent"
                            NewLineMode="P" AllowScripts="true" Width="680" Height="450" DialogsCssFile="~/iAPPS_Editor/radeditor_dialog.css" EditModes="Design,Html">
                          <Content>
                          </Content>
                          <CssFiles>
                            <radE:EditorCssFile Value="~/css/authorStyles.css" />
                          </CssFiles>
                        </radE:RadEditor>
                      </td>
                    </xsl:when>
                    <xsl:otherwise>
                      <td valign="top" class="editorCell form-value">
                        <CKEditor:CKEditorControl ID="{@objectId}" runat="server" AutoGrowMinHeight="200" EnterMode="P" BasePath="~/iAPPS_Editor"
                            Width="680" Height="450" Entities="true" HtmlEncodeOutput="true" IgnoreEmptyParagraph="true"
                            BrowserContextMenuOnCtrl="true" EntitiesGreek="true" ScaytAutoStartup="true"
                            ShiftEnterMode="BR" TemplatesReplaceContent="false"
                            ForceSimpleAmpersand="true" ToolbarCanCollapse="false">
                        </CKEditor:CKEditorControl>
                      </td>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='hr'">
              <td colspan ="2" valign="top" class="form-value">
                <hr />
              </td>
            </xsl:when>
            <xsl:when test="@type='heading'">
              <td colspan ="2" valign="top" class="form-value">
                <h2>
                  <xsl:value-of select="@label" />
                </h2>
              </td>
            </xsl:when>
            <xsl:when test="@type='paragraph'">
              <td colspan ="2" valign="top" class="form-value">
                <p>
                  <xsl:value-of select="@label" />
                </p>
              </td>
            </xsl:when>
            <xsl:when test="@type='label'">
              <xsl:choose>
                <xsl:when test="@select">
                  <td valign="top" class="form-value">
                    <asp:Label id="{@objectId}" runat="server"  Text="{@select}" CssClass="textBoxes" />
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td valign="top" class="form-value">
                    <asp:Label id="{@objectId}" runat="server"  Text="{default}" CssClass="textBoxes" />
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='textbox'">
              <xsl:choose>
                <xsl:when test="@select">
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server"  Text="{@select}" CssClass="textBoxes" Width="640" Tooltip="{@Tooltip}" />
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server"  Text="{default}" CssClass="textBoxes" Width="640" Tooltip="{@Tooltip}" />
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='textArea'">
              <xsl:choose>
                <xsl:when test="@select">
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server" TextMode="MultiLine" Rows="6" Text="{@select}" CssClass="textBoxes" Width="640" Tooltip="{@Tooltip}" />
                  </td >
                </xsl:when>
                <xsl:otherwise>
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server" TextMode="MultiLine" Rows="6" Text="{default}" CssClass="textBoxes" Width="640" Tooltip="{@Tooltip}" />
                  </td >
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='hidden'">
              <xsl:choose>
                <xsl:when test="@select">
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server"  Text="{@select}" CssClass="textBoxes disabled read-only" Width="640" />
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server"  Text="{default}" CssClass="textBoxes disabled read-only" Width="640" />
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='readOnly'">
              <xsl:choose>
                <xsl:when test="@select">
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server"  Text="{@select}" CssClass="textBoxes disabled" Width="640" ReadOnly="true" Tooltip="{@Tooltip}" />
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td valign="top" class="form-value">
                    <asp:TextBox id="{@objectId}" runat="server"  Text="{default}" CssClass="textBoxes disabled" Width="640" ReadOnly="true" Tooltip="{@Tooltip}" />
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='checkbox'">
              <xsl:choose>
                <xsl:when test="@select='True'">
                  <td valign="top" class="form-value">
                    <asp:CheckBox id="{@objectId}" runat="server" Checked="true" Tooltip="{@Tooltip}" />
                  </td>
                </xsl:when>
                <xsl:otherwise>
                  <td valign="top">
                    <asp:CheckBox id="{@objectId}" runat="server" Tooltip="{@Tooltip}"/>
                  </td>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='radio'">
              <td valign="top" class="form-value">
                <asp:RadioButtonList id="{@objectId}" runat="server" Width="640" cssClass="radioContainer">
                  <xsl:variable name="selectedValue" select="@select" />
                  <xsl:for-each select="choice">
                    <xsl:choose>
                      <xsl:when test="@selected and @selected = 'true'">
                        <asp:ListItem Selected="True" Value="{text()}">
                          <xsl:value-of select="text()" />
                        </asp:ListItem>
                      </xsl:when>
                      <!-- On first time entry is the only time we want the default utilized. -->
                      <xsl:when test="$selectedValue=not(string(.)) and (name()='default' or @default='true')">
                        <asp:ListItem Selected="True" Value="{text()}">
                          <xsl:value-of select="text()" />
                        </asp:ListItem>
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:ListItem Value="{text()}">
                          <xsl:value-of select="text()" />
                        </asp:ListItem>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </asp:RadioButtonList>
              </td>
            </xsl:when>
            <xsl:when test="@type='combo'">
              <td valign="top" class="form-value">
                <xsl:variable name="selectedValue" select="@select" />
                <asp:DropDownList id="{@objectId}" runat="server" Tooltip="{@Tooltip}" Width="640">
                  <xsl:for-each select="*">
                    <xsl:choose>
                      <xsl:when test="@selected and @selected = 'true'">
                        <asp:ListItem Selected="True" Value="{text()}">
                          <xsl:value-of select="text()" />
                        </asp:ListItem>
                      </xsl:when>
                      <!-- On first time entry is the only time we want the default utilized. -->
                      <xsl:when test="$selectedValue=not(string(.)) and (name()='default' or @default='true')">
                        <asp:ListItem Selected="True" Value="{text()}">
                          <xsl:value-of select="text()" />
                        </asp:ListItem>
                      </xsl:when>
                      <xsl:otherwise>
                        <asp:ListItem Value="{text()}">
                          <xsl:value-of select="text()" />
                        </asp:ListItem>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each>
                </asp:DropDownList>
              </td>
            </xsl:when>
            <xsl:when test="@type='multicheckbox'">
              <td valign="top" class="form-value">
                <asp:CheckBoxList id="{@objectId}" runat="server" class="mcb" RepeatColumns="2">
                  <xsl:variable name="selectedValue" select="@select" />
                  <xsl:for-each select="choice">
                    <xsl:call-template name="divide">
                      <xsl:with-param name="to-be-divided" select="$selectedValue" />
                      <xsl:with-param name="delimiter" select="','" />
                      <xsl:with-param name="element-text" select ="text()" />
                      <xsl:with-param name="selected" select ="@selected" />
                    </xsl:call-template>
                  </xsl:for-each>
                </asp:CheckBoxList>
              </td>
            </xsl:when>
            <xsl:when test="@type='listbox'">
              <td valign="top" class="form-value">
                <asp:ListBox id="{@objectId}" runat="server" SelectionMode="Multiple" Width="640">
                  <xsl:variable name="selectedValue" select="@select" />
                  <xsl:for-each select="choice">
                    <xsl:call-template name="divide">
                      <xsl:with-param name="to-be-divided" select="$selectedValue" />
                      <xsl:with-param name="delimiter" select="','" />
                      <xsl:with-param name="element-text" select ="text()" />
                      <xsl:with-param name="selected" select ="@selected" />
                    </xsl:call-template>
                  </xsl:for-each>
                </asp:ListBox>
              </td>
            </xsl:when>
            <xsl:when test="@type='datepicker'">
              <td valign="top" class="form-value">
                <table cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td onmouseup="Button_OnMouseUp(Calendar{@objectId})">
                      <xsl:variable name="formatValue">
                        <xsl:choose>
                          <xsl:when test="@format != ''">
                            <xsl:value-of select="@format"/>
                          </xsl:when>
                          <xsl:otherwise>MM-dd-yyyy</xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>
                      <xsl:choose>
                        <xsl:when test="@select">
                          <ComponentArt:Calendar id="Picker{@objectId}" runat="server" PickerFormat="Custom" PickerCustomFormat="{$formatValue}"
                                ControlType="Picker" SelectedDate="{@select}" PickerCssClass="picker textBoxes">
                            <ClientEvents>
                              <Load EventHandler="onCalendarLoad" />
                            </ClientEvents>
                          </ComponentArt:Calendar>
                        </xsl:when>
                        <xsl:otherwise>
                          <ComponentArt:Calendar id="Picker{@objectId}" runat="server" PickerFormat="Custom" PickerCustomFormat="{$formatValue}"
                              ControlType="Picker" SelectedDate="2007-04-10" PickerCssClass="picker textBoxes">
                            <ClientEvents>
                              <Load EventHandler="onCalendarLoad" />
                            </ClientEvents>
                          </ComponentArt:Calendar>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td style="font-size:10px;">&#160;</td>
                    <td>
                      <img id="Button{@objectId}" alt=""
                    onclick="onButtonClick(Calendar{@objectId},Picker{@objectId})"
                    class="calendar_button" src="../App_Themes/General/images/calendar-button.png" title="{@Tooltip}" />
                    </td>
                    <td style="font-size:10px;">&#160;</td>
                    <td>
                      <xsl:choose>
                        <xsl:when test="@select">
                          <asp:DropDownList selectedTime="{@select}"  id="Time{@objectId}" runat="server" style="width: 90px">
                            <asp:ListItem Value="12:00" Text="12:00"/>
                            <asp:ListItem Value="12:15" Text="12:15"/>
                            <asp:ListItem Value="12:30" Text="12:30"/>
                            <asp:ListItem Value="12:45" Text="12:45"/>
                            <asp:ListItem Value="01:00" Text="01:00"/>
                            <asp:ListItem Value="01:15" Text="01:15"/>
                            <asp:ListItem Value="01:30" Text="01:30"/>
                            <asp:ListItem Value="01:45" Text="01:45"/>
                            <asp:ListItem Value="02:00" Text="02:00"/>
                            <asp:ListItem Value="02:15" Text="02:15"/>
                            <asp:ListItem Value="02:30" Text="02:30"/>
                            <asp:ListItem Value="02:45" Text="02:45"/>
                            <asp:ListItem Value="03:00" Text="03:00"/>
                            <asp:ListItem Value="03:15" Text="03:15"/>
                            <asp:ListItem Value="03:30" Text="03:30"/>
                            <asp:ListItem Value="03:45" Text="03:45"/>
                            <asp:ListItem Value="04:00" Text="04:00"/>
                            <asp:ListItem Value="04:15" Text="04:15"/>
                            <asp:ListItem Value="04:30" Text="04:30"/>
                            <asp:ListItem Value="04:45" Text="04:45"/>
                            <asp:ListItem Value="05:00" Text="05:00"/>
                            <asp:ListItem Value="05:15" Text="05:15"/>
                            <asp:ListItem Value="05:30" Text="05:30"/>
                            <asp:ListItem Value="05:45" Text="05:45"/>
                            <asp:ListItem Value="06:00" Text="06:00"/>
                            <asp:ListItem Value="06:15" Text="06:15"/>
                            <asp:ListItem Value="06:30" Text="06:30"/>
                            <asp:ListItem Value="06:45" Text="06:45"/>
                            <asp:ListItem Value="07:00" Text="07:00"/>
                            <asp:ListItem Value="07:15" Text="07:15"/>
                            <asp:ListItem Value="07:30" Text="07:30"/>
                            <asp:ListItem Value="07:45" Text="07:45"/>
                            <asp:ListItem Value="08:00" Text="08:00"/>
                            <asp:ListItem Value="08:15" Text="08:15"/>
                            <asp:ListItem Value="08:30" Text="08:30"/>
                            <asp:ListItem Value="08:45" Text="08:45"/>
                            <asp:ListItem Value="09:00" Text="09:00"/>
                            <asp:ListItem Value="09:15" Text="09:15"/>
                            <asp:ListItem Value="09:30" Text="09:30"/>
                            <asp:ListItem Value="09:45" Text="09:45"/>
                            <asp:ListItem Value="10:00" Text="10:00"/>
                            <asp:ListItem Value="10:15" Text="10:15"/>
                            <asp:ListItem Value="10:30" Text="10:30"/>
                            <asp:ListItem Value="10:45" Text="10:45"/>
                            <asp:ListItem Value="11:00" Text="11:00"/>
                            <asp:ListItem Value="11:15" Text="11:15"/>
                            <asp:ListItem Value="11:30" Text="11:30"/>
                            <asp:ListItem Value="11:45" Text="11:45"/>
                          </asp:DropDownList>
                        </xsl:when>
                        <xsl:otherwise>
                          <asp:DropDownList selectedTime="2007-04-10 10:15" id="Time{@objectId}" runat="server" style="width: 90px">
                            <asp:ListItem Value="12:00" Text="12:00"/>
                            <asp:ListItem Value="12:15" Text="12:15"/>
                            <asp:ListItem Value="12:30" Text="12:30"/>
                            <asp:ListItem Value="12:45" Text="12:45"/>
                            <asp:ListItem Value="01:00" Text="01:00"/>
                            <asp:ListItem Value="01:15" Text="01:15"/>
                            <asp:ListItem Value="01:30" Text="01:30"/>
                            <asp:ListItem Value="01:45" Text="01:45"/>
                            <asp:ListItem Value="02:00" Text="02:00"/>
                            <asp:ListItem Value="02:15" Text="02:15"/>
                            <asp:ListItem Value="02:30" Text="02:30"/>
                            <asp:ListItem Value="02:45" Text="02:45"/>
                            <asp:ListItem Value="03:00" Text="03:00"/>
                            <asp:ListItem Value="03:15" Text="03:15"/>
                            <asp:ListItem Value="03:30" Text="03:30"/>
                            <asp:ListItem Value="03:45" Text="03:45"/>
                            <asp:ListItem Value="04:00" Text="04:00"/>
                            <asp:ListItem Value="04:15" Text="04:15"/>
                            <asp:ListItem Value="04:30" Text="04:30"/>
                            <asp:ListItem Value="04:45" Text="04:45"/>
                            <asp:ListItem Value="05:00" Text="05:00"/>
                            <asp:ListItem Value="05:15" Text="05:15"/>
                            <asp:ListItem Value="05:30" Text="05:30"/>
                            <asp:ListItem Value="05:45" Text="05:45"/>
                            <asp:ListItem Value="06:00" Text="06:00"/>
                            <asp:ListItem Value="06:15" Text="06:15"/>
                            <asp:ListItem Value="06:30" Text="06:30"/>
                            <asp:ListItem Value="06:45" Text="06:45"/>
                            <asp:ListItem Value="07:00" Text="07:00"/>
                            <asp:ListItem Value="07:15" Text="07:15"/>
                            <asp:ListItem Value="07:30" Text="07:30"/>
                            <asp:ListItem Value="07:45" Text="07:45"/>
                            <asp:ListItem Value="08:00" Text="08:00"/>
                            <asp:ListItem Value="08:15" Text="08:15"/>
                            <asp:ListItem Value="08:30" Text="08:30"/>
                            <asp:ListItem Value="08:45" Text="08:45"/>
                            <asp:ListItem Value="09:00" Text="09:00"/>
                            <asp:ListItem Value="09:15" Text="09:15"/>
                            <asp:ListItem Value="09:30" Text="09:30"/>
                            <asp:ListItem Value="09:45" Text="09:45"/>
                            <asp:ListItem Value="10:00" Text="10:00"/>
                            <asp:ListItem Value="10:15" Text="10:15"/>
                            <asp:ListItem Value="10:30" Text="10:30"/>
                            <asp:ListItem Value="10:45" Text="10:45"/>
                            <asp:ListItem Value="11:00" Text="11:00"/>
                            <asp:ListItem Value="11:15" Text="11:15"/>
                            <asp:ListItem Value="11:30" Text="11:30"/>
                            <asp:ListItem Value="11:45" Text="11:45"/>
                          </asp:DropDownList>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td style="font-size:10px;">&#160;</td>
                    <td>
                      <asp:DropDownList id="AMPM{@objectId}" runat="server" style="width: 70px">
                        <asp:ListItem Value="AM" Text="AM">
                        </asp:ListItem>
                        <asp:ListItem Value="PM" Text="PM">
                        </asp:ListItem>
                      </asp:DropDownList>
                    </td>
                    <td style="font-size:10px;">&#160;</td>
                    <td>
                      <xsl:if test="@required = 'no' or @required = 'No' or @required = 'NO'">
                        <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear Date and Time" Tooltip="Clear Date and Time" onclick="clearCalendarDate(Calendar{@objectId}, Picker{@objectId})"></asp:Image>
                      </xsl:if>
                    </td>
                  </tr>
                </table>

                <ComponentArt:Calendar runat="server" id="Calendar{@objectId}" AllowMultipleSelection="false" AllowWeekSelection="false"
                  AllowMonthSelection="false" ControlType="Calendar" PopUp="Custom" PopUpExpandControlId="Button{@objectId}"
                  CalendarTitleCssClass="title" ClientSideOnSelectionChanged="onCalendarChange"
                  DayHeaderCssClass="dayheader" DayCssClass="day" DayHoverCssClass="dayhover" OtherMonthDayCssClass="othermonthday"
                  SelectedDayCssClass="selectedday" CalendarCssClass="calendar" NextPrevCssClass="nextprev" MonthCssClass="month"
                  SwapSlide="Linear" SwapDuration="300" DayNameFormat="FirstTwoLetters" ImagesBaseUrl="images" PrevImageUrl="../../../App_Themes/General/images/cal_prevMonth.gif"
                  NextImageUrl="../../../App_Themes/General/images/cal_nextMonth.gif"/>
              </td>
            </xsl:when>
            <xsl:when test="@type='dateonlypicker'">
              <td valign="top" class="form-value">
                <table cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td onmouseup="Button_OnMouseUp(Calendar{@objectId})">
                      <xsl:variable name="formatValue">
                        <xsl:choose>
                          <xsl:when test="@format != ''">
                            <xsl:value-of select="@format"/>
                          </xsl:when>
                          <xsl:otherwise>MM-dd-yyyy</xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>
                      <xsl:choose>
                        <xsl:when test="@select">
                          <ComponentArt:Calendar id="Picker{@objectId}" runat="server" PickerFormat="Custom" PickerCustomFormat="{$formatValue}"
                                ControlType="Picker" SelectedDate="{@select}" PickerCssClass="picker">
                            <ClientEvents>
                              <Load EventHandler="onCalendarLoad" />
                            </ClientEvents>
                          </ComponentArt:Calendar>
                        </xsl:when>
                        <xsl:otherwise>
                          <ComponentArt:Calendar id="Picker{@objectId}" runat="server" PickerFormat="Custom" PickerCustomFormat="{$formatValue}"
                              ControlType="Picker" SelectedDate ="2007-04-10" PickerCssClass="picker">
                            <ClientEvents>
                              <Load EventHandler="onCalendarLoad" />
                            </ClientEvents>
                          </ComponentArt:Calendar>
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                    <td style="font-size:10px;"></td>
                    <td>
                      <img id="Button{@objectId}" alt=""
                    onclick="onButtonClick(Calendar{@objectId},Picker{@objectId})"
                    class="calendar_button" src="../App_Themes/General/images/calendar-button.png" title="{@Tooltip}" />
                    </td>
                    <td>
                      <xsl:if test="@required = 'no'">
                        <asp:Image id="Image{@objectId}" runat="server" style="cursor: pointer;" ImageUrl="~/App_Themes/General/images/cm-icon-delete.png" AlternateText="Clear Date" Tooltip="Clear Date" onclick="clearCalendarDate(Calendar{@objectId}, Picker{@objectId})"></asp:Image>
                      </xsl:if>
                    </td>
                  </tr>
                </table>
                <ComponentArt:Calendar runat="server" id="Calendar{@objectId}" AllowMultipleSelection="false" AllowWeekSelection="false"
                  AllowMonthSelection="false" ControlType="Calendar" PopUp="Custom" PopUpExpandControlId="Button{@objectId}"
                  CalendarTitleCssClass="title" ClientSideOnSelectionChanged="onCalendarChange"
                  DayHeaderCssClass="dayheader" DayCssClass="day" DayHoverCssClass="dayhover" OtherMonthDayCssClass="othermonthday"
                  SelectedDayCssClass="selectedday" CalendarCssClass="calendar" NextPrevCssClass="nextprev" MonthCssClass="month"
                  SwapSlide="Linear" SwapDuration="300" DayNameFormat="FirstTwoLetters" ImagesBaseUrl="images" PrevImageUrl="../../../App_Themes/General/images/cal_prevMonth.gif"
                  NextImageUrl="../../../App_Themes/General/images/cal_nextMonth.gif"/>
              </td>
            </xsl:when>
            <xsl:when test="@type='recurrence'">
              <td valign="top" class="form-value">
                <div class="usercontrol">
                  <RecurrenceControl:Repeat id="{@objectId}" runat="server">
                    <xsl:for-each select="Properties">
                      <xsl:choose>
                        <xsl:when test="@select">
                          <Properties name="{@Name}" value="{@select}"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <Properties name="{@Name}" value="{@Value}"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:for-each>
                  </RecurrenceControl:Repeat>
                </div>
              </td>
            </xsl:when>
          </xsl:choose>
          <xsl:if test="@required = 'yes'">
            <xsl:choose>
              <xsl:when test="@type='datepicker' or @type='dateonlypicker'">
                <!--<asp:RequiredFieldValidator ErrorMessage=" Required Field" runat="server" ControlToValidate="Picker{@objectId}" />-->
              </xsl:when>
              <xsl:when test="@type='popup'">
                <asp:RequiredFieldValidator ErrorMessage="Please enter values for all required fields." runat="server" ControlToValidate="TextBox{@objectId}"  SetFocusOnError="True" Display="None" ClientIDMode="AutoID"/>
              </xsl:when>
              <xsl:otherwise>
                <asp:RequiredFieldValidator ErrorMessage="Please enter values for all required fields." runat="server" ControlToValidate="{@objectId}" SetFocusOnError="True" Display="None" ClientIDMode="AutoID" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <!--</label>-->
        </tr>
      </xsl:for-each>
      <script type="text/javascript">
        <![CDATA[
				  function SetContentTitle(titleString){
				      if(parent.self['SetContentTitle']){
					      parent.SetContentTitle(titleString);
				      }
				  }
          function CanFocusOnThis(thisObj)                 {
              if(canFocusInLinkTextBox.toLowerCase()=='yes') {
                  //
              }
              else {
                  thisObj.blur();
              }
          }
          function CanFocusOnThisAndCheckCommerceLicense(thisObj) {
              if(parent.hasCommerceLicense && parent.hasCommercePermission ) {
                  CanFocusOnThis(thisObj);
              }
              else {
                  thisObj.blur();
              }
          }
				]]>
      </script>
    </table>
  </xsl:template>

  <!-- This template splits comma separated lists. Primarily used for multi-selection tools (CheckBoxList and ComboBox) -->
  <xsl:template name="divide">
    <xsl:param name="to-be-divided"/>
    <xsl:param name="delimiter"/>
    <xsl:param name="element-text" />
    <xsl:param name="selected" />
    <xsl:choose>
      <xsl:when test="$to-be-divided != ''">
        <xsl:variable name="element" select="substring-before($to-be-divided, $delimiter)"/>
        <xsl:choose>
          <xsl:when test="$element != ''">
            <xsl:choose>
              <xsl:when test="@selected and @selected='true'">
                <asp:ListItem Selected="True" Value="{$element-text}">
                  <xsl:value-of select="$element-text" />
                </asp:ListItem>
              </xsl:when>
              <xsl:when test="$element=$element-text">
                <asp:ListItem Selected="True" Value="{$element-text}">
                  <xsl:value-of select="$element-text" />
                </asp:ListItem>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="divide">
                  <xsl:with-param name="to-be-divided" select="substring-after($to-be-divided, $delimiter)" />
                  <xsl:with-param name="delimiter" select="$delimiter" />
                  <xsl:with-param name="element-text" select="$element-text" />
                  <xsl:with-param name="selected" select="$selected" />
                </xsl:call-template>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:choose>
              <xsl:when test="$to-be-divided=$element-text">
                <asp:ListItem Selected="True" Value="{$element-text}">
                  <xsl:value-of select="$element-text" />
                </asp:ListItem>
              </xsl:when>
              <xsl:otherwise>
                <asp:ListItem Value="{$element-text}">
                  <xsl:value-of select="$element-text" />
                </asp:ListItem>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <asp:ListItem Value="{$element-text}">
          <xsl:value-of select="$element-text" />
        </asp:ListItem>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
