<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    ClientIDMode="Static" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageFeedDetails" StylesheetTheme="General"
    CodeBehind="ManageFeedDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<asp:Content ID="header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function UpdateFeedUrl() {
            $("#divVirtualPath").html("/RssFeeds/" + $("#txtFileName").val());
        }

        function CreateChannel() {
            OpeniAppsAdminPopup("ManageChannelDetails", "ChannelType=1", "CloseChannelDetails"); return false;

            return false;
        }

        function CloseChannelDetails() {
            upChannels.set_callbackParameter(popupActionsJson.SelectedItems.first());
            upChannels.callback();
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, Save %>" ValidationGroup="valSaveFeed" CausesValidation="true"
            OnClick="btnSave_Click" />
        <asp:Button ID="btnSaveView" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, SaveAndAddChannel %>"
            ToolTip="<%$ Resources:GUIStrings, Save %>" ValidationGroup="valSaveFeed" CausesValidation="true"
            OnClick="btnSaveEdit_Click" Visible="false" />
        <a href="ManageRSS.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
        ValidationGroup="valSaveFeed" />
    <div class="dark-section-header clear-fix">
        <h2><%= GUIStrings.ListProperties %></h2>
    </div>
    <div class="columns">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span
                    class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="360" />
                <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>" Display="None"
                    SetFocusOnError="true" ValidationGroup="valSaveFeed" />
                <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheTitle %>"
                    Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" ValidationGroup="valSaveFeed" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" TextMode="MultiLine"
                    Width="360" />
                <asp:RegularExpressionValidator ID="reqtxtDescription" runat="server" ControlToValidate="txtDescription"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheDescription %>"
                    Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" ValidationGroup="valSaveFeed" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, FileNameColon %>" /><span
                    class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtFileName" runat="server" CssClass="textBoxes" Width="360" />
                <asp:RequiredFieldValidator ID="reqtxtFileName" ControlToValidate="txtFileName" runat="server"
                    ErrorMessage="<%$ Resources:JSMessages, FileNameIsRequired %>" Display="None"
                    SetFocusOnError="true" ValidationGroup="valSaveFeed" />
                <asp:RegularExpressionValidator ID="regtxtFileName" ControlToValidate="txtFileName"
                    runat="server" ErrorMessage="<%$ Resources:JSMessages, FileNameIsNotValidEnterXml %>"
                    Display="None" ValidationExpression="^[0-9A-Za-z_ ]+(.[xX][mM][lL])$" SetFocusOnError="true"
                    ValidationGroup="valSaveFeed" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, URLVirtualPathColon %>" />
            </label>
            <div class="form-value text-value" id="divVirtualPath">
                <asp:Literal ID="ltVirtualPath" runat="server" Text="/RssFeeds/" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, PublishEveryColon %>" />
            </label>
            <div class="form-value">
                <asp:DropDownList ID="ddlPublish" CssClass="textBoxes" Width="370" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" runat="server" Value="1" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, _15Minutes %>" runat="server" Value="15" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, _30Minutes %>" runat="server" Value="30" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, _1Hour %>" runat="server" Value="60" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, _6Hours %>" runat="server" Value="360" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, _12Hours %>" runat="server" Value="720" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, _Midnight %>" runat="server" Value="0" />
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqddlPublish" ControlToValidate="ddlPublish" runat="server"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseSelectAPublishInterval %>" Display="None"
                    SetFocusOnError="true" ValidationGroup="valSaveFeed" />
                <asp:Label ID="lblPublishMessage" runat="server" Text="<%$ Resources:GUIStrings, PublishFeedMessage %>"
                    CssClass="coaching-text" />
            </div>
        </div>
        <iAppsControls:CallbackPanel ID="upChannels" runat="server">
            <ContentTemplate>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, RSSChannel %>" />
                    </label>
                    <div class="form-value">
                        <asp:DropDownList ID="ddlChannels" CssClass="textBoxes" Width="370" runat="server"
                            AutoPostBack="true" OnSelectedIndexChanged="ddlChannels_SelectedIndexChanged" />
                    </div>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
        <div class="form-row">
            <label class="form-label">&nbsp;</label>
            <div class="form-value">
                <asp:Button ID="btnCreateChannels" runat="server" CssClass="button" Text="Create Channel" OnClientClick="return CreateChannel();" />
            </div>
        </div>
    </div>
    <div class="columns right-column">
        <h3>
            <%= GUIStrings.Tags %></h3>
        <div class="form-row assign-tags">
            <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="300" />
        </div>
    </div>
</asp:Content>
