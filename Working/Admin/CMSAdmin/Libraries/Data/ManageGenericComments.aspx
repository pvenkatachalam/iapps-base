<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageGenericComments" MasterPageFile="~/MasterPage.master"
    Theme="General" CodeBehind="ManageGenericComments.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="DateRange" Src="~/UserControls/DateRange.ascx" %>
<%@ Register TagPrefix="UC" TagName="Comments" Src="~/UserControls/Libraries/Data/CommentsGrid.ascx" %>
<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function SearchComments() {
            gridActionsJson.Keyword = $("#txtKeywords").val();
            gridActionsJson.Status = $("#ddlStatus").val();
            gridActionsJson.CustomAttributes["ObjectType"] = $("#ddlCommentType").val();

            var selectedDate = eval("GetSelectedDate_<%=dateRange.ClientID %>()");
            gridActionsJson.StartDate = selectedDate.StartDate;
            gridActionsJson.EndDate = selectedDate.EndDate;
            
            $(".comments-grid").gridActions("callback");

            return false;
        }

        function OnAfterGridRenderComplete() {
            $(".manage-genric-comments").iAppsSplitter();

            $("#txtKeywords").val(gridActionsJson.Keyword);
            $("#ddlStatus").val(gridActionsJson.Status);
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.SiteComments %></h1>
    <div class="left-control">
        <asp:Literal ID="ltSiteName" runat="server" />
        <div class="form-section">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Keywords %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtKeywords" runat="server" CssClass="textBoxes" Width="280" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Type %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlCommentType" runat="server" Width="290" ClientIDMode="Static">
                        <asp:ListItem Value="" Text="<%$ Resources:GUIStrings, All %>" runat="server" />
                        <asp:ListItem Value="7" Text="<%$ Resources:GUIStrings, Content %>" runat="server" />
                        <asp:ListItem Value="8" Text="<%$ Resources:GUIStrings, Page %>" runat="server" />
                        <asp:ListItem Value="33" Text="<%$ Resources:GUIStrings, Image %>" runat="server" />
                        <asp:ListItem Value="9" Text="<%$ Resources:GUIStrings, File %>" runat="server" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Date %>" /></label>
                <div class="form-value">
                    <UC:DateRange ID="dateRange" runat="server" Width="290" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Status %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlStatus" runat="server" Width="290" ClientIDMode="Static">
                        <asp:ListItem Value="0" Text="<%$ Resources:GUIStrings, All %>" runat="server" />
                        <asp:ListItem Value="4" Text="<%$ Resources:GUIStrings, Approved %>" runat="server" />
                        <asp:ListItem Value="1" Text="<%$ Resources:GUIStrings, Unapproved %>" runat="server" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="button-row">
                <asp:Button ID="btnSearch" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Search %>"
                    OnClientClick="return SearchComments();" />
            </div>
        </div>
    </div>
    <div class="right-control">
        <UC:Comments ID="commentList" runat="server" />
    </div>
</asp:Content>
