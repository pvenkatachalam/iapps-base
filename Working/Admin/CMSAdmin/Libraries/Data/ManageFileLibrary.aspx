<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageFileLibrary" MasterPageFile="~/MasterPage.master"
    StylesheetTheme="General" CodeBehind="ManageFileLibrary.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="Library" Src="~/UserControls/Libraries/Data/ManageAssetFile.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.FilesAndMedia %></h1>
    <UC:Library ID="ucManageAssetFile" runat="server" AllowReorder="true" AllowMove="true">
    </UC:Library>
</asp:Content>
