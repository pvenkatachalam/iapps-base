<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageLists" StylesheetTheme="General" CodeBehind="ManageLists.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdLists_OnLoad(sender, eventArgs) {
            $(".manage-lists").gridActions({ objList: grdLists });
        }

        function grdLists_OnRenderComplete(sender, eventArgs) {
            $(".manage-lists").gridActions("bindControls");
        }

        function grdLists_onCallbackComplete(sender, eventArgs) {
            $(".manage-lists").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (gridActionsJson.SelectedItems.length > 0)
                selectedItemId = gridActionsJson.SelectedItems[0];

            switch (gridActionsJson.Action) {
                case "AddList":
                    doCallback = false;
                    window.location = "ManageListDetails.aspx";
                    break;
                case "EditList":
                    doCallback = false;
                    window.location = "ManageListDetails.aspx?ListId=" + selectedItemId;
                    break;
                case "DeleteList":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisList  %>");
                    break;
            }

            if (doCallback) {
                grdLists.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdLists.callback();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manage-lists">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdLists" SkinID="default" runat="server" Width="100%" RunningMode="Callback"
            ShowFooter="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="grdListssDragTemplate"
            LoadingPanelClientTemplateId="grdListssLoadingPanelTemplate" CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="grdLists_OnLoad" />
                <CallbackComplete EventHandler="grdLists_onCallbackComplete" />
                <RenderComplete EventHandler="grdLists_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ListType" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="large-cell">
                                <%# string.Format("<h4>{0}</h4><p>{1}</p>",
                                    Container.DataItem["Title"], Container.DataItem["Description"])%>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Type, Container.DataItem["ListType"])%>
                                </div>
                                 <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["ModifiedDate"]), Container.DataItem["ModifiedByFullName"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdListssDragTemplate">
                    ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdListssLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdLists) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
