<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageBlogComments" MasterPageFile="~/MasterPage.master"
    Theme="General" CodeBehind="ManageBlogComments.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="LeftTree" Src="~/UserControls/Libraries/Data/BlogTree.ascx" %>
<%@ Register TagPrefix="UC" TagName="Comments" Src="~/UserControls/Libraries/Data/CommentsGrid.ascx" %>
<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function LoadBlogGrid() {
            $(".comments-grid").gridActions("selectNode", treeActionsJson.FolderId);
        }

        function SetCustomAttributes() {
            var selectedNode = tvBlogLibrary.get_selectedNode();
            if (selectedNode) {
                if (selectedNode.Depth == 3) {
                    gridActionsJson.CustomAttributes["BlogId"] = selectedNode.ParentNode.ParentNode.get_id();
                    gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.ParentNode.ParentNode.get_text();
                    gridActionsJson.CustomAttributes["BlogYear"] = selectedNode.ParentNode.get_text();
                    gridActionsJson.CustomAttributes["BlogMonth"] = selectedNode.get_text();
                }
                else if (selectedNode.Depth == 2) {
                    gridActionsJson.CustomAttributes["BlogId"] = selectedNode.ParentNode.get_id();
                    gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.ParentNode.get_text();
                    gridActionsJson.CustomAttributes["BlogYear"] = selectedNode.get_text();
                    gridActionsJson.CustomAttributes["BlogMonth"] = null;
                }
                else if (selectedNode.Depth == 1) {
                    gridActionsJson.CustomAttributes["BlogId"] = selectedNode.get_id();
                    gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.get_text();
                    gridActionsJson.CustomAttributes["BlogYear"] = null;
                    gridActionsJson.CustomAttributes["BlogMonth"] = null;
                }
            }
        }

        function OnAfterGridRenderComplete() {
            $(".manage-blog-comments").iAppsSplitter();
        }

        function GetRightSectionTitle() {
            SetCustomAttributes();

            return stringformat("{0} - {1}", gridActionsJson.CustomAttributes["BlogTitle"],
                $(".manage-blog-comments").find(".selected[commandName='Redirect']").html());
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="left-control">
        <UC:LeftTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <UC:Comments ID="commentList" runat="server" />
    </div>
</asp:Content>
