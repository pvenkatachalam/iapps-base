<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ComparePages" StylesheetTheme="General" CodeBehind="ComparePages.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="cphButtons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
            CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    </div>
</asp:Content>
<asp:Content ID="ComparePage" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="columns">
        <div class="form-row">
            <h3>
                <asp:Literal ID="ltVersion1Number" runat="server" />&nbsp;
                <asp:Literal ID="ltVersion1Date" runat="server" />
            </h3>
        </div>
        <div class="compare-panel clear-fix">
            <asp:PlaceHolder ID="Version1PlaceHolder" runat="server" />
        </div>
        <div class="form-footer">
            <asp:Button ID="btnVersion1Rollback" runat="server" Text="<%$ Resources:GUIStrings, RollBackToThisVersion %>"
                ToolTip="<%$ Resources:GUIStrings, RollBackToThisVersion %>" CssClass="button"
                OnClick="btnVersion1Rollback_Click" />
        </div>
    </div>
    <div class="columns right-column">
        <div class="form-row">
            <h3>
                <asp:Literal ID="ltVersion2Number" runat="server" />&nbsp;
                <asp:Literal ID="ltVersion2Date" runat="server" />
            </h3>
        </div>
        <div class="compare-panel clear-fix">
            <asp:PlaceHolder ID="Version2PlaceHolder" runat="server" />
        </div>
        <div class="form-footer">
            <asp:Button ID="btnVersion2Rollback" runat="server" Text="<%$ Resources:GUIStrings, RollBackToThisVersion %>"
                ToolTip="<%$ Resources:GUIStrings, RollBackToThisVersion %>" CssClass="button"
                OnClick="btnVersion2Rollback_Click" />
        </div>
    </div>
</asp:Content>
