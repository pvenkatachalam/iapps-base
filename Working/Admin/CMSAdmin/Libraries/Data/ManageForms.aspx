<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageForms" MasterPageFile="~/MasterPage.master"
    StylesheetTheme="General" CodeBehind="ManageForms.aspx.cs" %>

<%@ Register Src="~/UserControls/Libraries/Data/ManageFormLibrary.ascx" TagName="FormLibrary"
    TagPrefix="UC" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.Forms %></h1>
    <UC:FormLibrary ID="ctlFormLibrary" runat="server" />
</asp:Content>
