<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" CodeBehind="~/Libraries/Data/ContentDefinitionPopup.cs"
    AutoEventWireup="True" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ContentDefinitionPopup" ClientIDMode="Static"
    StylesheetTheme="General" ValidateRequest="false" %>

<%@ Register Src="~/UserControls/Libraries/RecurrenceControl.ascx" TagPrefix="RecurrenceControl" TagName="Repeat" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Web" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <!--[if IE]>
        <style type="text/css">
            .editorCell { padding-bottom: 25px; }
        </style>
    <![endif]-->
    <style type="text/css">
        table, select
        {
            font-size: 11px;
        }
    </style>
    <script type="text/javascript" src="../../common/js/JSMessages.js.aspx"></script>
    <% if (HttpContext.Current.IsDebuggingEnabled)
       { %>
    <script language="javascript" type="text/javascript" src="../../script/General.js"></script>
    <script language="javascript" type="text/javascript" src="../../script/ContentDefinitionEvent.js"></script>
    <script language="javascript" type="text/javascript" src="../../script/GetAppLoginToken.js"></script>
    <% }
       else
       { %>
    <script type="text/javascript" src="../../script/iapps-cms.min.js"></script>
    <% } %>
    <script type="text/javascript">
        var canFocusInLinkTextBox;
        var selectedPage;
        var selectedFile;
        var selectedPageURL;
        var selectedPageId;
        var selectedFileURL;
        var selectedFileId;
        var selectedImageURL;
        var selectedImageId;
        var SelectedList = new Array(1);
        var selectedListId;
        var selectedListURL;
        var selectedContentId;
        var selectedContentURL;
        var selectedProductId;
        var selectedProductURL;
        var ContentDefinitionPopup = 'true';
        var csSharedPopup = "0";
        var targetFolder;
        var returnTarget;
        var requestChars = '<asp:Localize runat="server" Text="<%$ Resources:JSMessages, RequestIsTooLargePleaseSelectLessThan0Characters %>" />';  //Created by Adams
        var adminSiteUrl = "../../";
        var pagePopupWindow;
        var pagepopup;
        var viewPopupWindow;
        var adminpath;
        var productSearchPopup;
        var jTemplateType = '0';

        function CloseDialogPopupFromSelectTargetDirectory() {
            selectTargetDirectory.hide();
            CanceliAppsAdminPopup();
        }

        function CloseDialogPopup() {
            CanceliAppsAdminPopup();
        }
        function CloseDialog() {
            OpeniAppsAdminPopup("SelectTargetDirectory");
        }
        function SaveRow() {
            if (window.parent.location.pathname.indexOf("/Libraries/Data/ManageContentLibrary.aspx") >= 0 || window.parent.location.pathname.indexOf("/StoreManager/NavigationCategories/NavigationBuilder.aspx") >= 0) {
                window.parent.gridContent_BeforeUpdate();
            }
        }
        function ShowImagePopup(paramImageObjectURL, paramImageObjectId) {
            selectedImageURL = paramImageObjectURL;
            selectedImageId = paramImageObjectId;
            OpeniAppsAdminPopup("SelectImageLibraryPopup", "Resource=Image", "SelectImage");
        }
        function SelectImage() {
            selectedImageURL.value = "/" + popupActionsJson.CustomAttributes.ImageUrl.replace(new RegExp(PublicSiteURL, "gi"), "");
            selectedImageId.value = popupActionsJson.SelectedItems.first();
        }
        function ShowFilePopup(paramFileObjectURL, paramFileObjectId) {
            selectedFileId = paramFileObjectId;
            selectedFileURL = paramFileObjectURL;
            OpeniAppsAdminPopup("SelectFileLibraryPopup", "Resource=File", "SelectFile");
        }
        function SelectFile() {
            FullUrl = popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"];
            relativeUrl = FullUrl.replace(new RegExp(PublicSiteURL, "gi"), "");
            selectedFileURL.value = "/" + relativeUrl;
            selectedFileId.value = popupActionsJson.SelectedItems.first();
        }
        function ShowPagePopup(paramPageObjectURL, paramPageObjectId) {
            selectedPageId = paramPageObjectId;
            selectedPageURL = paramPageObjectURL;
            OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
        }
        function SelectPage() {
            var relativeUrl = popupActionsJson.CustomAttributes["SelectedPageUrl"];
            selectedPageURL.value = relativeUrl.replace(new RegExp(jPublicSiteUrl.replace(new RegExp(/\/+$/), ''), "gi"), "");
            selectedPageId.value = popupActionsJson.SelectedItems[0];
        }
        function ShowListPopup(paramListObjectURL, paramListObjectId) {
            OpeniAppsAdminPopup("SelectListPopup", "", "SelectList");
        }

        function SelectList() {
            selectedListId.value = popupActionsJson.SelectedItems.first();
        }

        function ShowContentPopup(paramContentObjectURL, paramContentObjectId) {
            selectedContentId = paramContentObjectId;
            selectedContentURL = paramContentObjectURL;
            OpeniAppsAdminPopup("SelectContentLibraryPopup", "", "SelectContent");
        }

        function SelectContent() {
            selectedContentId.value = popupActionsJson.CustomAttributes["ContentId"];
            selectedContentURL.value = popupActionsJson.CustomAttributes["Title"];
        }

        //List with ProductType
        function ShowProductPopup(paramProductObjectURL, paramProductObjectId) {
            if (CheckLicense("Commerce", true)) {
                selectedProductId = paramProductObjectId;
                selectedProductURL = paramProductObjectURL;
                var popupPath = parent.jCommerceAdminSiteUrl + '/Popups/StoreManager/SelectProductSkuPopup.aspx?showProducts=true';
                SendTokenRequestByURLWithMethodNameForCommerce(popupPath, "OpenProductPopup()");
            }
        }

        function OpenProductPopup() {
            productSearchPopup = dhtmlmodal.open('ProductSearchPopup', 'iframe', destinationUrl, '', 'width=845px,height=540px,center=1,resize=0,scrolling=1');
            productSearchPopup.onclose = function () {
                var a = document.getElementById('ProductSearchPopup');
                var ifr = a.getElementsByTagName("iframe");
                window.frames[ifr[0].name].location.replace("about:blank");
                return true;
            }
        }

        function SelectProductFromPopup(product) {
            if (product != null && product != '' && product != 'undefined') {
                selectedProductId.value = product.Id;
                selectedProductURL.value = product.Url;
            }
            productSearchPopup.hide();
        }

        function CloseProductPopupFromPopup() {
            productSearchPopup.hide();

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <script language="Javascript" type="text/javascript">
        function ShowXmlContentDetail(xmlContentId) {
            var xmlContent;
            if (parent.document.getElementById(xmlContentId) != null) {
                xmlContent = parent.document.getElementById(xmlContentId).value;
                document.getElementById("<%=hdnIsCallback.ClientID%>").value = xmlContent;
                __doPostBack("<%= btnRefreshUpdatePanel.ClientID %>", "");
            }
        }
        function ResetHiddenFieldValueSaveAs() {
            window.parent.NewContent = 1; //save As new Content
            ResetHiddenFieldValue();
        }
        function ResetHiddenFieldValue() {
            document.getElementById("<%=hdnIsCallback.ClientID%>").value = "";
        }
    </script>
    <div class="content-definition-editor" style="width: 970px;border:0;">
        <asp:PlaceHolder runat="server" ID="PlaceHolderXMLForm" />
        <asp:HiddenField ID="hdnIsCallback" runat="server" />
        <asp:Button ID="btnRefreshUpdatePanel" runat="server" Text="<%$ Resources:GUIStrings, Refresh %>"
            Style="visibility: hidden;" />
        <asp:Label ID="lblXlst" CssClass="formLabels" Text="<%$ Resources:GUIStrings, XsltList %>"
            runat="server" Visible="false" />
        <asp:DropDownList ID="drpXsltList" runat="server" Visible="false" Width="600" Style="float: left; margin-left: 9px;" />
    </div>
    <asp:PlaceHolder runat="server" ID="phScriptTags" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="closebtn" onclick="CloseDialogPopup();" runat="server" value="<%$ Resources:GUIStrings, Close %>"
        title="<%$ Resources:GUIStrings, Close %>" class="button" />
    <asp:Button ID="btnSaveAs" Text="<%$ Resources:GUIStrings, SaveAs %>" runat="server" CssClass="button"
        Visible="false" OnClick="btnSaveAs_Click" ToolTip="<%$ Resources:GUIStrings, SaveAs %>"
        OnClientClick="ResetHiddenFieldValueSaveAs();" />
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" runat="server"
        OnClick="btnSave_Click" ToolTip="<%$ Resources:GUIStrings, Save %>"
        OnClientClick="ResetHiddenFieldValue();" CssClass="primarybutton" />
</asp:Content>
