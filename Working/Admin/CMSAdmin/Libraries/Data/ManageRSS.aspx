<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageRSS" StylesheetTheme="General" CodeBehind="ManageRSS.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdRssFeeds_OnLoad(sender, eventArgs) {
            $(".manage-rss").gridActions({ objList: grdRssFeeds });
        }

        function grdRssFeeds_OnRenderComplete(sender, eventArgs) {
            $(".manage-rss").gridActions("bindControls");
        }

        function grdRssFeeds_onCallbackComplete(sender, eventArgs) {
            $(".manage-rss").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (gridActionsJson.SelectedItems.length > 0)
                selectedItemId = gridActionsJson.SelectedItems[0];
            var isFeed = false;
            var channelType = '';
            var ManageChannel = "ManageChannelDetails.aspx";
            if(gridActionsJson.TabIndex == 1)
                isFeed = true;
            else if(gridActionsJson.TabIndex == 2)
                channelType =  "ChannelType=1";
            else if(gridActionsJson.TabIndex == 3)
                channelType = "ChannelType=2";
            
            ManageChannel = ManageChannel + "?" + channelType;

            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    if (!isFeed)
                        OpeniAppsAdminPopup('ManageChannelDetails', channelType, 'fn_RefreshChannels');
                    else
                        window.location = "ManageFeedDetails.aspx";
                    break;
                case "Edit":
                    doCallback = false;
                    if (!isFeed)
                        OpeniAppsAdminPopup('ManageChannelDetails', 'ChannelId=' + selectedItemId + '&' + channelType, 'fn_RefreshChannels');
                    else
                        window.location = "ManageFeedDetails.aspx?FeedId=" + selectedItemId;
                    break;
                case "Delete":
                    doCallback = window.confirm(isFeed ? "<%= JSMessages.AreYouSureYouWantToDeleteThisFeed  %>" : "<%= JSMessages.AreYouSureYouWantToDeleteThisChannel  %>");
                    break;
            }

            if (doCallback) {
                grdRssFeeds.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdRssFeeds.callback();
            }
        }

        function fn_RefreshChannels() {
            $(".manage-rss").gridActions("callback");
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manage-rss">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdRssFeeds" SkinID="default" runat="server" Width="100%"
            RunningMode="Callback" ShowFooter="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="grdRssFeedssDragTemplate"
            LoadingPanelClientTemplateId="grdRssFeedssLoadingPanelTemplate" CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="grdRssFeeds_OnLoad" />
                <CallbackComplete EventHandler="grdRssFeeds_onCallbackComplete" />
                <RenderComplete EventHandler="grdRssFeeds_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ChannelId" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ChannelName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="large-cell">
                                <%# string.Format("<h4>{0}</h4><p>{1}</p>",
                                    Container.DataItem["Title"], Container.DataItem["Description"])%>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# Container.DataItem["ChannelName"] != string.Empty ? string.Format("<strong>{0}</strong>: {1}", GUIStrings.Channels, Container.DataItem["ChannelName"]) : string.Empty %>
                                </div>
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["ModifiedDate"]), Container.DataItem["ModifiedByFullName"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdRssFeedssDragTemplate">
                    ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdRssFeedssLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdRssFeeds) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
