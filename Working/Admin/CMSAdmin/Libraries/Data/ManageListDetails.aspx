<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    ClientIDMode="Static" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageListDetails" StylesheetTheme="General"
    CodeBehind="ManageListDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="ListProperties" Src="~/UserControls/Libraries/Data/ListProperties.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><%= GUIStrings.ManageListDetails %></h1>
        <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" CausesValidation="true" />
        <a href="ManageLists.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <UC:ListProperties ID="listProperties" runat="server" />
</asp:Content>
