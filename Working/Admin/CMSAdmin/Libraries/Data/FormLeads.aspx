﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.FormLeads" MasterPageFile="~/MasterPage.master"
    CodeBehind="FormLeads.aspx.cs" Theme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function upLeads_OnLoad() {
            $(".form-leads").gridActions({
                objList: $(".item-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upLeads_Callback(sender, eventArgs);
                }
            });
        }

        function upLeads_OnRenderComplete() {
            $(".form-leads").gridActions("bindControls");
        }

        function upLeads_OnCallbackComplete() {
            $(".form-leads").gridActions("displayMessage");
        }

        function upLeads_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];

            switch (gridActionsJson.Action) {
                case "ViewContact":
                    doCallback = false;
                    RedirectWithToken("marketier", "/MarketierAdmin/Contacts/ManageContactDetails.aspx?Id=" + selectedItemId, jAppId);
                    break;
                default:
                    break;
            }

            if (doCallback) {
                upLeads.set_callbackParameter(JSON.stringify(gridActionsJson));
                upLeads.callback();
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="leads clear-fix">
        <iAppsControls:CallbackPanel ID="upLeads" runat="server" OnClientCallbackComplete="upLeads_OnCallbackComplete"
            OnClientRenderComplete="upLeads_OnRenderComplete" OnClientLoad="upLeads_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvLeads" runat="server" ItemPlaceholderID="phLeads">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NoItemsFound %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
						<table class="grid" width="100%">
                            <thead>
                                <tr>
                                    <th>
                                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Contact %>" />
                                    </th>
                                    <th>
                                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Email %>" />
                                    </th>
									<th>
                                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, DateCreated %>" />
									</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="phLeads" runat="server" />
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class="item-list collection-row grid-item" objectid='<%# Eval("Id") %>'>
                            <td>
                                <asp:Literal ID="litContact" runat="server" />
                            </td>
                            <td>
                                <asp:Literal ID="litEmail" runat="server" />
                            </td>
							<td>
                                <asp:Literal ID="litDate" runat="server" />
							</td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</asp:Content>
<asp:Content ID="pageheader" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>