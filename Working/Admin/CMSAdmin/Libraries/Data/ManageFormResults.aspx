<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageFormResults" StylesheetTheme="General"
    MasterPageFile="~/MasterPage.Master" CodeBehind="ManageFormResults.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="DateRange" Src="~/UserControls/DateRange.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".select-all-fields input[type='checkbox']").click(function () {
                $(".select-field input[type='checkbox']").prop("checked", $(this).prop("checked"));
            });
        });

        function FilterResults() {
            var selectedDate = eval("GetSelectedDate_<%=dateRange.ClientID %>()");
            gridActionsJson.StartDate = selectedDate.StartDate;
            gridActionsJson.EndDate = selectedDate.EndDate;

            gridActionsJson.CustomAttributes["SelectedFormFields"] = "";
            $(".select-field input[type='checkbox']:checked").each(function () {
                gridActionsJson.CustomAttributes["SelectedFormFields"] += $(this).parent().attr("fieldValue") + ",";
            });

            if (gridActionsJson.CustomAttributes["SelectedFormFields"] != "") {
                SetColumnVisibility();
                $(".form-results").gridActions("callback");
            }
            else {
                alert("<%= JSMessages.PleaseSelectAtLeastOneValueFromTheListToView %>");
            }
            return false;
        }

        function SetColumnVisibility() {
            var grdColumns = grdFormResults.get_table().get_columns();
            for (var i = 1; i < grdColumns.length; i++) {
                var checkBox = $(".select-field *[fieldValue='" + grdColumns[i].get_dataField() + "'] input[type='checkbox']");
                grdColumns[i].set_visible(checkBox.prop("checked"));
            }
        }

        function grdForms_OnLoad(sender, eventArgs) {
            $(".form-results").gridActions({
                objList: grdFormResults,
                listType: "tableGrid",
                onSortMenu: function (sender, eventArgs) {
                    grdFormResults_OnSortMenu(sender, eventArgs)
                }
            });

            var treeHeader = $(".form-results .tree-header");
            var scrollToPosition = treeHeader.offset().top + treeHeader.outerHeight() - 40;
            $(window).on('scroll', function () {
                var scrollTop = $(window).scrollTop();
                if (treeHeader.hasClass("tree-header-fixed"))
                    scrollTop += treeHeader.height();
                if (scrollTop > scrollToPosition) {
                    treeHeader.addClass('tree-header-fixed').width($(".left-control").width());;
                }
                else {
                    treeHeader.removeClass('tree-header-fixed');
                }
            });
        }

        function grdForms_OnRenderComplete(sender, eventArgs) {
            $(".form-results").gridActions("bindControls");
            $(".form-results").iAppsSplitter({ leftHeaderHeight: 51 });
            $("#" + grdFormResults.get_id()).css("width", "100%");
        }

        function grdForms_OnCallbackComplete(sender, eventArgs) {
            $(".form-results").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems.first();
            if (typeof eventArgs != "undefined")
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = confirm("<%= JSMessages.ConfirmFormResponseDelete %>");
                break;
            case "ExportToExcel":
            case "ExportToCsv":
                doCallback = false;
                break;
        }

        if (doCallback) {
            grdFormResults.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdFormResults.callback();
        }
    }

    function grdFormResults_OnSortMenu(sender, eventArgs) {
        eventArgs.menu.empty();

        $(".select-field input[type='checkbox']:checked").each(function () {
            eventArgs.menu.addSortItem($(this).next().text(), $(this).parent().attr("fieldValue"));
        });
    }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="phFormResults" runat="server">
        <div class="left-control">
            <div class="tree-header">
                <h2>
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Filters %>" /></h2>
            </div>
            <div class="form-section">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DateFilterColon %>" /></label>
                    <div class="form-value">
                        <UC:DateRange ID="dateRange" runat="server" Width="290" ShowCustomRange="true" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Fields %>" /></label>
                    <div class="form-value">
                        <asp:CheckBox ID="chkAllElements" CssClass="select-all-fields" Checked="true" runat="server"
                            Text="<%$ Resources:GUIStrings, ShowAll %>" />
                    </div>
                </div>
                <div class="form-row checkbox-list">
                    <asp:CheckBoxList ID="chkFormElements" CssClass="select-field" runat="server" RepeatDirection="Vertical" />
                </div>
                <div class="button-row">
                    <asp:Button ID="btnSearch" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Search %>"
                        OnClientClick="return FilterResults();" />
                </div>
            </div>
        </div>
        <div class="right-control">
            <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
            <div class="grid-section">
                <ComponentArt:Grid ID="grdFormResults" SkinID="default" runat="server" Width="100%"
                    ShowFooter="false" EnableViewState="True" RunningMode="Callback" AutoCallBackOnUpdate="true"
                    CallbackCachingEnabled="true" AutoCallBackOnInsert="true" AllowPaging="True"
                    CallbackCacheLookAhead="10" LoadingPanelClientTemplateId="grdFormResultsLoadingPanelTemplate">
                    <ClientEvents>
                        <Load EventHandler="grdForms_OnLoad" />
                        <RenderComplete EventHandler="grdForms_OnRenderComplete" />
                        <CallbackComplete EventHandler="grdForms_OnCallbackComplete" />
                    </ClientEvents>
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                            HeadingCellCssClass="heading-cell" AllowSorting="false" HeadingRowCssClass="heading-row"
                            SelectedRowCssClass="selected-row" ShowSortHeadings="false"
                            AllowGrouping="false" AlternatingRowCssClass="alternate-row">
                            <Columns>
                                <ComponentArt:GridColumn AllowReordering="false" DataField="Id" DataCellClientTemplateId="EditCellTemplate"
                                    IsSearchable="false" FixedWidth="true" Width="48" Align="Center" HeadingText="&nbsp;" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="EditCellTemplate">
                            <div class="first-cell">
                                ## stringformat('<input type="checkbox" class="item-selector" value="{0}" />', DataItem.getMember('Id').Value)
                            ##
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdFormResultsLoadingPanelTemplate">
                            ## GetGridLoadingPanelContent(grdFormResults) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phEmailResponses" runat="server" Visible="false">
        <div class="page-header clear-fix">
            <h1>
                <%= GUIStrings.FormResponses %></h1>
        </div>
        <div class="form-row">
            <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" ValidationGroup="EmailResponse" />
            <asp:Literal ID="ltEmailMessage" runat="server" />
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Email %>" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtEmail" CssClass="textBoxes email-element" runat="server" Width="400" ValidationGroup="Emailresponse" /><br />
                <asp:Label ID="Localize8" CssClass="coaching-text" runat="server" Text="<%$ Resources:GUIStrings, CommaSeparateMultipleAddresses %>" />
                <asp:RequiredFieldValidator ID="reqtxtEmail" ControlToValidate="txtEmail" runat="server" ErrorMessage="<%$ Resources: JSMessages, EmailRequired %>"
                    Display="None" SetFocusOnError="true" ValidationGroup="EmailResponse" />
                <iAppsControls:iAppsCustomValidator ID="cvtxtEmail" ControlToValidate="txtEmail" runat="server" ValidationGroup="EmailResponse"
                    ErrorMessage="<%$ Resources: JSMessages, TheEmailAddressIsInvalid %>" ValidateType="Emails" />
            </div>
        </div>
        <div class="form-row">
            <asp:Button ID="btnEmailResponses" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Submit %>"
                ValidationGroup="EmailResponse" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
