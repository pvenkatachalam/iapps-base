<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageContentLibrary" MasterPageFile="~/MasterPage.master"
    StylesheetTheme="General" CodeBehind="ManageContentLibrary.aspx.cs" %>

<%@ Register TagPrefix="uc" TagName="Library" Src="~/UserControls/Libraries/Data/ManageContent.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.ContentItems %></h1>
    <uc:library id="UContentLibrary" runat="server"></uc:library>
</asp:Content>
