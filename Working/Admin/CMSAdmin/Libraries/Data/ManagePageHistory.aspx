<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManagePageHistory"
    StylesheetTheme="General" CodeBehind="ManagePageHistory.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdPages_OnLoad(sender, eventArgs) {
            $(".page-history").gridActions({ objList: grdPages });
        }

        function LoadLibraryGrid() {
            $(".page-history").gridActions("selectNode", treeActionsJson.FolderId);
        }

        function grdPages_OnRenderComplete(sender, eventArgs) {
            $(".page-history").gridActions("bindControls");
        }

        function grdPages_OnCallbackComplete(sender, eventArgs) {
            $(".page-history").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            doCallback = false;
            switch (gridActionsJson.Action) {
                case "Compare":
                    if (gridActionsJson.SelectedItems.length != 2)
                        alert("<%= JSMessages.ComparePagesCount %>");
                    else
                        window.location = stringformat("ComparePages.aspx?PageId={0}&Version1={1}&Version2={2}{3}",
                            eventArgs.selectedItem.getMember("ObjectId").get_value(), gridActionsJson.SelectedItems[0], gridActionsJson.SelectedItems[1],
                            gridActionsJson.CustomAttributes["QueryString"]);
                    break;
                case "Rollback":
                    doCallback = true;
                    gridActionsJson.CustomAttributes["VersionId"] = eventArgs.selectedItem.getMember("Id").get_value();
//                    gridActionsJson.CustomAttributes["VersionNumber"] = eventArgs.selectedItem.getMember("VersionNumber").get_value();
//                    gridActionsJson.CustomAttributes["RevisionNumber"] = eventArgs.selectedItem.getMember("RevisionNumber").get_value();
                    break;
            }

            if (doCallback) {
                grdPages.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdPages.callback();
            }
        }

        function Grid_ItemCheckChange(sender, eventArgs) {
            SetCompareButton(sender);
        }

        function Grid_ItemSelect(sender, eventArgs) {
            SetCompareButton(sender);
        }

        function SetCompareButton(sender) {
            if (gridActionsJson.SelectedItems.length == 2)
                sender.getItemByCommand("Compare").showButton();
            else
                sender.getItemByCommand("Compare").hideButton();
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="page-history">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" Position="Top" />
        <div class="grid-section">
            <ComponentArt:Grid ID="grdPages" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
                ShowFooter="false" CssClass="fat-grid" LoadingPanelClientTemplateId="grdPagesLoadingPanelTemplate">
                <ClientEvents>
                    <Load EventHandler="grdPages_OnLoad" />
                    <CallbackComplete EventHandler="grdPages_OnCallbackComplete" />
                    <RenderComplete EventHandler="grdPages_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="CurrentVersion" DataCellServerTemplateId="DetailsTemplate"
                                IsSearchable="true" Visible="true" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="ItemEdited" Visible="false" />
                            <ComponentArt:GridColumn DataField="CreatedDate" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="CreatedByFullName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="CreatedBy" Visible="false" />
                            <ComponentArt:GridColumn DataField="RevisionNumber" Visible="false" />
                            <ComponentArt:GridColumn DataField="VersionNumber" Visible="false" />
                            <ComponentArt:GridColumn DataField="ObjectId" Visible="false" />
                            <ComponentArt:GridColumn DataField="TriggeredObjectId" Visible="false" FormatString="d" />
                            <ComponentArt:GridColumn DataField="TriggeredObjectTypeId" Visible="false" />
                            <ComponentArt:GridColumn DataField="PageAction" Visible="false" />
                            <ComponentArt:GridColumn DataField="SharedPage" Visible="false" />
                            <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                        <Template>
                            <div class="collection-row grid-item clear-fix">
                                <div class="first-cell">
                                    <input type="checkbox" class="item-selector" runat="server" value='<%# Container.DataItem["Id"]%>' />
                                </div>
                                <div class="middle-cell">
                                    <h4>
                                        <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.VersionNo, Container.DataItem["CurrentVersion"])%>
                                    </h4>
                                    <p>
                                        <%# Container.DataItem["ItemEdited"] != string.Empty ? String.Format("{0} : {1}", GUIStrings.ItemsEdited, Container.DataItem["ItemEdited"]) : string.Empty%>
                                    </p>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row">
                                        <%# String.Format("<strong>{0}</strong> : {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["CreatedDate"]),
                                          GUIStrings.By, Container.DataItem["CreatedByFullName"])%>
                                    </div>
                                    <div class="child-row">
                                        <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.Action, Container.DataItem["PageAction"])%>
                                        <%# (Container.DataItem["TriggeredObjectTypeId"] != null && Container.DataItem["TriggeredObjectTypeId"].ToString() == "8") ? "<img src=\"/Admin/App_Themes/General/images/share.png\" alt=\"Page Shared\" title=\"" + Container.DataItem["SharedPage"] + "\"" + "/>" : ""%>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdPagesLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdPages) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</asp:Content>
