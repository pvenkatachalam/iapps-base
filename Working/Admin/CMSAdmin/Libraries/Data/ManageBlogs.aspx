<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageBlogs" StylesheetTheme="General" CodeBehind="ManageBlogs.aspx.cs" %>

<%@ Register TagPrefix="uc" TagName="Library" Src="~/UserControls/Libraries/Data/ManageBlog.ascx" %>
<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
   
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.Blogs %></h1>
    <uc:library id="UBlogLibrary" runat="server"></uc:library>
</asp:Content>
