<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" ValidateRequest="false"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManagePages" StylesheetTheme="General" CodeBehind="ManagePages.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="PageLibrary" Src="~/UserControls/Libraries/Data/ManagePageLibrary.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.MenusAndPages %></h1>
   <UC:PageLibrary ID="pageLibrary" runat="server" />
</asp:Content>
