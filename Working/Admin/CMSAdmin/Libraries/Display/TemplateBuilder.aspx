﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateBuilder.aspx.cs"
    MasterPageFile="~/MasterPage.master" Theme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.TemplateBuilder" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".left-control .template-item").on("click", function () {
                $(this).addClass("selected");
                $(this).siblings().removeClass("selected");
                gridActionsJson.CustomAttributes["SelectedLayout"] = $.trim($(this).find("h4").html());
                $(".template-builder").gridActions("selectNode", $(this).find(".hdnLayoutId").val());
            });

            $(".left-control .template-item[objectId='" + gridActionsJson.FolderId + "']").addClass("selected");
            if ($(".left-control .template-item.selected").length > 0)
                $(".left-control .grid-item-button, .right-control .grid-button").show();
            else
                $(".left-control .grid-item-button, .right-control .grid-button").hide();
        });

        function upTemplates_OnLoad() {
            $(".template-builder").gridActions({
                objList: $(".template-list"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upTemplates_Callback(sender, eventArgs);
                }
            });
        }

        function upTemplates_OnRenderComplete() {
            $(".template-builder").gridActions("bindControls");
            $(".template-builder").iAppsSplitter({ leftScrollSection: ".layout-list", maxHeight: 700 });

            if ($(".left-control .template-item.selected").length > 0)
                $(".left-control .grid-item-button").show();
            else
                $(".left-control .grid-item-button").hide();
        }

        function upTemplates_OnCallbackComplete() {
            $(".template-builder").gridActions("displayMessage");
        }

        function upTemplates_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            var selectedTitle = "";
            if (eventArgs && eventArgs.selectedItem)
                selectedTitle = eventArgs.selectedItem.getValue("Title");

            switch (gridActionsJson.Action) {
                case "CreateTemplate":
                    CreateTemplate();
                    break;
                case "Delete":
                    doCallback = window.confirm("<%= JSMessages.TemplateDeleteConfirmMessage %>");
                    break;
                case "Preview":
                    doCallback = false;
                    OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=3&Id={0}&Name={1}&IsTemplatePreview=true", selectedItemId, selectedTitle));
                    break;
                case "Edit":
                    doCallback = false;
                    JumpToFrontPage("<%= TemplateBuilderUrl %>", "LayoutTemplateId=" + gridActionsJson.FolderId + "&TemplateId=" + selectedItemId);
                    break;
                case "EditProperties":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageTemplateDetails", "NodeId=" + eventArgs.selectedItem.getValue("ParentId") + "&TemplateId=" + selectedItemId, "RefreshTemplateLibrary");
                    break;
            }

            if (doCallback) {
                upTemplates.set_callbackParameter(JSON.stringify(gridActionsJson));
                upTemplates.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            sender.getItemByCommand("Edit").showButton();
            sender.getItemByCommand("EditProperties").showButton();
            sender.getItemByCommand("Delete").showButton();
            if (eventArgs.selectedItem.getValue("SiteId").toLowerCase() != jAppId.toLowerCase()) {
                sender.getItemByCommand("Edit").hideButton();
                sender.getItemByCommand("EditProperties").hideButton();
                sender.getItemByCommand("Delete").hideButton();
            }
        }

        function RefreshTemplateLibrary() {
            $(".template-builder").gridActions("callback");
        }

        function CreateTemplate() {
            JumpToFrontPage("<%=TemplateBuilderUrl %>", "LayoutTemplateId=" + $(".left-control .template-item.selected .hdnLayoutId").val());
        }

        function EditLayoutTemplate(option) {
            var qs = "TemplateType=2&NodeId=<%= LayoutFolderId %>";
            if (option != "new")
                qs += "&TemplateId=" + $(".left-control .template-item.selected .hdnLayoutId").val();

            OpeniAppsAdminPopup("ManageTemplateDetails", qs, "ReloadTemplateBuilder");

            return false;
        }

        function DeleteLayoutTemplate() {
            if (window.confirm("<%= JSMessages.TemplateDeleteConfirmMessage %>")) {
                if (gridActionsJson.RecordCount > 0) {
                    alert("<%= JSMessages.LayoutTemplateDeleteMessage %>");
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "TemplateBuilder.aspx/DeleteLayoutTemplate",
                        data: "{'templateId':'" + gridActionsJson.FolderId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d == "") {
                                gridActionsJson.FolderId = EmptyGuid;
                                ReloadTemplateBuilder(false);
                            }
                            else {
                                alert(msg.d);
                            }
                        }
                    });
                }
            }
        }

        function ReloadTemplateBuilder(assignFolderId) {
            if (assignFolderId != false)
                gridActionsJson.FolderId = popupActionsJson.FolderId;

            var url = jAdminSiteUrl + "/Libraries/Display/TemplateBuilder.aspx";
            if (gridActionsJson.FolderId != EmptyGuid)
                url += "?LayoutId=" + gridActionsJson.FolderId;

            window.location = url;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.TemplateBuilder %></h1>
    <div class="left-control">
        <div class="tree-header clear-fix">
            <h2>
                <asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, LayoutTemplates %>" /></h2>
            <div class="tree-actions clear-fix">
                <div class="row clear-fix">
                    <div class="columns">
                        <div class="grid-button">
                            <input type="button" id="btnAddNew" runat="server" class="grid-button" value="<%$ Resources:GUIStrings, AddLayout %>"
                                onclick="return EditLayoutTemplate('new');" />
                        </div>
                        <div class="grid-item-button" style="display: none;">
                            <input type="button" id="btnEdit" runat="server" class="grid-item-button edit-properties"
                                value="<%$ Resources:GUIStrings, Edit %>" onclick="return EditLayoutTemplate();" />
                        </div>
                        <div class="grid-item-button" style="display: none;">
                            <input type="button" id="btnDelete" runat="server" class="grid-item-button delete-item"
                                value="<%$ Resources:GUIStrings, Delete %>" onclick="return DeleteLayoutTemplate();" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:ListView ID="lvLayoutTemplates" runat="server" ItemPlaceholderID="phLayoutList">
            <LayoutTemplate>
                <div class="layout-list">
                    <asp:PlaceHolder ID="phLayoutList" runat="server" />
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="template-item clear-fix" objectId="<%# Eval("Id") %>">
                    <div class="template-image">
                        <asp:Image ID="imgLayout" runat="server" />
                    </div>
                    <div class="template-info">
                        <h4>
                            <asp:Literal ID="ltlLayoutName" runat="server" /></h4>
                        <p>
                            <asp:Literal ID="ltlLayoutDescription" runat="server" /></p>
                        <input type="hidden" class="hdnLayoutId" id="hdnLayoutId" runat="server" />
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upTemplates" runat="server" OnClientCallbackComplete="upTemplates_OnCallbackComplete"
            OnClientRenderComplete="upTemplates_OnRenderComplete" OnClientLoad="upTemplates_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvTemplates" runat="server" ItemPlaceholderID="phTemplateList">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            No templates found!
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table fat-grid template-list">
                            <asp:PlaceHolder ID="phTemplateList" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# GetRowClassName(Container.DisplayIndex, Eval("SiteId")) %>">
                            <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id") %>'>
                                <div class="large-cell">
                                    <h4 datafield="Title">
                                        <asp:Literal ID="ltTemplateName" runat="server" /></h4>
                                    <p>
                                        <asp:Literal ID="ltTemplateDescription" runat="server" /></p>
                                    <input type="hidden" runat="server" id="hdnParentId" datafield="ParentId" />
                                    <input type="hidden" runat="server" id="hdnSiteId" datafield="SiteId" />
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <asp:Literal ID="ltUpdated" runat="server" />
                                    </div>
                                    <div class="child-row">
                                        <asp:Literal ID="ltStatus" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
        <iAppsControls:GridActions ID="gridActionsBottom" runat="server" />
    </div>
</asp:Content>
