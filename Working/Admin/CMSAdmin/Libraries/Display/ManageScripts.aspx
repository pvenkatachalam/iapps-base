<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageScripts" MasterPageFile="~/MasterPage.master"
    StylesheetTheme="General" CodeBehind="ManageScripts.aspx.cs" %>

<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdScripts_OnLoad(sender, eventArgs) {
            $(".display-library").gridActions({ objList: grdScripts });
        }

        function LoadLibraryGrid() {
            $(".display-library").gridActions("selectNode", treeActionsJson.FolderId);
        }

        function grdScripts_OnRenderComplete(sender, eventArgs) {
            $(".display-library").gridActions("bindControls");
            $(".display-library").iAppsSplitter();
        }

        function grdScripts_OnCallbackComplete(sender, eventArgs) {
            $(".display-library").gridActions("displayMessage");
        }

        function RefreshScriptLibrary() {
            $(".display-library").gridActions("callback");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
//                case "EditSequence":
//                    doCallback = false;
//                    OpeniAppsAdminPopup("ManageDisplayOrder", "ObjectTypeId=35", "RefreshScriptLibrary");
//                    break;
                case "ViewPages":
                    doCallback = false;
                    OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=35&Id={0}&Name={1}", selectedItemId, selectedItem.getMember('Title').get_value()));
                    break;
                case "View":
                    doCallback = false;
                    window.open(selectedItem.GetMember("Path").Value);
                    break;
                case "Archive":
                    doCallback = false;
                    var qString = stringformat("Mode=Warning&ObjectTypeId=35&Id={0}&Name={1}&ButtonText={2}", selectedItemId, selectedItem.getMember('Title').get_value(), "<%= GUIStrings.Archive %>");
                    OpeniAppsAdminPopup("ViewPagesUsingObject", qString, "CloseArchiveJS");
                    break;
                case "Edit":
                    doCallback = false;
                    EditScriptDetails(selectedItemId);
                    break;
                case "Add":
                    doCallback = false;
                    EditScriptDetails(null);
                    break;
            }

            if (doCallback && typeof grdScripts != "undefined") {
                grdScripts.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdScripts.callback();
            }
        }

        var operation;
        function CloseManageScriptDetails() {
            if (operation == "add") 
                RefreshLibraryTree();

            $(".display-library").gridActions("callback", "Load");
        }

        function CloseArchiveJS() {
            $(".display-library").gridActions("callback", "ArchiveScript");
        }

        function EditScriptDetails(scriptId) {
            var editUrl = 'NodeId=' + gridActionsJson.FolderId;
            operation = "add";
            if (scriptId != null) {
                editUrl += '&ScriptId=' + scriptId;
                operation = "edit";
            }

            OpeniAppsAdminPopup("ManageScriptDetails", editUrl, "CloseManageScriptDetails");
        }

        function Grid_ItemSelect(sender, eventArgs) {

            sender.getItemByCommand("Edit").showButton();
            sender.getItemByCommand("Archive").show();


            if (eventArgs.selectedItem.getMember("IsEditable").get_value() == false) {
                sender.getItemByCommand("Edit").hideButton();
                sender.getItemByCommand("Archive").hide();

            }
        }

        var selectedItem;
        function GridItem_ShowMenu(menu) {
            selectedItem = grdScripts.getItemFromKey(0, gridActionsJson.SelectedItems[0]);

            if (selectedItem.getMember("Status").get_value() == 1) {
                menu.getItemByCommand("Archive").setText("<%= JSMessages.Archive %>");
                menu.getItemByCommand("Archive").setComandArgument("Archive");
            }
            else {
                menu.getItemByCommand("Archive").setText("<%= JSMessages.MakeActive %>");
                menu.getItemByCommand("Archive").setComandArgument("MakeActive");
            }
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.Scripts %></h1>
    <div class="left-control">
        <UC:LibraryTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdScripts" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
            AllowEditing="false" AutoCallBackOnInsert="true" CallbackCachingEnabled="true" CssClass="fat-grid"
            AutoCallBackOnUpdate="true" ShowFooter="false" CallbackCacheLookAhead="10" AllowMultipleSelect="false"
            LoadingPanelClientTemplateId="grdScriptsLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdScripts_OnLoad" />
                <CallbackComplete EventHandler="grdScripts_OnCallbackComplete" />
                <RenderComplete EventHandler="grdScripts_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Status').Value==7"
                            RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                         <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('IsEditable').Value==false"
                            RowCssClass="disabled-row" SelectedRowCssClass="disabled-row" HoverRowCssClass="disabled-row" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="FileName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                        <ComponentArt:GridColumn DataField="StatusName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="GlobalName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="Path" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" />
                        <ComponentArt:GridColumn DataField="OrderNo" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsEditable" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["Title"].ToString() != Container.DataItem["Description"].ToString() ? Container.DataItem["Description"] : ""%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                   <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.FileName, Container.DataItem["FileName"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}",Container.DataItem["ModifiedDate"]),
                                        GUIStrings.By, Container.DataItem["ModifiedByFullName"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1} | <strong>{2}</strong>: {3}",
                                        GUIStrings.Status, Container.DataItem["StatusName"], GUIStrings.Global, Container.DataItem["GlobalName"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdScriptsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdScripts) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
