<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageCSS" StylesheetTheme="General" CodeBehind="ManageCSS.aspx.cs" %>

<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdStyles_OnLoad(sender, eventArgs) {
            $(".display-library").gridActions({ objList: grdStyles });
        }

        function LoadLibraryGrid() {
            $(".display-library").gridActions("selectNode", treeActionsJson.FolderId);
        }

        function grdStyles_OnRenderComplete(sender, eventArgs) {
            $(".display-library").gridActions("bindControls");
            $(".display-library").iAppsSplitter();
        }

        function grdStyles_OnCallbackComplete(sender, eventArgs) {
            $(".display-library").gridActions("displayMessage");
        }

        function RefreshStyleLibrary() {
            $(".display-library").gridActions("callback");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
//                case "EditSequence":
//                    doCallback = false;
//                    OpeniAppsAdminPopup("ManageDisplayOrder", "ObjectTypeId=4", "RefreshStyleLibrary");
//                    break;
                case "ViewPages":
                    doCallback = false;
                    OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=4&Id={0}&Name={1}", selectedItemId, selectedItem.getMember('Title').get_value()));
                    break;
                case "ViewCSS":
                    doCallback = false;
                    window.open(selectedItem.GetMember("Path").Value);
                    break;
                case "Archive":
                    doCallback = false;
                    var qString = stringformat("Mode=Warning&ObjectTypeId=4&Id={0}&Name={1}&ButtonText={2}", selectedItemId, selectedItem.getMember('Title').get_value(), "<%= GUIStrings.Archive %>");
                    OpeniAppsAdminPopup("ViewPagesUsingObject", qString, "CloseArchiveCSS");
                    break;
                case "EditStyle":
                    doCallback = false;
                    EditStyleDetails(selectedItemId);
                    break;
                case "AddStyle":
                    doCallback = false;
                    EditStyleDetails(null)
                    break;                
            }

            if (doCallback && typeof grdStyles != "undefined") {
                grdStyles.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdStyles.callback();
            }
        }

        var operation;
        function EditStyleDetails(styleId) {
            var editUrl = 'NodeId=' + gridActionsJson.FolderId;
            operation = "add";

            if (styleId != null) {
                editUrl += '&StyleId=' + styleId;
                operation = "edit";
            }

            OpeniAppsAdminPopup("ManageStyleDetails", editUrl, "CloseManageStyleDetails");
        }

        function CloseManageStyleDetails() {
            if (operation == "add")
                RefreshLibraryTree();

            $(".display-library").gridActions("callback");
        }

        function CloseArchiveCSS() {
            $(".display-library").gridActions("callback", "ArchiveCSS");
        }

        function Grid_ItemSelect(sender, eventArgs) {

            sender.getItemByCommand("EditStyle").showButton();
            sender.getItemByCommand("Archive").show();
            

            if (eventArgs.selectedItem.getMember("IsEditable").get_value() == false) {
                sender.getItemByCommand("EditStyle").hideButton();
                sender.getItemByCommand("Archive").hide();
                
            }
        }

        function GridItem_ShowMenu(sender, eventArgs) {
            if (eventArgs.selectedItem.getMember("Status").get_value() == 1) {
                eventArgs.menu.getItemByCommand("Archive").setText("<%= JSMessages.Archive %>");
                eventArgs.menu.getItemByCommand("Archive").setComandArgument("Archive");
            }
            else {
                eventArgs.menu.getItemByCommand("Archive").setText("<%= JSMessages.MakeActive %>");
                eventArgs.menu.getItemByCommand("Archive").setComandArgument("MakeActive");
            }
        }


    </script>
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.Styles %></h1>
    <div class="left-control">
        <UC:LibraryTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdStyles" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
            AllowEditing="false" AutoCallBackOnInsert="true" CallbackCachingEnabled="true" CssClass="fat-grid"
            AutoCallBackOnUpdate="true" ShowFooter="false" CallbackCacheLookAhead="10" AllowMultipleSelect="false"
            LoadingPanelClientTemplateId="grdStylesLoadingPanelTemplate">
            <ClientEvents>
                <CallbackComplete EventHandler="grdStyles_OnCallbackComplete" />
                <RenderComplete EventHandler="grdStyles_OnRenderComplete" />
                <Load EventHandler="grdStyles_OnLoad" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Status').Value==7"
                            RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                         <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('IsEditable').Value==false"
                            RowCssClass="disabled-row" SelectedRowCssClass="disabled-row" HoverRowCssClass="disabled-row" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="FileName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="MediaName" Visible="false" />
                        <ComponentArt:GridColumn DataField="GlobalName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ConditionalCss" Visible="false" />
                        <ComponentArt:GridColumn DataField="StatusName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                        <ComponentArt:GridColumn DataField="Path" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" />
                        <ComponentArt:GridColumn DataField="OrderNo" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                         <ComponentArt:GridColumn DataField="IsEditable" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["Title"].ToString() != Container.DataItem["Description"].ToString() ? Container.DataItem["Description"] : ""%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                   <%# String.Format("<strong>{0}</strong>: {1} | <strong>{2}</strong>: {3}", GUIStrings.FileName, Container.DataItem["FileName"],
                                        GUIStrings.Media, Container.DataItem["MediaName"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}",Container.DataItem["ModifiedDate"]),
                                        GUIStrings.By, Container.DataItem["ModifiedByFullName"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1} | <strong>{2}</strong>: {3}",
                                        GUIStrings.Status, Container.DataItem["StatusName"], GUIStrings.Global, Container.DataItem["GlobalName"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdStylesLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdStyles) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
