﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogActivity.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.BlogActivityWidget" %>
<asp:PlaceHolder ID="phBlogComments" runat="server">
    <h4>
        <asp:Literal ID="ltLatestDate" runat="server" /></h4>
    <h5>
        <asp:HyperLink ID="hplLatestTitle" runat="server" /></h5>
    <p>
        <asp:Literal ID="ltLatestComments" runat="server" /></p>
    <hr />
    <asp:Repeater ID="rpBlogComments" runat="server">
        <HeaderTemplate>
            <h4>
                <%= GUIStrings.TotalCommentsFor %></h4>
        </HeaderTemplate>
        <ItemTemplate>
            <div class="form-row">
                <h5>
                    <asp:HyperLink ID="hplBlog" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# "~/Libraries/Data/ManageBlogs.aspx?BlogId=" + Eval("BlogId") %>' />
                </h5>
                <p>
                    <%# String.Format("{0}&nbsp;{1}", Eval("TotalComments"), GUIStrings.CommentsWaitingApproval)%></p>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:PlaceHolder>
<asp:Literal ID="ltNoResults" runat="server" />
<div class="form-row" style="margin-top: 20px;">
    <asp:HyperLink ID="hplManageBlogs" runat="server" Text="<%$ Resources:GUIStrings, ManageBlogsLink %>"
        NavigateUrl="~/Libraries/Data/ManageBlogs.aspx" /></div>
