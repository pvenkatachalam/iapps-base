﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormActivity.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.FormActivityWidget" %>
<asp:PlaceHolder ID="phFormResponses" runat="server">
    <asp:Repeater ID="rpFormResponses" runat="server">
        <ItemTemplate>
            <div class="form-row">
                <h5>
                    <%# Eval("Title") %>
                </h5>
                <p>
                    <asp:HyperLink ID="hplForm" runat="server" NavigateUrl='<%# "~/Libraries/Data/ManageFormResults.aspx?FormId=" + Eval("FormId") %>'>
                            <%# String.Format("{0}&nbsp;{1}", Eval("TotalResponses"), GUIStrings.Responses) %>
                    </asp:HyperLink>
                </p>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Literal ID="ltNoResults" runat="server" />
    <div class="form-row" style="margin-top: 20px;">
        <asp:HyperLink ID="hplManageForms" runat="server" Text="<%$ Resources:GUIStrings, ManageFormsLink %>"
            NavigateUrl="~/Libraries/Data/ManageForms.aspx" /></div>
</asp:PlaceHolder>
