﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PublishingActivity.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.PublishingActivityWidget" %>
<asp:PlaceHolder ID="phActivity" runat="server" Visible="false">
    <div class="form-row">
        <div class="form-value">
            <asp:DropDownList ID="ddlActivity" runat="server" AutoPostBack="true">
                <asp:ListItem Text="<%$ Resources:GUIStrings, AllActivities %>" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, MyActivity %>" />
            </asp:DropDownList>
        </div>
    </div>
</asp:PlaceHolder>
<asp:Repeater ID="rptActivity" runat="server">
    <ItemTemplate>
        <p class="links">
            <asp:HyperLink ID="hplCount" runat="server" NavigateUrl='<%# Eval("Url") %>'>
                <%# Eval("Count") %>
            </asp:HyperLink>&nbsp;
            <%# Eval("Text") %>
        </p>
    </ItemTemplate>
</asp:Repeater>
<asp:Literal ID="ltNoResults" runat="server" />
