﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UsageReport.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.UsageReport" %>

<script type="text/javascript">
    var barHeight = 165;
    var minBarHeight = 2;

    $(function () {
        UpdateUsageGraph();
    });

    function UpdateUsageGraph(lastUsage, currentUsage, currentCapacity) {
        var allowedUsageBarId = "allowedUsage";
        var lastMonthUsageBarId = "lastMonthUsage";
        var thisMonthUsageBarId = "thisMonthUsage";

        SetBarValues(allowedUsageBarId, lastMonthUsageBarId, thisMonthUsageBarId, currentCapacity, lastUsage, currentUsage);
        $('.barGraphSection:last').css('border-right', '0');
    }

    function SetBarValues(allowedUsageBarId, lastMonthUsageBarId, thisMonthUsageBarId, currentCapacity, lastUsage, currentUsage) {
        if (currentCapacity >= lastUsage && currentCapacity >= currentUsage) {
            var lastMonthUsageBarHeight = (barHeight * lastUsage / currentCapacity) > barHeight ? barHeight : (barHeight * lastUsage / currentCapacity);
            var thisMonthUsageBarHeight = (barHeight * currentUsage / currentCapacity) > barHeight ? barHeight : (barHeight * currentUsage / currentCapacity);
            $('#' + allowedUsageBarId + ' div.colorDiv').css('height', (barHeight + 'px'));
            $('#' + lastMonthUsageBarId + ' div.colorDiv').css('height', (lastUsage != 0 ? lastMonthUsageBarHeight + "px" : minBarHeight + "px"));
            $('#' + thisMonthUsageBarId + ' div.colorDiv').css('height', (currentUsage != 0 ? thisMonthUsageBarHeight + "px" : minBarHeight + "px"));
        }
        else if (lastUsage >= currentCapacity && lastUsage >= currentUsage) {
            var allowedUsageBarHeight = (barHeight * currentCapacity / lastUsage) > barHeight ? barHeight : barHeight * currentCapacity / lastUsage;
            var thisMonthUsageBarHeight = (barHeight * currentUsage / lastUsage) > barHeight ? barHeight : barHeight * currentUsage / lastUsage;
            $('#' + allowedUsageBarId + ' div.colorDiv').css('height', (allowedUsageBarHeight + "px"));
            $('#' + lastMonthUsageBarId + ' div.colorDiv').css('height', (barHeight + "px"));
            $('#' + thisMonthUsageBarId + ' div.colorDiv').css('height', (currentUsage != 0 ? (thisMonthUsageBarHeight) + "px" : minBarHeight + "px"));
        }
        else if (currentUsage >= currentCapacity && currentUsage >= lastUsage) {
            var allowedUsageBarHeight = (barHeight * currentCapacity / currentUsage) > barHeight ? barHeight : barHeight * currentCapacity / currentUsage;
            var lastMonthUsageBarHeight = (barHeight * lastUsage / currentUsage) > barHeight ? barHeight : barHeight * lastUsage / currentUsage;
            $('#' + allowedUsageBarId + ' div.colorDiv').css('height', (allowedUsageBarHeight + "px"));
            $('#' + lastMonthUsageBarId + ' div.colorDiv').css('height', (lastUsage != 0 ? (lastMonthUsageBarHeight) + "px" : minBarHeight + "px"));
            $('#' + thisMonthUsageBarId + ' div.colorDiv').css('height', (barHeight + "px"));
        }
        $('#' + allowedUsageBarId + ' div.valueDiv').html(currentCapacity);
        $('#' + lastMonthUsageBarId + ' div.valueDiv').html(lastUsage);
        $('#' + thisMonthUsageBarId + ' div.valueDiv').html(currentUsage);
    }
</script>
<asp:Panel runat="server" ID="uxErrorPanel" Visible="false">
    <asp:Literal runat="server" ID="uxMessage" Text="My Usage"></asp:Literal>
    <br />
</asp:Panel>
<asp:Panel runat="server" ID="uxUsageGraphsPanel" Visible="false">
<div class="usage-graph clear-fix">
    <div class="graph-legends clear-fix">
        <div class="usage-limit">
            <span>&nbsp;</span>
            <span><%= GUIStrings.UsageLimit %></span>
        </div>
        <div class="last-usage">
            <span>&nbsp;</span>
            <span><%= GUIStrings.LastMonthUsage %></span>
        </div>
        <div class="current-usage">
            <span>&nbsp;</span>
            <span><%= GUIStrings.CurrentMonthUsage %></span>
        </div>
    </div>
    <asp:Repeater ID="uxBarGraphList" runat="Server" Visible="false">
        <ItemTemplate>
            <div class="bar-graphs">
                <div class="bar-title">
                    <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).Title %>
                </div>
                <div class="bar-cover clear-fix">
                    <div id="allowedUsage" class="allowedUsage bar">
                        <div class="valueDiv">
                            <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).AllowedValue %>
                        </div>
                        <div class="colorDiv" style="height: <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).AllowedValueHeight %>px;">
                        </div>
                    </div>
                    <div id="lastMonthUsage" class="lastMonthUsage bar <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).IsPreviousOver ? "over" : "" %>">
                        <div class="valueDiv">
                            <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).PreviousValue %>
                        </div>
                        <div class="colorDiv" style="height: <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).PreviousValueHeight %>px;">
                        </div>
                    </div>
                    <div id="thisMonthUsage" class="thisMonthUsage bar <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).IsCurrentOver ? "over" : ""  %>">
                        <div class="valueDiv">
                            <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).CurrentValue %>
                        </div>
                        <div class="colorDiv" style="height: <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).CurrentValueHeight %>px;">
                        </div>
                    </div>
                </div>
                <div class="graph-text">
                    <%# (Container.DataItem as Bridgeline.iAPPS.Admin.CMS.Web.Controls.BarGraphItem).UnitsFriendly %>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
</asp:Panel>
