<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.CommentsGrid" 
    CodeBehind="CommentsGrid.ascx.cs" %>
<script type="text/javascript">
    $(function () {
        $("#editComments").iAppsDialog({ appendToEnd: true });
    });

    function gridComments_OnLoad(sender, eventArgs) {
        $(".comments-grid").gridActions({ objList: gridComments,
            onCallback: function (sender, eventArgs) {
                gridActions_Callback(sender, eventArgs);
            },
            onDropMenu: function (sender, eventArgs) {
                gridActions_DropMenu(sender, eventArgs);
            }
        });
    }

    function gridComments_OnRenderComplete(sender, eventArgs) {
        $(".comments-grid").gridActions("bindControls");
        if (typeof OnAfterGridRenderComplete == 'function')
            OnAfterGridRenderComplete();
    }

    function gridComments_onCallbackComplete(sender, eventArgs) {
        $(".comments-grid").gridActions("displayMessage");
    }

    function gridActions_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = "";
        if (gridActionsJson.SelectedItems.length > 0)
            selectedItemId = gridActionsJson.SelectedItems[0];

        if (typeof SetCustomAttributes == 'function')
            SetCustomAttributes();

        switch (gridActionsJson.Action) {
            case "Edit":
                doCallback = false;
                showEditComments(eventArgs.selectedItem, 'edit');
                break;
            case "Reply":
                doCallback = false;
                showEditComments(eventArgs.selectedItem, 'reply');
                break;
            case "DeleteComments":
                doCallback = confirm("<%= JSMessages.AreYouSureYouWantToDelete %>");
                break;
            case "ApproveComments":
                doCallback = confirm("<%= JSMessages.SureToApproveComments %>");
                break;
            case "UnApproveComments":
                doCallback = confirm("<%= JSMessages.SureToUnApproveComments %>");
                break;
            case "Redirect":
                doCallback = false;
                window.location = stringformat("{0}?BlogId={1}", gridActionsJson.CustomAttributes["Redirect"], gridActionsJson.CustomAttributes["BlogId"]);
                break;
        }

        if (doCallback) {
            if (gridActionsJson.SelectedItems.length > 0)
                gridActionsJson.CustomAttributes["ObjectTypeId"] = eventArgs.selectedItem.getMember('ObjectTypeId').get_value();
            
            gridComments.set_callbackParameter(JSON.stringify(gridActionsJson));
            gridComments.callback();
        }
    }

    function showEditComments(item, operation) {
        gridActionsJson.SelectedItems = [];
        gridActionsJson.SelectedItems.push(item.Key);

        gridActionsJson.CustomAttributes["PostedBy"] = item.getMember('PostedBy').get_value();
        gridActionsJson.CustomAttributes["ObjectId"] = item.getMember('ObjectId').get_value();
        gridActionsJson.CustomAttributes["CommentText"] = "";

        if (operation == 'edit') {
            gridActionsJson.CustomAttributes["CommentText"] = item.getMember('Comments').get_value();
            gridActionsJson.CustomAttributes["ParentCommentId"] = item.getMember('ParentId').get_value();
            gridActionsJson.CustomAttributes["Operation"] = "UpdateComment";

            $("#divReply").hide();
            $("#txtEditComments").prop("readonly", false);
        }
        else {
            gridActionsJson.CustomAttributes["CommentText"] = item.getMember('Comments').get_value();
            gridActionsJson.CustomAttributes["ParentCommentId"] = item.getMember('Id').get_value();
            gridActionsJson.CustomAttributes["Operation"] = "ReplyComment";

            $("#divReply").show();
            $("#txtEditComments").prop("readonly", true);
        }

        $("#txtEditPostedBy").val(juserName);
        $("#txtEditComments").val(gridActionsJson.CustomAttributes["CommentText"]);
        $("#txtReplyComments").val("");

        $("#editComments").iAppsDialog("open");

        return false;
    }

    function SaveComment() {
        $("#editComments").iAppsDialog("close");
        gridActionsJson.CustomAttributes["PostedBy"] = $("#txtEditPostedBy").val();

        if (gridActionsJson.CustomAttributes["Operation"] == 'ReplyComment')
            gridActionsJson.CustomAttributes["CommentText"] = $("#txtReplyComments").val();
        else
            gridActionsJson.CustomAttributes["CommentText"] = $("#txtEditComments").val();

        $(".comments-grid").gridActions("callback", gridActionsJson.CustomAttributes["Operation"]);

        return false;
    }

    function CloseSaveComment() {
        $("#editComments").iAppsDialog("close");

        return false;
    }
</script>
<div class="comments-grid">
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="gridComments" SkinID="Default" runat="server" Width="100%"
            RunningMode="Callback" ShowFooter="false" LoadingPanelClientTemplateId="gridCommentsLoadingPanelTemplate"
            CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="gridComments_OnLoad" />
                <CallbackComplete EventHandler="gridComments_onCallbackComplete" />
                <RenderComplete EventHandler="gridComments_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Comments" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="PostedBy" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CreatedDate" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ObjectId" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ObjectTypeId" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="ParentId" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CommentedOn" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="gridCommentsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(gridComments) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="first-cell">
                                <input type="checkbox" class="item-selector" value="<%# Container.DataItem["Id"] %>" />
                            </div>
                            <div class="middle-cell">
                                <%# string.Format("<h4>{0}</h4><p>{1}</p>", Container.DataItem["Title"], Container.DataItem["Comments"])%>
                            </div>
                            <div class="last-cell">
                                <%# Container.DataItem["CommentedOn"] != null && Container.DataItem["CommentedOn"].ToString() != string.Empty ?
                                    String.Format("<div class='child-row'><strong>{0}</strong>: {1}</div>", GUIStrings.CommentedOn, Container.DataItem["CommentedOn"]) : string.Empty%>
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Posted, String.Format("{0:d}",Container.DataItem["CreatedDate"]), Container.DataItem["PostedBy"])%>
                                </div>
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Enum.ToObject(typeof(Bridgeline.FW.iAPPSComments.Comments.StatusTypes), Container.DataItem["Status"]))%>
                                </div>
                            </div>
                            <div class="clear-fix">
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:Grid>
    </div>
</div>
<asp:PlaceHolder ID="phEditComments" runat="server" ClientIDMode="Static">
    <div id="editComments" class="iapps-modal" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Comments %>" /></h2>
        </div>
        <div class="modal-content">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, PostedBy %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtEditPostedBy" runat="server" Width="440" CssClass="textBoxes"
                        ClientIDMode="Static" /></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Comments %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox CssClass="textBoxes" ID="txtEditComments" runat="server" TextMode="MultiLine"
                        Rows="8" Width="440" ClientIDMode="Static" /></div>
            </div>
            <div class="form-row" id="divReply">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Reply %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox CssClass="textBoxes" ID="txtReplyComments" runat="server" TextMode="MultiLine"
                        Rows="8" Width="440" ClientIDMode="Static" /></div>
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                CssClass="button" OnClientClick="return CloseSaveComment();" />
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, SaveAndApprove %>"
                CssClass="primarybutton" OnClientClick="return SaveComment();" ClientIDMode="Static" />
        </div>
    </div>
</asp:PlaceHolder>
