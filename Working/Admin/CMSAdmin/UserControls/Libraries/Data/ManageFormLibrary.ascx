<%@ Control Language="C#" AutoEventWireup="True" Codebehind="ManageFormLibrary.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ManageFormLibrary" %>
<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<%@ Register assembly="ComponentArt.Web.UI" namespace="ComponentArt.Web.UI" tagprefix="ComponentArt" %>
<script type="text/javascript">
    function grdForms_OnLoad(sender, eventArgs) {
        $("#form_library").gridActions({ objList: grdForms });
    }

    function LoadLibraryGrid() {
        $("#form_library").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function grdForms_OnRenderComplete(sender, eventArgs) {
        $("#form_library").gridActions("bindControls");
        $("#form_library").iAppsSplitter();
    }

    function grdForms_OnCallbackComplete(sender, eventArgs) {
        $("#form_library").gridActions("displayMessage", { complete: function () {
            if (gridActionsJson.ResponseCode == 100)
                CanceliAppsAdminPopup(true);
        }
        });
    }

    var xmlValue;
    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;

        switch (gridActionsJson.Action) {
            case "Preview":
                doCallback = false;
                qString = 'formId=' + selectedItemId + '&fName=' + selectedItem.getMember('Title').get_value() + '&formType=' + selectedItem.getMember('FormType').get_value();
                OpeniAppsAdminPopup("FormPreview", qString);
                break;
            case "ViewResults":
                doCallback = false;
                var responseCount = selectedItem.getMember('ResponseCount').get_value();
                if (responseCount > 0) {
                    window.location = "ManageFormResults.aspx?FormId=" + selectedItemId;
                }
                else {
                    alert("<%= JSMessages.ThereAreNoResponsesToThisForm %>");
                }
                break;
            case 'ExportLeads':
                break;
            case 'ViewLeads':
                window.location = "FormLeads.aspx?FormId=" + selectedItemId;
                break;
            case "DeleteForm":
                doCallback = confirm("<%= JSMessages.AreYouSureYouWantToDeleteTheSelectedFormDeletingWillRemoveThisFormFromAllPagesThatUtilizeThisForm %>");
                break;
            case "EditForm":
                doCallback = false;

                var formType = parseInt(selectedItem.getMember('FormType').get_value());

                if (formType == 2) {
                    if (hasMarketierLicense) {
                        var editUrl = 'FormId=' + selectedItemId;

                        OpeniAppsAdminPopup("EditLeadFormPopup", editUrl, "RefreshFormLibrary");
                    }
                }
                else {
                    xmlValue = ChangeSpecialCharacters(selectedItem.getMember("XMLString").get_text());
                    var qString = 'fName=' + selectedItem.getMember('Title').get_value() + '&formId=' + selectedItemId + '&formType=' + selectedItem.getMember("FormType").get_text() + '&SaveForm=true';
                    OpeniAppsAdminPopup("FormDefinitionPopup", qString);
                }

                break;
            case "AddForm":
                doCallback = false;
                OpeniAppsAdminPopup("CreateForm", 'NodeId=' + gridActionsJson.FolderId, "RefreshFormLibrary");
                break;
            case "Download":
                doCallback = false;
                var responseCount = selectedItem.getMember('ResponseCount').get_value();
                var downloadExists = CheckForDownloads(selectedItem.getMember('Title').get_value(), selectedItemId);

                if (responseCount == 0 || downloadExists.toLowerCase() == "false") {
                    alert("<%= JSMessages.ThereAreNoDownloadsForThisForm %>");
                    eventArgs.event.preventDefault();
                }
                else {
                    $("#hdnFileName").val(selectedItem.getMember('Title').get_value());
                    $("#hdnFormId").val(selectedItemId);
                }
            case "AddToList":
                doCallback = false;
                AddManualListItem(grdForms);
                break;
        }

        if (doCallback) {
            grdForms.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdForms.callback();
        }
    }

    function Grid_ItemSelect(sender, eventArgs) {
        selectedItem = eventArgs.selectedItem;

        var formType = parseInt(selectedItem.getMember('FormType').get_value());

        if (formType === 0)
            sender.getItemByCommand("Download").show();
        else
            sender.getItemByCommand("Download").hide();

        if (formType === 2) {
            sender.getItemByCommand("ViewResults").hide();

            if (hasMarketierLicense) {
                sender.getItemByCommand("EditForm").showButton();
                sender.getItemByCommand("ViewLeads").show();
                sender.getItemByCommand("ExportLeads").show();
            }
            else
                sender.getItemByCommand("EditForm").hideButton();
        }
        else {
            sender.getItemByCommand("EditForm").showButton();
            sender.getItemByCommand("ViewResults").show();
            sender.getItemByCommand("ViewLeads").hide();
            sender.getItemByCommand("ExportLeads").hide();
        }

        if (selectedItem.getMember('SourceFormId').get_value() == "")
            sender.getItemByCommand("ReImport").hideButton();
        else
            sender.getItemByCommand("ReImport").showButton();
    }

    function RefreshFormLibrary() {
        $("#form_library").gridActions("callback", { refreshTree: true });
    }

    function ImportFromMaster() {
        if (gridActionsJson.SelectedItems.length > 0) {
            if (GetFormVariantStatus() == "true") {
                if (window.confirm("<%= JSMessages.FormReimportAlertMessage %>")) {
                    $("#form_library").gridActions("callback", "ImportFromMaster");
                }
                else
                    doCallback = false;
            }
            else {
                $("#form_library").gridActions("callback", "ImportFromMaster");
            }
        }
        else
            alert("<%= JSMessages.PleaseSelectAFormToImport %>");

        return false;
    }

    function GetFormVariantStatus() {
        var formVariantStatus;
        $.ajax({
            type: "POST",
            url: "SelectFormLibraryPopup.aspx/IsFormVariantExist",
            data: "{'formIds':'" + JSON.stringify(gridActionsJson.SelectedItems) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                formVariantStatus = msg.d;
            }
        })
        return formVariantStatus;
    }

    function SelectFormFromLibrary() {
        var selectedItem = $("#form_library").gridActions("getSelectedItem");
        if (selectedItem) {
            popupActionsJson.CustomAttributes["FormName"] = selectedItem.getMember("Title").get_text();
        }

        popupActionsJson.CustomAttributes["SelectedFormSiteId"] = GetQueryStringValueByName("LoadSiteId");
        SelectiAppsAdminPopup();

        return false;
    }

    function SelectMenuFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        SelectiAppsAdminPopup();

        return false;
    }

    function GetQueryStringValueByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(window.location.href);
        if (results == null)
            return "";
        else
            return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
</script>
<div id="form_library">
    <div class="left-control">
        <UC:LibraryTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <asp:HiddenField ID="hdnFileName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnFormId" runat="server" ClientIDMode="Static" />
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <div class="grid-section">
            <ComponentArt:Grid ID="grdForms" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
                ShowFooter="false" LoadingPanelClientTemplateId="grdFormsLoadingPanelTemplate"
                CssClass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="grdForms_OnLoad" />
                    <CallbackComplete EventHandler="grdForms_OnCallbackComplete" />
                    <RenderComplete EventHandler="grdForms_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                                IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="FormType" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="ResponseCount" Visible="false" />
                            <ComponentArt:GridColumn DataField="LeadCount" Visible="false" />
                            <ComponentArt:GridColumn DataField="ResponseDate" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="XMLString" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="SourceFormId" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="true" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                        <Template>
                            <div class="collection-row grid-item clear-fix">
                                <div class="first-cell">
                                    <p><asp:HyperLink ID="hplFormLink" runat="server" Text='<%# Container.DataItem["Title"]%>' /></p>
                                    <p><%# Container.DataItem["Description"]%></p>
                                </div>
                                <div class="middle-cell">
                                    <%# String.Format("<strong>{0}: </strong>{1}", GUIStrings.LastUpdated, String.Format("{0:d}", Container.DataItem["ModifiedDate"])) %>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row">
                                        <%# String.Format("{0}: {1}", GUIStrings.Type, Container.DataItem["FormType"].ToString() == "0" ? GUIStrings.SubmissionForms : Container.DataItem["FormType"].ToString() == "1"  ? GUIStrings.Poll : GUIStrings.LeadForm) %>
                                    </div>
                                    <div class="child-row">
                                        <%# GetResponseText(Container.DataItem) %>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="PopupTemplate" runat="server">
                        <Template>
                            <div class="modal-grid-row grid-item clear-fix">
                                <div class="first-cell">
                                    <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                        visible="false" value='<%# Container.DataItem["Id"]%>' />
                                    <h4>
                                        <%# Container.DataItem["Title"]%></h4>
                                </div>
                                <div class="last-cell">
                                    <%# String.Format("<strong>{0}</strong> : {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["ModifiedDate"]),
                                         GUIStrings.By, Container.DataItem["ModifiedByFullName"])%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdFormsLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdForms) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</div>
