﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageContent.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.ManageContent" %>
<%@ Register TagPrefix="ucTree" TagName="LetfTree" Src="~/UserControls/Libraries/DirectoryTree.ascx" %>
<script type="text/javascript">
    function gridContent_OnLoad(sender, eventArgs) {
        $("#content_library").gridActions({ objList: gridContent });
    }

    function gridContent_OnRenderComplete(sender, eventArgs) {
        $("#content_library").gridActions("bindControls");
        $("#content_library").iAppsSplitter();
        SetTaxonomyForList();

        if (gridActionsJson.HideActions)
            $(".movable-row").removeClass("movable-row").addClass("disable-move");
    }

    function gridContent_OnCallbackComplete(sender, eventArgs) {
        $("#content_library").gridActions("displayMessage", { complete: function () {
            if (gridActionsJson.ResponseCode == 100)
                CanceliAppsAdminPopup(true);
        }
        });
    }

    // This function call when a tree node selected (calling from Java Script)
    function LoadLibraryGrid() {
        $("#content_library").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function Grid_Callback() {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        selectedItem = gridContent.getItemFromKey(0, selectedItemId);
        var qString;
        switch (gridActionsJson.Action) {

            case "ViewPages":
                doCallback = false;
                OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=7&Id={0}&Name={1}", selectedItemId, selectedItem.getMember("Title").get_value()));
                break;
            case "Archive":
                if (selectedItem.GetMember("Status").Value == 'Active') {
                    var contentId = selectedItemId;
                    associatedPageCount = GetAssociatePageCount(contentId);  // call back method
                    if (associatedPageCount > 0) {
                        qString = stringformat("Mode=Warning&ObjectTypeId=7&Id={0}&Name={1}&ButtonText={2}", selectedItemId, selectedItem.getMember('Title').get_value(), "<%= GUIStrings.Archive %>");
                        OpeniAppsAdminPopup("ViewPagesUsingObject", qString, "ArchiveContent");
                    }
                    else {
                        ArchiveContent('Archive');
                    }
                }
                else {
                    ArchiveContent('MakeActive');
                }
                doCallback = false;
                break;

            case "EditContent":
            case "AddContent":
                doCallback = false;
                qString = 'NodeId=' + gridActionsJson.FolderId;
                if (selectedItemId && gridActionsJson.Action == "EditContent")
                    qString += '&Id=' + selectedItemId;
                OpeniAppsAdminPopup("ManageContentDetails", qString, "RefreshContentLibrary");
                break;
            case "ReImport":
                doCallback = false;
                OpeniAppsAdminPopup("ReimportFromMaster", "ObjectTypeId=7", "SetReImportMasterPage");
                break;
            case "AddToList":
                doCallback = false;
                AddManualListItem(gridContent);
                break;
        }

        if (doCallback) {
            gridContent.set_callbackParameter(JSON.stringify(gridActionsJson));
            gridContent.callback();
        }

    }

    function SetReImportMasterPage() {
        $("#content_library").gridActions("callback", "ReImportFromMaster", { refreshTree: false });
    }

    function ImportFromMaster() {
        if (gridActionsJson.SelectedItems.length > 0) {
            PageMethods.WebMethod_HasManagerAccess('7', gridActionsJson.FolderId, function (result) {
                if (result == "true") {
                    var contentVariantStatus = GetContentVariantStatus(gridActionsJson.SelectedItems)
                    if (contentVariantStatus == "true") {
                        if (window.confirm("<%= JSMessages.ContentReimportAlertMessage %>"))
                            $("#content_library").gridActions("callback", "ImportFromMaster");
                        else
                            doCallback = false;
                    }
                    else
                        $("#content_library").gridActions("callback", "ImportFromMaster");
                }
                else {
                    alert("<%= JSMessages.NoImportAccessonDirectory %>");
                }
            });
        }
        else
            alert("<%= JSMessages.PleaseSelectAContentToImport %>");

        return false;
    }

    function GetContentVariantStatus(SelectedItems) {
        var contentVariantStatus;
        $.ajax({
            type: "POST",
            url: "SelectContentLibraryPopup.aspx/IsContentVariantExistWebMethod",
            data: "{'contentIds':'" + JSON.stringify(SelectedItems) + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "text",
            async: false,
            success: function (msg) {
                var contentVariant = eval('(' + msg + ')');
                contentVariantStatus = contentVariant.d.IsContentVariantExist;
            }
        })
        return contentVariantStatus;
    }
    function RefreshContentLibrary() {
        if ((popupActionsJson.Action == "Publish" || popupActionsJson.Action == "SubmitIntoWorkflow" || popupActionsJson.Action == "SaveAsDraft") &&
            typeof popupActionsJson.CustomAttributes["PageUrl"] != "undefined")
            JumpToFrontPage(popupActionsJson.CustomAttributes["PageUrl"]) == false;
        else
            $("#content_library").gridActions("callback", { refreshTree: true });
    }

    function CloseMoveCopyContent() {
        gridActionsJson.FolderId = gridActionsJson.CustomAttributes["MoveCopyNodeId"];
        gridActionsJson.PageNumber = 1;
        RefreshContentLibrary();
    }

    function ArchiveContent(operationType) {
        var selectedItemId = gridActionsJson.SelectedItems[0];
        gridActionsJson.Action = operationType;
        if (operationType == 'archive')
            alert("<%= JSMessages.ThisContentHasBeenSuccessfullyArchived %>");

        $("#content_library").gridActions("callback", 'ArchiveContent');
    }

    function GridItem_ShowMenu(menu) {
        if (gridActionsJson.SelectedItems.length > 0) {
            selectedItem = gridContent.getItemFromKey(0, gridActionsJson.SelectedItems.first());
            if (selectedItem.getMember("Status").get_value() == 'Active')
                menu.getItemByCommand("Archive").setText("<%= JSMessages.Archive %>");
            else
                menu.getItemByCommand("Archive").setText("<%= JSMessages.MakeActive %>");
        }
    }
    function Grid_ItemSelect(sender, eventArgs) {

        if (eventArgs.selectedItem.getMember("SourceContentId").get_value() == EmptyGuid)
            sender.getItemByCommand("ReImport").hideButton();
        else
            sender.getItemByCommand("ReImport").showButton();

    }

    function grdTreeContentLibrary_OnExternalDrop(sender, eventArgs) {
        var targetObjectName = eventArgs.get_targetControl().getProperty("GlobalAlias");
        if (targetObjectName == "tvDirectory") {
            var fromMenuId = gridActionsJson.FolderId;
            var toMenuId = eventArgs.get_target().get_id();

            var disabled = GetCurrentProductName() == "marketier" && !IsMarketierDirectory(eventArgs.get_target());
            if (!disabled) {
                var dragContentId = eventArgs.get_item().getMember("Id").get_value();
                gridActionsJson.CustomAttributes["MoveCopyNodeId"] = toMenuId;

                if (dragContentId != null) {
                    if (fromMenuId != toMenuId) {
                        var targetNode = tvDirectory.findNodeById(toMenuId);
                        var targetDepth = targetNode.get_depth();
                        if (targetDepth == 0) {
                            alert("<%= JSMessages.MovingAContentToTheRootMenuIsNotAllowed %>");
                        }
                        else {
                            OpeniAppsAdminPopup("MoveCopyContent", "ContentId=" + dragContentId + "&FromMenuId=" + fromMenuId + "&ToMenuId=" + toMenuId, "CloseMoveCopyContent")
                        }
                    }
                }
            }
        }
        if (targetObjectName == "gridContent") {
            if (gridActionsJson.SortBy == null || gridActionsJson.SortBy == "OrderNo") {
                var draggedItem = eventArgs.get_item();
                var fromContentId = null;
                var toContentId = null;
                if (draggedItem != null) {
                    fromContentId = draggedItem.getMember('Id').get_value();
                }
                var targetItem = eventArgs.get_target();
                if (targetItem != null) {
                    toContentId = targetItem.getMember('Id').get_value();
                }
                if (toContentId != null && fromContentId != null) {
                    gridActionsJson.CustomAttributes.FromContentId = fromContentId;
                    gridActionsJson.CustomAttributes.ToContentId = toContentId;

                    $("#content_library").gridActions("callback", "ChangeDisplayOrder");
                }
            }
            else {
                alert("<%= JSMessages.ChangeSortToDisplayOrderBeforeReorder %>");
            }
        }
    }

    function SelectMenuFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        popupActionsJson.CustomAttributes["SelectedFolderId"] = treeActionsJson.FolderId;
        popupActionsJson.CustomAttributes["DirHierarchypath"] = treeActionsJson.SelectedPath;

        SelectiAppsAdminPopup();
        return false;
    }

    function SelectContentFromLibrary() {
        var item = $("#content_library").gridActions("getSelectedItem");
        if (item) {
            var xsltArgList = item.getMember("XsltArgList").get_text();
            var contentId = item.GetMember("Id").get_text();
            var contentTypeId = item.getMember("ObjectTypeId").get_text();

            $.ajax({
                type: "POST",
                url: "SelectContentLibraryPopup.aspx/GetContentDetials",
                data: "{'contentId':'" + contentId + "', 'contentTypeId':'" + contentTypeId + "'," + "'xsltArgList':'" + xsltArgList + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    popupActionsJson.CustomAttributes["ContentXml"] = msg.d.contentXmlString;
                    popupActionsJson.CustomAttributes["ContentHtml"] = msg.d.contentHtml;
                    popupActionsJson.CustomAttributes["ContentSiteId"] = msg.d.contentSiteId;
                    popupActionsJson.CustomAttributes["ContentTemplate"] = item.GetMember("TemplateId").get_text();
                    popupActionsJson.CustomAttributes["ContentTitle"] = item.GetMember("Title").get_text();
                    popupActionsJson.CustomAttributes["SelectedTitle"] = item.GetMember("Title").get_text();

                    SelectiAppsAdminPopup();
                }
            });
        }
        else {
            alert("<%= JSMessages.PleaseSelectAContent %>");
        }
        return false;
    }

</script>
<div id="content_library" class="clear-fix">
    <div class="left-control">
        <ucTree:LetfTree ID="libraryTree" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <div class="grid-section">
            <ComponentArt:Grid ID="gridContent" SkinID="Default" runat="server" Width="100%"
                ShowFooter="false" RunningMode="Callback" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="gridContentDragTemplate"
                ExternalDropTargets="tvDirectory,gridContent" LoadingPanelClientTemplateId="gridContentLoadingPanelTemplate"
                ManualPaging="true" CssClass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="gridContent_OnLoad" />
                    <CallbackComplete EventHandler="gridContent_OnCallbackComplete" />
                    <ItemExternalDrop EventHandler="grdTreeContentLibrary_OnExternalDrop" />
                    <RenderComplete EventHandler="gridContent_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <ConditionalFormats>
                            <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('StatusId').Value==7"
                                RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                        </ConditionalFormats>
                        <Columns>
                            <ComponentArt:GridColumn IsSearchable="false" DataField="OrderNo" DataCellServerTemplateId="detailsTemplate" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="true" DataField="Title" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="true" DataField="Description" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="ObjectTypeName" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="Status" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="CreatedByFullName" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="ModifiedDate" Visible="false"
                                FormatString="d" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="ModifiedByFullName" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="ObjectTypeId" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="XsltArgList" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="TemplateId" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="StatusId" Visible="false" />
                            <ComponentArt:GridColumn IsSearchable="false" DataField="SourceContentId" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                        <Template>
                            <div class="collection-row grid-item clear-fix movable-row" objectid='<%# Container.DataItem["Id"]%>'>
                                <%--<div class="first-cell">
                                    <p>
                                        <%# Container.DataItem["OrderNo"]%></p>
                                </div>--%>
                                <div class="large-cell">
                                    <h4>
                                        <%# Container.DataItem["Title"]%>
                                    </h4>
                                    <p>
                                        <%# Container.DataItem["Description"]%>
                                    </p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong> : {1} ", GUIStrings.Type, Container.DataItem["ObjectTypeName"])%>
                                    </div>
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong> : {1} {2} {3}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["ModifiedDate"]),
                                                GUIStrings.By, Container.DataItem["ModifiedByFullName"] != string.Empty ? Container.DataItem["ModifiedByFullName"] : Container.DataItem["CreatedByFullName"])%>
                                    </div>
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong>: {1} | <a class='view-tags' objectid='{2}'>{3}</a>",
                                            GUIStrings.Status, Container.DataItem["Status"], Container.DataItem["Id"], GUIStrings.Tags)%>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="PopupTemplate">
                        <Template>
                            <div class="modal-grid-row grid-item clear-fix">
                                <div class="first-cell">
                                    <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                        visible="false" value='<%# Container.DataItem["Id"]%>' />
                                    <h4>
                                        <%# Container.DataItem["Title"]%></h4>
                                </div>
                                <div class="last-cell">
                                    <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.Type, Container.DataItem["ObjectTypeName"])%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="gridContentDragTemplate">
                        ## GetGridDragTemplateById(gridContent, DataItem) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="gridContentLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(gridContent) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</div>
