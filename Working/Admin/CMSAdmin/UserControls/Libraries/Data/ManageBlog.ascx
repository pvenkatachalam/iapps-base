﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageBlog.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ManageBlog" %>
<%@ Register TagPrefix="UC" TagName="LeftTree" Src="~/UserControls/Libraries/Data/BlogTree.ascx" %>
<script type="text/javascript">

    function SelectBlogFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        popupActionsJson.CustomAttributes["SelectedFolderId"] = treeActionsJson.FolderId;
        popupActionsJson.CustomAttributes["DirHierarchypath"] = treeActionsJson.SelectedPath;

        SelectiAppsAdminPopup();

        return false;
    }

    function gridBlog_OnLoad(sender, eventArgs) {
        $("#blog_library").gridActions({ objList: gridBlog });
    }

    function gridBlog_OnRenderComplete(sender, eventArgs) {
        $("#blog_library").gridActions("bindControls");

        $(".view-tags").iAppsClickMenu({
            width: "150px",
            heading: "<%= GUIStrings.Categories %>",
            enableLoadingPanel: true,
            create: function (e, menu) {
                var categories = gridBlog.getItemFromKey(0, e.attr("objectId")).getMember("Categories").get_value();
                if (categories.length > 0) {
                    var arrCategories = categories.split(',');
                    $.each(arrCategories, function () {
                        menu.addListItem(this.toString());
                    });
                }
                else {
                    menu.addListItem("<%= GUIStrings.NoCategoriesForPost %>");
                 }
             }
        });

         $("#blog_library").iAppsSplitter();
     }

     function LoadBlogGrid() {
         $("#blog_library").gridActions("selectNode", treeActionsJson.FolderId);
     }

     function gridBlog_onCallbackComplete(sender, eventArgs) {
         $("#blog_library").gridActions("displayMessage");
     }

     function GetRightSectionTitle() {
         SetCustomAttributes();

         var $selectedOption = $("#blog_library").find(".selected[commandName='Redirect']");
         if ($selectedOption.length > 0) {
             return stringformat("{0} - {1}", gridActionsJson.CustomAttributes["BlogTitle"],
                 $("#blog_library").find(".selected[commandName='Redirect']").html());
         }
         else {
             return gridActionsJson.CustomAttributes["BlogTitle"];
         }
     }

     function GridItem_ShowMenu(sender, eventArgs) {
         if (eventArgs.selectedItem) {
             var noOfComments = eventArgs.selectedItem.getMember("NoOfComments").get_value();
             if (noOfComments == "0")
                 eventArgs.menu.getItemByCommand("ViewComments").hide();
             else
                 eventArgs.menu.getItemByCommand("ViewComments").show();

             var statusId = eventArgs.selectedItem.getMember("StatusId").get_value();
             eventArgs.menu.getItemByCommand("MakeSticky").hide();
             eventArgs.menu.getItemByCommand("MakeNonSticky").hide();
             eventArgs.menu.getItemByCommand("MakeArchive").hide();
             eventArgs.menu.getItemByCommand("MakeActive").hide();
             eventArgs.menu.getItemByCommand("JumpToPost").hide();

             if (statusId == 1 || statusId == 8) {
                 eventArgs.menu.getItemByCommand("JumpToPost").show();
                 eventArgs.menu.getItemByCommand("MakeArchive").show();

                 if (eventArgs.selectedItem.getMember("IsSticky").get_value())
                     eventArgs.menu.getItemByCommand("MakeNonSticky").show();
                 else
                     eventArgs.menu.getItemByCommand("MakeSticky").show();
             }
             else if (statusId == 6) {
                 eventArgs.menu.getItemByCommand("MakeActive").show();
             }
         }
     }

     function Grid_Callback(sender, eventArgs) {
         var doCallback = true;
         var selectedItemId = "";
         if (gridActionsJson.SelectedItems.length > 0)
             selectedItemId = gridActionsJson.SelectedItems[0];

         SetCustomAttributes();
         switch (gridActionsJson.Action) {
             case "JumpToPost":
                 doCallback = false;
                 var postUrl = eventArgs.selectedItem.getMember("PostUrl").get_value();
                 if (postUrl == "")
                     alert("<%= JSMessages.NoMenuIsAssignedToTheSelectedPost %>");
                 else
                     JumpToFrontPage(postUrl);
                 break;
             case "AddPost":
                 doCallback = false;
                 AddEditBlogPost(gridActionsJson.CustomAttributes["BlogTitle"], gridActionsJson.CustomAttributes["BlogId"], null);
                 break;
             case "EditPost":
                 doCallback = false;
                 AddEditBlogPost(gridActionsJson.CustomAttributes["BlogTitle"], gridActionsJson.CustomAttributes["BlogId"], selectedItemId);
                 break;
             case "ViewComments":
                 doCallback = false;
                 OpenCommentsPopup(selectedItemId);
                 break;
             case "DeletePost":
                 doCallback = confirm("<%= JSMessages.AreYouSureYouWantToDeleteTheSelectedPost %>");
                    break;
                case "ViewHistory":
                    doCallback = false;
                    window.location = "ManagePostHistory.aspx?PostId=" + selectedItemId;
                    break;
                case "Redirect":
                    doCallback = false;
                    window.location = stringformat("{0}?BlogId={1}", gridActionsJson.CustomAttributes["Redirect"], gridActionsJson.CustomAttributes["BlogId"]);
                    break;
            }
            if (doCallback) {
                gridBlog.set_callbackParameter(JSON.stringify(gridActionsJson));
                gridBlog.callback();
            }
        }

        function OpenCommentsPopup(selectedId) {
            var selectedTitle = gridBlog.getItemFromKey(0, selectedId).getMember('Title').get_value();
            OpeniAppsAdminPopup("ManageComments", stringformat("ObjectTypeId=40&ObjectId={0}&ObjectTitle={1}", selectedId, selectedTitle));
        }

        function AddEditBlogPost(blogName, blogId, postId) {
            var qString = 'blogName=' + blogName + '&blogId=' + blogId;
            if (postId != null)
                qString += '&postId=' + postId

            OpeniAppsAdminPopup("ManagePostDetails", qString, "CloseAddPostPopup");
        }

        function SetCustomAttributes() {
            if (tvBlogLibrary != null) {
                var selectedNode = tvBlogLibrary.get_selectedNode();
                if (selectedNode) {
                    if (selectedNode.Depth == 3) {
                        gridActionsJson.CustomAttributes["BlogId"] = selectedNode.ParentNode.ParentNode.get_id();
                        gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.ParentNode.ParentNode.get_text();
                    }
                    else if (selectedNode.Depth == 2) {
                        gridActionsJson.CustomAttributes["BlogId"] = selectedNode.ParentNode.get_id();
                        gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.ParentNode.get_text();
                    }
                    else if (selectedNode.Depth == 1) {
                        gridActionsJson.CustomAttributes["BlogId"] = selectedNode.get_id();
                        gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.get_text();
                    }
                    else {
                        gridActionsJson.CustomAttributes["BlogTitle"] = selectedNode.get_text();
                    }
                    gridActionsJson.CustomAttributes["NodeDepth"] = selectedNode.Depth;
                    gridActionsJson.CustomAttributes["NodeTitle"] = selectedNode.get_text();
                }
            }
        }

        function CloseAddPostPopup(operation) {
            $("#blog_library").gridActions("callback");
            RefreshLibraryTree();
        }
</script>

<div id="blog_library" class="clear-fix">
    <asp:PlaceHolder ID="phBlogs" runat="server">
        <div class="left-control">
            <UC:LeftTree ID="libraryTree" runat="server" />
        </div>
        <div class="right-control">
            <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
            <div class="grid-section">
                <ComponentArt:Grid ID="gridBlog" SkinID="default" runat="server" Width="100%" RunningMode="Callback"
                    ShowFooter="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="grdBlogsDragTemplate"
                    LoadingPanelClientTemplateId="grdBlogsLoadingPanelTemplate" CssClass="fat-grid">
                    <ClientEvents>
                        <Load EventHandler="gridBlog_OnLoad" />
                        <CallbackComplete EventHandler="gridBlog_onCallbackComplete" />
                        <RenderComplete EventHandler="gridBlog_OnRenderComplete" />
                    </ClientEvents>
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                            SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                            HoverRowCssClass="hover-row">
                            <ConditionalFormats>
                                <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Status').Value=='Archived'"
                                    RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                            </ConditionalFormats>
                            <Columns>
                                <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                                <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="ShortDescription" Visible="false" />
                                <ComponentArt:GridColumn DataField="PostDate" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="Categories" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="NoOfComments" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="AuthorName" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="Status" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="StatusId" Visible="false" IsSearchable="false" />
                                <ComponentArt:GridColumn DataField="PostUrl" IsSearchable="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="IsSticky" IsSearchable="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ServerTemplates>
                        <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                            <Template>
                                <div class="collection-row grid-item clear-fix">
                                    <div class="large-cell">
                                        <%# string.Format("<h4>{0}</h4><p>{1}</p>",
                                    Container.DataItem["Title"], Container.DataItem["ShortDescription"])%>
                                    </div>
                                    <div class="small-cell">
                                        <div class="child-row">
                                            <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Posted, String.Format("{0:d}", Container.DataItem["PostDate"]), Container.DataItem["AuthorName"])%>
                                        </div>
                                        <div class="child-row">
                                            <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["Status"])%>
                                            <%# Container.DataItem["NoOfComments"].Equals(0) ? string.Empty : string.Format(" | <a class='view-comments' onclick=\"OpenCommentsPopup('{0}');\">{1} ({2})</a>", Container.DataItem["Id"], GUIStrings.Comments, Container.DataItem["NoOfComments"])%>
                                            <%# Container.DataItem["Categories"].Equals(string.Empty) ? string.Empty : string.Format(" | <a class='view-tags' objectid='{0}'>{1}</a>", Container.DataItem["Id"], GUIStrings.Categories) %>
                                            <%# Container.DataItem["IsSticky"].Equals(false) ? string.Empty : string.Format(" | Sticky") %>
                                        </div>
                                    </div>
                                    <div class="clear-fix">
                                    </div>
                                </div>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                    </ServerTemplates>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="grdBlogsDragTemplate">
                            ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdBlogsLoadingPanelTemplate">
                            ## GetGridLoadingPanelContent(gridBlog) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
    </asp:PlaceHolder>
</div>
