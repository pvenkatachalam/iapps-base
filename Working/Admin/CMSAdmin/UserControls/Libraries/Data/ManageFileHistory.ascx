<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ManageFileHistory" 
    CodeBehind="ManageFileHistory.ascx.cs" %>
<script type="text/javascript">
    function grdFileHistory_OnLoad(sender, eventArgs) {
        $("#file_history").gridActions({ objList: grdFileHistory });
    }

    function grdFileHistory_OnRenderComplete(sender, eventArgs) {
        $("#file_history").gridActions("bindControls");
    }

    function grdFileHistory_OnCallbackComplete(sender, eventArgs) {
        $("#file_history").gridActions("displayMessage", { complete: function () {
            if (gridActionsJson.ResponseCode == 100)
                CanceliAppsAdminPopup(true);
        }
        });
    }

    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems[0];
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;
        switch (gridActionsJson.Action) {
            case "Rollback":
                gridActionsJson.CustomAttributes["RollbackToVersion"] = selectedItem.getMember("CurrentVersion").get_value();
                break;
            case "ViewFile":
                OpeniAppsFile(selectedItem.getMember("VersionedFileName").get_value());
                doCallback = false;
                break;
        }

        if (doCallback) {
            grdFileHistory.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdFileHistory.callback();
        }
    }

</script>
<div id="file_history">
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="grdFileHistory" SkinID="Default" runat="server" Width="100%" CssClass="fat-grid"
            RunningMode="Callback" ShowFooter="false" LoadingPanelClientTemplateId="grdFileHistoryLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdFileHistory_OnLoad" />
                <CallbackComplete EventHandler="grdFileHistory_OnCallbackComplete" />
                <RenderComplete EventHandler="grdFileHistory_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="CurrentVersion" DataCellServerTemplateId="PopupTemplate" />
                        <ComponentArt:GridColumn DataField="FileSize" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CreatedDate" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="VersionedFileName" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="PopupTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item clear-fix">
                            <div class="first-cell">
                                <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.VersionNo, Container.DataItem["CurrentVersion"])%>
                            </div>
                            <div class="last-cell">
                                <%# string.Format("<strong>{0}</strong> : {1} | <strong>{2}</strong>: {3} {4} {5}", GUIStrings.Size, Container.DataItem["FileSize"], GUIStrings.Updated,
                                    Container.DataItem["CreatedDate"], GUIStrings.By, Container.DataItem["CreatedByFullName"])%>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdFileHistoryLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdFileHistory) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
    <iAppsControls:GridActions ID="gridActionsBottom" runat="server" />
</div>
