<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ListProperties" 
    CodeBehind="ListProperties.ascx.cs" %>
<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<script type="text/javascript">
    $(function () {
        if ($("#hdnNodeId").val() == "") {
            $("#btnPreview").hide();
            $("#btnReset").hide();
        }
    });

    function InitializeScripts() {
        $("#hplLinkTo").on("click", function () {
            var objectTypeId = $("#hdnObjectType").val();
            var nodeId = $("#hdnNodeId").val();
            if ($("#hdnLinkTo").val() == "1") { //Auto
                
                switch (objectTypeId) {
                    case "7":
                        OpeniAppsAdminPopup("SelectContentLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectDirectory");
                        break;
                    case "8":
                        OpeniAppsAdminPopup("SelectPageLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectDirectory");
                        break;
                    case "9":
                        OpeniAppsAdminPopup("SelectFileLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectDirectory");
                        break;
                    case "33":
                        OpeniAppsAdminPopup("SelectImageLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectDirectory");
                        break;
                    case "39":
                        OpeniAppsAdminPopup("SelectBlogLibraryPopup", "SelectDirectory=true&NodeId=" + nodeId, "CloseSelectDirectory");
                        break;

                    case "6":
                        break;
                }
            }
            else { //Manual
                OpenAddListItems();
            }
        });

        $("#imgFromDate, #txtFromPublishDate").on("click", function () {
            var thisDate = new Date();
            if ($("#hdnFromDate").val() != "") {
                thisDate = new Date($("#hdnFromDate").val());
                CalendarFrom.setSelectedDate(thisDate);
            }
            CalendarFrom.show();
        });

        $("#imgToDate, #txtToPublishDate").on("click", function () {
            var thisDate = new Date();
            if ($("#hdnToDate").val() != "") {
                thisDate = new Date($("#hdnToDate").val());
                CalendarTo.setSelectedDate(thisDate);
            }
            CalendarTo.show();
        });

        $("#hplSelectImage").on("click", function () {
            OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectChannelImage");
        });

        $("#hplRemoveImage").on("click", function () {
            $("#hdnChannelImageId").val("");
            $("#hdnChannelImageName").val("");
            $("#lblChannelImage").html("<%= GUIStrings.NoImageSelected %>");
            $("#hplRemoveImage").hide();
        });
    }

    function OnObjectTypeChange(selValue) {
        if (selValue == 6)
            $("#btnSave").show();
    }

    function OnLinkToChange(selValue) {
        if (selValue == 1) { //Auto
            $("#btnPreview").show();
            $("#btnSave").hide();
        }
        else {
            $("#btnPreview").hide();
            $("#btnSave").show();
            $("#btnReset").hide();
            $("#btnPreview").hide();
        }
    }

    function SelectChannelImage() {
        $("#hdnChannelImageId").val(popupActionsJson.SelectedItems.first());
        $("#hdnChannelImageName").val(popupActionsJson.CustomAttributes["ImageTitle"]);
        $("#lblChannelImage").html(popupActionsJson.CustomAttributes["ImageTitle"]);
        $("#hplRemoveImage").show();
    }

    var varUpdated = '<asp:Localize ID="lcUpdated" runat="server" Text="<%$ Resources:GUIStrings, Updated %>" />';
    function FormatListGridDate(date) {
        var modifiedDate = new Date(date);
        return stringformat("<strong>{0}</strong> : {1}", varUpdated, modifiedDate.toDateString());
    }

    function CalendarFrom_Changed(sender, eventArgs) {
        var selectedDate = CalendarFrom.getSelectedDate();

        $("#txtFromPublishDate").val(CalendarFrom.formatDate(selectedDate, shortCultureDateFormat));
        $("#hdnFromDate").val(CalendarFrom.formatDate(selectedDate, calendarDateFormat));
    }

    function CalendarTo_Changed(sender, eventArgs) {
        var selectedDate = CalendarTo.getSelectedDate();

        $("#txtToPublishDate").val(CalendarTo.formatDate(selectedDate, shortCultureDateFormat));
        $("#hdnToDate").val(CalendarTo.formatDate(selectedDate, calendarDateFormat));
    }

    function grdListContent_OnLoad(sender, eventArgs) {
        $(".manage-list-details").gridActions({
            objList: grdListContent,
            fixHeader: false
        });
    }

    function grdListContent_OnRenderComplete(sender, eventArgs) {
        $(".manage-list-details").gridActions("bindControls");
    }

    function grdListContent_onCallbackComplete(sender, eventArgs) {
        $(".manage-list-details").gridActions("displayMessage");
    }

    function Grid_Callback(sender, eventArgs) {
        switch (gridActionsJson.Action) {
            case "AddItems":
                OpenAddListItems();
                break;
            case "Remove":
                grdListContent.deleteItem(eventArgs.selectedItem);
                gridActionsJson.SelectedItems = [];
                break;
        }
    }

    function CloseSelectDirectory() {
        $("#btnPreview").show();
        $("#btnReset").show();


        $("#hdnNodeId").val(popupActionsJson.CustomAttributes["SelectedFolderId"]);
        $("#lblLinkTo").html(popupActionsJson.CustomAttributes["DirHierarchypath"]);
    }

    function OpenAddListItems() {
        RefreshListJson();

        var customAttributes = {};
        customAttributes["ListItemsJson"] = JSON.stringify(listItemsJson);

        var objectTypeId = $("#hdnObjectType").val();
        if (objectTypeId == "6") objectTypeId = $("#ddlComboType").val()
        OpeniAppsAdminPopup("SelectManualListPopup", "ObjectTypeId=" + objectTypeId, "CloseAddListItems", customAttributes);
    }

    function CloseAddListItems() {
        $("#ddlObjectType").prop("disabled", true);
        $("#rbtnLinkTo input[type='radio']").prop("disabled", true);

        listItemsJson = [];
        if (popupActionsJson.CustomAttributes["ListItemsJson"] != "")
            listItemsJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);
       
        grdListContent.selectAll();
        grdListContent.deleteSelected();

        $.each(listItemsJson, function () {
            AddNewListItem(this.Id, this.Title, this.ObjectTypeId, this.ModifiedDate);
        });
    }

    function AddNewListItem(id, title, type, modifiedDate) {
        var rowNumber = grdListContent.RecordCount;
        grdListContent.get_table().addEmptyRow(rowNumber);
        var newRowItem = grdListContent.get_table().getRow(rowNumber);
        grdListContent.edit(newRowItem);
        newRowItem.setValue(0, id);
        newRowItem.setValue(1, title);
        newRowItem.setValue(2, type);
        newRowItem.setValue(3, modifiedDate);
        grdListContent.editComplete();
        grdListContent.render();
    }

    function CheckSelectedNodeId(source, args) {
        var isValid = true;
        if ($("#hdnLinkTo").val() == "1") { //Auto
            if ($("#hdnNodeId").val() == "")
                isValid = false;
        }
        args.IsValid = isValid;
    }

    function RefreshListJson() {
        listItemsJson = [];
        for (var i = 0; i < grdListContent.RecordCount; i++) {
            gItem = grdListContent.get_table().getRow(i);
            listItemsJson.push({ "Id": gItem.Key, "Title": gItem.getMember("Title").get_value(), "ObjectTypeId": gItem.getMember("Type").get_value(), "ModifiedDate": gItem.getMember("ModifiedDate").get_value() });
        }
    }

    function SaveListProperties() {
        if (Page_ClientValidate("valSaveList") == true) {
            if ($("#hdnLinkTo").val() == "2") { //Manual
                RefreshListJson();
                $("#hdnListItemsJson").val(JSON.stringify(listItemsJson));
            }
            return true;
        }
        return false;
    }
    function ValidatePage() {
        if (Page_ClientValidate('valSaveList')) {
            ShowiAppsLoadingPanel();
            return true;
        }
        return false;
    }
</script>
<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
    ValidationGroup="valSaveList" />
<div class="left-column">
    <div class="form-header clear-fix">
        <h3><asp:Localize ID="lcListProperties" runat="server" Text="<%$ Resources:GUIStrings, ListProperties %>" /></h3>
        <asp:Button ID="btnPreview" runat="server" CssClass="button small-button" Text="<%$ Resources:GUIStrings, Preview %>"
            ToolTip="<%$ Resources:GUIStrings, Preview %>" OnClick="btnPreview_Click" ValidationGroup="valSaveList" OnClientClick="return ValidatePage();"
            CausesValidation="true" />
        <asp:Button ID="btnReset" runat="server" CssClass="button small-button show-loading" Text="<%$ Resources:GUIStrings, Reset %>"
            ToolTip="<%$ Resources:GUIStrings, Reset %>" OnClick="btnReset_Click" Visible="false" />
    </div>
    <asp:UpdatePanel ID="cbLeftColumn" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnTempListId" runat="server" />
            <div class="form-section">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="360" />
                        <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>" Display="None"
                            SetFocusOnError="true" ValidationGroup="valSaveList" />
                        <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheTitle %>"
                            Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" ValidationGroup="valSaveList" />
                    </div>
                </div>
                <asp:PlaceHolder ID="phObjectType" runat="server">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Type %>" /><span class="req">&nbsp;*</span>
                        </label>
                        <div class="form-value">
                            <asp:DropDownList ID="ddlObjectType" runat="server" Width="370" AutoPostBack="true">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, ContentItem %>" runat="server" Value="7"
                                    Selected="True" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Page %>" runat="server" Value="8" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, File %>" runat="server" Value="9" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Image %>" runat="server" Value="33" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Combo %>" runat="server" Value="6" />
                            </asp:DropDownList>
                            <asp:Label ID="lblObjectType" runat="server" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <asp:HiddenField ID="hdnObjectType" runat="server" Value="7" />
                <asp:PlaceHolder ID="phChannelProperties" runat="server" Visible="false">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, URL %>" /><span class="req">&nbsp;*</span>
                        </label>
                        <div class="form-value">
                            <asp:TextBox ID="txtChannelUrl" runat="server" CssClass="textBoxes" Width="360" />
                            <asp:RequiredFieldValidator ID="reqtxtChannelUrl" ControlToValidate="txtChannelUrl"
                                runat="server" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheURL %>"
                                Display="None" SetFocusOnError="true" ValidationGroup="valSaveList" />
                            <asp:RegularExpressionValidator ID="regtxtChannelUrl" ControlToValidate="txtChannelUrl"
                                runat="server" ErrorMessage="<%$ Resources:JSMessages, TheURLFormatIsInvalid %>"
                                Display="None" SetFocusOnError="true" ValidationGroup="valSaveList" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, IconColon %>" />
                        </label>
                        <div class="form-value">
                            <div class="multi-column clear-fix">
                                <asp:Label ID="lblChannelImage" CssClass="no-wrap" runat="server" Text="<%$ Resources:GUIStrings, NoImageSelected %>" />
                                <asp:HiddenField ID="hdnChannelImageId" runat="server" />
                                <asp:HiddenField ID="hdnChannelImageName" runat="server" />
                                <asp:HyperLink ID="hplSelectImage" runat="server" Text="<%$ Resources:GUIStrings, SelectImage %>" />
                                <asp:HyperLink ID="hplRemoveImage" runat="server" Text="<%$ Resources:GUIStrings, Remove %>" />
                            </div>
                            <asp:Label ID="lblIconMessage" runat="server" CssClass="coaching-text" Text="<%$ Resources:GUIStrings, MaximumSizeIs144X400Pixels %>" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, AdminEmailColon %>" /><span class="req">&nbsp;*</span>
                        </label>
                        <div class="form-value">
                            <asp:TextBox ID="txtAdminEmail" runat="server" CssClass="textBoxes" Width="360" />
                            <asp:RequiredFieldValidator ID="reqtxtAdminEmail" ControlToValidate="txtAdminEmail"
                                runat="server" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheAdminEmailAddress %>"
                                Display="None" SetFocusOnError="true" ValidationGroup="valSaveList" />
                            <asp:RegularExpressionValidator ID="regtxtAdminEmail" ControlToValidate="txtAdminEmail"
                                runat="server" ErrorMessage="<%$ Resources:JSMessages, TheAdminEmailAddressIsInvalid %>"
                                Display="None" SetFocusOnError="true" ValidationGroup="valSaveList" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                        </div>
                    </div>
                </asp:PlaceHolder>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" TextMode="MultiLine"
                            Width="360" />
                        <asp:RegularExpressionValidator ID="reqtxtDescription" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheDescription %>"
                            Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" ValidationGroup="valSaveList" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, LinkTo %>" /><span class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:RadioButtonList ID="rbtnLinkTo" runat="server" RepeatDirection="Horizontal"
                            CssClass="radio-list" AutoPostBack="true">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, Directory %>" Value="1" Selected="True" />
                            <asp:ListItem Text="<%$ Resources:GUIStrings, BuildManually %>" Value="2" />
                        </asp:RadioButtonList>
                        <div class="multi-column">
                            <asp:DropDownList ID="ddlComboType" runat="server" Width="200" Visible="false">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, ContentItem %>" runat="server" Value="7"
                                    Selected="True" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Page %>" runat="server" Value="8" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, File %>" runat="server" Value="9" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Image %>" runat="server" Value="33" />
                            </asp:DropDownList>
                            <asp:Label ID="lblLinkTo" runat="server" />
                            <asp:HyperLink ID="hplLinkTo" runat="server" Text="<%$ Resources:GUIStrings, SelectDirectory %>" />
                            <asp:HiddenField ID="hdnNodeId" runat="server" />
                            <asp:HiddenField ID="hdnLinkTo" runat="server" Value="1" />
                            <asp:CustomValidator ID="cvhdnNodeId" runat="server" ControlToValidate="rbtnLinkTo"
                                ErrorMessage="<%$ Resources:JSMessages, PleaseSelectTheDirectory %>" Display="None"
                                ClientValidationFunction="CheckSelectedNodeId" ValidationGroup="valSaveList" />
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, MaxItemsToShow %>" />
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtMaxItems" runat="server" CssClass="textBoxes" Width="100" />
                        <asp:CompareValidator ID="cvtxtMaxItems" runat="server" ControlToValidate="txtMaxItems"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidNumber %>" Display="None"
                            SetFocusOnError="true" Type="Integer" Operator="GreaterThan" ValueToCompare="0"
                            ValidationGroup="valSaveList" />
                    </div>
                </div>
                <asp:PlaceHolder ID="phOrderBy" runat="server">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, OrderBy %>" />
                        </label>
                        <div class="form-value multi-column">
                            <asp:DropDownList ID="ddlOrderBy" runat="server" Width="150">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, None %>" runat="server" Value="0" Selected="True" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Title %>" runat="server" Value="Title" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Description %>" runat="server" Value="Description" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, CreatedDate %>" runat="server" Value="CreatedDate" />
                            </asp:DropDownList>
                            <asp:RadioButtonList runat="server" RepeatDirection="horizontal" ID="rbtnOrderDir"
                                CssClass="radio-list">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Ascending %>" runat="server" Value="1"
                                    Selected="True" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, Descending %>" runat="server" Value="2" />
                            </asp:RadioButtonList>
                        </div>
                    </div>
                </asp:PlaceHolder>
            </div>
            <asp:PlaceHolder ID="phFilter" runat="server">
                <div class="form-section">
                    <div class="form-header clear-fix">
                        <h3><asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, Filter %>" /></h3>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, ShowItemsIndexedAs %>" />
                        </label>
                        <div class="form-value">
                            <UC:AssignTags ID="assignTags" runat="server" TreeHeight="200" TreeWidth="360" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, PublishDate %>" />
                        </label>
                        <div class="form-value date-range multi-column">
                            <asp:RadioButton ID="rbtnPublishBetween" Text="<%$ Resources:GUIStrings, From %>"
                                runat="server" GroupName="PublishDate" />
                            <div class="calendar-value clear-fix">
                                <asp:TextBox ID="txtFromPublishDate" AutoCompleteType="none" runat="server" Width="80"
                                    CssClass="textBoxes" />
                                <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, SelectFromDate %>"
                                    ToolTip="<%$ Resources:GUIStrings, SelectFromDate %>" ID="imgFromDate" runat="server"
                                    CssClass="calenderButton" ClientIDMode="Static" />
                            </div>
                            <asp:HiddenField ID="hdnFromDate" runat="server" />
                            <asp:CompareValidator CultureInvariantValues="true" ID="valFromDate" runat="server"
                                Display="None" Type="Date" ControlToValidate="txtFromPublishDate" Operator="DataTypeCheck"
                                ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDateForFromDate %>"
                                ValidationGroup="valSaveList" />
                            <ComponentArt:Calendar runat="server" ID="CalendarFrom" SkinID="Default" ClientIDMode="AutoID" PopUpExpandControlId="imgFromDate">
                                <ClientEvents>
                                    <SelectionChanged EventHandler="CalendarFrom_Changed" />
                                </ClientEvents>
                            </ComponentArt:Calendar>
                            <label>
                                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, To %>" /></label>
                            <div class="calendar-value clear-fix">
                                <asp:TextBox ID="txtToPublishDate" AutoCompleteType="none" runat="server" Width="80"
                                    CssClass="textBoxes" />
                                <asp:Image ImageUrl="~/App_Themes/General/images/calendar-button.png" AlternateText="<%$ Resources:GUIStrings, SelectToDate %>"
                                    ToolTip="<%$ Resources:GUIStrings, SelectToDate %>" ID="imgToDate" runat="server" ClientIDMode="Static"
                                    CssClass="calenderButton" />
                            </div>
                            <asp:HiddenField ID="hdnToDate" runat="server" />
                            <asp:CompareValidator CultureInvariantValues="true" ID="valToDate" runat="server"
                                Display="None" Type="Date" ControlToValidate="txtToPublishDate" Operator="DataTypeCheck"
                                ErrorMessage="<%$ Resources:GUIStrings, PleaseEnterAValidDateForToDate %>" ValidationGroup="valSaveList" />
                            <ComponentArt:Calendar runat="server" ID="CalendarTo" SkinID="Default" ClientIDMode="AutoID" PopUpExpandControlId="imgToDate">
                                <ClientEvents>
                                    <SelectionChanged EventHandler="CalendarTo_Changed" />
                                </ClientEvents>
                            </ComponentArt:Calendar>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            &nbsp;</label>
                        <div class="form-value">
                            <asp:RadioButton ID="rbtnPublishOlderThan" Text="<%$ Resources:GUIStrings, RemoveWhenOlderThan %>"
                                runat="server" GroupName="PublishDate" />
                            <asp:DropDownList ID="ddlPublishOlderThan" runat="server" Width="95">
                                <asp:ListItem Text="<%$ Resources:GUIStrings, _30Days %>" runat="server" Value="30" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, _60Days %>" runat="server" Value="60" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, _90Days %>" runat="server" Value="90" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, _6Months %>" runat="server" Value="180" />
                                <asp:ListItem Text="<%$ Resources:GUIStrings, _1Year %>" runat="server" Value="365" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            &nbsp;
                        </label>
                        <div class="form-value">
                            <asp:RadioButton ID="rbtnPublishNone" Text="<%$ Resources:GUIStrings, None %>" runat="server"
                                GroupName="PublishDate" Checked="true" />
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlObjectType" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="clear-fix">&nbsp;</div>
</div>
<asp:Panel ID="pnlRightColumn" runat="server" CssClass="right-column">
    <iAppsControls:GridActions ID="gridActions" runat="server" Position="Top" />
    <div class="grid-section">
        <asp:HiddenField ID="hdnListItemsJson" runat="server" />
        <ComponentArt:Grid ID="grdListContent" SkinID="Default" runat="server" ItemDraggingEnabled="false"
            CssClass="fat-grid" ExternalDropTargets="grdListContent" ItemDraggingClientTemplateId="grdListContentDragTemplate"
            ShowFooter="false" ShowHeader="false" Width="100%" ClientIDMode="AutoID" RunningMode="Client">
            <ClientEvents>
                <Load EventHandler="grdListContent_OnLoad" />
                <CallbackComplete EventHandler="grdListContent_onCallbackComplete" />
                <RenderComplete EventHandler="grdListContent_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Id" DataCellClientTemplateId="listTemplate" TextWrap="true" />
                        <ComponentArt:GridColumn DataField="Title" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Type" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="listTemplate">
                    <div class="modal-grid-row grid-item clear-fix">
                        <div class="first-cell">
                            ## DataItem.getMember('Title').get_text() ##
                        </div>
                        <div class="last-cell">
                            ## FormatListGridDate(DataItem.getMember('ModifiedDate').get_text()) ##
                        </div>
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdListContentDragTemplate">
                    ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Panel>
