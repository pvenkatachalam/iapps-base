﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlogTree.ascx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.BlogTree" %>
<script type="text/javascript">
    $(function () {
        cbTreeRefresh.LoadingPanelClientTemplate = GetTreeLoadingPanelContent(cbTreeRefresh);
    });

    function RefreshLibraryTree() {
        if (tvBlogLibrary != null)
            tvBlogLibrary.dispose();
        cbTreeRefresh.callback(JSON.stringify(treeActionsJson));
    }

    function tvBlogLibrary_onLoad(sender, eventArgs) {
        $(".tree-container").treeActions({ objTree: tvBlogLibrary, pageTree: false, blogTree: true, maxNodeType: 1 });
    }

    function tvBlogLibrary_onSelect(sender, eventArgs) {
        $(".tree-container").treeActions("selectNode", eventArgs);
    }

    function tvBlogLibrary_onBeforeRename(sender, eventArgs) {
        $(".tree-container").treeActions("beforeRename", eventArgs);
    }

    function Tree_Callback(sender, eventArgs) {
        treeActionsJson.Success = true;

        switch (eventArgs.Action) {
            case "DeleteNode":
                var result = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThisBlogWillBeDeletedAreYouSureYouWantToDoThis %>' />");
                if (result == true) {
                    result = DeleteBlogNode(treeActionsJson.FolderId);
                    if (result.toLowerCase() != "true") {
                        alert(result);
                        treeActionsJson.Success = false;
                    }
                }
                break;
            case "UpdateName":
                updateNodeName();
                break;
            case "JumpToBlog":
                var blogUrl = GetBlogUrl(treeActionsJson.FolderId);
                if (blogUrl == "")
                    alert("<%= JSMessages.NoMenuIsAssignedToTheSelectedBlog %>");
                else
                    JumpToFrontPage(blogUrl);
                break;
            case 'EditSettings':
                OpeniAppsAdminPopup("BlogSettings", "BlogId=" + treeActionsJson.FolderId);
                break;
        }
    }

    function Tree_ItemSelect(sender, eventArgs) {
        LoadBlogGrid();
    }

    function updateNodeName() {
        if (treeActionsJson.Action == "AddNode") {
            var message = CreateNode(treeActionsJson.NewText, treeActionsJson.ParentId);

            if (message.length != 36) {
                alert(message);
                treeActionsJson.Success = false;
                return;
            }

            treeActionsJson.FolderId = message;
        }
        else {
            EditNode(treeActionsJson.FolderId, treeActionsJson.NewText);
        }

        var result = ManageBlogNode(treeActionsJson.NewText, treeActionsJson.FolderId, treeActionsJson.Action);
        if (result != "") {
            alert(result);
            treeActionsJson.Success = false;
        }
    }

</script>
<div class="tree-container">
    <div class="tree-header">
        <asp:Literal ID="ltTreeHeading" runat="server" />
        <iAppsControls:TreeActions ID="treeActions" runat="server" TreeName="tvBlogLibrary" />
    </div>
    <ComponentArt:CallBack ID="cbTreeRefresh" runat="server" SkinID="TreeCallback" CssClass="tree-content">
        <Content>
            <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvBlogLibrary"
                runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                <ClientEvents>
                    <Load EventHandler="tvBlogLibrary_onLoad" />
                    <NodeSelect EventHandler="tvBlogLibrary_onSelect" />
                    <NodeBeforeRename EventHandler="tvBlogLibrary_onBeforeRename" />
                </ClientEvents>
                <CustomAttributeMappings>
                    <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
                    <ComponentArt:CustomAttributeMapping From="NumberOfFiles" To="Value" />
                </CustomAttributeMappings>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="TreeNodeTemplate">
                        <div>
                            ## DataItem.GetProperty('Text') ## <span>## DataItem.GetProperty('Value')==0 ? '' :
                                '(' + DataItem.GetProperty('Value') + ')' ##</span>
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:TreeView>
        </Content>
    </ComponentArt:CallBack>
</div>
<asp:XmlDataSource ID="xmlDataSource" runat="server" />
