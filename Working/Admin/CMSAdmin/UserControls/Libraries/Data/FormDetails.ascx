﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormDetails.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.FormDetails" ClientIDMode="Static" %>
<script type="text/javascript">
    $(function () {
        fn_TogglechkAddAsContact();
        fn_ToggleEmailSubmission();
        fn_ToggleWatchName();
        fn_ToggleWatchList();
        fn_ToggleAdvancedOptions();

        $("#fileXslt").iAppsFileUpload({ textboxWidth: "310px", allowedExtensions: "xsl,xslt" });

        $(".form-definition").on("change", "#ddlEmailFields", function () {
            $("#hdnEmailField").val($(this).val());
        });

         $(".form-definition").on("change", "#ddEmailFiledsForESub", function () {
            $("#hdnEmailFiledsForESub").val($(this).val());
        });
    });

    function SelectFormThankyouPage() {
        OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "selectedPageItem");
    }

    function selectedPageItem() {
        document.getElementById("txtThankYouURL").value = popupActionsJson.CustomAttributes["SelectedPageUrl"];
    }

    function fn_ToggleAdvancedOptions(toggleHandler) {
        if ($('#divAdvancedOptions').is(':visible')) {
            $('#divAdvancedOptions').slideUp();
            $(toggleHandler).text('<%= GUIStrings.AdvancedOptions %>');
        }
        else {
            $('#divAdvancedOptions').slideDown();
            $(toggleHandler).text('<%= GUIStrings.HideAdvancedOptions %>');
        }
    }

    function fn_TogglechkAddAsContact() {
        if ($("#chkAddAsContact").prop('checked')) {
            $('#ddlEmailFields').prop('disabled', false);
        }
        else {
            $('#ddlEmailFields').prop('disabled', true);
        }
    }

    function fn_ToggleWatchList() {
        if ($("#chkAddWatchedEvent").prop('checked'))
            $('#ddlWatches').prop('disabled', false);
        else
            $('#ddlWatches').prop('disabled', true);
    }

    function fn_ToggleWatchName() {
        if ($("#ddlWatches").val() == "New")    //do not translate
            $('#txtWatchedEventName').css("visibility", "visible");
        else
            $('#txtWatchedEventName').css("visibility", "hidden");

        $("#hdnWatchedEventId").val($("#ddlWatches").val());
    }

    function fn_ToggleEmailSubmission() {
        if ($("#chkSendEmail").prop('checked')) {
            $("#txtEmailSubject").prop('disabled', false);
            $("#txtEmailTo").prop('disabled', false);
            $("#ddEmailFiledsForESub").prop('disabled', false);
            $(".email-element").removeClass("disabled");
            ValidatorEnable(regtxtEmailTo, true);
        }
        else {
            $("#txtEmailSubject").prop('disabled', true);
            $("#txtEmailTo").prop('disabled', true);
            $("#ddEmailFiledsForESub").prop('disabled', true);
            $(".email-element").addClass("disabled");
            ValidatorEnable(regtxtEmailTo, false);
        }
    }

    function OnUpdateFormField(fields) {
        $("#ddlEmailFields").html("");
        $("#ddEmailFiledsForESub").html("");

        $("#ddlEmailFields").append($('<option>').text("<%= GUIStrings.__Select__ %>"));
        $("#ddEmailFiledsForESub").append($('<option>').text("<%= GUIStrings.__Select__ %>"));
        $.each(fields, function () {
            if (this.Type == "textbox") {
                $("#ddlEmailFields").append($('<option>').attr("value", this.Id).text(this.Title));
                $("#ddEmailFiledsForESub").append($('<option>').attr("value", this.Id).text(this.Title));
            }
        });

        $("#ddlEmailFields").val($("#hdnEmailField").val());
        $("#ddEmailFiledsForESub").val($("#hdnEmailFiledsForESub").val());
    }

    function ValidatePage() {
        if (Page_ClientValidate() == true) {
            //$("#txtFormDescription").val($("#txtFormDescription").val().replace(/</g, "&lt;"));
            $("#txtSubmitButtonText").val($("#txtSubmitButtonText").val().replace(/</g, "&lt;"));

            if ($("#chkAddWatchedEvent").prop('checked') && $("#ddlWatches").val() == "New") {
                var newWatchName = $.trim($("#txtWatchedEventName").val());
                if (newWatchName == "") {
                    alert($("#hdnEnterWatchMessage").val());
                    return false;
                }
                else {
                    var watchExists = false;
                    $("#ddlWatches > option").each(function () {
                        if ($(this).html().toLowerCase() == newWatchName.toLowerCase()) {
                            watchExists = true;
                        }
                    });

                    if (watchExists) {
                        alert("<%= GUIStrings.WatchAlreadyExists %>");
                        return false;
                    }
                }
            }

            $("#hdnEmailField").val($("#ddlEmailFields").val());
            $("#hdnSelectedFormNode").val(treeActionsJson.FolderId);
            ShowiAppsLoadingPanel();
            return true;
        }

        return false;
    }
</script>
<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
<div class="form-row">
    <asp:Label ID="lblFormTitle" runat="server" AssociatedControlID="txtFormTitle" CssClass="form-label">
        <asp:Localize ID="Localize4" runat="server" Text="Form Name" /><span class="req">&nbsp;*</span>
    </asp:Label>
    <div class="form-value">
        <asp:TextBox ID="txtFormTitle" runat="server" CssClass="textBoxes" Width="245" />
        <asp:RequiredFieldValidator ID="reqtxtFormTitle" ControlToValidate="txtFormTitle"
            runat="server" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>"
            Display="None" SetFocusOnError="true" />
        <asp:HiddenField ID="hdnFormId" runat="server" />
        <asp:HiddenField ID="hdnFolderId" runat="server" />
    </div>
</div>
<div id="divAdvancedOptions" style="display: none;">
    <asp:PlaceHolder ID="phXSLT" runat="server">
        <div class="form-row">
            <asp:Label ID="lblFileXslt" runat="server" AssociatedControlID="fileXslt" CssClass="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, XSLTFileName %>" /></asp:Label>
            <div class="form-value">
                <asp:FileUpload ID="fileXslt" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                    CssClass="fileUpload" oncontextmenu="return false;" Width="245" />
                <asp:Label ID="lblXsltFileName" runat="server" CssClass="coaching-text" />
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-row">
        <asp:Label ID="lblSubmitButtonText" runat="server" AssociatedControlID="txtSubmitButtonText" CssClass="form-label">
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, SubmitButtonText %>" /></asp:Label>
        <div class="form-value">
            <asp:TextBox ID="txtSubmitButtonText" CssClass="textBoxes" runat="server" Width="245" placeholder="<%$ Resources:GUIStrings, FormSubmissionSubmitButtonHelp %>" />
        </div>
    </div>
    <asp:PlaceHolder ID="phFormFields" runat="server">
        <div class="form-row">
            <asp:Label ID="lblThankYouURL" runat="server" AssociatedControlID="txtThankYouURL" CssClass="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, ThankYouURL %>" />
                (<a href="javascript://" onclick="SelectFormThankyouPage();"><%= GUIStrings.Browse %></a>)
            </asp:Label>
            <div class="form-value">
                <asp:TextBox ID="txtThankYouURL" CssClass="textBoxes" runat="server" Width="245" placeholder="<%$ Resources:GUIStrings, FormSubmissionThankyouURLHelp %>" />
            </div>
        </div>
        <div class="form-row">
            <asp:CheckBox ID="chkSendEmail" runat="server" OnClick="fn_ToggleEmailSubmission()" />
            <asp:Label ID="lblSendEmail" runat="server" AssociatedControlID="chkSendEmail">
                <strong>
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, EmailSubmissionsQuestion %>" /></strong></asp:Label>
        </div>
        <div class="form-row">
            <asp:Label ID="lblEmailTo" runat="server" AssociatedControlID="txtEmailTo" CssClass="form-label">
                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, EmailSubmissionsTo %>" /></asp:Label>
            <div class="form-value">
                <asp:TextBox ID="txtEmailTo" CssClass="textBoxes email-element" runat="server" Width="245" placeholder="<%$ Resources:GUIStrings, FormSubmissionEmailHelp %>" /><br />
                <asp:Label ID="Localize8" CssClass="coaching-text" runat="server" Text="<%$ Resources:GUIStrings, CommaSeparateMultipleAddresses %>" />
                <asp:RegularExpressionValidator ID="regtxtEmailTo" runat="server" ControlToValidate="txtEmailTo"
                    Display="None" ErrorMessage="<%$ Resources:JSMessages, TheEmailAddressIsInvalid %>"
                    ValidationExpression="^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([,])*)*$"></asp:RegularExpressionValidator>
            </div>
        </div>
        <asp:PlaceHolder ID="phEmail" runat="server">
            <div class="form-row">
                <asp:Label ID="lblEmailSubject" runat="server" AssociatedControlID="txtEmailSubject" CssClass="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, EmailSubmissionsSubject %>" /></asp:Label>
                <div class="form-value">
                    <asp:TextBox ID="txtEmailSubject" CssClass="textBoxes email-element" runat="server" Width="245" placeholder="<%$ Resources:GUIStrings, FormSubmissionSubjectHelp %>" />
                    <asp:DropDownList ID="ddEmailFiledsForESub" runat="server" Width="255" Style="margin-top: 5px;" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnEmailFiledsForESub" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row" id="divSubmitterAsContact" runat="server">
                <input id="chkAddAsContact" runat="server" type="checkbox" onclick="fn_TogglechkAddAsContact()" />
                <asp:Label ID="lblAddAsContact" runat="server" AssociatedControlID="chkAddAsContact">
                    <strong>
                        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, AddSubmitterAsContact %>" /></strong>
                </asp:Label>
            </div>
            <asp:Panel ID="pnlContactEmail" runat="server" CssClass="form-row">
                <asp:Label ID="lblEmailFields" runat="server" AssociatedControlID="ddlEmailFields" CssClass="form-label">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, EmailFieldId %>" />
                </asp:Label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlEmailFields" runat="server" Width="255" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnEmailField" runat="server" ClientIDMode="Static" />
                </div>
            </asp:Panel>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phPollFields" runat="server" Visible="false">
        <div class="form-row">
            <asp:Label ID="lblResultType" runat="server" AssociatedControlID="rbResultType" CssClass="form-label">
                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, DisplayResultsAsColon %>" /></asp:Label>
            <div class="form-value">
                <asp:RadioButtonList ID="rbResultType" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, PieChart %>" Value="1" Selected="True" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, BarChart %>" Value="2" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Text %>" Value="3" />
                </asp:RadioButtonList>
            </div>
        </div>
        <div class="form-row">
            <asp:Label ID="lblVotingType" runat="server" AssociatedControlID="rbVotingType" CssClass="form-label">
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, VotingColon %>" /></asp:Label>
            <div class="form-value">
                <asp:RadioButtonList ID="rbVotingType" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, OncePerIP %>" Value="1" Selected="True" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, OncePerVisit %>" Value="2" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Unlimited %>" Value="3" />
                </asp:RadioButtonList>
            </div>
        </div>
    </asp:PlaceHolder>
    <!-- Need to show hide based on analytics license. -->
    <asp:PlaceHolder ID="phGenerateWatch" runat="server">
        <div class="form-row">
            <asp:Label ID="lblAddWatchedEvent" runat="server" AssociatedControlID="chkAddWatchedEvent" CssClass="form-label">
                <input id="chkAddWatchedEvent" runat="server" type="checkbox" onclick="fn_ToggleWatchList();" /><%= GUIStrings.AttachWatch %></asp:Label>
            <div class="form-value">
                <asp:DropDownList ID="ddlWatches" runat="server" Width="255" onchange="fn_ToggleWatchName();">
                    <asp:ListItem Value="Select" Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    <asp:ListItem Value="New" Text="<%$ Resources:GUIStrings, NewWatch %>" />
                </asp:DropDownList>
                <asp:TextBox ID="txtWatchedEventName" runat="server" Width="245" CssClass="textBoxes" Style="margin-top: 5px;" />
                <asp:HiddenField ID="hdnWatchedEventId" runat="server" />
                <asp:HiddenField ID="hdnEnterWatchMessage" runat="server" Value="<%$ Resources:JSMessages, PleaseEnterAWatchName %>" />
            </div>
        </div>
    </asp:PlaceHolder>
</div>
<div class="form-row">
    <a href="javascript://" onclick="fn_ToggleAdvancedOptions(this)"><%= GUIStrings.AdvancedOptions %></a>
</div>
