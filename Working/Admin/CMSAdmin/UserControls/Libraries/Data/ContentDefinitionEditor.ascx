﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ContentDefinitionEditor.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ContentDefinitionEditor" ClientIDMode="Static" %>

<%@ Register Src="~/UserControls/Libraries/RecurrenceControl.ascx" TagPrefix="RecurrenceControl" TagName="Repeat" %>
<!--[if IE]>
    <style type="text/css">
        .editorCell { padding-bottom: 25px; }
    </style>
<![endif]-->

<script type="text/javascript">
    var canFocusInLinkTextBox;
    var selectedPage;
    var selectedFile;
    var selectedPageURL;
    var selectedPageId;
    var selectedFileURL;
    var selectedFileId;
    var selectedImageURL;
    var selectedImageId;
    var selectedImageThumb;
    var SelectedList = new Array(1);
    var selectedListId;
    var selectedListURL;
    var selectedContentId;
    var selectedContentURL;
    var selectedProductId;
    var selectedProductURL;
    var ContentDefinitionPopup = 'true';
    var csSharedPopup = "0";
    var targetFolder;
    var returnTarget;
    var productSearchPopup;
    var jTemplateType = '0';

    function CloseDialogPopup() {
        CanceliAppsAdminPopup();
    }

    function CloseDialog() {
        OpeniAppsAdminPopup("SelectTargetDirectory");
    }

    function SaveRow() {
        if (window.parent.location.pathname.indexOf("/Libraries/Data/ManageContentLibrary.aspx") >= 0 || window.parent.location.pathname.indexOf("/StoreManager/NavigationCategories/NavigationBuilder.aspx") >= 0) {
            window.parent.gridContent_BeforeUpdate();
        }
    }

    function ShowImagePopup(paramImageObjectURL, paramImageObjectId, paramImageObjectThumb) {
        selectedImageURL = paramImageObjectURL;
        selectedImageId = paramImageObjectId;
        selectedImageThumb = paramImageObjectThumb;
        OpeniAppsAdminPopup("SelectImageLibraryPopup", "Resource=Image", "SelectImage");
    }

    function SelectImage() {
        //selectedImageURL.value = GetRelativeUrl(popupActionsJson.CustomAttributes.ImageUrl);
        selectedImageURL.value = popupActionsJson.CustomAttributes.ImageUrl;
        selectedImageId.value = popupActionsJson.SelectedItems.first();
        selectedImageThumb.src = popupActionsJson.CustomAttributes.ThumbUrl;
    }

    function ShowFilePopup(paramFileObjectURL, paramFileObjectId) {
        selectedFileId = paramFileObjectId;
        selectedFileURL = paramFileObjectURL;
        OpeniAppsAdminPopup("SelectFileLibraryPopup", "Resource=File", "SelectFile");
    }

    function SelectFile() {
        //selectedFileURL.value = GetRelativeUrl(popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"]);
        selectedFileURL.value = popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"];
        selectedFileId.value = popupActionsJson.SelectedItems.first();
    }

    function ShowPagePopup(paramPageObjectURL, paramPageObjectId) {
        selectedPageId = paramPageObjectId;
        selectedPageURL = paramPageObjectURL;
        OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
    }

    function SelectPage() {
        //selectedPageURL.value = GetRelativeUrl(popupActionsJson.CustomAttributes["SelectedPageUrl"]);
        selectedPageURL.value = popupActionsJson.CustomAttributes["SelectedPageUrl"];
        selectedPageId.value = popupActionsJson.SelectedItems[0];
    }

    function ShowListPopup(paramListObjectURL, paramListObjectId) {
        OpeniAppsAdminPopup("SelectListPopup", "", "SelectList");
    }

    function SelectList() {
        selectedListId.value = popupActionsJson.SelectedItems.first();
    }

    function ShowContentPopup(paramContentObjectURL, paramContentObjectId) {
        selectedContentId = paramContentObjectId;
        selectedContentURL = paramContentObjectURL;
        OpeniAppsAdminPopup("SelectContentLibraryPopup", "", "SelectContent");
    }

    function SelectContent() {
        selectedContentId.value = popupActionsJson.CustomAttributes["ContentId"];
        selectedContentURL.value = popupActionsJson.CustomAttributes["Title"];
    }

    //List with ProductType
    function ShowProductPopup(paramProductObjectURL, paramProductObjectId) {
        selectedProductURL = paramProductObjectURL;
        selectedProductId = paramProductObjectId;
        OpeniAppsCommercePopup("SelectProductPopup", "showProducts=true");
    }

    function SelectProductFromPopup(product) {
        if (product != null && product != '' && product != 'undefined') {
            selectedProductId.value = product.Id;
            selectedProductURL.value = product.Url;
        }

        CloseiAppsCommercePopup();
    }

    function CloseProductPopupFromPopup() {
        CloseiAppsCommercePopup();

        return false;
    }

    function ShowXmlContentDetail(xmlContentId) {
        var xmlContent;
        if (parent.document.getElementById(xmlContentId) != null) {
            xmlContent = parent.document.getElementById(xmlContentId).value;
            document.getElementById("<%=hdnIsCallback.ClientID%>").value = xmlContent;
            //alert(xmlContent);
            __doPostBack("<%= btnRefreshUpdatePanel.ClientID %>", "");
        }
    }

    function ResetHiddenFieldValueSaveAs() {
        window.parent.NewContent = 1; //save As new Content
        ResetHiddenFieldValue();
    }

    function ResetHiddenFieldValue() {
        document.getElementById("<%=hdnIsCallback.ClientID%>").value = "";
    }

    function GetCDPopupActionsJson() {
        return JSON.parse($("#hdnCDPopupActionsjson").val())
    }
</script>
<div class="content-definition-editor">
    <asp:PlaceHolder runat="server" ID="PlaceHolderXMLForm" />
    <asp:HiddenField ID="hdnIsCallback" runat="server" />
    <asp:HiddenField ID="hdnCDPopupActionsjson" runat="server" />
    <asp:Button ID="btnRefreshUpdatePanel" runat="server" Text="<%$ Resources:GUIStrings, Refresh %>"
        Style="visibility: hidden;" CausesValidation="false" />
    <asp:Label ID="lblXlst" CssClass="formLabels" Text="<%$ Resources:GUIStrings, XsltList %>"
        runat="server" Visible="false" />
    <asp:DropDownList ID="drpXsltList" runat="server" Visible="false" Width="600" Style="float: left; margin-left: 9px;" />
</div>
<asp:PlaceHolder runat="server" ID="phScriptTags" />

