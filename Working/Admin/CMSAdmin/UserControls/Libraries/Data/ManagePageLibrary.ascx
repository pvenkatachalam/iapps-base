<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ManagePageLibrary"
    CodeBehind="ManagePageLibrary.ascx.cs" %>
<%@ Register TagPrefix="PT" TagName="PageTree" Src="~/UserControls/Libraries/Data/PageMapTree.ascx" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<script type="text/javascript">
    function grdPages_OnLoad(sender, eventArgs) {
        $("#page_library").gridActions({ objList: grdPages });

        $(window).load(function () {
            shortcut.add("Ctrl+ALT+U", function () {
                var selectedItem = $("#page_library").gridActions("getSelectedItem");
                if (selectedItem)
                    OpeniAppsShortcut(jPublicSiteUrl + selectedItem.getMember("CompleteFriendlyURL").get_value());
            });
        });
    }

    function grdPages_OnRenderComplete(sender, eventArgs) {
        $("#page_library").gridActions("bindControls");
        $("#page_library").iAppsSplitter();
        SetTaxonomyForList();
    }

    function grdPages_OnCallbackComplete(sender, eventArgs) {
        var pageId = gridActionsJson.SelectedItems.first();
        var action = gridActionsJson.Action;
        $("#page_library").gridActions("displayMessage", {
            complete: function () {
                if ((gridActionsJson.ResponseMessage == null || gridActionsJson.ResponseMessage == "") &&
                    typeof gridActionsJson.CustomAttributes.SubmitToTranslation != 'undefined' &&
                    gridActionsJson.CustomAttributes.SubmitToTranslation == "true") {
                    OpeniAppsAdminPopup("SubmitForTranslation", "PopupMode=0" + (action == "ReImport") ? "&PageId=" + pageId : "");
                }
            }
        });
    }

    function LoadPageGrid() {
        $("#page_library").gridActions("selectNode", treeActionsJson.FolderId);
    }

    function CloseManagePageDetails(operation) {
        if (popupActionsJson.Action == "AddAndJump")
            JumpToFrontPage(popupActionsJson.CustomAttributes["PageUrl"]);
        else if (gridActionsJson.Action == "AddPage")
            $("#page_library").gridActions("callback", { refreshTree: true });
        else
            $("#page_library").gridActions("callback");

    }

    function Grid_Callback(sender, eventArgs) {
        var doCallback = true;
        var selectedItemId = gridActionsJson.SelectedItems.first();
        if (eventArgs)
            selectedItem = eventArgs.selectedItem;
        switch (gridActionsJson.Action) {
            case "ViewHistory":
                doCallback = false;
                window.location = "ManagePageHistory.aspx?PageId=" + selectedItemId;
                break;
            case "ViewPage":
                doCallback = false;
                JumpToFrontPage(selectedItem.getMember("CompleteFriendlyURL").get_value());
                break;
            case "Delete":
                doCallback = false;
                if (IsHomePage(selectedItemId, gridActionsJson.FolderId).toLowerCase() == "true") {
                    ShowiAppsNotification("<%= GUIStrings.PageIsSetAsHomePageForTheSitePleaseSetAnotherPageAs %>", false);
                }
                else {
                    var blogPageName = GetBlogTitleByPage(selectedItemId);
                    if (blogPageName != "") {
                        ShowiAppsNotification(stringformat("<%= JSMessages.ThisPageIsBlogPageCannotDelete %>", blogPageName), false);
                    }
                    else {
                        var pageStatus = IsPageInWFNotPendingGoLive(selectedItemId);
                        if (pageStatus == "true") {
                            OpeniAppsAdminPopup("DeletePageWarning", "PageId=" + selectedItemId, "CloseDeletePagePopup");
                        }
                        else if (pageStatus == "error") {
                            ShowiAppsNotification("<%= JSMessages.ThisPageIsInWorkflowPleaseEitherPublishOrRemoveFromWorkflowBeforeDeleting %>", false);
                        }
                    }
                }
                break;
            case "DisconnectPage":
            case "Archive":
                if (IsHomePage(selectedItemId, gridActionsJson.FolderId).toLowerCase() == "true") {
                    doCallback = false;
                    ShowiAppsNotification("<%= GUIStrings.PageIsSetAsHomePageForTheSitePleaseSetAnotherPageAs %>", false);
                }
                break;
            case "MakeActive":
                doCallback = false;
                if (treeActionsJson.SelectedValue == menuRolesJson.COAdminUser) {
                    OpeniAppsAdminPopup("SubmitToWorkFlow", "SubmitToWorkFlow=true&PageId=" + selectedItemId, "CloseMakeActive");
                }
                else if (treeActionsJson.SelectedValue == menuRolesJson.GlobalPublisher) {
                    OpeniAppsAdminPopup("SubmitToWorkFlow", "PageId=" + selectedItemId, "CloseMakeActive");
                }
                break;
            case "EditPage":
                doCallback = false;
                var queryString = 'NodeId=' + gridActionsJson.FolderId;
                if (selectedItemId)
                    queryString += '&PageId=' + selectedItemId;
                OpeniAppsAdminPopup("ManagePageDetails", queryString, "CloseManagePageDetails");
                break;
            case "AddPage":
                doCallback = false;
                OpeniAppsAdminPopup("ManagePageDetails", "NodeId=" + gridActionsJson.FolderId, "CloseManagePageDetails");
                break;
            case "ReImport":
                doCallback = false;
                OpenReImportPagePopUp(gridActionsJson.FolderId);
                break;
            case "AddToList":
                doCallback = false;
                AddManualListItem(grdPages);
                break;
        }

    if (doCallback) {
        grdPages.set_callbackParameter(JSON.stringify(gridActionsJson));
        grdPages.callback();
    }
}

function CloseDeletePagePopup() {
    $("#page_library").gridActions("callback", popupActionsJson.CustomAttributes["DeleteType"], { refreshTree: true });
}

function CloseMakeActive() {
    $("#page_library").gridActions("callback", { refreshTree: true });
}

function GridItem_ShowMenu(sender, eventArgs) {
    selectedItem = eventArgs.selectedItem;

    eventArgs.menu.getItemByCommand("Archive").show();
    eventArgs.menu.getItemByCommand("Publish").show();
    eventArgs.menu.getItemByCommand("Delete").show();

    eventArgs.menu.getItemByCommand("Archive").setComandArgument("Archive");
    if (treeActionsJson.SelectedValue != menuRolesJson.GlobalPublisher && treeActionsJson.SelectedValue != menuRolesJson.COAdminUser) {
        eventArgs.menu.getItemByCommand("Archive").hide();
    }
    else {
        if (eventArgs.selectedItem.getMember("PageStatusId").get_value() == 8) {
            eventArgs.menu.getItemByCommand("Archive").setText("<%= JSMessages.Archive %>");
                eventArgs.menu.getItemByCommand("Archive").setComandArgument("Archive");
            }
            else if (eventArgs.selectedItem.getMember("PageStatusId").get_value() == 7) {
                eventArgs.menu.getItemByCommand("Archive").setText("<%= JSMessages.MakeActive %>");
                    eventArgs.menu.getItemByCommand("Archive").setComandArgument("MakeActive");
                }
                else {
                    eventArgs.menu.getItemByCommand("Archive").hide();
                }
        }

        if (eventArgs.selectedItem.getMember('IsConnectedPage').get_value()) {
            eventArgs.menu.getItemByCommand("ConnectPage").setText("<%= JSMessages.DisconnectThisPageFromMenuItem %>");
            eventArgs.menu.getItemByCommand("ConnectPage").setComandArgument("DisconnectPage");
        }
        else {
            eventArgs.menu.getItemByCommand("ConnectPage").setText("<%= JSMessages.ConnectThisPageToMenuItem %>");
            eventArgs.menu.getItemByCommand("ConnectPage").setComandArgument("ConnectPage");
        }

        if (treeActionsJson.SelectedValue != menuRolesJson.GlobalPublisher &&
            treeActionsJson.SelectedValue != menuRolesJson.COAdminUser) {
            eventArgs.menu.getItemByCommand("Publish").hide();
            eventArgs.menu.getItemByCommand("Archive").hide();
        }

        if (treeActionsJson.CustomAttributes["IsPublisher"] == "True") {
            eventArgs.menu.getItemByCommand("Publish").show();
        }

        if (selectedItem.getMember("IsInGroupPublish").get_value() == true ||
            eventArgs.selectedItem.getMember("WorkflowStateId").get_value() == 14) {
            eventArgs.menu.getItemByCommand("Publish").hide();
        }

        /*if (IsHomePage(gridActionsJson.SelectedItems.first(), gridActionsJson.FolderId).toLowerCase() == "true") {
        eventArgs.menu.getItemByCommand("Delete").hide();
        eventArgs.menu.getItemByCommand("Archive").hide();
        }*/
    }

    function Grid_ItemSelect(sender, eventArgs) {
        sender.getItemByCommand("ArchivePage").hideButton();
        if (gridActionsJson.HideActions == false) {
            if (treeActionsJson.SelectedValue == menuRolesJson.NavEditor || treeActionsJson.SelectedValue == menuRolesJson.CustomNavEditor)
                sender.find(".grid-actions .grid-item-action").hide();

            if (eventArgs.selectedItem.getMember("SourcePageDefinitionId").get_value() == EmptyGuid)
                sender.getItemByCommand("ReImport").hideButton();
            else
                sender.getItemByCommand("ReImport").showButton();
        }
        else if (treeActionsJson.SelectedValue == menuRolesJson.Archiver) {
            if (eventArgs.selectedItem.getMember("PageStatusId").get_value() == 8)
                sender.getItemByCommand("ArchivePage").showButton();
        }
    }

    function grdPages_OnExternalDrop(sender, eventArgs) {
        var replacedString = /['\\"]/g;
        var dragPageId = eventArgs.get_item().getMember("Id").get_text();
        var dragPageTitle = eventArgs.get_item().getMember("Title").get_text().replace(replacedString, '\\$&');
        var targetControl = eventArgs.get_targetControl();
        var targetControlId;
        if (targetControl) {
            targetControlId = targetControl.GlobalAlias;
        }

        if (targetControlId == 'grdPages') {
            if (gridActionsJson.SortBy == null || gridActionsJson.SortBy == "DisplayOrder") {
                var targetItem = eventArgs.get_target();
                var targetItemId = null;
                if (targetItem != null) {
                    targetItemId = targetItem.getMember("Id").get_text();
                }
                if (targetItemId != null && targetItemId != dragPageId) {
                    gridActionsJson.CustomAttributes.FromPageId = dragPageId;
                    gridActionsJson.CustomAttributes.ToPageId = targetItemId;

                    $("#page_library").gridActions("callback", "ChangeDisplayOrder");
                }
            }
            else {
                ShowiAppsNotification("<%= JSMessages.ChangeSortToDisplayOrderBeforeReorder %>", false);
            }
        }
        else {
            var toMenuId = eventArgs.get_target().get_id();
            if (dragPageId != null && dragPageId.length == 36 && gridActionsJson.FolderId != toMenuId) {
                var targetNode = tvPageLibrary.findNodeById(toMenuId);
                var targetDepth = targetNode.get_depth();
                if (targetDepth == 0) {
                    ShowiAppsNotification("<%= JSMessages.MovingAPageToTheRootMenuIsNotAllowed %>", false);
                }
                else if (targetDepth == 1 && targetNode.get_text() != "Unassigned") {
                    ShowiAppsNotification("<%= JSMessages.MovingPageToTheSelectedTargetMenuIsNotAllowed %>", false);
                }
                else {
                    var queryString = 'PageId=' + dragPageId + "&FromMenuId=" + gridActionsJson.FolderId + "&ToMenuId=" + toMenuId;
                    OpeniAppsAdminPopup("MoveCopyPage", queryString, "MoveOrCopyPage");
                }
        }
    }
}

function OpenReImportPagePopUp(nodeId) {
    gridActionsJson.FolderId = nodeId;
    var pageDefinitionViewsExists = ISPageDefinitionViewsExists(selectedItem.getMember("SourcePageDefinitionId").get_value());
    OpeniAppsAdminPopup("ReimportFromMaster", "PageDefinitionViewsExists=" + pageDefinitionViewsExists, "SetReImportMasterPage");
}

function SetReImportMasterPage() {
    gridActionsJson.CustomAttributes.SourcePageDefinitionId = selectedItem.getMember("SourcePageDefinitionId").get_value();
    gridActionsJson.CustomAttributes.OverWritePage = popupActionsJson.CustomAttributes.OverWritePage;
    gridActionsJson.CustomAttributes.GetPageDefinitionViews = popupActionsJson.CustomAttributes.GetPageDefinitionViews;
    gridActionsJson.CustomAttributes.SubmitToTranslation = popupActionsJson.CustomAttributes.SubmitToTranslation;
    $("#page_library").gridActions("callback", "ReImportPage", { refreshTree: false });
}

function SetImportMasterPages() {
    gridActionsJson.CustomAttributes.GetPageDefinitionViews = popupActionsJson.CustomAttributes.GetPageDefinitionViews;
    gridActionsJson.CustomAttributes.ImportPages = popupActionsJson.CustomAttributes.ImportPages;
    gridActionsJson.CustomAttributes.SubmitToTranslation = popupActionsJson.CustomAttributes.SubmitToTranslation;
    $("#page_library").gridActions("callback", "ImportPages", { refreshTree: true });
}

function MoveOrCopyPage() {
    if (popupActionsJson.CustomAttributes.MoveCopySuccess) {
        gridActionsJson.SelectedItems.clear();
        gridActionsJson.FolderId = popupActionsJson.CustomAttributes.ToMenuId;
        $("#page_library").gridActions("callback", { refreshTree: true });
    }
}

function ImportFromMaster() {
    if (gridActionsJson.SelectedItems.length > 0) {
        var doImport = true;
        if (IsPageVariantExists(JSON.stringify(gridActionsJson.SelectedItems)).toLowerCase() == "true")
            doImport = window.confirm("<%= JSMessages.PageVariantAlreadyExists %>");

        if (doImport) {
            if (typeof GetPageVariationsValue != "undefined")
                popupActionsJson.CustomAttributes.GetPageDefinitionViews = GetPageVariationsValue();
            popupActionsJson.CustomAttributes.ImportPages = gridActionsJson.SelectedItems.join(',');
            SelectiAppsAdminPopup();
        }
    }
    else
        ShowiAppsNotification("<%= JSMessages.PleaseSelectAPageToImport %>", false);


    return false;
}


function ImportFromMasterAndSubmitForTranslation() {
    popupActionsJson.CustomAttributes.SubmitToTranslation = "true";
    ImportFromMaster();

    return false;
}

function SelectPageFromLibrary() {
    var selectedItem = $("#page_library").gridActions("getSelectedItem");
    if (selectedItem) {
        popupActionsJson.CustomAttributes["SelectedPageUrl"] = selectedItem.getMember("CompleteFriendlyURL").get_text();
        popupActionsJson.CustomAttributes["SelectedPageTitle"] = selectedItem.getMember("Title").get_text();
        popupActionsJson.CustomAttributes["SelectedTitle"] = selectedItem.getMember("Title").get_text();
        popupActionsJson.CustomAttributes["SelectedUrl"] = selectedItem.getMember("CompleteFriendlyURL").get_text();
        SelectiAppsAdminPopup();
    }
    else {
        ShowiAppsNotification("<%= JSMessages.PleaseSelectAPage %>", false);
        }

        return false;
    }

    function SelectMenuFromLibrary() {
        popupActionsJson.CustomAttributes["SelectedFolderName"] = treeActionsJson.Text;
        popupActionsJson.CustomAttributes["SelectedFolderId"] = treeActionsJson.FolderId;
        popupActionsJson.CustomAttributes["DirHierarchypath"] = treeActionsJson.SelectedPath;

        SelectiAppsAdminPopup();

        return false;
    }
</script>
<div id="page_library" class="clear-fix">
    <div class="left-control">
        <asp:HiddenField ID="hdnSelectedPagesForImportProcessing" ClientIDMode="Static" runat="server" />
        <PT:PageTree ID="pageTree" runat="server" />
    </div>
    <div class="right-control">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <div class="grid-section">
            <componentart:grid id="grdPages" skinid="Default" runat="server" width="100%" runningmode="Callback"
                itemdraggingenabled="true" showfooter="false" itemdraggingclienttemplateid="grdPagesDragTemplate"
                externaldroptargets="tvPageLibrary,grdPages" loadingpanelclienttemplateid="grdPagesLoadingPanelTemplate"
                manualpaging="true" cssclass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="grdPages_OnLoad" />
                    <CallbackComplete EventHandler="grdPages_OnCallbackComplete" />
                    <ItemExternalDrop EventHandler="grdPages_OnExternalDrop" />
                    <RenderComplete EventHandler="grdPages_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <ConditionalFormats>
                            <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('IsConnectedPage').Value == true"
                                RowCssClass="connected" SelectedRowCssClass="connected" />
                            <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('PageStatus').Value==7"
                                RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                        </ConditionalFormats>
                        <Columns>
                            <ComponentArt:GridColumn DataField="DisplayOrder" DataCellServerTemplateId="DetailsTemplate" />
                            <ComponentArt:GridColumn DataField="Title" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="TemplateName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="PublishDate" Visible="false" />
                            <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" FormatString="d" />
                            <ComponentArt:GridColumn DataField="AuthorName" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="CompleteFriendlyURL" Visible="false" />
                            <ComponentArt:GridColumn DataField="PageStatus" Visible="false" />
                            <ComponentArt:GridColumn DataField="PageStatusId" Visible="false" />
                            <ComponentArt:GridColumn DataField="WorkflowStateId" Visible="false" />
                            <ComponentArt:GridColumn DataField="IsConnectedPage" Visible="false" />
                            <ComponentArt:GridColumn DataField="IsInGroupPublish" Visible="false" />
                            <ComponentArt:GridColumn DataField="SourcePageDefinitionId" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                        <Template>
                            <div class="collection-row grid-item clear-fix movable-row" objectid='<%# Container.DataItem["Id"]%>'>
                                <%-- <div class="first-cell">
                                   <%# Container.DataItem["DisplayOrder"] %>
                                </div>--%>
                                <div class="large-cell">
                                    <h4>
                                        <%# Container.DataItem["Title"]%>
                                        <%# string.Format(Convert.ToBoolean(Container.DataItem["IsConnectedPage"]) == true?
                                    "<img src='/Admin/App_Themes/General/images/linked-object.png' alt='Connected to Menu' title='Connected to Menu' />" : "&nbsp")%></h4>
                                    <p>
                                        <%# Container.DataItem["Description"]%></p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <%# Container.DataItem["TemplateName"].Equals(string.Empty) ? string.Empty : string.Format("<strong>{0}</strong>: {1}", GUIStrings.Template, Container.DataItem["TemplateName"]) %>
                                    </div>
                                    <div class="child-row">
                                        <%# (Convert.ToDateTime(Container.DataItem["PublishDate"]).Equals(new DateTime(1900, 1, 1)) ?
                                            string.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Created, string.Format("{0:d}", Container.DataItem["ModifiedDate"]), GUIStrings.By, Container.DataItem["AuthorName"]) :
                                            string.Format("<strong>{0}</strong>: {1} {2} {3}", GUIStrings.Published, string.Format("{0:d}", Container.DataItem["PublishDate"]), GUIStrings.By, Container.DataItem["AuthorName"])) %>
                                    </div>
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong>: {1} | <a class='view-tags' objectid='{2}'>{3}</a>", 
                                    GUIStrings.Status, Container.DataItem["PageStatus"], Container.DataItem["Id"],  GUIStrings.Tags) %>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                    <ComponentArt:GridServerTemplate ID="PopupTemplate">
                        <Template>
                            <div class="modal-grid-row grid-item clear-fix">
                                <div class="first-cell">
                                    <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                        visible="false" value='<%# Container.DataItem["Id"]%>' />
                                    <h4>
                                        <%# Container.DataItem["Title"]%></h4>
                                </div>
                                <div class="last-cell">
                                    <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Template, Container.DataItem["TemplateName"])%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdPagesDragTemplate">
                        ## GetGridDragTemplateById(grdPages, DataItem) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdPagesLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdPages) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </componentart:grid>
        </div>
    </div>
</div>
