<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.SecurityLevelWithListbox"
    CodeBehind="SecurityLevelWithListbox.ascx.cs" %>
<script type="text/javascript">
    function OpenEditSecLevel() {
        var output = "";
        $(".checkbox-list input:checkbox").each(function (a, b) {
            if (b.checked)
                output += a + ",";
        });

        cbSecurityLevel.set_callbackParameter(output.slice(0, -1));

        OpeniAppsAdminPopup("ManageSecurityLevelDetails", "", "RefreshSecurityLevel");
        return false;
    }

    function RefreshSecurityLevel() {
        cbSecurityLevel.callback();
    }
</script>
<asp:Panel ID="pnlWrapper" runat="server" CssClass="form-row form-edit-row security-level-row">
    <label class="form-label">
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SecurityLevels %>" />
        <asp:PlaceHolder ID="phEditButton" runat="server"><a href="#" onclick="return OpenEditSecLevel();"
            class="button small-button">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, EditSecurityLevels %>" /></a>
        </asp:PlaceHolder>
    </label>
    <asp:Panel ID="pnlSecurityLevel" runat="server" CssClass="form-value checkbox-list">
        <iAppsControls:CallbackPanel ID="cbSecurityLevel" runat="server">
            <ContentTemplate>
                <asp:CheckBoxList ID="chkSecurityLevel" runat="server" />
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </asp:Panel>
</asp:Panel>
