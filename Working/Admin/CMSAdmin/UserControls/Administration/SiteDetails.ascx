<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.SiteDetails"
    CodeBehind="SiteDetails.ascx.cs" ClientIDMode="Static" %>
<%@ Register TagPrefix="UC" TagName="SelectLanaguage" Src="Translation/SelectLanguageCulture.ascx" %>

<script type="text/javascript">
    function toggleTranslateToCheckbox(state) {
        var translateToCheckboxElement = document.getElementById("<%= cbSetAsDefaultLanguage.ClientID %>");

        translateToCheckboxElement.checked = state;
        translateToCheckboxElement.disabled = !state;
    }
    function fn_TogglePageStatus(optionSelected) {
        var objPageStatus = $('#<%= ddlImportPageStatus.ClientID %>');
        switch (optionSelected) {
            case 'none':
            case 'menus':
                $(objPageStatus).prop('disabled', true);
                break;
            case 'pages':
                $(objPageStatus).prop('disabled', false);
                break;
        }
    }

</script>

<asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
    ValidationGroup="valSiteDetails" />
<asp:Panel ID="pnlSiteHierarchy" runat="server" CssClass="form-row" Visible="false">
    <label class="form-label">
        <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, SiteHierarchy %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtSiteHierarchy" runat="server" Width="500" Enabled="false" CssClass="disabled" />
    </div>
</asp:Panel>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, NameColon %>" /><span class="req">&nbsp;*</span></label>
    <div class="form-value">
        <asp:TextBox ID="txtName" runat="server" Width="500" />
        <iAppsControls:iAppsRequiredValidator ID="rvtxtName" ControlToValidate="txtName"
            ErrorMessage="<%$ Resources:JSMessages, ISRequiredName %>"
            runat="server" ValidationGroup="valSiteDetails" />
        <iAppsControls:iAppsCustomValidator ID="cvtxtName" ControlToValidate="txtName"
            ErrorMessage="<%$ Resources: JSMessages, FormatSiteName %>"
            runat="server" ValidateType="Name" ValidationGroup="valSiteDetails" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, PrimaryUrl %>" /><span class="req">&nbsp;*</span><img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= GUIStrings.PrimaryUrlHelp %>" alt="" class="form-help" /><br />
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtPrimaryURL" runat="server" Wrap="true" Width="500" />
        <iAppsControls:iAppsRequiredValidator ID="rvtxtPrimaryURL" ControlToValidate="txtPrimaryURL"
            ErrorMessage="<%$ Resources:JSMessages, ISRequiredURL %>"
            runat="server" ValidationGroup="valSiteDetails" />
        <iAppsControls:iAppsCustomValidator ID="cvtxtPrimaryURL" ControlToValidate="txtPrimaryURL"
            ErrorMessage="<%$ Resources: JSMessages, FormatSiteUrl %>"
            runat="server" ValidateType="AbsoluteUrl" ValidationGroup="valSiteDetails" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, AdditionalUrls %>" /><img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= GUIStrings.SiteURLHelp %>" alt="" class="form-help" /><br />
        <asp:Label ID="Localize5" runat="server" CssClass="coaching-text" Text="<%$ Resources:GUIStrings, SeparateWithCommas %>" />
    </label>
    <div class="form-value">
        <asp:TextBox ID="txtURL" runat="server" Wrap="true" TextMode="MultiLine" Width="500"
            Rows="4" />
        <iAppsControls:iAppsCustomValidator ID="cvtxtURL" ControlToValidate="txtURL"
            ErrorMessage="<%$ Resources: JSMessages, FormatSiteUrls %>"
            runat="server" ValidateType="AbsoluteUrls" ValidationGroup="valSiteDetails" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, DescriptionColon %>" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtDescription" runat="server" Wrap="true" TextMode="MultiLine"
            Rows="4" Width="500" />
        <iAppsControls:iAppsCustomValidator ID="revtxtDescription" ControlToValidate="txtDescription"
            ErrorMessage="<%$ Resources: JSMessages, InvalidInput %>"
            runat="server" ValidateType="Description" ValidationGroup="valSiteDetails" />
    </div>
</div>
<asp:PlaceHolder ID="phLocation" runat="server">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SpecifyDirectoryWhereSiteWillBeLocatedColon %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtUploadLocation" runat="server" Width="500" />
            <iAppsControls:iAppsRequiredValidator ID="rvtxtUploadLocation" ControlToValidate="txtUploadLocation"
                ErrorMessage="<%$ Resources:JSMessages, ISRequiredDirectory %>"
                runat="server" ValidationGroup="valSiteDetails" />
            <iAppsControls:iAppsCustomValidator ID="cvtxtUploadLocation" ControlToValidate="txtUploadLocation"
                ErrorMessage="<%$ Resources: JSMessages, EnterValidDirectory %>"
                runat="server" ValidateType="PhysicalPath" ValidationGroup="valSiteDetails" />
            <iAppsControls:iAppsCustomValidator ID="cvtxtUploadLocationP" ControlToValidate="txtUploadLocation"
                ErrorMessage="<%$ Resources: JSMessages, DirectoryDoesNotHaveWritePermission %>"
                runat="server" ValidateType="WritePermission" ValidationGroup="valSiteDetails" />
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phSiteGroups" runat="server">
    <div class="form-row">
        <label class="form-label">
            <%= Bridgeline.iAPPS.Resources.GUIStrings.Groups %><br />
            <input type="button" id="btnEditGrouping" value="<%= GUIStrings.EditSiteGrouping %>" class="small-button" onclick="OpeniAppsAdminPopup('EditSiteGrouping');" />
        </label>
        <div class="form-value site-groups checkbox-list">
            <asp:CheckBoxList ID="cblSiteGroups" runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
            </asp:CheckBoxList>
        </div>
    </div>
</asp:PlaceHolder>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, EnableTranslation %>" />
    </label>
    <div class="form-value">
        <input type="checkbox" id="cbEnableTranslation" runat="server" onchange="toggleTranslateToCheckbox(this.checked)" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, UICulture %>" /></label>
    <div class="form-value">
        <UC:SelectLanaguage ID="ctlSelectLanguage" runat="server" UseSingleDropdown="false" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, TimeZone %>" /></label>
    <div class="form-value">
        <asp:DropDownList ID="ddlLocalTimeZone" runat="server" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">&nbsp;</label>
    <div class="form-value">
        <asp:CheckBox runat="server" ID="cbSetAsDefaultLanguage" Text="<%$ Resources:GUIStrings, SetAsDefaultTranslateToLanguage %>" />
    </div>
</div>
<asp:PlaceHolder ID="phTheme" runat="server">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, Theme %>" /></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlThemes" runat="server"></asp:DropDownList>
        </div>
    </div>
</asp:PlaceHolder>

<asp:PlaceHolder ID="phVariantOnly" runat="server">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, LoginURL %>" /><img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= GUIStrings.SiteLoginURLHelp %>" alt="" class="form-help" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtLoginURL" runat="server" Width="500" />
            <iAppsControls:iAppsCustomValidator ID="cvtxtLoginURL" ControlToValidate="txtLoginURL"
                ErrorMessage="<%$ Resources: JSMessages, LoginURLShouldBeARelativeAddress %>"
                runat="server" ValidateType="RelativeUrl" ValidationGroup="valSiteDetails" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, DefaultURL %>" /><img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= GUIStrings.SiteDefaultURLHelp %>" alt="" class="form-help" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtDefaultURL" runat="server" Width="500" />
            <iAppsControls:iAppsCustomValidator ID="cvtxtDefaultURL" ControlToValidate="txtDefaultURL"
                ErrorMessage="<%$ Resources: JSMessages, DefaultURLShouldBeARelativeAddress %>"
                runat="server" ValidateType="RelativeUrl" ValidationGroup="valSiteDetails" />
        </div>
    </div>
    <asp:PlaceHolder ID="phImportoptions" runat="server">
        <hr />
        <h3><%= GUIStrings.ImportOptions %></h3>
        <div class="form-row import-options">
            <label class="form-label"><%= GUIStrings.MenusAndPages %></label>
            <div class="form-value">
                <asp:RadioButton ID="rbNoMenus" runat="server" Text="<%$ Resources:GUIStrings, DontImportMenuOrPages %>" GroupName="ImportOptions" onchange="fn_TogglePageStatus('none');" /><br />
                <asp:RadioButton ID="rbImportMenus" runat="server" Text="<%$ Resources:GUIStrings, ImportMenusOnly %>" GroupName="ImportOptions" onchange="fn_TogglePageStatus('menus');" /><br />
                <asp:RadioButton ID="rbImportPages" runat="server" Text="<%$ Resources:GUIStrings, ImportPages %>" GroupName="ImportOptions" onchange="fn_TogglePageStatus('pages');" Checked="true" />
                <asp:DropDownList ID="ddlImportPageStatus" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, ImportPagesAsDraft %>" Value="1" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, ImportPagesWithSameStatusAsMaster %>" Value="2" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row import-options">
            <label class="form-label"><%= GUIStrings.ContentItems %></label>
            <div class="form-value">
                <asp:RadioButton ID="rbNoContentDirectories" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectories %>" GroupName="ImportContentItems" Checked="true" /><br />
                <asp:RadioButton ID="rbImportContentDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportContentItems" />
            </div>
        </div>
        <div class="form-row import-options">
            <label class="form-label"><%= GUIStrings.Images %></label>
            <div class="form-value">
                <asp:RadioButton ID="rbNoImages" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectoriesOrImages %>" GroupName="ImportImages" Checked="true" /><br />
                <asp:RadioButton ID="rbImportImageDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportImages" /><br />
            </div>
        </div>
        <div class="form-row import-options">
            <label class="form-label"><%= GUIStrings.FilesMedia %></label>
            <div class="form-value">
                <asp:RadioButton ID="rbNoFilesAndMedia" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectoriesOrFilesMedia %>" GroupName="ImportFiles" Checked="true" /><br />
                <asp:RadioButton ID="rbImportFileDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportFiles" /><br />
            </div>
        </div>
        <div class="form-row import-options">
            <label class="form-label"><%= GUIStrings.Forms %></label>
            <div class="form-value">
                <asp:RadioButton ID="rbNoForms" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectories %>" GroupName="ImportForms" Checked="true" /><br />
                <asp:RadioButton ID="rbImportFormDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportForms" />
            </div>
        </div>
    </asp:PlaceHolder>
</asp:PlaceHolder>
