<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.DirectoryPermissionByTarget"
    CodeBehind="DirectoryPermissionByTarget.ascx.cs" %>
<%@ Register assembly="ComponentArt.Web.UI" namespace="ComponentArt.Web.UI" tagprefix="ComponentArt" %>
<script type="text/javascript">
    var allViewers = false;
    var allManagers = false;
    var allNone = false;
    var allMasterImports = false;
    var MEMBERID = 0;
    var MEMBERNAME = 1;
    var VIEWER = 2;
    var MANAGER = 3;
    var MASTERIMPORT = 4;
    var NONE = 5;

    function ScrollGridToBegin() {
        var gridId = GetCurrentGridId();
        if (gridId != null) {
            var grid = window[gridId];
            if (grid != null) {
                grid.scrollTo(0);
            }
        }
    }

    function checkAllViewer(obj) {
        if (allViewers) {
            allViewers = false;
            UncheckAllItems(VIEWER);
        }
        else {
            allViewers = true;
            CheckAllItems(VIEWER);
            document.getElementById("chkNone").checked = false;
            allNone = false;
        }
    }

    function checkAllManager(obj) {
        if (allManagers) {
            allManagers = false;
            UncheckAllItems(MANAGER);
        }
        else {
            allManagers = true;
            CheckAllItems(MANAGER);
            UncheckAllItems(NONE);
            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkAllMasterImports(obj) {
        if (allMasterImports) {
            allMasterImports = false;
            UncheckAllItems(MASTERIMPORT);
        }
        else {
            allMasterImports = true;
            CheckAllItems(MASTERIMPORT);
            UncheckAllItems(NONE);
            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkAllNone(obj) {
        //ScrollGridToBegin();
        if (allNone) {
            allNone = false;
            UncheckAllItems(NONE);
        }
        else {
            allNone = true;
            CheckAllItems(NONE);
            UncheckAllItems(VIEWER);
            UncheckAllItems(MANAGER);
            UncheckAllItems(MASTERIMPORT);
            document.getElementById("chkViewer").checked = false;
            document.getElementById("chkManager").checked = false;
            document.getElementById('chkMasterImport').checked = false;

            allViewers = false;
            allManagers = false;
            allMasterImports = false;
        }
    }

    function CheckAllItems(columnno) {
        if (grdPermissions.RecordCount > 0) {
            var rowCount = grdPermissions.Table.GetRowCount();
            for (var i = 0; i < grdPermissions.Table.GetRowCount(); i++) {
                grdPermissions.Table.GetRow(i).SetValue(columnno, true, false);
            }

            grdPermissions.Render();
        }
    }

    function UncheckAllItems(columnno) {
        if (grdPermissions.RecordCount > 0) {
            var rowCount = grdPermissions.Table.GetRowCount();
            for (var i = 0; i < grdPermissions.Table.GetRowCount(); i++) {
                grdPermissions.Table.GetRow(i).SetValue(columnno, false, false);
            }

            grdPermissions.Render();
        }
    }

    function grdPermissions_onItemBeforeCheckChange(sender, eventArgs) {
        var selectedItem = eventArgs.get_item();
        var selectedColumnIndex = eventArgs.get_columnIndex();

        if (selectedColumnIndex == NONE) //If the checked change event happens on the None column.
        {
            if (selectedItem.Data[NONE] == false) //It is about to checked
            {
                document.getElementById("chkViewer").checked = false; //Uncheck the Select all viewer column
                document.getElementById("chkManager").checked = false; //Uncheck the Select all manager column
                document.getElementById('chkMasterImport').checked = false; //Uncheck the Select all masterimport column

                allViewers = false;
                allManagers = false;
                allMasterImports = false;

                selectedItem.SetValue(VIEWER, false, false);
                selectedItem.SetValue(MANAGER, false, false);
                selectedItem.SetValue(MASTERIMPORT, false, false);
            }
            else //None column is going to be unchecked
            {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all manager column
                allNone = false;
            }
            //Uncheck 
        }
        else // If any other column is checked or unchecked
        {
            if (selectedItem.Data[VIEWER] == true) {
                document.getElementById("chkViewer").checked = false; //Uncheck the Select all viewer column
                allViewers = false;

            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all manager column
                allNone = false;
            }

            if (selectedItem.Data[MANAGER] == true) {
                document.getElementById("chkManager").checked = false; //Uncheck the Select all manager column
                allManagers = false;
            }
            else //Any one of the check box other than none is checked, uncheck the select All none check box.
            {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all manager column
                allNone = false;
            }
            if (selectedItem.Data[MASTERIMPORT] == true) {
                document.getElementById("chkMasterImport").checked = false; //Uncheck the Select all manager column
                allMasterImports = false;
            }
            else //Any one of the check box other than none is checked, uncheck the select All none check box.
            {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all manager column
                allNone = false;
            }
            selectedItem.SetValue(NONE, false, false);
        }
    }

    function grdPermissions_onItemCheckChanged(sender, eventArgs) {
        sender.render();
    }

    function grdPermissions_onItemExpand(sender, eventArgs) {
        grdPermissions.render();
    }

    function GetMainHeader() {
        if (gridActionsJson.CustomFilter == "1")
            return "<%= GUIStrings.User%>";
        else
            return "<%= GUIStrings.Group%>";
    }

    var headerFormat = '<div style="padding-bottom:4px;">{0}</div><input type="checkbox" id="{1}" name="group1" {2} onclick="{3}(this);" />';
    var authorColumnTitle = "<%= GUIStrings.Viewer %>";
    var approverColumnTitle = "<%= GUIStrings.Manager %>";
    var MasterImportColumnTitle = "<%= GUIStrings.MasterImport %>";
    var noneColumnTitle = "<%= GUIStrings.ClearAll %>";

</script>
<ComponentArt:Grid ID="grdPermissions" SkinID="Default" RunningMode="Client" Width="850"
    runat="server" Height="260" ShowFooter="false" CallbackCachingEnabled="true"
    CallbackCacheSize="25" CssClass="iapps-grid" AllowVerticalScrolling="true" LoadingPanelClientTemplateId="grdPermissions_loadingPanelTemplate">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="UserId" HeadingCellCssClass="iapps-heading-cell"
            HeadingRowCssClass="iapps-heading-row" HeadingTextCssClass="iapps-heading-text"
            DataCellCssClass="iapps-data-cell" RowCssClass="iapps-row" SelectorCellWidth="1"
            SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false" HeadingCellHoverCssClass="iapps-heading-cell-hover"
            SelectedRowCssClass="iapps-selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
            SortImageWidth="8" SortImageHeight="7" AlternatingRowCssClass="iapps-alternate-row"
            ShowSelectorCells="false" AllowGrouping="false" HoverRowCssClass="iapps-hover-row">
            <Columns>
                <ComponentArt:GridColumn FixedWidth="true" DataField="UserId" Visible="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="UserName" AllowSorting="False"
                    AllowEditing="False" Width="400" AllowReordering="false" HeadingCellClientTemplateId="MainHeaderTemplate" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="13" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, Manager %>"
                    AllowEditing="True" Width="100" Align="Center" HeadingCellClientTemplateId="HeaderTemplateAuthor"
                    AllowReordering="false" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="12" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, Viewer %>"
                    AllowEditing="True" Width="100" Align="Center" HeadingCellClientTemplateId="HeaderTemplateApprover"
                    HeadingCellCssClass="HeadingCell" AllowReordering="false" />
                    <ComponentArt:GridColumn Width="100" AllowEditing="true" DataField="36" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, MasterImport %>" 
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateMasterImport" />
                <ComponentArt:GridColumn FixedWidth="true" DataField="0" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, ClearAll %>"
                    AllowEditing="True" Width="100" Align="Center" HeadingCellClientTemplateId="HeaderTemplateNone"
                    HeadingCellCssClass="HeadingCell" AllowReordering="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="MainHeaderTemplate" runat="server">
            ## GetMainHeader() ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateAuthor" runat="server">
            ## stringformat(headerFormat, authorColumnTitle, 'chkViewer', allViewers ? 'checked':'',
            'checkAllViewer') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateApprover" runat="server">
            ## stringformat(headerFormat, approverColumnTitle, 'chkManager', allManagers ? 'checked':'',
            'checkAllManager') ##
        </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="HeaderTemplateMasterImport" runat="server">
            ## stringformat(headerFormat, MasterImportColumnTitle, 'chkMasterImport', allMasterImports ? 'checked':'',
            'checkAllMasterImports') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateNone" runat="server">
            ## stringformat(headerFormat, noneColumnTitle, 'chkNone', allNone ? 'checked':'',
            'checkAllNone') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdPermissions_loadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdPermissions) ##
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
    <ClientEvents>
        <ItemExpand EventHandler="grdPermissions_onItemExpand" />
        <ItemCollapse EventHandler="grdPermissions_onItemExpand" />
        <ItemCheckChange EventHandler="grdPermissions_onItemCheckChanged" />
        <ItemBeforeCheckChange EventHandler="grdPermissions_onItemBeforeCheckChange" />
        <RenderComplete EventHandler="grdPermissions_onRenderComplete" />
        <Load EventHandler="grdPermissions_onLoad" />
    </ClientEvents>
</ComponentArt:Grid>
