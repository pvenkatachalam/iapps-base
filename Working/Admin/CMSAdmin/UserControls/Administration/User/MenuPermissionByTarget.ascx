<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.MenuPermissionByTarget"
    CodeBehind="MenuPermissionByTarget.ascx.cs" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<script type="text/javascript">
    var allAuthors = false;
    var allApprovers = false;
    var allPublishers = false;
    var allNone = false;
    var allArchive = false;
    var allCustomNavEditor = false;
    var allMasterImport = false;
    var MEMBERNAME = 0;
    var MEMBERID = 1;
    var AUTHOR = 2;
    var APPROVER = 3;
    var PUBLISHER = 4;
    var ARCHIVE = 5;
    var CUSTOMNAVEDITOR = 6;
    var MASTERIMPORT = 7;
    var MENU_NONE = 8;

    var TRACK = 9;
    var GRPTRACKTARGET = 10;

    function checkAllAuthor(obj) {
        //MenuPermission
        if (allAuthors) {
            allAuthors = false;
            UncheckAllItems(AUTHOR);
            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
        else {
            allAuthors = true;
            CheckAllItems(AUTHOR);
            UncheckAllItems(MENU_NONE);
            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkAllApprover(obj) {
        if (allApprovers) {
            allApprovers = false;
            UncheckAllItems(APPROVER);
        }
        else {
            allApprovers = true;
            CheckAllItems(APPROVER);
            UncheckAllItems(MENU_NONE);
            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkAllPublisher(obj) {
        if (allPublishers) {
            allPublishers = false;
            UncheckAllItems(PUBLISHER);
        }
        else {
            allPublishers = true;
            CheckAllItems(PUBLISHER);
            UncheckAllItems(MENU_NONE);
            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkAllNone(obj) {
        if (allNone) {
            allNone = false;
            UncheckAllItems(MENU_NONE);
        }
        else {
            allNone = true;
            CheckAllItems(MENU_NONE);
            UncheckAllItems(AUTHOR);
            UncheckAllItems(APPROVER);
            UncheckAllItems(PUBLISHER);
            UncheckAllItems(CUSTOMNAVEDITOR);
            UncheckAllItems(ARCHIVE);
            UncheckAllItems(MASTERIMPORT);

            document.getElementById("chkAuthor").checked = false;
            document.getElementById("chkApprover").checked = false;
            document.getElementById("chkPublisher").checked = false;
            document.getElementById("chkArchiver").checked = false;
            document.getElementById("chkCustomNavEditor").checked = false;
            document.getElementById("chkMasterImport").checked = false;

            allAuthors = false;
            allApprovers = false;
            allPublishers = false;
            //allNone = false;
            allArchive = false;
            allCustomNavEditor = false;
            allMasterImport = false;
        }
    }

    function checkAllArchive(obj) {
        if (allArchive) {
            allArchive = false;
            UncheckAllItems(ARCHIVE);
        }
        else {
            allArchive = true;
            CheckAllItems(ARCHIVE);
            UncheckAllItems(MENU_NONE);

            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkallCustomNavEditor(obj) {
        if (allCustomNavEditor) {
            allCustomNavEditor = false;
            UncheckAllItems(CUSTOMNAVEDITOR);
        }
        else {
            allCustomNavEditor = true;
            CheckAllItems(CUSTOMNAVEDITOR);
            UncheckAllItems(MENU_NONE);

            document.getElementById("chkNone").checked = false;

            allNone = false;
        }
    }

    function checkallMasterImport(obj) {
        if (allMasterImport) {
            allMasterImport = false;
            UncheckAllItems(MASTERIMPORT);
        }
        else {
            allMasterImport = true;
            CheckAllItems(MASTERIMPORT);
            UncheckAllItems(MENU_NONE);

            document.getElementById("chkMasterImport").checked = false;

            allNone = false;
        }
    }

    function CheckAllItems(columnno) {
        if (grdPermissions.RecordCount > 0) {
            isPermissionsChanged = true;

            var rowCount = grdPermissions.Table.GetRowCount();
            for (var i = 0; i < grdPermissions.Table.GetRowCount(); i++) {
                var grpTrackValue = grdPermissions.get_table().getRow(i).getMember(GRPTRACKTARGET).get_value().split(":");
                if (grpTrackValue.length < columnno || isNaN(grpTrackValue[columnno - 1]) || parseInt(grpTrackValue[columnno - 1]) != columnno - 1)
                    grdPermissions.Table.GetRow(i).SetValue(columnno, true, false);
            }

            grdPermissions.Render();
        }
    }

    function UncheckAllItems(columnno) {
        if (grdPermissions.RecordCount > 0) {
            isPermissionsChanged = true;

            var rowCount = grdPermissions.Table.GetRowCount();
            for (var i = 0; i < grdPermissions.Table.GetRowCount(); i++) {
                var grpTrackValue = grdPermissions.get_table().getRow(i).getMember(GRPTRACKTARGET).get_value().split(":");
                if (grpTrackValue.length < columnno || isNaN(grpTrackValue[columnno - 1]) || parseInt(grpTrackValue[columnno - 1]) != columnno - 1)
                    grdPermissions.Table.GetRow(i).SetValue(columnno, false, false);
            }

            grdPermissions.Render();
        }
    }

    function grdPermissions_onItemExpand(sender, eventArgs) {
        grdPermissions.render();
    }

    function UpdateCheckboxState() {
        for (var i = 0; i < grdPermissions.get_table().getRowCount(); i++) {
            var grpTrackValue = grdPermissions.get_table().getRow(i).getMember(GRPTRACKTARGET).get_value().split(":");
            for (var cnt = 1; cnt < grpTrackValue.length; cnt++) {
                if (!isNaN(grpTrackValue[cnt])) {
                    var columnId = stringformat("{0}_cell_{1}_{2}", grdPermissions.get_id(), i, parseInt(grpTrackValue[cnt]) + 1);
                    $("#" + columnId + " input").prop("disabled", true);
                }
            }
        }
    }

    function grdPermissions_onBeforeItemCheckChange(sender, eventArgs) {

        //If the column is unchecked then the track column is to be updated with the role id.
        var selectedItem = eventArgs.get_item();
        var selectedColumnIndex = eventArgs.get_columnIndex();
        var trackColumnValue = selectedItem.Data[TRACK];

        if (selectedItem.Data[selectedColumnIndex] == true) //if the column is about to be unchecked
        {
            //This is to ensure that the global roles are not included in the changed memberroles.
            var grpTrackValue = new Array();
            grpTrackValue = selectedItem.Data[GRPTRACKTARGET].split(":");
            var updateChangedColumn = true;

            for (var cnt = 0; cnt < grpTrackValue.length; cnt++) {
                if (grpTrackValue[cnt] == selectedColumnIndex - 1) //This is because the group track colum starts with 1.
                    updateChangedColumn = false;
            }

            //alert(updateChangedColumn);
            if (updateChangedColumn) {
                var returnvalue = UpdateTrackColumn(trackColumnValue, selectedColumnIndex);
                if (returnvalue != null) {
                    //Sets the new track value to the column.
                    selectedItem.SetValue(TRACK, returnvalue, false);

                }
            }
        }

        if (selectedColumnIndex == MENU_NONE) //If the checked change event happens on the None column.
        {
            if (selectedItem.Data[MENU_NONE] == false) //It is about to checked
            {
                document.getElementById("chkAuthor").checked = false; //Uncheck the Select all author column
                document.getElementById("chkApprover").checked = false; //Uncheck the Select all approver column
                document.getElementById("chkPublisher").checked = false; //Uncheck the Select all publisher column
                document.getElementById("chkCustomNavEditor").checked = false; //Uncheck the Select all editnav column
                document.getElementById("chkArchiver").checked = false; //Uncheck the Select all archiver column
                document.getElementById("chkMasterImport").checked = false; //Uncheck the Select all archiver column

                allAuthors = false;
                allApprovers = false;
                allPublishers = false;
                allArchive = false;
                allCustomNavEditor = false;
                allMasterImport = false;

                var grpTrackValue = selectedItem.getMember(GRPTRACKTARGET).get_value().split(":");


                if ($.inArray((AUTHOR-1).toString(), grpTrackValue) == -1)
                    selectedItem.SetValue(AUTHOR, false, false);

                if ($.inArray((APPROVER-1).toString(), grpTrackValue) == -1)
                    selectedItem.SetValue(APPROVER, false, false);

                if ($.inArray((PUBLISHER-1).toString(), grpTrackValue) == -1)
                    selectedItem.SetValue(PUBLISHER, false, false);

                if ($.inArray((CUSTOMNAVEDITOR-1).toString(), grpTrackValue) == -1 )
                    selectedItem.SetValue(CUSTOMNAVEDITOR, false, false);

                if ($.inArray((ARCHIVE-1).toString(), grpTrackValue) == -1)
                    selectedItem.SetValue(ARCHIVE, false, false);

                if ($.inArray((MENU_NONE-1).toString(), grpTrackValue) == -1)
                    selectedItem.SetValue(MENU_NONE, false, false);

                if ($.inArray((MASTERIMPORT-1).toString(), grpTrackValue) == -1)
                    selectedItem.SetValue(MASTERIMPORT, false, false);
            }
            else //None column is going to be unchecked
            {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }
            //Uncheck 
        }
        else // If any other column is checked or unchecked
        {
            if (selectedItem.Data[AUTHOR] == true && selectedColumnIndex == AUTHOR) {
                document.getElementById("chkAuthor").checked = false; //Uncheck the Select all AUTHOR column
                allAuthors = false;
            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }

            if (selectedItem.Data[APPROVER] == true && selectedColumnIndex == APPROVER) {
                document.getElementById("chkApprover").checked = false; //Uncheck the Select all APPROVER column
                allApprovers = false;
            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }

            if (selectedItem.Data[PUBLISHER] == true && selectedColumnIndex == PUBLISHER) {
                document.getElementById("chkPublisher").checked = false; //Uncheck the Select all PUBLISHER column
                allPublishers = false;
            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }

            if (selectedItem.Data[CUSTOMNAVEDITOR] == true && selectedColumnIndex == CUSTOMNAVEDITOR) {
                document.getElementById("chkCustomNavEditor").checked = false; //Uncheck the Select all EDITNAV column
                allCustomNavEditor = false;
            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }

            if (selectedItem.Data[ARCHIVE] == true && selectedColumnIndex == ARCHIVE) {
                document.getElementById("chkArchiver").checked = false; //Uncheck the Select all ARCHIVE column
                allArchive = false;
            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }

            if (selectedItem.Data[MASTERIMPORT] == true && selectedColumnIndex == MASTERIMPORT) {
                document.getElementById("chkMasterImport").checked = false; //Uncheck the Select all MASTERIMPORT column
                allArchive = false;
            }
            else {
                document.getElementById("chkNone").checked = false; //Uncheck the Select all none column
                allNone = false;
            }
            selectedItem.SetValue(MENU_NONE, false, false);
        }

       
    }

    function grdPermissions_onItemCheckChange(sender, eventArgs) {
        isPermissionsChanged = true;
        sender.render();
    }

    function UpdateTrackColumn(trackColvalue, newColumnIndex) {
        var newTrackColumnvalue = null;
        if (!Contains(trackColvalue, newColumnIndex)) {
            var oldTrackColumnValues = trackColvalue.toString().split(':');
            if (oldTrackColumnValues == "")
                return newColumnIndex;
            oldTrackColumnValues[oldTrackColumnValues.length] = newColumnIndex;
            newTrackColumnvalue = oldTrackColumnValues.join(':');
        }
        return newTrackColumnvalue;
    }

    function Contains(sourceValue, targetValue) {
        var returnStatus = false;
        var splittedValues = sourceValue.toString().split(':');

        for (i = 0; i < splittedValues.length; i++) {
            if (splittedValues[i] == targetValue)
                return true;
        }

        return returnStatus;
    }

    function GetMainHeader() {
        if (gridActionsJson.CustomFilter == "1")
            return "<%= GUIStrings.User%>";
        else
            return "<%= GUIStrings.Group%>";
    }

    var headerFormat = '<div style="padding-bottom:4px;">{0}</div><input type="checkbox" id="{1}" name="group1" {2} onclick="{3}(this);" />';
    var authorColumnTitle = "<%= GUIStrings.Author %>";
    var approverColumnTitle = "<%= GUIStrings.Approver %>";
    var publisherColumnTitle = "<%= GUIStrings.Publisher %>";
    var noneColumnTitle = "<%= GUIStrings.ClearAll %>";
    var archiveColumnTitle = "<%= GUIStrings.Archive %>";
    var customNavEditorColumnTitle = "<%= GUIStrings.NavEditor %>";
    var masterImportColumnTitle = "<%= GUIStrings.MasterImport %>";
</script>
<ComponentArt:Grid ID="grdPermissions" SkinID="Default" RunningMode="Client" Width="960"
    runat="server" Height="260" ShowFooter="false" CallbackCachingEnabled="true"
    CallbackCacheSize="25" CssClass="iapps-grid" AllowVerticalScrolling="true" LoadingPanelClientTemplateId="grdPermissions_loadingPanelTemplate">
    <levels>
        <ComponentArt:GridLevel DataKeyField="UserId" HeadingCellCssClass="iapps-heading-cell"
            HeadingRowCssClass="iapps-heading-row" HeadingTextCssClass="iapps-heading-text"
            DataCellCssClass="iapps-data-cell" RowCssClass="iapps-row" SelectorCellWidth="1"
            SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false" HeadingCellHoverCssClass="iapps-heading-cell-hover"
            SelectedRowCssClass="iapps-selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
            SortImageWidth="8" SortImageHeight="7" AlternatingRowCssClass="iapps-alternate-row"
            ShowSelectorCells="false" AllowGrouping="false" HoverRowCssClass="iapps-hover-row">
            <Columns>
                <ComponentArt:GridColumn AllowEditing="true" DataField="UserId" Visible="false" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="UserName" Width="300" HeadingCellClientTemplateId="MainHeaderTemplate" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="7" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, Author %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateAuthor" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="8" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, Approver %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateApprover" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="9" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, Publisher %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplatePublisher" />                
                <ComponentArt:GridColumn AllowEditing="true" DataField="10" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, Archive %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateArchive" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="30" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, NavEditor %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateEditNav" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="36" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, MasterImport %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateMasterImport" />
                <ComponentArt:GridColumn AllowEditing="true" DataField="0" ColumnType="CheckBox" HeadingText="<%$ Resources:GUIStrings, ClearAll %>" Width="80"
                    Align="Center" HeadingCellClientTemplateId="HeaderTemplateNone" HeadingCellCssClass="iapps-last-heading-cell" />
                <ComponentArt:GridColumn DataField="Track" Visible="false" />
                <ComponentArt:GridColumn DataField="GrpTrack" Visible="false" />
            </Columns>
        </ComponentArt:GridLevel>
    </levels>
    <clienttemplates>
        <ComponentArt:ClientTemplate ID="MainHeaderTemplate" runat="server">
            ## GetMainHeader() ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateAuthor" runat="server">
            ## stringformat(headerFormat, authorColumnTitle, 'chkAuthor', allAuthors ? 'checked':'','checkAllAuthor') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateApprover" runat="server">
            ## stringformat(headerFormat, approverColumnTitle, 'chkApprover', allApprovers ?'checked':'', 'checkAllApprover') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplatePublisher" runat="server">
            ## stringformat(headerFormat, publisherColumnTitle, 'chkPublisher', allPublishers ? 'checked':'', 'checkAllPublisher') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateNone" runat="server">
            ## stringformat(headerFormat, noneColumnTitle, 'chkNone', allNone ? 'checked':'','checkAllNone') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateArchive" runat="server">
            ## stringformat(headerFormat, archiveColumnTitle, 'chkArchiver', allArchive ? 'checked':'','checkAllArchive') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="HeaderTemplateEditNav" runat="server">
            ## stringformat(headerFormat, customNavEditorColumnTitle, 'chkCustomNavEditor', allCustomNavEditor ? 'checked':'', 'checkallCustomNavEditor') ##
        </ComponentArt:ClientTemplate>
         <ComponentArt:ClientTemplate ID="HeaderTemplateMasterImport" runat="server">
            ## stringformat(headerFormat, masterImportColumnTitle, 'chkMasterImport', allMasterImport ? 'checked':'', 'checkallMasterImport') ##
        </ComponentArt:ClientTemplate>
        <ComponentArt:ClientTemplate ID="grdPermissions_loadingPanelTemplate">
            ## GetGridLoadingPanelContent(grdPermissions) ##
        </ComponentArt:ClientTemplate>
    </clienttemplates>
    <clientevents>
        <ItemExpand EventHandler="grdPermissions_onItemExpand" />
        <ItemCollapse EventHandler="grdPermissions_onItemExpand" />
        <ItemBeforeCheckChange EventHandler="grdPermissions_onBeforeItemCheckChange" />
        <ItemCheckChange EventHandler="grdPermissions_onItemCheckChange" />
        <RenderComplete EventHandler="grdPermissions_onRenderComplete" />
        <Load EventHandler="grdPermissions_onLoad" />
    </clientevents>
</ComponentArt:Grid>
