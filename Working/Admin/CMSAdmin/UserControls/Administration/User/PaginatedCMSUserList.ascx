﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaginatedCMSUserList.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.PaginatedCMSUserList" %>
<script type="text/javascript">
    function SearchValidation() {
        var searchVal = $("#<%= txtSearch.ClientID %>").val();
        if (searchVal.match(/^[&<>]+$/)) {
            alert('<%= JSMessages.ThisCharactersAreNotAllowed %>');
            return false;
        }
        return true;
    }
    var arrSelected = new Array();

    function UpdateSelectedUserAll(chkboxAll, hdnId) {
        Array.clear(arrSelected);
        $('#hdnSelectedUsers').val('');
        $('#chkSelectAllTmp').prop('checked', chkboxAll.checked);
        UpdateSelectedUserInGrid(chkboxAll.checked, hdnId);
    }
    function UpdateSelectedUserInGrid(chkboxAllValue, hdnId) {
        var boxes = $('#gridview input[type=checkbox]');
        if (chkboxAllValue) {
            Array.clear(arrSelected);
            $('#hdnSelectedUsers').val('');
            boxes.each(function () {
                if ((this).id.indexOf('chkSelectAll') <= 0) {
                    (this).checked = true;
                    UpdateSelectedUserArray((this).id, hdnId, true);
                    $('#hdnSelectedUsers').val(arrSelected.join(','));
                }
            }
        );
        }

        if (!chkboxAllValue && $('#hdnSelectedUsers').val() == '') {
            boxes.each(function () {
                if ((this).id.indexOf('chkSelectAll') <= 0) {
                    (this).checked = false;
                }
            }
            );
        }
    }
    function UpdateSelectedUserArray(chkboxId, hdnId, isChecked) {
        var hdnUserId = chkboxId.replace('chkSelect', hdnId);
        var userId = $('#' + hdnUserId).val();

        if (isChecked) {
            if (jQuery.inArray(userId, arrSelected) == -1) {
                arrSelected.push(userId);
            }
        }
        else {
            arrSelected.pop(userId);
        }
    }
    function UpdateSelectedUser(chkbox, hdnId) {
        UpdateSelectedUserArray(chkbox.id, hdnId, chkbox.checked);
        $('#hdnSelectedUsers').val(arrSelected.join(','));
    }
    function GetSelectedUserIds() {
        return arrSelected;
    }
    function ajaxError(ajaxErrorObject, type, errorThrown) {
        alert('error'); // ajaxErrorObject.responseText);
    }
    function InitializeImportUserScript() {
        if ($('#hdnSelectedUsers').val() != '') {
            arrSelected = $('#hdnSelectedUsers').val().split(',');
        }

        $(".view-permissions").iAppsClickMenu({
            width: "250px",
            heading: "<%= GUIStrings.Permissions %>",
            create: function (e, menu) {
                var permissionsList = AdminCallback.GetUserPermission(e.attr("objectId"));
                if (permissionsList != null && permissionsList != '') {
                    var permissions = permissionsList.split('#');
                    if (permissions.length > 0) {
                        $.each(permissions, function () {
                            if (this.toString() != "")
                                menu.addListItem(this.toString());
                        });
                    }
                }
            }
        });

        UpdateSelectedUserInGrid($('#chkSelectAllTmp').prop('checked'), 'hdnUserId');
    }
</script>
<div class="grid-actions clear-fix" id="divSearch" runat="server">
    <div class="row clear-fix">
        <div class="columns">
            <asp:TextBox runat="server" ID="txtSearch" Width="240" ToolTip="<%$ Resources:GUIStrings, TypeHereToFilterResults %>"
                ClientIDMode="Static" />
        </div>
        <div class="columns">
            <asp:DropDownList runat="server" ID="ddlRole" Width="210" ClientIDMode="Static">
                <asp:ListItem Text="<%$ Resources:GUIStrings, AllUsers %>" Value="0" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, ContentAdministrator %>" Value="4" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, SiteAdministrator %>" Value="5" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, InstallAdministrator %>" Value="6" />
                <asp:ListItem Text="<%$ Resources:GUIStrings, Previewer %>" Value="35" />
            </asp:DropDownList>
        </div>
        <div class="columns">
            <asp:Button ID="btnSearch" runat="server" Text="<%$ Resources:GUIStrings, Search %>"
                ToolTip="<%$ Resources:GUIStrings, Search %>" CssClass="small-button" OnClientClick="return SearchValidation();"
                CausesValidation="false" OnClick="btnSearch_Click" />
        </div>
        <div class="columns right-column">
            <div class="pager-info">
                <asp:Literal ID="ltrCurrentPage" runat="server" />
            </div>
            <div class="pager-buttons">
                <asp:ImageButton ID="cmdPrev" runat="server" OnClick="cmdPrev_Click" CausesValidation="false"
                    ImageUrl="~/App_Themes/General/images/prev.gif" ToolTip="Previous Page" CssClass="prev-page" />
                <asp:ImageButton ID="cmdNext" runat="server" OnClick="cmdNext_Click" CausesValidation="false"
                    ImageUrl="~/App_Themes/General/images/next.gif" ToolTip="Next Page" CssClass="next-page" />
            </div>
        </div>
    </div>
</div>
<asp:Repeater ID="rptUserList" runat="server" OnItemCreated="rptUserList_ItemCreated"
    OnItemDataBound="rptUserList_ItemBound">
    <HeaderTemplate>
        <table width="100%" cellpadding="0" cellspacing="0" class="grid" id="gridview">
            <thead>
                <tr>
                    <th id="tdHdSelect" runat="server">
                        <asp:CheckBox ID="chkSelectAll" runat="server" onclick="javascript:UpdateSelectedUserAll(this,'hdnUserId');" />
                    </th>
                    <th id="tdHdUserName">
                        <asp:LinkButton ID="lnkUserName" runat="server" CausesValidation="false" OnClick="SortUserNameClick"><%= GUIStrings.UserName%></asp:LinkButton>
                        <asp:Localize ID="lcUserName" runat="server" Text="<%$Resources:GUIStrings,UserName%>"
                            Visible="false" />
                    </th>
                    <th id="tdHdLastName">
                        <asp:LinkButton ID="lnkLastName" runat="server" CausesValidation="false" OnClick="SortLastNameClick"><%= GUIStrings.LastName%></asp:LinkButton>
                        <asp:Localize ID="lcLastName" runat="server" Text="<%$Resources:GUIStrings,LastName%>"
                            Visible="false" />
                    </th>
                    <th id="tdHdFirstName">
                        <asp:LinkButton ID="lnkFirstName" runat="server" CausesValidation="false" OnClick="SortFirstNameClick"><%= GUIStrings.FirstName%></asp:LinkButton>
                        <asp:Localize ID="lcFirstName" runat="server" Text="<%$Resources:GUIStrings,FirstName%>"
                            Visible="false" />
                    </th>
                    <th id="tdHdSendNotification">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$Resources:GUIStrings,SendNotification%>" />
                    </th>
                    <th id="tdHDExpires">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$Resources:GUIStrings,Expires%>" />
                    </th>
                    <th id="tdHDUserGroupPermission">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$Resources:GUIStrings,UserGroupPermission%>" />
                    </th>
                </tr>
            </thead>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class='<%# Container.ItemIndex % 2 == 0 ? "odd-row" : "even-row" %>'>
            <td runat="server" id="tdSelect">
                <asp:CheckBox ID="chkSelect" runat="server" onclick="javascript:UpdateSelectedUser(this,'hdnUserId');" />
            </td>
            <td>
                <asp:Literal ID="ltrUserName" runat="server" Text='<%# Eval("UserName") %>' />
            </td>
            <td>
                <asp:Literal ID="ltLastName" runat="server" Text='<%# Eval("LastName") %>' />
                <asp:HiddenField ID="hdnUserId" runat="server" Value='<%# Eval("UserId")  %>' />
            </td>
            <td>
                <asp:Literal ID="ltFirstName" runat="server" Text='<%# Eval("FirstName") %>' />
            </td>
            <td>
                <asp:Literal ID="ltSendNotification" runat="server" Text='<%# Eval("EmailNotification") %>' />
            </td>
            <td>
                <asp:Literal ID="ltExpires" runat="server" Text='<%# Eval("ExpiryDate").ToString() %>' />
            </td>
            <td>
                <asp:HyperLink runat="server" objectId='<%# Eval("UserId") %>' CssClass="view-permissions" 
                    NavigateUrl="#" Text="<%$Resources:GUIStrings,CurrentPermissions%>" />
            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
<asp:HiddenField ID="hdnMicroSiteId" runat="server" />
<asp:HiddenField ID="hdnSelectedUsers" runat="server" ClientIDMode="Static" />
<asp:CheckBox ID="chkSelectAllTmp" runat="server" ClientIDMode="Static" Style="display: none;" />