﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TranslationForm.ascx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.TranslationForm" %>
<script type="text/javascript">
    function ShowSubmitNow() {
        $('#divSubmitNow').iAppsDialog("open");
        return false;
    }
    function HideSubmitNow() {
        $("#divSubmitNow").iAppsDialog("close");
        return false;
    }
    function ShowAddBatch() {
        $('#divAddBatch').iAppsDialog("open");
        return false;
    }
    function HideAddBatch() {
        $("#divAddBatch").iAppsDialog("close");
        return false;
    }

    function ddlBatch_onClientChange() {
        $('#<%=txtBatchName.ClientID %>').val('');
    }

    function txtBatchName_onClientFocusout() {
        if ($('#<%=txtBatchName.ClientID %>').val() != '') {
            $('#<%=ddlBatch.ClientID %>').val(EmptyGuid);
        }
    }

    function ddlTranslateFrom_onClientChange() {
        if ($('#<%=ddlTranslateFrom.ClientID %>').val() == $('#<%=ddlTranslateTo.ClientID %>').val()) {
            alert('The source and target languages can not be the same.');
        }
    }

    function ValidateOnSubmit() {

        if ($('#<%=txtBatchName.ClientID %>').val() == '' && $('#<%=ddlBatch.ClientID %>').val() == EmptyGuid) {
            alert("<%= JSMessages.PleaseSelectOrEnterBatch %>");
            return false;
        }

        if ($('#<%=ddlTranslateFrom.ClientID %>').val() == $('#<%=ddlTranslateTo.ClientID %>').val()) {
            alert('The source and target languages can not be the same.');
            return false;
        }

        else if ($('#<%=ddlBatch.ClientID %>').val() == EmptyGuid &&
            $('#<%=txtBatchName.ClientID %>').val() != "" && CheckBatchNameExists()) {
            alert("<%= JSMessages.BatchNameAlreadyExists %>");
            return false;
        }

    return true;
}

function CheckBatchNameExists() {
    var exists = false;
    var bName = $('#<%=txtBatchName.ClientID %>').val();
        $.ajax({
            type: "POST",
            url: jAdminSiteUrl + "/SiteActivity/ManageTranslationDetails.aspx/WebMethod_CheckBatchNameExists",
            data: "{'batchName':'" + bName + "','batchId':''}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (msg) {
                exists = msg.d;
            }
        });

        return exists;
    }

    function ValidateBatchIsEmpty(sender, args) {
        args.IsValid = ValidateOnSubmit();
    }
</script>
<asp:Panel ID="phTranslationDetails" runat="server" CssClass="translation-submission-form">
    <div class="dark-section-header clear-fix">
        <h2><%= GUIStrings.BatchDetails %></h2>
    </div>
    <div class="form-section">
        <asp:PlaceHolder ID="phBatchDropDown" runat="server">
            <div class="form-row" style="margin-bottom: 0px;">
                 <label class="form-label">
                <asp:Literal ID="ltBatchLabel" runat="server" Text="<%$ Resources:GUIStrings, AddToExistingBatch %>" />
            </label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlBatch" runat="server" Width="400" onchange="ddlBatch_onClientChange()">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, __Select__ %>" />
                    </asp:DropDownList>
                </div>
            </div>
            <div>
                <label class="form-label" style="padding-left: 10px;">
                    <%= GUIStrings.Or %>
                </label>
            </div>
        </asp:PlaceHolder>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.NewBatchName %></label>
            <div class="form-value">
                <asp:TextBox ID="txtBatchName" runat="server" Width="390" onfocusout="txtBatchName_onClientFocusout()" />
            </div>
        </div>
    </div>
    <div class="form-section" style="border-bottom: 0;">
        <h3>
            <asp:Literal ID="ltDetails" runat="server" Text="<%$ Resources:GUIStrings, PageDetails %>" Visible="false" />
        </h3>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.AlsoTranslate %></label>
            <div class="form-value">
                <asp:CheckBoxList ID="chklTranslate" runat="server" RepeatLayout="UnorderedList" CssClass="translate-properties-list">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, PageProperties %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, PageTitle %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, PageDescription %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, URLFriendlyName %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, MenuTitle %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, MenuFriendlyUrl %>" />
                    <asp:ListItem Text="<%$ Resources:GUIStrings, IncludeMenuDescription %>" />
                </asp:CheckBoxList>
            </div>
        </div>
    </div>
    <div class="form-section">
        <div class="dark-section-header clear-fix">
            <h2><%= GUIStrings.TranslationDetails %></h2>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.TranslateFrom %><span class="req"> *</span></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlTranslateFrom" onchange="ddlTranslateFrom_onClientChange()" runat="server" Width="400" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.TranslateTo %><span class="req"> *</span></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlTranslateTo" onchange="ddlTranslateFrom_onClientChange()"
                    runat="server" Width="400" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.Instructions %></label>
            <div class="form-value">
                <asp:TextBox ID="txtInstructions" runat="server" Width="390" TextMode="MultiLine" />
            </div>
        </div>
    </div>
    <div class="iapps-modal" id="divSubmitNow" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.SubmitNow %></h2>
        </div>
        <div class="modal-content clear-fix">
            <p>
                <%= GUIStrings.BatchNotSubmitted %></p>
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" value="<%= GUIStrings.Cancel %>" class="button" onclick="HideSubmitNow();" />
            <asp:Button ID="btnSubmitBatch" runat="server" Text="<%$ Resources:GUIStrings, SubmitBatch %>"
                CssClass="primarybutton" />
            <asp:Button ID="btnSubmitPages" runat="server" Text="<%$ Resources:GUIStrings, SubmitPagesOnly %>"
                CssClass="primarybutton" />
        </div>
    </div>
    <div class="iapps-modal" id="divAddBatch" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.AddToBatch %></h2>
        </div>
        <div class="modal-content clear-fix">
            <p>
                <%= GUIStrings.BatchAlreadySubmitted %></p>
        </div>
        <div class="modal-footer clear-fix">
            <input type="button" value="<%= GUIStrings.Cancel %>" class="button" onclick="HideAddBatch();" />
            <asp:Button ID="Button1" runat="server" Text="<%$ Resources:GUIStrings, ResubmitBatch %>"
                CssClass="primarybutton" />
            <asp:Button ID="Button2" runat="server" Text="<%$ Resources:GUIStrings, SubmitPages %>"
                CssClass="primarybutton" />
        </div>
        <asp:HiddenField runat="server" ID="hdnPopupMode" Value="0" />
    </div>
</asp:Panel>

