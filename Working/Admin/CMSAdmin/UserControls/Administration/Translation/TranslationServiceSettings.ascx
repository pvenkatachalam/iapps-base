﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TranslationServiceSettings.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.TranslationServiceSettings" %>
<asp:PlaceHolder ID="phEnabledSetting" runat="server">
    <h2>
        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, General %>" /></h2>
    <div class="form-row">
        <label class="form-label" style="width: 260px;">
            <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, TranslatePagePropertiesByDefault %>" /></label>
        <div class="form-value text-value">
            <asp:CheckBox ID="chkTranslatePageProperties" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label" style="width: 260px;">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, AutomaticallyImportTranslatedSubmissions %>" /></label>
        <div class="form-value text-value">
            <asp:CheckBox ID="chkAutoImportSubmissions" runat="server" />
        </div>
    </div>
    <hr />
</asp:PlaceHolder>
<asp:PlaceHolder ID="phDisabledSetting" runat="server">
    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, YouNeedToEnableTranslationToModifyTheseSettings %>" />
    &nbsp;<asp:LinkButton ID="lbEnableTranslation" runat="server" Text="<%$ Resources:GUIStrings, PleaseClickHereToEnableTranslation %>"
        OnClick="lbEnableTranslation_OnClick"></asp:LinkButton>
</asp:PlaceHolder>
