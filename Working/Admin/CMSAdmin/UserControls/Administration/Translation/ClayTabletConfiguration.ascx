﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClayTabletConfiguration.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ClayTabletConfiguration" %>
<div class="form-row">
    <label class="form-label">
        Source Account File
        <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ClayTabletSourceAccFile %>"
            alt="" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtSourceAccountFile" runat="server" CssClass="textBoxes" />
    </div>
</div>
<div class="form-row">
    <label class="form-label">
        Target Account File
        <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ClayTabletDestinatinAccFile %>"
            alt="" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtTargetAccountFile" runat="server" CssClass="textBoxes" />
    </div>
</div>

<div class="form-row">
    <label class="form-label">
        Content Folder Path
        <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ConnectionContextPath %>"
            alt="" /></label>
    <div class="form-value">
        <asp:TextBox ID="txtConnectionContextFolder" runat="server" CssClass="textBoxes" />
    </div>
</div>