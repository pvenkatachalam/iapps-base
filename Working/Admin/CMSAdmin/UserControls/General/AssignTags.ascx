<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false" Inherits="Bridgeline.iAPPS.Admin.Common.Web.Controls.AssignTags"
    CodeBehind="AssignTags.ascx.cs" ClientIDMode="AutoID" %>
<script type="text/javascript">
    $(function () {
        setTimeout(function(){
            cbTagsRefresh.LoadingPanelClientTemplate = GetTreeLoadingPanelContent(cbTagsRefresh);
        }, 500);
        
        hdnSelectedTags_Populate(true);
    });

    function AssignTags_GetSelectedTags() {
        return $("#<%=hdnSelectedTags.ClientID %>").val();
    }

    function AssignTags_SetSelectedTags(selectedTags) {
        if (selectedTags != "") {
            var strTags = "";
            $.each(JSON.parse(selectedTags), function () {
                strTags += this.Id + ",";
            });

            SetSelectedTags(strTags);
        }
    }

    function AssignTags_AddCheckedItem(id)
    {
        treeActionsJson.CheckedItems.remove(id);
        treeActionsJson.CheckedItems.push(id);
    }

    function SetSelectedTags(selectedTags) {
        if (typeof selectedTags != "undefined") {
            var selectedIds = selectedTags.split(",");
            for (var i = 0; i < selectedIds.length; i++) {
                if (selectedIds[i] != "") {
                    var node = tvAssignTags.findNodeById(selectedIds[i]);
                    if (node) {
                        node.set_checked(true);
                        AssignTags_AddCheckedItem(selectedIds[i]);
                    }
                }
            }

            hdnSelectedTags_Populate(false);
        }
    }

    function tvEditTags_onLoad(sender, eventArgs) {
        $(".tree-container").treeActions({
            objTree: tvEditTags,
            objSelectTree: typeof tvAssignTags != "undefined" ? tvAssignTags : null,
            pageTree: false,
            maxNodeType: 1,
            expandOnSelect: false,
            fixHeader: <%= FixHeader.ToString().ToLower() %>,
            onItemSelect: function () {
                if (typeof LoadIndexGrid == "function")
                    LoadIndexGrid();
            }
        });
        setTimeout(function () {
            //SetSelectedTags(popupActionsJson.CustomAttributes["SelectedTaxonomyIds"]);
        }, 300);
    }

    function tvEditTags_onBeforeRename(sender, eventArgs) {
        $(".tree-container").treeActions("beforeRename", eventArgs);
    }

    function Tree_Callback(sender, eventArgs) {
        treeActionsJson.Success = true;
        var doCallback = false;
        treeActionsJson.HideActions = !treeActionsJson.SiteId.equals(jAppId) ||  treeActionsJson.TabIndex == 0;

        switch (eventArgs.Action) {
            case "TabIndex":
                doCallback = treeActionsJson.TabIndex == 0;
                Tags_SetTabs();
                break;
            case "DeleteNode":
                treeActionsJson.Success = window.confirm("<%= JSMessages.AreYouSureYouWantToDeleteThisIndexTermAndAllItsSubTerms %>");
                if (treeActionsJson.Success) {
                    result = DeleteTaxonomyItem(treeActionsJson.FolderId);
                    if (result.toLowerCase() != "true") {
                        alert("<%= JSMessages.DeletingFailedPleaseTryAgain %>");
                        treeActionsJson.Success = false;
                    }
                }
                break;
            case "UpdateName":
                updateNodeName();
                break;
            case "EditSettings":
                $("#txtTagTitle").val(treeActionsJson.Text);
                $("#txtTagDescription").val(typeof treeActionsJson.SelectedValue != "undefined" ? treeActionsJson.SelectedValue : "");
                $("#editTag").iAppsDialog("open");
                break;
            case "SiteSelector":
                doCallback = true;
                tagsChanged = false;
                treeActionsJson.FolderId = EmptyGuid;
                treeActionsJson.NodeType = -1;
                break;
        }

        if (doCallback) {
            Tags_Callback();
        }
    }

    function Tree_BeforeSiteChange(sender, eventArgs)
    {
        var proceed = true;
        if (tagsChanged)
            proceed = window.confirm("<%= JSMessages.TagSiteSwitchConfirmationMessage %>");

        return proceed;
    }

    function Tags_Callback() {
        if (tvEditTags != null)
            tvEditTags.dispose();

        if(typeof tvAssignTags != "undefined" && tvAssignTags != null)
            tvAssignTags.dispose();

        cbTagsRefresh.callback(JSON.stringify(treeActionsJson));
    }

    function cbTagsRefresh_onCallbackComplete(sender, eventArgs) {
        Tags_SetTabs();
    }

    function Tags_SetTabs()
    {
        if (treeActionsJson.TabIndex == 1)
        {
            $("#" + tvEditTags.get_id()).show();
            if(typeof tvAssignTags != "undefined" && tvAssignTags != null)
                $("#" + tvAssignTags.get_id()).hide();
            sNode = tvEditTags.findNodeById(treeActionsJson.FolderId);
            if (sNode != null) sNode.select();
        }
        else
        {
            $(".tree-container .tree-actions .tree-node-action").hide();
            $("#" + tvEditTags.get_id()).hide();
            $("#" + tvAssignTags.get_id()).show();
            sNode = tvAssignTags.findNodeById(treeActionsJson.FolderId);
            if (sNode != null) {
                //sNode.select();
                sNode.set_checked(true);
                AssignTags_AddCheckedItem(treeActionsJson.FolderId);
            }
        }

        $(".tree-container").treeActions("bindControls");
    }

    function updateNodeName() {
        if (treeActionsJson.Action == "AddNode") {
            var result = CreateTaxonomyItem(treeActionsJson.NewText, treeActionsJson.ParentId);
            if (result.length == 36)
                treeActionsJson.FolderId = result;
            else {
                alert(result);
                treeActionsJson.Success = false;
            }
        }
        else {
            RenameTaxonomyItem(treeActionsJson.NewText, treeActionsJson.FolderId);
        }
    }

    var checkChanged = false;
    var tagsChanged = false;
    function tvAssignTags_onNodeCheckChange(sender, eventArgs) {
        checkChanged = true;
        OnNodeCheckChanged(eventArgs.get_node(), eventArgs.get_node().get_checked());
    }

    function tvTags_onSelect(sender, eventArgs) {
        if (!checkChanged) {
            if (sender.get_id().indexOf("tvAssignTags") > -1) {
                if (eventArgs.get_node().Depth == 0){
                    //OnNodeCheckChanged(eventArgs.get_node(), eventArgs.get_node().get_checked() != true);
                }
                else {
                    SetCheckboxes(eventArgs.get_node(), eventArgs.get_node().get_checked() != true, false);
                    hdnSelectedTags_Populate(false);
                }
            }

            $(".tree-container").treeActions("selectNode", eventArgs);
        }
        checkChanged = false;
    }

    function OnNodeCheckChanged(node, checked) {
        var propogate = false;
        if (node.get_nodes().get_length() > 0)
            propogate = window.confirm(checked ? "<%= JSMessages.DoYouWantToSelectAllSubTerms %>" : "<%= JSMessages.DoYouWantToDeselectAllSubTerms %>");

        SetCheckboxes(node, checked, propogate);
        hdnSelectedTags_Populate(false);
    }
    
    function SetCheckboxes(node, checked, propogate) {
        if (node.get_depth() > 0) {
            node.set_checked(checked);
            if (checked)
                AssignTags_AddCheckedItem(node.get_id());
            else
                treeActionsJson.CheckedItems.remove(node.get_id());
        }

        if (propogate) {
            for (var i = 0; i < node.Nodes().length; i++) {
                SetCheckboxes(node.Nodes()[i], checked, propogate);
            }
        }
    }

    function CloseSaveTag() {
        $("#editTag").iAppsDialog("close");
        return false;
    }

    function SaveTag() {
        if (/^[^<>]+$/.test($("#txtTagDescription").val()) == false && $("#txtTagDescription").val().length > 0) {
            alert("<%= JSMessages.PleaseEnterValidDescription %>");
    }
    else {
        treeActionsJson.Action = "SaveDescription";
        treeActionsJson.SelectedValue = $("#txtTagDescription").val();

        Tags_Callback();
        $("#editTag").iAppsDialog("close");
    }
    return false;
}

function hdnSelectedTags_Populate(onload) {
    var selectedTags = [];
    $.each(treeActionsJson.CheckedItems, function () {
        var sNode = tvAssignTags.findNodeById(this);
        if (sNode != null)
            selectedTags.push({ "Id": this, "Title": sNode.get_text() });
    });

    if (selectedTags.length > 0)
        $("#<%=hdnSelectedTags.ClientID %>").val(JSON.stringify(selectedTags));
    else
        $("#<%=hdnSelectedTags.ClientID %>").val("");

    if (!onload)
        tagsChanged = true;
}
</script>
<div class="tree-container">
    <div class="tree-header">
        <asp:Literal ID="ltTreeHeading" runat="server" />
        <iAppsControls:TreeActions ID="treeActions" runat="server" CssClass="EditTagsAction" PageName="AssignTags" TreeName="tvEditTags" />
    </div>
    <ComponentArt:CallBack ID="cbTagsRefresh" PostState="true" EnableViewState="true" runat="server" SkinID="TreeCallback" CssClass="tree-content">
        <Content>
            <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvEditTags"
                runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                <ClientEvents>
                    <Load EventHandler="tvEditTags_onLoad" />
                    <NodeSelect EventHandler="tvTags_onSelect" />
                    <NodeBeforeRename EventHandler="tvEditTags_onBeforeRename" />
                </ClientEvents>
                <CustomAttributeMappings>
                    <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
                    <ComponentArt:CustomAttributeMapping From="Description" To="Value" />
                </CustomAttributeMappings>
            </ComponentArt:TreeView>
            <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvAssignTags"
                runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                <ClientEvents>
                    <NodeSelect EventHandler="tvTags_onSelect" />
                    <NodeCheckChange EventHandler="tvAssignTags_onNodeCheckChange" />
                </ClientEvents>
                <CustomAttributeMappings>
                    <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
                </CustomAttributeMappings>
            </ComponentArt:TreeView>
            <asp:HiddenField ID="hdnTagSiteId" runat="server" />
            <asp:HiddenField ID="hdnBindComplete" runat="server" />
        </Content>
        <ClientEvents>
            <CallbackComplete EventHandler="cbTagsRefresh_onCallbackComplete" />
        </ClientEvents>
    </ComponentArt:CallBack>
</div>
<asp:HiddenField ID="hdnSelectedTags" runat="server" />
<asp:XmlDataSource ID="xmlDataSource" runat="server" />
<div id="editTag" class="iapps-modal" style="display: none;">
    <div class="modal-header clear-fix">
        <h2>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Tags %>" /></h2>
    </div>
    <div class="modal-content" style="width: 400px;">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span
                    class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtTagTitle" runat="server" Width="300" CssClass="textBoxes" ReadOnly="true"
                    ClientIDMode="Static" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
            </label>
            <div class="form-value">
                <asp:TextBox CssClass="textBoxes" ID="txtTagDescription" runat="server" TextMode="MultiLine"
                    Rows="8" Width="298" ClientIDMode="Static" />
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
            CssClass="button" OnClientClick="return CloseSaveTag();" />
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            CssClass="primarybutton" OnClientClick="return SaveTag();" />
    </div>
</div>
