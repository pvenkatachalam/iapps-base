<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignWorkflowGrid.ascx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.AssignWorkflowGrid" %>

<ComponentArt:Grid ID="grdAssignWorkflow" SkinID="ScrollingGrid" ScrollPopupClientTemplateId="ScrollPopupTemplate"
    Width="376" runat="server" PageSize="10" Height="240" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
    <Levels>
        <ComponentArt:GridLevel DataKeyField="UserId" ShowTableHeading="false" ShowSelectorCells="false"
            RowCssClass="sRow" DataCellCssClass="sDataCell" ColumnReorderIndicatorImageUrl="reorder.gif"
            HeadingCellCssClass="sHeadingCell" HeadingCellHoverCssClass="sHeadingCellHover"
            HeadingCellActiveCssClass="sHeadingCellActive" HeadingRowCssClass="sHeadingRow"
            HeadingTextCssClass="sHeadingCellText" SelectedRowCssClass="sSelectedRow" GroupHeadingCssClass="GroupHeading"
            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="sAlternateDataRow">
            <Columns>
                <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, UserGroups %>" Width="150" FixedWidth="true" AllowReordering="false" HeadingCellCssClass="sFirstHeadingCell"
                    DataField="UserName" DataCellCssClass="sFirstDataCell" SortedDataCellCssClass="sSortedDataCell" />
                <ComponentArt:GridColumn DataField="UserId" Visible="false" />
                <ComponentArt:GridColumn HeadingText="RoleId" DefaultSortDirection="Descending" AllowReordering="false"
                    DataField="RoleId" Visible="false" SortedDataCellCssClass="sSortedDataCell" />
                <ComponentArt:GridColumn DataField="Permission" FixedWidth="true" Visible="true" Width="100" />
            </Columns>
        </ComponentArt:GridLevel>
    </Levels>
    <ClientTemplates>
        <ComponentArt:ClientTemplate ID="ScrollPopupTemplate">
            <div class="sScrollPopup">
                <h5>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ItemHeadingHere %>" /></h5>
            </div>
        </ComponentArt:ClientTemplate>
    </ClientTemplates>
</ComponentArt:Grid>
