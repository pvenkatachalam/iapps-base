<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.AssignWorkflowtoMenu" Codebehind="AssignWorkflowtoMenu.ascx.cs" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register TagPrefix="WorkFlow" TagName="Grid" Src="~/UserControls/Popup/AssignWorkflowGrid.ascx" %>
<div class="assignWorkflowMenu">
    <div class="popupBoxShadow">
        <div class="popupboxContainer">
            <div class="dialogBox">
                <h3>
                    <asp:localize runat="server" text="<%$ Resources:GUIStrings, Warning %>"/></h3>
                <div class="middleContent">
                    <p>
                        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:GUIStrings, TheFollowingCMSUsersDoNotHaveTheProperPermissions %>"></asp:Label>
                    </p>
                    <div class="gridContent">
                        <WorkFlow:Grid ID="WorkFlowGrid" runat="server" />
                    </div>
                    <p class="confirm">
                        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:GUIStrings, WouldYouLikeToAssignTheMinimumLevelNeededToTheseUsers %>"></asp:Label>
                    </p>
                </div>
                <div class="footerContent">
                    <asp:Button ID="updatePermission" runat="server" OnClientClick="javascript:CloseAssignWorkflowPopup('yes');" Text="<%$ Resources:GUIStrings, UpdatePermissions %>" Cssclass="primarybutton" ToolTip="<%$ Resources:GUIStrings, UpdatePermissions %>" />
                    <asp:Button ID="cancelRequest" runat="server" Text="<%$ Resources:GUIStrings, CancelRequest %>" OnClientClick="javascript:CloseAssignWorkflowPopup('no');" Cssclass="button buttonSpace" tooltip="<%$ Resources:GUIStrings, CancelRequest %>" />                                        
                </div>
            </div>
        </div>
    </div>
</div>