<%@ Control Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Controls.ViewPagesByObject" 
    CodeBehind="ViewPagesByObject.ascx.cs" %>
<script type="text/javascript">
    function grdViewPages_OnLoad(sender, eventArgs) {
        $("#view_pages").gridActions({ objList: grdViewPages });
    }
    function grdViewPages_RenderComplete(sender, eventArgs) {
        $("#view_pages").gridActions("bindControls");
    }
    function grdViewPages_CallbackComplete(sender, eventArgs) {
        $("#view_pages").gridActions("displayMessage", { complete: function () {
            if (gridActionsJson.ResponseCode == 100)
                CanceliAppsAdminPopup(true);
        }
        });
    }

    function grdViewPages_ItemSelect(sender, eventArgs) {
        if (eventArgs.get_item().getMember("Status").get_value() == "NoAccess") {
            alert("<%= JSMessages.PageNoAccess %>");
            grdViewPages.unSelectAll();
        }
        else if (eventArgs.get_item().getMember("Status").get_value() == "InWorkflow") {
            alert("<%= JSMessages.PageInWorkflow %>");
            grdViewPages.unSelectAll();
        }
    }

    function JumpToThisPage(pageLink, isTemplatePreview) {
        if (typeof(isTemplatePreview) !='undefined' &&  isTemplatePreview  == true) {
            parent.JumpToTemplatePreview(pageLink.innerHTML);
        }
        else {
            ShowiAppsLoadingPanel();
            parent.JumpToFrontPage(pageLink.innerHTML);
        }

        return false;
    }
</script>
<div id="view_pages">
    <div class="grid-message">
        <asp:Literal ID="ltMessage" runat="server" />
    </div>
    <asp:Panel ID="pnlGrid" runat="server" CssClass="grid-section">
        <ComponentArt:Grid ID="grdViewPages" SkinID="Default" runat="server" ShowFooter="false"
            Width="100%" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" CssClass="fat-grid">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="PageId" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="ViewTemplate" />
                        <ComponentArt:GridColumn DataField="CompleteFriendlyUrl" Visible="false" />
                        <ComponentArt:GridColumn DataField="PageId" Visible="false" />
                        <ComponentArt:GridColumn DataField="SiteId" Visible="false" />
                        <ComponentArt:GridColumn DataField="SiteName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <ItemSelect EventHandler="grdViewPages_ItemSelect" />
            </ClientEvents>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="WarningTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item">
                            <h4>
                                <%# Container.DataItem["Title"]%></h4>
                            <p>
                                <%# GetPageUrlWithSiteName(Container.DataItem, false)%></p>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="ViewTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item">
                            <h4>
                                <%# Container.DataItem["Title"]%></h4>
                            <p>
                                <%# GetPageUrlWithSiteName(Container.DataItem, true)%></p>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
        </ComponentArt:Grid>
    </asp:Panel>
</div>
