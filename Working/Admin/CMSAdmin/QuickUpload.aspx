<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.QuickUpload"
    CodeBehind="QuickUpload.aspx.cs" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, QuickUpload %>" />
    </title>
    <link href="App_Themes/General/dhtmlwindow.css" rel="Stylesheet" media="all" />
    <link href="App_Themes/General/modal.css" rel="Stylesheet" media="all" />
    <link href="App_Themes/General/General.css" rel="stylesheet" media="all" />
    <script type="text/javascript" src="script/swfobject.js"></script>
</head>
<body id="fUpload" class="upload">
    <script type="text/javascript">

        var params = {};

        var attributes = {
            id: "FlashFilesUpload",
            name: "FlashFilesUpload"
        };

        var flashvars = { "listViewButton.visible": "false",
            "thumbViewButton.visible": "false",
            "sortButton.visible": "false",
            "uploadButton.visible": "false",
            "language.autoDetect": "true",
            formName: "<%=frmUploadForm.ClientID%>",
            uploadUrl: "MultiFileUploadProcessor.aspx",
            useExternalInterface: "Yes"
        };
        swfobject.embedSWF("ElementITMultiPowUpload3.0.swf", "flashObject", "325", "275", "9.0.0", "expressInstall.swf", flashvars, params, attributes);

        function MultiPowUpload_onComplete(type, fileIndex) {
            window.alert(stringformat("<asp:localize runat='server' text='<%$ Resources:JSMessages, _0OfFile1CompletedSuccessfully %>'/>", type, Flash.fileList()[fileIndex].name));
        }
        function ged() {
            var randomnumber = Math.floor(Math.random() * 11);
            return randomnumber;
        }
        function RefreshTree() {

            if (parent.window["UploadFileType"] == "AssetFile") {
                if (parent.window["fileLibraryTree"].get_selectedNode()) {
                    var nodeId = parent.window["fileLibraryTree"].get_selectedNode().get_id();
                    parent.window["cbFileTreeRefresh"].Callback(nodeId);
                }
                else {
                    var nodeId = document.getElementById("hdnUploadFolderId").value;
                    parent.window["cbFileTreeRefresh"].Callback(nodeId);
                }
            }
            else if (parent.window["UploadFileType"] = "AssetImage") {
                if (parent.window["imageLibraryTree"].get_selectedNode()) {
                    var nodeId = parent.window["imageLibraryTree"].get_selectedNode().get_id();
                    parent.window["cbImageTreeRefresh"].Callback(nodeId);
                }
                else {
                    var nodeId = document.getElementById("hdnUploadFolderId").value;
                    parent.window["cbImageTreeRefresh"].Callback(nodeId);
                }
            }
        }
        function mysubmit() {
            if (!window.parent.IsThisSessionValid()) {
                window.parent.location.reload();
                return;
            }
            var Flash;
            if (document.embeds && document.embeds.length >= 1 && navigator.userAgent.indexOf("Safari") == -1) {
                Flash = document.getElementById("EmbedFlashFilesUpload");
            }
            else {
                Flash = document.getElementById("FlashFilesUpload");
            }
            var submitElement = document.getElementById("submitted");
            submitElement.value = "true";
            // server side call;
            var uniqeElement = GetUniqueUploadID();
            document.getElementById("ProgressID").value = uniqeElement;

            var selectedTreeNodeId = "";
            if (parent.window["UploadFileType"] == "AssetFile") {
                if (parent.window["FileSelectedNodeId"] != null) {
                    //If they have permission to select the node they should able to upload it
                    //                    if (parent.window["HavePermission"] == false) {
                    //                        alert(GetMessage("NoPermissoinUploadFile"));
                    //                        return false;
                    //                    }
                    selectedTreeNodeId = parent.window["FileSelectedNodeId"];
                }
                else {
                    alert(GetMessage("SelectFolderUploadFile"));
                    return false;
                }
            }
            else if (parent.window["UploadFileType"] == "AssetImage") {
                if (parent.window["ImageSelectedNodeId"] != null) {

                    selectedTreeNodeId = parent.window["ImageSelectedNodeId"];
                }
                else {
                    alert(GetMessage("SelectFolderUploadImage"));
                    return false;
                }
            }
            document.getElementById("hdnUploadFileType").value = parent.window["UploadFileType"];
            document.getElementById("hdnUploadFolderId").value = selectedTreeNodeId;
            Flash.uploadAll();
            return false;
        }

        // this function is the custom function added to the flash and is always called just when upload button is called.
        // here the fileSize variable has been named wrongly - it actually refers to the number of files which are being uploaded.
        // so do not use the numberOfFiles for any purpose
        function downloadInfo(fileSize, numberOfFiles) {
            if (fileSize > 0) {
                var progressId = document.getElementById("ProgressID").value;
                var winl = (screen.width - 300) / 2;
                var wint = (screen.height - 380) / 2;
                var winprops = 'height=' + 380 + ',width=' + 300 + ',top=' + wint + ',left=' + winl + 'resizable=0,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes';
                var strUrl = "UploadProgress.aspx?ProgressID=" + progressId + "&FunctionName=Refresh&noOfFiles=" + fileSize;
            }
            else {
                if (parent.window["UploadFileType"] == "AssetImage") {
                    alert(GetMessage("SelectFileUploadImage"));
                }
                else {
                    alert(GetMessage("SelectFileUpload"));
                }
            }
        }
    </script>
    <div id="flashObject">
    </div>
    <script type="text/javascript">
        // this is required because - sometimes flash does not get initialized in its normal way.
        window.FlashFilesUpload = document.getElementById("FlashFilesUpload");
    </script>
    <form id="frmUploadForm" name="frmUploadForm" runat="server" method="post">
    <div class="multiUpload">
        <input name="submitbtn" title="<%= GUIStrings.UploadFiles %>" onclick="return mysubmit();"
            type="button" value="<%= GUIStrings.UploadFiles %>" class="primarybutton" />
        <input type="hidden" id="hdnUploadFolderId" name="hdnUploadFolderId" value="" />
        <input type="hidden" id="hdnUploadFileType" name="hdnUploadFileType" value="" />
        <input type="hidden" id="submitted" value="false" />
        <input type="hidden" id="Tick" runat="server" />
        <input type="hidden" id="ProgressID" runat="server" />
    </div>
    </form>
</body>
</html>
