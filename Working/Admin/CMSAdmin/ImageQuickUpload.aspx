<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ImageQuickUpload"
    Theme="General" CodeBehind="ImageQuickUpload.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, QuickUpload %>" />
    </title>
    <script type="text/javascript" src="script/swfobject.js"></script>
    <style type="text/css">
        body > object
        {
            display: none;
        }
    </style>
</head>
<body id="fUpload" class="popupBody">
    <form id="frmUploadForm" runat="server" method="post">
    <div class="iapps-modal multi-upload">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ImageFileQuickUpload %>" /></h2>
        </div>
        <div class="modal-content clear-fix">
            <script type="text/javascript">
                var params = {};
                var attributes = {
                    id: "flashfilesupload",
                    name: "flashfilesupload"
                };

                var flashvars = { "listViewButton.visible": "false",
                    "thumbViewButton.visible": "false",
                    "sortButton.visible": "false",
                    "uploadButton.visible": "false",
                    "language.autoDetect": "true",
                    formName: "<%=frmUploadForm.ClientID%>",
                    uploadUrl: "MultiFileUploadProcessor.aspx",
                    useExternalInterface: "Yes"
                };

                swfobject.embedSWF("ElementITMultiPowUpload3.0.swf", "flashObject", "325", "275", "10.0.0", "expressInstall.swf", flashvars, params, attributes);

                function closeIframe() {

                    parent.CloseQuickUploadPopup();
                    CanceliAppsAdminPopup(true);
                }

                function setFolderId() {
                    var o_j = document.getElementById("JUpload");
                    var hdnFolder = document.getElementById("hdnFolderId");
                    if (hdnFolder != null)
                        hdnFolder.value = '123-456-789';
                }
                function MultiPowUpload_onComplete(type, fileIndex) {
                    window.alert(stringformat("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, _0OfFile1CompletedSuccessfully %>' />", type, Flash.fileList()[fileIndex].name));
                }
                function ged() {
                    var randomnumber = Math.floor(Math.random() * 11);
                    return randomnumber;
                }
                function RefreshTree() { }
                function mysubmit() {
                    var Flash;
                    if (document.embeds && document.embeds.length >= 1 && navigator.userAgent.indexOf("Safari") == -1) {
                        Flash = document.getElementById("EmbedFlashFilesUpload");
                    }
                    else {
                        Flash = document.getElementById("flashfilesupload");
                    }
                    var submitElement = document.getElementById("submitted");
                    submitElement.value = "true";
                    // server side call;
                    var uniqeElement = GetUniqueUploadID();
                    document.getElementById("ProgressID").value = uniqeElement;
                    var selectedTreeNodeId = "";

                    if (parent.window["UploadFileType"] == "AssetFile") {
                        if (parent.gridActionsJson != null) {
                            selectedTreeNodeId = parent.gridActionsJson.FolderId;
                        }
                        else {
                            alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, PleaseSelectAFolderToUploadFiles %>' />");
                            return false;
                        }
                    }
                    if (parent.window["UploadFileType"] == "AssetImage") {
                        if (parent.gridActionsJson != null) {
                            selectedTreeNodeId = parent.gridActionsJson.FolderId;
                        }
                        else {
                            alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, PleaseSelectAFolderToUploadImages %>' />");
                            return false;
                        }
                    }
                    if (parent.window["UploadFileType"] == "Xslt") {
                        if (parent.window["SelectedXmlId"] != null) {
                            selectedTreeNodeId = parent.window["SelectedXmlId"];
                            document.getElementById("UploadFolderPath").value = parent.window["UploadFolderPath"];
                        }
                        else {
                            alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, PleaseSelectAXMLToUploadXslts %>' />");
                            return false;
                        }
                    }
                    document.getElementById("hdnUploadFileType").value = parent.window["UploadFileType"];
                    document.getElementById("hdnUploadFolderId").value = selectedTreeNodeId;
                    Flash.uploadAll();
                    return false;
                }


                // this function is the custom function added to the flash and is always called just when upload button is called.
                // here the fileSize variable has been named wrongly - it actually refers to the number of files which are being uploaded.
                // so do not use the numberOfFiles for any purpose
                function downloadInfo(fileSize, numberOfFiles) {
                    if (fileSize > 0) {
                        var progressId = document.getElementById("ProgressID").value;
                        var winl = (screen.width - 300) / 2;
                        var wint = (screen.height - 380) / 2;
                        var winprops = 'height=' + 380 + ',width=' + 300 + ',top=' + wint + ',left=' + winl + 'resizable=0,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes';
                        var strUrl = "UploadProgress.aspx?ProgressID=" + progressId + "&FunctionName=Refresh&noOfFiles=" + fileSize;
                        //window.open(strUrl, "UploadProgress", winprops);
                    }
                    else
                        alert('<asp:Localize runat="server" Text="<%$ Resources:JSMessages, NoFilesHaveBeenSelectedForUpload %>" />');
                }

            </script>
            <p><em><%= GUIStrings.HoldCNTRLToSelectMultiple %></em></p>
            <div id="flashObject">
                <p><%= GUIStrings.QuickUploadNoFlashText %></p>
            </div>
            <input type="hidden" id="hdnUploadFolderId" name="hdnUploadFolderId" value="" />
            <input type="hidden" id="hdnUploadFileType" name="hdnUploadFileType" value="" />
            <input type="hidden" id="UploadFolderPath" name="UploadFolderPath" value="" />
            <input type="hidden" id="submitted" value="false" />
            <input type="hidden" id="Tick" runat="server" />
            <input type="hidden" id="ProgressID" runat="server" />
            <script type="text/javascript">
                // this is required because - sometimes flash does not get initialized in its normal way.
                window.FlashFilesUpload = document.getElementById("flashfilesupload");
            </script>
        </div>
        <div class="modal-footer clear-fix">
            <input name="Closebtn" onclick="closeIframe();" type="button" value="<%= GUIStrings.Close %>"
                class="button" />
            <input name="submitbtn" title="<%= GUIStrings.UploadFiles %>" onclick="return mysubmit();"
                type="button" value="<%= GUIStrings.UploadFiles %>" class="primarybutton" />
        </div>
    </div>
    </form>
</body>
</html>
