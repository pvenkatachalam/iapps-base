﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ControlCenter.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ControlCenter" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="DateRange" Src="~/UserControls/DateRange.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function BindScripts() {
            $(".manage-widgets").each(function () {
                var $this = $(this);
                $this.off("click", ".edit-widget");
                $this.on("click", ".edit-widget", function () {
                    if ($(this).hasClass("expanded")) {
                        $(".widget-list").hide();
                        $(".edit-widget").removeClass("expanded");
                    }
                    else {
                        $(".widget-list").hide();
                        $(".edit-widget").removeClass("expanded");
                        $this.find(".widget-list").show();
                        $(this).addClass("expanded");
                    }
                });
                $this.off("click", "input[type='checkbox']");
                $this.on("click", "input[type='checkbox']", function (e) {
                    var selCount = $this.find("input[type='checkbox']:checked").length;
                    var maxLength = $this.attr("type") == "links" ? 8 : 4;
                    if (selCount == 0) {
                        alert("You should have atleast one item selected.");
                        $(this).prop("checked", true);
                    }
                    else if (selCount > maxLength) {
                        alert("You cannot have more than " + maxLength + " items selected.");
                        $(this).prop("checked", false);
                    }
                    else {
                        if ($this.attr("type") == "links")
                            cbLinks.callback();
                        else
                            cbWidgets.callback();
                    }
                });

                $this.off("click", ".iapps-menu-close");
                $this.on("click", ".iapps-menu-close", function () {
                    $(this).parent().hide();
                    $(".edit-widget").removeClass("expanded");
                });
            });

            if ($(".manage-widgets").length > 0) {
                $(".cc-buttons, .site-activity").sortable({
                    forcePlaceHolderSize: true,
                    revert: true
                });

                $(document).click(function (e) {
                    if (!$(e.target).closest(".manage-widgets").length) {
                        $(".widget-list").hide();
                    }
                });
            }
        }

        function OnSaveClick() {
            dashboardDataJson.Links = [];
            dashboardDataJson.Widgets = [];

            $(".cc-buttons > a").each(function () {
                dashboardDataJson.Links.push($(this).attr("linkUrl"));
            });
            $(".site-activity > .boxes").each(function () {
                dashboardDataJson.Widgets.push($(this).attr("widgetUrl"));
            });

            $("#hdnDashboardData").val(JSON.stringify(dashboardDataJson));
            return true;
        }

        $(function () {
            BindScripts();
        });

        function cbLinks_OnCallbackComplete() {
            BindScripts();
            $(".cc-buttons .widget-list").show();
        }

        function cbWidgets_OnCallbackComplete() {
            BindScripts();
            $(".site-activity .widget-list").show();
        }
    </script>
</asp:Content>
<asp:Content ID="headerbuttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:HyperLink ID="hplCustomize" runat="server" Text="<%$ Resources:GUIStrings, Customize %>"
            CssClass="button" NavigateUrl="~/General/ControlCenter.aspx?Customize=true" />
        <asp:PlaceHolder ID="phCustomizeButtons" runat="server" Visible="false">
            <asp:HiddenField ID="hdnDashboardData" runat="server" ClientIDMode="Static" />
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return OnSaveClick();"
                OnClick="btnSave_Click" />
            <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                NavigateUrl="~/General/ControlCenter.aspx" CssClass="button" />
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:CallbackPanel ID="cbLinks" runat="server" CssClass="cc-buttons clear-fix"
        OnClientCallbackComplete="cbLinks_OnCallbackComplete">
        <ContentTemplate>
            <asp:PlaceHolder ID="phEditLinks" runat="server" Visible="false">
                <div class="manage-widgets" type="links">
                    <a class="edit-widget">
                        <%= GUIStrings.AddRemove %></a>
                    <div class="widget-list" style="display: none;">
                        <div class="iapps-menu-close">
                            <img alt="close" src="/Admin/App_Themes/General/images/menu-close.png" /></div>
                        <h4>
                            <asp:Literal ID="ltLinks" runat="server" Text="<%$ Resources:GUIStrings, DashboardLinksHelp %>" /></h4>
                        <asp:CheckBoxList ID="chkLinks" runat="server" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:Literal ID="ltCurrentLinks" runat="server" />
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
    <iAppsControls:CallbackPanel ID="cbWidgets" runat="server" CssClass="site-activity clear-fix"
        OnClientCallbackComplete="cbWidgets_OnCallbackComplete">
        <ContentTemplate>
            <div class="section-header clear-fix">
                <h2>
                    <asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, SiteActivity %>" />
                </h2>
                <UC:DateRange ID="dataRange" runat="server" Width="200" />
            </div>
            <asp:PlaceHolder ID="phEditWidgets" runat="server" Visible="false">
                <div class="manage-widgets" type="widgets">
                    <a class="edit-widget">
                        <%= GUIStrings.AddRemove %></a>
                    <div class="widget-list" style="display: none;">
                        <div class="iapps-menu-close">
                            <img alt="close" src="/Admin/App_Themes/General/images/menu-close.png" /></div>
                        <h4>
                            <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:GUIStrings, DashboardWidgetsHelp %>" /></h4>
                        <asp:CheckBoxList ID="chkWidgets" runat="server" />
                    </div>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phWidgets" runat="server" />
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
</asp:Content>
