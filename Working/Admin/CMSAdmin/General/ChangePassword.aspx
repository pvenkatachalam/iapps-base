<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ChangePassword" CodeBehind="ChangePassword.aspx.cs" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function RedirectToLogin() {
            var loginURL = jCommonLoginUrl + '/General/Login.aspx';
            parent.window.location.href = loginURL;
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ChangePassword ID="changePassword" runat="server" ChangePasswordFailureText="<%$ Resources: JSMessages, ChangePasswordError %>"
        RenderOuterTable="false">
        <ChangePasswordTemplate>
            <asp:Panel ID="pnlChangePassword" runat="server" DefaultButton="btnChangePwd">
                <asp:ValidationSummary ID="summaryValidation" runat="server" EnableClientScript="true"
                    ShowMessageBox="true" ShowSummary="false" />
                <div class="form-section">
                    <div class="error-message">
                        <asp:Literal ID="FailureText" runat="server" Text="&nbsp;" />
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, CurrentPasswordColon %>" /><span class="req">&nbsp;*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="CurrentPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                                Width="290" />
                            <asp:RequiredFieldValidator ID="reqCurrentPassword" runat="server" ControlToValidate="CurrentPassword"
                                ErrorMessage="<%$ Resources:JSMessages, CurrentPasswordRequired %>" Display="None"
                                SetFocusOnError="true" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, NewPasswordColon %>" /><span class="req">&nbsp;*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="NewPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                                Width="290" />
                            <asp:RequiredFieldValidator ID="reqNewPassword" runat="server" ControlToValidate="NewPassword"
                                Text=" " ErrorMessage="<%$ Resources:JSMessages, NewPasswordRequired %>" Display="None"
                                SetFocusOnError="true" />
                            <asp:RegularExpressionValidator ID="regNewPassword" runat="server" ControlToValidate="NewPassword"
                                ErrorMessage="<%$ Resources: JSMessages, InvalidInput %>"
                                ValidationExpression="^[^<>]+$" Display="None" SetFocusOnError="true" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ConfirmPassword %>" /><span class="req">&nbsp;*</span></label>
                        <div class="form-value">
                            <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="textBoxes" TextMode="Password"
                                Width="290" />
                            <asp:RequiredFieldValidator ID="reqConfirmNewPassword" runat="server" ControlToValidate="ConfirmNewPassword"
                                ErrorMessage="<%$ Resources:JSMessages, ConfirmNewPasswordRequired %>" Display="None"
                                SetFocusOnError="true" />
                            <asp:RegularExpressionValidator ID="regConfirmNewPassword" runat="server" ControlToValidate="ConfirmNewPassword"
                                ErrorMessage="<%$ Resources: JSMessages, InvalidInput %>"
                                ValidationExpression="^[^<>]+$" Display="None" SetFocusOnError="true" />
                            <asp:CompareValidator ID="cvConfirmNewPassword" runat="server" ControlToCompare="NewPassword"
                                ControlToValidate="ConfirmNewPassword" ErrorMessage="<%$ Resources:JSMessages, PasswordMismatch %>"
                                Display="None" SetFocusOnError="true" />
                        </div>
                    </div>
                    <p style="text-align: right;"><em><%= Bridgeline.iAPPS.Resources.GUIStrings.YouWillNeedToLoginAgainAfterChangingYourPassword %></em></p>
                </div>
                <div class="modal-footer clear-fix">
                    <asp:Button ID="btnCancel" runat="server" CausesValidation="False" OnClientClick="return CanceliAppsAdminPopup();"
                        CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
                    <asp:Button ID="btnChangePwd" runat="server" CommandName="ChangePassword" Text="<%$ Resources:GUIStrings, ChangePassword %>"
                        ToolTip="<%$ Resources:GUIStrings, ChangePassword %>" CssClass="primarybutton" />
                </div>
            </asp:Panel>
        </ChangePasswordTemplate>
        <SuccessTemplate>
            <div class="form-section">
                <div class="success-message">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, YourPasswordHasBeenChangedSuccessfully %>" />
                </div>
            </div>
            <div class="modal-footer clear-fix">
                <asp:Button ID="btnContinue" Text="<%$ Resources:GUIStrings, Continue %>" ToolTip="<%$ Resources:GUIStrings, Continue %>"
                    CssClass="primarybutton" runat="server" OnClientClick="return RedirectToLogin();" />
            </div>
        </SuccessTemplate>
    </asp:ChangePassword>
</asp:Content>
