<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SearchResults" StylesheetTheme="General"
    MasterPageFile="~/MasterPage.Master" CodeBehind="SearchResults.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="DateRange" Src="~/UserControls/DateRange.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".select-all-fields input[type='checkbox']").click(function () {
                $(".select-field input[type='checkbox']").prop("checked", $(this).prop("checked"));
            });

            $("#previewResult").iAppsDialog({ appendToEnd: false });
        });

        function grdSearchResults_OnLoad(sender, eventArgs) {
            $("#search_results").gridActions({
                objList: grdSearchResults,
                listType: "tableGrid"
            });
        }

        function grdSearchResults_OnRenderComplete(sender, eventArgs) {
            $("#search_results").gridActions("bindControls");
            $("#search_results").iAppsSplitter();
        }

        function grdSearchResults_OnCallbackComplete(sender, eventArgs) {
            $("#search_results").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
                case "Preview":
                    doCallback = false;
                    ShowPreview(selectedItem);
                    break;
                case "ViewPage":
                    doCallback = false;
                    JumpToFrontPage(selectedItem.getMember("CompleteFriendlyURL").get_value());
                    break;
                case "JumpToLibrary":
                    doCallback = false;
                    var folderId = selectedItem.getMember("FolderId").get_value();
                    switch (eventArgs.selectedItem.getMember("IndexType").get_value()) {
                        case "Content Definitions":
                        case "Content Items":
                            RedirectToAdminPage("ManageContentLibrary", "DirectoryId=" + folderId);
                            break;
                        case "Menus":
                        case "Pages":
                            RedirectToAdminPage("ManagePageLibrary", "DirectoryId=" + folderId);
                            break;
                        case "Files":
                            RedirectToAdminPage("ManageFileLibrary", "DirectoryId=" + folderId);
                            break;
                        case "Images":
                            RedirectToAdminPage("ManageImageLibrary", "DirectoryId=" + folderId);
                            break;
                    }
                    break;
            }

            if (doCallback) {
                grdSearchResults.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdSearchResults.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            switch (eventArgs.selectedItem.getMember("IndexType").get_value()) {
                case "Content Definitions":
                case "Content Items":
                case "Menus":
                case "Pages":
                case "Files":
                case "Images":
                    sender.getItemByCommand("JumpToLibrary").show();
                    break;
                default:
                    sender.getItemByCommand("JumpToLibrary").hide();
                    break;
            }

            if (eventArgs.selectedItem.getMember("IndexType").get_value() == "Pages")
                sender.getItemByCommand("ViewPage").show();
            else
                sender.getItemByCommand("ViewPage").hide();
        }

        function ShowPreview(selectedItem) {
            $("#previewResult .preview-content").html(PreviewContent(selectedItem.Key, selectedItem.getMember("IndexType").get_value()));
            $("#previewResult").iAppsDialog("open");

            return false;
        }

        function ClosePreview() {
            $("#previewResult .preview-content").html("");
            $("#previewResult").iAppsDialog("close");

            return false;
        }

        function FilterSearchResults() {

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div style="display:none;"><asp:Literal runat="server" ID="uxSearchDebug"></asp:Literal></div>
    <div id="search_results">
        <asp:Panel ID="pnlLeftColumn" runat="server" DefaultButton="btnSearch" CssClass="left-control">
            <h2>
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Filters %>" /></h2>
            <div class="form-section">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, SearchTerm %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtSearchTerms" runat="server" CssClass="textBoxes" Width="280" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FilterBy %>" /></label>
                    <div class="form-value">
                        <asp:CheckBox ID="chkAllElements" CssClass="select-all-fields" Checked="true" runat="server"
                            Text="<%$ Resources:GUIStrings, ShowAll %>" />
                    </div>
                </div>
                <div class="form-row checkbox-list">
                    <asp:CheckBoxList ID="chkFilterBy" CssClass="select-field" runat="server" RepeatDirection="Vertical" />
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Created %>" /></label>
                    <div class="form-value">
                        <UC:DateRange ID="createdDateRange" runat="server" Width="290" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, LastEdited %>" /></label>
                    <div class="form-value">
                        <UC:DateRange ID="editedDateRange" runat="server" Width="290" />
                    </div>
                </div>
                <div class="button-row">
                    <asp:Button ID="btnSearch" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Search %>"
                        ToolTip="<%$ Resources:GUIStrings, Search %>" OnClick="btnSearch_Click" ValidationGroup="valSearchResults" />
                </div>
            </div>
        </asp:Panel>
        <div class="right-control">
            <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
            <div class="grid-section">
                <ComponentArt:Grid ID="grdSearchResults" SkinID="default" runat="server" Width="100%"
                    ShowFooter="false" RunningMode="Callback" LoadingPanelClientTemplateId="grdSearchResultsLoadingPanelTemplate" ManualPaging="true">
                    <ClientEvents>
                        <Load EventHandler="grdSearchResults_OnLoad" />
                        <RenderComplete EventHandler="grdSearchResults_OnRenderComplete" />
                        <CallbackComplete EventHandler="grdSearchResults_OnCallbackComplete" />
                    </ClientEvents>
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                            HeadingCellCssClass="heading-cell" AllowSorting="false" HeadingRowCssClass="heading-row"
                            SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            AllowGrouping="false" AlternatingRowCssClass="alternate-row">
                            <Columns>
                                <ComponentArt:GridColumn DataField="FirstTitle" DataCellClientTemplateId="FirstTitleTemplate"
                                    IsSearchable="true" HeadingText="<%$ Resources:GUIStrings, Title %>" DataCellCssClass="first-cell"
                                    HeadingCellCssClass="first-cell" />
                                <ComponentArt:GridColumn DataField="SecondTitle" DataCellClientTemplateId="DescriptionTemplate"
                                    IsSearchable="true" HeadingText="<%$ Resources:GUIStrings, Description %>" />
                                <ComponentArt:GridColumn DataField="IndexType" DataCellClientTemplateId="IndexTypeTemplate"
                                    HeadingText="<%$ Resources:GUIStrings, Type %>" />
                                <ComponentArt:GridColumn DataField="CreatedDate" DataCellClientTemplateId="CreatedDateTemplate"
                                    HeadingText="<%$ Resources:GUIStrings, Created %>" FormatString="d" />
                                <ComponentArt:GridColumn DataField="ModifiedDate" DataCellClientTemplateId="ModifiedDateTemplate"
                                    HeadingText="<%$ Resources:GUIStrings, LastEdited %>" FormatString="d" DataCellCssClass="last-cell"
                                    HeadingCellCssClass="last-cell" />
                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                <ComponentArt:GridColumn DataField="FolderId" Visible="false" />
                                <ComponentArt:GridColumn DataField="CompleteFriendlyURL" Visible="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="FirstTitleTemplate">
                            ## DataItem.getMember('FirstTitle').get_text() ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="DescriptionTemplate">
                            ## DataItem.getMember('SecondTitle').get_text() ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="IndexTypeTemplate">
                            ## DataItem.getMember('IndexType').get_text() ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="CreatedDateTemplate">
                            ## DataItem.getMember('CreatedDate').get_text() ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="ModifiedDateTemplate">
                            ## DataItem.getMember('ModifiedDate').get_text() ##
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdSearchResultsLoadingPanelTemplate">
                            ## GetGridLoadingPanelContent(grdSearchResults) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
    </div>
    <div id="previewResult" class="iapps-modal" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Preview %>" /></h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="preview-content">
                &nbsp;
            </div>
            <div class="preview-options clear-fix">
                <a href="#" class="content-view-button selected">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Content %>" /></a>
                <a href="#" class="html-view-button">
                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, HTML %>" /></a>
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                CssClass="button" OnClientClick="return ClosePreview();" />
        </div>
    </div>
</asp:Content>
