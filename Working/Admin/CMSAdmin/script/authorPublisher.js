
function DeleteSequence_onclick() {
    workflowSequenceGrid.deleteSelected();
}
function AddSequence_onclick(sender, eventArgs)//new change added this sender, eventArgs
{
    var TabId = tabSite;
    var TabIndex = TabId.getSelectedTab().get_index();
    var GridName;
    if (TabIndex == 0) {
        GridName = authorGrid;
    }
    if (TabIndex == 1) {
        GridName = approverGrid;
    }
    if (TabIndex == 2) {
        GridName = publisherGrid;
    }
    var draggedItem;
    if (eventArgs != null)//new 
    {
        draggedItem = eventArgs.get_item();
    }
    else {
        //var targetItem = eventArgs.get_target();
        draggedItem = GridName.GetSelectedItems()[0];
    }
    //var draggedItemLength=GridName.GetSelectedItems().length;
    var isExist = false;
    if (draggedItem != null)//new        
    {
        var count = workflowSequenceGrid.get_table().getRowCount();
        var actorId = draggedItem.getMember("ActorId").get_text();
        var roleId = draggedItem.getMember("RoleId").get_text();
        for (var i = 0; i < count; i++) {
            var Actorid = workflowSequenceGrid.get_table().getRow(i).get_cells(0)[0].Value;
            var Roleid = workflowSequenceGrid.get_table().getRow(i).get_cells(0)[3].Value;
            if ((Actorid == actorId) && (Roleid == roleId)) {
                isExist = true;
            }
        }
        if (isExist == false) {
            var actorName = draggedItem.getMember("ActorName").get_text();
            var actorType = draggedItem.getMember("ActorType").get_text();
            var roleName = draggedItem.getMember("RoleName").get_text();
            var rowitem;
            var targetItem;
            if (eventArgs != null)//new 
            {
                targetItem = eventArgs.get_target();
                if (targetItem != null) {
                    rowitem = workflowSequenceGrid.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    rowitem = workflowSequenceGrid.get_table().addEmptyRow();
                }
            }
            else {
                rowitem = workflowSequenceGrid.get_table().addEmptyRow();
            }
            workflowSequenceGrid.edit(rowitem);
            rowitem.setValue(0, draggedItem.getMember("ActorId").get_text());
            rowitem.setValue(1, draggedItem.getMember("ActorName").get_text());
            rowitem.setValue(2, draggedItem.getMember("ActorType").get_text());
            rowitem.setValue(3, draggedItem.getMember("RoleId").get_text());
            rowitem.setValue(4, draggedItem.getMember("RoleName").get_text());

            workflowSequenceGrid.editComplete();
            workflowSequenceGrid.unSelect(rowitem);
            workflowSequenceGrid.render();
        }
    }
}
//
function workflowSequenceGrid_onItemMoveAndDelete(sender, eventArgs) {
    if (eventArgs.get_targetControl().get_id() != workflowSequenceGrid.get_id()) //document.getElementById('<%=.ClientID%>').id)
    {
        workflowSequenceGrid.deleteItem(eventArgs.get_item());
    }
    else if (eventArgs.get_targetControl().get_id() == workflowSequenceGrid.get_id()) //document.getElementById('<%=workflowSequenceGrid.ClientID%>').id)
    {
        var targetItem = eventArgs.get_target();
        var newRow;
        if (targetItem != null) {
            workflowSequenceGrid.deleteItem(eventArgs.get_item());
            newRow = workflowSequenceGrid.get_table().addEmptyRow(targetItem.get_index());
        }
        else {
            workflowSequenceGrid.deleteItem(eventArgs.get_item());
            newRow = workflowSequenceGrid.get_table().addEmptyRow();
        }
        workflowSequenceGrid.beginUpdate();
        newRow.setValue(0, eventArgs.get_item().getMember('ActorId').get_text());
        newRow.setValue(1, eventArgs.get_item().getMember('ActorName').get_text());
        newRow.setValue(2, eventArgs.get_item().getMember('ActorType').get_text());
        newRow.setValue(3, eventArgs.get_item().getMember('RoleId').get_text());
        newRow.setValue(4, eventArgs.get_item().getMember('RoleName').get_text());
        workflowSequenceGrid.endUpdate();
    }
}

//
function MoveDown_onclick() {

    var recordcount = workflowSequenceGrid.get_recordCount();
    if (recordcount != 0) {
        var draggedItem = workflowSequenceGrid.GetSelectedItems()[0];
        var draggedItemLength = workflowSequenceGrid.GetSelectedItems().length;
        if (draggedItemLength > 0) {
            var draggedItemIndex = workflowSequenceGrid.GetSelectedItems()[0].get_index();
            if (draggedItemIndex != recordcount - 1) {
                var actorId = draggedItem.getMember("ActorId").get_text();
                var actorName = draggedItem.getMember("ActorName").get_text();
                var actorType = draggedItem.getMember("ActorType").get_text();
                var roleId = draggedItem.getMember("RoleId").get_text();
                var roleName = draggedItem.getMember("RoleName").get_text();
                var rowItemIndex = draggedItemIndex + 1;
                workflowSequenceGrid.deleteItem(draggedItem);
                workflowSequenceGrid.unSelect(draggedItem); //deleting row is unselect and new will be select
                var rowitem = workflowSequenceGrid.get_table().addEmptyRow(rowItemIndex);
                workflowSequenceGrid.edit(rowitem);

                rowitem.setValue(0, draggedItem.getMember("ActorId").get_text());
                rowitem.setValue(1, draggedItem.getMember("ActorName").get_text());
                rowitem.setValue(2, draggedItem.getMember("ActorType").get_text());
                rowitem.setValue(3, draggedItem.getMember("RoleId").get_text());
                rowitem.setValue(4, draggedItem.getMember("RoleName").get_text());

                workflowSequenceGrid.editComplete();
                workflowSequenceGrid.select(rowitem); //                
                workflowSequenceGrid.render();
            }
        }
    }
}
function MoveUp_onclick() {
    var recordcount = workflowSequenceGrid.get_recordCount();
    if (recordcount != 0) {
        var draggedItem; var draggedItemIndex;
        draggedItem = workflowSequenceGrid.GetSelectedItems()[0];
        var draggedItemLength = workflowSequenceGrid.GetSelectedItems().length;
        if (draggedItemLength > 0) {
            draggedItemIndex = workflowSequenceGrid.GetSelectedItems()[0].get_index();
            if (draggedItemIndex != 0) {
                var actorId = draggedItem.getMember("ActorId").get_text();
                var actorName = draggedItem.getMember("ActorName").get_text();
                var actorType = draggedItem.getMember("ActorType").get_text();
                var roleId = draggedItem.getMember("RoleId").get_text();
                var roleName = draggedItem.getMember("RoleName").get_text();
                var rowItemIndex = draggedItemIndex - 1;
                workflowSequenceGrid.deleteItem(draggedItem);
                workflowSequenceGrid.unSelect(draggedItem); //deleting row is unselect and new will be select
                var rowitem = workflowSequenceGrid.get_table().addEmptyRow(rowItemIndex);

                workflowSequenceGrid.edit(rowitem);

                rowitem.setValue(0, draggedItem.getMember("ActorId").get_text());
                rowitem.setValue(1, draggedItem.getMember("ActorName").get_text());
                rowitem.setValue(2, draggedItem.getMember("ActorType").get_text());
                rowitem.setValue(3, draggedItem.getMember("RoleId").get_text());
                rowitem.setValue(4, draggedItem.getMember("RoleName").get_text());

                workflowSequenceGrid.editComplete();
                workflowSequenceGrid.select(rowitem);
                workflowSequenceGrid.render();
            }
        }
    }
}
function workflowSelection(objRbt, objRbt1, objDrp, objGrid, objTxt, objChk, objSaveWorkflowTxt) {

    if (objRbt.checked) {
        objDrp.selectedIndex = 0;
        objDrp.disabled = true;
        objTxt.value = "";
        objChk.checked = false;
        var count = objGrid.get_recordCount();
        objGrid.get_table().clearData()
        objGrid.render();
        objSaveWorkflowTxt.value = "";
    }
    if (objRbt1.checked) {
        objDrp.selectedIndex = 0;
        objDrp.disabled = false;
    }

}
function ExistingWorkflow(objRbt1, objDrp) {

    if (objRbt1.checked) {
        //objDrp.selectedIndex = 0;
        objDrp.disabled = false;
    }
}
function EnableSkipUserDays(objCheckbox, objTextbox1) {
    var str = objCheckbox.checked;
    if (str == true) {
        objTextbox1.disabled = false;
        objTextbox1.value = 1;
        objTextbox1.focus();
    }
    else {
        objTextbox1.disabled = true;
        objTextbox1.value = "";
    }
}
function validate(obj, objchk, objTxt) {
    var chk = objchk.value;
    var txt = objTxt.value;
    if ((chk) && (txt == "")) {
        obj.focus();

    }
}
function CheckPublisher(e, objbtn, objSaveWorkflowTxt, objGrid, objCheckbox, objTextbox1) {
    var count = objGrid.get_recordCount();
    var success = true;
    var errormessage = "";
    if (count != "") {
        var lastItem = objGrid.get_table().get_data()[count - 1][3];
        if (lastItem != 3) {
            if (lastItem != 9) {
                if (lastItem != 4) {
                    //alert(GetMessage("LastSequenceUser"));
                    errormessage = errormessage + GetMessage("LastSequenceUser") + "\n";
                    if (browser == "ie") {
                        objSaveWorkflowTxt.focus();
                        event.returnValue = false;
                    }
                    else {
                        objSaveWorkflowTxt.focus();
                        e.preventDefault();
                    }
                    success = false;
                }
            }
        }
    }
    else {
        //alert(GetMessage("EmptyWorkflow"));
        errormessage = errormessage + GetMessage("EmptyWorkflow") + "\n";
        if (browser == "ie") {
            objSaveWorkflowTxt.focus();
            event.returnValue = false;
        }
        else {
            objSaveWorkflowTxt.focus();
            e.preventDefault();
        }
        success = false;
    }
    var str = objCheckbox.checked;
    if (str == true) {
        if (objTextbox1.value == "") {
            //alert(GetMessage("EmptySkipUserDays")); 
            errormessage = errormessage + GetMessage("EmptySkipUserDays") + "\n";
            if (browser == "ie") {
                objTextbox1.focus();
                event.returnValue = false;
            }
            else {
                objTextbox1.focus();
                e.preventDefault();
            }
            success = false;
        }
        else {
            if (objTextbox1.value == "00" || objTextbox1.value == "0") {
                errormessage = errormessage + GetMessage("ValidSkipDays") + "\n";
                if (browser == "ie") {
                    objTextbox1.focus();
                    event.returnValue = false;
                }
                else {
                    objTextbox1.focus();
                    e.preventDefault();
                }
                success = false;
            }
        }
    }
    if (errormessage != "") {
        alert(errormessage);
    }
    //overright confirmation        
    var workflowname = objSaveWorkflowTxt.value;
    var workflowexist = false;
    if (workflowname == "") {
        //alert(GetMessage("EmptyWorkflowName"));
        if (browser == "ie") {
            objSaveWorkflowTxt.focus();
            event.returnValue = false;
        }
        else {
            objSaveWorkflowTxt.focus();
            e.preventDefault();
        }
        success = false;
    }
    else {
        workflowexist = isWorkflowExist(workflowname);
        if (workflowexist == "True") {
            if (!confirm(GetMessage("OverwriteWorkflow"))) {
                if (browser == "ie") {
                    objSaveWorkflowTxt.focus();
                    event.returnValue = false;
                }
                else {
                    objSaveWorkflowTxt.focus();
                    e.preventDefault();
                }
                success = false;
            }
        }
    }
    if (success == true) {
        return true;
    }
}

function EditWorkflowCheck(e, objbtn, objSaveWorkflowTxt, objGrid, objCheckbox, objTextbox1) {
    var count = objGrid.get_recordCount();
    var success = true;
    var errormessage = "";
    if (count != "") {
        var lastItem = objGrid.get_table().get_data()[count - 1][3];
        if (lastItem != 3) {
            if (lastItem != 9) {
                if (lastItem != 4) {
                    //alert(GetMessage("LastSequenceUser"));
                    errormessage = errormessage + GetMessage("LastSequenceUser") + "\n";
                    objSaveWorkflowTxt.focus();
                    success = false;
                }
            }
        }

        var firstItemRoleId = objGrid.get_table().get_data()[0][3];
        if (firstItemRoleId == 37) {
            //first item can't be Translation Service
            errormessage = errormessage + GetMessage("TranslationServiceUserFirst") + "\n";
            success = false;
        }
        if (firstItemRoleId == 38) {
            //first item can't be Translation Admin
            errormessage = errormessage + GetMessage("TranslationAdminUserFirst") + "\n";
            success = false;
        }
    }
    else {
        //alert(GetMessage("EmptyWorkflow"));
        errormessage = errormessage + GetMessage("EmptyWorkflow") + "\n";
        objSaveWorkflowTxt.focus();
        success = false;
    }
    var str = objCheckbox.checked;
    if (str == true) {
        if (objTextbox1.value == "") {
            //alert(GetMessage("EmptySkipUserDays")); 
            errormessage = errormessage + GetMessage("EmptySkipUserDays") + "\n";
            objTextbox1.focus();
            success = false;
        }
        else {
            if (objTextbox1.value == "00" || objTextbox1.value == "0") {
                errormessage = errormessage + GetMessage("ValidSkipDays") + "\n";
                objTextbox1.focus();
                success = false;
            }
        }
    }
    if (errormessage != "") {
        alert(errormessage);
    }
    //overright confirmation
    var workflowexist = false;
    var workflowname = objSaveWorkflowTxt.value;
    //       if(objcbSaveWorkflowChk.checked==true)
    //       { 
    if (workflowname == "") {
        //alert(GetMessage("EmptyWorkflowName"));
        objSaveWorkflowTxt.focus();

        success = false;
    }
    else {
        if (workflowname != objSaveWorkflowTxt.value) {
            workflowexist = isWorkflowExist(workflowname);
            if (workflowexist == "True") {
                if (!confirm(GetMessage("OverwriteWorkflow"))) {
                    objSaveWorkflowTxt.focus();

                    success = false;
                }
            }
        }
    }
    //        }     
    return success;
}
//
function SubmitWorkflowCheck(e, objbtn, objSaveWorkflowTxt, objGrid, objCheckbox, objTextbox1, objWorkflowDrdn) {
    var success = true;
    var errormessage = "";
    var workflowdropdwncount = objWorkflowDrdn.length;
    var workflowexist = false;
    var workflowname = objSaveWorkflowTxt.value;
    var count = objGrid.get_recordCount();
    //
    if (workflowname == "") {
        errormessage = errormessage + GetMessage("EmptyWorkflowName") + "\n";
        if (browser == "ie") {
            objSaveWorkflowTxt.focus();
            event.returnValue = false;
        }
        else {
            objSaveWorkflowTxt.focus();
            e.preventDefault();
        }
        success = false;
    }
    if (count != "") {
        var lastItem = objGrid.get_table().get_data()[count - 1][3];
        if (lastItem != 3) {
            if (lastItem != 9) {
                if (lastItem != 4) {
                    //alert(GetMessage("LastSequenceUser"));
                    errormessage = errormessage + GetMessage("LastSequenceUser") + "\n";
                    if (browser == "ie") {
                        objbtn.focus();
                        event.returnValue = false;
                    }
                    else {
                        objbtn.focus();
                        e.preventDefault();
                    }
                    success = false;
                }
            }
        }
    }
    else {
        errormessage = errormessage + GetMessage("EmptyWorkflow") + "\n";
        if (browser == "ie") {
            objbtn.focus();
            event.returnValue = false;
        }
        else {
            objbtn.focus();
            e.preventDefault();
        }
        success = false;
    }
    var str = objCheckbox.checked;
    if (str == true) {
        if (objTextbox1.value == "") {
            errormessage = errormessage + GetMessage("EmptySkipUserDays") + "\n";
            if (browser == "ie") {
                objTextbox1.focus();
                event.returnValue = false;
            }
            else {
                objTextbox1.focus();
                e.preventDefault();
            }
            success = false;
        }
        else {
            if (objTextbox1.value == "00" || objTextbox1.value == "0") {
                errormessage = errormessage + GetMessage("ValidSkipDays") + "\n";
                if (browser == "ie") {
                    objTextbox1.focus();
                    event.returnValue = false;
                }
                else {
                    objTextbox1.focus();
                    e.preventDefault();
                }
                success = false;
            }
        }
    }
    if (errormessage != "") {
        alert(errormessage);
    }
    else {
        if (workflowname != "") {
            var WFIndex = objWorkflowDrdn.selectedIndex;
            if (objWorkflowDrdn[WFIndex].text != workflowname) {
                workflowexist = isWorkflowExist(workflowname);
                if (workflowexist == "True") {
                    if (!confirm(GetMessage("OverwriteWorkflow"))) {
                        if (browser == "ie") {
                            objSaveWorkflowTxt.focus();
                            event.returnValue = false;
                        }
                        else {
                            objSaveWorkflowTxt.focus();
                            e.preventDefault();
                        }
                        success = false;
                    }
                }
            }
        }
    }
    if (success == true) {
        return true;
    }
    //
    /*      if(count!="")
    {
    if(objWorkflowDrdn.value=="Please select" && objSaveWorkflowTxt.value=="")
    {
    errormessage=errormessage+GetMessage("EmptyWorkflowName")+"\n";
    if(browser == "ie")
    {
    objbtn.focus();
    event.returnValue = false;
    }
    else
    {
    objbtn.focus();
    e.preventDefault();                           
    }
    success=false;  
    }
    var lastItem=objGrid.get_table().get_data()[count-1][3];
    if(lastItem != 3) 
    {
    if(lastItem != 9)
    { 
    if(lastItem != 4)
    {
    //alert(GetMessage("LastSequenceUser"));
    errormessage=errormessage+GetMessage("LastSequenceUser")+"\n";
    if(browser == "ie")
    {
    objbtn.focus(); 
    event.returnValue = false;
    }
    else
    {
    objbtn.focus(); 
    e.preventDefault();                           
    }
    success=false;
    }
    }          
    }
            
    }        
    else
    {
    //alert(GetMessage("EmptyWorkflow"));
    if(objWorkflowDrdn.value=="Please select" && objSaveWorkflowTxt.value!="")
    {
    errormessage=errormessage+GetMessage("EmptyWorkflow")+"\n";
    if(browser == "ie")
    {
    objbtn.focus();
    event.returnValue = false;
    }
    else
    {
    objbtn.focus();
    e.preventDefault();                           
    }                 
                 
    success=false;
    }
    }
    var str = objCheckbox.checked;
    if(str==true)
    { 
    if(objTextbox1.value=="")
    {
    //alert(GetMessage("EmptySkipUserDays")); 
    errormessage=errormessage+GetMessage("EmptySkipUserDays")+"\n";
    if(browser == "ie")
    {
    objTextbox1.focus();
    event.returnValue = false;
    }
    else
    {
    objTextbox1.focus();
    e.preventDefault();                           
    }               
    success=false;
    } 
    }   
    if(errormessage!="")
    {  
    alert(errormessage);
    }     
    //overright confirmation
    var workflowexist=false;
    var workflowname=objSaveWorkflowTxt.value;       
    //       if(objcbSaveWorkflowChk.checked==true)
    //       { 
    if(workflowname=="")
    {
    //TO DO validation for workflowname
    errormessage=errormessage+GetMessage("EmptyWorkflowName")+"\n";
    alert(errormessage);
    if(browser == "ie")
    {
    objSaveWorkflowTxt.focus();
    event.returnValue = false;
    }
    else
    {
    objSaveWorkflowTxt.focus();
    e.preventDefault();                           
    }   
    success=false;           
    }
    else
    {//TO DO check workflow name and selected workflow name if not do this 
    var WFIndex=objWorkflowDrdn.selectedIndex;
    if(objWorkflowDrdn[WFIndex].text!=workflowname)
    {
    workflowexist=isWorkflowExist(workflowname);
    if(workflowexist=="True")
    { 
    if ( !confirm(GetMessage("OverwriteWorkflow" )))                   
    {    
    if(browser == "ie")
    {
    objSaveWorkflowTxt.focus();
    event.returnValue = false;
    }
    else
    {
    objSaveWorkflowTxt.focus();
    e.preventDefault();                           
    }
    success=false;                       
    }
    } 
    }
    } 
    //        } 
    //        else
    //        {
    //           if(errormessage!="")
    //           {
    //                if(objcbSaveWorkflowChk.checked==true)
    //                { 
    //                    if(workflowname=="")
    //                    {
    //                        errormessage=errormessage+GetMessage("EmptyWorkflowName")+"\n";
    //                    }
    //                }
    //                alert(errormessage);
    //           }  
    //        }        
    //           if(errormessage!="")
    //           {    
    //                if(workflowname=="")
    //                {
    //                    errormessage=errormessage+GetMessage("EmptyWorkflowName")+"\n";
    //                }
    //                alert(errormessage);
    //           }  
    if (success==true)
    {
    return true;
    }*/
}
//
function CreateWorkflowClearFields(e, objSequenceGrid, objSkipuserTxt, objSkipUserChk, objSaveWorkflowTxt, objWorkflowDrdn, objNewRBt, objExistingRBt) {

    if (!confirm(GetMessage("ClearCreateWorkflow"))) {
        if (browser == "ie") {
            objSaveWorkflowTxt.focus();
            event.returnValue = false;
        }
        else {
            objSaveWorkflowTxt.focus();
            e.preventDefault();
        }
    }
    else {
        objSequenceGrid.get_table().clearData()
        objSequenceGrid.render();
        objSkipuserTxt.value = "";
        objSkipUserChk.checked = false;
        objSaveWorkflowTxt.value = "";
        objWorkflowDrdn.selectedIndex = 0;
        //objNewRBt.checked=true;  
        if (browser == "ie") {
            objSaveWorkflowTxt.focus();
            event.returnValue = false;
        }
        else {
            objSaveWorkflowTxt.focus();
            e.preventDefault();
        }

    }
}
function EditWorkflowClearFields(e, objSequenceGrid, objSkipuserTxt, objSkipUserChk, objSaveWorkflowTxt, objSaveWorkflowChk) {
    //clear the fields on click of clear form button
    if (!confirm(GetMessage("ClearEditWorkflow"))) {
        if (browser == "ie") {
            objSaveWorkflowTxt.focus();
            event.returnValue = false;
        }
        else {
            objSaveWorkflowTxt.focus();
            e.preventDefault();
        }
    }
    /*else
    { 
    var i,actorid;
    var count=objSequenceGrid.RecordCount;
    var actorids;
    actorids= document.getElementById(NewlyAdded).value;
    var actoridsArray;
    var item;
    actoridsArray=actorids.split(',');
    for(i=count-1;i>=0;i--)
    {
    actorid=objSequenceGrid.get_table().get_data()[i][0];
                
    if(actoridsArray[i]!= actorid)          
    {
    item=objSequenceGrid.get_table().getRow(i);
    objSequenceGrid.deleteItem(item);
    }
    }
    objSequenceGrid.render();
    objSkipuserTxt.value="";
    objSkipUserChk.checked=false;
    objSaveWorkflowTxt.value="";
    objSaveWorkflowChk.checked=false;
    if(browser == "ie")
    {
    objSaveWorkflowTxt.focus();
    event.returnValue = false;
    }
    else
    {
    objSaveWorkflowTxt.focus();
    e.preventDefault();                           
    }
    } */
}
//redirection from submit workflow,publish workflow to editor and WFworkflowAssignToMenu page to manageworkflow page these alert will come. 
function RedirectToEditor(e, objbtn) {
    if (!confirm(GetMessage("CancelRequest"))) {
        if (browser == "ie") {
            objbtn.focus();
            event.returnValue = false;
        }
        else {
            objbtn.focus();
            e.preventDefault();
        }
    }
}
function bindWorkflowSequence(objWorkflowDrdn, objSequenceGrid, objSkipUserTxt, objSkipUserChk) {
    var selectedWorkflow = objWorkflowDrdn.value;
    OnWorkflowChanged(selectedWorkflow, objSequenceGrid);
}

//
/************************Drag And Drop And ArrowClick start**********************************/
function PopUp() {
    UpdatePermissionPopup = dhtmlmodal.open('Content', 'iframe', '../../AssignWorkflowtoMenuWarning.aspx',
        __JSMessages["UpdatePermissionPopup"], 'width=410px,height=435px,center=1,resize=0,scrolling=0');
    UpdatePermissionPopup.onclose = function () {
        var a = document.getElementById('Content');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        return true;
    }
}
var draggedItem;
var draggedItemLength;
var workflowId;
var workflowName;
var selectedMenuNode;
var menuId;
var menu;
var result;
var itemSelected;
var workflowhidden = "";
var workflowIdFromGrid;
var target;
function AddWorkflowNode_onclick(sender, eventArgs)//sender, eventArgs this is new
{
    if (eventArgs != null && eventArgs.get_target() != null) {
        draggedItem = eventArgs.get_item();
        target = eventArgs.get_target(); //new                 
        if (target.get_selectable() == false) {
            var oldNode = menuTree.get_selectedNode();
            if (oldNode != null) {
                menuTree.SelectedNode = null;
                menuTree.render();
            }
            alert(GetMessage("SelectDestination"));
            return false;
        }
        else {
            menuTree.SelectNode(target); //new
        }
    }
    else {
        draggedItem = ExistingWorkflowGrid.GetSelectedItems()[0];
    }
    if (draggedItem != null) {

        workflowId = draggedItem.getMember("WorkflowId").get_text();
        workflowIdFromGrid = "W-" + workflowId;
        workflowName = draggedItem.getMember("WorkflowName").get_text();
        if (menuTree.get_selectedNode() != null) {
            selectedMenuNode = menuTree.get_selectedNode();
            menuId = selectedMenuNode.get_id();
            menu = menuId.substring(0, 2);
            if (menu == "M-") {
                AddWorkflowToOneNode();
                if (window.confirm(GetMessage("PropagateWorkflowToChildMenus"))) {
                    AddWorkflowToChildNodes(menuTree.get_selectedNode());
                }
                menuTree.render();
            }
            else {
                alert(GetMessage("SelectMenu"));
            }
        }
        else {
            alert(GetMessage("SelectMenu"));
        }
    }
    else {
        alert(GetMessage("SelectSource"));
    }
}
function AddWorkflowToChildNodes(objSelected) {
    var count = objSelected.get_nodes().get_length();
    var nodesArray = new Array(count);
    for (var i = 0; i < count; i++) {
        //node.getnodes returns newly added workflow. but the count variable cannot modify inside of the loop. so we are using array here 
        nodesArray[i] = objSelected.get_nodes().getNode(i);
    }
    for (var i = 0; i < count; i++) {
        var childNode = nodesArray[i]; //objSelected.get_nodes().getNode(i);
        if (childNode != null) {
            var childNodeId = childNode.get_id();
            if (childNodeId.substring(0, 2) == "M-") {
                menuTree.SelectNode(childNode);
                selectedMenuNode = menuTree.get_selectedNode();
                menuId = selectedMenuNode.get_id();
                AddWorkflowToOneNode();
                objSelected = menuTree.get_selectedNode();
                AddWorkflowToChildNodes(objSelected);
            }
        }
    }
}
function AddWorkflowToOneNode() {
    var count = menuTree.get_selectedNode().get_nodes().get_length();
    var isExist = false;
    for (var i = 0; i < count; i++) {
        wId = menuTree.get_selectedNode().get_nodes().getNode(i).get_id();
        if (workflowIdFromGrid == wId)//only for duplication checking but while adding we have to use only workflowid without "W-".
        {
            isExist = true;
        }
    }
    if (isExist == false) {
        result = checkActorsPermission(workflowId, menuId);
        if (result == "True") {
            PopUp();
        }
        else {
            var newNode = new ComponentArt.Web.UI.TreeViewNode();
            newNode.set_id(workflowIdFromGrid);
            newNode.set_text(workflowName);
            newNode.set_imageUrl('icon-workflow.gif');
            selectedMenuNode.set_expanded(true);
            selectedMenuNode.get_nodes().add(newNode);
            if (workflowhidden == "") {
                workflowhidden = menuId + "," + workflowId + ",0;";
            }
            else {
                workflowhidden = workflowhidden + menuId + "," + workflowId + ",0;";
            }
            document.getElementById(workflowNodeHolder).value = workflowhidden;
        }
    }
    //menuTree.render();  
}

function addNode(yesno) {
    if (yesno == "yes") {
        var newNode = new ComponentArt.Web.UI.TreeViewNode();
        newNode.set_id(workflowIdFromGrid);
        newNode.set_text(workflowName);
        newNode.set_imageUrl('icon-workflow.gif');
        selectedMenuNode.set_expanded(true);
        selectedMenuNode.get_nodes().add(newNode);
        if (workflowhidden == "") {
            workflowhidden = menuId + "," + workflowId + ",1;";
        }
        else {
            workflowhidden = workflowhidden + menuId + "," + workflowId + ",1;";
        }
        document.getElementById(workflowNodeHolder).value = workflowhidden;

        menuTree.render();
    }
}
function CheckNode(objbtn, hiddenField, rmHiddentFiled) {
    //workflowhidden is a global variable.because it will concatenate the string value.
    //            if(workflowhidden=="")
    var hidden = hiddenField.value;
    var rmHidden = rmHiddentFiled.value;
    if (hidden == "" && rmHidden == "") {
        alert(GetMessage("Nothing"));
        return false;
    }
}
/************************Drag And Drop And ArrowClick end**********************************/ 