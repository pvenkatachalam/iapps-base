﻿// JScript File
var globalPickerobject;
function onCalendarChange(calendar)
{
    globalPickerobject.SetSelectedDate(calendar.GetSelectedDate());
    enableTimeObjects(globalPickerobject);
}

function disableTimeObjects(pickerObject) {
    var timeObject = document.getElementById(pickerObject.Id.replace('Picker', 'Time'));
    if (timeObject != null) {
        var AMPMObject = document.getElementById(pickerObject.Id.replace('Picker', 'AMPM'));
        timeObject.disabled = true;
        AMPMObject.disabled = true;
    }
}
function enableTimeObjects(pickerObject) {
    var timeObject = document.getElementById(pickerObject.Id.replace('Picker', 'Time'));
    if (timeObject != null) {
        var AMPMObject = document.getElementById(pickerObject.Id.replace('Picker', 'AMPM'));
        timeObject.disabled = false;
        AMPMObject.disabled = false;
    }
}
function onButtonClick(calendarObject, pickerObject)
{
    globalPickerobject = pickerObject;
    //calendarObject.SetSelectedDate(pickerObject.GetSelectedDate());
    calendarObject.Show();
}
function clearCalendarDate(calendarObject, pickerObject) {
    calendarObject.ClearSelectedDate();
    pickerObject.ClearSelectedDate();

    var timeObject = document.getElementById(pickerObject.Id.replace('Picker', 'Time'));
    var AMPM = document.getElementById(pickerObject.Id.replace('Picker', 'AMPM'));

    if (timeObject != null) {
        disableTimeObjects(pickerObject);
        timeObject.options[0].selected = true;
        AMPM.options[0].selected = true;
    }
}
function Button_OnMouseUp(calendar)
{
    if (calendar.PopUpObjectShowing)
    {
      event.cancelBubble=true;
      event.returnValue=false;
      return false;
    }
    else
    {
      return true;
    }
}
function onCalendarLoad(sender, eventArgs)
{
    var pickerId = sender.CalendarId;
    var timeId = pickerId.replace('Picker', 'Time');
    window.onLoad = FillTime(document.getElementById(timeId), timeId);

    var timeObject = document.getElementById(pickerId.replace('Picker', 'Time'));
    if (timeObject != null) {
        if (sender.GetSelectedDate() == null) {
            disableTimeObjects(sender);
        }
    }
}
function FillTime(timeObject,timeId)
{
    var timeOption;
    var hourString;
    var selectedHour = 12;
    var selectedUnFormatTime= new Array('12','00');
    if (timeObject!=null)
    {
        var selectedDateTime =  timeObject.getAttribute('selectedTime');
        var splitDateTime = selectedDateTime.split(' ');
        if (splitDateTime.length > 1) {
            var selectedTime = splitDateTime[1]; //24 mode
            selectedUnFormatTime = selectedTime.split(':');
            if (selectedUnFormatTime[0].charAt(0) == '0') {
                selectedHour = parseInt(selectedUnFormatTime[0].substring(1, 2));
            }
            else {
                selectedHour = parseInt(selectedUnFormatTime[0]);
            }
        }
        var AMPM = "AM";
        if (selectedHour>12) 
        {
            selectedHour = selectedHour-12;
            AMPM = "PM"
        }
        else if(selectedHour == 12)
        {
            AMPM = "PM"
        }
        var selectedFormatTime;
        if (selectedHour <10)
        {
            if(selectedHour == 0)
            {
                selectedFormatTime = 12 + ":" + selectedUnFormatTime[1];
            }
            else
            {
                selectedFormatTime = "0"+ selectedHour + ":" + selectedUnFormatTime[1];
            }
        }
        else
        {
            selectedFormatTime = selectedHour + ":" + selectedUnFormatTime[1];
        }        
        
        for(var hourCount=0; hourCount<timeObject.options.length; hourCount++)
        {
                if (timeObject.options[hourCount].value == selectedFormatTime)
                {
                    timeObject.options[hourCount].selected =true;
                    break;
                }
        }
        var AMPMId = timeId.replace('Time','AMPM');
        var AMPMObject= document.getElementById(AMPMId);
        for(var count=0;count<AMPMObject.options.length ; count++)
        {
            if(AMPMObject.options[count].value == AMPM)
            {
                AMPMObject.options[count].selected =true;
            }
        }        
    }
}