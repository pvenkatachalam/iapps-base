﻿function UpdateSessionStatus() {
    // get session timeout value in minutes (it comes from server and is set to this window value in MasterPage.master
    var sessionTimeout = window["SessionTimeoutValue"]; // 1 minute less than the actual server session timeout
    if (window["IsSessionValid"] != false) {
        if (sessionTimeoutCounter >= sessionTimeout) // session has already expired
        {
            window["IsSessionValid"] = false;
        }
        else {
            var shouldResetSessionCounter = GetCookie('ResetSessionCounter');
            if (shouldResetSessionCounter == "yes") {
                sessionTimeoutCounter = 2;
                SetCookie('ResetSessionCounter', 'no');
            }
            else
                sessionTimeoutCounter = sessionTimeoutCounter + 1;

            window["IsSessionValid"] = true;
        }
    }
}

function IsThisSessionValid() {
    if (window["IsSessionValid"]) {
        DeleteCookie('ResetSessionCounter');
        return true;
    }
    return false;
}

var timerObj;
var sessionTimeoutCounter = 0;
window["IsSessionValid"] = true;
function SetTimer() {
    var dblMinutes = 1;       // every minute check call this function
    //set timer to call function to confirm update 
    timerObj = setInterval("UpdateSessionStatus()", 1000 * 60 * dblMinutes);
}

function CheckSession() {
    if (!IsThisSessionValid()) {
        window.location.reload();
        return false;
    }
    return true;
}

function CheckSessionEventHandler(sender, eventArgs) {
    if (!IsThisSessionValid()) {
        eventArgs.set_cancel(true);
        window.location.reload();
    }
}

var cacheResetPopup;
function openCacheResetPopup() {
    cacheResetPopup = dhtmlmodal.open('Content', 'iframe', '/Admin/Popups/ResetCachePopup.aspx', 'Cache Reset',
                                              'width=388px,height=234px,center=1,resize=0,scrolling=0');
    cacheResetPopup.onclose = function () {
        var a = document.getElementById('Content');
        var ifr = a.getElementsByTagName("iframe");
        window.frames[ifr[0].name].location.replace("/blank.html");
        return true;
    }
}

function convert(value) {
    var hours = parseInt(value);
    value -= parseInt(value);
    value *= 60;
    var mins = parseInt(value);
    value -= parseInt(value);
    value *= 60;
    var secs = parseInt(value);
    var display_hours = hours;
    // handle GMT case (00:00)
    if (hours == 0) {
        display_hours = "00";
    } else if (hours > 0) {
        // add a plus sign and perhaps an extra 0
        display_hours = (hours < 10) ? "+0" + hours : "+" + hours;
    } else {
        // add an extra 0 if needed 
        display_hours = (hours > -10) ? "-0" + Math.abs(hours) : hours;
    }

    mins = (mins < 10) ? "0" + mins : mins;
    return display_hours + ":" + mins;
}

/*
GENERIC CONTROL SUPPORT FUNCTIONS
*/

function enableControl(ctl, enabledState) {
    for (var n = 0; n < ctl.childNodes.length; n++)
        ctl.childNodes[n].disabled = !enabledState;

    ctl.disabled = !enabledState;
}


function getCookieValue(cookieName) {
    // upper case for comparison
    cookieName = cookieName.toUpperCase();

    // get cookie array
    var cookies = document.cookie.split(';');
    for (var n = 0; n < cookies.length; n++) {
        // split into name and value
        var pair = cookies[n].split('=');
        // if name matches, return value
        if ((pair[0].toUpperCase() == cookieName) && (pair.length > 1))
            return pair[1];
    }
    // if here, return empty string
    return "";
}


function getSessionId() {
    return getCookieValue('ASP.NET_SessionId');
}



//
// returns true/false if the string is a percent or not
//
function isPercent(strValue) {
    strValue = strValue.toString();
    return (strValue.substr(strValue.length - 1) == '%');
}

//
// converts a string representation of % value to the float equivalent
//
function convertToPercent(strValue) {
    strValue = strValue.toString();
    return (strValue.substr(0, (strValue.length - 1)) / 100);
}





/*
TEXTBOXEX VALIDATION FUNCTIONS
*/

// global flag indicating already in a javascript validation
var g_inValidationFlag = false;


function isEmpty(s) {
    return ((s == null) || (s.length == 0));
}

function trim(str) {
    return str.replace(/^\s*/, "").replace(/\s*$/, "");
}

function monthToNumber(theMonth) {

    theMonth = theMonth.toUpperCase();

    if (theMonth == "JAN")
        return "01";
    if (theMonth == "FEB")
        return "02";
    if (theMonth == "MAR")
        return "03";
    if (theMonth == "APR")
        return "04";
    if (theMonth == "MAY")
        return "05";
    if (theMonth == "JUN")
        return "06";
    if (theMonth == "JUL")
        return "07";
    if (theMonth == "AUG")
        return "08";
    if (theMonth == "SEP")
        return "09";
    if (theMonth == "OCT")
        return "10";
    if (theMonth == "NOV")
        return "11";
    if (theMonth == "DEC")
        return "12";

    return "";
}

function linkStringToDate(theString) {
    var firstpos1 = theString.indexOf("-");
    var secondpos1 = theString.indexOf("-", firstpos1 + 1);

    var d1 = theString.substring(0, firstpos1)// day
    var c1 = theString.substring(firstpos1, firstpos1 + 1)// '-'
    var m1 = theString.substring(firstpos1 + 1, secondpos1).toUpperCase()// month
    var e1 = theString.substring(secondpos1, secondpos1 + 1)// '-'
    var y1 = theString.substring(secondpos1 + 1, theString.length)// year

    var resultDate = new Date(y1, monthToNumber(m1) - 1, d1)

    return resultDate
}

function checkDate(s) {
    var dateString = s.value;
    var err = 0;

    if (dateString.length != 11) err = 1;

    if (err == 0) {
        var count = 0;
        pos = dateString.indexOf("-");
        while (pos != -1) {
            count++;
            pos = dateString.indexOf("-", pos + 1);
        }

        if (count != 2) err = 1;
    }
    if (err == 0) {
        firstpos = dateString.indexOf("-");
        secondpos = dateString.indexOf("-", firstpos + 1);
        b = dateString.substring(0, firstpos)// day
        if (!isInteger(b))
            err = 1
        c = dateString.substring(firstpos, firstpos + 1)// '/'
        d = dateString.substring(firstpos + 1, secondpos).toUpperCase()// month
        e = dateString.substring(secondpos, secondpos + 1)// '/'
        f = dateString.substring(secondpos + 1, dateString.length)// year
        if (!isInteger(f))
            err = 1
    }

    if (err == 0) {
        if (!(d == "JAN" || d == "FEB" || d == "MAR" || d == "APR" || d == "MAY" || d == "JUN" || d == "JUL" || d == "AUG" || d == "SEP" || d == "OCT" || d == "NOV" || d == "DEC"))
            err = 1

        if (b < 1 || b > 31) err = 1
        if (b.length > 2) err = 1
        if (c != '-') err = 1
        if (d.length > 3) err = 1
        if (d < 1 || d > 31) err = 1
        if (e != '-') err = 1
        if (f.length > 4) err = 1


        if (d == "APR" || d == "JUN" || d == "SEP" || d == "NOV") {
            if (b == 31) err = 1

            // february, leap year
            if (d == "FEB" || d == "feb") {
                var g = parseInt(f / 4)
                if (isNaN(g) && (f < 1 || f > 3))
                    err = 14
                if (b > 29) err = 1
                if (b == 29 && ((f / 4) != parseInt(f / 4))) err = 1
            }
        }

        if (err == 0) {
            if (f < 1900) err = 1
        }

        if (err == 1) {
            return false;
        }
        else {
            return true;
        }

    }
}

function isInteger(s) {
    var i;

    if (isEmpty(s))
        if (isInteger.arguments.length == 1) return true;
        else return (isInteger.arguments[1] == true);

    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.

    for (i = 0; i < s.length; i++) {
        // Check that current character is number.
        var c = s.charAt(i);

        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}

function isDigit(c) {
    return ((c >= "0") && (c <= "9"));
}

function isMakeIntegerOnly(str) {
    str = str.replace(/\s/g, "");
    str = str.replace(/[^0-9]/g, "");
    return str;
}

function isReFormat(s) {
    var arg;
    var sPos = 0;
    var resultString = "";

    for (var i = 1; i < isReFormat.arguments.length; i++) {
        arg = isReFormat.arguments[i];
        if (i % 2 == 1) resultString += arg;
        else {
            resultString += s.substring(sPos, sPos + arg);
            sPos += arg;
        }
    }
    return resultString;
}



function fnDateValidator(theField) {
    if (g_inValidationFlag)
        return true;

    var ret = true;

    g_inValidationFlag = true;

    try {
        // is valid date?	
        if (shouldValidate(theField) && !checkDate(theField)) {
            window.event.returnValue = false;

            theField.focus();
            theField.select();

            var message = getValidationErrorMsg(theField, __JSMessages["VALUEIsNotAValidDateUseTheFormatDDMONYEAR"]);
            alert(message);

            ret = false;
        }
    }
    finally {
        g_inValidationFlag = false;
    }

    return ret;
}


function fnAlphaNumericValidator(theField) {
    var regIsAlphaNum = /^[\_a-zA-Z0-9]+$/; // Accept AlphaNumeric only  
    var message = __JSMessages["VALUEIsInvalidAcceptableValues"];

    return validateByRegExp(theField, regIsAlphaNum, message);
}


function fnTextValidator(theField) {
    var regIsAlpha = /^[a-zA-Z]+$/; // Accept Alpha only  
    var message = __JSMessages["VALUEIsInvalidAcceptableValuesMayOnlyIncludeLetters"];

    return validateByRegExp(theField, regIsAlpha, message);
}


function fnEmailAddressValidator(theField) {
    var regIsEmailFormat = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;   // Accept Email Format with or without underscores and dashes
    var message = __JSMessages["VALUEIsNotAValidEmailAddress"];

    return validateByRegExp(theField, regIsEmailFormat, message);
}


function fnFloatValidator(theField) {
    if (g_inValidationFlag)
        return true;

    var ret = true;

    g_inValidationFlag = true;

    try {
        if (shouldValidate(theField)) {
            var regIsFloat = /^\-?\d+(\.\d*)?$/;  // positive or negative numbers with decimals
            var regIsAcctngFloat = /^\(\d+(\.\d*)?\)$/;  // negative numbers in accounting style

            if (!regIsFloat.test(theField.value) && !regIsAcctngFloat.test(theField.value)) {
                window.event.returnValue = false;

                theField.focus();
                theField.select();

                var message = getValidationErrorMsg(theField, __JSMessages["VALUEIsNotAValidNumber"]);
                alert(message);

                ret = false;
            }
        }
    }
    finally {
        g_inValidationFlag = false;
    }

    return ret;
}


function fnPositiveFloatValidator(theField) {
    var regIsFloat = /^\.?\d+(\.?\d*)?$/   // positive numbers with decimals
    var message = __JSMessages["VALUEIsNotAValidPositiveNumberOrZero"];

    return validateByRegExp(theField, regIsFloat, message);
}

function fnPositiveFloatNoZeroValidator(theField) {
    var regIsFloat = /^\.?\d+(\.?\d*)?$/;  // positive numbers with decimals
    var message = stringformat(__JSMessages["IsNotAValidPositiveNumber"], theField.value);
    var ret = true;
    if (shouldValidate(theField) && (!regIsFloat.test(theField.value) || (theField.value + 0) == 0)) {
        window.event.returnValue = false;

        theField.focus();
        theField.select();

        alert(message);

        ret = false;
    }
    return true;
}

function fnIntegerValidator(theField) {
    var regIsInt = /^\-?\d+$/;  // Accept positive or Negative Number only  
    var message = __JSMessages["VALUEIsNotAValidWholeNumber"];

    return validateByRegExp(theField, regIsInt, message);
}


function fnPositiveIntegerValidator(theField) {
    var regIsInt = /^\d+$/;  // Accept positive Number only  
    var message = __JSMessages["VALUEIsNotAValidPositiveWholeNumberOrZero"];

    return validateByRegExp(theField, regIsInt, message);
}


function fnPercentValidator(theField) {
    var regIsPct = /^(?:(?:100)|(?:\d?\d))$/;  // Accept integer between 0 and 100 only
    var message = __JSMessages["VALUEMustBeAWholeNumberBetween0And100"];

    return validateByRegExp(theField, regIsPct, message);
}




/*
TEXTAREA MAXLENGTH FUNCTIONALITY SUPPORT FUNCTIONS
*/

function limitMultiLineLength(ctlTextArea) {
    if (ctlTextArea.maxLength != "") {
        var eAny_Event = window.event;
        var iKey = eAny_Event.keyCode;
        //var re = new RegExp("\r\n", "g")   

        x = ctlTextArea.value.length;

        if ((x >= ctlTextArea.maxLength) && !(iKey == 8 && iKey == 48))
            window.event.returnValue = false;
    }
}


function limitPasteLength(ctlTextArea) {
    if (ctlTextArea.maxLength != "") {
        var selectedTxt = document.selection.createRange().text;
        var clipTxt = window.clipboardData.getData("Text");

        if ((ctlTextArea.innerText.length - selectedTxt.length + clipTxt.length) < ctlTextArea.maxLength) {
            event.returnValue = true;
        }
        else {
            document.selection.createRange().text = clipTxt;
            ctlTextArea.innerText = ctlTextArea.innerText.substr(0, parseInt(ctlTextArea.maxLength))
            event.returnValue = false;
        }
    }
}

function SetTaxonomyForList() {
    $(".view-tags").iAppsClickMenu({
        width: "150px",
        heading: __JSMessages["Tags"],
        enableLoadingPanel: true,
        create: function (e, menu) {
            var taxonomyNames = JSON.parse(GetTaxonomyJson(e.attr("objectId")));
            if (taxonomyNames.length > 0) {
                $.each(taxonomyNames, function () {
                    menu.addListItem(this.toString());
                });
            }
            else {
                menu.addListItem(__JSMessages["NoTagsAttached"]);
            }
        }
    });
}

function GetSelectedTaxonomyIds(indexTree) {
    var selectedTaxonomyIds = [];
    PopulateSelectedTaxonomyIds(indexTree.Nodes(), selectedTaxonomyIds);

    return JSON.stringify(selectedTaxonomyIds);
}

function PopulateSelectedTaxonomyIds(nodes, selectedTaxonomyIds) {
    if (nodes != null && nodes.length > 0) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].Checked) {
                selectedTaxonomyIds.push({ "Id": nodes[i].ID, "Title": nodes[i].get_text() });
            }

            var childNodes = nodes[i].Nodes();

            if (childNodes != null && childNodes.length > 0) {
                PopulateSelectedTaxonomyIds(childNodes, selectedTaxonomyIds);
            }
        }
    }
}

function showTemplateThumbnail(virtualPath, imageUrl, title) {
    var returnHtml;

    var fullImageUrl = "/Admin/App_Themes/General/images/no-template-preview.gif";
    var cssClass = "noImage";
    if (imageUrl != null && imageUrl != "" && virtualPath != null) {
        virtualPath = virtualPath.replace("~", "") + "/";
        fullImageUrl = virtualPath + imageUrl;
        cssClass = "";
    }
    returnHtml = '<div class="form-row template-preview ' + cssClass + ' clear-fix">';
    returnHtml += '<label class="form-label"><img src="' + fullImageUrl + '" alt="" /></label>';
    returnHtml += '<div class="form-value">' + title + '</div></div>';

    return returnHtml;
}

$(function () {
    $(".preview-options .content-view-button").on("click", function () {
        $(this).parents(".modal-content").find(".content-view").show();
        $(this).parents(".modal-content").find(".html-view").hide();

        $(this).addClass("selected");
        $(this).siblings().removeClass("selected");
    });

    $(".preview-options .html-view-button").on("click", function () {
        $(this).parents(".modal-content").find(".html-view").show();
        $(this).parents(".modal-content").find(".content-view").hide();

        $(this).addClass("selected");
        $(this).siblings().removeClass("selected");
    });

    $(".use-master-url input").on("click", function () {
        if (this.checked) {
            $("#txtPrimaryURL").val($("#hdnMasterPrimaryUrl").val());
            $("#txtURL").val($("#hdnMasterUrls").val());

            $("#txtPrimaryURL").prop("readonly", true);
            $("#txtURL").prop("readonly", true);
        }
        else {
            $("#txtPrimaryURL").val($("#hdnPrimaryUrl").val());
            $("#txtURL").val($("#hdnUrls").val());

            $("#txtPrimaryURL").attr("readonly", false);
            $("#txtURL").attr("readonly", false);
        }
    });
});

function ValidateHeaderSearch(btnSearch) {
    var txtSearch = $("#" + btnSearch.id).siblings("input[type='text']");
    if (txtSearch.val() == "" || /^[^<>]+$/.test(txtSearch.val()) == false) {
        alert(__JSMessages["PleaseEnterASearchTermSearchTermsCannotContainTheSpecialCharactersLtOrGt"]);
        return false;
    }

    return true;
}

function SetBlogNavLinks(blogId) {
    $(".toggle-links a").each(function () {
        var linkUrl = $(this).prop("href");
        if (linkUrl.indexOf('?') > -1)
            linkUrl = linkUrl.substring(0, linkUrl.indexOf('?'));
        $(this).prop("href", linkUrl + "?BlogId=" + blogId);
    });
}
