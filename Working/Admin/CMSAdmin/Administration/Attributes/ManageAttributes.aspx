<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    Theme="General" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageAttributes" CodeBehind="ManageAttributes.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="Attributes" Src="~/UserControls/Administration/ManageAttributes.ascx" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="page-header-section clear-fix">
        <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.ManageAttributes  %></h1>
        <a href="ManageAttributeCategory.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, AttributeCategories %>" /></a>
    </div>
    <UC:Attributes ID="ucAttributes" runat="server" />
</asp:Content>
