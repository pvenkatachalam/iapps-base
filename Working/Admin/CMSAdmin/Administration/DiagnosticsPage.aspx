﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="DiagnosticsPage.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.DiagnosticsPage"
    StylesheetTheme="General" ClientIDMode="Static" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="page-header clear-fix">
        <h1><%= GUIStrings.DiagnosticsPage %></h1>
    </div>
    <asp:Repeater ID="rptGroup" runat="server">
        <ItemTemplate>
            <div class="dark-section-header clear-fix">
                <h2><%# Container.DataItem %></h2>
            </div>
            <asp:Repeater ID="rptTests" runat="server">
                <ItemTemplate>
                    <div class="<%# DataBinder.Eval(Container.DataItem, "Result") %>">
                        <h3><%# DataBinder.Eval(Container.DataItem, "Name")%></h3>
                        <p>
                            <%# DataBinder.Eval(Container.DataItem, "Description")%>
                        </p>
                        <p>
                            <%# DataBinder.Eval(Container.DataItem, "OutputMessage")%>
                            <%# DataBinder.Eval(Container.DataItem, "HelpText")%>
                            <%# DataBinder.Eval(Container.DataItem, "Link")%> 
                        </p>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
