<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="false" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageSites" StylesheetTheme="general"
    CodeBehind="ManageSites.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#divBackup").iAppsDialog({ appendToEnd: true });
        });

        function grdSite_OnLoad(sender, eventArgs) {
            $(".manage-site").gridActions({ objList: grdSite });
        }

        function grdSite_OnRenderComplete(sender, eventArgs) {
            $(".manage-site").gridActions("bindControls");
        }

        function grdSite_OnCallbackComplete(sender, eventArgs) {
            $(".manage-site").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;
            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageSiteDetails", "", "CloseManageSiteDetails");
                    break;
                case "Edit":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageSiteDetails", "SiteId=" + selectedItemId, "CloseManageSiteDetails");
                    break;
                case "EditAttributes":
                    doCallback = false;
                    OpeniAppsAdminPopup("AssignAttributes", "CategoryId=1&ObjectId=" + selectedItemId);
                    break;
                case "Delete":
                    doCallback = window.confirm("<%= GUIStrings.YouAreAboutToDeleteThisSite %>");
                    break;
                case "CopySite":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageSiteDetails", "CopySite=true&SiteId=" + selectedItemId, "CloseManageSiteDetails");
                    break;
                case "Backup":
                    doCallback = false;
                    $("#divBackup").iAppsDialog("open");
                    break;
                case "BackupHistory":
                    doCallback = false;
                    break;
            }

            if (doCallback) {
                grdSite.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdSite.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            if (eventArgs.selectedItem.getMember("Id").get_text().toLowerCase() == jAppId) {
                sender.getItemByCommand("Delete").hideButton();
            }
            else {
                sender.getItemByCommand("Delete").showButton();
            }
        }

        function BackupSite() {
            $("#divBackup").iAppsDialog("close");

            gridActionsJson.CustomAttributes["BackupComments"] = $("#txtComments").val();

            $(".manage-site").gridActions("callback", "Backup");
        }

        function CloseManageSiteDetails() {
            $(".manage-site").gridActions("callback");
        }

        function CancelBackupSite() {
            $("#divBackup").iAppsDialog("close");
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manage-site">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdSite" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
            CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdSiteLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdSite_OnLoad" />
                <CallbackComplete EventHandler="grdSite_OnCallbackComplete" />
                <RenderComplete EventHandler="grdSite_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="CreatedDate" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                        <ComponentArt:GridColumn DataField="PrimarySiteUrl" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["PrimarySiteUrl"]%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong> : {1} {2} {3}", GUIStrings.Created, String.Format("{0:d}", Container.DataItem["CreatedDate"]),
                                        GUIStrings.By, Container.DataItem["CreatedByFullName"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdSiteLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdSite) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
      <div class="iapps-modal backup-site" id="divBackup" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, BackupSiteComments %>" /></h2>
        </div>
        <div class="modal-content">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Comments %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtComments" runat="server" CssClass="textBoxes" Width="360" TextMode="MultiLine" ClientIDMode="Static" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnBackup" Text="<%$ Resources:GUIStrings, BackupSite %>" CssClass="primarybutton"
                runat="server" title="<%$ Resources:GUIStrings, BackupSite %>" OnClientClick="return BackupSite();" />
            <input type="button" onclick="return CancelBackupSite();" class="button" value="<%= GUIStrings.Cancel %>" />
        </div>
    </div>
</asp:Content>
