﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master"
    AutoEventWireup="true" ValidateRequest="false" CodeBehind="SiteSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SiteSettings" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" ValidationGroup="callback"
        OnClick="btnSave_Onclick" />
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
        ValidationGroup="callback" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="global-settings">
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.AdminEmail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.AdminEmailTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtAdminEmail" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.AnalyticsProductId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.AnalyticsProductIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAnalyticsProductId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.AnalyticsAdminVDName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.AnalyticsAdminVDNameTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAnalyticsAdminVDName" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.AnalyticsProductKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.AnalyticsProductKeyTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAnalyticsProductKey" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CacheEmail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CacheEmailTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtcacheemail" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CMSAdminVDName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CMSAdminVDNameTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtCMSAdminVDName" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CMSProductId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CMSProductIdTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtCMSProductId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CMSProductKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CMSProductKeyTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtCMSProductKey" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CommonLoginVD %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CommonLoginVDTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtCommonLoginVD" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CommerceAdminVDName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CommerceAdminVDNameTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txteCommerceAdminVDName" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CommerceProductKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CommerceProductKeyTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txteCommerceProductKey" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.CommerceProductId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.CommerceProductIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCommerceProductId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.ComponentArtScriptControls %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.ComponentArtScriptControlsTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtComponentArtScriptControls" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.ContextMenuXml %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.ContextMenuXmlTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtContextMenuXml" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.DefaultSecuredImageUrl %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= SiteSettings.DefaultSecuredImageUrlTooltip %>"
                    alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtDefaultSecuredImageUrl" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.Editortouse %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.EditortouseTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEditor" runat="server">
                    <asp:ListItem Text="RadEditor" Value="RadEditor"></asp:ListItem>
                    <asp:ListItem Text="CKEditor" Value="CKEditor"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.EnableAssetFolderSecurity%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.EnableAssetFolderSecurityTooltip %>" alt="<%= SiteSettings.EnableAssetFolderSecurityTooltip %>" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableAssetFolderSecurity" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" Selected="True" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.EnableAuthenticationForSecureObjects %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.EnableAuthenticationForSecureObjectsTooltip %>" alt="<%= SiteSettings.EnableAuthenticationForSecureObjectsTooltip %>" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableAuthenticationForSecureObjects" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" Selected="True" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.EnableOutputCache %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.EnableOutputCacheTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:CheckBox ID="chkEnableOutputCache" runat="server" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.EventLogApplicationKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.EventLogApplicationKeyTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFrontEndEventLogApplicationKey" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.FilesToOpenInBrowser %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.FilesToOpenInBrowserTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFilesToOpenInBrowser" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>        
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.iAPPS_Version %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.iAPPS_VersionTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtiAPPS_Version" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.iAppsDateFormat %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.iAppsDateFormatTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtiAppsDateFormat" runat="server" CssClass="disabled" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.IAppsSystemUserId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.IAppsSystemUserIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtiAPPSUser" runat="server" CssClass="textBoxes" ReadOnly="false" />
                        <asp:RegularExpressionValidator ID="retxtiAPPSUser" runat="server" ControlToValidate="txtiAPPSUser"
                            ValidationExpression="^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$"
                            ValidationGroup="callback" ErrorMessage="Please enter a valid Guid in 'iAPPS System User' field"
                            Text="*" />
                    </div>
        </div>        
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.InvisibleTreeViewNodeCssClass %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.InvisibleTreeViewNodeCssClassTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtInvisibleTreeViewNodeCssClass" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.InvisibleSelectedNodeCssClass %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.InvisibleSelectedNodeCssClassTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtInvisibleSelectedNodeCssClass" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.ISYSBinPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.ISYSBinPathTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtISYSBinPath" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.ISYSLicenseKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.ISYSLicenseKeyTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtISYSLicenseKey" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.ISYSProductSearchIndex %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.ISYSProductSearchIndexTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtISYSProductSearchIndex" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryInclude %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryIncludeTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddljQueryInclude" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryUseCDN %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryCDNTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddljQueryUseCDN" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryCDNPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryCDNPathTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtjQueryCDNPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryCDNPathForAdmin %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryCDNforAdminTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtjQueryCDNAdminPath" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryUIInclude %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryUIIncludeTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddljQueryUIInclude" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryUIUseCDN %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryUICDNTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddljQueryUIUseCDN" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryUICDNPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryUICDNPathTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtjQueryUICDNPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.JQueryUICDNPathForAdmin %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.JQueryUICDNforAdminTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtjQueryUICDNAdminPath" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.MarketierAdminVD %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.MarketierAdminVDTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtMarketierAdminVD" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.MarketierProductId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.MarketierProductIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtMarketierProductId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.MarketierProductKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.MarketierProductKeyTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtMarketierProductKey" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.PageAccessXml %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.PageAccessXmlTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtPageAccessXml" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.PublicSiteURL %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.PublicSiteURLTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtPublicSiteURL" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.PublicSiteEnableDebugInfo %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.PublicSiteEnableDebugInfoTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:DropDownList ID="ddlEnableDebugInfo" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.RenderControlsAttributesInLive %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.RenderControlsAttributesInLiveTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlRenderControlsAttributesInLive" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.RenderiAPPSSEODescription %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.RenderiAPPSSEODescriptionTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlRenderiAPPSSEODescription" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.SiteId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.SiteIdTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtSiteId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.UseCache %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.UseCacheTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlUseCache" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= SiteSettings.UseISYSForAdmin %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= SiteSettings.UseISYSForAdminTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlUseISYSForAdmin" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
         
    </div>
</asp:Content>
