﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master"
    AutoEventWireup="true" ValidateRequest="false" CodeBehind="SiteSettingsPage.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SiteSettingsPage" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">

</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="settings-content">
        <div class="global-settings">
            <asp:Repeater ID="rptSiteSetting" runat="server">
                <ItemTemplate>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Literal ID="ltName" runat="server" />
                            <asp:Image ID="imgName" runat="server" ImageUrl="~/App_Themes/General/images/icon-help.gif" />
                            <asp:HiddenField ID="hdnName" runat="server" />
                        </label>
                        <div class="form-value">
                            <asp:PlaceHolder ID="phControl" runat="server" />
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
