﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master"
    AutoEventWireup="true" CodeBehind="GeneralSettings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.GeneralSettings"
    StylesheetTheme="General" ValidateRequest="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" ValidationGroup="callback"
        OnClick="btnSave_Onclick" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="general-settings">
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AddBeginTag %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AddBeginTagTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtAddBeginTag" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AddEndTag %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AddEndTagTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtAddEndTag" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AddModifyGroupGroupMaxLevel %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AddModifyGroupGroupMaxLevelTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAddModifyGroupGroupMaxLevel" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AdminFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AdminFolderTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtAdminFolder" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AdminSiteTempFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AdminSiteTempFolderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAdminSiteTempFolder" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
         <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AllowSiblingSiteAccess %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AllowSiblingSiteAccess %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlAllowSiblingsiteAccess" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AnalyticsAdminUser %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AnalyticsAdminUserTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAnalyticsAdminUser" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AnalyticalSessionLength %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AnalyticalSessionLengthTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAnalyticalSessionLength" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageOptimumImageSize %><img src="../../App_Themes/General/images/icon-help.gif"
                rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageOptimumImageSizeTooltip %>"
                alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtOptimumImageSize" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageThumbnailImagePrefix %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageThumbnailImagePrefixTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetImageThumbnailImagePrefix" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImagePreviewImagePrefix %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImagePreviewImagePrefixTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetImagePreviewImagePrefix" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageThumbnailImageWidth %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageThumbnailImageWidthTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetImageThumbnailImageWidth" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageThumbnailImageHeight %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImageThumbnailImageHeightTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetImageThumbnailImageHeight" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImagePreviewImageWidth %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImagePreviewImageWidthTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetImagePreviewImageWidth" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImagePreviewImageHeight %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetImagePreviewImageHeightTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetImagePreviewImageHeight" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsThumbnailImageFolderName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsThumbnailImageFolderNameTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetsThumbnailImageFolderName" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsPreviewImageFolderName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsPreviewImageFolderNameTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtAssetsPreviewImageFolderName" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsCreateImageThumbnail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsCreateImageThumbnailTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:DropDownList ID="ddlAssetsCreateImageThumbnail" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsCreateImagePreview %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AssetsCreateImagePreviewTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlAssetsCreateImagePreview" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.AutoUpdateLinks %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.AutoUpdateLinksTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlAutoUpdateLinks" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.BlogsBlogXsltPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BlogsBlogXsltPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtBlogsBlogXsltPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.BlogsPostXsltPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BlogsPostXsltPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtBlogsPostXsltPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.BlogsCategoryXsltPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BlogsCategoryXsltPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtBlogsCategoryXsltPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.BlogListTemplateId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BlogListTemplateIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtBlogListTemplateId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.BlogPostTemplateId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BlogPostTemplateIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtBlogPostTemplateId" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.BrowserCapsAxd %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.BrowserCapsAxdTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtBrowserCapsAxd" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CanFocusInLinkTextBox %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CanFocusInLinkTextBoxTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlCanFocusInLinkTextBox" runat="server">
                    <asp:ListItem Text="No" Value="No" />
                    <asp:ListItem Text="Yes" Value="Yes" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CanPublishWithoutWorkflow %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CanPublishWithoutWorkflowTooltip %>"
                    alt="" /></label><div class="form-value">
                        <div class="form-value">
                            <asp:DropDownList ID="ddlCanPublishWithoutWorkflow" runat="server">
                                <asp:ListItem Text="true" Value="true" />
                                <asp:ListItem Text="false" Value="false" />
                            </asp:DropDownList>
                        </div>
                    </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailOnSubmitToWorkflow %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailOnSubmitToWorkflowTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:DropDownList ID="ddlCMSWorkflowBaseEnableEmailOnSubmitToWorkflow" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailOnApprove %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailOnApproveTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:DropDownList ID="ddlCMSWorkflowBaseEnableEmailOnApprove" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailOnReject %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailOnRejectTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:DropDownList ID="ddlCMSWorkflowBaseEnableEmailOnReject" runat="server">
                            <asp:ListItem Text="true" Value="true" />
                            <asp:ListItem Text="false" Value="false" />
                        </asp:DropDownList>
                    </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailForSubmitterOnPageApprove %><img
                    src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailForSubmitterOnPageApproveTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlCMSWorkflowBaseEnableEmailForSubmitterOnPageApprove" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailForSubmitterOnPagePublish %><img
                    src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEnableEmailForSubmitterOnPagePublishTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlCMSWorkflowBaseEnableEmailForSubmitterOnPagePublish" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBasePublishOrArchivePage %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBasePublishOrArchivePageTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBasePublishOrArchivePage" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForSubmitToWorkflow %><img
                    src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForSubmitToWorkflowTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForSubmitToWorkflow" runat="server"
                            CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForApprove %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForApproveTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForApprove" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForReject %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForRejectTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForReject" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForPublish %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForPublishTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForPublish" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForSubmitterPageApprove %><img
                    src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForSubmitterPageApproveTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForSubmitterPageApprove" runat="server"
                            CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForSubmitterPagePublish %><img
                    src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForSubmitterPagePublishTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForSubmitterPagePublish" runat="server"
                            CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForTranslationCompleted%><img
                    src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailTemplateForTranslationCompletedTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailTemplateForTranslationCompleted" runat="server"
                            CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailSender %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CMSWorkflowBaseEmailSenderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSWorkflowBaseEmailSender" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsCommentXsltPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsCommentXsltPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCommentsCommentXsltPath" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsPageEmailTemplateId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsPageEmailTemplateIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCommentsPageEmailTemplateId" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsPostEmailTemplateId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsPostEmailTemplateIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCommentsPostEmailTemplateId" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsNewCommentSenderEmail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CommentsNewCommentSenderEmailTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCommentsNewCommentSenderEmail" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CommentOffBeginTag %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CommentOffBeginTagTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCommentOffBeginTag" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.CommentOffEndTag %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.CommentOffEndTagTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtCommentOffEndTag" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DedicateSiteForClientData %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DedicateSiteForClientDataTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtDedicateSiteForClientData" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultWidth %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultWidthTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtDefaultWidth" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultHeight %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultHeightTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtDefaultHeigh" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultFilter %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultFilterTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtDefaultFilter" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultSaveThumbnails %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultSaveThumbnailsTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtDefaultSaveThumbnails" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultPageSize %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultPageSizeTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtDefaultPageSize" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultAllowStretch %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DefaultAllowStretchTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlDefaultAllowStretch" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DisableSessionMonitoring %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DisableSessionMonitoringTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlDisableSessionMonitoring" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DtSearchStoredIndexPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DtSearchStoredIndexPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtDtSearchStoredIndexPath" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.DtsearchHomeDir %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DtsearchHomeDirTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtDtsearchHomeDir" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EditorSkin %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EditorSkinTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtEditorSkin" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EmailForgotPwd %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EmailForgotPwdTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtEmailForgotPwd" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EnableAnalyticsTracking %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EnableAnalyticsTrackingTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableAnalyticsTracking" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EnableGenerateFileSecurityXML %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EnableGenerateFileSecurityXML %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableGenerateFileSecurityXML" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EnableSegmentingAudience%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EnableSegmentingAudienceTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableSegmentingAudience" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.UseJSForPageTracking %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.UseJSForPageTrackingTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlUseJSForPageTracking" runat="server" Enabled="false">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EnableThumbnailCOMobject %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EnableThumbnailCOMobjectTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableThumbnailCOMobject" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.EnableSharedPageVersion %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.EnableSharedPageVersionTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlEnableSharedPageVersion" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.Executables %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ExecutablesTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtExecutables" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FilesDownloadedOverlayReport %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FilesDownloadedOverlayReportTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFilesDownloadedOverlayReport" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadFileIconFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadFileIconFolderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileUploadFileIconFolder" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadFileIconMapXml %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadFileIconMapXmlTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileUploadFileIconMapXml" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadEnableCDN %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadEnableCDNTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileUploadEnableCDN" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadWebServerUNCPaths %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadWebServerUNCPathsTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileUploadWebServerUNCPaths" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadDefaultFileIcon %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileUploadDefaultFileIconTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileUploadDefaultFileIcon" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionAssetFiles %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionAssetFilesTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlFileVersioningVersionAssetFiles" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionAssetImages %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionAssetImagesTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlFileVersioningVersionAssetImages" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionTemplates %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionTemplatesTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlFileVersioningVersionTemplates" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionStyle %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningVersionStyleTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlFileVersioningVersionStyle" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningAutoOverwrite %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningAutoOverwriteTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlFileVersioningAutoOverwrite" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningAssetFilesVersionFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningAssetFilesVersionFolderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileVersioningAssetFilesVersionFolder" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningAssetImagesVersionFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningAssetImagesVersionFolderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileVersioningAssetImagesVersionFolder" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningTempFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FileVersioningTempFolderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFileVersioningTempFolder" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryToolBox %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryToolBoxTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFormsLibraryToolBox" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryFormsXSL %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryFormsXSLTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFormsLibraryFormsXSL" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibrarySenderEmail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibrarySenderEmailTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFormsLibrarySenderEmail" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryEmailTemplateId %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryEmailTemplateIdTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFormsLibraryEmailTemplateId" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryPollToolBox %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryPollToolBoxTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFormsLibraryPollToolBox" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryValidationJsFile %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FormsLibraryValidationJsFileTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtFormsLibraryValidationJsFile" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.ImageDraw50LicenseOwner %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ImageDraw50LicenseOwnerTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtImageDraw50LicenseOwner" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.ImageDraw50LicenseKey %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ImageDraw50LicenseKeyTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtImageDraw50LicenseKey" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.JobTask %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.JobTaskTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtJobTask" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterServiceUrl %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterServiceUrlTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtLinkUpdaterServiceUrl" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterApplicationPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterApplicationPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtLinkUpdaterApplicationPath" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label"><%= Bridgeline.iAPPS.Resources.SiteSettings.MasterContentVariantSaveBehaviorUnlinked %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.MasterContentVariantSaveBehaviorUnlinkedTooltip %>"
                    alt="" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlMasterContentVariantSaveBehavior" runat="server">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, True %>" Value="true" />
                        <asp:ListItem Text="<%$ Resources:GUIStrings, False %>" Value="false" />
                    </asp:DropDownList>
                </div>
        </div>  
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.MaxDaysForRecentlyPublishedPages %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.MaxDaysForRecentlyPublishedPagesTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtMaxDaysForRecentlyPublishedPages" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.MenuItemRolloverImagesFolder %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.MenuItemRolloverImagesFolderTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtMenuItemRolloverImagesFolder" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.MenuItemRolloverImagesVirtualPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.MenuItemRolloverImagesVirtualPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtMenuItemRolloverImagesVirtualPath" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.MultiXsltLocation %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.MultiXsltLocationTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtMultiXsltLocation" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.NewUserEmailTemplateForSendingMail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.NewUserEmailTemplateForSendingMailTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtNewUserEmailTemplateForSendingMail" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.NewUserApproveSenderEmail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.NewUserApproveSenderEmailTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtNewUserApproveSenderEmail" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.NewUserEnableEmail %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.NewUserEnableEmailTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlNewUserEnableEmail" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.NoMemberRolesGrantPermissionToMenuItem%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.NoRolesGrantPermissionToMenuItemTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlNoRolesGrantPermissionToMenuItem" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.PagesViewedOverlayReport %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.PagesViewedOverlayReportTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtPagesViewedOverlayReport" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.PassthroughFileExtensions %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.PassthroughFileExtensionsTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtPassthroughFileExtensions" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.PostTitleLengthLimit %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.PostTitleLengthLimitTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtPostTitleLengthLimit" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.PublishFeedExeLocation %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.PublishFeedExeLocationTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtCMSPublishFeed" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.Quickwebsoft_EventCalendar_AdditionalFiles %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.Quickwebsoft_EventCalendar_AdditionalFilesTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtQuickwebsoft_EventCalendar_AdditionalFiles" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.RSSFeedPhysicalDirectory %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.RSSFeedPhysicalDirectoryTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtPhysicalDir" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.RSSFeedVirtualPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.RSSFeedVirtualPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtVirtualPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.ScheduleApplicationName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ScheduleApplicationNameTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtApplicationName" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.ShowPageNoteByDefault %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ShowPageNoteByDefaultTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlShowPageNoteByDefault" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.ShowPageNoteTrigger %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ShowPageNoteTriggerTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtShowPageNoteTrigger" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.ShowResetCacheInAdmin %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ShowResetCacheInAdminTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlShowResetCacheInAdmin" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.SiteBackupLocation %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SiteBackupLocationTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtBackupLocation" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.SlidingExpireMinutes %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SlidingExpireMinutesTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtSlidingExpireMinutes" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.SqlTempBackupLocation %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SqlTempBackupLocationTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtSqlTempBackupLocation" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.SqlTempNetworkPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SqlTempNetworkPathTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtSqlTempNetworkPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.StaticPageUrl %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.StaticPageUrlTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtStaticPageUrl" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.StoredIndexPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.StoredIndexPathTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtStoredIndexPath" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.TaskDuration %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.TaskDurationTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtTaskDuration" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.TaskName %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.TaskNameTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtTaskName" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.TaskParameter %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.TaskParameterTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtTaskParameter" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.TrackingTrackFileDownload %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.TrackingTrackFileDownloadTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlTrackingTrackFileDownload" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.UseWorkflowEmailScreenshots %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.UseWorkflowEmailScreenshotsTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlUseWorkflowEmailScreenshots" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.UseSessionForViewStatePersistence %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.UseSessionForViewStatePersistenceTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlUseSessionForViewStatePersistence" runat="server">
                    <asp:ListItem Text="true" Value="true" />
                    <asp:ListItem Text="false" Value="false" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.WatchesExecutedOverlayReport %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.WatchesExecutedOverlayReportTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtWatchesExecutedOverlayReport" runat="server" CssClass="disabled"
                            ReadOnly="true" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.WFScreenshotLocation %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.WFScreenshotLocationTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtWFScreenshotLocation" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.WFScreenshotUrl %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.WFScreenshotUrlTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtWFScreenshotUrl" runat="server" CssClass="textBoxes" ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.WFScreenshotUtilityLocation %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.WFScreenshotUtilityLocationTooltip %>"
                    alt="" /></label><div class="form-value">
                        <asp:TextBox ID="txtWFScreenshotUtilityLocation" runat="server" CssClass="textBoxes"
                            ReadOnly="false" /></div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.XsltPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.XsltPathTooltip %>" alt="" /></label><div
                        class="form-value">
                        <asp:TextBox ID="txtXsltPath" runat="server" CssClass="disabled" ReadOnly="true" /></div>
        </div>
    </div>
</asp:Content>
