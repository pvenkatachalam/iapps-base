﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master" AutoEventWireup="true"
    CodeBehind="CacheURLs.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.CacheURLs" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdCacheUrls_OnLoad(sender, eventArgs) {
            $(".cache-urls").gridActions({
                objList: grdCacheUrls,
                listType: "tableGrid",
                onCallback: function (sender, eventArgs) {
                    gridActions_Callback(sender, eventArgs);
                }
            });
        }

        function grdCacheUrls_OnRenderComplete(sender, eventArgs) {
            $(".cache-urls").gridActions("bindControls");
        }

        function grdCacheUrls_onCallbackComplete(sender, eventArgs) {
            $(".cache-urls").gridActions("displayMessage");
        }

        function gridActions_Callback(sender, eventArgs) {
            var doCallback = true;

            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    $("#txtTitle").val();
                    $("#txtURL").val();
                    $("#divAddNewCacheURL").iAppsDialog("open");
                    break;
                case "Delete":
                    if (!fn_DeleteURL()) {
                        doCallback = false;
                    }
                    break;
            }

            if (doCallback) {
                grdCacheUrls.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdCacheUrls.callback();
            }
        }

        function fn_AddURL() {
            $("#txtTitle").val();
            $("#txtURL").val();
            $("#divAddNewCacheURL").iAppsDialog("open");
        }
        function fn_DeleteURL() {
            var result = confirm("<%= Bridgeline.iAPPS.Resources.GUIStrings.AreYouSureYouWantToDeleteThisItem %>");
            return result;
        }
        function fn_CancelAddURL() {
            $("#divAddNewCacheURL").iAppsDialog("close");
        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="grid-section">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdCacheUrls" SkinID="Default" Width="100%" runat="server"
            ShowFooter="false" RunningMode="Callback" LoadingPanelClientTemplateId="grdCacheUrlsLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Url" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" AllowSorting="false" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn Width="400" HeadingText="URL"
                            DataField="Url" runat="server" AllowReordering="false" FixedWidth="true" IsSearchable="true" />
                        <ComponentArt:GridColumn Width="400" HeadingText="URL"
                            DataField="PublicSiteUrl" runat="server" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <Load EventHandler="grdCacheUrls_OnLoad" />
                <CallbackComplete EventHandler="grdCacheUrls_onCallbackComplete" />
                <RenderComplete EventHandler="grdCacheUrls_OnRenderComplete" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdCacheUrlsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdCacheUrls) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
    <asp:PlaceHolder ID="phNoRecords" runat="server" Visible="false">
        <p style="text-align: center;"><%= Bridgeline.iAPPS.Resources.GUIStrings.NoRecordsFound %></p>
    </asp:PlaceHolder>
    <div class="iapps-modal" id="divAddNewCacheURL" style="display: none;">
        <div class="modal-header clear-fix">
            <h2><%= Bridgeline.iAPPS.Resources.GUIStrings.AddNewURL %></h2>
        </div>
        <div class="modal-content">
            <div class="form-row">
                <label class="form-label"><span class="req">*</span><%= Bridgeline.iAPPS.Resources.GUIStrings.URL %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtURL" runat="server" Width="400" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="button" id="btnCancel" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Cancel %>" class="button" onclick="fn_CancelAddURL();" />
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnSaveNewUrl_OnClick" />
        </div>
    </div>
</asp:Content>
