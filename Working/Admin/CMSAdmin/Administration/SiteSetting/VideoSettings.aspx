﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master"
    AutoEventWireup="true" ValidateRequest="false" CodeBehind="VideoSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.VideoSettings" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var _PlayerName = '<%= GUIStrings.PlayerName %>';
        var _PlayerId = '<%= GUIStrings.PlayerId %>';
        var _PlayerKey = '<%= GUIStrings.PlayerKey %>';
        var _PlayerNameTooltip = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerNameTooltip %>';
        var _PlayerIdTooltip = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerIdTooltip %>';
        var _PlayerKeyTooltip = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerKeyTooltip %>';
        var _MinimumHeight = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerMinimumHeight %>';
        var _MinimumHeightTooltip = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerMinimumHeightTooltip %>';
        var _MinimumWidth = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerMinimumWidth %>';
        var _MinimumWidthTooltip = '<%= Bridgeline.iAPPS.Resources.SiteSettings.PlayerMinimumWidthTooltip %>';
        var _IsPlayListPlayer = '<%= Bridgeline.iAPPS.Resources.SiteSettings.IsPlayListPlayer %>';
        var _IsPlayListPlayerTooltip = '<%= Bridgeline.iAPPS.Resources.SiteSettings.IsPlayListPlayerTooltip %>';

        function grdPlayer_OnLoad(sender, eventArgs) {
            if ($('#hdnVideoSetingId').val() == '')
                $(".brightcove-player").hide();
            else
                $(".brightcove-player").show();
            $(".brightcove-player").gridActions({ objList: grdPlayer,
                listType: "tableGrid"
            });
        }

        function grdPlayer_OnRenderComplete(sender, eventArgs) {
            $(".brightcove-player").gridActions("bindControls");
        }

        function grdPlayer_OnCallbackComplete(sender, eventArgs) {
            $(".brightcove-player").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var qString;
            var selectedItemId = gridActionsJson.SelectedIds[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = ConfirmDelete();
                    gridActionsJson.CustomAttributes["DeleteItemId"] = selectedItemId;
                    break;
                case "Edit":
                case "Add":
                    doCallback = false;
                    qString = 'VideoId=' + $('#hdnVideoSetingId').val();
                    if (selectedItemId && gridActionsJson.Action == "Edit")
                        qString += '&PlayerId=' + selectedItemId;
                    OpeniAppsAdminPopup("AddBrightCovePlayerPopup", qString, "RefreshVideoLibrary");
                    break;
            }

            if (doCallback && typeof grdPlayer != "undefined") {
                gridActionsJson.SelectedItems = [];
                grdPlayer.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdPlayer.callback();
            }
        }

        function RefreshVideoLibrary() {
            $(".brightcove-player").gridActions("callback");
        }

        function ConfirmDelete() {
            var yesNo = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, YouAreAboutToDeleteThisPlayer %>' />");
            if (yesNo == true) {
                return true;
            }
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" 
        ValidationGroup="callback" OnClick="btnSave_Onclick" />
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
        ValidationGroup="callback" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="video-settings">
        <h2>
            <%= GUIStrings.BrightCoveSettings %></h2>
        <asp:HiddenField ID="hdnVideoSetingId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnName" runat="server" />
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.PublisherId %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.PublisherIDTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtPublisherId" runat="server" CssClass="textBoxes" />
                <asp:RequiredFieldValidator runat="server" ID="reqtxtPublisherId" ControlToValidate="txtPublisherId" 
                    ValidationGroup="callback" SetFocusOnError="true" ErrorMessage="<%$ Resources:JSMessages, PublisherIdIsRequired %>" Display="None" ></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regtxtPublisherId" runat="server" ControlToValidate="txtPublisherId"
                    ErrorMessage="<%$ Resources:JSMessages, PublisherIdIsNotValid %>" Display="None" SetFocusOnError="true"
                    ValidationExpression="^[^<>]+$" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.ReadToken %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.ReadTokenTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtReadToken" runat="server" CssClass="textBoxes" />
                <asp:RequiredFieldValidator runat="server" ID="reqtxtReadToken" ControlToValidate="txtReadToken" 
                    ValidationGroup="callback" SetFocusOnError="true" ErrorMessage="<%$ Resources:JSMessages, ReadTokenIsRequired %>" Display="None" ></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regtxtReadToken" runat="server" ControlToValidate="txtReadToken"
                    ErrorMessage="<%$ Resources:JSMessages, ReadTokenIsNotValid %>" Display="None" SetFocusOnError="true"
                    ValidationExpression="^[^<>]+$" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.WriteToken %>
                <img src="../../App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.WriteTokenTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtWriteToken" runat="server" CssClass="textBoxes" />
                <asp:RegularExpressionValidator ID="regtxtWriteToken" runat="server" ControlToValidate="txtWriteToken"
                    ErrorMessage="<%$ Resources:JSMessages, WriteTokenIsNotValid %>" Display="None" SetFocusOnError="true"
                    ValidationExpression="^[^<>]+$" />
            </div>
        </div>
        <p><%= GUIStrings.PlayerIdHelp %></p>
        <div class="brightcove-player">
          <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
          <ComponentArt:Grid ID="grdPlayer" SkinID="Default" runat="server" Width="100%"
              RunningMode="Callback" AllowEditing="false" AutoCallBackOnInsert="true" CallbackCachingEnabled="true"
              AutoCallBackOnUpdate="true" ShowFooter="false" CallbackCacheLookAhead="10"
              AllowMultipleSelect="false" LoadingPanelClientTemplateId="grdPlayerLoadingPanelTemplate">
            <ClientEvents>
              <Load EventHandler="grdPlayer_OnLoad" />
              <CallbackComplete EventHandler="grdPlayer_OnCallbackComplete" />
              <RenderComplete EventHandler="grdPlayer_OnRenderComplete" />
            </ClientEvents>
            <Levels>
              <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                    HeadingRowCssClass="heading-row" SelectedRowCssClass="selected-row"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" AllowSorting="false" SortImageHeight="7" AllowGrouping="false"
                    AlternatingRowCssClass="alternate-row">
                <Columns>
                        <ComponentArt:GridColumn runat="server" HeadingCellClientTemplateId="PlayerNameHT"
                            DataField="FriendlyName" Width="215"
                            AllowReordering="False" FixedWidth="true" />
                        <ComponentArt:GridColumn runat="server" HeadingCellClientTemplateId="PlayerIdHT"
                            DataField="Key" Width="215" AllowReordering="False"
                            FixedWidth="true" />
                        <ComponentArt:GridColumn runat="server" HeadingCellClientTemplateId="PlayerKeyHT"
                            DataField="Value" Width="315" AllowReordering="False"
                            FixedWidth="true" />
                        <ComponentArt:GridColumn runat="server" HeadingCellClientTemplateId="PlayerMinHT"
                            DataField="MinimumHeight"  Width="115"
                            AllowReordering="False" FixedWidth="true" />
                        <ComponentArt:GridColumn runat="server" HeadingCellClientTemplateId="PlayerMinWHT"
                            DataField="MinimumWidth" Width="115"
                            AllowReordering="False" FixedWidth="true" />
                        <ComponentArt:GridColumn runat="server" HeadingCellClientTemplateId="PlayerCanPlayListT" 
                            DataField="CanPlayList" Width="150" 
                            AllowReordering="False" FixedWidth="true" />
                        <ComponentArt:GridColumn runat="server" DataField="Sequence" Visible="false" />
                        <ComponentArt:GridColumn runat="server" DataField="Id" Visible="false" />
                  <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                </Columns>
              </ComponentArt:GridLevel>
            </Levels>

            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="PlayerNameHT">
                    <div class="custom-header">
                        <span class="header-text">##_PlayerName##</span>
                        <img class="help-icon" src="../../App_Themes/General/images/icon-help.gif" title="##_PlayerNameTooltip##"
                            rel="tooltip" alt="" />
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PlayerIdHT">
                    <div class="custom-header">
                        <span class="header-text">##_PlayerId##</span>
                        <img class="help-icon" src="../../App_Themes/General/images/icon-help.gif" title="##_PlayerIdTooltip##"
                            rel="tooltip" alt="" />
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PlayerKeyHT">
                    <div class="custom-header">
                        <span class="header-text">##_PlayerKey##</span>
                        <img class="help-icon" src="../../App_Themes/General/images/icon-help.gif" title="##_PlayerKeyTooltip##"
                            rel="tooltip" alt="" />
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PlayerMinHT">
                    <div class="custom-header">
                        <span class="header-text">##_MinimumHeight##</span>
                        <img class="help-icon" src="../../App_Themes/General/images/icon-help.gif" title="##_MinimumHeightTooltip##"
                            rel="tooltip" alt="" />
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PlayerMinWHT">
                    <div class="custom-header">
                        <span class="header-text">##_MinimumWidth##</span>
                        <img class="help-icon" src="../../App_Themes/General/images/icon-help.gif" title="##_MinimumWidthTooltip##"
                            rel="tooltip" alt="" />
                    </div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="PlayerCanPlayListT">
                    <div class="custom-header">
                        <span class="header-text">##_IsPlayListPlayer##</span>
                        <img class="help-icon" src="../../App_Themes/General/images/icon-help.gif" title="##_IsPlayListPlayerTooltip##"
                            rel="tooltip" alt="" />
                    </div>
                </ComponentArt:ClientTemplate>
              <ComponentArt:ClientTemplate ID="grdPlayerLoadingPanelTemplate">
                ## GetGridLoadingPanelContent(grdPlayer) ##
              </ComponentArt:ClientTemplate>
            </ClientTemplates>
          </ComponentArt:Grid>
        </div>
    </div>
</asp:Content>
