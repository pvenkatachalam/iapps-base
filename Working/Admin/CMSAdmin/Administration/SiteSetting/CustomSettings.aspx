﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master"
    AutoEventWireup="true" CodeBehind="CustomSettings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.CustomSettings"
    StylesheetTheme="General" ValidateRequest="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdCustomSettings_onLoad() {
            if (grdCustomSettings.RecordCount == 0)
                $(".custom-settings").setEmptyGridStyle();
        }

        function AddSetting() {
            grdCustomSettings.get_table().addRow();

            return false;
        }

        function DeleteSetting(rowId) {
            var gridItem = grdCustomSettings.getItemFromClientId(rowId);
            var proceed = true;
            if (gridItem.getMember("Key").get_value() != null)
                proceed = confirm("<%= JSMessages.AreYouSureYouWantToDelete %>");
            if (proceed) {
                grdCustomSettings.deleteItem(gridItem);

                if (grdCustomSettings.RecordCount == 0)
                    $(".custom-settings").setEmptyGridStyle();
            }

            return false;
        }

        function SaveSetting(itemId, columnField, newValue) {
            var row = grdCustomSettings.GetRowFromClientId(itemId);

            if (row != null) {
                // Check if value was changed
                var oldValue = row.GetMember(columnField).Value;
                if (oldValue != newValue) {
                    // Get column index for SetValue
                    var col = 0;
                    for (var i = 0; i < grdCustomSettings.Table.Columns.length; i++) {
                        if (grdCustomSettings.Table.Columns[i].DataField == columnField) {
                            col = i;
                            break;
                        }
                    }
                    row.SetValue(col, newValue, true);
                }
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnSave_Onclick" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <div class="custom-settings">
        <div class="grid-actions clear-fix">
            <div class="grid-button">
                <asp:Button ID="btnAdd" runat="server" Text="<%$ Resources:GUIStrings, AddNew %>"
                    ToolTip="<%$ Resources:GUIStrings, AddNew %>" OnClientClick="return AddSetting();"
                    CssClass="grid-button" />
            </div>
        </div>
        <ComponentArt:Grid ID="grdCustomSettings" SkinID="Default" runat="server" Width="100%"
            EditOnClickSelectedItem="false" AllowEditing="true" KeyboardEnabled="false" AllowTextSelection="true"
            ShowFooter="false" RunningMode="Client" LoadingPanelClientTemplateId="gridContentLoadingPanelTemplate"
            CssClass="fat-grid" PageSize="1000" EmptyGridText="<%$ Resources:GUIStrings, NoCustomSettings %>">
            <Levels>
                <ComponentArt:GridLevel ShowSelectorCells="false" ShowHeadingCells="false" RowCssClass="row"
                    ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell" SelectedRowCssClass="selected"
                    SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                    SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, Key %>"
                            DataField="Key" DataCellClientTemplateId="KeyTemplate" />
                        <ComponentArt:GridColumn runat="server" HeadingText="<%$ Resources:GUIStrings, Value %>"
                            DataField="Value" DataCellClientTemplateId="ValueTemplate" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <Load EventHandler="grdCustomSettings_onLoad" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="KeyTemplate">
                    <strong>## DataItem.getCurrentMember().get_column().get_headingText() ## : </strong>
                    <input id="txtKey" class="textBoxes" style="width: 400px;" type="text" value="## DataItem.getCurrentMember().get_value() ##"
                        onblur="SaveSetting('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this.value);" />
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ValueTemplate">
                    <strong>## DataItem.getCurrentMember().get_column().get_headingText() ## : </strong>
                    <input id="txtValue" class="textBoxes" style="width: 600px;" type="text" value="## DataItem.getCurrentMember().get_value() ##"
                        onblur="SaveSetting('## DataItem.ClientId ##', '## DataItem.getCurrentMember().get_column().get_dataField() ##', this.value);" />
                    <a href="#" onclick="return DeleteSetting('## DataItem.ClientId ##');">
                        <img src="/Admin/App_Themes/General/images/icon-delete-cross.png" border="0" alt="X" /></a>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
