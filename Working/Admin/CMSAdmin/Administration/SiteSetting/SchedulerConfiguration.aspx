﻿<%@ Page Language="C#" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master" AutoEventWireup="true"
    ValidateRequest="false" CodeBehind="SchedulerConfiguration.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SchedulerConfiguration"
    StylesheetTheme="General" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SetValueFromHidden(objHdnValue, objTextBox) {
            document.getElementById(objTextBox).value = objHdnValue;
            if (document.getElementById(objTextBox).type == "text")
                document.getElementById(objTextBox).type = "password";
            else
                document.getElementById(objTextBox).type = "text";
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="scheduler-config">
        <h2><%= GUIStrings.DelayedPublish %></h2>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, UserName %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DelayedPublishUserNameTooltip %>" alt="" />
            </label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDPUserName" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Password %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DelayedPublishPasswordTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDPPassword" TextMode="Password" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">&nbsp;</label>
            <div class="form-value">
                <asp:CheckBox ID="chkDPShowPassword" runat="server" Text="<%$ Resources:GUIStrings, ShowPassword %>" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, CMSWebserviceURL %>" />
                <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DelayedPublishCMSWebserviceURLTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDPCMSWebserviceURL" CssClass="textBoxes" />
            </div>
        </div>
        <div class="button-row">
            <asp:Button runat="server" ID="btnSaveDelayedPublish" Text="<%$ Resources:GUIStrings, SaveDelayedPublish %>"
                ToolTip="<%$ Resources:GUIStrings, SaveDelayedPublish %>" cssclass="button"
                ValidationGroup="callback" OnClick="btnSaveDelayedPublish_Click" />
        </div>
        <h2><%= GUIStrings.LinkUpdater %></h2>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize23" runat="server" Text="<%$ Resources:GUIStrings, UserName %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterUserNameTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtLinkUserName" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize24" runat="server" Text="<%$ Resources:GUIStrings, Password %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterPasswordTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtLinkPassword" TextMode="Password" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">&nbsp;</label>
            <div class="form-value">
                <asp:CheckBox ID="chkLinkShowPassword" runat="server" Text="<%$ Resources:GUIStrings, ShowPassword %>" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize25" runat="server" Text="<%$ Resources:GUIStrings, CMSWebserviceURL %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LinkUpdaterCMSWebserviceURLTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtLinkCMSWebserviceURL" CssClass="textBoxes" />
            </div>
        </div>
        <div class="button-row">
            <asp:Button runat="server" ID="btnLinkUpdater" Text="<%$ Resources:GUIStrings, SaveLinkUpdater %>"
                ToolTip="<%$ Resources:GUIStrings, SaveLinkUpdater %>" cssclass="button"
                ValidationGroup="callback" OnClick="btnLinkUpdater_Click" />
        </div>
        <h2><%= GUIStrings.SchedulerExe %></h2>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, TaskUserName %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SchedulerExeTaskUserNameTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtTaskUserName" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, TaskUserPassword %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SchedulerExeTaskUserPasswordTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtTaskUserPassword" TextMode="Password" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">&nbsp;</label>
            <div class="form-value">
                <asp:CheckBox ID="chkTaskShowPassword" runat="server" Text="<%$ Resources:GUIStrings, ShowPassword %>"  Visible="false" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, DBServer %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DBServerTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDBServer" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, FailOverServer %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.FailOverServerTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtFailOverServer" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, DBName %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DBNameTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDBName" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, DBUser %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DBUserTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDBUser" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, DBPassword %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.DBPasswordTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtDBPassword" TextMode="Password" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">&nbsp;</label>
            <div class="form-value">
                <asp:CheckBox ID="chkDBShowPassword" runat="server" Text="<%$ Resources:GUIStrings, ShowPassword %>" />
                <asp:HiddenField ID="hdnDBPassword" runat="server" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, SMTP %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SchedulerExeSMTPTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSMTP" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, SiteId %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SchedulerExeSiteIdTooltip %>" alt="" /></label>
            <div class="form-value">
            <asp:TextBox runat="server" ID="txtSiteId" CssClass="textBoxes"></asp:TextBox>
            </div>
        </div>
        <div class="button-row">
            <asp:Button runat="server" ID="btnSaveSchedulerExe" Text="<%$ Resources:GUIStrings, SaveSchedulerExe %>"
                ToolTip="<%$ Resources:GUIStrings, SaveSchedulerExe %>" cssclass="button"
                ValidationGroup="callback" OnClick="btnSaveSchedulerExe_Click" />
        </div>
        <h2><%= GUIStrings.SendTestEmail %></h2>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize33" runat="server" Text="<%$ Resources:GUIStrings, SMTP %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SMTPTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSESMTP" CssClass="disabled" ReadOnly="true" />
        </div>
        </div>
        <div class="form-row">
            <label class="form-label"><asp:Localize ID="Localize34" runat="server" Text="<%$ Resources:GUIStrings, TestEmail %>" />
            <img src="/Admin/App_Themes/General/images/icon-help.gif" rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.TestEmailTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox runat="server" ID="txtSETestEmail" CssClass="textBoxes" />
            </div>
                            <asp:RequiredFieldValidator ID="rqTestEmail" ErrorMessage="Please enter email id" runat="server" Display="None" ControlToValidate="txtSETestEmail" ValidationGroup="vgEmailTest"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regEmail" ControlToValidate="txtSETestEmail" ErrorMessage="Please enter valid email id" Display="None" runat="server"  ValidationGroup="vgEmailTest" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

        </div>
        <div class="button-row">
            <asp:Button runat="server" ID="btnSendEmailTest" Text="<%$ Resources:GUIStrings, SendTestEmail %>"
                ToolTip="<%$ Resources:GUIStrings, SendTestEmail %>" cssclass="primarybutton" 
                ValidationGroup="vgEmailTest" OnClick="btnSendEmailTest_Click" />
        </div>
        <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true" ValidationGroup="callback" />
        <asp:ValidationSummary runat="server" ID="valSummaryEmail" ShowSummary="false" ShowMessageBox="true"  ValidationGroup="vgEmailTest" />
    </div>
</asp:Content>
