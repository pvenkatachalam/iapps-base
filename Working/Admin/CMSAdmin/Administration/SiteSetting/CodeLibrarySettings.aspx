﻿<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master"
    AutoEventWireup="true" CodeBehind="CodeLibrarySettings.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.CodeLibrarySettings"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
        ValidationGroup="callback" OnClick="btnSave_Onclick" />
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <div class="cl-settings">
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.XSLTFolderPath %><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.XSLTFolderPathTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtCLXsltFolderPath" runat="server" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.XSLTFolderId%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.XSLTFolderIdTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtCLXsltFolderId" runat="server" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.LinkStylesXMLPath%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LinkStylesXMLPathTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtCLLinkStyleXmlPath" runat="server" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.SiteSearchFolderPath%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.SiteSearchFolderPathTooltip %>"
                    alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtCLSiteSearchFolder" runat="server" CssClass="textBoxes" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= Bridgeline.iAPPS.Resources.SiteSettings.LogXmlFolderPath%><img src="../../App_Themes/General/images/icon-help.gif"
                    rel="tooltip" title="<%= Bridgeline.iAPPS.Resources.SiteSettings.LogXmlFolderPathTooltip %>" alt="" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtCLLogXmlPath" runat="server" CssClass="textBoxes" />
            </div>
        </div>
    </div>
</asp:Content>
