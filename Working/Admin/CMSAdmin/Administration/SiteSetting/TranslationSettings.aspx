﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TranslationSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.TranslationSettings" StylesheetTheme="General"
    MasterPageFile="~/Administration/SiteSetting/SettingsMaster.master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphPageHeader" runat="server">
    <asp:Button runat="server" ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" ValidationGroup="callback"
        OnClick="btnSave_Onclick" />
    <asp:ValidationSummary runat="server" ID="valSummary" ShowSummary="false" ShowMessageBox="true"
        ValidationGroup="callback" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="video-settings">
        <h2>
            <%= GUIStrings.TranslationSettings %></h2>
        <asp:PlaceHolder runat="server" ID="phConfig"></asp:PlaceHolder>
    </div>
</asp:Content>
