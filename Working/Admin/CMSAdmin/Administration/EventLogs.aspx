<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.EventLogs" StylesheetTheme="General"
    MasterPageFile="~/MasterPage.Master" CodeBehind="EventLogs.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="DateRange" Src="~/UserControls/DateRange.ascx" %>
<%@ Register assembly="ComponentArt.Web.UI" namespace="ComponentArt.Web.UI" tagprefix="ComponentArt" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function FilterResults() {
            var selectedDate = eval("GetSelectedDate_<%=dateRange.ClientID %>()");
            gridActionsJson.StartDate = selectedDate.StartDate;
            gridActionsJson.EndDate = selectedDate.EndDate;

            gridActionsJson.CustomAttributes["Log_Categories"] = "";
            $("#chkCategories").find(" input[type='checkbox']:checked").each(function () {
                gridActionsJson.CustomAttributes["Log_Categories"] += $(this).parent().attr("dataid")+ ",";
            });

            gridActionsJson.CustomAttributes["Log_Severity"] = $("#ddlSeverity").val();
            gridActionsJson.CustomAttributes["Log_MachineName"] = $("#txtMachineName").val();

            gridActionsJson.TabIndex = $(".event-type input[type='radio']:checked").val();

            $(".event-log").gridActions("callback");

            return false;
        }

        $(function () {
            $(".event-type input[type='radio']").on("click", function () {
                FilterResults();
            });
        });

        function grdLogs_OnLoad(sender, eventArgs) {
            $(".event-log").gridActions({ objList: grdLogs });

            var treeHeader = $(".event-log .tree-header");
            var scrollToPosition = treeHeader.offset().top + treeHeader.outerHeight() - 40;
            $(window).on('scroll', function () {
                var scrollTop = $(window).scrollTop();
                if (treeHeader.hasClass("tree-header-fixed"))
                    scrollTop += treeHeader.height();
                if (scrollTop > scrollToPosition) {
                    treeHeader.addClass('tree-header-fixed').width($(".left-control").width());;
                }
                else {
                    treeHeader.removeClass('tree-header-fixed');
                }
            });
        }

        function grdLogs_OnRenderComplete(sender, eventArgs) {
            $(".event-log").gridActions("bindControls");
            $(".event-log").iAppsSplitter();
        }

        function grdLogs_OnCallbackComplete(sender, eventArgs) {
            $(".event-log").gridActions("displayMessage");
        }

        function grdLogs_OnItemDoubleClick(sender, eventArgs) {
            ShowLogDetail(eventArgs.get_item());
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems.first();
            if (typeof eventArgs != "undefined")
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = confirm("<%= JSMessages.ConfirmDeleteSelectedLog %>");
                    break;
                case "Clear":
                    doCallback = confirm("<%= JSMessages.ConfirmClearEventLog %>");
                    break;
                case "ViewDetails":
                    doCallback = false;
                    ShowLogDetail(selectedItem);
                    break;
                case "ExportToExcel":
                case "ExportToCsv":
                    doCallback = false;
                    break;
            }

            if (doCallback) {
                grdLogs.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdLogs.callback();
            }
        }

        function ShowLogDetail(item) {
            $("#ltDate").html(item.getMember('CreatedDate').get_text());
            $("#ltMachineName").html(item.getMember('MachineName').get_value());
            $("#ltCategories").html(item.getMember('Categories').get_value());
            $("#ltSeverity").html(item.getMember('Severity').get_value());
            $("#ltMessage").html(FormatGridHtml(item.getMember('Message').get_value()));
            $("#ltStackTrace").html(FormatGridHtml(item.getMember('StackTrace').get_value()));
            $("#ltInnerException").html(FormatGridHtml(item.getMember('InnerExceptionHtml').get_value()));
               
            $("#ltlUserIdentification").html(FormatGridHtml(item.getMember('UserIdentification').get_value()));
            $("#ltlOriginationOfEvent").html(FormatGridHtml(item.getMember('OriginationOfEvent').get_value()));
            $("#ltlSystemComponent").html(FormatGridHtml(item.getMember('SystemComponent').get_value()));
            $("#ltlErrorMessage").html(FormatGridHtml(item.getMember('ErrorMessage').get_value()));
            $("#ltlUserUpdated").html(FormatGridHtml(item.getMember('UserUpdated').get_value()));

            var logItemId = item.getMember('ActivityId').get_value();

            if (gridActionsJson.TabIndex == 0)
                $("#divException").hide();
            else
                $("#divException").show();

            $("#logDetail").iAppsDialog("open");

            $.ajax({
                type: "POST",
                url: "EventLogs.aspx/LogDetailAccessed",
                data: '{ logId: "'+logItemId+'" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    // Do something interesting here.
                }
            });
        }

        function CloseLogDetail() {
            $("#logDetail").iAppsDialog("close");

            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="left-control">
        <div class="tree-header">
            <h2>
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Filters %>" /></h2>
            <div class="tree-actions clear-fix">
                <div class="row clear-fix">
                    <asp:RadioButtonList ID="rbEventType" runat="server" RepeatDirection="Horizontal" CssClass="event-type" />
                </div>
            </div>
        </div>
        <div class="form-section">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DateFilterColon %>" /></label>
                <div class="form-value">
                    <UC:DateRange ID="dateRange" runat="server" Width="290" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Category %>" /></label>
                <div class="form-value">
                    <asp:CheckBox ID="chkAllElements" CssClass="select-all-fields" Checked="true" runat="server"
                        Text="<%$ Resources:GUIStrings, ShowAll %>" />
                </div>
            </div>
            <div class="form-row checkbox-list">
                <asp:CheckBoxList ID="chkCategories" CssClass="select-field" runat="server" RepeatDirection="Vertical" ClientIDMode="Static" />
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, MachineName %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtMachineName" runat="server" Width="282" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Severity %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlSeverity" runat="server" Width="290" ClientIDMode="Static" />
                </div>
            </div>
            <div class="button-row">
                <asp:Button ID="btnSearch" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Search %>"
                    OnClientClick="return FilterResults();" />
            </div>
        </div>
    </div>
    <div class="right-control">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <div class="grid-section">
            <componentart:grid id="grdLogs" skinid="Default" runat="server" width="100%" runningmode="Callback"
                showfooter="false" loadingpanelclienttemplateid="grdLogsLoadingPanelTemplate" ManualPaging="true"
                cssclass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="grdLogs_OnLoad" />
                    <CallbackComplete EventHandler="grdLogs_OnCallbackComplete" />
                    <RenderComplete EventHandler="grdLogs_OnRenderComplete" />
                    <ItemDoubleClick EventHandler="grdLogs_OnItemDoubleClick" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="ActivityId" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell" 
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Message" DataCellServerTemplateId="DetailsTemplate" />
                            <ComponentArt:GridColumn DataField="Priority" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Severity" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="MachineName" Visible="false" />
                            <ComponentArt:GridColumn DataField="ActivityId" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="LogId" Visible="false" />
                            <ComponentArt:GridColumn DataField="CreatedDate" Visible="false" />
                            <ComponentArt:GridColumn DataField="StackTrace" Visible="false" />
                            <ComponentArt:GridColumn DataField="InnerExceptionHtml" Visible="false" />
                            <ComponentArt:GridColumn DataField="Categories" Visible="false" />                            
                            <ComponentArt:GridColumn DataField="UserIdentification" Visible="false" />
                            <ComponentArt:GridColumn DataField="OriginationOfEvent" Visible="false" />
                            <ComponentArt:GridColumn DataField="SystemComponent" Visible="false" />
                            <ComponentArt:GridColumn DataField="ErrorMessage" Visible="false" />
                            <ComponentArt:GridColumn DataField="UserUpdated" Visible="false" />
                            

                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                        <Template>
                            <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["ActivityId"]%>'>
                                 <div class="first-cell">
                                     <input type="checkbox" class="item-selector" runat="server" value='<%# Container.DataItem["ActivityId"]%>' />
                                 </div>
                                <div class="middle-cell">
                                    <p>
                                        <%# Container.DataItem["Message"]%></p>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.CreatedDate, Container.DataItem["CreatedDate"]) %>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdLogsDragTemplate">
                        ## GetGridDragTemplateById(grdLogs, DataItem) ##
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdLogsLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdLogs) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </componentart:grid>
        </div>
    </div>
    <asp:PlaceHolder ID="phLogDetails" runat="server" ClientIDMode="Static">
        <div id="logDetail" class="iapps-modal log-detail" style="display: none;">
            <div class="modal-header clear-fix">
                <h2>
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, LogDetails %>" /></h2>
            </div>
            <div class="modal-content">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, Date %>" />
                    </label>
                    <div class="form-value" id="ltDate">
                        &nbsp;
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, MachineName %>" />
                    </label>
                    <div class="form-value" id="ltMachineName">
                        &nbsp;
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Categories %>" />
                    </label>
                    <div class="form-value" id="ltCategories">
                        &nbsp;
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, Severity %>" />
                    </label>
                    <div class="form-value" id="ltSeverity">
                        &nbsp;
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, Message %>" />
                    </label>
                    <div class="form-value" id="ltMessage">
                        &nbsp;
                    </div>
                </div>               
                 <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings, UserIdentification %>" />
                    </label>
                    <div class="form-value" id="ltlUserIdentification">
                        &nbsp;
                    </div>
                </div>
                 <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, OriginationOfEvent %>" />
                    </label>
                    <div class="form-value" id="ltlOriginationOfEvent">
                        &nbsp;
                    </div>
                </div>
                 <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, SystemComponent %>" />
                    </label>
                    <div class="form-value" id="ltlSystemComponent">
                        &nbsp;
                    </div>
                </div>
                 <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, ErrorMessage %>" />
                    </label>
                    <div class="form-value" id="ltlErrorMessage">
                        &nbsp;
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, UserUpdated %>" />
                    </label>
                    <div class="form-value" id="ltlUserUpdated">
                        &nbsp;
                    </div>
                </div>
                <div id="divException">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, StackTrace %>" />
                        </label>
                        <div class="form-value" id="ltStackTrace">
                            &nbsp;
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, InnerException %>" />
                        </label>
                        <div class="form-value" id="ltInnerException">
                            &nbsp;
                        </div>
                    </div>                     
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
                    CssClass="button" OnClientClick="return CloseLogDetail();" />
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>
