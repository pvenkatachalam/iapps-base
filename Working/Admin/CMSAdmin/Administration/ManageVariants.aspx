﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageVariants" CodeBehind="ManageVariants.aspx.cs" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function ConfirmDelete() {
            var yesNo = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:GUIStrings, YouAreAboutToDeleteThisSite %>' />");
            if (yesNo == true) {
                return true;
            }
            else
                return false;
        }
        function EditVariant(objId) {
            window.location = "ManageVariantDetails.aspx?VariantSiteId=" + objId;
        }
        function AddVariant() {
            window.location = "ManageVariantDetails.aspx";
        }

        function grdVariantList_OnLoad(sender, eventArgs) {
            $(".site-variant").gridActions({ objList: grdVariantList });
        }

        function grdVariantList_OnRenderComplete(sender, eventArgs) {
            $(".site-variant").gridActions("bindControls");
            $(".site-variant").iAppsSplitter();
        }

        function grdVariantList_OnCallbackComplete(sender, eventArgs) {
            $(".site-variant").gridActions("displayMessage", { complete: function () {
                if (gridActionsJson.ResponseCode == 1) {
                    window.location = window.location;
                }
            }
            });
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;

            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = ConfirmDelete();
                    break;
                case "Edit":
                    doCallback = false;
                    EditVariant(selectedItemId);
                    break;
                case "Add":
                    doCallback = false;
                    AddVariant();
                    break;
                case "ViewHierarchy":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageSiteHierarchy", "Mode=View");
                    break;
                case "Move":
                    doCallback = false;
                    OpeniAppsAdminPopup("ManageSiteHierarchy", "Mode=Move&SiteId=" + gridActionsJson.SelectedItems[0]);
                    break;
            }

            if (doCallback && typeof grdVariantList != "undefined") {
                grdVariantList.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdVariantList.callback();
            }
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="cphContent" ID="cntBody" runat="server">
    <div class="site-variant">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdVariantList" SkinID="Default" runat="server" Width="100%"
            RunningMode="Callback" AllowEditing="false" AutoCallBackOnInsert="true" CallbackCachingEnabled="true"
            CssClass="fat-grid" AutoCallBackOnUpdate="true" ShowFooter="false" CallbackCacheLookAhead="10"
            AllowMultipleSelect="false" LoadingPanelClientTemplateId="grdVariantListLoadingPanelTemplate" ManualPaging="true">
            <ClientEvents>
                <Load EventHandler="grdVariantList_OnLoad" />
                <CallbackComplete EventHandler="grdVariantList_OnCallbackComplete" />
                <RenderComplete EventHandler="grdVariantList_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <ConditionalFormats>
                        <ComponentArt:GridConditionalFormat ClientFilter="DataItem.GetMember('Status').Value==7"
                            RowCssClass="archive-row" SelectedRowCssClass="archive-row" />
                    </ConditionalFormats>
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" />
                        <ComponentArt:GridColumn DataField="StatusName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                        <ComponentArt:GridColumn DataField="PrimarySiteUrl" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                        <ComponentArt:GridColumn DataField="SitePath" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="first-cell">
                                <%--<%# GetGridItemRowNumber(Container.DataItem) %>--%>
                            </div>
                            <div class="middle-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["PrimarySiteUrl"]%>
                                </p>
                            </div>
                            <div class="last-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong> : {1} {2} {3} | <strong>{4}</strong>: {5}", GUIStrings.Updated, String.Format("{0:d}",Container.DataItem["ModifiedDate"]),
                                        GUIStrings.By, Container.DataItem["ModifiedByFullName"], GUIStrings.Status, Container.DataItem["StatusName"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.SiteHierarchy, Container.DataItem["SitePath"]) %>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdVariantListLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdVariantList) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
