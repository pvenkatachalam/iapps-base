﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeBehind="AssignWorkflows.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.AssignWorkflows"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="PT" TagName="PageTree" Src="~/UserControls/Libraries/Data/PageMapTree.ascx" %>
<asp:Content ID="header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdWorkflows_onItemExternalDropSourceToDestination(sender, eventArgs) {
            var newRow;
            var isExist = false;
            var targetItem = eventArgs.get_target();
            var selectedId = eventArgs.get_item().getMember('Id').get_text();

            var count = grdSelectedWorkflows.get_table().getRowCount();
            for (var i = 0; i < count; i++) {
                var wfId = grdSelectedWorkflows.get_table().getRow(i).get_cells(0)[0].Value;

                if ((wfId == selectedId)) {
                    isExist = true;
                }
            }
            if (isExist == false) {
                if (targetItem != null) {
                    newRow = grdSelectedWorkflows.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    newRow = grdSelectedWorkflows.get_table().addEmptyRow();
                }
                grdSelectedWorkflows.beginUpdate();
                newRow.setValue(0, eventArgs.get_item().getMember('Id').get_text());
                newRow.setValue(1, eventArgs.get_item().getMember('Title').get_text());

                grdSelectedWorkflows.endUpdate();
            }
        }

        function DeleteWorkflows_onclick() {
            grdSelectedWorkflows.deleteSelected();
        }
        function AddWorkflows_onclick(sender, eventArgs)//new change added this sender, eventArgs
        {
            var draggedItem;
            if (eventArgs != null)//new 
            {
                draggedItem = eventArgs.get_item();
            }
            else {
                draggedItem = grdWorkflows.GetSelectedItems()[0];
            }
            var isExist = false;
            if (draggedItem != null)//new        
            {
                var count = grdSelectedWorkflows.get_table().getRowCount();
                var selectedId = draggedItem.getMember("Id").get_text();

                for (var i = 0; i < count; i++) {
                    var wfId = grdSelectedWorkflows.get_table().getRow(i).get_cells(0)[0].Value;

                    if ((wfId == selectedId)) {
                        isExist = true;
                    }
                }
                if (isExist == false) {
                    var wfTitle = draggedItem.getMember("Title").get_text();

                    var rowitem;
                    var targetItem;
                    if (eventArgs != null)//new 
                    {
                        targetItem = eventArgs.get_target();
                        if (targetItem != null) {
                            rowitem = grdSelectedWorkflows.get_table().addEmptyRow(targetItem.get_index());
                        }
                        else {
                            rowitem = grdSelectedWorkflows.get_table().addEmptyRow();
                        }
                    }
                    else {
                        rowitem = grdSelectedWorkflows.get_table().addEmptyRow();
                    }
                    grdSelectedWorkflows.edit(rowitem);
                    rowitem.setValue(0, draggedItem.getMember("Id").get_text());
                    rowitem.setValue(1, draggedItem.getMember("Title").get_text());
                    grdSelectedWorkflows.editComplete();
                    grdSelectedWorkflows.unSelect(rowitem);
                    grdSelectedWorkflows.render();
                }
            }
        }

        function grdSelectedWorkflows_onItemDelete(sender, eventArgs) {
            if (eventArgs.get_targetControl().get_id() != document.getElementById('<%=grdSelectedWorkflows.ClientID%>').id) {
                grdSelectedWorkflows.deleteItem(eventArgs.get_item());
            }
            else if (eventArgs.get_targetControl().get_id() == document.getElementById('<%=grdSelectedWorkflows.ClientID%>').id) {
                var targetItem = eventArgs.get_target();
                var newRow;
                if (targetItem != null) {
                    grdSelectedWorkflows.deleteItem(eventArgs.get_item());
                    newRow = grdSelectedWorkflows.get_table().addEmptyRow(targetItem.get_index());
                }
                else {
                    grdSelectedWorkflows.deleteItem(eventArgs.get_item());
                    newRow = grdSelectedWorkflows.get_table().addEmptyRow();
                }
                grdSelectedWorkflows.beginUpdate();
                newRow.setValue(0, eventArgs.get_item().getMember('Id').get_text());
                newRow.setValue(1, eventArgs.get_item().getMember('Title').get_text());

                grdSelectedWorkflows.endUpdate();
            }
        }

        function LoadMenuWorkflows() {
            grdSelectedWorkflows.set_callbackParameter(treeActionsJson.FolderId);
            grdSelectedWorkflows.callback();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="dark-section-header clear-fix">
        &nbsp;
    </div>
    <p><em><%= GUIStrings.AssignWorkflowInstructions %></em></p>
    <div class="left-column">
        <PT:PageTree ID="pageTree" runat="server" />
    </div>
    <div class="right-column clear-fix">
        <div class="form-row">
            <div class="form-value text-value">
                <asp:CheckBox ID="chkPropagate" runat="server" Checked="True" Text="<%$ Resources:GUIStrings, Automaticallypropagateselectedworkflowstoalllevelsbelow %>" />
            </div>
        </div>
        <div class="columns">
            <h3>
                <%= GUIStrings.AllWorkflows %></h3>
            <ComponentArt:Grid ID="grdWorkflows" EditOnClickSelectedItem="false" SkinID="scrollinggrid"
                AllowEditing="true" ShowHeader="False" RunningMode="Client" PageSize="15" ShowFooter="false"
                ImagesBaseUrl="~/App_Themes/General/images/" Width="100%" SearchText="" GroupingNotificationText=""
                GroupingNotificationPosition="TopRight" EnableViewState="true" runat="server" ItemDraggingClientTemplateId="grdWorkflowsDragTemplate"
                ItemDraggingEnabled="true" AllowMultipleSelect="false" ExternalDropTargets="grdSelectedWorkflows"
                Height="485">
                <ClientEvents>
                    <ItemExternalDrop EventHandler="grdWorkflows_onItemExternalDropSourceToDestination" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel AlternatingRowCssClass="alternate-row" DataKeyField="" ShowTableHeading="false"
                        ShowSelectorCells="false" SelectorCellWidth="18" SelectorImageUrl="selector.gif"
                        SelectorImageWidth="17" SelectorImageHeight="15" DataCellCssClass="data-cell"
                        RowCssClass="row" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif" AllowReordering="true" HeadingCellCssClass="heading-cell"
                        HeadingRowCssClass="heading-row" HoverRowCssClass="hover-row" ShowHeadingCells="false">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellCssClass="first-cell last-cell" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdWorkflowsDragTemplate">
                        ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
        <div class="action-column">
            <img src="/Admin/App_Themes/General/images/action-right-arrow.gif" alt="<%= GUIStrings.SelectWorkFlow %>"
                title="<%= GUIStrings.SelectWorkFlow %>" onclick="return AddWorkflows_onclick()" />
            <img src="/Admin/App_Themes/General/images/action-left-arrow.gif" alt="<%= GUIStrings.RemoveWorkflow %>"
                title="<%= GUIStrings.RemoveWorkflow %>" onclick="return DeleteWorkflows_onclick()" />
        </div>
        <div class="columns">
            <h3>
                <%= GUIStrings.WorkflowSequence %></h3>
            <ComponentArt:Grid ID="grdSelectedWorkflows" EditOnClickSelectedItem="false" SkinID="ScrollingGrid"
                EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" ScrollPopupClientTemplateId="ScrollPopupTemplate3"
                AllowEditing="false" RunningMode="Callback" ItemDraggingEnabled="true" PageSize="7"
                ShowFooter="false" ImagesBaseUrl="images/" Width="100%" ExternalDropTargets="grdSelectedWorkflows,grdWorkflows"
                SearchText="" ShowHeader="false" GroupingNotificationText="" GroupingNotificationPosition="TopRight"
                Height="485" runat="server" AutoAdjustPageSize="true" ItemDraggingClientTemplateId="grdSelectedWorkflowsDragTemplate">
                <ClientEvents>
                    <ItemExternalDrop EventHandler="grdSelectedWorkflows_onItemDelete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel AlternatingRowCssClass="alternate-row" DataKeyField="" ShowTableHeading="false"
                        ShowSelectorCells="false" SelectorCellWidth="18" SelectorImageUrl="selector.gif"
                        SelectorImageWidth="17" SelectorImageHeight="15" DataCellCssClass="data-cell"
                        RowCssClass="row" SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif" AllowReordering="true" HeadingCellCssClass="heading-cell"
                        HeadingRowCssClass="heading-row" HoverRowCssClass="hover-row" ShowHeadingCells="false">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" Visible="false" />
                            <ComponentArt:GridColumn runat="server" DataField="Title" DataCellCssClass="first-cell last-cell" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdSelectedWorkflowsDragTemplate">
                        ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <%= GUIStrings.MenuWorkflows%></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            OnClick="btnSave_Click" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
        <a href="ManageWorkflows.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
