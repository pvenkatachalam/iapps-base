﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageTasks.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageTasks"
    StylesheetTheme="General" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <link href="../../common/css/kendo.common.min.css" rel="stylesheet" />
    <link href="../../common/css/kendo.default.min.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h1>Manage Tasks</h1>
    <div id="grid"></div>
    <div id="detail" class="iapps-modal" style="display: none; width: 900px;">
        <div class="modal-header clear-fix">
            <h2>Task Information</h2>
        </div>
        <div class="modal-content" style="max-height: 600px; overflow: auto;">
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
                CssClass="button" OnClientClick="return CloseTaskDetail();" />
        </div>
        <div id="template" style="display: none;">
            <div class="form-row">
                <label class="form-label">
                </label>
                <div class="form-value">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#grid").kendoGrid({
                dataSource: {
                    type: "jsonp",
                    transport: {
                        read:
                            {
                                url: "/Admin/api/task/getitems",
                                dataType: "json"
                            },
                        parameterMap: function (data, type) {
                            var sortBy, sortOrder;
                            var customParams = [];
                            customParams.push({ "Key": "IsScheduled", "Value": true });
                            if (typeof data.sort != "undefined" && data.sort != null && data.sort.length > 0) {
                                sortBy = data.sort[0].field;
                                sortOrder = data.sort[0].dir;
                            }
                            return {
                                SiteId: "8039CE09-E7DA-47E1-BCEC-DF96B5E411F4",
                                PageSize: data.pageSize,
                                PageNumber: data.page,
                                SortBy: sortBy,
                                SortOrder: sortOrder,
                                CustomParameters: customParams
                            };
                        }
                    },
                    schema: {
                        data: "Items",
                        total: "TotalRecords",
                        parse: function (response) {
                            $.each(response.Items, function (index, elem) {
                                if (elem.Schedule != null && elem.Schedule.StartDate && typeof elem.Schedule.StartDate === "string")
                                    elem.Schedule.StartDate = kendo.parseDate(elem.Schedule.StartDate, "yyyy-MM-ddTHH:mm:ss");
                            });
                            return response;
                        }
                    },
                    pageSize: 20,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },
                height: 600,
                filterable: false,
                sortable: true,
                pageable: true,
                selectable: "row",
                change: function (e) {
                    /*var selectedRows = this.select();
                    var selectedDataItems = [];
                    for (var i = 0; i < selectedRows.length; i++) {
                        var dataItem = this.dataItem(selectedRows[i]);
                        selectedDataItems.push(dataItem);
                    }
                    alert(selectedDataItems[0]);*/
                    var selectedRow = this.dataItem(this.select());
                    PopulateDetail(selectedRow.Id);
                },
                columns: [
                    {
                        field: "Title",
                        title: "Title",
                        width: 300
                    }, {
                        field: "TaskStatus",
                        title: "Status",
                        width: 260
                    }, {
                        field: "Schedule.StartDate",
                        title: "Start Date",
                        width: 150,
                        format: "{0:MM/dd/yyyy}"
                    }
                ]
            });
        });

        function PopulateDetail(selectedId) {
            $("#detail").iAppsDialog("open");

            $.get("/Admin/api/task/getitem?Id=" + selectedId, function (data) {
                $(".modal-content").html("");
                $("#template .form-row").clone()
                   .find(".form-label").html("Title").end()
                   .find(".form-value").html(data.Title).end()
                   .appendTo(".modal-content");

                $("#template .form-row").clone()
                   .find(".form-label").html("Status").end()
                   .find(".form-value").html(data.TaskStatus).end()
                   .appendTo(".modal-content");

                $("#template .form-row").clone()
                  .find(".form-label").html("Last Runtime").end()
                  .find(".form-value").html(data.Schedule.LastRunTime).end()
                  .appendTo(".modal-content");

                $("#template .form-row").clone()
                  .find(".form-label").html("Next Runtime").end()
                  .find(".form-value").html(data.Schedule.NextRunTime).end()
                  .appendTo(".modal-content");

                $("<h3>History</h3><div id='history'></div>").appendTo(".modal-content");

                $("#history").kendoGrid({
                    dataSource: {
                        data: data.History,
                        schema: {
                            parse: function (response) {
                                $.each(response, function (i, history) {
                                    if (history.StartTime && typeof history.StartTime === "string")
                                        history.StartTime = kendo.parseDate(history.StartTime, "yyyy-MM-ddTHH:mm:ss.fff");
                                    if (history.EndTime && typeof history.EndTime === "string")
                                        history.EndTime = kendo.parseDate(history.EndTime, "yyyy-MM-ddTHH:mm:ss.fff");
                                });
                                return response;
                            }
                        },
                    },
                    columns: [
                        {
                            field: "StartTime",
                            title: "Start Time",
                            width: 150,
                            format: "{0:MM/dd/yyyy HH:mm:ss}"
                        }, {
                            field: "EndTime",
                            title: "End Time",
                            width: 150,
                            format: "{0:MM/dd/yyyy HH:mm:ss}"
                        }, {
                            field: "Result",
                            title: "Result",
                            width: 150,
                        }, {
                            field: "Exceptions",
                            title: "Message",
                            width: 150,
                        }
                    ]
                });

            });

            return false;
        }

        function CloseTaskDetail() {
            $("#detail").iAppsDialog("close");

            return false;
        }
    </script>
</asp:Content>

<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
</asp:Content>
