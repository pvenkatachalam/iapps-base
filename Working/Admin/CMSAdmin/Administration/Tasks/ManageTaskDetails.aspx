<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Theme="General"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageTaskDetails" CodeBehind="ManageTaskDetails.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <link rel="stylesheet" type="text/css" href="../../common/css/jquery.datetimepicker.css" />
    <script type="text/javascript" src="../../common/jquery/jquery.datetimepicker.js"></script>
    <script type="text/javascript" src="../../common/jquery/jquery.validate.min.js"></script>
    <script type="text/javascript" language="javascript">
        $(function () {
            $("input[type='date']").datetimepicker();

            $(".task-properties").validate();
        });
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>Manage Task Details</h1>
        <asp:Button ID="btnAddTask" runat="server" Text="Add Task" CssClass="primarybutton" />
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="task-properties clear-fix">
        <div class="form-row">
            <label class="form-label">Task Name</label>
            <div class="form-value">
                <asp:TextBox ID="txtName" runat="server" Width="400" required />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">Assembly Name</label>
            <div class="form-value">
                <asp:TextBox ID="txtAssemblyName" runat="server" Width="400" required />
            </div>
        </div>
         <div class="form-row">
            <label class="form-label">Full Qualified class Name</label>
            <div class="form-value">
                <asp:TextBox ID="txtClassName" runat="server" Width="400" required />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">Method Name</label>
            <div class="form-value">
                <asp:TextBox ID="txtMethodName" runat="server" Width="400" required />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">Parameters
                <span class="coaching-text">Enter as Type:value and separate params by |</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtParams" runat="server" Width="400" TextMode="MultiLine" required />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">Schedule Type</label>
            <div class="form-value">
                <asp:DropDownList ID="ddlSchedule" runat="server">
                    <asp:ListItem Value="0" Text="Schedule Once" />
                    <asp:ListItem Value="1" Text="Schedule Daily" />
                    <asp:ListItem Value="2" Text="Schedule Weekly" />
                    <asp:ListItem Value="3" Text="Schedule Monthly" />
                    <asp:ListItem Value="4" Text="Schedule Yearly" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">Start Date & Time</label>
            <div class="form-value">
                <asp:TextBox ID="txtStartDate" runat="server" Width="400" TextMode="Date" required />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">Maximum Occurence
                <span class="coaching-text">Blank for unlimited</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtMaxOccurence" runat="server" Width="400" />
            </div>
        </div>
    </div>
</asp:Content>
