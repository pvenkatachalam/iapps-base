<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="false" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageWebsiteUser" StylesheetTheme="general"
    CodeBehind="ManageWebsiteUser.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdWebsiteUser_OnLoad(sender, eventArgs) {
            $(".manage-users").gridActions({ objList: grdWebsiteUser });
        }

        function grdWebsiteUser_OnRenderComplete(sender, eventArgs) {
            $(".manage-users").gridActions("bindControls");

            $(".view-sec-levels").iAppsClickMenu({
                width: "250px",
                heading: "<%= GUIStrings.SecurityLevels %>",
                create: function (e, menu) {
                    var secLevels = grdWebsiteUser.getItemFromKey(0, e.attr("objectId")).getMember("AllSecurityLevel").get_value().split('#');
                    if (secLevels.length > 0) {
                        $.each(secLevels, function () {
                            if (this.toString() != "")
                                menu.addListItem(this.toString());
                        });
                    }
                }
            });
        }

        function grdWebsiteUser_OnCallbackComplete(sender, eventArgs) {
            $(".manage-users").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;
            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    window.location = "ManageWebsiteUserDetails.aspx";
                    break;
                case "Edit":
                    doCallback = false;
                    window.location = "ManageWebsiteUserDetails.aspx?UserId=" + selectedItemId;
                    break;
                case "Delete":
                    doCallback = DeleteWebsiteUser(selectedItem, true);
                    break;
                case "Deactivate":
                    doCallback = DeleteWebsiteUser(selectedItem, false, false);
                    break;
                case "Activate":
                    doCallback = DeleteWebsiteUser(selectedItem, false, true);
                    break;
            }

            if (doCallback) {
                grdWebsiteUser.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdWebsiteUser.callback();
            }
        }

        function DeleteWebsiteUser(selectedItem, deleteUser, activateUser) {
            var selectedUserId = selectedItem.getMember("UserId").get_text();
            var selectedUserName = selectedItem.getMember("UserName").get_text();

            if (deleteUser)
                isConfirm = window.confirm(GetMessage("DeleteConfirmation"));
            else if (activateUser)
                isConfirm = window.confirm(GetMessage("ActivateConfirmation"));
            else
                isConfirm = window.confirm(GetMessage("DeactivateConfirmation"));

            if (isConfirm) {
                var statusOfOperation = DeleteDeactivateWebsiteUser(selectedUserId, selectedUserName, deleteUser ? 1 : (activateUser ? 3 : 2), null);
                if (statusOfOperation == "True")
                    return true;
                else
                    alert("Error in deleting this user.");
            }

            return false;
        }

        function Grid_ItemSelect(sender, eventArgs) {
            if (eventArgs.selectedItem.getMember("IsActivated").get_text().toLowerCase() == "false") {
                sender.getItemByCommand("Deactivate").setText("<%= JSMessages.DeactivateUser %>");
                sender.getItemByCommand("Deactivate").setComandArgument("Deactivate");
                sender.getItemByCommand("Deactivate").removeClass("activate");
                sender.getItemByCommand("Deactivate").addClass("deactivate");
            }
            else {
                sender.getItemByCommand("Deactivate").setText("<%= JSMessages.ActivateUser %>");
                sender.getItemByCommand("Deactivate").setComandArgument("Activate");
                sender.getItemByCommand("Deactivate").removeClass("deactivate");
                sender.getItemByCommand("Deactivate").addClass("activate");
            }

            if (gridActionsJson.Status == 3) {
                sender.getItemByCommand("Approve").showButton();
                sender.getItemByCommand("Reject").showButton();
            }
            else {
                sender.getItemByCommand("Approve").hideButton();
                sender.getItemByCommand("Reject").hideButton();
            }            
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manage-users">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdWebsiteUser" SkinID="Default" runat="server" Width="100%" ManualPaging="true"
            RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdWebsiteUserLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdWebsiteUser_OnLoad" />
                <CallbackComplete EventHandler="grdWebsiteUser_OnCallbackComplete" />
                <RenderComplete EventHandler="grdWebsiteUser_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="UserId" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="UserName" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="LastName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="FirstName" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="UserId" Visible="false" />
                        <ComponentArt:GridColumn DataField="Email" Visible="false" />
                        <ComponentArt:GridColumn DataField="ExpiryDate" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsActivated" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsApproved" Visible="false" />
                        <ComponentArt:GridColumn DataField="StatusText" Visible="false" />
                        <ComponentArt:GridColumn DataField="SecurityLevelText" Visible="false" />
                        <ComponentArt:GridColumn DataField="AllSecurityLevel" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["UserId"]%>'>
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["UserName"]%>
                                </h4>
                                <p>
                                    <%# String.Format("{0}, {1}",  Container.DataItem["LastName"], Container.DataItem["FirstName"])%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["StatusText"])%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.SecurityLevels, Container.DataItem["SecurityLevelText"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdWebsiteUserLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdWebsiteUser) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
