<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageWebsiteUserDetails" Theme="General" CodeBehind="ManageWebsiteUserDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="IndexTerms" Src="~/UserControls/AssignTagsWithListbox.ascx" %>
<%@ Register TagPrefix="UC" TagName="SecurityLevels" Src="~/UserControls/Libraries/SecurityLevelWithListbox.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript" language="javascript">
        function cbGroups_Callback(operation) {
            cbGroups.set_callbackParameter(operation);
            cbGroups.callback();
            return false;
        }        

        function OpenAddGroup() {
            $("#txtGroupName").val();
            $("#txtGroupDescription").val();
            $("#addNewGroup").iAppsDialog("open");
            return false;
        }

        function CancelAddGroup() {
            $("#addNewGroup").iAppsDialog("close");
            return false;
        }

        function SaveAddGroup() {
            if (Page_ClientValidate("valAddGroup") == true) {
                cbGroups_Callback('AddNewGroup');

                $("#addNewGroup").iAppsDialog("close");
            }
            return false;
        }

        function popUpExpiryDateCalendar(ctl) {
            var thisDate = new Date();
            if (document.getElementById("<%=hdnDate.ClientID%>").value != "") {
                thisDate = new Date(document.getElementById("<%=hdnDate.ClientID%>").value);
                expirationCalendar.setSelectedDate(thisDate)
            }

            if (!(expirationCalendar.get_popUpShowing()));
            expirationCalendar.show(ctl);
        }

        function calExpiryDate_onSelectionChanged(sender, eventArgs) {
            var selectedDate = expirationCalendar.getSelectedDate();
            if (selectedDate <= new Date()) {
                alert("<asp:localize runat='server' text='<%$ Resources:GUIStrings, ExpiryDateShouldbeGreaterThanToday %>'/>");
            }
            else {
                document.getElementById("<%=expiryDate.ClientID%>").value = expirationCalendar.formatDate(selectedDate, shortCultureDateFormat);
                document.getElementById("<%=hdnDate.ClientID%>").value = expirationCalendar.formatDate(selectedDate, calendarDateFormat);
            }
        }

        function ShowImagePopup() {
            OpeniAppsAdminPopup("SelectImageLibraryPopup", "ForceEnableSelection=true", "SelectImage");
        }

        function SelectImage() {
            var imageUrl = popupActionsJson.CustomAttributes.ImageUrl.replace(new RegExp(jPublicSiteUrl, "gi"), "");
            var imageFileName = popupActionsJson.CustomAttributes.ImageFileName;
            imageUrl = imageUrl.replace(new RegExp(imageFileName, "gi"), "thumbs/thumb_" + imageFileName);
            $("#imgImageURL").prop("src", imageUrl);
            $("#hdnImageId").val(popupActionsJson.SelectedItems[0]);
        }

        function ClearImageUrlValue() {
            $("#imgImageURL").prop("src", "/Admin/App_Themes/General/images/no_photo.jpg");
            $("#hdnImageId").val("");
        }        
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <%= GUIStrings.AddEditWebsiteUser %></h1>
        <asp:Button ID="btnSaveUser" runat="server" OnClick="btnSaveUser_Click" Text="<%$ Resources:GUIStrings, Save %>"
            ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" ValidationGroup="valSaveUser" />
        <a href="ManageWebsiteUser.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
        ValidationGroup="valSaveUser" />
    <asp:HiddenField ID="hdnUserId" runat="server" />
    <div class="dark-section-header clear-fix">
        <h2><%= GUIStrings.USERDETAILS %></h2>
    </div>
    <div class="profile-properties clear-fix">
        <div class="profile-picture picture-column">
            <div class="form-row">
                <asp:Image runat="server" ID="imgImageURL" ImageUrl="~/App_Themes/General/images/no_photo.jpg"
                    ClientIDMode="Static" />
            </div>
            <div class="form-row">
                <a href="#" class="edit-image" onclick="return ShowImagePopup();">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, EditProfilePicture %>" /></a>
                <a href="#" class="remove-image" onclick="return ClearImageUrlValue();">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, Remove %>" /></a>
                <asp:HiddenField ID="hdnImageId" runat="server" ClientIDMode="Static" />
            </div>
        </div>
        <div class="clear-fix">
            <div class="columns">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FirstName %>" /><span
                            class="req">&nbsp;*</span></label>
                    <div class="form-value">
                        <asp:TextBox ID="firstName" runat="server" CssClass="textBoxes" Width="280" />
                        <asp:RequiredFieldValidator ID="firstNameValidator" ControlToValidate="firstName"
                            runat="server" ErrorMessage="<%$ Resources: JSMessages, FirstNameRequired %>"
                            Display="None" SetFocusOnError="true" ValidationGroup="valSaveUser" />
                        <iAppsControls:iAppsCustomValidator ID="cvfirstName" ControlToValidate="firstName"
                            ErrorMessage="<%$ Resources: JSMessages, FirstNameNotValid %>"
                            runat="server" ValidateType="Name" ValidationGroup="valSaveUser" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, LastName %>" /><span
                            class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="lastName" runat="server" CssClass="textBoxes" Width="280" />
                        <asp:RequiredFieldValidator ID="lastNameValidator" ControlToValidate="lastName" ErrorMessage="<%$ Resources: JSMessages, LastNameRequired %>"
                            Display="None" runat="server" ValidationGroup="valSaveUser" />
                        <iAppsControls:iAppsCustomValidator ID="cvlastName" ControlToValidate="lastName"
                            ErrorMessage="<%$ Resources: JSMessages, LastNameNotValid %>"
                            runat="server" ValidateType="Name" ValidationGroup="valSaveUser" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Email %>" /><span
                            class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="email" runat="server" CssClass="textBoxes" Width="280" />
                        <asp:RequiredFieldValidator ID="emailValidator" ControlToValidate="email" ErrorMessage="<%$ Resources: JSMessages, EmailRequired %>"
                            Display="None" runat="server" ValidationGroup="valSaveUser" />
                        <iAppsControls:iAppsCustomValidator ID="cvemailValidator" ControlToValidate="email"
                            ErrorMessage="<%$ Resources: JSMessages, EmailNotValid %>"
                            runat="server" ValidateType="Email" ValidationGroup="valSaveUser" />
                    </div>
                </div>
            </div>
            <div class="columns right-column">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, UserName %>" /><span
                            class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="userName" runat="server" CssClass="textBoxes" Width="274" />
                        <asp:RequiredFieldValidator ID="usernameValidator" ControlToValidate="userName" ErrorMessage="<%$ Resources: JSMessages, UserNameRequired %>"
                            Display="None" runat="server" ValidationGroup="valSaveUser" />
                        <iAppsControls:iAppsCustomValidator ID="cvusernameValidator" ControlToValidate="userName"
                            ErrorMessage="<%$ Resources: JSMessages, UserNameNotValid %>"
                            runat="server" ValidateType="UserName" ValidationGroup="valSaveUser" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Password %>" /><span
                            class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="Password" TextMode="Password" runat="server" CssClass="textBoxes"
                            Width="274" AutoCompleteType="None" autocomplete="off" />
                        <asp:RequiredFieldValidator ID="passwordValidator" ControlToValidate="password" ErrorMessage="<%$ Resources: JSMessages, PasswordRequired %>"
                            Display="None" runat="server" ValidationGroup="valSaveUser" />
                    <asp:RegularExpressionValidator ID="regtxtPassword" runat="server" ControlToValidate="Password"
                        ErrorMessage="<%$ Resources:JSMessages, InvalidCharactersInPassword %>" SetFocusOnError="True"
                        ValidationExpression="^[^<>]+$" ValidationGroup="valSaveUser" Display="None" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ConfirmPassword %>" /><span
                            class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="confirmPassword" TextMode="Password" runat="server" CssClass="textBoxes"
                            Width="274" AutoCompleteType="None" autocomplete="off" />
                        <asp:CompareValidator ID="passwordCompareValidator" ControlToValidate="Password"
                            ValidationGroup="valSaveUser" runat="server" ControlToCompare="confirmPassword"
                            Display="None" ErrorMessage="<%$ Resources: JSMessages, ConfirmPasswordRequired %>">
                        </asp:CompareValidator>
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, ExpirationDateColon %>" />
                    </label>
                    <div class="form-value calendar-value">
                        <asp:HiddenField ID="hdnDate" runat="server" />
                        <asp:TextBox ID="expiryDate" runat="server" CssClass="textBoxes" Width="262" AutoCompleteType="none"></asp:TextBox>
                        <span>
                            <img id="calendar_button" class="buttonCalendar" alt="<%= GUIStrings.ShowCalendar %>"
                                src="../../App_Themes/General/images/calendar-button.png" onclick="popUpExpiryDateCalendar(this);" />
                        </span>
                        <ComponentArt:Calendar runat="server" SkinID="Default" PickerFormat="Custom" ID="expirationCalendar"
                            MaxDate="9998-12-31" PopUpExpandControlId="calendar_button">
                            <ClientEvents>
                                <SelectionChanged EventHandler="calExpiryDate_onSelectionChanged" />
                            </ClientEvents>
                        </ComponentArt:Calendar>
                        <asp:CompareValidator ID="cvexpiryDate" runat="server" ControlToValidate="expiryDate"
                            Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources: JSMessages, DateNotValid %>"
                            Display="None" ValidationGroup="valSaveUser" />
                        <asp:RangeValidator ID="expiryDateValidator" runat="server" ControlToValidate="expiryDate"
                            ErrorMessage="<%$ Resources: JSMessages, DateNotValid %>"
                            Display="None" SetFocusOnError="true" MaximumValue="01/13/3000" MinimumValue="01/01/1900"
                            Type="Date" ValidationGroup="valSaveUser" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="iapps-modal" id="addNewGroup" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, AddNewGroup %>" />
            </h2>
        </div>
        <div class="modal-content">
            <asp:ValidationSummary ID="valAddGroup" runat="server" ShowSummary="false" ShowMessageBox="True"
                ValidationGroup="valAddGroup" />
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings, GroupName %>" /><span
                        class="req">&nbsp;*</span>
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtGroupName" runat="server" CssClass="textBoxes" Width="260" ClientIDMode="Static" />
                    <asp:RequiredFieldValidator ID="reqtxtGroupName" runat="server" Display="None" SetFocusOnError="true"
                        ControlToValidate="txtGroupName" ErrorMessage="<%$ Resources: JSMessages, GroupNameRequired %>"
                        ValidationGroup="valAddGroup" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtGroupName" ControlToValidate="txtGroupName"
                        ErrorMessage="<%$ Resources: JSMessages, GroupNameNotValid %>"
                        runat="server" ValidateType="Name" ValidationGroup="valAddGroup" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize20" runat="server" Text="<%$ Resources:GUIStrings, Description %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtGroupDescription" runat="server" CssClass="textBoxes" Width="260"
                        TextMode="MultiLine" ClientIDMode="Static" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtGroupDescription" ControlToValidate="txtGroupDescription"
                        ErrorMessage="<%$ Resources: JSMessages, DescriptionNotValid %>"
                        runat="server" ValidateType="Description" ValidationGroup="valAddGroup" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnCancelGroup" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
                ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CancelAddGroup();"
                CausesValidation="false" />
            <asp:Button ID="btnSaveGroup" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                OnClientClick="return SaveAddGroup();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"
                ValidationGroup="valAddGroup" />
        </div>
    </div>
    <div class="bottom-section clear-fix">
        <div class="dark-section-header clear-fix" style="margin-top: 50px;">
            <h2><%= GUIStrings.UserGroups %></h2>
        </div>
        <iAppsControls:CallbackPanel ID="cbGroups" runat="server" CssClass="clear-fix">
            <ContentTemplate>
                <div class="columns">
                    <label class="form-label">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, SitesBr %>" /></label>
                    <div class="form-value">
                        <asp:ListBox ID="lstSites" runat="server"
                            Rows="6" SelectionMode="Single" Width="373" />
                    </div>
                </div>
                <div class="action-column">
                    <img src="/Admin/App_Themes/General/images/icon-plus.gif" alt="Plus" title="Plus" />
                </div>
                <div class="columns center-column form-edit-row">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, GroupsBr %>" />
                            <a href="#" onclick="return OpenAddGroup();" class="button small-button">
                                <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, AddNewGroup %>" /></a>
                        </label>
                        <div class="form-value">
                            <asp:ListBox ID="lstGroups" runat="server" Width="373" SelectionMode="Multiple" />
                        </div>
                    </div>
                </div>
                <div class="action-column">
                    <img id="rightArrow" src="~/App_Themes/General/images/action-right-arrow.gif" title="<%$ Resources:GUIStrings, ClickToAdd %>"
                        alt="<%$ Resources:GUIStrings, ClickToAdd %>" runat="server" onclick="return cbGroups_Callback('AddToGroup');" /><br />
                    <img id="leftArrow" src="~/App_Themes/General/images/action-left-arrow.gif" title="<%$ Resources:GUIStrings, ClickToRemove %>"
                        alt="<%$ Resources:GUIStrings, ClickToRemove %>" runat="server" onclick="return cbGroups_Callback('RemoveFromGroup');" />
                </div>
                <div class="columns">
                    <div class="form-row">
                        <label class="form-label">
                            <asp:Localize ID="Localize21" runat="server" Text="<%$ Resources:GUIStrings,SelectedSitesGroups %>" />
                        </label>
                        <div class="form-value">
                            <asp:ListBox ID="lstUserGroups" runat="server" Width="373" SelectionMode="Multiple" />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="bottom-section clear-fix">
        <div class="dark-section-header clear-fix" style="margin-top: 50px;">
            <h2><%= GUIStrings.UserSiteAccess %></h2>
        </div>
        <div class="clear-fix">
            <div class="columns">
                <UC:SecurityLevels ID="securityLevels" runat="server" />
            </div>
            <div class="action-column">
                &nbsp;</div>
            <div class="columns">
                <UC:IndexTerms ID="indexs" runat="server" ListBoxWidth="375" />
            </div>
        </div>
    </div>
</asp:Content>
