﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageWebsiteGroupDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageWebsiteGroupDetails"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<%@ Register TagPrefix="UC" TagName="SecurityLevels" Src="~/UserControls/Libraries/SecurityLevelWithListbox.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function UpdateAvailableUser() {
            grdAvailableUsers.set_callbackParameter($("#ddlType").val());
            grdAvailableUsers.callback();

            return false;
        }

        function AddSelectedUserToGroup() {
            for (var selIndex = 0; selIndex < grdAvailableUsers.getSelectedItems().length; selIndex++) {
                var gSelItem = grdAvailableUsers.getSelectedItems()[selIndex];

                if (grdSelectedUsers.getItemFromKey(0, gSelItem.GetMemberAt(0).get_text()) == null) {
                    grdSelectedUsers.beginUpdate();

                    var newItem = grdSelectedUsers.get_table().addEmptyRow(0);
                    grdSelectedUsers.edit(newItem);

                    newItem.setValue(0, gSelItem.GetMemberAt(0).get_text());
                    newItem.setValue(1, gSelItem.GetMemberAt(1).get_text());
                    newItem.setValue(2, $("#ddlType").val());
                    grdSelectedUsers.editComplete();
                }
            }
        }

        function RemoveSelectedUserFromGroup() {
            for (var selIndex in grdSelectedUsers.getSelectedItems()) {
                var gSelItem = grdSelectedUsers.getSelectedItems()[selIndex];

                grdSelectedUsers.unSelect(gSelItem);
                grdSelectedUsers.deleteItem(gSelItem);
            }
        }

        function validateGroupName(oSrc, args) {
            var GroupPattern = /^[a-zA-Z0-9 ]+$/

            if (args.Value.search(GroupPattern) == -1 || args.Value.length > 256) // If match failed
                args.IsValid = false;
            else
                args.IsValid = true;
        }

        function validateDescription(sender, args) {
            var description = args.Value;
            var compare1 = "<";
            var compare2 = ">";

            if (description.search(compare1) == -1 || description.search(compare2) == -1)
                args.IsValid = true;
            else
                args.IsValid = false;
        }



        function OpenEditSecLevel() {
            OpeniAppsAdminPopup("ManageSecurityLevelDetails", "", "RefreshSecurityLevel");
            return false;
        }
        function RefreshSecurityLevel() {
            cbSecurityLevel.callback();
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" HeaderText=""
        DisplayMode="List" ShowMessageBox="True"></asp:ValidationSummary>
    <div class="top-section clear-fix">
        <div class="dark-section-header clear-fix">
            <h2><%= GUIStrings.GROUPDETAILS %></h2>
        </div>
        <div class="columns">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, GroupName %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtGroupName" CssClass="textBoxes" runat="server" Width="380" />
                    <asp:RequiredFieldValidator ID="RFVGroupName" runat="server" Display="None" SetFocusOnError="true"
                        ControlToValidate="txtGroupName" ErrorMessage="<%$ Resources: JSMessages, GroupNameRequired %>"></asp:RequiredFieldValidator>
                    <asp:CustomValidator ID="groupNameCheckCustomValidator" runat="server" ControlToValidate="txtGroupName"
                        Display="None" ErrorMessage="<%$ Resources: JSMessages, GroupNameNotValid %>"
                        ClientValidationFunction="validateGroupName">
                    </asp:CustomValidator>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, DescriptionColon %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" CssClass="textBoxes" runat="server" Width="380"
                        TextMode="MultiLine" />
                    <asp:CustomValidator ID="DescriptionValidator" ClientValidationFunction="validateDescription"
                        runat="server" ControlToValidate="txtDescription" ErrorMessage="<%$ Resources: JSMessages, DescriptionNotValid %>"
                        Display="None" SetFocusOnError="true"></asp:CustomValidator>
                </div>
            </div>
            <UC:SecurityLevels ID="securityLevels" runat="server" />
        </div>
        <div class="columns right-column">
            <h3>
                <%= GUIStrings.Tags %></h3>
            <div class="form-row assign-tags">
                <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="260" />
            </div>
        </div>
    </div>
    <div class="bottom-section clear-fix">
        <div class="dark-section-header clear-fix" style="margin-top: 50px;">
            <h2><%= GUIStrings.AddUsersGroups %></h2>
        </div>
        <div class="columns">
            <h4>
                <%= GUIStrings.SelectUserGroups %></h4>
            <div class="grid-utility clear-fix">
                <select onchange="UpdateAvailableUser();" id="ddlType">
                    <option value="1">User</option>
                    <option value="2">Group</option>
                </select>
                <div class="search-box iapps-client-grid-search">
                    <input id="txtSearch" type="text" class="textBoxes" title="<%= GUIStrings.TypeHereToFilterResults %>"
                        style="width: 160px;" />
                    <input type="button" class="button small-button" value="<%= GUIStrings.Filter %>"
                        onclick="SearchClientGrid('txtSearch', grdAvailableUsers);" />
                </div>
            </div>
            <div class="scroll-box">
                <ComponentArt:Grid ID="grdAvailableUsers" SkinID="Default" runat="server" Width="100%"
                    AllowEditing="true" EditOnClickSelectedItem="false" RunningMode="Callback" CssClass="fat-grid"
                    ShowFooter="false" LoadingPanelClientTemplateId="grdWebsiteGroupAvailableLoadingPanelTemplate" EmptyGridText="<%$ Resources:GUIStrings, NoUsersFound %>">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                            SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                            SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                            HoverRowCssClass="hover-row">
                            <Columns>
                                <ComponentArt:GridColumn DataField="Id" Visible="false" />
                                <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate" />
                                <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ServerTemplates>
                        <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                            <Template>
                                <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["Id"]%>'>
                                    <div class="first-cell">
                                        <%# Container.DataItem["CurrentRowIndex"]%>
                                    </div>
                                    <div class="middle-cell">
                                        <%# Container.DataItem["Title"]%>
                                    </div>
                                </div>
                            </Template>
                        </ComponentArt:GridServerTemplate>
                    </ServerTemplates>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="grdWebsiteGroupAvailableLoadingPanelTemplate">
                            ## GetGridLoadingPanelContent(grdAvailableUsers) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
        <div class="action-column">
            <img id="rightArrow" src="~/App_Themes/General/images/action-right-arrow.gif" title="<%$ Resources:GUIStrings, ClickToAdd %>"
                alt="<%$ Resources:GUIStrings, ClickToAdd %>" runat="server" onclick="return AddSelectedUserToGroup();" /><br />
            <img id="leftArrow" src="~/App_Themes/General/images/action-left-arrow.gif" title="<%$ Resources:GUIStrings, ClickToRemove %>"
                alt="<%$ Resources:GUIStrings, ClickToRemove %>" runat="server" onclick="return RemoveSelectedUserFromGroup();" />
        </div>
        <div class="columns right-column">
            <h4>
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, SelectedUsersGroups %>" /></h4>
            <div class="scroll-box">
                <ComponentArt:Grid ID="grdSelectedUsers" SkinID="Default" runat="server" Width="100%"
                    RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdWebsiteGroupSelectedLoadingPanelTemplate"
                    EmptyGridText="<%$ Resources:GUIStrings, NoUsersGroupsSelected %>">
                    <Levels>
                        <ComponentArt:GridLevel DataKeyField="Id" ShowTableHeading="false" ShowSelectorCells="false"
                            RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                            HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" SelectedRowCssClass="selected-row"
                            SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif" SortImageWidth="8"
                            SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                            ShowHeadingCells="false" HoverRowCssClass="hover-row">
                            <Columns>
                                <ComponentArt:GridColumn FixedWidth="true" AllowEditing="false" Align="left" DataField="Id"
                                    Visible="false" />
                                <ComponentArt:GridColumn FixedWidth="true" EditControlType="Default" AllowReordering="False"
                                    DataField="Title" DataCellCssClass="first-cell last-cell" DataCellClientTemplateId="DetailsClientTemplateSelected" />
                                <ComponentArt:GridColumn FixedWidth="true" DataField="Type" EditControlType="Default"
                                    AllowReordering="False" Visible="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="DetailsClientTemplateSelected">
                            <div class="collection-row grid-item clear-fix">
                                <div class="first-cell">
                                    ## GetGridItemRowNumber(grdSelectedUsers, DataItem) ##
                                </div>
                                <div class="middle-cell">
                                    ## DataItem.getMember('Title').get_text() ##
                                </div>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="grdWebsiteGroupSelectedLoadingPanelTemplate">
                            ## GetGridLoadingPanelContent(grdSelectedUsers) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="buttons" runat="server" ContentPlaceHolderID="cphHeaderButtons">
    <div class="page-header clear-fix">
        <h1>
            <%= GUIStrings.AddEditGroup %></h1>
        <asp:Button ID="btnSaveUser" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
            OnClick="btnSaveGroup_Click" ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" />
        <a href="ManageWebsiteGroups.aspx" class="button">
            <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" /></a>
    </div>
</asp:Content>
