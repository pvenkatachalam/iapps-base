﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.Common.Web.ManageAdminGroup"
    MasterPageFile="~/MasterPage.master" StylesheetTheme="General" CodeBehind="ManageAdminGroup.aspx.cs" %>
     
<%@ Register TagPrefix="UC" TagName="Group" Src="~/UserControls/Administration/User/ManageAdminGroup.ascx" %>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">  
    <UC:Group ID="cltManageAdminGroup" runat="server" />
</asp:Content>
