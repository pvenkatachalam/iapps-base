﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeBehind="ManageWebsiteGroups.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageWebsiteGroups" EnableEventValidation="false"
    StylesheetTheme="General" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdWebsiteGroups_OnLoad(sender, eventArgs) {
            $(".website-groups").gridActions({ objList: grdWebsiteGroups });
        }

        function grdWebsiteGroups_onRenderComplete(sender, eventArgs) {
            $(".website-groups").gridActions("bindControls");

            $(".view-sec-levels").iAppsClickMenu({
                width: "250px",
                heading: "<%= GUIStrings.SecurityLevels %>",
                create: function (e, menu) {
                    var secLevels = grdWebsiteGroups.getItemFromKey(0, e.attr("objectId")).getMember("AllSecurityLevel").get_value().split('#');
                    if (secLevels.length > 0) {
                        $.each(secLevels, function () {
                            if (this.toString() != "")
                                menu.addListItem(this.toString());
                        });
                    }
                }
            });
        }

        function grdWebsiteGroups_OnCallbackComplete(sender, eventArgs) {
            $(".website-groups").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];
            if (eventArgs)
                selectedItem = eventArgs.selectedItem;
            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    window.location = "ManageWebsiteGroupDetails.aspx";
                    break;
                case "Edit":
                    doCallback = false;
                    window.location = "ManageWebsiteGroupDetails.aspx?GroupId=" + selectedItemId;
                    break;
                case "Delete":
                    doCallback = DeleteWebsiteGroup(selectedItem, true);
                    break;
                case "Deactivate":
                    doCallback = DeleteWebsiteGroup(selectedItem, false, false);
                    break;
                case "Activate":
                    doCallback = DeleteWebsiteGroup(selectedItem, false, true);
                    break;
            }

            if (doCallback) {
                grdWebsiteGroups.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdWebsiteGroups.callback();
            }
        }

        function DeleteWebsiteGroup(selectedItem, deleteUser, activateUser) {
            var selectedGroupId = selectedItem.getMember("Id").get_text();
            var selectedGroupName = selectedItem.getMember("Title").get_text();
            var memberType = 2;
            if (deleteUser)
                isConfirm = window.confirm(GetMessage("DeleteConfirmation"));
            else if (activateUser)
                isConfirm = window.confirm(GetMessage("ActivateConfirmation"));
            else
                isConfirm = window.confirm(GetMessage("DeactivateConfirmation"));

            if (isConfirm) {
                var statusOfOperation = DeleteDeactivateGroup(selectedGroupId, selectedGroupName, deleteUser ? 1 : (activateUser ? 3 : 2), null);
                if (statusOfOperation == "True")
                    return true;
                else
                    alert(statusOfOperation);
            }
            return false;
        }

        function Grid_ItemSelect(sender, eventArgs) {
            if (eventArgs.selectedItem.getMember("Status").get_text() == "1") {
                sender.getItemByCommand("Deactivate").setText("<%= JSMessages.DeactivateGroup %>");
                sender.getItemByCommand("Deactivate").setComandArgument("Deactivate");
                sender.getItemByCommand("Deactivate").removeClass("activate");
                sender.getItemByCommand("Deactivate").addClass("deactivate");
            }
            else {
                sender.getItemByCommand("Deactivate").setText("<%= JSMessages.ActivateGroup %>");
                sender.getItemByCommand("Deactivate").setComandArgument("Activate");
                sender.getItemByCommand("Deactivate").removeClass("deactivate");
                sender.getItemByCommand("Deactivate").addClass("activate");
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="website-groups-list">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdWebsiteGroups" SkinID="Default" runat="server" Width="100%"
            RunningMode="Callback" CssClass="fat-grid" ShowFooter="false" LoadingPanelClientTemplateId="grdWebsiteGroupsLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Status" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        <ComponentArt:GridColumn DataField="SecurityLevelText" Visible="false" />
                        <ComponentArt:GridColumn DataField="AllSecurityLevel" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate" runat="server">
                    <Template>
                        <div class="collection-row grid-item clear-fix" objectid='<%# Container.DataItem["Id"]%>'>
                            <div class="large-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                                <p>
                                    <%# Container.DataItem["Description"]%>
                                </p>
                            </div>
                            <div class="small-cell">
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, Container.DataItem["Status"].ToString().ToLower() == "1" ? GUIStrings.Active : GUIStrings.InActive)%>
                                </div>
                                <div class="child-row">
                                    <%# String.Format("<strong>{0}</strong>: {1}", GUIStrings.SecurityLevels, Container.DataItem["SecurityLevelText"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdWebsiteGroupsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdWebsiteGroups) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
            <ClientEvents>
                <RenderComplete EventHandler="grdWebsiteGroups_onRenderComplete" />
                <Load EventHandler="grdWebsiteGroups_OnLoad" />
                <CallbackComplete EventHandler="grdWebsiteGroups_OnCallbackComplete" />
            </ClientEvents>
        </ComponentArt:Grid>
    </div>
</asp:Content>
