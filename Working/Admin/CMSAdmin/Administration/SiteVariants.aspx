﻿<%@  Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="SiteVariants.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.SiteVariants" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.ManageSiteVariants %></h1>
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="left-column">
        <div class="tree-container">
            <div class="tree-header clear-fix">
                <asp:Literal ID="ltTreeHeading" runat="server" />
                <iAppsControls:TreeActions ID="treeActions" runat="server" TreeName="tvSiteVariant" />
            </div>
            <ComponentArt:CallBack ID="cbTreeRefresh" runat="server" SkinID="TreeCallback" CssClass="tree-content">
                <Content>
                    <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvSiteVariant"
                        runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                        DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                        <ClientEvents>
                            <Load EventHandler="tvSiteVariant_onLoad" />
                            <NodeSelect EventHandler="tvSiteVariant_onSelect" />
                            <NodeBeforeRename EventHandler="tvSiteVariant_onBeforeRename" />
                            <NodeBeforeMove EventHandler="tvSiteVariant_onBeforeMove" />
                            <NodeMove EventHandler="tvSiteVariant_onMove" />
                        </ClientEvents>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="TreeNodeTemplate">
                                <div>
                                    ## DataItem.GetProperty('Text') ## 
                                    ## GetTreeNodeCount(DataItem) ## 
                                    ## GetTreeNodeIcons(DataItem) ## 
                                </div>
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                    </ComponentArt:TreeView>
                </Content>
                <ClientEvents>
                    <CallbackComplete EventHandler="cbTreeRefresh_onCallbackComplete" />
                </ClientEvents>
            </ComponentArt:CallBack>
        </div>
    </div>
    <div class="right-column">
    </div>
</asp:Content>
