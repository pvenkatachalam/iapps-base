﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageDevices.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageDevices" StylesheetTheme="General"
    ClientIDMode="Static" %>

<asp:Content ID="cntHeader" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function SelectDevice(id, title, minWidth, maxWidth, previewWidth, previewHeight, isDefault, rowClicked) {
            $("#txtDeviceName").val(title);
            $("#txtMinWidth").val(minWidth);
            $("#txtMaxWidth").val(maxWidth);
            $("#txtPreviewWidth").val(previewWidth);
            $("#txtPreviewHeight").val(previewHeight);
            $("#hdnId").val(id);
            if (isDefault == "True")
                $("#chkSetDefault").attr("checked", true);
            else
                $("#chkSetDefault").attr("checked", false);
            $(rowClicked).parents('tr').addClass('selected-row').siblings('tr').removeClass('selected-row');
            $('#btnSave').val('<%= GUIStrings.Update %>');

            return false;
        }

        function ClearDevice() {
            $("#txtDeviceName").val('');
            $("#txtMinWidth").val('');
            $("#txtMaxWidth").val('');
            $("#txtPreviewWidth").val('');
            $("#txtPreviewHeight").val('');
            $("#hdnId").val('');
            $("#chkSetDefault").attr("checked", false);
            $('.grid tr').removeClass('selected-row');
            $('#btnSave').val('<%= GUIStrings.Add %>');
            return false;
        }

        function OnDeviceSave() {
            if (Page_ClientValidate('deviceSave')) {
                var isValidMinWidth = true;
                var isValidMaxWidth = true;
                var minWidth = parseInt($("#txtMinWidth").val());
                var maxWidth = parseInt($("#txtMaxWidth").val());
                if (minWidth > maxWidth) {
                    alert("<%= JSMessages.MinimumWidthLessThanMaximumWidth %>");
                    return false;
                }

                $(".device-grid tbody tr").each(function () {
                    if (!$(this).hasClass("selected-row")) {
                        var miWidth = parseInt($(this).children("td.min-width").text());
                        var maWidth = parseInt($(this).children("td.max-width").text());

                        if (parseInt(minWidth) >= miWidth && parseInt(minWidth) <= maWidth)
                            isValidMinWidth = false;

                        if (parseInt(maxWidth) >= miWidth && parseInt(maxWidth) <= maWidth)
                            isValidMaxWidth = false;
                    }
                });

                if (!isValidMinWidth) {
                    alert("<%= JSMessages.OverlappingMinimumWidth %>");
                    return false;
                }
                else if (!isValidMaxWidth) {
                    alert("<%= JSMessages.OverlappingMaximumWidth %>");
                    return false;
                }

                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="cndBody" ContentPlaceHolderID="cphContent" runat="server">
    <h1><%= GUIStrings.ManageDevices %></h1>
    <asp:ValidationSummary ID="vsDeviceMgmt" runat="server" ShowMessageBox="true" ShowSummary="false"
        DisplayMode="List" />
    <asp:ObjectDataSource ID="odsDevices" runat="server" EnablePaging="true" EnableViewState="true"
        EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.CMS.Web.ManageDevices" SelectCountMethod="GetDevicesCount"
        SelectMethod="GetDevices" DeleteMethod="DeleteDevice">
        <DeleteParameters>
            <asp:Parameter Name="Id" Type="Object" />
        </DeleteParameters>
    </asp:ObjectDataSource>
    <asp:ListView ID="lvDevices" runat="server" ItemPlaceholderID="itemContainer" DataSourceID="odsDevices"
        OnItemDeleted="lvDevices_ItemDeleted" DataKeyNames="Id">
        <LayoutTemplate>
            <table class="grid device-grid" cellpadding="0" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 25%;">
                            <asp:Localize ID="locDeviceName" runat="server" Text="<%$ Resources:GUIStrings,DeviceName %>" />
                        </th>
                        <th>
                            <asp:Localize ID="locDeviceResolution" runat="server" Text="<%$ Resources:GUIStrings,MinimumWidth %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings,MaximumWidth %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings,PreviewWidth %>" />
                        </th>
                        <th>
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings,PreviewHeight %>" />
                        </th>
                        <th style="width: 10%;">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="itemContainer" runat="server" />
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10" align="right">
                            <asp:DataPager ID="dpDevices" runat="server" PageSize="10">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Image" ShowFirstPageButton="false" ShowLastPageButton="false"
                                        NextPageText="" PreviousPageText="" RenderDisabledButtonsAsLabels="true" NextPageImageUrl="~/App_Themes/General/images/next.gif"
                                        RenderNonBreakingSpacesBetweenControls="false" PreviousPageImageUrl="~/App_Themes/General/images/prev.gif" />
                                    <asp:TemplatePagerField>
                                        <PagerTemplate>
                                            <div class="pager">
                                                <%# string.Format(JSMessages.Page0Of1Items, (Container.StartRowIndex / Container.PageSize) + 1, 
                                                    Math.Ceiling((double)Container.TotalRowCount / Container.PageSize), Container.TotalRowCount)%>
                                            </div>
                                        </PagerTemplate>
                                    </asp:TemplatePagerField>
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class='<%# Container.DataItemIndex % 2 == 0 ? "odd-row" : "even-row" %>'>
                <td>
                    <asp:Literal ID="ltname" runat="server" Text='<%# Eval("Title") %>' />
                </td>
                <td class="min-width">
                    <asp:Literal ID="ltMinWidth" runat="server" Text='<%# Eval("MinWidth") %>' />
                </td>
                <td class="max-width">
                    <asp:Literal ID="ltMaxWidth" runat="server" Text='<%# Eval("MaxWidth") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltPreviewWidth" runat="server" Text='<%# Eval("PreviewWidth") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltPreviewHeight" runat="server" Text='<%# Eval("PreviewHeight") %>' />
                </td>
                <td>
                    <a href="#" onclick="return SelectDevice('<%# Eval("Id") %>','<%# Eval("Title") %>',
                    '<%# Eval("MinWidth") %>','<%# Eval("MaxWidth") %>','<%# Eval("PreviewWidth") %>','<%# Eval("PreviewHeight") %>','<%# Eval("IsDefault") %>', this);">
                        <%= GUIStrings.Edit %></a>
                    <asp:PlaceHolder ID="phSeperator" runat="server" Visible='<%# Eval("IsSystem").Equals(false) %>'>&nbsp;|&nbsp; </asp:PlaceHolder>
                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" Text="<%$ Resources:GUIStrings, Delete %>"
                        Visible='<%# Eval("IsSystem").Equals(false) %>' />
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="grid-emptyText">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NoItemsFound %>" /></div>
        </EmptyDataTemplate>
    </asp:ListView>
    <asp:HiddenField ID="hdnId" runat="server" />
    <div class="dark-section-header clear-fix" style="margin-top: 50px;">
        <h2><%= GUIStrings.Properties%></h2>
    </div>
    <div class="device-properties">
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.DeviceName%><span class="req">&nbsp;*</span></label>
            <div class="form-value">
                <asp:TextBox ID="txtDeviceName" runat="server" Width="200" />
                <asp:RequiredFieldValidator ID="rfvtxtDeviceName" runat="server" SetFocusOnError="true"
                    Display="None" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterADeviceName %>"
                    ControlToValidate="txtDeviceName" ValidationGroup="deviceSave" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.MinimumWidth %><span class="req">&nbsp;*</span></label>
            <div class="form-value dimension-value">
                <asp:TextBox ID="txtMinWidth" runat="server" Width="200" />
                <asp:RequiredFieldValidator ID="rfvtxtMinWidth" runat="server" SetFocusOnError="true"
                    Display="None" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAMinimumWidth %>"
                    ValidationGroup="deviceSave" ControlToValidate="txtMinWidth" />
                <asp:CompareValidator ID="cvtxtMinWidth" runat="server" Type="Integer" ControlToValidate="txtMinWidth"
                    Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidIntegerValueForMinimumWidth %>"
                    CultureInvariantValues="true" Display="None" Text="*" ValidationGroup="deviceSave" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.MaximumWidth %><span class="req">&nbsp;*</span></label>
            <div class="form-value dimension-value">
                <asp:TextBox ID="txtMaxWidth" runat="server" Width="200" />
                <asp:RequiredFieldValidator ID="rfvtxtMaxWidth" runat="server" SetFocusOnError="true"
                    Display="None" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAMaximumWidth %>"
                    ValidationGroup="deviceSave" ControlToValidate="txtMaxWidth" />
                <asp:CompareValidator ID="cvtxtMaxWidth" runat="server" Type="Integer" ControlToValidate="txtMaxWidth"
                    Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidIntegerValueForMaximumWidth %>"
                    CultureInvariantValues="true" Display="None" Text="*" ValidationGroup="deviceSave" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.PreviewWidth %><span class="req">&nbsp;*</span></label>
            <div class="form-value dimension-value">
                <asp:TextBox ID="txtPreviewWidth" runat="server" Width="200" />
                <asp:RequiredFieldValidator ID="rqtxtPreviewWidth" runat="server" SetFocusOnError="true"
                    Display="None" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAPreviewWidth %>"
                    ValidationGroup="deviceSave" ControlToValidate="txtPreviewWidth" />
                <asp:CompareValidator ID="cvtxtPreviewWidth" runat="server" Type="Integer" ControlToValidate="txtPreviewWidth"
                    Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidIntegerValueForPreviewWidth %>"
                    CultureInvariantValues="true" Display="None" Text="*" ValidationGroup="deviceSave" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.PreviewHeight %><span class="req">&nbsp;*</span></label>
            <div class="form-value dimension-value">
                <asp:TextBox ID="txtPreviewHeight" runat="server" Width="200" />
                <asp:RequiredFieldValidator ID="rqtxtPreviewHeight" runat="server" SetFocusOnError="true"
                    Display="None" Text="*" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAPreviewHeight%>"
                    ValidationGroup="deviceSave" ControlToValidate="txtPreviewHeight" />
                <asp:CompareValidator ID="cvtxtPreviewHeight" runat="server" Type="Integer" ControlToValidate="txtPreviewHeight"
                    Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAValidIntegerValueForPreviewHeight %>"
                    CultureInvariantValues="true" Display="None" Text="*" ValidationGroup="deviceSave" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.DeviceManagement_SetDefault%></label>
            <div class="form-value text-value">
                <asp:CheckBox ID="chkSetDefault" runat="server" />
            </div>
        </div>
        <div class="button-row">
            <asp:Button ID="btnClear" runat="server" Text="<%$ Resources:GUIStrings, Clear %>"
                CssClass="button" OnClientClick="return ClearDevice()" />
            <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Add %>" CssClass="primarybutton"
                OnClick="btnSave_Click" ValidationGroup="deviceSave" OnClientClick="return OnDeviceSave();" />
            <asp:ValidationSummary ID="vsSaveDevice" runat="server" DisplayMode="BulletList"
                ShowMessageBox="true" ShowSummary="false" ValidationGroup="deviceSave" />
        </div>
    </div>
</asp:Content>
