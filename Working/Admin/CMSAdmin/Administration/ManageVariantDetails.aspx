﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" StylesheetTheme="General"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageVariantDetails" CodeBehind="ManageVariantDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="UserList" Src="~/UserControls/Administration/User/ManageAdminUser.ascx" %>
<%@ Register TagPrefix="UC" TagName="ManageSite" Src="~/UserControls/Administration/SiteDetails.ascx" %>
<%@ Register TagPrefix="UC" TagName="Attributes" Src="~/UserControls/General/AssignAttributes.ascx" %>
<%@ Register TagPrefix="UC" TagName="TranslationServiceSettings" Src="~/UserControls/Administration/Translation/TranslationServiceSettings.ascx" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#rbtnAdminUsers").on("change", "input:radio:checked", ToggleUserAccess);
            ToggleUserAccess();
        });

        function ToggleUserAccess() {
            var selectedOption = $("#rbtnAdminUsers input:radio:checked").val();
            if (selectedOption == "1")
                $("#user_access").hide();
            else
                $("#user_access").show();
        }

        function Custom_Grid_Callback(sender, eventArgs) {
            switch (gridActionsJson.Action) {
                case "AddUser":
                    OpeniAppsAdminPopup("ManageAdminUserDetails", "IsMicroSite=true&MicroSiteId=" + $("#hdnVariantSiteId").val(), "RefreshUserList");
                    break;
                case "ImportUser":
                    OpeniAppsAdminPopup("SelectManualListPopup", "ObjectTypeId=14", "CloseImportUser");
                    break;
            }

            return false;
        }

        function VariantSite_OnMoveNext() {
            if (typeof SaveAttributeValues == "function")
                SaveAttributeValues();
        }

        function CloseImportUser() {
            setTimeout(function () {
                $("#import_roles").iAppsDialog("open");
            }, 500);
        }

        function RefreshUserList() {
            cbUserList.callback();
        }

        function ImportUsers() {
            var cParam = stringformat("{0}|{1}", popupActionsJson.SelectedItems, $("#lstGroups").val());
            cbUserList.set_callbackParameter(cParam);
            cbUserList.callback();

            $("#import_roles").iAppsDialog("close");

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="cntBody" ContentPlaceHolderID="cphContent" runat="server">
    <div class="page-header-section clear-fix">
        <h1>
            <asp:Literal ID="LtrTitle" runat="server" Text="<%$ Resources:GUIStrings, CreateVariantSite %>" /></h1>
    </div>
    <div class="wizard page-content-section clear-fix">
        <asp:Wizard ID="wzVariantSite" runat="server" EnableViewState="true" ActiveStepIndex="0"
            Width="100%" DisplaySideBar="false">
            <LayoutTemplate>
                <div class="clear-fix wizard-header">
                    <div class="header-container">
                        <asp:PlaceHolder ID="headerPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                    <div class="navigation-container">
                        <asp:PlaceHolder ID="navigationPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
                <div class="step-container">
                    <asp:PlaceHolder ID="wizardStepPlaceHolder" runat="server"></asp:PlaceHolder>
                </div>
            </LayoutTemplate>
            <HeaderTemplate>
                <asp:LinkButton ID="lbtnDefineSiteVariant" runat="server" Text="<%$ Resources:GUIStrings, DefineSiteVariant %>"
                    Enabled="false" OnClick="lbtnDefineSiteVariant_Click" CausesValidation="false" />
                <asp:LinkButton ID="lbtnAttributes" runat="server" Text="<%$ Resources:GUIStrings, Attributes %>"
                    Enabled="false" OnClick="lbtnAttributes_Click" CausesValidation="false" />
                <asp:LinkButton ID="lbtnIdentifySiteAdmin" runat="server" Text="<%$ Resources:GUIStrings, IdentifySiteAdministratorsVisitors %>"
                    Enabled="false" OnClick="lbtnIdentifySiteAdmin_Click" CausesValidation="false" />
                <asp:LinkButton ID="lbtnSiteWorkflowOption" runat="server" Text="<%$ Resources:GUIStrings, SiteWorkflowOption %>"
                    Enabled="false" OnClick="lbtnSiteWorkflowOption_Click" CausesValidation="false" />
                <asp:LinkButton ID="lbtnPreview" runat="server" Text="<%$ Resources:GUIStrings, PreviewSummary %>"
                    Enabled="false" OnClick="lbtnPreview_Click" CausesValidation="false" ClientIDMode="Static" />
            </HeaderTemplate>
            <WizardSteps>
                <asp:WizardStep ID="WizardStep1" runat="server" Title="<%$ Resources:GUIStrings, DefineSiteVariant %>">
                    <asp:Panel runat="server" ID="pnlStep1">
                        <UC:ManageSite ID="ctlDefineSiteVariant" runat="server" />
                    </asp:Panel>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep2" runat="server" Title="<%$ Resources:GUIStrings, Attributes %>">
                    <asp:Panel runat="server" ID="pnlStep2" CssClass="step-2 clear-fix" ClientIDMode="Static">
                        <h2>
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Attributes %>" /></h2>
                        <UC:Attributes ID="ucAttributes" runat="server" Category="Site" />
                        <asp:Literal ID="ltNoAttributes" runat="server" />
                    </asp:Panel>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep3" runat="server" Title="<%$ Resources:GUIStrings, IdentifySiteAdministratorsVisitors %>">
                    <asp:Panel runat="server" ID="pnlStep3" CssClass="step-3 clear-fix" ClientIDMode="Static">
                        <h2>
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, SiteAdministrators %>" /></h2>
                        <div class="form-row">
                            <label class="form-label long-label">
                                <%= GUIStrings.ImportAllAdminPermission%></label>
                            <div class="form-value">
                                <asp:RadioButtonList runat="server" ID="rbtnAdminUsers" ClientIDMode="Static" RepeatDirection="Horizontal" CssClass="radiobutton-list">
                                    <asp:ListItem Value="1" Text="<%$Resources:GUIStrings, Yes %>" />
                                    <asp:ListItem Value="2" Text="<%$Resources:GUIStrings, No %>" />
                                </asp:RadioButtonList>
                            </div>
                        </div>
                        <div id="user_access">
                            <h4><%= GUIStrings.SiteUsersToHaveAccess%></h4>
                            <iAppsControls:CallbackPanel ID="cbUserList" runat="server" CssClass="scroll-section">
                                <ContentTemplate>
                                    <UC:UserList ID="userList" runat="server" />
                                </ContentTemplate>
                            </iAppsControls:CallbackPanel>
                        </div>
                        <hr />
                        <div class="site-visitors">
                            <h2>
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SecureSiteVisitors %>" /></h2>
                            <div class="form-row">
                                <label class="form-label long-label">
                                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Allowmastersiteauthenticatedvisitorsaccesstovariantsite%>" /></label>
                                <div class="form-value">
                                    <asp:RadioButtonList runat="server" ID="rbtnSiteVisitors" ClientIDMode="Static" RepeatDirection="Horizontal" CssClass="radiobutton-list">
                                        <asp:ListItem Value="1" Text="<%$Resources:GUIStrings, Yes %>" />
                                        <asp:ListItem Value="2" Text="<%$Resources:GUIStrings, No %>" />
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep4" runat="server" Title="<%$ Resources:GUIStrings, SiteWorkflowOption %>">
                    <asp:Panel runat="server" ID="pnlStep4" CssClass="step-4">
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, SendNotificationWhenMasterIsUpdated %>" />
                            </label>
                            <div class="form-value">
                                <asp:RadioButton runat="server" ID="rdSendNotificationYes" Text="<%$Resources:GUIStrings, Yes %>"
                                    GroupName="Notification" />
                                <asp:RadioButton runat="server" ID="rdSendNotificationNo" Text="<%$Resources:GUIStrings, No %>"
                                    GroupName="Notification" Checked="true" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:GUIStrings, ProvideOptionForSubmitIntoTransaction %>" />
                            </label>
                            <div class="form-value">
                                <asp:RadioButton runat="server" ID="rdSubmitIntoTransactionYes" Text="<%$Resources:GUIStrings, Yes %>"
                                    GroupName="SubmitIntoTransaction" />
                                <asp:RadioButton runat="server" ID="rdSubmitIntoTransactionNo" Text="<%$Resources:GUIStrings, No %>"
                                    GroupName="SubmitIntoTransaction" Checked="true" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:GUIStrings,SendNotificationWhenThereIsAnErrorWithaTranslationSubmission %>" />
                            </label>
                            <div class="form-value">
                                <asp:RadioButton runat="server" ID="rdSendErrorNotificationYes" Text="<%$Resources:GUIStrings, Yes %>"
                                    GroupName="SendErrorNotification" />
                                <asp:RadioButton runat="server" ID="rdSendErrorNotificationNo" Text="<%$Resources:GUIStrings, No %>"
                                    GroupName="SendErrorNotification" Checked="true" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:WizardStep>
                <asp:WizardStep ID="WizardStep5" runat="server" Title="<%$ Resources:GUIStrings, PreviewSummary %>">
                    <asp:Panel runat="server" ID="pnlStep5" ClientIDMode="Static" CssClass="step-5">
                        <asp:Panel ID="pnlInstructions" runat="server" CssClass="instructions">
                            <asp:Literal ID="ltPreviewInstructions" runat="server" Text="<%$ Resources:GUIStrings, Previewandconfirm %>" />
                        </asp:Panel>
                        <h2>
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, DefineNewVariant1 %>" /></h2>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, SiteNamePW %>" /></label>
                            <div class="form-value text-value">
                                <asp:Literal ID="ltrSiteName" runat="server" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, SiteDescrPW %>" /></label>
                            <div class="form-value text-value">
                                <asp:Literal ID="ltrSiteDescription" runat="server" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, SiteURLPW %>" /></label>
                            <div class="form-value text-value">
                                <asp:Literal ID="ltrSiteURL" runat="server" />
                            </div>
                        </div>
                        <div class="form-row">
                            <label class="form-label">
                                <%= Bridgeline.iAPPS.Resources.GUIStrings.Groups %><br />
                            </label>
                            <div class="form-value site-groups">
                                <asp:CheckBoxList ID="cblSiteGroups" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" Enabled="false">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <asp:PlaceHolder ID="phIdentifyUsers" runat="server">
                            <h2>
                                <asp:Localize ID="llSAVisitors" runat="server" Text="<%$ Resources:GUIStrings, SiteAdministratorsVisitors2 %>" /></h2>
                            <div class="form-row">
                                <label class="form-label long-label">
                                    <%= GUIStrings.ImportAllAdminPermission%></label>
                                <div class="form-value">
                                    <asp:RadioButtonList runat="server" ID="rbtnAdminUsersPrv" Enabled="false" ClientIDMode="Static" RepeatDirection="Horizontal" CssClass="radiobutton-list">
                                        <asp:ListItem Value="1" Text="<%$Resources:GUIStrings, Yes %>" />
                                        <asp:ListItem Value="2" Text="<%$Resources:GUIStrings, No %>" />
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <UC:UserList ID="userListPreview" runat="server" />
                            <div class="form-row">
                                <label class="form-label long-label">
                                    <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, Allowmastersiteauthenticatedvisitorsaccesstovariantsite%>" />
                                </label>
                                <div class="form-value">
                                    <asp:RadioButtonList runat="server" ID="rbtnSiteVisitorsPrv" Enabled="false" ClientIDMode="Static" RepeatDirection="Horizontal" CssClass="radiobutton-list">
                                        <asp:ListItem Value="1" Text="<%$Resources:GUIStrings, Yes %>" />
                                        <asp:ListItem Value="2" Text="<%$Resources:GUIStrings, No %>" />
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <h2>
                            <asp:Localize ID="llSiteWorkFlowOption" runat="server" Text="<%$ Resources:GUIStrings, SiteWorkflowOption3 %>" /></h2>
                        <div class="form-row">
                            <label class="form-label long-label">
                                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, SendNotificationWhenMasterIsUpdated%>" />
                            </label>
                            <div class="form-value">
                                <asp:RadioButton runat="server" ID="rdNotificationOptionValueYes" Text="<%$Resources:GUIStrings, Yes %>"
                                    GroupName="NotificationOptionValue" Enabled="false" />
                                <asp:RadioButton runat="server" ID="rdNotificationOptionValueNo" Text="<%$Resources:GUIStrings, No %>"
                                    GroupName="NotificationOptionValue" Enabled="false" />
                            </div>
                        </div>

                        <asp:PlaceHolder runat="server" ID="phSiteWorkflowTranslationOptions">
                            <div class="form-row">
                                <label class="form-label">
                                    <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, ProvideOptionForSubmitIntoTransaction%>" />
                                </label>
                                <div class="form-value long-label">
                                    <asp:RadioButton runat="server" ID="rdWorkFlowOptionValueYes" Text="<%$Resources:GUIStrings, Yes %>"
                                        GroupName="WorkFlowOptionValue" Enabled="false" />
                                    <asp:RadioButton runat="server" ID="rdWorkFlowOptionValueNo" Text="<%$Resources:GUIStrings, No %>"
                                        GroupName="WorkFlowOptionValue" Enabled="false" />
                                </div>
                            </div>
                            <div class="form-row">
                                <label class="form-label long-label">
                                    <asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:GUIStrings, SendNotificationWhenThereIsAnErrorWithaTranslationSubmission%>" />
                                </label>
                                <div class="form-value">
                                    <asp:RadioButton runat="server" ID="rdTranslationErrorNotificationYes" Text="<%$Resources:GUIStrings, Yes %>"
                                        GroupName="TranslationErrorNotificationOptionValue" Enabled="false" />
                                    <asp:RadioButton runat="server" ID="rdTranslationErrorNotificationNo" Text="<%$Resources:GUIStrings, No %>"
                                        GroupName="TranslationErrorNotificationOptionValue" Enabled="false" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <h2>
                            <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings,TranslationServiceSettings %>" /></h2>
                        <div class="form-row">
                            <label class="form-label" style="width: 260px">
                                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:GUIStrings,EnableTranslationServices %>" /></label>
                            <div class="form-value">
                                <asp:CheckBox ID="cbPrevEnableTranslation" runat="server" Enabled="false" />
                            </div>
                        </div>

                        <asp:PlaceHolder ID="phImportoptions" runat="server">
                            <hr />
                            <h3><%= GUIStrings.ImportOptions %></h3>
                            <div class="form-row import-options">
                                <label class="form-label"><%= GUIStrings.MenusAndPages %></label>
                                <div class="form-value">
                                    <asp:RadioButton ID="rbNoMenus" runat="server" Text="<%$ Resources:GUIStrings, DontImportMenuOrPages %>" GroupName="ImportOptions" onchange="fn_TogglePageStatus('none');" Enabled="false" /><br />
                                    <asp:RadioButton ID="rbImportMenus" runat="server" Text="<%$ Resources:GUIStrings, ImportMenusOnly %>" GroupName="ImportOptions" onchange="fn_TogglePageStatus('menus');" Enabled="false" /><br />
                                    <asp:RadioButton ID="rbImportPages" runat="server" Text="<%$ Resources:GUIStrings, ImportPages %>" GroupName="ImportOptions" onchange="fn_TogglePageStatus('pages');" Enabled="false" />
                                    <asp:DropDownList ID="ddlImportPageStatus" runat="server" Enabled="false">
                                        <asp:ListItem Text="<%$ Resources:GUIStrings, ImportPagesAsDraft %>" Value="1" />
                                        <asp:ListItem Text="<%$ Resources:GUIStrings, ImportPagesWithSameStatusAsMaster %>" Value="2" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-row import-options">
                                <label class="form-label"><%= GUIStrings.ContentItems %></label>
                                <div class="form-value">
                                    <asp:RadioButton ID="rbNoContentDirectories" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectories %>" GroupName="ImportContentItems" Checked="true" Enabled="false" /><br />
                                    <asp:RadioButton ID="rbImportContentDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportContentItems" Enabled="false" />
                                </div>
                            </div>
                            <div class="form-row import-options">
                                <label class="form-label"><%= GUIStrings.Images %></label>
                                <div class="form-value">
                                    <asp:RadioButton ID="rbNoImages" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectoriesOrImages %>" GroupName="ImportImages" Checked="true" Enabled="false" /><br />
                                    <asp:RadioButton ID="rbImportImageDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportImages" Enabled="false" /><br />
                                </div>
                            </div>
                            <div class="form-row import-options">
                                <label class="form-label"><%= GUIStrings.FilesMedia %></label>
                                <div class="form-value">
                                    <asp:RadioButton ID="rbNoFilesAndMedia" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectoriesOrFilesMedia %>" GroupName="ImportFiles" Checked="true" Enabled="false" /><br />
                                    <asp:RadioButton ID="rbImportFileDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportFiles" Enabled="false" /><br />
                                </div>
                            </div>
                            <div class="form-row import-options">
                                <label class="form-label"><%= GUIStrings.Forms %></label>
                                <div class="form-value">
                                    <asp:RadioButton ID="rbNoForms" runat="server" Text="<%$ Resources:GUIStrings, DontImportDirectories %>" GroupName="ImportForms" Checked="true" Enabled="false" /><br />
                                    <asp:RadioButton ID="rbImportFormDirectories" runat="server" Text="<%$ Resources:GUIStrings, ImportDirectoriesOnly %>" GroupName="ImportForms" Enabled="false" />
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </asp:Panel>
                </asp:WizardStep>
            </WizardSteps>
            <StartNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CommandName="Cancel"
                        CausesValidation="false" Visible="true" />
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, SaveAndContinue %>"
                        ValidationGroup="valSiteDetails" ToolTip="<%$ Resources:GUIStrings, SaveAndContinue %>"
                        CssClass="primarybutton" CommandName="MoveNext" />
                </div>
            </StartNavigationTemplate>
            <StepNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnBack" runat="server" Text="<%$ Resources:GUIStrings, Back %>"
                        ToolTip="<%$ Resources:GUIStrings, Back %>" CssClass="button" CommandName="MovePrevious"
                        CausesValidation="false" />
                    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CommandName="Cancel"
                        CausesValidation="false" />
                    <asp:Button ID="btnContinue" runat="server" Text="<%$ Resources:GUIStrings, SaveAndContinue %>"
                        CausesValidation="false" ToolTip="<%$ Resources:GUIStrings, SaveAndContinue %>" CssClass="primarybutton"
                        CommandName="MoveNext" OnClientClick="return VariantSite_OnMoveNext();" />
                </div>
            </StepNavigationTemplate>
            <FinishNavigationTemplate>
                <div class="button-row">
                    <asp:Button ID="btnBack" runat="server" Text="<%$ Resources:GUIStrings, Back %>"
                        ToolTip="<%$ Resources:GUIStrings, Back %>" CssClass="button" CommandName="MovePrevious" />
                    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
                        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CommandName="Cancel"
                        ClientIDMode="Static" />
                    <asp:Button ID="btnFinish" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
                        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" CommandName="MoveComplete"
                        ClientIDMode="Static" />
                </div>
            </FinishNavigationTemplate>
        </asp:Wizard>
        <asp:HiddenField ID="hdnVariantSiteId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="hdnStatus" runat="server" ClientIDMode="Static" />
    </div>
    <div id="import_roles" class="iapps-modal apply-roles" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, Roles %>" /></h2>
        </div>
        <div class="modal-content">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, RoleAppliedInVariantSite %>" /></label>
                <div class="form-value">
                    <asp:ListBox runat="server" ID="lstGroups" ClientIDMode="Static" SelectionMode="multiple" Rows="8" Width="300" />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <asp:Button ID="btnImport" runat="server" Text="<%$ Resources:GUIStrings, Import %>"
                CssClass="primarybutton" OnClientClick="return ImportUsers();" />
        </div>
    </div>
</asp:Content>
