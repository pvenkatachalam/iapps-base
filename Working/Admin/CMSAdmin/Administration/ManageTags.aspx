<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    Theme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageTags" CodeBehind="ManageTags.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<asp:Content ID="ContentHead" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">

        function upManageIndex_OnCallbackComplete(sender, eventArgs) {
            $(".index-terms").gridActions("displayMessage");
        }

        function upManageIndex_OnRenderComplete(sender, eventArgs) {
            $(".index-terms").gridActions("bindControls");
            $(".index-terms").iAppsSplitter();
        }

        function upManageIndex_OnLoad(sender, eventArgs) {
            $(".index-terms").gridActions({ objList: $(".collection-table"),
                type: "list",
                pageSize: 10,
                onCallback: function (sender, eventArgs) {
                    upManageIndex_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upManageIndex_ItemSelect(sender, eventArgs);
                }
            });
        }

        function LoadIndexGrid() {
            $(".index-terms").gridActions("selectNode", treeActionsJson.FolderId);
        }

        function upManageIndex_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems[0];

            switch (gridActionsJson.Action) {
                case "EditProperties":
                    doCallback = false;
                    switch (gridActionsJson.CustomFilter) {
                        case "14":
                            RedirectToAdminPage("ManageWebsiteUserDetails", "UserId=" + selectedItemId);
                            break;
                    }
                    break;
                case "JumpToLibrary":
                    doCallback = false;
                    switch (gridActionsJson.CustomFilter) {
                        case "7":
                            RedirectToAdminPage("ManageContentLibrary", "DirectoryId=" + selectedItemId);
                            break;
                        case "8":
                            RedirectToAdminPage("ManagePageLibrary", "DirectoryId=" + selectedItemId);
                            break;
                        case "9":
                            RedirectToAdminPage("ManageFileLibrary", "DirectoryId=" + selectedItemId);
                            break;
                        case "33":
                            RedirectToAdminPage("ManageImageLibrary", "DirectoryId=" + selectedItemId);
                            break;
                    }
                    break;
                case "ViewPage":
                    doCallback = false;
                    JumpToFrontPage(eventArgs.selectedItem.getValue("CompleteFriendlyURL"));
                    break;
            }

            if (doCallback) {
                upManageIndex.set_callbackParameter(JSON.stringify(gridActionsJson));
                upManageIndex.callback();
            }
        }

        function upManageIndex_ItemSelect(sender, eventArgs) {
            var selectedItemId = gridActionsJson.SelectedItems[0];

            if (gridActionsJson.CustomFilter == "14") {
                sender.getItemByCommand("EditProperties").showButton();
                sender.getItemByCommand("JumpToLibrary").hideButton();
            }
            else {
                sender.getItemByCommand("EditProperties").hideButton();
                sender.getItemByCommand("JumpToLibrary").showButton();
            }

            if (gridActionsJson.CustomFilter == "8")
                sender.getItemByCommand("ViewPage").showButton();
            else
                sender.getItemByCommand("ViewPage").hideButton();
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <h1><%= Bridgeline.iAPPS.Resources.GUIStrings.Tags %></h1>
    <div class="left-control">
        <UC:AssignTags ID="trIndexTerms" runat="server" AllowMultiSelect="false" FixHeader="true" />
    </div>
    <div class="right-control">
        <iAppsControls:CallbackPanel ID="upManageIndex" runat="server" OnClientCallbackComplete="upManageIndex_OnCallbackComplete"
            OnClientRenderComplete="upManageIndex_OnRenderComplete" OnClientLoad="upManageIndex_OnLoad">
            <ContentTemplate>
                <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
                <asp:ListView ID="lvManageIndex" runat="server" ItemPlaceholderID="phManageIndex">
                    <EmptyDataTemplate>
                        <div class="empty-list">
                            No Items found!
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table fat-grid">
                            <asp:PlaceHolder ID="phManageIndex" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                            <div class="collection-row clear-fix grid-item" objectid='<%# Eval("Id")%>'>
                                <div class="large-cell">
                                    <h4>
                                        <asp:Literal ID="ltTitle" runat="server" /></h4>
                                    <p>
                                        <asp:Literal ID="ltPath" runat="server" /></p>
                                </div>
                                <div class="small-cell">
                                    <div class="child-row">
                                        <asp:Literal ID="ltType" runat="server" />
                                    </div>
                                    <div class="child-row">
                                        <asp:Literal ID="ltStatus" runat="server" />
                                    </div>
                                </div>
                                <input type="hidden" id="hdnUrl" runat="server" datafield="CompleteFriendlyURL" />
                                <input type="hidden" id="hdnFolderId" runat="server" datafield="FolderId" />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
</asp:Content>
