<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    StylesheetTheme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.UrlRedirects" CodeBehind="UrlRedirects.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#divAddRedirect").iAppsDialog({ appendToEnd: true });
            $("#fileUpload").iAppsFileUpload({ textboxWidth: "140px", allowedExtensions: "csv" });
        });

        function grdRedirects_OnLoad(sender, eventArgs) {
            $(".url-redirects").gridActions({ objList: grdRedirects,
                listType: "tableGrid",
                onCallback: function (sender, eventArgs) {
                    gridActions_Callback(sender, eventArgs);
                }
            });
        }

        function grdRedirects_OnRenderComplete(sender, eventArgs) {
            $(".url-redirects").gridActions("bindControls");
        }

        function grdRedirects_onCallbackComplete(sender, eventArgs) {
            $(".url-redirects").gridActions("displayMessage");
        }

        function gridActions_Callback(sender, eventArgs) {
            var doCallback = true;
            
            switch (gridActionsJson.Action) {
                case "Add":
                    doCallback = false;
                    $("#txtOldUrl").val("");
                    $("#txtNewUrl").val("");
                    $("#divAddRedirect").iAppsDialog("open");
                    break;
                case "Delete":
                    if (!confirm("<%= JSMessages.Delete301Confirmation %>"))
                        doCallback = false;
                    else {
                        gridActionsJson.CustomAttributes["OldUrl"] = eventArgs.selectedItem.getMember("OldUrl").get_value();
                        gridActionsJson.CustomAttributes["NewUrl"] = eventArgs.selectedItem.getMember("NewUrl").get_value();
                    }
                    break;
                case "Import":
                    doCallback = false;
                    $("#divImport").iAppsDialog("open");
                    break;
            }

            if (doCallback) {
                gridActionsJson.SelectedItems = [];
                grdRedirects.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdRedirects.callback();
            }
        }

        function CancelRedirectSave() {
            $("#divAddRedirect").iAppsDialog("close");
        }

        function CancelRedirectImport() {
            $("#divImport").iAppsDialog("close");
        }

        function SaveRedirect() {
            var regularEval = new RegExp("[\<\>]", "i");
            if ($("#txtOldUrl").val() == "") {
                alert("<%= JSMessages.PleaseEnterAOldURL %>");
                $("#txtOldUrl").focus();
            }
            else if (regularEval.test($("#txtOldUrl").val())) {
                alert("<%= JSMessages.SpecialCharactersAreNotAllowedPleaseRemoveThem %>");
                $("#txtOldUrl").focus();
            }
            else if ($("#txtNewUrl").val() == "") {
                alert("<%= JSMessages.PleaseEnterANewURL %>");
                $("#txtNewUrl").focus();
            }
            else if (regularEval.test($("#txtNewUrl").val())) {
                alert("<%= JSMessages.SpecialCharactersAreNotAllowedPleaseRemoveThem %>");
                $("#txtNewUrl").focus();
            }
            else {
                $("#divAddRedirect").iAppsDialog("close");

                gridActionsJson.CustomAttributes["OldUrl"] = $("#txtOldUrl").val();
                gridActionsJson.CustomAttributes["NewUrl"] = $("#txtNewUrl").val();

                $(".url-redirects").gridActions("callback", "Save");
            }

            return false;
        }

        function FormatTypeColumn(type) {
            if (type.toLowerCase() == "true")
                return "<%= GUIStrings.Automatic %>";
            else
                return "<%= GUIStrings.Manual %>";
        }

    </script>
</asp:Content>
<asp:Content ID="Configuration" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="grid-section">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid SkinID="Default" ID="grdRedirects" Width="100%" runat="server"
            ManualPaging="true" ShowFooter="false" RunningMode="Callback" LoadingPanelClientTemplateId="grdRedirectsLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="RowNo" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" AllowSorting="false" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings, OldUrl %>"
                            DataField="OldUrl" runat="server" AllowReordering="false" FixedWidth="true" EditControlType="Custom"
                            DataCellClientTemplateId="TitleRowToolTip" DataCellCssClass="first-cell" HeadingCellCssClass="first-cell" />
                        <ComponentArt:GridColumn Width="400" HeadingText="<%$ Resources:GUIStrings, NewUrl %>"
                            DataField="NewUrl" runat="server" EditControlType="Custom" DataCellClientTemplateId="DescriptionRowToolTip" />
                        <ComponentArt:GridColumn Width="70" HeadingText="<%$ Resources:GUIStrings, Type %>"
                            DataField="IsGenerated" runat="server" DataCellClientTemplateId="TypeRowToolTip"
                            DataCellCssClass="last-cell" HeadingCellCssClass="last-cell" />
                        <ComponentArt:GridColumn DataField="RowNo" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <Load EventHandler="grdRedirects_OnLoad" />
                <CallbackComplete EventHandler="grdRedirects_onCallbackComplete" />
                <RenderComplete EventHandler="grdRedirects_OnRenderComplete" />
            </ClientEvents>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="TitleRowToolTip">
                    <span title="## ChangeSpecialCharacters(DataItem.getMember('OldUrl').get_text()) ##">
                        ## DataItem.getMember('OldUrl').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="DescriptionRowToolTip">
                    <span title="## ChangeSpecialCharacters(DataItem.getMember('NewUrl').get_text()) ##">
                        ## DataItem.getMember('NewUrl').get_text() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="TypeRowToolTip">
                    <span title="## FormatTypeColumn(DataItem.getMember('IsGenerated').get_text()) ##">##
                        FormatTypeColumn(DataItem.getMember('IsGenerated').get_text()) ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdRedirectsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdRedirects) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
    <div class="iapps-modal add-redirect" id="divAddRedirect" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, AddNewRedirect %>" /></h2>
        </div>
        <div class="modal-content">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, OldUrl %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtOldUrl" runat="server" CssClass="textBoxes" ClientIDMode="Static"
                        Width="360" /></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, NewUrl %>" /><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtNewUrl" runat="server" CssClass="textBoxes" ClientIDMode="Static"
                        Width="360" /></div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="button" onclick="return CancelRedirectSave();" class="button" value="<%= GUIStrings.Cancel %>" />
            <input type="submit" onclick="return SaveRedirect();" class="primarybutton" value="<%= GUIStrings.Save %>" />
        </div>
    </div>
    <div class="iapps-modal import-redirect" id="divImport" style="display: none;">
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
            ValidationGroup="valImport" />
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Import301Redirects %>" /></h2>
        </div>
        <div class="modal-content">
            <asp:FileUpload ID="fileUpload" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="fileUpload" ClientIDMode="Static" />
            <asp:RequiredFieldValidator ID="reqfileUpload" ControlToValidate="fileUpload" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, FileNameIsRequired %>" Display="None"
                SetFocusOnError="true" ValidationGroup="valImport" />
            <div class="download-template">
                <asp:HyperLink ID="hplDownload" runat="server" NavigateUrl="~/Sample301Redirects.csv" Target="_blank"
                    Text="<%$ Resources:GUIStrings, DownloadSampleTemplate %>" />
            </div>
        </div>
        <div class="modal-footer">
            <input type="button" onclick="return CancelRedirectImport();" class="button" value="<%= GUIStrings.Cancel %>" />
            <asp:Button ID="btnImport" Text="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton"
                runat="server" title="<%$ Resources:GUIStrings, Save %>" OnClick="btnImport_Click"
                ValidationGroup="valImport" />
        </div>
    </div>
</asp:Content>
