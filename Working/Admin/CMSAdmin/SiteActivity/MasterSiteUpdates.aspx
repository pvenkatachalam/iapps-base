<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.MasterSiteUpdates" StylesheetTheme="General" CodeBehind="MasterSiteUpdates.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdMasterSiteUpdates_OnLoad(sender, eventArgs) {
            $(".mastersite-updates").gridActions({ objList: grdMasterSiteUpdates });
        }

        function grdMasterSiteUpdates_OnRenderComplete(sender, eventArgs) {
            $(".mastersite-updates").gridActions("bindControls");
        }

        function grdMasterSiteUpdates_onCallbackComplete(sender, eventArgs) {
            $(".mastersite-updates").gridActions("displayMessage");
        }

        function BuildNotificationDictionary() {
            var notificationJson = {};
            $.each(gridActionsJson.SelectedItems, function () {
                var selItem = grdMasterSiteUpdates.getItemFromKey(0, this.toString());
                notificationJson[this.toString()] = selItem.getMember("NotificationId").get_value();
            });

            gridActionsJson.CustomAttributes["Notifications"] = JSON.stringify(notificationJson);
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (gridActionsJson.SelectedItems.length > 0)
                selectedItemId = gridActionsJson.SelectedItems[0];
            switch (gridActionsJson.Action) {
                case "Import":
                    if (gridActionsJson.SelectedItems.length > 0) {
                        BuildNotificationDictionary();
                        doCallback = false;
                        if (gridActionsJson.TabIndex == 1) {
                            OpeniAppsAdminPopup("ImportPages", "", "RefreshMasterUpdates");
                        }
                        else {
                            OpeniAppsAdminPopup("ImportContents", "ObjectTypeId=" + gridActionsJson.ObjectTypeId, "RefreshMasterUpdates");
                        }                        
                    }
                    break;
                case "Ignore":
                    if (gridActionsJson.SelectedItems.length > 0) {
                        doCallback = true;
                        if (confirm("<%= GUIStrings.ConfirmUpdateIgnore %>")) {
                            BuildNotificationDictionary();
                        }
                        else {
                            doCallback = false;
                        }
                    }
                    break;
                case "View":
                    doCallback = false;
                    var masterSiteUrl = $("#hdnMasterSiteUrl").val();
                    var url;
                    var selectedItem;
                    if (gridActionsJson.SelectedItems.length == 1) {
                        selectedItem = grdMasterSiteUpdates.getItemFromKey(0, gridActionsJson.SelectedItems[0]);
                    }
                    else
                        alert("Please select only one item to view in editor");

                    if (gridActionsJson.TabIndex == 1) {
                        url = masterSiteUrl + selectedItem.getMember("CompleteFriendlyUrl").get_value();
                        window.open(url);

                    }
                    if (gridActionsJson.TabIndex == 2) {
                        qString = 'HideSave=true';
                        qString += '&Id=' + selectedItem.getMember("Id").get_value();
                        OpeniAppsAdminPopup("ManageContentDetails", qString);
                    }
                    if (gridActionsJson.TabIndex == 3) {
                        url = masterSiteUrl + selectedItem.getMember("CompleteFriendlyUrl").get_value().substring(1);
                        window.open(url);
                    }
                    if (gridActionsJson.TabIndex == 4) {
                        url = masterSiteUrl + selectedItem.getMember("CompleteFriendlyUrl").get_value().substring(1);
                        window.open(url);
                    }
                    break;
                default:
                    break;
            }

            if (doCallback) {
                var dateRangeId = $(".mastersite-updates .grid-actions .date-range").attr("clientId");
                var selectedDate = eval("GetSelectedDate_" + dateRangeId + "()");
                gridActionsJson.StartDate = selectedDate.StartDate;
                gridActionsJson.EndDate = selectedDate.EndDate;

                grdMasterSiteUpdates.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdMasterSiteUpdates.callback();
            }
        }

        function RefreshMasterUpdates() {
            $(".mastersite-updates").gridActions("callback");
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="mastersite-updates">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdMasterSiteUpdates" SkinID="default" runat="server" Width="100%"
            RunningMode="Callback" ShowFooter="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="grdMasterSiteUpdatessDragTemplate"
            LoadingPanelClientTemplateId="grdMasterSiteUpdatessLoadingPanelTemplate" CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="grdMasterSiteUpdates_OnLoad" />
                <CallbackComplete EventHandler="grdMasterSiteUpdates_onCallbackComplete" />
                <RenderComplete EventHandler="grdMasterSiteUpdates_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="Description" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CompleteFriendlyUrl" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedDate" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="TemplateName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="NotificationId" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="first-cell">
                                <input type="checkbox" id="chkSelectedForImport" class="item-selector" runat="server"
                                    visible="true" value='<%# Container.DataItem["Id"]%>' />
                            </div>
                            <div class="middle-cell">
                                <%# string.Format("<h4>{0}</h4><p>{1}</p>",
                                    Container.DataItem["Title"], Container.DataItem["Description"])%>
                                <%# Container.DataItem["CompleteFriendlyUrl"]%>
                            </div>
                            <div class="last-cell">
                                <%# Container.DataItem["TemplateName"] != string.Empty ?
                                    string.Format("<div class='child-row'><strong>{0}</strong> : {1}</div>", GUIStrings.Template, Container.DataItem["TemplateName"]) : string.Empty%>
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Created, String.Format("{0:d}", Container.DataItem["CreatedDate"]), Container.DataItem["CreatedByFullName"])%>
                                </div>
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["ModifiedDate"]), Container.DataItem["ModifiedByFullName"])%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdMasterSiteUpdatessDragTemplate">
                    ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdMasterSiteUpdatessLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdMasterSiteUpdates) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
    <asp:HiddenField ID="hdnMasterSiteUrl" runat="server" ClientIDMode="Static" />
</asp:Content>
