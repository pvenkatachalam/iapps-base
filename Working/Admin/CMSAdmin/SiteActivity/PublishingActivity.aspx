<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.PublishingActivity" StylesheetTheme="General" CodeBehind="PublishingActivity.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdPublishActivity_OnLoad(sender, eventArgs) {
            $(".publish-activity").gridActions({ objList: grdPublishActivity });
        }

        function grdPublishActivity_OnRenderComplete(sender, eventArgs) {
            $(".publish-activity").gridActions("bindControls");
        }

        function grdPublishActivity_onCallbackComplete(sender, eventArgs) {
            $(".publish-activity").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (gridActionsJson.SelectedItems.length > 0)
                selectedItemId = gridActionsJson.SelectedItems[0];
            switch (gridActionsJson.Action) {
                case "ViewPage":
                    doCallback = false;
                    JumpToFrontPage(eventArgs.selectedItem.getMember("CompleteFriendlyURL").get_value());
                    break;
                case "ViewSequence":
                    doCallback = false;
                    OpeniAppsAdminPopup("ViewWorkflowExecution", "objectId=" + selectedItemId + "&workflowId=" + eventArgs.selectedItem.getMember("WorkflowId").get_value());
                    break;
                case "Delete":
                    doCallback = true;
                    var blogPageName = GetBlogTitleByPage(selectedItemId);
                    if (blogPageName != "") {
                        doCallback = false;
                        alert(stringformat("<%= JSMessages.ThisPageIsBlogPageCannotDelete %>", blogPageName));
                    }
                    break;
            }

            if (doCallback) {
                var dateRangeId = $(".publish-activity .grid-actions .date-range").attr("clientId");
                var selectedDate = eval("GetSelectedDate_" + dateRangeId + "()");
                gridActionsJson.StartDate = selectedDate.StartDate;
                gridActionsJson.EndDate = selectedDate.EndDate;

                grdPublishActivity.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdPublishActivity.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            sender.getItemByCommand("Publish").hideButton();
            sender.getItemByCommand("Delete").hideButton();
            sender.getItemByCommand("ViewSequence").hideButton();

            if (gridActionsJson.TabIndex == 1 || gridActionsJson.TabIndex == 2) {
                sender.getItemByCommand("ViewSequence").showButton();
                if (GetPageAccessLevel(gridActionsJson.SelectedItems.first()) == "2")
                    sender.getItemByCommand("Publish").showButton();
            }

            if (gridActionsJson.TabIndex == 4) {
                sender.getItemByCommand("Delete").showButton();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="publish-activity">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdPublishActivity" SkinID="default" runat="server" Width="100%"
            RunningMode="Callback" ShowFooter="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="grdPublishActivitysDragTemplate"
            LoadingPanelClientTemplateId="grdPublishActivitysLoadingPanelTemplate" CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="grdPublishActivity_OnLoad" />
                <CallbackComplete EventHandler="grdPublishActivity_onCallbackComplete" />
                <RenderComplete EventHandler="grdPublishActivity_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="Description" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CompleteFriendlyURL" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedDate" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="ModifiedByFullName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedDate" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="TemplateName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="WorkflowId" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="WorkflowState" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="NextActorName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CurrentRowIndex" Visible="false" IsSearchable="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <table width="100%" border="0">
                                <tr>
                                    <td class='first-cell <%# Container.DataItem["WorkflowState"] == string.Empty ? "" : "background" %>'>
                                        <%# Container.DataItem["WorkflowState"] == string.Empty ? GetGridItemRowNumber(Container.DataItem) : Container.DataItem["WorkflowState"]%>
                                        <%# Container.DataItem["NextActorName"] == string.Empty ? string.Empty : string.Format("<br />{0}", Container.DataItem["NextActorName"])%>
                                    </td>
                                    <td class="middle-cell">
                                        <%# string.Format("<h4>{0}</h4><p>{1}</p>",
                                    Container.DataItem["Title"], Container.DataItem["Description"])%>
                                    </td>
                                    <td class="last-cell">
                                        <div class="child-row">
                                            <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Template, Container.DataItem["TemplateName"])%>
                                        </div>
                                        <div class="child-row">
                                            <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Created, String.Format("{0:d}", Container.DataItem["CreatedDate"]), Container.DataItem["CreatedByFullName"])%>
                                        </div>
                                        <div class="child-row">
                                            <%# string.Format("<strong>{0}</strong>: {1} by {2}", GUIStrings.Updated, String.Format("{0:d}", Container.DataItem["ModifiedDate"]), Container.DataItem["ModifiedByFullName"])%>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdPublishActivitysDragTemplate">
                    ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdPublishActivitysLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdPublishActivity) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
