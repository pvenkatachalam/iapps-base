﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageMenuTranslationDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageMenuTranslationDetails"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function tvMenuTranslation_onLoad(sender, eventArgs) {
            $(".tree-container").treeActions({ objTree: tvMenuTranslation, pageTree: false, maxNodeType: 1 });
        }
    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:Button ID="btnReject" runat="server" Text="<%$ Resources:GUIStrings, Reject %>"
            CssClass="primarybutton" OnClick="btnReject_Click" />
        <asp:Button ID="btnApprove" runat="server" Text="<%$ Resources:GUIStrings, ApproveAndPublish %>"
            CommandArgument="Approve" CssClass="primarybutton" OnClick="btnApprove_Click" />
        <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
            CssClass="button" NavigateUrl="~/SiteActivity/TranslationActivity.aspx" />
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, BatchName %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtBatchName" runat="server" CssClass="textBoxes" Width="300" />
        </div>
    </div>
    <asp:Panel ID="pnlRightColumn" runat="server" CssClass="form-section">
        <div class="left-control">
            <div class="tree-container">
                <div class="tree-header">
                    <h2>
                        <asp:Literal ID="ltTreeHeading" runat="server" /></h2>
                    <iAppsControls:TreeActions ID="treeActions" runat="server" TreeName="tvMenuTranslation" />
                </div>
                <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvMenuTranslation"
                    runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
                    DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
                    <ClientEvents>
                        <Load EventHandler="tvMenuTranslation_onLoad" />
                    </ClientEvents>
                </ComponentArt:TreeView>
            </div>
        </div>
        <div class="right-control">
            <div class="grid-actions">
                <div class=" row top-row clear-fix">
                    <div class="columns">
                        <h2>
                              <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, TranslationDetails %>" />
                            </h2>
                    </div>
                </div>
            </div>
            <div style="padding: 10px;">
                <div class="form-row">
                    <label class="form-label">
                          <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, TranslateFrom %>" />:</label>
                    <div class="form-value text-value">
                        <asp:Literal ID="litTranslateFrom" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, TranslateTo %>" />:</label>
                    <div class="form-value text-value">
                        <asp:Literal ID="litTranslateTo" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Instructions %>" />:</label>
                    <div class="form-value text-value">
                        <asp:Literal ID="litInstructions" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, IncludeMenuDescription %>" />:</label>
                    <div class="form-value text-value">
                        <asp:Literal ID="litMenuDescription" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, IncludeMenuFriendlyUrl %>" />:</label>
                    <div class="form-value text-value">
                        <asp:Literal ID="litMenuUrl" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:SiteMapDataSource ID="SiteMapDSMenu" runat="server" SiteMapProvider="FolderWebSiteMapProvider"
        ShowStartingNode="True" />
    <asp:HiddenField ID="hdnAssetInfoId" runat="server" />
</asp:Content>

