﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="TranslateMenu.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.TranslateMenu" StylesheetTheme="General" %>

    <%@ Register TagPrefix="tf" TagName="Translation" Src="~/UserControls/Administration/Translation/TranslationForm.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
<div class="page-header clear-fix">
    <h1><%= GUIStrings.TranslateMenu %></h1>
    <asp:Button ID="btnSubmitNow" runat="server" Text="<%$ Resources:GUIStrings, SubmitNow %>" CommandArgument="SubmitNow" CssClass="primarybutton" OnClick="btnSubmitNow_Click" />
    
    <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" NavigateUrl="~/SiteActivity/TranslationActivity.aspx" />
</div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
 <asp:ValidationSummary ID="vsSubmitForTranslation" runat="server" ShowMessageBox="true"
        ShowSummary="false" EnableClientScript="true" />
    <tf:Translation id="ctlTranslation" runat="server"   />
  
</asp:Content>
