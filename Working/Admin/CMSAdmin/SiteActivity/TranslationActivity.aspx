<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.TranslationActivity" StylesheetTheme="General" CodeBehind="TranslationActivity.aspx.cs" %>

<asp:Content ID="Header" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        function grdTranslation_OnLoad(sender, eventArgs) {
            $(".translation-activity").gridActions({ objList: grdTranslation });
        }

        function grdTranslation_OnRenderComplete(sender, eventArgs) {
            $(".translation-activity").gridActions("bindControls");
        }

        function grdTranslation_onCallbackComplete(sender, eventArgs) {
            $(".translation-activity").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (gridActionsJson.SelectedItems.length > 0)
                selectedItemId = gridActionsJson.SelectedItems.first();
            switch (gridActionsJson.Action) {
                case "AddNew":
                    doCallback = false;
                    window.location = jCMSAdminSiteUrl + "/SiteActivity/ManageTranslationDetails.aspx";
                    break;
                case "ViewEdit":
                    doCallback = false;
                    var selectedItem = grdTranslation.getItemFromKey(0, selectedItemId);
                    if(selectedItem.getMember("IsMenuBatch").get_value())
                        window.location = jCMSAdminSiteUrl + "/SiteActivity/ManageMenuTranslationDetails.aspx?BatchId=" + selectedItemId;
                    else
                        window.location = jCMSAdminSiteUrl + "/SiteActivity/ManageTranslationDetails.aspx?BatchId=" + selectedItemId;
                    break;
                case "TranslateMenu":
                    doCallback = false;
                    //call a callback method to check there is any Menu Batch, if yes its in deleted status
                    var isMenuCanBeTranslated = IsMenuCanBeTranslated();

                    if(isMenuCanBeTranslated == "true")
                        window.location = jCMSAdminSiteUrl + "/SiteActivity/TranslateMenu.aspx";
                    else
                        alert("<%= JSMessages.MenuIsInTranslation %>");
                    break;
            }

            if (doCallback) {
                var dateRangeId = $(".translation-activity .grid-actions .date-range").attr("clientId");
                var selectedDate = eval("GetSelectedDate_" + dateRangeId + "()");
                gridActionsJson.StartDate = selectedDate.StartDate;
                gridActionsJson.EndDate = selectedDate.EndDate;

                grdTranslation.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdTranslation.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            var status = eventArgs.selectedItem.getMember("Status").get_value();

            if (gridActionsJson.TabIndex == 1 && status != 11) {
                sender.getItemByCommand("Submit").showButton();
            }
            else {
                sender.getItemByCommand("Submit").hideButton();
            }

            if ((gridActionsJson.TabIndex == 1 && status != 11) || (gridActionsJson.TabIndex == 5 && status == 19)) {
                sender.getItemByCommand("Delete").showButton();
            }
            else {
                sender.getItemByCommand("Delete").hideButton();
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="translation-activity">
        <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
        <ComponentArt:Grid ID="grdTranslation" SkinID="default" runat="server" Width="100%"
            RunningMode="Callback" ShowFooter="false" ItemDraggingEnabled="false" ItemDraggingClientTemplateId="grdTranslationsDragTemplate"
            LoadingPanelClientTemplateId="grdTranslationsLoadingPanelTemplate" CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="grdTranslation_OnLoad" />
                <CallbackComplete EventHandler="grdTranslation_onCallbackComplete" />
                <RenderComplete EventHandler="grdTranslation_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" IsSearchable="true" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="Description" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="SubmittedDate" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="SubmittedByFullName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedDate" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="CreatedByFullName" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="SourceLanguage" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="TargetLanguage" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="TotalAssets" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="Status" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="RowNumber" IsSearchable="false" Visible="false" />
                        <ComponentArt:GridColumn DataField="IsMenuBatch" IsSearchable="false" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="collection-row grid-item clear-fix">
                            <div class="first-cell">
                                <%# Container.DataItem["RowNumber"]%>
                            </div>
                            <div class="middle-cell">
                                <%# string.Format("<h4>{0}</h4><p>{1} : {2}</p>", Container.DataItem["Title"], GUIStrings.NumberOfItems, Container.DataItem["TotalAssets"])%>
                            </div>
                            <div class="last-cell">
                                <div class="child-row">
                                    <%# GetTranslationDate(Container.DataItem)%>
                                </div>
                                <div class="child-row">
                                    <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, GetTranslationStatus(Container.DataItem))%>
                                </div>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdTranslationsDragTemplate">
                    ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdTranslationsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdTranslation) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
    
</asp:Content>
