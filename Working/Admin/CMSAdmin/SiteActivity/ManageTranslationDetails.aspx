﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeBehind="ManageTranslationDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ManageTranslationDetails"
    StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#chkIncludeMenu").on("change", function () {
                $("#btnMenuDetails").toggle();
            });

            $("#btnMenuDetails").on("click", function () {
                OpeniAppsAdminPopup("TranslationAssetDetails", "ObjectTypeId=2", "CloseMenuEditProperties");

                return false;
            });
        });

        function grdTranslation_OnLoad(sender, eventArgs) {
            $(".translation-details").gridActions({ objList: grdTranslation, fixHeader: false });
        }

        function grdTranslation_OnRenderComplete(sender, eventArgs) {
            $(".translation-details").gridActions("bindControls");
        }

        function grdTranslation_onCallbackComplete(sender, eventArgs) {
            $(".translation-details").gridActions("displayMessage", { complete: function () {
                if (typeof gridActionsJson.CustomAttributes["ReturnUrl"] != "undefined")
                    window.location = gridActionsJson.CustomAttributes["ReturnUrl"];
            }
            });
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = "";
            if (gridActionsJson.SelectedItems.length > 0)
                selectedItemId = gridActionsJson.SelectedItems.first();
            switch (gridActionsJson.Action) {
                case "AddToList":
                    doCallback = false;
                    OpeniAppsAdminPopup("SelectManualListPopup", "ObjectTypeId=8&HideWFPages=true&ForceEnableSelection=true", "CloseAddListItems");
                    break;
                case "Remove":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToDelete %>");
                    break;
                case "Ignore":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToIgnoreTranslation %>");
                    break;
                case "EditProperties":
                    doCallback = false;
                    var qs = "ObjectTypeId=" + eventArgs.selectedItem.getMember("ObjectType").get_value();
                    if (gridActionsJson.SelectedItems.length == 1)
                        qs += "&ObjectId=" + gridActionsJson.SelectedItems.first();
                    OpeniAppsAdminPopup("TranslationAssetDetails", qs, "CloseEditProperties");
                    break;
                case "Cancel":
                    doCallback = window.confirm("<%= JSMessages.AreYouSureYouWantToCancelTranslation %>");
                    break;
                case "ViewPage":
                    doCallback = false;
                    PageMethods.WebMethod_GetPageUrl(selectedItemId, function (result) {
                        JumpToFrontPage(result);
                    });
                    break;
            }

            if (doCallback) {
                grdTranslation.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdTranslation.callback();
            }
        }

        function fn_ShowTranslatedProperties(pageId) {
            doCallback = false;
            var qs = "PageId=" + pageId;
            OpeniAppsAdminPopup("TranslatedProperties", qs);
        }

        function Grid_ItemSelect(sender, eventArgs) {
            var status = eventArgs.selectedItem.getMember("Status").get_value();

            sender.getItemByCommand("Publish").hideButton();
            sender.getItemByCommand("Cancel").hideButton();
            sender.getItemByCommand("EditProperties").hideButton();
            sender.getItemByCommand("Remove").hideButton();
            sender.getItemByCommand("Ignore").hideButton();

            if (status == 22) { // Translation Received
                sender.getItemByCommand("Publish").showButton();
                sender.getItemByCommand("Ignore").showButton();
            }

            if (status == 13 || status == 14 || status == 15) { // In translation or error
                sender.getItemByCommand("Cancel").showButton();
            }

            if (status == 1 || status == 10 || status == 12) { 
                sender.getItemByCommand("EditProperties").showButton();
                sender.getItemByCommand("Remove").showButton();
            }
            
            if (status == 11 || status == 19)
                sender.getItemByCommand("EditProperties").showButton();


            if (status == 17 || status == 18 || status == 19)
                sender.getItemByCommand("ViewPage").hideButton();
        }

        function CloseEditProperties() {
            gridActionsJson.CustomAttributes["AssetProperties"] = popupActionsJson.CustomAttributes["AssetProperties"];
            $(".translation-details").gridActions("callback", "UpdateListItems");
        }

        function CloseAddListItems() {
            var assetsJson = {};
            if (typeof gridActionsJson.CustomAttributes["AssetsJson"] != "undefined")
                assetsJson = JSON.parse(gridActionsJson.CustomAttributes["AssetsJson"]);

            var listItemsJson = [];
            if (popupActionsJson.CustomAttributes["ListItemsJson"] != "")
                listItemsJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);

            $.each(listItemsJson, function () {
                if (typeof assetsJson[this.Id.toString()] == "undefined")
                    assetsJson[this.Id.toString()] = ({ "ObjectId": this.Id.toString(), "ObjectType": 8 });
            });

            gridActionsJson.CustomAttributes["AssetsJson"] = JSON.stringify(assetsJson);
            $(".translation-details").gridActions("callback", "AddListItems");
        }

        function CloseMenuEditProperties() {
            $("#hdnMenuDetails").val(popupActionsJson.CustomAttributes["AssetProperties"]);
        }

        function ValidateBatchName(e, args) {

            var success = false;
            
            var batchId = $('#<%=hdnBatchId.ClientID %>').val();
            
                $.ajax({
                    type: "POST",
                    url: "ManageTranslationDetails.aspx/WebMethod_CheckBatchNameExists",
                    data: "{'batchName':'" + args.Value + "','batchId':'" + batchId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (msg) {
                        success = msg.d == false;
                    }
                });
                
            args.IsValid = success;
        }

        function OnSaveBatch() {
            
            if (Page_ClientValidate("valBatch")) {
                return true;
            }

            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="buttons" ContentPlaceHolderID="cphHeaderButtons" runat="server">
    <div class="page-header clear-fix">
        <h1>
            <asp:Literal ID="ltPageHeading" runat="server" /></h1>
        <asp:Button ID="btnResubmit" runat="server" Text="<%$ Resources:GUIStrings, Resubmit %>"
            CommandArgument="Resubmmit" CssClass="primarybutton" OnClick="btnResubmit_Click"
            Visible="false" OnClientClick="return OnSaveBatch();" />
        <asp:Button ID="btnSubmitNow" runat="server" Text="<%$ Resources:GUIStrings, SubmitNow %>"
            CommandArgument="SubmitNow" CssClass="primarybutton" OnClick="btnSubmitNow_Click"
            OnClientClick="return OnSaveBatch();" />
        <asp:Button ID="btnSaveForLater" runat="server" Text="<%$ Resources:GUIStrings, SaveForLater %>"
            CommandArgument="SaveForLater" CssClass="primarybutton" OnClick="btnSaveForLater_Click"
            OnClientClick="return OnSaveBatch();" />
        <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, CancelSubmission %>"
            CssClass="primarybutton" OnClick="btnCancel_Click" Visible="false" />
        <asp:HyperLink ID="hplCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
            CssClass="button" NavigateUrl="~/SiteActivity/TranslationActivity.aspx" />
    </div>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
        ValidationGroup="valBatch" />
    <div class="dark-section-header clear-fix">
        <h2><%= GUIStrings.BatchDetails %></h2>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, BatchName %>" />
        </label>
        <div class="form-value">
        <asp:HiddenField ID="hdnBatchId" runat="server"></asp:HiddenField>
            <asp:TextBox ID="txtBatchName" runat="server" CssClass="textBoxes" Width="300" />
            <asp:RequiredFieldValidator ID="reqtxtBatchName" ControlToValidate="txtBatchName"
                runat="server" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterBatchName %>"
                Display="None" SetFocusOnError="true" ValidationGroup="valBatch" />
            <asp:CustomValidator ID="cvtxtBatchName" runat="server" ControlToValidate="txtBatchName"
                Display="None" ErrorMessage="<%$ Resources:JSMessages, BatchNameAlreadyExists %>"
                ClientValidationFunction="ValidateBatchName" SetFocusOnError="true" ValidationGroup="valBatch" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Status %>" />
        </label>
        <div class="form-value">
            <asp:Label ID="lblStatus" runat="server" Text="<%$ Resources:GUIStrings, Created %>" />
        </div>
    </div>
    <asp:Panel ID="pnlDetails" runat="server" CssClass="form-section">
        <div class="dark-section-header clear-fix">
            <h2><%= GUIStrings.TranslationDetails %></h2>
        </div>
        <iAppsControls:GridActions ID="gridActions" runat="server" Position="Top" />
        <div class="grid-section">
            <ComponentArt:Grid ID="grdTranslation" SkinID="Default" runat="server" CssClass="fat-grid"
                ShowFooter="false" ShowHeader="false" Width="100%" ClientIDMode="AutoID" RunningMode="Callback"
                LoadingPanelClientTemplateId="grdTranslationLoadingPanelTemplate">
                <ClientEvents>
                    <Load EventHandler="grdTranslation_OnLoad" />
                    <CallbackComplete EventHandler="grdTranslation_onCallbackComplete" />
                    <RenderComplete EventHandler="grdTranslation_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="ObjectId" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="ObjectId" DataCellServerTemplateId="listTemplate"
                                TextWrap="true" />
                            <ComponentArt:GridColumn DataField="Title" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="ObjectType" Visible="false" />
                            <ComponentArt:GridColumn DataField="Source" IsSearchable="false" Visible="false" />
                            <ComponentArt:GridColumn DataField="Target" IsSearchable="false" Visible="false" />
                            <ComponentArt:GridColumn DataField="Status" IsSearchable="false" Visible="false" />
                            <ComponentArt:GridColumn DataField="Instructions" IsSearchable="false" Visible="false" />
                            <ComponentArt:GridColumn DataField="CurrentRowIndex" IsSearchable="false" Visible="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="listTemplate">
                        <Template>
                            <div class="collection-row grid-item clear-fix">
                                <div class="first-cell">
                                    <%# GetSelectorColumnText(Container.DataItem)%>
                                </div>
                                <div class="middle-cell">
                                    <h4>
                                        <%# Container.DataItem["Title"] %></h4>
                                    <%#  Container.DataItem["Instructions"] != string.Empty ? string.Format("<p>{0} : {1}</p>", GUIStrings.Instructions, Container.DataItem["Instructions"]) : string.Empty%>
                                </div>
                                <div class="last-cell">
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong>: {1} | <strong>{2}</strong>: {3}", GUIStrings.SourceLanguage, Container.DataItem["Source"], GUIStrings.TargetLanguage, Container.DataItem["Target"])%>
                                    </div>
                                    <div class="child-row">
                                        <%# string.Format("<strong>{0}</strong>: {1}", GUIStrings.Status, GetTranslationStatus(Container.DataItem))%> <%#  (Container.DataItem["Status"].ToString() == "20" || Container.DataItem["Status"].ToString() == "22") ? string.Format("|  <a href=\"javascript: //\" onclick=\"fn_ShowTranslatedProperties('{0}')\">{1}</a>", Container.DataItem["ObjectId"], GUIStrings.TranslatedProperties) : ""%>
                                    </div>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdTranslationLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdTranslation) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </asp:Panel>
</asp:Content>
