﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorSearch.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ErrorSearch"
    StylesheetTheme="General" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <meta name="robots" content="noindex, nofollow" />
    <script type="text/javascript" src="../script/General.js"></script>
    <script type="text/javascript" src="../common/js/JSMessages.js.aspx"></script>
    <script type="text/javascript">
        function calStartDate_onSelectionChanged(sender, eventArgs) {
            var selectedDate = calStartDate.getSelectedDate();
            document.getElementById("<%= txtStartDate.ClientID %>").value = calEndDate.formatDate(selectedDate, shortCultureDateFormat);
     }
     function calEndDate_onSelectionChanged(sender, eventArgs) {
         var selectedDate = calEndDate.getSelectedDate();
         document.getElementById("<%= txtEndDate.ClientID %>").value = calEndDate.formatDate(selectedDate, shortCultureDateFormat);
     }
     function ShowError(eventid) {
         var url = 'ErrorResult.aspx?EventId=' + eventid;
         errorResultWindow = dhtmlmodal.open('', 'iframe', url, '', 'width=800px,height=550px,center=1,resize=0,scrolling=1');
     }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="page-header clear-fix">
        <h1 style="float: left;"><%= GUIStrings.SearchErrorLogs%></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Submit %>"
            ToolTip="<%$ Resources:GUIStrings, Submit %>" CssClass="primarybutton" OnClick="btnSave_Click" />
    </div>
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <%= GUIStrings.StartDateTime%>
            </td>
            <td>
                <asp:TextBox ID="txtStartDate" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
                <asp:Image ID="imgStartTime" runat="server" ImageUrl="~/App_Themes/General/images/calendar-button.png"
                    AlternateText="<%$ Resources:GUIStrings, SelectStartDate %>" onclick="calStartDate.Show();" />
                <asp:DropDownList ID="ddlStartHours" runat="server" EnableViewState="true"></asp:DropDownList>
                <asp:DropDownList ID="ddlStartMinutes" runat="server" EnableViewState="true"></asp:DropDownList>
                <asp:CompareValidator ID="cvStartDate" runat="server" Type="Date" ControlToValidate="txtStartDate"
                    Operator="DataTypeCheck" ErrorMessage="Please enter a valid start date" Text="*" CultureInvariantValues="true"></asp:CompareValidator>
                <ComponentArt:Calendar ID="calStartDate" runat="server" SkinID="Default">
                    <ClientEvents>
                        <SelectionChanged EventHandler="calStartDate_onSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                <%= GUIStrings.EndDateTime%>
            </td>
            <td>
                <asp:TextBox ID="txtEndDate" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
                <asp:Image ID="imgEndTime" runat="server" ImageUrl="~/App_Themes/General/images/calendar-button.png"
                    AlternateText="<%$ Resources:GUIStrings, SelectEndDate %>" onclick="calEndDate.Show();" />
                <asp:DropDownList ID="ddlEndHours" runat="server" EnableViewState="true"></asp:DropDownList>
                <asp:DropDownList ID="ddlEndMinutes" runat="server" EnableViewState="true"></asp:DropDownList>
                <asp:CompareValidator ID="cvEndDate" runat="server" Type="Date" ControlToValidate="txtEndDate"
                    Operator="DataTypeCheck" ErrorMessage="Please enter a valid end date" Text="*" CultureInvariantValues="true"></asp:CompareValidator>
                <ComponentArt:Calendar ID="calEndDate" runat="server" SkinID="Default">
                    <ClientEvents>
                        <SelectionChanged EventHandler="calEndDate_onSelectionChanged" />
                    </ClientEvents>
                </ComponentArt:Calendar>
            </td>
        </tr>
        <tr>
            <td>
                <%= GUIStrings.Keywords%>
            </td>
            <td>
                <asp:TextBox ID="txtKeywords" runat="server" CssClass="textBoxes" Width="406"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <%= GUIStrings.ErrorType%>
            </td>
            <td>
                <asp:DropDownList ID="ddlErrorType" runat="server">
                    <asp:ListItem Text="404" Value="404"></asp:ListItem>
                    <asp:ListItem Text="500" Value="500"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    <asp:PlaceHolder ID="phErrorResults" runat="server">
        <asp:GridView ID="gvErrorResults" runat="server" AutoGenerateColumns="false" AllowSorting="true" DataKeyNames="EventTime"
            AllowPaging="true" EmptyDataText="No items found" AlternatingRowStyle-CssClass="evenRow" RowStyle-CssClass="oddRow"
            CssClass="error-results" PagerStyle-CssClass="pager" PagerSettings-Mode="NextPreviousFirstLast" PagerStyle-HorizontalAlign="Right"
            PagerSettings-FirstPageImageUrl="~/App_Themes/General/images/start.gif" PagerSettings-FirstPageText="Goto last page"
            PagerSettings-LastPageImageUrl="~/App_Themes/General/images/end.gif" PagerSettings-LastPageText="Goto last page"
            PagerSettings-NextPageImageUrl="~/App_Themes/General/images/next.gif" PagerSettings-NextPageText="Goto next page"
            PagerSettings-PreviousPageImageUrl="~/App_Themes/General/images/prev.gif" PagerSettings-PreviousPageText="Goto previous page">
            <Columns>
                <asp:BoundField HeaderText="Event Time" DataField="EventTime" SortExpression="EventTime"></asp:BoundField>
                <asp:TemplateField HeaderText="Exception Type">
                    <ItemTemplate>
                        <a href="javascript:ShowError('<%#DataBinder.Eval(Container.DataItem, "EventId") %>')">
                            <%#DataBinder.Eval(Container.DataItem, "ExceptionType") %></a>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <a href="javascript:ShowError('<%#DataBinder.Eval(Container.DataItem, "EventId") %>')">
                            <%#DataBinder.Eval(Container.DataItem, "ExceptionType") %></a>
                    </AlternatingItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:PlaceHolder>
</asp:Content>

