﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ErrorLogging"
    CodeBehind="ErrorLogging.aspx.cs" StylesheetTheme="General" MasterPageFile="~/MasterPage.master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHeader" runat="server">
    <meta name="robots" content="noindex, nofollow" />
    <link href="../css/ErrorConfiguration.css" rel="Stylesheet" media="all" type="text/css" />
    <script type="text/javascript" src="../script/General.js"></script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="page-header clear-fix">
        <h1 style="float: left;"><%= GUIStrings.ErrorLoggingConfiguration%></h1>
        <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" ToolTip="<%$ Resources:GUIStrings, Save %>"
            CssClass="primarybutton" OnClick="btnSave_Click" Style="float: right;" />
        <asp:HyperLink ID="hplSearchError" runat="server" Text="Search Error Logs" NavigateUrl="~/ErrorLogging/ErrorSearch.aspx" CssClass="button"></asp:HyperLink>
    </div>
    <table border="0" cellpadding="0" cellspacing="0" class="error-config" width="100%">
        <tr>
            <th width="75"><%= GUIStrings.Enable%></th>
            <th width="100"><%= GUIStrings.ErrorType%></th>
            <th align="left"><%= GUIStrings.FileTypestoInclude%></th>
        </tr>
        <tr>
            <td align="center" width="75">
                <asp:CheckBox ID="chk404" runat="server" /></td>
            <td align="center" width="100"><%= GUIStrings.Error404%></td>
            <td>
                <asp:TextBox ID="txt404" runat="server" CssClass="textBoxes" Width="440"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="center" width="75">
                <asp:CheckBox ID="chk500" runat="server" /></td>
            <td align="center" width="100"><%= GUIStrings.Error500%></td>
            <td>
                <asp:TextBox ID="txt500" runat="server" CssClass="textBoxes" Width="440"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="center" width="75">
                <asp:CheckBox ID="chkOther" runat="server" /></td>
            <td align="center" width="100"><%= GUIStrings.ErrorOthers%></td>
            <td>
                <asp:TextBox ID="txtOther" runat="server" CssClass="textBoxes" Width="440"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="3"><em><%= GUIStrings.KeywordsHelp%></em></td>
        </tr>
        <tr>
            <td><%= GUIStrings.Keywords%></td>
            <td colspan="2">
                <asp:TextBox ID="txtKeywords" runat="server" CssClass="textBoxes" Width="550"></asp:TextBox></td>
        </tr>
    </table>
</asp:Content>
