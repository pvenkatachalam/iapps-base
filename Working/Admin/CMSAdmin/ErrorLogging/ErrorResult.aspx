﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorResult.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.ErrorResult" StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="Head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <div class="error-content" style="height: 423px">
        <asp:Literal ID="ltErrorData" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="Close" title="Close" onclick="parent.errorResultWindow.hide();" class="button" />
</asp:Content>
