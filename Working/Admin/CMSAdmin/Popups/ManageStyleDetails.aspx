<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManageStyleDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageStyleDetails" Theme="General"
    ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#fileCSS").iAppsFileUpload({ textboxWidth: "140px", allowedExtensions: "css" });
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Title %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.FileName %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:FileUpload ID="fileCSS" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" />
            <asp:Label ID="ltFileName" runat="server" CssClass="coaching-text" />
            <asp:RequiredFieldValidator ID="reqfileCSS" ControlToValidate="fileCSS" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, FileNameIsRequired %>" Display="None"
                SetFocusOnError="true" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Description %></label>
        <div class="form-value">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="200"
                TextMode="MultiLine" />
            <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.ConditionalCss %></label>
        <div class="form-value">
            <asp:TextBox ID="txtConditionalCSS" runat="server" CssClass="textBoxes" Width="200"
                TextMode="MultiLine" />
            <asp:RegularExpressionValidator ID="regtxtConditionalCSS" runat="server" ControlToValidate="txtConditionalCSS"
                ErrorMessage="<%$ Resources:JSMessages, InvalidInputCharacters %>" Display="None"
                SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Media %></label>
        <div class="form-value checkbox-list">
            <asp:CheckBoxList ID="chkMediaType" runat="server">
                <asp:ListItem Value="All" Text="<%$ Resources:GUIStrings, All %>"></asp:ListItem>
                <asp:ListItem Value="Screen" Text="<%$ Resources:GUIStrings, Screen %>"></asp:ListItem>
                <asp:ListItem Value="Projection" Text="<%$ Resources:GUIStrings, Projection %>"></asp:ListItem>
                <asp:ListItem Value="Handheld" Text="<%$ Resources:GUIStrings, Handheld %>"></asp:ListItem>
                <asp:ListItem Value="Print" Text="<%$ Resources:GUIStrings, Print %>"></asp:ListItem>
            </asp:CheckBoxList>
        </div>
    </div>
    <asp:PlaceHolder ID="phTheme" runat="server">
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.Theme %></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlThemes" Enabled="false" runat="server" OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged" AutoPostBack="true" ToolTip="<%$ Resources:GUIStrings, ThemeToolTip %>">
                    <asp:ListItem Text="Default" Value="00000000-0000-0000-0000-000000000000" />
                </asp:DropDownList>
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <asp:CheckBox ID="chkIsGlobal" runat="server" Text="<%$ Resources:GUIStrings, StyleIsGlobal %>" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
</asp:Content>
