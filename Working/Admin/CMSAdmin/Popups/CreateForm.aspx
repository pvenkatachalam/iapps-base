﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master" CodeBehind="CreateForm.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.CreateForm" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function fn_ShowFormOptions(formOption) {
            switch (formOption) {
                case 'form':
                    $('#divFormOptions').show();
                    break;
                case 'poll':
                    $('#divFormOptions').hide();
                    break;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.FormPollName %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtFormPollName" runat="server" Width="340" />
            <asp:RequiredFieldValidator ID="reqtxtFormTitle" ControlToValidate="txtFormPollName"
                runat="server" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>"
                Display="None" SetFocusOnError="true" />
        </div>
    </div>
    <div class="form-row strong">
        <asp:RadioButton ID="rbtnForm" runat="server" Text="<%$ Resources:GUIStrings, CreateForm %>" GroupName="FormOptions" Checked="true" onclick="fn_ShowFormOptions('form')" />
    </div>
    <asp:PlaceHolder ID="phMarketierLicense" runat="server">
        <div class="form-row form-options" id="divFormOptions">
            <label class="form-label"><%= GUIStrings.DoYouWantToRecordFormResponsesOrCaptureLeads %></label>
            <div class="form-value">
                <asp:RadioButtonList ID="rbtn" runat="server" RepeatLayout="UnorderedList">
                    <asp:ListItem Value="0" Text="<%$ Resources:GUIStrings, RecordFormResponses %>" Selected="True" />
                    <asp:ListItem Value="2" Text="<%$ Resources:GUIStrings, CaptureLeads %>" />
                </asp:RadioButtonList>
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-row strong">
        <asp:RadioButton ID="rbtnPoll" runat="server" Text="<%$ Resources:GUIStrings, CreatePoll %>" GroupName="FormOptions" onclick="fn_ShowFormOptions('poll')" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" CausesValidation="false" />
    <asp:Button ID="btnCreate" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Create %>" />
</asp:Content>
