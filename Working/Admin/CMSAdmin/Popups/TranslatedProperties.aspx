﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="TranslatedProperties.aspx.cs" 
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.TranslatedProperties" StylesheetTheme="General" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
<asp:PlaceHolder ID="phPageProperties" runat="server" Visible="false">
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.PageName %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litPageName" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Description %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litDescription" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.TitleTag %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litTitleTag" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.H1Tag %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litH1Tag" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.URLFriendlyName %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litURLFriendlyName" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Keywords %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litKeywords" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.DescriptiveMetadata %>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="litDescriptiveMetaData" runat="server" />
        </div>
    </div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="phMenuProperties" runat="server" Visible="false">
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Menu%>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="ltlMenuName" runat="server" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.FriendlyUrl%>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="ltlMenuFriendlyUrl" runat="server" />
        </div>
    </div>
     <div class="form-row">
        <label class="form-label"><%= GUIStrings.Description%>:</label>
        <div class="form-value text-value">
            <asp:Literal ID="ltlMenuDescription" runat="server" />
        </div>
    </div>
    </asp:PlaceHolder>
<asp:PlaceHolder ID="phNoPageProperties" runat="server">
    <p><%= GUIStrings.PagePropertiesWereNotSentForTranslation %></p>
</asp:PlaceHolder>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" class="button cancel-button" />
</asp:Content>
