<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master" EnableEventValidation="false"
    CodeBehind="FormDefinitionPopup.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.FormDefinitionPopup" ValidateRequest="false"
    StylesheetTheme="General" %>

<%@ Register TagName="FormDetails" TagPrefix="fd" Src="~/UserControls/Libraries/Data/FormDetails.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var selectedItem;
        var adminSiteUrl = "../../";
        var pagePopupWindow;
        var pagepopup;
        var viewPopupWindow;
        var adminpath;

        function DeleteTemplateItem(clntId) {
            var flag = false;
            flag = confirm('<asp:Localize runat="server" Text="<%$ Resources:JSMessages, AreYouSureYouWantToDeleteThisItem %>" />');
            if (flag) {
                destination.deleteItem(clntId);
                destination.unSelectAll();
                if (destination.get_table().GetRowCount() > 0)
                    destination.select(destination.get_table().getRow(0), true);
                else
                    propertiesCallback.set_visible(false);
            }

            UpdateFormField();
        }

        function updateProperty(rowNo, key, control) {
            SetProperties(rowNo, key, control);
        }

        function OnPropertyFocus() {
            destination.render();
        }

        function SetProperties(rowNo, propertyKey, propertyControl) {
            var properties, html, propertyText, propertyValue, propertyValues;
            properties = "";
            html = "";
            var p1 = selectedItem.Data[2].replace(/#%cLt#%/g, "<");
            p1 = jQuery.trim(p1);
            p1 = p1.replace(/\n/g, "#%BR%#");
            p1 = p1.replace(/&#xA;/g, "#%BR%#");
            if (window.ActiveXObject) {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(p1);
            }
            else {
                var parser = new DOMParser();
                xmlDoc = parser.parseFromString(p1, "text/xml");
            }

            var tagFormat = eval(selectedItem.Data[3]);
            html = tagFormat;
            if (propertyControl && propertyControl.nodeName == "INPUT") {
                propertyValue = propertyControl.value.replace(new RegExp("\"", "g"), "'").replace(new RegExp("<", "g"), "&lt;");
                //Can't allow colons in the name field because of its implication the later xml transform for exporting. Colons have special significance, even encoded.
                if (propertyKey == 'Name') {
                    HideErrorForElementFields(propertyControl);
                    if (propertyValue.indexOf(':') > -1) {
                        ShowErrorForElementFields(propertyControl, "<%= GUIStrings.FormsColonsAreNotAllowedInTheNameField %>");
                    return;
                }
            }

            if (propertyKey == "Label")
                selectedItem.SetValue(0, propertyValue);
            else
                propertyValue = propertyControl.value.replace(new RegExp("<.*?>", "g"), "");

            if (tagFormat.indexOf("##" + propertyKey + "##") > -1) {
                html = html.replace("##" + propertyKey + "##", propertyValue);
                selectedItem.SetValue(1, html);
            }

            propertyValue = propertyValue.replace(new RegExp("&", "g"), "spamp");
            xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("selectedValue").nodeValue = propertyValue;

        }

        if (propertyControl && propertyControl.nodeName == "SELECT") {
            if (propertyControl.getAttribute("objectType") == "3") {
                propertyText = propertyControl[propertyControl.selectedIndex].text;
                propertyValue = propertyControl[propertyControl.selectedIndex].value;
                selectedItem.SetValue(7, propertyValue);
                if (propertyValue != "" && propertyValue != "None") {
                    propertyValues = propertyValue.split("~");
                    html = CreateListHtml(tagFormat, propertyValues);
                    selectedItem.SetValue(1, html);
                    xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("defaultValue").nodeValue = propertyText;
                    xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("selectedValue").nodeValue = propertyText;
                }
                else {
                    var textAreas = document.getElementById("controlProperties").getElementsByTagName("TEXTAREA");
                    for (var i = 0; i < textAreas.length; i++) {
                        if (textAreas[i].getAttribute("objectLabel") == "Items") {
                            propertyValue = textAreas[i].value.replace(new RegExp("\"", "g"), "'").replace(new RegExp("<", "g"), "&lt;");
                            propertyValue = propertyValue.replace(new RegExp("<.*?>", "g"), "");
                            propertyValue = propertyValue.replace(new RegExp("[ \t\r\n]+$", "g"), "");
                            propertyValues = propertyValue.split("#%BR%#");
                            html = CreateListHtml(tagFormat, propertyValues);
                            propertyValue = propertyValue.replace(new RegExp("&", "g"), "spamp");
                            propertyValue = propertyValue.replace(/\n/g, "");
                            propertyValue = propertyValue.replace(/\r/g, "");
                            selectedItem.SetValue(1, html);
                            xmlDoc.getElementsByTagName("Attribute")[parseInt(textAreas[i].getAttribute("rowNo"))].attributes[2].nodeValue = propertyValue;
                        }
                    }
                    xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("defaultValue").nodeValue = "";
                    xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("selectedValue").nodeValue = "";
                }
            }
            else {
                propertyValue = propertyControl[propertyControl.selectedIndex].value;

                xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("selectedValue").nodeValue = propertyValue;
            }
        }

        if (propertyControl && propertyControl.nodeName == "TEXTAREA") {
            if (propertyControl.getAttribute("objectLabel") != "Items") {
                propertyValue = propertyControl.value.replace(new RegExp("\"", "g"), "'").replace(new RegExp("<", "g"), "&lt;");
                if (tagFormat.indexOf("##" + propertyKey + "##") > -1) {
                    html = html.replace("##" + propertyKey + "##", propertyValue);
                    selectedItem.SetValue(1, html);
                }
                propertyValue = propertyValue.replace(new RegExp("&", "g"), "spamp");
                xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("selectedValue").nodeValue = propertyValue;
            }
            else {
                var isSpecial = false;
                var selects = document.getElementById("controlProperties").getElementsByTagName("SELECT");
                for (var i = 0; i < selects.length; i++) {
                    if (selects[i].getAttribute("objectType") == "3" && selects[i][selects[i].selectedIndex].value != "" && selects[i][selects[i].selectedIndex].value != "None") {
                        isSpecial = true;
                        break;
                    }
                }
                if (!isSpecial) {
                    propertyValue = propertyControl.value.replace(new RegExp("\"", "g"), "'").replace(new RegExp("<", "g"), "&lt;");
                    propertyValue = propertyValue.replace(new RegExp("<.*?>", "g"), "");
                    propertyValue = propertyValue.replace(new RegExp("[ \t\r\n]+$", "g"), "");
                    propertyValue = propertyValue.replace(/\n/g, "#%BR%#");
                    propertyValues = propertyValue.split("#%BR%#");
                    html = CreateListHtml(tagFormat, propertyValues);
                    propertyValue = propertyValue.replace(new RegExp("&", "g"), "spamp");
                    propertyValue = propertyValue.replace(/\n/g, "");
                    propertyValue = propertyValue.replace(/\r/g, "");
                    selectedItem.SetValue(1, html);
                    xmlDoc.getElementsByTagName("Attribute")[rowNo].attributes.getNamedItem("defaultValue").nodeValue = propertyValue;
                }
            }
        }
        if (window.ActiveXObject)
            properties = xmlDoc.xml;
        else
            properties = new XMLSerializer().serializeToString(xmlDoc);
        selectedItem.SetValue(2, properties.replace(/</g, "#%cLt#%"));

        UpdateFormField();
    }

    function CreateListHtml(tagFormat, propertyValues) {
        html = tagFormat;
        if (tagFormat.indexOf("{") == -1 && tagFormat.indexOf("##") == -1)
            html = tagFormat;
        else {
            if (tagFormat.indexOf("{") > -1 && tagFormat.indexOf("}") > -1) {
                html = tagFormat.substring(0, tagFormat.indexOf("{"));
                if (tagFormat.indexOf("##") > -1) {
                    for (var j = 0; j < propertyValues.length; j++) {
                        html += GetStringBetween("{", "}", tagFormat);
                        html = html.replace("##" + GetStringBetween("##", "##", tagFormat) + "##", GetOptionText(propertyValues[j]));
                    }
                }
                else {
                    for (var j = 0; j < propertyValues.length; j++)
                        html += GetStringBetween("{", "}", tagFormat);
                }
                html += tagFormat.substr(tagFormat.indexOf("}") + 1);
            }
        }
        return html;
    }

    function GetOptionText(optionValue) {
        if (optionValue.indexOf('^') > -1) {
            optionValue = optionValue.substr(0, optionValue.indexOf('^'));
        }
        return optionValue;
    }

    function GetStringBetween(startTag, endTag, source) {
        var s = source;
        var i = s.indexOf(startTag);
        if (i >= 0)
            s = s.substring(i + startTag.length);
        else
            return '';
        if (endTag) {
            i = s.indexOf(endTag);
            if (i >= 0)
                s = s.substring(0, i);
            else
                return '';
        }
        return s;
    }

    function source_OnLoad(sender, eventArgs) {
        source.ToggleGroupExpand(null, null, "0");
    }

    function ToggleGroup(groupIndex) {
        var groups = source.get_levels()[0].Table.get_groups();

        for (var i = 0; i < groups.length; i++) {
            if (groups[i].get_expanded()) {
                source.ToggleGroupExpand(null, null, i + "");
            }
        }

        source.ToggleGroupExpand(null, null, groupIndex);
    }

    function RenderHeading(headingText, dataItem) {
        var headingLink = "<h5><a onclick=\"ToggleGroup('" + dataItem.Index + "');\">" + headingText + "</a></h5>";

        return headingLink;
    }

    function UpdateFormField() {
        var fields = [];
        for (var i = 0; i < destination.get_recordCount() ; i++) {
            var dItem = destination.get_table().getRow(i);

            fields.push({ Id: dItem.Data[4], Title: dItem.Data[0], Type: dItem.Data[3].toLowerCase() });
        }

        if (typeof OnUpdateFormField == "function")
            OnUpdateFormField(fields);
    }

    function source_OnExternalDrop(sender, eventArgs) {
        var draggedItem = eventArgs.get_item();
        var ctrlExists = false;
        if (document.getElementById("hdnFormType").value == "1") {
            if (draggedItem.Data[3].toLowerCase() == "checkbox" || draggedItem.Data[3].toLowerCase() == "radiobutton") {
                for (var i = 0; i < destination.get_recordCount(); i++) {
                    var dItem = destination.get_table().getRow(i);
                    if (dItem.Data[3].toLowerCase() == "checkbox" || dItem.Data[3].toLowerCase() == "radiobutton") {
                        alert('<asp:Localize runat="server" Text="<%$ Resources:JSMessages, APollCanHaveOnlyOneQuestionLabelAndOneOptionControl %>" />.');
                    ctrlExists = true;
                    break;
                }
            }
        }
    }
    if (!ctrlExists) {
        var table = destination.get_table();
        var item = table.AddEmptyRow();
        SetControlProperties(draggedItem.Data, item);
        destination.render();
        destination.unSelectAll();
        destination.select(item, true);
    }

    UpdateFormField();
}

function SetControlProperties(data, item) {
    item.SetValue(0, data[0]);
    item.SetValue(1, data[1]);
    item.SetValue(2, data[2]);
    item.SetValue(3, data[3]);
    item.SetValue(4, "<%= Guid.NewGuid() %>");
    item.SetValue(6, data[6]);
}

function destination_OnExternalDrop(sender, eventArgs) {
    destination.unSelectAll();

    var draggedItem = eventArgs.get_item();
    var targetItem = eventArgs.get_target();
    var table = destination.get_table();
    var index = destination.get_recordCount() + 1;

    if (targetItem)
        index = targetItem.get_index();

    table.Data.splice(draggedItem.get_index(), 1);
    destination.Data = table.Data = table.Data.slice(0, index).concat([draggedItem.Data]).concat(table.Data.slice(index));
    destination.render();

    destination.select(destination.get_table().getRow(index), true);
}

function destination_MoveUp(draggedItem) {
    var index = draggedItem.get_index() - 1;
    if (index < 0)
        return;

    var targetItem = destination.get_table().getRow(index);

    destination_sort(draggedItem, targetItem);

    return false;
}

function destination_MoveDown(draggedItem) {
    var index = draggedItem.get_index() + 1;
    if (index >= destination.get_table().GetRowCount())
        return;

    var targetItem = destination.get_table().getRow(index);

    destination_sort(draggedItem, targetItem);

    return false;
}

function destination_sort(draggedItem, targetItem) {
    var table = destination.get_table();
    var index = destination.get_recordCount() + 1;

    if (targetItem)
        index = targetItem.get_index();

    table.Data.splice(draggedItem.get_index(), 1);
    destination.Data = table.Data = table.Data.slice(0, index).concat([draggedItem.Data]).concat(table.Data.slice(index));
    destination.render();

    destination.unSelectAll();
    destination.select(destination.get_table().getRow(index), true);
}

function destination_OnSelect(sender, eventArgs) {
    propertiesCallback.set_visible(true);
    selectedItem = eventArgs.get_item();
    var pValue = eventArgs.get_item().getMember('Properties').get_value().replace(/&#xD;/g, "");
    pValue = pValue.replace(/&#xA;/g, "#%BR%#");
    propertiesCallback.callback(pValue, eventArgs.get_item().getMember('ControlName').get_value());
    //destination.render();
    var containerScrollTop = $('.form-selected-fields-box').scrollTop();
    var selectedRow = $('.selected-row .control-row');
    $('#divFormActions').css({
        top: ($('.selected-row .control-row').position().top + containerScrollTop + ($('.selected-row .control-row').outerHeight(true) / 2) - 17) + 'px',
        left: ($('.selected-row .control-row').position().left + $('.selected-row .control-row .row-label').outerWidth(true) + $('.selected-row .control-row .row-value').outerWidth(true) + 25) + 'px',
        display: 'block',
        position: 'absolute',
        width: '60px'
    });
}

function destination_OnLoad(sender, eventArgs) {
    if (destination.RecordCount > 0)
        destination.select(destination.get_table().getRow(0), true);

    UpdateFormField();
}

function ChangeSpecialCharacters(str) {
    var replacedString = str;
    if (replacedString != null) {
        replacedString = replacedString.replace(new RegExp("&", "g"), "&amp;");
        replacedString = replacedString.replace(new RegExp("<", "g"), "&lt;");
        replacedString = replacedString.replace(new RegExp(">", "g"), "&gt;");
        replacedString = replacedString.replace(new RegExp("\"", "g"), "&quot;");
        replacedString = replacedString.replace(new RegExp("'", "g"), "\\'");
        replacedString = replacedString.replace(new RegExp("\n", "g"), " ");
    }
    return replacedString;
}

function ShowErrorForElementFields(element, error) {
    var submitButton = $("#<%= btnSave.ClientID %>");
    submitButton.attr('disabled', 'disabled');
    submitButton.addClass('disabled');
    $("<p class='formInputError' style='color:red;'>" + error + "</p>").appendTo($(element).parent());
}

function HideErrorForElementFields(element) {
    var submitButton = $("#<%= btnSave.ClientID %>");
    submitButton.removeProp('disabled');
    submitButton.removeClass('disabled');
    $(".formInputError").remove();
}

$(function () {
    $(".tabs").iAppsTabs();
});
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="row clear-fix">
        <div class="tabs form-options">
            <ul>
                <li><span><%= GUIStrings.FormDetails %></span></li>
                <li><span><%= GUIStrings.FormFields %></span></li>
            </ul>
            <div class="tab1 form-details">
            <div class="scroll-box">
                <fd:FormDetails ID="ctlFormDetails" runat="server" />
            </div>
        </div>
            <div class="tabs2 form-fields">
            <div class="form-fields-box">
                    <%--<h3 class="colon">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, FormFields %>" /></h3>--%>
                <ComponentArt:Grid ID="source" runat="server" RunningMode="Callback" ItemDraggingEnabled="true"
                    ExternalDropTargets="destination" ShowHeader="false" ShowFooter="false" AllowEditing="false"
                    PreExpandOnGroup="false" GroupBy="Group ASC" GroupingMode="ConstantRows" GroupByCssClass="GroupByCell"
                    TreeLineImageWidth="0" TreeLineImageHeight="0" IndentCellWidth="0" Width="100%">
                    <ClientEvents>
                        <ItemExternalDrop EventHandler="source_OnExternalDrop" />
                        <Load EventHandler="source_OnLoad" />
                    </ClientEvents>
                    <Levels>
                        <ComponentArt:GridLevel AllowGrouping="True" ShowHeadingCells="false" ShowTableHeading="false"
                            RowCssClass="toolBoxRow" GroupHeadingCssClass="GroupHeading" DataCellCssClass="toolBoxCell"
                            GroupHeadingClientTemplateId="GroupHeadingTemplate">
                            <Columns>
                                <ComponentArt:GridColumn DataField="Label" Visible="false" Width="0" />
                                <ComponentArt:GridColumn DataField="ControlTag" Visible="false" Width="0" />
                                <ComponentArt:GridColumn DataField="Properties" Visible="false" Width="0" />
                                <ComponentArt:GridColumn DataField="ControlName" DataCellClientTemplateId="ControlNameTemplate" />
                                <ComponentArt:GridColumn DataField="Id" Visible="false" Width="0" />
                                <ComponentArt:GridColumn DataField="Class" Visible="false" Width="0" />
                                <ComponentArt:GridColumn DataField="IncludeInResults" Visible="false" Width="0" />
                                <ComponentArt:GridColumn DataField="Group" Visible="false" Width="0" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="ControlNameTemplate">
                            <div class="formTool ## DataItem.getMember('Class').Value ##">
                                <span>## DataItem.getMember('ControlName').Value ##</span>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="GroupHeadingTemplate_old">
                            <span>## RenderHeading(DataItem.get_columnValue(), DataItem) ##</span>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="GroupHeadingTemplate">
                            <div class="spacer">
                            </div>
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
                <asp:Literal runat="server" ID="LtTagFormats" />
            </div>
        </div>
        </div>
        <div class="form-selected-fields">
            <h3>Your Form</h3>
            <div class="form-selected-fields-box">
                <ComponentArt:Grid ID="destination" runat="server" RunningMode="Callback" CssClass="blank-grid"
                    Width="100%" ExternalDropTargets="destination" ShowHeader="false" ShowFooter="false"
                    EmptyGridText="<%$ Resources:GUIStrings, Loading %>" ItemDraggingEnabled="false"
                    ItemDraggingClientTemplateId="dragTemplate" AllowEditing="false" AllowMultipleSelect="false"
                    AllowPaging="false" PageSize="1000" Height="464">
                    <ClientEvents>
                        <ItemExternalDrop EventHandler="destination_OnExternalDrop" />
                        <ItemSelect EventHandler="destination_OnSelect" />
                        <Load EventHandler="destination_OnLoad" />
                    </ClientEvents>
                    <Levels>
                        <ComponentArt:GridLevel ShowTableHeading="false" ShowHeadingCells="false" ShowSelectorCells="false"
                            RowCssClass="row" DataCellCssClass="data-cell" SelectedRowCssClass="selected-row"
                            AllowGrouping="true" AlternatingRowCssClass="alternate-row" HoverRowCssClass="hover-row">
                            <Columns>
                                <ComponentArt:GridColumn DataField="Label" AllowEditing="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="ControlTag" AllowEditing="false" DataCellClientTemplateId="RowTemplate" />
                                <ComponentArt:GridColumn DataField="Properties" Width="0" AllowEditing="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="ControlName" Width="0" AllowEditing="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="Id" Width="0" AllowEditing="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="Class" Width="0" AllowEditing="false" Visible="false" />
                                <ComponentArt:GridColumn DataField="IncludeInResults" Width="0" AllowEditing="false"
                                    Visible="false" />
                                <ComponentArt:GridColumn Width="0" AllowEditing="false" Visible="false" />
                            </Columns>
                        </ComponentArt:GridLevel>
                    </Levels>
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="RowTemplate">
                            <div class="control-row form-row">
                                <div class="row-label">
                                    <label class="form-label colon">## DataItem.getMember('Label').get_text() ##</label>
                                </div>
                                <div class="row-value">
                                    ## DataItem.getMember('ControlTag').get_text() ##
                                </div>
                            </div>
                        </ComponentArt:ClientTemplate>
                        <ComponentArt:ClientTemplate ID="dragTemplate">
                            <div class="dragField clear-fix">
                                <div class="fieldName">
                                    ## DataItem.getMember('Label').get_text() ##
                                </div>
                                <div class="formField">
                                    ## DataItem.getMember('ControlTag').get_text() ##
                                </div>
                            </div>
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                </ComponentArt:Grid>
                <div class="form-actions clear-fix" style="display: none;" id="divFormActions">
                    <a id="A1" href="#" onclick="return destination_MoveUp(selectedItem);">
                        <img src="/admin/App_Themes/General/images/icon-move-up.png" alt="move up" /></a>
                    <a id="A2" href="#" onclick="return destination_MoveDown(selectedItem);">
                        <img src="/admin/App_Themes/General/images/icon-move-down.png" alt="move down" /></a>
                    <a id="A3" href="#" onclick="return DeleteTemplateItem(selectedItem);">
                        <img src="/admin/App_Themes/General/images/icon-delete-cross-dark.png" alt="delete" /></a>
                </div>
            </div>
        </div>
        <div class="form-field-properties">
            <div class="form-properties-box">
                <ComponentArt:CallBack ID="propertiesCallback" runat="server" EnableViewState="true"
                    CacheContent="true">
                    <Content>
                        <h3>
                            <asp:Literal ID="propertyHeading" runat="server" /></h3>
                        <div id="controlProperties" class="control-properties">
                            <asp:Repeater ID="repControlProperties" runat="server">
                                <HeaderTemplate>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="form-row">
                                        <label class="form-label">
                                            <%# Eval("PropertyKey") %></label>
                                        <div class="form-value">
                                            <%# Eval("PropertyValue") %>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </Content>
                </ComponentArt:CallBack>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:HiddenField ID="hdnFormType" runat="server" ClientIDMode="Static" />
    <asp:Button ID="btnCancel" Text="<%$ Resources:GUIStrings, Cancel %>" runat="server" UseSubmitBehavior="false"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" runat="server"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return ValidatePage();" />
</asp:Content>
