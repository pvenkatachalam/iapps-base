<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManagePageMapNodeDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManagePageMapNodeDetails"
    StylesheetTheme="General" ClientIDMode="Static" %>

<%@ Register TagPrefix="UC" TagName="SecurityLevels" Src="~/UserControls/Libraries/SecurityLevelWithListbox.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".right-column").iAppsTabs();

            $("#txtTitle").bind("blur", function () {
                $("#txtUrlFriendlyName").val(GetCleanFriendlyName($("#txtTitle").val()));
            });

            $(".menu-target .form-value img").on("click", function () {
                $(this).parents(".form-row").find("input[type='radio']").prop("checked", true);

                switch ($(this).attr("id")) {
                    case "browsePage":
                        OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
                        break;
                    case "browseFile":
                        OpeniAppsAdminPopup("SelectFileLibraryPopup", "", "SelectFile");
                        break;
                    case "browseImage":
                        OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectImage");
                        break;
                    case "browseMenu":
                        OpeniAppsAdminPopup("SelectPageLibraryPopup", "SelectDirectory=true", "SelectMenu");
                        break;
                }

            });

            $(".menu-target .form-validator").each(function () {
                ValidatorEnable($(this)[0], false);
            });

            $(".menu-target .form-value input[type='text'], .menu-target .form-label input[type='radio']").on("focus", function () {
                $(this).parents(".form-row").find("input[type='radio']").prop("checked", true);
                $(this).parents(".form-row").find(".form-validator").each(function () {
                    ValidatorEnable($(this)[0], true);
                });

                $(this).parents(".form-row").siblings().find("input[type='text']").val("");
                $(this).parents(".form-row").siblings().find(".form-validator").each(function () {
                    ValidatorEnable($(this)[0], false);
                });
            });

            if ($("#chkRolloverImages").length > 0)
                ToggleRollOverImage($("#chkRolloverImages")[0]);
        });

        function SelectPage() {
            $("#txtTargetPage").val(popupActionsJson.CustomAttributes["SelectedPageTitle"]);
            $("#hdnTargetId").val(popupActionsJson.SelectedItems.first());
            $("#txtTargetPage").trigger("focus");
        }

        function SelectFile() {
            $("#txtTargetFile").val(popupActionsJson.CustomAttributes["SelectedTitle"]);
            $("#hdnTargetId").val(popupActionsJson.SelectedItems.first());
            $("#txtTargetFile").trigger("focus");
        }

        function SelectMenu() {
            $("#txtTargetOtherMenu").val(popupActionsJson.CustomAttributes["SelectedFolderName"]);
            $("#hdnTargetId").val(popupActionsJson.FolderId);
            $("#txtTargetOtherMenu").trigger("focus");
        }

        function SelectImage() {
            $("#txtTargetImage").val(popupActionsJson.CustomAttributes["SelectedTitle"]);
            $("#hdnTargetId").val(popupActionsJson.SelectedItems.first());
            $("#txtTargetImage").trigger("focus");
        }

        function SaveMenuProperties() {
            if (Page_ClientValidate() == true) {
                var msg = "";
                var bExists = DoesMenuNameOrUrlExist($("#hdnMenuId").val(), $("#hdnParentMenuId").val(), $("#txtUrlFriendlyName").val(), $("#txtTitle").val());

                if (bExists == "1" || bExists == 1) {
                    msg += "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, MenuItemWithTheSameUrlAlreadyExistsPleaseReEnterADifferentUrlFriendlyName %>' />";
                }
                else if (bExists == "2" || bExists == 2) {
                    msg += "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, MenuItemWithTheSameNameAlreadyExistsInTheHierarchyPleaseReEnterADifferentMenuName %>' />";
                }
                else if (bExists == "3" || bExists == 3) {
                    msg += "<asp:Localize runat='server' Text='<%$ Resources:JSMessages, MenuItemWithTheSameUrlAndNameAlreadyExistsPleaseReEnterADifferentMenuNameUrlFriendlyName %>' />";
                }

        if (msg != "") {
            alert(msg);
            return false;
        }

        var selectedWorkflowIds = "";
        if (selectedWorkflowIds != "") {
            var result = CheckActorsPermission(selectedWorkflowIds, $("#hdnMenuId").val());
            if (result.toLowerCase() == "true") {
                OpeniAppsAdminPopup("AssignWorkflowWarning", "EditMenu=true", "CloseWorkflowWarning");
                return false;
            }
        }

        ShowiAppsLoadingPanel();
        return true;
    }

    return false;
}

function CloseWorkflowWarning() {
}

function ValidateMailTo(sender, args) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    args.IsValid = $("#txtTargetMailto").val() == "" ? true : filter.test($("#txtTargetMailto").val());
}

function ToggleRollOverImage(thisObject) {
    if (thisObject.checked) {
        $('#divOnImage').show();
        $('#divOffImage').show();
    }
    else {
        $('#divOnImage').hide();
        $('#divOffImage').hide();
    }
}
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="columns left-column">
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
        <asp:HiddenField ID="hdnMenuId" runat="server" />
        <asp:HiddenField ID="hdnParentMenuId" runat="server" />
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.Menu %><span class="req">&nbsp;*</span></label>
            <div class="form-value">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="275" />
                <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheMenuItemName %>" Display="None" />
                <iAppsControls:iAppsCustomValidator ID="cvtxtTitle" ControlToValidate="txtTitle"
                    ErrorMessage="<%$ Resources: JSMessages, MenuNameHasInvalidCharctersAndPleaseReEnter %>"
                    runat="server" ValidateType="Title" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.URLFriendlyName %><span class="req">&nbsp;*</span></label>
            <div class="form-value">
                <asp:TextBox ID="txtUrlFriendlyName" runat="server" CssClass="textBoxes" Width="275" />
                <asp:RequiredFieldValidator ID="reqtxtUrlFriendlyName" runat="server" ControlToValidate="txtUrlFriendlyName"
                    ErrorMessage="<%$ Resources:JSMessages, UrlFriendlyNameCanNotBeNull %>"
                    Display="None" />
                <asp:RegularExpressionValidator ID="regtxtUrlFriendlyName" runat="server" ControlToValidate="txtUrlFriendlyName"
                    ErrorMessage="<%$ Resources:JSMessages, MenuItemUrlFriendlyNameCanHaveOnlyAlphanumericCharactersAndPleaseReEnter %>"
                    ValidationExpression="^[^\/?:&]+$" Display="None" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.Description %></label>
            <div class="form-value">
                <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="275"
                    TextMode="MultiLine" />
                <asp:RegularExpressionValidator ID="reqtxtDescription" runat="server" ControlToValidate="txtDescription"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromThePageDescription %>"
                    Display="None" ValidationExpression="^[^<>]+$" />
            </div>
        </div>
        <asp:PlaceHolder ID="phLocationIdentifier" runat="server" Visible="false">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, MenuIdentifierColon %>" /><span
                        class="req">&nbsp;*</span>
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtLocationIdentifier" runat="server" CssClass="textBoxes" Width="275" />
                    <asp:RequiredFieldValidator ID="reqtxtLocationIdentifier" runat="server" ControlToValidate="txtLocationIdentifier"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheMenuIdentifier %>"
                        Display="None" />
                </div>
            </div>
           

        </asp:PlaceHolder>
         <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, MenuCssClass %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtMenuCssClass" runat="server" CssClass="textBoxes" Width="275" />
                    <iAppsControls:iAppsCustomValidator ID="cvtxtMenuCssClass" ControlToValidate="txtMenuCssClass"
                    ErrorMessage="<%$ Resources: JSMessages, MenuCssClassHasInvalidCharctersAndPleaseReEnter %>"
                    runat="server" ValidateType="Title" />
                </div>
            </div>
        <div class="form-row">
            <asp:CheckBox ID="chkExcludeFromSearch" runat="server" Text="<%$ Resources:GUIStrings, ExcludeFromSearch %>" />
        </div>
        <asp:PlaceHolder ID="phMenuProperties" runat="server">
            <div class="form-row">
                <asp:CheckBox ID="chkSetAsHome" runat="server" Text="<%$ Resources:GUIStrings, SetMenuAsHome %>" />
            </div>
            <div class="form-row">
                <asp:CheckBox ID="chkInVisible" runat="server" Text="<%$ Resources:GUIStrings, MenuItemIsNotVisibleWillNotAppearAsVisibleMenuContentWillNotBeFoundWithSearch %>" />
            </div>
            <div class="form-row">
                <asp:CheckBox ID="chkHiddenFromNavigation" runat="server" Text="<%$ Resources:GUIStrings, MenuItemIsHiddenFromNavigation %>" />
            </div>
            <div class="form-row">
                <asp:CheckBox ID="chkRolloverImages" runat="server" Text="<%$ Resources:GUIStrings, EnableRolloverImages %>" onclick="ToggleRollOverImage(this);" />
            </div>
            <div class="form-row" id="divOffImage">
                <label class="form-label">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, RolloverOnImageColon %>" /></label>
                <div class="form-value">
                    <asp:FileUpload ID="fileUploadOnImage" ToolTip="<%$ Resources:GUIStrings, Browse %>"
                        runat="server" Width="230" CssClass="fileUpload" /><br />
                    <asp:Label ID="lblRolloverOnImage" runat="server" CssClass="coaching-text" />
                </div>
            </div>
            <div class="form-row" id="divOnImage">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, RolloverOffImageColon %>" /></label>
                <div class="form-value">
                    <asp:FileUpload ID="fileUploadOffImage" ToolTip="<%$ Resources:GUIStrings, Browse %>"
                        runat="server" Width="230" CssClass="fileUpload" /><br />
                    <asp:Label ID="lblRolloverOffImage" runat="server" CssClass="coaching-text" />
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
    <asp:PlaceHolder ID="phAdditionalProperties" runat="server">
        <div class="columns right-column">
            <ul>
                <li><span><a href="javascript://">
                    <asp:Localize ID="lcTarget" runat="server" Text="<%$ Resources:GUIStrings, Target %>" /></a></span></li>
                <li><span><a href="javascript://">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Security %>" /></a></span></li>
                <li><span><a href="javascript://">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Workflow %>" /></a></span></li>
            </ul>
            <div class="menu-target">
                <asp:HiddenField ID="hdnTargetId" runat="server" />
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetPage" runat="server" Text="<%$ Resources:GUIStrings, PageColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetPage" runat="server" CssClass="textBoxes" ReadOnly="true"
                            Width="250" />
                        <asp:Image ToolTip="<%$ Resources:GUIStrings, Browse %>" ID="browsePage" runat="server"
                            ImageUrl="~/App_Themes/General/images/icon-browse.gif" />
                        <asp:RequiredFieldValidator ID="reqtxtTargetPage" runat="server" ControlToValidate="txtTargetPage"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseSelectATargetPageForTheMenuItem %>"
                            Display="None" CssClass="form-validator" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetFile" runat="server" Text="<%$ Resources:GUIStrings, FileColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetFile" runat="server" CssClass="textBoxes" ReadOnly="true"
                            Width="250" />
                        <asp:Image ToolTip="<%$ Resources:GUIStrings, Browse %>" ID="browseFile" runat="server"
                            ImageUrl="~/App_Themes/General/images/icon-browse.gif" />
                        <asp:RequiredFieldValidator ID="reqtxtTargetFile" runat="server" ControlToValidate="txtTargetFile"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseSelectATargetFileForTheMenuItem %>"
                            Display="None" CssClass="form-validator" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetImage" runat="server" Text="<%$ Resources:GUIStrings, ImageColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetImage" runat="server" CssClass="textBoxes" Text="" ReadOnly="true"
                            Width="250" />
                        <asp:Image ToolTip="<%$ Resources:GUIStrings, Browse %>" ID="browseImage" runat="server"
                            ImageUrl="~/App_Themes/General/images/icon-browse.gif" />
                        <asp:RequiredFieldValidator ID="reqtxtTargetImage" runat="server" ControlToValidate="txtTargetImage"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseSelectATargetImageForTheMenuItem %>"
                            Display="None" CssClass="form-validator" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetMenuItem" runat="server" Text="<%$ Resources:GUIStrings, OtherMenuItemColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetOtherMenu" runat="server" CssClass="textBoxes" Text=""
                            ReadOnly="true" Width="250" />
                        <asp:Image ToolTip="<%$ Resources:GUIStrings, Browse %>" ID="browseMenu" runat="server"
                            ImageUrl="~/App_Themes/General/images/icon-browse.gif" />
                        <asp:RequiredFieldValidator ID="reqtxtTargetOtherMenu" runat="server" ControlToValidate="txtTargetOtherMenu"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseSelectATargetMenuForTheMenuItem %>"
                            Display="None" CssClass="form-validator" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetExternal" runat="server" Text="<%$ Resources:GUIStrings, ExternalColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetExternal" runat="server" CssClass="textBoxes" Width="250" />
                        <iAppsControls:iAppsCustomValidator ID="reqtxtTargetExternal" ControlToValidate="txtTargetExternal"
                            ErrorMessage="<%$ Resources: JSMessages, PleasEenterAValidInternetAddressForTheMenuItem %>"
                            runat="server" ValidateType="AbsoluteUrl" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetInternal" runat="server" Text="<%$ Resources:GUIStrings, InternalColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetInternal" runat="server" CssClass="textBoxes" Width="250" />
                        <iAppsControls:iAppsCustomValidator ID="reqtxtTargetInternal" ControlToValidate="txtTargetInternal"
                            ErrorMessage="<%$ Resources: JSMessages, PleaseEnterTheDirectoryPathAndFilenameForTheInternalURL %>"
                            runat="server" ValidateType="RelativeUrl" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetMailto" runat="server" Text="<%$ Resources:GUIStrings, MailToColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTargetMailto" runat="server" CssClass="textBoxes" Width="250" />
                        <asp:RequiredFieldValidator ID="reqtxtTargetMailto" runat="server" ControlToValidate="txtTargetMailto"
                            ErrorMessage="<%$ Resources:JSMessages, PleasEenterAValidEmailAddressFormattedAsAccountnameAtDomainComInTheMailtoField %>"
                            Display="None" CssClass="form-validator" />
                        <asp:CustomValidator ID="cvtxtTargetMailto" runat="server" Display="None" ErrorMessage="<%$ Resources:JSMessages, PleasEenterAValidEmailAddressFormattedAsAccountnameAtDomainComInTheMailtoField %>"
                            ClientValidationFunction="ValidateMailTo" CssClass="form-validator" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:RadioButton ID="rdTargetNone" runat="server" Text="<%$ Resources:GUIStrings, NoneColon %>"
                            GroupName="RadioButtonMenuItemTarget" /></label>
                    <div class="form-value">
                        &nbsp;
                    </div>
                </div>
            </div>
            <div style="display: none;">
                <UC:SecurityLevels ID="securityLevels" runat="server" Width="400" />
                <div class="form-row">
                    <asp:CheckBox ID="chkPropagateSecurity" runat="server" Text="<%$ Resources:GUIStrings, PropagateSecurityLevelToAllSubMenuItems %>" />
                </div>
                <div class="form-row">
                    <asp:CheckBox ID="chkPropagatePermissions" runat="server" Text="<%$ Resources:GUIStrings, PropagateUserGroupPermissionsToSubMenuItems %>" />
                </div>
                <div class="form-row">
                    <asp:CheckBox ID="chkInheritPemissions" runat="server" Text="<%$ Resources:GUIStrings, InheritUserGroupPermissionsFromParentMenuItem %>" />
                </div>
            </div>
            <div style="display: none;">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, WorkflowAssignedColon %>" /></label>
                    <div class="form-value checkbox-list">
                        <asp:CheckBoxList ID="chkWorkflow" runat="server" />
                    </div>
                </div>
                <div class="form-row">
                    <asp:CheckBox ID="chkPropagateWorkflow" runat="server" Text="<%$ Resources:GUIStrings, PropagateWorkflowToAllSubMenuItems %>" />
                </div>
                <div class="form-row">
                    <asp:CheckBox ID="chkInheritWorkflow" runat="server" Text="<%$ Resources:GUIStrings, InheritWorkflowFromParentMenuItem %>" />
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CausesValidation="false" CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
        OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClientClick="return SaveMenuProperties();"
        OnClick="btnSave_Click" />
</asp:Content>
