﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" StylesheetTheme="General"
    AutoEventWireup="true" CodeBehind="VariantContentListing.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.VariantContentListing" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function fn_iAPPSPreviewContent(hpl, contenttype) {
            var text;
            if (contenttype == 'master')
                text = $(hpl).siblings("div.masterContentclass").text();
            else
                text = $(hpl).siblings("div.variantContentclass").text();
            if (typeof text == "undefined" || text == null || $.trim(text) == "")
                text = "Preview not available.";
            $("#diviAPPSPreviewContent").text(text);
            $("#diviAPPSPreview").iAppsDialog("open");
        }

        function fn_iAPPSHidePreviewContent() {
            $("#diviAPPSPreview").iAppsDialog("close");
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="grid-section">
        <asp:Repeater ID="rptVariantContent" runat="server">
            <HeaderTemplate>
                <table class="grid" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr class="heading-row">
                        <th>
                            <%= GUIStrings.ContentItemTitle%>
                        </th>
                        <th>
                            <%= GUIStrings.VariantExistsTitle%>
                        </th>
                        <th>
                            <%= GUIStrings.ViewTitle%>
                        </th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%# Container.ItemIndex % 2 == 0 ? "even" : "odd" %>'>
                    <td>
                        <%# Eval("ContentTitleToDisplay") %>
                    </td>
                    <td>
                        <%# Eval("VariantExists") %>
                    </td>
                    <td>
                        <a href="#" onclick="javascript: fn_iAPPSPreviewContent(this, 'master'); return false;">
                            <%= GUIStrings.Master%></a> <a href="#" onclick="javascript: fn_iAPPSPreviewContent(this, 'variant');return false;"
                                style="<%# Eval("VariantExists").ToString().ToLower()=="yes"? "display:visible": "display:none" %>">
                                <%= GUIStrings.Variant%></a>
                        <div class="masterContentclass" style="display: none">
                            <%# Eval("MasterContent") %></div>
                        <div class="variantContentclass" style="display: none">
                            <%# Eval("VariantContent") %></div>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <asp:Label ID="lblNoRecords" Text="<%$ Resources:GUIStrings,ThereisnoMasterContentpresentinthepage %>"
        runat="server" />
    <div class="iapps-modal preview-variant-content" id="diviAPPSPreview" style="display: none;">
        <div class="modal-header clear-fix">
            <h2>
                <%= GUIStrings.Preview %></h2>
        </div>
        <div class="modal-content iapps-clear-fix">
            <div id="diviAPPSPreviewContent">
                &nbsp;</div>
        </div>
        <div class="modal-footer">
            <input type="button" value="<%= GUIStrings.Close %>" onclick="fn_iAPPSHidePreviewContent();"
                class="button" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" class="button canel-button"
        onclick="parent.ClosePageContentVariantsPopup();" />
</asp:Content>
