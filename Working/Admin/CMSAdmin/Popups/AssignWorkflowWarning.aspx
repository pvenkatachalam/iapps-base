<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.AssignWorkflowWarning" StylesheetTheme="general" CodeBehind="AssignWorkflowWarning.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <asp:Literal ID="ltMessage" runat="server" Text="<%$ Resources:GUIStrings, TheFollowingCMSUsersDoNotHaveTheProperPermissions %>" />
        <br />
        <asp:Literal ID="ltConfirm" runat="server" Text="<%$ Resources:GUIStrings, WouldYouLikeToAssignTheMinimumLevelNeededToTheseUsers %>" />
    </p>
    <div class="grid-section">
        <ComponentArt:Grid ID="grdMembers" RunningMode="Client" SkinID="Default" Width="100%"
            runat="server" ShowFooter="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="UserId" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" AllowSorting="false" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, UserGroups %>" Width="150"
                            FixedWidth="true" AllowReordering="false" DataField="UserName" DataCellCssClass="first-cell"
                            HeadingCellCssClass="first-cell" />
                        <ComponentArt:GridColumn DataField="UserId" Visible="false" />
                        <ComponentArt:GridColumn HeadingText="RoleId" DefaultSortDirection="Descending" AllowReordering="false"
                            DataField="RoleId" Visible="false" />
                        <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, Permission %>" DataField="Permission"
                            FixedWidth="true" Visible="true" Width="100" DataCellCssClass="last-cell" HeadingCellCssClass="last-cell" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, CancelRequest %>"
        OnClientClick="return CanceliAppsAdminPopup();" CssClass="button" ToolTip="<%$ Resources:GUIStrings, CancelRequest %>" />
    <asp:Button ID="btnUpdate" runat="server" Text="<%$ Resources:GUIStrings, UpdatePermissions %>"
        OnClientClick="return CanceliAppsAdminPopup(true);" CssClass="primarybutton"
        ToolTip="<%$ Resources:GUIStrings, UpdatePermissions %>" />
</asp:Content>
