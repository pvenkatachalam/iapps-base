<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="DeletePageWarning.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.DeletePageWarning" StylesheetTheme="General" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function DeletePage(type) {
            popupActionsJson.CustomAttributes["DeleteType"] = type;

            SelectiAppsAdminPopup();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="grid-message">
        <p>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, YouAreAboutToDeleteThisPage %>" />&nbsp;
            <strong>
                <asp:Literal ID="ltPageName" runat="Server" /></strong>
        </p>
    </div>
    <asp:Panel ID="pnlGrid" runat="server" CssClass="grid-section">
        <ComponentArt:Grid ID="grdPages" SkinID="Default" ShowFooter="false" Width="100%"
            runat="server" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>" CssClass="fat-grid no-selection">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="PageMapNodeId" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="">
                    <Columns>
                        <ComponentArt:GridColumn DataField="DisplayTitle" DataCellClientTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="FriendlyUrl" Visible="false" />
                        <ComponentArt:GridColumn DataField="PageMapNodeId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="DetailsTemplate">
                    <div class="modal-grid-row grid-item">
                        <h4>
                            ## DataItem.GetMember("DisplayTitle").Value ##</h4>
                            <p>
                            ## DataItem.GetMember("FriendlyUrl").Value ##</p>
                    </div>
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </asp:Panel>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" OnClientClick="return CanceliAppsAdminPopup();" UseSubmitBehavior="false"
        CausesValidation="false" />
    <asp:Button ID="btnDeleteAllMenu" runat="server" Text="<%$ Resources:GUIStrings, AllMenus %>"
        CssClass="primarybutton" OnClientClick="return DeletePage('DeleteFromAllMenu');" />
    <asp:Button ID="btnCurrentmenu" runat="server" Text="<%$ Resources:GUIStrings, CurrentMenu %>"
        CssClass="primarybutton" OnClientClick="return DeletePage('DeleteFromCurrentMenu');" />
</asp:Content>
