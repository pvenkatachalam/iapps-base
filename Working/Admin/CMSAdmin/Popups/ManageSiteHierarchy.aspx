﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ManageSiteHierarchy.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageSiteHierarchy" StylesheetTheme="General" %>

<asp:Content ID="Head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ValidateSelection() {
            var error = "";
            var selectedId = $("#hdnSelectedId").val();

            if (selectedId == null || selectedId == "")
                error += "<%= GUIStrings.PleaseSelectANodeFromTheTree %>";

            if ($("#hdnSiteId").val() == selectedId)
                error += "<%= GUIStrings.YouCannotChooseTheSiteYouAreAttemptingToMove %>";

            if ($("#hdnParentSiteId").val() == selectedId)
                error += "<%= GUIStrings.TheNodeYouSelectedIsAlreadyTheParentOfTheSiteYouAreAttemptingToMove %>";

            if (error != "") {
                alert(error);
                return false;
            }

            ShowiAppsLoadingPanel();
            return true;
        }

        function tvSiteHierarchy_onSelect(sender, eventArgs) {
            $("#hdnSelectedId").val(tvSiteHierarchy.SelectedNode.Value);
        }
    </script>
</asp:Content>
<asp:Content ID="Body" ContentPlaceHolderID="cphContent" runat="server">
    <ComponentArt:TreeView AutoPostBackOnSelect="false" SkinID="Default" ID="tvSiteHierarchy"
        runat="server" DragAndDropEnabled="false" MultipleSelectEnabled="false" OutputCustomAttributes="true"
        DropRootEnabled="false" NodeRowCssClass="NodeRow" SelectedNodeRowCssClass="SelectedNodeRowCssClass">
        <CustomAttributeMappings>
            <ComponentArt:CustomAttributeMapping From="Title" To="Text" />
            <ComponentArt:CustomAttributeMapping From="Id" To="Value" />
        </CustomAttributeMappings>
        <ClientEvents>
            <NodeSelect EventHandler="tvSiteHierarchy_onSelect" />
        </ClientEvents>
    </ComponentArt:TreeView>
    <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnSiteId" runat="server" ClientIDMode="Static" Value="" />
    <asp:HiddenField ID="hdnParentSiteId" runat="server" ClientIDMode="Static" Value="" />
    <asp:XmlDataSource ID="xmlDataSource" runat="server" />
</asp:Content>
<asp:Content ID="Footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button runat="server" ID="btnClose" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnMove" CssClass="primarybutton" runat="server" OnClick="btnMove_Click"
        Text="<%$ Resources:GUIStrings, Move %>" OnClientClick="return ValidateSelection();" />
</asp:Content>
