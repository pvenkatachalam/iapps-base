<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageDisplayOrder.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageDisplayOrder" StylesheetTheme="General" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdDisplayOrder_OnExternalDrop(sender, eventArgs) {
            gridActionsJson.CustomAttributes["FromItemId"] = eventArgs.get_item().getMember('Id').get_value();
            gridActionsJson.CustomAttributes["ToItemId"] = eventArgs.get_target().getMember('Id').get_value();
            gridActionsJson.CustomAttributes["TargetIndex"] = eventArgs.get_target().get_index();

            gridActionsJson.Action = "ChangeDisplayOrder";
            grdDisplayOrder.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdDisplayOrder.callback();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p class="instructions"><em><%= GUIStrings.ReorderMenuHelpText %></em></p>
    <div class="grid-section">
        <ComponentArt:Grid ID="grdDisplayOrder" SkinID="ScrollingGrid" runat="server" ShowFooter="false"
            Width="100%" CssClass="fat-grid" ItemDraggingEnabled="true" ItemDraggingClientTemplateId="dragTemplate"
            ExternalDropTargets="grdDisplayOrder" LoadingPanelClientTemplateId="grdDisplayOrderLoadingPanelTemplate">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="DetailsTemplate" />
                        <ComponentArt:GridColumn DataField="OrderNo" Visible="false" />
                        <ComponentArt:GridColumn DataField="FileName" Visible="false" />
                        <ComponentArt:GridColumn DataField="StatusName" Visible="false" />
                        <ComponentArt:GridColumn DataField="GlobalName" Visible="false" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientEvents>
                <ItemExternalDrop EventHandler="grdDisplayOrder_OnExternalDrop" />
            </ClientEvents>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item clear-fix">
                            <div class="first-cell">
                                <p>
                                    <%# Container.DataItem["OrderNo"]%></p>
                            </div>
                            <div class="middle-cell">
                                <h4>
                                    <%# String.Format("{0} | {1}", Container.DataItem["Title"], Container.DataItem["FileName"])%>
                                </h4>
                            </div>
                            <div class="last-cell">
                                <%# String.Format("{0} : {1} | {2} : {3}", GUIStrings.Status, Container.DataItem["StatusName"], 
                                GUIStrings.Global, Container.DataItem["GlobalName"]) %>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
                <ComponentArt:GridServerTemplate ID="MenuOrderTemplate">
                    <Template>
                        <div class="modal-grid-row grid-item clear-fix">
                            <div class="first-cell">
                                <p>
                                    <%# Container.DataItem["OrderNo"]%></p>
                            </div>
                            <div class="middle-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%>
                                </h4>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="dragTemplate">
                    <div class="drag-template">
                        ## DataItem.getMember('Title').get_text() ##</div>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdDisplayOrderLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdDisplayOrder) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="cancelBtn" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" OnClientClick="return CanceliAppsAdminPopup(true);" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
</asp:Content>
