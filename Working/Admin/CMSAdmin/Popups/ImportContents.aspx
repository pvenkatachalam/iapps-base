﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="ImportContents.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ImportContents" Theme="General" %>

<%@ Register TagPrefix="ucTree" TagName="LetfTree" Src="~/UserControls/Libraries/DirectoryTree.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function LoadMasterContents() {
            $(function () {
                if (typeof parent.gridActionsJson != "undefined") {
                    gridActionsJson = JSON.parse(JSON.stringify(parent.gridActionsJson));
                    ContentImport_Callback("Load");
                }
            });
        }

        function cbMasterContents_OnCallbackComplete() {
            if (gridActionsJson.Action != "Load") {
                if ($(".left-control input[type='checkbox']").length == 0) {
                    CanceliAppsAdminPopup(true);
                }
            }
        }

        function ContentImport_Callback(action) {
            var doCallback = true;

            if (action != "Load") {
                var selectedItems = [];
                $(".left-control input[type='checkbox']:checked").each(function () {
                    selectedItems.push($(this).parent().attr("contentId"));
                });

                if (selectedItems.length == 0) {
                    alert("<%= GUIStrings.PleaseSelectItemsToImport %>");
                    doCallback = false;
                }
                else {
                    if (gridActionsJson.ObjectTypeId == 9 && IsFileVariantExist(JSON.stringify(selectedItems)).toLowerCase() == "true")
                        doCallback = window.confirm("<%= JSMessages.FileReimportAlertMessage  %>");
                    else if (gridActionsJson.ObjectTypeId == 33 & IsImageVariantExist(JSON.stringify(selectedItems)).toLowerCase() == "true")
                        doCallback = window.confirm("<%= JSMessages.ImageReimportAlertMessage  %>");
                    else if(IsContentVariantExist(JSON.stringify(selectedItems)).toLowerCase() == "true")
                        doCallback = window.confirm("<%= JSMessages.ContentReimportAlertMessage  %>");
                }
            }

            if (doCallback) {
                gridActionsJson.Action = action;
                gridActionsJson.FolderId = treeActionsJson.FolderId;

                cbMasterContents.set_callbackParameter(JSON.stringify(gridActionsJson));
                cbMasterContents.callback();
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <asp:Literal ID="ltInstructions" runat="server" />
    </div>
    <div class="left-control">
        <h2>
            <asp:Literal ID="ltSelectedTitle" runat="server"/></h2>
        <iAppsControls:CallbackPanel ID="cbMasterContents" runat="server" CssClass="grid-section"
            OnClientCallbackComplete="cbMasterContents_OnCallbackComplete">
            <ContentTemplate>
                <asp:CheckBoxList ID="chkMasterContents" runat="server" RepeatLayout="UnorderedList" />
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="right-control">
        <ucTree:LetfTree ID="libraryTree" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        OnClientClick="return CanceliAppsAdminPopup(true);" CssClass="button" />
    <asp:Button ID="btnImport" runat="server" Text="<%$ Resources:GUIStrings, Import %>"
        CssClass="primarybutton" OnClientClick="return ContentImport_Callback('Import');" />
</asp:Content>
