<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.EditImageGalleryProperties"
    StylesheetTheme="General" CodeBehind="EditImageGalleryProperties.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var isVideoGallery;
        function SelectProperties() {
            var sortBy, sortOrder, noItemsText, paging, includeChild, noOfThumbnails;

            dwObjectId = document.getElementById('<%=dwSortBy.ClientID%>');
            sortBy = dwObjectId.value;
            if (sortBy != '')
                sortBy = dwObjectId[dwObjectId.selectedIndex].value;

            dwObjectId = GetRadioButtonList('<%=dwSortOrder.ClientID%>'); ;
            if (dwObjectId && dwObjectId != '')
                sortOrder = dwObjectId;

            dwObjectId = document.getElementById('<%=cbShowPaging.ClientID%>');
            if (dwObjectId.checked)
                paging = "true^";
            else
                paging = "false^";

            dwObjectId = document.getElementById('<%=txtRows.ClientID%>');
            if (dwObjectId)
                paging += dwObjectId.value + "^";

            dwObjectId = document.getElementById('<%=txtColumns.ClientID%>');
            if (dwObjectId)
                paging += dwObjectId.value;

            dwObjectId = document.getElementById('<%=cbIncludeChildNodes.ClientID%>');
            if (dwObjectId.checked)
                includeChild = "true";
            else
                includeChild = "false";

            noItemsText = document.getElementById('<%=txtNoItems.ClientID%>').value;
            noOfThumbnails = document.getElementById('<%=txtThumbnails.ClientID%>').value;

            if (isVideoGallery == true) {

                var noSelectedVideoText = document.getElementById('<%=txtNoVideoSelectedText.ClientID%>').value;
                parent.selectVideoGalleryProperties(sortBy, sortOrder, includeChild, noItemsText, paging, noOfThumbnails, noSelectedVideoText);
            }
            else {
                parent.selectImageGalleryProperties(sortBy, sortOrder, includeChild, noItemsText, paging, noOfThumbnails);
            }
            parent.ListGridWin.hide();

        }

        function Close() {
            parent.ListGridWin.hide();
        }

        function SelectListBox(listBoxId, selectedValue) {
            var listBox = document.getElementById(listBoxId);

            for (var i = 0; i < listBox.options.length; ++i) {
                if (listBox.options[i].value == selectedValue)
                    listBox.options[i].selected = true;
            }
        }

        function SelectRadioButtonList(radioButtonId, selectedValue) {
            var radioButtonList = document.getElementById(radioButtonId);
            var options = radioButtonList.getElementsByTagName('INPUT');
            var labels = radioButtonList.getElementsByTagName('LABEL');

            for (var i = 0; i < options.length; i++) {
                options[i].checked = false;
                if (labels[i].innerHTML == selectedValue) {
                    options[i].checked = true;
                }
            }

        }

        function OnSortByChanged(sortBy) {
            var radioButtons = document.getElementById('<%=dwSortOrder.ClientID%>').getElementsByTagName("INPUT");

            for (var i = 0; i < radioButtons.length; i++) {
                if (sortBy == '')
                    radioButtons[i].disabled = true;
                else
                    radioButtons[i].disabled = false;
            }
        }

        function OnShowPagingChanged(dwObjectId) {
            showPaging = dwObjectId.checked;
            if (!showPaging) {
                document.getElementById('<%=txtRows.ClientID%>').disabled = true;
                document.getElementById('<%=txtColumns.ClientID%>').disabled = true;
            }
            else {
                document.getElementById('<%=txtRows.ClientID%>').disabled = false;
                document.getElementById('<%=txtColumns.ClientID%>').disabled = false;
            }
        }

        function GetRadioButtonList(radioButtonId) {
            var radioButtonList = document.getElementById(radioButtonId);
            var options = radioButtonList.getElementsByTagName('INPUT');
            var labels = radioButtonList.getElementsByTagName("LABEL");

            for (var i = 0; i < options.length; i++) {
                if (options[i].checked == true) {
                    return labels[i].innerHTML;
                }
            }

        }
    
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <div class="left-column">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, IncludeSubMenus %>" /></label>
            <asp:CheckBox ID="cbIncludeChildNodes" runat="server" />
        </div>
        <div class="right-column">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <label class="form-label">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, SortBy %>" /></label>
                        <ComponentArt:CallBack ID="callBackSortBy" runat="server">
                            <Content>
                                <asp:DropDownList ID="dwSortBy" onchange="OnSortByChanged(this.value);" runat="server"
                                    Width="230" CssClass="contentListBox">
                                </asp:DropDownList>
                            </Content>
                        </ComponentArt:CallBack>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="form-label">
                            &nbsp;</label>
                        <asp:RadioButtonList ID="dwSortOrder" runat="server" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                            <asp:ListItem Text="ASC" Value="ASC"></asp:ListItem>
                            <asp:ListItem Text="DESC" Value="DESC"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-row">
        <div class="left-column" id="pagerPaging" runat="server">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="cbShowPaging" onclick="OnShowPagingChanged(this)" runat="server"
                            Text="<%$ Resources:GUIStrings, ShowPaging %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="form-label" style="width: 60px;">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Rows %>" /></label>
                        <input id="txtRows" runat="server" class="textBoxes" style="width: 50px;" />
                    </td>
                    <td>
                        <label class="form-label" style="width: 60px;">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Columns %>" /></label>
                        <input id="txtColumns" runat="server" class="textBoxes" style="width: 50px;" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-row">
        <div class="left-column" id="sliderPaging" runat="server">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, NoOfThumnails %>" /></label>
                <input id="txtThumbnails" runat="server" class="textBoxes" style="width: 185px;" />
                <asp:Label ID="lblPreviewMsg" runat="server" Text="<%$ Resources:GUIStrings, TotalWidthOfThumbnailsShouldBeGreaterThanPreviewImageWidth %>" />
            </div>
        </div>
        <div class="right-column">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NoItemsText %>" /></label>
                <input id="txtNoItems" runat="server" class="textBoxes" style="width: 185px;" />
            </div>
            <div class="form-row" id="spanNoVideoSelectedText" runat="server">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, NoSelectedVideoText %>" /></label>
                <input id="txtNoVideoSelectedText" runat="server" class="textBoxes" style="width: 185px;" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" class="primarybutton" value="<%= GUIStrings.Select %>"
        onclick="return SelectProperties();" title="<%= GUIStrings.Select %>" />
    <input type="button" id="cancel" onclick="Close(); " runat="server" value="<%$ Resources:GUIStrings, Close %>"
        class="primarybutton" title="<%$ Resources:GUIStrings, Close %>" />
    <script type="text/javascript">
        var sortBy = '';
        hiddenControl = parent.Data.HiddenSortBy;
        if (hiddenControl && hiddenControl.value != '') {
            SelectListBox('<%=dwSortBy.ClientID%>', hiddenControl.value);
            sortBy = hiddenControl.value;
        }

        hiddenControl = parent.Data.HiddenSortOrder;
        if (hiddenControl && hiddenControl.value != '') {
            SelectRadioButtonList('<%=dwSortOrder.ClientID%>', hiddenControl.value);
        }

        hiddenControl = parent.Data.HiddenPaging;
        if (hiddenControl && hiddenControl.value != '') {
            var paging = hiddenControl.value.split("^");
            if (paging.length == 3) {
                var control = document.getElementById('<%=cbShowPaging.ClientID%>');
                if (paging[0] == "true")
                    control.checked = true;
                else
                    control.checked = false;

                control = document.getElementById('<%=txtRows.ClientID%>');
                if (parseInt(paging[1]) > 0)
                    control.value = paging[1];

                control = document.getElementById('<%=txtColumns.ClientID%>');
                if (parseInt(paging[2]) > 0)
                    control.value = paging[2];
            }
        }

        hiddenControl = parent.Data.HiddenIncludeChild;
        if (hiddenControl) {
            if (hiddenControl.value == 'true')
                document.getElementById('<%=cbIncludeChildNodes.ClientID%>').checked = true;
            else
                document.getElementById('<%=cbIncludeChildNodes.ClientID%>').checked = false;
        }

        hiddenControl = parent.Data.HiddenNoItems;
        if (hiddenControl && hiddenControl.value != '') {
            document.getElementById('<%=txtNoItems.ClientID%>').value = hiddenControl.value;
        }

        hiddenControl = parent.Data.HiddenNoSelectedVideoText;
        if (hiddenControl && hiddenControl.value != '') {
            document.getElementById('<%=txtNoVideoSelectedText.ClientID%>').value = hiddenControl.value;
        }

        hiddenControl = parent.Data.HiddenThumbnails;
        if (hiddenControl && hiddenControl.value != '') {
            document.getElementById('<%=txtThumbnails.ClientID%>').value = hiddenControl.value;
        }

        OnSortByChanged(sortBy);
        OnShowPagingChanged(document.getElementById('<%=cbShowPaging.ClientID%>'));
    </script>
</asp:Content>
