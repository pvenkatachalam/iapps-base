<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.IndexTermFilter" StylesheetTheme="General"
    CodeBehind="IndexTermFilter.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:CallbackPanel ID="cbIndexFilter" runat="server">
        <ContentTemplate>
             </ContentTemplate>
    </iAppsControls:CallbackPanel>
            <div class="columns left-column">
                <asp:RadioButtonList ID="rblFilter" runat="server" AutoPostBack="true">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, MatchAnyOfThePagesTerms %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, ManuallySelectTerms %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, MatchAnyOfThePagesTermsWithinASpecificTermGroupOnly %>"
                        Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </div>
           
            <div class="columns">
                <asp:Panel ID="pnlAssignTags" runat="server">
                    <UC:AssignTags ID="indexTerms" runat="server" TreeHeight="345" />
                </asp:Panel>
                <asp:Panel ID="pnlPageTags" runat="server" CssClass="checkbox-list">
                    <asp:Literal ID="ltNoPageTags" runat="server" />
                    <asp:BulletedList ID="lstPageTags" runat="server" />
                </asp:Panel>
            </div>
        
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" runat="server" />
    <asp:Button ID="btnSave" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" runat="server" />
</asp:Content>
