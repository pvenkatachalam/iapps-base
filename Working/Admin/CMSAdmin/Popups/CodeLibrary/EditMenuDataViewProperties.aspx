<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.EditMenuDataViewProperties"
    StylesheetTheme="General" CodeBehind="EditMenuDataViewProperties.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SelectProperties() {
            var contentDefinitionId, xsltFileName, sortBy, listId, sortOrder, depth, noItemsText;
            var dwObjectId = document.getElementById('<%=dwContentTemplates.ClientID%>');
            contentDefinitionId = dwObjectId.value;
            if (contentDefinitionId != '')
                contentDefinitionId = dwObjectId[dwObjectId.selectedIndex].value;

            dwObjectId = document.getElementById('<%=dwXslt.ClientID%>');
            xsltFileName = dwObjectId.value;
            if (xsltFileName != '')
                xsltFileName = dwObjectId[dwObjectId.selectedIndex].value;

            dwObjectId = document.getElementById('<%=dwSortBy.ClientID%>');
            sortBy = dwObjectId.value;
            if (sortBy != '')
                sortBy = dwObjectId[dwObjectId.selectedIndex].value;

            dwObjectId = parent.Data.HiddenMenuId;
            if (dwObjectId)
                listId = dwObjectId.value;

            dwObjectId = GetRadioButtonList('<%=dwSortOrder.ClientID%>'); ;
            if (dwObjectId && dwObjectId != '')
                sortOrder = dwObjectId;

            dwObjectId = document.getElementById('<%=cbIncludeChildNodes.ClientID%>');
            if (dwObjectId.checked) {
                depth = '0';

                if (document.getElementById('<%=txtDepth.ClientID%>').value != '')
                    depth = document.getElementById('<%=txtDepth.ClientID%>').value;
            }
            else
                depth = '-1';

            noItemsText = document.getElementById('<%=txtNoItems.ClientID%>').value;


            if (contentDefinitionId != '' || xsltFileName != '' || sortBy != '')
                parent.selectMenuProperties(listId, contentDefinitionId, xsltFileName, sortBy, sortOrder, '', depth, noItemsText, '');
            //parent.ListGridWin.hide();

            //Closes the popup
            CanceliAppsAdminPopup();
        }

        function SelectListBox(listBoxId, selectedValue) {
            var listBox = document.getElementById(listBoxId);

            for (var i = 0; i < listBox.options.length; ++i) {
                if (listBox.options[i].value == selectedValue)
                    listBox.options[i].selected = true;
            }
        }

        function SelectRadioButtonList(radioButtonId, selectedValue) {
            var radioButtonList = document.getElementById(radioButtonId);
            var options = radioButtonList.getElementsByTagName('INPUT');
            var labels = radioButtonList.getElementsByTagName('LABEL');

            for (var i = 0; i < options.length; i++) {
                options[i].checked = false;
                if (labels[i].innerHTML == selectedValue) {
                    options[i].checked = true;
                }
            }

        }

        function OnIncludeChildChanged(dwObjectId) {
            includeChild = dwObjectId.checked;
            if (!includeChild) {
                document.getElementById('<%=txtDepth.ClientID%>').disabled = true;
                document.getElementById('<%=dwSortBy.ClientID%>').disabled = false;
                OnSortByChanged('a');
            }
            else {
                document.getElementById('<%=txtDepth.ClientID%>').disabled = false;
                document.getElementById('<%=dwSortBy.ClientID%>').disabled = true;
                OnSortByChanged('');
            }
        }

        function ChangeSortBy(dwObjectId) {
            contentDefinitionId = dwObjectId.value;
            if (contentDefinitionId != '')
                contentDefinitionId = dwObjectId[dwObjectId.selectedIndex].value;

            callBackSortBy.callback(contentDefinitionId, '', document.getElementById('<%=cbIncludeChildNodes.ClientID%>').checked);
        }

        function OnSortByChanged(sortBy) {
            var radioButtons = document.getElementById('<%=dwSortOrder.ClientID%>').getElementsByTagName("INPUT");

            for (var i = 0; i < radioButtons.length; i++) {
                if (sortBy == '')
                    radioButtons[i].disabled = true;
                else
                    radioButtons[i].disabled = false;
            }
        }

        function GetRadioButtonList(radioButtonId) {
            var radioButtonList = document.getElementById(radioButtonId);
            var options = radioButtonList.getElementsByTagName('INPUT');
            var labels = radioButtonList.getElementsByTagName("LABEL");

            for (var i = 0; i < options.length; i++) {
                if (options[i].checked == true) {
                    return labels[i].innerHTML;
                }
            }

        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <div class="left-column">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ChooseContentDefinition %>" /></label>
            <asp:ListBox ID="dwContentTemplates" Width="230" runat="server" CssClass="contentListBox"
                onclick="ChangeSortBy(this);" />
        </div>
        <div class="right-column">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ChooseXSLT %>" /></label>
            <asp:ListBox ID="dwXslt" runat="server" Width="230" CssClass="contentListBox"></asp:ListBox>
        </div>
    </div>
    <div class="form-row">
        <div class="left-column">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, IncludeSubMenus %>" /></label>
                <asp:CheckBox ID="cbIncludeChildNodes" onclick="OnIncludeChildChanged(this)" runat="server" />
            </div>
            <div class="form-row">
                <label class="form-label" id="lblDepth" runat="server">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, Depth %>" /></label>
                <input id="txtDepth" runat="server" class="textBoxes" style="width: 55px;" />
                <span class="coachingText">
                    <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, BlankShowsAll %>" /></span>
            </div>
        </div>
        <div class="right-column">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <label class="form-label">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, SortBy %>" /></label>
                        <ComponentArt:CallBack ID="callBackSortBy" runat="server">
                            <Content>
                                <asp:DropDownList ID="dwSortBy" onchange="OnSortByChanged(this.value);" runat="server"
                                    Width="230" CssClass="contentListBox">
                                </asp:DropDownList>
                            </Content>
                        </ComponentArt:CallBack>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="form-label">
                            &nbsp;</label>
                        <asp:RadioButtonList ID="dwSortOrder" runat="server" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                            <asp:ListItem Text="ASC" Value="ASC"></asp:ListItem>
                            <asp:ListItem Text="DESC" Value="DESC"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-row">
        <div class="left-column">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, NoItemsText %>" /></label>
                <input id="txtNoItems" runat="server" class="textBoxes" style="width: 220px;" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="cancel"  runat="server" value="<%$ Resources:GUIStrings, Cancel %>"
        class="button cancel-button" title="<%$ Resources:GUIStrings, Cancel %>" />
    <input type="button" class="primarybutton" value="<%= GUIStrings.Select %>"
        onclick="return SelectProperties();" title="<%= GUIStrings.Select %>" />
    <script type="text/javascript">
        var hiddenControl = parent.Data.HiddenXsltId;
        if (hiddenControl && hiddenControl.value != '')
            SelectListBox('<%=dwXslt.ClientID%>', hiddenControl.value);

        var sortBy = '';
        hiddenControl = parent.Data.HiddenSortById;
        if (hiddenControl && hiddenControl.value != '') {
            //SelectListBox('<%=dwSortBy.ClientID%>', hiddenControl.value);
            sortBy = hiddenControl.value;
        }

        var includeChild = false;
        hiddenControl = parent.Data.HiddenDepthId;
        if (hiddenControl && hiddenControl.value != '-1') {
            includeChild = true;
            document.getElementById('<%=cbIncludeChildNodes.ClientID%>').checked = true;
            if (hiddenControl.value != '0')
                document.getElementById('<%=txtDepth.ClientID%>').value = hiddenControl.value;
        }

        hiddenControl = parent.Data.HiddenContentDefinitionId;
        if (hiddenControl && hiddenControl.value != '' && hiddenControl.value != '00000000-0000-0000-0000-000000000000') {
            SelectListBox('<%=dwContentTemplates.ClientID%>', hiddenControl.value);
            callBackSortBy.callback(hiddenControl.value, sortBy);
        }
        else
            callBackSortBy.callback('', sortBy, includeChild);

        hiddenControl = parent.Data.HiddenSortOrderId;
        if (hiddenControl && hiddenControl.value != '') {
            SelectRadioButtonList('<%=dwSortOrder.ClientID%>', hiddenControl.value);
        }

        hiddenControl = parent.Data.HiddenNoItemsId;
        if (hiddenControl && hiddenControl.value != '') {
            document.getElementById('<%=txtNoItems.ClientID%>').value = hiddenControl.value;
        }

        OnSortByChanged(sortBy);
        OnIncludeChildChanged(document.getElementById('<%=cbIncludeChildNodes.ClientID%>'));
    </script>
</asp:Content>
