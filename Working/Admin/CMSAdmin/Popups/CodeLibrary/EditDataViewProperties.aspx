<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.EditDataViewProperties" StylesheetTheme="General"
    CodeBehind="EditDataViewProperties.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SelectProperties() {
            var xsltFileName, sortBy, sortOrder, paging, noItemsText, maxItems;

            dwObjectId = document.getElementById('<%=dwXslt.ClientID%>');
            xsltFileName = dwObjectId.value;
            if (xsltFileName != '')
                xsltFileName = dwObjectId[dwObjectId.selectedIndex].value;

            dwObjectId = document.getElementById('<%=dwSortBy.ClientID%>');
            sortBy = dwObjectId.value;
            if (sortBy != '')
                sortBy = dwObjectId[dwObjectId.selectedIndex].value;

            dwObjectId = GetRadioButtonList('<%=dwSortOrder.ClientID%>'); ;
            if (dwObjectId && dwObjectId != '')
                sortOrder = dwObjectId;

            dwObjectId = document.getElementById('<%=cbShowPaging.ClientID%>');
            if (dwObjectId.checked)
                paging = "true^";
            else
                paging = "false^";

            dwObjectId = document.getElementById('<%=txtPageSize.ClientID%>');
            if (dwObjectId)
                paging += dwObjectId.value + "^";

            dwObjectId = document.getElementById('<%=cbNumericPaging.ClientID%>');
            if (dwObjectId.checked)
                paging += "true";
            else
                paging += "false";

            noItemsText = document.getElementById('<%=txtNoItems.ClientID%>').value;
            maxItems = document.getElementById('<%=txtMaxItems.ClientID%>').value;
            if (maxItems == '')
                maxItems = 0;
            parent.selectDataViewProperties(xsltFileName, sortBy, sortOrder, paging, noItemsText, maxItems);

            CanceliAppsAdminPopup();
        }

        function SelectListBox(listBoxId, selectedValue) {
            var listBox = document.getElementById(listBoxId);

            for (var i = 0; i < listBox.options.length; ++i) {
                if (listBox.options[i].value == selectedValue)
                    listBox.options[i].selected = true;
            }
        }

        function SelectRadioButtonList(radioButtonId, selectedValue) {
            var radioButtonList = document.getElementById(radioButtonId);
            var options = radioButtonList.getElementsByTagName('INPUT');
            var labels = radioButtonList.getElementsByTagName('LABEL');

            for (var i = 0; i < options.length; i++) {
                options[i].checked = false;
                if (labels[i].innerHTML == selectedValue) {
                    options[i].checked = true;
                }
            }

        }

        function OnSortByChanged(sortBy) {
            var radioButtons = document.getElementById('<%=dwSortOrder.ClientID%>').getElementsByTagName("INPUT");

            for (var i = 0; i < radioButtons.length; i++) {
                if (sortBy == '')
                    radioButtons[i].disabled = true;
                else
                    radioButtons[i].disabled = false;
            }
        }

        function OnShowPagingChanged(dwObjectId) {
            showPaging = dwObjectId.checked;
            if (!showPaging) {
                document.getElementById('<%=txtPageSize.ClientID%>').disabled = true;
                document.getElementById('<%=cbNumericPaging.ClientID%>').disabled = true;
            }
            else {
                document.getElementById('<%=txtPageSize.ClientID%>').disabled = false;
                document.getElementById('<%=cbNumericPaging.ClientID%>').disabled = false;
            }
        }

        function GetRadioButtonList(radioButtonId) {
            var radioButtonList = document.getElementById(radioButtonId);
            var options = radioButtonList.getElementsByTagName('INPUT');
            var labels = radioButtonList.getElementsByTagName("LABEL");

            for (var i = 0; i < options.length; i++) {
                if (options[i].checked == true) {
                    return labels[i].innerHTML;
                }
            }

        }
    
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <div class="left-column">
            <label class="form-label">
                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ChooseXSLT %>" /></label>
            <asp:ListBox ID="dwXslt" runat="server" Width="230" CssClass="contentListBox"></asp:ListBox>
        </div>
        <div class="right-column">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, MaxItemsToShow %>" /></label>
                <input id="txtMaxItems" runat="server" class="textBoxes" style="width: 55px;" />
                <span class="coachingText">
                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, BlankShowsAll %>" /></span>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, NoItemsText %>" /></label>
                <input id="txtNoItems" runat="server" class="textBoxes" style="width: 220px;" />
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="left-column">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <asp:CheckBox ID="cbShowPaging" onclick="OnShowPagingChanged(this)" runat="server"
                            Text="<%$ Resources:GUIStrings, ShowPaging %>" />
                    </td>
                    <td>
                        <label class="form-label" style="width: 60px;">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, PageSize %>" /></label>
                        <input id="txtPageSize" runat="server" class="textBoxes" style="width: 55px;" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBox ID="cbNumericPaging" runat="server" Text="<%$ Resources:GUIStrings, NumericPaging %>" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="right-column">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td>
                        <label class="form-label">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, SortBy %>" /></label>
                        <ComponentArt:CallBack ID="callBackSortBy" runat="server">
                            <Content>
                                <asp:DropDownList ID="dwSortBy" onchange="OnSortByChanged(this.value);" runat="server"
                                    Width="230" CssClass="contentListBox">
                                </asp:DropDownList>
                            </Content>
                        </ComponentArt:CallBack>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="form-label">
                            &nbsp;</label>
                        <asp:RadioButtonList ID="dwSortOrder" runat="server" RepeatDirection="Horizontal"
                            RepeatLayout="Flow">
                            <asp:ListItem Text="ASC" Value="ASC"></asp:ListItem>
                            <asp:ListItem Text="DESC" Value="DESC"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="cancel" runat="server" value="<%$ Resources:GUIStrings, Cancel %>"
        class="button cancel-button" title="<%$ Resources:GUIStrings, Cancel %>" />
    <input type="button" class="primarybutton" value="<%= GUIStrings.Select %>"
        onclick="return SelectProperties();" title="<%= GUIStrings.Select %>" />
    <asp:HiddenField ID="hdnColumns" runat="server" />
    <script type="text/javascript">
        if (parent.Data.HiddenColumnsId) {
            var columns = parent.Data.HiddenColumnsId.value.split(',');
            var selectBox = document.getElementById('<%=dwSortBy.ClientID%>');
            var optn = document.createElement("OPTION");
            optn.text = "<None>";
            optn.value = "";
            selectBox.options.add(optn);

            for (var i = 0; i < columns.length - 1; i++) {
                optn = document.createElement("OPTION");
                optn.text = optn.value = columns[i];
                selectBox.options.add(optn);
            }
        }

        var hiddenControl = parent.Data.HiddenXsltId;
        if (hiddenControl && hiddenControl.value != '')
            SelectListBox('<%=dwXslt.ClientID%>', hiddenControl.value);

        var sortBy = '';
        hiddenControl = parent.Data.HiddenSortById;
        if (hiddenControl && hiddenControl.value != '') {
            //SelectListBox('<%=dwSortBy.ClientID%>', hiddenControl.value);
            sortBy = hiddenControl.value;
        }

        hiddenControl = parent.Data.HiddenSortOrderId;
        if (hiddenControl && hiddenControl.value != '') {
            SelectRadioButtonList('<%=dwSortOrder.ClientID%>', hiddenControl.value);
        }

        hiddenControl = parent.Data.HiddenPagingId;
        if (hiddenControl && hiddenControl.value != '') {
            var paging = hiddenControl.value.split("^");
            if (paging.length == 3) {
                var control = document.getElementById('<%=cbShowPaging.ClientID%>');
                if (paging[0] == "true")
                    control.checked = true;
                else
                    control.checked = false;

                control = document.getElementById('<%=txtPageSize.ClientID%>');
                if (parseInt(paging[1]) > 0)
                    control.value = paging[1];

                control = document.getElementById('<%=cbNumericPaging.ClientID%>');
                if (paging[2] == "true")
                    control.checked = true;
                else
                    control.checked = false;
            }
        }

        hiddenControl = parent.Data.HiddenNoItemsId;
        if (hiddenControl && hiddenControl.value != '') {
            document.getElementById('<%=txtNoItems.ClientID%>').value = hiddenControl.value;
        }

        hiddenControl = parent.Data.HiddenMaxItemsId;
        if (hiddenControl && hiddenControl.value != '' && parseInt(hiddenControl.value) > 0) {
            document.getElementById('<%=txtMaxItems.ClientID%>').value = hiddenControl.value;
        }

        OnShowPagingChanged(document.getElementById('<%=cbShowPaging.ClientID%>'));
        OnSortByChanged(sortBy);
    </script>
</asp:Content>
