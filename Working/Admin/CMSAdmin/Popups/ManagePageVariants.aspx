<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManagePageVariants.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManagePageVariants" StylesheetTheme="General" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdVariants_OnLoad(sender, eventArgs) {
            $(".page-variants").gridActions({ objList: grdVariants });
        }

        function grdVariants_OnRenderComplete(sender, eventArgs) {
            $(".page-variants").gridActions("bindControls");

            SetVerticalScrollStyles(grdVariants.get_id());
        }

        function grdVariants_OnCallbackComplete(sender, eventArgs) {
            $(".page-variants").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedIds.first();

            var arrIds = selectedItemId.split('|');
            gridActionsJson.CustomAttributes["DeviceId"] = arrIds[0];
            gridActionsJson.CustomAttributes["AudienceId"] = arrIds[1];

            switch (gridActionsJson.Action) {
                case "View":
                    doCallback = false;
                    var returnVal = GetDeviceInfo(gridActionsJson, eventArgs, 'true');
                    parent.OpenSegmentationFrame(returnVal);
                    break;
                case "Edit":
                    doCallback = false;
                    var returnVal = GetDeviceInfo(gridActionsJson, eventArgs, 'false');
                    parent.OpenSegmentationFrame(returnVal, function () {
                        $(".page-variants").gridActions("callback");
                    });
                    break;
                case "Remove":
                    doCallback = window.confirm("<%= JSMessages.RemovePageSegmentConfirmation %>");
                    break;
            }
            if (doCallback) {
                gridActionsJson.SelectedItems = [];
                grdVariants.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdVariants.collapseItem(eventArgs.selectedItem);
                grdVariants.TableCache = null;
                grdVariants.callback();
            }
        }

        function Grid_ItemSelect(sender, eventArgs) {
            if (eventArgs.selectedItem.getMember("Editable").get_value() == false) {
                sender.getItemByCommand("Remove").hideButton();
            }
            else {
                sender.getItemByCommand("Remove").showButton();
            }

            if (gridActionsJson.SelectedItems.first().toLowerCase() == "<%= WebDefaultId %>") {
                sender.getItemByCommand("View").hideButton();
                sender.getItemByCommand("Edit").hideButton();
                sender.getItemByCommand("Remove").hideButton();
            }
        }
        function GetDeviceInfo(gridActionsJson, eventArgs, viewMode) {
            var returnVal = {};
            returnVal.DeviceId = gridActionsJson.CustomAttributes["DeviceId"];
            returnVal.AudienceId = gridActionsJson.CustomAttributes["AudienceId"];
            if (eventArgs.selectedItem.ParentItem != null) {
                returnVal.DeviceText = eventArgs.selectedItem.ParentItem.getMember("Title").get_text();
                returnVal.AudienceText = eventArgs.selectedItem.getMember("Title").get_text();
            }
            else {
                returnVal.DeviceText = eventArgs.selectedItem.getMember("Title").get_text();
                returnVal.AudienceText = "";
            }

            returnVal.NavigateUrl = "ViewOnly=" + viewMode + "&" + GetSegmentIdentifierQueryString(returnVal.DeviceId, returnVal.AudienceId);

            return returnVal;
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <p><%= GUIStrings.MangePageSegmentsHelpText %></p>
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="grdVariants" SkinID="Hierarchical" runat="server" Width="807"
            CallbackCachingEnabled="false" CallbackCacheLookAhead="0" RunningMode="Callback"
            AllowVerticalScrolling="true" Height="336" ShowFooter="false" AllowEditing="false"
            EditOnClickSelectedItem="false" LoadingPanelClientTemplateId="grdVariantsLoadingPanelTemplate">
            <ClientEvents>
                <Load EventHandler="grdVariants_OnLoad" />
                <CallbackComplete EventHandler="grdVariants_OnCallbackComplete" />
                <RenderComplete EventHandler="grdVariants_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" DataCellCssClass="iapps-data-cell grid-item"
                    HeadingCellCssClass="iapps-heading-cell" HeadingRowCssClass="iapps-heading-row"
                    HeadingTextCssClass="iapps-heading-text" RowCssClass="iapps-row" SelectorCellWidth="1"
                    SelectorImageWidth="1" SelectorImageUrl="last.gif" ShowTableHeading="false" HeadingCellHoverCssClass="iapps-heading-cell-hover"
                    SelectedRowCssClass="iapps-selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AlternatingRowCssClass="iapps-alternate-row"
                    ShowSelectorCells="false" AllowGrouping="false" HoverRowCssClass="iapps-hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" Width="540" HeadingText="<%$ Resources:GUIStrings, DeviceAudienceSegment %>" />
                        <ComponentArt:GridColumn DataField="StatusText" HeadingText="<%$ Resources:GUIStrings, Segment %>" DataCellClientTemplateId="StatusTemplate" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="true" Visible="false" />
                        <ComponentArt:GridColumn DataField="Editable" IsSearchable="true" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
                <ComponentArt:GridLevel DataKeyField="Id" DataCellCssClass="iapps-data-cell grid-item"
                    RowCssClass="iapps-row" SelectedRowCssClass="iapps-selected-row" AlternatingRowCssClass="iapps-alternate-row"
                    HoverRowCssClass="iapps-hover-row" ShowHeadingCells="false">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" Width="527" />
                        <ComponentArt:GridColumn DataField="StatusText" DataCellClientTemplateId="StatusTemplate" />
                        <ComponentArt:GridColumn DataField="Id" IsSearchable="true" Visible="false" />
                        <ComponentArt:GridColumn DataField="Editable" IsSearchable="true" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="DimensionTemplate">
                    ## stringformat("{0} x {1}", DataItem.getMember('Height').get_text(), DataItem.getMember('Width').get_text())##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="StatusTemplate">
                    ## DataItem.getMember('StatusText').get_text() ##
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="grdVariantsLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdVariants) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup(true);" />
</asp:Content>
