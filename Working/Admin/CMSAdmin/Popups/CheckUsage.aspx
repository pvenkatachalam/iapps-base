﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" StylesheetTheme="General"
    CodeBehind="CheckUsage.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.CheckUsage" %>
<%@ Register TagPrefix="ug" TagName="UsageGraph" Src="~/UserControls/Dashboard/UsageReport.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <ug:UsageGraph ID="ctlUsageGraph" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" class="button cancel-button" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Close %>" />
</asp:Content>
