<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManagePermissionsByTarget.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManagePermissionsByTarget"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="MenuPermission" Src="~/UserControls/Administration/User/MenuPermissionByTarget.ascx" %>
<%@ Register TagPrefix="UC" TagName="DirectoryPermission" Src="~/UserControls/Administration/User/DirectoryPermissionByTarget.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var isPermissionsChanged = false;
        function grdPermissions_onLoad(sender, eventArgs) {
            $(".target-permissions").gridActions({ objList: grdPermissions });            
        }

        function grdPermissions_onRenderComplete() {
            $(".target-permissions").gridActions("bindControls");
            SetVerticalScrollStyles(grdPermissions.get_id());

            if (gridActionsJson.CustomFilter == "1")
                $(".grid-actions input.grid-button").val("<%= GUIStrings.AddUser %>");
            else
                $(".grid-actions input.grid-button").val("<%= GUIStrings.AddGroup %>");

            $(".grid-actions select").val(gridActionsJson.CustomFilter);

            if (grdPermissions.RecordCount > 0) {
                $(".modal-footer .primarybutton").removeClass("hidden-button");
                $("#btnCancel").val("<%= GUIStrings.Cancel %>");
            }
            else {
                $(".modal-footer .primarybutton").addClass("hidden-button");
                $("#btnCancel").val("<%= GUIStrings.Close %>");
            }
            
            if (typeof UpdateCheckboxState == "function")
                UpdateCheckboxState();
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;
            switch (gridActionsJson.Action) {
                case "AddUser":
                    doCallback = false;
                    var objectType = gridActionsJson.CustomFilter == "1" ? 14 : 15;
                    OpeniAppsAdminPopup("SelectManualListPopup", "RoleId=14&ObjectTypeId=" + objectType, "CloseAddUsers");
                    break;
                case "ViewDefinitions":
                    doCallback = false;
                    OpeniAppsAdminPopup('ViewGroupDefinitions');
                    break;
                default:
                    if (isPermissionsChanged) {
                        doCallback = window.confirm("<%= JSMessages.SwithUserGroupPermissionConfirm %>");
                        if (!doCallback) {
                            if (gridActionsJson.CustomFilter == "1")
                                gridActionsJson.CustomFilter = "2";
                            else
                                gridActionsJson.CustomFilter = "1";

                            $(".grid-actions select").val(gridActionsJson.CustomFilter);
                        }
                    }

                    break;
            }

            if (doCallback) {
                isPermissionsChanged = false;
                grdPermissions.set_callbackParameter(JSON.stringify(gridActionsJson));
                grdPermissions.callback();
            }
        }

        function CallbackGrid() {
            gridActionsJson.CustomFilter = $(".grid-actions select").val();
            grdPermissions.set_callbackParameter(JSON.stringify(gridActionsJson));
            grdPermissions.callback();
        }

        function CloseAddUsers() {
            var listItemsJson = [];
            if (popupActionsJson.CustomAttributes["ListItemsJson"] != "")
                listItemsJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);

            $.each(listItemsJson, function () {
                if (!CheckUserExists(this.Id))
                    AddUser(this.Id.toString(), this.Title)
            });
        }

        function CheckUserExists(userId) {
            for (var i = 0; i < grdPermissions.RecordCount; i++) {
                gItem = grdPermissions.get_table().getRow(i);
                if (gItem.Key.toString().toLowerCase() == userId.toString().toLowerCase())
                    return true;
            }

            return false;
        }

        function AddUser(userId, userName) {
            var rowNumber = grdPermissions.RecordCount;
            grdPermissions.get_table().addEmptyRow(rowNumber);
            var newRowItem = grdPermissions.get_table().getRow(rowNumber);
            grdPermissions.edit(newRowItem);
            newRowItem.setValue(0, userId);
            newRowItem.setValue(1, userName);
            grdPermissions.editComplete();
            grdPermissions.render();
        }
    </script>
</asp:Content>
<asp:Content ID="content" ContentPlaceHolderID="cphContent" runat="Server">
    <p class="help-text">
        Use the dropdown filter on the right to switch between viewing CMS Users and Groups. 
        Disabled options indicate the user have global permission for the corresponding role.
        <br />
        Note: Only users or groups with custom permissions for the menu node appear in this
        interface. Additional users may have permissions because they are members of a default
        CMS Group or have universal permissions. To view or edit a group's members, go to
        Content Manager Groups.</p>
    <iAppsControls:GridActions ID="gridActions" runat="server" />
    <UC:MenuPermission ID="mnuPermission" runat="server" />
    <UC:DirectoryPermission ID="dirPermission" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server" ClientIDMode="Static">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CausesValidation="false" CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>"
        OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSaveClose" runat="server" Text="<%$ Resources:GUIStrings, SaveAndClose %>"
        ToolTip="<%$ Resources:GUIStrings, SaveAndClose %>" CssClass="primarybutton show-loading"
        OnClick="btnSaveClose_Click" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton show-loading"
        OnClick="btnSave_Click" />
</asp:Content>
