<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectManualListPopup" StylesheetTheme="General"
    CodeBehind="SelectManualListPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="PageLibrary" Src="~/UserControls/Libraries/Data/ManagePageLibrary.ascx" %>
<%@ Register TagPrefix="UC" TagName="ContentLibrary" Src="~/UserControls/Libraries/Data/ManageContent.ascx" %>
<%@ Register TagPrefix="UC" TagName="FileLibrary" Src="~/UserControls/Libraries/Data/ManageAssetFile.ascx" %>
<%@ Register TagPrefix="UC" TagName="ImageLibrary" Src="~/UserControls/Libraries/Data/ManageAssetImage.ascx" %>
<%@ Register TagPrefix="UC" TagName="UserLibrary" Src="~/UserControls/Administration/User/ManageAdminUser.ascx" %>
<%@ Register TagPrefix="UC" TagName="GroupLibrary" Src="~/UserControls/Administration/User/ManageAdminGroup.ascx" %>
<%@ Register TagPrefix="UC" TagName="FormLibrary" Src="~/UserControls/Libraries/Data/ManageFormLibrary.ascx" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdListContent_onItemExternalDrop(sender, eventArgs) {
            var draggedItem = eventArgs.get_item();
            var targetItem = eventArgs.get_target();
            var table = grdListContent.get_table();
            var index = grdListContent.get_recordCount() + 1;
            if (targetItem)
                index = targetItem.get_index();

            table.Data.splice(draggedItem.get_index(), 1);
            grdListContent.Data = table.Data = table.Data.slice(0, index).concat([draggedItem.Data]).concat(table.Data.slice(index));
            grdListContent.render();
        }

        function SelectListItems() {
            listItemsJson = [];
            var listItemsJsonTemp = [];
            if (popupActionsJson.CustomAttributes["ListItemsJson"] != "undefined" &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != null &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != "") {
                listItemsJsonTemp = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);
                if (listItemsJsonTemp.length > 0) {
                    $.each(listItemsJsonTemp, function () {
                        if (this.ObjectTypeId != gridActionsJson.CustomAttributes["ObjectTypeId"]) {
                            //alert(this);
                            listItemsJson.push({ "Id": this.Id, "Title": this.Title, "ObjectTypeId": this.ObjectTypeId, "ModifiedDate": this.ModifiedDate });
                        }
                    });
                }
            }


            for (var i = 0; i < grdListContent.RecordCount; i++) {
                gItem = grdListContent.get_table().getRow(i);
                listItemsJson.push({ "Id": gItem.Key, "Title": gItem.getMember("Title").get_value(), "ObjectTypeId": gridActionsJson.CustomAttributes["ObjectTypeId"], "ModifiedDate": gItem.getMember("ModifiedDate").get_value() });
            }

            if (gridActionsJson.SelectedItems.length > 0 && !CheckListItemExist(gridActionsJson.SelectedItems.first())) {
                alert("<%= JSMessages.PleaseAddListItems %>");
            }
            else {
                popupActionsJson.CustomAttributes["ListItemsJson"] = JSON.stringify(listItemsJson);

                SelectiAppsAdminPopup();
            }

            return false;
        }

        var listItemsJson = [];
        function grdListContent_onLoad() {            
            if (popupActionsJson.CustomAttributes["ListItemsJson"] != "undefined" &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != null &&
                popupActionsJson.CustomAttributes["ListItemsJson"] != "")
                listItemsJson = JSON.parse(popupActionsJson.CustomAttributes["ListItemsJson"]);

            $.each(listItemsJson, function () {
                if (this.ObjectTypeId == gridActionsJson.CustomAttributes["ObjectTypeId"])
                    AddNewListItem(this.Id, this.Title, this.ModifiedDate);
            });
        }

        function AddManualListItem(objGrid) {
            $.each(gridActionsJson.SelectedItems, function () {
                var selItem = objGrid.getItemFromKey(0, this);
                if (!CheckListItemExist(this)) {
                    var modifiedDate = "";
                    if (selItem.getMember("ModifiedDate") != null)
                        modifiedDate = selItem.getMember("ModifiedDate").get_value();
                    listItemsJson.push({ "Id": this, "Title": selItem.getMember("Title").get_value(), "ModifiedDate": modifiedDate });
                    AddNewListItem(this, selItem.getMember("Title").get_value(), modifiedDate);
                }
            });
        }

        function AddManualImageItem(objList) {
            $.each(gridActionsJson.SelectedItems, function () {
                var selItem = objList.getItemFromKey(this);
                if (!CheckListItemExist(this)) {
                    listItemsJson.push({ "Id": this, "Title": selItem.getValue("Title"), "ModifiedDate": selItem.getValue("ModifiedDate") });
                    AddNewListItem(this, selItem.getValue("Title"), selItem.getValue("ModifiedDate"));
                }
            });
        }      

        function AddManualUserItem(objList) {
            $.each(gridActionsJson.SelectedItems, function () {
                var selItem = objList.getItemFromKey(0, this);
                if (!CheckListItemExist(this)) {
                    listItemsJson.push({ "Id": this, "Title": selItem.getMember("UserName").get_value() });
                    AddNewListItem(this, selItem.getMember("UserName").get_value());
                }
            });
        }

        function AddNewListItem(id, title, modifiedDate) {
            var rowNumber = grdListContent.RecordCount;
            grdListContent.get_table().addEmptyRow(rowNumber);
            var newRowItem = grdListContent.get_table().getRow(rowNumber);
            grdListContent.edit(newRowItem);
            newRowItem.setValue(0, id);
            newRowItem.setValue(1, title);
            //newRowItem.setValue(2, selectedItemType);
            newRowItem.setValue(3, modifiedDate);
            grdListContent.editComplete();
            grdListContent.render();
        }

        function DeleteListItem(rowId) {
            var gItem = grdListContent.getItemFromClientId(rowId);
            $.each(listItemsJson, function (i) {
                if (this.Id.toString() == gItem.Key.toString()) {
                    listItemsJson.splice(i, 1);
                    return false;
                }
            });

            grdListContent.deleteItem(gItem);

            return false;
        }

        function CheckListItemExist(id) {
            var exists = false;
            $.each(listItemsJson, function () {
                if (this.Id.toString() == id.toString()) {
                    exists = true;
                    return false;
                }
            });
            return exists;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="manual-list-left">
        <UC:PageLibrary ID="pageLibrary" runat="server" HideTreeContextMenu="true" AllowMultiSelect="true" />
        <UC:FormLibrary ID="formLibrary" runat="server" AllowMultiSelect="true" />
        <UC:ContentLibrary ID="contentLibrary" runat="server" AllowMultiSelect="true" Visible="false" />
        <UC:FileLibrary ID="fileLibrary" runat="server" AllowReorder="false" AllowMove="false"
            AllowMultiSelect="true" Visible="false" />
        <UC:ImageLibrary ID="imageLibrary" runat="server" AllowMultiSelect="true" Visible="false" />
        <UC:UserLibrary ID="userLibrary" runat="server" Visible="false" />
        <UC:GroupLibrary ID="groupLibrary" runat="server" Visible="false" />
    </div>
    <div class="manual-list-right">
        <h2>
            <asp:Literal ID="ltGridHeading" runat="server" /></h2>
        <div class="grid-section">
            <ComponentArt:Grid ID="grdListContent" SkinID="Default" runat="server" ItemDraggingEnabled="true"
                ExternalDropTargets="grdListContent" ItemDraggingClientTemplateId="grdListContentDragTemplate"
                ShowFooter="false" ShowHeader="false" Width="100%" CssClass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="grdListContent_onLoad" />
                    <ItemExternalDrop EventHandler="grdListContent_onItemExternalDrop" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="Id" DataCellClientTemplateId="listTemplate" TextWrap="true" />
                            <ComponentArt:GridColumn DataField="Title" Visible="false" IsSearchable="true" />
                            <ComponentArt:GridColumn DataField="Type" Visible="false" />
                            <ComponentArt:GridColumn DataField="ModifiedDate" Visible="false" FormatString="d" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="listTemplate">
                        <div class="modal-grid-row clear-fix">
                            <div class="first-cell">
                                ## DataItem.getMember('Title').get_text() ##
                            </div>
                            <div class="last-cell">
                                <a href="#" onclick="return DeleteListItem('## DataItem.ClientId ##');">
                                    <img src="/Admin/App_Themes/General/images/icon-delete-cross.png" border="0" alt="X" /></a>
                            </div>
                        </div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="grdListContentDragTemplate">
                        ## GetGridDragTemplate(DataItem.getMember('Title').get_text()) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, Save%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SelectListItems();" />
</asp:Content>
