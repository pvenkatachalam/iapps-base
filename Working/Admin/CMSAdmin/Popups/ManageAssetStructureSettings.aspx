﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAssetStructureSettings.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageAssetStructureSettings" MasterPageFile="~/Popups/iAPPSPopup.master"
    Theme="General" ClientIDMode="Static" %>

<%@ Register TagPrefix="UC" TagName="SecurityLevels" Src="~/UserControls/Libraries/SecurityLevelWithListbox.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="form-section" runat ="server">
        <div class="columns" runat ="server" id = "div1">
            <div class="form-row">
                <label class="form-label">
                   <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, MenuName %>"/><span>&nbsp;*</span>
                </label>
                <asp:TextBox ID="tbAssetName" runat="server" ReadOnly="true" CssClass="disabled"
                    Style="width: 290px;" />
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Description %>"/>
                </label>
                <asp:TextBox ID="tbAssetDescription" runat="server" TextMode="MultiLine" Rows="4"
                    Columns="20" Style="width: 287px;" />
            </div>
        </div>
        <div class="form-row" runat="server" id ="div2">
            <UC:SecurityLevels runat="server" ID="ctlSecurityLevels" />
        </div>
        <div class="form-row">
            <asp:CheckBox ID="cbPropagateSecurityLevels" Text="<%$ Resources:GUIStrings, PropagateSecurityLevelToAllSubMenuItems %>" runat="server" /><br />
            <asp:CheckBox ID="cbInheritSecurityLevels" Text="<%$ Resources:GUIStrings, InheritUserGroupPermissionsFromParentMenuItem %>" runat="server" /> <br />
            <asp:CheckBox ID="cbAllowAccessInChildrenSite" Text="<%$ Resources:GUIStrings, AllowAccessInChildrenSites %>" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>"
        CausesValidation="false" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_OnClick" />
</asp:Content>
