<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    ClientIDMode="Static" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageChannelDetails" StylesheetTheme="General"
    CodeBehind="ManageChannelDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="ListProperties" Src="~/UserControls/Libraries/Data/ListProperties.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button cancel-button" Text="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" CausesValidation="true" />
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:ListProperties ID="listProperties" runat="server" />
</asp:Content>
