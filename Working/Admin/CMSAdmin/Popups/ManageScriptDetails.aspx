﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManageScriptDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageScriptDetails"
    Theme="General" ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#fileScript").iAppsFileUpload({ textboxWidth: "140px", allowedExtensions: "js" });
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Title %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="200" />
            <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.FileName %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:FileUpload ID="fileScript" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" />
            <asp:Label ID="ltFileName" runat="server" CssClass="coaching-text" />
            <asp:RequiredFieldValidator ID="reqfileScript" ControlToValidate="fileScript" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, FileNameIsRequired %>" Display="None"
                SetFocusOnError="true" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Description %></label>
        <div class="form-value">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="200"
                TextMode="MultiLine" />
            <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <asp:PlaceHolder ID="phTheme" runat="server">
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.Theme %></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlThemes" runat="server" OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged" AutoPostBack="true" ToolTip="<%$ Resources:GUIStrings, ThemeToolTip %>">
                </asp:DropDownList>
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <asp:CheckBox ID="chkIsGlobal" runat="server" Text="<%$ Resources:GUIStrings, ScriptIsGlobal %>" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
</asp:Content>
