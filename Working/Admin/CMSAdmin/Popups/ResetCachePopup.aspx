﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ResetCachePopup"
    MasterPageFile="~/Popups/iAPPSPopup.Master" CodeBehind="ResetCachePopup.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $.ajax({
                url: "../InvalidateCache.aspx",
                type: "POST",
                data: {},
                success: function (result) {
                    populateResultsTable(JSON.parse(result));
                },
                error: function (result) {
                    alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, ErrorLoadingResults %>'/>");
                }
            });
        });

        function populateResultsTable(results) {
            for (var i in results) {
                if (typeof results[i].Url != "undefined") {
                    var row = $("<tr>").addClass(i % 2 == 0 ? "odd-row" : "even-row");
                    row.append($("<td>").html(results[i].Url));
                    row.append($("<td>").html(results[i].Result));

                    $("#tblCache").append(row);
                }
            }
            $("div#results").show();
            $("div#loading").hide();
        }

    </script>
</asp:Content>
<asp:Content ID="cntContent" runat="server" ContentPlaceHolderID="cphContent">
    <div id="results" style="display: none;" class="results">
        <table width="100%" cellpadding="0" cellspacing="0" class="grid">
            <thead>
                <tr class="heading-row">
                    <th style="width: 70%;">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:JSMessages, Cache %>" />
                    </th>
                    <th style="width: 30%;">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:JSMessages, Result %>" />
                    </th>
                </tr>
            </thead>
            <tbody id="tblCache">
            </tbody>
        </table>
    </div>
    <div id="loading">
        <img src="/Admin/App_Themes/General/images/spinner.gif" alt="Please wait..." />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" class="button" onclick="return CanceliAppsAdminPopup(true);" value="<%= GUIStrings.Close %>" />
</asp:Content>
