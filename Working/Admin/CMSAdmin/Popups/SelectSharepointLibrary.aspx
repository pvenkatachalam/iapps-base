<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectSharepointLibrary"
    StylesheetTheme="General" CodeBehind="SelectSharepointLibrary.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register Src="~/UserControls/Libraries/SPLibraryTreeView.ascx" TagName="SPLibraryTree"
    TagPrefix="UC" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function LoadSPGrid(spSiteId, folderPath, spTemplateType) {
            gridActionsJson.CustomAttributes["FolderPath"] = folderPath;
            $("#sharepoint_library").gridActions("callback");
        }

        function grdContents_OnLoad(sender, eventArgs) {
            $("#sharepoint_library").gridActions({ objList: grdContents });
        }

        function grdContents_OnRenderComplete(sender, eventArgs) {
            $("#sharepoint_library").gridActions("bindControls");
            $("#sharepoint_library").iAppsSplitter();
        }

        function grdContents_OnCallbackComplete(sender, eventArgs) {
            $("#sharepoint_library").gridActions("displayMessage");
        }

        function Grid_Callback(sender, eventArgs) {
            var doCallback = true;

            if (doCallback) {
                gridActionsJson.SelectedItems = [];
                if (typeof grdContents != "undefined") {
                    grdContents.set_callbackParameter(JSON.stringify(gridActionsJson));
                    grdContents.callback();
                }
                else {
                    upImages.set_callbackParameter(JSON.stringify(gridActionsJson));
                    upImages.callback();
                }
            }
        }

        function SelectFromSPLibrary() {
            if (gridActionsJson.SelectedItems.length > 0) {
                var selectedItem = $("#sharepoint_library").gridActions("getSelectedItem");
                var fileUrl = "";
                if (typeof grdContents != "undefined") {
                    fileUrl = selectedItem.getMember("FileURL").get_value();
                }
                else {
                    fileUrl = selectedItem.getValue("FileURL");
                }

                popupActionsJson.CustomAttributes["FileUrl"] = fileUrl;
                popupActionsJson.CustomAttributes["Type"] = gridActionsJson.CustomAttributes["Type"];
                SelectiAppsAdminPopup();
            }
            else {
                alert("Please select an item.");
            }

            return false;
        }

        function upImages_OnLoad() {
            $("#sharepoint_library").gridActions({
                objList: $(".asset-list"),
                type: "list"
            });
        }

        function upImages_OnRenderComplete() {
            $("#sharepoint_library").gridActions("bindControls");
            $("#sharepoint_library").iAppsSplitter();
        }

        function upImages_OnCallbackComplete() {
            $("#sharepoint_library").gridActions("displayMessage");
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div id="sharepoint_library">
        <div class="left-control">
            <UC:SPLibraryTree ID="libraryTree" runat="server" />
        </div>
        <div class="right-control">
            <div class="form-row clear-fix">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, SelectSite %>" />
                </label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlSites" Width="160" runat="server" AutoPostBack="true" EnableViewState="true"
                        OnSelectedIndexChanged="ddlSites_OnSelectedIndexChanged" />
                </div>
            </div>
            <asp:PlaceHolder ID="phDocuments" runat="server">
                <iAppsControls:GridActions ID="gridActionsDocuments" runat="server" />
                <div class="grid-section">
                    <ComponentArt:Grid ID="grdContents" SkinID="Default" runat="server" Width="100%"
                        RunningMode="Callback" ShowFooter="false" LoadingPanelClientTemplateId="grdContentsLoadingPanelTemplate"
                        CssClass="fat-grid">
                        <ClientEvents>
                            <Load EventHandler="grdContents_OnLoad" />
                            <CallbackComplete EventHandler="grdContents_OnCallbackComplete" />
                            <RenderComplete EventHandler="grdContents_OnRenderComplete" />
                        </ClientEvents>
                        <Levels>
                            <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                                SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                                SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row">
                                <Columns>
                                    <ComponentArt:GridColumn DataField="FileName" DataCellServerTemplateId="DetailsTemplate" />
                                    <ComponentArt:GridColumn DataField="Title" Visible="false" IsSearchable="true" />
                                    <ComponentArt:GridColumn DataField="FileURL" Visible="false" IsSearchable="true" />
                                    <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="true" />
                                </Columns>
                            </ComponentArt:GridLevel>
                        </Levels>
                        <ServerTemplates>
                            <ComponentArt:GridServerTemplate ID="DetailsTemplate">
                                <Template>
                                    <div class="modal-grid-row grid-item">
                                        <h4>
                                            <%# Container.DataItem["FileName"]%></h4>
                                    </div>
                                </Template>
                            </ComponentArt:GridServerTemplate>
                        </ServerTemplates>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="grdContentsLoadingPanelTemplate">
                                ## GetGridLoadingPanelContent(grdContents) ##
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                    </ComponentArt:Grid>
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phPictures" runat="server" Visible="false">
                <iAppsControls:CallbackPanel ID="upImages" runat="server" OnClientCallbackComplete="upImages_OnCallbackComplete"
                    OnClientRenderComplete="upImages_OnRenderComplete" OnClientLoad="upImages_OnLoad"
                    CssClass="sp-pictures">
                    <ContentTemplate>
                        <iAppsControls:GridActions ID="gridActionsPictures" runat="server" />
                        <div class="grid-section">
                            <asp:ListView ID="lvImages" runat="server" ItemPlaceholderID="phImageList">
                                <EmptyDataTemplate>
                                    <div class="empty-list">
                                        No images found!
                                    </div>
                                </EmptyDataTemplate>
                                <LayoutTemplate>
                                    <div class="asset-list clear-fix">
                                        <asp:PlaceHolder ID="phImageList" runat="server" />
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <div class="asset-list-item grid-item clear-fix" runat="server" id="divItem">
                                        <div class="asset-thumbnail-holder">
                                            <label>
                                                <asp:Literal ID="ltSerialNo" runat="server" /></label>
                                            <asp:Image ID="imgThumbnail" runat="server" ImageUrl="~/App_Themes/General/images/no-thumbnail.png" />
                                        </div>
                                        <div class="asset-title">
                                            <h5>
                                                <asp:Literal ID="ltFileName" runat="server" /></h5>
                                            <asp:Literal ID="ltTitle" runat="server" />
                                            <input type="hidden" id="hdnFileUrl" runat="server" datafield="FileURL" />
                                            <input type="hidden" id="hdnId" runat="server" />
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </ContentTemplate>
                </iAppsControls:CallbackPanel>
            </asp:PlaceHolder>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, Select%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Select %>" OnClientClick="return SelectFromSPLibrary();" />
</asp:Content>
