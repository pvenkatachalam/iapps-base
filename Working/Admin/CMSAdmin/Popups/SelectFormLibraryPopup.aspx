﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectFormLibraryPopup"
    StylesheetTheme="General" CodeBehind="SelectFormLibraryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="FormLibrary" Src="~/UserControls/Libraries/Data/ManageFormLibrary.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
<script type="text/javascript">
    $(function () {
        setTimeout(function () {
            $("#form_library").iAppsSplitter();
        }, 200);
    });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:FormLibrary ID="formLibrary" runat="server"  />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectForm%>" OnClientClick="return SelectFormFromLibrary();"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectForm %>"  />
</asp:Content>
