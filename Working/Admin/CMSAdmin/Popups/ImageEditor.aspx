<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageEditor.aspx.cs" StylesheetTheme="General"
    ClientIDMode="Static" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ImageEditor" MasterPageFile="~/Popups/iAPPSPopup.Master" ValidateRequest="false" %>

<%@ Register Assembly="Neodynamic.WebControls.ImageDraw" Namespace="Neodynamic.WebControls.ImageDraw"
    TagPrefix="neoimg" %>
<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="server">
    <link href="../css/uicrop.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            if ($(".errorMessage").length == 0 || $(".errorMessage").html() == "") {
                init_imageCrop();

                $("#image-saveas").iAppsDialog();
            }
            else {
                $(".left-control").hide();
                $(".primarybutton").hide();
                $("#btnReset").hide();
            }
        });

        function toggleAspectControls(radio) {
            $("#txtHeight").removeAttr("disabled").removeClass("disabled");
            $("#txtWidth").removeAttr("disabled").removeClass("disabled");

            if ($("#ddlConstrain").prop("selectedIndex") == 1)
                $("#txtHeight").attr("disabled", "disabled").addClass("disabled");
            else if ($("#ddlConstrain").prop("selectedIndex") == 2)
                $("#txtWidth").attr("disabled", "disabled").addClass("disabled");

            return true;
        }

        function CheckResizeDimension(oWidth, oHeight) {
            if (parseInt(document.getElementById('txtWidth').value.length) <= 0 || parseInt(document.getElementById('txtHeight').value.length) <= 0) {
                alert("<%= JSMessages.EnterImageWidthAndHeightToResize %>");
                return false;
            }
            else
                return true;
        }

        function SaveChanges() {
            var accept = confirm("<%= JSMessages.AreYouSureYouWantToSaveTheChanges %>");

            if (accept)
                ShowiAppsLoadingPanel();

            return accept;
        }

        function SaveAsChanges() {
            if (Page_ClientValidate() == true) {
                $("#hdnSelectedImageNode").val(treeActionsJson.FolderId);
                ShowiAppsLoadingPanel();
                return true;
            }

            return false;
        }

        function OpenSaveAs() {
            $("#image-saveas").iAppsDialog("open");

            return false;
        }

        function CloseSaveAs() {
            $("#image-saveas").iAppsDialog("close");

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="cntBody" ContentPlaceHolderID="cphContent" runat="server">
    <asp:PlaceHolder ID="phImageEditor" runat="server">
    <div class="left-control">
        <h2>
            Crop</h2>
        <div class="tool-section">
            <asp:Button CssClass="crop image-button" ID="imgBtnCrop" runat="server" ToolTip="<%$ Resources:GUIStrings, CropImage %>"
                Text="<%$ Resources:GUIStrings, CropImage %>" OnClick="CropImage" />
        </div>
        <h2>
            Rotate</h2>
        <div class="tool-section">
            <asp:Button CssClass="rotateAnti image-button" ID="imgBtnFlipVertical" runat="server"
                ToolTip="<%$ Resources:GUIStrings, RotateLeft %>" Text="<%$ Resources:GUIStrings, RotateLeft %>"
                OnClick="imageRotateLeft" />
            <asp:Button CssClass="rotate image-button" ID="imgBtnFlopHorizontal" runat="server"
                ToolTip="<%$ Resources:GUIStrings, RotateRight %>" Text="<%$ Resources:GUIStrings, RotateRight %>"
                OnClick="imageRotateRight" />
            <asp:Button CssClass="flipHorizontal image-button" ID="imgBtnRotateLeft" runat="server"
                ToolTip="<%$ Resources:GUIStrings, FlipVertical %>" Text="<%$ Resources:GUIStrings, FlipVertical %>"
                OnClick="flipImageVertical" />
            <asp:Button CssClass="flipVertical image-button" ID="imgBtnRotateRight" runat="server"
                ToolTip="<%$ Resources:GUIStrings, FlipHorizontal %>" Text="<%$ Resources:GUIStrings, FlipHorizontal %>"
                OnClick="flopImageHorizontal" />
        </div>
        <h2>
            Resize</h2>
        <div class="tool-section">
            <div class="form-row">
                <label class="form-label">
                    Width</label>
                <div class="form-value">
                    <asp:TextBox ID="txtWidth" CssClass="textBoxes" Width="135" runat="server" /></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    Height</label>
                <div class="form-value">
                    <asp:TextBox ID="txtHeight" CssClass="textBoxes" Width="135" runat="server" /></div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ConstrainColon %>" /></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlConstrain" runat="server" Width="145" onchange="return toggleAspectControls(this);">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, None %>" Value="none" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Width %>" Value="width"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, Height %>" Value="height"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="button-row">
                <asp:Button ToolTip="<%$ Resources:GUIStrings, Apply %>" ID="btnImgResize" runat="server"
                    CssClass="button" Text="<%$ Resources:GUIStrings, Apply %>" OnClientClick="return CheckResizeDimension();"
                    OnClick="submit_resize" />
            </div>
        </div>
    </div>
    <div class="right-control">
        <input id="input_crop_x" type="hidden" runat="server" />
        <input id="input_crop_y" type="hidden" runat="server" />
        <input id="input_crop_width" type="hidden" runat="server" />
        <input id="input_crop_height" type="hidden" runat="server" />
        <input id="image_type" type="hidden" runat="server" />
        <!-- Image Editor -->
        <div id="imageWrapper" class="image-wrapper">
            <div class="messageContainer">
                <asp:Label ID="ltError" runat="server" CssClass="errorMessage" />
                <div id="crop_details" class="crop-details">
                </div>
            </div>
            <div id="imageContainer">
                <neoimg:ImageDraw runat="server" ID="ImageDraw1">
                    <Elements>
                        <neoimg:ImageElement Source="File" SourceFile="/Admin/App_Themes/General/images/butterfly.jpg">
                        </neoimg:ImageElement>
                    </Elements>
                </neoimg:ImageDraw>
            </div>
        </div>
    </div>
    <div id="image-saveas" class="iapps-modal" style="display: none;">
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True"
            ValidationGroup="SaveAs" />
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SelectDirectory %>" />
            </h2>
        </div>
        <div class="modal-content clear-fix">
            <div class="left-control">
                <asp:HiddenField ID="hdnSelectedImageNode" runat="server" ClientIDMode="Static" />
                <UC:LibraryTree ID="libraryTree" runat="server" />
            </div>
            <div class="right-control">
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FileName %>" /></label>
                    <div class="form-value multi-column">
                        <asp:TextBox ID="txtFileName" runat="server" CssClass="textBoxes" Width="160" />
                        <span><strong>.</strong></span>
                        <asp:DropDownList ID="ddlFileType" runat="server" Width="70">
                            <asp:ListItem Text="<%$ Resources:GUIStrings, BMP %>" Value="bmp"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:GUIStrings, JPG %>" Value="jpg"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:GUIStrings, GIF %>" Value="gif"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:GUIStrings, PNG %>" Value="png"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="reqtxtFileName" ControlToValidate="txtFileName" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, FileNameIsRequired %>" Display="None"
                            SetFocusOnError="true" ValidationGroup="SaveAs" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="240" />
                        <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>" Display="None"
                            SetFocusOnError="true" ValidationGroup="SaveAs" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" TextMode="MultiLine"
                            Rows="3" Width="240" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, AltText %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtAltText" runat="server" CssClass="textBoxes" TextMode="MultiLine"
                            Rows="3" Width="240" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer clear-fix">
            <asp:Button ID="btnSaveAsCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Cancel %>"
                ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CloseSaveAs();" />
             <asp:Button ID="btnSaveAsSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
                ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="OnSaveAs" OnClientClick="return SaveAsChanges();"
                ValidationGroup="SaveAs" />
        </div>
    </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phAccessDenied" runat="server" Visible="false">
        <div>
            <asp:Localize ID="lc44" runat="server" Text="<%$ Resources:GUIStrings, EditImageAccessDenied %>" />
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Close %>"
        ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnReset" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, ResetOriginalImage %>"
        ToolTip="<%$ Resources:GUIStrings, ResetOriginalImage %>" OnClick="ShowOriginalImage" ClientIDMode="Static" />
    <asp:Button ID="btnSaveAs" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, SaveAs %>"
        ToolTip="<%$ Resources:GUIStrings, SaveAs %>" OnClientClick="return OpenSaveAs();" />
    <asp:Button ID="btnSave" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SaveChanges();"
        OnClick="OnSave" />
</asp:Content>
