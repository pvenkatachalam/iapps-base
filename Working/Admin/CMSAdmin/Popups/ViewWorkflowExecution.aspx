<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewWorkflowExecution" Theme="General"
    CodeBehind="ViewWorkflowExecution.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" ClientIDMode="Static" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h3><asp:Literal ID="litWorkflow" runat="server" /></h3>
    <asp:ObjectDataSource ID="odsWorkflowList" runat="server" EnablePaging="false" EnableViewState="true" 
        EnableCaching="false" TypeName="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewWorkflowExecution" OnSelecting="odsWorkflowList_Selecting"
        SelectMethod="GetWorkflowExecutionSequence"></asp:ObjectDataSource>
    <asp:ListView ID="lstWorkflowSequenceList" runat="server" ItemPlaceholderID="pageListContainer"
        DataSourceID="odsWorkflowList">
        <LayoutTemplate>
            <table width="100%" cellpadding="0" cellspacing="0" class="grid" id="gridview">
                <thead>
                    <tr class="heading-row">
                        <th id="th1" style="text-align:center">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ExecutionOrder %>" />
                        </th>
                        <th id="th2">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, UserGroupName %>" />
                        </th>
                        <th id="th3">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, ActorName %>" />
                        </th>
                        <th id="th4">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Status %>" />
                        </th>
                        <th id="th5">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, ActionDate %>" />
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:PlaceHolder ID="pageListContainer" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr class='<%# Container.DataItemIndex % 2 == 0 ? "odd-row" : "even-row" %>'>
                <td style="text-align:center">
                    <asp:Literal ID="ltSequence" runat="server" Text='<%# Eval("ExecutionOrder") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltUserGroupActorName" runat="server" Text='<%# Eval("SequenceActorName") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltActorName" runat="server" Text='<%# Eval("ActorName") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltWorkflowState" runat="server" Text='<%# Eval("ActionName") %>' />
                </td>
                <td>
                    <asp:Literal ID="ltLastActionDate" runat="server" Text='<%# Eval("ActionDate") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <EmptyDataTemplate>
            <div class="grid-emptyText">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NoSequenceToDisplay %>" />
            </div>
        </EmptyDataTemplate>
    </asp:ListView>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        ToolTip="<%$ Resources:GUIStrings, Close %>" CssClass="button secondButton"
        OnClientClick="return CanceliAppsAdminPopup();" />
</asp:Content>
