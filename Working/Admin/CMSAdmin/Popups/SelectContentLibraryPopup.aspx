<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectContentLibraryPopup" StylesheetTheme="General"
    CodeBehind="SelectContentLibraryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="ContentLibrary" Src="~/UserControls/Libraries/Data/ManageContent.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $("#content_library").iAppsSplitter();
            }, 200);
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:ContentLibrary ID="contentLibrary" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectContent%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectContent %>" OnClientClick="return SelectContentFromLibrary()" />
</asp:Content>
