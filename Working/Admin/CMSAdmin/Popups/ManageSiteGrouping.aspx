﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ManageSiteGrouping.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageSiteGrouping" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script language="javascript" type="text/javascript">
        function upSiteGrouping_OnLoad(sender, eventArgs) {
            $(".site-grouping").gridActions({
                objList: $(".collection-table"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upSiteGrouping_Callback(sender, eventArgs);
                }
            });
        }

        function upSiteGrouping_OnRenderComplete(sender, eventArgs) {
            $(".site-grouping").gridActions("bindControls");
        }

        function upSiteGrouping_OnCallbackComplete(sender, eventArgs) {
            $(".site-grouping").gridActions("displayMessage");

            $("#txtTitle").val("");
            $("#txtDescription").val("");
        }

        function upSiteGrouping_Callback(sender, eventArgs) {
            var doCallback = true;

            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");
                    break;
                case "Edit":
                    doCallback = false;
                    $("#txtTitle").val(eventArgs.selectedItem.getValue("Title"));
                    $("#txtDescription").val(eventArgs.selectedItem.getValue("Description"));
                    $("#btnAdd").hide();
                    $("#btnUpdate").show();
                    break;
            }

            if (doCallback) {
                if (gridActionsJson.SelectedItems.length > 0) {
                    gridActionsJson.CustomAttributes["SelectedItem"] = gridActionsJson.SelectedItems[0];
                    gridActionsJson.SelectedItems = [];
                }

                upSiteGrouping.set_callbackParameter(JSON.stringify(gridActionsJson));
                upSiteGrouping.callback();
            }
        }

        function AddSiteGrouping() {
            if (Page_ClientValidate() == true) {
                gridActionsJson.CustomAttributes["Title"] = $("#txtTitle").val();
                gridActionsJson.CustomAttributes["Description"] = $("#txtDescription").val();

                $(".site-grouping").gridActions("callback", "Add");
            }
            return false;
        }

        function UpdateSiteGrouping() {
            if (Page_ClientValidate() == true) {
                gridActionsJson.CustomAttributes["Title"] = $("#txtTitle").val();
                gridActionsJson.CustomAttributes["Description"] = $("#txtDescription").val();

                $(".site-grouping").gridActions("callback", "Update");
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:CallbackPanel ID="upSiteGrouping" runat="server" OnClientCallbackComplete="upSiteGrouping_OnCallbackComplete"
        OnClientRenderComplete="upSiteGrouping_OnRenderComplete" OnClientLoad="upSiteGrouping_OnLoad">
        <ContentTemplate>
            <div class="grid-section">
                <asp:ListView ID="lvSiteGrouping" runat="server" ItemPlaceholderID="phSiteGrouping">
                    <EmptyDataTemplate>
                        <div class="empty-collection-table">
                            No item(s) found.
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table">
                            <asp:PlaceHolder ID="phSiteGrouping" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class='<%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "row" : "alternate-row" %>'>
                            <div class="collection-row grid-item clear-fix" objectid='<%# Eval("Id") %>'>
                                <div class="first-cell">
                                    <%# Container.DataItemIndex + 1%>
                                </div>
                                <div class="middle-cell">
                                    <%# String.Format("{0} | {1}", Eval("Title"), Eval("Description"))%>
                                </div>
                                <div class="last-cell">
                                    <%# String.Format("<a href='#' objectId='{0}' class='edit-grid-item'>{1}</a> {3} <a href='#' objectId='{0}' class='delete-grid-item'>{2}</a>",
                                                                            Eval("Id"), 
                                                                            GUIStrings.Edit, 
                                                                            GUIStrings.Delete,
                                                                             "|") %>
                                </div>
                                <input type="hidden" class="hdnTitle" datafield="Title" value='<%# Eval("Title") %>' />
                                <input type="hidden" class="hdnDescription" datafield="Description" value='<%# Eval("Description") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <asp:ValidationSummary ID="valSummary" ValidationGroup="valAdd" runat="server" ShowSummary="false"
                ShowMessageBox="True" />
            <div class="form-section">
                <h3>Site Group Properties</h3>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="297" ClientIDMode="Static" />
                        <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true"
                            ValidationGroup="valAdd" />
                        <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                            ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                            ValidationExpression="^[^<>]+$" ValidationGroup="valAdd" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="297"
                            TextMode="MultiLine" ClientIDMode="Static" ValidationGroup="valAdd" />
                        <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                            SetFocusOnError="true" ValidationExpression="^[^<>]+$" ValidationGroup="valAdd" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="button-row">
                    <asp:Button ID="btnAdd" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Add %>"
                        ToolTip="<%$ Resources:GUIStrings, Add %>" OnClientClick="return AddSiteGrouping();"
                        ClientIDMode="Static" ValidationGroup="valAdd" />
                    <asp:Button ID="btnUpdate" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Update %>"
                        ToolTip="<%$ Resources:GUIStrings, Update %>" OnClientClick="return UpdateSiteGrouping();"
                        ClientIDMode="Static" ValidationGroup="valAdd" Style="display: none;" />
                </div>
            </div>
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Close %>" class="button cancel-button" />
</asp:Content>
