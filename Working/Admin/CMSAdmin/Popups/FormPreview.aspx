<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="FormPreview.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.FormPreview" Theme="General" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:Literal runat="server" ID="ltFormData"></asp:Literal>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnCancel" onclick="CanceliAppsAdminPopup();" runat="server"
        value="<%$ Resources:GUIStrings, Close %>" class="button" title="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
