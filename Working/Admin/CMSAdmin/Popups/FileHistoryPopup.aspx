﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.FileHistoryPopup"
    StylesheetTheme="General" CodeBehind="FileHistoryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="Library" Src="~/UserControls/Libraries/Data/ManageFileHistory.ascx" %>

<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:Library ID="fileHistory" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
