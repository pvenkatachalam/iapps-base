<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageBlogSettings" MasterPageFile="~/Popups/iAPPSPopup.Master"
    StylesheetTheme="General" CodeBehind="ManageBlogSettings.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function trvPageview_onNodeSelect(sender, eventArgs) {
            cmbPageView.set_text(eventArgs.get_node().get_text().replaceAll("&amp;", "&"));
            cmbPageView.collapse();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.Title %></label>
        <div class="form-value">
            <asp:TextBox ID="txtTitle" runat="server" Width="260" />
            <asp:RequiredFieldValidator ID="reqtxtTitle" runat="server" SetFocusOnError="true"
                Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterABlogTitle %>"
                ControlToValidate="txtTitle" />
            <iAppsControls:iAppsCustomValidator ID="regtxtTitle" ControlToValidate="txtTitle"
                ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheTitle %>"
                runat="server" ValidateType="Title" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ConnectBlogToTheFollowingMenuColon %>" /></label>
        <div class="form-value">
            <ComponentArt:ComboBox ID="cmbPageView" runat="server" DropDownHeight="320" DropDownWidth="315"
                Height="25" SkinID="Default" Width="270" TextBoxCssClass="combo-noneditable">
                <DropDownContent>
                    <ComponentArt:TreeView ID="trvPages" runat="server" SkinID="Default" Height="320"
                        Width="315">
                        <ClientEvents>
                            <NodeSelect EventHandler="trvPageview_onNodeSelect" />
                        </ClientEvents>
                        <ClientTemplates>
                            <ComponentArt:ClientTemplate ID="TreeNodeTemplate">
                                <div>
                                    ## DataItem.GetProperty('Text') ##
                                </div>
                            </ComponentArt:ClientTemplate>
                        </ClientTemplates>
                    </ComponentArt:TreeView>
                </DropDownContent>
            </ComponentArt:ComboBox>
            <asp:SiteMapDataSource ID="SiteMapDSMenu" runat="server" SiteMapProvider="FolderWebSiteMapProvider"
                ShowStartingNode="True" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.BlogListTemplate %></label>
        <div class="form-value">
            <ComponentArt:ComboBox ID="cmbBlogTemplate" runat="server" SkinID="Default" Width="270"
                Height="25" DropDownHeight="275" DropDownWidth="315" DataFields="VirtualPath,Image"
                ItemClientTemplateId="ctBlogTemplatePreview">
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="ctBlogTemplatePreview" runat="server">
                        ## showTemplateThumbnail(DataItem.VirtualPath, DataItem.Image, DataItem.Text) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:ComboBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.PostDetailTemplate %></label>
        <div class="form-value">
            <ComponentArt:ComboBox ID="cmbPostTemplate" runat="server" SkinID="Default" Width="270"
                Height="25" DropDownHeight="250" DropDownWidth="315" DataFields="VirtualPath,Image"
                ItemClientTemplateId="ctPostTemplatePreview">
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="ctPostTemplatePreview" runat="server">
                        ## showTemplateThumbnail(DataItem.VirtualPath, DataItem.Image, DataItem.Text) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:ComboBox>
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.ContentDefinition %></label>
        <div class="form-value">
            <asp:DropDownList ID="ddlContentDefinition" runat="server" Width="270" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, DisplaySettingsColon %>" /></label>
        <div class="form-value">
            <asp:CheckBox ID="chkShowPostDate" runat="server" Text="<%$ Resources:GUIStrings, ShowPostDate %>"
                CssClass="check-box" />
            <asp:CheckBox ID="chkShowAuthorName" runat="server" Text="<%$ Resources:GUIStrings, DisplayAuthorName %>"
                CssClass="check-box" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, CommentSettingsColon %>" /></label>
        <div class="form-value">
            <asp:CheckBox ID="chkRequiresCaptcha" runat="server" CssClass="check-box" Text="<%$ Resources:GUIStrings, UnauthenticatedUsersMustFillOutNameEmailAndSecurityFields %>" />
            <asp:CheckBox ID="chkEmailOwner" runat="server" CssClass="check-box" Text="<%$ Resources:GUIStrings, EmailBlogOwnerWheneverAnyonePostsAComment %>" />
            <asp:CheckBox ID="chkRequiresAdminApproval" runat="server" CssClass="check-box" Text="<%$ Resources:GUIStrings, AnAdministratorMustAlwaysApproveNewCommentsBeforeTheyAppearOnTheSite %>" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" class="button" value="<%= GUIStrings.Cancel %>" onclick="parent.CloseiAppsAdminPopup();" />
    <asp:Button ID="btnSave" CssClass="primarybutton show-loading" runat="server" OnClick="btnSave_Click"
        Text="<%$ Resources:GUIStrings, Save %>" />
</asp:Content>
