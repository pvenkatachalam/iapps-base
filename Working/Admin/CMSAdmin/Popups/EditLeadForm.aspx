<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="EditLeadForm.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.EditLeadFormPopup" ValidateRequest="false"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="uc" TagName="FormDetails" Src="~/UserControls/Libraries/Data/FormDetails.ascx" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <style type="text/css">
        .form-definition .form-fields .form-fields-box {
            margin-right: -5px;
            padding-right: 5px;
            height: 360px;
            overflow-y: scroll;
        }

        .form-definition li.available-field {
            display: block;
            margin: 0 0 5px 0;
            border-radius: 5px;
            padding: 5px 10px;
            width: 225px;
            background: #ececed url('/admin/App_Themes/General/images/icon-drag-drop.png') no-repeat 98% center;
            z-index: 10;
            cursor: pointer;
        }

        .form-definition .form-selected-fields .form-selected-fields-box {
            overflow-y: scroll;
        }

            .form-definition .form-selected-fields .form-selected-fields-box.active {
                outline: dashed 2px #cdfad0;
            }

        .form-definition .form-selected-fields li.form-field {
            display: table;
            margin: 0 0 5px 0;
            border-radius: 5px;
            padding: 5px 10px;
            width: 350px;
            background: #ececed;
            cursor: pointer;
        }

            .form-definition .form-selected-fields li.form-field:before {
                content: "";
                float: left;
                width: 28px;
                height: 31px;
                margin: 0 20px 0 0;
                background: url('/admin/App_Themes/General/images/icon-move-gray.png') no-repeat left center;
            }

        .form-definition .form-selected-fields li.selected {
            background: #b9b9b9;
        }

            .form-definition .form-selected-fields li.selected:before {
                background: #b9b9b9 url('/admin/App_Themes/General/images/icon-move-white.png') no-repeat left center;
            }

        .form-definition .form-selected-fields li.placeholder {
            height: 40px;
            border: dashed 2px #cdfad0;
            background: transparent;
        }

        .form-definition .form-selected-fields li.form-field * {
            cursor: pointer;
        }

        .form-definition .form-selected-fields li.form-field > div {
            display: table-cell;
            vertical-align: middle;
        }

            .form-definition .form-selected-fields li.form-field > div.row-label {
                width: 130px;
            }

            .form-definition .form-selected-fields li.form-field > div.row-control {
                padding: 0 10px;
            }

                .form-definition .form-selected-fields li.form-field > div.row-control input[type="text"],
                .form-definition .form-selected-fields li.form-field > div.row-control select,
                .form-definition .form-selected-fields li.form-field > div.row-control textarea {
                    display: block;
                    margin: 0 !important;
                    width: 105px;
                }

            .form-definition .form-selected-fields li.form-field > div.row-actions {
                width: 50px;
                text-align: right;
            }

        .form-definition .form-properties-box .control-properties {
            border: none;
            padding: 0;
        }

        .form-definition .form-field-properties .form-properties-box h3 {
            margin-left: 0;
        }
    </style>
    <script type="text/javascript">
        var FormBuilder = {
            GetProperty: function (id) {
                for (var i = 0; i < availableProperties.length; i++) {
                    var property = availableProperties[i];

                    if (property.Id === id)
                        return property;
                }
            },

            AddField: function (field, property, select) {
                var list = $(".form-selected-fields-box > ul");

                var html = '';

                html += '<li class="form-field">';
                html += '<div class="row-label">';
                html += '<label class="form-label colon"></label>';
                html += '</div>';
                html += '<div class="row-control">';
                html += '</div>';
                html += '<div class="row-actions">';

                if (field.Id !== "Email") {
                    html += '<a href="#" class="remove">';
                    html += '<img src="/admin/App_Themes/General/images/icon-delete-cross-dark.png" alt="delete" />';
                    html += '</a>';
                }

                html += '</div>';
                html += '</li>';

                var element = $(html);

                element.appendTo(list);
                element.data("property", property);
                element.data("field", field);

                this.UpdateField(field);

                if (select) {
                    element.addClass("selected");
                    element.siblings().removeClass("selected");

                    this.RenderFieldOptions(field);
                }
            },

            UpdateField: function (field) {
                var item = null;
                var property = this.GetProperty(field.Id);

                $("div.form-selected-fields-box > ul > li").each(function () {
                    if ($(this).data("field") === field) {
                        item = $(this);

                        return false;
                    }
                });

                if (item !== null) {
                    // Label
                    var label = field.Label;

                    if ($.trim(label).length === 0) {
                        if ($.trim(field.Title).length > 0)
                            label = field.Title;
                        else
                            label = property.Title;
                    }

                    if (field.Required)
                        label += '<span class="req">&nbsp;*</span>';

                    $("label", item).html(label);

                    // Control
                    var html = '';

                    switch (field.ControlType) {
                        case "CheckBox":
                            html += '<input type="checkbox">';
                            break;
                        case "CheckBoxList":
                            html += '<ul>';

                            if (field.Options) {
                                for (var i = 0; i < field.Options.length; i++) {
                                    html += '<li>';
                                    html += '<input type="checkbox" name="sample_' + field.Id + '" />' + field.Options[i];
                                    html += '</li>';
                                }
                            }

                            html += '</ul>';
                            break;
                        case "DropdownList":
                            html += '<select>';

                            if (field.Options) {
                                for (var i = 0; i < field.Options.length; i++) {
                                    html += '<option>' + field.Options[i] + '</option>';
                                }
                            }

                            html += '</select>';
                            break;
                        case "FileUpload":
                            html += '<input type="file">';
                            break;
                        case "RadioButtonList":
                            html += '<ul>';

                            if (field.Options) {
                                for (var i = 0; i < field.Options.length; i++) {
                                    html += '<li>';
                                    html += '<input type="radio" name="sample_' + field.Id + '" />' + field.Options[i];
                                    html += '</li>';
                                }
                            }

                            html += '</ul>';
                            break;
                        case "TextArea":
                            html += '<textarea';

                            if (field.Placeholder)
                                html += ' placeholder="' + field.Placeholder + '"';

                            html += '></textarea>';
                            break;
                        case "TextBox":
                        case "DateSelector":
                            html += '<input type="text"';

                            if (field.Placeholder)
                                html += ' placeholder="' + field.Placeholder + '"';

                            html += ' />';
                            break;
                    }

                    $("div.row-control", item).html(html);
                }
            },

            RenderFieldOptions: function (field) {
                var property = this.GetProperty(field.Id);

                var container = $("div.form-properties-box");
                container.children().remove();
                container.data("field", field);

                container.append('<h3>' + property.Title + '&nbsp; Properties</h3>');

                var properties = $('<div class="control-properties" />');
                container.append(properties);

                var txtTitle = addFormRow(__JSMessages.Title, "TextBox", "String", field.Title);

                txtTitle.on("blur", function () {
                    field.Title = txtTitle.val();

                    FormBuilder.UpdateField(field);
                });

                var txtLabel = addFormRow(__JSMessages.Label, "TextBox", "String", field.Label);

                txtLabel.on("blur", function () {
                    field.Label = txtLabel.val();

                    FormBuilder.UpdateField(field);
                });

                if (field.ControlType === "TextArea" || field.ControlType === "TextBox") {
                    var txtPlaceholder = addFormRow(__JSMessages.Placeholder, "TextBox", "String", field.Placeholder);

                    txtPlaceholder.on("blur", function () {
                        field.Placeholder = txtPlaceholder.val();

                        FormBuilder.UpdateField(field);
                    });
                }

                if (field.Id !== "Email") {
                    var cbRequired = addFormRow(__JSMessages.Required, "DropDownList", "Boolean", field.Required);

                    cbRequired.on("change", function () {
                        field.Required = cbRequired.val() === "true";

                        FormBuilder.UpdateField(field);
                    });
                }

                function addFormRow(label, controlType, dataType, value) {
                    var html = '';

                    html += '<div class="form-row">';
                    html += '<label class="form-label">' + label + '</label>';
                    html += '<div class="form-value"></div>';
                    html += '</div>';

                    var formRow = $(html);
                    var formValue = formRow.children("div.form-value");
                    var element = null;

                    switch (controlType) {
                        case "TextBox":
                            element = $('<input type="text" class="textBoxes" />');

                            if (value)
                                element.val(value);

                            break;
                        case "DropDownList":
                            element = new $('<select />');

                            if (dataType === "Boolean") {
                                element.append('<option value="true">' + 'True' + '</option>')
                                element.append('<option value="false">' + 'False' + '</option>')

                                element.val(value.toString());
                            }

                            break;
                    }

                    if (element != null)
                        formValue.append(element);

                    properties.append(formRow);

                    return element;
                }
            },

            RenderAvailableProperties: function () {
                var list = $("div.form-fields-box ul");
                list.children().remove();

                for (var i = 0; i < availableProperties.length; i++) {
                    var property = availableProperties[i];
                    var available = true;

                    var count = formFields.length;

                    for (var j = 0; j < formFields.length; j++) {
                        var field = formFields[j];

                        if (field.Id === property.Id) {
                            available = false;
                            break;
                        }
                    }

                    if (available) {
                        var html = '';

                        html += '<li class="available-field">';
                        html += property.Title;
                        html += '</li>';

                        var element = $(html);

                        element.appendTo(list);
                        element.data("property", property);
                    }
                }

                $("div.form-fields-box li").draggable({
                    helper: "clone",
                    revert: true
                });
            },

            RenderFormFields: function () {
                var list = $("div.form-fields-selected-box ul");
                list.children().remove();

                var emailExists = false;

                for (var i = 0; i < formFields.length; i++) {
                    var field = formFields[i];

                    if (field.Id === "Email") {
                        emailExists = true;
                        break;
                    }
                }

                if (!emailExists) {
                    var property = this.GetProperty("Email");
                    var field = this.GenerateField(property);

                    field.Required = true;

                    formFields.push(field);
                }

                for (var i = 0; i < formFields.length; i++) {
                    var field = formFields[i];
                    var property = this.GetProperty(field.Id);

                    this.AddField(field, property, i === 0);
                }

                $("div.form-selected-fields-box > ul").sortable({
                    placeholder: "placeholder",
                    appendTo: ".form-definition .form-selected-fields",
                    helper: "clone",
                    start: function (event, ui) {
                        $(":focus").blur();

                        ui.item.trigger("select");
                        ui.helper.addClass("sorting");
                    },
                    update: function (event, ui) {
                        FormBuilder.UpdateFieldsList();
                    }
                });

                $(document).on("click", "div.form-selected-fields-box > ul > li", function () {
                    $(":focus").blur();

                    $(this).trigger("select");
                });

                $(document).on("select", "div.form-selected-fields-box > ul > li", function () {
                    $(this).addClass("selected");
                    $(this).siblings().removeClass("selected");

                    var field = $(this).data("field");

                    FormBuilder.RenderFieldOptions(field);
                });

                $(document).on("click", "div.form-selected-fields-box a.remove", function (event) {
                    event.preventDefault();

                    var element = $(this).parents("li:first");
                    var selectFirst = element.hasClass("selected");

                    element.remove();

                    FormBuilder.UpdateFieldsList();
                    FormBuilder.RenderAvailableProperties();

                    $("div.form-selected-fields-box > ul > li:first").trigger("select");
                });
            },

            GenerateField: function (property) {
                return {
                    Type: property.Type,
                    Id: property.Id,
                    DataType: property.DataType,
                    ControlType: property.ControlType,
                    Title: property.Title,
                    Label: property.Title,
                    Options: property.Options,
                    Required: false
                };
            },

            UpdateFieldsList: function () {
                formFields = [];

                $("div.form-selected-fields-box > ul > li").each(function () {
                    var field = $(this).data("field");

                    formFields.push(field);
                });
            },

            Initialize: function () {
                this.RenderFormFields();
                this.RenderAvailableProperties();

                $(".form-selected-fields-box").droppable({
                    accept: "div.form-fields-box li",
                    activeClass: "active",
                    drop: function (event, ui) {
                        var property = ui.draggable.data("property");
                        var field = FormBuilder.GenerateField(property);

                        formFields.push(field);

                        FormBuilder.AddField(field, property, true);
                        FormBuilder.RenderAvailableProperties();
                    }
                });

                $("#<%= btnSave.ClientID %>").on("click", function () {
                    $("#<%= hdnFormFields.ClientID %>").val(JSON.stringify(formFields));
                });
            }
        };

        $(document).ready(function () {
            FormBuilder.Initialize();

            $(".tabs").iAppsTabs();
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="row clear-fix">
        <div class="tabs form-options">
            <ul>
                <li><span><%= GUIStrings.FormDetails %></span></li>
                <li><span><%= GUIStrings.FormFields %></span></li>
            </ul>
            <div class="tab1 form-details">
                <div class="scroll-box">
                    <uc:FormDetails ID="ucFormDetails" runat="server" />
                </div>
            </div>
            <div class="tabs2 form-fields">
                <p><strong><%= GUIStrings.DontSeeAnAttributeThatYouWantAsAForField %></strong> <a href="javascript: //" onclick="parent.RedirectWithToken('marketier','/marketieradmin/Administration/Attributes/ManageAttributes.aspx');"><%= GUIStrings.ClickToManageOrAddContactAttributesInMarketier %></a></p>
                <div class="form-fields-box">
                    <ul>
                    </ul>
                </div>
            </div>
        </div>
        <div class="form-selected-fields">
            <h3>
                <%= GUIStrings.YourForm %>
            </h3>
            <div class="form-selected-fields-box">
                <ul>
                </ul>
            </div>
        </div>
        <div class="form-field-properties">
            <div class="form-properties-box">
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnFormFields" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:HiddenField ID="hdnFormType" runat="server" ClientIDMode="Static" />
    <asp:Button ID="btnCancel" Text="<%$ Resources:GUIStrings, Close %>" runat="server" UseSubmitBehavior="false"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" Text="<%$ Resources:GUIStrings, Save %>" runat="server"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" />
</asp:Content>
