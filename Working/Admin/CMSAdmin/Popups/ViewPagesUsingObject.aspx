<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="ViewPagesUsingObject.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewPagesUsingObject" StylesheetTheme="General" %>

<%@ Register TagPrefix="UC" TagName="ViewPagesByObject" Src="~/UserControls/Popup/ViewPagesByObject.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <UC:ViewPagesByObject ID="viewPagesByObject" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button " ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup();"
        UseSubmitBehavior="false" CausesValidation="false" />
    <asp:Button ID="btnProceed" runat="server" Text="<%$ Resources:GUIStrings, Delete %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Delete %>" OnClientClick="return SaveiAppsAdminPopup();"
        UseSubmitBehavior="false" CausesValidation="false" />
</asp:Content>
