<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Popups/iAPPSPopup.Master"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewWorkflowPopup" StylesheetTheme="General" CodeBehind="ViewWorkflowPopup.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <h3><asp:Literal ID="litWorkflow" runat="server" /></h3>
    <ComponentArt:Grid ID="grdViewWorklow" runat="server" RunningMode="Client" SkinID="Default"
        ShowFooter="false" Height="200" PageSize="100" AllowVerticalScrolling="true"
        EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
        <Levels>
            <ComponentArt:GridLevel DataKeyField="RoleId" ShowTableHeading="false" ShowSelectorCells="false"
                RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="data-cell"
                HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row" SelectedRowCssClass="selected-row"
                AlternatingRowCssClass="alternate-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                SortImageWidth="8" SortImageHeight="7" AllowGrouping="false">
                <Columns>
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, UserGroupRoleName %>"
                        DataField="ActorName" Width="260" DataCellCssClass="first-cell" />
                    <ComponentArt:GridColumn HeadingText="<%$ Resources:GUIStrings, RoleName %>"
                        DataField="RoleName" Width="180" DataCellCssClass="last-cell" />
                    <ComponentArt:GridColumn DataField="ActorId" Visible="false" />
                    <ComponentArt:GridColumn DataField="ActorType" Visible="false" />
                    <ComponentArt:GridColumn DataField="RoleId" Visible="false" />
                </Columns>
            </ComponentArt:GridLevel>
        </Levels>
    </ComponentArt:Grid>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" OnClientClick="return CanceliAppsAdminPopup();" ToolTip="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
