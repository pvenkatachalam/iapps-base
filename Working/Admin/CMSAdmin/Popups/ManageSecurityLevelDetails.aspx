﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="ManageSecurityLevelDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageSecurityLevelDetails"
    Theme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <script language="javascript" type="text/javascript">
        function upSecurityLevel_OnLoad(sender, eventArgs) {
            $(".security-levels").gridActions({ objList: $(".collection-table"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upSecurityLevel_Callback(sender, eventArgs);
                }
            });
        }

        function upSecurityLevel_OnRenderComplete(sender, eventArgs) {
            $(".security-levels").gridActions("bindControls");
        }

        function upSecurityLevel_OnCallbackComplete(sender, eventArgs) {
            $(".security-levels").gridActions("displayMessage");

            $("#txtTitle").val("");
            $("#txtDescription").val("");
        }

        function upSecurityLevel_Callback(sender, eventArgs) {
            var doCallback = true;

            switch (gridActionsJson.Action) {
                case "Delete":
                    doCallback = window.confirm("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, DeleteConfirmation %>' />");
                    break;
                case "Edit":
                    doCallback = false;
                    $("#txtTitle").val(eventArgs.selectedItem.getValue("Title"));
                    $("#txtDescription").val(eventArgs.selectedItem.getValue("Description"));
                    $("#btnAdd").hide();
                    $("#btnUpdate").show();
                    break;
            }

            if (doCallback) {
                if (gridActionsJson.SelectedItems.length > 0) {
                    gridActionsJson.CustomAttributes["SelectedItem"] = gridActionsJson.SelectedItems[0];
                    gridActionsJson.SelectedItems = [];
                }

                upSecurityLevel.set_callbackParameter(JSON.stringify(gridActionsJson));
                upSecurityLevel.callback();
            }
        }

        function AddSecurityLevel() {
            if (Page_ClientValidate() == true) {
                gridActionsJson.CustomAttributes["Title"] = $("#txtTitle").val();
                gridActionsJson.CustomAttributes["Description"] = $("#txtDescription").val();

                $(".security-levels").gridActions("callback", "Add");
            }
            return false;
        }

        function UpdateSecurityLevel() {
            if (Page_ClientValidate() == true) {
                gridActionsJson.CustomAttributes["Title"] = $("#txtTitle").val();
                gridActionsJson.CustomAttributes["Description"] = $("#txtDescription").val();

                $(".security-levels").gridActions("callback", "Update");
            }
            return false;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:CallbackPanel ID="upSecurityLevel" runat="server" OnClientCallbackComplete="upSecurityLevel_OnCallbackComplete"
        OnClientRenderComplete="upSecurityLevel_OnRenderComplete" OnClientLoad="upSecurityLevel_OnLoad">
        <ContentTemplate>
            <div class="grid-section">
                <asp:ListView ID="lvSecurityLevel" runat="server" ItemPlaceholderID="phSecurityLevel">
                    <EmptyDataTemplate>
                        <div class="empty-collection-table">
                            No security levels found!
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <div class="collection-table">
                            <asp:PlaceHolder ID="phSecurityLevel" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class='<%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "row" : "alternate-row" %>'>
                            <div class="collection-row grid-item clear-fix" objectid='<%# Eval("Id") %>'>
                                <div class="first-cell">
                                    <%# Container.DataItemIndex + 1%>
                                </div>
                                <div class="middle-cell">
                                    <%# String.Format("{0} | {1}", Eval("Title"), Eval("Description"))%>
                                </div>
                                <div class="last-cell">
                                    <%# String.Format("<a href='#' objectId='{0}' class='edit-grid-item'>{1}</a> {3} <a href='#' objectId='{0}' class='delete-grid-item'>{2}</a>",
                                                                            Eval("Id"), 
                                                                            GUIStrings.Edit, 
                                                                            Eval("IsSystem").ToString().Equals(Boolean.TrueString) ? "" : GUIStrings.Delete,
                                                                            Eval("IsSystem").ToString().Equals(Boolean.TrueString) ? "" : "|") %>
                                </div>
                                <input type="hidden" class="hdnTitle" dataField="Title" value='<%# Eval("Title") %>' />
                                <input type="hidden" class="hdnDescription" dataField="Description" value='<%# Eval("Description") %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <asp:ValidationSummary ID="valSummary" ValidationGroup="valAdd" runat="server" ShowSummary="false"
                ShowMessageBox="True" />
            <div class="form-section">
                <h3>
                    Security Level Properties</h3>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span class="req">&nbsp;*</span>
                    </label>
                    <div class="form-value">
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="297" ClientIDMode="Static" />
                        <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                            ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true"
                            ValidationGroup="valAdd" />
                        <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                            ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                            ValidationExpression="^[^<>]+$" ValidationGroup="valAdd" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Description %>" /></label>
                    <div class="form-value">
                        <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="297"
                            TextMode="MultiLine" ClientIDMode="Static" ValidationGroup="valAdd" />
                        <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                            ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                            SetFocusOnError="true" ValidationExpression="^[^<>]+$" ValidationGroup="valAdd" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="button-row">
                    <asp:Button ID="btnAdd" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Add %>"
                        ToolTip="<%$ Resources:GUIStrings, Add %>" OnClientClick="return AddSecurityLevel();"
                        ClientIDMode="Static" ValidationGroup="valAdd" />
                    <asp:Button ID="btnUpdate" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Update %>"
                        ToolTip="<%$ Resources:GUIStrings, Update %>" OnClientClick="return UpdateSecurityLevel();"
                        ClientIDMode="Static" ValidationGroup="valAdd" Style="display:none;" />
                </div>
            </div>
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup(true);" />
</asp:Content>
