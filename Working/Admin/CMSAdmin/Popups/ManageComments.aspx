<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageComments" StylesheetTheme="General"
    CodeBehind="ManageComments.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="uc" TagName="Comments" Src="~/UserControls/Libraries/Data/CommentsGrid.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <uc:Comments ID="blogComments" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
<asp:Button ID="cancelBtn" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        cssclass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup(true);" />
</asp:Content>
