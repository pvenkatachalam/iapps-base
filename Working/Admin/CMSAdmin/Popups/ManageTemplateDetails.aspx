﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="ManageTemplateDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageTemplateDetails"
    Theme="General" ClientIDMode="Static" %>

<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#fileTemplate").iAppsFileUpload({ allowedExtensions: "ascx" });
            $("#fileCodeBehind").iAppsFileUpload({ allowedExtensions: "cs" });
            $("#fileImage").iAppsFileUpload({ allowedExtensions: "gif,jpeg,jpg,png" });

            var templateJson = parent.templateJson;
            if (typeof templateJson != "undefined") {
                $("#txtTitle").val(templateJson.Title);
                $("#txtDescription").val(templateJson.Description);
                $("#ltTemplateImage").html(templateJson.ImageURL);

                $.each(parent.templateJson.CSS, function () {
                    $("#chkStyles #" + this.toString() + " input").prop("checked", true);
                });

                $.each(parent.templateJson.Script, function () {
                    $("#chkScripts #" + this.toString() + " input").prop("checked", true);
                });
            }
        });

        function SaveManageTemplateDetails() {
            var isValid = Page_ClientValidate();
            if (isValid) {
                if (typeof parent.templateJson != "undefined") {
                    parent.templateJson.Title = $("#txtTitle").val();
                    parent.templateJson.Description = $("#txtDescription").val();
                    if ($("#fileImage").val() != "")
                        parent.templateJson.ImageURL = $("#fileImage").val();

                    parent.templateJson.CSS.splice(0, parent.templateJson.CSS.length);
                    $("#chkStyles input:checked").each(function () {
                        parent.templateJson.CSS.push($(this).parent().attr("id"));
                    });

                    parent.templateJson.Script.splice(0, parent.templateJson.Script.length);
                    $("#chkScripts input:checked").each(function () {
                        parent.templateJson.Script.push($(this).parent().attr("id"));
                    });

                    if ($(".tree-container").length > 0) {
                        parent.templateJson.ParentId = treeActionsJson.FolderId;
                        $("#hdnParentId").val(treeActionsJson.FolderId);
                    }
                }
                ShowiAppsLoadingPanel();
            }
            return isValid;
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-section">
        <div class="columns left-column">
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Title%><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="260" />
                    <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
                    <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                        ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Description %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="260"
                        TextMode="MultiLine" />
                    <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                        ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                        SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
                <div class="clear-fix">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Image%></label>
                <div class="form-value">
                    <asp:FileUpload ID="fileImage" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                        CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" Width="280" />
                    <asp:Label ID="ltTemplateImage" runat="server" CssClass="coaching-text" />
                </div>
            </div>
            <asp:PlaceHolder ID="phDirectory" runat="server">
                <UC:LibraryTree ID="libraryTree" runat="server" TreeHeight="300" />
                <asp:HiddenField ID="hdnParentId" runat="server" ClientIDMode="Static" />
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phAdminOnly" runat="server">
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.Template%><span class="req">&nbsp;*</span></label>
                    <div class="form-value">
                        <asp:FileUpload ID="fileTemplate" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                            CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" Width="260" />
                        <asp:Label ID="ltTemplateFileName" runat="server" CssClass="coaching-text" />
                        <asp:RequiredFieldValidator ID="reqfileTemplate" ControlToValidate="fileTemplate"
                            runat="server" ErrorMessage="<%$ Resources:JSMessages, PleaseSelectATemplate %>"
                            Display="None" SetFocusOnError="true" />
                    </div>
                </div>
                <asp:PlaceHolder ID="phTemplateOnly" runat="server">
                    <div class="form-row">
                        <label class="form-label">
                            <%= GUIStrings.CodeBehind%></label>
                        <div class="form-value">
                            <asp:FileUpload ID="fileCodeBehind" ToolTip="<%$ Resources:GUIStrings, Browse %>"
                                runat="server" CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;"
                                Width="280" />
                            <asp:Label ID="ltCodeBehindFile" runat="server" CssClass="coaching-text" />
                        </div>
                    </div>

                    <asp:PlaceHolder ID="phTheme" runat="server" Visible="false">
                        <%--For Theme section--%>
                        <div class="form-row">
                            <label class="form-label" id="lblThemes" runat="server">
                                <%= GUIStrings.Theme%></label>
                            <div class="form-value">
                                <asp:DropDownList ID="ddlThemes" Enabled="false" OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged" AutoPostBack="true" runat="server" ToolTip="<%$ Resources:GUIStrings, ThemeToolTip %>">
                                    <asp:ListItem Text="Default" Value="00000000-0000-0000-0000-000000000000" />
                                </asp:DropDownList>
                                <%--<asp:Label ID="Label1" runat="server" CssClass="coaching-text" />--%>
                            </div>
                        </div>
                    </asp:PlaceHolder>
                    <div class="form-row">
                        <label class="form-label">
                            &nbsp;</label>
                        <div class="form-value check-box">
                            <asp:CheckBox ID="chkPagePart" runat="server" Text="<%$ Resources:GUIStrings, PagePart %>" />
                        </div>
                    </div>
                    <div class="form-row">
                        <label class="form-label">
                            &nbsp;</label>
                        <div class="form-value check-box">
                            <asp:CheckBox ID="chkIsSharedWithVariant" runat="server" Text="<%$ Resources:GUIStrings, IsSharedTemplateWithVariants %>" />
                            <%--Text="<%$ Resources:GUIStrings, IsSharedTemplateWithVariants %>"--%>
                        </div>
                    </div>
                </asp:PlaceHolder>
            </asp:PlaceHolder>
        </div>
        <asp:Panel ID="pnlTemplateStyleAndScripts" runat="server">
            <div class="columns right-column">
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.TemplateStyles%></label>
                    <div class="form-value checkbox-list">
                        <asp:CheckBoxList ID="chkStyles" runat="server" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
                <div class="form-row">
                    <label class="form-label">
                        <%= GUIStrings.TemplateScripts%></label>
                    <div class="form-value checkbox-list">
                        <asp:CheckBoxList ID="chkScripts" runat="server" />
                    </div>
                    <div class="clear-fix">
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CausesValidation="false" CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SaveManageTemplateDetails();"
        OnClick="btnSave_Click" />
</asp:Content>
