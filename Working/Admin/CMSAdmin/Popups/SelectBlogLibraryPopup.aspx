<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectBlogLibraryPopup" StylesheetTheme="General"
    CodeBehind="SelectBlogLibraryPopup.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register TagPrefix="UC" TagName="BlogLibrary" Src="~/UserControls/Libraries/Data/ManageBlog.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            setTimeout(function () {
                $("#content_library").iAppsSplitter();
            }, 200);
        });
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <UC:BlogLibrary ID="blogLibrary" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectBlog%>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectContent %>" OnClientClick="return SelectPostFromLibrary()" />
</asp:Content>
