﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SiteBackupHistory"
    StylesheetTheme="General" CodeBehind="SiteBackupHistory.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdBackupHistory_OnLoad(sender, eventArgs) {
            $("#backup_history").gridActions({ objList: grdBackupHistory });
        }

        function grdBackupHistory_OnRenderComplete(sender, eventArgs) {
            $("#backup_history").gridActions("bindControls");
        }

        function grdBackupHistory_OnCallbackComplete(sender, eventArgs) {
            $("#backup_history").gridActions("displayMessage");
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div id="backup_history">
        <div class="grid-section">
            <ComponentArt:Grid ID="grdBackupHistory" SkinID="Default" runat="server" Width="100%" RunningMode="Client"
                ShowFooter="false" LoadingPanelClientTemplateId="grdBackupHistoryLoadingPanelTemplate" CssClass="fat-grid">
                <ClientEvents>
                    <Load EventHandler="grdBackupHistory_OnLoad" />
                    <CallbackComplete EventHandler="grdBackupHistory_OnCallbackComplete" />
                    <RenderComplete EventHandler="grdBackupHistory_OnRenderComplete" />
                </ClientEvents>
                <Levels>
                    <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                        RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                        SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                        SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                        HoverRowCssClass="hover-row">
                        <Columns>
                            <ComponentArt:GridColumn DataField="BackupDate" DataCellServerTemplateId="PopupTemplate" />
                            <ComponentArt:GridColumn DataField="Comments" Visible="false" IsSearchable="false" />
                            <ComponentArt:GridColumn DataField="Id" Visible="false" IsSearchable="false" />
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ServerTemplates>
                    <ComponentArt:GridServerTemplate ID="PopupTemplate">
                        <Template>
                            <div class="modal-grid-row grid-item clear-fix">
                                <div class="first-cell">
                                    <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.BackupDate, Container.DataItem["BackupDate"])%>
                                </div>
                                <div class="last-cell">
                                    <%# Container.DataItem["Comments"] != string.Empty ? string.Format("<strong>{0}</strong> : {1}", GUIStrings.Comments, Container.DataItem["Comments"]) : string.Empty%>
                                </div>
                            </div>
                        </Template>
                    </ComponentArt:GridServerTemplate>
                </ServerTemplates>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="grdBackupHistoryLoadingPanelTemplate">
                        ## GetGridLoadingPanelContent(grdBackupHistory) ##
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
