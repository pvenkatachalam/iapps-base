﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubmitForTranslation.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SubmitForTranslation" MasterPageFile="~/Popups/iAPPSPopup.master"
    StylesheetTheme="General" %>

<%@ Register TagPrefix="tf" TagName="Translation" Src="~/UserControls/Administration/Translation/TranslationForm.ascx" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="vsSubmitForTranslation" runat="server" ShowMessageBox="true"
        ShowSummary="false" EnableClientScript="true" />
    <tf:Translation id="ctlTranslation" runat="server" />
</asp:Content>

<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
        <asp:Button ID="btnSaveForLater" runat="server" Text="<%$ Resources:GUIStrings, SaveForLater %>"
        ToolTip="<%$ Resources:GUIStrings, SaveForLater %>" OnclientClick="return ValidateOnSubmit()" CommandArgument="SaveForLater" OnClick="SubmitBtn_Click"
        CssClass="primarybutton" />
    <asp:Button ID="submitBtn" runat="server" Text="<%$ Resources:GUIStrings, SubmitNow %>"
        ToolTip="<%$ Resources:GUIStrings, SubmitNow %>" OnclientClick="return ValidateOnSubmit()" CommandArgument="SubmitNow" OnClick="SubmitBtn_Click"
        CssClass="primarybutton" />
</asp:Content>
