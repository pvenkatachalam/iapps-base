﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="ReimportFromMaster.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ReimportFromMaster" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ReImportFromMaster() {
            var checkedValue = $(".rblListClass input:checked").val();
            if (checkedValue == "1") {
                popupActionsJson.CustomAttributes.OverWritePage = "true";
                popupActionsJson.CustomAttributes.GetPageDefinitionViews = "false";
            }
            else if (checkedValue == "2") {
                popupActionsJson.CustomAttributes.OverWritePage = "false";
                popupActionsJson.CustomAttributes.GetPageDefinitionViews = "true";

            }
            else {
                popupActionsJson.CustomAttributes.OverWritePage = "true";
                popupActionsJson.CustomAttributes.GetPageDefinitionViews = "true";
            }
            SelectiAppsAdminPopup();

        }

        function ReImportAndSumitForTranslation() {
            popupActionsJson.CustomAttributes.SubmitToTranslation = "true";

            ReImportFromMaster();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <asp:Literal ID="ltWarningText" runat="server" Text="<%$ Resources:GUIStrings, ReimportingWarning %>" />
    </p>
    <div class="form-row" id="divrdblReimportOptions" style="display: none;">
        <asp:RadioButtonList ID="rdblReimportOptions" runat="server" CssClass="rblListClass">
            <asp:ListItem Text="<%$ Resources:GUIStrings, ReimportIncludePageAndVariantions %>"
                Value="0" Selected="True" />
            <asp:ListItem Text="<%$ Resources:GUIStrings, ReimportPageOnly %>" Value="1" />
            <asp:ListItem Text="<%$ Resources:GUIStrings, ReimportOnlyAudienceSegmentsAndDevice %>"
                Value="2" />
        </asp:RadioButtonList>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" />
    <asp:Button ID="btnReimportAndTranslate" runat="server" CssClass="primarybutton"
        Text="<%$ Resources:GUIStrings, ReimportAndTranslate %>" OnClientClick="return ReImportAndSumitForTranslation()" />
    <asp:Button ID="btnReimport" runat="server" CssClass="primarybutton" Text="<%$ Resources:GUIStrings, Reimport %>"
        OnClientClick="return ReImportFromMaster()" />
</asp:Content>
