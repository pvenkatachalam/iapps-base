<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManageContentDefinitionDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageContentDefinitionDetails"
    Theme="General" ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#fileXML").iAppsFileUpload({ textboxWidth: "140px", allowedExtensions: "xml", checkFileName: false });
            $("#fileXslt").iAppsFileUpload({ textboxWidth: "140px", allowedExtensions: "xsl,xslt", checkFileName: false });
        });
        function RemoveUploadedXSLT() {
            $('#ltXSLTFileName').text('');
            $('#hdnIsRemoved').val('removed');
            $('#hplRemoveXslt').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <asp:PlaceHolder ID="plCanNotEditOtherVariantOrMaster" runat="server" Visible="false">
        <p>
            <em><%= GUIStrings.CDUploadedFromAnotherVariantSiteOrMaster%></em>
        </p>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plCanNotEditOtherVariant" runat="server" Visible="false">
        <p>
            <em><%= GUIStrings.CDUploadedFromVariantSite%></em>
        </p>
    </asp:PlaceHolder>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Title %><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="300" />
            <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
            <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.Description %></label>
        <div class="form-value">
            <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="300"
                TextMode="MultiLine" />
            <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.XMLFile%><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:FileUpload ID="fileXML" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" />
            <asp:Label ID="ltXMLFileName" runat="server" CssClass="coaching-text" />
            <asp:RequiredFieldValidator ID="reqfileXML" ControlToValidate="fileXML" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, XMLFileNameIsRequired %>" Display="None"
                SetFocusOnError="true" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label"><%= GUIStrings.XSLTFile%><span class="req">&nbsp;*</span></label>
        <div class="form-value">
            <asp:FileUpload ID="fileXslt" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" />
            <asp:Label ID="ltXSLTFileName" runat="server" CssClass="coaching-text" />
            <asp:HiddenField ID="hdnXsltId" runat="server" />
            <asp:HiddenField ID="hdnIsRemoved" runat="server" />
            <a href="javascript: RemoveUploadedXSLT();" id="hplRemoveXslt" runat="server">
                <%= GUIStrings.RemoveXslt%></a>
            <asp:RequiredFieldValidator ID="reqfileXSLT" ControlToValidate="fileXslt" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, XSLTFileNameIsRequired %>" Display="None"
                SetFocusOnError="true" />
        </div>
    </div>
    <asp:PlaceHolder ID="phTheme" runat="server">
        <div class="form-row">
            <label class="form-label"><%= GUIStrings.Theme %></label>
            <div class="form-value">
                <asp:DropDownList ID="ddlThemes" Enabled="false" runat="server" OnSelectedIndexChanged="ddlThemes_SelectedIndexChanged" AutoPostBack="true" ToolTip="<%$ Resources:GUIStrings, ThemeToolTip %>">
                </asp:DropDownList>
            </div>
        </div>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="plMicrosSite" runat="server">
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.MasterSiteXSLTFile%></label>
            <div class="form-value text-value">
                <asp:Literal ID="ltMasterXSLTFileName" runat="server" />
            </div>
        </div>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
</asp:Content>
