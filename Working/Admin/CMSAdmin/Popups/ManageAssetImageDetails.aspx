﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="ManageAssetImageDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageAssetImageDetails"  ValidateRequest="false"
    Theme="General" ClientIDMode="Static"  %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SaveManageAssetImageDetails() {
            if (Page_ClientValidate() == true) {
                var oldFileName = $("#ltAssetImageName").html();
                var newFileName = $("#imageAsset").val();
                if (newFileName.indexOf("\\") > 0)
                    newFileName = newFileName.substr(newFileName.lastIndexOf("\\") + 1);

                if (newFileName != "")
                    newFileName = ReplaceSpecialCharactersInFileName(newFileName);
                if (oldFileName != "" && newFileName != "" &&
                    oldFileName.toLowerCase() != newFileName.toLowerCase()) {
                    alert("<%= JSMessages.ImageUploadNameMisMatch %>");
                    return false;
                }

                ShowiAppsLoadingPanel();
                return true;
            }

            return false;
        }        
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-section">
        <div class="columns">
            <h3><%= GUIStrings.ImageProperties %></h3>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.FileName%></label>
                <div class="form-value">
                    <asp:FileUpload ID="imageAsset" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                        CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" Width="290" />
                    <asp:Label ID="ltAssetImageName" runat="server" CssClass="coaching-text" />
                    <asp:RequiredFieldValidator ID="reqimageAsset" ControlToValidate="imageAsset" runat="server"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseSelectAFile %>" Display="None"
                        SetFocusOnError="true" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.FileTitle%><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="280" />
                    <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
                    <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                        ValidationExpression="^[^<>]+$" />
                </div>
                <div class="clear-fix">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Description %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="280"
                        TextMode="MultiLine" />
                    <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                        ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                        SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.ToolTip%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTooltip" runat="server" CssClass="textBoxes" Width="280" />
                    <asp:RegularExpressionValidator ID="regtxtTooltip" runat="server" ControlToValidate="txtTooltip"
                        ErrorMessage="<%$ Resources:JSMessages, TooltipIsNotValid %>" Display="None"
                        SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Keywords%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtKeyword" runat="server" CssClass="textBoxes" Width="280" TextMode="MultiLine" />
                    <asp:RegularExpressionValidator ID="regtxtkeyword" runat="server" ControlToValidate="txtKeyword"
                        ErrorMessage="<%$ Resources:GUIStrings, KeywordIsNotValid %>" Display="None"
                        SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
        </div>
        <div class="columns right-column">
            <h3><%= GUIStrings.Tags %></h3>
            <div class="form-row">
                <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="280" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>"  CausesValidation="false" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" CausesValidation="false"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SaveManageAssetImageDetails();" />
</asp:Content>
