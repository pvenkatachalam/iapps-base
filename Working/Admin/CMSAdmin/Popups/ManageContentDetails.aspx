<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManageContentDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageContentDetails" StylesheetTheme="General"
    ClientIDMode="AutoID" ValidateRequest="false" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<%@ Register TagPrefix="UC" TagName="HTMLEditor" Src="~/UserControls/Editor/HTMLEditor.ascx" %>
<%@ Register TagPrefix="UC" TagName="ContentDefinitionEditor" Src="~/UserControls/Libraries/Data/ContentDefinitionEditor.ascx" %>
<%@ Register TagPrefix="UC" TagName="ViewPagesByObject" Src="~/UserControls/Popup/ViewPagesByObject.ascx" %>
<%@ Register TagPrefix="UC" TagName="Tree" Src="~/UserControls/Libraries/DirectoryTree.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        var pageId;
        function SaveSelectTarget() {
            var title = popupActionsJson.CustomAttributes["ContentTitle"];
            var nodeId = popupActionsJson.FolderId;
            popupActionsJson = GetCDPopupActionsJson();
            popupActionsJson.CustomAttributes["ContentTitle"] = title;
            popupActionsJson.FolderId = nodeId;
            CanceliAppsAdminPopup(true);
            if (isFromSiteEditor) parent.Data.HiddenIsMasterContent.value = "False";
        }

        function OpenSelectTargetDirectory() {
            if (typeof Page_ClientValidate === "undefined" || Page_ClientValidate() == true) {
                OpeniAppsAdminPopup("SelectTargetDirectory",
                    "IsMasterContent=" + parent.Data.HiddenIsMasterContent.value + "&ContentId=" + parent.Data.HiddenFwObjectId.value + "&VariantContentExists=" + parent.Data.VariantContentExists,
                    "SaveSelectTarget", null, 800);
                return true;
            }
            return false;
        }

        function SaveManageContentDetails() {
            if (typeof Page_ClientValidate === "undefined" || Page_ClientValidate() == true) {
                if (!isFromSiteEditor) {
                    if (gridActionsJson.RecordCount > 0) {
                        $("#pagesUsingContent").iAppsDialog("open");
                        return false;
                    }
                }

                if (isForceSaveContent) {
                    OpeniAppsAdminPopup("SelectTargetDirectory", "HideTitle=true", "CloseForceSavePopup");
                    return false;
                }

                ShowiAppsLoadingPanel();
                return true;
            }
            return false;
        }

        function CloseForceSavePopup() {
            $("#hdnNodeId").val(popupActionsJson.FolderId);

            ShowiAppsLoadingPanel();
            ContentDetails.SaveData();
        }

        function selectPage() {
            if (gridActionsJson.SelectedItems.length == 0) {
                alert("<%= JSMessages.SelectPageToSaveContent %>");
                return false;
            }

            $("#hdnPageId").val(gridActionsJson.SelectedItems.first());
            ShowiAppsLoadingPanel();
            return true;
        }

        function PublishPage() {
            if (gridActionsJson.SelectedItems.length == 0) {
                alert("<%= JSMessages.SelectPageToSaveContent %>");
                return false;
            }

            $("#hdnPageId").val(gridActionsJson.SelectedItems.first());

            var isPublisher = IsPublisherByPageId(gridActionsJson.SelectedItems.first());
            if (isPublisher.toLowerCase() == "false") {
                alert("<%= JSMessages.YouDoNotHavePermissionToPublishThisPage %>");
                return false;
            }

            ShowiAppsLoadingPanel();
            return true;
        }

        function SubmitToWorkflow() {
            if (gridActionsJson.SelectedItems.length == 0) {
                alert("<%= JSMessages.SelectPageToSaveContent %>");
                return false;
            }
            $("#hdnPageId").val(gridActionsJson.SelectedItems.first());

            var workFlowAvailable = IsWorkFlowAvailable(gridActionsJson.SelectedItems.first());
            if (workFlowAvailable.toLowerCase() == "false") {
                alert("<%= JSMessages.NoWorkflowAvailableForTheSelectedPage %>");
                return false;
            }
            ShowiAppsLoadingPanel();
            return true;
        }

        function CloseSubmitIntoWorkflow() {
            $("#pagesUsingContent").iAppsDialog("close");

            return false;
        }

        $(function () {
            var title = $.getCookie("iApps_Content_Title");
            var description = $.getCookie("iApps_Content_Description");
            var tags = $.getCookie("iApps_Content_Tags");

            $.setCookie("iApps_Content_Title", "", -7);
            $.setCookie("iApps_Content_Description", "", -7);
            $.setCookie("iApps_Content_Tags", "", -7);

            if (title) $("#<%= txtTitle.ClientID %>").val(title);
            if (description) $("#<%= txtDescription.ClientID %>").val(description);
            if (tags) AssignTags_SetSelectedTags(tags);
        });

        function onDropChange(dropdown) {
            if (!isFromSiteEditor) {
                $.setCookie("iApps_Content_Title", $("#<%= txtTitle.ClientID %>").val(), 1);
                $.setCookie("iApps_Content_Description", $("#<%= txtDescription.ClientID %>").val(), 1);
                $.setCookie("iApps_Content_Tags", AssignTags_GetSelectedTags(), 1);
            }

            var selectedValue = dropdown.options[dropdown.selectedIndex].value;
            if (selectedValue == 'Free Form') {
                window.location.href = 'ManageContentDetails.aspx?NodeId=' + qParams["NodeId"];
            }
            else {
                if (isFromSiteEditor) {
                    qParams["TemplateId"] = selectedValue;
                    window.location.replace('ManageContentDetails.aspx' + createUrl(qParams));
                }
                else
                    window.location.replace('ManageContentDetails.aspx?NodeId=' + qParams["NodeId"] + '&TemplateId=' + selectedValue);
            }

            ShowiAppsLoadingPanel();

            return false;
        }

        function getUrlVars() {
            var qParams = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                qParams.push(hash[0]);
                qParams[hash[0]] = hash[1];
            }
            return qParams;
        }

        function createUrl(qParams) {
            var url = '';
            for (var i = 0; i < qParams.length; i++) {
                if (i == 0) {
                    url += '?';
                }
                else {
                    url += '&';
                }
                url += qParams[i] + '=' + qParams[qParams[i]];
            }
            return url;
        }

        var qParams;
        var isFromSiteEditor = false;
        var isForceSaveContent = false;
        $(function () {
            qParams = getUrlVars();
            isFromSiteEditor = typeof qParams["isFromSiteEditor"] != "undefined" && qParams["isFromSiteEditor"] == "true" ? true : false;
            isForceSaveContent = typeof qParams["ForceSaveContent"] != "undefined" && qParams["ForceSaveContent"] == "true" ? true : false;
            $("#btnSave").show();
            if (isFromSiteEditor) {
                if (typeof qParams["Operation"] != "undefined" && qParams["Operation"] == "Create") {
                    $("#<%= btnSaveAs.ClientID %>").hide();
                }
                if ((typeof parent.Data != 'undefined' && parent.Data.HiddenIsMasterContent.value.toLowerCase() == 'true') && (typeof qParams["Operation"] != "undefined" && qParams["Operation"] != "Create")) {
                    $("#<%= btnSave.ClientID %>").hide();

                }
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <asp:HiddenField ID="hdnPageId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnNodeId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnContentId" runat="server" ClientIDMode="Static" />
    <asp:PlaceHolder ID="phContentDetails" runat="server">
        <div class="columns left-column" id="divLeft" runat="server">
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Title %><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="240" />
                    <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>" Display="None"
                        SetFocusOnError="true" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Description %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="240"
                        MaxLength="2048" TextMode="MultiLine" />
                    <asp:RegularExpressionValidator ID="reqtxtDescription" runat="server" ControlToValidate="txtDescription"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheDescription %>"
                        Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div id="divIndexTerms">
                <h4>
                    <%= GUIStrings.ApplyTagsToContent%></h4>
                <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="346" />
            </div>
        </div>
        <div class="columns right-column" id="divRight" runat="server">
            <div class="form-row">
                <label class="form-label">
                    <strong>
                        <%= GUIStrings.ContentType %></strong></label>
                <div class="form-value text-value">
                    <asp:Literal ID="ltTemplate" runat="server" Text=" - " />
                    <asp:HiddenField ID="hdnTemplateId" runat="server" />
                    <asp:DropDownList runat="server" ID="cmbTemplate" onchange="onDropChange(this);">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, FreeForm %>" runat="server" Value="Free Form"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btnShowAll" runat="server" Text="<%$ Resources:GUIStrings, ShowAll %>"
                        OnClick="btnShowAll_Click" CssClass="button" CausesValidation="false" />
                </div>
            </div>
            <UC:HTMLEditor ID="htmlEditor" runat="server" Width="810" Height="513" />
            <UC:ContentDefinitionEditor ID="contentDefinitionEditor" runat="server" Visible="false" />
        </div>
        <div class="iapps-modal view-pages" id="pagesUsingContent" style="display: none;">
            <div class="modal-header clear-fix">
                <h2>
                    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, SubmitIntoWorkflow %>" />
                </h2>
            </div>
            <div class="modal-content">
                <UC:ViewPagesByObject ID="viewPagesByObject" IsWarning="true" runat="server" />
                <asp:HiddenField ID="hdnPageAccessLevel" runat="server" ClientIDMode="Static" />
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSaveAsCancel" runat="server" CssClass="button" Text="<%$ Resources:GUIStrings, Close %>"
                    ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CloseSubmitIntoWorkflow();" />
                <asp:Button ID="btnSaveAsDraft" runat="server" Text="<%$ Resources:GUIStrings, SaveAsDraft %>"
                    OnClientClick="return selectPage();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAsDraft %>"
                    OnClick="btnSaveAsDraft_Click" ClientIDMode="Static" />
                <asp:Button ID="btnSubmitToWorkflow" runat="server" Text="<%$ Resources:GUIStrings, SubmitIntoWorkflow %>"
                    OnClientClick="return SubmitToWorkflow();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SubmitIntoWorkflow %>"
                    OnClick="btnSubmitToWorkflow_Click" ClientIDMode="Static" />
                <asp:Button ID="btnPublish" runat="server" Text="<%$ Resources:GUIStrings, Publish %>"
                    OnClientClick="return PublishPage();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Publish %>"
                    OnClick="btnPublish_Click" ClientIDMode="Static" />
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMessage" runat="server" Visible="false">
        <div class="grid-message">
            <asp:Literal ID="ltMessage" runat="server" Text="<%$ Resources:GUIStrings, YouCannotEditContentPageNoAccess %>" />
        </div>
        <asp:ListView ID="lvPageList" runat="server" ItemPlaceholderID="phPageList">
            <LayoutTemplate>
                <div class="grid-section">
                    <div class="collection-table fat-grid">
                        <asp:PlaceHolder ID="phPageList" runat="server" />
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <div class="<%# Container.DisplayIndex % 2 == 0 ? "row" : "alternate-row" %>">
                    <div class="collection-row clear-fix">
                        <div class="first-cell">
                            <%# gridActionsDTO.PageSize * (gridActionsDTO.PageNumber - 1) + Container.DisplayIndex + 1 %>
                        </div>
                        <div class="middle-cell">
                            <h4>
                                <%# Eval("Title") %>
                            </h4>
                            <p>
                                <%# Eval("Path") %>
                            </p>
                        </div>
                        <div class="last-cell">
                            <div class="child-row">
                                <%# Eval("Reason") %>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:ListView>
    </asp:PlaceHolder>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CausesValidation="false" UseSubmitBehavior="false" CssClass="button cancel-button"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSaveAs" Text="<%$ Resources:GUIStrings, SaveAs %>" runat="server"
        CssClass="primarybutton" Visible="false" ToolTip="<%$ Resources:GUIStrings, SaveAs %>"
        OnClientClick="return OpenSelectTargetDirectory();" OnClick="btnSave_Click" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        OnClientClick="return SaveManageContentDetails();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"
        OnClick="btnSave_Click" />
</asp:Content>
