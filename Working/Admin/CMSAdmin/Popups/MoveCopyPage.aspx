<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="MoveCopyPage.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.MoveCopyPage" Theme="General" ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" language="javascript">
        var fromMenuId;
        var toMenuId;
        var dragPageId;
        var boolIsInWorkflow;

        function MoveCopyComplete(success, toMenuId) {
            popupActionsJson.CustomAttributes.MoveCopySuccess = success;
            popupActionsJson.CustomAttributes.ToMenuId = toMenuId;
            SelectiAppsAdminPopup();
        }

        function onMovePageBtnClick() {
            ShowiAppsLoadingPanel();
            if (boolIsInWorkflow == "False") {
                var error = CheckForDuplicatePages(toMenuId, dragPageId);
                var moveResult;
                if (error == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThePageYoureTryingToMoveAlreadyExistsPleaseTryToMoveADifferentPage %>'/>");
                }
                else {
                    moveResult = MovePage(dragPageId, fromMenuId, toMenuId);
                }
                if (moveResult == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThePageHasBeenSuccessfullyMoved %>'/>");
                    MoveCopyComplete(true, toMenuId);
                    return;
                }
                else {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ErrorMoveActionFailed %>'/>");
                }
            }
            else {
                alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThisPageIsInWorkflowTheRequestedOperationCanNotBeDone %>'/>");
            }

            MoveCopyComplete(false, toMenuId);
        }

        function onCopyPageBtnClick() {
            ShowiAppsLoadingPanel();
            if (boolIsInWorkflow == "False") {
                var error = CheckForDuplicatePages(toMenuId, dragPageId);
                var moveResult;
                if (error == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThePageYoureTryingToCopyAlreadyExistsPleaseTryToCopyADifferentPage %>'/>");
                }
                else {
                    moveResult = CopyPage(dragPageId, fromMenuId, toMenuId);
                }
                if (moveResult == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThePageHasBeenSuccessfullyCopied %>'/>");
                    MoveCopyComplete(true, toMenuId);
                    return;
                }
                else {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ErrorCopyActionFailed %>'/>");
                }
            }
            else {
                alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThisPageIsInWorkflowTheRequestedOperationCanNotBeDone %>'/>");
            }
            MoveCopyComplete(false, toMenuId);
        }

        function onCopyAsNewBtnClick() {
            ShowiAppsLoadingPanel();

            if (boolIsInWorkflow == "False") {
                var error = CheckForDuplicatePagesCopyAsNew(toMenuId, dragPageId, "true");
                var moveResult;
                if (error == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThePageNameYoureTryingToManageAlreadyExistsPleaseTryAgainWithADifferentName %>'/>");
                }
                else {
                    moveResult = CopyPageAsNew(dragPageId, fromMenuId, toMenuId);
                }
                if (moveResult == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThePageHasBeenSuccessfullyCopiedToNew %>'/>");
                    MoveCopyComplete(true, toMenuId);
                    return;
                }
                else {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ErrorCopyAsNewActionFailed %>'/>");
                }
            }
            else {
                alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, ThisPageIsInWorkflowTheRequestedOperationCanNotBeDone %>'/>");
            }

            MoveCopyComplete(false, toMenuId);
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <h3>
        <asp:Literal ID="deleteFileMessagelbl" runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToDoTheFollowing %>" />
    </h3>
    <ul>
        <li><asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, MovePageInstruction %>" /></li>
        <li><asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, LinkPageInstruction %>" /></li>
        <li><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, CopyPageInstruction %>" /></li>
    </ul>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="cancelBtn" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="moveBtn" Text="<%$ Resources:GUIStrings, MovePage %>" CssClass="primarybutton"
        runat="server" ToolTip="<%$ Resources:GUIStrings, MovePage %>" OnClientClick="onMovePageBtnClick();" />
    <asp:Button ID="copyBtn" Text="<%$ Resources:GUIStrings, LinkPage %>" CssClass="primarybutton"
        runat="server" ToolTip="<%$ Resources:GUIStrings, LinkPage %>" OnClientClick="onCopyPageBtnClick();" />
    <asp:Button ID="copyAsBtn" Text="<%$ Resources:GUIStrings, CopyPage %>" CssClass="primarybutton"
        runat="server" ToolTip="<%$ Resources:GUIStrings, CopyPage %>" OnClientClick="onCopyAsNewBtnClick();" />
</asp:Content>
