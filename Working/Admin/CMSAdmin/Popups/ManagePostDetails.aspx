<%@ Page Language="C#" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManagePostDetails" Theme="General" CodeBehind="ManagePostDetails.aspx.cs"
    MasterPageFile="~/Popups/iAPPSPopup.Master" EnableEventValidation="false" ClientIDMode="Static" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<%@ Register TagPrefix="UC" TagName="ContentDefinitionEditor" Src="~/UserControls/Libraries/Data/ContentDefinitionEditor.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $("#rightColumn").iAppsTabs();
        });

        function OpenInsertImagePopup() {
            viewPopupWindow = OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectPostImage");

            return false;
        }

        function SelectPostImage() {
            var imgURL = popupActionsJson.CustomAttributes.ImageUrl.replace(jPublicSiteUrl, '');
            imgURL = (jPublicSiteUrl.lastIndexOf('/') == (jPublicSiteUrl.length - 1)) ? '/' + imgURL : imgURL;
            $("#txtImageUrl").val(imgURL);
        }

        var oldPostTitle;
        function onPostTitleFocus() {
            oldPostTitle = $("#txtTitle").val();
        }

        function onPostTitleChange() {
            var postTitle = $("#txtTitle").val();
            var friendlyName = $("#txtUrlFriendlyName").val();

            if (oldPostTitle != postTitle || friendlyName == "") {
                var newFriendlyName = GetCleanFriendlyName(postTitle);
                var proceed = true;
                if ($("#txtUrlFriendlyName").val() != "")
                    proceed = window.confirm(stringformat("Do you want to change the Post URL friendly name from {0} to {1}", friendlyName, newFriendlyName));

                if (proceed)
                    $("#txtUrlFriendlyName").val(newFriendlyName);
            }
        }

        function SaveBlogPost() {
            var pageValid = Page_ClientValidate();
            if (pageValid) {
                var CheckPostName = IsPostFriendlyNameAvailable($("#hdnBlogId").val(), $("#hdnPostId").val(), $("#txtUrlFriendlyName").val());
                if (CheckPostName == "1") {
                    alert('<%= JSMessages.ThisPostNameAlreadyExistsPleaseEnterADifferentName %>');
                    pageValid = false;
                }
            }

            if (pageValid)
                ShowiAppsLoadingPanel();

            return pageValid;
        }

        function popUpPostDateCalendar(ctl) {
            var thisDate = new Date();
            if ($("#hdnPostDate").val() != "") {
                thisDate = new Date($("#hdnPostDate").val());
                CalendarPostDate.setSelectedDate(thisDate)
            }

            if (!(CalendarPostDate.get_popUpShowing()))
                CalendarPostDate.show(ctl);
        }

        function CalendarPostDate_OnChange(sender, eventArgs) {
            var selectedDate = CalendarPostDate.getSelectedDate();
            if (selectedDate > new Date()) {
                alert("<%= JSMessages.DateShouldNotExceedTodaysDate %>");
            }
            else {
                $("#txtPostDate").val(CalendarPostDate.formatDate(selectedDate, shortCultureDateFormat));
                $("#hdnPostDate").val(CalendarPostDate.formatDate(selectedDate, calendarDateFormat));
            }
        }

        function ShowCreateContentPopup(templateId) {
            OpeniAppsAdminPopup("ManageContentDetails", "TemplateId=" + templateId, "ClosePostContent");
        }

        function ShowInsertContentPopup() {
            OpeniAppsAdminPopup("SelectContentLibraryPopup", "", "ClosePostContent");
        }

        function ClosePostContent() {
            $("#txtContentTitle").show();
            $("#hdnAdditionalContentId").val(popupActionsJson.SelectedItems.first());
            $("#txtContentTitle").val(popupActionsJson.CustomAttributes["ContentTitle"]);
        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:HiddenField ID="hdnBlogId" runat="server" />
    <asp:HiddenField ID="hdnPostId" runat="server" />
    <div class="columns left-column">
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Title %>" /><span class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="250" onfocus="onPostTitleFocus();"
                    onblur="onPostTitleChange();" />
                <asp:RequiredFieldValidator ID="reqtxtTitle" runat="server" SetFocusOnError="true"
                    Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAPostTitle %>"
                    ControlToValidate="txtTitle" />
                <iAppsControls:iAppsCustomValidator ID="regtxtTitle" ControlToValidate="txtTitle"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheTitle %>"
                    runat="server" ValidateType="Title" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, Date %>" /><span
                    class="req">&nbsp;*</span>
            </label>
            <div class="form-value calendar-value">
                <asp:TextBox ID="txtPostDate" runat="server" CssClass="textBoxes" Width="238" />
                <asp:HiddenField ID="hdnPostDate" runat="server" />
                <span>
                    <img id="calendar_button" class="buttonCalendar" alt="<%= GUIStrings.ShowCalendar %>"
                        src="../App_Themes/General/images/calendar-button.png" onclick="popUpPostDateCalendar(this);" />
                </span>
                <ComponentArt:Calendar runat="server" ID="CalendarPostDate" SkinID="Default" PickerFormat="Custom"
                    PopUpExpandControlId="calendar_button">
                    <ClientEvents>
                        <SelectionChanged EventHandler="CalendarPostDate_OnChange" />
                    </ClientEvents>
                </ComponentArt:Calendar>
                <asp:CompareValidator ID="cvtxtPostDate" runat="server" ControlToValidate="txtPostDate"
                    Type="Date" Operator="DataTypeCheck" ErrorMessage="<%$ Resources:JSMessages, PleaseFormatTheDatePostDateAsMmddyyyy%>"
                    Display="None" />
                <asp:RangeValidator ID="rvtxtPostDate" runat="server" ControlToValidate="txtPostDate"
                    ErrorMessage="<%$ Resources:JSMessages, PostDateShouldNotExceedTodaysDate %>"
                    Display="None" SetFocusOnError="true" MinimumValue="01/01/1900" Type="Date" Enabled="false" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:GUIStrings, Author %>" /><span
                    class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtAuthor" runat="server" CssClass="textBoxes" Width="250" />
                <asp:RequiredFieldValidator ID="reqtxtAuthor" runat="server" SetFocusOnError="true"
                    Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAAuthorName %>"
                    ControlToValidate="txtAuthor" />
                <iAppsControls:iAppsCustomValidator ID="regtxtAuthor" ControlToValidate="txtAuthor"
                    ErrorMessage="<%$ Resources:JSMessages, AuthorNameCanHaveOnlyAlphanumericCharactersPleaseReenter %>"
                    runat="server" ValidateType="Name" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:GUIStrings, Email %>" /><span
                    class="req">&nbsp;*</span>
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtAuthorEmail" runat="server" CssClass="textBoxes" Width="250" />
                <asp:RequiredFieldValidator ID="reqtxtAuthorEmail" runat="server" SetFocusOnError="true"
                    Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAAuthorEmail %>"
                    ControlToValidate="txtAuthorEmail" />
                <iAppsControls:iAppsCustomValidator ID="regtxtAuthorEmail" ControlToValidate="txtAuthorEmail"
                    ErrorMessage="<%$ Resources:JSMessages, TheEmailAddressIsInvalid %>" runat="server"
                    ValidateType="Email" />
            </div>
        </div>
        <div>
            <h4>
                <%= GUIStrings.Categories %></h4>
            <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="282" />
        </div>
    </div>
    <div class="columns right-column" id="rightColumn">
        <ul>
            <li><span><a href="javascript://"><%= GUIStrings.PostProperties %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.SEOProperties %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.AdditionalProperties %></a></span></li>
        </ul>
        <div class="content-tab">
            <UC:ContentDefinitionEditor ID="contentDefinitionEditor" runat="server" ClientIDMode="AutoID" />
            <asp:HiddenField ID="hdnContentId" runat="server" />
        </div>
        <div style="display: none;">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, TitleTag %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitleTag" runat="server" CssClass="textBoxes" Width="500" />
                    <asp:RegularExpressionValidator ID="regtxtTitleTag" runat="server" ControlToValidate="txtTitleTag"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheTitleTag %>"
                        Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:GUIStrings, H1Tag %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtH1Tag" runat="server" CssClass="textBoxes" Width="500" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, URLFriendlyName %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtUrlFriendlyName" runat="server" CssClass="textBoxes" Width="500" />
                    <asp:RequiredFieldValidator ID="reqtxtUrlFriendlyName" runat="server" ControlToValidate="txtUrlFriendlyName"
                        SetFocusOnError="true" ErrorMessage="<%$ Resources:JSMessages, UrlFriendlyNameCanNotBeNull %>"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="regtxtUrlFriendlyName" runat="server" ControlToValidate="txtUrlFriendlyName"
                        ErrorMessage="<%$ Resources:JSMessages, UrlFriendlyNameCanHaveOnlyAlphanumericCharactersAndPleaseReEnter %>"
                        SetFocusOnError="true" ValidationExpression="^[^\/?:&]+$" Display="None" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, Keywords %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtKeywords" runat="server" CssClass="textBoxes" Width="500" Rows="4"
                        TextMode="MultiLine" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.DescriptiveMetadata%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescMetadata" runat="server" CssClass="textBoxes" Width="500"
                        Rows="4" TextMode="MultiLine" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.OtherMetadata%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtOtherMetadata" runat="server" CssClass="textBoxes" Width="500"
                        Rows="4" TextMode="MultiLine" />
                </div>
            </div>
        </div>
        <div style="display: none;">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, ShortDescription %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Wrap="true"
                        TextMode="multiLine" Width="500" Rows="3" />
                    <iAppsControls:iAppsCustomValidator ID="regtxtDescription" ControlToValidate="txtDescription"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheShortDescription %>"
                        runat="server" ValidateType="Description" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Location %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtLocation" runat="server" CssClass="textBoxes" Width="500" />
                    <iAppsControls:iAppsCustomValidator ID="regtxtLocation" ControlToValidate="txtLocation"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheLocation %>"
                        runat="server" ValidateType="PlainText" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Image %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtImageUrl" runat="server" CssClass="textBoxes" Width="470" />
                    <asp:Button ID="btnAddImage" runat="server" Text="..." CssClass="textBoxes" Width="25"
                        OnClientClick="return OpenInsertImagePopup();" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize16" runat="server" Text="<%$ Resources:GUIStrings, Labels %>" /></label>
                <div class="form-value">
                    <asp:TextBox ID="txtLabels" runat="server" CssClass="textBoxes" Width="500" />
                    <iAppsControls:iAppsCustomValidator ID="regtxtLabels" ControlToValidate="txtLabels"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheLabels %>"
                        runat="server" ValidateType="PlainText" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, AllowComments %>" /></label>
                <div class="form-value">
                    <input type="checkbox" runat="server" id="chkAllowComments" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, AdditionalInformation %>" /></label>
                <div class="form-value multi-column">
                    <asp:TextBox ID="txtContentTitle" runat="server" CssClass="textBoxes" Width="307"
                        Style="display: none;" />&nbsp;
                    <asp:PlaceHolder ID="phCreateContent" runat="server">
                        <asp:HyperLink ID="hplCreateContent" runat="server" Text="Create New" />&nbsp;|&nbsp;</asp:PlaceHolder>
                    <asp:HyperLink ID="hplInsertFromLibrary" runat="server" Text="Insert from Library" />
                    <asp:HiddenField ID="hdnAdditionalContentId" runat="server" />
                </div>
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        ToolTip="<%$ Resources:GUIStrings, Cancel %>" CssClass="button" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, SaveAsDraft %>"
        ToolTip="<%$ Resources:GUIStrings, SaveAsDraft %>" CssClass="primarybutton" OnClientClick="return SaveBlogPost();" />
    <asp:Button ID="btnPublish" runat="server" Text="<%$ Resources:GUIStrings, Publish %>"
        ToolTip="<%$ Resources:GUIStrings, Publish %>" CssClass="primarybutton" OnClientClick="return SaveBlogPost();" />
</asp:Content>
