<%@ Page Language="C#" AutoEventWireup="True" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectTargetDirectory" StylesheetTheme="General"
    CodeBehind="SelectTargetDirectory.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<%@ Register Src="~/UserControls/Libraries/DirectoryTree.ascx" TagName="LibraryTree"
    TagPrefix="UC" %>
<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SelectTargetDirectory() {
            if (typeof Page_ClientValidate == "undefined" || Page_ClientValidate()) {
                if (treeActionsJson.FolderId == EmptyGuid) {
                    alert("<%= JSMessages.PleaseSelectDirectory %>");
                }
                else {
                    popupActionsJson.FolderId = treeActionsJson.FolderId;
                    popupActionsJson.CustomAttributes["ContentTitle"] = $("#txtTitle").val();
                    popupActionsJson.CustomAttributes["CreateVariantContent"] = $("#rblstMaster input:checked").val() == "0";
                    CanceliAppsAdminPopup(true);
                }
            }
            return false;
        }

        function fn_ShowAdvanceOptions() {
            $('.advance-section').slideToggle();
            $('.advance-opton-link').toggleClass('expanded');
        }
    </script>
</asp:Content>
<asp:Content ID="cntBody" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:iAppsValidationSummary ID="valDirectory" runat="server" ValidationGroup="val" />
    <asp:PlaceHolder ID="phTitle" runat="server">
        <div class="form-row">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ContentTitle %>" />
            </label>
            <div class="form-value">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="304" ClientIDMode="Static" />
                <iAppsControls:iAppsRequiredValidator ID="rvtxtTitle" ControlToValidate="txtTitle"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseEnterTheTitle %>" runat="server" ValidationGroup="valDirectory" />
                <iAppsControls:iAppsCustomValidator ID="regtxtTitle" ControlToValidate="txtTitle"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseCheckTheTitleForSpecialCharacters %>"
                    runat="server" ValidateType="Title" ValidationGroup="valDirectory" />
            </div>
        </div>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phMasterContent" runat="server" Visible="false">
        <div class="form-row">
            <label class="form-label">
                <a href="javascript: //" onclick="fn_ShowAdvanceOptions()" class="advance-opton-link">Advanced Content Variant Options</a>
            </label>
        </div>
        <div class="advance-section" style="display: none;">
            <div class="form-row">
                <div class="form-value text-value">
                    <asp:RadioButtonList ID="rblstMaster" runat="server" ClientIDMode="Static" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0" Text="<%$ Resources:GUIStrings, MasterContentVariant %>" />
                        <asp:ListItem Value="1" Text="<%$ Resources:GUIStrings, TargetDirectoryNewUnlinkedContent %>" />
                    </asp:RadioButtonList>
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, MasterLocation %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtLocation" runat="server" Enabled="false" CssClass="disabled" Width="285" />
                </div>
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="form-row tree-row">
        <UC:LibraryTree ID="libraryTree" runat="server" HideTreeHeading="true" TreeHeight="400" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSelect" runat="server" Text="<%$ Resources:GUIStrings, SelectDirectory %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectDirectory %>"
        OnClientClick="return SelectTargetDirectory();" />
</asp:Content>
