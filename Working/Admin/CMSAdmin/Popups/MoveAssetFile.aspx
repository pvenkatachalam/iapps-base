﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MoveAssetFile.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.MoveAssetFile" MasterPageFile="~/Popups/iAPPSPopup.Master"
    ClientIDMode="Static" StylesheetTheme="General" %>

<asp:Content ID="cphHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" language="javascript">
        var fromMenuId;
        var toMenuId;
        var dragFileId;
        var dragFileTitle;

        function onMoveFileBtnClick() {
            var success = false;
            var moveResult = MoveFile(dragFileId, fromMenuId, toMenuId);
            if (moveResult == "Move") {
                success = true;
                alert('<asp:Literal runat="server" Text="<%$ Resources:JSMessages,TheFileHasBeenSuccessfullyMoved %>" /> ');
            }
            else if (moveResult == "Override") {
                var var1 = confirm('<asp:Literal runat="server" Text="<%$ Resources:JSMessages,TheFileWithTheSameNameAlreadyExistsInThisDirectoryDoYouWantToOverride %>" /> ');
                if (var1 == true) {
                    var overrideResult = OverrideFile(dragFileId, fromMenuId, toMenuId);
                    if (overrideResult != null) {
                        if (overrideResult == "True") {
                            success = true;
                            alert('<asp:Literal runat="server" Text="<%$ Resources:JSMessages,TheFileHasBeenSuccessfullyMoved%>" /> ');
                        }
                    }
                }
            }
            else {
                alert('<asp:Literal runat="server" Text="<%$ Resources:JSMessages,ErrorMoveActionFailed %>" /> ');
            }

            CanceliAppsAdminPopup(success);
        }  
    </script>
</asp:Content>
<asp:Content ID="cphBody" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Label ID="deleteFileMessagelbl" runat="server" Text="<%$ Resources:GUIStrings, AreYouSureYouWantToMoveThisFile %>"></asp:Label>
</asp:Content>
<asp:Content ID="cphFooter" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="cancelBtn" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="moveBtn" Text="<%$ Resources:GUIStrings, Move %>" runat="server"
        ToolTip="<%$ Resources:GUIStrings, Move %>" OnClientClick="onMoveFileBtnClick();"
        CssClass="primarybutton" />
</asp:Content>
