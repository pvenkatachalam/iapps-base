<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.DeactivateUserPopup" StylesheetTheme="general" CodeBehind="DeactivateUserPopup.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script language="javascript" type="text/javascript">
        function DecativateMember_OnClick() {
            if (grdMembers.getSelectedItems().length <= 0) {
                alert("<%= JSMessages.SelectCA %>");
                return false;
            }
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <asp:Literal ID="ltMessage" runat="server" Text="<%$ Resources:GUIStrings, PleaseTransferThisMemberTypesWorkflowResponsibilitiesAndPermissionsToAContentAdministrator %>" />
    </p>
    <p class="confirm">
        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:GUIStrings, SelectContentAdministrator %>"></asp:Label></p>
    <div class="grid-section">
        <ComponentArt:Grid ID="grdMembers" RunningMode="Client" SkinID="Default" Width="100%"
            runat="server" ShowFooter="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="UserId" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" AllowSorting="false" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn AllowReordering="False" HeadingText="<%$ Resources:GUIStrings, UserName %>"
                            DataField="UserName" DataCellCssClass="first-cell" HeadingCellCssClass="first-cell" />
                        <ComponentArt:GridColumn Width="160" AllowReordering="False" HeadingText="<%$ Resources:GUIStrings, LastName %>"
                            DataField="LastName" />
                        <ComponentArt:GridColumn Width="160" AllowReordering="False" HeadingText="<%$ Resources:GUIStrings, FirstName %>"
                            DataField="FirstName" DataCellCssClass="last-cell" HeadingCellCssClass="last-cell" />
                        <ComponentArt:GridColumn Visible="false" DataField="UserId" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        OnClientClick="return CanceliAppsAdminPopup(true);" CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnDeactivate" runat="server" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, DeactivateGroup %>"
        OnClick="btnDeactivate_OnClick" OnClientClick="return DecativateMember_OnClick();" />
</asp:Content>
