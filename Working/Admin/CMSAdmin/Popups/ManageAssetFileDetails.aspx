﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="ManageAssetFileDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageAssetFileDetails"
    Theme="General" ClientIDMode="Static" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function SaveManageAssetFileDetails() {
            if (Page_ClientValidate() == true) {
                var oldFileName = $("#ltAssetFileName").html();
                var newFileName = $("#fileAsset").val();
                if (newFileName.indexOf("\\") > 0)
                    newFileName = newFileName.substr(newFileName.lastIndexOf("\\") + 1);

                if (newFileName != "")
                    newFileName = ReplaceSpecialCharactersInFileName(newFileName);
                if (oldFileName != "" && newFileName != "" &&
                    oldFileName.toLowerCase() != newFileName.toLowerCase()) {
                    alert("<%= JSMessages.FileUploadNameMisMatch %>");
                    return false;
                }

                ShowiAppsLoadingPanel();
                return true;
            }
            return false;
        }        
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
    <div class="form-section">
        <div class="columns">
            <h2><%= GUIStrings.FileProperties %></h2>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.FileName%></label>
                <div class="form-value" style="width: 280px;">
                    <asp:FileUpload ID="fileAsset" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                        CssClass="fileUpload" onkeydown="blur(this);" oncontextmenu="return false;" Width="280" />
                    <asp:Label ID="ltAssetFileName" runat="server" CssClass="coaching-text" />
                    <asp:RequiredFieldValidator ID="reqfileAsset" ControlToValidate="fileAsset" runat="server"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseSelectAFile %>" Display="None"
                        SetFocusOnError="true" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.FileTitle%><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="280" />
                    <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsRequired %>" Display="None" SetFocusOnError="true" />
                    <asp:RegularExpressionValidator ID="regtxtTitle" runat="server" ControlToValidate="txtTitle"
                        ErrorMessage="<%$ Resources:JSMessages, TitleIsNotValid %>" Display="None" SetFocusOnError="true"
                        ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label"><%= GUIStrings.Description %></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="280"
                        TextMode="MultiLine" />
                    <asp:RegularExpressionValidator ID="regtxtDescription" runat="server" ControlToValidate="txtDescription"
                        ErrorMessage="<%$ Resources:JSMessages, DescriptionIsNotValid %>" Display="None"
                        SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
                <div class="clear-fix">
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.ToolTip%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTooltip" runat="server" CssClass="textBoxes" Width="280" />
                    <asp:RegularExpressionValidator ID="regtxtTooltip" runat="server" ControlToValidate="txtTooltip"
                        ErrorMessage="<%$ Resources:JSMessages, TooltipIsNotValid %>" Display="None"
                        SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
        </div>
        <div class="columns right-column">
         <h2><%= GUIStrings.Tags %></h2>
            <div class="form-row">               
                <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="300" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        cssclass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" CausesValidation="false"/>
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        cssclass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClientClick="return SaveManageAssetFileDetails();"
        OnClick="btnSave_Click" />
</asp:Content>
