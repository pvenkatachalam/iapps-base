﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    CodeBehind="ImportPages.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ImportPages" Theme="General" %>

<%@ Register TagPrefix="PT" TagName="PageTree" Src="~/UserControls/Libraries/Data/PageMapTree.ascx" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function LoadMasterPages() {
            $(function () {
                if (typeof parent.gridActionsJson != "undefined") {
                    gridActionsJson = JSON.parse(JSON.stringify(parent.gridActionsJson));
                    pageImport_Callback("Load");
                }
            });
        }

        function cbMasterPages_OnCallbackComplete() {
            if (gridActionsJson.Action != "Load") {
                if ($(".left-control input[type='checkbox']").length == 0) {
                    if (gridActionsJson.Action == "Translate")
                        RedirectToAdminPage("SubmitForTranslation", "PopupMode=0&ReloadOnClose=true");
                    else
                        CanceliAppsAdminPopup(true);
                }
                else if (gridActionsJson.Action == "Translate") {
                    OpeniAppsAdminPopup("SubmitForTranslation", "PopupMode=0");
                }
            }
        }

        function pageImport_Callback(action) {
            var doCallback = true;

            if (action != "Load") {
                var selectedItems = [];
                $(".left-control input[type='checkbox']:checked").each(function () {
                    selectedItems.push($(this).parent().attr("pageId"));
                });

                if (selectedItems.length == 0) {
                    alert("<%= GUIStrings.PleaseSelectMasterPagesToImport %>");
                    doCallback = false;
                }
                else {
                    if (IsPageVariantExists(JSON.stringify(selectedItems)).toLowerCase() == "true")
                        doCallback = window.confirm("<%= JSMessages.PageVariantAlreadyExists %>");
                }
            }

            if (doCallback) {
                gridActionsJson.Action = action;
                gridActionsJson.FolderId = treeActionsJson.FolderId;

                cbMasterPages.set_callbackParameter(JSON.stringify(gridActionsJson));
                cbMasterPages.callback();
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <asp:CheckBox ID="chkPageVariations" runat="server" Text="<%$ Resources:GUIStrings,IncludePageVariations %>"
            Checked="true" />
    </div>
    <div class="form-row">
        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, ImportPagesFromMasterSiteInstructions %>" />
    </div>
    <div class="left-control">
        <h2>
            <asp:Localize ID="lc1" runat="server" Text="<%$ Resources:GUIStrings, MasterPagesSelected %>" /></h2>
        <iAppsControls:CallbackPanel ID="cbMasterPages" runat="server" CssClass="grid-section"
            OnClientCallbackComplete="cbMasterPages_OnCallbackComplete">
            <ContentTemplate>
                <asp:CheckBoxList ID="chkMasterPages" runat="server" RepeatLayout="UnorderedList" />
            </ContentTemplate>
        </iAppsControls:CallbackPanel>
    </div>
    <div class="right-control">
        <PT:PageTree ID="pageTree" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        OnClientClick="return CanceliAppsAdminPopup(true);" CssClass="button" />
    <asp:Button ID="btnImportAndTranslate" runat="server" Text="<%$ Resources:GUIStrings, ImportAndTranslate %>"
        CssClass="primarybutton" OnClientClick="return pageImport_Callback('Translate');" />
    <asp:Button ID="btnImport" runat="server" Text="<%$ Resources:GUIStrings, Import %>"
        CssClass="primarybutton" OnClientClick="return pageImport_Callback('Import');" />
</asp:Content>
