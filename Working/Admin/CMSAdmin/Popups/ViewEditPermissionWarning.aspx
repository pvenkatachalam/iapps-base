<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewEditPermissionWarning" StylesheetTheme="general" CodeBehind="ViewEditPermissionWarning.aspx.cs" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <p>
        <asp:Literal ID="ltMessage" runat="server" Text="<%$ Resources:GUIStrings, TheFollowingUsersIsareAssignedToWorkflowsThatRequirePermissionsYouAreTryingToRemove %>" />
    </p>
    <div class="grid-section">
        <ComponentArt:Grid ID="grdPermissions" RunningMode="Client" SkinID="Default" Width="100%"
            runat="server" ShowFooter="false" EmptyGridText="<%$ Resources:GUIStrings, NoItemsFound %>">
            <Levels>
                <ComponentArt:GridLevel DataKeyField="MemberId" ShowTableHeading="false" ShowSelectorCells="false"
                    RowCssClass="row" AllowSorting="false" ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="data-cell" HeadingCellCssClass="heading-cell" HeadingRowCssClass="heading-row"
                    SelectedRowCssClass="selected-row" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn Width="100" AllowReordering="False" HeadingText="<%$ Resources:GUIStrings, User %>"
                            DataField="MemberName" DataCellCssClass="first-cell" HeadingCellCssClass="first-cell" />
                        <ComponentArt:GridColumn Width="100" AllowReordering="False" HeadingText="<%$ Resources:GUIStrings, Workflow %>"
                            DataField="WorkflowName" />
                        <ComponentArt:GridColumn Width="100" AllowReordering="False" HeadingText="<%$ Resources:GUIStrings, Permission %>"
                            DataField="RoleName" DataCellCssClass="last-cell" HeadingCellCssClass="last-cell" />
                        <ComponentArt:GridColumn DataField="MemberId" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        ToolTip="<%$ Resources:GUIStrings, Close %>" CssClass="primarybutton" OnClientClick="return CanceliAppsAdminPopup();" />
</asp:Content>
