<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    Theme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageSiteDetails" CodeBehind="ManageSiteDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="SiteDetails" Src="~/UserControls/Administration/SiteDetails.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
<script type="text/javascript">
    $(document).ready(function () {
        setTimeout(function () {
            iAPPS_Tooltip.Initialize('rel', 'title', 'top');
        }, 1000);
    });
    function ValidatePage() {
        if (Page_ClientValidate()) {
            ShowiAppsLoadingPanel();
            return true;
        }
        return false;
    }
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <UC:SiteDetails ID="siteDetails" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CausesValidation="false"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" CausesValidation="true" OnClientClick ="return ValidatePage();"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>" OnClick="btnSave_Click" />
</asp:Content>
