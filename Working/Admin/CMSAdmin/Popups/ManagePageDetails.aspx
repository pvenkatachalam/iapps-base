<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="ManagePageDetails.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManagePageDetails"
    StylesheetTheme="General" ValidateRequest="false" ClientIDMode="Static" %>

<%@ Register TagPrefix="UC" TagName="AssignTags" Src="~/UserControls/General/AssignTags.ascx" %>
<%@ Register TagPrefix="UC" TagName="SecurityLevels" Src="~/UserControls/Libraries/SecurityLevelWithListbox.ascx" %>
<%@ Register TagPrefix="UC" TagName="Attributes" Src="~/UserControls/General/AssignAttributes.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(function () {
            $(".right-column.page-properties").iAppsTabs();

            $("#chkEnableCache").change(function () {
                if ($("#ddlCacheProfile option").length > 1)
                    $("#pnlCacheProfile").toggle();
            });
        });

        function SaveManagePageDetails() {
            if (Page_ClientValidate() == true) {
                var error = DuplicatePagesWithFriendlyNameAndTitle("", $("#txtTitle").val(), $("#hdnPageId").val(), "<%= PageMapNodeId %>");
                if (error == "1") {
                    alert("<%= JSMessages.ThePageWithSameUrlFriendlyNameAlreadyExists %>");
                    return false;
                }
                else if (error == "2") {
                    alert("<%= JSMessages.ThePageWithSameNameAlreadyExists %>");
                    return false;
                }
                else if (error == "3") {
                    alert("<%= JSMessages.ThePageWithSameNameUrlFriendlyNameAlreadyExists %>");
                        return false;
                    }

            if (SaveAttributeValues()) {
                ShowiAppsLoadingPanel();
                return true;
            }
        }

        return false;
    }

    var oldPageName;
    function onPageNameFocus() {
        oldPageName = $("#txtTitle").val();
    }

    function onPageNameChange() {
        var pageName = $("#txtTitle").val();
        var friendlyName = $("#txtUrlFriendlyName").val();

        var h1Tag = $("#txtH1Tag").val();
        if (h1Tag == "")
            $("#txtH1Tag").val(pageName);

        if (oldPageName != pageName) {
            $("#txtUrlFriendlyName").val(GetCleanFriendlyName(pageName));
        }
    }

    function ValidateUrlFriendlyName(sender, args) {
        if ($("#txtUrlFriendlyName").val() == "") {
            args.IsValid = true;
        }
        else {
            var error = DuplicatePagesWithFriendlyNameAndTitle($("#txtUrlFriendlyName").val(), "", $("#hdnPageId").val(), "<%= PageMapNodeId %>");
            if (error == "1" || error == 1)
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }

    function ValidateTemplate(sender, args) {
        if (typeof (cmbTemplate) != 'undefined' && (cmbTemplate.getSelectedItem() == null || cmbTemplate.getSelectedItem().Value == null || cmbTemplate.getSelectedItem().Value == "")) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }

    function cmbTemplate_OnLoad(sender, eventArgs) {
        var comboBox_inputCtl = document.getElementById(sender.get_id() + "_Input");
        comboBox_inputCtl.disabled = true;
    }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="columns left-column">
        <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" />
        <asp:HiddenField ID="hdnPageId" runat="server" />
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.PageName%><span class="req">&nbsp;*</span></label>
            <div class="form-value">
                <asp:TextBox ID="txtTitle" runat="server" CssClass="textBoxes" Width="220" onfocus="onPageNameFocus();"
                    onblur="onPageNameChange();" />
                <asp:RequiredFieldValidator ID="reqtxtTitle" ControlToValidate="txtTitle" runat="server"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseEnterAPageName %>" Display="None"
                    SetFocusOnError="true" />
                <iAppsControls:iAppsCustomValidator ID="cvtxtTitle" ControlToValidate="txtTitle"
                    ErrorMessage="<%$ Resources: JSMessages, PageNameHasInvalidCharctersAndPleaseReEnter %>"
                    runat="server" ValidateType="Title" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.Template%><span class="req">&nbsp;*</span></label>
            <div class="form-value text-value">
                <asp:Literal ID="ltTemplate" runat="server" Text=" - " />
                <ComponentArt:ComboBox ID="cmbTemplate" runat="server" SkinID="Default" Width="230"
                    Height="25" DropDownHeight="350" DropDownWidth="320" DataFields="VirtualPath,Image"
                    ItemClientTemplateId="ctTemplatePreview" ClientIDMode="AutoID">
                    <ClientTemplates>
                        <ComponentArt:ClientTemplate ID="ctTemplatePreview" runat="server">
                            ## showTemplateThumbnail(DataItem.VirtualPath, DataItem.Image, DataItem.Text) ##
                        </ComponentArt:ClientTemplate>
                    </ClientTemplates>
                    <ClientEvents>
                        <Load EventHandler="cmbTemplate_OnLoad" />
                    </ClientEvents>
                </ComponentArt:ComboBox>
                <asp:CustomValidator ID="cvcmbTemplate" runat="server" Display="None" ErrorMessage="<%$ Resources:JSMessages, PleaseSelectATemplateBeforeAddingAPage %>"
                    ClientValidationFunction="ValidateTemplate" SetFocusOnError="true" Enabled="false" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <%= GUIStrings.Description%></label>
            <div class="form-value">
                <asp:TextBox ID="txtDescription" runat="server" CssClass="textBoxes" Width="220"
                    TextMode="MultiLine" />
                <asp:RegularExpressionValidator ID="reqtxtDescription" runat="server" ControlToValidate="txtDescription"
                    ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromThePageDescription %>"
                    Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
            </div>
        </div>
        <div>
            <h4><%= GUIStrings.ApplyTagsToPage %></h4>
            <UC:AssignTags ID="trIndexTerms" runat="server" TreeHeight="255" />
        </div>
    </div>
    <div class="columns right-column page-properties">
        <ul>
            <li><span><a href="javascript://"><%= GUIStrings.SEOProperties %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.CustomStylesScripts %></a></span></li>
            <li><span><a href="javascript://"><%= GUIStrings.SecuritySettings %></a></span></li>
            <asp:PlaceHolder ID="phAttributes" runat="server">
                <li><span><a href="javascript://"><%= GUIStrings.Attributes %></a></span></li>
            </asp:PlaceHolder>
        </ul>
        <div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.TitleTag%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtTitleTag" runat="server" CssClass="textBoxes" Width="300" />
                    <asp:RegularExpressionValidator ID="regtxtTitleTag" runat="server" ControlToValidate="txtTitleTag"
                        ErrorMessage="<%$ Resources:JSMessages, PleaseRemoveAnyAndCharactersFromTheTitleTag %>"
                        Display="None" SetFocusOnError="true" ValidationExpression="^[^<>]+$" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.H1Tag%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtH1Tag" runat="server" CssClass="textBoxes" Width="300" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.URLFriendlyName%><span class="req">&nbsp;*</span></label>
                <div class="form-value">
                    <asp:TextBox ID="txtUrlFriendlyName" runat="server" CssClass="textBoxes" Width="300" />
                    <asp:RequiredFieldValidator ID="reqtxtUrlFriendlyName" runat="server" ControlToValidate="txtUrlFriendlyName"
                        SetFocusOnError="true" ErrorMessage="<%$ Resources:JSMessages, UrlFriendlyNameCanNotBeNull %>"
                        Display="None" />
                    <asp:RegularExpressionValidator ID="regtxtUrlFriendlyName" runat="server" ControlToValidate="txtUrlFriendlyName"
                        ErrorMessage="<%$ Resources:JSMessages, UrlFriendlyNameCanHaveOnlyAlphanumericCharactersAndPleaseReEnter %>"
                        SetFocusOnError="true" ValidationExpression="^[^\/?:&]+$" Display="None" />
                    <asp:CustomValidator ID="cvtxtUrlFriendlyName" runat="server" Display="None" ErrorMessage="<%$ Resources:JSMessages, ThePageWithThisUrlFriendlyNameAlreadyExistsPleaseEnterADifferentUrlFriendlyName %>"
                        ClientValidationFunction="ValidateUrlFriendlyName" SetFocusOnError="true" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.Keywords%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtKeywords" runat="server" CssClass="textBoxes" Width="300" Rows="4"
                        TextMode="MultiLine" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.DescriptiveMetadata%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtDescMetadata" runat="server" CssClass="textBoxes" Width="300"
                        Rows="4" TextMode="MultiLine" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.OtherMetadata%></label>
                <div class="form-value">
                    <asp:TextBox ID="txtOtherMetadata" runat="server" CssClass="textBoxes" Width="300"
                        Rows="4" TextMode="MultiLine" PlaceHolder="<%$ Resources:GUIStrings, OtherMetadataHelpText %>" />
                </div>
            </div>
        </div>
        <div class="tab2">
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.CustomStyles%></label>
                <div class="form-value checkbox-list">
                    <asp:CheckBoxList ID="chkStyles" runat="server" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <%= GUIStrings.CustomScripts%></label>
                <div class="form-value checkbox-list">
                    <asp:CheckBoxList ID="chkScripts" runat="server" />
                </div>
            </div>
        </div>
        <div class="tab3">
            <UC:SecurityLevels ID="securityLevels" runat="server" Width="450" Height="150" />
            <div class="form-row">
                <asp:CheckBox ID="chkEnableCache" runat="server" Text="<%$ Resources:GUIStrings, PageCachingEnabled %>" />
                <p>
                    <asp:Literal runat="server" ID="ltSiteWideCaching" />
                </p>
                <br />
                <div class="form-row">
                    <asp:CheckBox ID="chkExcludeSearch" runat="server" Text="<%$ Resources:GUIStrings, ExcludeFromSearch %>" />
                    <div class="form-row" id="searchEnterpriseSettings" runat="server" visible="false">
                        <label class="form-label" runat="server" id="lblIsPageInFrontEndIndex"><%= GUIStrings.IsPageIndexFrontEnd  %></label>
                        <label class="form-label" id="lblPageFrontEndIndexStatus" runat="server"></label>
                    </div>
                    <div class="form-row">
                        <label class="form-label" runat="server" id="lblIsPageInAdminIndex"><%= GUIStrings.IsPageIndexAdmin  %></label>
                        <label class="form-label" id="lblPageAdminIndexStatus" runat="server"></label>
                    </div>
                    <div class="form-row">
                        <asp:Button ID="btnReindex" runat="server" Text="<%$ Resources:GUIStrings, AddToIndex %>" CssClass="button" />
                    </div>
                </div>


            </div>
            <asp:Panel ID="pnlCacheProfile" runat="server" CssClass="form-row">
                <label class="form-label">
                    <%= GUIStrings.CacheProfile%></label>
                <div class="form-value">
                    <asp:DropDownList ID="ddlCacheProfile" runat="server" Width="200" />
                </div>
            </asp:Panel>
        </div>
        <div class="tab4">
            <UC:Attributes ID="ucAttributes" runat="server" Category="Page" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnSaveJump" runat="server" Text="<%$ Resources:GUIStrings, SaveAndViewInEditor %>"
        OnClientClick="return SaveManagePageDetails();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SaveAndViewInEditor %>"
        OnClick="btnSaveJump_Click" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        OnClientClick="return SaveManagePageDetails();" CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"
        OnClick="btnSave_Click" />
</asp:Content>
