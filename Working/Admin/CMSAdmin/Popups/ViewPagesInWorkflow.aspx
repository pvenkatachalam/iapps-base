<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewPagesInWorkflow" Theme="General"
    CodeBehind="ViewPagesInWorkflow.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master"
    ClientIDMode="Static" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">

        function upPageList_OnCallbackComplete(sender, eventArgs) {
            $("#upPageList").gridActions("displayMessage");
        }

        function upPageList_OnRenderComplete(sender, eventArgs) {
            $("#upPageList").gridActions("bindControls");
        }

        function upPageList_OnLoad(sender, eventArgs) {
            $("#upPageList").gridActions({
                objList: $("#gridview"),
                type: "list",
                onCallback: function (sender, eventArgs) {
                    upPageList_Callback(sender, eventArgs);
                },
                onItemSelect: function (sender, eventArgs) {
                    upPageList_ItemSelect(sender, eventArgs);
                }
            });
        }

        function upPageList_Callback(sender, eventArgs) {
            var doCallback = true;
            var selectedItemId = gridActionsJson.SelectedItems.first();

            switch (gridActionsJson.Action) {
                case "ViewPage":
                    doCallback = false;
                    ShowiAppsLoadingPanel();
                    var siteId = eventArgs.selectedItem.getValue("SiteId");
                    if (siteId.toLowerCase() == jAppId.toLowerCase()) {
                        parent.JumpToFrontPage(eventArgs.selectedItem.getValue("PageUrl"));
                    }
                    else {
                        var token = FWCallback.GetAuthToken(juserName, jUserId, jProductId, siteId, false);
                        var url = stringformat("{0}/{1}?PageState=Edit&token={2}",
                            eventArgs.selectedItem.getValue("SiteUrl"),
                            FWCallback.GetPageUrl(siteId, selectedItemId), token);
                        CloseiAppsLoadingPanel();
                        window.open(url);
                    }
                    break;
            }
        }

        function upPageList_ItemSelect(sender, eventArgs) {
            var selectedItemId = gridActionsJson.SelectedItems.first();

            

        }

    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:CallbackPanel ID="upPageList" runat="server" OnClientCallbackComplete="upPageList_OnCallbackComplete"
        OnClientRenderComplete="upPageList_OnRenderComplete" OnClientLoad="upPageList_OnLoad">
        <ContentTemplate>
            <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
            <div class="grid-section">
                <asp:ListView ID="lstPageList" runat="server" ItemPlaceholderID="phPageList">
                    <EmptyDataTemplate>
                        <div class="empty-grid-cell">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, NoPagesToDisplay %>" />
                        </div>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <table width="100%" cellpadding="0" cellspacing="0" class="grid" id="gridview">
                            <thead>
                                <tr class="heading-row">
                                    <th id="thHdSiteName" class="HeadingCell">
                                        <asp:Localize ID="site" runat="server" Text="<%$ Resources:GUIStrings, Site %>" />
                                    </th>
                                    <th id="tdHdTitle" class="HeadingCell">
                                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, Page %>" />
                                    </th>
                                    <th id="thHdWorkflowStatus" class="HeadingCell">
                                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, Status %>" />
                                    </th>
                                    <th id="th1" class="HeadingCell">
                                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, LastActor %>" />
                                    </th>
                                    <th id="th2" class="HeadingCell">
                                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, NextActor %>" />
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:PlaceHolder ID="phPageList" runat="server" />
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr class='<%# Container.DataItemIndex % 2 == 0 ? "grid-item odd-row" : "grid-item even-row" %>' objectid='<%# Eval("Id")%>'>
                            <td>
                                <asp:Literal ID="ltSiteName" runat="server" Text='<%# Eval("SiteName").ToString() %>' />
                                <input type="hidden" datafield="SiteId" id="hdnSiteId" runat="server" value='<%# Eval("SiteId")  %>' />
                                <input type="hidden" datafield="SiteUrl" id="hdnSiteUrl" runat="server" value='<%# Eval("SiteUrl")  %>' />
                                <input type="hidden" datafield="PageUrl" id="hdnPageUrl" runat="server" value='<%# Eval("CompleteFriendlyUrl")  %>' />
                            </td>
                            <td>
                                <h4>
                                    <asp:Literal ID="ltTitle" runat="server" Text='<%# Eval("Title") %>' />
                                </h4>
                            </td>
                            <td>
                                <asp:Literal ID="ltWorkflowStatus" runat="server" Text='<%# Eval("WorkflowState").ToString() %>' />
                            </td>
                            <td>
                                <asp:Literal ID="ltLastActor" runat="server" Text='<%# Eval("LastActor") %>' />
                            </td>
                            <td>
                                <asp:Literal ID="ltNextActor" runat="server" Text='<%# Eval("NextActor") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
    </iAppsControls:CallbackPanel>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        ToolTip="<%$ Resources:GUIStrings, Close %>" CssClass="button secondButton" OnClientClick="return CanceliAppsAdminPopup();" />
</asp:Content>
