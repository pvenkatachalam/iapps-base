<%@ Page Language="C#" AutoEventWireup="true" StylesheetTheme="General" ClientIDMode="Static"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ShowSnippetManager" CodeBehind="ShowSnippetManager.aspx.cs" 
    MasterPageFile="~/Popups/iAPPSPopup.Master" ValidateRequest="false" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function InsertSnippet() {
            popupActionsJson.CustomAttributes.SnippetHtml = $("#litSnippetHtml").html();
            SelectiAppsAdminPopup();

            return false;
        }
        function DeleteSnippet() {
            if (confirm('<asp:localize runat="server" text="<%$ Resources:JSMessages, AreYouSureYouWantToDeleteThisSnippet %>"/>') && window.parent.DeleteSnippet) {
                return true;
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Label ID="lblSnippet" runat="server" AssociatedControlID="ddlSnippet" Text="<%$ Resources:GUIStrings, SnippetColon %>" />
    <asp:DropDownList ID="ddlSnippet" runat="server" AutoPostBack="true" />
    <hr />
    <div id="litSnippetHtml" class="borderContainer clear-fix">
        <asp:Literal ID="litSnippet" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" value="<%= GUIStrings.Close %>" onclick="return CanceliAppsAdminPopup(true);"
        class="button cancel-button" />
    <asp:Button ID="btnDelete" runat="server" Text="<%$ Resources:GUIStrings, Delete %>"
        CssClass="button" OnClientClick="return DeleteSnippet();" />
    <asp:PlaceHolder ID="phInsert" runat="server">
        <input type="button" class="primarybutton" onclick="return InsertSnippet();" value="<%= GUIStrings.Insert %>" />
    </asp:PlaceHolder>
</asp:Content>
