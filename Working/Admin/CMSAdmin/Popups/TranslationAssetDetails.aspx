﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true"
    ClientIDMode="Static" StylesheetTheme="General" CodeBehind="TranslationAssetDetails.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.TranslationAssetDetails" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ddlTranslate_OnChange() {
            if ($("#ddlTranslateFrom").val() == $("#ddlTranslateTo").val()) {
                alert("<%= JSMessages.SourceTargetCannotBeSame %>");
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-section">
        <h3>
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, PageDetails %>" />
            </h3>
        <asp:PlaceHolder ID="phProperties" runat="server">
            <div class="clear-fix page-translation-items">
                <div class="form-row">
                    <div class="form-value">
                        <asp:CheckBox ID="chkProperties" runat="server" Text="<%$ Resources:GUIStrings, PageProperties %>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value">
                        <asp:CheckBox ID="chkPageTitle" runat="server" Text="<%$ Resources:GUIStrings, PageTitle %>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value">
                        <asp:CheckBox ID="chkPageDescription" runat="server" Text="<%$ Resources:GUIStrings, PageDescription %>" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-value">
                        <asp:CheckBox ID="chkPageFriendlyName" runat="server" Text="<%$ Resources:GUIStrings, URLFriendlyName %>" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <div class="clear-fix page-translation-items">
            <div class="form-row">
                <div class="form-value">
                    <asp:CheckBox ID="chkMenuTitle" runat="server" Text="<%$ Resources:GUIStrings, MenuTitle %>" />
                </div>
            </div>
            <div class="form-row">
                <div class="form-value">
                    <asp:CheckBox ID="chkMenuUrl" runat="server" Text="<%$ Resources:GUIStrings, MenuFriendlyUrl %>" />
                </div>
            </div>
            <div class="form-row">
                <div class="form-value">
                    <asp:CheckBox ID="chkMenuDescription" runat="server" Text="<%$ Resources:GUIStrings, IncludeMenuDescription %>" />
                </div>
            </div>

        </div>
    </div>
    <div class="form-section bottom-section">
        <h3>
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, TranslationDetails %>" />
            
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:GUIStrings, TranslateFrom %>" />
                </label>
            <div class="form-value text-value">
                <asp:DropDownList ID="ddlTranslateFrom" onchange="ddlTranslate_OnChange()" runat="server"
                    Width="310" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, TranslateTo %>" />
                </label>
            <div class="form-value text-value">
                <asp:DropDownList ID="ddlTranslateTo" onchange="ddlTranslate_OnChange()" runat="server"
                    Width="310" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Instructions %>" />
                </label>
            <div class="form-value">
                <asp:TextBox ID="txtInstructions" runat="server" Width="300" Rows="4" TextMode="MultiLine" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" class="button cancel-button" value="<%= JSMessages.Close %>" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>"
        ToolTip="<%$ Resources:GUIStrings, Save %>" CssClass="primarybutton" OnClick="btnSave_Click" />
</asp:Content>

