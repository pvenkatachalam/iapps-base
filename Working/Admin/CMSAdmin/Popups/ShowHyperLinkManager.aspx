<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ShowHyperLinkManager" StylesheetTheme="General"
    CodeBehind="ShowHyperLinkManager.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master"
    ClientIDMode="Static" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" language="javascript">
        var FullUrl;
        var csSharedPopup = "0";
        var selectedId = '';
        var workLink = null;
        var imageLibraryType;
        var linkImageUrl;

        function ShowLibrary(popup) {
            var isSharepoint = targetLibrary == "SharePoint";
            if (popup == "PageLibrary") {
                document.getElementById("rdPage").checked = true;
                if (isSharepoint)
                    OpeniAppsAdminPopup("SelectSharepointLibrary", "Type=Page", "SelectSPLibrary");
                else
                    OpeniAppsAdminPopup("SelectPageLibraryPopup", "", "SelectPage");
            }
            else if (popup == "FileLibrary") {
                document.getElementById("rdFileLibrary").checked = true;
                if (isSharepoint)
                    OpeniAppsAdminPopup("SelectSharepointLibrary", "Type=File", "SelectSPLibrary");
                else
                    OpeniAppsAdminPopup("SelectFileLibraryPopup", "Resource=File&TargetLibrary=" + targetLibrary, "SelectFile");
            }
            else if (popup == "ImageLibrary") {
                imageLibraryType = 1;
                document.getElementById("rdImageLibrary").checked = true;
                if (isSharepoint)
                    OpeniAppsAdminPopup("SelectSharepointLibrary", "Type=Image", "SelectSPLibrary");
                else
                    OpeniAppsAdminPopup("SelectImageLibraryPopup", "?Resource=Image&TargetLibrary=" + targetLibrary, "SelectImage");
            }
            else if (popup == "CommerceProduct") {
                document.getElementById("rdCommerceProduct").checked = true;
                ShowProductPopup();
            }
            else if (popup == "ImageLibrary_Icon") {
                imageLibraryType = 2;
                OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectImage");
            }
        }

        function SelectPage() {
            selectedId = popupActionsJson.SelectedItems.first();
            
            var pageUrl = popupActionsJson.CustomAttributes["SelectedPageUrl"];
            $("#txtPageName").val(pageUrl);
            $("#txtImageName").val("");
            $("#txtFileName").val("");
            $("#txtExternalUrl").val("");
            $("#txtProductURL").val("");
            if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true)
                setupCBWatchOptions();
        }

        function SelectFile() {
            selectedId = popupActionsJson.SelectedItems.first();

            var fileUrl = popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"];
            $("#txtFileName").val(fileUrl);
            $("#txtPageName").val("");
            $("#txtImageName").val("");
            $("#txtExternalUrl").val("");
            $("#txtProductURL").val("");
            if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true)
                setupAssetWatchOptions(selectedId);
        }

        function SelectImage() {
            var imageUrl = popupActionsJson.CustomAttributes.ImageUrl;

            if (imageLibraryType == 1) {
                selectedId = popupActionsJson.SelectedItems.first();

                $("#txtImageName").val(imageUrl);
                $("#txtPageName").val("");
                $("#txtFileName").val("");
                $("#txtExternalUrl").val("");
                $("#txtProductURL").val("");
                if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true)
                    setupAssetWatchOptions(selectedId);
            }
            else if (imageLibraryType == 2) {
                $("#txtLinkImage").val(imageUrl);
            }
        }

        function RemoveLinkImage() {
            $("#txtLinkImage").val("");
            linkImageUrl = '';
        }

        function SelectSPLibrary() {
            $("#txtPageName").val("");
            $("#txtImageName").val("");
            $("#txtExternalUrl").val("");
            $("#txtFileName").val("");
            $("#txtProductURL").val("");
            $("#txt" + popupActionsJson.CustomAttributes["Type"] + "Name").val(popupActionsJson.CustomAttributes["FileUrl"]);
            if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true)
                setupCBWatchOptions();
        }

        function setupAssetWatchOptions(selId) {
            var watchName = GetAssociatedAssetWatch(selId);
            if (watchName != 'NotFound' && watchName != '') {
                // watch already exists so disply the name and the message
                $('#msgAssetWatch').show();
                $('#msgAssetWatch').text('"' + watchName + '" ' + __JSMessages["AssetWatchAlreadyAssociated"]);
                $('#txtWatchedEventName').hide();
                $('#txtWatchedEventId').val(watchName);
                $('#chkAddWatchedEvent').hide();
                $('#ddlWatches').hide();
            }
            else {
                // watch does not exist so show the text box to get an input and allow to create
                $('#msgAssetWatch').hide();
                $('#txtWatchedEventName').show();
                $('#txtWatchedEventId').val('New');
                $('#chkAddWatchedEvent').show();
                $('#ddlWatches').show();
            }
        }

        function setupCBWatchOptions() {
            $('#ddlWatches').show();
            $('#msgAssetWatch').toggle();
            $('#chkAddWatchedEvent').show();
        }

        function UpdateRadioButton() {
            $("#rdExternalUrl").prop("checked", true);
            $("#txtImageName").val("");
            $("#txtFileName").val("");
            $("#txtPageName").val("");
            $("#txtProductURL").val("");
        }
        //this javascript is duplicated everywhere
        var productSearchPopup;
        function ShowProductPopup() {
            OpeniAppsCommercePopup("SelectProductPopup", "showProducts=true");
        }

        function SelectProductFromPopup(product) {
            if (product != null && product != '' && product != 'undefined') {
                var selectedProductURL = document.getElementById("txtProductURL");
                selectedProductURL.value = jPublicSiteUrl + "/" + product.Url;
                selectedId = product.Id;
                $("#txtPageName").val("");
                $("#txtFileName").val("");
                $("#txtImageName").val("");
                $("#txtExternalUrl").val("");
                if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true)
                    setupCBWatchOptions();
            }
            CloseiAppsCommercePopup();
        }

        function CloseProductPopupFromPopup() {
            CloseiAppsCommercePopup();
        }

        $(function () {
            $(".prop-link").on("click", function () {
                $("#divPopupProperties").iAppsDialog("open");
            });
        });

        function CloseProperties() {
            $("#divPopupProperties").iAppsDialog("close");
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-section">
        <h3>
            <%= GUIStrings.HyperLinkManager_LinkDetails %></h3>
        <div class="form-row">
            <label class="form-label" for="<%=txtLinkTitle.ClientID%>">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, LinkTitleColon %>" /></label>
            <div class="form-value">
                <asp:TextBox ID="txtLinkTitle" runat="server" CssClass="textBoxes" Width="740" />
            </div>
        </div>
        <div class="form-row" id="divLinkImage" runat="server">
            <label class="form-label">
                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, LinkImage %>" /></label>
            <div class="form-value">
                <img src="/Admin/App_Themes/General/images/icon-browse.gif" onclick="ShowLibrary('ImageLibrary_Icon');"
                    alt="<%=GUIStrings.Browse %>" />
                <asp:TextBox ID="txtLinkImage" runat="server" CssClass="textBoxes" Width="562"></asp:TextBox>
                <img src="/Admin/App_Themes/General/images/cm-icon-delete.png" onclick="RemoveLinkImage();"
                    alt="Remove" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, LinkTargetColon %>" /></label>
            <div class="form-value">
                <asp:RadioButton ID="rdPage" runat="server" Text="<%$ Resources:GUIStrings, Page %>"
                    CssClass="radioButtonWidth" GroupName="LinkTarget" />
                <img src="/Admin/App_Themes/General/images/icon-browse.gif" onclick="ShowLibrary('PageLibrary');" alt="<%=GUIStrings.Browse %>" />
                <asp:TextBox ID="txtPageName" runat="server" CssClass="textBoxes" Width="562"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value">
                <asp:RadioButton ID="rdFileLibrary" runat="server" Text="<%$ Resources:GUIStrings, FileInLibrary %>"
                    CssClass="radioButtonWidth" GroupName="LinkTarget" />
                <img src="/Admin/App_Themes/General/images/icon-browse.gif" onclick="ShowLibrary('FileLibrary');" alt="<%= GUIStrings.Browse %>" />
                <asp:TextBox ID="txtFileName" runat="server" CssClass="textBoxes" Width="562"></asp:TextBox>
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value">
                <asp:RadioButton ID="rdImageLibrary" runat="server" Text="<%$ Resources:GUIStrings, ImageInLibrary %>"
                    CssClass="radioButtonWidth" GroupName="LinkTarget" />
                <img src="/Admin/App_Themes/General/images/icon-browse.gif" onclick="ShowLibrary('ImageLibrary');" alt="<%= GUIStrings.ClicktoopenImageLibrary %>" />
                <asp:TextBox ID="txtImageName" runat="server" CssClass="textBoxes" Width="562"></asp:TextBox>
            </div>
        </div>
        <div class="form-row" id="divCommerce" runat="server" clientidmode="Static">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value">
                <asp:RadioButton ID="rdCommerceProduct" runat="server" Text="<%$ Resources:GUIStrings, CommerceProduct %>"
                    CssClass="radioButtonWidth" GroupName="LinkTarget" />
                <img src="/Admin/App_Themes/General/images/icon-browse.gif" onclick="ShowLibrary('CommerceProduct');" alt="<%= GUIStrings.Browse %>" />
                <asp:TextBox ID="txtProductURL" runat="server" CssClass="textBoxes" Width="562"></asp:TextBox>
            </div>
        </div>
        <div class="form-row" id="spanExternal">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value">
                <asp:RadioButton ID="rdExternalUrl" runat="server" Text="<%$ Resources:GUIStrings, ExternalURL %>"
                    CssClass="radioButtonWidth" GroupName="LinkTarget" />
                <asp:TextBox ID="txtExternalUrl" runat="server" CssClass="textBoxes" Width="562"
                    Style="margin-left: 19px;"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form-section">
        <h3>
            <%= GUIStrings.HyperLinkManager_LinkTarget %></h3>
        <div class="form-row">
            <label class="form-label">
                <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, DisplayTargetInColon %>" /></label>
            <div class="form-value">
                <asp:RadioButton ID="rdCurrentWindow" runat="server" Text="<%$ Resources:GUIStrings, CurrentWindow %>"
                    CssClass="radioButtonWidth" GroupName="PopupProperties" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value">
                <asp:RadioButton ID="rdNewWindow" runat="server" Text="<%$ Resources:GUIStrings, NewWindow %>"
                    CssClass="radioButtonWidth" GroupName="PopupProperties" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value">
                <asp:RadioButton ID="rdModal" runat="server" Text="<%$ Resources:GUIStrings, HyperlinkManager_Modal %>"
                    CssClass="radioButtonWidth" GroupName="PopupProperties" />
            </div>
        </div>
        <div class="form-row">
            <label class="form-label">
                &nbsp;</label>
            <div class="form-value multi-column">
                <asp:RadioButton ID="rdPopupWindow" runat="server" Text="<%$ Resources:GUIStrings, PopUp %>"
                    CssClass="radioButtonWidth" GroupName="PopupProperties" />
                <a class="prop-link">Properties</a>
            </div>
        </div>
        <!-- Need to show hide based on analytics license. -->
        <asp:PlaceHolder ID="phGenerateWatch" runat="server">
            <div id="divWatch" class="form-row">
                <label class="form-label">
                    <%= GUIStrings.AttachWatch %></label>
                <div class="form-value">
                    <input id="chkAddWatchedEvent" name="chkAddWatchedEvent" type="checkbox" onchange="fn_ToggleWatchList(this);" />
                    <asp:DropDownList ID="ddlWatches" runat="server" ClientIDMode="Static" Width="300"
                        Style="padding-right: 3px;" onchange="fn_ToggleWatchName(this);">
                        <asp:ListItem Value="Select" Text="<%$ Resources:GUIStrings, __Select__ %>" />
                        <asp:ListItem Value="New" Text="<%$ Resources:GUIStrings, NewWatch %>" />
                        <asp:ListItem Value="" Text="------------------------------------------------------------------------" />
                    </asp:DropDownList>
                    <input id="txtWatchedEventName" name="txtWatchedEventName" type="text" style="width: 370px;
                        padding: 4px;" class="textBoxes" />
                    <input type="hidden" id="txtWatchedEventId" runat="server" name="txtWatchedEventId" />
                    <div id="msgAssetWatch" style="display: none; float: left;">
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
    </div>
    <div id="divPopupProperties" class="popup-properties" style="display: none">
        <div class="modal-header clear-fix">
            <h2>
                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:GUIStrings, PopupProperties %>" /></h2>
        </div>
        <div class="modal-content">
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, Height %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtPopupHeight" runat="server" CssClass="textBoxes" Width="100" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Width %>" />
                </label>
                <div class="form-value">
                    <asp:TextBox ID="txtPopupWidth" runat="server" CssClass="textBoxes" Width="100" />
                </div>
            </div>
            <div class="form-row">
                <label class="form-label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:GUIStrings, Options %>" />
                </label>
                <div class="form-value">
                    <asp:CheckBoxList ID="popUpAttributes" runat="server">
                        <asp:ListItem Text="<%$ Resources:GUIStrings, ShowAddressBarNonIE7 %>" Value=""></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, ShowMenuBar %>" Value=""></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, ShowStatusBar %>" Value=""></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:GUIStrings, AllowScrollBars %>" Value=""></asp:ListItem>
                    </asp:CheckBoxList>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input id="Button1" type="button" runat="server" value="<%$ Resources:GUIStrings, Close %>"
                class="button" onclick="return CloseProperties();" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnCreateLink" runat="server" Text="<%$ Resources:GUIStrings, CreateLink %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, CreateLink %>" CausesValidation="true"
        OnClientClick="return insertLink();" />
    <asp:HiddenField ID="hdnLinkContainer" runat="server" ClientIDMode="Static" />
    <script type="text/javascript">

        if (parent.hasAnalyticsLicense == false || parent.hasAnalyticsPermission == false ||
        (parent.pageState != undefined &&
            (parent.pageState.toLowerCase() == 'emailview' || parent.pageState.toLowerCase() == 'emailedit'))
           ) {
            $('#divWatch').hide();
        }
        var selectedObject;
        if ($("#hdnLinkContainer").val() == "true") {
            selectedObject = parent.SelectedObject;
        }
        else if (EditorToUse.toLowerCase() == 'radeditor') {
            selectedObject = parent.SelectedObject;
        }
        else if (EditorToUse.toLowerCase() == 'ckeditor') {
            selectedObject = parent.SelectedObject;
        }
        var txtTitle = document.getElementById("txtLinkTitle");
        var txtImage = document.getElementById("txtLinkImage");
        var selectedText = selectedObject.SelectedText;
        var selectedLinkImage = selectedObject.SelectedLinkImage;
        var selectedLink = "";
        var selectedTarget = "";
        var selectedHeight = "";
        var selectedWidth = "";
        var showAddressBar = false;
        var showMenuBar = false;
        var showStatusBar = false;
        var allowScrollBar = false;
        var checkValid = true;
        var targetLibrary = typeof (selectedObject.TargetLibrary) != "undefined" ? selectedObject.TargetLibrary : "";
        txtTitle.value = typeof (selectedText) != "undefined" ? selectedText : "";
        if (txtImage != null)
            txtImage.value = typeof (selectedLinkImage) != "undefined" ? selectedLinkImage : "";
        setLinkTarget();
        setPopupLinkAttributes();

        if (targetLibrary == 'SharePoint') {
            var externalTextbox = document.getElementById("spanExternal");
            if (externalTextbox != 'undefined' || externalTextbox != null) {
                externalTextbox.style.display = "none";
            }
        }

        function setLinkTarget() {
            $(".prop-link").hide();
            if (selectedObject.TargetValue == null) {
                document.getElementById("rdCurrentWindow").checked = true;
            }
            else {
                switch (selectedObject.TargetValue) {
                    case "_self":
                        document.getElementById("rdCurrentWindow").checked = true;
                        break;
                    case "_blank":
                        document.getElementById("rdNewWindow").checked = true;
                        break;
                    case "_blankPopUp":
                        document.getElementById("rdPopupWindow").checked = true;
                        $(".prop-link").show();
                        break;
                    case "_modal":
                        document.getElementById("rdModal").checked = true;
                        break;
                }
            }

            if (selectedObject.TypeValue == null)
                return;

            if ((selectedObject.HrefValue == null || selectedObject.HrefValue == '')) {
                if (selectedObject.Id != null && selectedObject.Id != '') {
                    selectedObject.HrefValue = GetUrlBasedIdAndType(selectedObject.TypeValue, selectedObject.Id);
                }
            }

            switch (selectedObject.TypeValue) {
                case "Page":
                    document.getElementById("rdPage").checked = true;
                    if (selectedObject.HrefValue != null) {
                        document.getElementById("txtPageName").value = selectedObject.HrefValue;
                        selectedid = selectedObject.Id;
                    }
                    break;
                case "File":
                    document.getElementById("rdFileLibrary").checked = true;
                    if (selectedObject.HrefValue != null) {
                        document.getElementById("txtFileName").value = selectedObject.HrefValue;
                        selectedid = selectedObject.Id;
                    }
                    break;
                case "ExternalURL":
                    document.getElementById("rdExternalUrl").checked = true;
                    if (selectedObject.HrefValue != null) {
                        document.getElementById("txtExternalUrl").value = selectedObject.HrefValue;
                    }
                    break;
                case "Image":
                    document.getElementById("rdImageLibrary").checked = true;
                    if (selectedObject.HrefValue != null) {
                        document.getElementById("txtImageName").value = selectedObject.HrefValue;
                        selectedid = selectedObject.Id;
                    }
                    break;
                case "Product":
                    document.getElementById("rdCommerceProduct").checked = true;
                    if (selectedObject.HrefValue != null) {
                        document.getElementById("txtProductURL").value = selectedObject.HrefValue;
                        selectedid = selectedObject.Id;
                    }
                    break;
            }

            if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true) {
                if (selectedObject.TypeValue == 'File' || selectedObject.TypeValue == 'Image') {
                    setTimeout(function () {
                        setupAssetWatchOptions(selectedid);
                    }, 100);
                }
                else if ((selectedObject.watchcode != null && selectedObject.watchcode != '' && selectedObject.watchcode != undefined && selectedObject.watchcode.toLowerCase() != ';nowatch')) {
                    var watchvalue = selectedObject.watchcode.toString().split(";");
                    document.getElementById("chkAddWatchedEvent").checked = true;
                    $('#ddlWatches').val(watchvalue[0]);
                    document.getElementById("txtWatchedEventId").value = watchvalue[0];
                    $('#txtWatchedEventName').hide();
                }
            }
        }

        function setPopupLinkAttributes() {

            if (selectedObject.TargetValue == "_blankPopUp") {
                if (selectedObject.showAddressBar == "true")
                    document.getElementById("popUpAttributes_" + 0).checked = true;
                else
                    document.getElementById("popUpAttributes_" + 0).checked = false;

                if (selectedObject.showMenuBar == "true")
                    document.getElementById("popUpAttributes_" + 1).checked = true;
                else
                    document.getElementById("popUpAttributes_" + 1).checked = false;

                if (selectedObject.showStatusBar == "true")
                    document.getElementById("popUpAttributes_" + 2).checked = true;
                else
                    document.getElementById("popUpAttributes_" + 2).checked = false;

                if (selectedObject.allowScrollBar == "true")
                    document.getElementById("popUpAttributes_" + 3).checked = true;
                else
                    document.getElementById("popUpAttributes_" + 3).checked = false;

                document.getElementById("txtPopupHeight").value = selectedObject.Height;
                document.getElementById("txtPopupWidth").value = selectedObject.Width;

            }
        }

        function insertLink() {
            GetSelectedItem();
            GetTarget();
            GetHeightWidth();
            //GetPopUpWindowAtrributes();
            var watchCode = ";NoWatch";
            if (selectedObject.imgControlTag != null) {
                selectedText = "<img src=\"" + selectedText + "\">" + "</img>"
            }
            if ($("#hdnLinkContainer").val() == "true") {
                selectedText = $("#txtLinkTitle").val();
                selectedLinkImage = $("#txtLinkImage").val();
            }
            //Add License Check

            var bCreateWatch = false;
            if (parent.hasAnalyticsLicense == true && parent.hasAnalyticsPermission == true) {
                if (document.getElementById("chkAddWatchedEvent").checked) {
                    // call the callback method to generate the watch by the given name
                    var watchId = document.getElementById("txtWatchedEventId").value;
                    if (watchId == 'New') {
                        var watchName = document.getElementById("txtWatchedEventName").value;
                        if (watchName == '') {
                            alert('<%= JSMessages.PleaseEnterAWatchName %>');
                            checkValid = false;
                            return;
                        }

                    }
                    bCreateWatch = true;
                }
                else {
                    watchCode = ";NoWatch";
                }

                if (bCreateWatch) {
                    watchCode = CreateWatchedEvent(watchName, selectedId, FullUrl, selectedLink, watchId);
                    if (watchCode == 'NameAlreadyExistsOrError') {
                        alert('<%= JSMessages.WatchNameAlreadyExistsOrThereIsAnErrorLinkWillStillBeCreated %>');
                    }
                    var watchvalue = watchCode.toString().split(";");
                    document.getElementById("txtWatchedEventId").value = watchvalue[0];

                }
            }
            var returnValue =
			{
			    url: FullUrl,
			    text: selectedText,
			    type: selectedLink,
			    target: selectedTarget,
			    height: selectedHeight,
			    width: selectedWidth,
			    showAddressBar: showAddressBar,
			    showMenuBar: showMenuBar,
			    showStatusBar: showStatusBar,
			    allowScrollBar: allowScrollBar,
			    Id: selectedId,
			    watchcode: watchCode,
			    linkImage: selectedLinkImage

			};
            // CHECK FOLDER ID TOO
            if (selectedLink == "") {
                checkValid = false;
            }
            else {
                checkValid = true;
            }
            //If validation is done, then close the DialogBox
            //JSON.stringify(popupActionsJson)
            if (checkValid) {
                popupActionsJson.CustomAttributes.LinkReturnValue = JSON.stringify(returnValue);
                SelectiAppsAdminPopup();
            }

            return false;
        }

        function GetSelectedItem() {
            var validurl = true;
            var slectedLinkValue = true;
            FullUrl = "";
            if (document.getElementById("rdPage").checked) {
                selectedLink = "Page";
                FullUrl = document.getElementById("txtPageName").value;
                if (FullUrl == "") {
                    slectedLinkValue = false;
                }
            }
            else if (document.getElementById("rdFileLibrary").checked) {
                selectedLink = "File";
                FullUrl = document.getElementById("txtFileName").value;
                if (FullUrl == "") {
                    slectedLinkValue = false;
                }
            }
            else if (document.getElementById("rdImageLibrary").checked) {
                selectedLink = "Image";
                FullUrl = document.getElementById("txtImageName").value;
                if (FullUrl == "") {
                    slectedLinkValue = false;
                }
            }
            else if (document.getElementById("rdCommerceProduct") != null && document.getElementById("rdCommerceProduct").checked) {
                selectedLink = "Product";
                FullUrl = document.getElementById("txtProductURL").value;
                if (FullUrl == "") {
                    slectedLinkValue = false;
                }
            }
            else if (document.getElementById("rdExternalUrl").checked) {
                selectedLink = "ExternalURL";
                FullUrl = document.getElementById("txtExternalUrl").value;

                if (FullUrl == "") {
                    slectedLinkValue = false;
                }
                else {
                    if (!ValidateUrl(FullUrl)) {
                        alert('<%= JSMessages.TheExternalURLFormatForMenuItemIsInvalid %>');
                        validurl = false;
                    }
                    else {
                        validurl = true;
                    }
                }
            }
            if (selectedLink == "") {
                alert('<%= JSMessages.PleaseSelectATargetForTheHyperlink %>');
            }
            else if (!slectedLinkValue) {
                alert('<%= JSMessages.PleaseEnterTheTargetForTheHyperlink %>');
                selectedLink = "";
            }

            if (!validurl) {
                selectedLink = "";
            }
        }
        function ValidateUrl(urlValue) {
            var v = new RegExp();
            v.compile("^[http|https|ftp]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=\+!,@:]+$");
            if (!v.test(urlValue)) {
                return false;
            }
            else {
                return true;
            }
        }
        function GetTarget() {
            if (document.getElementById("rdCurrentWindow").checked) {
                selectedTarget = "_self";
            }
            else if (document.getElementById("rdNewWindow").checked) {
                selectedTarget = "_blank";
            }
            else if (document.getElementById("rdPopupWindow").checked) {
                selectedTarget = "_blankPopUp";
            }
            else if (document.getElementById("rdModal").checked) {
                selectedTarget = "_modal";
            }
        }

        function GetHeightWidth() {
            if (document.getElementById("rdPopupWindow").checked) {
                if (isNaN(document.getElementById("txtPopupHeight").value)) {
                    alert('<%= JSMessages.PleaseEnterANumberBetween1999ForThePopupWindowHeight %>');
                    checkValid = false;
                }
                if (isNaN(document.getElementById("txtPopupWidth").value)) {
                    alert('<%= JSMessages.PleaseEnterANumberBetween1999ForThePopupWindowWidth %>');
                    checkValid = false;
                }

                selectedHeight = document.getElementById("txtPopupHeight").value;
                selectedWidth = document.getElementById("txtPopupWidth").value;
            }
        }

        function GetPopUpWindowAtrributes() {
            if (document.getElementById("rdPopupWindow").checked) {
                for (i = 0; i < 4; i++) {
                    if (document.getElementById("popUpAttributes_" + i).checked) {
                        if (i == 0)
                            showAddressBar = true;
                        else if (i == 1)
                            showMenuBar = true;
                        else if (i == 2)
                            showStatusBar = true;
                        else if (i == 3)
                            allowScrollBar = true;
                    }
                }
            }
        }

        function EnablepopUpAttributes() {
            if (document.getElementById("rdPopupWindow").checked) {
                $(".prop-link").show();
            }
            else {
                $(".prop-link").hide();
            }
        }

        function fn_ToggleWatchList(objAttachWatch) {
            if ($(objAttachWatch).prop('checked')) {
                $('#ddlWatches').removeAttr('disabled');
            }
            else {
                $('#ddlWatches').prop('disabled', true);
            }
        }
        function fn_ToggleWatchName(objWatchList) {
            if ($(objWatchList).val() == 'New') {   //do not translate
                $('#txtWatchedEventName').show();
            }
            else {
                $('#txtWatchedEventName').hide();
            }
            document.getElementById("txtWatchedEventId").value = $(objWatchList).val();
        }
        $(document).ready(function () {
            if ($('#chkAddWatchedEvent').prop('checked') == false) {
                $('#ddlWatches').prop('disabled', true);
                $('#txtWatchedEventName').hide();
            }
            if (targetLibrary.toLowerCase() == 'sharepoint') {
                $('#divCommerce').hide();
            }
        });
    </script>
</asp:Content>
