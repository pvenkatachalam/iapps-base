<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ViewGroupDefinitions"
    Theme="General" CodeBehind="ViewGroupDefinitions.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ClosePermissionsPopup() {
            parent.dhtmlPopupModal.hide();
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Localize runat="server" Text="<%$ Resources:GUIStrings, ExplainPermissionsDetails %>" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="cancelBtn" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        cssclass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="CanceliAppsAdminPopup(false);" />
</asp:Content>
