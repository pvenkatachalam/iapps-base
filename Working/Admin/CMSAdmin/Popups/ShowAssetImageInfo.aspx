<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    CodeBehind="ShowAssetImageInfo.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ShowAssetImageInfo" Theme="General"
    ClientIDMode="Static" %>

<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <div class="image-row">
        <asp:Image runat="server" ID="imgPreview" AlternateText="Preview" ImageUrl="~/App_Themes/General/images/no-preview.jpg" />
    </div>
    <div class="details-row">
        <h2>
            <asp:Literal ID="ltFileName" runat="server" />&nbsp;|&nbsp;
            <asp:Literal ID="ltFileTitle" runat="server" /></h2>
        <p>
            <asp:Literal ID="ltDescription" runat="server" />
        </p>
        <p>
            <%= GUIStrings.Modified %>:
            <asp:Literal ID="ltUpdated" runat="server" />
        </p>
        <p>
            <%= GUIStrings.Size %>:
            <asp:Literal ID="ltSize" runat="server" />&nbsp;|&nbsp; <%= GUIStrings.Height %>:
            <asp:Literal ID="ltHeight" runat="server" />&nbsp;|&nbsp; <%= GUIStrings.Width %>:
            <asp:Literal ID="ltWidth" runat="server" />
        </p>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        cssclass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
</asp:Content>
