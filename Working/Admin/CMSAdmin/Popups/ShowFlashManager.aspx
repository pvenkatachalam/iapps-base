<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ShowFlashManager" StylesheetTheme="General"
    CodeBehind="ShowFlashManager.aspx.cs" MasterPageFile="~/Popups/iAPPSPopup.Master" ClientIDMode="Static" %>

<asp:Content ID="cntHead" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function ShowLibrary(popup) {
            if (popup == "FileLibrary") {
                OpeniAppsAdminPopup("SelectFileLibraryPopup", "Resource=File", "SelectFile");
            }
            else if (popup == "ImageLibrary") {
                OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "SelectImage");
            }
        }
        function SelectFile() {
            $("#txtFileName").val(popupActionsJson.CustomAttributes["RelativePath"] + "/" + popupActionsJson.CustomAttributes["FileName"]);
        }

        function SelectImage() {
            $("#txtImageName").val(popupActionsJson.CustomAttributes.ImageUrl);
        }
    </script>
</asp:Content>
<asp:Content ID="cntBody" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label" for="<%=txtFileName.ClientID%>">
            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:GUIStrings, FlashUrl %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtFileName" runat="server" CssClass="textBoxes" Width="200" />
            <input type="button" value="<%= GUIStrings.Browse %>" onclick="ShowLibrary('FileLibrary');"
                class="button" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=txtContainerId.ClientID%>">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:GUIStrings, ContainerId %>" />
        </label>
        <div class="form-value">
            <asp:TextBox ID="txtContainerId" runat="server" CssClass="textBoxes" Width="200" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <div class="form-value">
            <em>
                <%= GUIStrings.IdOfTheWrapperHTMLElementShouldBeUniqueForAPage %></em></div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=txtWidth.ClientID%>">
            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:GUIStrings, Width1 %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtWidth" runat="server" CssClass="textBoxes" Width="200" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <div class="form-value">
            <em>
                <%= GUIStrings.WidthOfTheWrapperHTMLElementIdeallySameAsFlashObject %></em></div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=txtHeight.ClientID%>">
            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:GUIStrings, Height1 %>" /></label>
        <div class="form-value">
            <asp:TextBox ID="txtHeight" runat="server" CssClass="textBoxes" Width="200" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <div class="form-value">
            <em>
                <%= GUIStrings.HeightOfTheWrapperHTMLElementIdeallySameAsFlashObject %></em></div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=txtImageName.ClientID%>">
            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:GUIStrings, AlternateImage %>" /></label><div
                class="form-value">
                <asp:TextBox ID="txtImageName" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
                <input type="button" class="button" onclick="ShowLibrary('ImageLibrary');" value="<%= GUIStrings.Browse %>" />
            </div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=txtMinVersion.ClientID%>">
            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:GUIStrings, MinVersion %>" /></label><div
                class="form-value">
                <asp:TextBox ID="txtMinVersion" runat="server" CssClass="textBoxes" Width="200"></asp:TextBox>
            </div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=ddQuality.ClientID%>">
            <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:GUIStrings, Quality1 %>" /></label><div
                class="form-value">
                <asp:DropDownList ID="ddQuality" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Low %>" Value="low"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, High %>" Value="high"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, AutoLow %>" Value="autolow"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, AutoHigh %>" Value="autohigh"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Best %>" Value="best"></asp:ListItem>
                </asp:DropDownList>
            </div>
    </div>
    <div class="form-row">
        <label class="form-label" for="<%=ddAutoPlay.ClientID%>">
            <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:GUIStrings, AutoPlay %>" /></label><div
                class="form-value">
                <asp:DropDownList ID="ddAutoPlay" runat="server">
                    <asp:ListItem Text="<%$ Resources:GUIStrings, No %>" Value="false"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:GUIStrings, Yes %>" Value="true"></asp:ListItem>
                </asp:DropDownList>
                <div style="margin-left: 130px; font-style: italic;">
                    <asp:Localize ID="Localize12" runat="server" Text="" /></div>
            </div>
    </div>
    <div class="form-row">
        <label class="form-label">
            &nbsp;</label>
        <div class="form-value">
            <em><%= GUIStrings.ApplicableOnlyForflvFiles %></em>
        </div>
    </div>
    <script type="text/javascript">
        var selectedObject = parent.selectedFlashObject;
        setFlashProperties();

        function setFlashProperties() {
            if (selectedObject.FilePathValue != null)
                document.getElementById("txtFileName").value = selectedObject.FilePathValue;
            if (selectedObject.ContainerIdValue != null)
                document.getElementById("txtContainerId").value = selectedObject.ContainerIdValue;
            if (selectedObject.WidthValue != null)
                document.getElementById("txtWidth").value = selectedObject.WidthValue;
            if (selectedObject.HeightValue != null)
                document.getElementById("txtHeight").value = selectedObject.HeightValue;
            if (selectedObject.AltImagePathValue != null)
                document.getElementById("txtImageName").value = selectedObject.AltImagePathValue;
            if (selectedObject.MinVersionValue != null)
                document.getElementById("txtMinVersion").value = selectedObject.MinVersionValue;
            if (selectedObject.QualityValue != null)
                setSelectedIndex(document.getElementById("ddQuality"), selectedObject.QualityValue);
            if (selectedObject.AutoPlayValue != null)
                setSelectedIndex(document.getElementById("ddAutoPlay"), selectedObject.AutoPlayValue);
        }

        function setSelectedIndex(dropDownList, selectedValue) {
            for (var i = 0; i < dropDownList.options.length; i++) {
                if (dropDownList.options[i].value == selectedValue) {
                    dropDownList.options[i].selected = true;
                    return;
                }
            }
        }

        function CloseLink() {
            if (EditorToUse.toLowerCase() == 'radeditor') {
                var radObj = getRadWindow();
                if (radObj != null) radObj.close();
            }
            else if (EditorToUse.toLowerCase() == 'ckeditor') {
                parent.fn_InsertFlashInEditor();
            }
            return false;
        }
        function insertFlash() {
            filePath = document.getElementById("txtFileName").value;
            containerId = document.getElementById("txtContainerId").value;
            containerWidth = document.getElementById("txtWidth").value;
            containerHeight = document.getElementById("txtHeight").value;
            altImageName = document.getElementById("txtImageName").value;
            minVersion = document.getElementById("txtMinVersion").value;
            quality = document.getElementById("ddQuality").value;
            autoPlay = document.getElementById("ddAutoPlay").value;

            if (filePath == "") {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleasePickAFlashFile %>'/>");
                document.getElementById("txtFileName").focus();
                return false;
            }
            if (containerId == "" || !(/^[a-z][a-z0-9_\-]*$/i.test(containerId))) {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseEnterAValidIdForContainer %>'/>");
                document.getElementById("txtContainerId").focus();
                return false;
            }
            if (containerWidth == "" || isNaN(containerWidth)) {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseEnterAValidWidthForTheContainer %>'/>");
                document.getElementById("txtWidth").focus();
                return false;
            }
            if (containerHeight == "" || isNaN(containerHeight)) {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseEnterAValidHeightForTheContainer %>'/>");
                document.getElementById("txtHeight").focus();
                return false;
            }
            if (minVersion != "" && isNaN(minVersion)) {
                alert("<asp:localize runat='server' text='<%$ Resources:JSMessages, PleaseEnterAValidVersion %>'/>");
                document.getElementById("txtMinVersion").focus();
                return false;
            }

            var returnValue =
			{
			    filePath: filePath,
			    containerId: containerId,
			    width: containerWidth,
			    height: containerHeight,
			    altImagePath: altImageName,
			    minVersion: minVersion,
			    quality: quality,
			    autoPlay: autoPlay
			};

            popupActionsJson.CustomAttributes.FlashReturnValue = returnValue;
            SelectiAppsAdminPopup();

            return false;
        }			
    </script>
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button" ToolTip="<%$ Resources:GUIStrings, Close %>" OnClientClick="return CanceliAppsAdminPopup();" />
    <asp:Button ID="btnInsert" runat="server" Text="<%$ Resources:GUIStrings, Insert %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Insert %>" CausesValidation="true"
        OnClientClick="return insertFlash();" />
</asp:Content>
