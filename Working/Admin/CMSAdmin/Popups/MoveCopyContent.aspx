<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.master" AutoEventWireup="true"
    CodeBehind="MoveCopyContent.aspx.cs" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.MoveCopyContent" Theme="General" ClientIDMode="Static" %>
<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript" language="javascript">
        var fromMenuId;
        var toMenuId;
        var dragContentId;
        function onCopyContentBtnClick() {
            var copyResult = CopyContent(dragContentId, toMenuId);
            if (copyResult != null) {
                if (copyResult == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, TheContentHasBeenSuccessfullyCopied %>'/>");
                    CanceliAppsAdminPopup(true);
                }
                else {
                    alert("<%=JSMessages.ErrorCopyActionFailed %>");
                    CanceliAppsAdminPopup();
                }
            }
        }

        function onMoveContentBtnClick() {            
            var moveResult = MoveContent(dragContentId, fromMenuId, toMenuId);            
            if (moveResult != null) {
                if (moveResult == "True") {
                    alert("<asp:Localize runat='server' Text='<%$ Resources:JSMessages, TheContentHasBeenSuccessfullyMoved %>'/>");
                    CanceliAppsAdminPopup(true);
                }
                else {
                    alert("<%=JSMessages.ErrorMoveActionFailed %>");
                    CanceliAppsAdminPopup();
                }
            }
        }  
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:Label ID="deleteContentMessagelbl" runat="server" Text="<%$ Resources:GUIStrings, AreYouSureDoYouWantToMoveTheContent %>"></asp:Label>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Close %>" cssclass="button cancel-button"
        ToolTip="<%$ Resources:GUIStrings, Close %>"  />
    <asp:Button ID="btnMove" Text="<%$ Resources:GUIStrings, Move %>" runat="server" Cssclass="primarybutton" 
        ToolTip="<%$ Resources:GUIStrings, Move %>" OnClientClick="onMoveContentBtnClick();" />
    <asp:Button ID="btnCopy" Text="<%$ Resources:GUIStrings, Copy %>" runat="server" Cssclass="primarybutton" 
        ToolTip="<%$ Resources:GUIStrings, Copy %>" OnClientClick="onCopyContentBtnClick();" />
</asp:Content>
