﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="ApplyMutliCSSClasses.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ApplyMutliCSSClasses" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            if (EditorToUse.toLowerCase() == 'radeditor') {
                var arguments = parent.o_Arguments;
                $('#divSelectedTag').html($(arguments.elem).get(0).tagName);
                $('#txtAppliedCssClasses').val($(arguments.elem).prop("class"));
                $(arguments.classes).each(function (i, e) {
                    var Option = "<option>" + e + "</option>";
                    $("#ddlAvailableCssClasses").append(Option);
                });
            }
            else if (EditorToUse.toLowerCase() == 'ckeditor') {
            }
            $('#btnClearCssClass').on('click', function () {
                $("#txtAppliedCssClasses").val('');
            });
            $('#btnAddCssClasses').on('click', function () {
                $("#ddlAvailableCssClasses option:selected").each(function () {
                    $("#txtAppliedCssClasses").val($("#txtAppliedCssClasses").val() + " " + $(this).val());
                });
            });
            $('#btnApplyCss').on('click', function () {
                parent.fn_AssignCssClasses($("#txtAppliedCssClasses").val());
                CanceliAppsAdminPopup(false);
            });
        });
        
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <div class="form-row">
        <label class="form-label">Selected Tagname:</label>
        <div class="form-value text-value" id="divSelectedTag">&nbsp;</div>
    </div>
    <div class="form-row">
        <label class="form-label">Available CSS Classes:<br /><em>(Ctrl click to select multiple)</em></label>
        <div class="form-value">
            <select id="ddlAvailableCssClasses" multiple="multiple" style="float: left;"></select>
            <input type="button" id="btnAddCssClasses" value="Add" class="button small-button" style="float: left; margin-left: 10px;" />
        </div>
    </div>
    <div class="form-row">
        <label class="form-label">Applied CSS Classes:</label>
        <div class="form-value">
            <input type="text" id="txtAppliedCssClasses" style="float: left;" />
            <input type="button" id="btnClearCssClass" value="Clear" class="button small-button" style="float: left; margin-left: 10px;" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <input type="button" id="btnCancel" class="button cancel-button" value="Cancel" />
    <input type="button" id="btnApplyCss" class="primarybutton" value="Apply CSS" />
</asp:Content>
