﻿<%@ Page Language="C#" MasterPageFile="~/Popups/iAPPSPopup.Master" AutoEventWireup="true" CodeBehind="FileUploader.aspx.cs"
    Inherits="Bridgeline.iAPPS.Admin.Common.Web.Popups.FileUploader" StylesheetTheme="General" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <asp:ValidationSummary ID="valSummary" runat="server" ShowSummary="false" ShowMessageBox="True" ValidationGroup="vgUpload" />
    <div class="form-row">
        <label class="form-label">
            <%= GUIStrings.FileName%></label>
        <div class="form-value">
            <asp:FileUpload ID="fileUpload" ToolTip="<%$ Resources:GUIStrings, Browse %>" runat="server"
                CssClass="file-upload" />
            <asp:RequiredFieldValidator ID="reqfileUpload" ControlToValidate="fileUpload" runat="server"
                ErrorMessage="<%$ Resources:JSMessages, PleaseSelectAFile %>" Display="None"
                SetFocusOnError="true" ValidationGroup="vgUpload" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" runat="server" CssClass="button cancel-button" />
    <asp:Button ID="btnUpload" runat="server" Text="<%$ Resources:GUIStrings, Upload %>" CssClass="primarybutton"
        ToolTip="<%$ Resources:GUIStrings, Upload %>" ValidationGroup="vgUpload" />
</asp:Content>
