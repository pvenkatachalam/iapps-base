<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.SelectListPopup" StylesheetTheme="General"
    CodeBehind="SelectListPopup.aspx.cs" EnableEventValidation="false" MasterPageFile="~/Popups/iAPPSPopup.Master" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="server">
    <script type="text/javascript">
        function grdList_OnLoad(sender, eventArgs) {
            $(".select-list").gridActions({ objList: grdList });
        }

        function grdList_OnRenderComplete(sender, eventArgs) {
            $(".select-list").gridActions("bindControls");
        }

        function grdList_OnCallbackComplete(sender, eventArgs) {
            $(".select-list").gridActions("displayMessage");
        }

        function SelectListFromLibrary() {
            if (gridActionsJson.SelectedItems.length > 0)
                SelectiAppsAdminPopup();
            else
                alert("<%= JSMessages.PleaseSelectAList %>");

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphContent" runat="server">
    <iAppsControls:GridActions ID="gridActionsTop" runat="server" />
    <div class="grid-section">
        <ComponentArt:Grid ID="grdList" SkinID="Default" runat="server" Width="100%" RunningMode="Callback"
            ShowFooter="false" LoadingPanelClientTemplateId="grdListLoadingPanelTemplate"
            CssClass="fat-grid">
            <ClientEvents>
                <Load EventHandler="grdList_OnLoad" />
                <CallbackComplete EventHandler="grdList_OnCallbackComplete" />
                <RenderComplete EventHandler="grdList_OnRenderComplete" />
            </ClientEvents>
            <Levels>
                <ComponentArt:GridLevel DataKeyField="Id" ShowSelectorCells="false" ShowHeadingCells="false"
                    RowCssClass="row" ColumnReorderIndicatorImageUrl="reorder.gif" DataCellCssClass="cell"
                    SelectedRowCssClass="selected" SortAscendingImageUrl="asc.gif" SortDescendingImageUrl="desc.gif"
                    SortImageWidth="8" SortImageHeight="7" AllowGrouping="false" AlternatingRowCssClass="alternate-row"
                    HoverRowCssClass="hover-row">
                    <Columns>
                        <ComponentArt:GridColumn DataField="Title" DataCellServerTemplateId="PopupTemplate"
                            IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Description" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="ListType" Visible="false" IsSearchable="true" />
                        <ComponentArt:GridColumn DataField="Id" Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
            <ServerTemplates>
                <ComponentArt:GridServerTemplate ID="PopupTemplate" runat="server">
                    <Template>
                        <div class="modal-grid-row grid-item clear-fix">
                            <div class="first-cell">
                                <h4>
                                    <%# Container.DataItem["Title"]%></h4>
                            </div>
                            <div class="last-cell">
                                <%# String.Format("<strong>{0}</strong> : {1}", GUIStrings.Type, Container.DataItem["ListType"])%>
                            </div>
                        </div>
                    </Template>
                </ComponentArt:GridServerTemplate>
            </ServerTemplates>
            <ClientTemplates>
                <ComponentArt:ClientTemplate ID="grdListLoadingPanelTemplate">
                    ## GetGridLoadingPanelContent(grdList) ##
                </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    </div>
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="cancelList" runat="server" Text="<%$ Resources:GUIStrings, Close %>"
        CssClass="button  cancel-button" ToolTip="<%$ Resources:GUIStrings, Close %>" />
    <asp:Button ID="selectList" runat="server" Text="<%$ Resources:GUIStrings, SelectList %>"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, SelectList %>" OnClientClick="return SelectListFromLibrary();" />
</asp:Content>
