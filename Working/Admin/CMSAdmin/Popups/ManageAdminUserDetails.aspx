<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Popups/iAPPSPopup.master"
    Theme="General" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.Popups.ManageAdminUserDetails" CodeBehind="ManageAdminUserDetails.aspx.cs" %>

<%@ Register TagPrefix="UC" TagName="UserDetails" Src="~/UserControls/Administration/User/ManageAdminUserDetails.ascx" %>
<asp:Content ID="headerContent" ContentPlaceHolderID="cphHead" runat="server">
<script type="text/javascript">
    
</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="cphContent" runat="Server">
    <UC:UserDetails ID="userDetails" runat="server" />
</asp:Content>
<asp:Content ID="footer" ContentPlaceHolderID="cphFooter" runat="server">
    <asp:Button ID="btnCancel" runat="server" Text="<%$ Resources:GUIStrings, Cancel %>" CausesValidation="false"
        CssClass="button cancel-button" ToolTip="<%$ Resources:GUIStrings, Cancel %>" />
    <asp:Button ID="btnSave" runat="server" Text="<%$ Resources:GUIStrings, Save %>" CausesValidation="true" OnClientClick ="return ValidateEditUser();"
        CssClass="primarybutton" ToolTip="<%$ Resources:GUIStrings, Save %>"  />
</asp:Content>
