<%@ Page Language="C#" AutoEventWireup="true" Inherits="Bridgeline.iAPPS.Admin.CMS.Web.UploadProgress" Theme="General" 
    Codebehind="UploadProgress.aspx.cs" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title><asp:localize runat="server" text="<%$ Resources:GUIStrings, UploadProgress %>"/></title>
<script type="text/javascript">
    var done=true;
    var firstLoad = true;
    function callback_complete(sender, eventargs)
    {
     
       if(!done)
        {
      
            if(!firstLoad)
                pausecomp(800);
                
            callbackStatus.callback();
            done=true;
        }
        firstLoad = false;
      //  InformParent();
    }    
    function pausecomp(millis) 
    {
        var date = new Date();
        var curDate = null;

        do { curDate = new Date(); } 
        while(curDate-date < millis);
         
    }
    function callback_error(sender, eventargs)
    {
           done=false;
           
    }    
    function DoCallback()
    {
        callbackStatus.callback();
    }    
    function InformParent()
    {
    window.opener.RefreshTree() ;
 // window.opener.execScript("RefreshTree()","javascript");
    }    
</script>
</head>
<body onload="DoCallback();" onunload="InformParent();" class="popupBody">
    <form id="form1" runat="server">
        <div class="popupBoxShadow browserWindow" style="width:273px;padding-right:4px;">
            <div class="popupboxContainer">
                <div class="dialogBox">
                    <h3><asp:localize runat="server" text="<%$ Resources:GUIStrings, UploadStatus %>"/></h3>
                    <ComponentArt:CallBack ID="callbackStatus" runat="server" Height="250" Width="250" OnCallback="callbackStatus_Callback" style="overflow:auto;">
                        <ClientEvents>
                            <CallbackComplete EventHandler="callback_complete" />
                            <CallbackError EventHandler="callback_error" />
                        </ClientEvents>
                        <Content>
                            <asp:Image ID="imgProgress" runat="server" ImageUrl="~/App_Themes/General/images/loading_bars.gif" />
                            <asp:Label id="lblUploadStatus" runat="server" CssClass="" />
                        </Content>
                    </ComponentArt:CallBack>
                    <div class="footerContent">
                        <asp:Button ToolTip="<%$ Resources:GUIStrings, Close %>" ID="btnClose" Text="<%$ Resources:GUIStrings, Close %>" Cssclass="primarybutton" 
                            runat="server" Visible="true" OnClientClick="window.close();" />
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
