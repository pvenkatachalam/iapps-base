﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using Bridgeline.FW.Global.Diagnostics;
using System.Linq;
using System.Text;

public partial class CheckWeb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Bridgeline.FW.Library.DAL dal = new Bridgeline.FW.Library.DAL();
        Bridgeline.FW.Library.DBParams parm = new Bridgeline.FW.Library.DBParams();
        dal.SetParameters(parm);
        dal.ExecuteScalar("Site_GetSite_Count");
        DiagnosticsManager diagManager = new DiagnosticsManager();
        var testResults = diagManager.RunTests(HealthCheckGroup.Checkweb);
        var failedTests = from tr in testResults
                          where tr.Result== HealthCheckResult.Error
                              select tr;
        if (failedTests.Count() > 0)
        { 
            StringBuilder errorMessage = new StringBuilder();
            foreach(var test in failedTests)
            {
                errorMessage.AppendLine(string.Format("{0} {1} ",test.OutputMessage,test.HelpText));
            }
            throw new Exception(errorMessage.ToString());
        }
    }
}
