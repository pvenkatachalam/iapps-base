<%@ Page Language="C#" AutoEventWireup="true" Inherits="ErrorPage" Theme="General" Codebehind="ErrorPage.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="iappsCMheaderWrapper" id="headerWrapper">
            <div class="iappsCMheader">
            </div>
        </div>
        <div class="iappsCMuserInformationWrapper" id="userInformationWrapper">
            <div class="iappsCMheaderUserInformation">
            </div>
        </div>
        <div class="iappsCMwrapper" id="wrapper">
            <div class="iappsCMcontentSection">
                <div class="error">
                    <div class="boxShadow">
                        <div class="boxContainer">
                            <div class="boxHeader">
                                <h5>
                                    Error</h5>
                            </div>
                            <div class="paddingContainer">
                                <div class="boxContent">
                                    <p>
                                        We are sorry, but we were unable to service your request. You may contact the administrator
                                        or go to the
                                        <asp:HyperLink ID="gotoControlCenter" runat="server" Text="Control Center" NavigateUrl="javascript:redirectToHome();"></asp:HyperLink>
                                        and try again.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Clear Fix -->
            <div class="clearFix">
            </div>
        </div>
        <!-- Footer Starts -->
        <div class="iappsCMfooterWrapper" id="footerWrapper">
            <div class="iappsCMfooter" id="footer">
                <div class="iappsCMfooterImages">
                    <span class="logo1">
                        <img src="~/App_Themes/General/images/bridgeline-logo.gif" title="Bridgeline" alt="Bridgeline"
                            id="logo1" runat="server" />
                    </span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
