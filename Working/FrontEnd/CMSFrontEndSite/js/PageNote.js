﻿var offsetX = null;
var offsetY = null;
var pageNote = document.getElementById("pageNote");
var pageNoteList = document.getElementById("pageNoteList").getElementsByTagName("li");
var pageNoteInstructions = document.getElementById("pageNoteInstructions");
var pageNoteTextbox = document.getElementById("txtPageNote");
var pageNoteHighPriorityCheckbox = document.getElementById("chkPageNoteHighPriority");
var isPageNoteFocused = false;
var isPageNoteCollapsed = false;
var isPageNotePinned = false;
var lastMessageDatePosted = "01/01/2000 12:00:00.000 AM";
var opacityTimer;

if (browser == "ie") {
    pageNoteTextbox.onfocus = PageNote_Focus;
    pageNoteTextbox.onblur = PageNote_Blur;

    pageNoteInstructions.onclick = PageNote_HideInstructions;

    pageNote.style.filter = "alpha(opacity='100')";
}
else {
    pageNoteTextbox.addEventListener("focus", PageNote_Focus, true);
    pageNoteTextbox.addEventListener("blur", PageNote_Blur, true);

    pageNoteInstructions.addEventListener("click", PageNote_HideInstructions, true);

    pageNote.style.opacity = 1;
}

PageNote_GetAll();

PageNote_Position();

PageNote_ShowInstructions();

opacityTimer = setTimeout(function () { SetPageNoteOpacity(false) }, 2000);

function ShowHidePageNote() {
    clearTimeout(opacityTimer);

    if (pageNote.style.display == "none") {
        pageNote.style.display = "block";

        if (browser == "ie")
            pageNote.style.filter = "alpha(opacity='100')";
        else
            pageNote.style.opacity = 1;

        opacityTimer = setTimeout(function () { SetPageNoteOpacity(false) }, 2000);
    }
    else
        pageNote.style.display = "none";

    PageNote_Position();

    return false;
}

function PageNote_Position() {
    var browserWidth = document.body.clientWidth;

    pageNote.style.left = (browserWidth - pageNote.offsetWidth - 50) + "px";
    pageNote.style.top = "50px";

    var pageNoteListContainer = document.getElementById("pageNoteList");
    pageNoteListContainer.scrollTop = pageNoteListContainer.scrollHeight;
}

function PageNote_TogglePin() {
    toggleClass(pageNote, "pinned");

    isPageNotePinned = !isPageNotePinned;
}

function PageNote_ExpandCollapse() {
    toggleClass(pageNote, "collapsed");

    isPageNoteCollapsed = !isPageNoteCollapsed
}

function SetPageNoteOpacity(isMouseOver) {
    clearTimeout(opacityTimer);
    var opacityLayer = pageNote;

    if (!isPageNoteFocused && !isPageNotePinned) {
        if (browser == "ie") {
            if (!isMouseOver)
                opacityTimer = setTimeout(function () { PageNote_FadeOut(opacityLayer, 100); }, 2000);
            else
                opacityLayer.style.filter = "alpha(opacity='100')";
        }
        else {
            if (!isMouseOver)
                opacityTimer = setTimeout(function () { PageNote_FadeOut(opacityLayer, 1); }, 2000);
            else
                opacityLayer.style.opacity = 1;
        }
    }
}

function PageNote_FadeOut(opacityLayer, currentOpacity) {
    clearTimeout(opacityTimer);

    if (browser == "ie") {
        if (currentOpacity <= 25) {
            opacityLayer.style.filter = "alpha(opacity='25')";
            clearTimeout(opacityTimer);
        }
        else {
            var newOpacity = currentOpacity - 5;
            opacityLayer.style.filter = "alpha(opacity='" + newOpacity + "')";
            opacityTimer = setTimeout(function () { PageNote_FadeOut(opacityLayer, newOpacity) }, 125);
        }
    }
    else {
        if (currentOpacity <= 0.25) {
            opacityLayer.style.opacity = 0.25;
            clearTimeout(opacityTimer);
        }
        else {
            var newOpacity = currentOpacity - 0.05;
            opacityLayer.style.opacity = newOpacity;
            opacityTimer = setTimeout(function () { PageNote_FadeOut(opacityLayer, newOpacity) }, 125);
        }
    }
}

function PageNote_Focus() {
    isPageNoteFocused = true;

    PageNote_HideInstructions();

    SetPageNoteOpacity();
}

function PageNote_Blur() {
    if (PageNote_ValidateText(pageNoteTextbox.value)) {
        isPageNoteFocused = false;

        PageNote_ShowInstructions();

        SetPageNoteOpacity();
    }
    else {
        setTimeout(function () { pageNoteTextbox.focus() }, 10);
        return false;
    }
}

function PageNote_ValidateText(value) {
    if (/<(.|[\n\r])+>/m.test(value)) {
        alert(__JSMessages["WorkflowNotesCannotContainHTMLTags"]);
        return false;
    }
    else
        return true;
}

function MovePageNote(evt) {
    evt = evt || window.event;

    offsetX = evt.clientX - parseInt(pageNote.style.left);
    offsetY = evt.clientY - parseInt(pageNote.style.top);

    if (browser == "ie") {
        document.onmousemove = MovePageNote_Move;
        pageNote.onmouseup = MovePageNote_Release;

        document.onselectstart = function () { return false; }
    }
    else {
        document.addEventListener("mousemove", MovePageNote_Move, true);
        pageNote.addEventListener("mouseup", MovePageNote_Release, true);

        document.onmousedown = function () { return false; }
    }
}

function MovePageNote_Move(evt) {
    evt = evt || window.event;

    var newX = evt.clientX - offsetX;
    var newY = evt.clientY - offsetY;

    pageNote.style.left = newX + "px";
    pageNote.style.top = newY + "px";
}

function MovePageNote_Release(evt) {
    offsetX = null;
    offsetY = null;

    if (browser == "ie") {
        document.onmousemove = null;
        pageNote.onmouseup = null;

        document.onselectstart = null;
    }
    else {
        document.removeEventListener("mousemove", MovePageNote_Move, true);
        pageNote.removeEventListener("mouseup", MovePageNote_Release, true);

        document.onmousedown = null;
    }
}

function PageNote_HideInstructions() {
    pageNoteInstructions.style.display = "none";
    setTimeout(function () { pageNoteTextbox.focus() }, 10);
}

function PageNote_ShowInstructions() {
    if (pageNoteTextbox.value == "")
        pageNoteInstructions.style.display = "block";
    else
        pageNoteInstructions.style.display = "none";
}

function PageNote_GetAll() {
    try {
        result = eval(GetAllPageNotes(pageDefinitionId));

        if (result != null) {
            for (var i = 0; i < result.length; i++) {
                PageNote_AddToList(result[i]);
            }

            if (result.length > 0 && showPageNoteByDefault) {
                lastMessageDatePosted = result[result.length - 1].FullDate;

                var lastDate = new Date(lastMessageDatePosted);
                var triggerDate = new Date(triggerDateString);

                if (lastDate >= triggerDate)
                    pageNote.style.display = "block";
            else
                pageNote.style.display = "none";

                var pageNoteListContainer = document.getElementById("pageNoteList");
                pageNoteListContainer.scrollTop = pageNoteListContainer.scrollHeight;
            }
            else
                pageNote.style.display = "none";
        }
        else
            pageNote.style.display = "none";
    }
    catch (error) {
        PageNote_OnError(__JSMessages["ErrorRetrievingPageNotes"]);
    }

}

function PageNote_GetSince() {
    try {
        result = eval(GetPageNotesSince(pageDefinitionId, lastMessageDatePosted));

        for (var i = 0; i < result.length; i++) {
            PageNote_AddToList(result[i]);
        }

        if (result.length > 0) {
            lastMessageDatePosted = result[result.length - 1].FullDate;

            var pageNoteListContainer = document.getElementById("pageNoteList");
            pageNoteListContainer.scrollTop = pageNoteListContainer.scrollHeight;
        }
    }
    catch (error) {
        PageNote_OnError(__JSMessages["ErrorRetrievingPageNotes"]);
    }
}

function PageNote_Insert() {
    if (/\w/.test(pageNoteTextbox.value)) {
        try {
            result = eval(InsertPageNote(pageDefinitionId, userId, pageNoteHighPriorityCheckbox.checked, PageNote_FormatHtml(pageNoteTextbox.value)));

            pageNoteTextbox.value = "";
            PageNote_ShowInstructions();

            PageNote_GetSince();
        }
        catch (error) {
            PageNote_OnError(__JSMessages["ErrorSubmittingPageNote"]);
        }
    }
    else {
        alert(__JSMessages["PleaseEnterAComment"]);
        setTimeout(function () { pageNoteTextbox.focus() }, 10);
    }
}

function PageNote_OnError(error) {
    alert(error);
}

function PageNote_AddToList(newNote) {
    var newListItem;

    if (newNote.HighPriority)
        newListItem = "<li class=\"highPriority\">\n";
    else
        newListItem = "<li>\n";

    newListItem += "<span class=\"pageNoteText\">"
                        + newNote.Note + "</span>"
                    + "\n<span class=\"pageNoteCommentInfo\">"
                        + "<span class=\"pageNoteAuthor\">"
                            + newNote.UserName
                        + "</span> on <span class=\"pageNoteDate\">"
                            + newNote.DatePosted
                        + "</span> at <span class=\"pageNoteDate\">"
                            + newNote.TimePosted
                        + "</span>.</span>\n"
                        + "<div class=\"clearFix\">&nbsp;</div>\n"
                + "</li>";
    var pageNoteListContainer = document.getElementById("pageNoteList").getElementsByTagName("ul");
    pageNoteListContainer[0].innerHTML += newListItem;
}

function PageNote_FormatHtml(input) {
    var output = "<p>" + jQuery.trim(input) + "</p>";

    output = output.replace(/\n\n/g, "</p><p>");
    output = output.replace(/\n/g, "<br />");

    return output;
}

function toggleClass(element, className) {
    var regex = new RegExp("\\b" + className + "\\b");
    var hasClass = regex.test(element.className);

    if (hasClass)
        element.className = element.className.replace(regex, "");
    else {
        if (element.className == "" || element.className == null)
            element.className = className;
        else
            element.className += " " + className;
    }

    element.className = element.className.replace(/^\s+|\s+$/g, "");
}
