﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateBuilder.aspx.cs"
    ValidateRequest="false" Theme="General" Inherits="CMSFrontEndSite.TemplateBuilder"
    ClientIDMode="Static" %>

<%@ Register Src="ContextMenuUserControls/Toolbox.ascx" TagName="Toolbox" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>iAPPS Content Manager: Template Builder</title>
    <asp:PlaceHolder ID="phLinkTags" runat="server" />
    <asp:PlaceHolder ID="phScriptTags" runat="server" />
    <FWControls:FWScriptHolder ID="phIncludeScriptBlock" runat="server" />
    <script type="text/javascript" src="/jsfile/basepage.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scTemplateBuilder" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>
    <script type="text/javascript">
        var iapps_template_container_modified = false;
        // toolboxDataJson is rendered from the server side.
        function getNewId(callback) {
            $.ajax({
                type: "POST",
                url: "/TemplateBuilder.aspx/GetNewId",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    callback.call(this, msg.d);
                }
            });
        }

        function iapps_showPropertiesWindow(containerImg) {
            $('#propEditor').empty();
            var zoneItemId = $(containerImg).prop("id");
            var imgId = $(containerImg).prop("id").split("_");
            var zoneId = imgId[0];

            $('#propEditor').prop('ZoneId', zoneId);
            $('#propEditor').prop('ZoneItemId', zoneItemId);
            var selectedStyle;
            $.each(zonesDataJson, function () {
                if (this.ZoneId == zoneId) {
                    var containerStyles = GetZoneBasedContainerStyles(this);
                    $.each(this["ZoneItems"], function () {
                        if (this.ZoneItemId == zoneItemId) {
                            if (this.Properties.length == 0 && containerStyles.length == 0)
                                $('#iapps-control-properties').hide();
                            else {
                                $('#iapps-control-properties').show();
                                selectedStyle = this.ItemCssClass;
                                $.each(this["Properties"], function () {
                                    createPropertyHtml(this["Name"], this["Options"]);
                                });
                            }
                        }
                    })
                    if (containerStyles.length > 0)
                        createContainerStylesHtml(this.ContainerStyles, selectedStyle);
                }
            });
        }

        function createContainerStylesHtml(containerStyles, selectedStyle) {
            var markup = '<div id="propRow" class="iapps-form-row"><label id="cssRowLabel" class="iapps-form-label">Css Class:</label><div id="cssRowValue" class="iapps-form-value"></div><div class="iapps-clear-fix"></div></div>';
            $(markup).appendTo('#propEditor');
            $('#cssRowValue').append('<select id="ddlCssValue"></select>');
            $.each(containerStyles, function () {
                $('#ddlCssValue').append(
                        $('<option></option').val(this.toString()).html(this.toString()));
                if (selectedStyle) {
                    if (this.toString() == selectedStyle) {
                        $('#ddlCssValue' + ' option[value="' + this.toString() + '"]').attr('selected', 'selected');
                    }
                }
            }
            );

        }

        function createPropertyHtml(name, options) {
            var markup = '<div id="propRow" class="iapps-form-row"><label id="propRowLabel" class="iapps-form-label"> </label><div id="propRowValue" class="iapps-form-value"> </div><div class="iapps-clear-fix"></div></div>';

            $(markup).appendTo('#propEditor')
                .prop('id', 'propRow_' + name)
                .children().each(function () {
                    this.id = this.id + '_' + name;
                });

            var propRowLabelID = 'propRowLabel_' + name;
            $('#' + propRowLabelID).html(name + ': ');

            var propRowValueID = 'propRowValue_' + name;
            var selectBoxId = 'ddlPropValue_' + name;
            $('#' + propRowValueID).append('<select id="' + selectBoxId + '"></select>');

            $.each(options, function () {
                $('#' + selectBoxId).append(
                    $('<option></option').val(this.OptionName).html(this.OptionName));
                if (this.IsSelected) {
                    $('#' + selectBoxId + ' option[value="' + this.OptionName + '"]').attr('selected', 'selected');
                }
            }
            );
        }

        function saveControlProperties(close) {
            var zoneItemId = $('#propEditor').prop('ZoneItemId');
            var selectedCssValue = $('#ddlCssValue').val();
            $.each(zonesDataJson, function () {
                var dropZone = this;
                $.each(dropZone.ZoneItems, function () {
                    if (this.ZoneItemId == zoneItemId) {
                        if (selectedCssValue)
                            this.ItemCssClass = selectedCssValue;
                        $.each(this["Properties"], function () {
                            var propName = this.Name;
                            var selectedValue = $('#' + 'ddlPropValue_' + propName).val();
                            $.each(this["Options"], function () {
                                if (this.OptionName == selectedValue) {
                                    this.IsSelected = true;
                                }
                            });
                        });
                    }
                });
            });

            if (close) {
                $("#iapps-control-properties").hide();
                $(".iapps-drop-zone img").removeClass("selected");
            }

            iapps_template_container_modified = true;
        }

        var isTemplateChanged = false;
        $(function () {
            isTemplateChanged = false;
            $('#iapps-control-properties').hide();
            $(".iapps-drop-zone").sortable({
                forcePlaceHolderSize: true,
                revert: true,
                stop: function (evt, ui) {
                    iapps_template_container_modified = true;

                    if (!ui.item.attr("widgetfqn")) //For sorting nothing to be done
                        return;
                    var dropZoneId = $(this).attr("id");
                    var noOfControls = $(this).find("img").length;
                    $.each(zonesDataJson, function () {
                        if (this.ZoneId == dropZoneId) {
                            var dropZone = this;
                            if (dropZone.MaxAllowedControls > 0 && noOfControls >= dropZone.MaxAllowedControls) {
                                ui.item.remove();
                                alert("<%= Bridgeline.iAPPS.Resources.JSMessages.Template_ZoneMaxLimitReached %>");
                            }
                            else if (IsZoneRestricted(dropZone, ui.item.attr("widgetfqn"))) {
                                ui.item.remove();
                                alert("<%= Bridgeline.iAPPS.Resources.JSMessages.Template_ZoneControlRestricted %>");
                            }
                            else {
                                var controlAttr = JSON.parse(JSON.stringify(toolboxDataJson[ui.item.attr("widgetfqn")]));
                                getNewId(function (generatedId) {
                                    var newId = dropZoneId + '_' + generatedId;
                                    ui.item.replaceWith(stringformat("<img src='{0}' alt='{1}' id='{2}' />",
                                    controlAttr.ItemDropzoneImageUrl, controlAttr.TypeQualifiedName, newId));

                                    controlAttr.ZoneItemId = newId;
                                    dropZone.ZoneItems.push(controlAttr);

                                    $("#" + newId).trigger("click");
                                    saveControlProperties(false);
                                });

                                isTemplateChanged = true;
                            }
                        }
                    });
                }
            });

            $("#recycle-bin").droppable({
                drop: function (evt, ui) {
                    if ($(ui.draggable).parent().hasClass("iapps-drop-zone")) {
                        var deletedItemId = $(ui.draggable).attr("id");
                        RemoveContainer(deletedItemId);
                    }
                }
            });

            //if (!$.browser.msie || ($.browser.msie && $.browser.version >= 9)) {
                $("#iapps-tool-box").draggable({
                    scroll: false,
                    containment: 'body'
                });
            //}

            $('#iapps-control-properties').draggable();
            $(".iapps-tool-box-item").draggable({
                revert: "invalid",
                helper: "clone",
                opacity: 0.5,
                connectToSortable: ".iapps-drop-zone"
            });

            try {
                $(".iapps-float-box .iapps-float-box-close").live("click", function () {
                    $(this).parents(".iapps-float-box").hide();
                });

                $(".iapps-drop-zone img").live("click", function () {
                    $(".iapps-drop-zone img").removeClass("selected");
                    $(this).addClass("selected");

                    iapps_showPropertiesWindow($(this));
                });

                $(document).live("keypress", function (e) {
                    if ($(".iapps-drop-zone img.selected").length > 0 && (e.keyCode == 46 || e.keyCode == 8)) {
                        var deletedItemId = $(".iapps-drop-zone img.selected").attr("id");
                        RemoveContainer(deletedItemId);
                        e.preventDefault();
                    }
                });
            }
            catch (e) {
                $(".iapps-float-box .iapps-float-box-close").on("click", function () {
                    $(this).parents(".iapps-float-box").hide();
                });

                $(".iapps-drop-zone img").on("click", function () {
                    $(".iapps-drop-zone img").removeClass("selected");
                    $(this).addClass("selected");

                    iapps_showPropertiesWindow($(this));
                });

                $(document).on("keypress", function (e) {
                    if ($(".iapps-drop-zone img.selected").length > 0 && (e.keyCode == 46 || e.keyCode == 8)) {
                        var deletedItemId = $(".iapps-drop-zone img.selected").attr("id");
                        RemoveContainer(deletedItemId);
                        e.preventDefault();
                    }
                });
            }            
        });

        function RemoveContainer(deletedItemId) {
            $("#iapps-control-properties").hide();

            if (confirm("<%= Bridgeline.iAPPS.Resources.JSMessages.Template_RemoveContainerMessage %>")) {
                $("#" + deletedItemId).remove();
                $("#recycle-bin").removeClass("recycle-bin").addClass("recycle-bin-full");
                $.each(zonesDataJson, function () {
                    var dropZone = this;
                    $.each(dropZone.ZoneItems, function () {
                        if (this.ZoneItemId == deletedItemId)
                            dropZone.ZoneItems.remove(this);
                    });
                });

                iapps_template_container_modified = true;
            }
        }

        function IsZoneRestricted(zone, controlName) {
            var restricted = false;
            $.each(zone.RestrictedControls, function () {
                if (this.toLowerCase() == controlName.toLowerCase())
                    restricted = true;
            });

            return restricted;
        }

        function GetZoneBasedContainerStyles(zone) {
            var arrStyles = new Array();
            $.each(zone.ContainerStyles, function () {
                arrStyles.push(this);
            });

            return arrStyles;
        }

        function SaveZoneInfo() {
            //Set the index of each control
            $.each(zonesDataJson, function () {
                var dropZoneItems = $("#" + $(this).attr("ZoneId") + " img");
                $.each(this.ZoneItems, function () {
                    this.ZoneItemIndex = dropZoneItems.index($("#" + this.ZoneItemId));
                });
            });

            //alert(JSON.stringify(zonesDataJson));
            $("#hdnZonesXml").val(JSON.stringify(zonesDataJson));
            return true;
        }

        function SetTemplateXml() {
            $("#hdnTemplateXml").val(JSON.stringify(templateJson));
        }

        var saveTemplate = false;
        function CloseTemplateProperties() {
            saveTemplate = true;
            templateJson.Id = popupActionsJson.FolderId;
            SetTemplateXml();
            __doPostBack("btnSave", "")
        }

        function TemplateBuilderNavItemSelect(sender, eventArgs) {
            var selectedId = eventArgs.get_item().get_id();
            if (JumpToAdminCheckLicense(selectedId)) {
                switch (selectedId) {
                    case "SaveAsDraft":
                    case "Publish":
                        if (selectedId == "SaveAsDraft") {
                            templateJson.Status = 2;
                        }
                        else {
                            templateJson.Status = 1;
                            if (templateJson.Id != EmptyGuid && !window.confirm("<%= Bridgeline.iAPPS.Resources.JSMessages.PublishTemplateConfirmation %>")) {
                                eventArgs.set_cancel(true);
                                break;
                            }
                        }
                        SaveZoneInfo();
                        if (isNewTemplate) {
                            OpeniAppsAdminPopup("ManageTemplateDetails", "TemplateBuilder=true&ShowDirectory=true&LayoutTemplateId=" + templateJson.LayoutTemplateId, "CloseTemplateProperties");
                            eventArgs.set_cancel(true);
                        }
                        else {
                            saveTemplate = true;
                            SetTemplateXml();
                        }
                        break;
                    case "ViewHistory":
                        if (templateJson.Id == EmptyGuid)
                            alert("<%= Bridgeline.iAPPS.Resources.JSMessages.SaveTemplateBeforeHistory %>");
                        else
                            OpeniAppsAdminPopup("FileHistoryPopup", "ObjectType=3&FileId=" + templateJson.Id);
                        eventArgs.set_cancel(true);
                        break;
                    case "ViewEditProperties":
                        if (templateJson.Id == EmptyGuid)
                            alert("<%= Bridgeline.iAPPS.Resources.JSMessages.SaveTemplateBeforeEditing %>");
                        else
                            OpeniAppsAdminPopup("ManageTemplateDetails", "TemplateBuilder=true&TemplateId=" + templateJson.Id);
                        eventArgs.set_cancel(true);
                        break;
                    case "Preview":
                        if (templateJson.Id == EmptyGuid)
                            alert("<%= Bridgeline.iAPPS.Resources.JSMessages.SaveTemplateBeforePreviewing %>");
                        else if (isTemplateChanged)
                            alert("<%= Bridgeline.iAPPS.Resources.JSMessages.SaveTemplateChangesBeforePreviewing %>");
                        else
                            OpeniAppsAdminPopup("ViewPagesUsingObject", stringformat("ObjectTypeId=3&Id={0}&Name={1}&IsTemplatePreview=true", templateJson.Id, templateJson.Title));
                        eventArgs.set_cancel(true);
                        break;
                    case "ShowToolBox":
                        $("#iapps-tool-box .iapps-float-box").show();
                        eventArgs.set_cancel(true);
                        break;
                    case "Title":
                    case "Status":
                        eventArgs.set_cancel(true);
                        break;
                }
            }
            else {
                eventArgs.set_cancel(true);
            }
        }

        if (window.onbeforeunload != 'undefined' || window.onbeforeunload)
            window.onbeforeunload = SaveUnsavedTemplateData;
        else
            window.onunload = SaveUnsavedTemplateData;

        function SaveUnsavedTemplateData() {
            if (iapps_template_container_modified && !saveTemplate) {
                return "<%= Bridgeline.iAPPS.Resources.JSMessages.SaveTemplateWarning %>";
            }

            return;
        }
    </script>
    <asp:PlaceHolder ID="phSiteEditorToolbar" runat="server">
        <div id="bar">
            <div class="bottomBorder" id="toolbarContainer">
                <asp:XmlDataSource ID="ToolbarDataSource" runat="server" DataFile="<%$ Resx:GUIStrings, TemplateBuilderToolBarDatasource %>">
                </asp:XmlDataSource>
                <div class="site-editor-toolbar">
                    <asp:PlaceHolder ID="phToolbarMenu" runat="server" />
                </div>
            </div>
            <div id="autohide" runat="server">
                <img src="/iapps_images/toolbar-collapse.png" id="expandArrow" onclick="alterToolBar();"
                    alt="<%$ Resx:GUIStrings, ClickToExpandToolbar %>" runat="server" title="<%$ Resx:GUIStrings, ClickToExpandToolbar %>" />
            </div>
        </div>
    </asp:PlaceHolder>
    <div class="iapps-float-box" id="iapps-control-properties">
        <div class="iapps-float-box-header">
            <asp:Localize ID="lc1" runat="server" Text="<%$ Resx:GUIStrings, Properties %>" />
            <img src="/iapps_images/close.gif" class="iapps-float-box-close" alt="Close" />
        </div>
        <div class="iapps-float-box-content" id="propEditor">
        </div>
        <div class="iapps-float-box-footer">
            <input type="button" id="btnSaveProperties" class="iapps-primary-button" value="Save" onclick="saveControlProperties(true)" /></div>
    </div>
    <div class="iapps-float-box recycle-bin" id="recycle-bin">
    </div>
    <uc1:Toolbox ID="toolBox" runat="server" />
    <asp:HiddenField ID="hdnZonesXml" runat="server" />
    <asp:HiddenField ID="hdnTemplateXml" runat="server" />
    <asp:PlaceHolder ID="TemplateHolder" runat="server"></asp:PlaceHolder>
    <asp:Button ID="btnSave" runat="server" Style="display: none;" OnClick="btnSave_Click" />
    </form>
</body>
</html>
