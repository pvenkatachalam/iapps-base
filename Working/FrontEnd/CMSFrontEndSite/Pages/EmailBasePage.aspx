﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailBasePage.aspx.cs"
    ValidateRequest="false" Theme="General" Inherits="CMSFrontEndSite.EmailBasePage"
    EnableEventValidation="false" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>iAPPS Marketier: Email Builder</title>
    <asp:PlaceHolder ID="phLinkTags" runat="server" />
    <asp:PlaceHolder ID="phScriptTags" runat="server" />
    <FWControls:FWScriptHolder ID="phIncludeScriptBlock" runat="server" />
    <script type="text/javascript">
        function Validate(btnObj) {
            var postback = Page_ClientValidate();
            document.getElementById("btnUpdate").className = "iapps-button show-loading";
            if (!postback) {
                document.getElementById("btnUpdate").className = "iapps-button";
            }
            return postback;
        }
    </script>
</head>
<body yahoo="fix" style="margin: 0; padding: 0;">
    <FWControls:FWActionLessForm runat="server" ID="form1" method="post">
        <asp:ScriptManager ID="scEmailBuilder" runat="server" EnablePageMethods="true" />
        <asp:PlaceHolder ID="phSiteEditorToolbar" runat="server">
            <div class="iapps-email-builder iapps-clear-fix">
                <div class="iapps-email-builder-toolbar-wrapper iapps-clear-fix">
                    <div class="iapps-email-builder-toolbar iapps-clear-fix">
                        <asp:Literal ID="ltNavigation" runat="server" />
                        <asp:PlaceHolder ID="phEmailBuilder" runat="server" Visible="true">
                            <ul class="iapps-email-builder-steps iapps-clear-fix">
                                <li><a id="choose_recipients" href="#" class="choose-recipients"><%= Bridgeline.iAPPS.Resources.GUIStrings.ChooseRecipients %></a></li>
                                <li class="iapps-schedule-email">
                                    <a id="schedule_email" href="#" class="schedule-email"><%= Bridgeline.iAPPS.Resources.GUIStrings.ScheduleEmailSend %></a>
                                    <asp:HiddenField ID="hdnScheduleId" runat="server" />
                                </li>
                                <li>
                                    <div class="confirm-email-overlay"></div>
                                    <asp:LinkButton ID="lbtnConfirm" runat="server" OnClick="lbtnConfirm_Click" CssClass="confirm-email" Text="<%$ Resx:GUIStrings, ReviewAndSend %>" />
                                </li>
                            </ul>
                        </asp:PlaceHolder>

                        <div class="iapps-email-builder-actions">
                            <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" CssClass="iapps-button validate-email-content" Text="<%$ Resx:GUIStrings, Close %>" />
                            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" CssClass="iapps-primary-button" Text="<%$ Resx:GUIStrings, Save %>" />
                        </div>
                    </div>
                </div>
                <asp:PlaceHolder ID="phPreview" runat="server" Visible="false">
                    <div class="iapps-email-preview">
                        <asp:LinkButton ID="lbtnClosePreview" runat="server" OnClick="lbtnClosePreview_Click" CssClass="iapps-button" Text="Close Preview" />
                    </div>
                </asp:PlaceHolder>
                <div class="iapps-email-details expanded">
                    <div class="iapps-email-details-header iapps-clear-fix">
                        <div class="iapps-form-row">
                            <label id="lblGenericHeading" runat="server"></label>
                            <div class="iapps-form-value">
                                <asp:Literal ID="ltEmailCampaign" runat="server" />
                                <br />
                                <span class="createdDate">
                                    <asp:Literal ID="ltEmailCreated" runat="server" /></span>
                            </div>
                        </div>
                    </div>
                    <div class="iapps-email-collapse">
                        <ul class="iapps-email-actions">
                            <li>
                                <asp:LinkButton ID="lbthTestEmail" runat="server" OnClick="lbtnTestEmail_Click" CssClass="iapps-test-email show-loading" Text="<%$ Resx:GUIStrings, SendATestEmail %>" />
                            </li>
                            <li>
                                <asp:LinkButton ID="lbtnPreview" runat="server" OnClick="lbtnPreview_Click" CssClass="iapps-preview-email show-loading" Text="<%$ Resx:GUIStrings, PreviewYourEmail %>" />
                            </li>
                            <li><a href="javascript://" class="iapps-spam-check-email"><%= Bridgeline.iAPPS.Resources.GUIStrings.RunASpamCheck %></a></li>
                            <li><a href="javascript://" id="lnkCreateTextVersion" class="iapps-text-only-email" runat="server" onclick="return Validate();"><%= Bridgeline.iAPPS.Resources.GUIStrings.CreateATextOnlyEmailVersion %></a></li>
                        </ul>
                        <div class="iapps-email-properties iapps-clear-fix">
                            <div class="iapps-select-template iapps-clear-fix">
                                <div class="iapps-clear-fix">
                                    <h3><%= Bridgeline.iAPPS.Resources.GUIStrings.EmailDetails %></h3>
                                    <a href="javascript: //" id="btnViewBestPractices" class="blue-bulb"><%= Bridgeline.iAPPS.Resources.GUIStrings.ViewEmailBestPractices %></a>
                                </div>
                                <asp:Panel ID="pnlGroupDetails" runat="server">
                                <div class="iapps-form-row">
                                    <label class="iapps-floated"><%= Bridgeline.iAPPS.Resources.GUIStrings.GroupName %>:</label>
                                    <div class="iapps-form-value iapps-text-value">
                                        <asp:Literal ID="ltGroupName" runat="server" />
                                    </div>
                                </div>
                                </asp:Panel>
                                <div class="iapps-form-row">
                                    <label class="iapps-floated"><%= Bridgeline.iAPPS.Resources.GUIStrings.Template %>:</label>
                                    <div class="iapps-form-value iapps-text-value">
                                        <asp:Label ID="lblTemplateName" class="collection-row iapps-form-value" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="iapps-form-row">
                                <label id="lblEmailName" runat="server">
                                    <asp:RequiredFieldValidator ID="EmailNameValidator" runat="server" ControlToValidate="txtEmailName" Text="<%$ Resx:GUIStrings, Required %>" Display="Dynamic" CssClass="validation-error" />
                                    <asp:CustomValidator ID="csvCampaignName" runat="server" ControlToValidate="txtEmailName" Text="<%$ Resx:GUIStrings, EmailAlreadyExists %>" Display="Dynamic" CssClass="validation-error" OnServerValidate="csvCampaignName_ServerValidate" />
                                </label>
                                <div class="iapps-form-value">
                                    <asp:TextBox ID="txtEmailName" runat="server" />
                                </div>
                            </div>
                            <div class="iapps-form-row">
                                <label>
                                    <%= Bridgeline.iAPPS.Resources.GUIStrings.EmailSubject %><br />
                                    <a href="javascript: //" onclick="return fn_ShowContactProperties('txtEmailSubject');"><%= Bridgeline.iAPPS.Resources.GUIStrings.InsertDynamicContactDetails %></a>
                                    <asp:RequiredFieldValidator ID="EmailSubjectValidator" runat="server" ControlToValidate="txtEmailSubject" Text="<%$ Resx:GUIStrings, Required %>" Display="Dynamic" CssClass="validation-error" />
                                </label>
                                <div class="iapps-form-value">
                                    <asp:TextBox ID="txtEmailSubject" runat="server" ClientIDMode="Static" />
                                </div>
                            </div>
                            <div class="iapps-form-row">
                                <label>
                                    <%= Bridgeline.iAPPS.Resources.GUIStrings.SenderName %><br />
                                    <a href="javascript: //" onclick="return fn_ShowContactProperties('txtSenderName');"><%= Bridgeline.iAPPS.Resources.GUIStrings.InsertDynamicContactDetails %></a>
                                    <asp:RequiredFieldValidator ID="SenderNameValidator" runat="server" ControlToValidate="txtSenderName" Text="<%$ Resx:GUIStrings, Required %>" Display="Dynamic" CssClass="validation-error" />
                                </label>
                                <div class="iapps-form-value">
                                    <asp:TextBox ID="txtSenderName" runat="server" ClientIDMode="Static" />
                                </div>
                            </div>
                            <div class="iapps-form-row">
                                <label>
                                    <%= Bridgeline.iAPPS.Resources.GUIStrings.SenderEmail %><br />
                                    <a href="javascript: //" onclick="return fn_ShowContactProperties('txtSenderEmail');"><%= Bridgeline.iAPPS.Resources.GUIStrings.InsertDynamicContactDetails %></a>
                                    <asp:RequiredFieldValidator ID="SenderEmailValidator" runat="server" ControlToValidate="txtSenderEmail" Text="<%$ Resx:GUIStrings, Required %>" Display="Dynamic" CssClass="validation-error" />
                                    <asp:RegularExpressionValidator ID="revSenderEmail" ControlToValidate="txtSenderEmail" Display="Dynamic"
                                        Text="<%$ Resx:GUIStrings, InvalidFormat %>" runat="server" ValidationExpression="(^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*[ ]*$)|(^\[[\w]*:[\w]*\]$)" CssClass="validation-error" />
                                </label>
                                <div class="iapps-form-value">
                                    <asp:TextBox ID="txtSenderEmail" runat="server" ClientIDMode="Static" />
                                </div>
                            </div>
                            <div class="iapps-form-row">
                                <label id="lblConfirmationEmail" runat="server">
                                    <%= Bridgeline.iAPPS.Resources.GUIStrings.ConfirmationEmail %>
                                    <asp:RegularExpressionValidator ID="revConfirmationEmail" ControlToValidate="txtConfirmationEmail"
                                        Text="<%$ Resx:GUIStrings, InvalidFormatSeparateAddressesWithComma %>" runat="server" ValidationExpression="^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$" Display="Dynamic" CssClass="validation-error" />
                                </label>
                                <div class="iapps-form-value">
                                    <asp:TextBox ID="txtConfirmationEmail" runat="server" />
                                </div>
                            </div>
                            <asp:UpdatePanel ID="upTriggers" UpdateMode="Always" runat="server">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlAutoResponders" runat="server" Visible="false">
                                        <div class="iapps-form-row">
                                            <label class="form-label"><%= Bridgeline.iAPPS.Resources.GUIStrings.TimeSentAfterTrigger %></label>
                                            <div class="iapps-form-value">
                                                <asp:TextBox ID="txtTimeValue" runat="server" Width="50" />
                                                <asp:DropDownList ID="ddlTimeMeasurement" runat="server" Style="margin-left: 10px;" OnSelectedIndexChanged="ddlTimeMeasurement_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Text="<%$ Resx:GUIStrings, Minutes %>" />
                                                    <asp:ListItem Text="<%$ Resx:GUIStrings, Hours %>" />
                                                    <asp:ListItem Text="<%$ Resx:GUIStrings, Days %>" />
                                                    <asp:ListItem Text="<%$ Resx:GUIStrings, MonthForTrigger %>" />
                                                </asp:DropDownList>
                                                <asp:RegularExpressionValidator ID="regTriggerTime" ControlToValidate="txtTimeValue" ValidationExpression="^\d+" Display="Dynamic" ErrorMessage="Enter only numbers" CssClass="validation-error" runat="server"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="iapps-button-row">
                                <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="iapps-primary-button show-loading" Text="<%$ Resx:GUIStrings, Update %>" OnClientClick="return Validate();" />
                            </div>
                        </div>
                    </div>
                    <div class="iapps-email-detail-msg">
                        <p class="iapps-email-msg">Hide Email Details and Options</p>
                    </div>
                    <div class="iapps-email-detail-handle">
                        <img src="/iapps_images/email-details-collapse.png" alt="Collapse email details" class="iapps-toggle-handle" />
                    </div>
                </div>
                <div class="iapps-modal insert-contact-property" id="divInsertContactProperty" style="display: none; z-index: 19999 !important;">
                    <div class="modal-header iapps-clear-fix">
                        <h2><%= Bridgeline.iAPPS.Resources.GUIStrings.InsertDynamicContactDetails %></h2>
                    </div>
                    <div class="modal-content iapps-clear-fix">
                        <asp:DropDownList ID="ddlContactProperties" runat="server" Width="300" ClientIDMode="Static" />
                    </div>
                    <div class="modal-footer iapps-clear-fix">
                        <asp:Button ID="btnCancelInsert" runat="server" CssClass="iapps-button" Text="<%$ Resx:GUIStrings, Cancel %>" OnClientClick="return fn_HideContactProperties();" />
                        <asp:Button ID="btnInsert" runat="server" CssClass="iapps-primary-button" Text="<%$ Resx:GUIStrings, Insert %>" OnClientClick="return fn_InsertContactProperty('ddlContactProperties');" />
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phEditor" runat="server" Visible="false">
            <asp:PlaceHolder ID="phRadEditor" runat="server" Visible="false">
                <div id="editableArea" style="display: none;">
                    <radE:RadEditor ID="RadEditor1" runat="server" ToolbarMode="Floating" StripFormattingOptions="MSWord"
                        NewLineMode="P" AllowScripts="true" Width="100px" ToolsWidth="718" OnClientLoad="OnClientLoad"
                        OnClientCommandExecuting="OnClientCommandExecuting" OnClientModeChange="OnClientModeChange" EditModes="Design,Html">
                        <Content>
                        &nbsp;
                        </Content>
                    </radE:RadEditor>
                    <input type="button" id="btniAPPSSave" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Save %>" class="iapps-primary-button iapps-toolbar-button telerik-toolbar" onclick="return ContentSave();" />
                    <input type="button" id="btniAPPSSaveAs" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.SaveAs %>" class="iapps-primary-button iapps-toolbar-button telerik-toolbar" onclick="return ContentSaveAs();" />
                    <input type="button" id="btniAPPSCancel" value="<%= Bridgeline.iAPPS.Resources.GUIStrings.Cancel %>" class="iapps-button iapps-toolbar-button telerik-toolbar" onclick="return ContentCancel();" />
                </div>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phCkEditor" runat="server" Visible="false">
                <div id="editorContainer" style="display: none;">
                    <div id="iAPPSToolbar">
                        <p>
                            <strong>
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resx:JSMessages, iAPPSSiteEditorToolbar %>" />
                            </strong>
                        </p>
                        <div id="cktoolbarContainer">
                        </div>
                    </div>
                    <CKEditor:CKEditorControl ID="ckEditor" runat="server" AutoGrowMinHeight="200" EnterMode="P"
                        BasePath="~/iAPPS_Editor" Width="100" Height="100" Entities="true" HtmlEncodeOutput="true"
                        IgnoreEmptyParagraph="true" BrowserContextMenuOnCtrl="true" EntitiesGreek="true"
                        ScaytAutoStartup="false" ShiftEnterMode="BR" TemplatesReplaceContent="false"
                        SharedSpacesTop="cktoolbarContainer" ForceSimpleAmpersand="true" ToolbarCanCollapse="false">
                    </CKEditor:CKEditorControl>
                </div>
            </asp:PlaceHolder>
            <script type="text/javascript" src="/iAPPS_Editor/radeditor_custom.js"></script>
            <script type="text/javascript" src="/iAPPS_Editor/ckeditor_custom.js"></script>
        </asp:PlaceHolder>
        <div id="iAppsTemplateHolder">
            <asp:PlaceHolder ID="TemplateHolder" runat="server" />
        </div>
    </FWControls:FWActionLessForm>
    <FWControls:FWScriptHolder ID="phStartUpScriptBlock" runat="server" />
</body>
</html>
