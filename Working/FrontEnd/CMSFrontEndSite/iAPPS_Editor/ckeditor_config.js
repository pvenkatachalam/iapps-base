﻿CKEDITOR.editorConfig = function (config) {
    config.extraPlugins = removeUnwantedTools('translatetool,SaveText,SaveAsText,Cancel,EditImage,InsertFlashFromLibrary,InsertImageFromLibrary,InsertImageFromSP,InsertProductImage,LinkToLibrary,LinkToSPLibrary,SnippetManager,SaveSelectionAsSnippet,InsertSnippet,InsertManagedLink,ViewInBrowser,mergefields,Unsubscribe,stylesheetparser,sharedspace');
    config.toolbar = 'iAPPSToolbar';
    config.toolbar_iAPPSToolbar = [
		{ name: 'editing', items: ['Scayt', 'Find', 'Replace', '-', 'SelectAll'] },
		{name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'RemoveFormat', '-', 'Undo', 'Redo'] },
        { name: 'iAPPSCustomTools', items: ['InsertImageFromLibrary', 'EditImage', 'LinkToLibrary', 'Link', 'Unlink', 'Anchor', 'InsertFlashFromLibrary', 'InsertProductImage'] },
        { name: 'SPLibrary', items: ['InsertImageFromSP', 'LinkToSPLibrary'] },
                '/',
        { name: 'translate', items: ['translatetool'] },
		{ name: 'paragraph', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Outdent', 'Indent', 'NumberedList', 'BulletedList', 'Table', 'HorizontalRule'] },
		{ name: 'insert', items: ['Styles', 'Format'] },
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', 'BidiLtr', 'BidiRtl', '-', 'Blockquote', 'CreateDiv', 'SpecialChar', 'ShowBlocks'] },
        { name: 'htmlsnippets', items: ['SnippetManager', 'SaveSelectionAsSnippet', 'InsertSnippet'] },
        { name: 'marketier', items: ['InsertManagedLink', 'ViewInBrowser', 'Unsubscribe', 'mergefields'] },
		{ name: 'document', items: ['Source', 'About', '-', 'SaveText', 'SaveAsText', 'Cancel'] }
    ];
    config.stylesSet = 'iAPPS_Styles:/iAPPS_Editor/ckeditor_styles.js';
    config.format_tags = 'p;h1;h2;h3;h4;h5;pre;address;div';
    config.templates_files = ['/iAPPS_Editor/ckeditor_templates.js'];
    config.bodyId = 'iapps_ckeditor';
    config.bodyClass = 'iapps_ckeditor';
    config.scayt_autoStartup = true;
    config.wsc_customerId = "1:V3ABk1-f0QEk1-nn7dD1-Ma1FI3-Dn7LQ3-oXZu04-P0Ngn-eUkWE1-5RtpN-DBIif1-2z3jU-jr2";
    config.resize_enabled = false;
};


function removeUnwantedTools(toolbar) {
    if (typeof (removeButtons) == "undefined" || removeButtons == '') { return toolbar; }
    var rmArray = removeButtons.split(',');
    for (var i = 0, len = rmArray.length; i < len; i++) {
        toolbar = toolbar.replace(rmArray[i], ''); 
    }
    return toolbar;
}
