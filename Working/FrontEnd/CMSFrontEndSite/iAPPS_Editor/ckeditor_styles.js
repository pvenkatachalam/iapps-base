﻿CKEDITOR.stylesSet.add('iAPPS_Styles',
[
    { name: 'Blue Title', element: 'h2', styles: { 'color': 'Blue'} },
    { name: 'Red Title', element: 'h3', styles: { 'color': 'Red'} },
    { name: 'Orange BG', element: 'p', styles: { 'background-color': 'orange'} },
    { name: 'CSS Style', element: 'p', attributes: { 'class': 'my_class'} },
    { name: 'Yellow', element: 'p', styles: { 'background-color': 'Yellow' } }
]);