﻿//method to position CKEditor and set the text
var targetLibrary;
var managedLinkText;
var isCKEditorToolbarTextAdded = false;
var iAPPS_ckEditorId = "";
var iAPPS_ckEditorClass = "";
var ckEditor_selection = '';
var ckEditor_selectedText = '';
var ckEditor_selectedElement;
function fn_GetCKEditorSelection(ourEditor) {
    ckEditor_selection = ourEditor.getSelection();
    ckEditor_selection.lock();
    ckEditor_selectedText = ckEditor_selection.getSelectedText();
    //var CKEditorIEDetection = parent.CKEDITOR ? parent.CKEDITOR.env.ie : CKEDITOR.env.ie;
    //if (CKEditorIEDetection) {
    //    ckEditor_selection.unlock(true);
    //    ckEditor_selectedText = ckEditor_selection.getNative().createRange().text;
    //}
    //else {
    //    ckEditor_selectedText = ckEditor_selection.getNative();
    //}
}
function fn_PositionCKEditor(objTextContainer, objEditorContainer, textToEdit) {
    var contentPosition = $(objTextContainer).parent().offset();
    //set the position of the editor
    $(objEditorContainer).css('left', contentPosition.left);
    $(objEditorContainer).css('top', contentPosition.top);
    $(objEditorContainer).css('position', 'absolute');
    $(objEditorContainer).css('zIndex', '19996');
    //resize the editor to the content dimensions
    ckEditor.resize($(objTextContainer).parent().outerWidth(), ($(objTextContainer).parent().outerHeight() + 100));
    //set the editors data
    ckEditor.setData(textToEdit);
    $(objTextContainer).css('visibility', 'hidden');
    $(objEditorContainer).css('display', 'block');
    objTextContainer.style.visibility = "hidden";
    fn_MakeToolbarDragable(contentPosition);
    fn_SetBodyIdAndClass();
}
//method called on mode change
function fn_CKEditorModeChange(ev) {
    var iAPPS_ckEditorMode = ev.editor.mode.toLowerCase();
    if (iAPPS_ckEditorMode == 'wysiwyg') {
        var ckeditor_body = ckEditor.document.getBody();
        ckeditor_body.setAttribute('class', iAPPS_ckEditorClass);
        ckeditor_body.setAttribute('id', iAPPS_ckEditorId);
        fn_CheckForInvalidTags(ev.editor);
    }
    else if (iAPPS_ckEditorMode == 'source') {
    }
}
//method called before mode changes
function fn_CKEditorBeforeModeChange(ev) {
    var iAPPS_ckEditorMode = ev.editor.mode.toLowerCase();
    ckEditor = ev.editor;
    if (iAPPS_ckEditorMode == 'wysiwyg') {
        var ckeditor_body = ckEditor.document.getBody();
        var ckEditor_bodyClass = ckeditor_body.getAttribute('class');
        var ckEditor_bodyId = ckeditor_body.getAttribute('id');
        iAPPS_ckEditorId = ckEditor_bodyId;
        iAPPS_ckEditorClass = ckEditor_bodyClass;
    }
    else if (iAPPS_ckEditorMode == 'source') {
    }
}
//method to set the body class and id of the editor to be same as the viewcssclass
function fn_SetBodyIdAndClass() {
    setTimeout(function () {
        var ckeditor_body = ckEditor.document.getBody();
        var ckEditor_bodyClass = ckeditor_body.getAttribute('class');
        var ckEditor_bodyId = ckeditor_body.getAttribute('id');
        if (Data.Container.getAttribute('ViewCssClass') != undefined) {
            var container_viewcssclass = Data.Container.getAttribute('ViewCssClass');
            ckeditor_body.setAttribute('class', ckEditor_bodyClass != '' ? ckEditor_bodyClass + ' ' + container_viewcssclass : container_viewcssclass);
            ckeditor_body.setAttribute('id', container_viewcssclass.replace('-', '_').replace(' ', '_'));
        }
    }, 500);
}
//method to add the sharepoint text to the editor toolbar
function fn_AddSharepointText() {
    if (!isCKEditorToolbarTextAdded) {
        $(document).find('[class*="OpenInsertImageSP"]').parents('[class="cke_toolgroup"]').prepend('<span class="iapps-toolbar-text">Sharepoint</span>');
        isCKEditorToolbarTextAdded = true;
    }
}
//method to set the editor toolbar outside the editor and make it floating
function fn_MakeToolbarDragable(contentOffset) {
    fn_ToggleToolbar(true, contentOffset);
    $('#iAPPSToolbar').draggable({ handle: "p" });
    $('#iAPPSToolbar p').css('cursor', 'move');
    //save button
    $('.cke_button__savetext .cke_button__savetext_label').css({ display: 'block' });
    $('.cke_button__savetext .cke_button__savetext_icon').css({ display: 'none' });
    //save as button
    $('.cke_button__saveastext .cke_button__saveastext_label').css({ display: 'block' });
    $('.cke_button__saveastext .cke_button__saveastext_icon').css({ display: 'none' });
    //cancel button
    $('.cke_button__cancel .cke_button__cancel_label').css({ display: 'block' });
    $('.cke_button__cancel .cke_button__cancel_icon').css({ display: 'none' });
}
//method to hide the toolbar, passing true will show the toolbar
function fn_ToggleToolbar(isVisible, contentOffset) {
    if (isVisible) {
        var toolbarTop = contentOffset.top - 133 > 0 ? (contentOffset.top - 133) : 0;
        var toolbarLeft;
        if ((screen.width - contentOffset.left) < 718) {
            toolbarLeft = (contentOffset.left - 718);
        }
        else {
            toolbarLeft = contentOffset.left;
        }
        $('#iAPPSToolbar').offset({ left: toolbarLeft, top: toolbarTop });
        $('#iAPPSToolbar').css('visibility', 'visible');
    }
    else {
        $('#iAPPSToolbar').offset({ left: 0, top: -9999 });
        $('#iAPPSToolbar').css('visibility', 'hidden');
    }
}
//method to save content from editor to page
function fn_SaveContent() {
    if (editorContext == 'SiteEditor') {
        if (Data.HiddenIsMasterContent.value != 'True' || NewContent == true) {
            OnClientSubmit(ckEditor);
            ShowContentSaveWarning();
            fn_ToggleToolbar(false);
            Data.HiddenIsMasterContent.value = 'False';
        }
        else {
            alert(__JSMessages["YouCanNotEditMasterSiteContent"]);
        }
    }
    else {
        OnClientSubmitAdminContext(ckEditor);
    }
}
function fn_SaveContentAs() {
    NewContent = true;
    OnClientSubmit(ckEditor);
    fn_ToggleToolbar(false);
    ShowContentSaveWarning()
}
//method to cancel editing
function fn_CancelTextEdit() {
    if (editorContext == 'SiteEditor') {
        srcControl.style.visibility = "visible";
        $('#editorContainer').css('display', 'none');
        ckEditor.setData('');
        NewContent = false;
        fn_ToggleToolbar(false);
    }
    else {
        OnClientCancelAdminContext(ckEditor);
    }
}
//method to close the image dialog
function fn_CloseImageDialog() {
    viewPopupWindow.hide();
}
//method to open the image dialog
function fn_InserImageOnClick(objEditor) {
    ckEditor = objEditor;
    EditorImageDialog = __JSMessages["Editor"];
    selectedObject = new Object();
    selectedObject.Scope = "Editor";

    OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "fn_InsertImageInEditor");
}
//method to open the edit image dialog
function fn_EditImageOnClick(objEditor) {
    ckEditor = objEditor;
    //get current selection
    var sel = ckEditor.getSelection();
    //get the selected element or the element having the cursor
    var element = sel.getStartElement();
    //if nearest tag is not found or is not an anchor tag then create a new anchor tag and insert it into the ourEditor
    if (!element || element.getName() != 'img') {
        alert(__JSMessages["PleaseSelectTheImageYouWantToEdit"]);
        return;
    }
    if (element.getAttribute("isproductimage") != null) {
        alert(__JSMessages["CanNotEditProductImage"]);
        return;
    }
    if (element.getAttribute("objectId") != null) {
        OpeniAppsAdminPopup("ImageEditor", "ImageId=" + element.getAttribute("objectId"), "fn_InsertImageInEditor");
    }
    else if (element.getAttribute("source") == "SharePoint") {
        alert(__JSMessages["ThisImageIsLoadedFromSharePointLibrary"]);
        return;
    }
    else {
        alert(__JSMessages["ThisVersionOfTheImageCanNotBeEdited"]);
        return;
    }
}
//method to insert / update a link
function fn_LinkInEditorOnClick(objEditor, librarySource) {
    ckEditor = objEditor;
    fn_GetCKEditorSelection(ckEditor);
    SelectedObject = new Object();
    targetLibrary = librarySource;
    //get current selection
    var sel = objEditor.getSelection();
    //get the selected element or the element having the cursor
    var element = sel.getStartElement();
    //get selected text
    var selectedText = sel.getSelectedText();
    var imgControl = false;
    if (element.getName().toLowerCase() != 'img') {
        if (element.getName().toLowerCase() == 'a') {
            selectedText = element.getText();
        }
        if (selectedText == '') {
            alert(__JSMessages["PleaseSelectTheTextYouWantToConvertToALink"]);
            return;
        }
    }
    if (element.getName().toLowerCase() == 'img') {
        var parentElement = element.getParent();
        if (parentElement != null && parentElement.getName().toLowerCase() == 'a') {
            element = parentElement;
        }
        imgControl = true;
    }
    var isCMSLink = true;
    if (element.getName().toLowerCase() == 'a') {
        var linkProperties = new Object();
        if (element.getAttribute("returnValue") != null) {
            linkProperties = element.getAttribute("returnValue").split(",");
            SelectedObject.HrefValue = linkProperties[0];
        }
        else {
            if (element.getAttribute("href").indexOf("Link:") > 0) {
                //it is a marketier link
                var linkType = element.getAttribute("href").substring(6, 11);
                var linkTypeId;
                var linkContainerId;
                if (linkType == 'Page:' || linkType == 'File:') {
                    linkType = element.getAttribute("href").substring(6, 10); //Image is having 5 letters
                    linkTypeId = element.getAttribute("href").substring(11, 47);
                    linkContainerId = element.getAttribute("href").substring(48, 84);
                }
                if (linkType == 'Image') {
                    linkTypeId = element.getAttribute("href").substring(12, 48);
                    linkContainerId = element.getAttribute("href").substring(49, 85);
                }
                isCMSLink = false;
                SelectedObject.TypeValue = linkType;
                SelectedObject.Id = linkTypeId;
                SelectedObject.HrefValue = '';
            }
            else {
                alert(__JSMessages["PleaseUseTheLinksAndBookmarksToOpenThisLink"]);
                return;
            }
        }
        if (isCMSLink) {
            SelectedObject.TypeValue = linkProperties[2];
            SelectedObject.TargetValue = linkProperties[3];
            SelectedObject.Height = linkProperties[4];
            SelectedObject.Width = linkProperties[5];
            SelectedObject.showAddressBar = linkProperties[6];
            SelectedObject.showMenuBar = linkProperties[7];
            SelectedObject.showStatusBar = linkProperties[8];
            SelectedObject.allowScrollBar = linkProperties[9];
            SelectedObject.Id = linkProperties[10];
            SelectedObject.watchcode = linkProperties[11];
        }
    }
    //fix issue with linking an image
    if (imgControl) {
        SelectedObject.SelectedText = element.getOuterHtml();
    }
    else {
        SelectedObject.SelectedText = selectedText;
    }
    imgControl = false;
    SelectedObject.TargetLibrary = targetLibrary;

    OpeniAppsAdminPopup("ShowHyperLinkManager", "OpenSharedPopup=1", "fn_InsertLibraryLinkInEditor");
}
//method to open insert flash dialog
function fn_InsertFlashOnClick(objEditor) {
    ckEditor = objEditor;
    selectedFlashElement = null;
    //get current selection
    var sel = objEditor.getSelection();
    //get the selected element or the element having the cursor
    var element = sel.getStartElement();
    selectedFlashObject = new Object();
    if (element && element.getName().toLowerCase() == 'img') {
        element = element.getParent();
        if (element && element.getName().toLowerCase() == 'div') {
            selectedFlashElement = element;
            var linkProperties = new Object();
            if (element.getAttribute("returnValue") != null) {
                linkProperties = element.getAttribute("returnValue").split(",");
                selectedFlashObject.FilePathValue = linkProperties[0].replace("%3A", ":");
                selectedFlashObject.ContainerIdValue = linkProperties[1];
                selectedFlashObject.WidthValue = linkProperties[2];
                selectedFlashObject.HeightValue = linkProperties[3];
                selectedFlashObject.AltImagePathValue = linkProperties[4].replace("%3A", ":");
                selectedFlashObject.MinVersionValue = linkProperties[5];
                selectedFlashObject.QualityValue = linkProperties[6];
                selectedFlashObject.AutoPlayValue = linkProperties[7];
            }
        }
    }
    EditorFlashDialog = 'Editor';   //do not translate
    selectedFlashObject.Scope = "Editor";
    OpeniAppsAdminPopup("ShowFlashManager", "", "fn_InsertFlashInEditor");
}
//method called on click of insert product image
function fn_InsertProductImageOnClick(objEditor) {
    ckEditor = objEditor;
    EditorLinkToProductDialog = "Editor";   //do not translate
    selectedLinkToProductObject = new Object();
    selectedLinkToProductObject.Scope = "Editor";
    OpeniAppsCommercePopup('SelectProductImage', 'ShowProducts=true');
}
//method to insert sharepoint image
function fn_InsertSPImageOnClick(objEditor) {
    ckEditor = objEditor;
    EditorImageDialog = "Editor";   //do not translate
    selectedObject = new Object();
    selectedObject.Scope = "Editor";
    OpeniAppsAdminPopup("SelectSharepointLibrary", "Type=Image", "fn_InsertSPImageInEditor");
}
//method to open the snippet manager
function fn_SnippetManagerOnClick(objEditor) {
    ckEditor = objEditor;
    EditorImageDialog = 'Editor';   //do not transalte
    selectedObject = new Object();
    selectedObject.Scope = "Editor";
    if (jProductId.toLowerCase() == jMarketierProductId.toLowerCase()) {
        var adminpath = jmarketierAdminURL + "/Popups/ShowSnippetManager.aspx";
        adminpath = SendTokenRequestByURLWithMethodName(adminpath, "fn_OpenCKEditorSnippetManager();");
    }
    else {
        OpeniAppsAdminPopup("ShowSnippetManager", "", "InsertCodeSnippetCKEditor");
    }
}
//method to open inert snipper
function fn_InsertSnippetOnClick(objEditor) {
    ckEditor = objEditor;
    EditorImageDialog = 'Editor';   //do not transalte
    selectedObject = new Object();
    selectedObject.Scope = "Editor";
    if (jProductId.toLowerCase() == jMarketierProductId.toLowerCase()) {
        var adminpath = jmarketierAdminURL + "/Popups/ShowSnippetManager.aspx";
        adminpath = SendTokenRequestByURLWithMethodName(adminpath, "fn_OpenCKEditorSnippetManager();");
    }
    else {
        OpeniAppsAdminPopup("ShowSnippetManager", "ReadOnly=true", "InsertCodeSnippetCKEditor");
    }
}
//method to handle click on save selection as snippet
function fn_SaveSelectionAsSnippetOnClick(objEditor) {
    ckEditor = objEditor;
    var htmlString = ckEditor.getData();
    if (htmlString != "" && htmlString != "&nbsp;" && htmlString != "<br>") {
        var snippetName = prompt(__JSMessages["PleaseEnterANameForTheSnippet"]);
        if (snippetName != null && snippetName != "") {
            var snippetId = SaveSnippetToDb(snippetName, htmlString, siteId);
        }
        else {
            alert(__JSMessages["YouMustEnterANameForTheSnippet"]);
        }
    }
    else {
        alert(__JSMessages["YouMustAddContentToTheEditor"]);
    }
}
//method to handle translate tool
function fn_TranslateToolOnClick(selectedLanguage, objEditor) {
    ckEditor = objEditor;
    //get current selection
    var sel = objEditor.getSelection();
    //get the selected element or the element having the cursor
    var element = sel.getStartElement();
    //get selected text
    var selectedText = sel.getSelectedText();
    var hasSelection = true;
    var target_lang = selectedLanguage;
    if (selectedText == '') {
        selectedText = ckEditor.getData();
        hasSelection = false;
    }
    if (selectedLanguage != '') {
        var _maxLength = 5000; //Max Length in Characters that can be sent to the Google API for translation
        var src_lang = selectedLanguage;
        if (selectedText.length <= _maxLength) {
            var result = SendTranslation('', target_lang, selectedText);
            if (result) {
                ckEditor.insertHtml(result);
            }
        }
    }
    else {
        alert(stringformat(__JSMessages["RequestIsTooLarge"], _maxLength));
    }
}
//method to handle managed link click
function fn_ManagedLinkOnClick(objEditor) {
    ckEditor = objEditor;
    //get current selection
    var sel = objEditor.getSelection();
    //get the selected element or the element having the cursor
    var element = sel.getStartElement();
    //get selected text
    var selectedText = sel.getSelectedText();
    var imgControl = false;
    if (element.getName().toLowerCase() != 'img') {
        if (element.getName().toLowerCase() == 'a') {
            selectedText = element.getText();
        }
        if (selectedText == '') {
            alert(__JSMessages["PleaseSelectTheTextYouWantToConvertToALink"]);
            return;
        }
    }
    if (element.getName().toLowerCase() == 'img') {
        var parentElement = element.getParent();
        if (parentElement != null && parentElement.getName().toLowerCase() == 'a') {
            element = parentElement;
        }
        imgControl = true;
    }
    if (element.getName().toLowerCase() == 'a') {
        var linkProperties = new Object();
        if (element.getAttribute("returnValue") != null) {
            linkProperties = element.getAttribute("returnValue").split(",");
            SelectedObject.HrefValue = linkProperties[0];
        }
        else {
            alert(__JSMessages["PleaseUseTheLinksAndBookmarksToOpenThisLink"]);
            return;
        }
    }
    else {
        SelectedObject = new Object();
    }
    if (imgControl) {
        SelectedObject.SelectedText = element.getOuterHtml();
        managedLinkText = element.getOuterHtml();
    }
    else {
        SelectedObject.SelectedText = selectedText;
        managedLinkText = selectedText;
    }
    imgControl = false;
    OpeniAppsMarketierPopup('InsertManagedLink', '', 'fn_InsertManagedLinkInEditor');
}
//method to handle view in browser click
function fn_ViewInBrowserOnClick(objEditor) {
    ckEditor = objEditor;
    if (objEditor) {
        element = ckEditor.document.createElement('a');
        element.setAttribute('href', '[ViewInBrowserLink]');
        element.setText(__JSMessages["ViewThisEmailInBrowser"]);
        ckEditor.insertElement(element);
    }
}

//method to open CKEditor's insert product image
function fn_OpenCKEditorProductInsertImageDialog() {
    viewPopupWindow = dhtmlmodal.open('', 'iframe', destinationUrl, '', 'width=800px,height=550px,center=1,resize=0,scrolling=1');
}
//method to open CKEditor's insert image from sharepoint
function fn_OpenCKEditorInsertSPImage() {
    var queryString = destinationUrl.replace('?', '&');
    OpeniAppsAdminPopup("SelectSharepointLibrary", queryString, "");
}
//method to open CKEditor's Snippet manager
function fn_OpenCKEditorSnippetManager() {
    viewPopupWindow = dhtmlmodal.open('', 'iframe', destinationUrl, '', 'width=800px,height=550px,center=1,resize=0,scrolling=1');
}
//method to open managed link dialog
function fn_OpenCKEditorManagedLinkDialog() {
    viewPopupWindow = dhtmlmodal.open('', 'iframe', destinationUrl, '', 'width=800px,height=550px,center=1,resize=0,scrolling=1');
}
//method to insert / update the image from insert image dialog
function fn_InsertImageInEditor() {
    var element;
    //get current selection
    var sel = ckEditor.getSelection();
    if (sel) {
        //get the selected element or the element having the cursor
        element = sel.getStartElement();
    }
    if (element) {
        //get the nearest anchor tag
        element = element.getAscendant('img', true);
    }
    //if nearest tag is not found or is not an anchor tag then create a new anchor tag and insert it into the ourEditor
    if (!element || element.getName() != 'img') {
        element = ckEditor.document.createElement('img');
        element.setAttribute('src', popupActionsJson.CustomAttributes.ImageUrl);
        element.setAttribute('alt', popupActionsJson.CustomAttributes.ImageTitle);
        element.setAttribute('objectId', popupActionsJson.SelectedItems[0]);
        ckEditor.insertElement(element);
    }
    else {
        //update the image tags when replacing an existing image
        var classAttribute = '';
        var styleAttribute = '';
        if ($(element).attr('class') != '') {
            classAttribute = $(element).attr('class');
        }
        if ($(element).attr('style') != '') {
            styleAttribute = $(element).attr('style');
        }
        element.remove();
        element = ckEditor.document.createElement('img');
        element.setAttribute('src', popupActionsJson.CustomAttributes.ImageUrl);
        element.setAttribute('alt', popupActionsJson.CustomAttributes.ImageTitle);
        element.setAttribute('objectId', popupActionsJson.FolderId);
        if (styleAttribute != '') {
            $(element).attr('style', styleAttribute);
        }
        if (classAttribute != '') {
            $(element).attr('class', classAttribute);
        }
        ckEditor.insertElement(element);
    }
}
//method to insert link in CKEditor
function fn_InsertLibraryLinkInEditor(objLink) {
    returnValue = JSON.parse(popupActionsJson.CustomAttributes.LinkReturnValue);
    if (returnValue == undefined) {
        return;
    }
    CreateWindowOpenFunction(returnValue);
    OpenWindow(returnValue);
    CreateLinkPropertiesArray(returnValue);


    var onclickEvent = '';
    //start watch code
    if (returnValue.watchcode != undefined && returnValue.watchcode.toLowerCase() != ';nowatch') {
        var watchvalue = returnValue.watchcode.toString().split(";");

        if (watchvalue[1] == "ClientWatch") {
            onclickEvent = "iAPPSTracker('" + watchvalue[0] + "');";
        }
    }
    //End watch code


    //get the selected element or the element having the cursor
    var element = ckEditor_selection.getStartElement();
    ckEditor_selection.unlock(true);
    //get selected text
    var selectedText = ckEditor_selectedText;
    var imgControl = false;
    if (element.getName().toLowerCase() == 'img') {
        var parentElement = element.getParent();
        if (parentElement != null && parentElement.getName().toLowerCase() == 'a') {
            element = parentElement;
        }
        imgControl = true;
    }
    if (element.getName().toLowerCase() == 'a') {
        element.remove();
    }
    element = ckEditor.document.createElement('a');
    if (jTemplateType == '1' && LinkPropertiesArray[2] != 'ExternalURL') // Email Template
    {
        element.setAttribute('href', '[Link:' + LinkPropertiesArray[2] + ':' + LinkPropertiesArray[10] + '|' + Data.Container.getAttribute('FWContainerID') + '|' + Data.Container.getAttribute('id') + ']');
        element.setText(returnValue.text);
        element.setAttribute('returnValue', LinkPropertiesArray.toString());
    }
    else    //CMS Template
    {
        if (returnValue.target == "_blank") {
            element.setAttribute('target', '_blank');
        }
        else if (returnValue.target == "_modal") {
            element.setAttribute('rel', 'modal');
        }
        element.setAttribute('href', windowOpen);
        element.setAttribute('returnValue', LinkPropertiesArray.toString());
        if (imgControl) {
            element.setHtml(returnValue.text);
        }
        else {
            element.setText(returnValue.text);
        }

        // This is for inserting watch from link manager
        if (onclickEvent != '')
            element.setAttribute("onclick", onclickEvent);
    }
    ckEditor.insertElement(element);
}
//method to insert flash
function fn_InsertFlashInEditor() {
    var objFlash = popupActionsJson.CustomAttributes.FlashReturnValue;
    if (objFlash) {
        CreateFlashPropertiesArray(objFlash);
        var lbr = "{";
        var rbr = "}";
        var altImagePath = "/App_Themes/General/images/FlashManager.gif";
        if (objFlash.altImagePath && objFlash.altImagePath != "undefined" && objFlash.altImagePath != "")
            altImagePath = objFlash.altImagePath;
        var minVersion = "0,0";
        if (objFlash.minVersion && objFlash.minVersion != "undefined" && objFlash.minVersion != "")
            minVersion = objFlash.minVersion + ",0";
        var fSrc = escape(objFlash.filePath);
        fSrc = fSrc.replace(new RegExp("%3A", "g"), ":");
        var flashVars = "";
        if (fSrc.toLowerCase().indexOf(".flv") > 0) {
            flashVars = "file: '" + fSrc + "',autostart:'" + objFlash.autoPlay + "'";
            fSrc = "/FlashPlayer/mediaplayer.swf";
        }

        var flashEvent = String.format("flashembed('{2}', {0}src:'{3}',version:'{4}',wmode:'opaque', quality:'{5}'{1}, {0}{6}{1});", lbr, rbr, objFlash.containerId, fSrc, minVersion, objFlash.quality, flashVars);
        flashEvents += flashEvent;
        flashEvent = '<' + 'script type="text/javascript">' + flashEvent + '<' + '/script>';
        var flashHtml = String.format('<div returnValue="{0}" id="{1}" style="height:{2}px;width:{3}px;"><img src="{4}" style="height:{2}px;width:{3}px;" />{5}</div>', FlashPropertiesArray, objFlash.containerId, objFlash.height, objFlash.width, altImagePath, flashEvent);

        if (selectedFlashElement) {
            selectedFlashElement.remove();
        }
        element = ckEditor.document.createElement('table');
        element.setAttribute('cellpadding', '0');
        element.setAttribute('cellspacing', '0');
        ckEditor.insertElement(element);
        flashHtml = String.format("<tbody><tr><td>{0}</td></tr></tbody>", flashHtml);
        element.setHtml(flashHtml);
    }
}
//method to link to sharepoint library
function fn_InsertProudctImageInEditor(objImage) {
    if (objImage) {
        //get current selection
        var sel = ckEditor.getSelection();
        //get the selected element or the element having the cursor
        var element = sel.getStartElement();
        if (element) {
            //get the nearest anchor tag
            element = element.getAscendant('img', true);
        }
        //if nearest tag is not found or is not an anchor tag then create a new anchor tag and insert it into the ourEditor
        if (!element || element.getName() != 'img') {
            element = ckEditor.document.createElement('img');
            element.setAttribute('src', objImage.src);
            element.setAttribute('alt', objImage.altText);
            element.setAttribute('objectId', objImage.objectId);
            element.setAttribute('isproductimage', 'true');
            ckEditor.insertElement(element);
        }
        else {
            //update the image tags when replacing an existing image
            element.remove();
            element = ckEditor.document.createElement('img');
            element.setAttribute('src', objImage.src);
            element.setAttribute('alt', objImage.altText);
            element.setAttribute('objectId', objImage.objectId);
            element.setAttribute('isproductimage', 'true');
            ckEditor.insertElement(element);
        }
    }
}
//method to insery sharepoint image
function fn_InsertSPImageInEditor() {
    var imageURL = popupActionsJson.CustomAttributes.FileUrl;
    if (imageURL) {
        //get current selection
        var sel = ckEditor.getSelection();
        //get the selected element or the element having the cursor
        var element = sel.getStartElement();
        if (element) {
            //get the nearest anchor tag
            element = element.getAscendant('img', true);
        }
        //if nearest tag is not found or is not an anchor tag then create a new anchor tag and insert it into the ourEditor
        if (!element || element.getName() != 'img') {
            element = ckEditor.document.createElement('img');
            element.setAttribute('src', imageURL);
            element.setAttribute('alt', '');
            element.setAttribute('source', 'SharePoint');   //do not translate
            ckEditor.insertElement(element);
        }
        else {
            //update the image tags when replacing an existing image
            element.remove();
            element = ckEditor.document.createElement('img');
            element.setAttribute('src', imageURL);
            element.setAttribute('alt', '');
            element.setAttribute('source', 'SharePoint');   //do not translate
            ckEditor.insertElement(element);
        }
    }
}
//method to link to sharepoint library
function fn_InsertSPLinkInEditor(objEditorProperties) {
    viewPopupWindow.hide();
}
//method to insert code snippet
function InsertCodeSnippetCKEditor() {
    if (typeof popupActionsJson.CustomAttributes.SnippetHtml != "undefined") {
        ckEditor.insertHtml(popupActionsJson.CustomAttributes.SnippetHtml);
    }
}

function fn_InsertCodeSnippet(snippetHtml) {
    if (snippetHtml) {
        ckEditor.insertHtml(snippetHtml);
    }
    viewPopupWindow.hide();
}
//method to insert a managed link
function fn_InsertManagedLinkInEditor() {
    var selectedLinkId = popupActionsJson.CustomAttributes.Id;
    if (selectedLinkId) {
        //get current selection
        var sel = ckEditor.getSelection();
        //get the selected element or the element having the cursor
        var element = sel.getStartElement();
        //get selected text
        var selectedText = sel.getSelectedText();
        var imgControl = false;
        if (element.getName().toLowerCase() == 'img') {
            var parentElement = element.getParent();
            if (parentElement != null && parentElement.getName().toLowerCase() == 'a') {
                element = parentElement;
            }
            imgControl = true;
        }
        if (element.getName().toLowerCase() == 'a') {
            element.remove();
        }
        element = ckEditor.document.createElement('a');
        ckEditor.insertElement(element);
        element.setAttribute('href', "[Link:ManagedLink:" + selectedLinkId + "|" + Data.Container.getAttribute("FWContainerID") + "]");
        if (imgControl) {
            element.setHtml(managedLinkText);
        }
        else {
            element.setText(managedLinkText);
        }
    }
}
//method to insert the unsubscribe link
function fn_InsertUnsubscribeLink(objEditor) {
    ckEditor = objEditor;
    if (objEditor) {
        element = ckEditor.document.createElement('a');
        element.setAttribute('href', '[Unsubscribe]');
        element.setText(__JSMessages["Unsubscribe"]);
        ckEditor.insertElement(element);
    }
}
