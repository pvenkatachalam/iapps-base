﻿CKEDITOR.plugins.add('SaveAsText',
{
    init: function (editor) {
        editor.addCommand('OpenSaveAsText',
        {
            exec: function (editor) {
                fn_SaveContentAs();
            }
        });
        editor.ui.addButton('SaveAsText',
        {
            label: 'Save As',
            command: 'OpenSaveAsText',
            icon: this.path + 'images/saveas.png'
        });
    }
});