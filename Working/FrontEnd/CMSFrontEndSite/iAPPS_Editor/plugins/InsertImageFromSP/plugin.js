﻿CKEDITOR.plugins.add('InsertImageFromSP',
{
    init: function (editor) {
        editor.addCommand('OpenInsertImageSP',
        {
            exec: function (editor) {
                fn_InsertSPImageOnClick(editor);
            }
        });
        editor.ui.addButton('InsertImageFromSP',
        {
            label: 'Insert image from Sharepoint',
            command: 'OpenInsertImageSP',
            icon: this.path + 'images/insertimage.png'
        });
    }
});