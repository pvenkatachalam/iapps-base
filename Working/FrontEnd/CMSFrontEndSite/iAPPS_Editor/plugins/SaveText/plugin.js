﻿CKEDITOR.plugins.add('SaveText',
{
    init: function (editor) {
        editor.addCommand('OpenSaveText',
        {
            exec: function (editor) {
                fn_SaveContent();
            }
        });
        editor.ui.addButton('SaveText',
        {
            label: 'Save',
            command: 'OpenSaveText',
            icon: this.path + 'images/save.png'
        });
    }
});