﻿CKEDITOR.plugins.add('LinkToSPLibrary',
{
    init: function (editor) {
        editor.addCommand('OpenLinkToSPLibrary',
        {
            exec: function (editor) {
                fn_LinkInEditorOnClick(editor, 'SharePoint');
            }
        });
        editor.ui.addButton('LinkToSPLibrary',
        {
            label: 'Link to Sharepoint Library',
            command: 'OpenLinkToSPLibrary',
            icon: this.path + 'images/LinkToLibrary.png'
        });
    }
});