﻿CKEDITOR.plugins.add('LinkToLibrary',
{
    init: function (editor) {
        editor.addCommand('OpenLinkToLibrary',
        {
            exec: function (editor) {
                fn_LinkInEditorOnClick(editor, 'Local');
            }
        });
        editor.ui.addButton('LinkToLibrary',
        {
            label: 'Link to Library',
            command: 'OpenLinkToLibrary',
            icon: this.path + 'images/LinkToLibrary.png'
        });
    }
});