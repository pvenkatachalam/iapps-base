﻿CKEDITOR.plugins.add('SaveSelectionAsSnippet',
{
    init: function (editor) {
        editor.addCommand('OpenSaveSelectionAsSnippet',
        {
            exec: function (editor) {
                fn_SaveSelectionAsSnippetOnClick(editor);
            }
        });
        editor.ui.addButton('SaveSelectionAsSnippet',
        {
            label: 'Save as Snippet',
            command: 'OpenSaveSelectionAsSnippet',
            icon: this.path + 'images/SaveSelectionAsSnippet.gif'
        });
    }
});