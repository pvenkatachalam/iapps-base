﻿CKEDITOR.plugins.add('InsertImageFromLibrary',
{
    init: function (editor) {
        editor.addCommand('OpenInsertImage',
        {
            exec: function (editor) {
                fn_InserImageOnClick(editor);
            }
        });
        editor.ui.addButton('InsertImageFromLibrary',
        {
            label: 'Insert Image',
            command: 'OpenInsertImage',
            icon: this.path + 'images/insertimage.png'
        });
    }
});