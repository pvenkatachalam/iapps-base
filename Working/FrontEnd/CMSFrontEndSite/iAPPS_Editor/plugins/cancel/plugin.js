﻿CKEDITOR.plugins.add('Cancel',
{
    init: function (editor) {
        editor.addCommand('OpenCancel',
        {
            exec: function (editor) {
                fn_CancelTextEdit();
            }
        });
        editor.ui.addButton('Cancel',
        {
            label: 'Cancel',
            command: 'OpenCancel',
            icon: this.path + 'images/cancel.png'
        });
    }
});