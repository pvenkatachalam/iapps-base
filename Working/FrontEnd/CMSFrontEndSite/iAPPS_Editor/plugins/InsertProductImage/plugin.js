﻿CKEDITOR.plugins.add('InsertProductImage',
{
    init: function (editor) {
        editor.addCommand('OpenInsertProductImage',
        {
            exec: function (editor) {
                fn_InsertProductImageOnClick(editor);
            }
        });
        editor.ui.addButton('InsertProductImage',
        {
            label: 'Insert product image',
            command: 'OpenInsertProductImage',
            icon: this.path + 'images/insertproductimage.png'
        });
    }
});