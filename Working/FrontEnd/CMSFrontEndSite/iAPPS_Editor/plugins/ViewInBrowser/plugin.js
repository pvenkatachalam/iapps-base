﻿CKEDITOR.plugins.add('ViewInBrowser',
{
    init: function (editor) {
        editor.addCommand('OpenViewInBrowser',
        {
            exec: function (editor) {
                fn_ViewInBrowserOnClick(editor);
            }
        });
        editor.ui.addButton('ViewInBrowser',
        {
            label: 'View in browser',
            command: 'OpenViewInBrowser',
            icon: this.path + 'images/viewinbrowser.png'
        });
    }
});