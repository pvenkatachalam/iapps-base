﻿CKEDITOR.plugins.add('InsertManagedLink',
{
    init: function (editor) {
        editor.addCommand('OpenManagedLink',
        {
            exec: function (editor) {
                fn_ManagedLinkOnClick(editor, 'Local');
            }
        });
        editor.ui.addButton('InsertManagedLink',
        {
            label: 'Managed Link',
            command: 'OpenManagedLink',
            icon: this.path + 'images/LinkToLibrary.png'
        });
    }
});