﻿CKEDITOR.plugins.add('EditImage',
{
    init: function (editor) {
        editor.addCommand('OpenEditImage',
        {
            exec: function (editor) {
                fn_EditImageOnClick(editor);
            }
        });
        editor.ui.addButton('EditImage',
        {
            label: 'Edit Image',
            command: 'OpenEditImage',
            icon: this.path + 'images/editimage.png'
        });
    }
});