﻿CKEDITOR.plugins.add('translatetool',
{
    requires: ['richcombo'],
    init: function (editor) {
        var config = editor.config,
         lang = 'en';  //Changed that

        // Gets the list of v_iappslanguage from the settings.
        //var v_iappslanguage = [];
        //this.add('value', 'drop_text', 'drop_label');
        //v_iappslanguage[0] = ["I", "One", "1"];
        //v_iappslanguage[1] = ["II", "Two", "2"];
        //v_iappslanguage[2] = ["III", "Three", "3"];

        // Create style objects for all defined styles.
        editor.ui.addRichCombo('translatetool',
         {
             label: "Translate Tool",
             title: "Translate Tool",
             voiceLabel: "Translate Tool",
             className: 'cke_format',
             multiSelect: false,
             toolbar: 'styles,20',

             panel:
            {
                css: [CKEDITOR.skin.getPath('editor')],
                voiceLabel: lang.panelVoiceLabel
            },

             init: function () {
                 this.startGroup("Translate Tool");
                 //this.add('value', 'drop_text', 'drop_label');
                 //v_iappslanguage array is rendered from CMSAPI
                 for (var this_tag = 0; this_tag < v_iappslanguage.length; this_tag++) {
                     this.add(v_iappslanguage[this_tag][0], v_iappslanguage[this_tag][1], v_iappslanguage[this_tag][2]);
                 }
             },

             onClick: function (value) {
                 editor.focus();
                 editor.fire('saveSnapshot');
                 fn_TranslateToolOnClick(value, editor);
                 editor.fire('saveSnapshot');
             }
         });
    }
});
