﻿CKEDITOR.plugins.add('InsertSnippet',
{
    init: function (editor) {
        editor.addCommand('OpenInsertSnippet',
        {
            exec: function (editor) {
                fn_InsertSnippetOnClick(editor);
            }
        });
        editor.ui.addButton('InsertSnippet',
        {
            label: 'InsertSnippet',
            command: 'OpenInsertSnippet',
            icon: this.path + 'images/InsertSnippet.png'
        });
    }
});