﻿CKEDITOR.plugins.add('InsertFlashFromLibrary',
{
    init: function (editor) {
        editor.addCommand('OpenInsertFlash',
        {
            exec: function (editor) {
                fn_InsertFlashOnClick(editor);
            }
        });
        editor.ui.addButton('InsertFlashFromLibrary',
        {
            label: 'Insert Flash',
            command: 'OpenInsertFlash',
            icon: this.path + 'images/insertflash.png'
        });
    }
});