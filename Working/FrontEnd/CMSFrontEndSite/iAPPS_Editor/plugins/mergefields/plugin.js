﻿CKEDITOR.plugins.add('mergefields',
{
    requires: ['richcombo'], //, 'styles' ],
    init: function (editor) {
        var config = editor.config,
         lang = 'en';

        // Create style objects for all defined styles.

        editor.ui.addRichCombo('mergefields',
         {
             label: "Merge Fields",
             title: "Merge Fields",
             voiceLabel: "Merge Fields",
             className: 'cke_format',
             multiSelect: false,
             toolbar: 'styles,20',

             panel:
            {
                css: [CKEDITOR.skin.getPath('editor')],
                voiceLabel: lang.panelVoiceLabel
            },

             init: function () {
                 this.startGroup("Merge Fields");
                 //this.add('value', 'drop_text', 'drop_label');
                 //v_iappsmergefields array is rendered from CMSAPI
                 for (var this_tag = 0; this_tag < v_iappsmergefields.length; this_tag++) {
                     this.add(v_iappsmergefields[this_tag][0], v_iappsmergefields[this_tag][1], v_iappsmergefields[this_tag][2]);
                 }
             },

             onClick: function (value) {
                 editor.focus();
                 editor.fire('saveSnapshot');
                 editor.insertHtml(value);
                 editor.fire('saveSnapshot');
             }
         });
    }
});
