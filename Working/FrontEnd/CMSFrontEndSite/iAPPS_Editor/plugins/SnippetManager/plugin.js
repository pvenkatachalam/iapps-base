﻿CKEDITOR.plugins.add('SnippetManager',
{
    init: function (editor) {
        editor.addCommand('OpenSnippetManager',
        {
            exec: function (editor) {
                fn_SnippetManagerOnClick(editor);
            }
        });
        editor.ui.addButton('SnippetManager',
        {
            label: 'Snippet Manager',
            command: 'OpenSnippetManager',
            icon: this.path + 'images/SnippetManager.gif'
        });
    }
});