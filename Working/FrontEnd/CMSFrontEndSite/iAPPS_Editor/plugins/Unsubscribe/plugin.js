﻿CKEDITOR.plugins.add('Unsubscribe',
{
    init: function (editor) {
        editor.addCommand('Unsubscribe',
        {
            exec: function (editor) {
                fn_InsertUnsubscribeLink(editor);
            }
        });
        editor.ui.addButton('Unsubscribe',
        {
            label: 'Insert Unsubscribe Link',
            command: 'Unsubscribe',
            icon: this.path + 'images/unsubscribe-icon.gif'
        });
    }
});