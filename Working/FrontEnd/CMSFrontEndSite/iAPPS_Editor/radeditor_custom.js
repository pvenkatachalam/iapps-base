//global variable reference to the editor
var ckEditor = null;
var oureditor = null;
var currentRange = null;
var o_Arguments = new Object();
var iapps_invalidTags = '<form';
//method called from editor's onclientload
function EditorOnLoad(editor) {
    oureditor = editor;
    //setTimeout(function () { PopulateSnippets(editor); }, 250);
    var languagedropdown = oureditor.getToolByName("TranslateTool").get_element();
    languagedropdown.setAttribute("title", __JSMessages["PoweredByBing"]);
}

function fn_CheckForInvalidTags(editorInUse) {
    var iapps_actualText;
    var iapps_editor = editorInUse;
    if (EditorToUse.toLowerCase() == 'radeditor') {
        oureditor = editorInUse;
        iapps_actualText = iapps_editor.get_html(true);
    }
    else {
        ckEditor = editorInUse;
        iapps_actualText = iapps_editor.getData();
    }
    var objRE = new RegExp(iapps_invalidTags, "i");
    if (objRE.test(iapps_actualText)) {
        alert(__JSMessages["InvalidTagsInContent"]);
        if (EditorToUse.toLowerCase() == 'radeditor') {
            iapps_editor.set_mode(2);
        }
        else {
            iapps_editor.setMode('source');
        }
    }
}

function OnClientModeChange(editor, args) {
    var mode = editor.get_mode();
    switch (mode) {
        case 1:
            //design mode
            fn_CheckForInvalidTags(editor);
            break;
        case 2:
            //html mode
            break;
        case 4:
            //preview mode
            break;
    }
}

//Site Edit Mode - Edit and Directory selection part
var tempFwObjectId;
function SelectTargetDirectoryToSaveContent() {

    tempFwObjectId = Data.HiddenFwObjectId.value
    OpeniAppsAdminPopup("SelectTargetDirectory", "IsMasterContent=" + Data.HiddenIsMasterContent.value + "&ContentId=" + Data.HiddenFwObjectId.value + "&VariantContentExists=" + Data.VariantContentExists, "CloseTargetDirectory");
}

function CloseTargetDirectory() {
    Data.HiddenParentId.value = popupActionsJson.FolderId;
    if (popupActionsJson.CustomAttributes["CreateVariantContent"] == true) {
        Data.HiddenSourceObjectId.value = tempFwObjectId;
    }

    if (Data.HiddenContentTitleElement) {
        Data.HiddenContentTitleElement.value = popupActionsJson.CustomAttributes["ContentTitle"];
    }

    if (Data.HiddenIsMasterContent) {
        Data.HiddenIsMasterContent.value = "False";
    }
}

function SetSelectedNodeId(Id) {
    if (Id != "" && Id != null) {
        Data.HiddenParentId.value = Id;

    }
}
function SetContentTitle(title) {
    if (Data.HiddenContentTitleElement) {
        Data.HiddenContentTitleElement.value = title;
    }
}
var show2ndTime = "No";
var textToEdit;
function openEditor() {
    NewContent = true;
    show2ndTime = "Yes";
    ShowEditor(srcControl);
}
function OnClientCancel(editor) {
    srcControl.style.visibility = "visible";
    document.getElementById("editableArea").style.display = "none";
    oureditor.set_html("");
    NewContent = false;
    return false;
}
function ShowEditor(controlID) {
    if (EditorToUse.toLowerCase() == 'radeditor') {
        if (srcControl != null) {
            srcControl.style.visibility = "visible";
            document.getElementById("editableArea").style.display = "none";
        }
        srcControl = controlID;
        if (Trim(Data.HiddenControlValue.value) == "") {
            // If there is no content dont show the Edit Text Context Menu Option or give a alert
            if (NewContent) {
                CreateNewContent(controlID);
            }
            else {
                alert(__JSMessages["PleaseInsertAContentItem"]);
                return;
            }
        }
        else {
            if (NewContent) {
                if (show2ndTime == "No") {
                    textToEdit = "";
                }
                else {
                    textToEdit = Data.HiddenControlValue.value;
                }
                show2ndTime = "No";
            }
            else {
                textToEdit = Data.HiddenControlValue.value;
            }

            if (Trim(textToEdit) == '&nbsp;')
                textToEdit = '';
            fn_PositionRadEditor(srcControl, $('#editableArea'), textToEdit);
        }
    }
    else if (EditorToUse.toLowerCase() == 'ckeditor') {
        if (srcControl != null) {
            srcControl.style.visibility = "visible";
            document.getElementById("editorContainer").style.display = "none";
        }
        srcControl = controlID;
        if (Trim(Data.HiddenControlValue.value) == "") {
            // If there is no content dont show the Edit Text Context Menu Option or give a alert
            if (NewContent) {
                textToEdit = " ";
                fn_PositionCKEditor(controlID, $('#editorContainer'), textToEdit);
            }
            else {
                alert(__JSMessages["PleaseInsertAContentItem"]);
                return;
            }
        }
        else {
            if (NewContent) {
                if (show2ndTime == "No") {
                    textToEdit = "";
                }
                else {
                    textToEdit = Data.HiddenControlValue.value;
                }
                show2ndTime = "No";
            }
            else {
                textToEdit = Data.HiddenControlValue.value;
            }
            //get the position of the content
            fn_PositionCKEditor(controlID, $('#editorContainer'), textToEdit);
            if (Trim(textToEdit) == '&nbsp;')
                textToEdit = '';
        }
    }
}
//method to position RadEditor and set the text
function fn_PositionRadEditor(objTextContainer, objEditorContainer, textToEdit) {
    var contentPosition = $(objTextContainer).parent().offset();
    //set the position of the editor
    $(objEditorContainer).css('left', contentPosition.left);
    $(objEditorContainer).css('top', contentPosition.top);
    $(objEditorContainer).css('position', 'absolute');
    $(objEditorContainer).css('zIndex', '19996');
    oureditor.setSize($(objTextContainer).parent().outerWidth(), ($(objTextContainer).parent().outerHeight() + 200));
    $(objTextContainer).css('visibility', 'hidden');
    $(objEditorContainer).css('display', 'block');
    objTextContainer.style.visibility = "hidden";
    var t = setTimeout("ShowFloatingToolBar()", 1000);
    oureditor.setFocus();
    var editorMode = oureditor.get_mode();
    if (editorMode != 1) {
        oureditor.set_mode(1);
        setTimeout('oureditor.repaint();', 1000);
    }
}

function CreateNewContent(controlID) {
    //assign a blank space to textToEdit variable for it to work fine in FireFox.
    textToEdit = " ";
    fn_PositionRadEditor(srcControl, $('#editableArea'), textToEdit);
}

function ConvertToHtml(str) {
    var replacedString = str;
    replacedString = replacedString.replace(new RegExp("&amp;", "g"), "&");
    replacedString = replacedString.replace(new RegExp("&lt;", "g"), "<");
    replacedString = replacedString.replace(new RegExp("&gt;", "g"), ">");
    replacedString = replacedString.replace(new RegExp("&quot;", "g"), "\"");
    replacedString = replacedString.replace(new RegExp("&apos;", "g"), "'");
    replacedString = replacedString.replace(new RegExp("&slash;", "g"), "\\");
    replacedString = replacedString.replace(new RegExp("&spamp;", "g"), "&");
    replacedString = replacedString.replace(new RegExp("&splt;", "g"), "<");
    replacedString = replacedString.replace(new RegExp("&doublequot;", "g"), "\"");
    return replacedString;
}

//For Link to Library button
var selectedText;
var controlTag;
var selectedValue = null;
var hrefPath;
var imgTagPos;
var imgControlTag;
var typeValue;
var s, s1, s2, str2, str3;
var SelectedObject = new Object();
var features = "";

//check if radeditor is used
if (EditorToUse.toLowerCase() == 'radeditor') {
    Telerik.Web.UI.Editor.CommandList["LinktoLibrary"] =
		function CustomDialogCommand(commandName, editor, oTool) {
		    CommandToggleLink(commandName, editor, oTool, "Local");
		};
    Telerik.Web.UI.Editor.CommandList["LinktoSPLibrary"] =
		function ShowSPLibrary(commandName, editor, oTool) {
		    CommandToggleLink(commandName, editor, oTool, "SharePoint");
		};
    Telerik.Web.UI.Editor.CommandList["SaveAsContent"] =
		function SaveAsContent(commandName, editor, oTool) {
		    ContentSaveAs();
		};
    Telerik.Web.UI.Editor.CommandList["SaveContent"] =
		function SaveContent(commandName, editor, oTool) {
		    ContentSave();
		};
    Telerik.Web.UI.Editor.CommandList["CancelContent"] =
		function CancelContent(commandName, editor, oTool) {
		    ContentCancel();
		};
    Telerik.Web.UI.Editor.CommandList["InsertImagefromLibrary"] =
		function CustomImageManagementCommand(commandName, editor, oTool) {
		    EditorImageDialog = 'Editor';   //do not translate
		    selectedObject = new Object();
		    selectedObject.Scope = "Editor";
		    oureditor = editor;
		    currentRange = oureditor.getSelection().getRange();
		    OpeniAppsAdminPopup("SelectImageLibraryPopup", "", "InsertImageInRadEditor");
		};
    Telerik.Web.UI.Editor.CommandList["InsertImagefromSPLibrary"] =
		function CustomSPImageManagementCommand(commandName, editor, oTool) {
		    EditorImageDialog = "Editor";   //do not translate
		    selectedObject = new Object();
		    selectedObject.Scope = "Editor";
		    oureditor = editor;
		    currentRange = oureditor.getSelection().getRange();
		    OpeniAppsAdminPopup("SelectSharepointLibrary", "Type=Image", "InsertSPImageInRadEditor");
		};
    Telerik.Web.UI.Editor.CommandList["LinkToProduct"] =
		function CustomLinkToProductManagementCommand(commandName, editor, oTool) {
		    var adminpath = jCommerceAdminSiteUrl + '/Popups/StoreManager/SelectProductImagePopup.aspx?ShowProducts=true';
		    EditorLinkToProductDialog = "Editor";   //do not translate
		    selectedLinkToProductObject = new Object();
		    selectedLinkToProductObject.Scope = "Editor";
		    oureditor = editor;
		    adminpath = SendTokenRequestByURLInSiteWithEditorForInsertLinkToProduct(adminpath);
		};
    Telerik.Web.UI.Editor.CommandList["InsertProductImage"] =
		function CustomProductImageManagementCommand(commandName, editor, oTool) {
		    EditorProductImageDialog = 'Editor';    //do not trasnlate
		    selectedProductImageObject = new Object();
		    selectedProductImageObject.Scope = "Editor";
		    oureditor = editor;
		    currentRange = oureditor.getSelection().getRange();
		    OpeniAppsCommercePopup('SelectProductImage', 'ShowProducts=true');
		};
    Telerik.Web.UI.Editor.CommandList["InsertFlashFromLibrary"] =
		function CustomFlashManagementCommand(commandName, editor, oTool) {
		    selectedFlashElement = null;
		    var theSelectionObject = editor.getSelection();
		    selectedFlashObject = new Object();
		    if (theSelectionObject != null) {
		        var theSelectedElement = theSelectionObject.GetParentElement();
		        if (theSelectedElement && theSelectedElement.tagName == "IMG") {
		            theSelectedElement = theSelectedElement.parentNode;
		            if (theSelectedElement && theSelectedElement.tagName == "DIV") {
		                selectedFlashElement = theSelectedElement;
		                var linkProperties = new Object();
		                if (theSelectedElement.getAttribute("returnValue") != null) {
		                    linkProperties = theSelectedElement.getAttribute("returnValue").split(",");
		                    selectedFlashObject.FilePathValue = linkProperties[0].replace("%3A", ":");
		                    selectedFlashObject.ContainerIdValue = linkProperties[1];
		                    selectedFlashObject.WidthValue = linkProperties[2];
		                    selectedFlashObject.HeightValue = linkProperties[3];
		                    selectedFlashObject.AltImagePathValue = linkProperties[4].replace("%3A", ":");
		                    selectedFlashObject.MinVersionValue = linkProperties[5];
		                    selectedFlashObject.QualityValue = linkProperties[6];
		                    selectedFlashObject.AutoPlayValue = linkProperties[7];
		                }
		            }
		        }
		    }
		    EditorFlashDialog = 'Editor';   //do not translate
		    selectedFlashObject.Scope = "Editor";
		    oureditor = editor;
		    OpeniAppsAdminPopup("ShowFlashManager", "", "SelectedFlash");
		};
    Telerik.Web.UI.Editor.CommandList["EditImage"] =
		function CustomEditImageCommand(commandName, editor, oTool) {
		    var theSelectionObject = editor.getSelection();
		    var theSelectedElement = theSelectionObject.GetParentElement();
		    if (theSelectedElement.tagName.toLowerCase() != "img") {
		        alert(__JSMessages["PleaseSelectTheImageYouWantToEdit"]);
		        return;
		    }

		    if (theSelectedElement.getAttribute("isproductimage") != null) {
		        alert(__JSMessages["CanNotEditProductImage"]);
		        return;
		    }

		    if (theSelectedElement.getAttribute("source") == "SharePoint") {
		        alert(__JSMessages["ThisImageIsLoadedFromSharePointLibrary"]);
		    }
		    else {
		        var imageObj = JSON.parse(FWCallback.GetImageIdAndSiteId(siteId, theSelectedElement.getAttribute("src")));
		        if (imageObj.SiteId.toLowerCase() != siteId.toLowerCase()) {
		            alert(__JSMessages["YouDoNotHavePermissionToEditThisImage"]);
		        }
		        else if (imageObj.ImageId == null) {
		            alert(__JSMessages["ThisVersionOfTheImageCanNotBeEdited"]);
		        }
		        else {
		            EditorImageDialog = "Editor";   //do not translate
		            editedObject = new Object();
		            editedObject.Scope = "Editor";
		            oureditor = editor;
		            OpeniAppsAdminPopup("ImageEditor", "ImageId=" + imageObj.ImageId, "InsertImageInRadEditor");
		        }
		    }
		};
    // START added for Marketier - Unsubscribe Link 
    Telerik.Web.UI.Editor.CommandList["UnsubscribeLink"] =
		function CustomUnsubscribeCommand(commandName, editor, oTool) {
		    oureditor.pasteHtml('"<a href=[Unsubscribe]>Unsubscribe</a>"');
		};
    // START added for Marketier - Manage Link 
    Telerik.Web.UI.Editor.CommandList["ManageLink"] =
		function ManageLinkCommand(commandName, editor, oTool) {
		    selectedText = editor.getSelectionHtml();
		    if (selectedText == "") {
		        alert(__JSMessages["PleaseSelectTheTextYouWantToConvertToALink"]);
		        return;
		    }
		    var theSelectionObject = editor.getSelection();
		    var theSelectedElement = theSelectionObject.GetParentElement();
		    if (theSelectedElement) {
		        var tempSelectedElement = theSelectedElement.parentElement;
		        if (tempSelectedElement) {
		            if (tempSelectedElement.tagName == "A") {
		                theSelectedElement = tempSelectedElement;
		                imgControl = true;
		            }
		        }
		    }
		    if (theSelectedElement) {
		        if (theSelectedElement.tagName == "A") {
		            var linkProperties = new Object();
		            if (theSelectedElement.getAttribute("href") != null) {
		                linkProperties = theSelectedElement.getAttribute("href").split(",");
		                SelectedObject.HrefValue = linkProperties[0];
		            }
		            else {
		                alert(__JSMessages["PleaseUseTheLinksAndBookmarksToOpenThisLink"]);
		                return;
		            }
		            SelectedObject.SelectedText = theSelectedElement.innerHTML;
		        }
		        else {
		            SelectedObject = new Object();
		            SelectedObject.SelectedText = selectedText;
		        }
		    }
		    currentRange = oureditor.getSelection().getRange();
		    OpeniAppsMarketierPopup('InsertManagedLink', '', 'fn_InsertManagedLink');
		    oureditor = editor;
		};
    Telerik.Web.UI.Editor.CommandList["SnippetManager"] =
		function ShowSnippetManager(commandName, editor, oTool) {
		    EditorImageDialog = 'Editor';   //do not translate
		    selectedObject = new Object();
		    selectedObject.Scope = "Editor";
		    oureditor = editor;
		    currentRange = oureditor.getSelection().getRange();

		    if (jProductId.toLowerCase() == jMarketierProductId.toLowerCase()) {
		        var adminpath = jmarketierAdminURL + "/Popups/ShowSnippetManager.aspx";
		        SendTokenRequestByURLInSiteWithEditorForSnippetManager(adminpath);
		    }
		    else {
		        OpeniAppsAdminPopup("ShowSnippetManager", "", "InsertCodeSnippet");
		    }
		};
    Telerik.Web.UI.Editor.CommandList["CustomInsertSnippet"] =
		function ShowSnippetManager(commandName, editor, oTool) {
		    EditorImageDialog = 'Editor';   //do not translate
		    selectedObject = new Object();
		    selectedObject.Scope = "Editor";
		    oureditor = editor;
		    currentRange = oureditor.getSelection().getRange();

		    if (jProductId.toLowerCase() == jMarketierProductId.toLowerCase()) {
		        var adminpath = jmarketierAdminURL + "/Popups/ShowSnippetManager.aspx";
		        SendTokenRequestByURLInSiteWithEditorForSnippetManager(adminpath);
		    }
		    else {
		        OpeniAppsAdminPopup("ShowSnippetManager", "ReadOnly=true", "InsertCodeSnippet");
		    }
		};
    Telerik.Web.UI.Editor.CommandList["SaveSelectionAsSnippet"] =
		function SaveSelectionAsSnippet(commandName, editor, oTool) {
		    var htmlString;
		    if (editor.getSelectionHtml() != "" && editor.getSelectionHtml() != "&nbsp;" && editor.getSelectionHtml() != "<br>") {
		        htmlString = editor.getSelectionHtml();
		    }
		    else {
		        htmlString = editor.get_html();
		    }
		    if (htmlString != "" && htmlString != "&nbsp;" && htmlString != "<br>") {
		        var snippetName = prompt(__JSMessages["PleaseEnterANameForTheSnippet"]);
		        if (snippetName != null && snippetName != "") {
		            var snippetId = SaveSnippetToDb(snippetName, htmlString, siteId);
		            //PopulateSnippets(editor);
		        }
		        else {
		            alert(__JSMessages["YouMustEnterANameForTheSnippet"]);
		        }
		    }
		    else {
		        alert(__JSMessages["YouMustAddContentToTheEditor"]);
		    }
		};
    /* Google Translate Tool for Site Editor Translation */
    Telerik.Web.UI.Editor.CommandList["TranslateTool"] =
		function TranslateText(commandName, editor, args) {
		    if (args.get_name() == "TranslateTool") {
		        var _maxLength = 5000; //Max Length in Characters that can be sent to the Google API for translation
		        var tool = editor.getToolByName("TranslateTool");
		        src_lang = ''; //Set to blank for auto language detection
		        target_lang = args.get_value(); //Grab language code for the language being translated to
		        var hasSelection = true;
		        var html = editor.getSelectionHtml().trim(); //returns the currently selected HTML content
		        if (!html) {
		            html = editor.get_html(true); //obtain the editor's content
		            hasSelection = false;
		        }
		        if (html.length <= _maxLength) {
		            var result = SendTranslation(src_lang, target_lang, html);
		            if (hasSelection)
		                editor.pasteHtml(result);
		            else
		                editor.set_html(result);
		        }
		        else { alert(stringformat(__JSMessages["RequestIsTooLarge"], _maxLength)); }
		        args.set_cancel(true);
		    }
		};

    Telerik.Web.UI.Editor.CommandList["InsertViewInBrowserLink"] = function InsertViewInBrowserLink(commandName, editor, args) {
        oureditor.pasteHtml('"<a href=[ViewInBrowserLink]>' + __JSMessages["ViewThisEmailInBrowser"] + '</a>"');
    };

    Telerik.Web.UI.Editor.CommandList["CSSClass"] = function CSSClass(commandName, editor, args) {
        o_Arguments.elem = editor.getSelectedElement();
        var v_CssArray = editor.getCssArray();
        var v_Classes = new Array();
        $(v_CssArray).each(function (i, e) {
            v_Classes.push(e[0]);
        });
        o_Arguments.classes = v_Classes;
        if ($(o_Arguments.elem).get(0).tagName.toLowerCase() != 'body') {
            OpeniAppsAdminPopup("ApplyMutliCSSClasses", "", "");
        }
        else {
            alert('Please select an element.');
        }
    };
}
else if (EditorToUse.toLowerCase() == 'ckeditor') {
    //setting the editor global variable when the editor initializes
    $(document).ready(function () {
        CKEDITOR.on('instanceReady', function (ev) {
            ckEditor = ev.editor;
            fn_AddSharepointText();
            if (typeof SetClassAndIdCKEditor == 'function') {
                SetClassAndIdCKEditor();
            }
            //fired when editors mode is set
            ckEditor.on('mode', function (ev) {
                fn_CKEditorModeChange(ev);
            });
            //fired before setting editor's mode
            ckEditor.on('beforeSetMode', function (ev) {

            });
            ckEditor.on('beforeModeUnload', function (ev) {
                fn_CKEditorBeforeModeChange(ev);
            });
            ckEditor.on('key', function (ev) {

            });
        });
    });
}

var selectionRange;
function CommandToggleLink(commandName, editor, oTool, targetLibrary) {
    selectedText = editor.getSelectionHtml();
    if (selectedText == "") {
        alert(__JSMessages["PleaseSelectTheTextYouWantToConvertToALink"]);
        return;
    }

    var theSelectionObject = editor.getSelection();
    //selecting range, as the selection was lost on opening a popup.
    selectionRange = theSelectionObject.getRange();
    //Get a reference the selected element:
    var imgControl = false;
    var theSelectedElement = theSelectionObject.GetParentElement();
    if (theSelectedElement) {
        var tempSelectedElement = theSelectedElement.parentElement;
        if (tempSelectedElement) {
            if (tempSelectedElement.tagName == "A") {
                theSelectedElement = tempSelectedElement;
                imgControl = true;
            }
        }
    }
    var isCMSLink = true;
    if (theSelectedElement) {
        if (theSelectedElement.tagName == "A") {
            var linkProperties = new Object();
            if (theSelectedElement.getAttribute("returnValue") != null) {
                linkProperties = theSelectedElement.getAttribute("returnValue").split(",");
                SelectedObject.HrefValue = linkProperties[0];
            }
            else {
                if (theSelectedElement.getAttribute("href").indexOf("Link:") > 0) {
                    //it is a marketier link
                    var linkType = theSelectedElement.getAttribute("href").substring(6, 11);
                    var linkTypeId;
                    var linkContainerId;
                    if (linkType == 'Page:' || linkType == 'File:') {
                        linkType = theSelectedElement.getAttribute("href").substring(6, 10); //Image is having 5 letters
                        linkTypeId = theSelectedElement.getAttribute("href").substring(11, 47);
                        linkContainerId = theSelectedElement.getAttribute("href").substring(48, 84);
                    }
                    if (linkType == 'Image') {
                        linkTypeId = theSelectedElement.getAttribute("href").substring(12, 48);
                        linkContainerId = theSelectedElement.getAttribute("href").substring(49, 85);
                    }
                    isCMSLink = false;
                    SelectedObject.TypeValue = linkType;
                    SelectedObject.Id = linkTypeId;
                    SelectedObject.HrefValue = '';
                }
                else {
                    alert(__JSMessages["PleaseUseTheLinksAndBookmarksToOpenThisLink"]);
                    return;
                }
            }
            if (imgControl) {
                SelectedObject.SelectedText = theSelectedElement.innerHTML;
            }
            else {
                SelectedObject.SelectedText = theSelectedElement.innerHTML;
            }

            if (isCMSLink) {
                SelectedObject.TypeValue = linkProperties[2];
                SelectedObject.TargetValue = linkProperties[3];
                SelectedObject.Height = linkProperties[4];
                SelectedObject.Width = linkProperties[5];
                SelectedObject.showAddressBar = linkProperties[6];
                SelectedObject.showMenuBar = linkProperties[7];
                SelectedObject.showStatusBar = linkProperties[8];
                SelectedObject.allowScrollBar = linkProperties[9];
                SelectedObject.Id = linkProperties[10];
                SelectedObject.watchcode = linkProperties[11];
            }
        }
        else {
            SelectedObject = new Object();
            SelectedObject.SelectedText = selectedText;
        }
    }
    imgControl = false;
    oureditor = editor;
    gTargetLibrary = targetLibrary;
    SelectedObject.TargetLibrary = targetLibrary;
    OpeniAppsAdminPopup("ShowHyperLinkManager", "OpenSharedPopup=1", "CloseHyperLinkManager");
};

function CloseHyperLinkManager() {
    returnValue = JSON.parse(popupActionsJson.CustomAttributes.LinkReturnValue);
    if (returnValue) {
        CreateWindowOpenFunction(returnValue);
        OpenWindow(returnValue);
        CreateLinkPropertiesArray(returnValue);

        var onclickEvent = '';
        //start watch code
        if (returnValue.watchcode != undefined && returnValue.watchcode.toLowerCase() != ';nowatch') {
            var watchvalue = returnValue.watchcode.toString().split(";");

            if (watchvalue[1] = "ClientWatch") {
                onclickEvent = "onclick=\"iAPPSTracker('" + watchvalue[0] + "');\"";
            }
        }
        //End watch code

        var linkValue = '';
        //START added for Marketier - Email Template
        if (jTemplateType == '1' && LinkPropertiesArray[2] != 'ExternalURL') // Email Template
        {
            linkValue = "<a href=\"[Link:" + LinkPropertiesArray[2] + ":" + LinkPropertiesArray[10] + "|" + Data.Container.getAttribute("FWContainerID") + "|" + Data.Container.getAttribute("id") + "]" + "\" returnValue=\"" + LinkPropertiesArray + "\">" + returnValue.text + "</a>";
        }
        else    //CMS Template
        {
            if (returnValue.target == "_blank") {
                linkValue = "<a href=\"" + windowOpen + "\" target=\"_blank\" " + onclickEvent + " returnValue=\"" + LinkPropertiesArray + "\" >" + returnValue.text + "</a>";
            }
            else if (returnValue.target == "_modal") {
                linkValue = "<a href=\"" + windowOpen + "\" rel=\"modal\" " + onclickEvent + " returnValue=\"" + LinkPropertiesArray + "\" >" + returnValue.text + "</a>";
            }
            else {
                linkValue = "<a href=\"" + windowOpen + "\" " + onclickEvent + " returnValue=\"" + LinkPropertiesArray + "\" >" + returnValue.text + "</a>";
            }
        }
        oureditor.getSelection().selectRange(selectionRange);
        //START added for Marketier - Email Template
        oureditor.pasteHtml(linkValue);
    }
}

var gTargetLibrary
function ShowEditorWithToken(hyperLinkManageradminpath) {

    SelectedObject.TargetLibrary = gTargetLibrary;
    var sLibraryWindowName = __JSMessages["LinkToLibrary"];
    if (gTargetLibrary == "SharePoint")
        sLibraryWindowName = __JSMessages["LinkToSharePointLibrary"];
    oureditor.showExternalDialog(
		hyperLinkManageradminpath,
		SelectedObject,
		1040,
		600,
		CustomDialogCallback,
		null,
		sLibraryWindowName,
		true,
		Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
		false,
		false);
}

function fn_AssignCssClasses(assignedClasses) {
    $(oureditor.getSelectedElement()).removeClass();
    $(oureditor.getSelectedElement()).addClass(assignedClasses);
}

function GetSelectedText(selectedText) {
    var str = ">";
    var str1 = "<";
    s = selectedText.indexOf(str, 2);
    s1 = selectedText.indexOf(str1, 2);
    s2 = s1 - s;
    selectedValue = selectedText.substr(s + 1, s2 - 1);
}
function GetHrefValue(selectedText) {
    str2 = "href";
    str3 = "\"";
    s = selectedText.indexOf(str2);
    s1 = selectedText.indexOf(str3, s + 6);
    hrefPath = selectedText.substring(s + 6, s1);
}
function GetTypeValue(selectedText) {
    str2 = "type";
    str3 = ">";
    s = selectedText.indexOf(str2);
    s1 = selectedText.indexOf(str3, s);
    typeValue = selectedText.substring(s + 5, s1);
}

function GetImagePath(selectedText) {
    str2 = "src";
    str3 = "\"";
    s = selectedText.indexOf(str2);
    s1 = selectedText.indexOf(str3, s + 5);
    selectedValue = selectedText.substring(s + 5, s1);
}

var CustomDialogCallback = function (sender, args) {
    returnValue = args;
    if (returnValue) {
        CreateWindowOpenFunction(returnValue);
        OpenWindow(returnValue);
        CreateLinkPropertiesArray(returnValue);

        var onclickEvent = '';
        //start watch code
        if (returnValue.watchcode != undefined && returnValue.watchcode.toLowerCase() != ';nowatch') {
            var watchvalue = returnValue.watchcode.toString().split(";");

            if (watchvalue[1] = "ClientWatch") {
                onclickEvent = "onclick=\"iAPPSTracker('" + watchvalue[0] + "');\"";
            }
        }
        //End watch code

        var linkValue = '';

        //START added for Marketier - Email Template
        if (jTemplateType == '1' && LinkPropertiesArray[2] != 'ExternalURL') // Email Template
        {
            linkValue = "<a href=\"[Link:" + LinkPropertiesArray[2] + ":" + LinkPropertiesArray[10] + "|" + Data.Container.getAttribute("FWContainerID") + "|" + Data.Container.getAttribute("id") + "]" + "\" >" + returnValue.text + "</a>";
        }
        else    //CMS Template
        {
            if (returnValue.target == "_blank") {
                linkValue = "<a href=\"" + windowOpen + "\" target=\"_blank\" " + onclickEvent + " returnValue=\"" + LinkPropertiesArray + "\" >" + returnValue.text + "</a>";
            }
            else if (returnValue.target == "_modal") {
                linkValue = "<a href=\"" + windowOpen + "\" rel=\"modal\" " + onclickEvent + " returnValue=\"" + LinkPropertiesArray + "\" >" + returnValue.text + "</a>";
            }
            else {
                linkValue = "<a href=\"" + windowOpen + "\" " + onclickEvent + " returnValue=\"" + LinkPropertiesArray + "\" >" + returnValue.text + "</a>";
            }
        }
        //START added for Marketier - Email Template
        oureditor.pasteHtml(linkValue);
    }
}
var windowOpen;

function OpenWindow(returnValue) {
    var URLToOpen = escape(returnValue.url.replace(new RegExp("\'", "g"), "\\'"));
    URLToOpen = URLToOpen.replace(new RegExp("%3A", "g"), ":");
    URLToOpen = URLToOpen.replace(new RegExp("%3a", "g"), ":");
    if (returnValue.target == "_blankPopUp") {
        //If target is popup, open the popup with the features
        windowOpen = "javascript:showLink('" + URLToOpen + "','_blankPopUp','" + features + "');"
    }
    else if (returnValue.target == "_self") {
        windowOpen = unescape(findAndReplaceURL(URLToOpen));; //in case just ahref link ..thats all
    }
    else if (returnValue.target == "_blank") {
        //open the window with the default values
        windowOpen = unescape(findAndReplaceURL(URLToOpen));;
    }
    else {
        //open the window with the default values
        windowOpen = "javascript:showLink('" + URLToOpen + "','" + returnValue.target + "');"
    }
}

function CreateWindowOpenFunction(returnValue) {

    features = "";
    if (returnValue.showAddressBar)
        features = "location=yes,"
    else
        features = "location=no,"

    if (returnValue.showMenuBar)
        features = features + "menubar=yes,"
    else
        features = features + "menubar=no,"

    if (returnValue.showStatusBar)
        features = features + "staus=yes,"
    else
        features = features + "staus=no,"

    if (returnValue.allowScrollBar)
        features = features + "scrollbars=yes,"
    else
        features = features + "scrollbars=no,"

    if (returnValue.height != null && returnValue.height != "")
        features = features + "height=" + returnValue.height + ",";

    if (returnValue.width != null && returnValue.width != "")
        features = features + "width=" + returnValue.width;

    //if (returnValue.watchcode != null && returnValue.watchcode != "")
    //   features = features + "watchcode=" + returnValue.watchcode;

    if (returnValue.id != null && returnValue.id != "")
        features = features + "Id=" + returnValue.Id;

}

var LinkPropertiesArray;

function CreateLinkPropertiesArray(returnValue) {
    LinkPropertiesArray = new Array();
    LinkPropertiesArray[0] = escape(returnValue.url).replace(new RegExp("%3A", "g"), ":");
    LinkPropertiesArray[1] = "";
    LinkPropertiesArray[2] = returnValue.type;
    LinkPropertiesArray[3] = returnValue.target;
    LinkPropertiesArray[4] = returnValue.height;
    LinkPropertiesArray[5] = returnValue.width;

    LinkPropertiesArray[6] = returnValue.showAddressBar;
    LinkPropertiesArray[7] = returnValue.showMenuBar;
    LinkPropertiesArray[8] = returnValue.showStatusBar;
    LinkPropertiesArray[9] = returnValue.allowScrollBar;
    LinkPropertiesArray[10] = returnValue.Id;
    LinkPropertiesArray[11] = returnValue.watchcode;
}
//For Insert Image option
var EditorImageDialog;
var selectedObject;

var SelectedImageInRad = function (args) {
    if (args != null && args.src != null) {
        oureditor.pasteHtml(String.format("<img src='{0}' alt='{1}' objectId='{2}' />", args.src, args.altText, args.objectId));
    }
}

//For Insert Product Image option
var EditorProductImageDialog;
var selectedProductImageObject;

function ShowEditorWithTokenForInsertProductImage(adminpath) {
    oureditor.showExternalDialog(
		adminpath,
		selectedProductImageObject,
		950,
		610,
		SelectedProductImage,
		null,
		__JSMessages["ProductImageDialog"],
		true,
		Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
		false,
		false);
}
var SelectedProductImage = function (sender, args) {
    if (args != null && args.src != null) {
        oureditor.pasteHtml(String.format("<img src='{0}' alt='{1}' objectId='{2}' isproductimage='true' />", args.src, args.altText, args.objectId));
    }
}

//For Link to Product option  -- It is not using now.
var EditorLinkToProductDialog;
var selectedLinkToProductObject;

function ShowEditorWithTokenForInsertLinkToProduct(adminpath) {
    oureditor.showExternalDialog(
		adminpath,
		selectedLinkToProductObject,
		845,
		560,
		SelectedLinkToProduct,
		null,
		__JSMessages["LinkToProductDialog"],
		true,
		Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
		false,
		false);
}
var SelectedLinkToProduct = function (sender, args) {
    if (args != null && args.src != null) {
        oureditor.pasteHtml(String.format("<a href='{0}' alt='{1}' objectId='{2}' isLinkToProduct='true' />", args.src, args.altText, args.objectId));
    }
}

//For Insert Flash option
var EditorFlashDialog;
var selectedFlashObject;
var selectedFlashElement = null;
function ShowEditorWithTokenForInsertFlash(adminpath) {
    oureditor.showExternalDialog(
		adminpath,
		selectedFlashObject,
		788,
		498,
		SelectedFlash,
		null,
		__JSMessages["FlashDialog"],
		true,
		Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
		false,
		false);
}

var SelectedFlash = function () {
    var args = popupActionsJson.CustomAttributes.FlashReturnValue;
    CreateFlashPropertiesArray(args);

    var lbr = "{";
    var rbr = "}";
    var altImagePath = "/App_Themes/General/images/FlashManager.gif";
    if (args.altImagePath && args.altImagePath != "undefined" && args.altImagePath != "")
        altImagePath = args.altImagePath;
    var minVersion = "0,0";
    if (args.minVersion && args.minVersion != "undefined" && args.minVersion != "")
        minVersion = args.minVersion + ",0";
    var fSrc = escape(args.filePath);
    fSrc = fSrc.replace(new RegExp("%3A", "g"), ":");
    var flashVars = "";
    if (fSrc.toLowerCase().indexOf(".flv") > 0) {
        flashVars = "file: '" + fSrc + "',autostart:'" + args.autoPlay + "'";
        fSrc = "/FlashPlayer/mediaplayer.swf";
    }

    var flashEvent = String.format("flashembed('{2}', {0}src:'{3}',version:'{4}',wmode:'opaque', quality:'{5}'{1}, {0}{6}{1});", lbr, rbr, args.containerId, fSrc, minVersion, args.quality, flashVars);
    flashEvents += flashEvent;
    flashEvent = "<" + "script type=\"text/javascript\" language=\"javascript\">" + flashEvent + "<" + "/script>";
    var flashHtml = String.format("<div returnValue=\"{0}\" id=\"{1}\" style=\"height:{2}px;width:{3}px;\"><img src=\"{4}\" style=\"height:{2}px;width:{3}px;\" />{5}</div>", FlashPropertiesArray, args.containerId, args.height, args.width, altImagePath, flashEvent);

    if (selectedFlashElement) {
        var selectedParentNode = selectedFlashElement.parentNode;
        selectedParentNode.removeChild(selectedFlashElement);
        oureditor.pasteHtml(flashHtml);
    }
    else {
        flashHtml = String.format("<table cellpadding='0' cellspacing='0'><tbody><tr><td>{0}</td></tr></tbody></table>", flashHtml);
        oureditor.pasteHtml(flashHtml);
    }
}

var FlashPropertiesArray;

function CreateFlashPropertiesArray(returnValue) {
    FlashPropertiesArray = new Array();
    FlashPropertiesArray[0] = escape(returnValue.filePath.replace("%3A", ":"));
    FlashPropertiesArray[1] = returnValue.containerId;
    FlashPropertiesArray[2] = returnValue.width;
    FlashPropertiesArray[3] = returnValue.height;
    FlashPropertiesArray[4] = returnValue.altImagePath.replace("%3A", ":");
    FlashPropertiesArray[5] = returnValue.minVersion;
    FlashPropertiesArray[6] = returnValue.quality;
    FlashPropertiesArray[7] = returnValue.autoPlay;
}

//For Edit Image option
var editedObject;

function fn_InsertProductImageInRadEditor(objImage) {
    oureditor.getSelection().selectRange(currentRange);
    oureditor.pasteHtml(String.format('<img src="{0}" alt="{1}" objectId="{2}" isproductimage="{4}" />',
        objImage.src,
        objImage.altText,
        objImage.objectId,
        'true'));
}

function InsertImageInRadEditor() {
    oureditor.getSelection().selectRange(currentRange);
    var imgSelected = oureditor.getSelection().GetParentElement();
    var extraAttributes = '';
    if (imgSelected != null && imgSelected.tagName.toLowerCase() == 'img') {
        if ($(imgSelected).attr('class') != '') {
            extraAttributes = 'class="' + $(imgSelected).attr('class') + '" ';
        }
        if ($(imgSelected).attr('style') != '') {
            extraAttributes += 'style="' + $(imgSelected).attr('style') + '" ';
        }
    }
    oureditor.pasteHtml(String.format('<img src="{0}" alt="{1}" objectId="{2}" ImageSiteId="{3}" {4} />',
            popupActionsJson.CustomAttributes.ImageUrl,
            popupActionsJson.CustomAttributes.ImageTitle,
            popupActionsJson.SelectedItems[0],
            popupActionsJson.CustomAttributes.ImageSiteId,
            extraAttributes));
}

function InsertSPImageInRadEditor() {
    oureditor.getSelection().selectRange(currentRange);
    oureditor.pasteHtml(String.format('<img src="{0}" alt="{1}" source="SharePoint" />',
            popupActionsJson.CustomAttributes.FileUrl,
            popupActionsJson.CustomAttributes.FileUrl));
}
function InsertCodeSnippet() {
    oureditor.getSelection().selectRange(currentRange);
    if (typeof popupActionsJson.CustomAttributes.SnippetHtml != "undefined")
        oureditor.pasteHtml(popupActionsJson.CustomAttributes.SnippetHtml);
}

function fn_InsertManagedLink() {
    var selectedLinkId = popupActionsJson.CustomAttributes.Id;
    var hrefValue = "[Link:ManagedLink:" + selectedLinkId + "|" + Data.Container.getAttribute("FWContainerID") + "]";
    if (selectedLinkId != '') {
        oureditor.getSelection().selectRange(currentRange);
        var theSelectionObject = oureditor.getSelection();
        var theSelectedElement = theSelectionObject.GetParentElement();
        if (theSelectedElement) {
            var tempSelectedElement = theSelectedElement.parentElement;
            if (tempSelectedElement) {
                if (tempSelectedElement.tagName == "A") {
                    theSelectedElement = tempSelectedElement;
                    imgControl = true;
                }
            }
        }
        if (theSelectedElement) {
            if (theSelectedElement.tagName == "A") {
                theSelectedElement.setAttribute('href', hrefValue);
            }
            else {
                var linkValue = '';
                linkValue = '<a href="' + hrefValue + '">' + selectedText + '</a>';
                oureditor.pasteHtml(linkValue);
            }
        }
    }
    else {
        alert('Encountered an error while inserting link, please try again.');
    }
}

function ShowEditorWithTokenForSnippetManager(snippetManagerAdminPath) {
    oureditor.showExternalDialog(
		snippetManagerAdminPath,
		selectedObject,
		620,
		550,
		SelectedImageInRad,
		null,
		__JSMessages["SnippetManager"],
		true,
		Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move,
		false,
		true);
}

//function PopulateSnippets(editor) {
//    var editorTools;

//    if (typeof (editor) !== "undefined" && typeof (editor) !== "number") {
//        editorTools = oureditor.get_toolAdapter().get_tools();
//    }
//    else {
//        editorTools = oureditor.get_toolAdapter().get_tools();
//    }

//    for (var i = 0; i < editorTools.length; i++) {
//        if (editorTools[i].get_name() == "InsertSnippet") {
//            var snippetDropdown = editorTools[i];
//            var snippetsObject = eval(GetSnippetsFromDb(siteId));
//            var snippetsArray = [];

//            for (var j = 0; j < snippetsObject.length; j++) {
//                var newSnippet = [];

//                newSnippet[0] = snippetsObject[j].Content;
//                newSnippet[1] = snippetsObject[j].Name;

//                snippetsArray[snippetsArray.length] = newSnippet;
//            }

//            snippetDropdown.set_items(snippetsArray);
//        }
//    }
//}

var InsertSnippet = function (snippetHtml) {
    if (snippetHtml != null) {
        oureditor.pasteHtml(snippetHtml);
    }
}

var DeleteSnippet = function () {
    //setTimeout(PopulateSnippets(oureditor), 2000);
}

function findAndReplaceURL(url) {
    var urlToReturn = url;
    var protocol = window.location.protocol;
    if (url.toLowerCase().indexOf(document.domain.toLowerCase()) > 0) {
        urlToReturn = url;
    }
    else if (url.toLowerCase().indexOf("http://") > -1 || url.toLowerCase().indexOf("https://") > -1) {
        urlToReturn = url;
    }
    else {
        var port = window.location.port != "" ? ":" + window.location.port : "";
        if ((protocol.toLowerCase() == "http:" && (port != 80 && port != "")) || (protocol.toLowerCase() == "https:" && port != 443))
            urlToReturn = protocol + '//' + document.domain + port + url;
        else
            urlToReturn = protocol + '//' + document.domain + url;
    }
    return urlToReturn;
}

function OnClientCommandExecuted(editor, args) {
    var command = args.get_commandName();

    if (command === "LinkManager") {
        var dialogOpener = editor.get_dialogOpener();
        var linkManager = dialogOpener._dialogContainers[command];
        linkManager.add_show(function () {
            linkManager.reload();
        });
    }
}
