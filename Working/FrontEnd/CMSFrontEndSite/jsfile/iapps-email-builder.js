﻿var iapps_contactpropertiestxt = '';
$(function () {
    fn_ResetToolbar();

    $(".iapps-email-builder-toolbar .iapps-dropdown").iAppsMegaMenu({ useToken: true, delay: 200 });

	$(".iapps-email-builder").on("click", "#choose_recipients", function () {
        OpeniAppsMarketierPopup("SelectEmailRecipients", "CampaignId=" + EmailBasePage.CampaignId, "fn_CloseEmailPopup");
    });
    $(".iapps-email-builder").on("click", "#schedule_email", function () {
        OpeniAppsMarketierPopup("ScheduleEmailSend", "CampaignId=" + EmailBasePage.CampaignId, "fn_CloseEmailPopup");
    });
    $(".iapps-email-builder").on("click", ".iapps-template-link", function () {
        OpeniAppsMarketierPopup("ChooseTemplate", "SelectTemplate=true", "fn_UpdateEmailTemplate");
    });
    $(".iapps-email-builder").on("click", "#btnViewBestPractices", function () {
        OpeniAppsMarketierPopup("EmailBestPractice");
    });
    $(".iapps-email-builder").on("click", ".iapps-text-only-email", function () {
        OpeniAppsMarketierPopup("TextEmail", "CampaignId=" + EmailBasePage.CampaignId);
    });
    $(".iapps-email-builder").on("click", ".iapps-spam-check-email", function () {
        OpeniAppsMarketierPopup("SpamCheck", "CampaignId=" + EmailBasePage.CampaignId + "&CampaignEmailId=" + EmailBasePage.CampaignEmailId);
        return false;
    });

    $(".iapps-email-details").on("click", ".iapps-toggle-handle", fn_ToggleEmailDetails);

    fn_SetEmailDetailsPosition()

    $(".iapps-email-builder").on("click", ".validate-email-content", function (e) {
        if (iapps_container_contentChanged_ShowAlert) {
            if (!window.confirm("There are some unsaved changes on the page. \nIf you click 'OK', all your changes will be lost. Click 'Cancel' and do 'Save As Draft' to save all your changes.")) {
                e.stopPropagation();
                return false;
            }
        }
    });
});

$(window).resize(function () {
    fn_SetEmailDetailsPosition();
});

function fn_ToggleEmailDetails() {
	if ($('.iapps-email-details').hasClass('expanded')) {
		$('.iapps-email-collapse').slideUp('slow', function () {
			$('.iapps-email-details')
				.removeClass('expanded')
				.addClass('collapsed');
			$('.iapps-email-details .iapps-toggle-handle')
				.prop('src', '/iapps_images/email-details-expand.png')
				.prop('alt', 'Collapse email details');
			$('.iapps-email-details .iapps-email-msg').text('View Email Details and Options');
		});
	}
	else if ($('.iapps-email-details').hasClass('collapsed')) {
		$('.iapps-email-collapse').slideDown('slow', function () {
			$('.iapps-email-details')
				.removeClass('collapsed')
				.addClass('expanded');
			$('.iapps-email-details .iapps-toggle-handle')
				.prop('src', '/iapps_images/email-details-collapse.png')
				.prop('alt', 'Expand email details');
			$('.iapps-email-details .iapps-email-msg').text('Hide Email Details and Options');
		});
	}
}

function fn_SetEmailDetailsPosition() {
	$('.iapps-email-builder .iapps-email-details').css({
		left: $('.iapps-email-builder .iapps-dropdown').offset().left,
		top: $('.iapps-email-builder-toolbar-wrapper').outerHeight(),
        display: 'block'
	});

	$('.iapps-email-builder .iapps-email-preview').css({
		left: $('.iapps-email-builder .iapps-email-builder-actions').offset().left - 200,
		top: $('.iapps-email-builder-toolbar-wrapper').outerHeight() + 12,
		display: 'block'
	});
}

function fn_ShowContactProperties(objTextbox) {
    $('#divInsertContactProperty').iAppsDialog('open');
    iapps_contactpropertiestxt = objTextbox;
    return false;
}

function fn_HideContactProperties() {
    $('#divInsertContactProperty').iAppsDialog('close');
    iapps_contactpropertiestxt = '';
    return false;
}

function fn_InsertContactProperty(objDropdown) {
    var iapps_textbox = $('#' + iapps_contactpropertiestxt);
    var iapps_dropdown = $('#' + objDropdown);
    switch (iapps_contactpropertiestxt) {
        case 'txtEmailSubject':
            iapps_textbox.val(iapps_textbox.val() + ' ' + iapps_dropdown.val());
            break;
        case 'txtSenderName':
        case 'txtSenderEmail':
            iapps_textbox.val(iapps_dropdown.val());
            break;
    }
    fn_HideContactProperties();
    return false;
}

function fn_EmailBuilderCallback() {
	location.reload(true);
}

function fn_ResetToolbar() {
    var entity = ExecuteWebApi("POST", "CampaignService", "GetItem", { "Id": EmailBasePage.CampaignId, "UseCache": false });

    var campaign = {};
    campaign.ScheduleId = $(".iapps-schedule-email input[type='hidden']").val();
    campaign.TotalContacts = 0;
    if (entity != null)
        campaign.TotalContacts = entity.TotalContacts;
    
    if (campaign.TotalContacts > 0)
        $(".iapps-email-builder-steps .choose-recipients").addClass("completed");
    else
        $(".iapps-email-builder-steps .choose-recipients").removeClass("completed");

    if (campaign.ScheduleId != EmptyGuid)
    	$(".iapps-email-builder-steps .schedule-email").addClass("completed");
    else
        $(".iapps-email-builder-steps .schedule-email").removeClass("completed");

    if (campaign.TotalContacts > 0 && campaign.ScheduleId != EmptyGuid)
        $(".iapps-email-builder .confirm-email-overlay").hide();
    else
        $(".iapps-email-builder .confirm-email-overlay").show();

    return campaign;
}

function fn_UpdateEmailTemplate() {
    var newTemplateId = popupActionsJson.SelectedItems.first();
    var oldTemplateId = $(".iapps-select-template input[type='hidden']").val();

    if (!newTemplateId.equals(oldTemplateId)) {
        $(".iapps-select-template input[type='hidden']").val(newTemplateId);
        EmailBasePage.ChangeTemplate();
    }
}

function fn_CloseEmailPopup() {
    if (typeof popupActionsJson.CustomAttributes["ScheduleId"] != "undefined" &&
        popupActionsJson.CustomAttributes["ScheduleId"].length == 36)
        $(".iapps-schedule-email input[type='hidden']").val(popupActionsJson.CustomAttributes["ScheduleId"]);

    fn_ResetToolbar();

    if (popupActionsJson.Action == "ReviewAndSend")
        fn_ConfirmAndSend();

    if (popupActionsJson.Action == "PublishEmail")
    	JumpToAdminPage("marketier");

    if (popupActionsJson.Action == "ManageEmails")
    	fn_JumpToManageEmails();
}

function fn_ConfirmAndSend() {
    EmailBasePage.ConfirmAndSend();
}

function fn_ShowLockedConfirmPopup() {
	OpeniAppsMarketierPopup("ConfirmEmailSchedule", "CampaignId=" + EmailBasePage.CampaignId + "&IsLocked=true", "fn_CloseEmailPopup");
}

function fn_JumpToManageEmails() {
	var token = FWCallback.GetAuthToken(juserName, jUserId, GetjProductId('marketier'), jAppId, false);
	if (token != "") {
		pageUrl = String.format("{0}/Emails/ManageEmails.aspx?Token={1}", jmarketierAdminURL, token);
	}

	window.location.href = pageUrl;
}