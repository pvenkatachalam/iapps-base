var hasCMSLicense;
var hasAnalyticsLicense;
var hasCommerceLicense;
var hasMarketierLicense;
var thisPageLinkedMenus;
var thisMenuId;

function JSSavePage() {
    iapps_container_contentChanged_ShowAlert = false;
    document.getElementById("hfSaveAsDraft").value = "True";
    __doPostBack('__Page', 'SavePageAsDraft');
    //document.forms["form1"].submit();
}

var RefreshPageVariants;
function OpenSegmentationFrame(returnVal, refresh) {
    RefreshPageVariants = refresh;
    $(document).scrollTop(0);
    iAPPS_ShowDeviceFrame(returnVal.DeviceId, returnVal.DeviceText, returnVal.AudienceText, returnVal.NavigateUrl, 'portrait');
    return false;
}

function SubmitToWorkFlowOrPublish(SubmitToWorkFlow) {
    OpeniAppsAdminPopup("SubmitToWorkFlow", "PageId=" + PageId + "&SubmitToWorkFlow=" + SubmitToWorkFlow, "ReloadBasePage");
}

function SubmitForTranslation(SubmitToWorkFlow, popupMode) //popup mode has the values of the TranslationPopupMode enum
{
    OpeniAppsAdminPopup("SubmitForTranslation", "PageId=" + PageId + "&SubmitToWorkFlow=" + SubmitToWorkFlow + "&ObjectTypeId=8&PopupMode=" + popupMode, "ReloadBasePage");
}

function ReloadBasePage() {
    if ((popupActionsJson.Action == "Publish") &&
            typeof popupActionsJson.CustomAttributes["PageRedirectUrl"] != "undefined") {
        window.location.href = popupActionsJson.CustomAttributes["PageRedirectUrl"];
    }
    else
        __doPostBack('', '');
}

function ReloadPage() {
    window.location.href = window.location.href;
}

function GlobalNavItemSelect(sender, eventArgs) {
    var selectedToolbarItem = eventArgs.get_item();
    var selectedToolbarItemId = eventArgs.get_item().get_id();
    if (selectedToolbarItemId == "ViewEditProperties") {
        OpeniAppsAdminPopup("ManagePageDetails", "FromSiteEditor=true&PageId=" + PageId);
        eventArgs.set_cancel(true);
    }
    if (selectedToolbarItemId == "ManagePageVariants") {
        $(document).scrollTop(0);
        OpeniAppsAdminPopup("ManagePageVariants", "ReloadFrame=true&FromSiteEditor=true&PageId=" + PageId, "ReloadPage");
        eventArgs.set_cancel(true);
    }
    if (selectedToolbarItemId == "GoToHome" || selectedToolbarItemId == "ViewPageHistory") {
        SendTokenRequest(sender, eventArgs);
    }
    if (selectedToolbarItemId == "DisplayPageNote") {
        ShowHidePageNote();
        eventArgs.set_cancel(true);
    }
    if (selectedToolbarItemId == "SaveAsDraft" || selectedToolbarItemId == "Publish" || selectedToolbarItemId == "Approve" || selectedToolbarItemId == "SubmitToWorkflow" || selectedToolbarItemId == "Reject" || selectedToolbarItemId == "SubmitForTranslation") {
        iapps_container_contentChanged_ShowAlert = false;

        var inValidContainers = $.grep(iapps_containerInfo, function (e, index) {
            return e.IsRequired == true && e.ContentId == EmptyGuid;
        });

        if (inValidContainers.length > 0) {
            var message = "Please create/insert content for the following containers.\n";
            $.each(inValidContainers, function () {
                message += stringformat("  - {0}\n", this.DisplayTitle)
            });

            alert(message);
            eventArgs.set_cancel(true);
        }
    }
    if (selectedToolbarItemId == "AddToRssChannel") {
        eventArgs.set_cancel(true);
    }

    if ((selectedToolbarItemId == "DraftMode" || selectedToolbarItemId == "ViewWorkflowSequence")) {
        if (WorkflowId != '') {
            OpeniAppsAdminPopup("ViewWorkflowExecution", "objectId=" + PageId + "&workflowId=" + WorkflowId);
        }
        eventArgs.set_cancel(true);
    }
    var id = eventArgs.get_item().get_id();
    if (JumpToAdminCheckLicense(selectedToolbarItemId)) {
        var audienceText = "";
        var deviceText = "";
        if (selectedToolbarItem.get_parentItem() != null && selectedToolbarItem.get_parentItem().get_id() != 'Devices') {
            deviceText = selectedToolbarItem.get_parentItem().get_text();
            audienceText = selectedToolbarItem.get_text();
        }
        else {
            deviceText = selectedToolbarItem.get_text();
            audienceText = "";
        }

        if (id.indexOf('deviceid_') > -1) {

            id = id.replace('deviceid_', '');
            id = id.replace(/_/g, '-');
            iAPPS_ShowDeviceFrame(id, deviceText, audienceText, selectedToolbarItem.get_navigateUrl(), 'portrait');
            sender.hide();
            eventArgs.set_cancel(true);
        }
        else if (id.indexOf('audienceSegmentid_') > -1) {
            var deviceId = selectedToolbarItem.get_parentItem().get_id().replace('deviceid_', '');
            deviceId = deviceId.replace(/_/g, '-');
            iAPPS_ShowDeviceFrame(deviceId, deviceText, audienceText, selectedToolbarItem.get_navigateUrl(), 'portrait');
            sender.hide();
            eventArgs.set_cancel(true);
        }
        else {
            switch (id) {
                case "ToolbarPin":
                    var pinItem = sender.findItemById("ToolbarPin");
                    if (isPinned.toLowerCase() == 'true') {
                        isPinned = 'false';
                        pinItem.set_lookId("UnPinnedTopItemLook");
                    }
                    else {
                        isPinned = 'true';
                        pinItem.set_lookId("PinnedTopItemLook");
                    }
                    var result = PinToolbar(isPinned.toLowerCase());
                    if (result.toLowerCase() == 'false') {
                        alert(__JSMessages["UnableToPinToolbar"]);
                    }
                    eventArgs.set_cancel(true);
                    break;
                case "SharedPage":
                    PositionSharedPageDialog();
                    eventArgs.set_cancel(true);
                    break;
                case "iAPPSHelp":
                    iAPPS_DisplayHelp();
                    eventArgs.set_cancel(true);
                    break;
                case "SendPageLink":
                    OpeniAppsAdminPopup("SendPageLink", "currentPageId=" + PageId + "&currentPageMapId=" + thisMenuId);
                    eventArgs.set_cancel(true);
                    break;
                case 'ViewContentLocations':
                    fn_ShowPageContentVariants();
                    eventArgs.set_cancel(true);
                    break;
            }
        }

    }
    else {
        eventArgs.set_cancel(true);
    }
}

function SetiAPPSContainerInfo(Data, contentId) {
    if (typeof iapps_containerInfo != "undefined") {
        var container = $.grep(iapps_containerInfo, function (e, index) {
            return e.Name.toLowerCase() == Data.Container.getAttribute("ContainerName").toLowerCase();
        });

        if (container && container.length == 1)
            container[0].ContentId = contentId;
    }
}

var iAPPS_SelectedDeviceWidth;
var iAPPS_SelectedDeviceHeight;
var iAPPS_SelectedDeviceItem;
var iAPPS_SelectedAudienceItem;
var iAPPS_SelectedNavigateUrl;
var iAPPS_SelectedDevice;
var iAPPS_SelectedDeviceMode;

function fn_iAPPSSaveDeviceContent() {
    var v_devicehtml = $('div#tablet');
    var v_savebutton = $(v_devicehtml).find('iframe').contents().find('#lbtnSaveDeviceData');
    $(v_savebutton).click();
}

function iAPPS_ShowDeviceFrame(iapps_device, deviceName, audienceName, navigateUrl, iapps_devicemode) {
    if (navigateUrl.indexOf("?") != 0)
        navigateUrl = "?" + navigateUrl;

    var v_deviceurl = document.URL.substring(0, document.URL.indexOf('?'));
    if (v_deviceurl == '')
        v_deviceurl = document.URL;
    var ViewOnly = querystringInNavigateUrl(navigateUrl, 'ViewOnly') == 'true';
    if (ViewOnly) {
        if (!SaveSegmentData())
            return;
    }
    if (!UseDeviceViewForEdit && !ViewOnly) {
        iAPPS_ShowEditDeviceFrame(v_deviceurl + navigateUrl);
    }
    else {
        var itemText = deviceName;
        var iPadDeviceId = 'FEC3A8AC-BF04-4FC6-91FF-1F2EDEABA929';
        var iPhoneDeviveId = '2F322BAB-038D-475C-817D-2A3F141FA690';
        var WebDefaultDeviceId = 'A913EBE9-9986-4EBF-9E5C-EFA1CA418ECA';
        if (audienceName != null && audienceName != "") {
            if (iapps_device.toLowerCase() == iPhoneDeviveId.toLowerCase()) {
                itemText += "<br />" + audienceName;
            }
            else {
                itemText += " - " + audienceName;
            }
        }

        var isDesktop = false;
        if (iapps_device == WebDefaultDeviceId) {
            isDesktop = true;
            iapps_devicemode = "landscape";
            if (audienceName == "")
                return;
        }

        iAPPS_SelectedDeviceMode = iapps_devicemode = iapps_devicemode == 'landscape' ? 'portrait' : 'landscape';
        iAPPS_SelectedDevice = iapps_device;
        $('div#tablet').remove();
        //tablet starts
        var iAPPS_DeviceHTML = '<div style="display: none;" id="tablet">';
        //top-row starts
        iAPPS_DeviceHTML += '<div class="top-row iapps-clear-fix">';
        iAPPS_DeviceHTML += '<img src="/iapps_images/iphone-portrait-speaker.png" alt="" class="iphone-speaker" />';
        iAPPS_DeviceHTML += '<div class="iapps-device-info iapps-clear-fix">';
        iAPPS_DeviceHTML += '<p>' + itemText + '</p>';
        iAPPS_DeviceHTML += '<img class="iapps-device-rotate" src="/iapps_images/rotate_cw.png" alt="rotate" onclick="iAPPS_RotateDevice();" />';
        iAPPS_DeviceHTML += '</div>';
        iAPPS_DeviceHTML += '<div class="iapps-device-toolbar">';
        iAPPS_DeviceHTML += '<input type="button" onclick="iAPPS_HideDeviceFrame();" value="' + __JSMessages['Close'] + '" class="iapps-small-button" />';
        iAPPS_DeviceHTML += '</div>';
        //top-row ends
        iAPPS_DeviceHTML += '</div>';
        //center-row starts
        iAPPS_DeviceHTML += '<div class="center-row iapps-clear-fix">';
        iAPPS_DeviceHTML += '<img src="/iapps_images/iphone-landscape-speaker.png" alt="" class="iphone-speaker" />';
        iAPPS_DeviceHTML += '<div class="data">';
        iAPPS_DeviceHTML += '<div class="iapps-modal loadingSpinner iapps-device-loading"><img src="/iapps_images/spinner.gif" alt="Please wait..." /></div>';
        iAPPS_DeviceHTML += '<iframe src=""></iframe>';
        iAPPS_DeviceHTML += '</div>';
        iAPPS_DeviceHTML += '<div class="center-right">';
        iAPPS_DeviceHTML += '<img src="/iapps_images/home-button.png" alt="home" />';
        iAPPS_DeviceHTML += '</div>';
        //center-row ends
        iAPPS_DeviceHTML += '</div>';
        //bottom-row starts
        iAPPS_DeviceHTML += '<div class="bottom-row">';
        iAPPS_DeviceHTML += '<img src="/iapps_images/home-button.png" alt="home" />';
        //bottom-row ends
        iAPPS_DeviceHTML += '</div>';
        //tablet ends
        iAPPS_DeviceHTML += '</div>';
        $('body').append(iAPPS_DeviceHTML);
        //popup overlay html
        if ($('div#deviceOverlay').size() == 0) {
            $('body').append('<div id="deviceOverlay" class="iAPPSPopupOverlay" style="display: none;z-index: 20002"></div>');
        }
        var iAPPSPopupOverlay = $("div#deviceOverlay.iAPPSPopupOverlay");
        $.each(renderingDevicesJson, function () {
            var device = this;
            if (this.Id.toString().toLowerCase() == iapps_device.toLowerCase()) {
                var v_devicewidth = iapps_devicemode != 'landscape' ? this.PreviewWidth : this.PreviewHeight;
                var v_deviceheight = iapps_devicemode != 'landscape' ? this.PreviewHeight : this.PreviewWidth;
                if (isDesktop) {
                    v_devicewidth = this.PreviewHeight;
                    v_deviceheight = this.PreviewWidth;
                }
                var v_devicehtml = $('div#tablet');
                var v_deviceiframe = $(v_devicehtml).find('iframe');
                var v_loadingPanel = $(v_devicehtml).find(".iapps-device-loading");

                $(v_loadingPanel).css({
                    position: 'absolute',
                    top: (v_deviceheight / 2) - 50,
                    left: (v_devicewidth / 2) - 120
                });

                $(v_deviceiframe).css({
                    width: v_devicewidth + 'px',
                    height: v_deviceheight + 'px'
                });
                var iAPPS_DeviceCSS = "";
                if (iapps_device.toLowerCase() == iPhoneDeviveId.toLowerCase()) {
                    iAPPS_DeviceCSS = 'iphone';
                }
                else if (iapps_device.toLowerCase() == iPadDeviceId.toLowerCase()) {
                    iAPPS_DeviceCSS = 'ipad';
                }
                else {
                    iAPPS_DeviceCSS = jQuery.trim(deviceName).toLowerCase().replace(/ /g, '_');
                }
                $(v_devicehtml).attr('class', iAPPS_DeviceCSS + (iapps_devicemode == 'landscape' ? ' landscape' : ' portrait') + (isDesktop ? ' iapps_generic_device iapps_desktop' : ' iapps_generic_device'));
                $(v_devicehtml).css({
                    position: 'absolute',
                    zIndex: '300003'
                });
                if (!isDesktop && iapps_device.toLowerCase() != iPhoneDeviveId.toLowerCase() && iapps_device.toLowerCase() != iPadDeviceId.toLowerCase()) {
                    $(v_devicehtml).addClass('iapps_generic_device');
                }
                iAPPS_SelectedNavigateUrl = navigateUrl;
                iAPPS_SelectedDeviceItem = deviceName;
                iAPPS_SelectedAudienceItem = audienceName;
                iAPPS_SelectedDeviceWidth = v_devicewidth;
                iAPPS_SelectedDeviceHeight = v_deviceheight;
                if (!isDesktop)
                    $(v_deviceiframe).attr('onload', 'iAPPS_ScaleWebsite(' + v_deviceheight + ', ' + v_devicewidth + ');');

                $(v_deviceiframe).attr('src', v_deviceurl + navigateUrl);

                //display the overlay
                if (!iAPPSPopupOverlay.is(":visible")) {
                    iAPPSPopupOverlay.css({ opacity: 0 });
                    iAPPSPopupOverlay.show().animate({ opacity: 0.5 }, "fast");
                }
                iAPPS_CenterDeviceFrame($(v_devicehtml));
                $(v_devicehtml).show();
            }
        });
    }
}

function iAPPS_ShowEditDeviceFrame(navigateUrl) {
    ShowiAppsLoadingPanel();
    $("#iapps-device-edit-mode").remove();

    var deviceFrame = $("<iframe />").css({
        width: $(window).width() - 40,
        height: $(window.parent).height(),
        position: 'absolute',
        zIndex: '300001',
        border: 0
    });

    deviceFrame.attr("src", navigateUrl + "&PageState=Edit").attr("scrolling", "No");
    deviceFrame.bind("load", function () {
        var content = deviceFrame.contents().find(".iapps-device-edit-content");
        var fHeight = content.height() + 120;
        if (fHeight > deviceFrame.height())
            deviceFrame.height(fHeight);
    });

    var deviceFrameDiv = $("<div id='iapps-device-edit-mode' class='iapps-device-edit-mode' />").css({
        position: 'absolute',
        zIndex: '300001',
        top: $(document).scrollTop(),
        left: 10
    });
    deviceFrameDiv.append(deviceFrame);

    $("body").append(deviceFrameDiv);
}

$(function () {
    //This has to be done, when there is a renderingdevice in query string
    if (GetQueryStringValue('renderingDeviceId') != '') {
        //$(window.parent).on('scroll', function () {
        // This will not working in jQuery < 1.7
        //});

        window.parent.onscroll = function () {
            $(".iapps-device-toolbar-holder").css({
                top: $(this).scrollTop()
            });
        }

        $("body").bind('contentchanged', function () {
            var cHeight = $(".iapps-device-edit-content").height();
            if (cHeight > $(window).height()) {
                $(".iapps-device-edit-content").css({
                    height: $(window).height() - $(".iapps-device-toolbar-holder").height() - 20,
                    "overflow-y": 'auto',
                    "overflow-x": 'hidden'
                });
            }
        });

        $("a[href]").live("click", function (e) {
            if ($(this).parents("#iAPPSToolbar").length == 0 && $(this).parents(".reToolbarWindow").length == 0) {
                e.preventDefault();
                e.stopPropagation();
                alert(__JSMessages["NavigationDisabledInDevicePopup"]);
            }
        });
    }
});

function SaveDeviceWindow() {
    iapps_container_contentChanged_ShowAlert = false;
    return true;
}

function CloseDeviceWindow() {
    if (SaveSegmentData()) {
        if (window.parent.RefreshPageVariants != null)
            window.parent.RefreshPageVariants.call();

        $(window.parent.document).scrollTop(0);
        $("#iApps_LoadingPanel", window.parent.document).remove();
        $(".iAPPSPopupOverlay", window.parent.document).css("z-index", "");

        $("#iapps-device-edit-mode", window.parent.document).fadeOut();
        $("#iapps-device-edit-mode", window.parent.document).remove();
        return false;
    }
    return false;
}

function SaveSegmentData() {
    if (typeof (iapps_container_contentChanged_ShowAlert) != 'undefined' && iapps_container_contentChanged_ShowAlert != null && iapps_container_contentChanged_ShowAlert) {
        return confirm(__JSMessages["PleaseSaveTheSegmentOtherwiseYourChangesWillBeLost"]);
    }

    return true;
}

$(function () {
    $(".iapps-device-loading", window.parent.document).hide();
    $(".iapps-device-loading").hide();
});

function iAPPS_RotateDevice() {
    var v_devicehtml = $('div#tablet');
    var v_temp;
    v_temp = iAPPS_SelectedDeviceHeight;
    iAPPS_SelectedDeviceHeight = iAPPS_SelectedDeviceWidth;
    iAPPS_SelectedDeviceWidth = v_temp;
    iAPPS_ShowDeviceFrame(iAPPS_SelectedDevice, iAPPS_SelectedDeviceItem, iAPPS_SelectedAudienceItem, iAPPS_SelectedNavigateUrl, iAPPS_SelectedDeviceMode);
}

function iAPPS_CenterDeviceFrame(element) {
    var topPosition = 0;
    topPosition = (($(window).height() - element.outerHeight(true)) / 2);
    if (topPosition < 0)
        topPosition = 0;
    topPosition += $(window).scrollTop();
    var leftPosition = (($(window).width() - element.outerWidth(true)) / 2);
    if (leftPosition < 0)
        leftPosition = 0;
    leftPosition += $(window).scrollLeft();
    element.css({
        top: topPosition + "px",
        left: leftPosition + "px"
    });
}

function iAPPS_HideDeviceFrame() {
    setTimeout(function () {
        $('#tablet').hide();
        $('#tablet').remove();
        $('#deviceOverlay').hide();
        $('#deviceOverlay').remove();
    }, 200);
    return false;
}

function iAPPS_ScaleWebsite(v_deviceheight, v_devicewidth) {
    var iAPPS_ScaleDownRatio = 1;
    var iAPPS_DeviceiFrame = $('#tablet').find('iframe');
    var iAPPS_DeviceHeight = $(iAPPS_DeviceiFrame).contents().height();
    var iAPPS_DeviceWidth = $(iAPPS_DeviceiFrame).contents().width();
    if (v_devicewidth < iAPPS_DeviceWidth) {
        iAPPS_ScaleDownRatio = v_devicewidth / iAPPS_DeviceWidth;
        $(iAPPS_DeviceiFrame).contents().find('body').css({
            'transform': 'scale(' + iAPPS_ScaleDownRatio + ')',
            '-ms-transform': 'scale(' + iAPPS_ScaleDownRatio + ')',
            '-ms-transform-origin': '0 0',
            '-webkit-transform': 'scale(' + iAPPS_ScaleDownRatio + ')',
            '-webkit-transform-origin': '0 0',
            '-o-transform': 'scale(' + iAPPS_ScaleDownRatio + ')',
            '-o-transform-origin': '0 0',
            '-moz-transform': 'scale(' + iAPPS_ScaleDownRatio + ')',
            '-moz-transform-origin': '0 0'
        });
    }
}

function JumpToAdminCheckLicense(selectedId) {
    return CheckLicense(selectedId, true);
}


function fn_ShowPageContentVariants() {
    OpeniAppsAdminPopup('PageContentVariants', 'PageId=' + PageId + '&LoadedFromVersion=' + loadedFromVersion);
}

function ClosePageContentVariantsPopup() {
    CloseiAppsAdminPopup();
}

var oureditor;
var srcControl;
function ContentSaveAs() {
    NewContent = true;
    HideEditorToolbar();
    OnClientSubmit(oureditor);
    //check if function js method is implemented by developers before calling it
    if (typeof iAPPS_onContentChange == 'function') {
        iAPPS_onContentChange();
    }
}

function ContentSave() {
    if (Data.HiddenIsMasterContent.value != 'True' || NewContent == true) {
        HideEditorToolbar();
        OnClientSubmit(oureditor);
        ShowContentSaveWarning();
        //check if function js method is implemented by developers before calling it
        if (typeof iAPPS_onContentChange == 'function') {
            iAPPS_onContentChange();
        }
        Data.HiddenIsMasterContent.value = 'False';
    }
    else {
        alert(__JSMessages["YouCanNotEditMasterSiteContent"])
    }
}

function ContentCancel() {
    HideEditorToolbar();
    OnClientCancel(oureditor);
}

function ShowContentSaveWarning() {
    var iappsTop = ($(window).height() - $('#iapps_warning_msg').outerHeight(true)) / 2 + $(window).scrollTop() + "px";
    var iappsLeft = ($(window).width() - $('#iapps_warning_msg').outerWidth(true)) / 2 + $(window).scrollLeft() + "px";
    $('#iapps_warning_msg').css({
        top: iappsTop,
        left: iappsLeft
    });
    $('#iapps_warning_msg').show();
    setTimeout(function () {
        $('#iapps_warning_msg').fadeOut('slow');
    }, 2000);
    //check if function js method is implemented by developers before calling it
    if (typeof iAPPS_onBeforeContentSave == 'function') {
        iAPPS_onBeforeContentSave();
    }
}

var editorTools;
var editor_onclientload = false;
function OnClientLoad(editor, args) {
    oureditor = editor;
    document.getElementById("editableArea").style.display = "none";

    editorTools = editor.get_toolAdapter().get_tools();

    var toolbarWindow = editor.get_toolAdapter().get_window();
    toolbarWindow.set_title(__JSMessages["iAPPSSiteEditorToolbar"]);

    //setTimeout("PopulateSnippets();", 250);
    editor_onclientload = true;
}

function OnClientModeChange(editor, args) {
    if (editor_onclientload) {
        editor_onclientload = false;
    }
    else {
        var mode = editor.get_mode();
        switch (mode) {
            case 1:
                //design mode
                ToggleFloatingToolbar(editor);
                break;
            case 2:
                //html mode
            case 4:
                //preview mode
                HideEditorToolbar();
                break;
        }
    }
}

function OnClientCommandExecuting(editor, args) {
    var name = args.get_name();
    var val = args.get_value();

    if (name == "MailMerge") {
        oureditor.pasteHtml(val);
        //Cancel the further execution of the command as such a command does not exist in the editor command list
        args.set_cancel(true);
    }
}

function OnClientSubmit(editor) {
    var actualText;
    if (Data.Container.getAttribute("maxCharacter") != "-1") {
        if (EditorToUse.toLowerCase() == 'radeditor') {
            actualText = oureditor.get_html(true).substring(0, Data.Container.getAttribute("maxCharacter"));
        }
        else {
            actualText = ckEditor.getData().substring(0, Data.Container.getAttribute("maxCharacter"));
        }

    }
    else {
        if (EditorToUse.toLowerCase() == 'radeditor') {
            actualText = oureditor.get_html(true);
        }
        else {
            actualText = ckEditor.getData();
        }
    }

    if (actualText == '')
        actualText = '&nbsp;';

    var pattern = "^<span class=\"?" + Data.Container.className + "\"?>((.|\\n|\\r)+?)</span>$";
    var objRE = new RegExp(pattern, "i");

    if (objRE.test(actualText)) {
        outStr = actualText.match(objRE);
        actualText = outStr[1];
    }

    srcControl.innerHTML = actualText;
    srcControl.style.visibility = "visible";
    if (EditorToUse.toLowerCase() == 'radeditor') {
        document.getElementById("editableArea").style.display = "none";
    }
    else {
        $('#editorContainer').css('display', 'none');
    }
    //To update the Content operation and IsModified property of the container
    Data.HiddenControlValue.value = actualText;
    if (NewContent) {

        SelectTargetDirectoryToSaveContent();
        setContainerOperation(Data, "NewContent");
        setFwObjectId(Data, "");
        ClearIndexTerms(Data);

        SetiAPPSContainerInfo(Data, NewGuid());
    }
    else {
        if (Data.HiddenContentOperation.value != "NewContent") {
            setContainerOperation(Data, "Modified");
            //added for commerce  done on 06/08/2009 *
            if (Data.Container.getAttribute("FWContainerType") == "FWProductPropertyControl")
                callbackForAutoSave.callback();
            //added for commerce  done on 06/08/2009 *

        }
    }

    NewContent = false;

    eval(flashEvents);
    return false;
}

function OpenBasePageForAudienceSegment(url) {
    basePageAudienceSegment = dhtmlmodal.open('AudienceSegment', 'iframe', url, '', '');
}

//script to avoid the horizontal scroll bar in the view mode
if (document.getElementById("bar")) {
    if (!document.getElementById("autohide")) {
        document.getElementById("bar").style.display = "none";
    }
    else {
        document.getElementById("bar").style.display = "block";
    }
}
