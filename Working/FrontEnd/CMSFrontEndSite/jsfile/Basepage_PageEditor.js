$(function () {
    $(".iapps-page-drop-zone").sortable({
        forcePlaceHolderSize: true,
        revert: true,
        stop: function (evt, ui) {
            if (!ui.item.attr("widgetfqn")) //For sorting nothing to be done
                return;

            var dropZoneId = $(this).attr("controlId");
            var noOfControls = $(this).find(".iapps-zone-item").length;
            var maxAllowedControls = $(this).attr("maxAllowedControls");
            var allowedControls = [];
            if (typeof $(this).attr("allowedControls") != "undefined")
                allowedControls = $(this).attr("allowedControls").split(",");

            var zoneExists = false;
            $.each(zonesDataJson, function () {
                if (this.ZoneId == dropZoneId) {
                    this.AllowedControls = allowedControls;
                    this.MaxAllowedControls = maxAllowedControls;
                    zoneExists = true;
                }
            });

            if (!zoneExists) {
                var zone = {};
                zone.ZoneId = dropZoneId;
                zone.AllowedControls = allowedControls;
                zone.MaxAllowedControls = maxAllowedControls;
                zone.ZoneItems = [];
                zonesDataJson.push(zone);
            }

            $.each(zonesDataJson, function () {
                if (this.ZoneId == dropZoneId) {
                    var dropZone = this;
                    if (dropZone.MaxAllowedControls > 0 && noOfControls >= dropZone.MaxAllowedControls) {
                        ui.item.remove();
                        alert("Maximum controls limit reached.");
                    }
                    else if (IsZoneRestricted(dropZone, ui.item.attr("widgetfqn"))) {
                        ui.item.remove();
                        alert("This control is restricted in this section.");
                    }
                    else {
                        var controlAttr = JSON.parse(JSON.stringify(toolboxDataJson[ui.item.attr("widgetfqn")]));
                        var generatedId = FWCallback.GetRandomString();
                        var newId = dropZoneId + '_' + generatedId;

                        ui.item.replaceWith(stringformat("<img class='iapps-zone-item' src='{0}' alt='{1}' id='{2}' controlId='{2}' />",
                                                controlAttr.ItemDropzoneImageUrl, controlAttr.TypeQualifiedName, newId));

                        controlAttr.ZoneItemId = newId;
                        dropZone.ZoneItems.push(controlAttr);
                    }
                }
            });
        }
    });

    $("#recycle-bin").droppable({
        drop: function (evt, ui) {
            if ($(ui.draggable).parent().hasClass("iapps-page-drop-zone")) {
                if (confirm("Are you sure you want to remove this control?")) {
                    $("#" + $(ui.draggable).attr("Id")).remove();
                    $("#recycle-bin").removeClass("recycle-bin").addClass("recycle-bin-full");
                    var deletedItemId = $(ui.draggable).attr("controlId");
                    $.each(zonesDataJson, function () {
                        var dropZone = this;
                        $.each(dropZone.ZoneItems, function () {
                            if (this.ZoneItemId == deletedItemId)
                                dropZone.ZoneItems.remove(this);
                        });
                    });
                }
            }
        }
    });

    if (!(/msie/.test(navigator.userAgent.toLowerCase()))) {
        $("#iapps-tool-box").draggable({
            scroll: false,
            containment: 'body'
        });
    }

    try{
        $(".iapps-page-drop-zone .iapps-zone-item").live("click", function () {
            $(".iapps-page-drop-zone .iapps-zone-item").removeClass("selected");
            $(this).addClass("selected");
        });
    }
    catch (e) {
        $(".iapps-page-drop-zone .iapps-zone-item").on("click", function () {
            $(".iapps-page-drop-zone .iapps-zone-item").removeClass("selected");
            $(this).addClass("selected");
        });
    }

    $(".iapps-page-drop-zone").each(function () {
        if (typeof $(this).attr("displayTitle") != "undefined" && $(this).attr("displayTitle") != "")
            $("<h4 class='iapps-page-drop-zone-header' />").text($(this).attr("displayTitle")).insertBefore($(this));
    });

    $(".iapps-tool-box-item").draggable({
        revert: "invalid",
        helper: "clone",
        opacity: 0.5,
        connectToSortable: ".iapps-page-drop-zone"
    });
});

function IsZoneRestricted(zone, controlName) {
    var restricted = false;

    if (typeof zone.RestrictedControls != "undefined" && zone.RestrictedControls.length > 0) {
        $.each(zone.RestrictedControls, function () {
            if (this.toLowerCase() == controlName.toLowerCase())
                restricted = true;
        });
    }

    if (typeof zone.AllowedControls != "undefined" && zone.AllowedControls.length > 0) {
        restricted = true;
        $.each(zone.AllowedControls, function () {
            if (this.toLowerCase() == controlName.toLowerCase())
                restricted = false;
        });
    }

    return restricted;
}

function GetPageZonesJSON() {
    /*var pageZonesJSON = [];
    $(".iapps-page-drop-zone").each(function () {
    var zone = {};
    zone.ZoneId = $(this).attr("controlId");
    zone.ZoneItems = [];
    var index = 0;
    $(this).children("img").each(function () {
    var zoneControl = {};
    zoneControl.ZoneItemId = $(this).attr("controlId");
    zoneControl.TypeQualifiedName = $(this).attr("alt");
    zoneControl.ZoneItemIndex = index;
    index++;
    zone.ZoneItems.push(zoneControl);
    });
    pageZonesJSON.push(zone);
    });*/

    //Set the index of each control
    $.each(zonesDataJson, function () {
        var dropZoneItems = $(stringformat(".iapps-page-drop-zone[controlId='{0}'] .iapps-zone-item", this.ZoneId));
        $.each(this.ZoneItems, function () {
            this.ZoneItemIndex = dropZoneItems.index($(stringformat(".iapps-zone-item[controlId='{0}']", this.ZoneItemId)));
        });
    });

    $("#hdnPageEditorJSON").val(JSON.stringify(zonesDataJson));
    $("#hdnPageEditorToolBoxJSON").val(JSON.stringify(toolboxDataJson));

    return true;
}