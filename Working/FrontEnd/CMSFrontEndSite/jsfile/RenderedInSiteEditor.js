﻿var documentProtocol = parent.location.protocol == "https:" ? "https://" : "http://";
var jQueryConflict = false;
var helpButtonNavigateUrl;
////Checking jquery conflicts with other js libraries and referring jQuery UI
setTimeout("UIConflictCheck()", 5000);
function UIConflictCheck() {
    if (jQueryConflict) {
        //removing any conflicts with using $, must use 'jQuery' now instead of '$'
        $.noConflict();
    }
    //Getting shared page data
    if (typeof pageState != "undefined" && pageState != 'emailview') {
        GetSharedPageData();
        if (jQuery.ui && jQuery().draggable) {
            //making the shared page windows draggable
            jQuery('#iAPPS_SharedPage').draggable({
                handle: 'span:first',
                start: function () {
                    if (jQuery('#iAPPS_SharedPage').hasClass('pinned')) {
                        return false;
                    }
                }
            });
        }
    }
    //looping through all the containers gaving the attribute associatepages
    if (typeof pageState != "undefined" && pageState.toLowerCase() == 'edit') {
        jQuery('*').filter('[fwobjectid]').each(function () {
            //if(jQuery(this).attr('fwobjectid') != '00000000-0000-0000-0000-000000000000') {
            var iAPPS_SiteName = "";
            var iAPPS_SharedPages = "";
            //getting the value for the attributes
            var strAssociatedPages = jQuery(this).attr('associatepages');
            if (strAssociatedPages != "") {
                //splitting it for multiple page sharing
                var arrAssocatedPages = strAssociatedPages.split(';');
                iAPPS_SharedPages = '<p class="qTipTitle">' + __JSMessages["SharedContentItem"] + '</p>';
                //creating content for the tooltip for page URLS
                iAPPS_SharedPages += '<p class="qTipText" style="padding-bottom: 0;">' + __JSMessages["ThisContentIsSharedOnTheFollowingPages"] + '</p>';
                iAPPS_SharedPages += '<ul class="qTipSharedPages" style="padding-bottom: 10px;">';
                for (var i = 0; i < arrAssocatedPages.length; i++) {
                    var arrPageDetails = arrAssocatedPages[i].split('|');
                    if (iAPPS_SiteName != arrPageDetails[2]) {  //check if site has changed
                        iAPPS_SiteName = arrPageDetails[2];
                        iAPPS_SharedPages += '<li class="qTipSiteName">' + arrPageDetails[2] + ':</li>';
                        iAPPS_SharedPages += '<li>' + GetDisplayPath(arrPageDetails[3]) + '</li>';
                        continue;
                    }
                    iAPPS_SharedPages += '<li>' + GetDisplayPath(arrPageDetails[3]) + '</li>';
                }
                iAPPS_SharedPages += '</ul>';
            }
            //getting the default tooltip
            var iAPPS_LibraryLocations = jQuery(this).attr('title');
            if (iAPPS_LibraryLocations != undefined && iAPPS_LibraryLocations != '') {
                //creating the content for the content item location in the library            
                iAPPS_SharedPages += '<p class="qTipText" style="padding-bottom: 0;">' + __JSMessages["ThisContentIsStoredInTheFollowingLocation"] + '</p>'
                iAPPS_SharedPages += '<ul class="qTipLibraryLoc"><li>' + GetDisplayPath(iAPPS_LibraryLocations) + '</li></ul>';
            }

            if (jQuery().qtip && iAPPS_SharedPages != '') {
                //creating the tooltip
                jQuery(this).qtip({
                    width: 351,
                    content: {
                        text: iAPPS_SharedPages
                    },
                    position: {
                        my: 'bottom center',
                        at: 'top center',
                        viewport: $(window)
                    },
                    style: {
                        classes: 'ui-tooltip-iapps'
                    },
                    tip: {
                        height: 10,
                        width: 17
                    }
                });
            }
        });
    }
    //}
}

function GetDisplayPath(folderPath) {
    if (folderPath != null && folderPath != undefined) {
        folderPath = folderPath.replace("~/", "");
        folderPath = folderPath.replace(/\//g, " > ");
        return folderPath;
    }
    return "";
}
//toggling display for the shared pages dialog
function PositionSharedPageDialog() {
    var isVisible = jQuery('#iAPPS_SharedPage').is(":visible");

    if (!isVisible) {
        var browserWidth = jQuery(document).width();
        jQuery('#iAPPS_SharedPage').css({
            'left': ($("img#expandArrow").offset().left + 264) + 'px',
            'top': '25px',
            'display': 'block'
        });
    }
    else {
        jQuery('#iAPPS_SharedPage').css({
            'display': 'none'
        });
    }
}
//getting the data for shared pages
function GetSharedPageData() {
    if (typeof (thisPageLinkedMenus) != 'undefined') {
        jQuery('#iAPPS_SharedPageList ul').html('');
        for (var i = 0; i < thisPageLinkedMenus.length; i++) {
            var pageItem = thisPageLinkedMenus[i].split('|');
            listItem = jQuery('<li>' + pageItem[2] + '</li>');
            jQuery('#iAPPS_SharedPageList ul').append(listItem);
        }
    }
}
//expand or collapse the shared page dialog
function iAPPS_SharedPagesToggleCollapse() {
    if (jQuery('#iAPPS_SharedPage').hasClass('iAPPS_SharedPageCollapsed')) {
        jQuery('#iAPPS_SharedPage').removeClass('iAPPS_SharedPageCollapsed');
    }
    else {
        jQuery('#iAPPS_SharedPage').addClass('iAPPS_SharedPageCollapsed')
    }
}
function iAPPS_DisplayHelp() {
    var width = 400;
    var height = 300;
    var left = parseInt((screen.availWidth / 2) - (width / 2));
    var top = parseInt((screen.availHeight / 2) - (height / 2));
    var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
    myWindow = window.open(helpButtonNavigateUrl, "subWind", windowFeatures);
}

/*$(function () {
    $(".iapps-drop-zone").sortable({
        stop: function (evt, ui) {
            var dropZone = $(this);
            $.each($(this).children("div"), function () {
                $("#" + $(this).attr("id") + "DisplayOrderHiddenField").val(dropZone.children("div").index($(this)));
            });
        }
    });
});*/