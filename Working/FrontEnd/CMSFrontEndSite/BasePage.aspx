<%@ Page Language="C#" AutoEventWireup="true" Inherits="BasePage" ValidateRequest="false"
    Theme="General" CodeBehind="BasePage.aspx.cs" EnableEventValidation="false" %>

<%@ Register TagPrefix="Rss" TagName="RssChannel" Src="~/ContextMenuUserControls/RssChannels.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!--Copyright Bridgeline Digital, Inc. An unpublished work created in 2009. All rights reserved. This software contains the confidential and trade secret information of Bridgeline Digital, Inc. ("Bridgeline").  Copying, distribution or disclosure without Bridgeline's express written permission is prohibited-->
    <title></title>
    <asp:PlaceHolder ID="phMetgaTag" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phLinkTags" runat="server"></asp:PlaceHolder>
    <asp:PlaceHolder ID="phScriptTags" runat="server"></asp:PlaceHolder>
    <FWControls:FWScriptHolder ID="phIncludeScriptBlock" runat="server" />
</head>
<body id="bodyTag" runat="server">
    <asp:PlaceHolder ID="phOverlayContainer" runat="server" Visible="false">
        <div id="overlay" class="overlayPlaceHolder">
        </div>
        <div id="treeOverlay" class="overlayPlaceHolder">
        </div>
    </asp:PlaceHolder>
    <FWControls:FWActionLessForm runat="server" ID="form1" method="post">
        <asp:ScriptManager ID="scriptManager" runat="server" ScriptMode="Release" EnableCdn="true" />
        <asp:PlaceHolder ID="phSiteEditorToolbar" runat="server">
            <div id="bar">
                <div class="bottomBorder" id="toolbarContainer">
                    <asp:XmlDataSource ID="ToolbarDataSource" runat="server" DataFile="<%$ Resx:GUIStrings, ToolbarDataSource%>"
                        EnableCaching="true" CacheExpirationPolicy="Absolute" CacheDuration="Infinite"></asp:XmlDataSource>
                    <asp:Panel ID="pnlToolbarMenu" runat="server" CssClass="site-editor-toolbar" />
                </div>
                <div id="autohide" runat="server">
                    <img src="/iapps_images/toolbar-collapse.png" id="expandArrow" onclick="alterToolBar();"
                        alt="<%$ Resx:GUIStrings, ClickToExpandToolbar %>" runat="server" title="<%$ Resx:GUIStrings, ClickToExpandToolbar %>" />
                </div>
            </div>
            <div id="iapps_help" class="help-box">
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phPreviewBar" runat="server" Visible="false">
            <div class="iapps-preview-wrapper">
                <div class="iapps-preview iapps-clear-fix">
                    <div class="iapps-left-column">
                        <asp:Image ID="imgiAPPSSitePreview" runat="server" ImageUrl="~/iapps_images/iapps-site-preview.png"
                            AlternateText="" />
                        <div class="iapps-bottom-row" style="text-align: right;">
                            <asp:Literal ID="litLoggedInUser" Text="Hi&nbsp;" runat="server" />
                            <asp:LoginName ID="lnUsername" runat="server" />
                            <asp:LoginStatus ID="lsLoginStatus" runat="server" LogoutText="<%$ Resx:GUIStrings, SignoutText %>"
                                OnLoggedOut="lsLoginStatus_LoggedOut" />
                        </div>
                    </div>
                    <div class="iapps-right-column iapps-clear-fix">
                        <div class="iapps-page-details">
                            <asp:Literal ID="litEditDetails" runat="server" Text="<%$ Resx:GUIStrings, Preview_EditDetails %>" />
                        </div>
                        <div class="iapps-version-details iapps-clear-fix">
                            <div class="iapps-left-section">
                                <p>
                                    <asp:Literal ID="litPageStatus" runat="server" Text="<%$ Resx:GUIStrings, Preview_Status %>" />
                                </p>
                                <p>
                                    <asp:Literal ID="litPageVersion" runat="server" Text="<%$ Resx:GUIStrings, Preview_Version %>" />
                                </p>
                            </div>
                            <div class="iapps-right-section">
                                <asp:Image ID="imgNotes" runat="server" ImageUrl="~/iapps_images/notes.png" AlternateText="<%$ Resx:GUIStrings, Preview_CommentOnPage%>"
                                    ToolTip="<%$ Resx:GUIStrings, Preview_CommentOnPage%>" onclick="javascript: ShowHidePageNote();" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phDeviceToolbar" runat="server" Visible="false">
            <asp:Panel ID="pnlDeviceToolbar" runat="server" CssClass="iapps-device-toolbar-holder iapps-clear-fix">
                <div class="iapps-device-edit-toolbar iapps-clear-fix">
                    <h2>
                        <asp:Literal ID="litDeviceAudience" runat="server" /></h2>
                    <div>
                        <asp:Button ID="lbtnCloseDevice" runat="server" ClientIDMode="Static" Text="<%$ Resx:GUIStrings, Close %>"
                            CssClass="iapps-button" OnClientClick="return CloseDeviceWindow();" />
                        <asp:Button ID="lbtnPreviewDeviceData" runat="server" ClientIDMode="Static" Text="<%$ Resx:GUIStrings, Preview %>"
                            CssClass="iapps-primary-button" />
                        <asp:Button ID="lbtnSaveDeviceData" runat="server" ClientIDMode="Static" Text="<%$ Resx:GUIStrings, Save %>"
                            CssClass="iapps-primary-button" OnClientClick="return SaveDeviceWindow();" />
                    </div>
                </div>
            </asp:Panel>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="ContextMenuHolder" runat="server"></asp:PlaceHolder>
        <div id="divMessage" runat="server" visible="false">
        </div>
        <asp:PlaceHolder ID="phMarketierStart" runat="server">
            <!-- Start added for Marketier - iAppsTemplateHolderStart -->
            <div id="iAppsTemplateHolderStart">
            </div>
            <!-- End added for Marketier - iAppsTemplateHolderStart -->
        </asp:PlaceHolder>
        <asp:Literal ID="ltTemplateHolderStart" runat="server" />
        <asp:PlaceHolder ID="TemplateHolder" runat="server" EnableViewState="true"></asp:PlaceHolder>
        <asp:Literal ID="ltTemplateHolderEnd" runat="server" />
        <asp:PlaceHolder ID="phMarketierEnd" runat="server">
            <!-- Start added for Marketier - iAppsTemplateHolderEnd -->
            <div id="iAppsTemplateHolderEnd">
            </div>
            <!-- End added for Marketier - iAppsTemplateHolderEnd -->
        </asp:PlaceHolder>
        <div id="editableArea" runat="server" visible="false" clientidmode="Static">
            <radE:RadEditor ID="RadEditor1" runat="server" Visible="false" ToolbarMode="Floating" StripFormattingOptions="MSWord"
                NewLineBr="false" AllowScripts="true" Width="100px" ToolsWidth="718" OnClientLoad="OnClientLoad"
                OnClientCommandExecuting="OnClientCommandExecuting" OnClientModeChange="OnClientModeChange"
                ContentAreaCssFile="~/iAPPS_Editor/EditorContentArea.css" EditModes="Design,Html">
                <Content>
                        &nbsp;
                </Content>
                <CssFiles>
                    <radE:EditorCssFile Value="~/iAPPS_Editor/authorStyles.css" />
                </CssFiles>
            </radE:RadEditor>
        </div>
        <asp:PlaceHolder ID="phCKEditor" runat="server">
            <div id="editorContainer" style="display: none;">
                <div id="iAPPSToolbar">
                    <p>
                        <strong>
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resx:JSMessages, iAPPSSiteEditorToolbar %>" />
                        </strong>
                    </p>
                    <div id="cktoolbarContainer">
                    </div>
                </div>
                <CKEditor:CKEditorControl ID="ckEditor" runat="server" AutoGrowMinHeight="200" EnterMode="P"
                    BasePath="~/iAPPS_Editor" Width="100" Height="100" Entities="true" HtmlEncodeOutput="true"
                    IgnoreEmptyParagraph="true" BrowserContextMenuOnCtrl="true" EntitiesGreek="true"
                    ScaytAutoStartup="false" ShiftEnterMode="BR" TemplatesReplaceContent="false"
                    SharedSpacesTop="cktoolbarContainer" ForceSimpleAmpersand="true" ToolbarCanCollapse="false">
                </CKEditor:CKEditorControl>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phAutoSave" runat="server" />
        <asp:Panel ID="pnlPageNote" runat="server" Visible="false">
            <div id="pageNote" class="iapps-modal" onmouseover="SetPageNoteOpacity(true);" onmouseout="SetPageNoteOpacity(false);"
                style="display: none;">
                <div class="pageNoteHeader modal-header iapps-clear-fix">
                    <h2 onmousedown="MovePageNote(event);">
                        <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, PageNotes %>" /></h2>
                    <a href="javascript:PageNote_ExpandCollapse();" id="pageNoteExpandCollapse">
                        <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, ExpandCollapse %>" /></a>
                </div>
                <div id="pageNoteContent">
                    <div class="modal-content iapps-clear-fix">
                        <div id="pageNoteList">
                            <ul>
                            </ul>
                        </div>
                        <textarea id="txtPageNote" class="pageNoteTextbox" rows="3" cols="40"></textarea>
                        <span id="pageNoteInstructions" style="display: block;">
                            <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, ClickToAddACommentCommentsEnteredHereWillBeSavedWhenThePageMovesThroughWorkflow %>" /></span>
                    </div>
                    <div class="modal-footer iapps-clear-fix">
                        <input id="chkPageNoteHighPriority" type="checkbox" /><label for="chkPageNoteHighPriority"><asp:Localize
                            runat="server" Text="<%$ Resx:GUIStrings, Highlight %>" /></label>
                        <button id="btnPageNoteSave" class="iapps-button" onclick="PageNote_Insert();return false;">
                            <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, SaveNote %>" />
                        </button>
                    </div>
                </div>
            </div>
            <script src="/js/PageNote.js" type="text/javascript"></script>
        </asp:Panel>
        <asp:PlaceHolder ID="pnlSharedWithMenus" runat="server" Visible="false">
            <div id="iAPPS_SharedPage" class="iapps-modal">
                <div class="iAPPS_SharedPageHeader modal-header iapps-clear-fix">
                    <h2>
                        <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, SharedPage %>" /></h2>
                    <a href="javascript:iAPPS_SharedPagesToggleCollapse();" id="iAPPS_ExpandCollapseDialog">
                        <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, ExpandCollapse %>" />
                    </a>
                </div>
                <div id="iAPPS_SharedPageContent" class="modal-content iapps-clear-fix">
                    <p>
                        <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, ThisPageExistsInTheFollowingMenus %>" />
                    </p>
                    <div id="iAPPS_SharedPageList">
                        <ul>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer clear-fix">
                    <input type="button" value="<%$ Resx:GUIStrings, Close %>" title="<%$ Resx:GUIStrings, Close %>"
                        class="iapps-button" onclick="PositionSharedPageDialog(false);" runat="server" />
                </div>
            </div>
            <div id="iapps_warning_msg" class="iapps-warning-msg">
                <p>
                    <asp:Localize runat="server" Text="<%$ Resx:GUIStrings, SavePageWarning %>" />
                </p>
            </div>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phSiteEditorLogout" runat="server">
            <asp:HiddenField ID="hfSaveAsDraft" runat="server" Value="False" />
            <asp:Button ID="btnLogoutFromSiteEditor" OnClick="btnLogoutFromSiteEditor_Click"
                runat="server" Style="display: none;" />
            <script type="text/javascript">
                function LogOutFromSiteEditor() {
                    var btnLogoutFromSiteEditor = document.getElementById('<%=btnLogoutFromSiteEditor.ClientID %>');
                    __doPostBack(btnLogoutFromSiteEditor.name, null);
                }
            </script>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phDebugInfo" runat="server" Visible="false">
            <div class="iapps-debug-info" id="iapps-debug-info">
                <div class="iapps-debug-header">
                    <h2>Debug Information</h2>
                    <img alt="close" src="/iapps_images/close.gif" onclick="document.getElementById('iapps-debug-info').style.display = 'none';" />
                </div>
                <div class="iapps-debug-content">
                    <asp:Repeater ID="rptDebugInfo" runat="server">
                        <HeaderTemplate>
                            <table>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# Eval("Key") %>
                                </td>
                                <td>
                                    <%# Eval("Value") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </asp:PlaceHolder>
        <script type="text/javascript" src="/jsfile/Basepage.js"></script>
    </FWControls:FWActionLessForm>
    <div id="RadAbc" runat="server">
        <script type="text/javascript" src="/iAPPS_Editor/radeditor_custom.js"></script>
        <script type="text/javascript" src="/iAPPS_Editor/ckeditor_custom.js"></script>
    </div>
    <FWControls:FWScriptHolder ID="phStartUpScriptBlock" runat="server" />
</body>
</html>
